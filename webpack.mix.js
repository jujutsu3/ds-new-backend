let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
   'public/css/bootstrap.min.css',
   'public/css/magnific-popup.cs',
   'public/css/jquery.selectBox.css',
   'public/css/dropzone.css',
   'public/css/rangeslider.css',
   'public/css/animate.min.css',
   'public/css/leaflet.css',
   'public/css/map.css',
   'public/css/jquery.mCustomScrollbar.css',
   'public/fonts/font-awesome/css/all.min.css',
   'public/fonts/flaticon/font/flaticon.css',
   'public/css/slick.css',
   'public/css/slick-theme.css',
   'public/css/animate.css',
   'public/css/swiper.css',
   'public/css/owlCarousel.css',
   'public/css/style2.css',
   'public/css/skins/white.css',
   'public/LineIcons/LineIcons.min.css',
], 'public/css/all.css');
mix.combine([
   'resources/assets/js/jquery-3.3.1.min.js',  
   'resources/assets/js/popper.min.js', 
   'resources/assets/js/bootstrap.min.js',   
    'resources/assets/js/jquery.selectBox.js',
    'resources/assets/js/rangeslider.js',
    'resources/assets/js/jquery.magnific-popup.min.js',
    'resources/assets/js/jquery.filterizr.js',
    'resources/assets/js/wow.min.js',
    'resources/assets/js/backstretch.js',
    'resources/assets/js/jquery.countdown.js',
    'resources/assets/js/jquery.scrollUp.js',
    'resources/assets/js/particles.min.js',
    'resources/assets/js/typed.min.js',
    'resources/assets/js/dropzone.js',
    'resources/assets/js/jquery.mb.YTPlayer.js',
    'resources/assets/js/leaflet.js',
    'resources/assets/js/slick.min.js',
    'resources/assets/js/leaflet-providers.js',
    'resources/assets/js/leaflet.markercluster.js',
    'resources/assets/js/maps.js',
    'resources/assets/js/ie-emulation-modes-warning.js',
    'resources/assets/js/owlCarousel.js',
    'resources/assets/js/swiper.js',
], 'public/js/allNew.js')
   .sass('resources/assets/sass/app.scss', 'public/css');
