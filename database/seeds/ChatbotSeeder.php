<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Chatbot;

class ChatbotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
        |--------------------------------------------------------------------------
        | Seeder For Pertanyaan Umum
        |--------------------------------------------------------------------------
        */
        Chatbot::create([
            'greetings' => 'Assalamualaikum, Wr, Wb,.
            Terima kasih telah menghubungi Danasyariah, Silahkan pilih terlebih dahulu kategori yang sesuai dengan profil Anda:
            
            1. Pertanyaan Umum
            2. Pemberi Pendanaan (Lender)
            3. Penerima Pembiayaan (Borrower)
            
            Silahkan lanjut dengan mengetik pilihan nomor sesuai pilihan Anda.
            
            Silahkan ikuti media sosial Danasyariahid dan Download Applikasinya di Play Store dan Apps Store!
            ',
            'category_id' => 1,
            'category' => 'Hallo, Teman Syariah! Apa yang dapat kami bantu hari ini?

            1. Apa itu Danasyariah?
            2. Apa saja produk dan layanan dari Danasyariah?
            3. Bagaimana akad yang digunakan Danasyariah?
            4. Apakah ada pinjaman konsumtif?
            
            Silahkan lanjut dengan mengetik nomor sesuai pilihan Anda.
            
            Silahkan ikuti media sosial Danasyariahid dan Download Applikasinya di Play Store dan Apps Store!',
            'question_id' => 1,
            'question' => 'Danasyariah.id adalah P2P lending yang fokus melayani dibidang pendanaan dan pembiayaan developer proyek properti rumah. Danasyariah.id mulai berdiri pada 2017, terdaftar OJK pada 8 Juni 2018 dan berizin di OJK pada 23 Februari 2021. Serta DSI juga memiliki Dewan Pengawas Syariah.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 1,
            'question_id' => 2,
            'question' => 'Layanan kami hanya memberikan 3 produk, yakni:
            1. Pendanaan proyek (menabung/deposito diproyek properti pilihan).
            2. Pembiayaan kepemilikan dana secara syariah.
            3. Pembiayaan kontruksi secara syariah.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 1,
            'question_id' => 3,
            'question' => 'Image Alur Akad',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 1,
            'question_id' => 4,
            'question' => 'Mohon maaf Danasyariah.id tidak menyediakan layanan pinjaman seperti pinjaman KTA, pinjaman modal usaha/UMKM, dan pinjaman uang tunai lainnya.
            Layanan kami saat ini terbatas untuk menjadi Pendana dan Penerima Pendanaan Proyek Properti saja.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);


        /*
        |--------------------------------------------------------------------------
        | Seeder For Lender
        |--------------------------------------------------------------------------
        */

        // Start Category 2 Question 1 + Answer
        Chatbot::create([
            'code' => 'regis',
            'category_id' => 2,
            'category' => 'Hallo, Pendana Halal! Apa yang dapat kami bantu hari ini?

            1. Registrasi & Akun
            2. Top Up Dana
            3. Pilih Proyek yang didanai
            4. Imbal Hasil
            5. Dokumen Pendanaan melalui Danasyariah
            6. Ketentuan Pajak
            7. Penarikan Dana

            Silahkan lanjut dengan mengetik nomor sesuai pilihan Anda.

            Silahkan ikuti media sosial Danasyariahid dan Download Applikasinya di Play Store dan Apps Store!',
            'question_id' => 1,
            'question' => 'Registrasi & Akun
            1. Bagaimana cara mendaftar sebagai Pendana?
            2. Bagaimana untuk melakukan perubahan data?
            3. Bagaimana jika akun tersuspend?
            4. Bagaimana lupa kata sandi?
            5. Bagaimana lupa semua akses yang berhubungan dengan akun DSI, atau ahli waris yang mau akses akun Pendana meninggal?
            6. Bagaimana terjadi kendala?
            7. Siapa yang boleh melakukan Pendanaan di Dana Syariah?
            8. Mengapa ada pembatasan terhadap nilai pendanaan?
            ',
            'answer_id' => 1,
            'answer' => 'Cara mendaftar sebagai Pendana dan/atau Penerima Dana, melalui aplikasi dan website.
            Silakan klik link dibawah ini:
            a) Aplikasi
            - Playstore
            https://play.google.com/store/apps/details?id=com.danasyariah.mobiledanasyariah
            
            -Appstore
            https://apps.apple.com/id/app/dana-syariah/id1461445952
            
            b) Website
            https://www.danasyariah.id/
            
            Pendaftaran menjadi Pendana dan Penerima Pendanaan wajib mengisi data lengkap sesuai permintaan sistem.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 1,
            'answer_id' => 2,
            'answer' => 'Mengenai Perubahan Data hanya dapat Admin CSO Danasyariah.id yang dapat ubahkan. 
            Mohon isikan form dibawah ini dan sertakan data yang butuh diubahkan:
            
            1. Perubahan No Rek Imbal Hasil.
            -No Rek Lama:
            -Nama Pemilik Lama:
            -Nama Bank Lama:
            
            -No Rek Baru:
            -Nama Pemilik Baru:
            -Nama Bank Baru:
            
            2. Perubahan No Tlp.
            -Nomor Kontak Lama:
            -Nomor Kontak Baru:
            
            3. Perubahan Alamat E-mail.
            -E-mail Lama:
            -E-mail Baru:
            
            VERIFIKASI PERUBAHAN DATA ANDA. WAJIB DI SERTAKAN
            - Foto KTP
            - Foto Selfie/Diri memegang KTP
            - Username/Alamat Email yang terdaftar di akun Danasyariah.id.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 1,
            'answer_id' => 3,
            'answer' => 'Mengenai Aktifkan Akun karena tersuspend dan hanya dapat diubahkan oleh Admin Dana Syariah.

            Mohon sertakan data, agar Admin bantu Aktifkan kembali akun pada sistem:
            
            Mohon Isi data yang butuh oleh Admin
            1. Username :
            Nama Jelas :
            Alamat Email :
            Nomor HP :
            
            VERIFIKASI DATA AKUN TERSUSPEND. WAJIB DI SERTAKAN
            - Foto KTP
            - Foto Selfie/Diri memegang KTP',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 1,
            'answer_id' => 4,
            'answer' => 'Kendala lupa password/username bisa menggunakan menu klik LUPA KATA SANDI?.
            Setelah klik menu tersebut, Anda akan memasukkan alamat e-mail dan mendapatkan link perubahan.
            
            Mohon ikuti beberapa syarat penggantian kata sandi:
            
            1. Minimal 8 karakter
            2. Huruf besar
            3. Hurud kecil
            4. Karakter angka
            5. Karakter special, berupa: !@#$%^&*
            
            Jika sudah sesuai dengan 5 syarat diatas, silahkan memulai login kembali.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 1,
            'answer_id' => 5,
            'answer' => 'Jika Anda mengalami kesulitan akses akun Danasyariah.id dikarenakan Email lupa password/blokir/off.
            Mohon isikan verifikasi data sebagai berikut:
            a)Nama lengkap:
            b)Nomor Tlp/WA aktif:
            c)Alamat Email terdaftar:
            d)Foto KTP (untuk dicocokan data sistem dengan KTP terdaftar).
            Jika sudah tersedia semua, akan kami bantu secara manual kembalikan akun Anda dan password default dari team internal kami. Lalu coba ubah kembali ke password milik Anda sendiri.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 1,
            'answer_id' => 6,
            'answer' => 'Mengenai kendala yang terjadi mohon isikan data dibawah ini, agar Admin dan Team terkait dapat memberikan solusi kendala saat ini.

            Nama Lengkap:
            Username:
            Email:
            No HP:
            Akses melalui;
            - Tipe dan Seri smartphone:
            - Provider:
            - Lokasi:
            
            NB: Mohon sertakan Screen Shotan kendalanya
            Terima kasih atas kerja samanya',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 1,
            'answer_id' => 7,
            'answer' => 'Warga Negara Indonesia (WNI) yang memiliki Identitas yang berlaku yaitu KTP dan NPWP atau Warga Negara Asing (WNA) yang mempunyai identitas Warga Negara di negara masing-masing.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 1,
            'answer_id' => 8,
            'answer' => 'Pertama, adanya aturan OJK yang membatasi jumlah pendanaan maksimum untuk setiap proyek, Kedua, batasan maksimum pendanaan dilakukan untuk mendorong pemilik dana mendiversifikasikan pendanaanya pada banyak pendanaan yang ditawarkan yang berarti mengurangi risiko pendanaan jika terjadi kegagalan pembayaran dari pemilik proyek atau wanprestasi.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 2 Question 1 + Answer

        // Start Category 2 Question 2 + Answer
        Chatbot::create([
            'code' => 'topup',
            'category_id' => 2,
            'question_id' => 2,
            'question' => 'Top Up Dana
            1. Kelanjutan setelah mendaftar bagaimana?
            2. Cara Isi dana ke VA yang disediakan?
            ',
            'answer_id' => 1,
            'answer' => 'Jika sudah mendaftarkan diri sampai mengisi data secara lengkap.
            Anda akan mendapatkan nomor Virtual Account (VA), yakni:
            a)Bank BNI - 957501xxxx
            b)Bank CIMB Niaga Syariah - 955911xxxx
            
            Silakan mengisi dana terlebih dahulu ke nomor VA (kami sarankan ke BNI agar terhindar kendala retur). Sesuai nominal keinginan Anda, lalu mulai alokasikan ke proyek pilihan Anda.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 2,
            'answer_id' => 2,
            'answer' => 'Cara isi dana ke dalam Virtual Account (VA) yang sudah Anda danai dari Mbanking diluar Bank BNI dan CIMBNSyariah.
            Lakukan cara-cara dibawah ini:
            1. Masuk ke Mbanking Anda
            2.Pilih Transfer antarbank lain/rekening lain (Tidak disarankan antar VA)
            3.Pilih bank BNI/CIMBNSyariah (tergantung mau isi dana ke VA yang mana)
            4.Ketik nomor rekening VA Anda
            5.Ketik nominal yang ingin ditransfer
            6.Selesai.
            
            Detailnya dapat lihat ke masing-masing akun untuk cara transfer dana melalui berbagai macam cara.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 2 Question 2 + Answer

        // Start Category 2 Question 3 + Answer
        Chatbot::create([
            'code' => 'proyek',
            'category_id' => 2,
            'question_id' => 3,
            'question' => 'Pilih Proyek yang didanai
            1. Cara Alokasikan dana ke Proyek pilihan?
            2. Cara memilih proyek yang baik?
            3. Bagaimana dengan resiko mendanai?
            4. Bagaimana mitigasi resiko?
            5. Kenapa nama developer disingkat?
            6. Bagaimana Progres proyek melalui video/foto?
            7. Apakah bisa berkunjung ke lokasi proyek?
            8. Bagaimana dengan legalitas proyek?
            9. Bagaimana cara mencari alamat proyek? (GDRIVE)
            ',
            'answer_id' => 1,
            'answer' => 'Cara Alokasikan dana ke proyek pilihan Anda, dengan cara:
            1. Login ke akun Danasyariah.id Anda.
            2.Pilih menu “KELOLA PENDANAAN”.
            3.Pilih menu Pendanaan Aktif dan Lihat Semua.
            4.Klik Proyek sesuai keinginan Anda
            5.Klik Pilih Pendanaan
            6.Ketikkan Paket dana yang mau dialokasikan (Jika 10,000,000 maka mohon ketik 10 saja, dst).
            7.Selesai',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 2,
            'answer' => 'Cara memilih proyek pendanaan yang baik atau aman, bisa dengan melihat proyek tersebut masuk ke Grade kategori resiko yang mana.
            Jika masuk grade A dan B jauh lebih aman karena proteksinya double dengan adanya asuransi syariah.
            Grade C tanpa asuransi syariah-pun aman, karena awal berdiri hanya menggunakan mitigasi resiko dengan minimal 150% agunan.
            Bisa juga melihat dari seringnya PT Developer yang listing pendanaan di Pendanaan sedang berlangsung.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 3,
            'answer' => 'Kemungkinan terjadinya kerugian terhadap pendanaan yang telah dilakukan pendana. Dalam hal ini risiko tersebut meliputi dan tetapi tidak terbatas pada:
            -Keterlambatan Pembayaran Imbal Hasil.
            -Keterlambatan Pengembalian dana pokok pendanaan.
            -Proses eksekusi jaminan yang tidak dapat di prediksi secara akurat baik dari sisi maupun harga.
            -Faktor-faktor lain yang diluar kendali penerima pendanaan dan diluar kendali Dana Syariah.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 4,
            'answer' => 'Dana Syariah dengan usaha terbaiknya melakukan mitigasi risiko pembiayaan yang meliputi
            -Pengecekan objek pembiayaan secara komprehensif,
            -Karakter penerima pembiayaan,
            -Kapasitas dan kondisi perusahaan serta memastikan mempunyai agunan dengan nilai taksasi yang memenuhi persyaratan yang telah ditentukan.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 5,
            'answer' => 'Nama - nama Developer yang menjadi listing pendanaan di Danasyariah.id disingkat karena peraturan dari OJK dibawah ini menyatakan bahwa pihak ketiga direkomdasikan tidak di ekspos terkait dengan sifat informasi yang confidential.
            Kami sesuaikan semua informasi dan peraturan sesuai dengan arahan OJK yang akan terlampir link, yang dapat Bapak/Ibu pelajari secara mandiri.
            
            Terlampir link yang bisa diakses: https://www.ojk.go.id/id/regulasi/otoritas-jasa-keuangan/peraturan-ojk/Documents/Pages/POJK-Nomor-77-POJK.01-2016/SAL%20-%20POJK%20Fintech.pdf',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 6,
            'answer' => 'Mengenai progres proyek melalui foto atau video dapat Anda saksikan di akun Youtube resmi kami pada link dibawah ini:
            https://www.youtube.com/watch?v=nqFj2pwwZAY&list=PLJlgBqm_O9gNYtyarfBQZGKagM036Ogc0',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 7,
            'answer' => 'Anda bisa berkunjung ke lokasi proyek yang sudah Anda danai, mohon informasikan data berikut:
                Nama lengkap:
                Nomor Tlp/WA aktif:
                Email Terdaftar:
                Proyek yang mau dikunjungi:
                Screenshot bukti pendanaan:
                
                Akan kami koordinasikan ke team proyek atas permintaan Anda berkunjung ke proyek yang Anda danai, mohon menunggu minimal 1x24 jam informasi selanjutnya.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 8,
            'answer' => 'Terkait permintaan legalitas proyek yang sudah Anda danai mohon isikan data dibawah ini;
            Nama lengkap:
            E-mail:
            No. Pendanaan Proyek:
            Bukti Pendanaan:
            
            Legalitas yang kami berikan adalah berupa Ikhtisar Pembiayaan Proyek Properti, yakni merupakan surat persetujuan pengajuan pendanaan yang di berikan ke borrower/penerima pendanaan, sebagai bukti persetujuan bahwa pendanaan yang borrower ajukan telah di setujui oleh Danasyariah.id.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 3,
            'answer_id' => 9,
            'answer' => 'CSO bisa cek daftar nama Developer pada GDrive dibawah ini, infokan ke pendana yang sudah mendanai proyek saja.
            Jika belum tidak diperkenankan diberikan info detail pada GDrive ini.
            https://docs.google.com/spreadsheets/d/1jKXlmTlWzre7hk0ly4w6rNO2RAEYGpqpm2n3DtIuRvw/edit#gid=0',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 2 Question 3 + Answer

        // Start Category 2 Question 4 + Answer
        Chatbot::create([
            'code' => 'imbal hasil',
            'category_id' => 2,
            'question_id' => 4,
            'question' => 'Imbal Hasil
            1. Berapa besar imbal bagi hasil bulanan dan kapan dana pendana bisa dikembalikan
            2. Bagaimana dengan simulasi hitungan Imbal Hasil?
            ',
            'answer_id' => 1,
            'answer' => 'Imbal bagi hasil bulanan yaitu sebesar 1% dari bagi hasil yang disepakati selama periode pembiayaan, dibayarkan bulanan dan langsung di transfer ke rekening pendana yang terdaftar. Sisa imbal hasil yang belum dibayarkan (jika ada) akan dibayarkan pada akhir periode pembiayaan.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 4,
            'answer_id' => 2,
            'answer' => 'Rumus Dasar Imbal Hasil Dana Syariah;

            Imbal Hasil 18%
            Tenor/lama proyek 5 bulan
            Dana Pokok Rp 1,000,000
            
            Hitung: (18% / 12 bulan) x 5 bulan = 7,5% besaran imbal hasil yang Anda dapatkan selama 5 bulan ini.
            
            Hitung Imbal Hasil: Dana pokok x Imbal Hasil selama 5 bulan = Rp 1,000,000 x 7,5% = Rp 75,000
            Pemberian imbal bulanan Danasyariah.id;
            - Bulan 1 (Rp 1,000,000 x 1%) = Rp 10,000
            - Bulan 2 (Rp 1,000,000 x 1%) = Rp 10,000
            - Bulan 3 (Rp 1,000,000 x 1%) = Rp 10,000
            - Bulan 4 (Rp 1,000,000 x 1%) = Rp 10,000
            - Bulan 5 (Rp 1,000,000 x 1%) = Rp 10,000 
            - di bulan yang sama (bulan 5), sisa imbal hasil 2,5% x Rp 1,000,000 = Rp 25,000
            Total semua adalah Rp 75,000.
            
            *Imbal hasil bulanan 1% diberikan langsung Danasyariah.id transferkan ke rekening pribadi Anda tanpa ada potongan biaya apapun.
            *Imbal hasil ditransferkan setiap tanggal mulai proyek yang bisa dicek pada deskripsi proyek tanggal mulai proyek',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 2 Question 4 + Answer

        // Start Category 2 Question 5 + Answer
        Chatbot::create([
            'code' => 'dokumen',
            'category_id' => 2,
            'question_id' => 5,
            'question' => 'Dokumen Pendanaan melalui Danasyariah
            1. Cara mendownload akad wakalah bilujroh yang biasa?
            2. Cara mendownload akad wakalah bilujroh yang digital?
            3. Jika kendala akses PRIVYID?
            4. JIka pendana meninggal dunia, apa yang harus dilakukan?
            ',
            'answer_id' => 1,
            'answer' => 'Jika total pendanaan dana Anda kurang dari 75,000,000 Anda bisa langsung memilih mengunduh akad wakalah yang keterangannya “BIASA”. 
            1. Login pada akun Anda.
            2. Pilih menu PROFILE.
            3. Pilih unduh akad wakalah bilujrah.
            Secara otomatis akan terdownload ke dalam device Anda, pastikan jaringan internet Anda stabil.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 5,
            'answer_id' => 2,
            'answer' => 'Jika total pendanaan dana Anda lebih dari 75,000,000 Anda bisa langsung memilih mengunduh akad wakalah yang keterangannya “DIGITAL”. 
            a)Login pada akun Anda.
            b)Pilih menu PROFILE.
            c)Pilih unduh akad wakalah bilujrah secara DIGITAL.
            d)Ikuti langkah tanda tangan dengan PRIVYID sesuai dengan popup informasi pada aplikasi.
            Secara otomatis akan terdownload ke dalam device Anda, pastikan jaringan internet Anda stabil.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 5,
            'answer_id' => 3,
            'answer' => 'Jika Anda bermasalah saat menggunakan PrivyID. Yang harus dilakukan adalah:
            1)Mohon upload ulang lagi data (Foto diri, foto diri memegang KTP dan KTP Anda) dengan jelas dan pencahayaan yang terang.
            2)Mohon refresh aplikasi setelah upload foto-foto terbaru.
            3)Mohon lakukan Unduh Akad kembali.
            4)Jika masih ada notifikasi ”Foto Tidak Sesuai/Foto Tidak Jelas/lainnya”. lanjut ke tahap ke 1:
            5)Buka Email Anda yang terdaftar di Danasyariah.id, ada email dari PrivyID.
            6)Mohon login ke privyID melalui websitenya, dengan Username&Password yang sudah disediakan pada email tsb.
            7)Mohon upload foto KTP Anda yang jelas. Menunggu verifikasi dari privyID minimal 30 menit.
            8)Setelah kurang lebih 30 menit, ke Aplikasi Danasayariah.id lagi untuk unduh akad.
            9)Selesai',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 5,
            'answer_id' => 4,
            'answer' => 'Salam hangat Bapak/Ibu Pendana, mengenai permintaan perubahan data yang disebabkan meninggalnya pendana selaku pemilik akun di Danasyariah.id.

            Mohon lengkapi beberapa dokumen pendukung permintaan perubahan dibawah ini:
            a)Fotocopy Surat Keterangan Kematian Pemberi Pembiayaan dari kelurahan
            b)Fotocopy Surat Keterangan Ahli Waris dari kelurahan
            c)Fotocopy Putusan Penetapan ahli waris dari Pengadilan (jika ada)
            d)Surat pernyataan tidak ada sengketa dari para ahli waris
            e)Fotocopy KTP Pemberi Pembiayaan
            f)Fotoccopy KTP Ahli Waris
            g)Fotocopy Kartu Keluarga Pemberi Pembiayaan
            h)Surat Permohonan pencairan
            
            Dokumen diatas mohon dilampirkan melalui e-mail CSO Danasyariah.id cso@danasyariah.id dengan subjek Dokumen Permintaan Perubahan Data kepada Ahli Waris Pendana.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 2 Question 5 + Answer

        // Start Category 2 Question 6 + Answer
        Chatbot::create([
            'code' => 'pajak',
            'category_id' => 2,
            'question_id' => 6,
            'question' => 'Ketentuan Pajak
            1. Apakah PT DSI akan menerapkan Pajak?
            2. Bagaimana hitungan pemotongan Pajak Pph 23?
            3. Pendana yang belum memiliki NPWP:
            4. Pendana yang belum melakukan pembaharuan data, yang mengakibatkan NPWP tidak valid, bisa datang langsung ke KPP untuk pengkinian data pribadi sebelumnya isi antrian kunjungan kantor pajak :
            5. Syarat dan Formulir Pengkinian Data Wajib Pajak:
            6. Pendana melakukan Validasi NPWP dengan Cek secara mandiri Online (NIK & No KK):
            7. Pendana memiliki NPWP namun status NPWP Non Efektif (NE), cara cek NPWP:
            8. Pendana memiliki NPWP Namun status NPWP Non Efektif (NE), cara aktifkan kembali NPWP:
            9. Hubungi KPP terdaftar bagian pelayanan (No WA KPP Jabodetabek)  021 - 1500200
            ',
            'answer_id' => 1,
            'answer' => 'Mengenai adanya *PMK No 69/PMK.03/2022 tentang Perlakuan Perpajakan atas Teknologi Finansial*. Danasyariah.id akan segera mengikuti PMK ini dengan ketentuan:
            -Danasyariah.id akan memotongkan pajak Pph 23 dengan besaran 15% untuk Pendana yang memiliki NPWP.
            -Danasyriah.iD akan memotngkan paja Pph 23 dengan besaran 30% untuk Pendana yang tidak memiliki NPWP.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 2,
            'answer' => 'Perhitungan pemotongan pajak Pph23 untuk Pendana dengan 2 kategori kepemilikan NPW, sebagai contoh dibawah:
            **Pendana ABC memiliki NPWP:
            Imbal hasil perbulan yang didapat totalnya akan dikurang Pajak Pph23 sebesar 15%
            Imbal hasil gross (sebelum pajak) Rp.10.000, setelah dipotong pajak/Nett sebesar Rp.8.500.
            
            **Pendana XYZ tidak memiliki NPWP:
            Imbal hasil perbulan yang didapat totalnya akan dkurnag Pajak Pph23 sebesar 30% (2x lipat karena tanpa NPWP).
            Imbal hasil gross (sebelum pajak) Rp. 10.000, setelah dipotong pajak/Nett sebesar Rp.7.000.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 3,
            'answer' => 'https://ereg.pajak.go.id/daftar',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 4,
            'answer' => 'https://kunjung.pajak.go.id/',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 5,
            'answer' => 'https://www.pajak.go.id/id/perubahan-data-wajib-pajak',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 6,
            'answer' => 'https://ereg.pajak.go.id/ceknpwp',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 7,
            'answer' => 'https://djponline.pajak.go.id/account/login',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 8,
            'answer' => 'https://www.pajak.go.id/id/pengumuman/penetapan-dan-pengaktifan-wajib-pajak-non-efektif-ne',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 6,
            'answer_id' => 9,
            'answer' => 'https://sadarpajak.com/alamatkpp/?msclkid=4ec85cd4d02511ecb5681b7ea5a7aff8',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 2 Question 6 + Answer

        // Start Category 2 Question 7 + Answer
        Chatbot::create([
            'code' => 'penarikan',
            'category_id' => 2,
            'question_id' => 7,
            'question' => 'Penarikan Dana
            1. Bagaimana cara menarik dana?
            2. Bagaimana waktu real time penarikan dana?',
            'answer_id' => 1,
            'answer' => 'Penarikan Dana ada 2 keadaan/kondisi:
            *1. Jika dana Anda sudah ada di VA atau di menu DANA TERSEDIA:*
            a) Klik Menu *Portofolio*.
            b) Klik Menu *Tarik Dana*.
            c) Masukkan nominal yang mau ditarik sesuai dengan Dana Tersedia Anda.
            d) Selesai menarik, akan masuk ke riwayat penarikan.
            
            *2. Jika dana Anda masih dalam sebuah proyeki:*
            a)Klik Menu *Portofolio*.
            b) Klik Menu *Kelola Penggalangan Dana*.
            c) Klik/Pilih mau tarik dari proyek yang mana.
            d) Klik Menu *Ambil/Tarik*.
            e) Dana akan masuk ke *Dana Tersedia*.
            f) Kembali ke Menu *Portofolio*.
            g) Klik Menu *Tarik Dana*.
            h) Masukkan nominal yang mau ditarik sesuai dengan Dana Tersedia Anda.
            I)Selesai menarik, akan masuk ke riwayat penarikan.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 2,
            'question_id' => 7,
            'answer_id' => 2,
            'answer' => 'Informasi mengenai Penarikan Dana:
                1. Penarikan dana yang dilakukan pada jam pagi (00.00 WIB - 11.00 WIB) dan dihari kerja, maka akan di proses hari itu juga (dan/atau maksimal 7 hari kerja). Setelah mendapatkan whatsapp/email konfirmasi dari Admin mengenai penarikan dana yang Bapak/Ibu lakukan.
                
                2. Penarikan dana yang dilakukan pada jam siang (11.00 - 24.00) dan di hari kerja, maka akan diproses pada keesokan harinya (dan/atau maksimal 7 hari kerja, sesuai SOP). Setelah mendapatkan whatsapp/email konfirmasi penarikan dana yang Bapak/Ibu lakukan.
                
                3. Penarikan dana yang dilakukan pada hari libur, maka akan di konfirmasi dan di proses saat hari kerja berlangsung. (Maksimal 7 hari kerja).',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 2 Question 7 + Answer

         /*
        |--------------------------------------------------------------------------
        | Seeder For Borrower
        |--------------------------------------------------------------------------
        */
        // Start Category 3 Question 1 + Answer
        Chatbot::create([
            'category_id' => 3,
            'category' => 'Hallo, Pendana Halal! Apa yang dapat kami bantu hari ini?

            1. Registrasi & Akun
            2. Pertanyaan Teknis

            Silahkan lanjut dengan mengetik nomor sesuai pilihan Anda.

            Silahkan ikuti media sosial Danasyariahid dan Download Applikasinya di Play Store dan Apps Store!',
            'question_id' => 1,
            'question' => 'Registrasi & Akun
            1. Bagaimana cara mendaftar sebagai Penerima Pendanaan (new user)?',
            'answer_id' => 1,
            'answer' => 'Anda dapat mendapatkan pembiayaan pembelian rumah impian Anda dengan skema syariah yang Danasyariah.id berikan. Pendaftaran dapat melalui aplikasi https://play.google.com/store/apps/details?id=com.danasyariah.mobiledanasyariah ataupun website https://www.danasyariah.id/.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 3 Question 1 + Answer

        // Start Category 3 Question 2 + Answer
        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'question' => 'Pertanyaan Teknis
            1. Bagaimana dengan simulasi hitungan cicilan Dana Rumah?
            2. Berapa margin yang dikenakan untuk produk Dana Rumah?
            3. Apakah ada angsuran selain dari yang tetap?
            4. Berapa tenor yang tersedia untuk produk Dana Rumah?
            5. Apakah harus ada Jaminan?
            6. Apakah ada DP (Down Payment)?
            7. Berapakah batas usia pengajuan?
            8. Syarat status pekerjaan saat pengajuan?
            9. Syarat tambahan untuk calon pemohon wirausaha?
            10. Apa saja syarat untuk calon pemohon perseorangan (umum)?
            11. Berapa minimal gaji saat pengajuan?
            12. Apakah bisa mengajukan pembelian rumah kepemilikan yang ke 2 dst?
            13. List Proyek Kerjasama dengan LAMUDI apa saja?',
            'answer_id' => 1,
            'answer' => 'Mengenai simulasi perhitungan cicilan pembelian Dana Rumah Anda dapat kunjungi website kami di: https://www.danasyariah.id/simulasi/danarumah atau melalui aplikasi kami.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 2,
            'answer' => 'Untuk besaran angsuran bulanan yang tetap. Setara mulai dari 12.5% per tahun',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 3,
            'answer' => 'Saat ini primary yang digunakan adalah Flat hingga akhir, namun tidak menutup kemungkinan untuk adanya skema-skema lain untuk menjawab kebutuhan pasar dengan beberapa ketentuan. Salah satu diantaranya adalah, Kami coba diskusikan dengan tim terkait kemudian akan kami sampaikan segera untuk tindak lanjut skema akad selain Flat seperti yang saat ini dipergunakan oleh Danasyariah.id',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 4,
            'answer' => 'Kami menawarkan skema flat dari awal hingga akhir masa pembiayaan yaitu minimal 5 tahun dan maksimal 15 tahun',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 5,
            'answer' => 'Agunan / Collateral asset yang dijadikan jaminan adalah objek yang menjadi pembiayaan itu sendiri. Artinya selama objek pembiayaan adalah rumah yang dibeli dan dijadikan tempat tinggal asset itulah yang akan menjadi kollateral asset selama masa pembiayaan berlangsung hingga kewajiban yang menjadi tanggungan selesai.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 6,
            'answer' => 'Case I: DP bisa 0% jika calon debitur secara analisa plafon pembiayaan yang diajukan masih masuk atau bahkan lebih. 
            Case II: DP bisa 0% jika ada program dari developer (penyedia unit properti) all in one. Ini dapat dibicarakan langsung kepada developer nya
            Case III: Kami menyarankan untuk adanya DP minimum 5% jika dua skenario case diatas tidak terpenuhi',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 7,
            'answer' => 'Untuk maksium usia dengan tenor maksimal 15 tahun adalah di umur 40 Tahun. Lebih dari 40 tahun akan disesuaikan proposional. Karena maksimal umur produktif (sudah pensiun) ada di angka 55 tahun.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 8,
            'answer' => 'Saat ini pengajuan pembiayaan Dana Rumah terbuka untuk Karyawan Swasta, Karyawan Kontrak, profesional dan wirausaha antara lain: Dokter, Pilot, Pelaut, Seniman, pengusaha online, pekerja kreatif, dan lain-lain. ',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 9,
            'answer' => 'Selain dokumen legalitas dari subjek yang mengajukan pembiayaan dan legalitas objek pembiayaan nya ada beberapa tambahan dokumen pendukun bagi wirausaha, antara lain:
            -Izin Usaha (SKDU, SIUP, TDP, / NIB dan NPWP)
            -Akta pendirian usaha (jika ada)
            -Menyerahkan laporan keuangan 1-2 tahun terakhir (atau rekap laporan penjualan)
            -Mutasi rekening bank minimum 6 bulan',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 10,
            'answer' => '- Rekening Bank
            - Identitas diri
            - Objek pengajuan (yang diajukan pembiayaan)',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 11,
            'answer' => 'Untuk batasan minimum gaji korelasi nya adalah kepada nilai objek pembiayaan yang diajukan. Silahkan bapak/Ibu ajukan unit mana yang ingin diajukan melalui Dana Rumah.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 12,
            'answer' => 'Bagi peruntukan Rumah ke-2 atau Rumah ke-3 perlu diperhatikan sisa outstanding atau sisa hutang pada pembiayaan-pembiayaan sebelumnya. Karena akan mempengaruhi proses dan hasil analisa yang dilaksanakan. Selama plafond yang diajukan masih mencukup untuk pembiayaan yang diajukan dan objek pembiayaan yang diajukan sesuai dan memenuhi persyaratan maka dapat di proses oleh Tim Danasyariah.id.',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);

        Chatbot::create([
            'category_id' => 3,
            'question_id' => 2,
            'answer_id' => 13,
            'answer' => 'Untuk dapat melihat list developer yang sudah kerjasama dengan Danasyariah dapat di cek melalui link berikut ini. https://www.lamudi.co.id/dana-syariah-indonesia-agn/',
            'created_at' => date('Y-m-d H:i:s', time()),
            'updated_at' => date('Y-m-d H:i:s', time())
        ]);
        // End Category 3 Question 2 + Answer
    }
}