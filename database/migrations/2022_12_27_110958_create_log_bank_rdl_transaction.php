<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_bank_rdl_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank_rdl_code')->nullable();
            $table->string('category')->nullable();
            $table->string('request_content')->nullable();
            $table->string('request_response')->nullable();
            $table->decimal('nominal_transaction',20,2)->nullable();
            $table->string('status')->nullable();
            $table->string('bank_reference')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_bank_rdl_transaction');
    }
};
