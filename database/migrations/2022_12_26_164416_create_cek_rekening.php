<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cek_rekening', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->nullable();
            $table->integer('bank_id')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('account_num')->nullable();
            $table->integer('is_verified')->nullable();
            $table->integer('is_dispute')->nullable();
            $table->integer('is_rejected')->nullable();
            $table->integer('tipe_pengguna');
            $table->string('tipe_user');
            $table->string('response_message');
            $table->timestamp('action_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cek_rekening');
    }
};
