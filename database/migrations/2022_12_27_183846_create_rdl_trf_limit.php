<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_trf_limit', function (Blueprint $table) {
            $table->integer('id_rdl_trf');
            $table->string('kode_bank')->nullable();
            $table->decimal('min_jumlah',10,0)->nullable();
            $table->decimal('max_jumlah',10,0)->nullable();
            $table->string('metode')->nullable();
            $table->string('limit_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_trf_limit');
    }
};
