<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_cabang', function (Blueprint $table) {
            $table->integer('id_rdl_cabang');
            $table->string('kode_bank')->nullable();
            $table->string('kode_cabang')->nullable();
            $table->string('nama_cabang')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_cabang');
    }
};
