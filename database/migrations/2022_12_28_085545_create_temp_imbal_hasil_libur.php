<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_imbal_hasil_libur', function (Blueprint $table) {
            $table->integer('id_list_imbal_user');
            $table->integer('id_proyek');
            $table->string('nama')->nullable();
            $table->date('tgl_mulai');
            $table->integer('id_pendanaan')->nullable();
            $table->date('tanggal_payout')->nullable();
            $table->integer('keterangan_payout')->nullable();
            $table->string('ket_libur')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_imbal_hasil_libur');
    }
};
