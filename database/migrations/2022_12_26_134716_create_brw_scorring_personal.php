<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_scorring_personal', function (Blueprint $table) {
            $table->integer('scorring_personal_id');
            $table->integer('brw_id')->nullable();
            $table->integer('nilai')->nullable();
            $table->integer('vendor_scorring')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_scorring_personal');
    }
};
