<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id')->nullable();
            $table->string('trx_id')->nullable();
            $table->string('request_data');
            $table->string('response_data');
            $table->integer('status');
            $table->string('errors')->nullable();
            $table->dateTime('created_at');
            $table->string('created_by')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_log');
    }
};
