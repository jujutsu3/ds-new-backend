<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brw_user_detail', function (Blueprint $table) {
            //
            $table->boolean('status_isi_data_pribadi')->default(0);
            $table->boolean('status_isi_data_alamat')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brw_user_detail', function (Blueprint $table) {
            //
        });
    }
};
