<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_privy_investor', function (Blueprint $table) {
            $table->integer('id_log_privy');
            $table->integer('investor_id')->nullable();
            $table->string('transaction_id')->nullable();
            $table->integer('nominal')->nullable();
            $table->integer('status')->nullable();
            $table->dateTime('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_privy_investor');
    }
};
