<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_riwayat_detil_investor_individu_ori', function (Blueprint $table) {
            $table->integer('hist_investor_id');
            $table->integer('investor_id');
            $table->string('nama_investor')->nullable();
            $table->integer('jenis_identitas')->nullable();
            $table->string('no_ktp_investor')->nullable();
            $table->string('no_passpor_investor')->nullable();
            $table->string('no_npwp_investor')->nullable();
            $table->string('tempat_lahir_investor')->nullable();//
            $table->string('tgl_lahir_investor')->nullable();
            $table->integer('jenis_kelamin_investor')->nullable();
            $table->integer('status_kawin_investor')->nullable();
            $table->string('alamat_investor')->nullable();
            $table->integer('provinsi_investor')->nullable();
            $table->string('kota_investor')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kecamatan')->nullable();
            $table->integer('kode_pos_investor')->nullable();//
            $table->string('kode_operator')->nullable();
            $table->string('phone_investor')->nullable();
            $table->integer('agama_investor')->nullable();
            $table->integer('pekerjaan_investor')->nullable();
            $table->string('bidang_pekerjaan')->nullable();
            $table->integer('online_investor')->nullable();
            $table->integer('pendapatan_investor')->nullable();
            $table->integer('pengalaman_investor')->nullable();
            $table->integer('pendidikan_investor')->nullable();//
            $table->string('bank_investor')->nullable();
            $table->integer('domisili_negara')->nullable();
            $table->string('sumber_dana')->nullable();
            $table->string('pic_investor')->nullable();
            $table->string('pic_ktp_investor')->nullable();
            $table->string('pic_user_ktp_investor')->nullable();
            $table->string('rekening')->nullable();
            $table->string('nama_pemilik_rek')->nullable();
            $table->string('nama_ibu_kandung')->nullable();
            $table->integer('warganegara')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_riwayat_detil_investor_individu_ori');
    }
};
