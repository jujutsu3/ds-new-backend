<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ahli_waris_investor', function (Blueprint $table) {
            $table->integer('id_ahli_waris');
            $table->integer('id_investor')->nullable();
            $table->string('nama_ahli_waris')->nullable();
            $table->integer('hubungan_keluarga_ahli_waris')->nullable();
            $table->string('nik_ahli_waris')->nullable();
            $table->string('kode_operator')->nullable();
            $table->string('no_hp_ahli_waris')->nullable();
            $table->string('alamat_ahli_waris')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ahli_waris_investor');
    }
};
