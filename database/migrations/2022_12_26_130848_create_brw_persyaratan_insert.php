<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_persyaratan_insert', function (Blueprint $table) {
            $table->integer('persyaratan_insert_id');
            $table->integer('brw_id')->nullable();
            $table->integer('tipe_id')->nullable();
            $table->integer('user_type')->nullable();
            $table->integer('persyaratan_id')->nullable();
            $table->integer('checked')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_persyaratan_insert');
    }
};
