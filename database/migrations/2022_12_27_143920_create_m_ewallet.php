<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_ewallet', function (Blueprint $table) {
            $table->integer('id_ewallet');
            $table->string('kode_ewallet')->nullable();
            $table->string('nama_ewallet')->nullable();
            $table->string('nama_penerbit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_ewallet');
    }
};
