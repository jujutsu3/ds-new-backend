<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('migrasi_va_dsi', function (Blueprint $table) {
            $table->string('nama_investor')->nullable();
            $table->string('email')->nullable();
            $table->string('va_number')->nullable();
            $table->string('status_dana')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('migrasi_va_dsi');
    }
};
