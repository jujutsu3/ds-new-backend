<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_shareholder', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id');
            $table->string('npwp_company')->nullable();
            $table->string('valid_npwp_company')->nullable();
            $table->string('company_name')->nullable();
            $table->string('valid_company_name')->nullable();
            $table->string('nik_share_holder')->nullable();
            $table->string('valid_nik_share_holder')->nullable();
            $table->string('name_share_holder')->nullable();
            $table->string('valid_name_share_holder')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_shareholder');
    }
};
