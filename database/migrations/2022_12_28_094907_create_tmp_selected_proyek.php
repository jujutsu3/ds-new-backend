<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_selected_proyek', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('investor_id')->nullable();
            $table->integer('proyek_id')->nullable();
            $table->integer('qty')->nullable();
            $table->decimal('total_price',15,2)->nullable();
            $table->timestamps();
            $table->dateTime('exp_date')->nullable();
            $table->integer('status');
            $table->integer('invoice_status');
            $table->string('invoice_id');
            $table->string('no_va');
            $table->decimal('amount',15,0);
            $table->timestamp('trx_date_va');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_selected_proyek');
    }
};
