<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_validasi_rekening', function (Blueprint $table) {
            $table->string('nama')->nullable();
            $table->string('email')->nullable();
            $table->string('no_rek')->nullable();
            $table->string('bank')->nullable();
            $table->string('nama_pemilik_rek')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_validasi_rekening');
    }
};
