<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_riwayat_ahli_waris_investor_ori', function (Blueprint $table) {
            $table->integer('hist_investor_id');
            $table->integer('investor_id');
            $table->string('nama_ahli_waris')->nullable();
            $table->integer('hubungan_keluarga_ahli_waris')->nullable();
            $table->string('nik_ahli_waris')->nullable();
            $table->string('kode_operator_ahli_waris')->nullable();
            $table->string('no_hp_ahli_waris')->nullable();
            $table->string('alamat_ahli_waris')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_riwayat_ahli_waris_investor_ori');
    }
};
