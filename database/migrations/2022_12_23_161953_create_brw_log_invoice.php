<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_log_invoice', function (Blueprint $table) {
            $table->integer('log_id');
            $table->integer('invoice_id')->nullable();
            $table->integer('brw_id')->nullable();
            $table->integer('pendanaan_id')->nullable();
            $table->integer('bukti_id')->nullable();
            $table->integer('status')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_log_invoice');
    }
};
