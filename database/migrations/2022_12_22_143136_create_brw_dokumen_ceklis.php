<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dokumen_ceklis', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->integer('dokumen_legalitas_1')->nullable();
            $table->string('dokumen_legalitas_1_hasil')->nullable();
            $table->integer('dokumen_legalitas_2')->nullable();
            $table->string('dokumen_legalitas_2_hasil')->nullable();
            $table->integer('dokumen_legalitas_3')->nullable();
            $table->string('dokumen_legalitas_3_hasil')->nullable();
            $table->integer('dokumen_legalitas_4')->nullable();
            $table->string('dokumen_legalitas_4_hasil')->nullable();
            $table->integer('dokumen_legalitas_5')->nullable();
            $table->string('dokumen_legalitas_5_hasil')->nullable();
            $table->integer('dokumen_legalitas_6')->nullable();
            $table->string('dokumen_legalitas_6_hasil')->nullable();
            $table->integer('dokumen_legalitas_7')->nullable();
            $table->string('dokumen_legalitas_7_hasil')->nullable();
            $table->integer('dokumen_legalitas_8')->nullable();
            $table->string('dokumen_legalitas_8_hasil')->nullable();
            $table->integer('dokumen_pekerjaan_1')->nullable();
            $table->string('dokumen_pekerjaan_1_hasil')->nullable();
            $table->integer('dokumen_penghasilan_1')->nullable();
            $table->string('dokumen_penghasilan_1_hasil')->nullable();
            $table->integer('dokumen_penghasilan_2')->nullable();
            $table->string('dokumen_penghasilan_2_hasil')->nullable();
            $table->integer('dokumen_penghasilan_3')->nullable();
            $table->string('dokumen_penghasilan_3_hasil')->nullable();
            $table->integer('dokumen_jaminan_1')->nullable();
            $table->string('dokumen_jaminan_1_hasil')->nullable();
            $table->integer('dokumen_jaminan_2')->nullable();
            $table->string('dokumen_jaminan_2_hasil')->nullable();
            $table->integer('dokumen_jaminan_3')->nullable();
            $table->string('dokumen_jaminan_3_hasil')->nullable();
            $table->integer('dokumen_jaminan_4')->nullable();
            $table->string('dokumen_jaminan_4_hasil')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dokumen_ceklis');
    }
};
