<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_negative_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id');
            $table->string('nik')->nullable();
            $table->string('valid_nik')->nullable();
            $table->string('name');
            $table->string('valid_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('valid_dob')->nullable();
            $table->string('pob');
            $table->string('valid_pob')->nullable();
            $table->string('detail_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_negative_list');
    }
};
