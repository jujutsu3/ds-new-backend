<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dokumen_objek_pendanaan', function (Blueprint $table) {
            $table->integer('id_pengajuan')->nullable();
            $table->string('surat_pemesanan_rumah')->nullable();
            $table->string('surat_permohonan_pengajuan')->nullable();
            $table->string('daftar_harga_perumahan')->nullable();
            $table->string('gambar_objek_pendanaan')->nullable();
            $table->string('sertifikat')->nullable();
            $table->string('imb')->nullable();
            $table->string('pbb')->nullable();
            $table->string('rab')->nullable();
            $table->string('rjpp')->nullable();
            $table->string('ktp_pemilik')->nullable();
            $table->string('jadwal_proyek_timeline')->nullable();
            $table->string('arus_kas_proyek')->nullable();
            $table->string('feasibility_study')->nullable();
            $table->string('hasil_penilaian_aset')->nullable();
            $table->string('site_plan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dokumen_objek_pendanaan');
    }
};
