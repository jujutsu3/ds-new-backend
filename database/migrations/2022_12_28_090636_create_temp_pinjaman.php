<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_pinjaman', function (Blueprint $table) {
            $table->integer('id_angsuran');
            $table->string('key_val');
            $table->float('hutang_pokok')->nullable();
            $table->float('angsuran_margin')->nullable();
            $table->float('angsuran_pokok')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_pinjaman');
    }
};
