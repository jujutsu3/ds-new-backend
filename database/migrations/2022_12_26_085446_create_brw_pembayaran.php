<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_pembayaran', function (Blueprint $table) {
            $table->integer('pembayaran_id');
            $table->string('invoice_id');
            $table->integer('brw_id')->nullable();
            $table->integer('pendanaan_id');
            $table->integer('tipe_pembayaran');
            $table->integer('tipe_percepatan')->nullable();
            $table->decimal('nilai_pelunasan',15,0);
            $table->string('pic_pembayaran')->nullable();
            $table->dateTime('tgl_pembayaran');
            $table->integer('status');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_pembayaran');
    }
};
