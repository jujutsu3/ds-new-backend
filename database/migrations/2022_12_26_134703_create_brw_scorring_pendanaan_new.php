<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_scorring_pendanaan_new', function (Blueprint $table) {
            $table->integer('scorring_pendanaan_id');
            $table->integer('pendanaan_id')->nullable();
            $table->string('scorring_judul')->nullable();
            $table->string('scorring_nilai')->nullable();
            $table->string('user_create')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_scorring_pendanaan_new');
    }
};
