<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_form_kunjungan_domisili', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->date('tanggal_kunjungan')->nullable();
            $table->date('jam_kunjungan')->nullable();
            $table->string('nama_yang_ditemui')->nullable();
            $table->integer('selaku')->nullable();
            $table->string('alamat_rumah_saat_ini')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota_kabupaten')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kode_pos')->nullable();
            $table->integer('status_rumah_saat_ini')->nullable();
            $table->string('lama_tinggal')->nullable();
            $table->string('kondisi_lingkungan')->nullable();
            $table->string('informasi_negatif')->nullable();
            $table->string('nama_pemberi_informasi')->nullable();
            $table->string('tlp_pemberi_informasi')->nullable();
            $table->string('penjelasan_terkait_pengajuan')->nullable();
            $table->string('foto_tampak_depan_rumah')->nullable();
            $table->string('foto_tampak_dalam_rumah')->nullable();
            $table->string('foto_lingkungan_rumah')->nullable();
            $table->string('foto_petugas_depan_rumah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_form_kunjungan_domisili');
    }
};
