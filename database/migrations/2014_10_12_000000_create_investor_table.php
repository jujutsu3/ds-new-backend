<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('email_verif')->nullable();
            $table->string('ref_number')->nullable();
            $table->string('status');
            $table->string('last_login');
            $table->string('provider')->nullable();
            $table->string('provider_id')->nullable();
            $table->string('otp')->nullable();
            $table->timestamps();
            $table->string('keterangan')->nullable();
            $table->string('suspended_by')->nullable();
            $table->string('actived_by')->nullable();
            $table->integer('password_expiry_days')->nullable();
            $table->dateTime('password_updated_at')->nullable();
            $table->string('kyc_step')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor');
    }
}
