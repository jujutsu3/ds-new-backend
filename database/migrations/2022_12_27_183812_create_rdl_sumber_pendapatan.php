<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_sumber_pendapatan', function (Blueprint $table) {
            $table->integer('id_sumber_pendapatan');
            $table->string('kode_bank')->nullable();
            $table->integer('kode_sumber_pendapatan')->nullable();
            $table->string('deskripsi_sumber_pendapatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_sumber_pendapatan');
    }
};
