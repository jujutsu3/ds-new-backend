<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dtl_biaya', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengajuan_id')->nullable();
            $table->string('rincian_biaya')->nullable();
            $table->string('keterangan')->nullable();
            $table->decimal('jumlah',15,2)->nullable();
            $table->integer('nama_rekaman')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dtl_biaya');
    }
};
