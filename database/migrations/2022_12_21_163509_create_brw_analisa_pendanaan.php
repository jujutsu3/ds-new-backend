<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_analisa_pendanaan', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->date('tanggal_pengajuan')->nullable();
            $table->date('tanggal_verifikasi')->nullable();
            $table->string('penerima_pendanaan')->nullable();
            $table->integer('jenis_pendanaan')->nullable();
            $table->integer('tujuan_pendanaan')->nullable();
            $table->decimal('nilai_pengajuan_pendanaan',15,2)->nullable();
            $table->integer('status_verifikator')->default(0)->nullable();
            $table->integer('status_analis')->default(0)->nullable();
            $table->integer('status_legal')->default(0)->nullable();
            $table->integer('status_risk')->default(0)->nullable();
            $table->integer('status_compliance')->default(0)->nullable();
            $table->integer('status_komite')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_analisa_pendanaan');
    }
};
