<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_akun_tipe', function (Blueprint $table) {
            $table->integer('id_akun_tipe');
            $table->string('kode_bank')->nullable();
            $table->string('tipe_akun')->nullable();
            $table->string('deskripsi_akun')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_akun_tipe');
    }
};
