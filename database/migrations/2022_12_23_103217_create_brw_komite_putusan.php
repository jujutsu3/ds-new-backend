<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_komite_putusan', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->string('nama_direktur')->nullable();
            $table->integer('rekomendasi_direktur')->nullable();
            $table->string('catatan_direktur')->nullable();
            $table->integer('penggalangan_dana_direktur')->nullable();
            $table->dateTime('tanggal_keputusan_direktur')->nullable();
            $table->dateTime('created_at_direktur')->nullable();
            $table->dateTime('updated_at_direktur')->nullable();
            $table->string('nama_direktur_utama')->nullable();
            $table->integer('rekomendasi_direktur_utama')->nullable();
            $table->string('catatan_direktur_utama')->nullable();
            $table->integer('penggalangan_dana_direktur_utama')->nullable();
            $table->dateTime('tanggal_keputusan_direktur_utama')->nullable();
            $table->dateTime('created_at_direktur_utama')->nullable();
            $table->dateTime('updated_at_direktur_utama')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_komite_putusan');
    }
};
