<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_pengembalian_dana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proyek_id')->nullable();
            $table->integer('investor_id')->nullable();
            $table->decimal('nominal',15,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_pengembalian_dana');
    }
};
