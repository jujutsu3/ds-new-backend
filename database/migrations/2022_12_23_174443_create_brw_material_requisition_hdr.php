<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_material_requisition_hdr', function (Blueprint $table) {
            $table->integer('requisition_hdr_id');
            $table->string('requisition_number')->nullable();
            $table->date('requisition_date')->nullable();
            $table->string('description')->nullable();
            $table->integer('brw_id');
            $table->integer('pengajuan_id');
            $table->integer('requisition_status');
            $table->string('approved_flag');
            $table->date('approved_date')->nullable();
            $table->string('approved_by')->nullable();
            $table->date('need_by_date')->nullable();
            $table->string('reject_reason')->nullable();
            $table->string('cancel_reason')->nullable();
            $table->string('delivery_to_address')->nullable();
            $table->float('shipping_fee')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamp('creation_date')->nullable();
            $table->string('last_updated_by')->nullable();
            $table->timestamp('last_update_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_material_requisition_hdr');
    }
};
