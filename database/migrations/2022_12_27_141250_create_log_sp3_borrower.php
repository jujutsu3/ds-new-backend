<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_sp3_borrower', function (Blueprint $table) {
            $table->integer('id_log_sp3_borrower');
            $table->string('no_sp3');
            $table->integer('brw_id');
            $table->integer('id_proyek');
            $table->decimal('total_pendanaan',10,0);
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_sp3_borrower');
    }
};
