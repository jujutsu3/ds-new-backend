<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_angsuran', function (Blueprint $table) {
            $table->float('start')->nullable();
            $table->float('end')->nullable();
            $table->float('tot_angsuran')->nullable();
            $table->float('tot_bayar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_angsuran');
    }
};
