<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_dana_proyek', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('investor_id');
            $table->integer('pendanaan_id');
            $table->decimal('jumlah_penarikan',15,2)->nullable();
            $table->integer('status_penarikan')->nullable();
            $table->integer('notification')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_dana_proyek');
    }
};
