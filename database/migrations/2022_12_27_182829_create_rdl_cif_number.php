<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_cif_number', function (Blueprint $table) {
            $table->integer('id_rdl_cif');
            $table->integer('investor_id')->nullable();
            $table->string('kode_bank')->nullable();
            $table->string('cif_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_cif_number');
    }
};
