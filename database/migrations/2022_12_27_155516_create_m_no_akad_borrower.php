<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_no_akad_borrower', function (Blueprint $table) {
            $table->integer('id_no_akad_bor');
            $table->integer('no_akad_bor');
            $table->integer('proyek_id');
            $table->string('bln_akad_bor');
            $table->string('thn_akad_bor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_no_akad_borrower');
    }
};
