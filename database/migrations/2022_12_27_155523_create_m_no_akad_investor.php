<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_no_akad_investor', function (Blueprint $table) {
            $table->integer('id_no_akad_inv');
            $table->integer('investor_id');
            $table->integer('mutasi_id');
            $table->integer('no_akad_inv');
            $table->string('bln_akad_inv');
            $table->string('thn_akad_inv');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_no_akad_investor');
    }
};
