<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_param_kalkulator', function (Blueprint $table) {
            $table->integer('id_kal');
            $table->integer('tenor')->nullable();
            $table->integer('sequen')->nullable();
            $table->integer('period')->nullable();
            $table->float('margin_efektif')->nullable();
            $table->float('penurunan_pokok')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_param_kalkulator');
    }
};
