<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detil_rekening_investor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('investor_id')->nullable();
            $table->string('va_number')->nullable();
            $table->decimal('last_amount',15,2)->nullable();
            $table->string('last_payment_ntb')->nullable();
            $table->string('kode_bank')->nullable();
            $table->string('type')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detil_rekening_investor');
    }
};
