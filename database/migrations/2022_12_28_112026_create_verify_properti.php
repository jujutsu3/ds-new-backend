<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_properti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id');
            $table->string('nop');
            $table->string('property_address')->nullable();
            $table->string('certificate_address')->nullable();
            $table->string('property_name')->nullable();
            $table->string('valid_property_name')->nullable();
            $table->string('property_building_area')->nullable();
            $table->string('valid_property_building_area')->nullable();
            $table->string('property_surface_area')->nullable();
            $table->string('valid_property_surface_area')->nullable();
            $table->double('property_estimation',15,2)->nullable();
            $table->string('valid_property_estimation')->nullable();
            $table->string('nik')->nullable();
            $table->string('certificate_id')->nullable();
            $table->string('valid_certificate_id')->nullable();
            $table->string('certificate_name')->nullable();
            $table->string('valid_certificate_name')->nullable();
            $table->string('certificate_type')->nullable();
            $table->string('valid_certificate_type')->nullable();
            $table->date('certificate_date')->nullable();
            $table->string('valid_certificate_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_properti');
    }
};
