<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_faq_content', function (Blueprint $table) {
            $table->integer('category_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->integer('content_id');
            $table->string('content_text')->nullable();
            $table->integer('related_content_id')->nullable();
            $table->integer('related_group_id')->nullable();
            $table->timestamp('creation_date')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamp('last_update_date')->nullable();
            $table->string('last_updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_faq_content');
    }
};
