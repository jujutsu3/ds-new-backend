<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_ahli_waris', function (Blueprint $table) {
            $table->integer('ahli_id');
            $table->integer('brw_id')->nullable();
            $table->string('nama_ahli_waris')->nullable();
            $table->integer('hub_ahli_waris')->nullable();
            $table->integer('jenis_kelamin')->nullable();
            $table->string('nik')->nullable();
            $table->string('tmpt_lahir')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('no_tlp')->nullable();
            $table->integer('agama')->nullable();
            $table->string('pendidikan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kd_pos')->nullable();
            $table->timestamps();
            $table->integer('jenis_identitas')->nullable();
            $table->string('email')->nullable();
            $table->string('foto_diri')->nullable();
            $table->string('foto_identitas')->nullable();
            $table->string('npwp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_ahli_waris');
    }
};
