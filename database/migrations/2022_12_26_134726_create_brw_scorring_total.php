<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_scorring_total', function (Blueprint $table) {
            $table->integer('total_id');
            $table->integer('pendanaan_id');
            $table->integer('brw_id');
            $table->string('scorring_total');
            $table->string('scorring_grade');
            $table->string('scrorring_keterangan'); // FIXME: scroring
            $table->integer('vendor_scorring');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_scorring_total');
    }
};
