<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_tgl_payout_sisa_imbal', function (Blueprint $table) {
            $table->integer('id_listimbal')->default(0);
            $table->integer('pendanaan_id')->nullable();
            $table->integer('id_proyek')->default(0);
            $table->string('nama')->nullable();
            $table->date('tgl_selesai')->nullable();
            $table->date('tanggal_payout')->nullable();
            $table->date('tanggal_payout_terakhir')->nullable();
            $table->string('ket_libur')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_tgl_payout_sisa_imbal');
    }
};
