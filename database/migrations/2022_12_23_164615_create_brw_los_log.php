<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_los_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengajuan_id');
            $table->string('app_no')->nullable();
            $table->string('request_data')->nullable();
            $table->string('response_data')->nullable();
            $table->string('method')->nullable();
            $table->string('status')->nullable();
            $table->string('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_los_log');
    }
};
