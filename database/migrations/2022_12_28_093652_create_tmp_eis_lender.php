<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmp_eis_lender', function (Blueprint $table) {
            $table->integer('inv_id')->nullable();
            $table->integer('usia')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('pengguna')->nullable();
            $table->string('nama_provinsi')->nullable();
            $table->string('nama_kota')->nullable();
            $table->string('pendapatan')->nullable();
            $table->decimal('asset_investor',15,2)->nullable();
            $table->string('nama_bank')->nullable();
            $table->date('join_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmp_eis_lender');
    }
};
