<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_log_rekening', function (Blueprint $table) {
            $table->integer('id_brw_log_rekening');
            $table->integer('brw_id')->nullable();
            $table->integer('pendanaan_id')->nullable();
            $table->decimal('debet',15,2)->nullable();
            $table->decimal('credit',15,2)->nullable();
            $table->decimal('total_terpakai',15,2)->nullable();
            $table->decimal('total_sisa',15,2)->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_log_rekening');
    }
};
