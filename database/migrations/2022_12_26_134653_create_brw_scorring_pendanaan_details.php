<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_scorring_pendanaan_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scorring_pendanaan_id')->nullable();
            $table->string('scorring_item')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_scorring_pendanaan_details');
    }
};
