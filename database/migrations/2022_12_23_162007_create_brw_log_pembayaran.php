<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_log_pembayaran', function (Blueprint $table) {
            $table->integer('log_id');
            $table->integer('invoice_id')->nullable();
            $table->integer('pembayaran_id')->nullable();
            $table->integer('brw_id')->nullable();
            $table->integer('pendanaan_id')->nullable();
            $table->integer('tipe_pembayaran')->nullable();
            $table->integer('tipe_percepatan')->nullable();
            $table->decimal('nilai_pelunasan',15,2)->nullable();
            $table->integer('bulan_ke')->nullable();
            $table->string('pic_pembayaran')->nullable();
            $table->dateTime('tgl_pembayaran')->nullable();
            $table->date('tgl_jatuh_tempo')->nullable();
            $table->date('tgl_konfirmasi')->nullable();
            $table->string('konfirmasi_oleh')->nullable();
            $table->integer('status');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_log_pembayaran');
    }
};
