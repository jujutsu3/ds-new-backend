<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor_corp_pemegang_saham', function (Blueprint $table) {
            $table->integer('pemegang_saham_id');
            $table->integer('investor_id');
            $table->string('nm_pemegang_saham')->nullable();
            $table->integer('jenis_kelamin')->nullable();
            $table->string('identitas_pemegang_saham')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('kode_operator')->nullable();
            $table->string('no_tlp')->nullable();
            $table->integer('agama')->nullable();
            $table->integer('pendidikan_terakhir')->nullable();
            $table->string('npwp')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('foto_diri')->nullable();
            $table->string('foto_ktp')->nullable();
            $table->string('foto_diri_ktp')->nullable();
            $table->string('foto_npwp')->nullable();
            $table->integer('jenis_identitas')->nullable();
            $table->integer('jenis_pemegang_saham')->nullable();
            $table->decimal('nilai_saham',17,2)->nullable();
            $table->decimal('lembar_saham',17,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investor_corp_pemegang_saham');
    }
};
