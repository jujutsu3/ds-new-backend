<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_form_interview_hrd', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->date('tanggal_interview')->nullable();
            $table->string('jam_interview')->nullable();
            $table->integer('jumlah_total_karyawan')->nullable();
            $table->integer('jabatan_hrd')->nullable();
            $table->integer('verifikasi_melalui')->nullable();
            $table->integer('pertanyaan_1_jawaban');
            $table->string('pertanyaan_1_deskripsi')->nullable();
            $table->string('pertanyaan_2')->nullable();
            $table->integer('pertanyaan_3_jawaban')->nullable();
            $table->string('pertanyaan_3_deskripsi')->nullable();
            $table->string('pertanyaan_4_jawaban')->nullable();
            $table->string('pertanyaan_4_deskripsi')->nullable();
            $table->decimal('pertanyaan_5',15,2)->nullable();
            $table->integer('pertanyaan_6_gaji_pokok')->nullable();
            $table->integer('pertanyaan_6_tunjangan_jabatan')->nullable();
            $table->integer('pertanyaan_6_lembur')->nullable();
            $table->integer('pertanyaan_6_transport')->nullable();
            $table->integer('pertanyaan_6_uang_makan')->nullable();
            $table->integer('pertanyaan_6_bonus')->nullable();
            $table->integer('pertanyaan_6_bpjs')->nullable();
            $table->integer('pertanyaan_6_jht')->nullable();
            $table->integer('pertanyaan_6_jkk')->nullable();
            $table->integer('pertanyaan_6_jpn')->nullable();
            $table->integer('pertanyaan_6_pph21')->nullable();
            $table->integer('pertanyaan_7_tanggal')->nullable();
            $table->string('pertanyaan_7_bank')->nullable();
            $table->string('pertanyaan_8_jawaban')->nullable();
            $table->string('pertanyaan_8_deskripsi')->nullable();
            $table->string('pertanyaan_9_jawaban')->nullable();
            $table->string('pertanyaan_9_deskripsi')->nullable();
            $table->integer('pertanyaan_10_jawaban')->nullable();
            $table->string('pertanyaan_10_deskripsi')->nullable();
            $table->integer('pertanyaan_11_jawaban')->nullable();
            $table->decimal('pertanyaan_11_plafond',15,2)->nullable();
            $table->decimal('pertanyaan_11_outstanding',15,2)->nullable();
            $table->string('pertanyaan_11_jangka_waktu')->nullable();
            $table->integer('pertanyaan_11_margin')->nullable();
            $table->decimal('pertanyaan_11_angsuran',15,2)->nullable();
            $table->string('foto_didepan_kantor_lobby')->nullable();
            $table->string('foto_bersama_hrd')->nullable();
            $table->string('bukti_interview')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_form_interview_hrd');
    }
};
