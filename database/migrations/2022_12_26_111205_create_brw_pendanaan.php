<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_pendanaan', function (Blueprint $table) {
            $table->integer('pendanaan_id');
            $table->integer('pengajuan_id')->nullable();
            $table->integer('id_proyek')->nullable();
            $table->integer('brw_id')->nullable();
            $table->string('pendanaan_nama')->nullable();
            $table->integer('pendanaan_tipe')->nullable();
            $table->integer('pendanaan_akad')->nullable();
            $table->decimal('pendanaan_dana_dibutuhkan',15,2)->nullable();
            $table->date('estimasi_mulai')->nullable();
            $table->integer('estimasi_imbal_hasil')->nullable();
            $table->integer('mode_pembayaran')->nullable();
            $table->integer('metode_pembayaran')->nullable();
            $table->integer('durasi_proyek')->nullable();
            $table->string('detail_pendanaan')->nullable();
            $table->decimal('dana_dicairkan',15,2)->nullable();
            $table->integer('status')->nullable();
            $table->integer('status_dana');
            $table->string('lokasi_proyek')->nullable();
            $table->string('geocode')->nullable();
            $table->string('va_number')->nullable();
            $table->string('kode_bank')->nullable();
            $table->string('gambar_utama')->nullable();
            $table->string('status_tampil_pengajuan')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('keterangan_approval')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_pendanaan');
    }
};
