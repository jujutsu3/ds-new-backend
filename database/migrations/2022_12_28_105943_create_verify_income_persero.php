<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_income_persero', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id');
            $table->string('npwp');
            $table->string('valid_npwp')->nullable();
            $table->decimal('income',15,2)->default(0.00);
            $table->string('valid_income')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_income_persero');
    }
};
