<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_tempat_kerja', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id');
            $table->string('nik')->nullable();
            $table->string('valid_nik')->nullable();
            $table->string('name');
            $table->string('valid_name')->nullable();
            $table->string('valid_company')->nullable();
            $table->string('company_name')->nullable();
            $table->string('valid_company_name')->nullable();
            $table->string('company_phone')->nullable();
            $table->string('valid_company_phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_tempat_kerja');
    }
};
