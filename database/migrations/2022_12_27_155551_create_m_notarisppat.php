<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_notarisppat', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomor');
            $table->date('tanggal')->nullable();
            $table->string('nama');
            $table->date('tanggal_berlaku');
            $table->date('tanggal_berakhir');
            $table->string('alamat')->nullable();
            $table->string('no_telp')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_notarisppat');
    }
};
