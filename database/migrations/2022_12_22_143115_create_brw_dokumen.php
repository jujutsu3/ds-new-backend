<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dokumen', function (Blueprint $table) {
            $table->integer('id_dokumen');
            $table->integer('brw_id')->nullable();
            $table->integer('pendanaan_id')->nullable();
            $table->integer('scoring_type')->nullable();
            $table->string('jenis_dokumen')->nullable();
            $table->string('nama_dokumen')->nullable();
            $table->string('path_file')->nullable();
            $table->string('author')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dokumen');
    }
};
