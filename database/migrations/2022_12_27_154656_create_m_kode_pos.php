<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_kode_pos', function (Blueprint $table) {
            $table->integer('id_kode_pos');
            $table->integer('kode_pos')->nullable();
            $table->integer('kode_provinsi')->nullable();
            $table->string('kode_kota')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('jenis')->nullable();
            $table->string('Kota')->nullable();
            $table->string('Provinsi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_kode_pos');
    }
};
