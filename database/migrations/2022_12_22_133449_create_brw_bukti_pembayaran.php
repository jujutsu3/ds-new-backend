<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_bukti_pembayaran', function (Blueprint $table) {
            $table->integer('bukti_id');
            $table->integer('invoice_id');
            $table->integer('brw_id');
            $table->integer('pendanaan_id');
            $table->decimal('nominal',15,2);
            $table->string('ref_no')->nullable();
            $table->string('pic_pembayaran');
            $table->dateTime('tgl_bukti_bayar');
            $table->string('keterangan')->nullable();
            $table->integer('status');
            $table->string('confirmed_by');
            $table->timestamps();
            $table->string('created_by')->nullable();
            $table->string('kode_bank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_bukti_pembayaran');
    }
};
