<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_refferal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipe_pengguna')->nullable();
            $table->integer('jenis_badan_hukum')->nullable();
            $table->string('nama_refferal')->nullable();
            $table->integer('jenis_identitas')->nullable();
            $table->string('no_ktp_investor')->nullable();
            $table->string('no_passpor_refferal')->nullable();
            $table->string('no_npwp_refferal')->nullable();
            $table->string('tempat_lahir_refferal')->nullable();
            $table->string('tgl_lahir_refferal')->nullable();
            $table->integer('jenis_kelamin_refferal')->nullable();
            $table->integer('status_kawin_investor')->nullable();
            $table->string('alamat_investor')->nullable();
            $table->integer('provinsi_investor')->nullable();
            $table->string('kota_investor')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kecamatan')->nullable();
            $table->integer('kode_pos_investor')->nullable();
            $table->integer('status_rumah_investor')->nullable();
            $table->string('kode_operator')->nullable();
            $table->string('phone_investor')->nullable();
            $table->integer('agama_investor')->nullable();
            $table->integer('pekerjaan_investor')->nullable();
            $table->string('bidang_pekerjaan')->nullable();
            $table->integer('online_investor')->nullable();
            $table->integer('pendapatan_investor')->nullable();
            $table->integer('asset_investor')->nullable();
            $table->integer('pengalaman_investor')->nullable();
            $table->integer('pendidikan_investor')->nullable();
            $table->string('bank_investor')->nullable();
            $table->integer('warganegara')->nullable();
            $table->integer('domisili_negara')->nullable();
            $table->string('sumber_dana')->nullable();
            $table->string('pic_investor')->nullable();
            $table->string('pic_ktp_investor')->nullable();
            $table->string('pic_user_ktp_investor')->nullable();
            $table->string('pasangan_investor')->nullable();
            $table->string('pasangan_email')->nullable();
            $table->string('pasangan_tempat_lhr')->nullable();
            $table->string('pasangan_tgl_lhr')->nullable();
            $table->integer('pasangan_jenis_kelamin')->nullable();
            $table->string('pasangan_ktp')->nullable();
            $table->string('pasangan_npwp')->nullable();
            $table->string('pasangan_phone')->nullable();
            $table->string('pasangan_alamat')->nullable();
            $table->integer('pasangan_provinsi')->nullable();
            $table->string('pasangan_kota')->nullable();
            $table->integer('pasangan_kode_pos')->nullable();
            $table->integer('pasangan_agama')->nullable();
            $table->integer('pasangan_pekerjaan')->nullable();
            $table->string('pasangan_bidang_pekerjaan')->nullable();
            $table->integer('pasangan_online')->nullable();
            $table->integer('pasangan_pendapatan')->nullable();
            $table->integer('pasangan_pengalaman')->nullable();
            $table->integer('pasangan_pendidikan')->nullable();
            $table->string('nama_perwakilan')->nullable();
            $table->string('no_ktp_perwakilan')->nullable();
            $table->string('job_investor')->nullable();
            $table->string('rekening')->nullable();
            $table->string('bank')->nullable();
            $table->string('nama_pemilik_rek')->nullable();
            $table->string('OTP')->nullable();
            $table->string('nama_ibu_kandung')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->string('npwp_perusahaan')->nullable();
            $table->string('foto_npwp_perusahaan')->nullable();
            $table->string('nib')->nullable();
            $table->string('nomor_akta_perusahaan')->nullable();
            $table->date('tanggal_akta_perusahaan')->nullable();
            $table->string('no_akta_pendirian')->nullable();
            $table->string('tgl_berdiri')->nullable();
            $table->string('nomor_akta_perubahan')->nullable();
            $table->date('tanggal_akta_perubahan')->nullable();
            $table->string('telpon_perusahaan')->nullable();
            $table->string('alamat_perusahaan')->nullable();
            $table->string('provinsi_perusahaan')->nullable();
            $table->string('kota_perusahaan')->nullable();
            $table->string('kecamatan_perusahaan')->nullable();
            $table->string('kelurahan_perusahaan')->nullable();
            $table->string('kode_pos_perusahaan')->nullable();
            $table->decimal('omset_tahun_terakhir',15,2)->nullable();
            $table->decimal('tot_aset_tahun_terakhr',15,2)->nullable();
            $table->integer('laporan_keuangan')->nullable();
            $table->integer('is_valid_npwp')->nullable();
            $table->dateTime('tgl_validasi_npwp')->nullable();
            $table->string('nama_wp')->nullable();
            $table->integer('is_valid_rekening')->nullable();
            $table->dateTime('tgl_validasi_rekening')->nullable();
            $table->string('pemilik_rekening_asli')->nullable();
            $table->integer('is_verified_rekening')->nullable();
            $table->integer('lender_class')->nullable();
            $table->integer('is_inquiry_rekening')->nullable();
            $table->dateTime('tgl_inquiry_rekening')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_refferal');
    }
};
