<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dtl_lembar_pendapatan', function (Blueprint $table) {
            $table->integer('id_pengajuan')->nullable();
            $table->decimal('gaji_pokok_1',15,2)->nullable();
            $table->decimal('gaji_pokok_2',15,2)->nullable();
            $table->decimal('gaji_pokok_3',15,2)->nullable();
            $table->decimal('gaji_pokok_rata2',15,2)->nullable();
            $table->decimal('tunjangan_1',15,2)->nullable();
            $table->decimal('tunjangan_2',15,2)->nullable();
            $table->decimal('tunjangan_3',15,2)->nullable();
            $table->decimal('tunjangan_rata2',15,2)->nullable();
            $table->decimal('thp_1',15,2)->nullable();
            $table->decimal('thp_2',15,2)->nullable();
            $table->decimal('thp_3',15,2)->nullable();
            $table->decimal('thp_rata2',15,2)->nullable();
            $table->decimal('gaji_pokok_1_pasangan',15,2)->nullable();
            $table->decimal('gaji_pokok_2_pasangan',15,2)->nullable();
            $table->decimal('gaji_pokok_3_pasangan',15,2)->nullable();
            $table->decimal('gaji_pokok_rata2_pasangan',15,2)->nullable();
            $table->decimal('tunjangan_1_pasangan',15,2)->nullable();
            $table->decimal('tunjangan_2_pasangan',15,2)->nullable();
            $table->decimal('tunjangan_3_pasangan',15,2)->nullable();
            $table->decimal('tunjangan_rata2_pasangan',15,2)->nullable();
            $table->decimal('thp_1_pasangan',15,2)->nullable();
            $table->decimal('thp_2_pasangan',15,2)->nullable();
            $table->decimal('thp_3_pasangan',15,2)->nullable();
            $table->decimal('thp_rata2_pasangan',15,2)->nullable();
            $table->decimal('total_pendapatan',15,2)->nullable();
            $table->integer('cr_maks')->nullable();
            $table->decimal('maks_angsuran_sebelum',15,2)->nullable();
            $table->decimal('maks_angsuran_setelah',15,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dtl_lembar_pendapatan');
    }
};
