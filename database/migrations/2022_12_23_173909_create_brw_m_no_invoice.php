<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_m_no_invoice', function (Blueprint $table) {
            $table->integer('id_m_no_invoice');
            $table->integer('invoice_id')->nullable();
            $table->string('no_invoice')->nullable();
            $table->integer('brw_id')->nullable();
            $table->string('bulan')->nullable();
            $table->string('tahun')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_m_no_invoice');
    }
};
