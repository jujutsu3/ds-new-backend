<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_map', function (Blueprint $table) {
            $table->integer('id_rdl_map');
            $table->string('kode_bank')->nullable();
            $table->integer('id_dsi')->nullable();
            $table->string('kode_map')->nullable();
            $table->string('deskripsi_map')->nullable();
            $table->timestamps();
            $table->string('kategori')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_map');
    }
};
