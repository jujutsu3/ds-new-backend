<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_riwayat_pengurus_investor_badan_hukum_ori', function (Blueprint $table) {
            $table->integer('pengurus_id');
            $table->integer('hist_investor_id');
            $table->integer('investor_id');
            $table->string('nama_pengurus')->nullable();
            $table->integer('jenis_kelamin')->nullable();
            $table->string('nomor_ktp');
            $table->string('tempat_lahir')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('kode_negara')->nullable();//
            $table->string('no_hp')->nullable();
            $table->integer('agama')->nullable();
            $table->integer('pendidikan_terakhir')->nullable();
            $table->string('npwp')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('alamat')->nullable();
            $table->integer('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kode_pos')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_riwayat_pengurus_investor_badan_hukum_ori');
    }
};
