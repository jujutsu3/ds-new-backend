<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_status_pengajuan', function (Blueprint $table) {
            $table->integer('id_status_pengajuan');
            $table->string('status_LOS')->nullable();
            $table->string('keterangan_pengajuan')->nullable();
            $table->string('status_workflow')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('update_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_status_pengajuan');
    }
};
