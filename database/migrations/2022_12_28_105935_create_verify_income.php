<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_income', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id');
            $table->string('npwp');
            $table->string('valid_npwp')->nullable();
            $table->string('nik')->nullable();
            $table->string('valid_nik')->nullable();
            $table->string('name');
            $table->string('valid_name')->nullable();
            $table->string('match_result')->nullable();
            $table->string('income')->nullable();
            $table->string('valid_income')->nullable();
            $table->date('birthdate');
            $table->string('valid_birthdate')->nullable();
            $table->string('birthplace');
            $table->string('valid_birthplace')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_income');
    }
};
