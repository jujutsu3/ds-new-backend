<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dtl_naup', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->string('no_naup')->nullable();
            $table->integer('wakalah')->nullable();
            $table->string('tujuan_penggunaan_wakalah')->nullable();
            $table->decimal('jumlah_wakalah',15,2)->nullable();
            $table->string('jangka_waktu_wakalah')->nullable();
            $table->integer('peningkatan_pendanaan_wakalah')->nullable();
            $table->integer('peningkatan_jaminan_wakalah')->nullable();
            $table->integer('qardh')->nullable();
            $table->string('tujuan_penggunaan_qardh')->nullable();
            $table->string('take_over_dari_qardh')->nullable();
            $table->decimal('jumlah_qardh',15,2)->nullable();
            $table->string('jangka_waktu_qardh')->nullable();
            $table->integer('peningkatan_pendanaan_qardh')->nullable();
            $table->integer('peningkatan_jaminan_qardh')->nullable();
            $table->integer('murabahah')->nullable();
            $table->string('tujuan_penggunaan_murabahah')->nullable();
            $table->decimal('uang_muka_murabahah',15,2)->nullable();
            $table->decimal('denda_murabahah',15,2)->nullable();
            $table->integer('peningkatan_pendanaan_murabahah')->nullable();
            $table->integer('peningkatan_jaminan_murabahah')->nullable();
            $table->integer('imbt')->nullable();
            $table->string('tujuan_penggunaan_imbt')->nullable();
            $table->integer('penetapan_nilai_ujroh_selanjutnya')->nullable();
            $table->decimal('uang_muka_imbt',15,2)->nullable();
            $table->integer('peningkatan_pendanaan_imbt')->nullable();
            $table->integer('peningkatan_jaminan_imbt')->nullable();
            $table->decimal('denda_imbt',15,2)->nullable();
            $table->integer('mmq')->nullable();
            $table->string('tujuan_penggunaan_mmq')->nullable();
            $table->decimal('hishshah_per_unit',15,2)->nullable();
            $table->decimal('uang_muka_mmq',15,2)->nullable();
            $table->string('jangka_waktu_penyerahan_objek_mmq')->nullable();
            $table->integer('objek_bagi_hasil_mmq')->nullable();
            $table->integer('nisbah_penyelenggara')->nullable();
            $table->integer('nisbah_penerima_dana')->nullable();
            $table->decimal('denda_mmq',15,2)->nullable();
            $table->integer('ijrah')->nullable();
            $table->string('objek_sewa')->nullable();
            $table->string('jangka_waktu_penyerahan_ijrah')->nullable();
            $table->integer('penyesuaian_nilai_sewa')->nullable();
            $table->string('imbt_atas_nama')->nullable();
            $table->date('tanggal_penilaian_agunan')->nullable();
            $table->integer('appraisal')->nullable();
            $table->integer('rekomendasi_appraisal')->nullable();
            $table->integer('jenis_pengikatan_agunan')->nullable();
            $table->decimal('nilai_pengikatan_agunan',15,2)->nullable();
            $table->decimal('plafon_pembiayaan',15,2)->nullable();
            $table->integer('hasil_scoring_akhir')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dtl_naup');
    }
};
