<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_permintaan_perubahan', function (Blueprint $table) {
            $table->integer('request_id');
            $table->integer('hist_id');
            $table->string('client_name');
            $table->string('client_id'); // add client id to avoid crash
            $table->string('client_account')->nullable();
            $table->integer('client_type');
            $table->integer('user_type');
            $table->string('edit_by')->nullable();
            $table->dateTime('edit_date')->nullable();
            $table->string('approve_by')->nullable();
            $table->dateTime('approve_date')->nullable();
            $table->integer('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_permintaan_perubahan');
    }
};
