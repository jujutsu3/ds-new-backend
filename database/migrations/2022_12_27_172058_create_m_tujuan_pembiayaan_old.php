<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_tujuan_pembiayaan_old', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipe_id');
            $table->string('tujuan_pembiayaan')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
            $table->integer('max_tenor')->nullable();
            $table->float('min_pembiayaan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_tujuan_pembiayaan_old');
    }
};
