<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_pendanaan_rumah_lain', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengajuan_id')->nullable();
            $table->integer('rumah_ke')->nullable();
            $table->integer('status')->nullable();
            $table->string('bank')->nullable();
            $table->decimal('plafond',15,2)->nullable();
            $table->string('jangka_waktu')->nullable();
            $table->decimal('outstanding',15,2)->nullable();
            $table->decimal('angsuran',15,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_pendanaan_rumah_lain');
    }
};
