<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_pencairan', function (Blueprint $table) {
            $table->integer('pencairan_id');
            $table->integer('brw_id')->nullable();
            $table->integer('pendanaan_id')->nullable();
            $table->decimal('nominal_pencairan',15,2)->nullable();
            $table->decimal('nominal_imbal_hasil',15,2)->nullable();
            $table->date('tgl_req_pencairan')->nullable();
            $table->dateTime('tgl_pencairan')->nullable();
            $table->string('dicairkan_oleh')->nullable();
            $table->integer('status')->nullable();
            $table->string('bukti_transfer')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_pencairan');
    }
};
