<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cek_user_reg_sign', function (Blueprint $table) {
            $table->integer('id_user_cek_reg');
            $table->integer('investor_id');
            $table->integer('brw_id');
            $table->integer('provider_id');
            $table->string('userToken');
            $table->string('privyID');
            $table->string('status');
            $table->string('link_aktifasi');
            $table->date('tgl_register');
            $table->date('tgl_aktifasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cek_user_reg_sign');
    }
};
