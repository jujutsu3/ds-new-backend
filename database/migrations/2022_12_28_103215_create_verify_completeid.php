<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_completeid', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('verify_id');
            $table->string('nik');
            $table->string('name');
            $table->string('valid_name')->nullable();
            $table->date('birthdate');
            $table->string('valid_birthdate')->nullable();
            $table->string('birthplace');
            $table->string('valid_birthplace')->nullable();
            $table->string('address');
            $table->string('selfie_photo')->nullable();
            $table->integer('valid_selfie_photo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_completeid');
    }
};
