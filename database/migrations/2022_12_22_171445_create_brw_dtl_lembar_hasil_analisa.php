<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dtl_lembar_hasil_analisa', function (Blueprint $table) {
            $table->integer('id_pengajuan')->nullable();
            $table->decimal('maks_plafond_by_ftv',10,0)->nullable();
            $table->decimal('maks_plafond_by_cr',10,0)->nullable();
            $table->decimal('plafond_rekomendasi',10,0)->nullable();
            $table->decimal('angsuran',10,0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dtl_lembar_hasil_analisa');
    }
};
