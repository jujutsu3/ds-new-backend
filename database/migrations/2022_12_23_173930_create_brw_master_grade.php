<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_master_grade', function (Blueprint $table) {
            $table->integer('grade_id');
            $table->integer('Min')->nullable();
            $table->integer('Max')->nullable();
            $table->string('grade_nilai')->nullable();
            $table->string('grade_keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_master_grade');
    }
};
