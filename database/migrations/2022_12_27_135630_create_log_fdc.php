<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_fdc', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_fdc');
            $table->string('filename')->nullable();
            $table->integer('version')->nullable();
            $table->date('tanggal_file')->nullable();
            $table->dateTime('tanggal_upload')->nullable();
            $table->integer('status_kirim')->nullable();
            $table->integer('status_file')->nullable();
            $table->string('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_fdc');
    }
};
