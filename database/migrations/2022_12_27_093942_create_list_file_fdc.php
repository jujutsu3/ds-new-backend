<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_file_fdc', function (Blueprint $table) {
            $table->integer('id_fdc');
            $table->string('user_upload')->nullable();
            $table->string('filename')->nullable();
            $table->decimal('version',10,0)->nullable();
            $table->integer('status_pengiriman')->nullable();
            $table->integer('status_file')->nullable();
            $table->integer('parent')->nullable();
            $table->date('tanggal_file')->nullable();
            $table->date('tanggal_upload')->nullable();
            $table->string('bulan')->nullable();
            $table->string('tahun')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_file_fdc');
    }
};
