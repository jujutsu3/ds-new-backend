<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PenarikanDana extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penarikan_dana', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('investor_id');
            $table->string('jumlah');
            $table->string('no_rekening');
            $table->string('bank');
            $table->integer('accepted');
            $table->string('perihal')->nullable();
            $table->integer('alasan_penarikan')->nullable();
            $table->string('note_alasan_penarikan')->nullable();
            $table->string('alasan_penolakan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penarikan_dana');
    }
}
