<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpt_daily_payout', function (Blueprint $table) {
            $table->integer('investor_id');
            $table->string('nama')->nullable();
            $table->string('nama_investor')->nullable();
            $table->string('email');
            $table->string('bank_investor')->nullable();
            $table->string('bank')->nullable();
            $table->string('rekening')->nullable();
            $table->string('nama_pemilik_rek')->nullable();
            $table->date('tanggal_payout')->nullable();
            $table->decimal('imbal_payout',38,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpt_daily_payout');
    }
};
