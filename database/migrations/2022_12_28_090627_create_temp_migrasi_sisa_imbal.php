<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_migrasi_sisa_imbal', function (Blueprint $table) {
            $table->integer('proyek_id');
            $table->integer('investor_id');
            $table->increments('id');
            $table->decimal('total_dana',15,2);
            $table->decimal('profit_margin',5,2)->nullable();
            $table->string('tenor_waktu')->nullable();
            $table->decimal('total_imbal',15,2)->nullable();
            $table->decimal('sisa_imbal',15,2)->nullable();
            $table->decimal('proposional',15,2)->nullable();
            $table->decimal('imbal_hasil_bulanan',15,2)->nullable();
            $table->decimal('imbal_payout',15,2)->nullable();
            $table->integer('status_payout')->nullable();
            $table->integer('keterangan_payout')->nullable();
            $table->date('tanggal_payout')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_migrasi_sisa_imbal');
    }
};
