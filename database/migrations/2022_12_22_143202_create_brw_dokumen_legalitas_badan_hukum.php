<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dokumen_legalitas_badan_hukum', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brw_id');
            $table->string('akta_pendirian')->nullable();
            $table->string('akta_perubahan_terakhir')->nullable();
            $table->string('arus_kas_proyek')->nullable();
            $table->string('bukti_pks_bank')->nullable();
            $table->string('cv_developer_portofolio_projek')->nullable();
            $table->string('company_profile_developer')->nullable();
            $table->string('daftar_pembeli_peminat')->nullable();
            $table->string('feasibility_study')->nullable();
            $table->string('izin_gangguan')->nullable();
            $table->string('imb')->nullable();
            $table->string('jaminan_shm_shgb')->nullable();
            $table->string('jadwal_proyek_timeline')->nullable();
            $table->string('ktp_pengurus')->nullable();
            $table->string('laporan_keuangan_2thn_terakhir')->nullable();
            $table->string('file_nib')->nullable();
            $table->string('npwp_perusahaan')->nullable();
            $table->string('npwp_pengurus')->nullable();
            $table->string('file_pbb')->nullable();
            $table->string('rekening_koran_6bln_terakhir')->nullable();
            $table->string('rencana_anggaran_biaya')->nullable();
            $table->string('sk_pendaftaran_kemenkumham_akta_pendirian')->nullable();
            $table->string('surat_ganti_nama_wni')->nullable();
            $table->string('surat_permohonan_pengajuan')->nullable();
            $table->string('sk_pendaftaran_kemenkumham_akta_perubahan')->nullable();
            $table->string('surat_kuasa_dari_direksi')->nullable();
            $table->string('sertifikat_keanggotaan_developer')->nullable();
            $table->string('sppt_tahun_terakhir')->nullable();
            $table->string('stts_tahun_terakhir')->nullable();
            $table->string('site_plan')->nullable();
            $table->string('siup_siujk')->nullable();
            $table->string('situ')->nullable();
            $table->string('surat_keterangan_domisili')->nullable();
            $table->string('tdp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dokumen_legalitas_badan_hukum');
    }
};
