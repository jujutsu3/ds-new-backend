<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_persyaratan_pendanaan', function (Blueprint $table) {
            $table->integer('persyaratan_id');
            $table->integer('tipe_id');
            $table->integer('user_type');
            $table->string('persyaratan_nama');
            $table->integer('persyaratan_mandatory');
            $table->string('nama_folder')->nullable();
            $table->string('field_name')->nullable();
            $table->string('table_name')->nullable();
            $table->integer('category')->nullable();
            $table->string('page_title')->nullable();
            $table->string('category_title')->nullable();
            $table->string('file_type')->nullable();
            $table->integer('sort')->nullable();
            $table->integer('status_kawin')->nullable();
            $table->integer('skema_pembiayaan')->nullable();
            $table->integer('sumber_pengembalian_dana')->nullable();
            $table->integer('sumber_pengembalian_dana_pasangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_persyaratan_pendanaan');
    }
};
