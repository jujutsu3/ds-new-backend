<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_hd_analisa', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->string('nama_analis_dsi')->nullable();
            $table->integer('flag_rac_dsi')->nullable();
            $table->integer('flag_lembar_dsi')->nullable();
            $table->integer('flag_resume_dsi')->nullable();
            $table->dateTime('created_at_rac_dsi')->nullable();
            $table->dateTime('created_at_lembar_dsi')->nullable();
            $table->dateTime('created_at_resume_dsi')->nullable();
            $table->dateTime('updated_at_rac_dsi')->nullable();
            $table->dateTime('updated_at_lembar_dsi')->nullable();
            $table->dateTime('updated_at_resume_dsi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_hd_analisa');
    }
};
