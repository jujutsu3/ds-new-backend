<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dtl_resume_akad', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->decimal('porsi_syirkah_penerima_dana',15,2)->nullable();
            $table->integer('porsi_syirkah_penerima_dana_persentase')->nullable();
            $table->decimal('porsi_nisbah_dsi',15,2)->nullable();
            $table->integer('porsi_nisbah_dsi_persentase')->nullable();
            $table->decimal('porsi_nisbah_penerima_dana',15,2)->nullable();
            $table->integer('porsi_nisbah_penerima_dana_persentase')->nullable();
            $table->string('kesimpulan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dtl_resume_akad');
    }
};
