<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verify_lender', function (Blueprint $table) {
            $table->increments('id');
            $table->string('trx_id')->nullable();
            $table->string('ref_id')->nullable();
            $table->integer('investor_id');
            $table->integer('vendor_id');
            $table->string('vendor_name');
            $table->string('path');
            $table->string('origin');
            $table->string('errors')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verify_lender');
    }
};
