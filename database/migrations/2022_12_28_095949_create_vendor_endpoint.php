<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_endpoint', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendor_name')->default('');
            $table->string('shortname')->default('');
            $table->string('api_key')->default('');
            $table->string('merchant_id')->default('0');
            $table->string('param1')->nullable();
            $table->string('param2')->nullable();
            $table->string('param3')->nullable();
            $table->string('param4')->nullable();
            $table->string('param5')->nullable();
            $table->string('param6')->nullable();
            $table->string('param7')->nullable();
            $table->string('param8')->nullable();
            $table->string('param9')->nullable();
            $table->string('param10')->nullable();
            $table->integer('status_synch')->default(0);
            $table->string('endpoint_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_endpoint');
    }
};
