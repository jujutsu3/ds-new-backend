<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatbots', function (Blueprint $table) {
            $table->increments('id');
            $table->text('code')->nullable();
            $table->text('greetings')->nullable();
            $table->integer('category_id')->nullable();
            $table->text('category')->nullable();
            $table->integer('question_id')->nullable();
            $table->text('question')->nullable();
            $table->integer('answer_id')->nullable();
            $table->text('answer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatbots');
    }
};
