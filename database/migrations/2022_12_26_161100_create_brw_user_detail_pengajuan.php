<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_user_detail_pengajuan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengajuan_id');
            $table->integer('brw_id');
            $table->string('nama')->nullable();
            $table->string('nm_bdn_hukum')->nullable();
            $table->string('nib')->nullable();
            $table->string('npwp_perusahaan')->nullable();
            $table->string('no_akta_pendirian')->nullable();
            $table->string('no_akta_perubahan')->nullable();
            $table->date('tgl_akta_perubahan')->nullable();
            $table->string('nama_notaris_akta_perubahan')->nullable();
            $table->string('kedudukan_notaris_perubahan')->nullable();
            $table->string('no_sk_kemenkumham_perubahan')->nullable();
            $table->string('tgl_sk_kemenkumham_perubahan')->nullable();
            $table->date('tgl_berdiri')->nullable();
            $table->string('nama_notaris_akta_pendirian')->nullable();
            $table->string('kedudukan_notaris_pendirian')->nullable();
            $table->string('no_sk_kemenkumham_pendirian')->nullable();
            $table->string('tgl_sk_kemenkumham_pendirian')->nullable();
            $table->string('kode_telpon')->nullable();
            $table->string('telpon_perusahaan')->nullable();
            $table->string('foto_npwp_perusahaan')->nullable();
            $table->string('bidang_usaha')->nullable();
            $table->string('kedudukan_perusahaan')->nullable();
            $table->double('omset_tahun_terakhir',15,2)->nullable();
            $table->integer('tot_aset_tahun_terakhr')->nullable();
            $table->string('jabatan')->nullable();
            $table->integer('brw_type')->nullable();
            $table->string('nm_ibu')->nullable();
            $table->string('ktp')->nullable();
            $table->string('npwp')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('no_tlp')->nullable();
            $table->integer('jns_kelamin')->nullable();
            $table->integer('status_kawin')->nullable();
            $table->integer('status_rumah')->nullable();
            $table->string('alamat')->nullable();
            $table->string('domisili_alamat')->nullable();
            $table->string('domisili_provinsi')->nullable();
            $table->string('domisili_kota')->nullable();
            $table->string('domisili_kecamatan')->nullable();
            $table->string('domisili_kelurahan')->nullable();
            $table->integer('domisili_kd_pos')->nullable();
            $table->integer('domisili_status_rumah')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->integer('kode_pos')->nullable();
            $table->integer('agama')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->integer('pendidikan_terakhir')->nullable();
            $table->integer('pekerjaan')->nullable();
            $table->string('bidang_perusahaan')->nullable();
            $table->integer('bidang_pekerjaan')->nullable();
            $table->integer('bidang_online')->nullable();
            $table->integer('pengalaman_pekerjaan')->nullable();
            $table->integer('pendapatan')->nullable();
            $table->decimal('total_aset',15,2)->nullable();
            $table->string('kewarganegaraan')->nullable();
            $table->integer('brw_online')->nullable();
            $table->string('brw_pic')->nullable();
            $table->string('brw_pic_ktp')->nullable();
            $table->string('brw_pic_user_ktp')->nullable();
            $table->string('brw_pic_npwp')->nullable();
            $table->string('nomor_kk_pribadi')->nullable();
            $table->integer('lamamenempati')->nullable();
            $table->string('tlprumah')->nullable();
            $table->string('kartukredit_pribadi')->nullable();
            $table->integer('usiapensiun')->nullable();
            $table->string('alamatpenagihanrumah')->nullable();
            $table->string('alamatpenagihankantor')->nullable();
            $table->string('npwp_pasangan')->nullable();
            $table->string('nomor_kk_pasangan')->nullable();
            $table->string('tempat_lahir_pasangan')->nullable();
            $table->date('tgl_lahir_pasangan')->nullable();
            $table->integer('pendidikan_pasangan')->nullable();
            $table->string('sd_pasangan')->nullable();
            $table->string('smp_pasangan')->nullable();
            $table->string('sma_pasangan')->nullable();
            $table->string('pt_pasangan')->nullable();
            $table->integer('pekerjaan_pasangan')->nullable();
            $table->integer('bd_pekerjaan_pasangan')->nullable();
            $table->integer('bd_pekerjaanO_pasangan')->nullable();
            $table->integer('pengalaman_pasangan')->nullable();
            $table->integer('pendapatan_pasangan')->nullable();
            $table->string('kartukredit_pasangan')->nullable();
            $table->decimal('jmlasettdkbergerak_pasangan',15,2)->nullable();
            $table->decimal('jmlasetbergerak_pasangan',15,2)->nullable();
            $table->integer('agama_pasangan')->nullable();
            $table->string('pic_kk')->nullable();
            $table->string('pic_ktp_pasangan')->nullable();
            $table->string('pic_surat_nikah')->nullable();
            $table->string('pic_spt')->nullable();
            $table->string('pic_rekkoran')->nullable();
            $table->string('pic_slipgaji')->nullable();
            $table->string('pic_lapkeuangan')->nullable();
            $table->string('is_tbk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_user_detail_pengajuan');
    }
};
