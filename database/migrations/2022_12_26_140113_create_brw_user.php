<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_user', function (Blueprint $table) {
            $table->integer('brw_id');
            $table->string('username');
            $table->string('email');
            $table->string('password');
            $table->string('remember_token')->nullable();
            $table->string('email_verif')->nullable();
            $table->string('ref_number')->nullable();
            $table->string('status')->nullable();
            $table->string('otp')->nullable();
            $table->integer('password_expiry_days')->default(180)->nullable();
            $table->dateTime('password_updated_at')->nullable();
            $table->timestamps();
            $table->string('kyc_step')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_user');
    }
};
