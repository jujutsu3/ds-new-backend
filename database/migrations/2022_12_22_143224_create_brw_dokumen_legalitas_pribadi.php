<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dokumen_legalitas_pribadi', function (Blueprint $table) {
            $table->integer('brw_id')->nullable();
            $table->string('ktp')->nullable();
            $table->string('npwp')->nullable();
            $table->string('kartu_keluarga')->nullable();
            $table->string('buku_nikah')->nullable();
            $table->string('surat_keterangan_belum_menikah')->nullable();
            $table->string('akta_cerai')->nullable();
            $table->string('surat_domisili')->nullable();
            $table->string('surat_beda_nama')->nullable();
            $table->string('perjanjian_pra_nikah')->nullable();
            $table->string('surat_bekerja')->nullable();
            $table->string('surat_pengalaman_kerja')->nullable();
            $table->string('slip_gaji')->nullable();
            $table->string('mutasi_rekening')->nullable();
            $table->string('spt')->nullable();
            $table->string('dokumen_pendukung')->nullable();
            $table->string('siup')->nullable();
            $table->string('skdu')->nullable();
            $table->string('npwp_usaha')->nullable();
            $table->string('izin_praktek_profesi')->nullable();
            $table->string('laporan_keuangan')->nullable();
            $table->string('ktp_pasangan')->nullable();
            $table->string('npwp_pasangan')->nullable();
            $table->string('surat_persetujuan_pasangan')->nullable();
            $table->string('surat_bekerja_pasangan')->nullable();
            $table->string('surat_pengalaman_kerja_pasangan')->nullable();
            $table->string('slip_gaji_pasangan')->nullable();
            $table->string('mutasi_rekening_pasangan')->nullable();
            $table->string('spt_pasangan')->nullable();
            $table->string('dokumen_pendukung_pasangan')->nullable();
            $table->string('siup_pasangan')->nullable();
            $table->string('skdu_pasangan')->nullable();
            $table->string('npwp_usaha_pasangan')->nullable();
            $table->string('izin_praktek_profesi_pasangan')->nullable();
            $table->string('laporan_keuangan_pasangan')->nullable();
            $table->string('surat_rekomendasi_asosiasi')->nullable();
            $table->string('cv_pribadi_developer')->nullable();
            $table->string('company_profile_usaha')->nullable();
            $table->string('pricelist_proyek')->nullable();
            $table->string('dokumentasi_pengalaman_pks_dg_bank')->nullable();
            $table->string('tdp')->nullable();
            $table->string('foto_unit_tersedia')->nullable();
            $table->string('Video_unit_tersedia')->nullable();
            $table->string('shgb_skbpn')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dokumen_legalitas_pribadi');
    }
};
