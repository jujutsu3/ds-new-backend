<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_verifikasi_awal', function (Blueprint $table) {
            $table->integer('id_verifikasi');
            $table->integer('brw_id')->nullable();
            $table->integer('pengajuan_id')->nullable();
            $table->integer('skor_pefindo')->nullable();
            $table->integer('grade_pefindo')->nullable();
            $table->integer('skor_personal_credolab')->nullable();
            $table->integer('skor_pendanaan_credolab')->nullable();
            $table->integer('complete_id')->nullable();
            $table->integer('ocr')->nullable();
            $table->integer('npwp_a')->nullable();
            $table->integer('npwp_c')->nullable();
            $table->integer('npwp_d')->nullable();
            $table->integer('npwp_verify')->nullable();
            $table->integer('workplace_verification_f')->nullable();
            $table->integer('negative_list_verification_j')->nullable();
            $table->integer('verify_property_k')->nullable();
            $table->decimal('financing_to_value',10,0)->nullable();
            $table->integer('status')->nullable();
            $table->string('catatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_verifikasi_awal');
    }
};
