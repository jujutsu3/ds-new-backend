<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_pengajuan', function (Blueprint $table) {
            $table->integer('pengajuan_id');
            $table->integer('id_proyek')->nullable();
            $table->integer('id_field_verifikator_vendor')->nullable();
            $table->integer('brw_id')->nullable();
            $table->string('pendanaan_nama')->nullable();
            $table->integer('pendanaan_tipe')->nullable();
            $table->integer('pendanaan_tujuan')->nullable();
            $table->integer('pendanaan_akad')->nullable();
            $table->decimal('pendanaan_dana_dibutuhkan',15,2)->nullable();
            $table->decimal('pendanaan_dana_disetujui',15,2)->nullable();
            $table->decimal('harga_objek_pendanaan',15,2)->nullable();
            $table->decimal('uang_muka',15,2)->nullable();
            $table->date('estimasi_mulai')->nullable();
            $table->integer('estimasi_imbal_hasil')->nullable();
            $table->float('persentase_margin')->nullable();
            $table->integer('mode_pembayaran')->nullable();
            $table->integer('metode_pembayaran')->nullable();
            $table->integer('durasi_proyek')->nullable();
            $table->string('detail_pendanaan')->nullable();
            $table->decimal('dana_dicairkan',15,2)->nullable();
            $table->integer('status')->nullable();
            $table->integer('status_dana');
            $table->string('lokasi_proyek')->nullable();
            $table->string('geocode')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('va_number')->nullable();
            $table->string('gambar_utama')->nullable();
            $table->string('nm_pemilik')->nullable();
            $table->string('no_tlp_pemilik')->nullable();
            $table->string('alamat_pemilik')->nullable();
            $table->string('provinsi_pemilik')->nullable();
            $table->string('kota_pemilik')->nullable();
            $table->string('kecamatan_pemilik')->nullable();
            $table->string('kelurahan_pemilik')->nullable();
            $table->integer('kd_pos_pemilik')->nullable();
            $table->string('status_tampil_pengajuan')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('keterangan_approval')->nullable();
            $table->integer('jenis_properti')->nullable();
            $table->string('telepon_rumah')->nullable();
            $table->string('daya_listrik')->nullable();
            $table->integer('air')->nullable();
            $table->integer('internet_wifi')->nullable();
            $table->integer('jarak_dari_pusat_kota')->nullable();
            $table->integer('jumlah_lantai')->nullable();
            $table->string('dibangun_tahun')->nullable();
            $table->string('renovasi_tahun')->nullable();
            $table->integer('akses_jalan')->nullable();
            $table->integer('lebar_jalan')->nullable();
            $table->integer('lingkungan_sekitar')->nullable();
            $table->integer('dihuni_oleh')->nullable();
            $table->integer('hubungan_sebagai')->nullable();
            $table->integer('dikontrak_atau_sewa')->nullable();
            $table->date('tanggal_berakhir_kontrak')->nullable();
            $table->string('informasi_lain_lain')->nullable();
            $table->string('narasumber_nama')->nullable();
            $table->string('narasumber_selaku')->nullable();
            $table->string('narasumber_tlp_hp')->nullable();
            $table->string('pic_tampak_depan_objek')->nullable();
            $table->string('pic_tampak_dalam_objek')->nullable();
            $table->string('pic_tampak_kondisi_lingkungan_objek')->nullable();
            $table->string('pic_petugas')->nullable();
            $table->date('tanggal_kunjungan')->nullable();
            $table->integer('setuju_verifikator_pengajuan')->nullable();
            $table->integer('setuju_verifikator_dokumen')->nullable();
            $table->string('status_LOS')->nullable();
            $table->dateTime('created_at_LOS')->nullable();
            $table->dateTime('updated_at_LOS')->nullable();
            $table->string('app_no_LOS')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_pengajuan');
    }
};
