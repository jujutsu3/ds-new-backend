<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ih_pendanaan_aktif', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('investor_id')->nullable();
            $table->integer('proyek_id')->nullable();
            $table->decimal('total_dana',15,2)->nullable();
            $table->decimal('nominal_awal',15,2)->nullable();
            $table->date('tanggal_invest')->nullable();
            $table->integer('efektif_day_proposional')->nullable();
            $table->integer('status')->nullable();
            $table->date('last_pay')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ih_pendanaan_aktif');
    }
};
