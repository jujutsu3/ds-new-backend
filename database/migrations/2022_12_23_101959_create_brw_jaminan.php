<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_jaminan', function (Blueprint $table) {
            $table->integer('jaminan_id');
            $table->integer('pengajuan_id')->nullable();
            $table->string('jaminan_nama')->nullable();
            $table->string('jaminan_nomor')->nullable();
            $table->integer('jaminan_jenis')->nullable();
            $table->decimal('jaminan_nilai',15,2)->nullable();
            $table->string('jaminan_detail')->nullable();
            $table->string('kantor_penerbit')->nullable();
            $table->string('NOP')->nullable();
            $table->string('sertifikat')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_jaminan');
    }
};
