<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validasi_npwp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Nama_wp')->nullable();
            $table->string('Nik_wp')->nullable();
            $table->string('Npwp_wp')->nullable();
            $table->string('Alamat_wp')->nullable();
            $table->string('status_wp')->nullable();
            $table->string('status_spt')->nullable();
            $table->integer('tipe_pengguna')->nullable();
            $table->string('tipe_user')->nullable();
            $table->string('response_message')->nullable();
            $table->dateTime('Action_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validasi_npwp');
    }
};
