<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_riwayat_detil_investor_badan_hukum_ori', function (Blueprint $table) {
            $table->integer('hist_investor_id');
            $table->integer('investor_id');
            $table->string('nama_perusahaan')->nullable();
            $table->string('nib')->nullable()->nullable();
            $table->string('npwp_perusahaan')->nullable();
            $table->string('no_akta_pendirian')->nullable();
            $table->date('tgl_berdiri')->nullable();
            $table->string('nomor_akta_perusahaan')->nullable();
            $table->date('tanggal_akta_perusahaan')->nullable();
            $table->string('nomor_akta_perubahan')->nullable();
            $table->date('tanggal_akta_perubahan')->nullable();
            $table->string('telpon_perusahaan')->nullable(); // FIXME: another
            $table->string('foto_npwp_perusahaan')->nullable();
            $table->string('alamat_perusahaan')->nullable();
            $table->integer('provinsi_perusahaan')->nullable();
            $table->string('kota_perusahaan')->nullable();
            $table->string('kecamatan_perusahaan')->nullable();
            $table->string('kelurahan_perusahaan')->nullable();
            $table->string('kode_pos_perusahaan')->nullable();
            $table->string('rekening')->nullable();
            $table->string('nama_pemilik_rek')->nullable();
            $table->string('bank_investor')->nullable();
            $table->string('bidang_pekerjaan')->nullable();
            $table->decimal('omset_tahun_terakhir',15,2)->nullable();
            $table->decimal('tot_aset_tahun_terakhr',15,2)->nullable();
            $table->string('laporan_keuangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_riwayat_detil_investor_badan_hukum_ori');
    }
};
