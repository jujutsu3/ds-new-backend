<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ih_list_imbal_user_back', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('detilimbaluser_id')->nullable();
            $table->integer('pendanaan_id')->nullable();
            $table->date('tanggal_payout')->nullable();
            $table->decimal('imbal_payout',15,2)->nullable();
            $table->integer('status_payout')->nullable();
            $table->integer('keterangan_payout')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('ket_libur')->nullable();
            $table->string('jenis_pajak')->nullable();
            $table->decimal('nominal_pajak',10,0)->nullable();
            $table->integer('masa_pajak')->nullable();
            $table->string('tahun_pajak')->nullable();
            $table->float('tarif')->nullable();
            $table->decimal('nominal_transfer',10,0)->nullable();
            $table->string('kode_akun_pajak')->nullable();
            $table->string('jenis_setoran')->nullable();
            $table->string('no_bukti_potong')->nullable();
            $table->string('jenis_rek')->nullable();
            $table->string('kode_bank')->nullable();
            $table->string('no_rek')->nullable();
            $table->string('npwp_lender')->nullable();
            $table->string('nama_wp')->nullable();
            $table->string('nama_pemilik_rek')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ih_list_imbal_user_back');
    }
};
