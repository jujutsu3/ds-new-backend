<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_akad_digisign_investor', function (Blueprint $table) {
            $table->integer('id_log_akad_investor');
            $table->integer('investor_id');
            $table->integer('mutasi_id');
            $table->integer('pendanaan_id')->nullable();
            $table->integer('provider_id');
            $table->decimal('total_aset',15,2);
            $table->string('document_id');
            $table->string('docToken');
            $table->string('urlDocument');
            $table->string('downloadURL');
            $table->string('status');
            $table->integer('status_download');
            $table->date('tgl_sign');
            $table->timestamps();
            $table->integer('proyek_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_akad_digisign_investor');
    }
};
