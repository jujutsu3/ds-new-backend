<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_dhn_trx', function (Blueprint $table) {
            $table->integer('Case_ID');
            $table->date('Ref_Date')->nullable();
            $table->string('Created_By');
            $table->timestamp('Created_Date');
            $table->string('Description')->nullable();
            $table->string('Subject')->nullable();
            $table->string('Updated_By')->nullable();
            $table->timestamp('Updated_Date')->nullable();
            $table->string('Close_By')->nullable();
            $table->timestamp('Closed_Date')->nullable();
            $table->string('Cancelled_By')->nullable();
            $table->timestamp('Cancelled_Date')->nullable();
            $table->string('Status')->nullable();
            $table->string('Case_Type');
            $table->string('Parties_Name')->nullable();
            $table->integer('Parties_IDType')->nullable();
            $table->string('Parties_ID')->nullable();
            $table->string('Parties_Address')->nullable();
            $table->dateTime('Parties_Birthdate')->nullable();
            $table->string('Parties_Birthplace')->nullable();
            $table->string('Parties_Sex')->nullable();
            $table->string('Parties_Photo')->nullable();
            $table->string('Employee_LicID')->nullable();
            $table->integer('Employee_Division')->nullable();
            $table->string('Employee_DivOther')->nullable();
            $table->integer('Employee_Firm')->nullable();
            $table->string('Employee_FirmName')->nullable();
            $table->integer('Case_Related')->nullable();
            $table->string('Case_RelatedOther')->nullable();
            $table->string('CancelReason')->nullable();
            $table->string('Parties_ID2')->nullable();
            $table->string('Parties_ID3')->nullable();
            $table->string('Parties_Address2')->nullable();
            $table->string('Parties_Info1')->nullable();
            $table->string('Parties_Info2')->nullable();
            $table->string('Parties_Info3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_dhn_trx');
    }
};
