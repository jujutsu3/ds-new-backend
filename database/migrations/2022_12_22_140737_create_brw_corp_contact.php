<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_corp_contact', function (Blueprint $table) {
            $table->integer('contact_id');
            $table->integer('jenis_kelamin')->nullable();
            $table->integer('brw_id');
            $table->integer('urutan');
            $table->string('nm_contact')->nullable();
            $table->string('identitas_contact')->nullable();
            $table->integer('jenis_identitas')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('no_tlp')->nullable();
            $table->integer('agama')->nullable();
            $table->integer('pendidikan_terakhir')->nullable();
            $table->string('npwp')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('alamat')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('foto_diri')->nullable();
            $table->string('foto_ktp')->nullable();
            $table->string('foto_diri_ktp')->nullable();
            $table->string('foto_npwp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_corp_contact');
    }
};
