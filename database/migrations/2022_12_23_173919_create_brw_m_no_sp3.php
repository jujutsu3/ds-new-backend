<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_m_no_sp3', function (Blueprint $table) {
            $table->integer('id_no_sp3');
            $table->integer('id_pengajuan');
            $table->integer('no_sp3');
            $table->integer('proyek_id');
            $table->string('bln_sp3');
            $table->integer('thn_sp3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_m_no_sp3');
    }
};
