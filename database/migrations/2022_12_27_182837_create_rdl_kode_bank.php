<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_kode_bank', function (Blueprint $table) {
            $table->integer('id_rdl_kode_bank');
            $table->string('nama_bank')->nullable();
            $table->string('kode_interbank')->nullable();
            $table->string('kode_clearing')->nullable();
            $table->string('kode_rtgs')->nullable();
            $table->timestamps();
            $table->string('kode_bank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_kode_bank');
    }
};
