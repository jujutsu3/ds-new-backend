<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdl_tipe_charging', function (Blueprint $table) {
            $table->integer('id_tipe_charging');
            $table->string('kode_bank')->nullable();
            $table->integer('kode_charging')->nullable();
            $table->string('deskripsi_charging')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdl_tipe_charging');
    }
};
