<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_pasangan', function (Blueprint $table) {
            $table->integer('pasangan_id');
            $table->string('nama')->nullable();
            $table->integer('jenis_kelamin')->nullable();
            $table->string('ktp')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tgl_lahir')->nullable();
            $table->string('no_hp')->nullable();
            $table->integer('agama')->nullable();
            $table->integer('pendidikan')->nullable();
            $table->string('npwp')->nullable();
            $table->string('alamat')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kode_pos')->nullable();
            $table->string('no_kk')->nullable();
            $table->integer('brw_id2')->nullable();
            $table->integer('sumber_pengembalian_dana')->nullable();
            $table->integer('skema_pembiayaan')->nullable();
            $table->string('nama_perusahaan')->nullable();
            $table->string('alamat_perusahaan')->nullable();
            $table->string('rt_pekerjaan_pasangan')->nullable();
            $table->string('rw_pekerjaan_pasangan')->nullable();
            $table->string('provinsi_pekerjaan_pasangan')->nullable();
            $table->string('kab_kota_pekerjaan_pasangan')->nullable();
            $table->string('kecamatan_pekerjaan_pasangan')->nullable();
            $table->string('kelurahan_pekerjaan_pasangan')->nullable();
            $table->string('kode_pos_pekerjaan_pasangan')->nullable();
            $table->string('no_telp_pekerjaan_pasangan')->nullable();
            $table->string('no_hp_pekerjaan_pasangan')->nullable();
            $table->integer('surat_ijin')->nullable();
            $table->string('no_surat_ijin')->nullable();
            $table->integer('bentuk_badan_usaha')->nullable();
            $table->integer('status_pekerjaan')->nullable();
            $table->string('usia_perusahaan')->nullable();
            $table->string('usia_tempat_usaha')->nullable();
            $table->string('departemen')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('masa_kerja_tahun')->nullable();
            $table->string('masa_kerja_bulan')->nullable();
            $table->string('nip_nrp_nik')->nullable();
            $table->string('nama_hrd')->nullable();
            $table->string('no_fixed_line_hrd')->nullable();
            $table->string('pengalaman_kerja_tahun')->nullable();
            $table->string('pengalaman_kerja_bulan')->nullable();
            $table->decimal('pendapatan_borrower',15,2)->nullable();
            $table->decimal('biaya_hidup',15,2)->nullable();
            $table->string('detail_penghasilan_lain_lain')->nullable();
            $table->decimal('total_penghasilan_lain_lain',15,2)->nullable();
            $table->timestamps();
            $table->decimal('nilai_spt',15,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_pasangan');
    }
};
