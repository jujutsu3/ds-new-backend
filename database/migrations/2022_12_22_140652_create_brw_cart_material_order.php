<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_cart_material_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengajuan_id')->nullable();
            $table->integer('material_item_id');
            $table->integer('item_category_id')->nullable();
            $table->string('material_item_name')->nullable();
            $table->string('material_item_desc')->nullable();
            $table->string('material_item_url')->nullable();
            $table->integer('task_id');
            $table->integer('qty');
            $table->float('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_cart_material_order');
    }
};
