<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_material_task', function (Blueprint $table) {
            $table->integer('task_id');
            $table->string('task_number')->nullable();
            $table->string('task_name')->nullable();
            $table->string('task_description')->nullable();
            $table->string('enabled_flag')->nullable();
            $table->integer('seq')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamp('creation_date')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamp('last_update_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_material_task');
    }
};
