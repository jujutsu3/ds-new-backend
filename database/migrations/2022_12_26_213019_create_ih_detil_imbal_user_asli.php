<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ih_detil_imbal_user_asli', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pendanaan_id')->nullable();
            $table->integer('proyek_id')->nullable();
            $table->decimal('total_imbal',15,2)->nullable();
            $table->decimal('sisa_imbal',15,2)->nullable();
            $table->decimal('proposional',15,2)->nullable();
            $table->decimal('imbal_hasil_bulanan',15,2)->nullable();
            $table->decimal('prospek_hasil_diterima',15,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ih_detil_imbal_user_asli');
    }
};
