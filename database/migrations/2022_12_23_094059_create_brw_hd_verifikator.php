<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_hd_verifikator', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->string('nama_verifikator')->nullable();
            $table->string('spk')->nullable();
            $table->integer('nomor_spk')->nullable();
            $table->integer('flag_spk')->nullable();
            $table->dateTime('created_at_spk')->nullable();
            $table->dateTime('updated_at_spk')->nullable();
            $table->integer('flag_dokumen_ceklis')->nullable();
            $table->dateTime('created_at_dokumen_ceklis')->nullable();
            $table->dateTime('updated_at_dokumen_ceklis')->nullable();
            $table->integer('flag_rac')->nullable();
            $table->dateTime('created_at_rac')->nullable();
            $table->dateTime('updated_at_rac')->nullable();
            $table->integer('flag_interview_hrd')->nullable();
            $table->dateTime('created_at_interview_hrd')->nullable();
            $table->dateTime('updated_at_interview_hrd')->nullable();
            $table->integer('flag_kunjungan_domisili')->nullable();
            $table->dateTime('created_at_kunjungan_domisili')->nullable();
            $table->dateTime('updated_at_kunjungan_domisili')->nullable();
            $table->integer('flag_kunjungan_objek_pendanaan')->nullable();
            $table->dateTime('created_at_kunjungan_objek_pendanaan')->nullable();
            $table->dateTime('updated_at_kunjungan_objek_pendanaan')->nullable();
            $table->integer('flag_informasi_pendanaan_berjalan')->nullable();
            $table->dateTime('created_at_informasi_pendanaan_berjalan')->nullable();
            $table->dateTime('updated_at_informasi_pendanaan_berjalan')->nullable();
            $table->integer('flag_laporan_appraisal')->nullable();
            $table->dateTime('created_at_laporan_appraisal')->nullable();
            $table->dateTime('updated_at_laporan_appraisal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_hd_verifikator');
    }
};
