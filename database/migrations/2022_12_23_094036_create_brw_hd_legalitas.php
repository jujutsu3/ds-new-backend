<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_hd_legalitas', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->string('nama_legal')->nullable();
            $table->integer('flag_naup')->nullable();
            $table->integer('flag_biaya2')->nullable();
            $table->dateTime('created_at_naup')->nullable();
            $table->dateTime('created_at_biaya2')->nullable();
            $table->dateTime('updated_at_naup')->nullable();
            $table->dateTime('updated_at_biaya2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_hd_legalitas');
    }
};
