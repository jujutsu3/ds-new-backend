<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_akta_badan_hukum', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brw_id');
            $table->integer('jenis_akta');
            $table->string('judul_akta')->nullable();
            $table->string('nomor_akta')->nullable();
            $table->string('tanggal_akta')->nullable();
            $table->string('nama_notaris')->nullable();
            $table->string('lokasi_notaris_kota_kabupaten')->nullable();
            $table->string('nomor_sk_kemenkumham')->nullable();
            $table->date('tanggal_sk_kemenkumham')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_akta_badan_hukum');
    }
};
