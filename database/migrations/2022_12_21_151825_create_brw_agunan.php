<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_agunan', function (Blueprint $table) {
            $table->integer('id_pengajuan')->nullable();
            $table->integer('jenis_agunan')->nullable();
            $table->string('nomor_agunan')->nullable();
            $table->string('atas_nama_agunan')->nullable();
            $table->integer('luas_bangunan')->nullable();
            $table->integer('luas_tanah')->nullable();
            $table->string('nomor_surat_ukur')->nullable();
            $table->date('tanggal_surat_ukur')->nullable();
            $table->date('tanggal_terbit')->nullable();
            $table->date('tanggal_jatuh_tempo')->nullable();
            $table->integer('kantor_penerbit')->nullable();
            $table->string('blok_nomor')->nullable();
            $table->string('RT')->nullable();
            $table->string('RW')->nullable();
            $table->string('no_imb')->nullable();
            $table->date('tanggal_terbit_imb')->nullable();
            $table->integer('status_imb')->nullable();
            $table->integer('luas_tanah_imb')->nullable();
            $table->integer('luas_bangunan_imb')->nullable();
            $table->string('atas_nama_imb')->nullable();
            $table->string('tipe_agunan')->nullable();
            $table->integer('jenis_objek_pendanaan')->nullable();
            $table->decimal('nilai_pasar_wajar',15,2)->nullable();
            $table->decimal('nilai_likuidasi',15,2)->nullable();
            $table->integer('maks_ftv_persentase')->nullable();
            $table->decimal('maks_ftv_nominal',15,2)->nullable();
            $table->integer('fasilitas_ke')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_agunan');
    }
};
