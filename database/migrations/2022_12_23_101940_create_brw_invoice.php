<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_invoice', function (Blueprint $table) {
            $table->integer('invoice_id');
            $table->string('no_invoice')->nullable();
            $table->integer('pencairan_id')->nullable();
            $table->integer('brw_id')->nullable();
            $table->integer('pendanaan_id')->nullable();
            $table->dateTime('tgl_jatuh_tempo')->nullable();
            $table->decimal('nominal_tagihan_perbulan',15,2)->nullable();
            $table->integer('bulan_ke')->nullable();
            $table->integer('status')->nullable();
            $table->string('no_referal')->nullable();
            $table->decimal('nominal_transfer_tagihan',15,2)->nullable();
            $table->dateTime('tgl_pembayaran')->nullable();
            $table->dateTime('tgl_konfirmasi')->nullable();
            $table->string('konfirmasi_oleh')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_invoice');
    }
};
