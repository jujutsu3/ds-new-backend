<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_log_sp3', function (Blueprint $table) {
            $table->integer('id_log_sp3_borrower');
            $table->string('no_sp3');
            $table->integer('brw_id');
            $table->integer('id_proyek');
            $table->decimal('total_pendanaan',15,2);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_log_sp3');
    }
};
