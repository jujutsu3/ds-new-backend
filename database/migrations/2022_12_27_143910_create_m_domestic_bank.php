<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_domestic_bank', function (Blueprint $table) {
            $table->integer('id_bank');
            $table->string('kode_bank')->nullable();
            $table->string('domestic_code')->nullable();
            $table->string('nama_bank')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_domestic_bank');
    }
};
