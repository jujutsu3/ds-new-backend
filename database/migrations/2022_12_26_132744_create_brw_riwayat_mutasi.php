<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_riwayat_mutasi', function (Blueprint $table) {
            $table->integer('riwayat_id');
            $table->integer('brw_id');
            $table->integer('nominal');
            $table->string('tipe');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_riwayat_mutasi');
    }
};
