<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_verifikator_pengajuan_test', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengajuan_id');
            $table->integer('status_aktifitas');
            $table->string('rekomendasi_file')->nullable();
            $table->string('catatan_rekomendasi')->nullable();
            $table->dateTime('tgl_kirim')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->string('created_by')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->string('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_verifikator_pengajuan_test');
    }
};
