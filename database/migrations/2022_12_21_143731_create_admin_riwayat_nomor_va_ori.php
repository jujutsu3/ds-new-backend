<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_riwayat_nomor_va_ori', function (Blueprint $table) {
            $table->integer('hist_id');
            $table->integer('investor_id');
            $table->string('va_bank')->nullable();
            $table->string('kode_bank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_riwayat_nomor_va_ori');
    }
};
