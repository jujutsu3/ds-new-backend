<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_akad_digisign_borrower', function (Blueprint $table) {
            $table->integer('id_log_akad_borrower');
            $table->integer('brw_id');
            $table->integer('investor_id');
            $table->integer('id_proyek');
            $table->integer('provider_id');
            $table->decimal('total_pendanaan',10,0);
            $table->string('document_id');
            $table->string('docToken');
            $table->string('urlDocument');
            $table->string('downloadURL');
            $table->string('status');
            $table->date('tgl_sign');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_akad_digisign_borrower');
    }
};
