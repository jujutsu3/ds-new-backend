<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_material_requisition_dtl', function (Blueprint $table) {
            $table->integer('requisition_line_id');
            $table->integer('requisition_hdr_id')->nullable();
            $table->integer('line_number')->nullable();
            $table->integer('item_category_id')->nullable();
            $table->string('item_description')->nullable();
            $table->integer('material_item_id')->nullable();
            $table->string('material_item_name')->nullable();
            $table->string('unit_of_measure')->nullable();
            $table->integer('unit_price')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('task_id')->nullable();
            $table->date('need_by_date')->nullable();
            $table->string('note_to_agent')->nullable();
            $table->string('note_to_receiver')->nullable();
            $table->string('created_by')->nullable();
            $table->date('creation_date')->nullable();
            $table->string('last_updated_by')->nullable();
            $table->timestamp('last_update_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_material_requisition_dtl');
    }
};
