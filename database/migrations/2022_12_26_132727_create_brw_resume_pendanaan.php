<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brw_resume_pendanaan', function (Blueprint $table) {
            $table->integer('pengajuan_id')->nullable();
            $table->string('nama_kredit_analis')->nullable();
            $table->integer('rekomendasi_kredit_analis')->nullable();
            $table->string('catatan_kredit_analis')->nullable();
            $table->dateTime('tgl_keputusan_kredit_analis')->nullable();
            $table->dateTime('created_at_kredit_analis')->nullable();
            $table->dateTime('updated_at_kredit_analis')->nullable();
            $table->string('nama_compliance')->nullable();
            $table->integer('rekomendasi_compliance')->nullable();
            $table->string('catatan_compliance')->nullable();
            $table->dateTime('tgl_keputusan_compliance')->nullable();
            $table->dateTime('created_at_compliance')->nullable();
            $table->dateTime('updated_at_compliance')->nullable();
            $table->string('nama_risk_management')->nullable();
            $table->integer('rekomendasi_risk_management')->nullable();
            $table->string('catatan_risk_management')->nullable();
            $table->dateTime('tgl_keputusan_risk_management')->nullable();
            $table->dateTime('created_at_risk_management')->nullable();
            $table->dateTime('updated_at_risk_management')->nullable();
            $table->string('nama_legal')->nullable();
            $table->integer('rekomendasi_legal')->nullable();
            $table->string('catatan_legal')->nullable();
            $table->dateTime('tgl_keputusan_legal')->nullable();
            $table->dateTime('created_at_legal')->nullable();
            $table->dateTime('updated_at_legal')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brw_resume_pendanaan');
    }
};
