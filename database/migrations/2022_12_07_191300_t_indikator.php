<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TIndikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_indikator', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('updated_at');
            $table->float('tkb90')->nullable();
            $table->integer('jumlah_investor')->nullable();
            $table->integer('dana_crowd')->nullable();
            $table->integer('jumlah_marketer')->nullable();
            $table->integer('jumlah_proyek_aktif')->nullable();
            $table->integer('dana_pinjaman')->nullable();
            $table->integer('dana_pinjaman_2')->nullable();
            $table->integer('dana_crowd_2')->nullable();
            $table->integer('dana_pinjaman_3')->nullable();
            $table->integer('dana_crowd_3')->nullable();
            $table->integer('proyek_borrower')->nullable();
            $table->integer('proyek_borrower_aktif')->nullable();
            $table->integer('total_biaya')->nullable();
            $table->integer('proyek_selesai')->nullable();
            $table->integer('dana_proyek_selesai')->nullable();
            $table->integer('total_imbal_hasil')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
