<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('danasyariah', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengajuan')->nullable();
            $table->string('jenis_fasilitas')->nullable();
            $table->string('fasilitas_milik')->nullable();
            $table->integer('tenor')->nullable();
            $table->integer('margin')->nullable();
            $table->string('bank_lembaga_keuangan')->nullable();
            $table->decimal('plafond_awal',15,2)->nullable();
            $table->decimal('baki_debet',15,2)->nullable();
            $table->decimal('angsuran',15,2)->nullable();
            $table->string('history_payment')->nullable();
            $table->decimal('total_plafond_awal',15,2)->nullable();
            $table->decimal('total_baki_debet',15,2)->nullable();
            $table->decimal('total_angsuran',15,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('danasyariah');
    }
};
