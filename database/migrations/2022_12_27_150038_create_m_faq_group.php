<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_faq_group', function (Blueprint $table) {
            $table->integer('category_id');
            $table->integer('group_id');
            $table->string('group_name')->nullable();
            $table->string('group_type')->nullable();
            $table->string('group_description')->nullable();
            $table->string('enabled_flag')->nullable();
            $table->timestamp('creation_date')->nullable();
            $table->string('created_by')->nullable();
            $table->timestamp('last_update_date')->nullable();
            $table->string('last_updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_faq_group');
    }
};
