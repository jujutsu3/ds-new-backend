<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_riwayat_investor_ori', function (Blueprint $table) {
            $table->integer('hist_investor_id')->nullable();
            $table->integer('investor_id');
            $table->string('account_name')->nullable();
            $table->string('email')->nullable();
            $table->string('status')->nullable();
            $table->string('suspended_by')->nullable();
            $table->string('actived_by')->nullable();
            $table->string('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_riwayat_investor_ori');
    }
};
