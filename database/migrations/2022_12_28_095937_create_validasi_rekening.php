<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validasi_rekening', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipe_pengguna')->nullable();
            $table->integer('rekening_id')->nullable();
            $table->string('tipe_user')->nullable();
            $table->string('bank_number');
            $table->string('bank_account_number');
            $table->string('bank_account_name');
            $table->string('customer_account_number')->nullable();
            $table->string('customer_account_name')->nullable();
            $table->string('beneficiary_bank_name')->nullable();
            $table->string('response_code')->nullable();
            $table->string('response_message')->nullable();
            $table->string('account_type')->nullable();
            $table->string('currency')->nullable();
            $table->string('account_status')->nullable();
            $table->integer('is_valid')->default(0)->nullable();
            $table->integer('http_status')->nullable();
            $table->dateTime('action_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validasi_rekening');
    }
};
