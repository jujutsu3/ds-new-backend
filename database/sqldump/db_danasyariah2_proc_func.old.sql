-- --------------------------------------------------------
-- Host:                         172.16.0.118
-- Server version:               10.4.19-MariaDB - MariaDB Server
-- Server OS:                    Linux
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for danasyariah
CREATE DATABASE IF NOT EXISTS `danasyariah` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `danasyariah`;

-- Dumping structure for table danasyariah.ahli_waris_investor
CREATE TABLE IF NOT EXISTS `ahli_waris_investor` (
  `id_ahli_waris` int(11) NOT NULL AUTO_INCREMENT,
  `id_investor` int(11) DEFAULT NULL,
  `nama_ahli_waris` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hubungan_keluarga_ahli_waris` int(5) DEFAULT NULL,
  `nik_ahli_waris` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_operator` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_hp_ahli_waris` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_ahli_waris` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_ahli_waris`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8752 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.audit_trail
CREATE TABLE IF NOT EXISTS `audit_trail` (
  `id_log_audit_trail` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) DEFAULT NULL,
  `menu` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_log_audit_trail`)
) ENGINE=InnoDB AUTO_INCREMENT=49405 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.brw_los_log
CREATE TABLE IF NOT EXISTS `brw_los_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pengajuan_id` int(11) unsigned NOT NULL,
  `app_no` varchar(20) DEFAULT NULL,
  `request_data` longtext DEFAULT NULL,
  `response_data` longtext DEFAULT NULL,
  `method` varchar(25) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `message` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Los_Pengajuan_Id` (`pengajuan_id`),
  CONSTRAINT `Los_Pengajuan_Id` FOREIGN KEY (`pengajuan_id`) REFERENCES `brw_pengajuan` (`pengajuan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.deskripsi_proyeks
CREATE TABLE IF NOT EXISTS `deskripsi_proyeks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deskripsi` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2025 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.detil_imbal_user
CREATE TABLE IF NOT EXISTS `detil_imbal_user` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `investor_id` int(50) DEFAULT NULL,
  `proyek_id` int(50) DEFAULT NULL,
  `pendanaan_id` int(50) DEFAULT NULL,
  `total_imbal` decimal(15,2) DEFAULT NULL,
  `total_dana` decimal(15,2) DEFAULT NULL,
  `sisa_imbal` decimal(15,2) DEFAULT NULL,
  `proposional` decimal(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx01` (`pendanaan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29106 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.detil_investor
CREATE TABLE IF NOT EXISTS `detil_investor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `investor_id` int(11) NOT NULL,
  `tipe_pengguna` int(5) DEFAULT NULL,
  `jenis_badan_hukum` int(5) DEFAULT NULL,
  `nama_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_identitas` int(1) unsigned DEFAULT 1,
  `no_ktp_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_passpor_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_npwp_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_lahir_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_kelamin_investor` int(5) DEFAULT NULL,
  `status_kawin_investor` int(5) DEFAULT NULL,
  `alamat_investor` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `provinsi_investor` int(5) DEFAULT NULL,
  `kota_investor` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kelurahan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kecamatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos_investor` int(10) DEFAULT NULL,
  `status_rumah_investor` int(5) DEFAULT NULL,
  `kode_operator` varchar(10) COLLATE utf8_unicode_ci DEFAULT '62',
  `phone_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agama_investor` int(5) DEFAULT NULL,
  `pekerjaan_investor` int(5) DEFAULT NULL,
  `bidang_pekerjaan` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `online_investor` int(5) DEFAULT NULL,
  `pendapatan_investor` int(5) DEFAULT NULL,
  `asset_investor` int(5) DEFAULT NULL,
  `pengalaman_investor` int(5) DEFAULT NULL,
  `pendidikan_investor` int(5) DEFAULT NULL,
  `bank_investor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warganegara` int(1) DEFAULT 0,
  `domisili_negara` int(1) DEFAULT 0,
  `sumber_dana` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_ktp_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_user_ktp_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_tempat_lhr` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_tgl_lhr` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_jenis_kelamin` int(5) DEFAULT NULL,
  `pasangan_ktp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_npwp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_phone` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_alamat` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_provinsi` int(5) DEFAULT NULL,
  `pasangan_kota` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_kode_pos` int(10) DEFAULT NULL,
  `pasangan_agama` int(5) DEFAULT NULL,
  `pasangan_pekerjaan` int(5) DEFAULT NULL,
  `pasangan_bidang_pekerjaan` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_online` int(5) DEFAULT NULL,
  `pasangan_pendapatan` int(5) DEFAULT NULL,
  `pasangan_pengalaman` int(5) DEFAULT NULL,
  `pasangan_pendidikan` int(5) DEFAULT NULL,
  `nama_perwakilan` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_ktp_perwakilan` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_investor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rekening` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_pemilik_rek` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OTP` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_ibu_kandung` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_perusahaan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npwp_perusahaan` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto_npwp_perusahaan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nib` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_akta_pendirian` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_berdiri` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_notaris_akta_pendirian` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kedudukan_notaris_pendirian` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_sk_kemenkumham_pendirian` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_sk_kemenkumham_pendirian` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_akta_perubahan` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_akta_perubahan` date DEFAULT NULL,
  `nama_notaris_akta_perubahan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kedudukan_notaris_perubahan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_sk_kemenkumham_perubahan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_sk_kemenkumham_perubahan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telpon_perusahaan` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_perusahaan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provinsi_perusahaan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_perusahaan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kecamatan_perusahaan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kelurahan_perusahaan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos_perusahaan` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kedudukan_perusahaan` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `omset_tahun_terakhir` decimal(15,2) DEFAULT NULL,
  `tot_aset_tahun_terakhr` decimal(15,2) DEFAULT NULL,
  `laporan_keuangan` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_valid_npwp` tinyint(4) DEFAULT NULL,
  `tgl_validasi_npwp` datetime DEFAULT current_timestamp(),
  `nama_wp` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_valid_rekening` tinyint(4) DEFAULT NULL,
  `tgl_validasi_rekening` datetime DEFAULT NULL,
  `pemilik_rekening_asli` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_verified_rekening` tinyint(4) DEFAULT NULL,
  `tgl_verified_rekening` datetime DEFAULT current_timestamp(),
  `lender_class` tinyint(4) DEFAULT 0,
  `is_inquiry_rekening` tinyint(4) DEFAULT NULL,
  `tgl_inquiry_rekening` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `detil_investor_phone_investor_unique` (`phone_investor`) USING BTREE,
  KEY `idx01` (`investor_id`) USING BTREE,
  KEY `idx02` (`nama_investor`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=78990 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.e_coll_bni
CREATE TABLE IF NOT EXISTS `e_coll_bni` (
  `id_ecoll` int(11) NOT NULL AUTO_INCREMENT,
  `no_va` varchar(155) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(155) CHARACTER SET utf8 DEFAULT NULL,
  `tgl_payment` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_ecoll`),
  KEY `idx01` (`id_ecoll`)
) ENGINE=InnoDB AUTO_INCREMENT=31232 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.ih_detil_imbal_user
CREATE TABLE IF NOT EXISTS `ih_detil_imbal_user` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `pendanaan_id` int(50) DEFAULT NULL,
  `proyek_id` int(50) DEFAULT NULL,
  `total_imbal` decimal(15,2) DEFAULT NULL,
  `sisa_imbal` decimal(15,2) DEFAULT NULL,
  `proposional` decimal(15,2) DEFAULT NULL,
  `imbal_hasil_bulanan` decimal(15,2) DEFAULT NULL,
  `prospek_hasil_diterima` decimal(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=130971 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.ih_detil_imbal_user_asli
CREATE TABLE IF NOT EXISTS `ih_detil_imbal_user_asli` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `pendanaan_id` int(50) DEFAULT NULL,
  `proyek_id` int(50) DEFAULT NULL,
  `total_imbal` decimal(15,2) DEFAULT NULL,
  `sisa_imbal` decimal(15,2) DEFAULT NULL,
  `proposional` decimal(15,2) DEFAULT NULL,
  `imbal_hasil_bulanan` decimal(15,2) DEFAULT NULL,
  `prospek_hasil_diterima` decimal(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28927 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.ih_list_imbal_user
CREATE TABLE IF NOT EXISTS `ih_list_imbal_user` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `detilimbaluser_id` int(50) DEFAULT NULL,
  `pendanaan_id` int(50) DEFAULT NULL,
  `tanggal_payout` date DEFAULT NULL,
  `tanggal_payout2` date DEFAULT NULL,
  `imbal_payout` decimal(15,2) DEFAULT NULL,
  `status_payout` int(1) DEFAULT NULL,
  `status_payout2` int(1) DEFAULT 1,
  `keterangan_payout` int(1) DEFAULT NULL,
  `keterangan` varchar(111) DEFAULT NULL,
  `ket_libur` varchar(111) DEFAULT NULL,
  `jenis_pajak` varchar(15) DEFAULT 'PPH23',
  `nominal_pajak` decimal(10,0) DEFAULT 0,
  `nominal_pajak2` decimal(10,0) DEFAULT 0,
  `masa_pajak` int(11) DEFAULT NULL,
  `tahun_pajak` varchar(15) DEFAULT NULL,
  `tarif` float DEFAULT NULL,
  `tarif2` float DEFAULT 0,
  `nominal_transfer` decimal(10,0) DEFAULT NULL,
  `kode_akun_pajak` varchar(10) DEFAULT '411124 ',
  `jenis_setoran` varchar(5) DEFAULT '102',
  `no_bukti_potong` varchar(25) DEFAULT NULL,
  `jenis_rek` varchar(10) DEFAULT NULL,
  `kode_bank` varchar(5) DEFAULT NULL,
  `no_rek` varchar(20) DEFAULT NULL,
  `npwp_lender` varchar(20) DEFAULT NULL,
  `nama_wp` varchar(20) DEFAULT NULL,
  `nama_pemilik_rek` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id_billing_DJP` varchar(25) DEFAULT NULL,
  `NTPN_DJP` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pickup_status` tinyint(4) DEFAULT 0,
  `pickup_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1072053 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.ih_list_imbal_user_asli
CREATE TABLE IF NOT EXISTS `ih_list_imbal_user_asli` (
  `id` int(50) NOT NULL DEFAULT 0,
  `detilimbaluser_id` int(50) DEFAULT NULL,
  `pendanaan_id` int(50) DEFAULT NULL,
  `tanggal_payout` date DEFAULT NULL,
  `imbal_payout` decimal(15,2) DEFAULT NULL,
  `status_payout` int(1) DEFAULT NULL,
  `keterangan_payout` int(1) DEFAULT NULL,
  `keterangan` varchar(111) DEFAULT NULL,
  `ket_libur` varchar(111) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.ih_list_imbal_user_old
CREATE TABLE IF NOT EXISTS `ih_list_imbal_user_old` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `detilimbaluser_id` int(50) DEFAULT NULL,
  `pendanaan_id` int(50) DEFAULT NULL,
  `tanggal_payout` date DEFAULT NULL,
  `tanggal_payout2` date DEFAULT NULL,
  `imbal_payout` decimal(15,2) DEFAULT NULL,
  `status_payout` int(1) DEFAULT NULL,
  `status_payout2` int(1) DEFAULT 1,
  `keterangan_payout` int(1) DEFAULT NULL,
  `keterangan` varchar(111) DEFAULT NULL,
  `ket_libur` varchar(111) DEFAULT NULL,
  `jenis_pajak` varchar(15) DEFAULT 'PPH23',
  `nominal_pajak` decimal(10,0) DEFAULT 0,
  `nominal_pajak2` decimal(10,0) DEFAULT 0,
  `masa_pajak` int(11) DEFAULT NULL,
  `tahun_pajak` varchar(15) DEFAULT NULL,
  `tarif` float DEFAULT NULL,
  `tarif2` float DEFAULT 0,
  `nominal_transfer` decimal(10,0) DEFAULT NULL,
  `kode_akun_pajak` varchar(10) DEFAULT '411124 ',
  `jenis_setoran` varchar(5) DEFAULT '102',
  `no_bukti_potong` varchar(25) DEFAULT NULL,
  `jenis_rek` varchar(10) DEFAULT NULL,
  `kode_bank` varchar(5) DEFAULT NULL,
  `no_rek` varchar(20) DEFAULT NULL,
  `npwp_lender` varchar(20) DEFAULT NULL,
  `nama_wp` varchar(20) DEFAULT NULL,
  `nama_pemilik_rek` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id_billing_DJP` varchar(25) DEFAULT NULL,
  `NTPN_DJP` varchar(25) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pickup_status` tinyint(4) DEFAULT 0,
  `pickup_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=786723 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.investor_location
CREATE TABLE IF NOT EXISTS `investor_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `investor_id` int(11) DEFAULT NULL,
  `longitude` varchar(225) DEFAULT NULL,
  `latitude` varchar(225) DEFAULT NULL,
  `altitude` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34012 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.legalitas_proyeks
CREATE TABLE IF NOT EXISTS `legalitas_proyeks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deskripsi_legalitas` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2874 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.list_imbal_user
CREATE TABLE IF NOT EXISTS `list_imbal_user` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `proyek_id` int(50) DEFAULT NULL,
  `pendanaan_id` int(50) DEFAULT NULL,
  `investor_id` int(50) DEFAULT NULL,
  `tanggal_payout` date DEFAULT NULL,
  `imbal_payout` decimal(15,2) DEFAULT NULL,
  `total_dana` decimal(15,2) DEFAULT NULL,
  `status_payout` int(5) DEFAULT NULL,
  `status_update` int(5) DEFAULT NULL,
  `tgl_update` date DEFAULT NULL,
  `keterangan` varchar(225) DEFAULT NULL,
  `status_libur` int(11) DEFAULT NULL,
  `keterangan_libur` varchar(225) DEFAULT NULL,
  `ket_weekend` varchar(500) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx01` (`pendanaan_id`),
  KEY `idx02` (`status_payout`)
) ENGINE=InnoDB AUTO_INCREMENT=238250 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.log_db_app
CREATE TABLE IF NOT EXISTS `log_db_app` (
  `id_log` bigint(20) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(100) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id_log`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20622 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.log_imbal_user
CREATE TABLE IF NOT EXISTS `log_imbal_user` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `investor_id` int(15) DEFAULT NULL,
  `proyek_id` int(15) DEFAULT NULL,
  `pendanaan_id` int(15) DEFAULT NULL,
  `nominal` decimal(10,0) DEFAULT NULL,
  `id_listimbaluser` int(15) DEFAULT 0,
  `keterangan` varchar(225) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index 2` (`id_listimbaluser`),
  KEY `idx02` (`investor_id`),
  KEY `idx01` (`proyek_id`),
  KEY `idx03` (`pendanaan_id`),
  KEY `idx04` (`keterangan`)
) ENGINE=InnoDB AUTO_INCREMENT=615644 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.log_pengembalian_dana
CREATE TABLE IF NOT EXISTS `log_pengembalian_dana` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `proyek_id` int(5) DEFAULT NULL,
  `investor_id` int(5) DEFAULT NULL,
  `nominal` decimal(15,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx01` (`investor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69337 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.log_sertifikats
CREATE TABLE IF NOT EXISTS `log_sertifikats` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `investor_id` int(15) DEFAULT NULL,
  `seri_sertifikat` varchar(225) COLLATE utf8_bin DEFAULT NULL,
  `total_dana` decimal(20,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23714 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.migrasi_va_dsi
CREATE TABLE IF NOT EXISTS `migrasi_va_dsi` (
  `nama_investor` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `va_number` varchar(25) DEFAULT NULL,
  `status_dana` varchar(15) DEFAULT NULL,
  UNIQUE KEY `va_number` (`va_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.m_kode_pos
CREATE TABLE IF NOT EXISTS `m_kode_pos` (
  `id_kode_pos` int(5) NOT NULL DEFAULT 0,
  `kode_pos` int(5) DEFAULT NULL,
  `kode_provinsi` int(11) DEFAULT NULL,
  `kode_kota` varchar(5) DEFAULT NULL,
  `kelurahan` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `Jenis` varchar(5) DEFAULT NULL,
  `Kota` varchar(50) DEFAULT NULL,
  `Provinsi` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_kode_pos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.m_kode_pos_new
CREATE TABLE IF NOT EXISTS `m_kode_pos_new` (
  `provinsi` varchar(50) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kelurahan` varchar(50) DEFAULT NULL,
  `kode_pos` int(5) DEFAULT 99999
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.m_refferal
CREATE TABLE IF NOT EXISTS `m_refferal` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipe_pengguna` int(5) DEFAULT 2,
  `jenis_badan_hukum` int(5) DEFAULT NULL,
  `nama_refferal` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_identitas` int(1) unsigned DEFAULT 1,
  `no_ktp_investor` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_passpor_refferal` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_npwp_refferal` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir_refferal` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_lahir_refferal` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_kelamin_refferal` int(5) DEFAULT NULL,
  `status_kawin_investor` int(5) DEFAULT NULL,
  `alamat_investor` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `provinsi_investor` int(5) DEFAULT NULL,
  `kota_investor` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kelurahan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kecamatan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos_investor` int(10) DEFAULT NULL,
  `status_rumah_investor` int(5) DEFAULT NULL,
  `kode_operator` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '62',
  `phone_investor` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `agama_investor` int(5) DEFAULT NULL,
  `pekerjaan_investor` int(5) DEFAULT NULL,
  `bidang_pekerjaan` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `online_investor` int(5) DEFAULT NULL,
  `pendapatan_investor` int(5) DEFAULT NULL,
  `asset_investor` int(5) DEFAULT NULL,
  `pengalaman_investor` int(5) DEFAULT NULL,
  `pendidikan_investor` int(5) DEFAULT NULL,
  `bank_investor` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `warganegara` int(1) DEFAULT 0,
  `domisili_negara` int(1) DEFAULT 0,
  `sumber_dana` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_investor` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_ktp_investor` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pic_user_ktp_investor` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_investor` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_email` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_tempat_lhr` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_tgl_lhr` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_jenis_kelamin` int(5) DEFAULT NULL,
  `pasangan_ktp` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_npwp` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_phone` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_alamat` text CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_provinsi` int(5) DEFAULT NULL,
  `pasangan_kota` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_kode_pos` int(10) DEFAULT NULL,
  `pasangan_agama` int(5) DEFAULT NULL,
  `pasangan_pekerjaan` int(5) DEFAULT NULL,
  `pasangan_bidang_pekerjaan` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `pasangan_online` int(5) DEFAULT NULL,
  `pasangan_pendapatan` int(5) DEFAULT NULL,
  `pasangan_pengalaman` int(5) DEFAULT NULL,
  `pasangan_pendidikan` int(5) DEFAULT NULL,
  `nama_perwakilan` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_ktp_perwakilan` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_investor` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `rekening` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_pemilik_rek` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `OTP` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_ibu_kandung` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `npwp_perusahaan` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `foto_npwp_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nib` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_akta_perusahaan` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_akta_perusahaan` date DEFAULT NULL,
  `no_akta_pendirian` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_berdiri` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_akta_perubahan` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_akta_perubahan` date DEFAULT NULL,
  `telpon_perusahaan` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_perusahaan` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `provinsi_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kota_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kecamatan_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kelurahan_perusahaan` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos_perusahaan` varchar(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `omset_tahun_terakhir` decimal(15,2) DEFAULT NULL,
  `tot_aset_tahun_terakhr` decimal(15,2) DEFAULT NULL,
  `laporan_keuangan` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_valid_npwp` tinyint(4) DEFAULT NULL,
  `tgl_validasi_npwp` datetime DEFAULT current_timestamp(),
  `nama_wp` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_valid_rekening` tinyint(4) DEFAULT NULL,
  `tgl_validasi_rekening` datetime DEFAULT NULL,
  `pemilik_rekening_asli` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_verified_rekening` tinyint(4) DEFAULT NULL,
  `lender_class` tinyint(4) DEFAULT 0,
  `is_inquiry_rekening` tinyint(4) DEFAULT NULL,
  `tgl_inquiry_rekening` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78896 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.penarikan_dana
CREATE TABLE IF NOT EXISTS `penarikan_dana` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `investor_id` int(11) NOT NULL,
  `jumlah` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `no_rekening` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `accepted` tinyint(1) NOT NULL DEFAULT 0,
  `perihal` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alasan_penarikan` tinyint(2) DEFAULT NULL,
  `note_alasan_penarikan` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `alasan_penolakan` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx01` (`investor_id`),
  KEY `idx02` (`accepted`)
) ENGINE=InnoDB AUTO_INCREMENT=18151 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.pendanaan_aktif_old
CREATE TABLE IF NOT EXISTS `pendanaan_aktif_old` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `investor_id` int(11) NOT NULL,
  `proyek_id` int(11) NOT NULL,
  `total_dana` decimal(15,2) NOT NULL,
  `nominal_awal` decimal(15,2) NOT NULL,
  `tanggal_invest` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_pay` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx01` (`status`),
  KEY `idx02` (`proyek_id`,`status`),
  KEY `idx03` (`investor_id`),
  KEY `idx04` (`total_dana`)
) ENGINE=InnoDB AUTO_INCREMENT=100863 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.proyek_lama
CREATE TABLE IF NOT EXISTS `proyek_lama` (
  `id` int(10) unsigned NOT NULL,
  `nama` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `geocode` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `profit_margin` decimal(5,2) DEFAULT NULL,
  `total_need` decimal(15,2) DEFAULT NULL,
  `harga_paket` decimal(15,2) DEFAULT NULL,
  `akad` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `deskripsi` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_mulai_penggalangan` date DEFAULT NULL,
  `tgl_selesai_penggalangan` date DEFAULT NULL,
  `terkumpul` decimal(15,2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `status_tampil` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `waktu_bagi` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tenor_waktu` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `embed_picture` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_deskripsi` int(11) DEFAULT NULL,
  `id_legalitas` int(11) DEFAULT NULL,
  `id_pemilik` int(11) DEFAULT NULL,
  `id_simulasi` int(11) DEFAULT NULL,
  `gambar_utama` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `interval` tinyint(5) DEFAULT 1,
  `status_rekap` tinyint(5) DEFAULT 0,
  `lender_class` tinyint(5) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.proyek_old
CREATE TABLE IF NOT EXISTS `proyek_old` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `geocode` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profit_margin` decimal(5,2) DEFAULT NULL,
  `total_need` decimal(15,2) DEFAULT NULL,
  `harga_paket` decimal(15,2) DEFAULT NULL,
  `akad` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `deskripsi` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_mulai_penggalangan` date DEFAULT NULL,
  `tgl_selesai_penggalangan` date DEFAULT NULL,
  `terkumpul` decimal(15,2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `status_tampil` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `waktu_bagi` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tenor_waktu` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `embed_picture` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_deskripsi` int(11) DEFAULT NULL,
  `id_legalitas` int(11) DEFAULT NULL,
  `id_pemilik` int(11) DEFAULT NULL,
  `id_simulasi` int(11) DEFAULT NULL,
  `gambar_utama` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `interval` tinyint(5) DEFAULT 1,
  `status_rekap` tinyint(5) DEFAULT 0,
  `lender_class` tinyint(5) DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `idx01` (`status`),
  KEY `idx02` (`id`),
  KEY `idx03` (`tgl_mulai`),
  KEY `idx04` (`tgl_selesai`)
) ENGINE=InnoDB AUTO_INCREMENT=1909 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.rekening_investor
CREATE TABLE IF NOT EXISTS `rekening_investor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `investor_id` int(11) NOT NULL,
  `va_number` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_dana` decimal(15,2) NOT NULL,
  `unallocated` decimal(15,2) NOT NULL,
  `payment_ntb` char(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `kode_bank` char(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT '427',
  `type` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'V',
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  PRIMARY KEY (`id`),
  KEY `idx01` (`investor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78941 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.rekon_VA
CREATE TABLE IF NOT EXISTS `rekon_VA` (
  `va_no` varchar(20) DEFAULT NULL,
  `email_addr` varchar(100) DEFAULT NULL,
  UNIQUE KEY `va_no` (`va_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.simulasi_proyeks
CREATE TABLE IF NOT EXISTS `simulasi_proyeks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deskripsi_simulasi` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1919 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.temp_imbal_hasil_libur
CREATE TABLE IF NOT EXISTS `temp_imbal_hasil_libur` (
  `id_list_imbal_user` int(50) NOT NULL DEFAULT 0,
  `id_Proyek` int(10) unsigned NOT NULL DEFAULT 0,
  `nama` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `id_pendanaan` int(10) unsigned NOT NULL DEFAULT 0,
  `tanggal_payout` date DEFAULT NULL,
  `keterangan_payout` int(1) DEFAULT NULL,
  `ket_libur` varchar(111) DEFAULT NULL,
  PRIMARY KEY (`id_list_imbal_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.temp_migrasi_sisa_imbal
CREATE TABLE IF NOT EXISTS `temp_migrasi_sisa_imbal` (
  `proyek_id` int(11) NOT NULL,
  `investor_id` int(11) NOT NULL,
  `id` int(10) unsigned NOT NULL,
  `total_dana` decimal(15,2) NOT NULL,
  `profit_margin` decimal(5,2) DEFAULT NULL,
  `tenor_waktu` varchar(191) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_imbal` decimal(15,2) DEFAULT NULL,
  `sisa_imbal` decimal(15,2) DEFAULT NULL,
  `proposional` decimal(15,2) DEFAULT NULL,
  `imbal_hasil_bulanan` decimal(15,2) DEFAULT NULL,
  `imbal_payout` decimal(15,2) DEFAULT NULL,
  `status_payout` int(1) DEFAULT NULL,
  `keterangan_payout` int(1) DEFAULT NULL,
  `tanggal_payout` date DEFAULT NULL,
  KEY `id_tanggal_payout` (`id`,`tanggal_payout`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.temp_validasi_rekening
CREATE TABLE IF NOT EXISTS `temp_validasi_rekening` (
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `no_rek` varchar(20) DEFAULT NULL,
  `bank` varchar(100) DEFAULT NULL,
  `nama_pemilik_rek` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.tmp_eis_lender
CREATE TABLE IF NOT EXISTS `tmp_eis_lender` (
  `inv_id` int(11) DEFAULT NULL,
  `usia` int(11) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `pengguna` varchar(20) DEFAULT NULL,
  `nama_provinsi` varchar(50) DEFAULT NULL,
  `nama_kota` varchar(100) DEFAULT NULL,
  `pendapatan` varchar(50) DEFAULT NULL,
  `asset_investor` decimal(15,2) DEFAULT NULL,
  `nama_bank` varchar(50) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  UNIQUE KEY `inv_id` (`inv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.token
CREATE TABLE IF NOT EXISTS `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `investor_id` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `login_token` longtext COLLATE utf8_bin DEFAULT NULL,
  `mobile_token` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=951458 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.t_indikator
CREATE TABLE IF NOT EXISTS `t_indikator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `tkb90` float unsigned DEFAULT 0,
  `jumlah_investor` int(11) unsigned DEFAULT 0,
  `dana_crowd` bigint(20) unsigned DEFAULT 0,
  `jumlah_marketer` int(11) unsigned DEFAULT 0,
  `jumlah_proyek_aktif` int(11) unsigned DEFAULT 0,
  `dana_pinjaman` bigint(20) unsigned DEFAULT 0,
  `dana_pinjaman_2` bigint(20) unsigned DEFAULT 0,
  `dana_crowd_2` bigint(20) unsigned DEFAULT 0,
  `dana_pinjaman_3` bigint(20) unsigned DEFAULT 0,
  `dana_crowd_3` bigint(20) unsigned DEFAULT 0,
  `proyek_borrower` int(11) unsigned DEFAULT 0,
  `proyek_borrower_aktif` int(11) unsigned DEFAULT 0,
  `total_biaya` bigint(20) unsigned DEFAULT 0,
  `proyek_selesai` int(11) unsigned DEFAULT 0,
  `dana_proyek_selesai` bigint(20) unsigned DEFAULT 0,
  `total_imbal_hasil` bigint(20) unsigned DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `updated_at` (`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=466592 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.VA_DSI_BNI
CREATE TABLE IF NOT EXISTS `VA_DSI_BNI` (
  `va_no` varchar(20) DEFAULT NULL,
  UNIQUE KEY `va_no` (`va_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.va_prod_dsi
CREATE TABLE IF NOT EXISTS `va_prod_dsi` (
  `id` bigint(20) DEFAULT NULL,
  `va_number` varchar(25) DEFAULT NULL,
  UNIQUE KEY `va_number` (`va_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table danasyariah.verify_log
CREATE TABLE IF NOT EXISTS `verify_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `verify_id` int(10) unsigned DEFAULT NULL,
  `trx_id` varchar(30) DEFAULT NULL,
  `request_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `response_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(3) NOT NULL,
  `errors` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_verify_id` (`verify_id`),
  CONSTRAINT `FK_verify_id` FOREIGN KEY (`verify_id`) REFERENCES `verify` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for procedure danasyariah.brw_input_reg_borrower_by_tgl
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `brw_input_reg_borrower_by_tgl`(
	IN _tahun VARCHAR(4),
	IN _bulan VARCHAR(2),
	IN _hari VARCHAR(2),
	OUT res_ int
)
BEGIN   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE gagal INT;
	DECLARE whereTgl TEXT;
	DECLARE krit TEXT;
	DECLARE result TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		GET DIAGNOSTICS CONDITION 1
		code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
		SET result = CONCAT('failed, error = ',code,', message = ',msg);
		ROLLBACK;
		SET gagal = -1;
		INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('proc: brw_input_reg_borrower_by_tgl',999,result,NOW());
	END;

	SET gagal = 1;
	IF _tahun != 0 AND _bulan != 0 AND _hari != 0 THEN
		SET krit = "%Y%m%d"; 
		SET whereTgl = CONCAT(_tahun,_bulan,_hari);
	ELSEIF _tahun != 0 AND _bulan != 0 AND _hari = 0 THEN
		SET krit = "%Y%m"; 
		SET whereTgl = CONCAT(_tahun,_bulan);
	ELSEIF _tahun != 0 AND _bulan = 0 AND _hari = 0 THEN
		SET krit = "%Y"; 
		SET whereTgl = _tahun;
	ELSE
		SET gagal = -1;
		SET result = "parameter salah, parameter tahun harus terisi ";
		INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('proc: brw_input_reg_borrower_by_tgl',999,result,NOW());
	END IF;

    if ( -1 != gagal ) then
		INSERT INTO rpt_pusdafil_reg_borrower_temp
				(id_penyelenggara, id_pengguna, id_borrower,
				total_aset, status_kepemilikan_rumah) 
		SELECT 	810059,CONCAT('2','-',a.brw_id),a.brw_id,
				IFNULL(b.tot_aset_tahun_terakhr, 0) tot_aset_tahun_terakhir,
				case when b.brw_type = 2 then 3 else IFNULL(b.status_rumah, 2) end status_rumah
		FROM 	brw_user a, brw_user_detail b
		WHERE 	a.brw_id = b.brw_id 
				AND DATE_FORMAT(a.created_at,krit) = whereTgl ;
	end if;
	
	SELECT gagal ;
   
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.brw_input_reg_lender_by_tgl
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `brw_input_reg_lender_by_tgl`(
	IN `_tahun` VARCHAR(50),
	IN `_bulan` VARCHAR(50),
	IN `_hari` VARCHAR(50)

)
BEGIN

DECLARE code CHAR(5) DEFAULT '00000';
DECLARE msg TEXT;
DECLARE gagal INT;
DECLARE whereTgl TEXT;
DECLARE krit TEXT;
DECLARE result TEXT;

DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
       ROLLBACK;
       SET gagal = 1;
       INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('proc: brw_input_reg_lender_by_tgl',999,result,NOW());
     END;
     
truncate table rpt_pusdafil_reg_lender_temp;
IF _tahun != 0 AND _bulan != 0 AND _hari != 0 THEN
	SET krit = "%Y%m%d"; 
	SET whereTgl = CONCAT(_tahun,_bulan,_hari);
ELSEIF _tahun != 0 AND _bulan != 0 AND _hari = 0 THEN
	SET krit = "%Y%m"; 
	SET whereTgl = CONCAT(_tahun,_bulan);
ELSEIF _tahun != 0 AND _bulan = 0 AND _hari = 0 THEN
	SET krit = "%Y"; 
	SET whereTgl = _tahun;
ELSE
	SET gagal = 1;
END IF;

 INSERT INTO rpt_pusdafil_reg_lender_temp 
		(id_penyelenggara,
		id_pengguna,
		id_lender,
		id_negara_domisili,
		id_kewarganegaraan,
		sumber_dana)
	SELECT 810059,
		concat('1','-',a.id),
		a.id,
		0,
		0,
		'penghasilan sendiri'
	FROM investor a
	JOIN detil_investor b ON a.id = b.investor_id
	WHERE DATE_FORMAT(a.created_at,krit) = whereTgl 
	AND LENGTH(no_ktp_investor) = 16
	AND substr(no_ktp_investor,9,2) <= 12
	AND alamat_investor IS NOT NULL;

SET gagal = 0;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.brw_input_reg_pengguna_by_tgl
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `brw_input_reg_pengguna_by_tgl`(
	IN `_tahun` VARCHAR(50),
	IN `_bulan` VARCHAR(50),
	IN `_hari` VARCHAR(50)

)
BEGIN

DECLARE code CHAR(5) DEFAULT '00000';
DECLARE msg TEXT;
DECLARE gagal INT;
DECLARE whereTgl TEXT;
DECLARE krit TEXT;
DECLARE result TEXT;

DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
       ROLLBACK;
       SET gagal = 1;
       INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('proc: brw_input_reg_pengguna_by_tgl',999,result,NOW());
     END;

SET gagal = 0;
truncate table rpt_pusdafil_reg_pengguna_temp;
IF _tahun != 0 AND _bulan != 0 AND _hari != 0 THEN
	SET krit = "%Y%m%d"; 
	SET whereTgl = CONCAT(_tahun,_bulan,_hari);
ELSEIF _tahun != 0 AND _bulan != 0 AND _hari = 0 THEN
	SET krit = "%Y%m"; 
	SET whereTgl = CONCAT(_tahun,_bulan);
ELSEIF _tahun != 0 AND _bulan = 0 AND _hari = 0 THEN
	SET krit = "%Y"; 
	SET whereTgl = _tahun;
ELSE
	SET gagal = 1;
	SET result = "parameter salah, parameter tahun harus terisi ";
	INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('proc: brw_input_reg_pengguna_by_tgl',999,result,NOW());
END IF;
			
			INSERT INTO rpt_pusdafil_reg_pengguna_temp 
			 	(id_penyelenggara,
				 id_pengguna,
				 jenis_pengguna,
				 tgl_registrasi,
				 nama_pengguna,
				 jenis_identitas,
				 no_identitas,
				 no_npwp,
				 id_jenis_badan_hukum,
				 tempat_lahir,
				 tgl_lahir,
				 id_jenis_kelamin,
				 alamat,
				 id_kota,
				 id_provinsi,
				 kode_pos,
				 id_agama,
				 id_status_perkawinan,
				 id_pekerjaan,
				 id_bidang_pekerjaan,
				 id_pekerjaan_online,
				 pendapatan,
				 pengalaman_kerja,
				 id_pendidikan,
				 nama_perwakilan,
				 no_identitas_perwakilan)
			SELECT 
					810059,
					concat('1','-',a.id),
					1,
					DATE_FORMAT(a.created_at,'%Y-%m-%d'),
					IFNULL(b.nama_investor,a.username),
					1,
					CASE WHEN no_ktp_investor = 0 THEN \N WHEN no_ktp_investor = 'null' THEN \N WHEN LENGTH(no_ktp_investor) <> 16 THEN \N ELSE no_ktp_investor END AS KTP,
					CASE WHEN no_npwp_investor = 0 THEN \N WHEN no_npwp_investor = 'null' THEN \N WHEN LENGTH(no_npwp_investor) <> 15 THEN \N ELSE no_npwp_investor END AS NPWP,
					5,
					tempat_lahir_investor,
					date_format(str_to_date(tgl_lahir_investor, '%d-%m-%Y'), '%Y-%m-%d'),
					CASE WHEN Jenis_kelamin_investor = 0 THEN 1 ELSE IFNULL(Jenis_kelamin_investor,1) END,
					alamat_investor,
					CASE WHEN kota_investor = 0 THEN 'e420' ELSE IFNULL(kota_investor,'e420') END,
					CASE WHEN provinsi_investor = 0 THEN 11 ELSE IFNULL(provinsi_investor,11) END,
					kode_pos_investor,
					CASE WHEN agama_investor = 0 THEN 1 ELSE IFNULL(agama_investor,1) END,
					CASE WHEN status_kawin_investor = 0 THEN 1 ELSE IFNULL(status_kawin_investor,1) END,
					CASE WHEN pekerjaan_investor = 0 THEN 1 ELSE IFNULL(pekerjaan_investor,1) END,
					case when bidang_pekerjaan = 0 then 'e99' ELSE IFNULL(bidang_pekerjaan,'e99') end,
					case when online_investor IS NULL OR online_investor=0 then 2 ELSE 1 END,
					CASE WHEN pendapatan_investor = 0 THEN 3 ELSE IFNULL(pendapatan_investor, 3) END AS pendapatan_investor,
					CASE WHEN trim(pengalaman_investor) = 0 THEN 1 ELSE IFNULL(pengalaman_investor, 1) END AS pengalaman_investor,
					CASE WHEN pendidikan_investor = 0 THEN 3 ELSE IFNULL(pendidikan_investor,5) END,
					"-",
					"-"
				FROM investor a, detil_investor b
 				WHERE a.id = b.investor_id
 				AND DATE_FORMAT(a.created_at,krit) = whereTgl 
				AND LENGTH(no_ktp_investor) = 16
				AND substr(no_ktp_investor,9,2) <= 12
				AND alamat_investor IS NOT NULL;

SELECT krit,whereTgl,gagal ;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.check_first_create
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `check_first_create`(
	IN `_table_name` varchar(35),
	IN `_key_name` varchar(35),
	IN `_key` int,
	IN `_desc` VARCHAR(100),
	IN `_file` varchar(100),
	IN `_line` INT,
	OUT `sout` varchar(1000)

)
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
  	declare res varchar(100) default NULL;
#	declare sout varchar(1000) default '0';	
			
	declare exit handler for sqlexception
     begin
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('is_first_create failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
     end;
       
  
	#--------------------------------------------------------------
	
	set _table_name = trim(_table_name);
	set _desc = trim(_desc);


	set @getname = concat("select ",_desc," into @var1 from ",_table_name," where ",_key_name," = '",_key,"' ");
	prepare stmt from @getname;
	execute stmt ;

	set res = @var1;
	deallocate prepare stmt;

	if res is not null then
		set sout = '1';	
	else
		set sout = '0';							 
   end if;
	
	#select sout ;

end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.create_report_payout
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `create_report_payout`(
	IN `begin_date_` VARCHAR(15),
	IN `end_date_` varchar(15),
	IN `file_` VARCHAR(100),
	IN `line_` INT,
	OUT `res_` DECIMAL(15,2)
)
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
	DECLARE investor_id_ INT;
	DECLARE prev_investor_id_ INT; 
	DECLARE nama_  VARCHAR(10);
	DECLARE nama_temp_ VARCHAR(500);
	DECLARE done INT;
	DECLARE b_out	INT;
	DECLARE b_first INT;
	DECLARE counter INT;
	DECLARE max_loop INT;
			
	DECLARE c_menu CURSOR FOR			
		SELECT  d.investor_id, SUBSTR(c.nama,11,INSTR(c.nama,'/')-11)
		FROM 	ih_list_imbal_user a,
				ih_detil_imbal_user b,
				proyek c,
				pendanaan_aktif d
		WHERE DATE_FORMAT(a.tanggal_payout,'%Y%m%d') >= begin_date_  
				AND DATE_FORMAT(a.tanggal_payout,'%Y%m%d') <= end_date_
				AND a.imbal_payout != 0
				AND a.detilimbaluser_id = b.id
				#and c.STATUS IN (2,3,4)
				AND a.keterangan_payout IN (1,2)
				AND c.id = d.proyek_id
				AND d.id = a.pendanaan_id		
		GROUP BY d.investor_id, c.nama
		ORDER by investor_id;	

/*
	DECLARE c_menu CURSOR FOR
		SELECT investor_id , substr(nama,11,4) nama FROM temp_investor_proyek2 ORDER BY 1;
*/		 	
	DECLARE CONTINUE HANDLER for not FOUND SET done =1 ;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
       ROLLBACK;
       SET res_ = -1;
       INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;
     
	SET b_out = 0;
	SET counter = 0;
	SET max_loop = 5000;
	SET prev_investor_id_ = 0;
	SET nama_temp_ = "Pendanaan";
	SET b_first = 1;
	 		
	 
	 TRUNCATE TABLE temp_investor_proyek;
	 TRUNCATE TABLE rpt_daily_payout;
	 		
	 Open c_menu;	    
	 While (0 = b_out AND counter < max_loop) DO
	 	FETCH c_menu into investor_id_ ,  nama_;	 
			  	 
				If 1 = done Then
					set b_out = 1;
				else					 
						if 0 = b_first then
					 		 if prev_investor_id_ != investor_id_ then
					      	INSERT INTO temp_investor_proyek (investor_id, nama) VALUES (prev_investor_id_, nama_temp_);
								SET nama_temp_ = "Pendanaan" ;	 
							END if;						
						ELSE 
							set b_first = 0;
						END if;
						
						set counter = counter + 1;
						SET prev_investor_id_ = investor_id_ ;
						set nama_temp_ = CONCAT(nama_temp_,'-', nama_);						
				End If;			
					
	 END While;
	 INSERT INTO temp_investor_proyek (investor_id, nama) VALUES (prev_investor_id_, nama_temp_);
	 Close c_menu;
	 
	 INSERT INTO rpt_daily_payout
		SELECT 	a.investor_id , e.nama, b.nama_investor, c.email, b.bank_investor, d.nama_bank, b.rekening,
					b.nama_pemilik_rek, a.tanggal_payout , a.imbal_payout
		from
		(
			SELECT 	a.investor_id, a.tanggal_payout, sum(a.imbal_payout) imbal_payout from
			(
					SELECT 	d.investor_id,a.tanggal_payout , c.tgl_mulai ,
								case when ( DATEDIFF(a.tanggal_payout,c.tgl_mulai) < 35 ) then 
										sum(a.imbal_payout + b.proposional) ELSE sum(a.imbal_payout) END imbal_payout
								#,a.imbal_payout , b.proposional, c.tgl_mulai,a.pendanaan_id 
					FROM 	ih_list_imbal_user a,
							ih_detil_imbal_user b,
							proyek c,
							pendanaan_aktif d
					WHERE DATE_FORMAT(a.tanggal_payout,'%Y%m%d') >= begin_date_
							AND DATE_FORMAT(a.tanggal_payout,'%Y%m%d') <= end_date_
							AND a.imbal_payout != 0
							AND a.detilimbaluser_id = b.id
							#and c.STATUS IN (2,3,4)
							AND a.keterangan_payout IN(1,2)
							AND c.id = d.proyek_id
							AND d.id = a.pendanaan_id		
					GROUP BY d.investor_id,a.tanggal_payout , c.tgl_mulai
			)a		
			GROUP BY a.investor_id,a.tanggal_payout		
		)a,
		detil_investor b,
		investor c,
		temp_investor_proyek e,
		m_bank	d
		WHERE a.investor_id = b.investor_id
				AND a.investor_id = c.id
				AND a.investor_id = e.investor_id
				AND b.bank_investor = d.kode_bank;

	 SET res_ = 1;
	 	 	 
	 SELECT res_ ;
   
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.generate_sabtuminggu
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `generate_sabtuminggu`(
	IN `year` YEAR



)
BEGIN
	declare keterangan varchar(50);
	declare startdate date;
	declare enddate date;
	
	SET startdate = concat(year,'-01-01');
	SET enddate = concat(year,'-12-31');
	SET enddate = enddate + INTERVAL 1 YEAR;
	
	WHILE startDate<=enddate DO
		IF(DAYNAME(startDate)='Saturday') THEN
			SET keterangan = "Hari Sabtu";
			insert into m_harilibur(tgl_harilibur,deskripsi,created_at) values(startDate,keterangan,NOW());
		ELSEIF( DAYNAME(startDate)='Sunday') THEN
			SET keterangan = "Hari Minggu";
			insert into m_harilibur(tgl_harilibur,deskripsi,created_at) values(startDate,keterangan,NOW());
		END IF;
		
		SET startDate=startDate + INTERVAL 1 DAY;
		END WHILE;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.get_desc_master
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `get_desc_master`(
	IN `table_name_` VARCHAR(25),
	IN `desc_` int,
	IN `file_` VARCHAR(100),
	IN `line_` INT,
	OUT `res` VARCHAR(155)
)
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE filter_ VARCHAR(25);
  	DECLARE get_field_ VARCHAR(100);

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	 SET res = NULL;
	 SET @var1 = NULL;
	 
	 CASE table_name_
		WHEN 'm_agama' THEN
			SET filter_ = 'id_agama';
			SET get_field_ = 'agama';
		WHEN 'm_asset' THEN
			SET filter_ = 'id_asset';
			SET get_field_ = 'asset';
		WHEN 'm_badan_hukum' THEN
			SET filter_ = 'id_badan_hukum';
			SET get_field_ = 'badan_hukum';
		WHEN 'm_bidang_pekerjaan' THEN
			SET filter_ = 'id_bidang_pekerjaan';
			SET get_field_ = 'bidang_pekerjaan';
		WHEN 'm_jenis_jaminan' THEN
			SET filter_ = 'id_jenis_jaminan';
			SET get_field_ = 'jenis_jaminan';
		WHEN 'm_jenis_kelamin' THEN
			SET filter_ = 'id_jenis_kelamin';
			SET get_field_ = 'jenis_kelamin';
		WHEN 'm_jenis_pengguna' THEN
			SET filter_ = 'id_jenis_pengguna';
			SET get_field_ = 'jenis_pengguna';
		WHEN 'm_kategori' THEN
			SET filter_ = 'id_kategori';
			SET get_field_ = 'kategori';
		WHEN 'm_kawin' THEN
			SET filter_ = 'id_kawin';
			SET get_field_ = 'jenis_kawin';	
		WHEN 'm_kepemilikan_rumah' THEN
			SET filter_ = 'id_kepemilikan_rumah';
			SET get_field_ = 'kepemilikan_rumah';
		WHEN 'm_negara' THEN
			SET filter_ = 'id_negara';
			SET get_field_ = 'negara';
		WHEN 'm_online' THEN
			SET filter_ = 'id_online';
			SET get_field_ = 'tipe_online';
		WHEN 'm_pekerjaan' THEN
			SET filter_ = 'id_pekerjaan';
			SET get_field_ = 'pekerjaan';
		WHEN 'm_pendapatan' THEN
			SET filter_ = 'id_pendapatan';
			SET get_field_ = 'pendapatan';
		WHEN 'm_pendidikan' THEN
			SET filter_ = 'id_pendidikan';
			SET get_field_ = 'pendidikan';
		WHEN 'm_pengalaman_kerja' THEN
			SET filter_ = 'id_pengalaman_kerja';
			SET get_field_ = 'pengalaman_kerja';
		WHEN 'm_tampilan_proyek' THEN
			SET filter_ = 'id_tampilan_proyek';
			SET get_field_ = 'status_tampil';
		WHEN 'm_status_proyek' THEN
			SET filter_ = 'id_status_proyek';
			SET get_field_ = 'status_proyek';
		WHEN 'm_term_condition' THEN
			SET filter_ = 'id_term_condition';
			SET get_field_ = 'term_condition';
		WHEN 'm_tipe_invest' THEN
			SET filter_ = 'id_tipe_invest';
			SET get_field_ = 'tipe_invest';
		WHEN 'm_notif' THEN
			SET filter_ = 'id_notif';
			SET get_field_ = 'notifikasi';
		WHEN 'm_hub_ahli_waris' THEN
			SET filter_ = 'id_hub_ahli_waris';
			SET get_field_ = 'jenis_hubungan';	
		END CASE;
    
    SET table_name_ = TRIM(table_name_);
	 SET desc_ = TRIM(desc_);
	
 	  	
 	 SET @GetName = CONCAT("SELECT ",get_field_," into @var1 FROM ",table_name_," WHERE ",filter_," = '",desc_,"' ");
  	 PREPARE stmt FROM @GetName;
  	 EXECUTE stmt ;
  	 
  	 SET res = @var1;
  	 DEALLOCATE PREPARE stmt;
  	 
  	 IF res is NULL THEN
   	  SET result = CONCAT('Look up desc failed, on   ', @GetName);
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
    END IF;

	 SELECT res; 
     
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.get_id_master
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `get_id_master`(
	IN `table_name_` VARCHAR(25),
	IN `desc_` VARCHAR(155),
	IN `file_` VARCHAR(100),
	IN `line_` INT,
	OUT `res` INT
)
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE filter_ VARCHAR(25);
  	DECLARE get_field_ VARCHAR(100);

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	 SET res = 0;
	 SET @var1 = 0;
	 
	 CASE table_name_
		WHEN 'm_agama' THEN
			SET get_field_ = 'id_agama';
			SET filter_ = 'agama';
		WHEN 'm_asset' THEN
			SET get_field_ = 'id_asset';
			SET filter_ = 'asset';
		WHEN 'm_badan_hukum' THEN
			SET get_field_ = 'id_badan_hukum';
			SET filter_ = 'badan_hukum';
		WHEN 'm_bidang_pekerjaan' THEN
			SET get_field_ = 'id_bidang_pekerjaan';
			SET filter_ = 'bidang_pekerjaan';
		WHEN 'm_jenis_jaminan' THEN
			SET get_field_ = 'id_jenis_jaminan';
			SET filter_ = 'jenis_jaminan';
		WHEN 'm_jenis_kelamin' THEN
			SET get_field_ = 'id_jenis_kelamin';
			SET filter_ = 'jenis_kelamin';
		WHEN 'm_jenis_pengguna' THEN
			SET get_field_ = 'id_jenis_pengguna';
			SET filter_ = 'jenis_pengguna';
		WHEN 'm_kategori' THEN
			SET get_field_ = 'id_kategori';
			SET filter_ = 'kategori';
		WHEN 'm_kawin' THEN
			SET get_field_ = 'id_kawin';
			SET filter_ = 'jenis_kawin';	
		WHEN 'm_kepemilikan_rumah' THEN
			SET get_field_ = 'id_kepemilikan_rumah';
			SET filter_ = 'kepemilikan_rumah';
		WHEN 'm_negara' THEN
			SET get_field_ = 'id_negara';
			SET filter_ = 'negara';
		WHEN 'm_online' THEN
			SET get_field_ = 'id_online';
			SET filter_ = 'tipe_online';
		WHEN 'm_pekerjaan' THEN
			SET get_field_ = 'id_pekerjaan';
			SET filter_ = 'pekerjaan';
		WHEN 'm_pendapatan' THEN
			SET get_field_ = 'id_pendapatan';
			SET filter_ = 'pendapatan';
		WHEN 'm_pendidikan' THEN
			SET get_field_ = 'id_pendidikan';
			SET filter_ = 'pendidikan';
		WHEN 'm_pengalaman_kerja' THEN
			SET get_field_ = 'id_pengalaman_kerja';
			SET filter_ = 'pengalaman_kerja';
		WHEN 'm_tampilan_proyek' THEN
			SET get_field_ = 'id_tampilan_proyek';
			SET filter_ = 'status_tampil';
		WHEN 'm_status_proyek' THEN
			SET get_field_ = 'id_status_proyek';
			SET filter_ = 'status_proyek';
		WHEN 'm_term_condition' THEN
			SET get_field_ = 'id_term_condition';
			SET filter_ = 'term_condition';
		WHEN 'm_tipe_invest' THEN
			SET get_field_ = 'id_tipe_invest';
			SET filter_ = 'tipe_invest';
		WHEN 'm_notif' THEN
			SET get_field_ = 'id_notif';
			SET filter_ = 'notifikasi';
		WHEN 'm_hub_ahli_waris' THEN
			SET filter_ = 'jenis_hubungan';
			SET get_field_ = 'id_hub_ahli_waris'; 
		END CASE;
    
    SET table_name_ = TRIM(table_name_);
	 SET desc_ = TRIM(desc_);
	
 	  	
 	 SET @GetName = CONCAT("SELECT ",get_field_," into @var1 FROM ",table_name_," WHERE upper(",filter_,") = upper('",desc_,"') ");
  	 PREPARE stmt FROM @GetName;
  	 EXECUTE stmt ;
  	 
  	 SET res = @var1;
  	 DEALLOCATE PREPARE stmt;
  	 
  	 IF res = 0 THEN
   	  SET result = CONCAT('Look up id failed, on   ', @GetName);
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
    END IF;

	 SELECT res; 
     
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.pay_for_borrower
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `pay_for_borrower`(
	IN `brw_id_` int,
	IN `pendanaan_id_` int,
	IN `bukti_id_` int,
	IN `paid_` decimal(15,2),
	IN `pic_pembayaran_` varchar(75),
	IN `no_referal_` varchar(25),
	IN `confirmed_by_` varchar(25),
	IN `file_` VARCHAR(100),
	IN `line_` INT,
	OUT `res_` int
)
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
	DECLARE invoice_id_ INT;
	DECLARE nominal_  decimal(15,2);
	DECLARE bayar_ decimal(15,2);
	DECLARE first_paid_ decimal(15,2);
	DECLARE v_status INT;
	DECLARE done INT;
	DECLARE b_out	INT;
	DECLARE counter INT;
   DECLARE flag_ int;
	DECLARE max_loop INT;
			
	DECLARE c_menu CURSOR FOR
    	SELECT invoice_id ,  nominal_tagihan_Perbulan , IFNULL(nominal_transfer_tagihan, 0), status FROM brw_invoice 
		WHERE STATUS not IN (1,6) -- paid , reject 
				and brw_id = brw_id_
				AND pendanaan_id = pendanaan_id_
		ORDER BY bulan_ke asc;
		 	
	DECLARE CONTINUE HANDLER for not FOUND SET done =1 ;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
       ROLLBACK;
       SET res_ = -1;
       INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;
     
	SET b_out = 0;
	SET counter = 0;
    set flag_ = 0;
	SET max_loop = 12;
	SET first_paid_ = paid_ ;
	 						 		

	 Open c_menu;
	 While (0 = b_out AND counter < max_loop) Do
		  	FETCH c_menu into invoice_id_ ,  nominal_, bayar_, v_status; 
			
			If 1 = done Then
				set b_out = 1;
			else
						
				if ( paid_ >= nominal_ - bayar_ ) then
					UPDATE brw_invoice 
						SET nominal_transfer_tagihan = nominal_ , 
							 STATUS = 1, -- paid
							 no_referal = no_referal_ ,
							 tgl_pembayaran = NOW() ,
							 tgl_konfirmasi = CURRENT_TIMESTAMP() ,
							 konfirmasi_oleh = confirmed_by_,
							 updated_at = current_timestamp()
					WHERE invoice_id = invoice_id_ ;
										
					if ( 1 = flag_ ) then
						INSERT into brw_bukti_pembayaran 
							(invoice_id, brw_id, pendanaan_id, nominal, pic_pembayaran, ref_no, confirmed_by, status, tgl_bukti_bayar, created_at, updated_at) 
						values 
							(invoice_id_, brw_id_, pendanaan_id_, nominal_, pic_pembayaran_, no_referal_, confirmed_by_, 1, current_timestamp(), current_timestamp(), current_timestamp() );
					else
						UPDATE brw_bukti_pembayaran 
							SET  status = 1, -- partial paid
								 nominal = nominal_ - bayar_,
								 confirmed_by = confirmed_by_,
								 updated_at = current_timestamp()
						 WHERE invoice_id = invoice_id_  AND bukti_id = bukti_id_ AND status not in (1, 3, 6);
					end if; 			
												
					set flag_ = 1;		
												
					SET paid_ = paid_ - nominal_ + bayar_ ;
					if 0 = paid_ then
						SET b_out = 1;
					END if;
								
				else
					UPDATE brw_invoice 
						SET nominal_transfer_tagihan = paid_ + bayar_, 
							 status = 3, -- partial
							 no_referal = no_referal_ ,
							 tgl_pembayaran = NOW() ,
							 tgl_konfirmasi = CURRENT_TIMESTAMP() ,
							 konfirmasi_oleh = confirmed_by_,
							 updated_at = current_timestamp()
					WHERE invoice_id = invoice_id_ ;
				
					
					SET b_out = 1;
				
					if ( 1 = flag_ ) then							
						INSERT into brw_bukti_pembayaran 
								(invoice_id, brw_id, pendanaan_id, nominal, ref_no, confirmed_by, pic_pembayaran, status, tgl_bukti_bayar, created_at, updated_at) 
							values 
								(invoice_id_, brw_id_, pendanaan_id_, paid_ + bayar_, no_referal_, confirmed_by_,pic_pembayaran_, 3, current_timestamp(), current_timestamp(), current_timestamp() );							
					else			
						UPDATE brw_bukti_pembayaran 
							SET   status = 3, -- partial paid
								  nominal = paid_,
								  confirmed_by = confirmed_by_,
								  updated_at = current_timestamp()
							WHERE invoice_id = invoice_id_ AND bukti_id = bukti_id_ AND status not in (1, 3, 6);							
				   end if;
				  
				   SET paid_ = 0;
					   
				END if;
				set counter = counter + 1;
				
			End If;
			
	 END While;
	 Close c_menu;
	 
	 
	 if  first_paid_ = paid_ then 
	 		SET res_ = -1;
	 ELSE  SET res_ = paid_;
	 END if;
		   
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.procImbalHasil
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `procImbalHasil`(
	IN `idproyek` INT


















)
    COMMENT 'generateImbalHasil'
BEGIN

DECLARE tgl varchar(100);
DECLARE totalrow_PA INT;
DECLARE totalrow_DIU INT;
DECLARE totalrow_LIU INT;
DECLARE v_id INT;
DECLARE v_nama varchar(100);
DECLARE v_tenor_waktu int;
DECLARE v_tgl_selesai_penggalangan date;
DECLARE v_profit_margin int;
DECLARE v_tgl_mulai date;
DECLARE v_tgl_selesai date;
DECLARE counter int;
DECLARE v_tanggal_invest date;
DECLARE rowCountDescription INT DEFAULT 0;
DECLARE rowCountDIU INT DEFAULT 0;
DECLARE v_total_dana int;
DECLARE marginbulanan DECIMAL(20,4);
DECLARE totalimbal DECIMAL(20,4);
DECLARE sisamargin DECIMAL(20,4);
DECLARE sisaimbal DECIMAL(20,4);
DECLARE selisihtanggal int;
DECLARE pengaliproposional DECIMAL(20,6);
DECLARE proposional DECIMAL(20,4);
DECLARE v_investor_id int;
DECLARE pengalimarginlesstwelve DECIMAL(20,6);
DECLARE updatePA CURSOR FOR
    SELECT id from pendanaan_aktif where pendanaan_aktif.proyek_id = idproyek AND status = '1';
DECLARE prosesLIU CURSOR FOR
    SELECT id from detil_imbal_user where detil_imbal_user.proyek_id = idproyek;
DECLARE prosesLIU_SD CURSOR FOR
    SELECT id from detil_imbal_user where detil_imbal_user.proyek_id = idproyek;



SELECT id,nama,tenor_waktu,tgl_selesai_penggalangan,profit_margin,tgl_mulai,tgl_selesai
into v_id,v_nama,v_tenor_waktu,v_tgl_selesai_penggalangan,v_profit_margin,v_tgl_mulai,v_tgl_selesai
from proyek
WHERE id = idproyek;

Select count(*) into totalrow_PA from pendanaan_aktif where pendanaan_aktif.proyek_id = idproyek AND status = '1';


START TRANSACTION;
OPEN updatePA;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE pen_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      

      updatePALoop: LOOP
        FETCH updatePA INTO pen_id;
            IF exit_flag THEN LEAVE updatePALoop; 
            END IF;
            SELECT total_dana,tanggal_invest,investor_id INTO v_total_dana,v_tanggal_invest,v_investor_id FROM pendanaan_aktif WHERE id = pen_id;
            SET selisihtanggal = datediff(v_tgl_selesai_penggalangan,v_tanggal_invest)+1;
            SET pengalimarginlesstwelve = (v_profit_margin/12);
            IF v_profit_margin < 12 THEN
			       SET marginbulanan = FLOOR(((pengalimarginlesstwelve*v_total_dana)/100)/100)*100;
			       SET totalimbal = marginbulanan*v_tenor_waktu;
			       SET sisaimbal = 0;
			       SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
			   	 SET proposional = FLOOR((pengaliproposional*v_total_dana)/100)*100;
			   ELSE
			   	SET marginbulanan = (1*v_total_dana)/100;
			   	SET totalimbal = marginbulanan*v_tenor_waktu;
			   	SET sisamargin = ((v_profit_margin/12)*v_tenor_waktu)-v_tenor_waktu;
			   	SET sisaimbal = (v_total_dana*sisamargin)/100;
			   	SET pengaliproposional = (0.01/30)*selisihtanggal;
			   	SET proposional = FLOOR((pengaliproposional*v_total_dana)/100)*100;
			   END IF;
			   
            INSERT INTO detil_imbal_user (investor_id, proyek_id, pendanaan_id, total_imbal, total_dana, sisa_imbal,proposional,created_at) VALUES (v_investor_id,idproyek,pen_id,totalimbal,v_total_dana,sisaimbal,proposional,NOW());
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE updatePA;
  
Select count(*) into totalrow_DIU from detil_imbal_user where detil_imbal_user.proyek_id = idproyek;


IF totalrow_PA = totalrow_DIU THEN
	COMMIT;
ELSE
	ROLLBACK;
END IF;



START TRANSACTION;
OPEN prosesLIU;
  BEGIN
  		DECLARE v_totaldana int(10);
      DECLARE v_totalimbal int(10);
      DECLARE imbalpayout int(10);
      DECLARE v_pendanaan_id int(10);
      DECLARE v_investor_id int(10);
      DECLARE tgljadi varchar(111);
      DECLARE tglimbal varchar(12);
      DECLARE keteranganweekend varchar(111);
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE DIU_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      

      prosesLIULoop: LOOP
        FETCH prosesLIU INTO DIU_id;
            IF exit_flag THEN LEAVE prosesLIULoop; 
            END IF;
            SELECT pendanaan_id,investor_id,total_imbal,total_dana into v_pendanaan_id,v_investor_id,v_totalimbal,v_totaldana from detil_imbal_user where detil_imbal_user.id = DIU_id;
            	
            	SET imbalpayout = v_totalimbal/v_tenor_waktu;
					SET counter = 1;
					
					looping_tanggal: LOOP
					IF counter > v_tenor_waktu THEN
						LEAVE looping_tanggal;
						END IF;
						SET tgl = v_tgl_mulai + INTERVAL counter MONTH;
						SELECT cek_tanggallibur(tgl) into tgljadi;
						SET keteranganweekend = SUBSTRING_INDEX(tgljadi,"|",1);
						SET tglimbal = SUBSTRING_INDEX(tgljadi,"|",-1);
						INSERT INTO list_imbal_user (proyek_id,pendanaan_id,investor_id,tanggal_payout, imbal_payout, total_dana, status_payout, status_update, ket_weekend,created_at) VALUES (idproyek,v_pendanaan_id,v_investor_id,tglimbal,imbalpayout,v_totaldana,5,0,keteranganweekend,NOW());
						SET counter = counter + 1;
					END LOOP;
        SET rowCountDIU = rowCountDIU + 1;
      END LOOP;
  END;
CLOSE prosesLIU;

Select count(*) into totalrow_LIU from list_imbal_user where list_imbal_user.proyek_id = idproyek;

IF totalrow_DIU*v_tenor_waktu = totalrow_LIU THEN
	COMMIT;
	
ELSE
	ROLLBACK;
END IF;
	
	

OPEN prosesLIU_SD;
  BEGIN
  		DECLARE v_totaldana int(10);
      DECLARE v_totalimbal int(10);
      DECLARE imbalpayout int(10);
      DECLARE v_sisaimbal int(10);
      DECLARE v_pendanaan_id int(10);
      DECLARE v_investor_id int(10);
      DECLARE tgljadi varchar(111);
      DECLARE tglimbal varchar(12);
      DECLARE keteranganweekend varchar(111);
      DECLARE v_keterangan varchar(111);
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE DIU_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      

      prosesLIU_SDLoop: LOOP
        FETCH prosesLIU_SD INTO DIU_id;
            IF exit_flag THEN LEAVE prosesLIU_SDLoop; 
            END IF;
            SELECT pendanaan_id,investor_id,total_imbal,total_dana,sisa_imbal into v_pendanaan_id,v_investor_id,v_totalimbal,v_totaldana,v_sisaimbal from detil_imbal_user where detil_imbal_user.id = DIU_id;
            	
					SET counter = 1;
					
					
					SET tgl = v_tgl_selesai;
					looping_tanggal: LOOP
					IF counter > 2 THEN
						LEAVE looping_tanggal;
						END IF;
						SELECT cek_tanggallibur(tgl) into tgljadi;
						SET keteranganweekend = SUBSTRING_INDEX(tgljadi,"|",1);
						SET tglimbal = SUBSTRING_INDEX(tgljadi,"|",-1);
						IF counter = 1 THEN
							SET imbalpayout = v_sisaimbal;
							SET v_keterangan = "sisa_imbal";
						ELSEIF counter = 2 THEN
							SET imbalpayout = v_totaldana;
							SET v_keterangan = "dana_pokok";
						END IF;
						INSERT INTO list_imbal_user (proyek_id,pendanaan_id,investor_id,tanggal_payout, imbal_payout, total_dana, status_payout, status_update, ket_weekend,keterangan,created_at) VALUES (idproyek,v_pendanaan_id,v_investor_id,tglimbal,imbalpayout,v_totaldana,5,0,keteranganweekend,v_keterangan,NOW());
						SET counter = counter + 1;
					END LOOP;
        SET rowCountDIU = rowCountDIU + 1;
      END LOOP;
  END;
CLOSE prosesLIU_SD;



END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_biayaRegdanTtdPrivy
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_biayaRegdanTtdPrivy`(
	IN `tglpayout` DATE



)
BEGIN

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE rowCountDescription_ INT DEFAULT 0;
DECLARE cutBiayaReg CURSOR FOR
   SELECT id_user_cek_reg FROM cek_user_reg_sign WHERE STATUS = 'verified' AND link_aktifasi = "";
DECLARE cutBiayaTtd CURSOR FOR
   SELECT id_log_privy FROM log_privy_investor WHERE STATUS = 0;

	OPEN cutBiayaReg;
	BEGIN
	DECLARE exit_flag INT DEFAULT 0;
	DECLARE cekReg_id INT(10);
	DECLARE LIU_id INT(10);
	DECLARE inv_id INT(10);
	DECLARE ket INT(10);
	DECLARE pendanaanid INT(10);
	DECLARE v_payoutpertama INT(10);
	DECLARE v_proposional INT(10);
	DECLARE biayaReg DECIMAL(15,2);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

			cutBiayaRegLoop: LOOP
				FETCH cutBiayaReg INTO cekReg_id;
				IF exit_flag THEN LEAVE cutBiayaRegLoop;
				END IF;
					SET LIU_id = 0;
					SET inv_id = 0;
					SET biayaReg  = 30000.00;
					
					IF EXISTS(SELECT a.id FROM ih_list_imbal_user a, pendanaan_aktif b WHERE a.pendanaan_id = b.id AND a.tanggal_payout = tglpayout AND b.investor_id = (SELECT investor_id FROM cek_user_reg_sign WHERE id_user_cek_reg = cekReg_id) AND a.imbal_payout-a.nominal_pajak >= biayaReg AND b.status = 1 AND a.status_payout = 1) THEN 
					
						SELECT a.id, b.investor_id,IFNULL(CAST(a.keterangan AS INT)+CAST(biayaReg AS INT),CAST(biayaReg AS INT)),a.pendanaan_id  into LIU_id, inv_id, ket,pendanaanid
						FROM ih_list_imbal_user a, pendanaan_aktif b
						WHERE a.pendanaan_id = b.id 
						AND a.tanggal_payout = tglpayout
						AND b.investor_id = (SELECT investor_id FROM cek_user_reg_sign WHERE id_user_cek_reg = cekReg_id) 
						AND a.imbal_payout-a.nominal_pajak >= biayaReg
						AND b.status = 1
						AND a.status_payout = 1
						LIMIT 1;
						
						-- SELECT MIN(id) into v_payoutpertama FROM ih_list_imbal_user WHERE pendanaan_id = pendanaanid;
						-- IF v_payoutpertama = LIU_id THEN
							-- SELECT proposional into v_proposional FROM ih_detil_imbal_user WHERE pendanaan_id = pendanaanid;
							-- UPDATE ih_list_imbal_user SET imbal_payout = imbal_payout+v_proposional-nominal_pajak-biayaReg , keterangan = ket WHERE id = LIU_id;
						-- ELSE
							UPDATE ih_list_imbal_user SET imbal_payout = imbal_payout-biayaReg , keterangan = ket WHERE id = LIU_id;
						-- END IF;
						
						#INSERT INTO mutasi_investor (investor_id,nominal,perihal,tipe,log_payout_id,created_at) 
						#VALUES(inv_id,biayaReg,"Biaya Registrasi Privy","DEBIT",0,NOW());
						UPDATE cek_user_reg_sign SET link_aktifasi = 1000 WHERE id_user_cek_reg = cekReg_id;
						#select concat(LIU_id,'-',cekReg_id,'-',ket);
					END IF;
				SET rowCountDescription = rowCountDescription + 1;
			END LOOP;
		END;
	CLOSE cutBiayaReg;
	
	OPEN cutBiayaTtd;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE privyLog_id INT(10);
			DECLARE LIU_id INT(10);
			DECLARE inv_id INT(10);
			DECLARE ket INT(10);
			DECLARE pendanaanid INT(10);
			DECLARE v_payoutpertama INT(10);
			DECLARE v_proposional INT(10);
			DECLARE biayaTtd DECIMAL(15,2);
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
	
			cutBiayaTtdLoop: LOOP
				FETCH cutBiayaTtd INTO privyLog_id;
				IF exit_flag THEN LEAVE cutBiayaTtdLoop;
				END IF;
					SET LIU_id = 0;
					SET inv_id = 0;
					SET biayaTtd = 0;
					
					IF EXISTS(SELECT a.id FROM ih_list_imbal_user a, pendanaan_aktif b WHERE a.pendanaan_id = b.id AND a.tanggal_payout = tglpayout AND b.investor_id = (SELECT investor_id FROM log_privy_investor WHERE id_log_privy = privyLog_id) AND a.imbal_payout-a.nominal_pajak >= 10000.00 AND b.status = 1 AND a.status_payout = 1) THEN 
					
						SELECT investor_id, nominal INTO inv_id,biayaTtd FROM log_privy_investor WHERE id_log_privy = privyLog_id; 
					 	SELECT a.id,IFNULL(CAST(a.keterangan AS INT)+CAST(biayaTtd AS INT),CAST(biayaTtd AS INT)),a.pendanaan_id into LIU_id, ket,pendanaanid
						FROM ih_list_imbal_user a, pendanaan_aktif b
						WHERE a.pendanaan_id = b.id 
						AND a.tanggal_payout = tglpayout
						AND b.investor_id = inv_id
						AND a.imbal_payout-a.nominal_pajak >= 10000.00
						AND b.status = 1 
						AND a.status_payout = 1
						LIMIT 1;
						
						-- SELECT MIN(id) into v_payoutpertama FROM ih_list_imbal_user WHERE pendanaan_id = pendanaanid;
						-- IF v_payoutpertama = LIU_id THEN
							-- SELECT proposional into v_proposional FROM ih_detil_imbal_user WHERE pendanaan_id = pendanaanid;
							-- UPDATE ih_list_imbal_user SET imbal_payout = imbal_payout+v_proposional-nominal_pajak-biayaTtd , keterangan = ket WHERE id = LIU_id;
						-- ELSE
							UPDATE ih_list_imbal_user SET imbal_payout = imbal_payout-biayaTtd , keterangan = ket WHERE id = LIU_id;
						-- END IF;
						
						#INSERT INTO mutasi_investor (investor_id,nominal,perihal,tipe,log_payout_id,created_at) 
						#VALUES(inv_id,biayaTtd,"Biaya TandaTangan Privy","DEBIT",0,NOW());
						UPDATE log_privy_investor SET status = 1000 WHERE id_log_privy = privyLog_id;
					
						#select concat(LIU_id,'-',inv_id,'-',privyLog_id);
					END IF;
						
				SET rowCountDescription_ = rowCountDescription_ + 1;
			END LOOP;
		END;
	CLOSE cutBiayaTtd;
	
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_brw_admin_konfirmasi_cicilan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_brw_admin_konfirmasi_cicilan`(
	IN `in_pencairan_id` int(10),
	IN `in_status_confirm` varchar(15),
	IN `in_brw_id_confirm` int(10),
	IN `in_pendanaan_id_confirm` int(10),
	IN `in_keterangan_confirm` varchar(225),
	IN `in_konfirm_by` varchar(100),
	IN `file_` varchar(50),
	IN `line_` int(10)
)
BEGIN
	
	DECLARE code CHAR(5) DEFAULT '00000';
    DECLARE msg TEXT;
    DECLARE nrows INT;
    DECLARE result TEXT;
    DECLARE res INT(1);

   
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
        SET res = 0;
        Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
        SELECT res, result;
     END;

	IF in_status_confirm = 'tolak' THEN
	
		#UPDATE brw_pencairan
		UPDATE brw_pencairan set status = 4, keterangan = in_keterangan_confirm WHERE pencairan_id = in_pencairan_id;
	
		#INSERT brw_log_pencairan
		INSERT into brw_log_pencairan (pencairan_id , brw_id , pendanaan_id , status , keterangan, created_at, updated_at) values 
		(in_pencairan_id, in_brw_id_confirm, in_pendanaan_id_confirm, 4, 'Konfirmasi Transfer Cicilan Diterima Admin', current_timestamp(), current_timestamp() );
	ELSE
		#UPDATE brw_pencairan
		UPDATE brw_pencairan set status = 3, keterangan = in_keterangan_confirm WHERE pencairan_id = in_pencairan_id;
	
		#INSERT brw_log_pencairan
		INSERT into brw_log_pencairan (pencairan_id , brw_id , pendanaan_id , status , keterangan, created_at, updated_at) values 
		(in_pencairan_id, in_brw_id_confirm, in_pendanaan_id_confirm, 3, 'Konfirmasi Transfer Cicilan Ditolak Admin', current_timestamp(), current_timestamp() );

	END IF;

	SET res = 1;
	SET result = "SUKSES";

    SELECT res, result;
	
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_brw_analyst_verif
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_brw_analyst_verif`(
	IN in_brw_id int(12),
	IN in_pengajuan_id int(10),
	IN in_score_personal int(12),
	IN in_score_pendanaan int(12),
	IN in_status_confirm varchar(10),
	IN in_keterangan varchar(125),
	IN file_ varchar(50),
	IN line_ int(10)
)
BEGIN
	
	DECLARE v_status varchar(10);
	DECLARE v_pendanaan_id int(10);
	DECLARE v_scorring_personal_exist int(1);
	DECLARE v_scorring_personal_id int(10);
	DECLARE v_scorring_pendanaan_exist int(1);
	DECLARE v_scorring_pendanaan_id int(10);
	DECLARE v_grade varchar(10);
	DECLARE code CHAR(5) DEFAULT '00000';
    DECLARE msg TEXT;
    DECLARE nrows INT;
    DECLARE result TEXT;
    DECLARE res INT(1);  
     
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
        SET res = 0;
        Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
        SELECT res, result;
     END;

 
    SET res = 1;
	SET result = "SUKSES";
	
	
	IF in_status_confirm = 'tolak' THEN
	
		SELECT status into v_status FROM brw_user where brw_id = in_brw_id;
		
		IF v_status = 'active' THEN
		
			UPDATE brw_pengajuan SET keterangan_approval = in_keterangan, status = 5 WHERE pengajuan_id = in_pengajuan_id;
			INSERT into brw_log_pengajuan (pengajuan_id, brw_id, status, keterangan) values (in_pengajuan_id, in_brw_id, 5, 'Pendanaan Ditolak oleh Super Admin');
				
		ELSEIF v_status = 'pending' THEN
		
			INSERT into brw_log_pengajuan (pengajuan_id, brw_id, status, keterangan) values (in_pengajuan_id, in_brw_id, 5, 'Pendanaan Ditolak oleh Super Admin');
		
		END IF;
	
	ELSE

		#INSERT brw_pendanaan
		INSERT INTO brw_pendanaan
		(pengajuan_id, brw_id, pendanaan_nama, pendanaan_tipe, lokasi_proyek , geocode ,pendanaan_akad, pendanaan_dana_dibutuhkan, estimasi_mulai, estimasi_imbal_hasil, mode_pembayaran, metode_pembayaran, durasi_proyek, detail_pendanaan, dana_dicairkan, status, status_dana, id_proyek, va_number, gambar_utama, keterangan, created_at, updated_at)
		(select pengajuan_id , brw_id , pendanaan_nama , pendanaan_tipe , lokasi_proyek , geocode , pendanaan_akad , pendanaan_dana_dibutuhkan , estimasi_mulai , estimasi_imbal_hasil , mode_pembayaran , metode_pembayaran , durasi_proyek , detail_pendanaan , dana_dicairkan , 0, status_dana , id_proyek , va_number , gambar_utama, keterangan, current_timestamp(), current_timestamp() FROM brw_pengajuan where pengajuan_id = in_pengajuan_id);    


		#INSERT brw_log_pengajuan
		INSERT INTO brw_log_pengajuan 
		(pengajuan_id, brw_id, status, keterangan, created_at, updated_at)
		(select pengajuan_id , brw_id ,1, 'Pengajuan Pendanaan Diterima', current_timestamp(), current_timestamp() FROM brw_pengajuan where pengajuan_id = in_pengajuan_id);
	
		SELECT pendanaan_id into v_pendanaan_id FROM brw_pendanaan ORDER BY pendanaan_id desc limit 1;
	
		#INSERT brw_log_pendanaan
		INSERT into brw_log_pendanaan (pendanaan_id, brw_id, status, keterangan) values (v_pendanaan_id, in_brw_id, 0, 'Pendanaan Telah Diapprove');
	
		#UPDATE brw_pengajuan
		UPDATE brw_pengajuan SET status_tampil_pengajuan = 'tidak', status = 1 WHERE pengajuan_id = in_pengajuan_id;
	
		#UPDATE brw_user
		UPDATE brw_user SET status = 'active' WHERE brw_id = in_brw_id;
	
		#INSERT brw_scorring_personal
		SELECT EXISTS (SELECT * FROM brw_scorring_personal WHERE brw_id =  in_brw_id) into v_scorring_personal_exist;
        
        IF v_scorring_personal_exist = 0 THEN
        	INSERT into brw_scorring_personal (brw_id, nilai) values (in_brw_id, in_score_personal);
        ELSE
        	UPDATE brw_scorring_personal SET nilai = in_score_personal, updated_at = CURRENT_TIMESTAMP() WHERE brw_id = in_brw_id;
        END IF;
       
        #INSERT brw_scorring_pendanaan
        SELECT EXISTS (SELECT * FROM brw_scorring_pendanaan WHERE pendanaan_id =  v_pendanaan_id) into v_scorring_pendanaan_exist;
        
       	SELECT grade_nilai into v_grade FROM brw_master_grade bmg where in_score_pendanaan BETWEEN min and max;
       
        IF v_scorring_pendanaan_exist = 0 THEN
        	INSERT into brw_scorring_pendanaan (pendanaan_id, scorring_judul, scorring_nilai, user_create) values (v_pendanaan_id, v_grade, in_score_pendanaan, 'admin brw');
        ELSE
        	UPDATE brw_scorring_pendanaan SET scorring_nilai = in_score_pendanaan, scorring_judul = v_grade, updated_at = CURRENT_TIMESTAMP() WHERE pendanaan_id = v_pendanaan_id;
        END IF;
       
       #INSERT brw_log_scorring
       SELECT scorring_personal_id into v_scorring_personal_id FROM brw_scorring_personal ORDER BY updated_at DESC limit 1;
       SELECT scorring_pendanaan_id into v_scorring_pendanaan_id FROM brw_scorring_pendanaan ORDER BY updated_at DESC limit 1;

       INSERT into brw_log_scorring (scorring_personal_id, scorring_pendanaan_id) values (v_scorring_personal_id, v_scorring_pendanaan_id);
       
       #INSERT brw_deskripsi_proyek
       INSERT into deskripsi_proyeks (deskripsi) (select detail_pendanaan from brw_pendanaan where pendanaan_id = v_pendanaan_id);
      
       #INSERT brw_scorring_total
       INSERT into brw_scorring_total (pendanaan_id, brw_id, scorring_total, scorring_grade) values (v_pendanaan_id, in_brw_id, in_score_personal+in_score_pendanaan, 'A');
      
	
   END if; 	
  
  	SELECT res, result;
  
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_brw_konfirmasi_cicilan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_brw_konfirmasi_cicilan`(
	IN `in_status_confirm` varchar(15),
	IN `in_invoice_id_confirm` int(10),
	IN `in_brw_id_confirm` int(10),
	IN `in_pendanaan_id_confirm` int(10),
	IN `in_bukti_id_confirm` int(10),
	IN `in_keterangan_confirm` varchar(500),
	IN `in_nominal_confirm_real` int(12),
	IN `in_konfirm_by` varchar(100),
	IN `file_` varchar(50),
	IN `line_` int(10)
)
BEGIN
	DECLARE v_pic_pembayaran varchar(50);
    DECLARE v_no_referal varchar(25);

	
	DECLARE code CHAR(5) DEFAULT '00000';
    DECLARE msg TEXT;
    DECLARE nrows INT;
    DECLARE result TEXT;
    DECLARE res INT(1);

   
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
        SET res = 0;
        Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
        SELECT res, result;
     END;


	IF in_status_confirm = 'tolak' THEN
	
		#UPDATE brw_invoice
		-- UPDATE brw_invoice set status = 6, tgl_konfirmasi = CURRENT_TIMESTAMP(), konfirmasi_oleh = in_konfirm_by, keterangan = in_keterangan_confirm WHERE invoice_id = in_invoice_id_confirm;
	
		#INSERT brw_log_invoice
		INSERT into brw_log_invoice (invoice_id, brw_id, pendanaan_id, STATUS, keterangan, created_at, updated_at) values 
		(in_invoice_id_confirm, in_brw_id_confirm, in_pendanaan_id_confirm, 6, in_keterangan_confirm, current_timestamp(), current_timestamp() );
	
		UPDATE brw_bukti_pembayaran 
		 	SET status = 6, -- waiting admin approval
		 		updated_at = NOW(),
		 		keterangan  = in_keterangan_confirm
		WHERE invoice_id = in_invoice_id_confirm and bukti_id = in_bukti_id_confirm and status = 5;
	ELSE
	
		SELECT  IFNULL(ref_no, '-') into v_no_referal FROM brw_bukti_pembayaran where bukti_id = in_bukti_id_confirm;
		SELECT  IFNULL(pic_pembayaran, '-') into v_pic_pembayaran FROM brw_bukti_pembayaran where invoice_id = in_invoice_id_confirm AND FIND_IN_SET(status, '5,3') ORDER BY bukti_id DESC LIMIT 1;

	
		call pay_for_borrower(in_brw_id_confirm,in_pendanaan_id_confirm,in_bukti_id_confirm,in_nominal_confirm_real,v_pic_pembayaran,v_no_referal,in_konfirm_by,'proc_brw_konfirmasi_cicilan',70,@res);
		IF @res >= 0 THEN
			SET res = 1;
			
			#UPDATE brw_rekening
			UPDATE brw_rekening SET total_terpakai = (total_terpakai-in_nominal_confirm_real), total_sisa =(total_sisa+in_nominal_confirm_real) WHERE brw_id = in_brw_id_confirm;
	
			#INSERT brw_log_rekening
			INSERT into brw_log_rekening 
			(brw_id, pendanaan_id, debet, credit, total_terpakai, total_sisa, keterangan, created_at, updated_at)
			(SELECT in_brw_id_confirm, in_pendanaan_id_confirm, in_nominal_confirm_real, 0, total_terpakai , total_sisa , 'Pembayaran Cicilan', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() FROM brw_rekening where brw_id = in_brw_id_confirm);

		ELSE 
			SET res = 0;
		END IF;
	
		
	END IF;


	SET res = 1;
	SET result = "SUKSES";

    SELECT res, result, @res;
	
	
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_brw_pencairan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_brw_pencairan`(
	IN `in_pencairan_id` INT(11),
	IN `in_brw_id` INT(11),
	IN `in_pendanaan_id` INT(11),
	IN `in_nominal_pencairan` INT(15),
	IN `in_author` varchar(30),
	IN `in_upload_path` varchar(100),
	IN `file_` varchar(50),
	IN `line_` int(12)
)
BEGIN
   DECLARE res INT(1);
   DECLARE v_tenor_waktu INT(5);
   DECLARE v_profit_margin INT(5);
   DECLARE v_imbal_hasil_dibayar INT(12);
   DECLARE v_total_tagihan INT(12);
   DECLARE v_akumulasi_nominal_tagihan INT(12);
   DECLARE v_total_tagihan_perbulan INT(12);
   DECLARE v_nominal_tagihan INT(12);
   DECLARE v_total_tagihan_bulan_akhir INT(12);
   DECLARE v_invoice_id INT(10);
   DECLARE counter  INT;
   DECLARE code CHAR(5) DEFAULT '00000';
   DECLARE msg TEXT;
   DECLARE nrows INT;
   DECLARE result TEXT;
   DECLARE v_no_invoice VARCHAR(50);
   
   DECLARE EXIT HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
        SET res = 0;
        Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
        SELECT res, result;
     END;

  
  #UPDATE brw_pencairan
  UPDATE brw_pencairan SET dicairkan_oleh = in_author, status = 2, tgl_pencairan = CURRENT_TIMESTAMP(), bukti_transfer = in_upload_path, updated_at = CURRENT_TIMESTAMP() WHERE pencairan_id = in_pencairan_id;
 
  #INSERT brw_log_pencairan
  INSERT into brw_log_pencairan (pencairan_id, brw_id, pendanaan_id, STATUS, keterangan, created_at, updated_at) values (in_pencairan_id, in_brw_id, in_pendanaan_id, 2, 'Dana Ditransfer', current_timestamp(), current_timestamp());
 
  SELECT durasi_proyek, estimasi_imbal_hasil into v_tenor_waktu, v_profit_margin FROM brw_pendanaan where pendanaan_id  = in_pendanaan_id;
 
  SET v_imbal_hasil_dibayar = FLOOR(in_nominal_pencairan*((v_profit_margin/100)*(v_tenor_waktu/12)));
  SET v_total_tagihan = in_nominal_pencairan+FLOOR(v_imbal_hasil_dibayar);
 
  IF v_tenor_waktu is NULL THEN 
 	SET v_tenor_waktu = 0;
  END IF;
 
  SET counter = 1; 
  SET v_akumulasi_nominal_tagihan = 0;
 
  WHILE counter <= v_tenor_waktu DO 
  
  	  IF counter = v_tenor_waktu THEN
  	  	  SET v_total_tagihan_bulan_akhir = v_total_tagihan - v_akumulasi_nominal_tagihan;
	  	  INSERT into brw_invoice (pencairan_id, brw_id, pendanaan_id, bulan_ke, nominal_tagihan_perbulan, tgl_jatuh_tempo, status)
	      values (in_pencairan_id, in_brw_id, in_pendanaan_id, counter, v_total_tagihan_bulan_akhir, TIMESTAMPADD(MONTH,counter,CURRENT_TIMESTAMP()), 4 );
	     
	      SELECT invoice_id into v_invoice_id FROM brw_invoice ORDER BY invoice_id DESC limit 1;
	     
			INSERT INTO brw_log_invoice
			(invoice_id, brw_id, pendanaan_id, STATUS, keterangan, created_at, updated_at)
			VALUES(v_invoice_id, in_brw_id, in_pendanaan_id, 4, 'Pencairan Invoice', current_timestamp(), current_timestamp());
		
			SET v_no_invoice = put_into_brw_m_no_invoice(v_invoice_id, in_brw_id); 
			UPDATE brw_invoice SET no_invoice = v_no_invoice WHERE invoice_id = v_invoice_id;
  	  	
  	  ELSE
		  SET v_nominal_tagihan = ((2/100)*in_nominal_pencairan);
		  SET v_akumulasi_nominal_tagihan = v_akumulasi_nominal_tagihan + v_nominal_tagihan;
		 
		  INSERT into brw_invoice (pencairan_id, brw_id, pendanaan_id, bulan_ke, nominal_tagihan_perbulan, tgl_jatuh_tempo, status)
	      values (in_pencairan_id, in_brw_id, in_pendanaan_id, counter, FLOOR(v_nominal_tagihan), TIMESTAMPADD(MONTH,counter,CURRENT_TIMESTAMP()), 4 );
	     
	      SELECT invoice_id into v_invoice_id FROM brw_invoice ORDER BY invoice_id DESC limit 1;
	     
	      INSERT INTO brw_log_invoice
			(invoice_id, brw_id, pendanaan_id, STATUS, keterangan, created_at, updated_at)
			VALUES(v_invoice_id, in_brw_id, in_pendanaan_id, 4, 'Pencairan Invoice', current_timestamp(), current_timestamp());
		
			SET v_no_invoice = put_into_brw_m_no_invoice(v_invoice_id, in_brw_id); 

			UPDATE brw_invoice SET no_invoice = v_no_invoice WHERE invoice_id = v_invoice_id;

  	  END IF;
  	  
  	  SET counter = counter + 1;
  
  END WHILE;
 
 
  #UPDATE brw_rekening
  UPDATE brw_rekening SET 
 	total_terpakai = (IFNULL(total_terpakai,0)+IFNULL(in_nominal_pencairan,0)), 
 	total_sisa =(IFNULL(total_sisa,0)-IFNULL(in_nominal_pencairan,0)) 
  WHERE brw_id = in_brw_id;
 
 #INSERT brw_log_rekening
 INSERT into brw_log_rekening 
(brw_id, pendanaan_id, debet, credit, total_terpakai, total_sisa, keterangan, created_at, updated_at)
(SELECT in_brw_id, in_pendanaan_id, 0, in_nominal_pencairan, total_terpakai , total_sisa , 'Pencairan', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP() FROM brw_rekening where brw_id = in_brw_id);

  	SET res = 1;
	SET result = "SUKSES";
   	
	SELECT res, result ;
  
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_click_button_borrower
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_click_button_borrower`(
	IN `_form_id` int,
	IN `_user_login` varchar(35),
	IN `_pengajuan_id` int,
	IN `_method` varchar(30),
	IN `_file` VARCHAR(100),
	IN `_line` int


)
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('proc_click_button_borrower failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
       select sout;
     end;
   
    set autocommit = 0;
    
  
	#--------------------------------------------------------------
	
	if (2 = _form_id) then
		if (upper(_method) = 'KIRIM') then
			
			#set status finish for legal detail
			update brw_hd_legalitas
			set nama_legal = _user_login,
				flag_naup = 2,
				flag_biaya2 = 2
				#updated_at_naup = now(),
				#updated_at_biaya2 = now()
			where pengajuan_id = _pengajuan_id;
			
			#set status finish for legal
			update brw_analisa_pendanaan
			set status_legal = 9,
				updated_at = now()
			where pengajuan_id = _pengajuan_id;
			
			/*
			select count(1), status_risk 
			into @flag_ , @status_risk_
			from brw_analisa_pendanaan
			where pengajuan_id = _pengajuan_id;
			
			#set status new for compliance
			if (0 != @flag_) then
				if ( 12 = @status_risk_) then
					update brw_analisa_pendanaan
					set status_compliance = 13,
						updated_at = now()
					where pengajuan_id = _pengajuan_id;
				end if;
			end if;
			*/
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (2 = _form_id)

	if (4 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for naup legal detail
			call check_first_create('brw_hd_legalitas','pengajuan_id',_pengajuan_id,'created_at_naup','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_legalitas
				set nama_legal = _user_login,
					flag_naup = 1,
					updated_at_naup = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_legalitas
				set nama_legal = _user_login,
					flag_naup = 1,
					created_at_naup = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			
			#set status proses for legal
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_legal = 8,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_legal = 8,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (4 = _form_id)

	if (5 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for biaya legal detail
			call check_first_create('brw_hd_legalitas','pengajuan_id',_pengajuan_id,'created_at_biaya2','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_legalitas
				set nama_legal = _user_login,
					flag_biaya2 = 1,
					updated_at_biaya2 = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_legalitas
				set nama_legal = _user_login,
					flag_biaya2 = 1,
					created_at_biaya2 = now()
				where pengajuan_id = _pengajuan_id;

			end if;
			
			#set status proses for legal
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_legal = 8,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_legal = 8,
					created_at = now()
				where pengajuan_id = _pengajuan_id;

			end if;
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (5 = _form_id)

	if (7 = _form_id) then
		#set status proses for risk
		if (upper(_method) = 'SIMPAN') then
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_risk = 11,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_risk = 11,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		#set status finish for risk	
		elseif (upper(_method) = 'KIRIM') then
			update brw_analisa_pendanaan
			set status_risk = 12,
				updated_at = now()
			where pengajuan_id = _pengajuan_id;
			
			/*
			select count(1), status_legal
			into @flag_ , @status_legal_
			from brw_analisa_pendanaan
			where pengajuan_id = _pengajuan_id;
			
			#set status new for compliance
			if (0 != @flag_) then
				if (9 = @status_legal_) then
					update brw_analisa_pendanaan
					set status_compliance = 13,
						updated_at = now()
					where pengajuan_id = _pengajuan_id;
				end if;
			end if;
			*/
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
		update brw_hd_deviasi
			set nama_risk = _user_login,
				updated_at = now()
			where pengajuan_id = _pengajuan_id;
	end if; #if (7 = _form_id)
	
	if (10 = _form_id) then
		if (upper(_method) = 'KIRIM') then
			#set status finish for analis detail
			update brw_hd_analisa
			set nama_analis_dsi = _user_login,
				flag_rac_dsi = 2 ,
				flag_lembar_dsi = 2 ,
				flag_resume_dsi = 2 
				#updated_at_rac_dsi = now(),
				#updated_at_lembar_dsi = now(),
				#updated_at_resume_dsi = now()
			where pengajuan_id = _pengajuan_id;
		
			#set status finish for analis
			update brw_analisa_pendanaan
			set status_analis = 6,
				updated_at = now()
			where pengajuan_id = _pengajuan_id;
			
			#set status new for risk		
			update brw_analisa_pendanaan
			set status_risk = 10,
				updated_at = now()
			where pengajuan_id = _pengajuan_id;
			
			#set status new for compliance		
			update brw_analisa_pendanaan
			set status_compliance = 13,
				updated_at = now()
			where pengajuan_id = _pengajuan_id;

		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	
	end if; #if (10 = _form_id)
	
	if (11 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for biaya analis rac
			call check_first_create('brw_hd_analisa','pengajuan_id',_pengajuan_id,'created_at_rac_dsi','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_rac_dsi = 1 ,
					updated_at_rac_dsi = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_rac_dsi = 1 ,
					created_at_rac_dsi = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			
			#set status proses for analis
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_analis = 5,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_analis = 5,
					created_at = now()
				where pengajuan_id = _pengajuan_id;		
			end if;
				
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (11 = _form_id)
	
	if (12 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for biaya analis lembar perhitungan
			call check_first_create('brw_hd_analisa','pengajuan_id',_pengajuan_id,'created_at_lembar_dsi','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_lembar_dsi = 1 ,
					updated_at_lembar_dsi = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_lembar_dsi = 1 ,
					created_at_lembar_dsi = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			
			#set status proses for analis
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_analis = 5,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_analis = 5,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (12 = _form_id)
	
	if (13 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for biaya analis resume akad murabahah
			call check_first_create('brw_hd_analisa','pengajuan_id',_pengajuan_id,'created_at_resume_dsi','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_resume_dsi = 1 ,
					updated_at_resume_dsi = now()
				where pengajuan_id = _pengajuan_id;
			else 
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_resume_dsi = 1 ,
					created_at_resume_dsi = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			#set status proses for analis
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_analis = 5,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_analis = 5,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (13 = _form_id)	
	
	if (14 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for biaya analis resume akad mmq
			call check_first_create('brw_hd_analisa','pengajuan_id',_pengajuan_id,'created_at_resume_dsi','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_resume_dsi = 1 ,
					updated_at_resume_dsi = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_analisa
				set nama_analis_dsi = _user_login,
					flag_resume_dsi = 1 ,
					created_at_resume_dsi = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			#set status proses for analis
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_analis = 5,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_analis = 5,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
				
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (14 = _form_id)	
		
	if (17 = _form_id) then
		if (upper(_method) = 'KIRIM') then
			#set status finish for verificator detail
			update brw_hd_verifikator
			set nama_verifikator = _user_login,
				flag_spk = 2,
				#updated_at_spk = now(),
				flag_dokumen_ceklis = 2,
				#updated_at_dokumen_ceklis = now(),
				flag_rac = 2,
				#updated_at_rac = now(),
				flag_interview_hrd = 2,
				#updated_at_interview_hrd = now(),
				flag_kunjungan_domisili = 2,
				#updated_at_kunjungan_domisili = now(),
				flag_kunjungan_objek_pendanaan = 2,
				#updated_at_kunjungan_objek_pendanaan = now(),
				flag_informasi_pendanaan_berjalan = 2,
				#updated_at_informasi_pendanaan_berjalan = now(),
				flag_laporan_appraisal = 2
				#updated_at_laporan_appraisal = now()
			where pengajuan_id = _pengajuan_id;
					
			#set status finish for verificator and new for analis & legal		
			update brw_analisa_pendanaan
			set  status_verifikator = 3,	
				 status_analis = 4,
				 status_legal = 7,
			    updated_at = now()
			where pengajuan_id = _pengajuan_id;
			
			insert into brw_hd_analisa (pengajuan_id) values (_pengajuan_id);
			insert into brw_hd_deviasi (pengajuan_id) values (_pengajuan_id);
			insert into brw_hd_legalitas (pengajuan_id) values (_pengajuan_id);
					
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	
	end if; #if (17 = _form_id)
	
	if (19 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for verificator detail spk
			call check_first_create('brw_hd_verifikator','pengajuan_id',_pengajuan_id,'created_at_dokumen_ceklis','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_dokumen_ceklis = 1,
					updated_at_dokumen_ceklis = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_dokumen_ceklis = 1,
					created_at_dokumen_ceklis = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			#set status proses for verifikator
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (19 = _form_id)	
	
	if (20 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			call check_first_create('brw_hd_verifikator','pengajuan_id',_pengajuan_id,'created_at_rac','test.php',199,@temp);
			#set status ubah for verificator detail rac
			if (1 = @temp) then
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_rac = 1,
					updated_at_rac = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_rac = 1,
					created_at_rac = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			#set status proses for verifikator
			if (1 = @temp) then
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					created_at = now()
				where pengajuan_id = _pengajuan_id;

			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (20 = _form_id)
	
	if (22 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for verificator detail interview hrd
			call check_first_create('brw_hd_verifikator','pengajuan_id',_pengajuan_id,'created_at_interview_hrd','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_interview_hrd = 1,
					updated_at_interview_hrd = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_interview_hrd = 1,
					created_at_interview_hrd = now()
				where pengajuan_id = _pengajuan_id;
			end if;
				
			#set status proses for verifikator
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (22 = _form_id)
	
	if (23 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for verificator detail kunjungan domisili
			call check_first_create('brw_hd_verifikator','pengajuan_id',_pengajuan_id,'created_at_kunjungan_domisili','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_kunjungan_domisili = 1,
					updated_at_kunjungan_domisili = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_kunjungan_domisili = 1,
					created_at_kunjungan_domisili = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			#set status proses for verifikator
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (23 = _form_id)
	
	if (24 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for verificator detail kunjungan objek pendanaan
			call check_first_create('brw_hd_verifikator','pengajuan_id',_pengajuan_id,'created_at_kunjungan_objek_pendanaan','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_kunjungan_objek_pendanaan = 1,
					updated_at_kunjungan_objek_pendanaan = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_kunjungan_objek_pendanaan = 1,
					created_at_kunjungan_objek_pendanaan = now()
				where pengajuan_id = _pengajuan_id;
			end if;
				
			#set status proses for verifikator
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (24 = _form_id)
	
	if (25 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for verificator detail informasi pendanaan berjalan
			call check_first_create('brw_hd_verifikator','pengajuan_id',_pengajuan_id,'created_at_informasi_pendanaan_berjalan','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_informasi_pendanaan_berjalan = 1,
					updated_at_informasi_pendanaan_berjalan = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_informasi_pendanaan_berjalan = 1,
					created_at_informasi_pendanaan_berjalan = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
			#set status proses for verifikator
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (25 = _form_id)
	
	if (26 = _form_id) then
		if (upper(_method) = 'SIMPAN') then
			#set status ubah for verificator detail laporan appraisal
			call check_first_create('brw_hd_verifikator','pengajuan_id',_pengajuan_id,'created_at_laporan_appraisal','test.php',199,@temp);
			if(1 = @temp) then
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_laporan_appraisal = 1,
					updated_at_laporan_appraisal = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_hd_verifikator
				set nama_verifikator = _user_login,
					flag_laporan_appraisal = 1,
					created_at_laporan_appraisal = now()
				where pengajuan_id = _pengajuan_id;
			end if;
						
			#set status proses for verifikator
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_verifikator = 2,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (26 = _form_id)
	
	if (28 = _form_id) then
		if (upper(_method) = 'SIMPAN_RISK') then
			#set status ubah for rekomendasi risk
			call check_first_create('brw_resume_pendanaan','pengajuan_id',_pengajuan_id,'created_at_risk_management','test.php',199,@temp);
			if(1 = @temp) then
				update brw_resume_pendanaan
				set nama_risk_management = _user_login,
					updated_at_risk_management = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_resume_pendanaan
				set nama_risk_management = _user_login,
					created_at_risk_management = now()
				where pengajuan_id = _pengajuan_id;
			end if;
						
			#set status proses for compliance
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_compliance = 14,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_compliance = 14,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
		
		elseif (upper(_method) = 'SIMPAN_LEGAL') then
			#set status ubah for rekomendasi legal
			call check_first_create('brw_resume_pendanaan','pengajuan_id',_pengajuan_id,'created_at_legal','test.php',199,@temp);
			if(1 = @temp) then
				update brw_resume_pendanaan
				set nama_legal = _user_login,
					updated_at_legal = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_resume_pendanaan
				set nama_legal = _user_login,
					created_at_legal = now()
				where pengajuan_id = _pengajuan_id;
			end if;
						
			#set status proses for compliance
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_compliance = 14,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_compliance = 14,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
		
		elseif (upper(_method) = 'SIMPAN_ANALIS') then
			#set status ubah for rekomendasi analis
			call check_first_create('brw_resume_pendanaan','pengajuan_id',_pengajuan_id,'created_at_kredit_analis','test.php',199,@temp);
			if(1 = @temp) then
				update brw_resume_pendanaan
				set nama_kredit_analis = _user_login,
					updated_at_kredit_analis = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_resume_pendanaan
				set nama_kredit_analis = _user_login,
					created_at_kredit_analis = now()
				where pengajuan_id = _pengajuan_id;
			end if;
						
			#set status proses for compliance
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_compliance = 14,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_compliance = 14,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
		
		elseif (upper(_method) = 'SIMPAN_COMPLIANCE') then
			#set status ubah for rekomendasi compliance
			call check_first_create('brw_resume_pendanaan','pengajuan_id',_pengajuan_id,'created_at_compliance','test.php',199,@temp);
			if(1 = @temp) then
				update brw_resume_pendanaan
				set nama_compliance = _user_login,
					updated_at_compliance = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_resume_pendanaan
				set nama_compliance = _user_login,
					created_at_compliance = now()
				where pengajuan_id = _pengajuan_id;
			end if;
						
			#set status proses for compliance
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_compliance = 14,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_compliance = 14,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		elseif (upper(_method) = 'KIRIM') then					
			#set status selesai for compliance
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_compliance = 15,
					status_komite = 16,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_compliance = 15,
					status_komite = 16,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (28 = _form_id)
	
	
	if (30 = _form_id) then
		if (upper(_method) = 'SIMPAN_DIREKTUR') then
			#set status ubah for rekomendasi direktur
			call check_first_create('brw_komite_putusan','pengajuan_id',_pengajuan_id,'created_at_direktur','test.php',199,@temp);
			if(1 = @temp) then
				update brw_komite_putusan
				set nama_direktur = _user_login,
					updated_at_direktur = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_komite_putusan
				set nama_direktur = _user_login,
					created_at_direktur = now()
				where pengajuan_id = _pengajuan_id;
			end if;
						
			#set status proses for komite
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_komite = 17,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_komite = 17,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
		
		elseif (upper(_method) = 'SIMPAN_DIRUT') then
			#set status ubah for rekomendasi direktur utama
			call check_first_create('brw_komite_putusan','pengajuan_id',_pengajuan_id,'created_at_direktur_utama','test.php',199,@temp);
			if(1 = @temp) then
				update brw_komite_putusan
				set nama_direktur_utama = _user_login,
					updated_at_direktur_utama = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_komite_putusan
				set nama_direktur_utama = _user_login,
					created_at_direktur_utama = now()
				where pengajuan_id = _pengajuan_id;
			end if;
						
			#set status proses for komite
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_komite = 17,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_komite = 17,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		elseif (upper(_method) = 'KIRIM') then					
			#set status selesai for komite
			call check_first_create('brw_analisa_pendanaan','pengajuan_id',_pengajuan_id,'created_at','test.php',199,@temp);
			if(1 = @temp) then
				update brw_analisa_pendanaan
				set status_komite = 18,
					updated_at = now()
				where pengajuan_id = _pengajuan_id;
			else
				update brw_analisa_pendanaan
				set status_komite = 18,
					created_at = now()
				where pengajuan_id = _pengajuan_id;
			end if;
			
		else
			set sout = concat('Method di form id = ',_form_id,' salah ');
		end if;
	end if; #if (30 = _form_id)


	set sout = '1';							
	select sout ; 
	commit;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_generatestatusrekapproyek
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_generatestatusrekapproyek`()
BEGIN

DECLARE v_status INT;
DECLARE rowCountDescription INT DEFAULT 0;

DECLARE getproyekid CURSOR FOR
    SELECT id from proyek;
    
-- START TRANSACTION;
OPEN getproyekid;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE proyekid INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      
      getproyekidLoop: LOOP
        FETCH getproyekid INTO proyekid;
            IF exit_flag THEN LEAVE getproyekidLoop; 
            END IF;
            
            SELECT status INTO v_status FROM proyek WHERE id = proyekid;
            
            IF v_status = 2 || v_status = 3 || v_status = 4 THEN
			      UPDATE proyek SET status_rekap = 1 WHERE id = proyekid;
			   END IF;
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE getproyekid;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_ihlogimbaluser
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_ihlogimbaluser`(
	IN `idproyek` INT




)
BEGIN

DECLARE rowCountDIU int default 0;
DECLARE imbalpayout int(50);
DECLARE updateimbal int(50);
DECLARE v_detilimbaluserid int(10);
DECLARE ids text;
DECLARE counter INT DEFAULT 1;
DECLARE ambilData_LIU CURSOR FOR
    SELECT ih_list_imbal_user.id from ih_list_imbal_user left join ih_detil_imbal_user on ih_list_imbal_user.detilimbaluser_id = ih_detil_imbal_user.id where ih_detil_imbal_user.proyek_id = idproyek;
	 
    
   
OPEN ambilData_LIU;
	BEGIN
	DECLARE exit_flag INT DEFAULT 0;
	DECLARE v_detilimbaluserid INT(10);
	DECLARE v_listimbaluser_id INT(10);
	DECLARE v_tanggal_payout DATE;
	DECLARE v_imbal_payout INT(50);
	DECLARE v_status_payout INT(5);
	DECLARE v_keterangan_payout INT(5);
	DECLARE v_keterangan TEXT;
	DECLARE v_ket_libur TEXT;
	DECLARE v_created_at TIMESTAMP;
	DECLARE v_updated_at TIMESTAMP;
	DECLARE v_total_dana INT(50);
	DECLARE v_nominal_awal INT(50);
	DECLARE LIU_id INT(10);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

	ambilData_LIULoop: LOOP
		FETCH ambilData_LIU INTO LIU_id;
			IF exit_flag THEN 
				LEAVE ambilData_LIULoop;
			END IF;
			
			SELECT a.id,a.detilimbaluser_id,a.tanggal_payout,a.imbal_payout,a.status_payout,a.keterangan_payout,a.keterangan,a.ket_libur,a.created_at,a.updated_at,c.total_dana,c.nominal_awal INTO v_listimbaluser_id,v_detilimbaluserid,v_tanggal_payout,v_imbal_payout,v_status_payout,v_keterangan_payout,v_keterangan,v_ket_libur,v_created_at, v_updated_at,v_total_dana,v_nominal_awal FROM ih_list_imbal_user a JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id JOIN ih_pendanaan_aktif c ON b.pendanaan_id = c.id WHERE a.id = LIU_id ORDER BY b.id ASC;
			 
			INSERT INTO ih_log_imbal_user (detilimbaluser_id,total_dana,nominal,listimbaluser_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) VALUES(v_detilimbaluserid,v_total_dana,v_nominal_awal,v_listimbaluser_id,v_tanggal_payout,v_imbal_payout,v_status_payout,v_keterangan_payout,v_keterangan,v_ket_libur,v_created_at, v_updated_at);



		SET rowCountDIU = rowCountDIU + 1;
		SET counter = counter+1;
	END LOOP;
	END;
CLOSE ambilData_LIU;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_imbalhasil
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_imbalhasil`(
	IN `idproyek` INT







)
BEGIN

DECLARE tgl varchar(100);
DECLARE totalrow_PA INT;
DECLARE totalrow_DIU INT;
DECLARE totalrow_LIU INT;
DECLARE totalrow_LIU_SD int;
DECLARE v_id INT;
DECLARE v_nama varchar(100);
DECLARE v_tenor_waktu int;
DECLARE v_tgl_selesai_penggalangan date;
DECLARE v_profit_margin int;
DECLARE v_tgl_mulai date;
DECLARE v_tgl_selesai date;
DECLARE counter int;
DECLARE v_tanggal_invest date;
DECLARE tglpayoutbulan1 DATE;
DECLARE rowCountDescription INT DEFAULT 0;
DECLARE rowCountDIU INT DEFAULT 0;
DECLARE v_total_dana int;
DECLARE marginbulanan DECIMAL(20,4);
DECLARE totalimbal DECIMAL(20,4);
DECLARE sisamargin DECIMAL(20,10);
DECLARE sisaimbal DECIMAL(20,4);
DECLARE selisihtanggal int;
DECLARE pengaliproposional DECIMAL(20,10);
DECLARE sisaimbal_b1 DECIMAL(20,10);
DECLARE sisaimbal_b2 DECIMAL(20,10);
DECLARE proposional_b1 DECIMAL(20,10);
DECLARE proposional DECIMAL(20,4);
DECLARE v_investor_id int;
-- DECLARE DIU_ids TEXT DEFAULT '';
DECLARE pengalimarginlesstwelve DECIMAL(20,6);
DECLARE updatePA CURSOR FOR
    SELECT id from pendanaan_aktif where pendanaan_aktif.proyek_id = idproyek AND status = '1';
DECLARE prosesLIU CURSOR FOR
    SELECT id from ih_detil_imbal_user where ih_detil_imbal_user.proyek_id = idproyek;
DECLARE prosesLIU_SD CURSOR FOR
    SELECT id from ih_detil_imbal_user where ih_detil_imbal_user.proyek_id = idproyek;


#data proyek
SELECT id,nama,tenor_waktu,tgl_selesai_penggalangan,profit_margin,tgl_mulai,tgl_selesai
into v_id,v_nama,v_tenor_waktu,v_tgl_selesai_penggalangan,v_profit_margin,v_tgl_mulai,v_tgl_selesai
from proyek WHERE id = idproyek;

Select count(*) into totalrow_PA from pendanaan_aktif where pendanaan_aktif.proyek_id = idproyek AND status = '1';

#detil_imbal_user
START TRANSACTION;
OPEN updatePA;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE pen_id INT(10);
      DECLARE v_prospekditerima DECIMAL(15,2);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      
      updatePALoop: LOOP
        FETCH updatePA INTO pen_id;
            IF exit_flag THEN LEAVE updatePALoop; 
            END IF;
            #select pen_id;
            SELECT total_dana,tanggal_invest,investor_id INTO v_total_dana,v_tanggal_invest,v_investor_id FROM pendanaan_aktif WHERE id = pen_id;
            SET selisihtanggal = datediff(v_tgl_mulai,v_tanggal_invest);
            SET pengalimarginlesstwelve = (v_profit_margin/12);
            IF v_profit_margin < 12 THEN
			       SET marginbulanan = FLOOR(((pengalimarginlesstwelve*v_total_dana)/100)/100)*100;
			       SET totalimbal = marginbulanan*v_tenor_waktu;
			       SET sisaimbal = 0;
			       SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
			   	 SET proposional = FLOOR((pengaliproposional*v_total_dana)/100)*100;
			   ELSE
			   	SET marginbulanan = (1*v_total_dana)/100;
			   	SET totalimbal = marginbulanan*v_tenor_waktu;
			   	SET sisamargin = ((v_profit_margin/12)*v_tenor_waktu)-v_tenor_waktu;
			   	SET sisaimbal_b1 = ((v_total_dana*sisamargin)/100)/100;
			   	SET sisaimbal_b2 = sisaimbal_b1 - FLOOR(((v_total_dana*sisamargin)/100)/100);
			   	IF sisaimbal_b2 > 0.95 THEN
						SET sisaimbal = CEIL(sisaimbal_b1)*100;
					ELSE
						SET sisaimbal = FLOOR(sisaimbal_b1)*100;
					END IF;
			   	#FLOOR(((v_total_dana*sisamargin)/100)/100)*100;
					#CEIL(((v_total_dana*sisamargin)/100)/100)*100;
			   	SET pengaliproposional = (0.01/30)*selisihtanggal;
			   	SET proposional_b1 = ((pengaliproposional*v_total_dana)/100) - FLOOR((pengaliproposional*v_total_dana)/100);
			   	IF proposional_b1 > 0.95 THEN
						SET proposional = CEIL((pengaliproposional*v_total_dana)/100)*100;
					ELSE
						SET proposional = FLOOR((pengaliproposional*v_total_dana)/100)*100;
					END IF;
			   	#select proposional_b1;
			   END IF;
			   SET v_prospekditerima = totalimbal+proposional+sisaimbal;
			   #select pengalimarginlesstwelve,v_tgl_selesai_penggalangan,v_tanggal_invest,selisihtanggal,pengaliproposional,proposional,sisaimbal,marginbulanan,v_profit_margin,v_total_dana,v_tenor_waktu, totalimbal;
            INSERT INTO ih_detil_imbal_user (pendanaan_id,proyek_id, total_imbal, sisa_imbal, proposional,imbal_hasil_bulanan,prospek_hasil_diterima,created_at) VALUES (pen_id,idproyek,totalimbal,sisaimbal,proposional,marginbulanan,v_prospekditerima,NOW());
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE updatePA;

Select count(*) into totalrow_DIU from ih_detil_imbal_user where ih_detil_imbal_user.proyek_id = idproyek;
--
#validasi insert detil_imbal_user
IF totalrow_PA = totalrow_DIU THEN
	COMMIT;
ELSE
	ROLLBACK;
END IF;

#list_imbal_user(imbal hasil)
START TRANSACTION;
	OPEN prosesLIU;
	BEGIN
	DECLARE v_totaldana int(10);
	DECLARE v_totalimbal int(10);
	DECLARE imbalpayout decimal(10,2);
	DECLARE v_pendanaan_id int(10);
	DECLARE v_pph23 decimal(10,2);
	DECLARE v_investor_id int(10);
	DECLARE tgljadi varchar(111);
	DECLARE tglimbal varchar(12);
	DECLARE keteranganweekend varchar(111);
	DECLARE v_id int(10);
	DECLARE v_jenispajak varchar(10);
	DECLARE v_nominalpajak int(10);
	DECLARE v_tarif float;
	DECLARE v_proposional decimal(10,2);
	
	DECLARE exit_flag INT DEFAULT 0;
	DECLARE DIU_id INT(10);
	DECLARE cntr int;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

	prosesLIULoop: LOOP
		FETCH prosesLIU INTO DIU_id;
			IF exit_flag THEN 
				LEAVE prosesLIULoop;
			END IF;
				SELECT a.id,a.pendanaan_id,a.total_imbal,a.imbal_hasil_bulanan, a.proposional, 
				-- CASE WHEN c.is_valid_npwp = 1
-- THEN (SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_npwp') ELSE
-- 				(SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_no_npwp')  END 
				0.15 AS pph23 
				INTO v_id,v_pendanaan_id,v_totalimbal,imbalpayout, v_proposional, v_pph23 
				FROM ih_detil_imbal_user a
				LEFT JOIN pendanaan_aktif b ON a.pendanaan_id = b.id 
				LEFT JOIN detil_investor c ON b.investor_id = c.investor_id 
				WHERE a.id = DIU_id;
				-- SET DIU_ids = concat("|",DIU_id,concat(DIU_ids));
				-- select DIU_ids ;
				SET counter = 1;
				SET cntr = 1;
				SET v_jenispajak = 'PPH23';
				SET v_tarif = v_pph23*100;
				
				#SET tgl = "";
				looping_tanggal: LOOP
					IF counter > v_tenor_waktu THEN
						LEAVE looping_tanggal;
					END IF;
						#membuat interval tanggal payout
						SET tgl = v_tgl_mulai + INTERVAL cntr MONTH;
						SELECT cek_tanggallibur(tgl) into tgljadi;
						SET keteranganweekend = SUBSTRING_INDEX(tgljadi,"|",1);
						-- SET tglimbal = tgl;
						SET tglimbal = SUBSTRING_INDEX(tgljadi,"|",-1);
						#select v_id,tglimbal,imbalpayout,5,1,NULL,keteranganweekend;
						SET v_nominalpajak = IF(counter = 1, (imbalpayout+v_proposional)*v_pph23, imbalpayout*v_pph23);
						INSERT INTO ih_list_imbal_user (detilimbaluser_id,pendanaan_id,tanggal_payout, imbal_payout, status_payout, keterangan_payout,keterangan, ket_libur,jenis_pajak,nominal_pajak,tarif,masa_pajak,tahun_pajak,created_at) VALUES (v_id,v_pendanaan_id,tglimbal,imbalpayout,1,1,NULL,keteranganweekend,v_jenispajak,v_nominalpajak,v_tarif,DATE_FORMAT(tglimbal,'%m'),DATE_FORMAT(tglimbal,'%Y'),NOW());
						SET cntr = cntr + 1;
						SET counter = counter + 1;
				END LOOP;
		SET rowCountDIU = rowCountDIU + 1;
	END LOOP;
	END;
CLOSE prosesLIU;
 
	Select count(*) into totalrow_LIU FROM ih_list_imbal_user a left JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek;
	#select totalrow_LIU, totalrow_DIU*v_tenor_waktu , totalrow_DIU, v_tenor_waktu;

		IF totalrow_PA*v_tenor_waktu = totalrow_LIU THEN
			COMMIT;
			#updateproposional
			-- SELECT a.tanggal_payout into tglpayoutbulan1 FROM ih_list_imbal_user a LEFT JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek AND keterangan_payout = 1 GROUP BY a.tanggal_payout ORDER BY a.id limit 1;
			-- CALL proc_updateproposional(idproyek,tglpayoutbulan1);
		ELSE
			ROLLBACK;
		END IF;

-- 
#list_imbal_user(sisa imbal dan dana pokok)
START TRANSACTION;
	OPEN prosesLIU_SD;
	BEGIN
	DECLARE v_totaldana int(10);
	DECLARE v_totalimbal int(10);
	DECLARE imbalpayout int(10);
	DECLARE v_sisaimbal int(10);
	DECLARE v_pendanaan_id int(10);
	DECLARE v_pph23 decimal(10,2);
	DECLARE tgljadi varchar(111);
	DECLARE tglimbal varchar(12);
	DECLARE keteranganweekend varchar(111);
	DECLARE v_id int(10);
	DECLARE v_jenispajak varchar(10);
	DECLARE v_nominalpajak int(10);
	DECLARE v_tarif float;
	DECLARE v_masapajak int(10);
	DECLARE v_tahunpajak int(10);
	
	DECLARE v_keterangan varchar(111);
	DECLARE exit_flag INT DEFAULT 0;
	DECLARE DIU_id INT(10);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

	prosesLIU_SDLoop: LOOP
		FETCH prosesLIU_SD INTO DIU_id;
			IF exit_flag THEN LEAVE prosesLIU_SDLoop;
			END IF;
			
			#SELECT pendanaan_id,investor_id,total_imbal,total_dana,sisa_imbal into v_pendanaan_id,v_investor_id,v_totalimbal,v_totaldana,v_sisaimbal from detil_imbal_user where detil_imbal_user.id = DIU_id;
			SELECT a.id,a.pendanaan_id,a.total_imbal,a.imbal_hasil_bulanan,a.sisa_imbal,b.total_dana,
			-- CASE WHEN c.is_valid_npwp = 1 THEN (SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_npwp') ELSE
-- 			(SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_no_npwp')  END 
			0.15 AS pph23
			INTO v_id,v_pendanaan_id,v_totalimbal,imbalpayout,v_sisaimbal,v_totaldana,v_pph23 
			FROM ih_detil_imbal_user a LEFT JOIN pendanaan_aktif b ON a.pendanaan_id = b.id 
			LEFT JOIN detil_investor c ON b.investor_id = c.investor_id WHERE a.id = DIU_id;
		#membuat row untuk sisa imbal dan dana pokok
			SET counter = 1;
			
			SET tgl = v_tgl_selesai;
			looping_tanggal: LOOP
				IF counter > 2 THEN
					LEAVE looping_tanggal;
				END IF;

				SELECT cek_tanggallibur(tgl) into tgljadi;
					SET keteranganweekend = SUBSTRING_INDEX(tgljadi,"|",1);
					SET tglimbal = SUBSTRING_INDEX(tgljadi,"|",-1);
				IF counter = 1 THEN
					SET imbalpayout = v_sisaimbal;
					SET v_keterangan = 2;
					SET v_jenispajak = 'PPH23';
					SET v_nominalpajak = v_sisaimbal*v_pph23;
					SET v_tarif = v_pph23*100;
					SET v_masapajak = DATE_FORMAT(tglimbal,'%m');
					SET v_tahunpajak= DATE_FORMAT(tglimbal,'%Y');
				ELSEIF counter = 2 THEN
					SET imbalpayout = v_totaldana;
					SET v_keterangan = 3;
					SET v_jenispajak = 'Bukan OP';
					SET v_nominalpajak = 0;
					SET v_tarif = 0;
					SET v_masapajak = \N;
					SET v_tahunpajak= \N;
				END IF;
				
				#select v_totaldana;
				INSERT INTO ih_list_imbal_user (detilimbaluser_id,pendanaan_id,tanggal_payout, imbal_payout, status_payout, keterangan_payout,keterangan, ket_libur,jenis_pajak,nominal_pajak,tarif,masa_pajak,tahun_pajak,created_at) VALUES (v_id,v_pendanaan_id,tglimbal,imbalpayout,1,v_keterangan,NULL,keteranganweekend,v_jenispajak,v_nominalpajak,v_tarif,v_masapajak,v_tahunpajak,NOW());
				SET counter = counter + 1;
				END LOOP;
				SET rowCountDIU = rowCountDIU + 1;
			END LOOP;
		END;
	CLOSE prosesLIU_SD;
	
	Select count(*) into totalrow_LIU_SD FROM ih_list_imbal_user a left JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek AND a.keterangan_payout <> 1;
	
		IF totalrow_PA*2 = totalrow_LIU_SD THEN
			COMMIT;
			-- update status rekap
			START TRANSACTION;
			  Update proyek SET status_rekap = 1 WHERE id = idproyek;
			COMMIT;
			#logimbal
			-- CALL proc_ihlogimbaluser(idproyek);
			-- selesai status rekap
		ELSE
			ROLLBACK;
		END IF;
	
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_imbalhasil_byidpendana
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_imbalhasil_byidpendana`(
	IN `idproyek` INT,
	IN `idpendana` INT

)
BEGIN


DECLARE rowCountDescription INT DEFAULT 0;
DECLARE v_counter INT unsigned DEFAULT 0;
DECLARE v_proyekid INT;
DECLARE v_totaldana INT;
DECLARE v_tanggalinvest DATE;
DECLARE v_tgl_selesai_penggalangan DATE;
DECLARE v_investorid INT;
DECLARE v_profit_margin DECIMAL(20,6);
DECLARE v_tenor_waktu INT;
DECLARE v_iddetil INT;
DECLARE v_id INT;
DECLARE selisihtanggal INT;
DECLARE pengalimarginlesstwelve DECIMAL(20,6);
DECLARE marginbulanan DECIMAL(20,6);
DECLARE totalimbal DECIMAL(20,6);
DECLARE sisaimbal DECIMAL(20,6);
DECLARE pengaliproposional DECIMAL(20,6);
DECLARE u_proposional DECIMAL(20,6);
DECLARE sisaimbal_b1 DECIMAL(20,6);
DECLARE sisamargin DECIMAL(20,6);
DECLARE sisaimbal_b2 DECIMAL(20,6);
DECLARE proposional_b1 DECIMAL(20,6);
DECLARE v_prospekditerima DECIMAL(20,6);
DECLARE v_tanggal_payout DATE;
DECLARE si_tanggal_payout DATE;
DECLARE dp_tanggal_payout DATE;


            
            select total_dana, tanggal_invest,investor_id into v_totaldana, v_tanggalinvest,v_investorid from pendanaan_aktif where id = idpendana;
            
            select tgl_selesai_penggalangan, profit_margin, tenor_waktu into v_tgl_selesai_penggalangan,v_profit_margin, v_tenor_waktu from proyek where id = idproyek;
				-- select idproyek,idpendana,v_totaldana, v_tanggalinvest,v_investorid,v_tgl_selesai_penggalangan,v_profit_margin, v_tenor_waktu;

				SET selisihtanggal = datediff(v_tgl_selesai_penggalangan,v_tanggalinvest)+1;
				SET pengalimarginlesstwelve = (v_profit_margin/12);
				select selisihtanggal,pengalimarginlesstwelve,v_tenor_waktu;
				
				IF v_profit_margin < 12 THEN
					SET marginbulanan = FLOOR(((pengalimarginlesstwelve*v_totaldana)/100)/100)*100;
					SET totalimbal = marginbulanan*v_tenor_waktu;
					SET sisaimbal = 0;
					SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
					SET u_proposional = FLOOR((pengaliproposional*v_totaldana)/100)*100;
				ELSE
					SET marginbulanan = (1*v_totaldana)/100;
					SET totalimbal = marginbulanan*v_tenor_waktu;
					SET sisamargin = ((v_profit_margin/12)*v_tenor_waktu)-v_tenor_waktu;
					SET sisaimbal_b1 = ((v_totaldana*sisamargin)/100)/100;
					SET sisaimbal_b2 = sisaimbal_b1 - FLOOR(((v_totaldana*sisamargin)/100)/100);
						IF sisaimbal_b2 > 0.95 THEN
							SET sisaimbal = CEIL(sisaimbal_b1)*100;
						ELSE
							SET sisaimbal = FLOOR(sisaimbal_b1)*100;
						END IF;
				SET pengaliproposional = (0.01/30)*selisihtanggal;
				SET proposional_b1 = ((pengaliproposional*v_totaldana)/100) - FLOOR((pengaliproposional*v_totaldana)/100);
						IF proposional_b1 > 0.95 THEN
							SET u_proposional = CEIL((pengaliproposional*v_totaldana)/100)*100;
						ELSE
							SET u_proposional = FLOOR((pengaliproposional*v_totaldana)/100)*100;
						END IF;
				END IF;
				
				SET v_prospekditerima = totalimbal+u_proposional+sisaimbal;

				IF EXISTS (SELECT 1 FROM ih_detil_imbal_user WHERE pendanaan_id = idpendana) THEN
					DELETE FROM ih_detil_imbal_user WHERE pendanaan_id = idpendana;
					IF EXISTS (SELECT 1 FROM ih_list_imbal_user WHERE pendanaan_id = idpendana) THEN
						DELETE FROM ih_list_imbal_user WHERE pendanaan_id = idpendana;
					END IF;
				END IF;
					INSERT INTO ih_detil_imbal_user (pendanaan_id,proyek_id, total_imbal, sisa_imbal, proposional,imbal_hasil_bulanan,prospek_hasil_diterima,created_at) VALUES (idpendana,idproyek,totalimbal,sisaimbal,u_proposional,marginbulanan,v_prospekditerima,NOW());
					
					select id into v_iddetil from ih_detil_imbal_user where pendanaan_id = idpendana;
					if v_iddetil is not null then
						set v_counter = 0;
						while v_counter < v_tenor_waktu do
							-- select tanggal payout
							SELECT a.tanggal_payout into v_tanggal_payout FROM ih_list_imbal_user a JOIN pendanaan_aktif b ON a.pendanaan_id = b.id WHERE b.proyek_id = idproyek GROUP BY a.tanggal_payout ORDER BY a.tanggal_payout ASC LIMIT 1 OFFSET v_counter;
							INSERT INTO ih_list_imbal_user(detilimbaluser_id,pendanaan_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) values(v_iddetil,idpendana,v_tanggal_payout,marginbulanan,1,1,'','',now(),now());
							-- select v_iddetil,v_tanggal_payout;
							set v_counter=v_counter+1;
						end while;
						
						SELECT a.tanggal_payout into si_tanggal_payout FROM ih_list_imbal_user a JOIN pendanaan_aktif b ON a.pendanaan_id = b.id WHERE b.proyek_id = idproyek AND a.keterangan_payout = 2 GROUP BY a.tanggal_payout LIMIT 1;
						SELECT a.tanggal_payout into dp_tanggal_payout FROM ih_list_imbal_user a JOIN pendanaan_aktif b ON a.pendanaan_id = b.id WHERE b.proyek_id = idproyek AND a.keterangan_payout = 3 GROUP BY a.tanggal_payout LIMIT 1;
						INSERT INTO ih_list_imbal_user(detilimbaluser_id,pendanaan_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) values(v_iddetil,idpendana,si_tanggal_payout,sisaimbal,1,2,'','',now(),now());
						INSERT INTO ih_list_imbal_user(detilimbaluser_id,pendanaan_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) values(v_iddetil,idpendana,dp_tanggal_payout,v_totaldana,1,3,'','',now(),now());
						-- select si_tanggal_payout,dp_tanggal_payout;
					end if;
				

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_imbalhasil_fixdetillist
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_imbalhasil_fixdetillist`()
BEGIN

DECLARE v_counter INT UNSIGNED default 0;
DECLARE ulang INT;
DECLARE idproyek INT;
DECLARE idinvestor INT;
DECLARE idpendana INT;
DECLARE tglmulai DATE;

		SELECT count(*)
		INTO ulang
		FROM pendanaan_aktif a
		JOIN proyek c ON a.proyek_id = c.id
		WHERE c.`status` IN (2,3) AND a.total_dana != 0 -- AND DATE_FORMAT(c.tgl_mulai,"%d") = '12' 
			   AND DATE_FORMAT(c.tgl_mulai,"%Y%m%d") >= '20200714'
			   AND c.status_rekap = 1
				and NOT EXISTS (
		  SELECT *
		  FROM ih_list_imbal_user AS b 
		  WHERE a.id=b.pendanaan_id GROUP BY pendanaan_id
		);
	
	
	WHILE v_counter < ulang do
		SELECT a.proyek_id, a.investor_id, a.id, c.tgl_mulai
		INTO idproyek, idinvestor,idpendana, tglmulai
		FROM pendanaan_aktif a
		JOIN proyek c ON a.proyek_id = c.id
		WHERE c.`status` IN (2,3) AND a.total_dana != 0 -- AND DATE_FORMAT(c.tgl_mulai,"%d") = '12' 
			   AND DATE_FORMAT(c.tgl_mulai,"%Y%m%d") >= '20200714'
			   AND c.status_rekap = 1
				and NOT EXISTS (
		  SELECT *
		  FROM ih_list_imbal_user AS b 
		  WHERE a.id=b.pendanaan_id GROUP BY pendanaan_id
		)
		LIMIT 1 OFFSET 0;
		
		-- select v_counter,ulang,idproyek, idinvestor,idpendana, tglmulai;
	   CALL proc_imbalhasil_byidpendana(idproyek,idpendana);
		
		set v_counter=v_counter+1;
	END WHILE;


END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_imbalhasil_old
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_imbalhasil_old`(
	IN `idproyek` INT

)
BEGIN

DECLARE tgl varchar(100);
DECLARE totalrow_PA INT;
DECLARE totalrow_DIU INT;
DECLARE totalrow_LIU INT;
DECLARE totalrow_LIU_SD int;
DECLARE v_id INT;
DECLARE v_nama varchar(100);
DECLARE v_tenor_waktu int;
DECLARE v_tgl_selesai_penggalangan date;
DECLARE v_profit_margin int;
DECLARE v_tgl_mulai date;
DECLARE v_tgl_selesai date;
DECLARE counter int;
DECLARE v_tanggal_invest date;
DECLARE tglpayoutbulan1 DATE;
DECLARE rowCountDescription INT DEFAULT 0;
DECLARE rowCountDIU INT DEFAULT 0;
DECLARE v_total_dana int;
DECLARE marginbulanan DECIMAL(20,4);
DECLARE totalimbal DECIMAL(20,4);
DECLARE sisamargin DECIMAL(20,10);
DECLARE sisaimbal DECIMAL(20,4);
DECLARE selisihtanggal int;
DECLARE pengaliproposional DECIMAL(20,10);
DECLARE sisaimbal_b1 DECIMAL(20,10);
DECLARE sisaimbal_b2 DECIMAL(20,10);
DECLARE proposional_b1 DECIMAL(20,10);
DECLARE proposional DECIMAL(20,4);
DECLARE v_investor_id int;
-- DECLARE DIU_ids TEXT DEFAULT '';
DECLARE pengalimarginlesstwelve DECIMAL(20,6);
DECLARE updatePA CURSOR FOR
    SELECT id from pendanaan_aktif where pendanaan_aktif.proyek_id = idproyek AND status = '1';
DECLARE prosesLIU CURSOR FOR
    SELECT id from ih_detil_imbal_user where ih_detil_imbal_user.proyek_id = idproyek;
DECLARE prosesLIU_SD CURSOR FOR
    SELECT id from ih_detil_imbal_user where ih_detil_imbal_user.proyek_id = idproyek;


#data proyek
SELECT id,nama,tenor_waktu,tgl_selesai_penggalangan,profit_margin,tgl_mulai,tgl_selesai
into v_id,v_nama,v_tenor_waktu,v_tgl_selesai_penggalangan,v_profit_margin,v_tgl_mulai,v_tgl_selesai
from proyek WHERE id = idproyek;

Select count(*) into totalrow_PA from pendanaan_aktif where pendanaan_aktif.proyek_id = idproyek AND status = '1';

#detil_imbal_user
START TRANSACTION;
OPEN updatePA;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE pen_id INT(10);
      DECLARE v_prospekditerima DECIMAL(15,2);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      
      updatePALoop: LOOP
        FETCH updatePA INTO pen_id;
            IF exit_flag THEN LEAVE updatePALoop; 
            END IF;
            #select pen_id;
            SELECT total_dana,tanggal_invest,investor_id INTO v_total_dana,v_tanggal_invest,v_investor_id FROM pendanaan_aktif WHERE id = pen_id;
            SET selisihtanggal = datediff(v_tgl_mulai,v_tanggal_invest);
            SET pengalimarginlesstwelve = (v_profit_margin/12);
            IF v_profit_margin < 12 THEN
			       SET marginbulanan = FLOOR(((pengalimarginlesstwelve*v_total_dana)/100)/100)*100;
			       SET totalimbal = marginbulanan*v_tenor_waktu;
			       SET sisaimbal = 0;
			       SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
			   	 SET proposional = FLOOR((pengaliproposional*v_total_dana)/100)*100;
			   ELSE
			   	SET marginbulanan = (1*v_total_dana)/100;
			   	SET totalimbal = marginbulanan*v_tenor_waktu;
			   	SET sisamargin = ((v_profit_margin/12)*v_tenor_waktu)-v_tenor_waktu;
			   	SET sisaimbal_b1 = ((v_total_dana*sisamargin)/100)/100;
			   	SET sisaimbal_b2 = sisaimbal_b1 - FLOOR(((v_total_dana*sisamargin)/100)/100);
			   	IF sisaimbal_b2 > 0.95 THEN
						SET sisaimbal = CEIL(sisaimbal_b1)*100;
					ELSE
						SET sisaimbal = FLOOR(sisaimbal_b1)*100;
					END IF;
			   	#FLOOR(((v_total_dana*sisamargin)/100)/100)*100;
					#CEIL(((v_total_dana*sisamargin)/100)/100)*100;
			   	SET pengaliproposional = (0.01/30)*selisihtanggal;
			   	SET proposional_b1 = ((pengaliproposional*v_total_dana)/100) - FLOOR((pengaliproposional*v_total_dana)/100);
			   	IF proposional_b1 > 0.95 THEN
						SET proposional = CEIL((pengaliproposional*v_total_dana)/100)*100;
					ELSE
						SET proposional = FLOOR((pengaliproposional*v_total_dana)/100)*100;
					END IF;
			   	#select proposional_b1;
			   END IF;
			   SET v_prospekditerima = totalimbal+proposional+sisaimbal;
			   #select pengalimarginlesstwelve,v_tgl_selesai_penggalangan,v_tanggal_invest,selisihtanggal,pengaliproposional,proposional,sisaimbal,marginbulanan,v_profit_margin,v_total_dana,v_tenor_waktu, totalimbal;
            INSERT INTO ih_detil_imbal_user (pendanaan_id,proyek_id, total_imbal, sisa_imbal, proposional,imbal_hasil_bulanan,prospek_hasil_diterima,created_at) VALUES (pen_id,idproyek,totalimbal,sisaimbal,proposional,marginbulanan,v_prospekditerima,NOW());
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE updatePA;

Select count(*) into totalrow_DIU from ih_detil_imbal_user where ih_detil_imbal_user.proyek_id = idproyek;
--
#validasi insert detil_imbal_user
IF totalrow_PA = totalrow_DIU THEN
	COMMIT;
ELSE
	ROLLBACK;
END IF;

#list_imbal_user(imbal hasil)
START TRANSACTION;
	OPEN prosesLIU;
	BEGIN
	DECLARE v_totaldana int(10);
	DECLARE v_totalimbal int(10);
	DECLARE imbalpayout int(10);
	DECLARE v_pendanaan_id int(10);
	DECLARE v_investor_id int(10);
	DECLARE tgljadi varchar(111);
	DECLARE tglimbal varchar(12);
	DECLARE keteranganweekend varchar(111);
	DECLARE v_id int(10);
	DECLARE exit_flag INT DEFAULT 0;
	DECLARE DIU_id INT(10);
	DECLARE cntr int;
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

	prosesLIULoop: LOOP
		FETCH prosesLIU INTO DIU_id;
			IF exit_flag THEN 
				LEAVE prosesLIULoop;
			END IF;
				SELECT id,pendanaan_id,total_imbal,imbal_hasil_bulanan into v_id,v_pendanaan_id,v_totalimbal,imbalpayout from ih_detil_imbal_user where ih_detil_imbal_user.id = DIU_id;
				-- SET DIU_ids = concat("|",DIU_id,concat(DIU_ids));
				-- select DIU_ids;
				SET counter = 1;
				SET cntr = 1;
				#SET tgl = "";
				looping_tanggal: LOOP
					IF counter > v_tenor_waktu THEN
						LEAVE looping_tanggal;
					END IF;
						#membuat interval tanggal payout
						SET tgl = v_tgl_mulai + INTERVAL cntr MONTH;
						SELECT cek_tanggallibur(tgl) into tgljadi;
						SET keteranganweekend = SUBSTRING_INDEX(tgljadi,"|",1);
						-- SET tglimbal = tgl;
						SET tglimbal = SUBSTRING_INDEX(tgljadi,"|",-1);
						#select v_id,tglimbal,imbalpayout,5,1,NULL,keteranganweekend;
						INSERT INTO ih_list_imbal_user (detilimbaluser_id,pendanaan_id,tanggal_payout, imbal_payout, status_payout, keterangan_payout,keterangan, ket_libur,created_at) VALUES (v_id,v_pendanaan_id,tglimbal,imbalpayout,1,1,NULL,keteranganweekend,NOW());
						SET cntr = cntr + 1;
						SET counter = counter + 1;
				END LOOP;
		SET rowCountDIU = rowCountDIU + 1;
	END LOOP;
	END;
CLOSE prosesLIU;
 
	Select count(*) into totalrow_LIU FROM ih_list_imbal_user a left JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek;
	#select totalrow_LIU, totalrow_DIU*v_tenor_waktu , totalrow_DIU, v_tenor_waktu;

		IF totalrow_PA*v_tenor_waktu = totalrow_LIU THEN
			COMMIT;
			#updateproposional
			-- SELECT a.tanggal_payout into tglpayoutbulan1 FROM ih_list_imbal_user a LEFT JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek AND keterangan_payout = 1 GROUP BY a.tanggal_payout ORDER BY a.id limit 1;
			-- CALL proc_updateproposional(idproyek,tglpayoutbulan1);
		ELSE
			ROLLBACK;
		END IF;

-- 
#list_imbal_user(sisa imbal dan dana pokok)
START TRANSACTION;
	OPEN prosesLIU_SD;
	BEGIN
	DECLARE v_totaldana int(10);
	DECLARE v_totalimbal int(10);
	DECLARE imbalpayout int(10);
	DECLARE v_sisaimbal int(10);
	DECLARE v_pendanaan_id int(10);
	DECLARE v_investor_id int(10);
	DECLARE tgljadi varchar(111);
	DECLARE tglimbal varchar(12);
	DECLARE keteranganweekend varchar(111);
	DECLARE v_id int(10);
	
	DECLARE v_keterangan varchar(111);
	DECLARE exit_flag INT DEFAULT 0;
	DECLARE DIU_id INT(10);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

	prosesLIU_SDLoop: LOOP
		FETCH prosesLIU_SD INTO DIU_id;
			IF exit_flag THEN LEAVE prosesLIU_SDLoop;
			END IF;
			
			#SELECT pendanaan_id,investor_id,total_imbal,total_dana,sisa_imbal into v_pendanaan_id,v_investor_id,v_totalimbal,v_totaldana,v_sisaimbal from detil_imbal_user where detil_imbal_user.id = DIU_id;
			SELECT a.id,a.pendanaan_id,a.total_imbal,a.imbal_hasil_bulanan,a.sisa_imbal,b.total_dana into v_id,v_pendanaan_id,v_totalimbal,imbalpayout,v_sisaimbal,v_totaldana from ih_detil_imbal_user a left JOIN pendanaan_aktif b ON a.pendanaan_id = b.id where a.id = DIU_id;
		#membuat row untuk sisa imbal dan dana pokok
			SET counter = 1;
			
			SET tgl = v_tgl_selesai;
			looping_tanggal: LOOP
				IF counter > 2 THEN
					LEAVE looping_tanggal;
				END IF;

				SELECT cek_tanggallibur(tgl) into tgljadi;
					SET keteranganweekend = SUBSTRING_INDEX(tgljadi,"|",1);
					SET tglimbal = SUBSTRING_INDEX(tgljadi,"|",-1);
				IF counter = 1 THEN
					SET imbalpayout = v_sisaimbal;
					SET v_keterangan = 2;
				ELSEIF counter = 2 THEN
					SET imbalpayout = v_totaldana;
					SET v_keterangan = 3;
				END IF;
				
				#select v_totaldana;
				INSERT INTO ih_list_imbal_user (detilimbaluser_id,pendanaan_id,tanggal_payout, imbal_payout, status_payout, keterangan_payout,keterangan, ket_libur,created_at) VALUES (v_id,v_pendanaan_id,tglimbal,imbalpayout,1,v_keterangan,NULL,keteranganweekend,NOW());
				SET counter = counter + 1;
				END LOOP;
				SET rowCountDIU = rowCountDIU + 1;
			END LOOP;
		END;
	CLOSE prosesLIU_SD;
	
	Select count(*) into totalrow_LIU_SD FROM ih_list_imbal_user a left JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek AND a.keterangan_payout <> 1;
	
		IF totalrow_PA*2 = totalrow_LIU_SD THEN
			COMMIT;
			-- update status rekap
			START TRANSACTION;
			  Update proyek SET status_rekap = 1 WHERE id = idproyek;
			COMMIT;
			#logimbal
			-- CALL proc_ihlogimbaluser(idproyek);
			-- selesai status rekap
		ELSE
			ROLLBACK;
		END IF;
	
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_ketikadpdansihsamadenganih
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_ketikadpdansihsamadenganih`(
	IN `idproyek` INT,
	IN `tgl_payout` DATE

)
BEGIN
DECLARE rowCountDescription INT DEFAULT 0;
DECLARE v_totaldana INT;
DECLARE v_sisaimbal DECIMAL(20,2);
DECLARE idsisaimbal INT;
DECLARE iddanapokok INT;



DECLARE idchange CURSOR FOR
    SELECT a.detilimbaluser_id FROM ih_list_imbal_user a LEFT JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE proyek_id = idproyek and tanggal_payout = tgl_payout GROUP BY detilimbaluser_id order BY detilimbaluser_id ASC ;

OPEN idchange;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE change_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      
      

      idchangeLoop: LOOP
        FETCH idchange INTO change_id;
            IF exit_flag THEN LEAVE idchangeLoop; 
            END IF;
            SELECT total_dana, sisa_imbal into v_totaldana,v_sisaimbal from detil_imbal_user where id = change_id; 
            #select v_totaldana,v_sisaimbal;
            SELECT id into idsisaimbal from ih_list_imbal_user where detilimbaluser_id = change_id and tanggal_payout = tgl_payout order by id ASC limit 1;
            #select change_id,idsisaimbal;
				UPDATE ih_list_imbal_user SET imbal_payout = v_sisaimbal , keterangan_payout = 2 WHERE id = idsisaimbal;
				SELECT id into iddanapokok from ih_list_imbal_user where detilimbaluser_id = change_id and tanggal_payout = tgl_payout order by id ASC limit 1 offset 1;
				select change_id,idsisaimbal,iddanapokok;
				UPDATE ih_list_imbal_user SET imbal_payout = v_totaldana , keterangan_payout = 3 WHERE id = iddanapokok; 
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE idchange;


END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_kirimimbal
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_kirimimbal`(
	IN `status` TEXT,
	IN `proyekid` INT,
	IN `tglpayout` DATE



,
	IN `flag` INT






































)
BEGIN

DECLARE statusid INT;
DECLARE rowCountDescription INT DEFAULT 0;
DECLARE dataListImbalUser CURSOR FOR
		SELECT ih_list_imbal_user.id
		FROM ih_list_imbal_user
		JOIN pendanaan_aktif ON ih_list_imbal_user.pendanaan_id = pendanaan_aktif.id
		WHERE pendanaan_aktif.proyek_id = proyekid AND ih_list_imbal_user.tanggal_payout = tglpayout AND ih_list_imbal_user.keterangan_payout = flag AND pendanaan_aktif.status = 1;

	OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE v_imbalpayout int(50);
			DECLARE v_outcekpembayaran VARCHAR(300);
			DECLARE v_perihal VARCHAR(300);
			DECLARE v_investorid int(10);
			DECLARE v_id int(10);
			DECLARE v_pendanaanid int(10);
			DECLARE v_unallocated DECIMAL(30,2);
			DECLARE v_proposional INT(50);
			DECLARE v_totaldana DECIMAL(30,2);
			DECLARE v_keterangan VARCHAR(300);
			DECLARE v_nominalpajak INT(10);
			DECLARE v_nominalpayout INT(50);
			DECLARE inv INT(50);
			DECLARE counter INT DEFAULT 1;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
	
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
					SELECT func_splitstr(status, '|', counter) into statusid;
					#update status pada ih_list_imbal_user
					UPDATE ih_list_imbal_user SET status_payout = statusid, updated_at = NOW() WHERE id= LIU_id AND tanggal_payout = tglpayout AND keterangan_payout = flag;
					
					
					  SET v_perihal = ' ';
					  SET v_investorid = 0;
					  SET v_unallocated = 0;
					  SET v_imbalpayout = 0;
					
					SELECT c.investor_id,a.pendanaan_id,a.imbal_payout,a.nominal_pajak  into v_investorid,v_pendanaanid,v_imbalpayout,v_nominalpajak from ih_list_imbal_user a inner join pendanaan_aktif c ON c.id=a.pendanaan_id where a.id = LIU_id;
				IF flag = 1 THEN
					set v_keterangan = 'imbal hasil';
				ELSEIF flag = 2 THEN
 					set v_keterangan = 'sisa_imbal_hasil';
				ELSE
					set v_keterangan = 'Dana Pokok';
				END IF;
				
				SET v_nominalpayout = v_imbalpayout - v_nominalpajak;
					
					SELECT MIN(id) into v_id FROM ih_list_imbal_user WHERE pendanaan_id = v_pendanaanid;
					IF LIU_id = v_id THEN
						SELECT proposional into v_proposional FROM ih_detil_imbal_user 
							WHERE pendanaan_id = v_pendanaanid 
									AND id IN (SELECT MAX(id) FROM ih_detil_imbal_user 
							WHERE pendanaan_id = v_pendanaanid) ; 
						-- select proposional into v_proposional from ih_detil_imbal_user where pendanaan_id = v_pendanaanid;
						SET v_nominalpayout = v_imbalpayout+v_proposional-v_nominalpajak;
						SET v_keterangan = 'imbal hasil + proposional';
					END IF;
					
					-- log imbal user
					INSERT INTO log_imbal_user (investor_id,proyek_id,pendanaan_id,nominal,id_listimbaluser,keterangan,created_at) values (v_investorid,proyekid,v_pendanaanid,v_nominalpayout,LIU_id,v_keterangan,NOW());
					
						
						IF statusid = 3 THEN
								#insert mutasi investor
									#cek pembayaran payout ke / perihal penyimpanan dana dan id investor
								SELECT func_cekpembayaranke(LIU_id,tglpayout,flag) into v_outcekpembayaran;
								SET v_perihal = v_outcekpembayaran;
 
								-- data jumlah payout disimpan
								-- SELECT imbal_payout into v_imbalpayout from ih_list_imbal_user where id = LIU_id;
								-- SELECT func_getUnallocated(v_investorid) into inv;
-- 								 SET v_unallocated = inv;
									SELECT total_dana,unallocated INTO v_totaldana,v_unallocated FROM rekening_investor WHERE investor_id = v_investorid;
									INSERT INTO mutasi_investor (investor_id,nominal,perihal,tipe,created_at,log_payout_id) values(v_investorid,v_nominalpayout,v_perihal,'CREDIT',NOW(),LIU_id);
								-- update dana unallocated rekening investor
									
									
									IF flag = 3 THEN 
										INSERT INTO log_pengembalian_dana (proyek_id,investor_id,nominal,created_at) values(proyekid,v_investorid,v_nominalpayout,NOW());
										UPDATE rekening_investor SET unallocated = v_unallocated+v_nominalpayout,updated_at = now() WHERE investor_id = v_investorid;
									ELSEIF flag <> 3 THEN
										UPDATE rekening_investor SET unallocated = v_unallocated+v_nominalpayout, total_dana = v_totaldana+v_nominalpayout,updated_at = now() WHERE investor_id = v_investorid;
									END IF;
						END IF;
					-- select statusid,v_outcekpembayaran,v_investorid,v_perihal,v_imbalpayout,v_unallocated;
				SET rowCountDescription = rowCountDescription + 1;
				SET counter = counter+1;
			END LOOP;
		END;
	CLOSE dataListImbalUser;
	#logimbal
	-- CALL proc_ihlogimbaluser(proyekid);
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_kirimimbal_cutoff
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_kirimimbal_cutoff`(
	IN `tglpayout` DATE



)
BEGIN

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE ketpay INT DEFAULT 1;
DECLARE statuspendana INT DEFAULT 1;
DECLARE dataListImbalUser CURSOR FOR
    SELECT a.id FROM ih_list_imbal_user a JOIN pendanaan_aktif b ON a.pendanaan_id = b.id WHERE a.tanggal_payout = tglpayout AND a.keterangan_payout = '1' AND b.STATUS = '1' and a.status_payout = 1;

	CALL proc_biayaRegdanTtdPrivy(tglpayout);
	OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE v_imbalpayout int(50);
			DECLARE v_investorid int(10);
			DECLARE v_id int(10);
			DECLARE v_proyekid int(10);
			DECLARE v_pendanaanid int(10);
			DECLARE v_proposional INT(50);
			DECLARE v_keterangan VARCHAR(300);
			DECLARE v_nominalpajak INT(10);
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
	
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
					#update status pada ih_list_imbal_user
					UPDATE ih_list_imbal_user SET status_payout = 2, updated_at = NOW() WHERE id= LIU_id AND tanggal_payout = tglpayout AND keterangan_payout = 1;
					
					  SET v_investorid = 0;
					  SET v_imbalpayout = 0;
                      SET v_keterangan = 'imbal hasil';
					
					SELECT c.investor_id,a.pendanaan_id,a.imbal_payout-a.nominal_pajak,c.proyek_id INTO v_investorid,v_pendanaanid,v_imbalpayout,v_proyekid
					FROM ih_list_imbal_user a
					INNER JOIN pendanaan_aktif c ON c.id=a.pendanaan_id
					WHERE a.id = LIU_id;
					
					SELECT MIN(id) into v_id FROM ih_list_imbal_user WHERE pendanaan_id = v_pendanaanid;
					IF LIU_id = v_id THEN
						SELECT proposional into v_proposional FROM ih_detil_imbal_user 
							WHERE pendanaan_id = v_pendanaanid 
									AND id IN (SELECT MAX(id) FROM ih_detil_imbal_user 
							WHERE pendanaan_id = v_pendanaanid) ; 
						SET v_imbalpayout = v_imbalpayout+v_proposional;
                        SET v_keterangan = 'imbal hasil + proposional';
					END IF;
					
					-- log imbal user
					INSERT INTO log_imbal_user (investor_id,proyek_id,pendanaan_id,nominal,id_listimbaluser,keterangan,created_at) 
					VALUES (v_investorid,v_proyekid,v_pendanaanid,v_imbalpayout,LIU_id,v_keterangan, NOW());
					
				SET rowCountDescription = rowCountDescription + 1;
			END LOOP;
		END;
	CLOSE dataListImbalUser;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_kirimimbal_cutoffSIDP
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_kirimimbal_cutoffSIDP`(
	IN `tglpayout` DATE

)
BEGIN

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE statuspendana INT DEFAULT 1;
DECLARE dataListImbalUser CURSOR FOR
    SELECT a.id FROM ih_list_imbal_user a JOIN pendanaan_aktif b ON a.pendanaan_id = b.id WHERE a.tanggal_payout = tglpayout AND a.keterangan_payout IN (2,3) AND a.status_payout = 1 AND b.STATUS = '1';

	OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE v_imbalpayout INT(50);
			DECLARE v_investorid INT(10);
			DECLARE v_id INT(10);
			DECLARE v_proyekid INT(10);
			DECLARE v_pendanaanid INT(10);
			DECLARE v_proposional INT(50);
			DECLARE v_keterangan VARCHAR(300);
			DECLARE v_statuspayout INT(3);
			DECLARE v_ketpay INT(3);
			DECLARE v_outcekpembayaran VARCHAR(300);
			DECLARE v_perihal VARCHAR(300);
			DECLARE v_totaldana DECIMAL(15,2);
			DECLARE v_unallocated DECIMAL(15,2);
			DECLARE v_nominalpajak INT(10);
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
	
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
					#update status pada ih_list_imbal_user
					SELECT keterangan_payout into v_ketpay FROM ih_list_imbal_user WHERE id = LIU_id;
					IF v_ketpay = 2 THEN 
						SET v_statuspayout = 2;
						SET v_keterangan = 'sisa imbal hasil';
					ELSE
						SET v_statuspayout = 3;	
						SET v_keterangan = 'Dana Pokok';
					END IF;
					
					UPDATE ih_list_imbal_user SET status_payout = v_statuspayout, updated_at = NOW() WHERE id= LIU_id AND tanggal_payout = tglpayout AND keterangan_payout = v_ketpay;
					
					  SET v_investorid = 0;
					  SET v_imbalpayout = 0;
                 
					SELECT c.investor_id,a.pendanaan_id,a.imbal_payout,c.proyek_id,a.nominal_pajak 
					INTO v_investorid,v_pendanaanid,v_imbalpayout,v_proyekid,v_nominalpajak
					FROM ih_list_imbal_user a
					INNER JOIN pendanaan_aktif c ON c.id=a.pendanaan_id
					WHERE a.id = LIU_id;
					
						IF v_ketpay = 3 THEN 
							SELECT func_cekpembayaranke(LIU_id,tglpayout,v_ketpay) into v_outcekpembayaran;
							SET v_perihal = v_outcekpembayaran;
							
							SELECT total_dana,unallocated INTO v_totaldana,v_unallocated FROM rekening_investor WHERE investor_id = v_investorid;
							INSERT INTO mutasi_investor (investor_id,nominal,perihal,tipe,created_at,log_payout_id) values(v_investorid,v_imbalpayout,v_perihal,'CREDIT',NOW(),LIU_id);
							
							INSERT INTO log_pengembalian_dana (proyek_id,investor_id,nominal,created_at) values(v_proyekid,v_investorid,v_imbalpayout,NOW());
							UPDATE rekening_investor SET unallocated = v_unallocated+v_imbalpayout,updated_at = now() WHERE investor_id = v_investorid;
						END IF;
						
					-- log imbal user
					SET v_imbalpayout = v_imbalpayout - v_nominalpajak;
					INSERT INTO log_imbal_user (investor_id,proyek_id,pendanaan_id,nominal,id_listimbaluser,keterangan,created_at) 
					VALUES (v_investorid,v_proyekid,v_pendanaanid,v_imbalpayout,LIU_id,v_keterangan, NOW());
					
				SET rowCountDescription = rowCountDescription + 1;
			END LOOP;
		END;
	CLOSE dataListImbalUser;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_melengkapiselisihpendanaan
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_melengkapiselisihpendanaan`()
BEGIN


DECLARE rowCountDescription INT DEFAULT 0;
DECLARE v_counter INT unsigned DEFAULT 1;
DECLARE v_proyekid INT;
DECLARE v_totaldana INT;
DECLARE v_tanggalinvest DATE;
DECLARE v_tgl_selesai_penggalangan DATE;
DECLARE v_investorid INT;
DECLARE v_profit_margin DECIMAL(20,6);
DECLARE v_tenor_waktu INT;
DECLARE v_iddetil INT;
DECLARE selisihtanggal INT;
DECLARE pengalimarginlesstwelve DECIMAL(20,6);
DECLARE marginbulanan DECIMAL(20,6);
DECLARE totalimbal DECIMAL(20,6);
DECLARE sisaimbal DECIMAL(20,6);
DECLARE pengaliproposional DECIMAL(20,6);
DECLARE u_proposional DECIMAL(20,6);
DECLARE sisaimbal_b1 DECIMAL(20,6);
DECLARE sisamargin DECIMAL(20,6);
DECLARE sisaimbal_b2 DECIMAL(20,6);
DECLARE proposional_b1 DECIMAL(20,6);

DECLARE cekselisih CURSOR FOR
   SELECT a.id
	FROM pendanaan_aktif a
	JOIN proyek c ON a.proyek_id = c.id
	WHERE c.tgl_selesai LIKE '%2020-07%' and a.status =1 and NOT EXISTS (
	  SELECT *
	  FROM ih_list_imbal_user AS b 
	  WHERE a.id=b.pendanaan_id GROUP BY pendanaan_id
	);
	-- GROUP BY a.proyek_id;
	
	OPEN cekselisih;
	BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE pen_id INT(10);
      DECLARE v_prospekditerima DECIMAL(15,2);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      
      cekselisihLoop: LOOP
        FETCH cekselisih INTO pen_id;
            IF exit_flag THEN LEAVE cekselisihLoop; 
            END IF;
            
            select proyek_id,total_dana, tanggal_invest,investor_id into v_proyekid,v_totaldana, v_tanggalinvest,v_investorid from pendanaan_aktif where id = pen_id;
            select tgl_selesai_penggalangan, profit_margin, tenor_waktu into v_tgl_selesai_penggalangan,v_profit_margin, v_tenor_waktu from proyek where id = v_proyekid;
            -- select pen_id,v_proyekid,v_totaldana,v_tanggalinvest,v_investorid;
            
            SET selisihtanggal = datediff(v_tgl_selesai_penggalangan,v_tanggalinvest)+1;
            SET pengalimarginlesstwelve = (v_profit_margin/12);
            IF v_profit_margin < 12 THEN
			       SET marginbulanan = FLOOR(((pengalimarginlesstwelve*v_totaldana)/100)/100)*100;
			       SET totalimbal = marginbulanan*v_tenor_waktu;
			       SET sisaimbal = 0;
			       SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
			   	 SET u_proposional = FLOOR((pengaliproposional*v_totaldana)/100)*100;
			   ELSE
			   	SET marginbulanan = (1*v_totaldana)/100;
			   	SET totalimbal = marginbulanan*v_tenor_waktu;
			   	SET sisamargin = ((v_profit_margin/12)*v_tenor_waktu)-v_tenor_waktu;
			   	SET sisaimbal_b1 = ((v_totaldana*sisamargin)/100)/100;
			   	SET sisaimbal_b2 = sisaimbal_b1 - FLOOR(((v_totaldana*sisamargin)/100)/100);
			   	IF sisaimbal_b2 > 0.95 THEN
						SET sisaimbal = CEIL(sisaimbal_b1)*100;
					ELSE
						SET sisaimbal = FLOOR(sisaimbal_b1)*100;
					END IF;
			   	#FLOOR(((v_total_dana*sisamargin)/100)/100)*100;
					#CEIL(((v_total_dana*sisamargin)/100)/100)*100;
			   	SET pengaliproposional = (0.01/30)*selisihtanggal;
			   	SET proposional_b1 = ((pengaliproposional*v_totaldana)/100) - FLOOR((pengaliproposional*v_totaldana)/100);
			   	IF proposional_b1 > 0.95 THEN
						SET u_proposional = CEIL((pengaliproposional*v_totaldana)/100)*100;
					ELSE
						SET u_proposional = FLOOR((pengaliproposional*v_totaldana)/100)*100;
					END IF;
			   	#select proposional_b1;
			   END IF;
			   SET v_prospekditerima = totalimbal+u_proposional+sisaimbal;
			   #select pengalimarginlesstwelve,v_tgl_selesai_penggalangan,v_tanggal_invest,selisihtanggal,pengaliproposional,proposional,sisaimbal,marginbulanan,v_profit_margin,v_total_dana,v_tenor_waktu, totalimbal;
			   IF NOT EXISTS (SELECT 1 FROM ih_detil_imbal_user WHERE pendanaan_id = pen_id) THEN
            	INSERT INTO ih_detil_imbal_user (pendanaan_id,proyek_id, total_imbal, sisa_imbal, proposional,imbal_hasil_bulanan,prospek_hasil_diterima,created_at) VALUES (pen_id,v_proyekid,totalimbal,sisaimbal,u_proposional,marginbulanan,v_prospekditerima,NOW());
            	
            	select id into v_iddetil from ih_detil_imbal_user where pendanaan_id = pen_id;
            	if v_iddetil is not null then
            	  		set v_counter = 1;
		            	while v_counter <= v_tenor_waktu do
		            		INSERT INTO ih_list_imbal_user(detilimbaluser_id,pendanaan_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) values(v_iddetil,pen_id,'2020-01-01',marginbulanan,1,1,'','',now(),now());
		            		set v_counter=v_counter+1;
							end while;
		            	
							INSERT INTO ih_list_imbal_user(detilimbaluser_id,pendanaan_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) values(v_iddetil,pen_id,'2020-01-01',sisaimbal,1,2,'','',now(),now());
		            	INSERT INTO ih_list_imbal_user(detilimbaluser_id,pendanaan_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) values(v_iddetil,pen_id,'2020-01-01',v_totaldana,1,3,'','',now(),now());
            	end if; -- id is notnull
            END IF;
            
            
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
	  END;
	CLOSE cekselisih;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_migrasidetilimbaluser
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_migrasidetilimbaluser`()
BEGIN

DECLARE v_pendanaanid int;
DECLARE v_total_imbal DECIMAL(20,10);
DECLARE v_sisa_imbal DECIMAL(20,10);
DECLARE v_proyekid int;
DECLARE v_proposional DECIMAL(20,10);
DECLARE v_totaldana DECIMAL(20,10);
DECLARE v_createdat timestamp;
DECLARE v_id int;
DECLARE v_tenor_waktu int;
DECLARE v_tgl_selesai_penggalangan int;
DECLARE v_profit_margin DECIMAL(20,10);
DECLARE v_tgl_mulai date;
DECLARE v_tgl_selesai date;
DECLARE pengalimarginlesstwelve DECIMAL(20,6);
DECLARE marginbulanan DECIMAL(20,10);

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE totalrow_DIU INT;
DECLARE totalrow_IDIU INT;

	DECLARE getDIU CURSOR FOR
    	#SELECT id from detil_imbal_user;
    	SELECT id from detil_imbal_user order by id LIMIT 2000 OFFSET 25683;
	
	Select count(*) into totalrow_DIU from detil_imbal_user;
	
	#detil_imbal_user
	#START TRANSACTION;
	OPEN getDIU;
	  BEGIN
	      DECLARE exit_flag INT DEFAULT 0;
	      DECLARE diu_id INT(10);
	      DECLARE v_prospekditerima DECIMAL(20,6);
	      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
	      
	      getDIULoop: LOOP
	        FETCH getDIU INTO diu_id;
	            IF exit_flag THEN LEAVE getDIULoop; 
	            END IF;
	            
					#getDIU
	            SELECT a.id,pendanaan_id, a.proyek_id, a.total_imbal, a.total_dana, a.sisa_imbal, a.proposional, a.created_at,b.tenor_waktu
					INTO v_id,v_pendanaanid,v_proyekid,v_total_imbal, v_totaldana,v_sisa_imbal,v_proposional,v_createdat, v_tenor_waktu FROM detil_imbal_user a LEFT JOIN proyek b ON a.proyek_id = b.id where a.id = diu_id;
					
			IF v_tenor_waktu = 0 THEN
				SET marginbulanan = 0.00;
			ELSE
				SET marginbulanan = v_total_imbal / v_tenor_waktu;
				#set v_profit_margin = 100;
				#getproyek
				-- SELECT tenor_waktu into v_tenor_waktu from proyek WHERE id = v_proyekid;
-- 				SET marginbulanan = v_total_imbal/v_tenor_waktu;
				
			-- SET pengalimarginlesstwelve = (v_profit_margin/12.00);
-- IF v_profit_margin < 12.00 THEN
-- SET marginbulanan = FLOOR(((pengalimarginlesstwelve*v_totaldana)/100)/100)*100;
-- ELSE
-- SET marginbulanan = (1*v_totaldana)/100;
-- 				END IF;
			END IF;
			SET v_prospekditerima = v_total_imbal+v_proposional+v_sisa_imbal;
					#SELECT v_pendanaanid,v_proyekid,v_total_imbal,v_sisa_imbal,v_proposional,marginbulanan,v_prospekditerima,v_createdat;
					
	           INSERT INTO ih_detil_imbal_user (id,pendanaan_id,proyek_id, total_imbal, sisa_imbal, proposional,imbal_hasil_bulanan,prospek_hasil_diterima,created_at,updated_at) VALUES (v_id,v_pendanaanid,v_proyekid,v_total_imbal,v_sisa_imbal,v_proposional,marginbulanan,v_prospekditerima,v_createdat,NOW());
	           
	        SET rowCountDescription = rowCountDescription + 1;
	      END LOOP;
	  END;
	CLOSE getDIU;
	
	Select count(*) into totalrow_IDIU from ih_detil_imbal_user;
	--
	#validasi insert detil_imbal_user
	-- IF totalrow_DIU = totalrow_IDIU THEN
-- COMMIT;
-- ELSE
-- ROLLBACK;
-- 	END IF;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_migrasilistimbaluser
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_migrasilistimbaluser`()
BEGIN

DECLARE v_investorid INT;
DECLARE v_id int;
DECLARE v_proyekid int;
DECLARE v_pendanaanid int;
DECLARE v_tanggalpayout DATE;
DECLARE v_imbalpayout int;
DECLARE v_statuspayout int;
DECLARE v_keterangan varchar(300);
DECLARE v_ketweekend varchar(300);
DECLARE v_createdat timestamp;
DECLARE v_detilimbaluserid int;
DECLARE c_statuspayout INT;
DECLARE v_sisaimbal INT;
DECLARE v_totaldana INT;
DECLARE v_tglselesai DATE;
DECLARE c_keteranganpayout INT;
DECLARE testExist_PA INT;
DECLARE testExist_DIU INT;

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE totalrow_LIU INT;
DECLARE totalrow_ILIU INT;

	DECLARE getLIU CURSOR FOR
    	SELECT id from list_imbal_user order by id ASC limit 100000;
	
	Select count(*) into totalrow_LIU from list_imbal_user;
	
	#detil_imbal_user
	#START TRANSACTION;
	OPEN getLIU;
	  BEGIN
	      DECLARE exit_flag INT DEFAULT 0;
	      DECLARE liu_id INT(10);
	      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
	      
	      getLIULoop: LOOP
	        FETCH getLIU INTO liu_id;
	            IF exit_flag THEN LEAVE getLIULoop; 
	            END IF;
	            
					#getlistimbaluser
	            SELECT id, pendanaan_id, investor_id, tanggal_payout,imbal_payout,status_payout,keterangan,ket_weekend,created_at
					INTO v_id,v_pendanaanid,v_investorid,v_tanggalpayout,v_imbalpayout,v_statuspayout,v_keterangan,v_ketweekend,v_createdat FROM list_imbal_user WHERE id = liu_id;
					
					#getdetilimbaluserid
					IF v_pendanaanid = 0 THEN 
						SET v_detilimbaluserid = 0;
						SET v_tanggalpayout = 0;
						SET v_imbalpayout=0;
						SET v_statuspayout = 0;
						SET v_keterangan = '';
						SET v_ketweekend = '';
						SET v_createdat = now();
						SET v_sisaimbal=0;
						SET v_totaldana=0; 
					ELSE
						Select count(*) into testExist_PA from pendanaan_aktif where id = v_pendanaanid;
						Select count(*) into testExist_DIU from detil_imbal_user where pendanaan_id = v_pendanaanid;
						IF testExist_PA = 0 AND testExist_DIU = 0 THEN
							SET v_detilimbaluserid = 0;
							SET v_tanggalpayout = 0;
							SET v_imbalpayout=0;
							SET v_statuspayout = 0;
							SET v_keterangan = '';
							SET v_ketweekend = '';
							SET v_createdat = now();
							SET v_sisaimbal=0;
							SET v_totaldana=0; 
						ELSEIF testExist_PA = 0 THEN
							SELECT a.id,a.sisa_imbal,a.total_dana,a.proyek_id into v_detilimbaluserid,v_sisaimbal,v_totaldana,v_proyekid from detil_imbal_user a where a.pendanaan_id = v_pendanaanid; 
						ELSE
							SELECT a.id,a.sisa_imbal,a.total_dana,b.proyek_id into v_detilimbaluserid,v_sisaimbal,v_totaldana,v_proyekid from pendanaan_aktif b left join detil_imbal_user a on a.pendanaan_id = b.id where b.id = v_pendanaanid; 
						END IF;
						
						#statuspayout
							IF v_statuspayout = 5 THEN
								set c_statuspayout = 1;
							ELSEIF v_statuspayout = 4 THEN
								set c_statuspayout = 3;
							ELSEIF v_statuspayout = 2 THEN
								set c_statuspayout = 2;
							ELSE
								#set error flag
								set c_statuspayout = 1000;
							END IF;
							
							IF v_proyekid = 0 || v_proyekid = NULL THEN
								SET c_keteranganpayout = 1000;
							ELSE
								#v_tglselesai proyek
								SELECT tgl_selesai + INTERVAL 7 DAY into v_tglselesai FROM proyek WHERE id = v_proyekid;
								#keteranganpayout
								IF v_tanggalpayout = v_tglselesai AND v_imbalpayout = v_totaldana THEN
									SET c_keteranganpayout = 3;
								ELSEIF v_tanggalpayout = v_tglselesai AND v_imbalpayout = v_sisaimbal THEN
									SET  c_keteranganpayout = 2;
								ELSE
									SET  c_keteranganpayout = 1;
								END IF;
							END IF;
								
					END IF;
						
					#SELECT v_tglselesai;
					#v_id,v_detilimbaluserid,v_tanggalpayout,v_imbalpayout,c_statuspayout, v_ketweekend,v_createdat;
					INSERT INTO ih_list_imbal_user (id,detilimbaluser_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) VALUES (v_id,v_detilimbaluserid,v_tanggalpayout,v_imbalpayout,c_statuspayout, c_keteranganpayout,v_keterangan,v_ketweekend,v_createdat,NOW());
					
	           #INSERT INTO ih_list_imbal_user (id,detilimbaluser_id,tanggal_payout,imbal_payout,status_payout,keterangan_payout,keterangan,ket_libur,created_at,updated_at) VALUES (v_id,v_detilimbaluserid,v_tanggalpayout,v_imbalpayout,c_statuspayout, c_keteranganpayout,v_keterangan,v_ketweekend,v_createdat,NOW());
	           
	        SET rowCountDescription = rowCountDescription + 1;
	      END LOOP;
	  END;
	CLOSE getLIU;
	
	Select count(*) into totalrow_ILIU from ih_list_imbal_user;
	-- validasi insert detil_imbal_user
		-- IF totalrow_ILIU = totalrow_LIU THEN
-- COMMIT;
-- ELSE
-- ROLLBACK;
-- 		END IF;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_migrasipendanaanaktif
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_migrasipendanaanaktif`()
BEGIN

DECLARE rowCountDescription INT;
DECLARE v_investor_id INT;
DECLARE v_proyek_id INT;
DECLARE v_total_dana DECIMAL(15,2);
DECLARE v_tanggal_invest DATE;
DECLARE v_nominal_awal DECIMAL(15,2);
DECLARE v_status INT;
DECLARE v_efektif_day_proposional INT;
DECLARE v_created_at DATE;
DECLARE v_updated_at DATE;
DECLARE v_last_pay DATE;
DECLARE migPA CURSOR FOR
    SELECT id from pendanaan_aktif;

OPEN migPA;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE pen_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      

      migPALoop: LOOP
        FETCH migPA INTO pen_id;
            IF exit_flag THEN LEAVE migPALoop; 
            END IF;
            SELECT investor_id,proyek_id,total_dana,tanggal_invest,nominal_awal,status,created_at,updated_at,last_pay,(select datediff(tgl_selesai_penggalangan,v_tanggal_invest)+1 from proyek where id=proyek_id) AS efektif_day INTO v_investor_id,v_proyek_id,v_total_dana,v_tanggal_invest,v_nominal_awal,v_status,v_created_at,v_updated_at,v_last_pay,v_efektif_day_proposional FROM pendanaan_aktif WHERE id = pen_id;
            
            INSERT INTO ih_pendanaan_aktif (id,investor_id, proyek_id,total_dana,nominal_awal, tanggal_invest,efektif_day_proposional, status,last_pay,created_at,updated_at) VALUES (pen_id,v_investor_id,v_proyek_id,v_total_dana,v_nominal_awal,v_tanggal_invest,v_efektif_day_proposional,v_status,v_last_pay,v_created_at,v_updated_at);
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE migPA;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_pengambilandanaproyek
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_pengambilandanaproyek`(
	IN `pendanaanid` INT,
	IN `qty` INT

)
BEGIN

	declare v_investorid INT;
	declare v_proyekid INT;
	declare v_hargapaket INT;
	declare v_totalpenarikan INT;
	declare v_totaldana INT;
	declare v_nominalawal INT;
	declare u_totaldana INT;
	declare u_nominalawal INT;
	declare u_status INT;
	declare v_tanggalinvest DATE;
	declare v_tglselesaipenggalangan DATE;
	declare v_profitmargin INT;
	declare v_tenorwaktu INT;
	declare selisihtanggal INT;
	declare pengalimarginlesstwelve INT;
	declare marginbulanan DECIMAL(20,4);
	declare totalimbal DECIMAL(20,4);
	declare sisaimbal DECIMAL(20,4);
	declare pengaliproposional DECIMAL(20,6);
	declare v_proposional DECIMAL(20,4);
	declare vu_proposional DECIMAL(20,4);
	declare vi_proposional DECIMAL(20,4);
	declare v_payoutpertama int(3);
	declare v_statuspayout int(3);
	declare v_statusrekap int(3);
	declare sisamargin DECIMAL(20,4);
	declare v_prospekditerima DECIMAL(15,2);
	declare v_detilimbaluserid INT;
	declare v_ihbulanan INT;
	declare v_sisaimbal INT;
	declare sisaimbal_b1 DECIMAL(20,10);
	declare sisaimbal_b2 DECIMAL(20,10);
	declare proposional_b1 DECIMAL(20,10);
	declare totalpayout DECIMAL(20,4);
	declare v_pph23 decimal(10,2);

	
	
	#get harga paket
	SELECT a.investor_id,a.proyek_id,b.harga_paket,a.total_dana, a.nominal_awal,a.tanggal_invest,b.tgl_selesai_penggalangan,b.profit_margin,b.tenor_waktu,b.status_rekap into v_investorid,v_proyekid,v_hargapaket,v_totaldana,v_nominalawal,v_tanggalinvest,v_tglselesaipenggalangan,v_profitmargin,v_tenorwaktu,v_statusrekap FROM pendanaan_aktif a join proyek b ON a.proyek_id = b.id WHERE a.id=pendanaanid;
	SET v_totalpenarikan = v_hargapaket*qty;
	SET u_totaldana = v_totaldana-v_totalpenarikan;
	SET u_nominalawal = v_nominalawal-v_totalpenarikan;
	
	IF u_totaldana = 0 THEN
		SET u_status = 0;
	ELSE
		SET u_status = 1;
	END IF;
		
	#update pendanaan_aktif
	 UPDATE pendanaan_aktif SET total_dana = u_totaldana,nominal_awal = u_nominalawal,status = u_status, updated_at = NOW() WHERE id = pendanaanid;
	
	#insert new row ih_detil_imbal_user
      SET selisihtanggal = datediff(v_tglselesaipenggalangan,v_tanggalinvest)+1;
      SET pengalimarginlesstwelve = (v_profitmargin/12);
         IF v_profitmargin < 12 THEN
			   SET marginbulanan = FLOOR(((pengalimarginlesstwelve*u_totaldana)/100)/100)*100;
		      -- SET totalimbal = marginbulanan*v_tenorwaktu;
			   SET sisaimbal = 0;
			   SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
	 			SET vu_proposional = FLOOR((pengaliproposional*u_totaldana)/100)*100;
			ELSE
					SET marginbulanan = (1*u_totaldana)/100;
-- 			   	SET totalimbal = marginbulanan*v_tenorwaktu;
			   	SET sisamargin = ((v_profitmargin/12)*v_tenorwaktu)-v_tenorwaktu;
			   	SET sisaimbal_b1 = ((u_totaldana*sisamargin)/100)/100;
			   	SET sisaimbal_b2 = sisaimbal_b1 - FLOOR(((u_totaldana*sisamargin)/100)/100);
			   	
			   	IF sisaimbal_b2 > 0.95 THEN
						SET sisaimbal = CEIL(sisaimbal_b1)*100;
					ELSE
						SET sisaimbal = FLOOR(sisaimbal_b1)*100;
					END IF;
					
			   	SET pengaliproposional = (0.01/30)*selisihtanggal;
					SET proposional_b1 = ((pengaliproposional*u_totaldana)/100) - FLOOR((pengaliproposional*u_totaldana)/100);
					IF proposional_b1 > 0.95 THEN
						SET vu_proposional = CEIL((pengaliproposional*u_totaldana)/100)*100;
					ELSE
						SET vu_proposional = FLOOR((pengaliproposional*u_totaldana)/100)*100;
					END IF;
					
			END IF;
			
			SELECT MIN(id) , status_payout into v_payoutpertama, v_statuspayout FROM ih_list_imbal_user WHERE pendanaan_id = pendanaanid;
			IF v_statuspayout <> 1 THEN
				select proposional into vi_proposional from ih_detil_imbal_user where pendanaan_id = pendanaanid order by id asc limit 1;
				SET v_proposional = vi_proposional;
			ELSEIF v_statuspayout = 1 THEN
				SET v_proposional = vu_proposional;
			END IF;
			-- select proposional into v_proposional from ih_detil_imbal_user where pendanaan_id = pendanaanid order by id asc limit 1;   
			
			IF v_statusrekap = 1 THEN
         	INSERT INTO ih_detil_imbal_user (pendanaan_id,proyek_id, sisa_imbal,proposional, imbal_hasil_bulanan,created_at) VALUES (pendanaanid,v_proyekid,sisaimbal,v_proposional,marginbulanan,NOW());
         	
         	#get new detilimbaluser_id
		      SELECT a.id,a.imbal_hasil_bulanan,a.sisa_imbal 
				,CASE WHEN c.is_valid_npwp = 1 
				THEN (SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_npwp') ELSE 
				(SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_no_npwp')  END AS pph23
				INTO v_detilimbaluserid, v_ihbulanan,v_sisaimbal, v_pph23
				FROM ih_detil_imbal_user a
				LEFT JOIN pendanaan_aktif b ON b.id = a.pendanaan_id 
				LEFT JOIN detil_investor c ON b.investor_id = c.investor_id
					WHERE a.pendanaan_id = pendanaanid 
							AND a.id IN (SELECT MAX(d.id) FROM ih_detil_imbal_user d 
					WHERE d.pendanaan_id = pendanaanid);
					
				#update ih_list_imbal_user - imbalhasil
		      UPDATE ih_list_imbal_user SET imbal_payout = v_ihbulanan , detilimbaluser_id = v_detilimbaluserid, nominal_pajak = v_ihbulanan*v_pph23,
				tarif = v_pph23*100, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1  
							AND keterangan_payout = 1;
							
				#update proposional bulan 1 
				UPDATE ih_list_imbal_user SET imbal_payout = v_ihbulanan , detilimbaluser_id = v_detilimbaluserid, nominal_pajak = (v_ihbulanan+v_proposional)*v_pph23,
				tarif = v_pph23*100, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1  
							AND keterangan_payout = 1
				AND id = (SELECT MIN(c.id) FROM ih_list_imbal_user c WHERE c.pendanaan_id = pendanaanid);
							
				#update ih_list_imbal_user - sisaimbalhasil			
				UPDATE ih_list_imbal_user SET imbal_payout = v_sisaimbal , detilimbaluser_id = v_detilimbaluserid, nominal_pajak = v_sisaimbal*v_pph23,
				tarif = v_pph23*100, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1
							AND keterangan_payout = 2;
							
				#update ih_list_imbal_user - danapokok	
				UPDATE ih_list_imbal_user SET imbal_payout = u_totaldana, detilimbaluser_id = v_detilimbaluserid, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1
							AND keterangan_payout = 3;
							
				#get prospek dana diterima
				select sum(imbal_payout) into totalpayout from ih_list_imbal_user where pendanaan_id = pendanaanid;
				SET totalimbal = totalpayout - (u_totaldana+sisaimbal);
				SET totalpayout = totalpayout+v_proposional-u_totaldana;
				UPDATE ih_detil_imbal_user SET prospek_hasil_diterima = totalpayout, total_imbal = totalimbal, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND id IN (
						SELECT MAX(id)
							FROM ih_detil_imbal_user
								WHERE pendanaan_id = pendanaanid);
         END IF;
         
      
					
		#insert logpendanaan
		 INSERT INTO log_pendanaan (pendanaanAktif_id,nominal,tipe,created_at,updated_at) values (pendanaanid,v_totalpenarikan, "ambil active investation",now(),now());
		#update rekening investor
		 UPDATE rekening_investor SET unallocated = unallocated+v_totalpenarikan,updated_at = now() WHERE investor_id = v_investorid;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_pengambilandanaproyek_web
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_pengambilandanaproyek_web`(
	IN `pendanaanid` int,
	IN `qty` int,
	IN `file_` VARCHAR(100),
	IN `line_` INT,
	OUT `res` INT




)
BEGIN

	declare v_investorid INT;
	declare v_proyekid INT;
	declare v_hargapaket INT;
	declare v_totalpenarikan INT;
	declare v_totaldana INT;
	declare v_nominalawal INT;
	declare u_totaldana INT;
	declare u_nominalawal INT;
	declare u_status INT;
	declare v_tanggalinvest DATE;
	declare v_tglselesaipenggalangan DATE;
	declare v_profitmargin INT;
	declare v_tenorwaktu INT;
	declare selisihtanggal INT;
	declare pengalimarginlesstwelve INT;
	declare marginbulanan DECIMAL(20,4);
	declare totalimbal DECIMAL(20,4);
	declare sisaimbal DECIMAL(20,4);
	declare pengaliproposional DECIMAL(20,6);
	declare v_proposional DECIMAL(20,4);
	declare vu_proposional DECIMAL(20,4);
	declare vi_proposional DECIMAL(20,4);
	declare v_payoutpertama int(3);
	declare v_statuspayout int(3);
	declare v_statusrekap int(3);
	declare sisamargin DECIMAL(20,4);
	declare v_prospekditerima DECIMAL(15,2);
	declare v_detilimbaluserid INT;
	declare v_ihbulanan INT;
	declare v_sisaimbal INT;
	declare sisaimbal_b1 DECIMAL(20,10);
	declare sisaimbal_b2 DECIMAL(20,10);
	declare proposional_b1 DECIMAL(20,10);
	declare totalpayout DECIMAL(20,4);
	declare v_pph23 decimal(10,2);
	
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
		Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;

	SET res = 1;
	
	
	#get harga paket
	SELECT a.investor_id,a.proyek_id,b.harga_paket,a.total_dana, a.nominal_awal,a.tanggal_invest,b.tgl_selesai_penggalangan,b.profit_margin,b.tenor_waktu,b.status_rekap into v_investorid,v_proyekid,v_hargapaket,v_totaldana,v_nominalawal,v_tanggalinvest,v_tglselesaipenggalangan,v_profitmargin,v_tenorwaktu,v_statusrekap FROM pendanaan_aktif a join proyek b ON a.proyek_id = b.id WHERE a.id=pendanaanid;
	SET v_totalpenarikan = v_hargapaket*qty;
	SET u_totaldana = v_totaldana-v_totalpenarikan;
	SET u_nominalawal = v_nominalawal-v_totalpenarikan;
	
	IF u_totaldana = 0 THEN
		SET u_status = 0;
	ELSE
		SET u_status = 1;
	END IF;
		
	#update pendanaan_aktif
	UPDATE pendanaan_aktif SET total_dana = u_totaldana,nominal_awal = u_nominalawal,status = u_status, updated_at = NOW() WHERE id = pendanaanid;
	
	#insert new row ih_detil_imbal_user
      SET selisihtanggal = datediff(v_tglselesaipenggalangan,v_tanggalinvest)+1;
      SET pengalimarginlesstwelve = (v_profitmargin/12);
         IF v_profitmargin < 12 THEN
			   SET marginbulanan = FLOOR(((pengalimarginlesstwelve*u_totaldana)/100)/100)*100;
		      -- SET totalimbal = marginbulanan*v_tenorwaktu;
			   SET sisaimbal = 0;
			   SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
	 			SET vu_proposional = FLOOR((pengaliproposional*u_totaldana)/100)*100;
			ELSE
					SET marginbulanan = (1*u_totaldana)/100;
-- 			   	SET totalimbal = marginbulanan*v_tenorwaktu;
			   	SET sisamargin = ((v_profitmargin/12)*v_tenorwaktu)-v_tenorwaktu;
			   	SET sisaimbal_b1 = ((u_totaldana*sisamargin)/100)/100;
			   	SET sisaimbal_b2 = sisaimbal_b1 - FLOOR(((u_totaldana*sisamargin)/100)/100);
			   	
			   	IF sisaimbal_b2 > 0.95 THEN
						SET sisaimbal = CEIL(sisaimbal_b1)*100;
					ELSE
						SET sisaimbal = FLOOR(sisaimbal_b1)*100;
					END IF;
					
			   	SET pengaliproposional = (0.01/30)*selisihtanggal;
					SET proposional_b1 = ((pengaliproposional*u_totaldana)/100) - FLOOR((pengaliproposional*u_totaldana)/100);
					IF proposional_b1 > 0.95 THEN
						SET vu_proposional = CEIL((pengaliproposional*u_totaldana)/100)*100;
					ELSE
						SET vu_proposional = FLOOR((pengaliproposional*u_totaldana)/100)*100;
					END IF;
					
			END IF;
			
			SELECT MIN(id) , status_payout into v_payoutpertama, v_statuspayout FROM ih_list_imbal_user WHERE pendanaan_id = pendanaanid;
			IF v_statuspayout <> 1 THEN
				select proposional into vi_proposional from ih_detil_imbal_user where pendanaan_id = pendanaanid order by id asc limit 1;
				SET v_proposional = vi_proposional;
			ELSEIF v_statuspayout = 1 THEN
				SET v_proposional = vu_proposional;
			END IF;
			-- select proposional into v_proposional from ih_detil_imbal_user where pendanaan_id = pendanaanid order by id asc limit 1;   
			
			IF v_statusrekap = 1 THEN
         	INSERT INTO ih_detil_imbal_user (pendanaan_id,proyek_id, sisa_imbal,proposional, imbal_hasil_bulanan,created_at) VALUES (pendanaanid,v_proyekid,sisaimbal,v_proposional,marginbulanan,NOW());
         	
         	#get new detilimbaluser_id
		      SELECT a.id,a.imbal_hasil_bulanan,a.sisa_imbal 
				,CASE WHEN c.is_valid_npwp = 1 
				THEN (SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_npwp') ELSE 
				(SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_no_npwp')  END AS pph23
				INTO v_detilimbaluserid, v_ihbulanan,v_sisaimbal, v_pph23
				FROM ih_detil_imbal_user a
				LEFT JOIN pendanaan_aktif b ON b.id = a.pendanaan_id 
				LEFT JOIN detil_investor c ON b.investor_id = c.investor_id
					WHERE a.pendanaan_id = pendanaanid 
							AND a.id IN (SELECT MAX(d.id) FROM ih_detil_imbal_user d 
					WHERE d.pendanaan_id = pendanaanid);
					
				#update ih_list_imbal_user - imbalhasil
		      UPDATE ih_list_imbal_user SET imbal_payout = v_ihbulanan , detilimbaluser_id = v_detilimbaluserid, nominal_pajak = v_ihbulanan*v_pph23,
				tarif = v_pph23*100, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1  
							AND keterangan_payout = 1;
							
				#update proposional bulan 1 
				UPDATE ih_list_imbal_user SET imbal_payout = v_ihbulanan , detilimbaluser_id = v_detilimbaluserid, nominal_pajak = (v_ihbulanan+v_proposional)*v_pph23,
				tarif = v_pph23*100, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1  
							AND keterangan_payout = 1
				AND id = (SELECT MIN(c.id) FROM ih_list_imbal_user c WHERE c.pendanaan_id = pendanaanid);
							
				#update ih_list_imbal_user - sisaimbalhasil			
				UPDATE ih_list_imbal_user SET imbal_payout = v_sisaimbal , detilimbaluser_id = v_detilimbaluserid, nominal_pajak = v_sisaimbal*v_pph23,
				tarif = v_pph23*100, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1
							AND keterangan_payout = 2;
							
				#update ih_list_imbal_user - danapokok	
				UPDATE ih_list_imbal_user SET imbal_payout = u_totaldana, detilimbaluser_id = v_detilimbaluserid, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1
							AND keterangan_payout = 3;
							
				#get prospek dana diterima
				select sum(imbal_payout) into totalpayout from ih_list_imbal_user where pendanaan_id = pendanaanid;
				SET totalimbal = totalpayout - (u_totaldana+sisaimbal);
				SET totalpayout = totalpayout+v_proposional-u_totaldana;
				UPDATE ih_detil_imbal_user SET prospek_hasil_diterima = totalpayout, total_imbal = totalimbal, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND id IN (
						SELECT MAX(id)
							FROM ih_detil_imbal_user
								WHERE pendanaan_id = pendanaanid);
         END IF;
         
      
					
		#insert logpendanaan
		INSERT INTO log_pendanaan (pendanaanAktif_id,nominal,tipe,created_at,updated_at) values (pendanaanid,v_totalpenarikan, "ambil active investation",now(),now());
		#update rekening investor
		UPDATE rekening_investor SET unallocated = unallocated+v_totalpenarikan,updated_at = now() WHERE investor_id = v_investorid;
		
		select res;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_pengambilandanaproyek_web_old
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_pengambilandanaproyek_web_old`(
	IN `pendanaanid` int,
	IN `qty` int,
	IN `file_` VARCHAR(100),
	IN `line_` INT,
	OUT `res` INT

)
BEGIN

	declare v_investorid INT;
	declare v_proyekid INT;
	declare v_hargapaket INT;
	declare v_totalpenarikan INT;
	declare v_totaldana INT;
	declare v_nominalawal INT;
	declare u_totaldana INT;
	declare u_nominalawal INT;
	declare u_status INT;
	declare v_tanggalinvest DATE;
	declare v_tglselesaipenggalangan DATE;
	declare v_profitmargin INT;
	declare v_tenorwaktu INT;
	declare selisihtanggal INT;
	declare pengalimarginlesstwelve INT;
	declare marginbulanan DECIMAL(20,4);
	declare totalimbal DECIMAL(20,4);
	declare sisaimbal DECIMAL(20,4);
	declare pengaliproposional DECIMAL(20,6);
	declare v_proposional DECIMAL(20,4);
	declare vu_proposional DECIMAL(20,4);
	declare vi_proposional DECIMAL(20,4);
	declare v_payoutpertama int(3);
	declare v_statuspayout int(3);
	declare v_statusrekap int(3);
	declare sisamargin DECIMAL(20,4);
	declare v_prospekditerima DECIMAL(15,2);
	declare v_detilimbaluserid INT;
	declare v_ihbulanan INT;
	declare v_sisaimbal INT;
	declare sisaimbal_b1 DECIMAL(20,10);
	declare sisaimbal_b2 DECIMAL(20,10);
	declare proposional_b1 DECIMAL(20,10);
	declare totalpayout DECIMAL(20,4);
	
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
		Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;

	SET res = 1;
	
	
	#get harga paket
	SELECT a.investor_id,a.proyek_id,b.harga_paket,a.total_dana, a.nominal_awal,a.tanggal_invest,b.tgl_selesai_penggalangan,b.profit_margin,b.tenor_waktu,b.status_rekap into v_investorid,v_proyekid,v_hargapaket,v_totaldana,v_nominalawal,v_tanggalinvest,v_tglselesaipenggalangan,v_profitmargin,v_tenorwaktu,v_statusrekap FROM pendanaan_aktif a join proyek b ON a.proyek_id = b.id WHERE a.id=pendanaanid;
	SET v_totalpenarikan = v_hargapaket*qty;
	SET u_totaldana = v_totaldana-v_totalpenarikan;
	SET u_nominalawal = v_nominalawal-v_totalpenarikan;
	
	IF u_totaldana = 0 THEN
		SET u_status = 0;
	ELSE
		SET u_status = 1;
	END IF;
		
	#update pendanaan_aktif
	UPDATE pendanaan_aktif SET total_dana = u_totaldana,nominal_awal = u_nominalawal,status = u_status, updated_at = NOW() WHERE id = pendanaanid;
	
	#insert new row ih_detil_imbal_user
      SET selisihtanggal = datediff(v_tglselesaipenggalangan,v_tanggalinvest)+1;
      SET pengalimarginlesstwelve = (v_profitmargin/12);
         IF v_profitmargin < 12 THEN
			   SET marginbulanan = FLOOR(((pengalimarginlesstwelve*u_totaldana)/100)/100)*100;
		      -- SET totalimbal = marginbulanan*v_tenorwaktu;
			   SET sisaimbal = 0;
			   SET pengaliproposional = ((pengalimarginlesstwelve/30)/100)*selisihtanggal;
	 			SET vu_proposional = FLOOR((pengaliproposional*u_totaldana)/100)*100;
			ELSE
					SET marginbulanan = (1*u_totaldana)/100;
-- 			   	SET totalimbal = marginbulanan*v_tenorwaktu;
			   	SET sisamargin = ((v_profitmargin/12)*v_tenorwaktu)-v_tenorwaktu;
			   	SET sisaimbal_b1 = ((u_totaldana*sisamargin)/100)/100;
			   	SET sisaimbal_b2 = sisaimbal_b1 - FLOOR(((u_totaldana*sisamargin)/100)/100);
			   	
			   	IF sisaimbal_b2 > 0.95 THEN
						SET sisaimbal = CEIL(sisaimbal_b1)*100;
					ELSE
						SET sisaimbal = FLOOR(sisaimbal_b1)*100;
					END IF;
					
			   	SET pengaliproposional = (0.01/30)*selisihtanggal;
					SET proposional_b1 = ((pengaliproposional*u_totaldana)/100) - FLOOR((pengaliproposional*u_totaldana)/100);
					IF proposional_b1 > 0.95 THEN
						SET vu_proposional = CEIL((pengaliproposional*u_totaldana)/100)*100;
					ELSE
						SET vu_proposional = FLOOR((pengaliproposional*u_totaldana)/100)*100;
					END IF;
					
			END IF;
			
			SELECT MIN(id) , status_payout into v_payoutpertama, v_statuspayout FROM ih_list_imbal_user WHERE pendanaan_id = pendanaanid;
			IF v_statuspayout <> 1 THEN
				select proposional into vi_proposional from ih_detil_imbal_user where pendanaan_id = pendanaanid order by id asc limit 1;
				SET v_proposional = vi_proposional;
			ELSEIF v_statuspayout = 1 THEN
				SET v_proposional = vu_proposional;
			END IF;
			-- select proposional into v_proposional from ih_detil_imbal_user where pendanaan_id = pendanaanid order by id asc limit 1;   
			
			IF v_statusrekap = 1 THEN
         	INSERT INTO ih_detil_imbal_user (pendanaan_id,proyek_id, sisa_imbal,proposional, imbal_hasil_bulanan,created_at) VALUES (pendanaanid,v_proyekid,sisaimbal,v_proposional,marginbulanan,NOW());
         	
         	#get new detilimbaluser_id
		      SELECT id,imbal_hasil_bulanan,sisa_imbal into v_detilimbaluserid, v_ihbulanan,v_sisaimbal FROM ih_detil_imbal_user 
					WHERE pendanaan_id = pendanaanid 
							AND id IN (SELECT MAX(id) FROM ih_detil_imbal_user 
					WHERE pendanaan_id = pendanaanid);
					
				#update ih_list_imbal_user - imbalhasil
		      UPDATE ih_list_imbal_user SET imbal_payout = v_ihbulanan , detilimbaluser_id = v_detilimbaluserid, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1  
							AND keterangan_payout = 1;
							
				#update ih_list_imbal_user - sisaimbalhasil			
				UPDATE ih_list_imbal_user SET imbal_payout = v_sisaimbal , detilimbaluser_id = v_detilimbaluserid, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1
							AND keterangan_payout = 2;
							
				#update ih_list_imbal_user - danapokok	
				UPDATE ih_list_imbal_user SET imbal_payout = u_totaldana, detilimbaluser_id = v_detilimbaluserid, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND status_payout = 1
							AND keterangan_payout = 3;
							
				#get prospek dana diterima
				select sum(imbal_payout) into totalpayout from ih_list_imbal_user where pendanaan_id = pendanaanid;
				SET totalimbal = totalpayout - (u_totaldana+sisaimbal);
				SET totalpayout = totalpayout+v_proposional-u_totaldana;
				UPDATE ih_detil_imbal_user SET prospek_hasil_diterima = totalpayout, total_imbal = totalimbal, updated_at = now()
					WHERE pendanaan_id = pendanaanid AND id IN (
						SELECT MAX(id)
							FROM ih_detil_imbal_user
								WHERE pendanaan_id = pendanaanid);
         END IF;
         
      
					
		#insert logpendanaan
		INSERT INTO log_pendanaan (pendanaanAktif_id,nominal,tipe,created_at,updated_at) values (pendanaanid,v_totalpenarikan, "ambil active investation",now(),now());
		#update rekening investor
		UPDATE rekening_investor SET unallocated = unallocated+v_totalpenarikan,updated_at = now() WHERE investor_id = v_investorid;
		
		select res;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_put_into_brw_verif
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_put_into_brw_verif`(
		_brw_id int,
		_file varchar(100), 
		_line int
)
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
	declare res varchar(1000) default '0';
			
	declare exit handler for sqlexception
     begin
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('is_first_create failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
     end;
       
  
	#--------------------------------------------------------------
    
	if ( 0 = _brw_id ) then
		insert into brw_verifikator_pengajuan 
		( pengajuan_id, status_aktifitas, rekomendasi_file, catatan_rekomendasi,tgl_kirim, 
		  created_at, created_by, updated_at, updated_by) 
		select 	pengajuan_id ,  
					case 
					   when 1 = status then 1
					   when 11 = status then 2
					   else 3
					end m_status, null,null,null,
					now(),'system',now(),'system'
		from brw_pengajuan
		where status not in (0,2) and status < 13 ;
	else
		insert into brw_verifikator_pengajuan
		( pengajuan_id, status_aktifitas, rekomendasi_file, catatan_rekomendasi,tgl_kirim, 
		  created_at, created_by, updated_at, updated_by) 
		select 	pengajuan_id ,  
					case 
					   when 1 = status then 1
					   when 11 = status then 2
					   else 3
					end m_status, null,null,null,
					now(),'system',now(),'system'
		from brw_pengajuan
		where status not in (0,2) and status < 13
			  and brw_id = _brw_id; 	
	end if;

end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_recampaign_project
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_recampaign_project`(
	IN `_check_limit` int,
	IN `_pctg_limit` int,
	IN `_day_limit` int,
	IN `_file` varchar(100),
	IN `_line` int

)
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
			
	declare exit handler for sqlexception
		begin
		rollback;
		get diagnostics condition 1
		code = returned_sqlstate, msg = message_text;
		set sout = concat('proc_recampaign_project failed , error = ',code,', message = ',msg);
		insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
		end;
   	
	#--------------------------validasi_param --------------------------------------
	
	if (_check_limit < 2 and _check_limit > 10) then
		set sout = concat('proc_recampaign_project failed ', 'check_limit = ',_check_limit);
		insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
	end if;
	
	if (_pctg_limit < 30 and _pctg_limit > 50) then
		set sout = concat('proc_recampaign_project failed ', 'pctg_limit = ',_pctg_limit);
		insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
	end if;
	
	if (_day_limit < 75 and _day_limit > 120) then
		set sout = concat('proc_recampaign_project failed ', 'day_limit = ',_day_limit);
		insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
	end if;
		
	#-------------------------------------------------------------------------------
	
	if ( sout = '' ) then
	
		set autocommit = 0;
		
		truncate table temp_proyek_campaign;
		
		insert into temp_proyek_campaign
		select a.* from
		(
		select a.* from
		(
		select b.proyek_id , SUM(a.nominal) total_dana
		from 	log_pendanaan a,
				pendanaan_aktif b
		where datediff(now(),a.created_at) < _check_limit
				and a.tipe = 'ambil active investation'
				and a.pendanaanaktif_id = b.id
				and b.total_dana > 0
		group by b.proyek_id
		)a,
		proyek b
		where a.proyek_id = b.id
				and a.total_dana >= b.total_need * _pctg_limit /100
				and datediff(b.tgl_selesai,NOW()) >= _day_limit
				and b.status_rekap = 1
		)a
		left join proyek b
		on a.proyek_id = ifnull(b.id_simulasi,0)
		where b.id_simulasi is null;		
		
					
		insert into proyek 
		(nama, alamat, geocode, profit_margin, total_need, harga_paket, akad, 
		tgl_mulai, tgl_selesai, deskripsi, tgl_mulai_penggalangan, tgl_selesai_penggalangan, 
		terkumpul, status, status_tampil, waktu_bagi, tenor_waktu, embed_picture, 
		id_deskripsi, id_legalitas, id_pemilik, id_simulasi, gambar_utama, 
		created_at, updated_at, `interval`,status_rekap, lender_class, investor_id)
		select	concat(b.nama,'-V2') , b.alamat, b.geocode, b.profit_margin, a.total_dana, 
			b.harga_paket, b.akad, 
			date_add(now(), interval 15 day) as tgl_mulai, 
			date_add(date_add(now(), interval 15 day), interval ((datediff(tgl_selesai,date_add(now(), interval 15 day))) div 30) month) as tgl_selesai, 
			b.deskripsi, 
			now() as tgl_mulai_penggalangan, 
			date_add(now(), interval 14 day) as tgl_selesai_penggalangan, 
			0, 1, 2, b.waktu_bagi, 
			(datediff(tgl_selesai,date_add(now(), interval 15 day))) div 30 as tenor_waktu, b.embed_picture, 
			b.id_deskripsi, b.id_legalitas, b.id_pemilik, b.id, b.gambar_utama, 
			now(), null,  b.interval,0, b.lender_class, b.investor_id 
		from 	temp_proyek_campaign a,
				proyek b
		where a.proyek_id = b.id
				and b.id_simulasi is null;
								
		commit;
	else
		select sout;
	end if;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_req_edit_inv_badan_hukum
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_req_edit_inv_badan_hukum`(
		_investor_id int,
		_ori_account_name varchar(35),
		_account_name varchar(35),
		_email varchar(35),
		_status varchar(15),
		_keterangan varchar(100),
		_nama_perusahaan	varchar(50),
		_nib	varchar(25),
		_npwp_perusahaan	varchar(15),
		_nomor_akta_perusahaan	varchar(25),
		_tanggal_akta_perusahaan	date,
		_no_akta_pendirian	varchar(25),
		_tgl_berdiri	date,
		_nomor_akta_perubahan	varchar(25),
		_tanggal_akta_perubahan	date,
		_telpon_perusahaan	varchar(20),
		_foto_npwp_perusahaan	varchar(50),
		_alamat_perusahaan	varchar(100),
		_provinsi_perusahaan	int,
		_kota_perusahaan	varchar(5),
		_kecamatan_perusahaan	varchar(50),
		_kelurahan_perusahaan	varchar(50),
		_kode_pos_perusahaan	varchar(5),
		_rekening	varchar(20),
		_nama_pemilik_rek	varchar(35),
		_bank_investor	varchar(5),
		_bidang_pekerjaan	varchar(5),
		_omset_tahun_terakhir	decimal(15,2),
		_tot_aset_tahun_terakhr	decimal(15,2),
		_laporan_keuangan	varchar(191),
		_edit_by			varchar(35),
		_va_bni varchar(25),
		_va_cimb varchar(25),
		_va_bsi varchar(25),
		_ref_number varchar(191),
		_file varchar(100),
		_line int
)
proc_req_edit_inv_bh:begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
	declare hist_id int;
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('proc_req_edit_inv_badan_hukum failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
       select sout;
     end;
   
    set autocommit = 0;
	
	#----------------------------insert header investor-------------------------
	
	if ( 'active' = _status ) then
		insert into admin_riwayat_investor (investor_id,account_name,email,status,keterangan,ref_number,suspended_by,actived_by)
						values(_investor_id, _account_name,_email,_status,_keterangan,_ref_number,null,_edit_by);

	else
		insert into admin_riwayat_investor (investor_id,account_name,email,status,keterangan,ref_number,suspended_by,actived_by)
								values(_investor_id, _account_name,_email,_status,_keterangan,_ref_number,_edit_by,null);
	end if;
	
	select count(1), max(hist_investor_id) into @flag_ , @hist_id_  
	from admin_riwayat_investor 
	where investor_id = _investor_id;
	
	if 0 = @flag_ then
		set sout = concat(sout,'proc_req_edit_inv_bh: couldn`t find history id','#');
		select sout;
		leave proc_req_edit_inv_bh;
	end if;
	
	set hist_id = cast(@hist_id_ as int);

	#-------------------------insert header investor ori-------------------------
	insert into admin_riwayat_investor_ori (hist_investor_id,investor_id,account_name,email,status,keterangan,ref_number,suspended_by,actived_by)
		select hist_id, a.id, a.username, a.email, a.status, a.keterangan, a.ref_number, a.suspended_by, a.actived_by from investor as a where id = _investor_id;
	
	
	#----------------------------insert detil investor-------------------------
		
	insert into admin_riwayat_detil_investor_badan_hukum
	(
		hist_investor_id,
		investor_id,
		nama_perusahaan,
		nib,
		npwp_perusahaan,
		-- nomor_akta_perusahaan,
		-- tanggal_akta_perusahaan,
		no_akta_pendirian,
		tgl_berdiri,
		nomor_akta_perubahan,
		tanggal_akta_perubahan,
		telpon_perusahaan,
		foto_npwp_perusahaan,
		alamat_perusahaan,
		provinsi_perusahaan,
		kota_perusahaan,
		kecamatan_perusahaan,
		kelurahan_perusahaan,
		kode_pos_perusahaan,
		rekening,
		nama_pemilik_rek,
		bank_investor,
		bidang_pekerjaan,
		omset_tahun_terakhir,
		tot_aset_tahun_terakhr,
		laporan_keuangan
	)
	values
	(
		hist_id,
		_investor_id,
		_nama_perusahaan,
		_nib,
		_npwp_perusahaan,
		-- _nomor_akta_perusahaan,
		-- _tanggal_akta_perusahaan,
		_no_akta_pendirian,
		_tgl_berdiri,
		_nomor_akta_perubahan,
		_tanggal_akta_perubahan,
		_telpon_perusahaan,
		_foto_npwp_perusahaan,
		_alamat_perusahaan,
		_provinsi_perusahaan,
		_kota_perusahaan,
		_kecamatan_perusahaan,
		_kelurahan_perusahaan,
		_kode_pos_perusahaan,
		_rekening,
		_nama_pemilik_rek,
		_bank_investor,
		_bidang_pekerjaan,
		_omset_tahun_terakhir,
		_tot_aset_tahun_terakhr,
		_laporan_keuangan
	);
	
	#----------------------------insert detil investor-------------------------
		
	insert into admin_riwayat_detil_investor_badan_hukum_ori
	(
		hist_investor_id,
		investor_id,
		nama_perusahaan,
		nib,
		npwp_perusahaan,
		-- nomor_akta_perusahaan,
		-- tanggal_akta_perusahaan,
		no_akta_pendirian,
		tgl_berdiri,
		nomor_akta_perubahan,
		tanggal_akta_perubahan,
		telpon_perusahaan,
		foto_npwp_perusahaan,
		alamat_perusahaan,
		provinsi_perusahaan,
		kota_perusahaan,
		kecamatan_perusahaan,
		kelurahan_perusahaan,
		kode_pos_perusahaan,
		rekening,
		nama_pemilik_rek,
		bank_investor,
		bidang_pekerjaan,
		omset_tahun_terakhir,
		tot_aset_tahun_terakhr,
		laporan_keuangan
	)
	select
		hist_id,
		a.investor_id,
		a.nama_perusahaan,
		a.nib,
		a.npwp_perusahaan,
		-- a.nomor_akta_perusahaan,
		-- a.tanggal_akta_perusahaan,
		a.no_akta_pendirian,
		a.tgl_berdiri,
		a.nomor_akta_perubahan,
		a.tanggal_akta_perubahan,
		a.telpon_perusahaan,
		a.foto_npwp_perusahaan,
		a.alamat_perusahaan,
		a.provinsi_perusahaan,
		a.kota_perusahaan,
		a.kecamatan_perusahaan,
		a.kelurahan_perusahaan,
		a.kode_pos_perusahaan,
		a.rekening,
		a.nama_pemilik_rek,
		a.bank_investor,
		a.bidang_pekerjaan,
		a.omset_tahun_terakhir,
		a.tot_aset_tahun_terakhr,
		a.laporan_keuangan
	from detil_investor as a where investor_id = _investor_id;
	
	#----------------------------insert investor pengurus -------------------------
	
	
	#----------------------------insert va number investor---------------------------


	insert into admin_riwayat_nomor_va
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		_va_bni,
		'009'
	);
	
	insert into admin_riwayat_nomor_va
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		_va_cimb,
		'022'
	);
	
	insert into admin_riwayat_nomor_va
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		_va_bsi,
		'451'
	);
	
	#-------------------------insert va number investor ori---------------------------
	select va_number into @va_bni_
	from rekening_investor
	where investor_id = _investor_id
		and investor_id = _investor_id;

	select va_number into @va_cimb_
	from detil_rekening_investor 
	where investor_id = _investor_id
		and kode_bank = '022';
	
	select va_number into @va_bsi_
	from detil_rekening_investor 
	where investor_id = _investor_id
		and kode_bank = '451';

	insert into admin_riwayat_nomor_va_ori
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		@va_bni_,
		'009'
	);
	
	insert into admin_riwayat_nomor_va_ori
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		@va_cimb_,
		'022'
	);
	
	insert into admin_riwayat_nomor_va_ori
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		@va_bsi_,
		'451'
	);


	#----------------------------insert request list investor-------------------------
	
	select nama_investor into @investor_name from detil_investor where investor_id = _investor_id;
	insert into admin_permintaan_perubahan
	(
		hist_id,
		client_id,
		client_name,
		client_account,
		client_type,
		user_type,
		edit_by,
		edit_date,
		approve_by,
		approve_date,
		status
	)
	values
	(
		hist_id,
		_investor_id,
		@investor_name,
		_ori_account_name,
		1, #lender
		2, #corporate
		_edit_by,
		now(),
		null,
		null,
		1 #in progress
	);
	
	set sout = cast(hist_id as varchar(20) );							
	select sout ; 
	commit;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_req_edit_inv_bh_pengurus
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_req_edit_inv_bh_pengurus`(
		_pengurus_id int,
		_hist_id int,
		_investor_id int,
		_nama_pengurus	varchar(35),
		_jenis_kelamin	tinyint,
		_nomor_ktp	varchar(16),
		_tempat_lahir	varchar(50),
		_tgl_lahir	date,
		_kode_negara	varchar(5),
		_no_hp	varchar(20),
		_agama	tinyint,
		_pendidikan_terakhir	tinyint,
		_npwp	varchar(15),
		_jabatan	varchar(25),
		_alamat	varchar(100),
		_provinsi	tinyint,
		_kota	varchar(5),
		_kecamatan	varchar(50),
		_kelurahan	varchar(50),
		_kode_pos	varchar(5),
		_file varchar(100),
		_line int
)
proc_req_edit_inv_bh_peng:begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
	declare hist_id int;
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('proc_req_edit_inv_bh_peng failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
       select sout;
     end;
   
    set autocommit = 0;
	
	#----------------------------insert investor pengurus -------------------------

	insert into admin_riwayat_pengurus_investor_badan_hukum
	(
		pengurus_id,
		hist_investor_id ,
		investor_id,
		nama_pengurus,
		jenis_kelamin,
		nomor_ktp,
		tempat_lahir,
		tgl_lahir,
		kode_negara,
		no_hp,
		agama,
		pendidikan_terakhir,
		npwp,
		jabatan,
		alamat,
		provinsi,
		kota,
		kecamatan,
		kelurahan,
		kode_pos
	)
	values
	(
		_pengurus_id,
		_hist_id,
		_investor_id,
		_nama_pengurus,
		_jenis_kelamin,
		_nomor_ktp,
		_tempat_lahir,
		_tgl_lahir,
		_kode_negara,
		_no_hp,
		_agama,
		_pendidikan_terakhir,
		_npwp,
		_jabatan,
		_alamat,
		_provinsi,
		_kota,
		_kecamatan,
		_kelurahan,
		_kode_pos
	);

	#-------------------------insert investor pengurus ori -------------------------

	insert into admin_riwayat_pengurus_investor_badan_hukum_ori
	(
		pengurus_id,
		hist_investor_id ,
		investor_id,
		nama_pengurus,
		jenis_kelamin,
		nomor_ktp,
		tempat_lahir,
		tgl_lahir,
		kode_negara,
		no_hp,
		agama,
		pendidikan_terakhir,
		npwp,
		jabatan,
		alamat,
		provinsi,
		kota,
		kecamatan,
		kelurahan,
		kode_pos
	)
	select
		_pengurus_id,
		_hist_id,
		a.investor_id,
		a.nm_pengurus,
		a.jenis_kelamin,
		a.nik_pengurus,
		a.tempat_lahir,
		a.tgl_lahir,
		a.kode_operator,
		a.no_tlp,
		a.agama,
		a.pendidikan_terakhir,
		a.npwp,
		a.jabatan,
		a.alamat,
		a.provinsi,
		a.kota,
		a.kecamatan,
		a.kelurahan,
		a.kode_pos
	from investor_pengurus as a where a.pengurus_id = _pengurus_id;

	set sout = '1';							
	select sout ; 
	commit;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_req_edit_inv_individu
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_req_edit_inv_individu`(
	IN `_investor_id` int,
	IN `_ori_account_name` varchar(35),
	IN `_account_name` varchar(35),
	IN `_email` varchar(35),
	IN `_status` varchar(15),
	IN `_keterangan` varchar(100),
	IN `_nama_investor` varchar(35),
	IN `_jenis_identitas` tinyint,
	IN `_no_ktp_investor` varchar(16),
	IN `_no_passpor_investor` varchar(35),
	IN `_no_npwp_investor` varchar(15),
	IN `_tempat_lahir_investor` varchar(50),
	IN `_tgl_lahir_investor` date,
	IN `_jenis_kelamin_investor` tinyint,
	IN `_status_kawin_investor` tinyint,
	IN `_alamat_investor` varchar(100),
	IN `_provinsi_investor` int,
	IN `_kota_investor` varchar(5),
	IN `_kelurahan` varchar(50),
	IN `_kecamatan` varchar(50),
	IN `_kode_pos_investor` varchar(5),
	IN `_kode_operator` varchar(5),
	IN `_phone_investor` varchar(20),
	IN `_agama_investor` tinyint,
	IN `_pekerjaan_investor` tinyint,
	IN `_bidang_pekerjaan` varchar(5),
	IN `_online_investor` tinyint,
	IN `_pendapatan_investor` tinyint,
	IN `_pengalaman_investor` tinyint,
	IN `_pendidikan_investor` tinyint,
	IN `_bank_investor` varchar(5),
	IN `_domisili_negara` tinyint,
	IN `_sumber_dana` varchar(100),
	IN `_pic_investor` varchar(100),
	IN `_pic_ktp_investor` varchar(100),
	IN `_pic_user_ktp_investor` varchar(100),
	IN `_rekening` varchar(20),
	IN `_nama_pemilik_rek` varchar(35),
	IN `_nama_ibu_kandung` varchar(35),
	IN `_warganegara` tinyint,
	IN `_nama_ahli_waris` varchar(35),
	IN `_hubungan_keluarga_ahli_waris` tinyint,
	IN `_nik_ahli_waris` varchar(16),
	IN `_kode_operator_ahli_waris` varchar(5),
	IN `_no_hp_ahli_waris` varchar(20),
	IN `_alamat_ahli_waris` varchar(100),
	IN `_edit_by` varchar(35),
	IN `_va_bni` varchar(25),
	IN `_va_cimb` varchar(25),
	IN `_va_bsi` varchar(25),
	IN `_ref_number` varchar(191),
	IN `_file` varchar(100),
	IN `_line` int
)
proc_req_edit_inv_ind:begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
	declare hist_id int;
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('proc_req_edit_inv_ind failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
       select sout;
     end;
   
    set autocommit = 0;
	
	#----------------------------insert header investor-------------------------
	
	if ( 'active' = _status ) then
		insert into admin_riwayat_investor (investor_id,account_name,email,status,keterangan,ref_number,suspended_by,actived_by)
						values(_investor_id, _account_name,_email,_status,_keterangan,_ref_number,null,_edit_by);

	else
		insert into admin_riwayat_investor (investor_id,account_name,email,status,keterangan,ref_number,suspended_by,actived_by)
								values(_investor_id, _account_name,_email,_status,_keterangan,_ref_number,_edit_by,null);
	end if;
	
	select count(1), max(hist_investor_id) into @flag_ , @hist_id_  
	from admin_riwayat_investor 
	where investor_id = _investor_id;
	
	if 0 = @flag_ then
		set sout = concat(sout,'proc_req_edit_inv_ind: couldn`t find history id','#');
		select sout;
		leave proc_req_edit_inv_ind;
	end if;
	
	set hist_id = cast(@hist_id_ as int);

	#-------------------------insert header investor ori-------------------------
	insert into admin_riwayat_investor_ori (hist_investor_id,investor_id,account_name,email,status,keterangan,ref_number,suspended_by,actived_by)
						select hist_id, a.id, a.username, a.email, a.status, a.keterangan, a.ref_number, a.suspended_by, a.actived_by from investor as a where id = _investor_id;
	
	#----------------------------insert detil investor-------------------------
	
	insert into admin_riwayat_detil_investor_individu
	(
		hist_investor_id,
		investor_id,
		nama_investor,
		jenis_identitas,
		no_ktp_investor,
		no_passpor_investor,
		no_npwp_investor,
		tempat_lahir_investor,
		tgl_lahir_investor,
		jenis_kelamin_investor,
		status_kawin_investor,
		alamat_investor,
		provinsi_investor,
		kota_investor,
		kelurahan,
		kecamatan,
		kode_pos_investor,
		kode_operator,
		phone_investor,
		agama_investor,
		pekerjaan_investor,
		bidang_pekerjaan,
		online_investor,
		pendapatan_investor,
		pengalaman_investor,
		pendidikan_investor,
		bank_investor,
		domisili_negara,
		sumber_dana,
		pic_investor,
		pic_ktp_investor,
		pic_user_ktp_investor,
		rekening,
		nama_pemilik_rek,
		nama_ibu_kandung,
		warganegara
	)
	values
	(
		hist_id,
		_investor_id,
		_nama_investor,
		_jenis_identitas,
		_no_ktp_investor,
		_no_passpor_investor,
		_no_npwp_investor,
		_tempat_lahir_investor,
		_tgl_lahir_investor,
		_jenis_kelamin_investor,
		_status_kawin_investor,
		_alamat_investor,
		_provinsi_investor,
		_kota_investor,
		_kelurahan,
		_kecamatan,
		_kode_pos_investor,
		_kode_operator,
		_phone_investor,
		_agama_investor,
		_pekerjaan_investor,
		_bidang_pekerjaan,
		_online_investor,
		_pendapatan_investor,
		_pengalaman_investor,
		_pendidikan_investor,
		_bank_investor,
		_domisili_negara,
		_sumber_dana,
		_pic_investor,
		_pic_ktp_investor,
		_pic_user_ktp_investor,
		_rekening,
		_nama_pemilik_rek,
		_nama_ibu_kandung,
		_warganegara
	);
	
	#------------------------insert detil investor ori-------------------------
	
	insert into admin_riwayat_detil_investor_individu_ori
	(
		hist_investor_id,
		investor_id,
		nama_investor,
		jenis_identitas,
		no_ktp_investor,
		no_passpor_investor,
		no_npwp_investor,
		tempat_lahir_investor,
		tgl_lahir_investor,
		jenis_kelamin_investor,
		status_kawin_investor,
		alamat_investor,
		provinsi_investor,
		kota_investor,
		kelurahan,
		kecamatan,
		kode_pos_investor,
		kode_operator,
		phone_investor,
		agama_investor,
		pekerjaan_investor,
		bidang_pekerjaan,
		online_investor,
		pendapatan_investor,
		pengalaman_investor,
		pendidikan_investor,
		bank_investor,
		domisili_negara,
		sumber_dana,
		pic_investor,
		pic_ktp_investor,
		pic_user_ktp_investor,
		rekening,
		nama_pemilik_rek,
		nama_ibu_kandung,
		warganegara
	) 
	select
		hist_id,
		a.investor_id,
		a.nama_investor,
		a.jenis_identitas,
		a.no_ktp_investor,
		a.no_passpor_investor,
		a.no_npwp_investor,
		a.tempat_lahir_investor,
		a.tgl_lahir_investor,
		a.jenis_kelamin_investor,
		a.status_kawin_investor,
		a.alamat_investor,
		a.provinsi_investor,
		a.kota_investor,
		a.kelurahan,
		a.kecamatan,
		a.kode_pos_investor,
		a.kode_operator,
		a.phone_investor,
		a.agama_investor,
		a.pekerjaan_investor,
		a.bidang_pekerjaan,
		a.online_investor,
		a.pendapatan_investor,
		a.pengalaman_investor,
		a.pendidikan_investor,
		a.bank_investor,
		a.domisili_negara,
		a.sumber_dana,
		a.pic_investor,
		a.pic_ktp_investor,
		a.pic_user_ktp_investor,
		a.rekening,
		a.nama_pemilik_rek,
		a.nama_ibu_kandung,
		a.warganegara
	from detil_investor as a where investor_id = _investor_id;
	
	#----------------------------insert ahli waris investor-------------------------
	-- if ( '' != trim(_nama_ahli_waris) and _nama_ahli_waris is not null) then
		insert into admin_riwayat_ahli_waris_investor
		(
			hist_investor_id,
			investor_id,
			nama_ahli_waris,
			hubungan_keluarga_ahli_waris,
			nik_ahli_waris,
			kode_operator_ahli_waris,
			no_hp_ahli_waris,
			alamat_ahli_waris
		)
		values
		(
			hist_id,
			_investor_id,
			_nama_ahli_waris,
			_hubungan_keluarga_ahli_waris,
			_nik_ahli_waris,
			_kode_operator_ahli_waris,
			_no_hp_ahli_waris,
			_alamat_ahli_waris
		);

		
		#----------------------insert ahli waris investor ori-------------------------
		insert into admin_riwayat_ahli_waris_investor_ori
		(
			hist_investor_id,
			investor_id,
			nama_ahli_waris,
			hubungan_keluarga_ahli_waris,
			nik_ahli_waris,
			kode_operator_ahli_waris,
			no_hp_ahli_waris,
			alamat_ahli_waris
		)
		select 
			hist_id,
			a.id_investor,
			a.nama_ahli_waris,
			a.hubungan_keluarga_ahli_waris,
			a.nik_ahli_waris,
			a.kode_operator,
			a.no_hp_ahli_waris,
			a.alamat_ahli_waris
		from ahli_waris_investor as a where id_investor = _investor_id;
	-- end if;
	
	#----------------------------insert va number investor---------------------------


	insert into admin_riwayat_nomor_va
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		_va_bni,
		'009'
	);
	
	insert into admin_riwayat_nomor_va
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		_va_cimb,
		'022'
	);
	
	insert into admin_riwayat_nomor_va
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		_va_bsi,
		'451'
	);

	#-------------------------insert va number investor ori---------------------------
	select va_number into @va_bni_
	from rekening_investor
	where 	investor_id = _investor_id
		and investor_id = _investor_id;

	select va_number into @va_cimb_
	from detil_rekening_investor 
	where 	investor_id = _investor_id
			and kode_bank = '022';
	
	select va_number into @va_bsi_
	from detil_rekening_investor 
	where 	investor_id = _investor_id
			and kode_bank = '451';

	insert into admin_riwayat_nomor_va_ori
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		@va_bni_,
		'009'
	);
	
	insert into admin_riwayat_nomor_va_ori
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		@va_cimb_,
		'022'
	);
	
	insert into admin_riwayat_nomor_va_ori
	(
		hist_id,
		investor_id,
		va_bank,
		kode_bank
	)
	values
	(
		hist_id,
		_investor_id,
		@va_bsi_,
		'451'
	);
	
	#----------------------------insert request list investor-------------------------
	
	select nama_investor into @investor_name from detil_investor where investor_id = _investor_id;
	insert into admin_permintaan_perubahan
	(
		hist_id,
		client_id,
		client_name,
		client_account,
		client_type,
		user_type,
		edit_by,
		edit_date,
		approve_by,
		approve_date,
		status
	)
	values
	(
		hist_id,
		_investor_id,
		@investor_name,
		_ori_account_name,
		1,  #lender
		1, #individu
		_edit_by,
		now(),
		null,
		null,
		1 #in progress
	);
	
	set sout = '1';							
	select sout ; 
	commit;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_setParamLandingPage
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_setParamLandingPage`()
BEGIN

DECLARE jmlinvestor INT;
DECLARE danadesemberduapuluh BIGINT;
DECLARE danapinjaman BIGINT;
DECLARE danapinjaman2 BIGINT;
DECLARE jmlborrower INT;
DECLARE jmlborroweraktif INT;

-- jmlinvestor
SELECT COUNT(1) 
INTO jmlinvestor 
FROM investor a,detil_investor b 
WHERE a.id = b.investor_id 
AND STATUS IN ('active','suspend');

-- danapinjaman, danapinjaman2
SET danadesemberduapuluh = 1050000000000;
SELECT SUM(total_dana)+danadesemberduapuluh, SUM(total_dana)
INTO danapinjaman, danapinjaman2
FROM proyek a, pendanaan_aktif b 
WHERE a.id = b.proyek_id 
AND a.STATUS != 4 
AND b.status = 1
AND DATE_FORMAT(a.created_at,'%Y%m%d') > '20201231';

-- jumlahborrower
-- SELECT COUNT(1)
-- INTO jmlborrower
-- FROM brw_user a, 
-- 		brw_user_detail b
-- WHERE a.brw_id = b.brw_id;
SELECT COUNT(id) INTO jmlborrower FROM proyek;


#jumlahborroweraktif
-- SELECT COUNT(DISTINCT brw_id)
-- INTO jmlborroweraktif
-- FROM brw_pendanaan;
Select COUNT(id) INTO jmlborroweraktif FROM proyek WHERE status <> 4;

#note
-- "3" ->	"jumlah_investor"
-- "4" ->	"total_pendanaan_sejak_berdiri"
-- "5" ->	"total_pendanaan_tahun_berjalan"
-- "6" ->	"jumlah_penerima"
-- "7" ->	"jumlah_penerima_aktif"

#jmlinvestor
UPDATE m_param 
SET value = jmlinvestor
WHERE id = 3;

#total_pendanaan_sejak_berdiri
UPDATE m_param 
SET value = danapinjaman
WHERE id = 4;

#total_pendanaan_tahun_berjalan
UPDATE m_param 
SET value = danapinjaman2
WHERE id = 5;

#jumlah_penerima
UPDATE m_param 
SET value = jmlborrower
WHERE id = 6;

#jumlah_penerima_aktif
UPDATE m_param 
SET value = jmlborroweraktif
WHERE id = 7;

-- select jmlinvestor,danapinjaman,danapinjaman2,jmlborrower,jmlborroweraktif;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_tindikator
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_tindikator`()
BEGIN

DECLARE jmlinvestor int(10);
DECLARE v_tkb90 decimal(15,2) default 99.85;
DECLARE danacrowd decimal(20,4);
DECLARE danacrowd2 decimal(20,4);
DECLARE danacrowd3 decimal(20,4);
DECLARE jmlmarketer int(10);
DECLARE jmlproyekaktif int(10);
DECLARE danapinjaman decimal(20,4);
DECLARE danapinjaman2 decimal(20,4);
DECLARE danapinjaman3 decimal(20,4);
DECLARE proyekborrower int(10);
DECLARE proyekborroweraktif int(10);
DECLARE biaya1 decimal(20,4);
DECLARE biaya2 decimal(20,4);
DECLARE totalbiaya decimal(20,4);
DECLARE proyekselesai int(10);
DECLARE danaproyekselesai decimal(20,4);
DECLARE totalimbalhasil decimal(20,4);


select count(*) into jmlinvestor from investor;

SELECT SUM(total_dana) into danacrowd from pendanaan_aktif WHERE STATUS = 1;

select count(*) into jmlmarketer from marketer;

SELECT COUNT(*) into jmlproyekaktif FROM proyek WHERE tgl_selesai > NOW() and tgl_mulai < NOW() ORDER BY tgl_selesai ASC;

SELECT SUM(total_need) into danapinjaman FROM proyek;

SELECT SUM(total_need) into danapinjaman2 FROM proyek where year(tgl_mulai) = year(now());

SELECT SUM(total_dana) into danacrowd2 FROM pendanaan_aktif LEFT JOIN proyek ON proyek.id = pendanaan_aktif.proyek_id WHERE pendanaan_aktif.status = 1 and YEAR(proyek.tgl_mulai) = year(NOW());

SELECT SUM(total_need) into danapinjaman3 FROM proyek where year(tgl_selesai) >= year(now());

SELECT SUM(total_dana) into danacrowd3 FROM pendanaan_aktif LEFT JOIN proyek ON proyek.id = pendanaan_aktif.proyek_id WHERE pendanaan_aktif.status = 1 and proyek.status = 1;

SELECT COUNT(*) into proyekborrower FROM proyek;

SELECT COUNT(*) into proyekborroweraktif FROM proyek WHERE STATUS = 1 AND YEAR(tgl_selesai) >= YEAR(NOW());

SELECT SUM(total_need*((profit_margin/12)*tenor_waktu)), SUM(total_need*((5/12)*tenor_waktu)) into biaya1, biaya2 FROM proyek;
SET totalbiaya = (biaya1/100) + (biaya2/100);

SELECT count(*) into proyekselesai FROM proyek WHERE tgl_selesai <= NOW();

SELECT sum(total_need) into danaproyekselesai FROM proyek WHERE tgl_selesai <= NOW();

SELECT SUM(imbal_payout) into totalimbalhasil FROM ih_list_imbal_user WHERE keterangan_payout IN (1,2);

insert into t_indikator(updated_at, tkb90,jumlah_investor,dana_crowd,jumlah_marketer,jumlah_proyek_aktif,dana_pinjaman,dana_pinjaman_2,dana_crowd_2,dana_pinjaman_3,dana_crowd_3,proyek_borrower,proyek_borrower_aktif,total_biaya,proyek_selesai,dana_proyek_selesai,total_imbal_hasil) 
values(now(),v_tkb90,jmlinvestor,danacrowd,jmlmarketer,jmlproyekaktif,danapinjaman,danapinjaman2,danacrowd2,danapinjaman3,danacrowd3,proyekborrower,proyekborroweraktif,totalbiaya,proyekselesai,danaproyekselesai,totalimbalhasil);
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_ubahketerangandanapokoknol
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_ubahketerangandanapokoknol`(
	IN `idproyek` INT,
	IN `tgl_payout` DATE

)
BEGIN
DECLARE rowCountDescription INT DEFAULT 0;


DECLARE idchange CURSOR FOR
    SELECT a.id FROM ih_list_imbal_user a LEFT JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek AND a.imbal_payout = 0 AND a.keterangan_payout = 3;
DECLARE idlist CURSOR FOR
    SELECT a.id FROM ih_list_imbal_user a LEFT JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = idproyek AND a.tanggal_payout = tgl_payout AND a.imbal_payout = 0;
    
OPEN idchange;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE change_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      
      

      idchangeLoop: LOOP
        FETCH idchange INTO change_id;
            IF exit_flag THEN LEAVE idchangeLoop; 
            END IF;
            
           	UPDATE ih_list_imbal_user SET keterangan_payout = 1 WHERE id = change_id; 
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE idchange;

OPEN idlist;
  BEGIN
      DECLARE exit_flag INT DEFAULT 0;
      DECLARE list_id INT(10);
      DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
      
      

      idlistLoop: LOOP
        FETCH idlist INTO list_id;
            IF exit_flag THEN LEAVE idlistLoop; 
            END IF;
            
           	UPDATE ih_list_imbal_user SET keterangan_payout = 99 WHERE id = list_id; 
        SET rowCountDescription = rowCountDescription + 1;
      END LOOP;
  END;
CLOSE idlist;
    
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_updatebylender_npwpnonvalid
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_updatebylender_npwpnonvalid`(
	IN `minrecord_` INT,
	IN `maxrecord_` INT,
	OUT `res` INT
)
    COMMENT 'merubah seluruh nilai pph23 (noimnal_pajak, tariff) pada tabel imbal hasil jika ada perubahan valid npwp dengan nilai is valid 1'
BEGIN
DECLARE idLIU INT(10);
DECLARE code TEXT;
DECLARE msg TEXT;
DECLARE result TEXT;
DECLARE counter int;
DECLARE tariff DECIMAL(10,2);

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE dataListImbalUser CURSOR FOR
SELECT a.investor_id FROM detil_investor a 
	 WHERE a.is_valid_npwp = 1
	 AND a.investor_id >= minrecord_
	 AND a.investor_id <= maxrecord_;
	 
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
		Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('updatevalidnpwpbylender',25,'gagal',NOW());
     END;

	SET res = 1;
	 
	 OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE counter INT DEFAULT 1;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
			
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
				
				CALL proc_updatebylender_pph23(LIU_id,0.15);
				
				SET rowCountDescription = rowCountDescription + 1;
				SET counter = counter + 1;
			END LOOP;
		END;
	 CLOSE dataListImbalUser;
 
	SELECT res;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_updatebylender_npwpvalid
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_updatebylender_npwpvalid`(
	IN `minrecord_` INT,
	IN `maxrecord_` INT,
	OUT `res` INT
)
    COMMENT 'merubah seluruh nilai pph23 (noimnal_pajak, tariff) pada tabel imbal hasil jika ada perubahan valid npwp dengan nilai is valid 1'
BEGIN
DECLARE idLIU INT(10);
DECLARE code TEXT;
DECLARE msg TEXT;
DECLARE result TEXT;
DECLARE counter int;
DECLARE tariff DECIMAL(10,2);

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE dataListImbalUser CURSOR FOR
SELECT a.investor_id FROM detil_investor a 
	 WHERE a.is_valid_npwp = 1
	 AND a.investor_id >= minrecord_
	 AND a.investor_id <= maxrecord_;
	 
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
		Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('updatevalidnpwpbylender',25,'gagal',NOW());
     END;

	SET res = 1;
	 
	 OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE counter INT DEFAULT 1;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
			
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
				
				CALL proc_updatebylender_pph23(LIU_id,0.15);
				
				SET rowCountDescription = rowCountDescription + 1;
				SET counter = counter + 1;
			END LOOP;
		END;
	 CLOSE dataListImbalUser;
 
	SELECT res;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_updatebylender_pph23
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_updatebylender_pph23`(
	IN `investorid_` INT,
	IN `pph23_` DECIMAL(10,2)
)
    COMMENT 'merubah seluruh nilai pph23 (noimnal_pajak, tariff) pada tabel imbal hasil jika ada perubahan valid npwp'
BEGIN
DECLARE idLIU INT(10);
DECLARE code TEXT;
DECLARE msg TEXT;
DECLARE result TEXT;
DECLARE res INT;
DECLARE counter int;
DECLARE tariff DECIMAL(10,2);

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE dataListImbalUser CURSOR FOR
SELECT a.id FROM ih_list_imbal_user a 
	 LEFT JOIN pendanaan_aktif b ON a.pendanaan_id = b.id
	 LEFT JOIN proyek c ON b.proyek_id = c.id 
	 WHERE b.investor_id = investorid_
	 AND c.status IN (2)
	 AND a.status_payout = 1 
	 AND a.keterangan_payout IN (1,2);
	 
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
		Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('updatevalidnpwpbylender',25,'gagal',NOW());
     END;

	SET res = 1;
	 
	 OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE v_imbalpayout int(50);
			DECLARE v_outcekpembayaran VARCHAR(300);
			DECLARE v_perihal VARCHAR(300);
			DECLARE v_investorid int(50);
			DECLARE v_detilid int(50);
			DECLARE v_pendanaanid int(50);
			DECLARE v_hasil TEXT;
			DECLARE v_jenispajak TEXT;
			DECLARE v_tarif INT(10);
			DECLARE v_nominalpajak INT(50);
			DECLARE v_proposional int(50);
			DECLARE pendanaanid INT(50);
			DECLARE v_unallocated DECIMAL(30,2);
			DECLARE v_pph23 DECIMAL(15,2);
			DECLARE v_nominal INT(50);
			DECLARE v_payoutpertama INT(50);
			DECLARE counter INT DEFAULT 1;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
			
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
				
				-- SELECT a.id into pendanaanid FROM pendanaan_aktif a
-- 				left JOIN proyek b ON a.proyek_id = b.id
-- 				WHERE a.investor_id = investorid_
-- 				AND b.status IN (2);
				SELECT pendanaan_id,detilimbaluser_id,imbal_payout into pendanaanid,v_detilid,v_imbalpayout FROM ih_list_imbal_user WHERE id = LIU_id;
					-- SELECT CASE WHEN is_valid_npwp = 1 
-- 					THEN (SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_npwp') ELSE 
-- 					(SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_no_npwp')  END AS pph23 
-- 					INTO v_pph23 FROM detil_investor WHERE investor_id = investorid_;
				-- select LIU_id, pendanaanid,v_pph23;
				   SET v_pph23 = pph23_;
					SET v_jenispajak = 'PPH23';
					SET v_tarif = v_pph23*100;	
		
				SELECT MIN(id) into v_payoutpertama FROM ih_list_imbal_user WHERE pendanaan_id = pendanaanid;
				IF LIU_id = v_payoutpertama THEN
					SELECT proposional INTO v_proposional FROM ih_detil_imbal_user WHERE id = v_detilid;
					SET v_nominalpajak = (v_imbalpayout+v_proposional)*v_pph23;
					SET v_hasil = 'YES';
				ELSE
					SET v_nominalpajak = v_imbalpayout*v_pph23;
				 	SET v_hasil = 'NO';
				END IF;
				
				UPDATE ih_list_imbal_user SET nominal_pajak = v_nominalpajak , tarif = v_tarif, jenis_pajak = v_jenispajak WHERE id = LIU_id; 
				
				SET rowCountDescription = rowCountDescription + 1;
				SET counter = counter + 1;
			END LOOP;
		END;
	 CLOSE dataListImbalUser;
 
	-- SELECT res;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_updatebylender_pph23_nonvalidnpwp
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_updatebylender_pph23_nonvalidnpwp`(
	IN `investorid_` INT,
	IN `pph23_` DECIMAL(10,2)
)
    COMMENT 'merubah seluruh nilai pph23 (noimnal_pajak, tariff) pada tabel imbal hasil jika ada perubahan valid npwp'
BEGIN
DECLARE idLIU INT(10);
DECLARE code TEXT;
DECLARE msg TEXT;
DECLARE result TEXT;
DECLARE res INT;
DECLARE counter int;
DECLARE tariff DECIMAL(10,2);

DECLARE rowCountDescription INT DEFAULT 0;
DECLARE dataListImbalUser CURSOR FOR
SELECT a.id FROM ih_list_imbal_user a 
	 LEFT JOIN pendanaan_aktif b ON a.pendanaan_id = b.id
	 LEFT JOIN proyek c ON b.proyek_id = c.id 
	 WHERE b.investor_id = investorid_
	 AND c.status IN (2)
	 AND a.status_payout = 1 
	 AND a.keterangan_payout IN (1,2);
	 
	 DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
		Rollback;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('updatevalidnpwpbylender',25,'gagal',NOW());
     END;

	SET res = 1;
	 
	 OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE v_imbalpayout int(50);
			DECLARE v_outcekpembayaran VARCHAR(300);
			DECLARE v_perihal VARCHAR(300);
			DECLARE v_investorid int(50);
			DECLARE v_detilid int(50);
			DECLARE v_pendanaanid int(50);
			DECLARE v_hasil TEXT;
			DECLARE v_jenispajak TEXT;
			DECLARE v_tarif INT(10);
			DECLARE v_nominalpajak INT(50);
			DECLARE v_proposional int(50);
			DECLARE pendanaanid INT(50);
			DECLARE v_unallocated DECIMAL(30,2);
			DECLARE v_pph23 DECIMAL(15,2);
			DECLARE v_nominal INT(50);
			DECLARE v_payoutpertama INT(50);
			DECLARE counter INT DEFAULT 1;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
			
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
				
				-- SELECT a.id into pendanaanid FROM pendanaan_aktif a
-- 				left JOIN proyek b ON a.proyek_id = b.id
-- 				WHERE a.investor_id = investorid_
-- 				AND b.status IN (2);
				SELECT pendanaan_id,detilimbaluser_id,imbal_payout into pendanaanid,v_detilid,v_imbalpayout FROM ih_list_imbal_user WHERE id = LIU_id;
					-- SELECT CASE WHEN is_valid_npwp = 1 
-- 					THEN (SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_npwp') ELSE 
-- 					(SELECT VALUE FROM m_param WHERE deskripsi = 'pph23_no_npwp')  END AS pph23 
-- 					INTO v_pph23 FROM detil_investor WHERE investor_id = investorid_;
				-- select LIU_id, pendanaanid,v_pph23;
				   SET v_pph23 = pph23_;
					SET v_jenispajak = 'PPH23';
					SET v_tarif = v_pph23*100;	
		
				SELECT MIN(id) into v_payoutpertama FROM ih_list_imbal_user WHERE pendanaan_id = pendanaanid;
				IF LIU_id = v_payoutpertama THEN
					SELECT proposional INTO v_proposional FROM ih_detil_imbal_user WHERE id = v_detilid;
					SET v_nominalpajak = (v_imbalpayout+v_proposional)*v_pph23;
					SET v_hasil = 'YES';
				ELSE
					SET v_nominalpajak = v_imbalpayout*v_pph23;
				 	SET v_hasil = 'NO';
				END IF;
				
				UPDATE ih_list_imbal_user SET nominal_pajak = v_nominalpajak , tarif = v_tarif, jenis_pajak = v_jenispajak WHERE id = LIU_id; 
				
				SET rowCountDescription = rowCountDescription + 1;
				SET counter = counter + 1;
			END LOOP;
		END;
	 CLOSE dataListImbalUser;
 
	-- SELECT res;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_updateimbal
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_updateimbal`(
	IN `status` TEXT,
	IN `proyekid` INT,
	IN `tglpayout` DATE,
	IN `flag` INT
)
BEGIN

DECLARE statusid INT;
DECLARE rowCountDescription INT DEFAULT 0;
DECLARE dataListImbalUser CURSOR FOR
    SELECT a.id from ih_list_imbal_user a join ih_detil_imbal_user b on a.detilimbaluser_id = b.id where b.proyek_id = proyekid AND a.tanggal_payout = tglpayout and a.keterangan_payout = flag;

	OPEN dataListImbalUser;
		BEGIN
			DECLARE exit_flag INT DEFAULT 0;
			DECLARE LIU_id INT(10);
			DECLARE v_imbalpayout int(50);
			DECLARE v_outcekpembayaran VARCHAR(300);
			DECLARE v_perihal VARCHAR(300);
			DECLARE v_investorid int(50);
			DECLARE v_pendanaanid int(50);
			DECLARE v_id int(50);
			DECLARE v_proposional int(50);
			DECLARE inv INT(50);
			DECLARE v_unallocated DECIMAL(30,2);
			DECLARE v_totaldana DECIMAL(30,2);
			DECLARE v_nominal INT(50);
			DECLARE counter INT DEFAULT 1;
			DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
			
			
			dataListImbalUserLoop: LOOP
				FETCH dataListImbalUser INTO LIU_id;
				IF exit_flag THEN LEAVE dataListImbalUserLoop;
				END IF;
					SELECT func_splitstr(status, '|', counter) into statusid;
					#update status pada ih_list_imbal_user
					UPDATE ih_list_imbal_user SET status_payout = statusid, updated_at = NOW() WHERE id= LIU_id AND tanggal_payout = tglpayout AND keterangan_payout = flag;
					  SET v_perihal = ' ';
					  SET v_investorid = 0;
					  SET v_unallocated = 0;
					  SET v_imbalpayout = 0;
					  
					  SELECT pendanaan_id,imbal_payout into v_pendanaanid,v_imbalpayout FROM ih_list_imbal_user WHERE id = LIU_id;
					  SELECT MIN(id) into v_id FROM ih_list_imbal_user WHERE pendanaan_id = v_pendanaanid;
						IF LIU_id = v_id THEN
							select proposional into v_proposional from ih_detil_imbal_user where pendanaan_id = v_pendanaanid;
							SET v_imbalpayout = v_imbalpayout+v_proposional;
						END IF;
						IF statusid = 3 THEN
								#insert mutasi investor
							  IF NOT EXISTS (SELECT 1 FROM mutasi_investor WHERE log_payout_id = LIU_id) THEN
									#cek pembayaran payout ke / perihal penyimpanan dana dan id investor
								SELECT func_cekpembayaranke(LIU_id,tglpayout,flag) into v_outcekpembayaran;
								SET v_perihal = v_outcekpembayaran;
 
								-- data jumlah payout disimpan
								 SELECT c.investor_id into v_investorid from ih_list_imbal_user a inner join pendanaan_aktif c ON c.id=a.pendanaan_id where a.id = LIU_id;	 
								 -- SELECT func_getUnallocated(v_investorid) into inv;
-- 								 SET v_unallocated = inv;
								 SELECT total_dana,unallocated INTO v_totaldana, v_unallocated FROM rekening_investor WHERE investor_id = v_investorid;
								 INSERT INTO mutasi_investor (investor_id,nominal,perihal,tipe,created_at,log_payout_id) values(v_investorid,v_imbalpayout,v_perihal,'CREDIT',NOW(),LIU_id);
								-- update dana unallocated rekening investor
								-- SET s_totaldana = v_totaldana + v_imbalpayout;
								UPDATE rekening_investor SET unallocated = v_unallocated+v_imbalpayout, total_dana = v_totaldana + v_imbalpayout, updated_at = now() WHERE investor_id = v_investorid;
								  -- select v_totaldana, s_totaldana;
								 -- select LIU_id,tglpayout,flag;
								END IF;
								
						ELSE
								IF EXISTS (SELECT 1 FROM mutasi_investor WHERE log_payout_id = LIU_id) THEN
									SELECT c.investor_id into v_investorid from ih_list_imbal_user a inner join pendanaan_aktif c ON c.id=a.pendanaan_id where a.id = LIU_id;
									SELECT nominal into v_nominal FROM mutasi_investor WHERE log_payout_id = LIU_id;
									SELECT total_dana, unallocated INTO v_totaldana,v_unallocated FROM rekening_investor WHERE investor_id = v_investorid;
									UPDATE rekening_investor SET unallocated = v_unallocated-v_nominal, total_dana = v_totaldana - v_nominal, updated_at = now() WHERE investor_id = v_investorid;
									DELETE FROM mutasi_investor where log_payout_id = LIU_id;
								END IF;
						END IF;
						-- select LIU_id,tglpayout,flag,counter,v_perihal,v_investorid,v_unallocated,v_imbalpayout;
				SET rowCountDescription = rowCountDescription + 1;
				SET counter = counter+1;
			END LOOP;
		END;
	CLOSE dataListImbalUser;
	#logimbal
	-- CALL proc_ihlogimbaluser(proyekid);
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_updateimbalhasil
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_updateimbalhasil`(
	IN `arrstatus` VARCHAR(20000),
	IN `proyekid` INT,
	IN `tglpayout` DATE,
	IN `flag` INT,
	IN `ulang` INT

)
    DETERMINISTIC
BEGIN
declare v_counter int unsigned default 1;
DECLARE statusid VARCHAR(30);
DECLARE statuspembayaran varchar(2);
DECLARE LIU_id INT;
DECLARE v_perihal VARCHAR(300);
DECLARE v_investorid int(50);
DECLARE v_unallocated DECIMAL(30,2);
DECLARE v_imbalpayout int(50);
DECLARE v_pendanaanid int(50);
DECLARE v_id INT(50);
DECLARE v_proposional INT(50);
DECLARE v_outcekpembayaran VARCHAR(300);
DECLARE v_totaldana DECIMAL(30,2);
DECLARE v_nominal INT(50);
DECLARE v_nominalpajak INT(50);
DECLARE v_nominalpayout INT(50);
DECLARE v_maxid INT(50);

while v_counter <= ulang do
    				SELECT func_splitstr(arrstatus, '|', v_counter) into statusid;
					#update status pada ih_list_imbal_user
					SELECT SUBSTRING_INDEX(statusid, '-', 1) into statuspembayaran;
					SELECT SUBSTRING_INDEX(statusid, '-', -1) into LIU_id;
					
					UPDATE ih_list_imbal_user SET status_payout = statuspembayaran, updated_at = NOW() WHERE id= LIU_id AND tanggal_payout = tglpayout AND keterangan_payout = flag;
					SET v_perihal = ' ';
					SET v_investorid = 0;
					SET v_unallocated = 0;
					SET v_imbalpayout = 0;

					SELECT pendanaan_id,imbal_payout,nominal_pajak into v_pendanaanid,v_imbalpayout,v_nominalpajak FROM ih_list_imbal_user WHERE id = LIU_id;
					SET v_nominalpayout = v_imbalpayout-v_nominalpajak;
					SELECT MIN(id) into v_id FROM ih_list_imbal_user WHERE pendanaan_id = v_pendanaanid;

					IF LIU_id = v_id THEN
						SELECT MAX(id) INTO v_maxid FROM ih_detil_imbal_user WHERE pendanaan_id = v_pendanaanid;
						select proposional into v_proposional from ih_detil_imbal_user where id = v_maxid;
						SET v_nominalpayout = v_imbalpayout+v_proposional-v_nominalpajak;
					END IF;
					-- select v_pendanaanid,v_imbalpayout,v_id;
					
					IF statuspembayaran = 3 THEN
						#insert mutasi investor
						IF NOT EXISTS (SELECT 1 FROM mutasi_investor WHERE log_payout_id = LIU_id) THEN
						#cek pembayaran payout ke / perihal penyimpanan dana dan id investor
						SELECT func_cekpembayaranke(LIU_id,tglpayout,flag) into v_outcekpembayaran;
						SET v_perihal = v_outcekpembayaran;

						-- data jumlah payout disimpan
						SELECT c.investor_id into v_investorid from ih_list_imbal_user a inner join pendanaan_aktif c ON c.id=a.pendanaan_id where a.id = LIU_id;
						-- SELECT func_getUnallocated(v_investorid) into inv;
						-- SET v_unallocated = inv;
						SELECT total_dana,unallocated INTO v_totaldana, v_unallocated FROM rekening_investor WHERE investor_id = v_investorid;
						INSERT INTO mutasi_investor (investor_id,nominal,perihal,tipe,created_at,log_payout_id) values(v_investorid,v_nominalpayout,v_perihal,'CREDIT',NOW(),LIU_id);
						-- update dana unallocated rekening investor
						-- SET s_totaldana = v_totaldana + v_imbalpayout;
						UPDATE rekening_investor SET unallocated = v_unallocated+v_nominalpayout, total_dana = v_totaldana + v_nominalpayout, updated_at = now() WHERE investor_id = v_investorid;
						-- select v_totaldana, s_totaldana;
						-- select LIU_id,tglpayout,flag;
						END IF;
					ELSE
						IF EXISTS (SELECT 1 FROM mutasi_investor WHERE log_payout_id = LIU_id) THEN
							SELECT c.investor_id into v_investorid from ih_list_imbal_user a inner join pendanaan_aktif c ON c.id=a.pendanaan_id where a.id = LIU_id;
							SELECT nominal into v_nominal FROM mutasi_investor WHERE log_payout_id = LIU_id;
							SELECT total_dana, unallocated INTO v_totaldana,v_unallocated FROM rekening_investor WHERE investor_id = v_investorid;
							UPDATE rekening_investor SET unallocated = v_unallocated-v_nominal, total_dana = v_totaldana - v_nominal, updated_at = now() WHERE investor_id = v_investorid;
							DELETE FROM mutasi_investor where log_payout_id = LIU_id;
						END IF;
					END IF;
						-- select LIU_id,tglpayout,flag,counter,v_perihal,v_investorid,v_unallocated,v_imbalpayout;
    set v_counter=v_counter+1;
end while;
END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_updateproposional
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_updateproposional`(
	IN `proyekid` INT
,
	IN `tglpayoutbulan1` DATE

)
BEGIN

DECLARE rowCountDIU int default 0;
DECLARE imbalpayout int(50);
DECLARE updateimbal int(50);
DECLARE v_proposional int(50);
DECLARE ambilData_LIU CURSOR FOR
    SELECT a.id FROM ih_list_imbal_user a JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = proyekid AND tanggal_payout = tglpayoutbulan1;
    


    
   
OPEN ambilData_LIU;
	BEGIN
	DECLARE exit_flag INT DEFAULT 0;
	DECLARE LIU_id INT(10);
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;

	ambilData_LIULoop: LOOP
		FETCH ambilData_LIU INTO LIU_id;
			IF exit_flag THEN 
				LEAVE ambilData_LIULoop;
			END IF;
				
				SELECT b.proposional,b.imbal_hasil_bulanan into v_proposional,imbalpayout FROM ih_list_imbal_user a JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE a.id = LIU_id;
				SET updateimbal = imbalpayout+v_proposional;
				
				UPDATE ih_list_imbal_user SET imbal_payout = updateimbal WHERE id = LIU_id;
		SET rowCountDIU = rowCountDIU + 1;
	END LOOP;
	END;
CLOSE ambilData_LIU;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_update_inv_badan_hukum
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_update_inv_badan_hukum`(
      _hist_id int,
		_investor_id int,
		_approve_by varchar(35),
		_status int,
		_file varchar(100),
		_line int
)
proc_req_upd_inv_bh:begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('proc_update_inv_badan_hukum failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
       select sout;
     end;
   
    set autocommit = 0;
    
   #----------------------------validasi param---------------------------------
	
	if ( 2 != _status and 3 != _status ) then
		set sout = concat(sout,'proc_update_inv_badan_hukum: status only approve or reject','#');
		select sout;
		leave proc_req_upd_inv_bh;
	end if;
	
	
	if ( 3 = _status ) then
		update admin_permintaan_perubahan
		set 
			approve_by = _approve_by,
			approve_date = now(),
			status = 3
		where hist_id = _hist_id
				and client_id = _investor_id;
	
		set sout = '1';							
		select sout ; 
		commit;
		
		leave proc_req_upd_inv_bh;
	end if;
	#----------------------------update header investor-------------------------
	
	update 	investor a,
				admin_riwayat_investor b
	set 	   
		a.username = b.account_name,
		a.email = b.email,
		a.status = b.status,
		a.updated_at = now(),
		a.keterangan = b.keterangan,
		a.suspended_by = b.suspended_by,
		a.actived_by = b.actived_by,
		a.ref_number = b.ref_number 
	where b.hist_investor_id = _hist_id
			and a.id = _investor_id
			and a.id = b.investor_id;
	
	#----------------------------update detil investor-------------------------

	update 	detil_investor a,
			admin_riwayat_detil_investor_badan_hukum b
	set
		a.nama_perusahaan = b.nama_perusahaan,
		a.nib = b.nib,
		a.npwp_perusahaan = b.npwp_perusahaan,
		a.no_akta_pendirian = b.no_akta_pendirian,
		a.tgl_berdiri = b.tgl_berdiri,
		-- a.nomor_akta_perusahaan = b.nomor_akta_perusahaan,
		-- a.tanggal_akta_perusahaan = b.tanggal_akta_perusahaan,
		a.nomor_akta_perubahan = b.nomor_akta_perubahan,
		a.tanggal_akta_perubahan = b.tanggal_akta_perubahan,
		a.telpon_perusahaan = b.telpon_perusahaan,
		a.foto_npwp_perusahaan = b.foto_npwp_perusahaan,
		a.alamat_perusahaan = b.alamat_perusahaan,
		a.provinsi_perusahaan = b.provinsi_perusahaan,
		a.kota_perusahaan = b.kota_perusahaan,
		a.kecamatan_perusahaan = b.kecamatan_perusahaan,
		a.kelurahan_perusahaan = b.kelurahan_perusahaan,
		a.kode_pos_perusahaan = b.kode_pos_perusahaan,
		a.bidang_pekerjaan = b.bidang_pekerjaan,
		a.omset_tahun_terakhir = b.omset_tahun_terakhir,
		a.tot_aset_tahun_terakhr = b.tot_aset_tahun_terakhr,
		a.laporan_keuangan = b.laporan_keuangan,
		a.rekening = b.rekening,
		a.nama_pemilik_rek = b.nama_pemilik_rek,
		a.bank_investor = b.bank_investor
	where b.hist_investor_id = _hist_id
			and a.investor_id = _investor_id
			and a.investor_id = b.investor_id;

	#----------------------------update va number bni investor_bh---------------------------
	
	select count(1), va_number into @flag_ , @va_number_
	from rekening_investor 
	where investor_id = _investor_id;
			
	if ( 0 = @flag_ ) then
		select count(1), va_bank into @flag2_ , @va_bni_
		from admin_riwayat_nomor_va
		where 	hist_id = _hist_id
				and investor_id = _investor_id
				and kode_bank = '009';
				
		if ( 0 != @flag2_) then
			insert into rekening_investor
			(
				investor_id,
				va_number,
				total_dana,
				unallocated,
				payment_ntb,
				kode_bank,
				type,
				status,
				created_at,
				updated_at
			)
			values
			(
				_investor_id,
				@va_bni_,
				0,
				0,
				null,
				'009',
				'V',
				'A',
				now(),
				null
			);
		end if;
	
	else	
		update 	rekening_investor a,
					admin_riwayat_nomor_va b
		set
			a.va_number = b.va_bank,
			a.updated_at = now()
		where b.hist_id = _hist_id
				and a.investor_id = _investor_id
				and a.investor_id = b.investor_id
				and b.kode_bank = '009';	
	end if;
	
	#---------------va niaga & va bsi--------------------------------
	
	select count(1), va_number into @flag_ , @va_cimb_
	from detil_rekening_investor 
	where 	investor_id = _investor_id
			and kode_bank = '022';
			
	if ( 0 = @flag_ ) then
		select count(1), va_bank into @flag2_ , @va_cimb_
		from admin_riwayat_nomor_va
		where 	hist_id = _hist_id
				and investor_id = _investor_id
				and kode_bank = '022';
			
		if ( 0 != @flag2_) then
			insert into detil_rekening_investor
			(
				investor_id,
				va_number,
				last_amount,
				last_payment_ntb,
				kode_bank,
				type,
				status,
				created_at,
				updated_at
			)
			values
			(
				_investor_id,
				@va_cimb_,
				0,
				null,
				'022',
				'V',
				'A',
				now(),
				null
			);		
		end if;
			
	
	else
		update 	detil_rekening_investor a,
					admin_riwayat_nomor_va b
		set
			a.va_number = b.va_bank,
			a.updated_at = now()
		where b.hist_id = _hist_id
				and a.investor_id = _investor_id
				and a.investor_id = b.investor_id
				and a.kode_bank = '022'
				and b.kode_bank = '022';
	end if;
			
			
			
	select count(1), va_number into @flag_ , @va_bsi_
	from detil_rekening_investor 
	where 	investor_id = _investor_id
			and kode_bank = '451';
			
	if ( 0 = @flag_ ) then
		select count(1), va_bank into @flag2_ , @va_bsi_
		from admin_riwayat_nomor_va
		where 	hist_id = _hist_id
				and investor_id = _investor_id
				and kode_bank = '451';
			
		if ( 0 != @flag2_) then
			insert into detil_rekening_investor
			(
				investor_id,
				va_number,
				last_amount,
				last_payment_ntb,
				kode_bank,
				type,
				status,
				created_at,
				updated_at
			)
			values
			(
				_investor_id,
				@va_bsi_,
				0,
				null,
				'451',
				'V',
				'A',
				now(),
				null
			);		
		end if;
	else		
		update 	detil_rekening_investor a,
					admin_riwayat_nomor_va b
		set
			a.va_number = b.va_bank,
			a.updated_at = now()
		where b.hist_id = _hist_id
				and a.investor_id = _investor_id
				and a.investor_id = b.investor_id
				and a.kode_bank = '451'
				and b.kode_bank = '451';
	end if;	
	
	#----------------------------update request list investor-------------------------
	
	update admin_permintaan_perubahan
	set 
		approve_by = _approve_by,
		approve_date = now(),
		status = 2
	where hist_id = _hist_id
			and client_id = _investor_id;
	
	
	set sout = '1';							
	select sout ; 
	commit;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_update_inv_badan_hukum_pengurus
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_update_inv_badan_hukum_pengurus`(
		_header_pengurus tinyint,#0 = pengurus biasa , #1 = pengurus kepala	
		_hist_id int,
		_investor_id int,
		_approve_by varchar(35),
		_status int,
		_file varchar(100),
		_line int
)
proc_req_upd_inv_bh_peng:begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('proc_update_inv_badan_hukum failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
       select sout;
     end;
   
    set autocommit = 0;
    
   #----------------------------validasi param---------------------------------
	
	if ( 2 != _status and 3 != _status ) then
		set sout = concat(sout,'proc_update_inv_badan_hukum: status only approve or reject','#');
		select sout;
		leave proc_req_upd_inv_bh_peng;
	end if;
	
	
	if ( 3 = _status ) then
		update admin_permintaan_perubahan
		set 
			approve_by = _approve_by,
			approve_date = now(),
			status = 3
		where hist_id = _hist_id
				and client_id = _investor_id;
	
		set sout = '1';							
		select sout ; 
		commit;
		
		leave proc_req_upd_inv_bh_peng;
	end if;
	
	#----------------------------update investor pengurus-------------------------------
	
	update 	investor_pengurus a, 
			admin_riwayat_pengurus_investor_badan_hukum b
	set
		a.pengurus_id = b.pengurus_id,
		a.investor_id = b.investor_id,
		a.nm_pengurus = b.nama_pengurus,
		a.jenis_kelamin = b.jenis_kelamin,
		a.nik_pengurus = b.nomor_ktp,
		a.tempat_lahir = b.tempat_lahir,
		a.tgl_lahir = b.tgl_lahir,
		a.kode_operator = b.kode_negara,
		a.no_tlp = b.no_hp,
		a.agama = b.agama,
		a.pendidikan_terakhir = b.pendidikan_terakhir,
		a.npwp = b.npwp,
		a.jabatan = b.jabatan,
		a.alamat = b.alamat,
		a.provinsi = b.provinsi,
		a.kota =	b.kota,
		a.kecamatan = b.kecamatan,
		a.kelurahan = b.kelurahan,
		a.kode_pos = b.kode_pos
	where b.hist_investor_id = _hist_id
			and a.investor_id = _investor_id
			and a.investor_id = b.investor_id
			and a.pengurus_id = b.pengurus_id;
			
	if( 1 = _header_pengurus ) then
		update 	detil_investor a, 
		admin_riwayat_pengurus_investor_badan_hukum b
		set
			a.nama_investor = b.nama_pengurus,
			a.jenis_identitas = 1,
			a.no_ktp_investor = b.nomor_ktp,
			a.no_npwp_investor = b.npwp,
			a.tempat_lahir_investor = b.tempat_lahir,
			a.tgl_lahir_investor = date_format(b.tgl_lahir,'%d-%m-%Y'),
			a.alamat_investor = b.alamat,
			a.provinsi_investor = b.provinsi,
			a.kota_investor = b.kota,
			a.kelurahan = b.kelurahan,
			a.kecamatan = b.kecamatan,
			a.kode_pos_investor = b.kode_pos,
			a.phone_investor = b.no_hp,
			a.jenis_kelamin_investor = b.jenis_kelamin,
			a.agama_investor = b.agama,
			a.pendidikan_investor = b.pendidikan_terakhir
		where b.hist_investor_id = _hist_id
					and a.investor_id = _investor_id
					and a.investor_id = b.investor_id;
	end if;
	
	
	set sout = '1';							
	select sout ; 
	commit;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_update_inv_individu
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_update_inv_individu`(
      _hist_id int,
		_investor_id int,
		_approve_by varchar(35),
		_status int,
		_file varchar(100),
		_line int
)
proc_req_upd_inv_ind:begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('proc_update_inv_individu failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
       select sout;
     end;
   
    set autocommit = 0;
    
   #----------------------------validasi param---------------------------------
	
	if ( 2 != _status and 3 != _status ) then
		set sout = concat(sout,'proc_update_inv_individu: status only approve or reject','#');
		select sout;
		leave proc_req_upd_inv_ind;
	end if;
	
	
	if ( 3 = _status ) then
		update admin_permintaan_perubahan
		set 
			approve_by = _approve_by,
			approve_date = now(),
			status = 3
		where hist_id = _hist_id
				and client_id = _investor_id;
	
		set sout = '1';							
		select sout ; 
		commit;
		
		leave proc_req_upd_inv_ind;
	end if;
	#----------------------------update header investor-------------------------
	
	update 	investor a,
				admin_riwayat_investor b
	set 	   
		a.username = b.account_name,
		a.email = b.email,
		a.status = b.status,
		a.updated_at = now(),
		a.keterangan = b.keterangan,
		a.suspended_by = b.suspended_by,
		a.actived_by = b.actived_by,
		a.ref_number = b.ref_number	
	where b.hist_investor_id = _hist_id
			and a.id = _investor_id
			and a.id = b.investor_id;
	
	#----------------------------update detil investor-------------------------
	
	update 	detil_investor a,
				admin_riwayat_detil_investor_individu b
	set		
		a.nama_investor = b.nama_investor,
		a.jenis_identitas = b.jenis_identitas,
		a.no_ktp_investor = b.no_ktp_investor,
		a.no_passpor_investor = b.no_passpor_investor,
		a.no_npwp_investor = b.no_npwp_investor,
		a.tempat_lahir_investor = b.tempat_lahir_investor,
		a.tgl_lahir_investor = date_format(b.tgl_lahir_investor,'%d-%m-%Y'),
		a.jenis_kelamin_investor = b.jenis_kelamin_investor,
		a.status_kawin_investor = b.status_kawin_investor,
		a.alamat_investor = b.alamat_investor,
		a.provinsi_investor = b.provinsi_investor,
		a.kota_investor = b.kota_investor,
		a.kelurahan = b.kelurahan,
		a.kecamatan = b.kecamatan,
		a.kode_pos_investor = b.kode_pos_investor,
		a.kode_operator = b.kode_operator,
		a.phone_investor = b.phone_investor,
		a.agama_investor = b.agama_investor,
		a.pekerjaan_investor = b.pekerjaan_investor,
		a.bidang_pekerjaan = b.bidang_pekerjaan,
		a.online_investor = b.online_investor,
		a.pendapatan_investor = b.pendapatan_investor,
		a.pengalaman_investor = b.pengalaman_investor,
		a.pendidikan_investor = b.pendidikan_investor,
		a.bank_investor = b.bank_investor,
		a.domisili_negara = b.domisili_negara,
		a.sumber_dana = b.sumber_dana,
		a.pic_investor = b.pic_investor,
		a.pic_ktp_investor = b.pic_ktp_investor,
		a.pic_user_ktp_investor = b.pic_user_ktp_investor,
		a.rekening = b.rekening,
		a.nama_pemilik_rek = b.nama_pemilik_rek,
		a.nama_ibu_kandung = b.nama_ibu_kandung,
		a.warganegara = b.warganegara
	where b.hist_investor_id = _hist_id
			and a.investor_id = _investor_id
			and a.investor_id = b.investor_id;

	
	#----------------------------update ahli waris investor-------------------------
	select count(1) into @flag_ahli_waris_ 
	from ahli_waris_investor 
	where id_investor = _investor_id;

	if ( 0 = @flag_ahli_waris_ ) then
		insert into ahli_waris_investor 
		( 
			id_investor,
			nama_ahli_waris,
			hubungan_keluarga_ahli_waris,
			nik_ahli_waris,
			kode_operator,
			no_hp_ahli_waris,
			alamat_ahli_waris,
			created_at
		)
		select 
			a.investor_id,
			a.nama_ahli_waris,
			a.hubungan_keluarga_ahli_waris,
			a.nik_ahli_waris,
			a.kode_operator_ahli_waris,
			a.no_hp_ahli_waris,
			a.alamat_ahli_waris,
			now()
		from admin_riwayat_ahli_waris_investor as a where a.hist_investor_id = _hist_id and a.investor_id = _investor_id;
	else
		update 	ahli_waris_investor a,
				admin_riwayat_ahli_waris_investor b
		set
			a.id_investor = b.investor_id,
			a.nama_ahli_waris = b.nama_ahli_waris,
			a.hubungan_keluarga_ahli_waris = b.hubungan_keluarga_ahli_waris,
			a.nik_ahli_waris = b.nik_ahli_waris,
			a.kode_operator = b.kode_operator_ahli_waris,
			a.no_hp_ahli_waris = b.no_hp_ahli_waris,
			a.alamat_ahli_waris = b.alamat_ahli_waris,
			a.updated_at = now()
		where b.hist_investor_id = _hist_id
				and a.id_investor = _investor_id
				and a.id_investor = b.investor_id;	

	end if;
	#----------------------------update va number bni---------------------------
	
	select count(1), va_number into @flag_ , @va_number_
	from rekening_investor 
	where investor_id = _investor_id;
			
	if ( 0 = @flag_ ) then
		select count(1), va_bank into @flag2_ , @va_bni_
		from admin_riwayat_nomor_va
		where 	hist_id = _hist_id
				and investor_id = _investor_id
				and kode_bank = '009';
				
		if ( 0 != @flag2_) then
			insert into rekening_investor
			(
				investor_id,
				va_number,
				total_dana,
				unallocated,
				payment_ntb,
				kode_bank,
				type,
				status,
				created_at,
				updated_at
			)
			values
			(
				_investor_id,
				@va_bni_,
				0,
				0,
				null,
				'009',
				'V',
				'A',
				now(),
				null
			);
		end if;
	
	else	
		update 	rekening_investor a,
					admin_riwayat_nomor_va b
		set
			a.va_number = b.va_bank,
			a.updated_at = now()
		where b.hist_id = _hist_id
				and a.investor_id = _investor_id
				and a.investor_id = b.investor_id
				and b.kode_bank = '009';	
	end if;
	
	#---------------va niaga & va bsi--------------------------------
	
	select count(1), va_number into @flag_ , @va_cimb_
	from detil_rekening_investor 
	where 	investor_id = _investor_id
			and kode_bank = '022';
			
	if ( 0 = @flag_ ) then
		select count(1), va_bank into @flag2_ , @va_cimb_
		from admin_riwayat_nomor_va
		where 	hist_id = _hist_id
				and investor_id = _investor_id
				and kode_bank = '022';
			
		if ( 0 != @flag2_) then
			insert into detil_rekening_investor
			(
				investor_id,
				va_number,
				last_amount,
				last_payment_ntb,
				kode_bank,
				type,
				status,
				created_at,
				updated_at
			)
			values
			(
				_investor_id,
				@va_cimb_,
				0,
				null,
				'022',
				'V',
				'A',
				now(),
				null
			);		
		end if;
			
	
	else
		update 	detil_rekening_investor a,
					admin_riwayat_nomor_va b
		set
			a.va_number = b.va_bank,
			a.updated_at = now()
		where b.hist_id = _hist_id
				and a.investor_id = _investor_id
				and a.investor_id = b.investor_id
				and a.kode_bank = '022'
				and b.kode_bank = '022';
	end if;
			
			
			
	select count(1), va_number into @flag_ , @va_bsi_
	from detil_rekening_investor 
	where 	investor_id = _investor_id
			and kode_bank = '451';
			
	if ( 0 = @flag_ ) then
		select count(1), va_bank into @flag2_ , @va_bsi_
		from admin_riwayat_nomor_va
		where 	hist_id = _hist_id
				and investor_id = _investor_id
				and kode_bank = '451';
			
		if ( 0 != @flag2_) then
			insert into detil_rekening_investor
			(
				investor_id,
				va_number,
				last_amount,
				last_payment_ntb,
				kode_bank,
				type,
				status,
				created_at,
				updated_at
			)
			values
			(
				_investor_id,
				@va_bsi_,
				0,
				null,
				'451',
				'V',
				'A',
				now(),
				null
			);		
		end if;
	else		
		update 	detil_rekening_investor a,
					admin_riwayat_nomor_va b
		set
			a.va_number = b.va_bank,
			a.updated_at = now()
		where b.hist_id = _hist_id
				and a.investor_id = _investor_id
				and a.investor_id = b.investor_id
				and a.kode_bank = '451'
				and b.kode_bank = '451';
	end if;	
	#----------------------------update request list investor-------------------------
	
	update admin_permintaan_perubahan
	set 
		approve_by = _approve_by,
		approve_date = now(),
		status = 2
	where hist_id = _hist_id
			and client_id = _investor_id;
	
	
	set sout = '1';							
	select sout ; 
	commit;
	
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_update_rek_investor
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_update_rek_investor`(
	IN `_investor_id` int,
	IN `_vanumber` VARCHAR(50),
	IN `_topup` decimal(15,2),
	IN `_ntb` CHAR(50),
	IN `_kodebank` VARCHAR(3),
	IN `_file` varchar(100),
	IN `_line` int







)
BEGIN

	DECLARE code char(5) default '00000';
  	DECLARE msg text;
  	DECLARE nrows int;
  	DECLARE cur_date bigint;
  	DECLARE cur_date2 bigint;
  	DECLARE trx_date_ bigint;
	DECLARE sout varchar(1000) default '';	
	
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
	    ROLLBACK;
	END;
	
	START TRANSACTION;
	
	SET sout = '1';
	IF ( 0 > _investor_id OR NULL = _investor_id ) THEN
		SET sout = concat(sout,'proc_update_rek_investor: Invalid investor id = ',_investor_id);
		INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (_file,_line,sout,now());
	END IF;
	
	IF ( 0 > _topup OR NULL = _topup ) THEN
		SET sout = concat(sout,'proc_update_rek_investor: Nilai top up harus lebih dari 0');
		INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (_file,_line,sout,now());
	END IF;
	
	#----------------------------update request list investor-------------------------
	
	SET cur_date = date_format(now(),'%Y%m%d');
	SET cur_date2 = date_format(date_sub(now(), interval 1 day),'%Y%m%d');
	SET @ntb_ = 0;
	SET trx_date_ = 0;
	
	SELECT trx_date,ntb INTO trx_date_,@ntb_ FROM hist_ntb		
	WHERE trim(ntb) = trim(_ntb) AND (trim(trx_date) = cur_date OR trim(trx_date) = cur_date2);
								
	IF (0 != @ntb_) THEN
		SET sout = CONCAT("Payment NTB double va=",_vanumber ,", ntb=",@ntb_,", nominal=",_topup," sudah dikirim tanggal ", trx_date_);
		INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (_file,_line,sout,now());
	END IF;
	
	IF(sout = '1') THEN
		IF (_kodebank = '427') THEN
			UPDATE rekening_investor
			SET 
				unallocated = unallocated + _topup,
				total_dana = total_dana + _topup,
				payment_ntb = _ntb,
				updated_at = NOW()
			WHERE investor_id = _investor_id;
		ELSEIF (_kodebank = '451') THEN
			UPDATE 
			rekening_investor a,
			detil_rekening_investor b 
			SET a.unallocated = unallocated + _topup,
				 a.total_dana = total_dana + _topup,
				 b.last_amount = _topup,
				 b.last_payment_ntb = _ntb,
				 b.updated_at = NOW()
			WHERE b.va_number = _vanumber
			AND a.investor_id = b.investor_id;
		END IF;
				
		INSERT INTO hist_ntb (trx_date,ntb,kode_bank) VALUES(date_format(NOW(),'%Y%m%d'), _ntb,_kodebank);
		INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (_file,_line,CONCAT("Success Bank Response va=",_vanumber ,", ntb=",_ntb,", nominal=",_topup," tanggal=", cur_date),now());
	END IF;
									
	SELECT sout ; 
	
	COMMIT;

END//
DELIMITER ;

-- Dumping structure for procedure danasyariah.proc_val_pendanaan_proyek
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `proc_val_pendanaan_proyek`(
		_channel int, #1=lender, 2=admin ,3=mobile
      _investor_id int,
      _proyek_id int,
		_pack_amount int ,
		_param_date varchar(20), #yyyymmdd
		file_ varchar(100),
		line_ int
)
proc_val_pendanaan:begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
  	declare avail_fund decimal(15,2);
	declare minimum_fund decimal(15,2) default 1000000;
	declare sout varchar(1000) default '';	
	declare sout2 varchar(1000) default '';	
			
	declare exit handler for sqlexception
     begin
       rollback;
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('Pendanaan Gagal karena failed, error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (file_,line_,sout,now());
       select sout;
     end;
   
   set autocommit = 0;
   #--------------------------------validasi parameter input ---------------------------------------
   if ( 3 < _channel or 1 > _channel or 0 > _investor_id or 0 > _proyek_id or 0 > _pack_amount or _param_date is null 
		  or trim(_param_date) = '' ) then
   	set sout = '-1';
   	#insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'proc val pendanaan: parameter error',now());
   	set sout = concat(sout,'Proc val pendanaan: parameter error','#');	
   	
   	select sout;
   	leave proc_val_pendanaan;
	end if;
	
	#----------------------------validasi unallocated dan penarikan-------------------------
	select count(1), sum(jumlah) into @flag_ , @penarikan_
	from penarikan_dana 
	where accepted = 0
			and investor_id = _investor_id;
			
	if (0 = @flag_) then
		set @penarikan_ = 0;
	end if;
						
	select count(1), sum(unallocated) into @flag_ , @unallocated_
	from rekening_investor
	where investor_id = _investor_id;
	
	if (0 = @flag_) then
		set sout = concat(sout,'pendanaan gagal , tidak ditemukan alokasi dana utk investor_id = ', _investor_id,'#');	
		select sout ;
		leave proc_val_pendanaan;
	end if;
	
						
	if ( @unallocated_ - @penarikan_ < _pack_amount * minimum_fund ) then
	   if (0 < @penarikan_ ) then
			set sout = concat(sout,'pendanaan gagal , saldo ( Unallocated - penarikan ) tersisa ', three_digit_format(cast((@unallocated_ - @penarikan_) as decimal(15))),' dan masih dalam proses penarikan ',three_digit_format(cast(@penarikan_ as decimal(15))) ,'#');	
		else
			set sout = concat(sout,'pendanaan gagal , saldo Unallocated tersisa ', three_digit_format(cast((@unallocated_) as decimal(15))) ,'#');	
		end if;
		select sout ;
		leave proc_val_pendanaan;
	end if;
	#-----------------------------------validasi anggaran dan dana terkumpul --------------------
	select count(1), total_need, terkumpul into @flag_ , @total_need_ ,@terkumpul_
	from proyek 
	where id = _proyek_id
			and status_rekap != 1
			and status != 2 
	for update;
			
	if 0 = @flag_ then
	   set sout = concat(sout,'Pendanaan gagal , proyek tersebut tidak ada atau sudah berakhir','#');	
	   select sout ; 
	   leave proc_val_pendanaan;
	end if;
	
	select count(1), sum(total_dana) into @flag_ , @total_dana_ 
	from pendanaan_aktif 
	where  proyek_id = _proyek_id
	for update;
	
	if 0 = @flag_ then
	   set @total_dana_ = 0;
	end if;
	
	set avail_fund = @total_need_ - @total_dana_ - @terkumpul_ ;
	set sout2 = concat('investor_id = ',_investor_id ,' @total_need_ = ',three_digit_format(@total_need_) ,' @total_dana_ = ',three_digit_format(@total_dana_) ,' @terkumpul_ = ', three_digit_format(@terkumpul_) ,' paket = ', _pack_amount ,' _param_date = ', _param_date );
	insert into db_app_log (file_name,line,description,created_at) values (file_,line_,sout2,now());
		
	if ( avail_fund >= _pack_amount * minimum_fund ) then
		
		#start transaction;
		select count(1), max(a.id) into @flag_ , @fund_id_
		from 	pendanaan_aktif a,
				proyek b
		where a.proyek_id = b.id			
				and a.total_dana > 0
				and a.investor_id = _investor_id
				and a.proyek_id = _proyek_id
				and (date_format(a.created_at,'%Y%m%d') = date_format(now(),'%Y%m%d') 
						or date_format(a.updated_at,'%Y%m%d') = date_format(now(),'%Y%m%d') );
			
		if ( 0 != @flag_ ) then
				update pendanaan_aktif 
				set 	total_dana = total_dana + (_pack_amount * minimum_fund),
						nominal_awal = nominal_awal + (_pack_amount * minimum_fund),
						#created_at = date_format(_param_date,'%Y-%m-%d'),
						updated_at = now()
				where id = @fund_id_ ;
				
				if ( 1 = _channel ) then
					insert into log_pendanaan (pendanaanAktif_id, nominal, tipe, created_at,updated_at)
					values(@fund_id_ , _pack_amount * minimum_fund, 'add active investation', null, now());	
				elseif ( 2 = _channel ) then
					insert into log_pendanaan (pendanaanAktif_id, nominal, tipe, created_at,updated_at)
					values(@fund_id_ , _pack_amount * minimum_fund, 'add active investation by admin', null, now());
				else 
					insert into log_pendanaan (pendanaanAktif_id, nominal, tipe, created_at,updated_at)
					values(@fund_id_ , _pack_amount * minimum_fund, 'add active investation mobile', null, now());
				end if;				
		else
				insert into pendanaan_aktif (investor_id , proyek_id , total_dana , nominal_awal,
													  tanggal_invest,status, created_at, updated_at,last_pay)
								values(_investor_id,_proyek_id, _pack_amount * minimum_fund,_pack_amount * minimum_fund,
										 date_format(_param_date,'%Y-%m-%d'),1,now(),null,null);
				
				
				select count(1), max(id) into @flag_ , @fund_id_
				from 	pendanaan_aktif		
				where investor_id = _investor_id
						and proyek_id = _proyek_id;
												 
				if ( 0 != @flag_ ) then
				
					if ( 1 = _channel ) then
						insert into log_pendanaan (pendanaanAktif_id, nominal, tipe, created_at,updated_at)
						values(@fund_id_ , _pack_amount * minimum_fund, 'add new investation', now(),null);	
					elseif ( 2 = _channel ) then
						insert into log_pendanaan (pendanaanAktif_id, nominal, tipe, created_at,updated_at)
						values(@fund_id_ , _pack_amount * minimum_fund, 'add new investation by admin', now(),null);
					else
						insert into log_pendanaan (pendanaanAktif_id, nominal, tipe, created_at,updated_at)
						values(@fund_id_ , _pack_amount * minimum_fund, 'add new investation mobile', now(),null);
					end if;
					
				end if;
				
		end if;
		
		update rekening_investor 
		set 	unallocated = unallocated - (_pack_amount * minimum_fund),
				updated_at = now()
		where investor_id = _investor_id;
		
		set sout = '1';
		
		commit;
	else
	   set avail_fund = round (avail_fund / minimum_fund);
	   if ( avail_fund < 0 ) then
	   	set avail_fund = 0 ;
	   end if;
	   set sout = concat(sout,'pendanaan gagal , sisa paket yg tersedia ',avail_fund,'#');	
	end if;
	
	select sout ; 
end//
DELIMITER ;

-- Dumping structure for procedure danasyariah.test_call
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` PROCEDURE `test_call`(
		_file varchar(100), 
		_line int
)
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
	declare res varchar(1000) default '0';
			
	declare exit handler for sqlexception
     begin
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('is_first_create failed , error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (_file,_line,sout,now());
     end;
       
  
	#--------------------------------------------------------------
	
	call check_first_create('brw_hd_analisa','pengajuan_id',133,'created_at_rac_dsi','test.php',199,@temp);
	select @temp ;

end//
DELIMITER ;

-- Dumping structure for function danasyariah.cek_tanggallibur
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `cek_tanggallibur`(`tanggal` DATE










) RETURNS varchar(111) CHARSET latin1
    DETERMINISTIC
BEGIN
declare jumlah int;
declare v_deskripsi varchar(111);
declare tanggalLama DATE;
declare tanggalBaru DATE;
declare keterangan varchar(111);
declare ketemu bool default 0;
declare v_out varchar(111);
	
	
		SET tanggalLama = tanggal;
		IF EXISTS (SELECT 1 FROM m_harilibur WHERE tgl_harilibur = tanggal) THEN
				SELECT deskripsi into v_deskripsi FROM m_harilibur WHERE tgl_harilibur = tanggal limit 1;
				SET keterangan = concat("<font color='red'>",v_deskripsi,"</font>");
			SET ketemu = 1;
			SET tanggal = tanggal + INTERVAL 1 DAY;
		ELSE
			SET ketemu = 0;
			SET keterangan = ' ';
			SET tanggalBaru = tanggal;
		END IF;
	
	
		WHILE ketemu = 1 DO
			IF EXISTS (SELECT 1 FROM m_harilibur WHERE tgl_harilibur = tanggal) THEN
				SET tanggal = tanggal + INTERVAL 1 DAY;
				SET ketemu = 1;
			ELSE
				SET tanggalBaru = tanggal;
				SET ketemu = 0;
			END IF;
		END WHILE;
	
		SET v_out = concat(keterangan,"|",tanggalBaru);

RETURN v_out;

END//
DELIMITER ;

-- Dumping structure for function danasyariah.dms_put_into_brw_pasangan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `dms_put_into_brw_pasangan`(`brw_id_` int(11),
	`nama_` varchar(25),
	`jenis_kelamin_` int(1),
	`ktp_` varchar(16),
	`tempat_lahir_` VARCHAR(50),
	`tgl_lahir_` date,
	`no_hp_` varchar(25),
	`agama_` int(1),
	`pendidikan_` int(1),
	`npwp_` varchar(15),
	`alamat_` VARCHAR(100),
	`provinsi_` VARCHAR(50),
	`kota_` varchar(50),
	`kecamatan_` varchar(50),
	`kelurahan_` varchar(50),
	`kode_pos_` varchar(5),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT,
	`no_kk_` VARCHAR(50)
) RETURNS int(11)
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_pasangan 
		(
			pasangan_id,
			nama,
			jenis_kelamin,
			ktp,
			tempat_lahir,
			tgl_lahir,
			no_hp,
			agama,
			pendidikan,
			npwp,
			alamat,
			provinsi,
			kota,
			kecamatan,
			kelurahan,
			kode_pos,
			created_at,
			updated_at,
			no_kk
		) VALUES (
			brw_id_ ,
			nama_ ,
			jenis_kelamin_ ,
			ktp_ ,
			tempat_lahir_ ,
			tgl_lahir_ ,
			no_hp_ ,
			agama_ ,
			pendidikan_ ,
			npwp_ ,
			alamat_ ,
			provinsi_ ,
			kota_ ,
			kecamatan_ ,
			kelurahan_ ,
			kode_pos_ ,
			created_at_ ,
			updated_at_,
			no_kk_ 
		)
		ON DUPLICATE KEY UPDATE 
			`pasangan_id`     = pasangan_id_,
			`nama`				= nama_,
			`jenis_kelamin`	= jenis_kelamin_,
			`ktp`					= ktp_,
			`tempat_lahir`		= tempat_lahir_,
			`tgl_lahir`			= tgl_lahir_,
			`no_hp`				= no_hp_,
			`agama`				= agama_,
			`pendidikan`		= pendidikan_,
			`npwp`				= npwp_,
			`alamat`				= alamat_,
			`provinsi`			= provinsi_,
			`kota`				= kota_,
			`kecamatan`			= kecamatan_,
			`kelurahan`			= kelurahan_,
			`kode_pos`			= kode_pos_,
			`created_at`		= created_at_,
			`updated_at`		= updated_at_,
			`no_kk`				= no_kk
		;
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN 222;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.dms_put_into_brw_pekerjaan_pasangan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `dms_put_into_brw_pekerjaan_pasangan`(`brw_id2` INT,
	`sumber_pengembalian_dana` INT,
	`nama_perusahaan` VARCHAR(50),
	`alamat_perusahaan` VARCHAR(50),
	`rt` VARCHAR(50),
	`rw` VARCHAR(50),
	`provinsi` VARCHAR(50),
	`kab_kota` VARCHAR(50),
	`kecamatan` VARCHAR(50),
	`kelurahan` VARCHAR(50),
	`kode_pos` VARCHAR(50),
	`no_telp` VARCHAR(50),
	`no_hp` VARCHAR(50),
	`surat_ijin` INT,
	`no_surat_ijin` VARCHAR(50),
	`bentuk_badan_usaha` INT,
	`status_pekerjaan` INT,
	`usia_perusahaan` VARCHAR(50),
	`usia_tempat_usaha` VARCHAR(50),
	`departemen` VARCHAR(50),
	`jabatan` VARCHAR(50),
	`masa_kerja_tahun` VARCHAR(50),
	`masa_kerja_bulan` VARCHAR(50),
	`nip_nrp_nik` VARCHAR(50),
	`nama_hrd` VARCHAR(50),
	`no_fixed_line_hrd` VARCHAR(50),
	`pengalaman_kerja_tahun` VARCHAR(50),
	`pengalaman_kerja_bulan` VARCHAR(50),
	`pendapatan_borrower` DECIMAL(10,0),
	`biaya_hidup` DECIMAL(10,0),
	`detail_penghasilan_lain_lain` VARCHAR(50),
	`total_penghasilan_lain_lain` DECIMAL(10,0),
	`nilai_spt` DECIMAL(10,0),
	`file_` VARCHAR(50),
	`line_` INT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   UPDATE brw_pasangan SET
   	
			`brw_id2` = brw_id2,
			`sumber_pengembalian_dana` = sumber_pengembalian_dana,
			`nama_perusahaan` = nama_perusahaan,
			`alamat_perusahaan` = alamat_perusahaan,
			`rt_pekerjaan_pasangan` = rt,
			`rw_pekerjaan_pasangan` = rw,
			`provinsi_pekerjaan_pasangan` = provinsi,
			`kab_kota_pekerjaan_pasangan` = kab_kota,
			`kecamatan_pekerjaan_pasangan` = kecamatan,
			`kelurahan_pekerjaan_pasangan` = kelurahan,
			`kode_pos_pekerjaan_pasangan` = kode_pos,
			`no_telp_pekerjaan_pasangan` = no_telp,
			`no_hp_pekerjaan_pasangan` = no_hp,
			`surat_ijin` = surat_ijin,
			`no_surat_ijin` = no_surat_ijin,
			`bentuk_badan_usaha` = bentuk_badan_usaha,
			`status_pekerjaan` = status_pekerjaan,
			`usia_perusahaan` = usia_perusahaan,
			`usia_tempat_usaha` = usia_tempat_usaha,
			`departemen` = departemen,
			`jabatan` = jabatan,
			`masa_kerja_tahun` = masa_kerja_tahun,
			`masa_kerja_bulan` = masa_kerja_bulan,
			`nip_nrp_nik` = nip_nrp_nik,
			`nama_hrd` = nama_hrd,
			`no_fixed_line_hrd` = no_fixed_line_hrd,
			`pengalaman_kerja_tahun` = pengalaman_kerja_tahun,
			`pengalaman_kerja_bulan` = pengalaman_kerja_bulan,
			`pendapatan_borrower` = pendapatan_borrower,
			`biaya_hidup` = biaya_hidup,
			`detail_penghasilan_lain_lain` = detail_penghasilan_lain_lain,
			`total_penghasilan_lain_lain` = total_penghasilan_lain_lain,
			`nilai_spt` = nilai_spt
	WHERE pasangan_id = brw_id2;
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.dms_put_into_brw_rekening
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `dms_put_into_brw_rekening`(`brw_id_` int(11),
	`va_number_` varchar(25),
	`brw_norek_` varchar(25),
	`brw_nm_pemilik_` varchar(50),
	`brw_kd_bank_` varchar(5),
	`total_plafon_` decimal(15,2),
	`total_terpakai_` decimal(15,2),
	`total_sisa_` decimal(15,2),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT,
	`kantor_cabang_pembuka_` VARCHAR(50)
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_rekening 
		   	(
				brw_id,
				va_number,
				brw_norek,
				brw_nm_pemilik,
				brw_kd_bank,
				total_plafon,
				total_terpakai,
				total_sisa,
				created_at,
				updated_at,
				kantor_cabang_pembuka)
				values
				(
				brw_id_ ,
				va_number_ ,
				brw_norek_ ,
				brw_nm_pemilik_ ,
				brw_kd_bank_ ,
				total_plafon_ ,
				total_terpakai_ ,
				total_sisa_ ,
				created_at_ ,
				updated_at_,
				kantor_cabang_pembuka_);
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.func_cekpembayaranke
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `func_cekpembayaranke`(`listimbaluser_id` INT,
	`tglpayout` DATE,
	`flag` INT
) RETURNS varchar(300) CHARSET latin1
    DETERMINISTIC
BEGIN
declare v_out text;
declare v_detilimbaluserid int;
declare v_out23 varchar(300);
declare namaproyek varchar(191);
declare investorid int(10);
declare v_unallocated int(50);
declare jml INT(3);
declare ketemu bool default 0;
declare rowCountDescription int default 1;
DECLARE updatePA CURSOR FOR
	SELECT id from ih_list_imbal_user where detilimbaluser_id = v_detilimbaluserid;

	SELECT a.detilimbaluser_id into v_detilimbaluserid from ih_list_imbal_user a join ih_detil_imbal_user b ON b.id = a.detilimbaluser_id where a.id = listimbaluser_id;
	select a.nama into namaproyek from proyek a join ih_detil_imbal_user b ON b.proyek_id = a.id where b.id = v_detilimbaluserid;
	OPEN updatePA;
	BEGIN
		DECLARE exit_flag INT DEFAULT 0;
		DECLARE LIU_id INT(10);
		DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET exit_flag = 1;
		--
		updatePALoop: LOOP
		FETCH updatePA INTO LIU_id;
		IF exit_flag THEN LEAVE updatePALoop;
		END IF;
		 	IF EXISTS (SELECT 1 FROM ih_list_imbal_user WHERE id = LIU_id AND tanggal_payout = tglpayout and keterangan_payout = flag ) THEN
				SET ketemu = 1;
				IF flag = 2 THEN 
					SET v_out23 = "SISA IH";
				ELSEIF flag = 3 THEN
				 	SET v_out23 = "DANA POKOK";
				ELSE
					SET v_out23 = concat("IMBAL HASIL BULAN KE ",rowCountDescription);
				END IF;
			ELSE
				SET ketemu = 0;
			END IF;
		
		SET rowCountDescription = rowCountDescription + 1;
		END LOOP;
	END;
	CLOSE updatePA;

	SET v_out = concat(v_out23," : ",namaproyek);

RETURN v_out;

END//
DELIMITER ;

-- Dumping structure for function danasyariah.func_check_threshold
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `func_check_threshold`(investor_id_ int(11)
) RETURNS int(11)
    DETERMINISTIC
BEGIN
	
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;
    DECLARE v_total_dana_ decimal(15,2);
  	DECLARE v_threshold_kontrak	decimal(15,2);
  	DECLARE v_return INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;

	SELECT total_dana into v_total_dana_ FROM rekening_investor where investor_id = investor_id_;
	SELECT threshold_kontrak into v_threshold_kontrak FROM threshold_kontrak where tipe = 1 ORDER BY id_threshold DESC LIMIT 1;

	IF v_total_dana_ >= v_threshold_kontrak THEN
		SET v_return = 1;
	ELSE
		SET v_return = 0;
	END IF;



	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  	ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   	END IF;
   
   	RETURN v_return;	
	
END//
DELIMITER ;

-- Dumping structure for function danasyariah.func_generate_serial_number
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `func_generate_serial_number`(inv_id int,
	file_ VARCHAR(100),
	line_ INT
) RETURNS varchar(100) CHARSET latin1
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

  	DECLARE dt_trx  VARCHAR(10);
  	DECLARE nom_change INT;
  	DECLARE tdana_rekening DECIMAL(15,2);
  	DECLARE nom_tdana VARCHAR(4);
  	DECLARE pjg_nom_tdana VARCHAR(2);
  	DECLARE pjg_nom_change VARCHAR(2);
	DECLARE vnom_change VARCHAR(4);
	DECLARE date_change VARCHAR(20);
	DECLARE no_seri VARCHAR(40);
	
	DECLARE exit HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	    SET result = CONCAT('failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;

	SET res = -1;

	SELECT id,nominal,DATE_FORMAT(updated_at,'%y%m%d%H%i') INTO dt_trx, nom_change, date_change
	FROM mutasi_investor 
	WHERE investor_id = inv_id 
	AND perihal IN ('Transfer Rekening' , 'Penarikan dana selesai') 
	ORDER BY id DESC
	LIMIT 1;
	
	SELECT total_dana INTO tdana_rekening
	FROM rekening_investor
	WHERE investor_id = inv_id;
	
  
		SET dt_trx = LPAD(dt_trx, 8, "0");
		SET pjg_nom_tdana = LENGTH(tdana_rekening)-4;
		SET nom_tdana = concat(SUBSTR(tdana_rekening,1,2),CASE WHEN LENGTH(pjg_nom_tdana)=1 THEN CONCAT('0',pjg_nom_tdana) ELSE pjg_nom_tdana END);
		SET pjg_nom_change = LENGTH(nom_change)-1;
		SET vnom_change = CONCAT(SUBSTR(nom_change,1,2),CASE WHEN LENGTH(pjg_nom_change)=1 THEN CONCAT('0',pjg_nom_change) ELSE pjg_nom_change END);
		SET no_seri = CONCAT(dt_trx,nom_tdana,vnom_change);
		RETURN CONCAT(no_seri,"/L-",inv_id,"/",date_change);
		-- exp : 0003129185095008/L-33016/2006111421
END//
DELIMITER ;

-- Dumping structure for function danasyariah.func_getUnallocated
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `func_getUnallocated`(`invid` INT
) RETURNS int(50)
    DETERMINISTIC
BEGIN

DECLARE v_unallocated DECIMAL(20,2);
DECLARE inv INT(50);
		SELECT unallocated into v_unallocated FROM rekening_investor WHERE investor_id = invid;
		
		return v_unallocated;
END//
DELIMITER ;

-- Dumping structure for function danasyariah.func_splitstr
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `func_splitstr`(`x` VARCHAR(20000),
	`delim` VARCHAR(5),
	`pos` INT
) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
	
	RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
	       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
	       delim, '');

END//
DELIMITER ;

-- Dumping structure for function danasyariah.get_new_simulation_kpr
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `get_new_simulation_kpr`(_tot_pinjaman float ,
		_tenor int,
		_pctg_margin float ,
		file_ varchar(100),
		line_ int
) RETURNS varchar(10000) CHARSET latin1
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
  	declare result text;
  	declare res int default 1;
	declare b_out	int;
	declare max_tenor int;
	declare skey_val varchar(15) default '';
	declare counter int;
	declare a,b,c, d,e,f,g float;
	declare margin_dsi float default 0;
	declare var_pinjaman float default 0;
	declare min_pinjaman float default 100000000;
	declare max_pinjaman float default 2000000000;
	declare sout varchar(10000) default '';	
			
	 
	declare exit handler for sqlexception
    begin
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('failed, error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (file_,line_,sout,now());
    end;
     
	set b_out = 0;
	set max_tenor = 180; 
	
	if (_tot_pinjaman < min_pinjaman) or (_tot_pinjaman > max_pinjaman) then 
	   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'nominal minimal 100 juta dan maks 2 m',now());
	   set b_out = 1;
	end if;
	 	 
	if (_tenor < 0 ) or (_tenor > max_tenor) then 
	   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'tenor nggak boleh minus dan maksimal 180 bulan',now());
	   set b_out = 1;
	end if;
	
	if (_pctg_margin < 0 ) then 
	   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'margin nggak boleh minus',now());
	   set b_out = 1;
	end if;
	 	 
	 if (0 = b_out) then
		set counter = 0;
		set a = 1 + _pctg_margin / (12 * 100);
		set c = a;
		
		while ( counter < _tenor - 1 ) do
			set c = c * a;
			set counter = counter + 1;
		end while;
		
		set d = 1 - ( 1 / c) ;
		set b = (_tot_pinjaman * _pctg_margin / (12 * 100)) ;
		set f = b / d ;
		set g = b / f;		
		
		set skey_val = concat(_tot_pinjaman,_tenor);
		
		set var_pinjaman = _tot_pinjaman;
		set counter = 0;
		while ( counter < _tenor - 1 ) do
			set margin_dsi = (var_pinjaman * f * g) / _tot_pinjaman;
			set var_pinjaman = var_pinjaman - f + margin_dsi;
			set counter = counter + 1;
			#insert into temp_pinjaman values (counter, skey_val,var_pinjaman,margin_dsi, f - margin_dsi);
			set sout = concat(sout,counter,';', f,';', f,';', f - margin_dsi,';',margin_dsi,';', var_pinjaman, '#');
		end while;
		 							
		#set sout = concat(sout, _tot_pinjaman,';',f, '#');

	 end if; #(0 = b_out)				
	 	  
	 #set sout = concat(sout, tot_pembayaran,'^');	
	 	  
	 return sout;  
end//
DELIMITER ;

-- Dumping structure for function danasyariah.get_simulation_kpr
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `get_simulation_kpr`(`_tot_pinjaman` float ,
	`_tenor` int,
	`file_` varchar(100),
	`line_` int

) RETURNS varchar(200) CHARSET latin1
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sequen_ int;
	declare period_ int;
	declare margin_efektif_  float;
	declare penurunan_pokok_ float;
	declare done int;
	declare b_out	int;
	declare max_period	int;
	declare counter int;
	declare tot float;
	declare counter2 int;
	declare max_loop int;
	declare last_balance float;
	declare tot_max_period float;
	declare angsuran_pokok float;
	declare prev_period int default 0;
	declare periode int default 0;
	declare start_month int default 0;
	declare end_month int default 0;
	declare pctg_margin float ;
	declare angsuran_margin float;
	declare tot_angsuran float;
	declare tot_bayar float;
	declare tot_pembayaran float default 0;
	declare min_pinjaman float default 100000000;
	declare max_pinjaman float default 2000000000;
	declare sout varchar(200) default '';	
			
	declare c_menu cursor for
    	select sequen, period,  margin_efektif, penurunan_pokok from m_param_kalkulator
		where tenor = _tenor
		order by sequen asc;
	
	declare continue handler for not found set done =1 ;

	declare exit handler for sqlexception
     begin
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('failed, error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (file_,line_,sout,now());
     end;
     
	set b_out = 0;
	set done = 0;
	set counter = 0;
	set max_loop = 5; 
	set last_balance = _tot_pinjaman ;
	set tot_max_period = 0;
	
	if (_tot_pinjaman < min_pinjaman) or (_tot_pinjaman > max_pinjaman) then 
	   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'nominal minimal 100 juta dan maks 2 m',now());
	   set b_out = 1;
	end if;

	 open c_menu;
	 while (0 = b_out and counter < max_loop) do
		  	fetch c_menu into sequen_ , period_ ,  margin_efektif_ , penurunan_pokok_; 
			
			if sequen_ < 1 or sequen_ > 3 then 
			   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'sequen salah',now());
			   set done = 1;
			end if;
			
			if period_ < 2 or period_ > 11 then 
			   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'period salah',now());
			   set done = 1;
			end if;
			
			if margin_efektif_ <= 0 or margin_efektif_ >= 1 then 
			   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'margin_efektif salah',now());
			   set done = 1;
			end if;
			
			if penurunan_pokok_ <= 0 or penurunan_pokok_ >= 1 then 
			   insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'penurunan_pokok salah',now());
			   set done = 1;
			end if;
			
			if 1 = done then
				set b_out = 1;
			else
				
				set tot = 0;
				set counter2 = 0;
				set max_period = period_ * 12 ;
				set angsuran_pokok = _tot_pinjaman * penurunan_pokok_ / max_period ;
							
				while (counter2 < max_period ) do
						set tot = tot +  (last_balance - angsuran_pokok * counter2) ;
						set counter2 = counter2 + 1;
				end while; #(counter2 < max_period)
				
				set last_balance = last_balance - angsuran_pokok * counter2 ;
				set margin_efektif_ = margin_efektif_ / 12;
				set tot = tot * margin_efektif_;
				set tot_max_period = tot_max_period + tot;
								
				set counter = counter + 1;
				
			end if;
			
	 end while; #(0 = b_out and counter < max_loop)
	 close c_menu;
	 
	 
	 if _tot_pinjaman > 0 then
	   set pctg_margin = tot_max_period / _tot_pinjaman ;
	 end if; 
	 
	 if (_tot_pinjaman < min_pinjaman) or (_tot_pinjaman > max_pinjaman) then 
	   set b_out = 1;
	 else
	   set b_out = 0;
	 end if;	 
	 set done = 0;
	 set counter = 0;
	 	 
	 open c_menu;
	 while (0 = b_out and counter < max_loop) do
		  	fetch c_menu into sequen_ , period_ ,  margin_efektif_ , penurunan_pokok_; 
						
			if 1 = done then
				set b_out = 1;
			else
				set max_period = period_ * 12 ;
				set periode = periode + period_ ;
				set angsuran_pokok = _tot_pinjaman * penurunan_pokok_ / max_period ;
				set angsuran_margin = angsuran_pokok * pctg_margin;
				set tot_angsuran = angsuran_pokok + angsuran_margin ;
				set start_month = prev_period * 12 + 1;
				set end_month = periode * 12;
				set tot_bayar = (end_month - start_month + 1) * tot_angsuran;
				set tot_pembayaran = tot_pembayaran + tot_bayar;
				
				set prev_period = prev_period + period_ ;
				set counter = counter + 1;
				
				set sout = concat(sout, start_month,'-',end_month,';',tot_angsuran,';',tot_bayar,';', angsuran_pokok, ';' ,angsuran_margin, '#');	
					
			end if;
	 end while; #(0 = b_out and counter < max_loop)				
	 close c_menu;
	 
	 set sout = concat(sout, tot_pembayaran,'^');	
	 	  
	 return sout; 
END//
DELIMITER ;

-- Dumping structure for function danasyariah.get_tagihan_jatuh_tempo
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `get_tagihan_jatuh_tempo`(`brw_id_` int,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	DECIMAL(15,2) DEFAULT 0 ;

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	     SET result = CONCAT('failed, error = ',code,', message = ',msg);
        -- SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;

	-- SET @var1 = -1;
	
  	SELECT SUM(nominal_tagihan_perbulan - IFNULL(nominal_transfer_tagihan,0)) tagihan_jatuh_tempo INTO @var1 FROM brw_invoice -- 1.paid  4.open  6.reject ;  2.unpaid  3.partial 5.pending
	WHERE STATUS IN (3,4) 
			AND brw_id = brw_id_
			AND DATE_FORMAT(tgl_jatuh_tempo,"%Y%m%d") <= DATE_FORMAT(NOW(),"%Y%m%d") ;
	
	if  @var1 IS not NULL then
		SET res = @var1;
	END if;
		
	RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.get_tot_pinjaman
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `get_tot_pinjaman`(`brw_id_` int,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	DECIMAL(15,2);

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	     SET result = CONCAT('failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;

	SET @var1 = 0;
	SET @var2 = 0;
	
   -- Total pinjaman
	SELECT sum(IFNULL(nominal_pencairan,0)) + SUM(IFNULL(nominal_imbal_hasil,0)) total_pinjaman  
	INTO @var1
	FROM 	brw_pencairan a  -- 1 request 2 cair
	WHERE a.STATUS = 2
			AND a.brw_id = brw_id_
			AND a.pencairan_id IN  (SELECT DISTINCT pencairan_id FROM brw_invoice WHERE STATUS IN(3,4) AND brw_id = brw_id_)
	GROUP BY brw_id ;
	
	-- Total pembayaran 
	SELECT SUM(IFNULL(nominal_transfer_tagihan,0)) total_pembayaran 
	INTO @var2
	FROM brw_invoice -- 1.paid  4.open  6.reject ;  2.unpaid  3.partial 5.pending
	WHERE	brw_id = brw_id_ 
			AND STATUS IN (1,3)
			and pencairan_id IN  (SELECT DISTINCT pencairan_id FROM brw_invoice WHERE STATUS IN(3,4) AND brw_id = brw_id_)
	GROUP BY brw_id;
	
	if (@var1 is not NULL) AND (@var2 is not NULL) then
		if @var1 > @var2 then
				SET res = @var1 - @var2;
		 else
		   	SET res = 0;
		 END if;
	else
		if @var2 IS NULL then
			SET res = @var1;
		else
			SET res = 0;
		END if;
	END if;
		
	RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.get_tot_pokok
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `get_tot_pokok`(`brw_id_` int,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS decimal(15,2)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	DECIMAL(15,2);

	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
	     SET result = CONCAT('failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
     END;

	SET @var1 = 0;
	SET @var2 = 0;
	
   -- Total pokok 
	SELECT sum(IFNULL(nominal_pencairan,0)) total_pinjaman  
	INTO @var1
	FROM 	brw_pencairan a  -- 1 request 2 cair
	WHERE a.STATUS = 2
			AND a.brw_id = brw_id_
			AND a.pencairan_id IN  (SELECT DISTINCT pencairan_id FROM brw_invoice WHERE STATUS IN (3,4) AND brw_id = brw_id_)
	GROUP BY brw_id ;
	
	-- Total pembayaran 
	SELECT SUM(IFNULL(nominal_transfer_tagihan,0)) total_pembayaran 
	INTO @var2
	FROM brw_invoice -- 1.paid  4.open  6.reject ;  2.unpaid  3.partial 5.pending
	WHERE	brw_id = brw_id_ 
			AND STATUS IN (1,3)
			and pencairan_id IN  (SELECT DISTINCT pencairan_id FROM brw_invoice WHERE STATUS IN (3,4) AND brw_id = brw_id_)
	GROUP BY brw_id;
	
	if (@var1 is not NULL) AND (@var2 is not NULL) then
		if @var1 > @var2 then
				SET res = @var1 - @var2;
		 else
		   	SET res = 0;
		 END if;
	else
		if @var2 IS NULL then
			SET res = @var1;
		else
			SET res = 0;
		END if;
	END if;
		
	RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_ahli_waris
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_ahli_waris`(`brw_id_` int(11),
	`nama_ahli_waris_` varchar(50),
	`hub_ahli_waris_` INT,
	`jenis_kelamin_` INT,
	`nik_` varchar(25),
	`tmpt_lahir_` VARCHAR(50),
	`tgl_lahir_` DATE,
	`no_tlp_` VARCHAR(20),
	`agama_` INT,
	`pendidikan_` INT,
	`provinsi_` VARCHAR(50),
	`kota_` VARCHAR(50),
	`kecamatan_` VARCHAR(50),
	`kelurahan_` VARCHAR(50),
	`kd_pos_` VARCHAR(5),
	`alamat_` VARCHAR(100),
	`created_at_` datetime,
	`updated_at_` DATETIME,
	`file_` VARCHAR(100),
	`line_` INT

) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_ahli_waris 
		   	(
				brw_id,
				nama_ahli_waris,
				hub_ahli_waris,
				jenis_kelamin,
				nik,
				tmpt_lahir,
				tgl_lahir,
				no_tlp,
				agama,
				pendidikan,
				alamat,
				provinsi,
				kota,
				kecamatan,
				kelurahan,
				kd_pos,
				created_at,
				updated_at)
				values
				(
				brw_id_,
				nama_ahli_waris_,
				hub_ahli_waris_,
				jenis_kelamin_,
				nik_,
				tmpt_lahir_,
				tgl_lahir_,
				no_tlp_,
				agama_,
				pendidikan_,
				alamat_,
				provinsi_,
				kota_,
				kecamatan_,
				kelurahan_,
				kd_pos_,
				created_at_ ,
				updated_at_ );
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_jaminan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_jaminan`(`pendanaan_id_` int(11),
	`jaminan_nama_` varchar(50),
	`jaminan_nomor_` varchar(50),
	`jaminan_jenis_` int(1),
	`jaminan_nilai_` decimal(15,2),
	`jaminan_detail_` varchar(50),
	`kantor_penerbit_` VARCHAR(50),
	`NOP_` CHAR(20),
	`sertifikat_` VARCHAR(200),
	`status_` int(1),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT

) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_jaminan 
		   	(
				pengajuan_id,
				jaminan_nama,
				jaminan_nomor,
				jaminan_jenis,
				jaminan_nilai,
				jaminan_detail,
				kantor_penerbit,
				NOP,
				sertifikat,
				status,
				created_at,
				updated_at)
				values
				(
				pendanaan_id_,
				jaminan_nama_,
				jaminan_nomor_,
				jaminan_jenis_,
				jaminan_nilai_,
				jaminan_detail_,
				kantor_penerbit_,
				NOP_,
				sertifikat_,
				status_,
				created_at_ ,
				updated_at_);
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_log_pengajuan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_log_pengajuan`(`pengajuan_id_` int(11),
	`brw_id_` int(11),
	`status_` int(1),
	`keterangan_` varchar(50),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_log_pengajuan 
		   	(
				pengajuan_id,
				brw_id,
				status,
				keterangan,
				created_at,
				updated_at)
				values
				(
				pengajuan_id_ ,
				brw_id_ ,
				status_ ,
				keterangan_ ,
				created_at_ ,
				updated_at_ );
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_m_no_invoice
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_m_no_invoice`(`in_invoice_id` INT(11),
	`in_brw_id` INT(11)
) RETURNS varchar(50) CHARSET latin1
    DETERMINISTIC
BEGIN
	
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;
   	DECLARE v_temp_no_invoice VARCHAR(5);
    DECLARE v_count_bulan INT(5);
 	DECLARE v_no_invoice VARCHAR(5);
  	DECLARE v_bulan VARCHAR(5);
  	DECLARE v_tahun VARCHAR(5);


   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;

	SELECT EXTRACT(MONTH from tgl_jatuh_tempo) into v_bulan FROM brw_invoice where invoice_id = in_invoice_id;
	SELECT EXTRACT(YEAR from tgl_jatuh_tempo) into v_tahun FROM brw_invoice where invoice_id = in_invoice_id;

	CASE
	    WHEN v_bulan = 1 THEN SET v_bulan = "I";
	    WHEN v_bulan = 2 THEN SET v_bulan = "II";
	    WHEN v_bulan = 3 THEN SET v_bulan = "III";
	    WHEN v_bulan = 4 THEN SET v_bulan = "IV";
	    WHEN v_bulan = 5 THEN SET v_bulan = "V";
	    WHEN v_bulan = 6 THEN SET v_bulan = "VI";
	    WHEN v_bulan = 7 THEN SET v_bulan = "VII";
	    WHEN v_bulan = 8 THEN SET v_bulan = "VIII";
	    WHEN v_bulan = 9 THEN SET v_bulan = "IX";
	    WHEN v_bulan = 10 THEN SET v_bulan = "X";
	    WHEN v_bulan = 11 THEN SET v_bulan = "XI";
	    ELSE SET v_bulan ="XII";
	END CASE;

	SELECT IFNULL((SELECT no_invoice FROM brw_m_no_invoice where brw_id = in_brw_id and bulan = v_bulan and tahun = v_tahun ORDER BY no_invoice DESC LIMIT 1), 0) into v_temp_no_invoice;
	SELECT COUNT(bulan) into v_count_bulan FROM brw_m_no_invoice where brw_id = in_brw_id and bulan = v_bulan and tahun = v_tahun;

	
	IF v_count_bulan = 0 THEN
		-- SET NO INVOICE MULAI DARI 001 LAGI BEDA BULAN DAN TAHUN
		SET v_no_invoice = CONVERT(LPAD(1, 3, 0), CHAR);
	ELSE
		SET v_no_invoice = CONVERT(LPAD(v_temp_no_invoice+1, 3, 0), CHAR);
	END IF;
	
	INSERT INTO danasyariah.brw_m_no_invoice
	(invoice_id, no_invoice, brw_id, bulan, tahun, created_at, updated_at)
	VALUES(in_invoice_id, v_no_invoice, in_brw_id, v_bulan, v_tahun, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());



	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
	ELSE
	      SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
	      SET res = 0;
	      INSERT INTO log_db_app (file_name,line,description,created_at) VALUES ('proc_brw_pencairan',50,result,NOW());
	END IF;
	   
	RETURN CONCAT(v_no_invoice,"/DSI-",in_brw_id,"/",v_bulan,"/",v_tahun);
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_pasangan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_pasangan`(`brw_id_` int(11),
	`nama_` varchar(25),
	`jenis_kelamin_` int(1),
	`ktp_` varchar(16),
	`tempat_lahir_` VARCHAR(50),
	`tgl_lahir_` date,
	`no_hp_` varchar(25),
	`agama_` int(1),
	`pendidikan_` int(1),
	`npwp_` varchar(15),
	`alamat_` VARCHAR(100),
	`provinsi_` VARCHAR(50),
	`kota_` varchar(50),
	`kecamatan_` varchar(50),
	`kelurahan_` varchar(50),
	`kode_pos_` varchar(5),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT,
	`no_kk_` VARCHAR(50)
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_pasangan 
		(
			pasangan_id,
			nama,
			jenis_kelamin,
			ktp,
			tempat_lahir,
			tgl_lahir,
			no_hp,
			agama,
			pendidikan,
			npwp,
			alamat,
			provinsi,
			kota,
			kecamatan,
			kelurahan,
			kode_pos,
			created_at,
			updated_at,
			no_kk
		) VALUES (
			brw_id_ ,
			nama_ ,
			jenis_kelamin_ ,
			ktp_ ,
			tempat_lahir_ ,
			tgl_lahir_ ,
			no_hp_ ,
			agama_ ,
			pendidikan_ ,
			npwp_ ,
			alamat_ ,
			provinsi_ ,
			kota_ ,
			kecamatan_ ,
			kelurahan_ ,
			kode_pos_ ,
			created_at_ ,
			updated_at_,
			no_kk_ 
		)
		ON DUPLICATE KEY UPDATE 
			`pasangan_id`     = brw_id_,
			`nama`				= nama_,
			`jenis_kelamin`	= jenis_kelamin_,
			`ktp`					= ktp_,
			`tempat_lahir`		= tempat_lahir_,
			`tgl_lahir`			= tgl_lahir_,
			`no_hp`				= no_hp_,
			`agama`				= agama_,
			`pendidikan`		= pendidikan_,
			`npwp`				= npwp_,
			`alamat`				= alamat_,
			`provinsi`			= provinsi_,
			`kota`				= kota_,
			`kecamatan`			= kecamatan_,
			`kelurahan`			= kelurahan_,
			`kode_pos`			= kode_pos_,
			`created_at`		= created_at_,
			`updated_at`		= updated_at_,
			`no_kk`				= no_kk
		;
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_pekerjaan_pasangan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_pekerjaan_pasangan`(`brw_id2` INT,
	`sumber_pengembalian_dana` INT,
	`nama_perusahaan` VARCHAR(50),
	`alamat_perusahaan` VARCHAR(50),
	`rt` VARCHAR(50),
	`rw` VARCHAR(50),
	`provinsi` VARCHAR(50),
	`kab_kota` VARCHAR(50),
	`kecamatan` VARCHAR(50),
	`kelurahan` VARCHAR(50),
	`kode_pos` VARCHAR(50),
	`no_telp` VARCHAR(50),
	`no_hp` VARCHAR(50),
	`surat_ijin` INT,
	`no_surat_ijin` VARCHAR(50),
	`bentuk_badan_usaha` INT,
	`status_pekerjaan` INT,
	`usia_perusahaan` VARCHAR(50),
	`usia_tempat_usaha` VARCHAR(50),
	`departemen` VARCHAR(50),
	`jabatan` VARCHAR(50),
	`masa_kerja_tahun` VARCHAR(50),
	`masa_kerja_bulan` VARCHAR(50),
	`nip_nrp_nik` VARCHAR(50),
	`nama_hrd` VARCHAR(50),
	`no_fixed_line_hrd` VARCHAR(50),
	`pengalaman_kerja_tahun` VARCHAR(50),
	`pengalaman_kerja_bulan` VARCHAR(50),
	`pendapatan_borrower` DECIMAL(10,0),
	`biaya_hidup` DECIMAL(10,0),
	`detail_penghasilan_lain_lain` VARCHAR(50),
	`total_penghasilan_lain_lain` DECIMAL(10,0),
	`nilai_spt` DECIMAL(10,0),
	`bidang_pekerjaan` INT(1),
	`bidang_online` INT(1),
	`file_` VARCHAR(50),
	`line_` INT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   UPDATE brw_pasangan SET
   	
			`brw_id2` = brw_id2,
			`sumber_pengembalian_dana` = sumber_pengembalian_dana,
			`nama_perusahaan` = nama_perusahaan,
			`alamat_perusahaan` = alamat_perusahaan,
			`rt_pekerjaan_pasangan` = rt,
			`rw_pekerjaan_pasangan` = rw,
			`provinsi_pekerjaan_pasangan` = provinsi,
			`kab_kota_pekerjaan_pasangan` = kab_kota,
			`kecamatan_pekerjaan_pasangan` = kecamatan,
			`kelurahan_pekerjaan_pasangan` = kelurahan,
			`kode_pos_pekerjaan_pasangan` = kode_pos,
			`no_telp_pekerjaan_pasangan` = no_telp,
			`no_hp_pekerjaan_pasangan` = no_hp,
			`surat_ijin` = surat_ijin,
			`no_surat_ijin` = no_surat_ijin,
			`bentuk_badan_usaha` = bentuk_badan_usaha,
			`status_pekerjaan` = status_pekerjaan,
			`usia_perusahaan` = usia_perusahaan,
			`usia_tempat_usaha` = usia_tempat_usaha,
			`departemen` = departemen,
			`jabatan` = jabatan,
			`masa_kerja_tahun` = masa_kerja_tahun,
			`masa_kerja_bulan` = masa_kerja_bulan,
			`nip_nrp_nik` = nip_nrp_nik,
			`nama_hrd` = nama_hrd,
			`no_fixed_line_hrd` = no_fixed_line_hrd,
			`pengalaman_kerja_tahun` = pengalaman_kerja_tahun,
			`pengalaman_kerja_bulan` = pengalaman_kerja_bulan,
			`pendapatan_borrower` = pendapatan_borrower,
			`biaya_hidup` = biaya_hidup,
			`detail_penghasilan_lain_lain` = detail_penghasilan_lain_lain,
			`total_penghasilan_lain_lain` = total_penghasilan_lain_lain,
			`nilai_spt` = nilai_spt
	WHERE pasangan_id = brw_id2;
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_pengajuan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_pengajuan`(`brw_id_` int(11),
	`pendanaan_nama_` varchar(100),
	`pendanaan_tipe_` int(1),
	`pendanaan_akad_` int(1),
	`pendanaan_dana_dibutuhkan_` decimal(15,2),
	`estimasi_mulai_` date,
	`estimasi_imbal_hasil_` int(2),
	`mode_pembayaran_` int(2),
	`metode_pembayaran_` int(1),
	`durasi_proyek_` int(2),
	`detail_pendanaan_` TEXT,
	`dana_dicairkan_` decimal(15,2),
	`status_` int(1),
	`status_dana_` int(1),
	`id_proyek_` int(1),
	`lokasi_proyek_` VARCHAR(25),
	`geocode_` VARCHAR(50),
	`va_number_` varchar(25),
	`gambar_utama_` VARCHAR(50),
	`status_tampil_pengajuan_` varchar(50),
	`keterangan_` varchar(50),
	`created_at_` datetime,
	`updated_at_` DATETIME,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_pengajuan 
		   	(
				brw_id,
				pendanaan_nama,
				pendanaan_tipe,
				pendanaan_akad,
				pendanaan_dana_dibutuhkan,
				estimasi_mulai,
				estimasi_imbal_hasil,
				mode_pembayaran,
				metode_pembayaran,
				durasi_proyek,
				detail_pendanaan,
				dana_dicairkan,
				status,
				status_dana,
				id_proyek,
				lokasi_proyek,
				geocode,
				va_number,
				gambar_utama,
				status_tampil_pengajuan,
				keterangan,
				created_at,
				updated_at)
				values
				(
				brw_id_ ,
				pendanaan_nama_ ,
				pendanaan_tipe_ ,
				pendanaan_akad_ ,
				pendanaan_dana_dibutuhkan_ ,
				estimasi_mulai_ ,
				estimasi_imbal_hasil_ ,
				mode_pembayaran_ ,
				metode_pembayaran_ ,
				durasi_proyek_ ,
				detail_pendanaan_ ,
				dana_dicairkan_ ,
				status_ ,
				status_dana_ ,
				id_proyek_ ,
				lokasi_proyek_ ,
				geocode_ ,
				va_number_ ,
				gambar_utama_ ,
				status_tampil_pengajuan_ ,
				keterangan_ ,
				created_at_ ,
				updated_at_ );
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
    	  select max(pengajuan_id) into res FROM brw_pengajuan  ;
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_pengurus
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_pengurus`(`brw_id_` int(11),
	`urutan_` INT(3),
	`nm_pengurus_` varchar(50),
	`jenis_kelamin_` int(1),
	`nik_pengurus_` varchar(25),
	`tempat_lahir_` varchar(25),
	`tgl_lahir_` date,
	`no_tlp_` varchar(15),
	`agama_` int(1),
	`pendidikan_terakhir_` int(1),
	`npwp_` varchar(15),
	`jabatan_` varchar(25),
	`alamat_` varchar(50),
	`provinsi_` varchar(50),
	`kota_` varchar(50),
	`kecamatan_` varchar(50),
	`kelurahan_` varchar(50),
	`kode_pos_` VARCHAR(5),
	`foto_diri_` varchar(50),
	`foto_ktp_` varchar(50),
	`foto_diri_ktp_` varchar(50),
	`foto_npwp_` varchar(50),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT

) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_pengurus 
		   	(
				brw_id,
				nm_pengurus,
				jenis_kelamin,
				nik_pengurus,
				tempat_lahir,
				tgl_lahir,
				no_tlp,
				agama,
				pendidikan_terakhir,
				npwp,
				jabatan,
				alamat,
				provinsi,
				kota,
				kecamatan,
				kelurahan,
				kode_pos,
				foto_diri,
				foto_ktp,
				foto_diri_ktp,
				foto_npwp,
				created_at,
				updated_at)
				values
				(
				brw_id_ ,
				nm_pengurus_ ,
				jenis_kelamin_ ,
				nik_pengurus_ ,
				tempat_lahir_ ,
				tgl_lahir_ ,
				no_tlp_ ,
				agama_ ,
				pendidikan_terakhir_ ,
				npwp_ ,
				jabatan_ ,
				alamat_ ,
				provinsi_ ,
				kota_ ,
				kecamatan_ ,
				kelurahan_ ,
				kode_pos_ ,
				foto_diri_ ,
				foto_ktp_ ,
				foto_diri_ktp_ ,
				foto_npwp_ ,
				created_at_ ,
				updated_at_   );
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_persyaratan_insert
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_persyaratan_insert`(`brw_id_` int(11),
	`tipe_id_` int(1),
	`user_type_` int(1),
	`persyaratan_id_` int(1),
	`checked_` int(1),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_persyaratan_insert 
		   	(
				brw_id,
				tipe_id,
				user_type,
				persyaratan_id,
				checked,
				created_at,
				updated_at)
				values
				(
				brw_id_ ,
				tipe_id_ ,
				user_type_ ,
				persyaratan_id_ ,
				checked_ ,
				created_at_ ,
				updated_at_);
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_rekening
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_rekening`(`brw_id_` int(11),
	`va_number_` varchar(25),
	`brw_norek_` varchar(25),
	`brw_nm_pemilik_` varchar(50),
	`brw_kd_bank_` varchar(5),
	`total_plafon_` decimal(15,2),
	`total_terpakai_` decimal(15,2),
	`total_sisa_` decimal(15,2),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT,
	`kantor_cabang_pembuka_` VARCHAR(50)
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_rekening 
		   	(
				brw_id,
				va_number,
				brw_norek,
				brw_nm_pemilik,
				brw_kd_bank,
				total_plafon,
				total_terpakai,
				total_sisa,
				created_at,
				updated_at,
				kantor_cabang_pembuka)
				values
				(
				brw_id_ ,
				va_number_ ,
				brw_norek_ ,
				brw_nm_pemilik_ ,
				brw_kd_bank_ ,
				total_plafon_ ,
				total_terpakai_ ,
				total_sisa_ ,
				created_at_ ,
				updated_at_,
				kantor_cabang_pembuka_ );
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_user
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_user`(`username_` varchar(25),
	`email_` VARCHAR(50),
	`password_` VARCHAR(100),
	`remember_token_` VARCHAR(100),
	`email_verif_` VARCHAR(50),
	`ref_number_` varchar(25),
	`status_` VARCHAR(15),
	`otp_` varchar(6),
	`password_expiry_days_` int,
	`password_updated_at_` TIMESTAMP,
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 0;
	
   INSERT INTO brw_user 
		   	(
				username,
				email,
				password,
				remember_token,
				email_verif,
				ref_number,
				status,
				otp,
				password_expiry_days,
				password_updated_at,
				created_at,
				updated_at)
				values
				(
				username_ ,
				email_ ,
				password_ ,
				remember_token_ ,
				email_verif_ ,
				ref_number_ ,
				status_ ,
				otp_ ,
				password_expiry_days_ ,
				password_updated_at_ ,
				created_at_ ,
				updated_at_ );
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
    	  select brw_id into res FROM brw_user where username = username_ ;
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        #SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_user_detail
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_user_detail`(`brw_id_` int(11),
	`nama_` varchar(50),
	`nm_bdn_hukum_` varchar(50),
	`nib_` VARCHAR(50),
	`npwp_perusahaan_` VARCHAR(15),
	`no_akta_pendirian_` VARCHAR(50),
	`no_akta_perubahan_` VARCHAR(50),
	`tgl_akta_perubahan_` DATE,
	`tgl_berdiri_` DATE,
	`kode_telepon_` VARCHAR(25),
	`telpon_perusahaan_` VARCHAR(20),
	`foto_npwp_perusahaan_` VARCHAR(150),
	`bidang_usaha_` INT(1),
	`omset_tahun_terakhir_` INT(1),
	`tot_aset_tahun_terakhr_` INT(1),
	`jabatan_` VARCHAR(50),
	`brw_type_` int(1),
	`nm_ibu_` varchar(50),
	`ktp_` varchar(20),
	`npwp_` VARCHAR(15),
	`tgl_lahir_` date,
	`no_tlp_` varchar(15),
	`jns_kelamin_` int(1),
	`status_kawin_` int(1),
	`status_rumah_` int(1),
	`alamat_` varchar(100),
	`domisili_alamat_` varchar(100),
	`domisili_provinsi_` VARCHAR(50),
	`domisili_kota_` VARCHAR(50),
	`domisili_kecamatan_` VARCHAR(50),
	`domisili_kelurahan_` VARCHAR(50),
	`domisili_kd_pos_` VARCHAR(50),
	`domisili_status_rumah_` INT,
	`provinsi_` VARCHAR(50),
	`kota_` VARCHAR(50),
	`kecamatan_` VARCHAR(50),
	`kelurahan_` VARCHAR(50),
	`kode_pos_` VARCHAR(5),
	`agama_` int(1),
	`tempat_lahir_` varchar(100),
	`pendidikan_terakhir_` int(1),
	`pekerjaan_` int(1),
	`bidang_perusahaan_` varchar(5),
	`bidang_pekerjaan_` int(1),
	`bidang_online_` int(1),
	`pengalaman_pekerjaan_` int(1),
	`pendapatan_` int(1),
	`total_aset_` decimal(15,2),
	`kewarganegaraan_` varchar(20),
	`brw_online_` int(1) unsigned,
	`brw_pic_` varchar(50),
	`brw_pic_ktp_` varchar(50),
	`brw_pic_user_ktp_` varchar(50),
	`brw_pic_npwp_` varchar(50),
	`nomor_kk_pribadi_` varchar(25),
	`lamamenempati_` int(11),
	`tlprumah_` varchar(15),
	`kartukredit_pribadi_` varchar(25),
	`usiapensiun_` int(11),
	`alamatpenagihanrumah_` varchar(50),
	`alamatpenagihankantor_` varchar(50),
	`npwp_pasangan_` char(16),
	`nomor_kk_pasangan_` varchar(25),
	`tempat_lahir_pasangan_` varchar(5),
	`tgl_lahir_pasangan_` date,
	`pendidikan_pasangan_` int(1),
	`sd_pasangan_` varchar(25),
	`smp_pasangan_` varchar(25),
	`sma_pasangan_` varchar(25),
	`pt_pasangan_` varchar(25),
	`pekerjaan_pasangan_` int(1),
	`bd_pekerjaan_pasangan_` int(1),
	`bd_pekerjaanO_pasangan_` int(1),
	`pengalaman_pasangan_` int(1),
	`pendapatan_pasangan_` int(1),
	`kartukredit_pasangan_` varchar(25),
	`jmlasettdkbergerak_pasangan_` decimal(15,2),
	`jmlasetbergerak_pasangan_` decimal(15,2),
	`agama_pasangan_` int(1),
	`pic_kk_` varchar(50),
	`pic_ktp_pasangan_` varchar(50),
	`pic_surat_nikah_` varchar(50),
	`pic_spt_` varchar(50),
	`pic_rekkoran_` varchar(50),
	`pic_slipgaji_` varchar(50),
	`pic_lapkeuangan_` varchar(50),
	`is_tbk_` CHAR(1),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT











) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_user_detail
   	(
			`brw_id`,
			`nama` ,
			`nm_bdn_hukum`,
			`nib`,
			`npwp_perusahaan`,
			`no_akta_pendirian` ,
			`no_akta_perubahan`,
			`tgl_akta_perubahan`,
			`tgl_berdiri`,
			`kode_telpon`,
			`telpon_perusahaan`,
			`foto_npwp_perusahaan`,
			`bidang_usaha`,
			`omset_tahun_terakhir`,
			`tot_aset_tahun_terakhr`,
			`jabatan`,
			`brw_type`,
			`nm_ibu`,
			`ktp`,
			`npwp`,
			`tgl_lahir`,
			`no_tlp`,
			`jns_kelamin`,
			`status_kawin`,
			`status_rumah`,
			`alamat`,
			`domisili_alamat`,
			`domisili_provinsi`,
			`domisili_kota`,
			`domisili_kecamatan`,
			`domisili_kelurahan`,
			`domisili_kd_pos`,
			`domisili_status_rumah`,
			`provinsi`,
			`kota` ,
			`kecamatan`,
			`kelurahan`,
			`kode_pos`,
			`agama` ,
			`tempat_lahir`,
			`pendidikan_terakhir`,
			`pekerjaan`,
			`bidang_perusahaan` ,
			`bidang_pekerjaan`,
			`bidang_online` ,
			`pengalaman_pekerjaan` ,
			`pendapatan`,
			`total_aset` ,
			`kewarganegaraan`,
			`brw_online`,
			`brw_pic` ,
			`brw_pic_ktp` ,
			`brw_pic_user_ktp`,
			`brw_pic_npwp`,
			`nomor_kk_pribadi`,
			`lamamenempati`,
			`tlprumah` ,
			`kartukredit_pribadi` ,
			`usiapensiun` ,
			`alamatpenagihanrumah`,
			`alamatpenagihankantor` ,
			`npwp_pasangan`,
			`nomor_kk_pasangan`,
			`tempat_lahir_pasangan` ,
			`tgl_lahir_pasangan` ,
			`pendidikan_pasangan`,
			`sd_pasangan` ,
			`smp_pasangan`,
			`sma_pasangan`,
			`pt_pasangan` ,
			`pekerjaan_pasangan` ,
			`bd_pekerjaan_pasangan` ,
			`bd_pekerjaanO_pasangan` ,
			`pengalaman_pasangan` ,
			`pendapatan_pasangan` ,
			`kartukredit_pasangan`,
			`jmlasettdkbergerak_pasangan`,
			`jmlasetbergerak_pasangan`,
			`agama_pasangan`,
			`pic_kk`,
			`pic_ktp_pasangan`,
			`pic_surat_nikah`,
			`pic_spt`,
			`pic_rekkoran`,
			`pic_slipgaji`,
			`pic_lapkeuangan`,
			`is_tbk`,
			`created_at`,
			`updated_at`
		)
		values
		(
			brw_id_,
			nama_,
			nm_bdn_hukum_,
			nib_,
			npwp_perusahaan_,
			no_akta_pendirian_,
			no_akta_perubahan_,
			tgl_akta_perubahan_,
			tgl_berdiri_,
			kode_telepon_,
			telpon_perusahaan_,
			foto_npwp_perusahaan_,
			bidang_usaha_,
			omset_tahun_terakhir_,
			tot_aset_tahun_terakhr_,
			jabatan_,
			brw_type_,
			nm_ibu_,
			ktp_,
			npwp_,
			tgl_lahir_,
			no_tlp_,
			jns_kelamin_,
			status_kawin_,
			status_rumah_,
			alamat_,
			domisili_alamat_,
			domisili_provinsi_,
			domisili_kota_,
			domisili_kecamatan_,
			domisili_kelurahan_,
			domisili_kd_pos_,
			domisili_status_rumah_,
			provinsi_,
			kota_,
			kecamatan_,
			kelurahan_,
			kode_pos_,
			agama_,
			tempat_lahir_,
			pendidikan_terakhir_,
			pekerjaan_,
			bidang_perusahaan_,
			bidang_pekerjaan_,
			bidang_online_,
			pengalaman_pekerjaan_,
			pendapatan_,
			total_aset_,
			kewarganegaraan_,
			brw_online_,
			brw_pic_,
			brw_pic_ktp_,
			brw_pic_user_ktp_,
			brw_pic_npwp_,
			nomor_kk_pribadi_,
			lamamenempati_,
			tlprumah_,
			kartukredit_pribadi_,
			usiapensiun_,
			alamatpenagihanrumah_,
			alamatpenagihankantor_,
			npwp_pasangan_,
			nomor_kk_pasangan_,
			tempat_lahir_pasangan_,
			tgl_lahir_pasangan_,
			pendidikan_pasangan_,
			sd_pasangan_,
			smp_pasangan_,
			sma_pasangan_,
			pt_pasangan_,
			pekerjaan_pasangan_,
			bd_pekerjaan_pasangan_,
			bd_pekerjaanO_pasangan_,
			pengalaman_pasangan_,
			pendapatan_pasangan_,
			kartukredit_pasangan_,
			jmlasettdkbergerak_pasangan_,
			jmlasetbergerak_pasangan_,
			agama_pasangan_,
			pic_kk_,
			pic_ktp_pasangan_,
			pic_surat_nikah_,
			pic_spt_,
			pic_rekkoran_,
			pic_slipgaji_,
			pic_lapkeuangan_,
			is_tbk_,
			created_at_,
			updated_at_
		) ON DUPLICATE KEY UPDATE 
			`brw_id` 						= brw_id_,
			`nama` 							=  nama_,
			`nm_bdn_hukum` 				= nm_bdn_hukum_,
			`nib`								= nib_,
			`npwp_perusahaan` 			= npwp_perusahaan_,
			`no_akta_pendirian` 			= no_akta_pendirian_,
			`no_akta_perubahan` 			= no_akta_perubahan_,
			`tgl_akta_perubahan` 		= tgl_akta_perubahan_,
			`tgl_berdiri` 					= tgl_berdiri_,
			`kode_telpon`              = kode_telepon_,
			`telpon_perusahaan`			= telpon_perusahaan_,
			`foto_npwp_perusahaan` 		= foto_npwp_perusahaan_,
			`bidang_usaha` 				= bidang_usaha_,
			`omset_tahun_terakhir` 		= omset_tahun_terakhir_,
			`tot_aset_tahun_terakhr` 	= tot_aset_tahun_terakhr_,
			`jabatan` 						= jabatan_,
			`brw_type`						= brw_type_,
			`nm_ibu` 						= nm_ibu_,
			`ktp`								= ktp_,
			`npwp` 							= npwp_,
			`tgl_lahir` 					= tgl_lahir_,
			`no_tlp` 						= no_tlp_,
			`jns_kelamin` 					= jns_kelamin_,
			`status_kawin` 				= status_kawin_,
			`status_rumah` 				= status_rumah_,
			`alamat` 						= alamat_,
			`domisili_alamat` 			= domisili_alamat_,
			`domisili_provinsi` 			= domisili_provinsi_,
			`domisili_kota` 				= domisili_kota_,
			`domisili_kecamatan` 		= domisili_kecamatan_,
			`domisili_kelurahan` 		= domisili_kelurahan_,
			`domisili_kd_pos` 			= domisili_kd_pos_,
			`domisili_status_rumah` 	= domisili_status_rumah_,
			`provinsi` 						= provinsi_,
			`kota` 							= kota_,
			`kecamatan` 					= kecamatan_,
			`kelurahan` 					= kelurahan_,
			`kode_pos` 						= kode_pos_,
			`agama` 							= agama_,
			`tempat_lahir`					= tempat_lahir_,
			`pendidikan_terakhir` 		= pendidikan_terakhir_,
			`pekerjaan` 					= pekerjaan_,
			`bidang_perusahaan` 			= bidang_perusahaan_,
			`bidang_pekerjaan` 			= bidang_pekerjaan_,
			`bidang_online` 				= bidang_online_,
			`pengalaman_pekerjaan` 		= pengalaman_pekerjaan_,
			`pendapatan` 					= pendapatan_,
			`total_aset`					= total_aset_,
			`kewarganegaraan` 			= kewarganegaraan_,
			`brw_online` 					= brw_online_,
			`brw_pic` 						= brw_pic_,
			`brw_pic_ktp` 					= brw_pic_ktp_,
			`brw_pic_user_ktp` 			= brw_pic_user_ktp_,
			`brw_pic_npwp` 				= brw_pic_npwp_,
			`nomor_kk_pribadi` 			= nomor_kk_pribadi_,
			`lamamenempati` 				= lamamenempati_,
			`tlprumah` 						= tlprumah_,
			`kartukredit_pribadi` 		= kartukredit_pribadi_,
			`usiapensiun` 					= usiapensiun_,
			`alamatpenagihanrumah`		= alamatpenagihanrumah_,
			`alamatpenagihankantor` 	= alamatpenagihankantor_,
			`npwp_pasangan` 				= npwp_pasangan_,
			`nomor_kk_pasangan` 			= nomor_kk_pasangan_,
			`tempat_lahir_pasangan` 	= tempat_lahir_pasangan_,
			`tgl_lahir_pasangan` 		= tgl_lahir_pasangan_,
			`pendidikan_pasangan` 		= pendidikan_pasangan_,
			`sd_pasangan` 					= sd_pasangan_,
			`smp_pasangan` 				= smp_pasangan_,
			`sma_pasangan` 				= sma_pasangan_,
			`pt_pasangan` 					= pt_pasangan_,
			`pekerjaan_pasangan` 		= pekerjaan_pasangan_,
			`bd_pekerjaan_pasangan`		= bd_pekerjaan_pasangan_,
			`bd_pekerjaanO_pasangan` 	= bd_pekerjaanO_pasangan_,
			`pengalaman_pasangan` 		= pengalaman_pasangan_,
			`pendapatan_pasangan` 		= pendapatan_pasangan_,
			`kartukredit_pasangan` 		= kartukredit_pasangan_,
			`jmlasettdkbergerak_pasangan` = jmlasettdkbergerak_pasangan_,
			`jmlasetbergerak_pasangan` 	= jmlasetbergerak_pasangan_,
			`agama_pasangan` 					= agama_pasangan_,
			`pic_kk` 							= pic_kk_,
			`pic_ktp_pasangan` 				= pic_ktp_pasangan_,
			`pic_surat_nikah` 				= pic_surat_nikah_,
			`pic_spt` 							= pic_spt_,
			`pic_rekkoran` 					= pic_rekkoran_,
			`pic_slipgaji` 					= pic_slipgaji_,
			`pic_lapkeuangan` 				= pic_lapkeuangan_,
			`is_tbk`                      = is_tbk_,
			`created_at` 						= created_at_,
			`updated_at` 						= updated_at_
		;
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_user_detail_penghasilan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_user_detail_penghasilan`(`brw_id2` INT,
	`sumber_pengembalian_dana` INT,
	`skema_pembiayaan` INT,
	`nama_perusahaan` VARCHAR(50),
	`alamat_perusahaan` VARCHAR(100),
	`rt` VARCHAR(50),
	`rw` VARCHAR(50),
	`provinsi` VARCHAR(50),
	`kab_kota` VARCHAR(50),
	`kecamatan` VARCHAR(50),
	`kelurahan` VARCHAR(50),
	`kode_pos` VARCHAR(50),
	`no_telp` VARCHAR(50),
	`no_hp` VARCHAR(50),
	`surat_ijin` INT,
	`no_surat_ijin` VARCHAR(50),
	`bentuk_badan_usaha` INT,
	`status_pekerjaan` INT,
	`usia_perusahaan` VARCHAR(50),
	`usia_tempat_usaha` VARCHAR(50),
	`departemen` VARCHAR(50),
	`jabatan` VARCHAR(50),
	`masa_kerja_tahun` VARCHAR(50),
	`masa_kerja_bulan` VARCHAR(50),
	`nip_nrp_nik` VARCHAR(50),
	`nama_hrd` VARCHAR(50),
	`no_fixed_line_hrd` VARCHAR(50),
	`pengalaman_kerja_tahun` VARCHAR(50),
	`pengalaman_kerja_bulan` VARCHAR(50),
	`pendapatan_borrower` DECIMAL(10,0),
	`biaya_hidup` DECIMAL(10,0),
	`detail_penghasilan_lain_lain` VARCHAR(50),
	`total_penghasilan_lain_lain` DECIMAL(10,0),
	`nilai_spt` DECIMAL(10,0),
	`file_` VARCHAR(50),
	`line_` INT,
	`created_at` DATETIME,
	`updated_at` DATETIME
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_user_detail_penghasilan
   	(
			`brw_id2`,
			`sumber_pengembalian_dana`,
			`skema_pembiayaan`,
			`nama_perusahaan`,
			`alamat_perusahaan`,
			`rt`,
			`rw`,
			`provinsi`,
			`kab_kota`,
			`kecamatan`,
			`kelurahan`,
			`kode_pos`,
			`no_telp`,
			`no_hp`,
			`surat_ijin`,
			`no_surat_ijin`,
			`bentuk_badan_usaha`,
			`status_pekerjaan`,
			`usia_perusahaan`,
			`usia_tempat_usaha`,
			`departemen`,
			`jabatan`,
			`masa_kerja_tahun`,
			`masa_kerja_bulan`,
			`nip_nrp_nik`,
			`nama_hrd`,
			`no_fixed_line_hrd`,
			`pengalaman_kerja_tahun`,
			`pengalaman_kerja_bulan`,
			`pendapatan_borrower`,
			`biaya_hidup`,
			`detail_penghasilan_lain_lain`,
			`total_penghasilan_lain_lain`,
			`nilai_spt`,
			`created_at`,
			`updated_at`
		) VALUES (
			brw_id2,
			sumber_pengembalian_dana,
			skema_pembiayaan,
			nama_perusahaan,
			alamat_perusahaan,
			rt,
			rw,
			provinsi,
			kab_kota,
			kecamatan,
			kelurahan,
			kode_pos,
			no_telp,
			no_hp,
			surat_ijin,
			no_surat_ijin,
			bentuk_badan_usaha,
			status_pekerjaan,
			usia_perusahaan,
			usia_tempat_usaha,
			departemen,
			jabatan,
			masa_kerja_tahun,
			masa_kerja_bulan,
			nip_nrp_nik,
			nama_hrd,
			no_fixed_line_hrd,
			pengalaman_kerja_tahun,
			pengalaman_kerja_bulan,
			pendapatan_borrower,
			biaya_hidup,
			detail_penghasilan_lain_lain,
			total_penghasilan_lain_lain,
			nilai_spt,
			created_at,
			updated_at
		);   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_user_detail_penghasilan_simplify
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_user_detail_penghasilan_simplify`(`brw_id2` INT,
	`sumber_pengembalian_dana` INT,
	`skema_pembiayaan` INT,
	`nama_perusahaan` VARCHAR(50),
	`alamat_perusahaan` VARCHAR(100),
	`rt` VARCHAR(50),
	`rw` VARCHAR(50),
	`provinsi` VARCHAR(50),
	`kab_kota` VARCHAR(50),
	`kecamatan` VARCHAR(50),
	`kelurahan` VARCHAR(50),
	`kode_pos` VARCHAR(50),
	`no_telp` VARCHAR(50),
	`no_hp` VARCHAR(50),
	`surat_ijin` INT,
	`no_surat_ijin` VARCHAR(50),
	`bentuk_badan_usaha` INT,
	`status_pekerjaan` INT,
	`usia_perusahaan` VARCHAR(50),
	`usia_tempat_usaha` VARCHAR(50),
	`departemen` VARCHAR(50),
	`jabatan` VARCHAR(50),
	`masa_kerja_tahun` VARCHAR(50),
	`masa_kerja_bulan` VARCHAR(50),
	`nip_nrp_nik` VARCHAR(50),
	`nama_hrd` VARCHAR(50),
	`no_fixed_line_hrd` VARCHAR(50),
	`pengalaman_kerja_tahun` VARCHAR(50),
	`pengalaman_kerja_bulan` VARCHAR(50),
	`pendapatan_borrower` DECIMAL(10,0),
	`biaya_hidup` DECIMAL(10,0),
	`detail_penghasilan_lain_lain` VARCHAR(50),
	`total_penghasilan_lain_lain` DECIMAL(10,0),
	`nilai_spt` DECIMAL(10,0),
	`file_` VARCHAR(50),
	`line_` INT,
	`created_at` DATETIME,
	`updated_at` DATETIME



) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_user_detail_penghasilan
   	(
			`brw_id2`,
			`nama_perusahaan`,
			`no_telp`,
			`no_surat_ijin`,
			`usia_perusahaan`,
			`usia_tempat_usaha`,
			`masa_kerja_tahun`,
			`masa_kerja_bulan`,
			`pendapatan_borrower`,
			`alamat_perusahaan`,
			`rt`,
			`rw`,
			`kode_pos`,
			`biaya_hidup`,
			`detail_penghasilan_lain_lain`,
			`total_penghasilan_lain_lain`,
			`nilai_spt`,
			`departemen`,
			`jabatan`,
			`nip_nrp_nik`,
			`nama_hrd`,
			`pengalaman_kerja_tahun`,
			`pengalaman_kerja_bulan`,
			`created_at`,
			`updated_at`
		) VALUES (
			brw_id2,
			nama_perusahaan,
			no_telp,
			no_surat_ijin,
			usia_perusahaan,
			usia_tempat_usaha,
			masa_kerja_tahun,
			masa_kerja_bulan,
			pendapatan_borrower,
			alamat_perusahaan,
			rt,
			rw,
			kode_pos,
			\N,
			\N,
			\N,
			\N,
			\N,
			\N,
			\N,
			\N,
			\N,
			\N,
			created_at,
			updated_at
		) ON DUPLICATE KEY UPDATE 
		
			`brw_id2` = brw_id2,
			`nama_perusahaan` = nama_perusahaan,
			`no_telp` = no_telp,
			`no_surat_ijin` = no_surat_ijin,
			`usia_perusahaan` = usia_perusahaan,
			`usia_tempat_usaha` = usia_tempat_usaha,
			`masa_kerja_tahun` = masa_kerja_tahun,
			`masa_kerja_bulan` = masa_kerja_bulan,
			`pendapatan_borrower` = pendapatan_borrower,
			`alamat_perusahaan` = alamat_perusahaan,
			`rt` = rt,
			`rw` = rw,
			`kode_pos` = kode_pos,
			`updated_at` = updated_at
		;   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.put_into_brw_user_detail_simplify
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `put_into_brw_user_detail_simplify`(`brw_id_` int(11),
	`nama_` varchar(50),
	`nm_bdn_hukum_` varchar(50),
	`nib_` VARCHAR(50),
	`npwp_perusahaan_` VARCHAR(15),
	`no_akta_pendirian_` VARCHAR(50),
	`tgl_berdiri_` DATE,
	`telpon_perusahaan_` VARCHAR(20),
	`foto_npwp_perusahaan_` VARCHAR(150),
	`bidang_usaha_` INT(1),
	`omset_tahun_terakhir_` INT(1),
	`tot_aset_tahun_terakhr_` INT(1),
	`jabatan_` VARCHAR(50),
	`brw_type_` int(1),
	`nm_ibu_` varchar(50),
	`ktp_` varchar(20),
	`npwp_` VARCHAR(15),
	`tgl_lahir_` date,
	`no_tlp_` varchar(15),
	`jns_kelamin_` int(1),
	`status_kawin_` int(1),
	`status_rumah_` int(1),
	`alamat_` varchar(100),
	`domisili_alamat_` varchar(100),
	`domisili_provinsi_` VARCHAR(50),
	`domisili_kota_` VARCHAR(50),
	`domisili_kecamatan_` VARCHAR(50),
	`domisili_kelurahan_` VARCHAR(50),
	`domisili_kd_pos_` VARCHAR(50),
	`domisili_status_rumah_` INT,
	`provinsi_` VARCHAR(50),
	`kota_` VARCHAR(50),
	`kecamatan_` VARCHAR(50),
	`kelurahan_` VARCHAR(50),
	`kode_pos_` VARCHAR(5),
	`agama_` int(1),
	`tempat_lahir_` varchar(100),
	`pendidikan_terakhir_` int(1),
	`pekerjaan_` int(1),
	`bidang_perusahaan_` varchar(5),
	`bidang_pekerjaan_` int(1),
	`bidang_online_` int(1),
	`pengalaman_pekerjaan_` int(1),
	`pendapatan_` int(1),
	`total_aset_` decimal(15,2),
	`kewarganegaraan_` varchar(20),
	`brw_online_` int(1) unsigned,
	`brw_pic_` varchar(50),
	`brw_pic_ktp_` varchar(50),
	`brw_pic_user_ktp_` varchar(50),
	`brw_pic_npwp_` varchar(50),
	`nomor_kk_pribadi_` varchar(25),
	`lamamenempati_` int(11),
	`tlprumah_` varchar(15),
	`kartukredit_pribadi_` varchar(25),
	`usiapensiun_` int(11),
	`alamatpenagihanrumah_` varchar(50),
	`alamatpenagihankantor_` varchar(50),
	`npwp_pasangan_` char(16),
	`nomor_kk_pasangan_` varchar(25),
	`tempat_lahir_pasangan_` varchar(5),
	`tgl_lahir_pasangan_` date,
	`pendidikan_pasangan_` int(1),
	`sd_pasangan_` varchar(25),
	`smp_pasangan_` varchar(25),
	`sma_pasangan_` varchar(25),
	`pt_pasangan_` varchar(25),
	`pekerjaan_pasangan_` int(1),
	`bd_pekerjaan_pasangan_` int(1),
	`bd_pekerjaanO_pasangan_` int(1),
	`pengalaman_pasangan_` int(1),
	`pendapatan_pasangan_` int(1),
	`kartukredit_pasangan_` varchar(25),
	`jmlasettdkbergerak_pasangan_` decimal(15,2),
	`jmlasetbergerak_pasangan_` decimal(15,2),
	`agama_pasangan_` int(1),
	`pic_kk_` varchar(50),
	`pic_ktp_pasangan_` varchar(50),
	`pic_surat_nikah_` varchar(50),
	`pic_spt_` varchar(50),
	`pic_rekkoran_` varchar(50),
	`pic_slipgaji_` varchar(50),
	`pic_lapkeuangan_` varchar(50),
	`created_at_` datetime,
	`updated_at_` datetime,
	`file_` VARCHAR(100),
	`line_` INT
) RETURNS int(11)
    DETERMINISTIC
BEGIN
   
	DECLARE code CHAR(5) DEFAULT '00000';
  	DECLARE msg TEXT;
  	DECLARE nrows INT;
  	DECLARE result TEXT;
  	DECLARE res	INT;

   DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
     END;

	SET res = 1;
	
   INSERT INTO brw_user_detail
   	(
			`brw_id`,
			`nama` ,
			`brw_type`,
			`ktp`,
			`npwp`,
			`tgl_lahir`,
			`no_tlp`,
			`jns_kelamin`,
			`status_kawin`,
			`status_rumah`,
			`alamat`,
			`domisili_kd_pos`,
			`provinsi`,
			`kota` ,
			`kecamatan`,
			`kelurahan`,
			`kode_pos`,
			`agama` ,
			`tempat_lahir`,
			`pendidikan_terakhir`,
			`pekerjaan`,
			`bidang_pekerjaan`,
			`bidang_online` ,
			`pendapatan`,
			`brw_online`,
			`brw_pic` ,
			`brw_pic_ktp` ,
			`brw_pic_user_ktp`,
			`brw_pic_npwp`,
			`created_at`,
			`updated_at`
		)
		values
		(
			brw_id_,
			nama_,
			brw_type_,
			ktp_,
			npwp_,
			tgl_lahir_,
			no_tlp_,
			jns_kelamin_,
			status_kawin_,
			status_rumah_,
			alamat_,
			domisili_kd_pos_,
			provinsi_,
			kota_,
			kecamatan_,
			kelurahan_,
			kode_pos_,
			agama_,
			tempat_lahir_,
			pendidikan_terakhir_,
			pekerjaan_,
			bidang_pekerjaan_,
			bidang_online_,
			pendapatan_,
			brw_online_,
			brw_pic_,
			brw_pic_ktp_,
			brw_pic_user_ktp_,
			brw_pic_npwp_,
			created_at_,
			updated_at_
		) ON DUPLICATE KEY UPDATE 
			`brw_id` 						= brw_id_,
			`nama` 							=  nama_,
			`brw_type`						= brw_type_,
			`ktp`								= ktp_,
			`npwp` 							= npwp_,
			`tgl_lahir` 					= tgl_lahir_,
			`no_tlp` 						= no_tlp_,
			`jns_kelamin` 					= jns_kelamin_,
			`status_kawin` 				= status_kawin_,
			`status_rumah` 				= status_rumah_,
			`alamat` 						= alamat_,
			`domisili_kd_pos` 			= domisili_kd_pos_,
			`provinsi` 						= provinsi_,
			`kota` 							= kota_,
			`kecamatan` 					= kecamatan_,
			`kelurahan` 					= kelurahan_,
			`kode_pos` 						= kode_pos_,
			`agama` 							= agama_,
			`tempat_lahir`					= tempat_lahir_,
			`pendidikan_terakhir` 		= pendidikan_terakhir_,
			`pekerjaan` 					= pekerjaan_,
			`bidang_pekerjaan` 			= bidang_pekerjaan_,
			`bidang_online` 				= bidang_online_,
			`pendapatan` 					= pendapatan_,
			`brw_online` 					= brw_online_,
			`brw_pic` 						= brw_pic_,
			`brw_pic_ktp` 					= brw_pic_ktp_,
			`brw_pic_user_ktp` 			= brw_pic_user_ktp_,
			`brw_pic_npwp` 				= brw_pic_npwp_,
			`updated_at` 						= updated_at_
		;
   	
	IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
    	  SET result = CONCAT('insert succeeded, row count = ',nrows);
  ELSE
        SET result = CONCAT('insert failed, error = ',code,', message = ',msg);
        SET res = 0;
        INSERT INTO log_db_app (file_name,line,description,created_at) VALUES (file_,line_,result,NOW());
   END IF;
   
   RETURN res;
   
END//
DELIMITER ;

-- Dumping structure for function danasyariah.Terbilang
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `Terbilang`(`nominal_` bigint
) RETURNS varchar(150) CHARSET latin1
Begin
	Declare strNominal_ varchar(150);
	Declare angka1_ varchar(150);
	Declare angka2_ varchar(150);
	Declare strNomTot_ varchar(150);
	Declare satuan_ int;
	Declare puluhan_ int;
	Declare ratusan_ int;
	Declare desc_ varchar(150);


   If (nominal_  > 2000000000) then
   	SET desc_ = 'Plafon Maksimal Cuman 2 Milyar Bos' ;
   	RETURN desc_ ;
   ElseIf (nominal_ < 0) then
      SET desc_ = 'Nominal kagak boleh negatif Bos ' ;
   	RETURN desc_ ;
   END If;
   
	set strNominal_ = Cast(nominal_ as char);
	set desc_ = '';
	set satuan_ = 0;
	set puluhan_ = 0;
	
	While satuan_ <> Length(strNominal_) Do
		set satuan_ = satuan_ + 1;
		set strNomTot_ = Mid(strNominal_, satuan_, 1);
		set puluhan_ = puluhan_ + Cast(strNomTot_ as unsigned);
		set ratusan_ = Length(strNominal_) - satuan_ + 1;
		Case Cast(strNomTot_ as unsigned)
		When 1 then
			Begin
			If (ratusan_ = 1 or ratusan_ = 7 or ratusan_ = 10 or ratusan_ = 13) then
				set angka1_ = 'Satu ';
			ElseIf (ratusan_ = 4) then
				If (satuan_ = 1) then
					set angka1_ = 'Se';
				Else
					set angka1_ = 'Satu';
				End If;
			ElseIf (ratusan_ = 2 or ratusan_ = 5 or ratusan_ = 8 or ratusan_ = 11 or ratusan_ = 14) then
				set satuan_ = satuan_ + 1;
				set strNomTot_ = Mid(strNominal_, satuan_, 1);
				set ratusan_ = Length(strNominal_) - satuan_ + 1;
				set angka2_ = '';
				Case Cast(strNomTot_ AS unsigned)
					When 0 then set angka1_ = 'Sepuluh ';
					When 1 then set angka1_ = 'Sebelas ';
					When 2 then set angka1_ = 'Dua belas ';
					When 3 then set angka1_ = 'Tiga belas ';
					When 4 then set angka1_ = 'Empat belas ';
					When 5 then set angka1_ = 'Lima belas ';
					When 6 then set angka1_ = 'Enam belas ';
					When 7 then set angka1_ = 'Tujuh belas ';
					When 8 then set angka1_ = 'Delapan belas ';
					When 9 then set angka1_ = 'Sembilan belas ';
				Else Begin End;
				End Case;
			Else
				set angka1_ = 'Se';
			End If;
		End;
	
		When 2 then set angka1_ = 'Dua ';
		When 3 then set angka1_ = 'Tiga ';
		When 4 then set angka1_ = 'Empat ';
		When 5 then set angka1_ = 'Lima ';
		When 6 then set angka1_ = 'Enam ';
		When 7 then set angka1_ = 'Tujuh ';
		When 8 then set angka1_ = 'Delapan ';
		When 9 then set angka1_ = 'Sembilan ';
		Else set angka1_ = '';
		End Case;
		
		If Cast(strNomTot_ as unsigned) > 0 then
			If (ratusan_ = 2 or ratusan_ = 5 or ratusan_ = 8 or ratusan_ = 11 or ratusan_ = 14) then
				set angka2_ = 'puluh ';
			ElseIf (ratusan_ = 3 or ratusan_ = 6 or ratusan_ = 9 or ratusan_ = 12 or ratusan_ = 15) then
				set angka2_ = 'ratus ';
			Else
				set angka2_ = '';
			End If;
		Else
				set angka2_ = '';
		End If;
		
		If puluhan_ > 0 then
			Case ratusan_
				 When 4 then Begin set angka2_ = Concat(angka2_, 'ribu '); set puluhan_ = 0; End;
				 When 7 then Begin set angka2_ = Concat(angka2_, 'juta '); set puluhan_ = 0; End;
				 When 10 then Begin set angka2_ = Concat(angka2_, 'milyar '); set puluhan_ = 0; End;
				 Else Begin End;
			End Case;
		End If;
		
		set desc_ = Concat(desc_, angka1_, angka2_);
		
	End While;
	return desc_;
END//
DELIMITER ;

-- Dumping structure for function danasyariah.three_digit_format
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `three_digit_format`(_number decimal(15)
) RETURNS varchar(25) CHARSET latin1
begin

	declare sout varchar(25) default '';		
	declare counter int default 0;
	declare max_loop int default 5;
	declare three_dg int default 3;
	declare snumber varchar(25);
	declare ilength int;
	
			
	set snumber = cast(_number as varchar(25));
	if ( 999 < _number ) then
	   set ilength = length(snumber);
		while ( counter < max_loop and ilength > three_dg) do	
				set sout = concat(sout,'.',right(snumber,three_dg));	
				set snumber = left(snumber, ilength - three_dg);
				set ilength = ilength - three_dg;
				set counter = counter + 1;
		end while;
		
		if( 0 != ilength) then
			set sout = concat(snumber , sout);
		end if;				
	else
		set sout = snumber ;
	end if;
	
	return sout; 
end//
DELIMITER ;

-- Dumping structure for function danasyariah.update_user_profile_pengajuan
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `update_user_profile_pengajuan`(`_pengajuan_id` INT(11),
	`_method` VARCHAR(10)







) RETURNS int(1)
    DETERMINISTIC
BEGIN

 DECLARE code CHAR(5) DEFAULT '00000';
 DECLARE nrows INT;
 DECLARE result TEXT;
 DECLARE msg TEXT;
 
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
     BEGIN
       GET DIAGNOSTICS CONDITION 1
         code = RETURNED_SQLSTATE, msg = MESSAGE_TEXT;
    END;
	
IF _method = 'update' THEN 	
  
  /* Update user detail pengajuan */
	UPDATE brw_user_detail_pengajuan u1 
    JOIN brw_user_detail u2 ON u1.brw_id = u2.brw_id
    SET u1.nama = u2.nama, 
		u1.nm_bdn_hukum = u2.nm_bdn_hukum,
		u1.nib = u2.nib,
		u1.npwp_perusahaan = u2.npwp_perusahaan,
		u1.no_akta_pendirian = u2.no_akta_pendirian,
		u1.tgl_berdiri = u2.tgl_berdiri,
		u1.kode_telpon = u2.kode_telpon,
		u1.telpon_perusahaan = u2.telpon_perusahaan,
		u1.foto_npwp_perusahaan = u2.foto_npwp_perusahaan,
		u1.bidang_usaha = u2.bidang_usaha,
		u1.omset_tahun_terakhir = u2.omset_tahun_terakhir,
		u1.tot_aset_tahun_terakhr = u2.tot_aset_tahun_terakhr,
		u1.jabatan = u2.jabatan,
		u1.brw_type = u2.brw_type,
		u1.nm_ibu = u2.nm_ibu,
		u1.ktp = u2.ktp,
		u1.npwp = u2.npwp,
		u1.tgl_lahir = u2.tgl_lahir,
		u1.no_tlp = u2.no_tlp,
		u1.jns_kelamin = u2.jns_kelamin,
		u1.status_kawin = u2.status_kawin,
		u1.status_rumah = u2.status_rumah,
		u1.alamat = u2.alamat,
		u1.domisili_alamat = u2.domisili_alamat,
		u1.domisili_provinsi = u2.domisili_provinsi,
		u1.domisili_kota = u2.domisili_kota,
		u1.domisili_kecamatan = u2.domisili_kecamatan,
		u1.domisili_kelurahan = u2.domisili_kelurahan,
		u1.domisili_kd_pos = u2.domisili_kd_pos,
		u1.domisili_status_rumah = u2.domisili_status_rumah,
		u1.provinsi = u2.provinsi,
		u1.kota = u2.kota,
		u1.kecamatan = u2.kecamatan,
		u1.kelurahan = u2.kelurahan,
		u1.kode_pos = u2.kode_pos,
		u1.agama = u2.agama,
		u1.tempat_lahir = u2.tempat_lahir,
		u1.pendidikan_terakhir = u2.pendidikan_terakhir,
		u1.pekerjaan = u2.pekerjaan,
		u1.bidang_perusahaan = u2.bidang_pekerjaan,
		u1.bidang_pekerjaan= u2.bidang_pekerjaan,
		u1.bidang_online = u2.bidang_online,
		u1.pengalaman_pekerjaan = u2.pengalaman_pekerjaan,
		u1.pendapatan = u2.pendapatan,
		u1.total_aset = u2.total_aset,
		u1.kewarganegaraan = u2.kewarganegaraan,
		u1.brw_online = u2.brw_online,
		u1.brw_pic = u2.brw_pic,
		u1.brw_pic_ktp = u2.brw_pic_ktp,
		u1.brw_pic_user_ktp = u2.brw_pic_user_ktp,
		u1.brw_pic_npwp = u2.brw_pic_npwp,
		u1.nomor_kk_pribadi = u2.nomor_kk_pribadi,
		u1.lamamenempati = u2.lamamenempati,
		u1.tlprumah = u2.tlprumah,
		u1.kartukredit_pribadi = u2.kartukredit_pribadi,
		u1.usiapensiun = u2.usiapensiun,
		u1.alamatpenagihanrumah = u2.alamatpenagihanrumah,
		u1.alamatpenagihankantor = u2.alamatpenagihankantor,
		u1.npwp_pasangan = u2.npwp_pasangan,
		u1.nomor_kk_pasangan = u2.nomor_kk_pasangan,
		u1.tempat_lahir_pasangan = u2.tempat_lahir_pasangan,
		u1.tgl_lahir_pasangan = u2.tgl_lahir_pasangan,
		u1.pendidikan_pasangan = u2.pendidikan_pasangan,
		u1.sd_pasangan = u2.sd_pasangan,
		u1.smp_pasangan = u2.smp_pasangan,
		u1.sma_pasangan = u2.sma_pasangan,
		u1.pt_pasangan = u2.pt_pasangan,
		u1.pekerjaan_pasangan = u2.pekerjaan_pasangan,
		u1.bd_pekerjaan_pasangan = u2.bd_pekerjaan_pasangan,
		u1.bd_pekerjaanO_pasangan = u2.bd_pekerjaanO_pasangan,
		u1.pengalaman_pasangan = u2.pengalaman_pasangan,
		u1.pendapatan_pasangan = u2.pendapatan_pasangan,
		u1.kartukredit_pasangan = u2.kartukredit_pasangan,
		u1.jmlasettdkbergerak_pasangan = u2.jmlasettdkbergerak_pasangan,
		u1.jmlasetbergerak_pasangan = u2.jmlasetbergerak_pasangan,
		u1.agama_pasangan = u2.agama_pasangan,
		u1.pic_kk = u2.pic_kk,
		u1.pic_ktp_pasangan = u2.pic_ktp_pasangan,
		u1.pic_surat_nikah = u2.pic_surat_nikah,
		u1.pic_spt = u2.pic_spt,
		u1.pic_rekkoran = u2.pic_rekkoran,
		u1.pic_slipgaji = u2.pic_slipgaji,
		u1.pic_lapkeuangan = u2.pic_lapkeuangan,
		u1.no_akta_perubahan = u2.no_akta_perubahan,
		u1.tgl_akta_perubahan = u2.tgl_akta_perubahan,
		u1.no_akta_pendirian = u2.no_akta_pendirian ,
		u1.kedudukan_perusahaan = u2.kedudukan_perusahaan ,
		u1.nama_notaris_akta_pendirian = u2.nama_notaris_akta_pendirian , 
		u1.kedudukan_notaris_pendirian = u2.kedudukan_notaris_pendirian , 
		u1.no_sk_kemenkumham_pendirian = u2.no_sk_kemenkumham_pendirian , 
		u1.tgl_sk_kemenkumham_pendirian = u2.tgl_sk_kemenkumham_pendirian , 
		u1.nama_notaris_akta_perubahan = u2.nama_notaris_akta_perubahan ,
		u1.kedudukan_notaris_perubahan = u2.kedudukan_notaris_perubahan ,
		u1.no_sk_kemenkumham_perubahan = u2.no_sk_kemenkumham_perubahan , 
		u1.tgl_sk_kemenkumham_perubahan = u2.tgl_sk_kemenkumham_perubahan , 
		u1.is_tbk = u2.is_tbk
    WHERE u1.pengajuan_id = _pengajuan_id;
	
	  /* Update user detail penghasilan pengajuan */
	UPDATE brw_user_detail_penghasilan_pengajuan u1 
    JOIN brw_user_detail_penghasilan u2 ON u1.brw_id2 = u2.brw_id2
    SET u1.sumber_pengembalian_dana = u2.sumber_pengembalian_dana,
		u1.skema_pembiayaan = u2.skema_pembiayaan,
		u1.nama_perusahaan = u2.nama_perusahaan,
		u1.alamat_perusahaan = u2.alamat_perusahaan,
		u1.rt = u2.rt,
		u1.rw = u2.rw,
		u1.provinsi = u2.provinsi,
		u1.kab_kota = u2.kab_kota,
		u1.kecamatan = u2.kecamatan,
		u1.kelurahan = u2.kelurahan,
		u1.kode_pos = u2.kode_pos,
		u1.no_telp = u2.no_telp,
		u1.no_hp = u2.no_hp,
		u1.surat_ijin = u2.surat_ijin,
		u1.no_surat_ijin = u2.no_surat_ijin,
		u1.bentuk_badan_usaha = u2.bentuk_badan_usaha,
		u1.status_pekerjaan = u2.status_pekerjaan,
		u1.usia_perusahaan = u2.usia_perusahaan,
		u1.usia_tempat_usaha = u2.usia_tempat_usaha,
		u1.departemen = u2.departemen,
		u1.jabatan = u2.jabatan,
		u1.masa_kerja_tahun = u2.masa_kerja_tahun,
		u1.masa_kerja_bulan = u2.masa_kerja_bulan,
		u1.nip_nrp_nik = u2.nip_nrp_nik,
		u1.nama_hrd = u2.nama_hrd,
		u1.no_fixed_line_hrd = u2.no_fixed_line_hrd,
		u1.pengalaman_kerja_tahun = u2.pengalaman_kerja_tahun,
		u1.pengalaman_kerja_bulan = u2.pengalaman_kerja_bulan,
		u1.pendapatan_borrower = u2.pendapatan_borrower,
		u1.biaya_hidup = u2.biaya_hidup,
		u1.detail_penghasilan_lain_lain = u2.detail_penghasilan_lain_lain,
		u1.total_penghasilan_lain_lain = u2.total_penghasilan_lain_lain,
		u1.nilai_spt = u2.nilai_spt
    WHERE u1.pengajuan_id = _pengajuan_id;
		
ELSEIF _method = 'create' THEN 

	/* Insert User Detail Pengajuan */
	INSERT INTO brw_user_detail_pengajuan
	(`pengajuan_id`, `brw_id`, `nama`, `nm_bdn_hukum`, `nib`, `npwp_perusahaan`, `no_akta_pendirian`, 
		`tgl_berdiri`,
		`kode_telpon`,
		`telpon_perusahaan`,
		`foto_npwp_perusahaan`,
		`bidang_usaha`,
		`omset_tahun_terakhir`,
		`tot_aset_tahun_terakhr`,
		`jabatan`,
		`brw_type`,
		`nm_ibu`,
		`ktp`,
		`npwp`,
		`tgl_lahir`,
		`no_tlp`,
		`jns_kelamin`,
		`status_kawin`,
		`status_rumah`,
		`alamat`,
		`domisili_alamat`,
		`domisili_provinsi`,
		`domisili_kota`,
		`domisili_kecamatan`,
		`domisili_kelurahan`,
		`domisili_kd_pos`,
		`domisili_status_rumah`,
		`provinsi`,
		`kota`,
		`kecamatan`,
		`kelurahan`,
		`kode_pos`,
		`agama`,
		`tempat_lahir`,
		`pendidikan_terakhir`,
		`pekerjaan`,
		`bidang_perusahaan`,
		`bidang_pekerjaan`,
		`bidang_online`,
		`pengalaman_pekerjaan`,
		`pendapatan`,
		`total_aset`,
		`kewarganegaraan`,
		`brw_online`,
		`brw_pic`,
		`brw_pic_ktp`,
		`brw_pic_user_ktp`,
		`brw_pic_npwp`,
		`nomor_kk_pribadi`,
		`lamamenempati`,
		`tlprumah`,
		`kartukredit_pribadi`,
		`usiapensiun`,
		`alamatpenagihanrumah`,
		`alamatpenagihankantor`,
		`npwp_pasangan`,
		`nomor_kk_pasangan`,
		`tempat_lahir_pasangan`,
		`tgl_lahir_pasangan`,
		`pendidikan_pasangan`,
		`sd_pasangan`,
		`smp_pasangan`,
		`sma_pasangan`,
		`pt_pasangan`,
		`pekerjaan_pasangan`,
		`bd_pekerjaan_pasangan`,
		`bd_pekerjaanO_pasangan`,
		`pengalaman_pasangan`,
		`pendapatan_pasangan`,
		`kartukredit_pasangan`,
		`jmlasettdkbergerak_pasangan`,
		`jmlasetbergerak_pasangan`,
		`agama_pasangan`,
		`pic_kk`,
		`pic_ktp_pasangan`,
		`pic_surat_nikah`,
		`pic_spt`,
		`pic_rekkoran`,
		`pic_slipgaji`,
		`pic_lapkeuangan`,
		`no_akta_perubahan`,
		`tgl_akta_perubahan`,
		`is_tbk`,
		kedudukan_perusahaan,
		nama_notaris_akta_pendirian,
		kedudukan_notaris_pendirian,
		no_sk_kemenkumham_pendirian,
		tgl_sk_kemenkumham_pendirian,
		nama_notaris_akta_perubahan,
		kedudukan_notaris_perubahan,
		no_sk_kemenkumham_perubahan,
		tgl_sk_kemenkumham_perubahan,
		`created_at`,
		`updated_at`)
	SELECT  brw_pengajuan.`pengajuan_id`,
		brw_user_detail.`brw_id`,
		brw_user_detail.`nama`, 
		brw_user_detail.`nm_bdn_hukum`,
		brw_user_detail.`nib`,
		brw_user_detail.`npwp_perusahaan`,
		brw_user_detail.`no_akta_pendirian`,
		brw_user_detail.`tgl_berdiri`,
		brw_user_detail.`kode_telpon`,
		brw_user_detail.`telpon_perusahaan`,
		brw_user_detail.`foto_npwp_perusahaan`,
		brw_user_detail.`bidang_usaha`,
		brw_user_detail.`omset_tahun_terakhir`,
		brw_user_detail.`tot_aset_tahun_terakhr`,
		brw_user_detail.`jabatan`,
		brw_user_detail.`brw_type`,
		brw_user_detail.`nm_ibu`,
		brw_user_detail.`ktp`,
		brw_user_detail.`npwp`,
		brw_user_detail.`tgl_lahir`,
		brw_user_detail.`no_tlp`,
		brw_user_detail.`jns_kelamin`,
		brw_user_detail.`status_kawin`,
		brw_user_detail.`status_rumah`,
		brw_user_detail.`alamat`,
		brw_user_detail.`domisili_alamat`,
		brw_user_detail.`domisili_provinsi`,
		brw_user_detail.`domisili_kota`,
		brw_user_detail.`domisili_kecamatan`,
		brw_user_detail.`domisili_kelurahan`,
		brw_user_detail.`domisili_kd_pos`,
		brw_user_detail.`domisili_status_rumah`,
		brw_user_detail.`provinsi`,
		brw_user_detail.`kota`,
		brw_user_detail.`kecamatan`,
		brw_user_detail.`kelurahan`,
		brw_user_detail.`kode_pos`,
		brw_user_detail.`agama`,
		brw_user_detail.`tempat_lahir`,
		brw_user_detail.`pendidikan_terakhir`,
		brw_user_detail.`pekerjaan`,
		brw_user_detail.`bidang_perusahaan`,
		brw_user_detail.`bidang_pekerjaan`,
		brw_user_detail.`bidang_online`,
		brw_user_detail.`pengalaman_pekerjaan`,
		brw_user_detail.`pendapatan`,
		brw_user_detail.`total_aset`,
		brw_user_detail.`kewarganegaraan`,
		brw_user_detail.`brw_online`,
		brw_user_detail.`brw_pic`,
		brw_user_detail.`brw_pic_ktp`,
		brw_user_detail.`brw_pic_user_ktp`,
		brw_user_detail.`brw_pic_npwp`,
		brw_user_detail.`nomor_kk_pribadi`,
		brw_user_detail.`lamamenempati`,
		brw_user_detail.`tlprumah`,
		brw_user_detail.`kartukredit_pribadi`,
		brw_user_detail.`usiapensiun`,
		brw_user_detail.`alamatpenagihanrumah`,
		brw_user_detail.`alamatpenagihankantor`,
		brw_user_detail.`npwp_pasangan`,
		brw_user_detail.`nomor_kk_pasangan`,
		brw_user_detail.`tempat_lahir_pasangan`,
		brw_user_detail.`tgl_lahir_pasangan`,
		brw_user_detail.`pendidikan_pasangan`,
		brw_user_detail.`sd_pasangan`,
		brw_user_detail.`smp_pasangan`,
		brw_user_detail.`sma_pasangan`,
		brw_user_detail.`pt_pasangan`,
		brw_user_detail.`pekerjaan_pasangan`,
		brw_user_detail.`bd_pekerjaan_pasangan`,
		brw_user_detail.`bd_pekerjaanO_pasangan`,
		brw_user_detail.`pengalaman_pasangan`,
		brw_user_detail.`pendapatan_pasangan`,
		brw_user_detail.`kartukredit_pasangan`,
		brw_user_detail.`jmlasettdkbergerak_pasangan`,
		brw_user_detail.`jmlasetbergerak_pasangan`,
		brw_user_detail.`agama_pasangan`,
		brw_user_detail.`pic_kk`,
		brw_user_detail.`pic_ktp_pasangan`,
		brw_user_detail.`pic_surat_nikah`,
		brw_user_detail.`pic_spt`,
		brw_user_detail.`pic_rekkoran`,
		brw_user_detail.`pic_slipgaji`,
		brw_user_detail.`pic_lapkeuangan`,
		brw_user_detail.`no_akta_perubahan`,
		brw_user_detail.`tgl_akta_perubahan`,
		brw_user_detail.`is_tbk`,
		brw_user_detail.kedudukan_perusahaan,
		brw_user_detail.nama_notaris_akta_pendirian,
		brw_user_detail.kedudukan_notaris_pendirian,
		brw_user_detail.no_sk_kemenkumham_pendirian,
		brw_user_detail.tgl_sk_kemenkumham_pendirian,
		brw_user_detail.nama_notaris_akta_perubahan,
		brw_user_detail.kedudukan_notaris_perubahan,
		brw_user_detail.no_sk_kemenkumham_perubahan,
		brw_user_detail.tgl_sk_kemenkumham_perubahan,
		brw_user_detail.`created_at`,
		brw_user_detail.`updated_at`
	FROM brw_user_detail 
	  JOIN brw_pengajuan ON brw_user_detail.brw_id = brw_pengajuan.brw_id
	WHERE brw_pengajuan.pengajuan_id = _pengajuan_id; 
	
	
	/* Insert User Detail Penghasilan Pengajuan */
	/* Insert pengajuan penghasilan */
	INSERT INTO brw_user_detail_penghasilan_pengajuan
	(
	  `pengajuan_id`,
	  `brw_id2`,
	  `sumber_pengembalian_dana`,
	  `skema_pembiayaan`,
	  `nama_perusahaan`,
	  `alamat_perusahaan`,
	  `rt`,
	  `rw`,
	  `provinsi`,
	  `kab_kota`,
	  `kecamatan`,
	  `kelurahan`,
	  `kode_pos`,
	  `no_telp`,
	  `no_hp`,
	  `surat_ijin`,
	  `no_surat_ijin`,
	  `bentuk_badan_usaha`,
	  `status_pekerjaan`,
	  `usia_perusahaan`,
	  `usia_tempat_usaha`,
	  `departemen`,
	  `jabatan`,
	  `masa_kerja_tahun`,
	  `masa_kerja_bulan`,
	  `nip_nrp_nik`,
	  `nama_hrd`,
	  `no_fixed_line_hrd`,
	  `pengalaman_kerja_tahun`,
	  `pengalaman_kerja_bulan`,
	  `pendapatan_borrower`,
	  `biaya_hidup`,
	  `detail_penghasilan_lain_lain`,
	  `total_penghasilan_lain_lain`,
	  `nilai_spt`,
	  `created_at`,
	  `updated_at` 
	)
	SELECT  brw_pengajuan.`pengajuan_id`,
		  brw_user_detail_penghasilan.`brw_id2`,
		  brw_user_detail_penghasilan.`sumber_pengembalian_dana`,
		  brw_user_detail_penghasilan.`skema_pembiayaan`,
		  brw_user_detail_penghasilan.`nama_perusahaan`,
		  brw_user_detail_penghasilan.`alamat_perusahaan`,
		  brw_user_detail_penghasilan.`rt`,
		  brw_user_detail_penghasilan.`rw`,
		  brw_user_detail_penghasilan.`provinsi`,
		  brw_user_detail_penghasilan.`kab_kota`,
		  brw_user_detail_penghasilan.`kecamatan`,
		  brw_user_detail_penghasilan.`kelurahan`,
		  brw_user_detail_penghasilan.`kode_pos`,
		  brw_user_detail_penghasilan.`no_telp`,
		  brw_user_detail_penghasilan.`no_hp`,
		  brw_user_detail_penghasilan.`surat_ijin`,
		  brw_user_detail_penghasilan.`no_surat_ijin`,
		  brw_user_detail_penghasilan.`bentuk_badan_usaha`,
		  brw_user_detail_penghasilan.`status_pekerjaan`,
		  brw_user_detail_penghasilan.`usia_perusahaan`,
		  brw_user_detail_penghasilan.`usia_tempat_usaha`,
		  brw_user_detail_penghasilan.`departemen`,
		  brw_user_detail_penghasilan.`jabatan`,
		  brw_user_detail_penghasilan.`masa_kerja_tahun`,
		  brw_user_detail_penghasilan.`masa_kerja_bulan`,
		  brw_user_detail_penghasilan.`nip_nrp_nik`,
		  brw_user_detail_penghasilan.`nama_hrd`,
		  brw_user_detail_penghasilan.`no_fixed_line_hrd`,
		  brw_user_detail_penghasilan.`pengalaman_kerja_tahun`,
		  brw_user_detail_penghasilan.`pengalaman_kerja_bulan`,
		  brw_user_detail_penghasilan.`pendapatan_borrower`,
		  brw_user_detail_penghasilan.`biaya_hidup`,
		  brw_user_detail_penghasilan.`detail_penghasilan_lain_lain`,
		  brw_user_detail_penghasilan.`total_penghasilan_lain_lain`,
		  brw_user_detail_penghasilan.`nilai_spt`,
		  brw_user_detail_penghasilan.`created_at`,
		  brw_user_detail_penghasilan.`updated_at` 
	 FROM brw_user_detail_penghasilan 
	   JOIN brw_pengajuan ON brw_user_detail_penghasilan.brw_id2 = brw_pengajuan.brw_id
     WHERE brw_pengajuan.pengajuan_id = _pengajuan_id;
	
ELSE 

    /* Copy all profile user master to profile user pengajuan   */	
	/* Insert all Pengajuan */
	INSERT INTO brw_user_detail_pengajuan
	(`pengajuan_id`, `brw_id`, `nama`, `nm_bdn_hukum`, `nib`, `npwp_perusahaan`, `no_akta_pendirian`, 
		`tgl_berdiri`,
		`kode_telpon`,
		`telpon_perusahaan`,
		`foto_npwp_perusahaan`,
		`bidang_usaha`,
		`omset_tahun_terakhir`,
		`tot_aset_tahun_terakhr`,
		`jabatan`,
		`brw_type`,
		`nm_ibu`,
		`ktp`,
		`npwp`,
		`tgl_lahir`,
		`no_tlp`,
		`jns_kelamin`,
		`status_kawin`,
		`status_rumah`,
		`alamat`,
		`domisili_alamat`,
		`domisili_provinsi`,
		`domisili_kota`,
		`domisili_kecamatan`,
		`domisili_kelurahan`,
		`domisili_kd_pos`,
		`domisili_status_rumah`,
		`provinsi`,
		`kota`,
		`kecamatan`,
		`kelurahan`,
		`kode_pos`,
		`agama`,
		`tempat_lahir`,
		`pendidikan_terakhir`,
		`pekerjaan`,
		`bidang_perusahaan`,
		`bidang_pekerjaan`,
		`bidang_online`,
		`pengalaman_pekerjaan`,
		`pendapatan`,
		`total_aset`,
		`kewarganegaraan`,
		`brw_online`,
		`brw_pic`,
		`brw_pic_ktp`,
		`brw_pic_user_ktp`,
		`brw_pic_npwp`,
		`nomor_kk_pribadi`,
		`lamamenempati`,
		`tlprumah`,
		`kartukredit_pribadi`,
		`usiapensiun`,
		`alamatpenagihanrumah`,
		`alamatpenagihankantor`,
		`npwp_pasangan`,
		`nomor_kk_pasangan`,
		`tempat_lahir_pasangan`,
		`tgl_lahir_pasangan`,
		`pendidikan_pasangan`,
		`sd_pasangan`,
		`smp_pasangan`,
		`sma_pasangan`,
		`pt_pasangan`,
		`pekerjaan_pasangan`,
		`bd_pekerjaan_pasangan`,
		`bd_pekerjaanO_pasangan`,
		`pengalaman_pasangan`,
		`pendapatan_pasangan`,
		`kartukredit_pasangan`,
		`jmlasettdkbergerak_pasangan`,
		`jmlasetbergerak_pasangan`,
		`agama_pasangan`,
		`pic_kk`,
		`pic_ktp_pasangan`,
		`pic_surat_nikah`,
		`pic_spt`,
		`pic_rekkoran`,
		`pic_slipgaji`,
		`pic_lapkeuangan`,
		`no_akta_perubahan`,
		`tgl_akta_perubahan`,
		`is_tbk`,
		kedudukan_perusahaan,
		nama_notaris_akta_pendirian,
		kedudukan_notaris_pendirian,
		no_sk_kemenkumham_pendirian,
		tgl_sk_kemenkumham_pendirian,
		nama_notaris_akta_perubahan,
		kedudukan_notaris_perubahan,
		no_sk_kemenkumham_perubahan,
		tgl_sk_kemenkumham_perubahan,
		`created_at`,
		`updated_at`)
	SELECT  brw_pengajuan.`pengajuan_id`,
		brw_user_detail.`brw_id`,
		brw_user_detail.`nama`, 
		brw_user_detail.`nm_bdn_hukum`,
		brw_user_detail.`nib`,
		brw_user_detail.`npwp_perusahaan`,
		brw_user_detail.`no_akta_pendirian`,
		brw_user_detail.`tgl_berdiri`,
		brw_user_detail.`kode_telpon`,
		brw_user_detail.`telpon_perusahaan`,
		brw_user_detail.`foto_npwp_perusahaan`,
		brw_user_detail.`bidang_usaha`,
		brw_user_detail.`omset_tahun_terakhir`,
		brw_user_detail.`tot_aset_tahun_terakhr`,
		brw_user_detail.`jabatan`,
		brw_user_detail.`brw_type`,
		brw_user_detail.`nm_ibu`,
		brw_user_detail.`ktp`,
		brw_user_detail.`npwp`,
		brw_user_detail.`tgl_lahir`,
		brw_user_detail.`no_tlp`,
		brw_user_detail.`jns_kelamin`,
		brw_user_detail.`status_kawin`,
		brw_user_detail.`status_rumah`,
		brw_user_detail.`alamat`,
		brw_user_detail.`domisili_alamat`,
		brw_user_detail.`domisili_provinsi`,
		brw_user_detail.`domisili_kota`,
		brw_user_detail.`domisili_kecamatan`,
		brw_user_detail.`domisili_kelurahan`,
		brw_user_detail.`domisili_kd_pos`,
		brw_user_detail.`domisili_status_rumah`,
		brw_user_detail.`provinsi`,
		brw_user_detail.`kota`,
		brw_user_detail.`kecamatan`,
		brw_user_detail.`kelurahan`,
		brw_user_detail.`kode_pos`,
		brw_user_detail.`agama`,
		brw_user_detail.`tempat_lahir`,
		brw_user_detail.`pendidikan_terakhir`,
		brw_user_detail.`pekerjaan`,
		brw_user_detail.`bidang_perusahaan`,
		brw_user_detail.`bidang_pekerjaan`,
		brw_user_detail.`bidang_online`,
		brw_user_detail.`pengalaman_pekerjaan`,
		brw_user_detail.`pendapatan`,
		brw_user_detail.`total_aset`,
		brw_user_detail.`kewarganegaraan`,
		brw_user_detail.`brw_online`,
		brw_user_detail.`brw_pic`,
		brw_user_detail.`brw_pic_ktp`,
		brw_user_detail.`brw_pic_user_ktp`,
		brw_user_detail.`brw_pic_npwp`,
		brw_user_detail.`nomor_kk_pribadi`,
		brw_user_detail.`lamamenempati`,
		brw_user_detail.`tlprumah`,
		brw_user_detail.`kartukredit_pribadi`,
		brw_user_detail.`usiapensiun`,
		brw_user_detail.`alamatpenagihanrumah`,
		brw_user_detail.`alamatpenagihankantor`,
		brw_user_detail.`npwp_pasangan`,
		brw_user_detail.`nomor_kk_pasangan`,
		brw_user_detail.`tempat_lahir_pasangan`,
		brw_user_detail.`tgl_lahir_pasangan`,
		brw_user_detail.`pendidikan_pasangan`,
		brw_user_detail.`sd_pasangan`,
		brw_user_detail.`smp_pasangan`,
		brw_user_detail.`sma_pasangan`,
		brw_user_detail.`pt_pasangan`,
		brw_user_detail.`pekerjaan_pasangan`,
		brw_user_detail.`bd_pekerjaan_pasangan`,
		brw_user_detail.`bd_pekerjaanO_pasangan`,
		brw_user_detail.`pengalaman_pasangan`,
		brw_user_detail.`pendapatan_pasangan`,
		brw_user_detail.`kartukredit_pasangan`,
		brw_user_detail.`jmlasettdkbergerak_pasangan`,
		brw_user_detail.`jmlasetbergerak_pasangan`,
		brw_user_detail.`agama_pasangan`,
		brw_user_detail.`pic_kk`,
		brw_user_detail.`pic_ktp_pasangan`,
		brw_user_detail.`pic_surat_nikah`,
		brw_user_detail.`pic_spt`,
		brw_user_detail.`pic_rekkoran`,
		brw_user_detail.`pic_slipgaji`,
		brw_user_detail.`pic_lapkeuangan`,
		brw_user_detail.`no_akta_perubahan`,
		brw_user_detail.`tgl_akta_perubahan`,
		brw_user_detail.`is_tbk`,
		brw_user_detail.kedudukan_perusahaan,
		brw_user_detail.nama_notaris_akta_pendirian,
		brw_user_detail.kedudukan_notaris_pendirian,
		brw_user_detail.no_sk_kemenkumham_pendirian,
		brw_user_detail.tgl_sk_kemenkumham_pendirian,
		brw_user_detail.nama_notaris_akta_perubahan,
		brw_user_detail.kedudukan_notaris_perubahan,
		brw_user_detail.no_sk_kemenkumham_perubahan,
		brw_user_detail.tgl_sk_kemenkumham_perubahan,
		brw_user_detail.`created_at`,
		brw_user_detail.`updated_at`
	FROM brw_user_detail 
	JOIN brw_pengajuan ON brw_user_detail.brw_id = brw_pengajuan.brw_id;


	/* Insert all pengajuan penghasilan */
	INSERT INTO brw_user_detail_penghasilan_pengajuan
	(
	  `pengajuan_id`,
	  `brw_id2`,
	  `sumber_pengembalian_dana`,
	  `skema_pembiayaan`,
	  `nama_perusahaan`,
	  `alamat_perusahaan`,
	  `rt`,
	  `rw`,
	  `provinsi`,
	  `kab_kota`,
	  `kecamatan`,
	  `kelurahan`,
	  `kode_pos`,
	  `no_telp`,
	  `no_hp`,
	  `surat_ijin`,
	  `no_surat_ijin`,
	  `bentuk_badan_usaha`,
	  `status_pekerjaan`,
	  `usia_perusahaan`,
	  `usia_tempat_usaha`,
	  `departemen`,
	  `jabatan`,
	  `masa_kerja_tahun`,
	  `masa_kerja_bulan`,
	  `nip_nrp_nik`,
	  `nama_hrd`,
	  `no_fixed_line_hrd`,
	  `pengalaman_kerja_tahun`,
	  `pengalaman_kerja_bulan`,
	  `pendapatan_borrower`,
	  `biaya_hidup`,
	  `detail_penghasilan_lain_lain`,
	  `total_penghasilan_lain_lain`,
	  `nilai_spt`,
	  `created_at`,
	  `updated_at` 
	)
	SELECT  brw_pengajuan.`pengajuan_id`,
	  brw_user_detail_penghasilan.`brw_id2`,
	  brw_user_detail_penghasilan.`sumber_pengembalian_dana`,
	  brw_user_detail_penghasilan.`skema_pembiayaan`,
	  brw_user_detail_penghasilan.`nama_perusahaan`,
	  brw_user_detail_penghasilan.`alamat_perusahaan`,
	  brw_user_detail_penghasilan.`rt`,
	  brw_user_detail_penghasilan.`rw`,
	  brw_user_detail_penghasilan.`provinsi`,
	  brw_user_detail_penghasilan.`kab_kota`,
	  brw_user_detail_penghasilan.`kecamatan`,
	  brw_user_detail_penghasilan.`kelurahan`,
	  brw_user_detail_penghasilan.`kode_pos`,
	  brw_user_detail_penghasilan.`no_telp`,
	  brw_user_detail_penghasilan.`no_hp`,
	  brw_user_detail_penghasilan.`surat_ijin`,
	  brw_user_detail_penghasilan.`no_surat_ijin`,
	  brw_user_detail_penghasilan.`bentuk_badan_usaha`,
	  brw_user_detail_penghasilan.`status_pekerjaan`,
	  brw_user_detail_penghasilan.`usia_perusahaan`,
	  brw_user_detail_penghasilan.`usia_tempat_usaha`,
	  brw_user_detail_penghasilan.`departemen`,
	  brw_user_detail_penghasilan.`jabatan`,
	  brw_user_detail_penghasilan.`masa_kerja_tahun`,
	  brw_user_detail_penghasilan.`masa_kerja_bulan`,
	  brw_user_detail_penghasilan.`nip_nrp_nik`,
	  brw_user_detail_penghasilan.`nama_hrd`,
	  brw_user_detail_penghasilan.`no_fixed_line_hrd`,
	  brw_user_detail_penghasilan.`pengalaman_kerja_tahun`,
	  brw_user_detail_penghasilan.`pengalaman_kerja_bulan`,
	  brw_user_detail_penghasilan.`pendapatan_borrower`,
	  brw_user_detail_penghasilan.`biaya_hidup`,
	  brw_user_detail_penghasilan.`detail_penghasilan_lain_lain`,
	  brw_user_detail_penghasilan.`total_penghasilan_lain_lain`,
	  brw_user_detail_penghasilan.`nilai_spt`,
	  brw_user_detail_penghasilan.`created_at`,
	  brw_user_detail_penghasilan.`updated_at` 
	FROM brw_user_detail_penghasilan 
	JOIN brw_pengajuan ON brw_user_detail_penghasilan.brw_id2 = brw_pengajuan.brw_id;
	 
END IF;

   IF CODE = '00000' THEN
   	  GET DIAGNOSTICS nrows = ROW_COUNT;
      RETURN nrows;
  ELSE
        SET result = CONCAT('process update user profile pengajuan failed, error = ',code,', message = ',msg);
        INSERT INTO log_db_app (file_name,description,created_at) VALUES ('update user profile pengajuan',result,NOW());
		RETURN 0;
  END IF;
	
END//
DELIMITER ;

-- Dumping structure for function danasyariah.val_ajukan_kpr
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `val_ajukan_kpr`(_tipe_pendanaan int,
		_tbox1 int,
		_obj_pendanaan decimal(15,2) ,
		_tbox2 int,
		_uang_muka decimal(15,2),
		_tbox3 int,
		_nom_pengajuan decimal(15,2),
		file_ varchar(100),
		line_ int
) RETURNS varchar(1000) CHARSET latin1
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
  	declare min_pinjaman_pra decimal(15,2) DEFAULT 2000000;
	declare min_pinjaman decimal(15,2) default 100000000;
	declare max_pinjaman decimal(15,2) default 2000000000;
	declare sout VARCHAR(1000) default '';	
			
	declare exit handler for sqlexception
     begin
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('failed, error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (file_,line_,sout,now());
     end;
     
   if ( 1 !=_tipe_pendanaan and 2 != _tipe_pendanaan) then
   	set sout = '-1';
   	insert into log_db_app (file_name,line,description,created_at) values (file_,line_,'Tipe pendanaan harus 1 atau 2',NOW());
   	
   	return sout;
	end if;
	
	
	if (1 = _tipe_pendanaan) then
		if (_obj_pendanaan < min_pinjaman ) then
			set sout = concat(sout,_tbox1,';','Object pendanaan tidak boleh kurang dari 100 Juta ','#');		
		end if;
		
		if (_nom_pengajuan < min_pinjaman_pra ) then
			set sout = concat(sout,_tbox3,';','Nominal pengajuan tidak boleh kurang dari 20 Juta ','#');		
		end if;
		
		if (_nom_pengajuan > max_pinjaman ) then
			set sout = concat(sout,_tbox3,';','Nominal pengajuan tidak boleh melebihi 2 Milyar ','#');		
		end if;
		
		if (_nom_pengajuan > _obj_pendanaan ) then
			set sout = concat(sout,_tbox3,';','Nominal pengajuan tidak boleh melebihi objek pendanaan ','#');		
		end if;
		
		if (_uang_muka > 0 ) then
			set sout = concat(sout,_tbox2,';','Uang muka harus 0 ','#');		
		end if;
		
   else
        if (_obj_pendanaan - _uang_muka != _nom_pengajuan) then
			set sout = concat(sout,_tbox3,';','Nominal pengajuan tidak sama dengan objek pendanaan dikurangi uang muka ','#');
		end if;
		
		if (_obj_pendanaan < min_pinjaman ) then
			set sout = concat(sout,_tbox1,';','Object pendanaan tidak boleh kurang dari 100 Juta ','#');		
		end if;
		
		if (_obj_pendanaan - _uang_muka < min_pinjaman ) then
			set sout = concat(sout,_tbox3,';','Nominal pengajuan tidak boleh kurang dari 100 Juta ','#');		
		end if;
		
		if (_obj_pendanaan - _uang_muka > max_pinjaman ) then
			set sout = concat(sout,_tbox3,';','Nominal pengajuan tidak boleh melebihi 2 Milyar ','#');		
		end if;
		
		if (_nom_pengajuan > _obj_pendanaan ) then
			set sout = concat(sout,_tbox3,';','Nominal pengajuan tidak boleh melebihi objek pendanaan ','#');		
		end if;
		
		if (_uang_muka < _obj_pendanaan / 20 ) then
			set sout = concat(sout,_tbox2,';','Uang muka minimal 5% dari objek pendanaan','#');		
		end if;
   end if;
   
	if ( '' = sout ) then
	  set sout = '1';
	end if ;
	
	return sout; 
END//
DELIMITER ;

-- Dumping structure for function danasyariah.val_register_user
DELIMITER //
CREATE DEFINER=`dbdanasyariah`@`%` FUNCTION `val_register_user`(_user_type int, #lender = 1 , borrower = 2
		_tbox1 int,#0 = off , 1 = on 
		_npwp varchar(25) ,
		_tbox2 int,#0 = off , 1 = on 
		_nik varchar(25),
		_tbox3 int,#0 = off , 1 = on 
		_hp varchar(25),
		_tbox4 int,#0 = off , 1 = on
		_email varchar(25),
		_tbox5 int,#0 = off , 1 = on
		_username varchar(50),
		file_ varchar(100),
		line_ int
) RETURNS varchar(1000) CHARSET latin1
begin
   
	declare code char(5) default '00000';
  	declare msg text;
  	declare nrows int;
	declare sout varchar(1000) default '';	
			
	declare exit handler for sqlexception
     begin
       get diagnostics condition 1
         code = returned_sqlstate, msg = message_text;
	    set sout = concat('failed, error = ',code,', message = ',msg);
       insert into log_db_app (file_name,line,description,created_at) values (file_,line_,sout,now());
     end;
   
	
	#-----------------------------validasi input parameter --------------------------------  
	
	if ( 1 > _user_type or 2 < _user_type) then
		set sout = concat(sout,_tbox1,';','type user harus 1-lender atau 2-borrower ','#');
   	return sout;
	end if;
	
	if (  0 > _tbox1 or 1 < _tbox1 ) then
		set sout = concat(sout,_tbox1,';','index tbox1 harus 0 atau 1 ','#');
   	return sout;
	end if;
	
	if (  0 > _tbox2 or 1 < _tbox2 ) then
		set sout = concat(sout,_tbox2,';','index tbox2 harus 0 atau 1 ','#');
   	return sout;
	end if;
	
	if ( 0 > _tbox3 or 1 < _tbox3 ) then
		set sout = concat(sout,_tbox3,';','index tbox3 harus 0 atau 1 ','#');
   	return sout;
	end if;
	
	if ( 0 > _tbox4 or 1 < _tbox4 ) then
		set sout = concat(sout,_tbox4,';','index tbox4 harus 0 atau 1 ','#');
   	return sout;
	end if;
	
	if ( 0 > _tbox5 or 1 < _tbox5 ) then
		set sout = concat(sout,_tbox5,';','index tbox5 harus 0 atau 1 ','#');
   	return sout;
	end if;
	
		
	#--------------------------validasi npwp--------------------------------------------
	
	if ( 1 = _tbox1 ) then
	
		if ( 15 != length(_npwp) ) then
			set sout = concat(sout,_tbox1,';','panjang npwp harus 15 digit ','#');
			return sout;
		end if;
		
		if (1 = _user_type ) then
				select count(1), investor_id into @flag_ , @investor_id_
							from 	detil_investor		
							where trim(no_npwp_investor) = trim(_npwp);	
							
				if (0 != @flag_) then
					set sout = concat(sout,_tbox1,';','npwp sudah terdaftar di investor_id =  ', @investor_id_ ,'#');
					return sout;
				end if;	
		else
				select count(1), brw_id into @flag_ , @brw_id_
							from 	brw_user_detail		
							where trim(npwp) = trim(_npwp)
									or trim(npwp_perusahaan) = trim(_npwp);	
				if (0 != @flag_) then
					set sout = concat(sout,_tbox1,';','npwp sudah terdaftar di borrower_id =  ', @brw_id_ ,'#');
					return sout;
				end if;		
		end if;
	end if;
			
	#--------------------------validasi nik--------------------------------------------
		
	if ( 1 = _tbox2 ) then
	
		if ( 16 != length(_nik) ) then
			set sout = concat(sout,_tbox2,';','panjang nik harus 16 digit ','#');
			return sout;
		else
				if ( '0' = substr(_nik,1,1) or '4' = substr(_nik,1,1) ) then
					set sout = concat(sout,_tbox2,';','nik digit awalnya tidak boleh 0 atau 4','#');
					return sout;
				end if;
		end if;

		if (1 = _user_type ) then
			select count(1), investor_id into @flag_ , @investor_id_
						from 	detil_investor		
						where trim(no_ktp_investor) = trim(_nik);
						
			if (0 != @flag_) then
				set sout = concat(sout,_tbox2,';','ktp sudah terdaftar di investor_id =  ', @investor_id_ ,'#');
				return sout;
			end if;
		else
			select count(1), brw_id into @flag_ , @brw_id_
						from 	brw_user_detail		
						where trim(ktp) = trim(_nik);
						
			if (0 != @flag_) then
				set sout = concat(sout,_tbox2,';','ktp sudah terdaftar di borrower_id =  ', @brw_id_ ,'#');
				return sout;
			end if;
		end if;	
	end if;
	
	#--------------------------validasi no hp--------------------------------------------

	if ( 1 = _tbox3 ) then
	
		if ( 13 < length(_hp) or 9 > length(_hp) ) then
			set sout = concat(sout,_tbox3,';','panjang no hp antara 9 sampai 13 digit ','#');
			return sout;
		else
				if ( '08' != substr(_hp,1,2) or '628' != substr(_hp,1,3) ) then
					set sout = concat(sout,_tbox3,';','no hp harus berawalan 08 atau 628','#');
					return sout;
				end if;
		end if;
		
		if (1 = _user_type ) then
				select count(1), investor_id into @flag_ , @investor_id_
							from 	detil_investor		
							where trim(phone_investor) = trim(_hp);
								
				if (0 != @flag_) then
					set sout = concat(sout,_tbox3,';','no hp sudah terdaftar di investor_id =  ', @investor_id_ ,'#');
					return sout;
				end if;
		
		else 
				select count(1), brw_id into @flag_ , @brw_id_
							from 	brw_user_detail		
							where trim(no_tlp) = trim(_hp);
								
				if (0 != @flag_) then
					set sout = concat(sout,_tbox3,';','no hp sudah terdaftar di borrower_id =  ', @brw_id_ ,'#');
					return sout;
				end if;
		end if;
		
	end if;
	
	#-------------------------------------email ---------------------------------------------
	
	if ( 1 = _tbox4 ) then
	
		if (1 = _user_type ) then
				select count(1), investor_id into @flag_ , @investor_id_
							from 	detil_investor		
							where trim(email) = trim(_email);
								
				if (0 != @flag_) then
					set sout = concat(sout,_tbox4,';','email sudah terdaftar di investor_id =  ', @investor_id_ ,'#');
					return sout;
				end if;
		
		else 
				select count(1), brw_id into @flag_ , @brw_id_
							from 	brw_user_detail		
							where trim(email) = trim(_email);
								
				if (0 != @flag_) then
					set sout = concat(sout,_tbox4,';','email sudah terdaftar di borrower_id =  ', @brw_id_ ,'#');
					return sout;
				end if;
		end if;

	end if;
	
	
	#---------------------------------------- Akun --------------------------------------------
	
	if ( 1 = _tbox5 ) then
	
		if (1 = _user_type ) then
				select count(1), investor_id into @flag_ , @investor_id_
							from 	detil_investor		
							where trim(username) = trim(_username);
								
				if (0 != @flag_) then
					set sout = concat(sout,_tbox4,';','akun sudah terdaftar di investor_id =  ', @investor_id_ ,'#');
					return sout;
				end if;
		
		else 
				select count(1), brw_id into @flag_ , @brw_id_
							from 	brw_user_detail		
							where trim(username) = trim(_username);
								
				if (0 != @flag_) then
					set sout = concat(sout,_tbox4,';','akun sudah terdaftar di borrower_id =  ', @brw_id_ ,'#');
					return sout;
				end if;
		end if;

	end if;
	
					
	if ( '' = sout ) then
	  set sout = '1';
	end if ;
	
	return sout; 
end//
DELIMITER ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
