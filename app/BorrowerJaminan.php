<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerJaminan extends Model
{
    protected $primaryKey = 'jaminan_id';
    protected $table = 'brw_jaminan';
    protected $guard = 'borrower';

    protected $fillable = [
        'pengajuan_id', 'jaminan_nama', 
        'jaminan_nomor', 'jaminan_jenis',
        'jaminan_nilai', 'jaminan_detail', 
        'status'
    ];
}