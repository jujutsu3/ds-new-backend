<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

// use Illuminate\Auth\Passwords\CanResetPassword;

/**
 * @property \App\DetilInvestor $detilInvestor
 * @property \App\DetilInvestor $location
 * @property \Illuminate\Support\Collection<\App\InvestorPengurus> $admins
 * @property \Illuminate\Support\Collection<\App\InvestorPemegangSaham> $shareholders
 * @property \Illuminate\Support\Collection<\App\InvestorContacts> $contacts
 */
class Investor extends Authenticatable implements JWTSubject
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_SUSPENDED = 'suspend';
    public const STATUS_EXPIRED = 'expired';
    use Notifiable;

    protected $table = 'investor';

    protected $fillable = [
        'username', 'email', 'password', 'email_verif', 'ref_number', 'status', 'provider', 'provider_id', 'keterangan', 'suspended_by',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rekeningInvestor()
    {
        return $this->hasOne('App\RekeningInvestor');
    }

    public function detilInvestor()
    {
        return $this->hasOne('App\DetilInvestor');
    }

    public function mutasiInvestor()
    {
        return $this->hasMany('App\MutasiInvestor');
    }

    public function perusahaanInvestor()
    {
        return $this->hasOne('App\PerusahaanInvestor');
    }

    public function pendanaanAktif()
    {
        return $this->hasMany('App\PendanaanAktif');
    }

    public function penarikanDana()
    {
        return $this->hasMany('App\PenarikanDana');
    }
    // public function activeCart(){
    //     return $this->hasMany('App\Cart');
    // }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new Notifications\ResetPasswordNotification($token));
    }

    public function subscribe()
    {
        return $this->hasMany('App\Subscribe');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function admins()
    {
        return $this->hasMany(InvestorPengurus::class, 'investor_id', 'id');
    }

    public function shareholders()
    {
        return $this->hasMany(InvestorPemegangSaham::class, 'investor_id', 'id');
    }

    public function contacts()
    {
        return $this->hasMany(InvestorContacts::class, 'investor_id', 'id');
    }

    public function location()
    {
        return $this->hasOne(InvestorLocation::class, 'investor_id', 'id');
    }
}
