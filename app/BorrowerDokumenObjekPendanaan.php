<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDokumenObjekPendanaan extends Model
{
    protected $primaryKey = 'id_pengajuan';
    protected $table = 'brw_dokumen_objek_pendanaan';
    protected $guarded = [];
}
