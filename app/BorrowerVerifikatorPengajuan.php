<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerVerifikatorPengajuan extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'brw_verifikator_pengajuan';
    protected $guarded = [];
}
