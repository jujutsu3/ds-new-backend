<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerCartMaterialOrder extends Model
{
    public $timestamps = false;
    
    protected $primaryKey = 'id';
    protected $table = 'brw_cart_material_order';
    protected $guarded = [];
}
