<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\Interfaces\Investable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

class LoginBorrower extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table = 'brw_user';
	protected $primaryKey = 'brw_id';
    protected $guard = 'borrower';


	protected $fillable = [
        'username', 'email', 'password', 'email_verif','ref_number', 'status', 'password_expiry_days', 'password_updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function detilBorrower() {
        return $this->hasOne('App\BorrowerDetails', 'brw_id', 'brw_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new Notifications\ResetPasswordNotification($token, 'borrower'));
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
