<?php

namespace App;

use Illuminate\Support\Facades\App;

class InvestorIndividu extends Investor
{
    use FieldFileHandler;
    use FieldDateHandler;

    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const PASSWORD = 'password';
    public const NAMA = 'nama';
    public const NIK = 'nik';
    public const JENIS_KELAMIN = 'jenis_kelamin';
    public const TEMPAT_LAHIR = 'tempat_lahir';
    public const TANGGAL_LAHIR = 'tanggal_lahir';
    public const AGAMA = 'agama';
    public const STATUS_PERNIKAHAN = 'status_pernikahan';
    public const KEWARGANEGARAAN = 'kewarganegaraan';
    public const WARGA_NEGARA = 'warga_negara';
    public const ALAMAT = 'alamat';
    public const PROVINSI = 'provinsi';
    public const KABUPATEN = 'kabupaten';
    public const KECAMATAN = 'kecamatan';
    public const KELURAHAN = 'kelurahan';
    public const KODE_POS = 'kode_pos';
    public const LONG = 'long';
    public const LAT = 'lat';
    public const ALT = 'alt';
    public const PENDIDIKAN = 'pendidikan';
    public const PEKERJAAN = 'pekerjaan';
    public const BIDANG_PEKERJAAN = 'bidang_pekerjaan';
    public const BIDANG_ONLINE = 'bidang_online';
    public const LAMA_KERJA = 'lama_kerja';
    public const ESTIMASI_PENGHASILAN = 'estimasi_penghasilan';
    public const SUMBER_PENGHASILAN = 'sumber_penghasilan';
    public const REKENING = 'rekening';
    public const BANK = 'bank';
    public const NAMA_PEMILIK = 'nama_pemilik';
    public const NO_TELEPON = 'no_telp';
    public const AHLI_WARIS = 'ahli_waris';
    public const AHLI_WARIS_NIK = 'NIK';
    public const AHLI_WARIS_HUBUNGAN = 'hubungan';
    public const AHLI_WARIS_KODE_OPERATOR = 'kode_operator';
    public const AHLI_WARIS_TELP = 'telp';
    public const AHLI_WARIS_ALAMAT = 'alamat';
    public const NPWP = 'npwp';

    /**
     * @return string[]
     */
    public static function getAttributeName()
    {
        if (App::isLocale('en')) {
            return [
                self::USERNAME => 'Username',
                self::EMAIL => 'Email',
                self::PASSWORD => 'Password',
                self::NAMA => 'Name',
                self::NIK => 'Identity',
                self::JENIS_KELAMIN => 'Gender',
                self::TEMPAT_LAHIR => 'Place of Birth',
                self::TANGGAL_LAHIR => 'Date of Birth',
                self::AGAMA => 'Religion',
                self::STATUS_PERNIKAHAN => 'Marital Status',
                self::KEWARGANEGARAAN => 'Nationality',
                self::WARGA_NEGARA => 'Citizen',
                self::ALAMAT => 'Address',
                self::PROVINSI => 'Province',
                self::KABUPATEN => 'Regency',
                self::KECAMATAN => 'District',
                self::KELURAHAN => 'Sub District',
                self::KODE_POS => 'Postal Code',
                self::LONG => 'Long',
                self::LAT => 'Lat',
                self::ALT => 'Alt',
                self::PENDIDIKAN => 'Education',
                self::PEKERJAAN => 'Occupation',
                self::BIDANG_PEKERJAAN => 'Field of Work',
                self::BIDANG_ONLINE => 'Field of Work Online',
                self::LAMA_KERJA => 'Work Experience',
                self::ESTIMASI_PENGHASILAN => 'Income',
                self::SUMBER_PENGHASILAN => 'Source of Income',
                self::REKENING => 'Bank Account',
                self::BANK => 'Bank',
                self::NAMA_PEMILIK => 'Bank Account Owner',
                self::NO_TELEPON => 'Phone',
            ];
        }

        return [
            self::USERNAME => 'Username',
            self::EMAIL => 'Email',
            self::PASSWORD => 'Password',
            self::NAMA => 'Nama',
            self::NIK => 'Identitas',
            self::JENIS_KELAMIN => 'Jenis Kelamin',
            self::TEMPAT_LAHIR => 'Tempat Lahir',
            self::TANGGAL_LAHIR => 'Tanggal Lahir',
            self::AGAMA => 'Agama',
            self::STATUS_PERNIKAHAN => 'Status Pernikahan',
            self::KEWARGANEGARAAN => 'Kewarganegaraan',
            self::WARGA_NEGARA => 'Warga Negara',
            self::ALAMAT => 'Alamat',
            self::PROVINSI => 'Provinsi',
            self::KABUPATEN => 'Kabupaten',
            self::KECAMATAN => 'Kecamatan',
            self::KELURAHAN => 'Kelurahan',
            self::KODE_POS => 'Kode Pos',
            self::LONG => 'Long',
            self::LAT => 'Lat',
            self::ALT => 'Alt',
            self::PENDIDIKAN => 'Pendidikan',
            self::PEKERJAAN => 'Pekerjaan',
            self::BIDANG_PEKERJAAN => 'Bidang Pekerjaan',
            self::BIDANG_ONLINE => 'Bidang Online',
            self::LAMA_KERJA => 'Lama Kerja',
            self::ESTIMASI_PENGHASILAN => 'Estimasi Penghasilan',
            self::SUMBER_PENGHASILAN => 'Sumber Penghasilan',
            self::REKENING => 'Rekening',
            self::BANK => 'Bank',
            self::NAMA_PEMILIK => 'Nama Pemilik',
            self::NO_TELEPON => 'No Telepon',
        ];
    }
}
