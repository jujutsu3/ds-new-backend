<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MasterPekerjaan extends Model
{
    public const WIRASWASTA = 'Wiraswasta';
    public const PELAJAR = 'Pelajar/Mahasiswa';
    public const TIDAK_BEKERJA = 'Tidak bekerja/bukan pelajar';
    protected $table = 'm_pekerjaan';
    protected $primaryKey = 'id_pekerjaan';
}
