<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property \App\BorrowerDetails $detail
 * @property \Illuminate\Support\Collection<\App\InvestorPengurus> $admins
 * @property \Illuminate\Support\Collection<\App\InvestorPemegangSaham> $shareholders
 * @property \Illuminate\Support\Collection<\App\InvestorContacts> $contacts
 */
class Borrower extends Authenticatable
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_NOTPREAPPROVED = 'notpreapproved';
    public const STATUS_NOTFILLED = 'notfilled';
    public const STATUS_SUSPENDED = 'suspend';
    public const STATUS_EXPIRED = 'expired';
    // use Notifiable;
    protected $table = 'brw_user';
    protected $primaryKey = 'brw_id';
    protected $guard = 'borrower';

    protected $fillable = [
        'username', 'email', 'password', 'email_verif', 'ref_number', 'status',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function admins()
    {
        return $this->hasMany(BorrowerPengurus::class, 'brw_id', 'brw_id');
    }

    public function shareholders()
    {
        return $this->hasMany(BorrowerPemegangSaham::class, 'brw_id', 'brw_id');
    }

    public function contacts()
    {
        return $this->hasMany(BorrowerContact::class, 'brw_id', 'brw_id');
    }

    public function detail()
    {
        return $this->hasOne(BorrowerDetails::class, 'brw_id', 'brw_id');
    }
}
