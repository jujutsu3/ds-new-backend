<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterTujuanPembiayaan extends Model
{
    protected $table = 'm_tujuan_pembiayaan';
    protected $primaryKey = 'id';
}
