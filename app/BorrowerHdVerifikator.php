<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerHdVerifikator extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_hd_verifikator';
    protected $guarded = [];
    // public $timestamps = false;
}
