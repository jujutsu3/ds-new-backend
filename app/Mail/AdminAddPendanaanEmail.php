<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminAddPendanaanEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $pendanaan, $rekening, $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $nominal, $namaproyek)
    {
        $this->username = $username;
        $this->nominal = $nominal;
        $this->namaproyek = $namaproyek;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.admin_pendanaan')->with('username', $this->username)->with('nominal', $this->nominal)->with('namaproyek', $this->namaproyek);
    }
}
