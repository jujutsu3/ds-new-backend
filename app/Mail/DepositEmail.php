<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class DepositEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $amount, $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($amount, $user)
    {
        $this->amount = $amount;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('email.email_deposit')->with('amount', $this->amount)->with('user', $this->user);
		return $this->view('email.email_deposit')->subject('Top Up Pendanaan')->from(config("app.email_no_reply"))->with('amount', $this->amount)->with('user', $this->user);
    }
}


// class DepositEmail extends Mailable
// {
    // use Queueable, SerializesModels;
    // //protected $amount, $user;
    // protected $user, $amount;
    // /**
     // * Create a new message instance.
     // *
     // * @return void
     // */
   // // public function __construct($amount, $user)
    // public function __construct($user, $amount)
    // {
        // // $this->amount = $amount;
        // $this->user = $user;
        // $this->amount = $amount;
    // }

    // /**
     // * Build the message.
     // *
     // * @return $this
     // */
    // public function build()
    // {
        // //return $this->view('email.SendInfoPengajuanBorrower')->subject('Pengajuan Pendanaan')->from(config("app.email_no_reply"))->with('data', $this->data);        
		// return $this->view('email.email_deposit')->subject('Pengajuan Pendanaan')->from(config("app.email_no_reply"))->with('user', $this->user)->with('amount', $this->amount);
    // }
// }
