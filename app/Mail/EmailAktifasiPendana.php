<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailAktifasiPendana extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $web;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $web=false)
    {
        $this->user = $user;
        $this->web = $web;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   

        if($this->web){
            return $this->subject("Aktivasi Pendaftaran Pendana - Danasyariah.id")->view('email.email_confirm')->with('data', $this->user);
        }else{
            return $this->subject("Aktivasi Pendaftaran Pendana - Danasyariah.id")->view('email.email_confirm_mobile')->with('data', $this->user);
        }
       
    }
}
