<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailKonfirmasiCicilan extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('email.konfirmasi_cicilan_borrower')->subject('Pembayaran Cicilan '.$this->data->no_invoice.' '.$this->data->status_pembayaran)
        ->from(config("app.email_no_reply"))->with('data', $this->data);        
    }
}