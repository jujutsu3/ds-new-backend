<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailSPKVerifikator extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
       $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        //attach pdf file
        $pdf = \PDF::loadView('pages.borrower.pdf_spk_verifikator',  ['data'=> $this->data]);

        return $this->view('email.email_spk_occasio')
            ->attachData($pdf->output(), 'SPK-Danasyariah.pdf')
            ->subject('Surat Perintah Kerja Verifikasi - DANA SYARIAH')
            ->from(config("app.email_no_reply"))
            ->with('data', $this->data);
    }
}
