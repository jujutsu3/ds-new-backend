<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class HasilAnalisaKelengkapanEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $penerimaPendanaan;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($penerimaPendanaan)
    {
        // array
        $this->penerimaPendanaan = $penerimaPendanaan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.hasilAnalisaKelengkapan')->subject('STATUS PENGAJUAN DANA RUMAH TAHAP II - DANA SYARIAH')->from(config("app.email_no_reply"))->with('penerimaPendanaan', $this->penerimaPendanaan);
    }
}
