<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailSPKOccasio extends Mailable
{
    use Queueable, SerializesModels;
    protected $penerimaPendanaan;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        // array
        $this->penerimaPendanaan = $penerimaPendanaan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.email_spk_occasio')
            ->subject('Surat Perintah Kerja Verifikasi - DANA SYARIAH')
            ->from(config("app.email_no_reply"));
            // ->with('penerimaPendanaan', $this->penerimaPendanaan);
    }
}
