<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendInfoPengajuanAnalys extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $data_analis)
    {
        $this->data = $data;
        $this->data_analis = $data_analis;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('email.SendInfoPengajuanAnalys')->subject('Pengajuan Pendanaan')->from(config("app.email_no_reply"))->with('data', $this->data)->with('data_analis', $this->data_analis);        
    }
}