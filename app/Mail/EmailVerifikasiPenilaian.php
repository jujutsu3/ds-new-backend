<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerifikasiPenilaian extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $data2, $email_ke)
    {
        $this->data = $data;
        $this->data2 = $data2;
        $this->email_ke = $email_ke;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('email.konfirmasi_verifikasi_penilaian')->subject('Pengajuan Pendanaan '.$this->data->status)->from(config("app.email_no_reply"))->with('data', $this->data)->with('data2', $this->data2)->with('email_ke', $this->email_ke);        
    }
}