<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class AutoDebetReg extends Mailable
{
    use Queueable, SerializesModels;
    protected $dataReg;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dataReg)
    {
        // array
        $this->dataReg = $dataReg;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.autoDebetRegEmail')->subject('AUTO DEBET PRIVY - DANA SYARIAH')->from(config("app.email_no_reply"))->with('dataReg', $this->dataReg);
    }
}
