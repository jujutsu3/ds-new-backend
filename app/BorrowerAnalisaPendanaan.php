<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerAnalisaPendanaan extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_analisa_pendanaan';
    protected $guarded = [];

    public function getData()
    {
        return static::orderBy('pengajuan_id','desc');
    }

}
