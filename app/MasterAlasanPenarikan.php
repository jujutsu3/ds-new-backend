<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MasterAlasanPenarikan extends Model
{
    protected $table = 'm_alasan_penarikan';
    protected $primaryKey = 'id';
}
