<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AhliWarisInvestor extends Model
{
    use FieldPhoneHandler;

    protected $table = 'ahli_waris_investor';
    protected $primaryKey = 'id_investor';
    protected $guarded = [];
}
