<?php

namespace App\Constants;

class KYCStatus
{

    public const COMPLETE_TRUE = "complete";
    public const COMPLETE_PARTIAL = "partial";
    public const COMPLETE_FALSE = "incomplete";
}
