<?php

namespace App\Constants;

class JenisKelamin
{
    public const PEREMPUAN = 2;
    public const LAKILAKI = 1;
}
