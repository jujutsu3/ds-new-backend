<?php

namespace App\Constants;

/**
 * Reference untuk kolom `status` pada `brw_pengajuan`
 *
 * @see \App\BorrowerPengajuan
 */
class BorrowerPengajuanStatus
{
    public const STATUS_DRAFT = 99;
    public const STATUS_BARU = 0;
    public const STATUS_OK_ANALYST_1 = 1;
    public const STATUS_KO_ANALYST_1 = 2;
    public const STATUS_OK_ANALYST_2 = 3;
    public const STATUS_DONE = 4;
    public const STATUS_OK_COMMITTEE = 5;
    public const STATUS_KO_COMMITTEE = 6;
    public const STATUS_OK_SP3 = 7;
    public const STATUS_FUNDING = 8;
    public const STATUS_WITHDRAWAL_REQUEST = 9;
    public const STATUS_WITHDRAWAL = 10;
    public const STATUS_COMPLETENESS_CHECK = 11;
    public const STATUS_KO_COMPLETENESS_CHECK = 12;

    /** @var string[] Keperluan back office */
    public static $statusNames = [
        self::STATUS_DRAFT => 'Draft',
        self::STATUS_BARU => 'Pengajuan Baru',
        self::STATUS_OK_ANALYST_1 => 'Diterima Analyst 1',
        self::STATUS_KO_ANALYST_1 => 'Ditolak Analyst',
        self::STATUS_OK_ANALYST_2 => 'Diterima Analyst 2',
        self::STATUS_DONE => 'Proyek Selesai',
        self::STATUS_OK_COMMITTEE => 'Diterima Komite',
        self::STATUS_KO_COMMITTEE => 'Ditolak Komite',
        self::STATUS_OK_SP3 => 'Persetujuan SP3',
        self::STATUS_FUNDING => 'Penggalangan Dana',
        self::STATUS_WITHDRAWAL_REQUEST => 'Permintaan Pencairan',
        self::STATUS_WITHDRAWAL => 'Pencairan Dana',
        self::STATUS_COMPLETENESS_CHECK => 'Analisa Kelengkapan Data',
        self::STATUS_KO_COMPLETENESS_CHECK => 'Ditolak Kelengkapan Data'
    ];

    /** @var string[] Keperluan penggunaan pada mobile listing pendanaan */
    public static $alternateStatusNames = [
        self::STATUS_DRAFT => 'Draft',
        self::STATUS_BARU => 'Pengajuan Baru',
        self::STATUS_OK_ANALYST_1 => 'Verifikasi',
        self::STATUS_KO_ANALYST_1 => 'Ditolak',
        self::STATUS_OK_ANALYST_2 => 'Verifikasi',
        self::STATUS_DONE => 'Proyek Selesai',
        self::STATUS_OK_COMMITTEE => 'Verifikasi',
        self::STATUS_KO_COMMITTEE => 'Ditolak',
        self::STATUS_OK_SP3 => 'Verifikasi',
        self::STATUS_FUNDING => 'Pendanaan Berjalan',
        self::STATUS_WITHDRAWAL_REQUEST => 'Dalam Proses Pencairan',
        self::STATUS_WITHDRAWAL => 'Fase Pembayaran',
        self::STATUS_COMPLETENESS_CHECK => 'Verifikasi',
        self::STATUS_KO_COMPLETENESS_CHECK => 'Ditolak'
    ];
}
