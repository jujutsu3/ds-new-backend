<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDanaRumahLain extends Model
{
    // use Notifiable;
    protected $primaryKey = 'id';
    protected $table = 'brw_pendanaan_rumah_lain';
    protected $guard = 'borrower';

    protected $fillable = [
        'pengajuan_id', 'rumah_ke', 'status', 'bank', 'plafond', 'jangka_waktu', 'outstanding', 'angsuran', 'updated_at'
    ];

}
