<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;


class VerifyComplete extends Model
{
    protected $table = 'verify_completeid';
    protected $fillable = ['verify_id', 'nik', 'name', 'valid_name', 'birthdate', 'valid_birthdate', 'birthplace', 'valid_birthplace', 'address', 'valid_address', 'selfie_photo', 'valid_selfie_photo', 'updated_at'];

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y H:i:s');
    }
}
