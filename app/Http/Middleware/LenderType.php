<?php

namespace App\Http\Middleware;

use App\DetilInvestor;
use App\Services\InvestorService;
use Closure;

class LenderType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        $investor = InvestorService::getInvestor();
        if ($investor->detilInvestor === null) {
            return $next($request);
        }

        $tipePengguna = $investor->detilInvestor->tipe_pengguna;
        if ($tipePengguna !== null && $tipePengguna !== (int) $type) {
            abort(403, 'Tipe pengguna tidak sesuai');
        }

        return $next($request);
    }
}
