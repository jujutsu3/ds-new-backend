<?php

namespace App\Http\Middleware;

use Closure;
use App\Investor;
use Auth;
use App\Http\Controllers\UserController;
use App\MasterProvinsi;
use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;
use App\DetilInvestor;
use App\MasterKodeOperator;
use DB;

class UserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
            $user = Auth::user();

            if ($user->email_verif) {

                return redirect('/user/userverification')->with('confirmationemail', true);
            } else if ($user->status == 'notfilled') {

                return redirect()->route('user.verif');
                // $master_provinsi = MasterProvinsi::distinct('kode_provinsi','nama_provinsi')->get(['kode_provinsi','nama_provinsi']);
                // $master_agama = MasterAgama::all();
                // // $master_asset = MasterAsset::all();
                // // $master_badan_hukum = MasterBadanHukum::all();
                // $master_bank = MasterBank::all();
                // $master_bidang_pekerjaan = MasterBidangPekerjaan::all();
                // $master_jenis_kelamin = MasterJenisKelamin::all();
                // $master_jenis_pengguna = MasterJenisPengguna::all();
                //  $master_kawin = MasterKawin::all();
                // // $master_kepemilikan_rumah = MasterKepemilikanRumah::all();
                // $master_negara = MasterNegara::all();
                // $master_kode_operator = DB::table("m_kode_operator_negara")->get();
                // $master_online = MasterOnline::all();
                // $master_pekerjaan = MasterPekerjaan::all();
                // $master_pendapatan = MasterPendapatan::all();
                // $master_pendidikan = MasterPendidikan::all();
                // $master_pengalaman_kerja = MasterPengalamanKerja::all();
                // $hubunganAhliWaris = DB::table("m_hub_ahli_waris")->get();
                // $tipePengguna = DB::table("m_jenis_pengguna")->get();

                // return redirect()->route('user.verif')->with([
                //         'dataverification' => true,
                //         'master_provinsi' => $master_provinsi,
                //         'master_agama' => $master_agama,
                //         'master_hubungan_ahli_waris' => $hubunganAhliWaris,
                //         // 'master_asset' => $master_asset,
                //         // 'master_badan_hukum' => $master_badan_hukum,
                //         'master_bank' => $master_bank,
                //         'master_kode_operator' => $master_kode_operator,
                //         'master_bidang_pekerjaan' => $master_bidang_pekerjaan,
                //         'master_jenis_kelamin' => $master_jenis_kelamin,
                //         'master_jenis_pengguna' => $master_jenis_pengguna,
                //          'master_kawin' => $master_kawin,
                //         // 'master_kepemilikan_rumah' => $master_kepemilikan_rumah,
                //         'master_negara' => $master_negara,
                //         'master_online' => $master_online,
                //         'master_pekerjaan' => $master_pekerjaan,
                //         'master_pendapatan' => $master_pendapatan,
                //         'master_pendidikan' => $master_pendidikan,
                //         'master_pengalaman_kerja' => $master_pengalaman_kerja,

                //     ]);
            } else if ($user->status == 'expired') {

                return redirect('user/ubahPasswordExpired');
            } else if ($user->status == 'suspend') {

                return redirect('/user/userverification')->with('suspend', true);
            }
        } elseif (Auth::guard('borrower')->check()) {
            $borrower = Auth::guard('borrower')->user();
            if ($borrower->status == 'notfilled') {
                return redirect('borrower/lengkapi_profile');
            } else if ($borrower->status == 'reject') {
                return redirect('borrower/status_reject');
            } else if ($borrower->status == 'pending') {
                return redirect('borrower/status_pending');
            } else if ($borrower->status == 'suspend') {
                return redirect('/#loginModal');
            } else if ($borrower->status == 'active') {
                return redirect('borrower/dashboardPendanaan');
            }
            /* ARL SIMPLIFIKASI */ else if ($borrower->status == 'notpreapproved') {
                return redirect('borrower/lengkapi_profile_pendanaan_simple');
            }
        } else {
            return redirect('/#loginModal');
        }

        return $next($request);
    }
}
