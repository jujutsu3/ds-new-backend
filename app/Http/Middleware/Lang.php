<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;

class Lang
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $type
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($lang = $request->header('X-LANG'), ['en', 'id'])) {
            App::setLocale($lang);
        }

        return $next($request);
    }
}
