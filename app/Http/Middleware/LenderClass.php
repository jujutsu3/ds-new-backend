<?php

namespace App\Http\Middleware;

use App\Services\InvestorService;
use Closure;

class LenderClass
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        $investor = InvestorService::getInvestor();

        if ($investor->detilInvestor === null
            || $investor->detilInvestor->lender_class !== (int) $type
        ) {
            abort(403, 'Kelas lender pengguna tidak sesuai');
        }

        return $next($request);
    }
}
