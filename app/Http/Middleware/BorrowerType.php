<?php

namespace App\Http\Middleware;

use App\DetilInvestor;
use App\Services\BorrowerService;
use App\Services\InvestorService;
use Closure;

class BorrowerType
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $type
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        $borrower = BorrowerService::getBorrower();
        if ($borrower->detail === null) {
            return $next($request);
        }
        $tipePengguna = $borrower->detail->tipe_pengguna;
        if ($tipePengguna !== null && $tipePengguna !== (int) $type) {
            abort(403, 'Tipe pengguna tidak sesuai');
        }

        return $next($request);
    }
}
