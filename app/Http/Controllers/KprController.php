<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\LoginBorrower;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Middleware\brwCheck;
use App\Http\Middleware\Check;
use App\Http\Middleware\StatusPendanaan;
use App\PendanaanAktif;
use Illuminate\Http\Request;
use Lang;
use Auth;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\LogAkadDigiSignBorrower;
use App\BorrowerRekening;
use App\BorrowerPendanaan;
use App\Proyek;
use App\TeksNotifikasi;
use App\CheckUserSign;
use App\LogSP3Borrower;
use DB;
use App\Http\Middleware\StatusProyek;
use App\Http\Middleware\CheckUserSessionBorrower;
use Carbon\Carbon;


class KprController extends Controller
{	
	public function __construct(){
    $this->middleware(StatusProyek::class);
		
		
		$getPendanaan = BorrowerPendanaan::where('id_proyek', '!=', 0)->get();
		if($getPendanaan){
			foreach($getPendanaan as $Pendanaans){
				$getStatus = Proyek::where('id', $Pendanaans->id_proyek)->first();
				if(empty($getStatus)){
					$status =0;
				}else{
					$status = $getStatus->status;
				}
				//cek
				if($status == 2 && $Pendanaans->status == 1 || $status == 3 && $Pendanaans->status == 1){
					//echo $Pendanaans->id_proyek;die;
					//echo $status;die;
					BorrowerPendanaan::where('id_proyek', $Pendanaans->id_proyek)->update(['status' => 6]);
					//BorrowerPendanaan::where('id_proyek', $Pendanaans->id_proyek)->update(['status' => $status]);
					//return $update;
					//$this->updatePendanaan($Pendanaans->pendanaan_id,6);
					
				}
				
				
			}
		}
		
		
		
		
		$this->middleware('auth:borrower')->only(['dashBoard',
    'detilProyek',
    'pendanaanPage',
    'getProyekbyId',
    'getlastproyekapproved',
    'add_pendanaan',
    'lengkapi_profile',
    'view_status_pending',
    'view_status_reject',
    'otpCode',
    'cekOTP',
    'updateSP3',
    'listInvoice',
    ]);
		// $this->middleware(brwCheck::class);
        // $this->middleware(UserCheck::class)->except(['lengkapi_profile','view_status_reject']);
		
		// $this->middleware(StatusPendanaan::class)->only(['dashboard','detilProyek','pendanaanPage','getProyekbyId']);

  }

  public function emailConfirm($code) 
  {
      if(empty($code))
      {
        return view('errors.email-verif-false_penerima_pendanaan');          
      }
      $user = LoginBorrower::where('email_verif', $code)->first();

      if(!$user){
          return view('errors.email-verif-false_penerima_pendanaan');
      }
      else{
          if($user->status == 'notpreapproved'){
              return view('errors.email-verif-false_penerima_pendanaan');
          }
          else if($user->email_verif === $code){
              $user->status = 'notpreapproved';
              $user->email_verif = Null;
              $user->save();

              return view('errors.email-verif-ok_penerima_pendanaan');

          }
          else{
              return redirect('/#loginModalAs');
          }
      }
  }


	public function lengkapi_profile() {
        return view('pages.kpr.lengkapi_profile');
    }
    
	public function remainingstatus() {
		return view('pages.kpr.remainingstatus');
    }
	
	public function view_status_pending() {
        return view('pages.borrower.view_pending_brw');
    }
	
	public function view_status_reject() {
        return view('pages.borrower.view_reject_brw');
    }

  
  
  public function kpr_list_data()
  {
    $proyeks = DB::table('proyek_kpr')->where('status_tampil', 2)->orderBy('id','DESC')->paginate(6);
    return view('pages.guest.kpr_list_data', compact('proyeks'));
  }

  public function kpr_detail_produk($id)
  {
    $detil = DB::table('proyek_kpr')->where('id', $id)->first();
    return view('pages.guest.kpr_detail_produk', compact('detil'));
  }

  public function kpr_pengajuan_proyek(Request $request)
  {
    if(empty($request->tlpPengajuKPR))
    {
      return redirect()->back()->with('error','Data Telepon anda kosong');
    }
    $idProyek = explode('-',$request->iidProyekKPR);
    $check = DB::table('brw_users')
               ->leftJoin('brw_users_details','brw_users_details.brw_id','=','brw_users.brw_id')
               ->select(['brw_users.email','brw_users_details.no_tlp','brw_users.brw_id'])
               ->where('brw_users_details.no_tlp','=',$request->tlpPengajuKPR)
               ->first();
    if(empty($check))
    {
      return redirect()->to(url()->previous().'/#modal_login_kpr')->with('error','Pengajuan anda gagal, Mohon registrasi terlebih dahulu')  ;
    }
    DB::table('kpr_reservasi')
      ->insert([
        'id_proyek_kpr' => $idProyek[0],
        'id_pengaju_kpr' => $check->brw_id,
        'no_hp' => $check->no_tlp,
        'email' => $check->email,
        'tenor_dp' => $request->tenorKPR,
        'margin' => $request->dpKPR,
        'cicilan' => $request->cicilKPR,
        'tgl_mulai_cicilan' => null,
        'tgl_selesai_cicilan' => null,
        'status' => 1,
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
      ]);
    return redirect()->back()->with('success','Pengajuan anda sedang dalam proses');
  }

  public function kpr_data_pengajuan()
  {
    
    $client = new Client();
    $request = $client->get(config('app.apilink')."/kpr/data-kpr-pengaju");
    $response = $request->getBody()->getContents();
    // return response()->json($request->getBody()->getContents());
    return response($response);
  }

  
  public function kpr_data_approve()
  {
    
    $client = new Client();
    $request = $client->get(config('app.apilink')."/kpr/data-kpr-approve");
    $response = $request->getBody()->getContents();
    // return response()->json($request->getBody()->getContents());
    return response($response);
  }

  
  public function kpr_data_tolak()
  {
    
    $client = new Client();
    $request = $client->get(config('app.apilink')."/kpr/data-kpr-tolak");
    $response = $request->getBody()->getContents();
    // return response()->json($request->getBody()->getContents());
    return response($response);
  }

  

  public function get_data_cicil($id)
  {
    $client = new Client();
    $request = $client->get(config('app.apilink')."/kpr/data-pengaju-cicil/".$id);
    $response = $request->getBody()->getContents();
    // return response()->json($request->getBody()->getContents());
    return response($response);
    // return $id;
  }

  
  public function get_data_keterangan($id)
  {
    $client = new Client();
    $request = $client->get(config('app.apilink')."/kpr/data-pengaju-keterangan/".$id);
    $response = $request->getBody()->getContents();
    // return response()->json($request->getBody()->getContents());
    return response($response);
    // return $id;
  }

  

  public function managePostKpr(Request $request)
  {
    $id_pengaju = $request->id_pengaju;
    $tgl_mulai = $request->tgl_mulai_pengaju;
    $tgl_selesai = $request->tgl_selesai_pengaju;
		$client = new Client();
		$request = $client->post(config('app.apilink')."/kpr/data-post-cicil",[
								  'form_params' => 
								  [
                  'id_pengajuan' => $id_pengaju,
                  'tgl_mulai_pengaju' => $tgl_mulai,
                  'tgl_selesai_pengaju' => $tgl_selesai,
								  ]
								]);
    $response = $request->getBody()->getContents();
    $return = json_decode($response);
    // dd($return);
    if($return->status == "Data Sukses")
    {
      return redirect()->back()->with('success','Data berhasil di setujui');
    }
    else
    {
      return redirect()->back()->with('error','Data gagal di update');      
    }
  }

  public function managePostTolak(Request $request)
  {
    $id_pengaju = $request->id_pengaju;
    $keterangan = $request->keterangan;
		$client = new Client();
		$request = $client->post(config('app.apilink')."/kpr/data-post-tolak",[
								  'form_params' => 
								  [
                  'id_pengajuan' => $id_pengaju,
                  'keterangan' => $keterangan,
								  ]
								]);
    $response = $request->getBody()->getContents();
    $return = json_decode($response);
    // dd($return);
    if($return->status == "Data Sukses")
    {
      return redirect()->back()->with('success','Data berhasil di tolak');
    }
    else
    {
      return redirect()->back()->with('error','Data gagal di update');      
    }
  }
}