<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investor;
use App\PenarikanDana;
use App\DetilInvestor;
use App\BniEnc;
use App\MutasiInvestor;
use App\Events\MutasiInvestorEvent;
use App\RekeningInvestor;
use App\Proyek;
use App\PendanaanAktif;
use App\LogPendanaan;
use App\LogPengembalianDana;
use App\LogSuspend;
use App\AuditTrail;
// use App\Subscribe;
use Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Auth;
//use App\Jobs\AdminAddPendanaan;

use App\Exports\DetilInvestorExport;
use App\Exports\DanaInvestorProyek;
use App\Exports\PenarikanExport;
use Excel;

use GuzzleHttp\Client;
use App\Jobs\InvestorVerif;
use App\MasterProvinsi;
use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;
use DB;
use Validator;
use Image;
use App\Http\Middleware\NotifikasiProyek;
use App\Http\Middleware\StatusProyek;
use App\Mail\AdminAddPendanaanEmail;
use Mail;
use App\LogAkadDigiSignInvestor;
use App\InvestorPengurus;

class DHController extends Controller
{

    public function __construct()
    {
         
        $this->middleware('auth:admin');
        
    }
    
    public function admin_manage()
    {
        
        $master_provinsi = MasterProvinsi::distinct('kode_provinsi', 'nama_provinsi')->get(['kode_provinsi', 'nama_provinsi']);
        $master_agama = MasterAgama::all();
        $master_asset = MasterAsset::all();
        $master_badan_hukum = MasterBadanHukum::all();
        $master_bank = MasterBank::all();
        $master_bidang_pekerjaan = MasterBidangPekerjaan::all();
        $master_jenis_kelamin = MasterJenisKelamin::all();
        $master_jenis_pengguna = MasterJenisPengguna::all();
        $master_kawin = MasterKawin::all();
        $master_kepemilikan_rumah = MasterKepemilikanRumah::all();
        $master_negara = MasterNegara::all();
        $master_online = MasterOnline::all();
        $master_pekerjaan = MasterPekerjaan::all();
        $master_pendapatan = MasterPendapatan::all();
        $master_pendidikan = MasterPendidikan::all();
        $master_pengalaman_kerja = MasterPengalamanKerja::all();
        $master_hub_ahli_waris = DB::table('m_hub_ahli_waris')->select('id_hub_ahli_waris', 'jenis_hubungan')->get();
        $master_kode_operator = DB::table("m_kode_operator_negara")->get();
        $master_jabatan = DB::table('m_jabatan')->get();


        return view('pages.admin.dh_manage', [
            'master_provinsi' => $master_provinsi,
            'master_agama' => $master_agama,
            'master_asset' => $master_asset,
            'master_badan_hukum' => $master_badan_hukum,
            'master_bank' => $master_bank,
            'master_bidang_pekerjaan' => $master_bidang_pekerjaan,
            'master_jenis_kelamin' => $master_jenis_kelamin,
            'master_jenis_pengguna' => $master_jenis_pengguna,
            'master_kawin' => $master_kawin,
            'master_kepemilikan_rumah' => $master_kepemilikan_rumah,
            'master_negara' => $master_negara,
            'master_online' => $master_online,
            'master_pekerjaan' => $master_pekerjaan,
            'master_pendapatan' => $master_pendapatan,
            'master_pendidikan' => $master_pendidikan,
            'master_pengalaman_kerja' => $master_pengalaman_kerja,
            'master_provinsi' => $master_provinsi,
            'master_hub_ahli_waris' => $master_hub_ahli_waris,
            'master_kode_operator' => $master_kode_operator,
            'master_jabatan' => $master_jabatan,
        ]) ;
    }
    
    public function admin_create_dh(Request $request)
    {

        $messages = [
            'error_upload'    => 'Tipe file gambar harus jpeg,jpg,bmp,png dan ukuran file gambar max 500 KB',
        ];

        $validator = Validator::make($request->all(), [
            'pic_investor' => 'mimes:jpeg,jpg,bmp,png|max:500',
            'pic_ktp_investor' => 'mimes:jpeg,jpg,bmp,png|max:500',
            'pic_user_ktp_investor' => 'mimes:jpeg,jpg,bmp,png|max:500',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($messages);
            
        } else {
            if (Investor::where('email', $request->email)->exists() || Investor::where('username', $request->username)->exists()) {
                return redirect()->back()->with('exist', "Username or Email already exist");
            }
            $user = Investor::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'email_verif' => null,
                'status' => 'active',
            ]);

            $detil = new DetilInvestor;
            if ($request->tipe_pengguna == 1) {
                $detil->investor_id = $user->id;
                $detil->tipe_pengguna = $request->tipe_pengguna;
                $detil->nama_investor = $request->nama;
                $detil->no_ktp_investor = $request->no_ktp;
                $detil->no_npwp_investor = $request->no_npwp;
                $detil->phone_investor = $request->no_telp;
                $detil->alamat_investor = $request->alamat;
                $detil->provinsi_investor = $request->provinsi;
                $detil->kota_investor = $request->kota;
                $detil->kode_pos_investor = $request->kode_pos;
                $detil->tempat_lahir_investor = $request->tempat_lahir;
                $detil->tgl_lahir_investor = $request->tgl_lahir . '-' . $request->bln_lahir . '-' . $request->thn_lahir;
                $detil->jenis_kelamin_investor = $request->jenis_kelamin;
                $detil->status_kawin_investor = $request->kawin;
                $detil->status_rumah_investor = $request->pemilik_rumah;
                $detil->agama_investor = $request->agama;
                $detil->pekerjaan_investor = $request->pekerjaan;
                $detil->bidang_pekerjaan = $request->bidang_pekerjaan;
                $detil->online_investor = $request->pekerjaan_online;
                $detil->pendapatan_investor = $request->pendapatan;
                $detil->asset_investor = null;
                $detil->pengalaman_investor = $request->pengalaman;
                $detil->pendidikan_investor = $request->pendidikan;
                $detil->bank_investor = $request->bank;
                $detil->rekening = $request->rekening;
                   
                $detil->pic_investor = $this->upload('pic_investor', $request, $user->id);
                $detil->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $user->id);
                $detil->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $user->id);
                $detil->jenis_badan_hukum = null;
                $detil->nama_perwakilan = null;
                $detil->no_ktp_perwakilan = null;

                if ($request->kawin == 1) {
                    $detil->pasangan_investor = $request->nama_pasangan;
                    $detil->pasangan_email = $request->email_pasangan;
                    $detil->pasangan_tempat_lhr = $request->tempat_lahir_pasangan;
                    $detil->pasangan_tgl_lhr = $request->tgl_lahir_pasangan . '-' . $request->bln_lahir_pasangan . '-' . $request->thn_lahir_pasangan;
                    $detil->pasangan_jenis_kelamin = $request->jenis_kelamin_pasangan;
                    $detil->pasangan_ktp = $request->no_ktp_pasangan;
                    $detil->pasangan_npwp = $request->no_npwp_pasangan;
                    $detil->pasangan_phone = $request->no_telp_pasangan;
                    $detil->pasangan_alamat = $request->alamat_pasangan;
                    $detil->pasangan_provinsi = $request->provinsi_pasangan;
                    $detil->pasangan_kota = $request->kota_pasangan;
                    $detil->pasangan_kode_pos = $request->kode_pos_pasangan;
                    $detil->pasangan_agama = $request->agama_pasangan;
                    $detil->pasangan_pekerjaan = $request->pekerjaan_pasangan;
                    $detil->pasangan_bidang_pekerjaan = $request->bidang_pekerjaan_pasangan;
                    $detil->pasangan_online = $request->pekerjaan_online_pasangan;
                    $detil->pasangan_pendapatan = $request->pendapatan_pasangan;
                    $detil->pasangan_pengalaman = $request->pengalaman_pasangan;
                    $detil->pasangan_pendidikan = $request->pendidikan_pasangan;
                } else {
                    $detil->pasangan_investor = null;
                    $detil->pasangan_email = null;
                    $detil->pasangan_tempat_lhr = null;
                    $detil->pasangan_tgl_lhr = null;
                    $detil->pasangan_jenis_kelamin = null;
                    $detil->pasangan_ktp = null;
                    $detil->pasangan_npwp = null;
                    $detil->pasangan_phone = null;
                    $detil->pasangan_alamat = null;
                    $detil->pasangan_provinsi = null;
                    $detil->pasangan_kota = null;
                    $detil->pasangan_kode_pos = null;
                    $detil->pasangan_agama = null;
                    $detil->pasangan_pekerjaan = null;
                    $detil->pasangan_bidang_pekerjaan = null;
                    $detil->pasangan_online = null;
                    $detil->pasangan_pendapatan = null;
                    $detil->pasangan_pengalaman = null;
                    $detil->pasangan_pendidikan = null;
                }
                 
                $detil->save();
                $hasil = $this->generateVA($request->username);
            } else {
                $detil->investor_id = $user->id;
                $detil->tipe_pengguna = $request->tipe_pengguna;
                $detil->nama_investor = $request->nama;
                $detil->no_ktp_investor = null;
                $detil->no_npwp_investor = $request->no_npwp;
                $detil->phone_investor = $request->no_telp;
                $detil->alamat_investor = $request->alamat;
                $detil->provinsi_investor = $request->provinsi;
                $detil->kota_investor = $request->kota;
                $detil->kode_pos_investor = $request->kode_pos;
                $detil->tempat_lahir_investor = null;
                $detil->tgl_lahir_investor = null;
                $detil->jenis_kelamin_investor = null;
                $detil->status_kawin_investor = null;
                $detil->status_rumah_investor = null;
                $detil->agama_investor = null;
                $detil->pekerjaan_investor = null;
                $detil->bidang_pekerjaan = $request->bidang_pekerjaan;
                $detil->online_investor = $request->pekerjaan_online;
                $detil->pendapatan_investor = $request->pendapatan;
                $detil->asset_investor = $request->asset;
                $detil->pengalaman_investor = null;
                $detil->pendidikan_investor = null;
                $detil->bank_investor = $request->bank;
                $detil->rekening = $request->rekening;

                $detil->pasangan_investor = null;
                $detil->pasangan_email = null;
                $detil->pasangan_tempat_lhr = null;
                $detil->pasangan_tgl_lhr = null;
                $detil->pasangan_jenis_kelamin = null;
                $detil->pasangan_ktp = null;
                $detil->pasangan_npwp = null;
                $detil->pasangan_phone = null;
                $detil->pasangan_alamat = null;
                $detil->pasangan_provinsi = null;
                $detil->pasangan_kota = null;
                $detil->pasangan_kode_pos = null;
                $detil->pasangan_agama = null;
                $detil->pasangan_pekerjaan = null;
                $detil->pasangan_bidang_pekerjaan = null;
                $detil->pasangan_online = null;
                $detil->pasangan_pendapatan = null;
                $detil->pasangan_pengalaman = null;
                $detil->pasangan_pendidikan = null;
                   
                $detil->pic_investor = $this->upload('pic_investor', $request, $user->id);
                $detil->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $user->id);
                $detil->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $user->id);
                $detil->jenis_badan_hukum = $request->jenis_badan_hukum;
                $detil->nama_perwakilan = $request->nama_perwakilan;
                $detil->no_ktp_perwakilan = $request->no_ktp_perwakilan;

                $detil->save();
                $hasil = $this->generateVA($request->username);
            }

            if (!$hasil) {
                Investor::where('id', $detil->investor_id)->update(['status' => 'pending']);
                return redirect()->back()->with('error', 'Pembuatan VA gagal');
            }
            return redirect()->back()->with('success', 'Create Success');
        }
    }

    public function admin_update_dh(Request $request)
    {
         

        if ($request->tipe_proses == 'baru') {
        
            $investor = Investor::where('id', $request->investor_id)->first();

            if ($investor->username != $request->username) {
                if (Investor::where('username', $request->username)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }
            if ($investor->email != $request->email) {
                if (Investor::where('email', $request->email)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }
             

            $investor->username = $request->username;
            $investor->email = $request->email;
            $investor->status = 'active';
            $investor->ref_number = $request->kode_ref;

            $investor->save();

            $detil_insert = new DetilInvestor;

            
            $detil_insert->investor_id = $request->investor_id;
            $detil_insert->tipe_pengguna = null;
            $detil_insert->nama_investor = $request->nama;
            $detil_insert->no_ktp_investor = $request->no_ktp;
            $detil_insert->no_npwp_investor = $request->no_npwp;
            $detil_insert->phone_investor = $request->no_telp;
            $detil_insert->alamat_investor = $request->alamat;
            $detil_insert->provinsi_investor = $request->provinsi;
            $detil_insert->kota_investor = $request->kota;
            $detil_insert->kode_pos_investor = $request->kode_pos;
            $detil_insert->tempat_lahir_investor = $request->tempat_lahir;
            $detil_insert->tgl_lahir_investor = $request->tgl_lahir . '-' . $request->bln_lahir . '-' . $request->thn_lahir;
            $detil_insert->jenis_kelamin_investor = $request->jenis_kelamin;
            $detil_insert->status_kawin_investor = null;
            $detil_insert->status_rumah_investor = null;
            $detil_insert->agama_investor = null;
            $detil_insert->pekerjaan_investor = null;
            $detil_insert->bidang_pekerjaan = null;
            $detil_insert->online_investor = null;
            $detil_insert->pendapatan_investor = null;
            $detil_insert->asset_investor = null;
            $detil_insert->pengalaman_investor = null;
            $detil_insert->pendidikan_investor = null;
            $detil_insert->bank_investor = $request->bank;
            $detil_insert->rekening = $request->rekening;
            $detil_insert->nama_pemilik_rek = $request->nama_pemilik_rek;
            
            $detil_insert->jenis_badan_hukum = null;
            $detil_insert->nama_perwakilan = null;
            $detil_insert->no_ktp_perwakilan = null;
 

            if ($request->hasFile('pic_investor')) {
                 
                Storage::disk('private')->delete($detil_insert->pic_investor);
                $detil_insert->pic_investor = $this->upload('pic_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_ktp_investor')) {
                
                Storage::disk('private')->delete($detil_insert->pic_ktp_investor);
                $detil_insert->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_user_ktp_investor')) {
                 
                Storage::disk('private')->delete($detil_insert->pic_user_ktp_investor);
                $detil_insert->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $request->investor_id);
            }

            $detil_insert->save();
 
        } else {
            $investor = Investor::where('id', $request->investor_id)->first();

            if ($investor->username != $request->username) {
                if (Investor::where('username', $request->username)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }
            if ($investor->email != $request->email) {
                if (Investor::where('email', $request->email)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }
            

            $investor->username = $request->username;
            $investor->email = $request->email;
            $investor->ref_number = $request->kode_ref;

            $investor->save();

            $detil = DetilInvestor::where('investor_id', $request->investor_id)->first();

             
            $detil->tipe_pengguna = null;
            $detil->nama_investor = $request->nama;
            $detil->no_ktp_investor = $request->no_ktp;
            $detil->no_npwp_investor = $request->no_npwp;
            $detil->phone_investor = $request->no_telp;
            $detil->alamat_investor = $request->alamat;
            $detil->provinsi_investor = $request->provinsi;
            $detil->kota_investor = $request->kota;
            $detil->kecamatan = $request->kecamatan;
            $detil->kelurahan = $request->kelurahan;
            $detil->kode_pos_investor = $request->kode_pos;
            $detil->tempat_lahir_investor = $request->tempat_lahir;
            $detil->tgl_lahir_investor = $request->tgl_lahir . '-' . $request->bln_lahir . '-' . $request->thn_lahir;
            $detil->jenis_kelamin_investor = $request->jenis_kelamin;
            $detil->status_kawin_investor = $request->status_kawin;
            $detil->status_rumah_investor = null;
            $detil->agama_investor = null;
            $detil->pekerjaan_investor = $request->pekerjaan;
            $detil->bidang_pekerjaan = null;
            $detil->online_investor = null;
            $detil->pendapatan_investor = $request->pendapatan;
            $detil->asset_investor = null;
            $detil->pengalaman_investor = null;
            $detil->pendidikan_investor = $request->pendidikan;
            $detil->bank_investor = $request->bank;
            $detil->rekening = $request->rekening;
            $detil->nama_pemilik_rek = $request->nama_pemilik_rek;
            $detil->nama_ibu_kandung = $request->nama_ibu_kandung;
            
            $detil->jenis_badan_hukum = null;
            $detil->nama_perwakilan = null;
            $detil->no_ktp_perwakilan = null;
 

            if ($request->status == 'suspend') {
                $suspended_by = Auth::guard('admin')->user()->firstname;
                $investor = Investor::where('id', $request->investor_id)->first();
                $investor->status = $request->status;
                $investor->keterangan = $request->alasan_suspend;
                $investor->suspended_by = $suspended_by;
                $investor->save();

                $Log = new LogSuspend;
                $Log->keterangan = $request->alasan_suspend;
                $Log->suspended_by = $suspended_by;
                $Log->save();
            } elseif ($request->status == 'active') {
                $actived_by = Auth::guard('admin')->user()->firstname;
                $investor = Investor::where('id', $request->investor_id)->first();
                $investor->status = $request->status;
                $investor->keterangan = $request->alasan_active;
                $investor->actived_by = $actived_by;
                $investor->save();
            }

            if ($request->hasFile('pic_investor')) {
                
                Storage::disk('private')->delete($detil->pic_investor);
                $detil->pic_investor = $this->upload('pic_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_ktp_investor')) {
                 
                Storage::disk('private')->delete($detil->pic_ktp_investor);
                $detil->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_user_ktp_investor')) {
                 
                Storage::disk('private')->delete($detil->pic_user_ktp_investor);
                $detil->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $request->investor_id);
            }

            $detil->save();

             

            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Kelola Pendana";
            $audit->description = "Ubah data profil Pendana";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return redirect()->back()->with('updated', "Update user Success");
        }

        // }

    }

}