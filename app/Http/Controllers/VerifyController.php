<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use App\Verify;
use App\VerifyLog;
use App\VerifyIncome;

use GuzzleHttp\Client;

use App\Helpers\Helper;
use App\VerifyComplete;
use App\VerifyOcrExtra;
use App\VerifyProperti;
use App\VerifyTempatKerja;
use App\VerifyShareHolder;

use App\VerificationVendor;
use App\VerifyNegativeList;
use App\VerifyIncomePersero;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VerifyController extends Controller
{

    public const PATH_COMPLETE = 'completeid';
    public const PATH_INCOME = 'income';
    public const PATH_INCOME_PERSERO = 'income_persero';
    public const PATH_TEMPATKERJA = 'tempat_kerja';
    public const PATH_PROPERTI = 'properti';
    public const PATH_NEGATIVE_LIST = 'negative_list';
    public const PATH_OCR_EXTRA = 'ocr_extra';
    public const PATH_SHARE_HOLDER = 'share_holder';
    public const VENDOR = 'verijelas';

    public const LABEL_TITLE = [
        'complete_id' => 'COMPLETE ID', 'ocr' => 'OCR', 'tempat_kerja' => 'Workplace Verification F',
        'negativelist' => 'Negative List Verification J',
        'npwp' => 'NPWP Individu', 'npwp_persero' => 'NPWP Badan Hukum'
    ];
    public $errorData = null;


    public function checkSessionTrxId($body): void {
        if(Session::has('session_trx_id')){
            Session::forget('session_trx_id');
        }
        Session::put('session_trx_id', $body['trx_id']);
    }

    public function connectApi($body, $url)
    {
        try {

            ini_set('max_execution_time', config('app.verijelas_timeout'));
            
            //Check session transaction Id
            $this->checkSessionTrxId($body);

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => config('app.verijelas_url') . '/' . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                'Token: '.config('app.verijelas_token'),
                'Content-Type: application/json'
            ),
            ));
    
            $response = curl_exec($curl);
            curl_close($curl);

            // $client = new Client();
            // $requestApi = $client->post(config('app.verijelas_url') . '/' . $url, [
            //     'headers' => [
            //         'Content-Type' => 'application/json',
            //         'Accept' => 'application/json',
            //         'charset' => 'utf-8',
            //         'Token' => config('app.verijelas_token')
            //     ],
            //     'body' => json_encode($body)
            // ]);

            //  $response = $requestApi->getBody()->getContents();
            //  return json_decode($response);
        
             if(Helper::isJSON($response)){
                  return json_decode($response);
              }else{
                  Helper::logDbApp('VerifyController.php', 71, "Trx id: ".$body['trx_id'].". Return data JSON verijelas tidak valid : ". $response);
                  return json_decode(json_encode(["status"=> 500, "error"=>"Return data dari Verijelas tidak valid : ". $response]));
             }

        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 67, "Trx id: ".$body['trx_id'].". ".$e->getMessage());
            return json_decode(json_encode(["status"=> 500, "error"=>"Terjadi error. Hubungi admin"])); 
        }
    }


    public function putVerifyMaster($request, $response, $path)
    {

        $resultVerify = Verify::updateOrCreate([
            'brw_user_id' => $request->borrower_id,
            'path' => $path
        ], [
            'ref_id' => (isset($response->ref_id) ? $response->ref_id : null),
            'vendor_id' => $this->getVerificationVendor(self::VENDOR)->id,
            'vendor_name' => self::VENDOR,
            'origin' => $request->origin,
            'errors' => $this->errorData,
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return $resultVerify;
    }

    public function getVerificationVendor($name)
    {
        return VerificationVendor::where('nama', $name)->first();
    }


    public function putVerifyLog($resultVerify, $request, $response, $errors)
    {

        $createdBy = !isset(Auth::guard('admin')->user()->email) ? null : Auth::guard('admin')->user()->email;
        VerifyLog::create([
            'trx_id' => $request['trx_id'],
            'verify_id' => isset($resultVerify->id) ? $resultVerify->id : null,
            'request_data' => json_encode($request),
            'response_data' => json_encode($response),
            'status' => $response->status,
            'errors' => $errors,
            'created_by' => $createdBy
        ]);
    }


    private function getTransactionId()
    {
        return 'dsi' . date('Ymdhis') . Helper::generate_number(3);
    }

    public function checkErrors($body, $response, $checkmessage = 0)
    {

        $error_arr = [];

        if ($checkmessage == 1) {
            if (isset($response->errors->message)) {
                $this->errorData = implode(', ', $response->errors->message);
            }
        } else {
            if (isset($response->errors)) {
                foreach ($response->errors as $key => $val) {
                    $error_arr[] = $key . " " . $val;
                }
                $this->errorData = implode(', ', $error_arr);
            }
        }

        $arrayStatusCode = ['204', '400', '401', '403', '404', '405', '409', '412', '413', '500', '501', '502'];
        if (in_array($response->status, $arrayStatusCode)) {
            if (isset($response->error) && !empty($response->error)) {
                $this->errorData = $response->error;

                if (isset($response->message) && !empty($response->message)) {
                    $this->errorData .= ' ' . $response->message;
                }
            }

            if (isset($response->message) && !empty($response->message)) {
                $this->errorData = $response->message;
            }

            $response->data = null;
        }


        if (!$response->data) {
            $this->putVerifyLog(null, $body, $response, $this->errorData);
            return  response(['status_code' => $response->status, 'data' => $response->data, 'errors' => $this->errorData],  $response->status);
        }
    }


    public function getBorrowerDetails($request){
        return ($request->pengajuan_id) ?  \DB::table('brw_user_detail_pengajuan')->where('pengajuan_id', $request->pengajuan_id)->first() : \DB::table('brw_user_detail')->where('brw_id', $request->borrower_id)->first();
    }

    public function verifyCompleteId(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required',
            ],

            ['required' => ':attribute field is required.']
        );


        if ($validator->fails()) {
            $res = ['status_code' => 400, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {

            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            $trx_id = $this->getTransactionId();
            $destinationPath = storage_path('app/private/');
            $filePath = $destinationPath . $borrowerDetails->brw_pic;

            if (file_exists($filePath)) {
                $selfie_photo = file_get_contents($filePath);
                $selfie_photo_base64 = base64_encode($selfie_photo);
            } else {
                $selfie_photo_base64 = null;
            }

            $body = [
                'trx_id' => $trx_id,
                'nik' => $borrowerDetails->ktp,
                'name' => $borrowerDetails->nama,
                'birthdate' => date('d-m-Y', strtotime($borrowerDetails->tgl_lahir)),
                'birthplace' => $borrowerDetails->tempat_lahir,
                'identity_photo' => '',
                'selfie_photo' => $selfie_photo_base64
            ];

            $response = $this->connectApi($body, 'completeid_autofix');
            $responseErrors = $this->checkErrors($body, $response);
            if ($responseErrors) return $responseErrors;


            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_COMPLETE);

            if ($resultVerify) {
                VerifyComplete::updateOrCreate(['verify_id' => $resultVerify->id], [
                    'nik' => $borrowerDetails->ktp,
                    'name' => $borrowerDetails->nama,
                    'valid_name' => $response->data->name,
                    'birthdate' => $borrowerDetails->tgl_lahir,
                    'valid_birthdate' => $response->data->birthdate,
                    'birthplace' => $borrowerDetails->tempat_lahir,
                    'valid_birthplace' => $response->data->birthplace,
                    'address' => $response->data->address,
                    'selfie_photo' => $borrowerDetails->brw_pic,
                    'valid_selfie_photo' => $response->data->selfie_photo,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);
                return response(['status_code' => $response->status, 'data' => $response->data, 'errors' => $this->errorData]);
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 218, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function verifyPendapatan(Request $request)
    {


        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required'
            ],

            ['required' => ':attribute field is required.']
        );


        if ($validator->fails()) {
            $res = ['status_code' => 400, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {

            //Get Data Borrower 
            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            $trx_id = $this->getTransactionId();
            $brw_income = ($request->pengajuan_id) ?  \DB::table('brw_user_detail_penghasilan_pengajuan')->where('pengajuan_id', $request->pengajuan_id)->value('pendapatan_borrower') : \DB::table('brw_user_detail_penghasilan')->where('brw_id2', $request->borrower_id)->value('pendapatan_borrower');

            $body = [
                'trx_id' => $trx_id,
                'npwp' => $borrowerDetails->npwp,
                'nik' => $borrowerDetails->ktp,
                'name' => $borrowerDetails->nama,
                'income' => floatval($brw_income),
                'birthdate' => date('d-m-Y', strtotime($borrowerDetails->tgl_lahir)),
                'birthplace' => $borrowerDetails->tempat_lahir,

            ];

            $response = $this->connectApi($body, 'pendapatan');
            $responseErrors = $this->checkErrors($body, $response);
            if ($responseErrors) return $responseErrors;

            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_INCOME);


            if ($resultVerify) {
                VerifyIncome::updateOrCreate(['verify_id' => $resultVerify->id], [
                    'nik' => $borrowerDetails->ktp,
                    'valid_nik' => $response->data->nik,
                    'name' => $borrowerDetails->nama,
                    'valid_name' => $response->data->name,
                    'npwp' => $borrowerDetails->npwp,
                    'valid_npwp' => $response->data->npwp,
                    'income' => $brw_income,
                    'valid_income' => $response->data->income,
                    'birthdate' => $borrowerDetails->tgl_lahir,
                    'valid_birthdate' => $response->data->birthdate,
                    'birthplace' => $borrowerDetails->tempat_lahir,
                    'valid_birthplace' => $response->data->birthplace,
                    'match_result' => $response->data->match_result,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);

                return response(['status_code' => $response->status, 'data' => $response->data, 'errors' =>  $this->errorData]);
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 294, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function verifyPendapatanPersero(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required'
            ],

            ['required' => ':attribute field is required.']
        );


        if ($validator->fails()) {
            $res = ['status_code' => 400, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {

            //Get Data Borrower 
            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            $trx_id = $this->getTransactionId();

            $body = [
                'trx_id' => $trx_id,
                'npwp' => $borrowerDetails->npwp_perusahaan,
                'income' => floatval($borrowerDetails->omset_tahun_terakhir)
            ];

            $response = $this->connectApi($body, 'pendapatan_perseroan');

            $responseErrors = $this->checkErrors($body, $response);
            if ($responseErrors) return $responseErrors;

            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_INCOME_PERSERO);

            if ($resultVerify) {
                VerifyIncomePersero::updateOrCreate(['verify_id' => $resultVerify->id], [
                    'npwp' => $borrowerDetails->npwp_perusahaan,
                    'valid_npwp' => $response->data->npwp,
                    'income' => $borrowerDetails->omset_tahun_terakhir,
                    'valid_income' => $response->data->income,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);

                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);
                return response(['status' => $response->status, 'data' => $response->data, 'errors' => $this->errorData]);
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 353, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function verifyTempatKerja(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required'
            ],

            ['required' => ':attribute field is required.']
        );

        if ($validator->fails()) {
            $res = ['status_code' => 400, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {
            //Get Data Borrower 
            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            $trx_id = $this->getTransactionId();
            $companyDetails = ($request->pengajuan_id) ?  \DB::table('brw_user_detail_penghasilan_pengajuan')->where('pengajuan_id', $request->pengajuan_id)->select('nama_perusahaan', 'no_telp')->first() : \DB::table('brw_user_detail_penghasilan')->where('brw_id2', $request->borrower_id)->select('nama_perusahaan', 'no_telp')->first();
            $body = [
                'trx_id' => $trx_id,
                'nik' => $borrowerDetails->ktp,
                'name' => $borrowerDetails->nama,
                'company_name' => $companyDetails->nama_perusahaan,
                'company_phone' => $companyDetails->no_telp
            ];

            $response = $this->connectApi($body, 'tempat_kerja');

            $responseErrors = $this->checkErrors($body, $response);
            if ($responseErrors) return $responseErrors;

            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_TEMPATKERJA);

            if ($resultVerify) {
                VerifyTempatKerja::updateOrCreate(
                    ['verify_id' => $resultVerify->id],
                    [
                        'nik' => $borrowerDetails->ktp,
                        'valid_nik' => $response->data->nik,
                        'name' => $borrowerDetails->nama,
                        'valid_name' => $response->data->name,
                        'company_name' => $companyDetails->nama_perusahaan,
                        'valid_company_name' => isset($response->data->company_name) ? $response->data->company_name : null,
                        'valid_company' => $response->data->company,
                        'company_phone' => $companyDetails->no_telp,
                        'valid_company_phone' => $response->data->company_phone,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                );

                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);
                return response(['status_code' => $response->status, 'data' => $response->data, 'errors' => $this->errorData]);
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 420, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function verifyProperti(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required'
            ],

            ['required' => ':attribute field is required.']
        );

        if ($validator->fails()) {
            $res = ['status_code' => 400, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {

            //Get Data Borrower
            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            $trx_id = $this->getTransactionId();

            //TODO
            // message Data not found property_name invalid property_building_area invalid property_surface_area invalid property_estimation invalid
            $body = [
                'trx_id' => $trx_id,
                'nop' => $request->nop,
                'nik' => $borrowerDetails->ktp,
                'property_name' => '',
                'property_building_area' => '',
                'property_surface_area' => '',
                'property_estimation' => ''
            ];

            $response = $this->connectApi($body, 'properti');
            $responseErrors = $this->checkErrors($body, $response);
            if ($responseErrors) return $responseErrors;

            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_PROPERTI);

            if ($resultVerify) {
                VerifyProperti::updateOrCreate(
                    ['verify_id' => $resultVerify->id],
                    [
                        'nop' => $request->nop,
                        'property_address' => $response->data->property_address,
                        'property_name' => $response->data->property_name,
                        'valid_property_name' => $response->data->property_name,
                        'property_building_area' => $response->data->property_building_area,
                        'valid_property_building_area' => $response->data->property_building_area,
                        'property_surface_area' => $response->data->property_surface_area,
                        'valid_property_surface_area' => $response->data->property_surface_area,
                        'property_estimation' => $response->data->property_estimation,
                        'valid_property_estimation' => $response->data->property_estimation,
                        'certificate_address' => $response->data->certificate_address,
                        'certificate_id' => $response->data->certificate_id,
                        'valid_certificate_id' => $response->data->certificate_id,
                        'certificate_name' => $response->data->certificate_name,
                        'valid_certificate_name' => $response->data->certificate_name,
                        'certificate_type' => $response->data->certificate_type,
                        'valid_certificate_type' => $response->data->certificate_type,
                        //TODO
                        'certificate_date' => null,
                        'valid_certificate_date' => $response->data->certificate_type,
                        'updated_at' =>  date('Y-m-d H:i:s')
                    ]
                );


                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);
                return response(['status_code' => $response->status, 'data' => $response->data, 'errors' => $this->errorData]);
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 503, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function verifyNegativeList(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required'
            ],

            ['required' => ':attribute field is required.']
        );

        if ($validator->fails()) {
            $res = ['status' => false, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {

            //Get Data Borrower 
            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            // $trx_id = $this->getTransactionId($request->borrower_id, self::PATH_NEGATIVE_LIST, self::VENDOR);
            $trx_id = $this->getTransactionId();

            $body = [
                'trx_id' => $trx_id,
                'nik' => $borrowerDetails->ktp,
                'name' => $borrowerDetails->nama,
                'dob' => $borrowerDetails->tgl_lahir,
                'pob' => $borrowerDetails->tempat_lahir
            ];

            $response = $this->connectApi($body, 'negative_list');

            $responseErrors = $this->checkErrors($body, $response);
            if ($responseErrors) return $responseErrors;

            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_NEGATIVE_LIST);
            if ($resultVerify) {

                $negativeListData =    [
                    'nik' =>  $borrowerDetails->ktp,
                    'valid_nik' => $response->data->nik,
                    'name' =>  $borrowerDetails->nama,
                    'valid_name' => $response->data->name,
                    'dob' => $borrowerDetails->tgl_lahir,
                    'valid_dob' => $response->data->dob,
                    'pob' => $borrowerDetails->tempat_lahir,
                    'valid_pob' =>  $response->data->pob,
                    'detail_url' => (isset($response->data->detail->url)) ? $response->data->detail->url : null,
                    'updated_at' => date('Y-m-d H:i:s')
                ];

                VerifyNegativeList::updateOrCreate(
                    ['verify_id' => $resultVerify->id],
                    $negativeListData
                );


                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);

                //If True, NIK is found, if Null, the NIK is not found on negative list
                if ($response->data->nik == true) {
                    return response(['status_code' => $response->status, 'data' => $negativeListData, 'errors' => $this->errorData]);
                } else {
                    return response(['status_code' => $response->status, 'errors' => 'Nik tidak ditemukan pada negative list', 'badge' => 'badge badge-success']);
                }
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 579, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function verifyOcrExtra(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required'
            ],

            ['required' => ':attribute field is required.']
        );

        if ($validator->fails()) {
            $res = ['status' => false, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {

            //Get Data Borrower 
            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            $trx_id = $this->getTransactionId();
            $destinationPath = storage_path('app/private/');
            $filePath = $destinationPath . $borrowerDetails->brw_pic_ktp;

            if (file_exists($filePath)) {
                $ktp_image = file_get_contents($filePath);
                $ktp_image = base64_encode($ktp_image);
            } else {
                $ktp_image = null;
            }

            $body = [
                'trx_id' => $trx_id,
                'ktp_image' => $ktp_image
            ];

            $response = $this->connectApi($body, 'ocr_extra');
            $responseErrors = $this->checkErrors($body, $response);
            if ($responseErrors) return $responseErrors;

            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_OCR_EXTRA);

            if ($resultVerify) {

                $responseDataArray = (array) $response->data;
                VerifyOcrExtra::updateOrCreate(
                    ['verify_id' => $resultVerify->id],
                    [
                        'nik' => $responseDataArray['nik'],
                        'nama' => $responseDataArray['nama'],
                        "tempat_lahir" => $responseDataArray['tempat_lahir'],
                        "tanggal_lahir" => $responseDataArray['tanggal_lahir'],
                        "jenis_kelamin" => $responseDataArray['jenis_kelamin'],
                        "gol_darah" => $responseDataArray['gol_darah'],
                        "alamat" => $responseDataArray['alamat'],
                        "rt_rw" =>  $responseDataArray['rt/rw'],
                        "kelurahan" => $responseDataArray['kelurahan/desa'],
                        "kecamatan" => $responseDataArray['kecamatan'],
                        "agama" => $responseDataArray['agama'],
                        "status_perkawinan" => $responseDataArray['status_perkawinan'],
                        "pekerjaan" => $responseDataArray['pekerjaan'],
                        "kewarganegaraan" => $responseDataArray['kewarganegaraan'],
                        "provinsi" => $responseDataArray['provinsi'],
                        "kota" => $responseDataArray['kota'],
                        "updated_at" => date('Y-m-d H:i:s')
                    ]
                );


                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);
                return response(['status_code' => $response->status, 'data' => $response->data, 'errors' => $this->errorData]);
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 662, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function verifyShareholder(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'borrower_id' => 'required',
                'origin' => 'required'
            ],

            ['required' => ':attribute field is required.']
        );

        if ($validator->fails()) {
            $res = ['status' => false, 'message' => 'Validation error', 'errors' => $validator->errors()];
            return response($res, 400);
        }

        try {

            //Get Data Borrower 
            $borrowerDetails = $this->getBorrowerDetails($request);
            if (!$borrowerDetails) return response(['status_code' => 404, 'errors' => 'Data borrower not found'], 404);

            // $trx_id = $this->getTransactionId($request->borrower_id, self::PATH_SHARE_HOLDER, self::VENDOR);
            $trx_id = $this->getTransactionId();

            //TODO where field value to get ?. 
            $body = [
                'trx_id' => $trx_id,
                'npwp_company' => $borrowerDetails->npwp_perusahaan,
                'company_name' => $borrowerDetails->nm_bdn_hukum,
                'nik_share_holder' => $borrowerDetails->ktp,
                'name_share_holder' => $borrowerDetails->nama
            ];

            $response = $this->connectApi($body, 'shareholder');
            $responseErrors = $this->checkErrors($body, $response, 1);
            if ($responseErrors) return $responseErrors;

            $resultVerify = $this->putVerifyMaster($request, $response, self::PATH_SHARE_HOLDER);

            if ($resultVerify) {

                VerifyShareHolder::updateOrCreate(
                    ['verijelas_id' => $resultVerify->id],
                    [
                        'npwp_company' =>  $borrowerDetails->npwp_perusahaan,
                        'valid_npwp_company' => $response->data->npwp_company,
                        'company_name' =>  $borrowerDetails->nm_bdn_hukum,
                        'valid_company_name' => $response->data->company_name,
                        'nik_share_holder' => $borrowerDetails->ktp,
                        'valid_nik_share_holder' => $response->data->nik_share_holder,
                        'name_share_holder' => $borrowerDetails->nama,
                        'valid_name_share_holder' => $response->data->name_share_holder,
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                );


                $this->putVerifyLog($resultVerify, $body, $response, $this->errorData);
                return response(['status_code' => $response->status, 'data' => $response->data, 'errors' => $this->errorData]);
            }
        } catch (\Exception $e) {
            Helper::logDbApp('VerifyController.php', 731, $e->getMessage());
            return response(['status_code' => 500, 'errors' => $e->getMessage()], 500);
        }
    }

    public function getDataList(Request $request)
    {

        $returnHTML = "";

        $resultVerify = Verify::where('brw_user_id', $request->borrower_id)->where('vendor_name', $request->vendor)->get();
        if ($resultVerify->count() == 0)  return '<div class="text-left mb-4"><span class="badge badge-dark" style="font-size: 14px;">Data belum diverifikasi </span></div>';

        foreach ($resultVerify as $val) {

            //complete id 
            $completeIdData = VerifyComplete::where('verify_id', $val['id'])->first();
            if ($completeIdData) {

                $selfie_photo_link = route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $completeIdData->selfie_photo) . '?t=' . date('Y-m-d h:i:sa')]);

                $returnHTML .= "<div class='text-left' style='background-color: #2c3e50;padding: 5px; color: #fff;'><b>COMPLETE ID</b></div><hr>";
                $returnHTML .= "<table class='table table-striped table-bordered table-responsive-sm' style='font-size: 11px; !important;'>";
                $returnHTML .= "<tr>";
                $returnHTML .= "<th>" . __('verify.hasil.nik') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.nama') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_name') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.birthdate') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_birthdate') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.birthplace') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_birthplace') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_address') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.selfie_photo') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_selfie_photo') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.created_at') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.updated_at') . "</th>";
                $returnHTML .= "</tr>";

                $returnHTML .= "<tr>";
                $returnHTML .= "<td>" . $completeIdData->nik . "</td>";
                $returnHTML .= "<td>" . $completeIdData->name . "</td>";
                $returnHTML .= "<td>" . Helper::validResultVeriJelas($completeIdData->valid_name) . "</td>";
                $returnHTML .= "<td>" . date('d-m-Y', strtotime($completeIdData->birthdate)) . "</td>";
                $returnHTML .= "<td>" . Helper::validResultVeriJelas($completeIdData->valid_birthdate) . "</td>";
                $returnHTML .= "<td>" . $completeIdData->birthplace . "</td>";
                $returnHTML .= "<td>" . Helper::validResultVeriJelas($completeIdData->valid_birthplace) . "</td>";
                $returnHTML .= "<td>" . $completeIdData->address . "</td>";
                $returnHTML .= "<td><a href='" . $selfie_photo_link . "' target='_blank' class='btn btn-secondary btn-sm'>" . __('verify.hasil.view_photo')  . "</a></td>";
                $returnHTML .= "<td>" . Helper::scorePhotoVeriJelas($completeIdData->valid_selfie_photo) . "</td>";
                $returnHTML .= "<td>" . $completeIdData->created_at . "</td>";
                $returnHTML .= "<td>" . $completeIdData->updated_at . "</td>";
                $returnHTML .= "</tr>";
                $returnHTML .= "</table>";
            }

            //OCR
            $ocrData = VerifyOcrExtra::where('verify_id', $val['id'])->first();
            if ($ocrData) {

                $returnHTML .= "<div class='text-left mt-2' style='background-color: #2c3e50;padding: 5px; color: #fff;'><b>OCR</b></div><hr>";
                $returnHTML .= "<table class='table table-striped table-bordered table-responsive-sm' style='font-size: 11px; !important;'>";
                $returnHTML .= "<tr>";
                $returnHTML .= "<th>" . __('verify.hasil.nik') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.nama') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.jenis_kelamin') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.gol_darah') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.birthdate') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.birthplace') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.address') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.kewarganegaraan') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.agama') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.status') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.pekerjaan') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.created_at') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.updated_at') . "</th>";
                $returnHTML .= "</tr>";
                $returnHTML .= "<tr>";
                $returnHTML .= "<td>" . $ocrData->nik . "</td>";
                $returnHTML .= "<td>" . $ocrData->nama . "</td>";
                $returnHTML .= "<td>" . $ocrData->jenis_kelamin . "</td>";
                $returnHTML .= "<td>" . $ocrData->gol_darah . "</td>";
                $returnHTML .= "<td>" . $ocrData->tgl_lahir . "</td>";
                $returnHTML .= "<td>" . $ocrData->tempat_lahir . "</td>";
                $returnHTML .= "<td>" . $ocrData->alamat;
                $returnHTML .= "<br/>" . $ocrData->kelurahan;
                $returnHTML .= "<br/>" . $ocrData->kecamatan;
                $returnHTML .= "<br/>" . $ocrData->kota;
                $returnHTML .= "<br/>" . $ocrData->provinsi . "</td>";
                $returnHTML .= "<td>" . $ocrData->kewarganegaraan . "</td>";
                $returnHTML .= "<td>" . $ocrData->agama . "</td>";
                $returnHTML .= "<td>" . $ocrData->status_perkawinan . "</td>";
                $returnHTML .= "<td>" . $ocrData->pekerjaan . "</td>";
                $returnHTML .= "<td>" . $ocrData->created_at . "</td>";
                $returnHTML .= "<td>" . $ocrData->updated_at . "</td>";
                $returnHTML .= "</tr>";
                $returnHTML .= "</table>";
            }

            //NPWP
            $npwpData = ($request->brw_type == 1) ? VerifyIncome::where('verify_id', $val['id'])->first() : VerifyIncomePersero::where('verify_id', $val['id'])->first();
            if ($npwpData) {

                $category = ($val['path'] === 'income') ? 'Individu' : 'Badan Hukum';
                $returnHTML .= "<div class='text-left mt-2' style='background-color: #2c3e50;padding: 5px; color: #fff;'><b>NPWP " . $category . "</b></div><hr>";
                $returnHTML .= "<table class='table table-striped table-bordered table-responsive-sm' style='font-size: 11px; !important;'>";
                $returnHTML .= "<tr>";
                $returnHTML .= "<th>" . __('verify.hasil.npwp') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_npwp') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.income') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_income') . "</th>";

                if ($val['path'] === 'income') {
                    $returnHTML .= "<th>" . __('verify.hasil.nik') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.valid_nik') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.name') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.valid_name') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.match_result') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.birthdate') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.valid_birthdate') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.birthplace') . "</th>";
                    $returnHTML .= "<th>" . __('verify.hasil.valid_birthplace') . "</th>";
                }

                $returnHTML .= "<th>" . __('verify.hasil.created_at') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.updated_at') . "</th>";
                $returnHTML .= "</tr>";

                $returnHTML .= "<tr>";
                $returnHTML .= "<td>" . $npwpData->npwp . "</td>";

                $returnHTML .= "<td>" . Helper::validResultVeriJelas($npwpData->valid_npwp) . "</td>";
                $returnHTML .= "<td>" . $npwpData->income . "</td>";
                $returnHTML .= "<td>" . Helper::scoreIncomeVerijelas($npwpData->valid_income) . "</td>";

                if ($val['path'] === 'income') {
                    $returnHTML .= "<td>" . $npwpData->nik . "</td>";
                    $returnHTML .= "<td>" . Helper::validResultVeriJelas($npwpData->valid_nik)  . "</td>";
                    $returnHTML .= "<td>" . $npwpData->name . "</td>";
                    $returnHTML .= "<td>" . Helper::validResultVeriJelas($npwpData->valid_name)  . "</td>";
                    $returnHTML .= "<td>" . (($npwpData->match_result == 1) ? '<span class="badge badge-success" style="font-size: 1em;">Valid</span>' : '<span class="badge badge-danger" style="font-size: 1em;">Tidak Valid</span>') . "</td>";

                    $returnHTML .= "<td>" . date('d-m-Y', strtotime($npwpData->birthdate)) . "</td>";
                    $returnHTML .= "<td>" .  Helper::validResultVeriJelas($npwpData->valid_birthdate) . "</td>";
                    $returnHTML .= "<td>" . $npwpData->birthplace . "</td>";
                    $returnHTML .= "<td>" .  Helper::validResultVeriJelas($npwpData->valid_birthplace) . "</td>";
                }

                $returnHTML .= "<td>" . $npwpData->created_at . "</td>";
                $returnHTML .= "<td>" . $npwpData->updated_at . "</td>";
                $returnHTML .= "</tr>";
                $returnHTML .= "</table>";
            }

            $tempatKerjaData = VerifyTempatKerja::where('verify_id', $val['id'])->first();
            if ($tempatKerjaData) {
                $returnHTML .= "<div class='text-left mt-2' style='background-color: #2c3e50;padding: 5px; color: #fff;'><b>Workplace Verification F</b></div><hr>";
                $returnHTML .= "<table class='table table-striped table-bordered table-responsive-sm' style='font-size: 11px; !important;'>";
                $returnHTML .= "<tr>";
                $returnHTML .= "<th>" . __('verify.hasil.nik') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_nik') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.name') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_name') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.company_name') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_company_name') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_company') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.company_phone') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.valid_company_phone') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.created_at') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.updated_at') . "</th>";
                $returnHTML .= "</tr>";
                $returnHTML .= "<tr>";
                $returnHTML .= "<td>" . $tempatKerjaData->nik . "</td>";
                $returnHTML .= "<td>" . Helper::validResultVeriJelas($tempatKerjaData->valid_nik)  . "</td>";
                $returnHTML .= "<td>" . $tempatKerjaData->name . "</td>";
                $returnHTML .= "<td>" . Helper::validResultVeriJelas($tempatKerjaData->valid_name)  . "</td>";
                $returnHTML .= "<td>" . $tempatKerjaData->company_name . "</td>";
                $returnHTML .= "<td>" . $tempatKerjaData->valid_company_name  . "</td>";
                $returnHTML .= "<td>" . Helper::validResultVeriJelas($tempatKerjaData->valid_company)  . "</td>";
                $returnHTML .= "<td>" . $tempatKerjaData->company_phone . "</td>";
                $returnHTML .= "<td>" . Helper::validResultVeriJelas($tempatKerjaData->valid_company_phone)  . "</td>";
                $returnHTML .= "<td>" . $tempatKerjaData->created_at . "</td>";
                $returnHTML .= "<td>" . $tempatKerjaData->updated_at . "</td>";
                $returnHTML .= "</tr>";
                $returnHTML .= "</table>";
            }

            //Negative List
            $negativeListData = VerifyNegativeList::where('verify_id', $val['id'])->first();
            if ($negativeListData) {

                $detailUrl =  $negativeListData->detail_url;
                $returnHTML .= "<div class='text-left mt-2' style='background-color: #2c3e50;padding: 5px; color: #fff;'><b>Negative List Verification J</b></div><hr>";
                $returnHTML .= "<table class='table table-striped table-bordered table-responsive-sm' style='font-size: 11px; !important;'>";
                $returnHTML .= "<tr>";
                $returnHTML .= "<th>" . __('verify.hasil.nik') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.nama') . "</th>";
                $returnHTML .=  (($negativeListData->valid_nik == 1) ? '<th>' . __('verify.hasil.valid_name') . '</th>' : '');
                $returnHTML .=  (($negativeListData->valid_nik == 1) ? '<th>Detail Url</th>' : '');
                $returnHTML .= "<th>Status</th>";
                $returnHTML .= "<th>" . __('verify.hasil.created_at') . "</th>";
                $returnHTML .= "<th>" . __('verify.hasil.updated_at') . "</th>";
                $returnHTML .= "</tr>";

                $returnHTML .= "<tr>";
                $returnHTML .= "<td>" . $negativeListData->nik . "</td>";
                $returnHTML .= "<td>" . $negativeListData->name . "</td>";
                $returnHTML .=  (($negativeListData->valid_nik == 1) ?  "<td>" . $negativeListData->valid_name . "</td>" : "");
                $returnHTML .=  (($negativeListData->valid_nik == 1) ? "<td><a href='" .  $detailUrl  . "' class='btn btn-secondary btn-sm' target='_blank'>" . __('verify.hasil.view_document') . "</a></td>" : "");
                $returnHTML .= "<td>" . (($negativeListData->valid_nik == 1) ? '<span class="badge badge-danger" style="font-size: 1em;">Data valid pada negative list </span>' : '<span class="badge badge-success" style="font-size: 1em;">Data tidak ditemukan pada negative list</span>') . "</td>";
                $returnHTML .= "<td>" . $negativeListData->created_at . "</td>";
                $returnHTML .= "<td>" . $negativeListData->updated_at . "</td>";
                $returnHTML .= "</tr>";
                $returnHTML .= "</table>";
            }
        }

        return $returnHTML;
    }

    public function postCheck(Request $request)
    {

        $dataVeriJelasResponses = [];

        if ($request->complete_id == 1) {
            $response = $this->verifyCompleteId($request);
            $dataVeriJelasResponses['complete_id'] =  json_decode($response->getContent());
        }


        if ($request->ocr == 1) {
            $response = $this->verifyOcrExtra($request);
            $dataVeriJelasResponses['ocr'] = json_decode($response->getContent());
        }

        if ($request->npwp_verify == 1) {
            $response = ($request->brw_type == 1) ?  $this->verifyPendapatan($request) : $this->verifyPendapatanPersero($request);
            $keyLabel =  ($request->brw_type == 1) ? 'npwp' : 'npwp_persero';
            $dataVeriJelasResponses[$keyLabel] = json_decode($response->getContent());
        }

        if ($request->tempat_kerja == 1) {
            $response = $this->verifyTempatKerja($request);
            $dataVeriJelasResponses['tempat_kerja'] = json_decode($response->getContent());
        }

        if ($request->negativelist == 1) {
            $response = $this->verifyNegativeList($request);
            $dataVeriJelasResponses['negativelist'] = json_decode($response->getContent());
        }


        $returnHTML = "";
        $arrayLabel = ['selfie_photo', 'income', 'detail_url'];

        if (count($dataVeriJelasResponses)) {

            foreach ($dataVeriJelasResponses as $key => $val) {

                $returnHTML .= "<div class='text-left' style='background-color: #2980b9;padding: 5px; color: #fff;'><b>" .  self::LABEL_TITLE[$key] . "</b></div><hr>";
                if (isset($val->data)) {
                    foreach ($val->data as $key2 => $val2) {
                        $titleField =  __('verify.cek.' . str_replace('_', ' ', ucfirst($key2)));

                        if ($val2 === true || $val2 == 1) {
                            $val2 = 'Valid';
                            $styleInput  = 'background-color: #28a745; color: white;font-size: 10px;width: 10% !important';
                        } elseif ($val2 === false) {
                            $val2 = 'Tidak Valid';
                            $styleInput = 'background-color: #dc3545; color: white;font-size: 10px;width: 16% !important';
                        } else {
                            $styleInput =  'font-size: 11px;';
                        }

                        $returnHTML .= '<div class="form-group text-left">';
                        $returnHTML .= '<label for="' . $key2 . '" class="control-label font-weight-bold">' . $titleField . '</label>';

                        if (in_array($key2, $arrayLabel)) {
                            switch ($key2) {
                                case 'selfie_photo':
                                    $returnHTML .= ' <br/>' . Helper::scorePhotoVerijelas($val2);
                                    break;
                                case 'income':
                                    $returnHTML .= ' <br/>' . Helper::scoreIncomeVerijelas($val2, true);
                                    break;
                                case 'detail_url':
                                    $returnHTML .= "<br/><a href='" .   $val2  . "' class='btn btn-secondary btn-sm' target='_blank'>" . __('verify.hasil.view_document') . "</a>";
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            $returnHTML .= '<input type="text" readonly class="form-control" value="' . $val2 . '" style="' . $styleInput . '">';
                        }


                        $returnHTML .= '</div>';
                    }
                } else {
                    if (isset($val->badge)) {
                        $returnHTML .= '<div class="text-left mb-2"><span class="' . $val->badge . '">' . $val->errors . '</span></div>';
                    } else {
                        $returnHTML .= '<div class="text-left mb-2"><span class="badge badge-danger">' . $val->errors . '</span></div>';
                    }
                }
            }
        } else {
            $returnHTML .= '<div class="text-left mb-2"><span class="badge badge-warning">Belum ada parameter verijelas yang dipilih</span></div>';
        }

        return $returnHTML;
    }


    public function verifyPutLogTimeout(Request $request)
    {
        $sessionTrxId = ($request->session()->has('session_trx_id')) ? $request->session()->get('session_trx_id') : ''; 
        $result = Helper::logDbApp('VerifyController.php', 1077, "Trx id: ". $sessionTrxId. ". ".$request->description);
        if ($result) {
            return response()->json(['message' => 'ok']);
        } else {
            return response()->json(['message' => 'failed']);
        }
    }
}
