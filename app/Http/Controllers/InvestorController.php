<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investor;
use App\PenarikanDana;
use App\DetilInvestor;
use App\BniEnc;
use App\MutasiInvestor;
use App\Events\MutasiInvestorEvent;
use App\RekeningInvestor;
use App\Proyek;
use App\PendanaanAktif;
use App\LogPendanaan;
use App\LogPengembalianDana;
use App\LogSuspend;
use App\AuditTrail;
// use App\Subscribe;
use Hash;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Auth;
//use App\Jobs\AdminAddPendanaan;

use App\Exports\DetilInvestorExport;
use App\Exports\DanaInvestorProyek;
use App\Exports\PenarikanExport;
use Excel;

use GuzzleHttp\Client;
use App\Jobs\InvestorVerif;
use App\MasterProvinsi;
use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;
use DB;
use Validator;
use Image;
use App\Http\Middleware\NotifikasiProyek;
use App\Http\Middleware\StatusProyek;
use App\Mail\AdminAddPendanaanEmail;
use Mail;
use App\LogAkadDigiSignInvestor;
use App\InvestorPengurus;
use App\Helpers\Helper;

class InvestorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //development
    // private const CLIENT_ID = '805';
    // private const KEY = '34e64c3fe14335eb64f5c1b2d6e75508';
    // private const API_URL = 'https://apibeta.bni-ecollection.com/';


    //production
    private const CLIENT_ID = '757';
    private const KEY = '9f918ff65dc67027fc5670b7b7a7e89f';
    private const API_URL = 'https://api.bni-ecollection.com/';

    public function __construct()
    {
        // $this->middleware('auth:admin',['except' => ['verificationCode']]);
        $this->middleware('auth:admin');
        // $this->middleware(NotifikasiProyek::class);
        $this->middleware(StatusProyek::class);
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function admin_verif()
    {

        // $userverif = Investor::where('status', 'pending')->get();
        $master_provinsi = MasterProvinsi::distinct('kode_provinsi', 'nama_provinsi')->get(['kode_provinsi', 'nama_provinsi']);
        $master_agama = MasterAgama::all();
        $master_asset = MasterAsset::all();
        $master_badan_hukum = MasterBadanHukum::all();
        $master_bank = MasterBank::all();
        $master_bidang_pekerjaan = MasterBidangPekerjaan::all();
        $master_jenis_kelamin = MasterJenisKelamin::all();
        $master_jenis_pengguna = MasterJenisPengguna::all();
        $master_kawin = MasterKawin::all();
        $master_kepemilikan_rumah = MasterKepemilikanRumah::all();
        $master_negara = MasterNegara::all();
        $master_online = MasterOnline::all();
        $master_pekerjaan = MasterPekerjaan::all();
        $master_pendapatan = MasterPendapatan::all();
        $master_pendidikan = MasterPendidikan::all();
        $master_pengalaman_kerja = MasterPengalamanKerja::all();

        return view('pages.admin.investor_verifikasi', [
            'master_provinsi' => $master_provinsi,
            'master_agama' => $master_agama,
            'master_asset' => $master_asset,
            'master_badan_hukum' => $master_badan_hukum,
            'master_bank' => $master_bank,
            'master_bidang_pekerjaan' => $master_bidang_pekerjaan,
            'master_jenis_kelamin' => $master_jenis_kelamin,
            'master_jenis_pengguna' => $master_jenis_pengguna,
            'master_kawin' => $master_kawin,
            'master_kepemilikan_rumah' => $master_kepemilikan_rumah,
            'master_negara' => $master_negara,
            'master_online' => $master_online,
            'master_pekerjaan' => $master_pekerjaan,
            'master_pendapatan' => $master_pendapatan,
            'master_pendidikan' => $master_pendidikan,
            'master_pengalaman_kerja' => $master_pengalaman_kerja,
            'master_provinsi' => $master_provinsi,
        ])/*->with('userverif', $userverif)*/;
    }

    public function get_verifikasi_datatables()
    {
        $verifikasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
            ->where('investor.status', 'pending')
            ->orderBy('investor.id', 'desc')
            ->get([
                'investor.id',
                'investor.username',
                'investor.email',
                'investor.password',
                'investor.status',
                'detil_investor.nama_investor'
            ]);
        // var_dump($verifikasi);die;
        $response = ['data' => $verifikasi];

        return response()->json($response);
    }

    public function data_email_verif(Request $request)
    {
        $cek_data = 0;
        $variabelCari = 0;
        if ($request->username != null) {
            $variabelCari = $request->username;
            $cek_data = Investor::where('username', 'like', '%' . $request->username . '%')
                ->whereIn('status',['Not Active','notfilled'])
                ->orderBy('id', 'desc')
                ->count();
        }else if ($request->search_email != null) {
            $variabelCari = $request->search_email;
            $cek_data = Investor::where('email', 'like', '%' . $request->search_email . '%')
                ->whereIn('status',['Not Active','notfilled'])
                ->orderBy('id', 'desc')
                ->count();
        } else if ($request->phone_number != '') {
            $variabelCari = $request->kode_operator . $request->phone_number;
            $cek_data = DetilInvestor::leftjoin('investor','detil_investor.investor_id', '=', 'investor.id')
                ->where('detil_investor.phone_investor', 'like', '%' . $request->kode_operator . $request->phone_number . '%')
                ->whereIn('investor.status',['Not Active','notfilled'])
                ->orderBy('investor.id', 'desc')
                ->count();
        }
        // echo $cek_data;die;

        if ($cek_data != 0) {
            $response = ['status' => 'Ada'];
            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Kelola Tautan / OTP Lender";
            $audit->description = "Kelola Tautan / OTP Lender => pencarian : ".$variabelCari;
            $audit->ip_address =  \Request::ip();
            $audit->save();
        } else {
            $response = ['status' => 'Kosong'];
        }

        return response()->json($response);
    }

    public function get_email_verif_datatables(Request $request)
    {
        if ($request->username != '') {
            $get_email_verif = Investor::where('investor.username', 'like', '%' . $request->username . '%')
                ->whereIn('status',['Not Active','notfilled'])
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status'
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'investor.email_verif',
                    'investor.otp'
                ]);
        } else if ($request->search_email != '') {
            $get_email_verif = Investor::where('investor.email', 'like', '%' . $request->search_email . '%')
                ->whereIn('status',['Not Active','notfilled'])
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status'
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'investor.email_verif',
                    'investor.otp'
                ]);
        }  else if ($request->phone_number != '') {
            $get_email_verif = DetilInvestor::leftjoin('investor','detil_investor.investor_id', '=', 'investor.id')
                ->where('detil_investor.phone_investor', 'like', '%' . $request->kode_operator . $request->phone_number . '%')
                ->whereIn('investor.status',['Not Active','notfilled'])
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status'
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'investor.email_verif',
                    'investor.otp'
                ]);
        }

        $response = ['data' => $get_email_verif];

        return response()->json($response);
    }

    public function get_mutasi_datatables(Request $request)
    {
        $user = Auth::user();
        if ($request->unallocated == 'yes') {
            $mutasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.investor_id', '=', 'investor.id')
                ->where('rekening_investor.unallocated', '>', 0)
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    'rekening_investor.unallocated',
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    DB::raw('(select count(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as total'),
                    DB::raw('(select count(log_pengembalian_dana.proyek_id) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as total_log'),
                    DB::raw('(SELECT COUNT(*) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status != 4 AND a.status = 1) as jumlah_pendanaan'),
                    DB::raw('(SELECT sum(pendanaan_aktif.nominal_awal)
                                FROM `proyek`
                                LEFT JOIN `pendanaan_aktif` ON `pendanaan_aktif`.`proyek_id` = `proyek`.`id`
                                WHERE `pendanaan_aktif`.`investor_id` = investor.id AND `pendanaan_aktif`.`status` = 1 AND `proyek`.`id` NOT IN (
                                SELECT proyek_id
                                FROM log_pengembalian_dana
                                WHERE investor_id = investor.id
                                GROUP BY proyek_id)) as total_pendanaan'),
                    DB::raw('(select sum(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as jumlah_nominal'),
                    DB::raw('(select sum(log_pengembalian_dana.nominal) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as jumlah_nominal_log'),
                    'rekening_investor.unallocated',
                    'detil_investor.tipe_pengguna',
                    'detil_investor.nama_perusahaan',
                ]);
        } else if ($request->unallocated == 'no') {
            $mutasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.investor_id', '=', 'investor.id')
                ->where('rekening_investor.unallocated', '=', 0)
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',

                    'rekening_investor.unallocated',
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    DB::raw('(select count(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as total'),
                    DB::raw('(select count(log_pengembalian_dana.proyek_id) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as total_log'),
                    DB::raw('(SELECT COUNT(*) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status != 4 AND a.status = 1) as jumlah_pendanaan'),
                    DB::raw('(SELECT sum(pendanaan_aktif.nominal_awal)
                                FROM `proyek`
                                LEFT JOIN `pendanaan_aktif` ON `pendanaan_aktif`.`proyek_id` = `proyek`.`id`
                                WHERE `pendanaan_aktif`.`investor_id` = investor.id AND `pendanaan_aktif`.`status` = 1 AND `proyek`.`id` NOT IN (
                                SELECT proyek_id
                                FROM log_pengembalian_dana
                                WHERE investor_id = investor.id
                                GROUP BY proyek_id)) as total_pendanaan'),
                    DB::raw('(select sum(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as jumlah_nominal'),
                    DB::raw('(select sum(log_pengembalian_dana.nominal) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as jumlah_nominal_log'),
                    'rekening_investor.unallocated',
                    'detil_investor.tipe_pengguna',
                    'detil_investor.nama_perusahaan',
                ]);
        } else if ($request->name != '') {
            $mutasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.investor_id', '=', 'investor.id')
                ->where('detil_investor.nama_investor', '=', $request->name)
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    'rekening_investor.unallocated'
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    DB::raw('(select count(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as total'),
                    DB::raw('(select count(log_pengembalian_dana.proyek_id) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as total_log'),
                    DB::raw('(SELECT COUNT(*) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status != 4 AND a.status = 1) as jumlah_pendanaan'),
                    DB::raw('(SELECT sum(pendanaan_aktif.nominal_awal)
                                FROM `proyek`
                                LEFT JOIN `pendanaan_aktif` ON `pendanaan_aktif`.`proyek_id` = `proyek`.`id`
                                WHERE `pendanaan_aktif`.`investor_id` = investor.id AND `pendanaan_aktif`.`status` = 1 AND `proyek`.`id` NOT IN (
                                SELECT proyek_id
                                FROM log_pengembalian_dana
                                WHERE investor_id = investor.id
                                GROUP BY proyek_id)) as total_pendanaan'),
                    DB::raw('(select sum(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as jumlah_nominal'),
                    DB::raw('(select sum(log_pengembalian_dana.nominal) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as jumlah_nominal_log'),
                    'rekening_investor.unallocated',
                    'detil_investor.tipe_pengguna',
                    'detil_investor.nama_perusahaan',
                ]);
        } else if ($request->username != '') {
            $mutasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.investor_id', '=', 'investor.id')
                ->where('investor.username', 'like', '%' . $request->username . '%')
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    'rekening_investor.unallocated',
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    DB::raw('(select count(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as total'),
                    DB::raw('(select count(log_pengembalian_dana.proyek_id) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as total_log'),
                    DB::raw('(SELECT COUNT(*) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status != 4 AND a.status = 1) as jumlah_pendanaan'),
                    DB::raw('(SELECT sum(pendanaan_aktif.nominal_awal)
                                FROM `proyek`
                                LEFT JOIN `pendanaan_aktif` ON `pendanaan_aktif`.`proyek_id` = `proyek`.`id`
                                WHERE `pendanaan_aktif`.`investor_id` = investor.id AND `pendanaan_aktif`.`status` = 1 AND `proyek`.`id` NOT IN (
                                SELECT proyek_id
                                FROM log_pengembalian_dana
                                WHERE investor_id = investor.id
                                GROUP BY proyek_id)) as total_pendanaan'),
                    DB::raw('(select sum(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as jumlah_nominal'),
                    DB::raw('(select sum(log_pengembalian_dana.nominal) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as jumlah_nominal_log'),
                    'rekening_investor.unallocated',
                    'detil_investor.tipe_pengguna',
                    'detil_investor.nama_perusahaan',
                ]);
        } else if ($request->search_email != '') {
            $mutasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.investor_id', '=', 'investor.id')
                ->where('investor.email', 'like', '%' . $request->search_email . '%')
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    'rekening_investor.unallocated',
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    DB::raw('(select count(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as total'),
                    DB::raw('(select count(log_pengembalian_dana.proyek_id) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as total_log'),
                    DB::raw('(SELECT COUNT(*) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status != 4 AND a.status = 1) as jumlah_pendanaan'),
                    //DB::raw('(SELECT SUM(a.nominal_awal) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status !=4 AND a.status=1) as total_pendanaan'),
                    DB::raw('(SELECT sum(pendanaan_aktif.nominal_awal)
                                FROM `proyek`
                                LEFT JOIN `pendanaan_aktif` ON `pendanaan_aktif`.`proyek_id` = `proyek`.`id`
                                WHERE `pendanaan_aktif`.`investor_id` = investor.id AND `pendanaan_aktif`.`status` = 1 AND `proyek`.`id` NOT IN (
                                SELECT proyek_id
                                FROM log_pengembalian_dana
                                WHERE investor_id = investor.id
                                GROUP BY proyek_id)) as total_pendanaan'),
                    DB::raw('(select sum(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as jumlah_nominal'),
                    DB::raw('(select sum(log_pengembalian_dana.nominal) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as jumlah_nominal_log'),
                    'rekening_investor.unallocated',
                    'detil_investor.tipe_pengguna',
                    'detil_investor.nama_perusahaan',
                ]);
        } else if ($request->va_number != '') {
            $mutasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('detil_rekening_investor', 'detil_rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.investor_id', '=', 'investor.id')
                ->where('rekening_investor.va_number', 'like', '%' . $request->va_number . '%')
                ->orWhere('detil_rekening_investor.va_number', 'like', '%' . $request->va_number . '%')
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    'rekening_investor.unallocated',
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    DB::raw('(select count(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as total'),
                    DB::raw('(select count(log_pengembalian_dana.proyek_id) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as total_log'),
                    DB::raw('(SELECT COUNT(*) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status != 4 AND a.status = 1) as jumlah_pendanaan'),
                    //DB::raw('(SELECT SUM(a.nominal_awal) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status !=4 AND a.status=1) as total_pendanaan'),
                    DB::raw('(SELECT sum(pendanaan_aktif.nominal_awal)
                                FROM `proyek`
                                LEFT JOIN `pendanaan_aktif` ON `pendanaan_aktif`.`proyek_id` = `proyek`.`id`
                                WHERE `pendanaan_aktif`.`investor_id` = investor.id AND `pendanaan_aktif`.`status` = 1 AND `proyek`.`id` NOT IN (
                                SELECT proyek_id
                                FROM log_pengembalian_dana
                                WHERE investor_id = investor.id
                                GROUP BY proyek_id)) as total_pendanaan'),
                    DB::raw('(select sum(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as jumlah_nominal'),
                    DB::raw('(select sum(log_pengembalian_dana.nominal) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as jumlah_nominal_log'),
                    'rekening_investor.unallocated',
                    'detil_investor.tipe_pengguna',
                    'detil_investor.nama_perusahaan',
                ]);
        } else if ($request->phone_number != '') {
            $mutasi = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
                ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.investor_id', '=', 'investor.id')
                ->where('detil_investor.phone_investor', 'like', '%' . $request->kode_operator . $request->phone_number . '%')
                ->orWhere('detil_investor.phone_investor', 'like', '%' . $request->phone_number . '%')
                ->groupBy([
                    'investor.id',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    'rekening_investor.unallocated',
                ])
                ->get([
                    'investor.id AS idInvestor',
                    'investor.email',
                    'investor.username',
                    'investor.status',
                    'detil_investor.nama_investor',
                    DB::raw('(select count(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as total'),
                    DB::raw('(select count(log_pengembalian_dana.proyek_id) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as total_log'),
                    DB::raw('(SELECT COUNT(*) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status != 4 AND a.status = 1) as jumlah_pendanaan'),
                    //DB::raw('(SELECT SUM(a.nominal_awal) FROM pendanaan_aktif a JOIN proyek b ON a.proyek_id = b.id WHERE a.investor_id = investor.id AND b.status !=4 AND a.status=1) as total_pendanaan'),
                    DB::raw('(SELECT sum(pendanaan_aktif.nominal_awal)
                                FROM `proyek`
                                LEFT JOIN `pendanaan_aktif` ON `pendanaan_aktif`.`proyek_id` = `proyek`.`id`
                                WHERE `pendanaan_aktif`.`investor_id` = investor.id AND `pendanaan_aktif`.`status` = 1 AND `proyek`.`id` NOT IN (
                                SELECT proyek_id
                                FROM log_pengembalian_dana
                                WHERE investor_id = investor.id
                                GROUP BY proyek_id)) as total_pendanaan'),
                    DB::raw('(select sum(pendanaan_aktif.nominal_awal) from pendanaan_aktif where investor_id = investor.id and status = 1) as jumlah_nominal'),
                    DB::raw('(select sum(log_pengembalian_dana.nominal) from log_pengembalian_dana where log_pengembalian_dana.investor_id = investor.id group by log_pengembalian_dana.investor_id) as jumlah_nominal_log'),
                    'rekening_investor.unallocated',
                    'detil_investor.tipe_pengguna',
                    'detil_investor.nama_perusahaan',
                ]);
        }

        // var_dump($mutasi);die;

        $response = ['data' => $mutasi];

        return response()->json($response);
    }

    public function get_mutasi_proyek(Request $request)
    {
        $id =  $request->id;

        $mutasiproyek = DB::select("SELECT proyek.nama, proyek.alamat, DATE_FORMAT(proyek.tgl_mulai, '%d %M %Y') AS tgl_mulai, DATE_FORMAT(pendanaan_aktif.tanggal_invest, '%d %M %Y') AS tanggal_invest, DATE_FORMAT(proyek.tgl_selesai, '%d %M %Y') AS tgl_selesai,pendanaan_aktif.id AS pendanaanAktifId FROM pendanaan_aktif INNER JOIN proyek ON proyek.id = pendanaan_aktif.proyek_id WHERE pendanaan_aktif.investor_id = '$id' GROUP BY proyek_id ASC");

        $response = ['data' => $mutasiproyek];
        return response()->json($response);
    }

    public function get_detil_mutasi_proyek(Request $request)
    {
        $id =  $request->id;

        $detilmutasiproyek = DB::select("SELECT log_pendanaan.tipe, format( log_pendanaan.nominal, 0) AS nominal,  DATE_FORMAT(log_pendanaan.created_at, '%d %M %Y') AS created_at, proyek.nama  FROM log_pendanaan INNER JOIN pendanaan_aktif ON pendanaan_aktif.id = log_pendanaan.pendanaanAktif_id INNER JOIN proyek ON pendanaan_aktif.proyek_id = proyek.id WHERE pendanaanAktif_id = '$id'");

        $response = ['datadetil' => $detilmutasiproyek];
        return response()->json($response);
    }

    public function get_detil_investor($id)
    {

        $detil_investor = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
            ->leftJoin('marketer', 'marketer.ref_code', '=', 'investor.ref_number')
            ->leftJoin('detil_marketer', 'detil_marketer.marketer_id', '=', 'marketer.id')
            ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
            ->leftJoin('ahli_waris_investor', 'ahli_waris_investor.id_investor', '=', 'investor.id')
            ->where('detil_investor.investor_id', $id)
            ->first([
                'detil_investor.*',
                'investor.keterangan as keteranganSuspend',
                'investor.suspended_by as namaSuspended',
                'investor.actived_by as namaActived',
                "investor.updated_at as tanggalSuspend",
                'marketer.ref_code',
                'detil_marketer.nama_lengkap as nama_marketer',
                'rekening_investor.va_number',

                'ahli_waris_investor.nama_ahli_waris as nama_ahli_waris',
                'ahli_waris_investor.hubungan_keluarga_ahli_waris as hub_ahli_waris',
                'ahli_waris_investor.nik_ahli_waris as nik_ahli_waris',
                'ahli_waris_investor.no_hp_ahli_waris as no_hp_ahli_waris',
                'ahli_waris_investor.kode_operator as kode_operator_ahli_waris',
                'ahli_waris_investor.alamat_ahli_waris as alamat_ahli_waris',

            ]);

        $getva = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE investor_id = $id");
        $va = array();
        if ($getva != NULL) {
            foreach ($getva as $item) {
                $index = "bank" . $item->kode_bank;
                $va[$index] = $item->va_number;
            }
        }

        $investor_pengurus = InvestorPengurus::where('investor_id', $id)->get();
        $response = ['detil_investor' => $detil_investor, 'investor_pengurus' => $investor_pengurus, 'va' => $va];

        return response()->json($response);
    }

    public function get_riwayat_mutasi($id)
    {

        $date = Carbon::now()->subDays(30);

        // $data_mutasi_awal = MutasiInvestor::where('investor_id', $id)
        //                                   // ->where('created_at', '<', $date)
        //                                   ->where('perihal', 'not like', '%pengembalian dana pokok%')
        //                                   ->where('perihal', 'not like', '%pengembalian pokok%')
        //                                   ->where('perihal', 'not like', '%sisa imbal hasil%')
        //                                   ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
        //                                   ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
        //                                   ->whereIn('tipe', ['CREDIT', 'DEBIT'])
        //                                   ->get();

        // $data_mutasi = "";
        $data_mutasi = MutasiInvestor::where('investor_id', $id)
            //  ->where('created_at','>=', Carbon::now()->subDays(30))
            ->where('perihal', 'not like', '%pengembalian dana pokok%')
            ->where('perihal', 'not like', '%pengembalian pokok%')
            ->where('perihal', 'not like', '%sisa imbal hasil%')
            ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
            ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
            ->where('perihal', 'not like', '%Akumulasi Keseluruhan Imbal Hasil%')
            ->where('perihal', 'not like', '%DANA POKOK : %')
            ->whereIn('tipe', ['CREDIT', 'DEBIT'])
            ->limit(10)
            ->get();
        // dd($data_mutasi);die;
        // dd($data_mutasi);die;

        $valuefirst = 0;
        $valuesecond = 0;
        $total = 0;
        $nul = 0;
        $i = 0;
        if (!empty($data_mutasi_awal)) {
            foreach ($data_mutasi_awal as $dt_a) {
                if ($dt_a->tipe == "DEBIT") {
                    $total_first = $dt_a->nominal - $valuefirst;
                    $valuesecond =  $total -= $total_first;
                } elseif ($dt_a->tipe == "CREDIT") {
                    $total_first = $dt_a->nominal + $valuefirst;
                    $valuesecond =  $total += $total_first;
                }
            }
        }
        if (!empty($data_mutasi)) {
            $data = array();
            foreach ($data_mutasi as $dt) {

                $i++;
                $column['Keterangan'] = (string) $dt->perihal . ' ' . $dt->kode_bank;
                $column['Tanggal'] = (string) Carbon::parse($dt->created_at)->format('d-m-Y');
                if ($dt->tipe == "DEBIT") {
                    $total_first = $dt->nominal - $valuefirst;
                    $valuesecond =  $total -= $total_first;

                    $column['Debit'] = (string) number_format($dt->nominal);
                    $column['Kredit'] = (string) $nul;
                } elseif ($dt->tipe == "CREDIT") {
                    $total_first = $dt->nominal + $valuefirst;
                    $valuesecond =  $total += $total_first;

                    $column['Debit'] = (string) $nul;
                    $column['Kredit'] = (string) number_format($dt->nominal);
                }
                $column['Saldo'] = (string) number_format($valuesecond);

                $data[] = $column;
            }
        }

        $parsingJSON = array("data" => $data);

        // dd($parsingJSON);die;

        echo json_encode($parsingJSON);
    }

    public function get_riwayat_mutasi_date($id, $startDate, $endDate)
    {

        $data_mutasi_awal = MutasiInvestor::where('investor_id', $id)
            ->where('created_at', '<', $startDate)
            ->where('perihal', 'not like', '%pengembalian dana pokok%')
            ->where('perihal', 'not like', '%pengembalian pokok%')
            ->where('perihal', 'not like', '%sisa imbal hasil%')
            ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
            ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
            ->where('perihal', 'not like', '%DANA POKOK : %')
            //    ->orderBy('created_at', 'ASC')
            ->whereIn('tipe', ['CREDIT', 'DEBIT'])
            ->get();
        $data_mutasi = MutasiInvestor::where('investor_id', $id)
            ->whereBetween('created_at', [$startDate . " 00:00:00", $endDate . " 23:59:59"])
            //  ->where('created_at', '>=', $startDate)
            //  ->where('created_at', '<=',$endDate)
            ->where('perihal', 'not like', '%pengembalian dana pokok%')
            ->where('perihal', 'not like', '%pengembalian pokok%')
            ->where('perihal', 'not like', '%sisa imbal hasil%')
            ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
            ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
            ->where('perihal', 'not like', '%Akumulasi Keseluruhan Imbal Hasil%')
            ->where('perihal', 'not like', '%DANA POKOK : %')
            //  ->orderBy('created_at', 'ASC')
            ->whereIn('tipe', ['CREDIT', 'DEBIT'])
            ->get();

        $valuefirst = 0;
        $valuesecond = 0;
        $valueLast = 0;
        $total = 0;
        $nul = 0;
        $i = 0;
        if (!empty($data_mutasi_awal)) {
            foreach ($data_mutasi_awal as $dt_a) {
                if ($dt_a->tipe == "DEBIT") {
                    $total_first = $dt_a->nominal - $valuefirst;
                    $valuesecond =  $total -= $total_first;
                } elseif ($dt_a->tipe == "CREDIT") {
                    $total_first = $dt_a->nominal + $valuefirst;
                    $valuesecond =  $total += $total_first;
                }
            }
        }
        if (!empty($data_mutasi)) {
            $data = array();
            foreach ($data_mutasi as $dt) {

                $i++;
                $column['Keterangan'] = (string) $dt->perihal . ' ' . $dt->kode_bank;
                $column['Tanggal'] = (string) Carbon::parse($dt->created_at)->format('d-m-Y');
                if ($dt->tipe == "DEBIT") {
                    $total_first = $dt->nominal - $valuefirst;
                    $valuesecond =  $total -= $total_first;

                    $column['Debit'] = (string) number_format($dt->nominal);
                    $column['Kredit'] = (string) $nul;
                } elseif ($dt->tipe == "CREDIT") {
                    $total_first = $dt->nominal + $valuefirst;
                    $valuesecond =  $total += $total_first;

                    $column['Debit'] = (string) $nul;
                    $column['Kredit'] = (string) number_format($dt->nominal);
                }
                $column['Saldo'] = (string) number_format($valuesecond);

                $data[] = $column;
            }
        }
        $parsingJSON = array("data" => $data);

        // dd($parsingJSON);die;

        echo json_encode($parsingJSON);
    }

    public function get_detil_pendanaan($id)
    {   
        $detil_pendanaan = Proyek::leftJoin('pendanaan_aktif', 'pendanaan_aktif.proyek_id', '=', 'proyek.id')
        ->where('pendanaan_aktif.investor_id', $id)
        ->where('proyek.status', '!=', 4)
        ->where('pendanaan_aktif.status', '=', 1)
        ->orderBy('pendanaan_aktif.id', 'desc')
        ->get([
            'proyek.nama',
            'proyek.tgl_mulai',
            'pendanaan_aktif.id',
            'pendanaan_aktif.total_dana',
            'pendanaan_aktif.tanggal_invest'
        ]);
        // $detil_pendanaan = Proyek::leftJoin('pendanaan_aktif', 'pendanaan_aktif.proyek_id', '=', 'proyek.id')
        //     ->leftJoin('log_pendanaan', 'log_pendanaan.pendanaanAktif_id', '=', 'pendanaan_aktif.id')
        //     ->where('pendanaan_aktif.investor_id', $id)
        //     ->where('pendanaan_aktif.status', '=', 1)
        //     ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $id . ' group by proyek_id')])
        //     ->groupBy('pendanaan_aktif.id')
        //     ->orderBy('pendanaan_aktif.id', 'desc')
        //     ->get([
        //         'proyek.nama',
        //         'proyek.tgl_mulai',
        //         'pendanaan_aktif.id',
        //         'pendanaan_aktif.total_dana',
        //         DB::raw('(case when log_pendanaan.tipe = "add new investation" then "investor" when log_pendanaan.tipe = "add active investation" then "investor" when log_pendanaan.tipe = "add active investation by admin" then "admin" else "" end) as tipe_user'),
        //         'pendanaan_aktif.tanggal_invest'
        //     ]);

        // $proyek_selesai = Proyek::leftJoin('log_pengembalian_dana', 'log_pengembalian_dana.proyek_id', '=', 'proyek.id')
        //     ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.proyek_id', '=', 'log_pengembalian_dana.proyek_id')
        //     ->leftJoin('log_pendanaan', 'log_pendanaan.pendanaanAktif_id', '=', 'pendanaan_aktif.id')
        //     ->where('log_pengembalian_dana.investor_id', $id)
        //     ->groupBy('log_pengembalian_dana.id')
        //     ->orderBy('log_pengembalian_dana.id', 'desc')
        //     ->get([
        //         'proyek.nama',
        //         'proyek.tgl_selesai',
        //         'log_pengembalian_dana.id',
        //         'log_pengembalian_dana.nominal',
        //         // DB::raw('(case when log_pendanaan.tipe = "add new investation" then "investor" when log_pendanaan.tipe = "add active investation by admin" then "admin" else "" end) as tipe_user'),
        //         // 'pendanaan_aktif.tanggal_invest'
        //     ]);

        $proyek_selesai = Proyek::leftJoin('log_pengembalian_dana', 'log_pengembalian_dana.proyek_id', '=', 'proyek.id')
        ->where('log_pengembalian_dana.investor_id', $id)
        ->where('proyek.status', 4)
        ->orderBy('log_pengembalian_dana.id', 'desc')
        ->get([
            'proyek.nama',
            'proyek.tgl_selesai',
            'log_pengembalian_dana.id',
            'log_pengembalian_dana.nominal'
        ]);
        
        $response = ['data' => $detil_pendanaan, 'data_selesai' => $proyek_selesai];

        return response()->json($response);
    }

    public function admin_mutasi()
    {
        // $investor = Investor::whereIn('status', ['active', 'suspend'])->with('rekeningInvestor')->with('pendanaanAktif')->with('detilInvestor')->with('mutasiInvestor')->get();
        // $proyek = Proyek::where('status', 1)->get();

        // return $investor;

        return view('pages.admin.investor_mutasi')/*->with('investor', $investor)->with('proyek', $proyek)*/;
    }

    public function get_nama_autocomplete(Request $request)
    {

        // echo $request->get('query');die;
        if ($request->get('query') != '') {
            $query = $request->get('query');
            $data = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('detil_investor.nama_investor', 'LIKE', "%{$query}%")
                ->orderBy('investor.id', 'desc')
                ->take(5)
                ->get();
            $output = '<ul class="list-group">';
            foreach ($data as $row) {
                $output .= '<li  class="list-group-item">&nbsp;<a href="#">' . $row->nama_investor . '</a>&nbsp;</li>';
            }
            $output .= '</ul>';

            echo $output;
        } else {
            $output = '';
            echo $output;
        }
    }

    public function data_investor(Request $request)
    {

        $cek_data = 0;

        if ($request->unallocated == 'yes') {
            $cek_data = RekeningInvestor::where('unallocated', '>', 0)
                ->orderBy('investor_id', 'desc')
                ->count();
        } else if ($request->unallocated == 'no') {
            $cek_data = RekeningInvestor::where('unallocated', '=', 0)
                ->orderBy('investor_id', 'desc')
                ->count();
        } else if ($request->name != null) {
            $cek_data = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('detil_investor.nama_investor', '=', $request->name)
                ->orderBy('investor.id', 'desc')
                ->count();
        } else if ($request->username != null) {
            $cek_data = Investor::where('username', 'like', '%' . $request->username . '%')
                ->orderBy('id', 'desc')
                ->count();
        }
        else if ($request->search_email != null) {
            $cek_data = Investor::where('email', 'like', '%' . $request->search_email . '%')
                ->orderBy('id', 'desc')
                ->count();
        } else if ($request->va_number != null) {
            $rekening_investor = DB::table('rekening_investor')->where('va_number', 'like', '%' . $request->va_number . '%')
                ->count();

            $detail_rekening_investor = DB::table('detil_rekening_investor')->where('va_number', 'like', '%' . $request->va_number . '%')
                ->where('va_number', 'like', '%' . $request->va_number . '%')
                ->count();

            if ($rekening_investor != 0) {
                $cek_data = $rekening_investor;
            } else {
                if ($detail_rekening_investor != 0) {
                    $cek_data = $detail_rekening_investor;
                }
            }
        } else if ($request->phone_number != null) {
            $kode_operator = $request->kode_operator;
            $phone_number = $request->phone_number;

            if ($kode_operator == '62') {
                $cek_data = DB::table('detil_investor')
                    ->where('phone_investor', 'like', '%' . $kode_operator . $phone_number . '%')
                    ->orWhere('phone_investor', 'like', '%0' . $phone_number . '%')
                    ->count();
            } else {
                $cek_data = DB::table('detil_investor')
                    ->where('phone_investor', 'like', '%' . $kode_operator . $phone_number . '%')
                    ->count();
            }
        }
        // echo $cek_data;die;

        if ($cek_data != 0) {
            $response = ['status' => 'Ada'];
        } else {
            $response = ['status' => 'Kosong'];
        }

        return response()->json($response);
    }

    public function get_investor_datatables($nama)
    {
        $investor = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
            ->where('detil_investor.nama_investor', '=', $nama)
            ->orderBy('investor.id', 'desc')
            ->get([
                'investor.id',
                'investor.username',
                'investor.email',
                'investor.status',
                'detil_investor.nama_investor',
                'detil_investor.no_ktp_investor',
                'detil_investor.no_npwp_investor',
                'detil_investor.pic_investor',
                'detil_investor.pic_ktp_investor',
                'detil_investor.pic_user_ktp_investor',
                'detil_investor.phone_investor',
                'detil_investor.pasangan_investor',
                'detil_investor.pasangan_phone',
                'detil_investor.job_investor',
                'detil_investor.alamat_investor',
                'detil_investor.rekening',
                'detil_investor.bank',
                'detil_investor.nama_investor',
            ]);
        $response = ['data' => $investor];

        return response()->json($response);
    }

    public function admin_manage()
    {
        // $investor = Investor::all();
        $master_provinsi = MasterProvinsi::distinct('kode_provinsi', 'nama_provinsi')->get(['kode_provinsi', 'nama_provinsi']);
        $master_agama = MasterAgama::all();
        $master_asset = MasterAsset::all();
        $master_badan_hukum = MasterBadanHukum::all();
        $master_bank = MasterBank::all();
        $master_bidang_pekerjaan = MasterBidangPekerjaan::all();
        $master_jenis_kelamin = MasterJenisKelamin::all();
        $master_jenis_pengguna = MasterJenisPengguna::all();
        $master_kawin = MasterKawin::all();
        $master_kepemilikan_rumah = MasterKepemilikanRumah::all();
        $master_negara = MasterNegara::all();
        $master_online = MasterOnline::all();
        $master_pekerjaan = MasterPekerjaan::all();
        $master_pendapatan = MasterPendapatan::all();
        $master_pendidikan = MasterPendidikan::all();
        $master_pengalaman_kerja = MasterPengalamanKerja::all();
        $master_hub_ahli_waris = DB::table('m_hub_ahli_waris')->select('id_hub_ahli_waris', 'jenis_hubungan')->get();
        $master_kode_operator = DB::table("m_kode_operator_negara")->get();
        $master_jabatan = DB::table('m_jabatan')->get();


        return view('pages.admin.investor_manage', [
            'master_provinsi' => $master_provinsi,
            'master_agama' => $master_agama,
            'master_asset' => $master_asset,
            'master_badan_hukum' => $master_badan_hukum,
            'master_bank' => $master_bank,
            'master_bidang_pekerjaan' => $master_bidang_pekerjaan,
            'master_jenis_kelamin' => $master_jenis_kelamin,
            'master_jenis_pengguna' => $master_jenis_pengguna,
            'master_kawin' => $master_kawin,
            'master_kepemilikan_rumah' => $master_kepemilikan_rumah,
            'master_negara' => $master_negara,
            'master_online' => $master_online,
            'master_pekerjaan' => $master_pekerjaan,
            'master_pendapatan' => $master_pendapatan,
            'master_pendidikan' => $master_pendidikan,
            'master_pengalaman_kerja' => $master_pengalaman_kerja,
            'master_provinsi' => $master_provinsi,
            'master_hub_ahli_waris' => $master_hub_ahli_waris,
            'master_kode_operator' => $master_kode_operator,
            'master_jabatan' => $master_jabatan,
        ])/*->with('investor', $investor)*/;
    }

    public function admin_requestwithdraw()
    {
        // $requestwithdraw = PenarikanDana::where('accepted', 0)->get(); 

        return view('pages.admin.investor_withdraw')/*->with('requestwithdraw', $requestwithdraw)*/;
    }

    public function get_req_penarikan_dana_datatables()
    {
        $req = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
            ->leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'penarikan_dana.investor_id')
            ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'penarikan_dana.investor_id')
            ->leftJoin('m_alasan_penarikan', 'm_alasan_penarikan.id', '=', 'penarikan_dana.alasan_penarikan')
            // ->leftJoin('e_coll_bni','e_coll_bni.no_va','=','rekening_investor.va_number')
            ->leftJoin(DB::raw("(select * from e_coll_bni group by no_va) data_e_coll"), 'data_e_coll.no_va', '=', 'rekening_investor.va_number')
            ->where('penarikan_dana.accepted', 0)
            ->orderBy('penarikan_dana.id', 'desc')
            ->get([
                'penarikan_dana.*',
                'investor.username',
                'investor.email',
                'detil_investor.nama_investor',
                'detil_investor.nama_pemilik_rek',
                'detil_investor.pemilik_rekening_asli',
                'detil_investor.phone_investor',
                'detil_investor.telpon_perusahaan',
                'detil_investor.nama_perusahaan',
                'detil_investor.tipe_pengguna',
                'rekening_investor.va_number',
                'data_e_coll.nama',
                'm_alasan_penarikan.alasan_penarikan as penarikan_alasan'
            ]);

        $response = ['data_req' => $req];

        return response()->json($response);
    }

    public function get_paid_penarikan_dana_datatables(Request $request)
    {

        $limit = $request->input('length');
        $start = $request->input('start');
        $search = $request->input('search.value');

        $countPaid = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
            ->where('penarikan_dana.accepted', 1)
            ->orderBy('penarikan_dana.id', 'desc')
            ->count();

        if (empty($search)) {
            $paid = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
                ->leftJoin('m_alasan_penarikan', 'm_alasan_penarikan.id', '=', 'penarikan_dana.alasan_penarikan')
                ->where('penarikan_dana.accepted', 1)
                ->orderBy('penarikan_dana.id', 'desc')
                ->offset($start)
                ->limit($limit)
                ->get([
                    'penarikan_dana.*',
                    'investor.username',
                    'investor.email',
                    'm_alasan_penarikan.alasan_penarikan as penarikan_alasan',
                ]);

            $totalFiltered = $countPaid;
        } else {

            $paid = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
                ->leftJoin('m_alasan_penarikan', 'm_alasan_penarikan.id', '=', 'penarikan_dana.alasan_penarikan')
                ->where('penarikan_dana.accepted', 1)
                ->where('investor.username', 'LIKE', "%{$search}%")
                ->orWhere('investor.email', 'LIKE', "%{$search}%")
                ->orderBy('penarikan_dana.id', 'desc')
                ->offset($start)
                ->limit($limit)
                ->get([
                    'penarikan_dana.*',
                    'investor.username',
                    'investor.email',
                    'm_alasan_penarikan.alasan_penarikan as penarikan_alasan',
                ]);

            $totalFiltered = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
                ->where('penarikan_dana.accepted', 1)
                ->where('investor.username', 'LIKE', "%{$search}%")
                ->orWhere('investor.email', 'LIKE', "%{$search}%")
                ->orderBy('penarikan_dana.id', 'desc')
                ->count();
        }

        $array_paid = array();
        for ($i = 0; $i < count($paid); $i++) {
            $array_paid[$i]['jumlah'] = number_format($paid[$i]['jumlah'], 0, '', '.');
            $array_paid[$i]['no_rekening'] = $paid[$i]['no_rekening'];
            $array_paid[$i]['bank'] = $paid[$i]['bank'];
            $array_paid[$i]['updated_at'] = (string)$paid[$i]['updated_at'];
            $array_paid[$i]['perihal'] = $paid[$i]['perihal'];
            $array_paid[$i]['username'] = $paid[$i]['username'];
            $array_paid[$i]['email'] = $paid[$i]['email'];
            $array_paid[$i]['alasan_penarikan'] = $paid[$i]['alasan_penarikan'];
            $array_paid[$i]['penarikan_alasan'] = $paid[$i]['penarikan_alasan'];
            $array_paid[$i]['note_alasan_penarikan'] = $paid[$i]['note_alasan_penarikan'];
        }

        $response = ['data_paid' => $array_paid, 'recordsTotal' => intval($countPaid), 'recordsFiltered' => intval($totalFiltered)];

        return response()->json($response);
    }

    public function get_fail_penarikan_dana_datatables(Request $request)
    {
        $limit = $request->input('length');
        $start = $request->input('start');
        $search = $request->input('search.value');

        $countFail = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
            ->where('penarikan_dana.accepted', 2)
            ->orderBy('penarikan_dana.id', 'desc')
            ->count();

        if (empty($search)) {
            $fail = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
                ->leftJoin('m_alasan_penarikan', 'm_alasan_penarikan.id', '=', 'penarikan_dana.alasan_penarikan')
                ->where('penarikan_dana.accepted', 2)
                ->orderBy('penarikan_dana.id', 'desc')
                ->offset($start)
                ->limit($limit)
                ->get([
                    'penarikan_dana.*',
                    'investor.username',
                    'investor.email',
                    'm_alasan_penarikan.alasan_penarikan as penarikan_alasan',
                ]);

            $totalFiltered = $countFail;
        } else {
            $fail = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
                ->leftJoin('m_alasan_penarikan', 'm_alasan_penarikan.id', '=', 'penarikan_dana.alasan_penarikan')
                ->where('penarikan_dana.accepted', 2)
                ->where('investor.username', 'LIKE', "%{$search}%")
                ->orWhere('investor.email', 'LIKE', "%{$search}%")
                ->orderBy('penarikan_dana.id', 'desc')
                ->offset($start)
                ->limit($limit)
                ->get([
                    'penarikan_dana.*',
                    'investor.username',
                    'investor.email',
                    'm_alasan_penarikan.alasan_penarikan as penarikan_alasan',
                ]);

            $totalFiltered = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
                ->where('penarikan_dana.accepted', 2)
                ->where('investor.username', 'LIKE', "%{$search}%")
                ->orWhere('investor.email', 'LIKE', "%{$search}%")
                ->orderBy('penarikan_dana.id', 'desc')
                ->count();
        }

        $array_fail = array();
        for ($i = 0; $i < count($fail); $i++) {
            $array_fail[$i]['jumlah'] = number_format($fail[$i]['jumlah'], 0, '', '.');
            $array_fail[$i]['no_rekening'] = $fail[$i]['no_rekening'];
            $array_fail[$i]['bank'] = $fail[$i]['bank'];
            $array_fail[$i]['updated_at'] = (string)$fail[$i]['updated_at'];
            $array_fail[$i]['perihal'] = $fail[$i]['perihal'];
            $array_fail[$i]['username'] = $fail[$i]['username'];
            $array_fail[$i]['email'] = $fail[$i]['email'];
            $array_fail[$i]['alasan_penarikan'] = $fail[$i]['alasan_penarikan'];
            $array_fail[$i]['penarikan_alasan'] = $fail[$i]['penarikan_alasan'];
            $array_fail[$i]['note_alasan_penarikan'] = $fail[$i]['note_alasan_penarikan'];
            $array_fail[$i]['alasan_penolakan'] = $fail[$i]['alasan_penolakan'];
        }
        // var_dump($array_fail);die;
        $response = ['data_fail' => $array_fail, 'recordsTotal' => intval($countFail), 'recordsFiltered' => intval($totalFiltered)];

        return response()->json($response);
    }

    public function admin_paidwithdraw()
    {
        // $requestwithdraw = PenarikanDana::where('accepted', 1)->get(); 

        return view('pages.admin.investor_withdraw_paid');
    }

    public function admin_failedwithdraw()
    {
        // $requestwithdraw = PenarikanDana::where('accepted', 2)->get();

        return view('pages.admin.investor_withdraw_failed');
    }

    public function admin_exportPenarikan()
    {
        return view('pages.admin.investor_withdraw_export');
    }

    public function generate_export_penarikan(Request $request)
    {
        $tgl_m = $request->tgl_mulai;
        $tgl_s = $request->tgl_selesai;
        $data_arr = array(
            'tgl_m' => $tgl_m,
            'tgl_s' => $tgl_s,
        );
        // dd($data_arr);
        $date = Carbon::now()->toDateString();
        if ($tgl_m > $tgl_s) {
            return redirect()->back()->with('pesaninput', 'Tanggal mulai penarikan tidak boleh lebih dari tanggal selesai penarikan');
        } elseif ($tgl_m != null && $tgl_s != null) {
            return Excel::download(new PenarikanExport($data_arr), 'Penarikan-Tanggal-' . $date . '.xlsx');
        } else {
            return redirect()->back()->with('pesaninput', 'Lengkapi data tanggal terlebih dahulu');
        }
    }

    public function admin_verif_ok(Request $request)
    {
        $user = Investor::where('username', $request->username)->first();
        $hasil = $this->generateVA($request->username);
        if (!$hasil) return redirect()->back()->with('verif_failed', 'Pembuatan VA gagal');

        Investor::where('username', $request->username)->update(['status' => 'active']);
        dispatch(new InvestorVerif($user, 1));
        #pesan verifikasi
        // $kirimverifikasi = $this->verificationCode($user);
        #end pesan verifikasi
        // if($kirimverifikasi) return redirect()->back()->with('verif_ok', 'Verifikasi telah berhasil');
        return redirect()->back()->with('verif_ok', 'Verifikasi telah berhasil');
    }

    public function admin_verif_fail(Request $request)
    {
        $investor = Investor::where('username', $request->username)->first();
        // DetilInvestor::where('investor_id', $investor->id)
        //                 ->delete();

        Investor::where('username', $request->username)
            ->update(['status' => 'reject']);

        $user = Investor::where('username', $request->username)->first();
        dispatch(new InvestorVerif($user, 0));

        return redirect()->back()->with('verif_failed', 'Verifikasi telah ditolak');
    }

    public function admin_withdraw_ok(Request $request)
    {
        $alasan_penolakan = $request->textpenolakan;
        $desc = "Permohonan penarikan dana ditolak";
        $index1 = 'withdraw_fail';
        $index2 = 'Anda telah menolak pembayaran '; 

        if($request->status == 1){
            $alasan_penolakan = NULL;

            $rekening = RekeningInvestor::where('investor_id', $request->investor_id)->first();
            $rekening->total_dana -= $request->nominal;
            $rekening->unallocated -= $request->nominal;
            $rekening->save();

            event(new MutasiInvestorEvent($request->investor_id, 'DEBIT', $request->nominal, 'Penarikan dana selesai'));

            $desc = "Konfirmasi permohonan penarikan dana";
            $index1 = 'withdraw_ok';
            $index2 = 'Anda telah melakukan konfirmasi pembayaran ';
        }

            PenarikanDana::where('id', $request->id)
            ->update(['accepted' => $request->status,'alasan_penolakan' => $alasan_penolakan]);

            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->menu = "Penarikan Dana";
            $audit->fullname = $username;
            $audit->description = $desc;
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return redirect()->route('admin.investor.requestwithdraw')->with($index1, $index2);

    }

    //Generate VA for user
    public function generateVA($username)
    {

        $date = \Carbon\Carbon::now()->addYear(4);
        $user = Investor::where('username', $username)->first();
        $data = [
            'type' => 'createbilling',
            'client_id' => self::CLIENT_ID,
            'trx_id' => $user->id,
            'trx_amount' => '0',
            'customer_name' => $user->detilInvestor->nama_investor,
            'customer_email' => $user->email,
            'virtual_account' => '8' . self::CLIENT_ID . $user->detilInvestor->getVa(),
            'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
            'billing_type' => 'o',
        ];


        $encrypted = BniEnc::encrypt($data, self::CLIENT_ID, self::KEY);

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post(self::API_URL, [
            'json' => [
                'client_id' => self::CLIENT_ID,
                'data' => $encrypted,
            ]
        ]);

        $result = json_decode($result->getBody()->getContents());
        if ($result->status !== '000') {
            return False;
        } else {
            $decrypted = BniEnc::decrypt($result->data, self::CLIENT_ID, self::KEY);
            //return json_encode($decrypted);
            $user->RekeningInvestor()->create([
                'investor_id' => $user->id,
                'total_dana' => 0,
                'va_number' => $decrypted['virtual_account'],
                'unallocated' => 0,
            ]);

            return True;
            // return view('pages.user.add_funds')->with('message','VA Generate Success!');
        }
    }

    public function generateVA_new($username)
    {

        $now = Carbon::now();
        echo $now->addHours(2);

        // $user = Investor::where('username', $username)->first();
        // $data = [
        // 'type' => 'createbilling',
        // 'client_id' => self::CLIENT_ID,
        // 'trx_id' => $user->id,
        // 'trx_amount' => '0',
        // 'customer_name' => $user->detilInvestor->nama_investor,
        // 'customer_email' => $user->email,
        // 'virtual_account' => '8'.self::CLIENT_ID.$user->detilInvestor->getVa(),
        // 'datetime_expired' => $date->format('Y-m-d').'T'.$date->format('H:i:sP'),
        // 'billing_type' => 'o',
        // ];


        // $encrypted = BniEnc::encrypt($data,self::CLIENT_ID,self::KEY);

        // $client = new Client(); //GuzzleHttp\Client
        // $result = $client->post(self::API_URL, [
        // 'json' => [
        // 'client_id' => self::CLIENT_ID,
        // 'data' => $encrypted,
        // ]
        // ]);

        // $result = json_decode($result->getBody()->getContents());
        // if($result->status !== '000'){
        // return False;
        // }
        // else{
        // $decrypted = BniEnc::decrypt($result->data,self::CLIENT_ID,self::KEY);
        // //return json_encode($decrypted);
        // $user->RekeningInvestor()->create([
        // 'investor_id' => $user->id,
        // 'total_dana' => 0,
        // 'va_number' => $decrypted['virtual_account'],
        // 'unallocated' => 0,
        // ]);

        // return True;
        // // return view('pages.user.add_funds')->with('message','VA Generate Success!');
        // }
    }

    public function admin_create_investor(Request $request)
    {

        $messages = [
            'error_upload'    => 'Tipe file gambar harus jpeg,jpg,bmp,png dan ukuran file gambar max 500 KB',
        ];

        $validator = Validator::make($request->all(), [
            'pic_investor' => 'mimes:jpeg,jpg,bmp,png|max:500',
            'pic_ktp_investor' => 'mimes:jpeg,jpg,bmp,png|max:500',
            'pic_user_ktp_investor' => 'mimes:jpeg,jpg,bmp,png|max:500',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($messages);
            // ->withInput();
        } else {
            if (Investor::where('email', $request->email)->exists() || Investor::where('username', $request->username)->exists()) {
                return redirect()->back()->with('exist', "Username or Email already exist");
            }
            $user = Investor::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'email_verif' => null,
                'status' => 'active',
            ]);

            $detil = new DetilInvestor;
            if ($request->tipe_pengguna == 1) {
                $detil->investor_id = $user->id;
                $detil->tipe_pengguna = $request->tipe_pengguna;
                $detil->nama_investor = $request->nama;
                $detil->no_ktp_investor = $request->no_ktp;
                $detil->no_npwp_investor = $request->no_npwp;
                $detil->phone_investor = $request->no_telp;
                $detil->alamat_investor = $request->alamat;
                $detil->provinsi_investor = $request->provinsi;
                $detil->kota_investor = $request->kota;
                $detil->kode_pos_investor = $request->kode_pos;
                $detil->tempat_lahir_investor = $request->tempat_lahir;
                $detil->tgl_lahir_investor = $request->tgl_lahir . '-' . $request->bln_lahir . '-' . $request->thn_lahir;
                $detil->jenis_kelamin_investor = $request->jenis_kelamin;
                $detil->status_kawin_investor = $request->kawin;
                $detil->status_rumah_investor = $request->pemilik_rumah;
                $detil->agama_investor = $request->agama;
                $detil->pekerjaan_investor = $request->pekerjaan;
                $detil->bidang_pekerjaan = $request->bidang_pekerjaan;
                $detil->online_investor = $request->pekerjaan_online;
                $detil->pendapatan_investor = $request->pendapatan;
                $detil->asset_investor = null;
                $detil->pengalaman_investor = $request->pengalaman;
                $detil->pendidikan_investor = $request->pendidikan;
                $detil->bank_investor = $request->bank;
                $detil->rekening = $request->rekening;
                // $detil->job_investor = $request->job_investor;     
                $detil->pic_investor = $this->upload('pic_investor', $request, $user->id);
                $detil->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $user->id);
                $detil->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $user->id);
                $detil->jenis_badan_hukum = null;
                $detil->nama_perwakilan = null;
                $detil->no_ktp_perwakilan = null;

                if ($request->kawin == 1) {
                    $detil->pasangan_investor = $request->nama_pasangan;
                    $detil->pasangan_email = $request->email_pasangan;
                    $detil->pasangan_tempat_lhr = $request->tempat_lahir_pasangan;
                    $detil->pasangan_tgl_lhr = $request->tgl_lahir_pasangan . '-' . $request->bln_lahir_pasangan . '-' . $request->thn_lahir_pasangan;
                    $detil->pasangan_jenis_kelamin = $request->jenis_kelamin_pasangan;
                    $detil->pasangan_ktp = $request->no_ktp_pasangan;
                    $detil->pasangan_npwp = $request->no_npwp_pasangan;
                    $detil->pasangan_phone = $request->no_telp_pasangan;
                    $detil->pasangan_alamat = $request->alamat_pasangan;
                    $detil->pasangan_provinsi = $request->provinsi_pasangan;
                    $detil->pasangan_kota = $request->kota_pasangan;
                    $detil->pasangan_kode_pos = $request->kode_pos_pasangan;
                    $detil->pasangan_agama = $request->agama_pasangan;
                    $detil->pasangan_pekerjaan = $request->pekerjaan_pasangan;
                    $detil->pasangan_bidang_pekerjaan = $request->bidang_pekerjaan_pasangan;
                    $detil->pasangan_online = $request->pekerjaan_online_pasangan;
                    $detil->pasangan_pendapatan = $request->pendapatan_pasangan;
                    $detil->pasangan_pengalaman = $request->pengalaman_pasangan;
                    $detil->pasangan_pendidikan = $request->pendidikan_pasangan;
                } else {
                    $detil->pasangan_investor = null;
                    $detil->pasangan_email = null;
                    $detil->pasangan_tempat_lhr = null;
                    $detil->pasangan_tgl_lhr = null;
                    $detil->pasangan_jenis_kelamin = null;
                    $detil->pasangan_ktp = null;
                    $detil->pasangan_npwp = null;
                    $detil->pasangan_phone = null;
                    $detil->pasangan_alamat = null;
                    $detil->pasangan_provinsi = null;
                    $detil->pasangan_kota = null;
                    $detil->pasangan_kode_pos = null;
                    $detil->pasangan_agama = null;
                    $detil->pasangan_pekerjaan = null;
                    $detil->pasangan_bidang_pekerjaan = null;
                    $detil->pasangan_online = null;
                    $detil->pasangan_pendapatan = null;
                    $detil->pasangan_pengalaman = null;
                    $detil->pasangan_pendidikan = null;
                }
                // $detil->bank = $request->bank;
                $detil->save();
                $hasil = $this->generateVA($request->username);
            } else {
                $detil->investor_id = $user->id;
                $detil->tipe_pengguna = $request->tipe_pengguna;
                $detil->nama_investor = $request->nama;
                $detil->no_ktp_investor = null;
                $detil->no_npwp_investor = $request->no_npwp;
                $detil->phone_investor = $request->no_telp;
                $detil->alamat_investor = $request->alamat;
                $detil->provinsi_investor = $request->provinsi;
                $detil->kota_investor = $request->kota;
                $detil->kode_pos_investor = $request->kode_pos;
                $detil->tempat_lahir_investor = null;
                $detil->tgl_lahir_investor = null;
                $detil->jenis_kelamin_investor = null;
                $detil->status_kawin_investor = null;
                $detil->status_rumah_investor = null;
                $detil->agama_investor = null;
                $detil->pekerjaan_investor = null;
                $detil->bidang_pekerjaan = $request->bidang_pekerjaan;
                $detil->online_investor = $request->pekerjaan_online;
                $detil->pendapatan_investor = $request->pendapatan;
                $detil->asset_investor = $request->asset;
                $detil->pengalaman_investor = null;
                $detil->pendidikan_investor = null;
                $detil->bank_investor = $request->bank;
                $detil->rekening = $request->rekening;

                $detil->pasangan_investor = null;
                $detil->pasangan_email = null;
                $detil->pasangan_tempat_lhr = null;
                $detil->pasangan_tgl_lhr = null;
                $detil->pasangan_jenis_kelamin = null;
                $detil->pasangan_ktp = null;
                $detil->pasangan_npwp = null;
                $detil->pasangan_phone = null;
                $detil->pasangan_alamat = null;
                $detil->pasangan_provinsi = null;
                $detil->pasangan_kota = null;
                $detil->pasangan_kode_pos = null;
                $detil->pasangan_agama = null;
                $detil->pasangan_pekerjaan = null;
                $detil->pasangan_bidang_pekerjaan = null;
                $detil->pasangan_online = null;
                $detil->pasangan_pendapatan = null;
                $detil->pasangan_pengalaman = null;
                $detil->pasangan_pendidikan = null;
                // $detil->job_investor = $request->job_investor;     
                $detil->pic_investor = $this->upload('pic_investor', $request, $user->id);
                $detil->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $user->id);
                $detil->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $user->id);
                $detil->jenis_badan_hukum = $request->jenis_badan_hukum;
                $detil->nama_perwakilan = $request->nama_perwakilan;
                $detil->no_ktp_perwakilan = $request->no_ktp_perwakilan;

                $detil->save();
                $hasil = $this->generateVA($request->username);
            }

            if (!$hasil) {
                Investor::where('id', $detil->investor_id)->update(['status' => 'pending']);
                return redirect()->back()->with('error', 'Pembuatan VA gagal');
            }
            return redirect()->back()->with('success', 'Create Success');
        }
    }

    public function admin_update_investor(Request $request, DetilInvestor $detil_investor)
    {   
        $edit_by                = Auth::user()->firstname . ' ' . Auth::user()->lastname;
        $tipe_pengguna          = $request->tipe_pengguna;
        $investor_id            = $request->investor_id;
        $old_username           = $request->old_username;
        $username               = $request->username;
        $email                  = $request->email;
        $acc_status             = $request->status ? $request->status : $request->status_saat_ini;
        $keterangan             =  $acc_status == 'suspend' ?  $request->alasan_suspend : $request->alasan_active;
        $va_number              = $request->va_number;
        $va_cimbs               = $request->va_cimbs;
        $va_bsi                 = $request->va_bsi;
        $va_number_bdn_hukum    = $request->va_number_bdn_hukum;
        $va_cimbs_bdn_hukum     = $request->va_cimbs_bdn_hukum;
        $va_bsi_bdn_hukum       = $request->va_bsi_bdn_hukum;
        $investor_path          = 'user/' . $investor_id . '/'.'temp';
        
        $is_req_exist   = DB::table('admin_permintaan_perubahan')->where('status', 1)->where('client_id', $investor_id)->exists();
        if ($is_req_exist) {
            return redirect()->back()->with('error', "Permintaan perubahan data tidak bisa dilanjutkan karena masih dalam proses antrian pengecekan");
        } else {
            switch ($tipe_pengguna) {
                case '1':
        
                    $ktp_investor               = $request->jenis_identitas == 1 ? $request->no_ktp : null;
                    $no_passpor_investor        = $request->jenis_identitas != 1 ? $request->no_passpor : null;
                    $no_hp_investor             = $request->kode_operator . $request->no_telp;
                    $no_hp_ahli_waris           = $request->kode_operator_ahli_waris . $request->no_hp_ahli_waris;
                    $pic_investor_val           = $request->pic_investor_val;
                    $pic_ktp_investor_val       = $request->pic_ktp_investor_val;
                    $pic_user_ktp_investor_val  = $request->pic_user_ktp_investor_val;

                    $tanggal_lahir_investor = '';
                    $new_format_lahir       = '';
                    if ($request->tanggal_lahir) {
                        $tanggal_lahir_investor = explode('-', $request->tanggal_lahir);
                        $new_format_lahir       = $tanggal_lahir_investor[0].'-'.$tanggal_lahir_investor[1].'-'.$tanggal_lahir_investor[2];
                    }

                    if ($request->hasFile('pic_investor')) {

                        $file        = $request->file('pic_investor');
                        $file_name   = 'pic_investor' . '.' . $file->getClientOriginalExtension();

                        $upload_file = Helper::uploadFile($investor_path, $file, $file_name);

                        if ($upload_file) {
                            $pic_investor_val = $investor_path. '/' .$file_name;
                        }
                    }

                    if ($request->hasFile('pic_ktp_investor')) {

                        $file        = $request->file('pic_ktp_investor');
                        $file_name    = 'pic_ktp_investor' . '.' . $file->getClientOriginalExtension();

                        $upload_file = Helper::uploadFile($investor_path, $file, $file_name);

                        if ($upload_file) {
                            $pic_ktp_investor_val = $investor_path. '/' .$file_name;
                        }
                    }

                    if ($request->hasFile('pic_user_ktp_investor')) {

                        $file        = $request->file('pic_user_ktp_investor');
                        $file_name    = 'pic_user_ktp_investor' . '.' . $file->getClientOriginalExtension();

                        $upload_file = Helper::uploadFile($investor_path, $file, $file_name);

                        if ($upload_file) {
                            $pic_user_ktp_investor_val = $investor_path. '/' .$file_name;
                        }
                    }

                    # set null is_valid_npwp if npwp change
                    if(isset($request->no_npwp)){
                        if($request->npwp_exist != $request->no_npwp){
                            $detilinvestor = new DetilInvestor;
                            $detilinvestor->updateIsValidNull($investor_id);
                        }    
                    }

                    $update_profile_data_inv_individu = 
                    DB::select(
                        "CALL proc_req_edit_inv_individu(
                            '".addslashes($investor_id)."',                         /* investor_id */
                            '".addslashes($old_username)."',                        /* ori account_name */
                            '".addslashes($username)."',                            /* account_name */
                            '".addslashes($email)."',                               /* email */
                            '".addslashes($acc_status)."',                          /* status varchar(15) */
                            '".addslashes($keterangan)."',                          /* keterangan varchar(15) */
                            '".addslashes($request->nama)."',                       /* nama_investor */
                            '".addslashes($request->jenis_identitas)."',            /* jenis_identitas */
                            '".addslashes($ktp_investor)."',                        /* no_ktp_investor */
                            '".addslashes($no_passpor_investor)."',                 /* no_passpor_investor */
                            '".addslashes($request->no_npwp)."',                    /* no_npwp_investor */
                            '".addslashes($request->tempat_lahir)."',               /* tempat_lahir_investor */
                            '".addslashes($request->tanggal_lahir)."',              /* tgl_lahir_investor */
                            '".addslashes($request->jenis_kelamin)."',              /* jenis_kelamin_investor */
                            '".addslashes($request->status_kawin)."',               /* status_kawin_investor */
                            '".addslashes($request->alamat)."',                     /* alamat_investor  */
                            '".addslashes($request->provinsi)."',                   /* provinsi_investor */
                            '".addslashes($request->kota)."',                       /* kota_investor */
                            '".addslashes($request->kelurahan)."',                  /* kelurahan */
                            '".addslashes($request->kecamatan)."',                  /* kecamatan */	
                            '".addslashes($request->kode_pos)."',                   /* kode_pos_investor */ 
                            '".addslashes($request->kode_operator)."',              /* kode_operator */	
                            '".addslashes($no_hp_investor)."',                      /* phone_investor */	
                            '".addslashes($request->agama)."',                      /* agama_investor */	
                            '".addslashes($request->pekerjaan)."',                  /* pekerjaan_investor */
                            '".addslashes($request->bidang_pekerjaan)."',           /* bidang_pekerjaan */
                            '".addslashes($request->bidang_online)."',              /* online_investor	*/
                            '".addslashes($request->pendapatan)."',                 /* pendapatan_investor */
                            '".addslashes($request->pengalaman_kerja)."',           /* pengalaman_investor */
                            '".addslashes($request->pendidikan)."',                 /* pendidikan_investor */
                            '".addslashes($request->bank)."',                       /* bank_investor */	
                            '".addslashes($request->domisili_negara)."',            /* domisili_negara	*/
                            '".addslashes($request->sumber_dana)."',                /* sumber_dana	*/
                            '".addslashes($pic_investor_val)."',                    /* pic_investor */
                            '".addslashes($pic_ktp_investor_val)."',                /* pic_ktp_investor */
                            '".addslashes($pic_user_ktp_investor_val)."',           /* pic_user_ktp_investor */
                            '".addslashes($request->rekening)."',                   /* rekening */
                            '".addslashes($request->nama_pemilik_rek)."',           /* nama_pemilik_rek */
                            '".addslashes($request->nama_ibu_kandung)."',           /* nama_ibu_kandung */
                            '".addslashes($request->warga_negara)."',               /* warganegara */

                            /* Ahli Waris */
                            '".addslashes($request->nama_ahli_waris)."',            /* nama_ahli_waris */
                            '".addslashes($request->hub_ahli_waris)."',             /* hubungan_keluarga_ahli_waris */
                            '".addslashes($request->nik_ahli_waris)."',             /* nik_ahli_waris	*/
                            '".addslashes($request->kode_operator_ahli_waris)."',   /* kode_operator_ahli_waris */		
                            '".addslashes($no_hp_ahli_waris)."',                    /* no_hp_ahli_waris */
                            '".addslashes($request->alamat_ahli_waris)."',          /* alamat_ahli_waris */
                            '".addslashes($edit_by)."',                             /* edit_by*/
                            '".addslashes($va_number)."',                           /* va_bni */
                            '".addslashes($va_cimbs)."',                            /* va_cimb */
                            '".addslashes($va_bsi)."',                              /* va_bsi */
                            '".addslashes($request->kode_ref)."',                   /* ref_number varchar(191) */
                            'InvestorController',                                   /* file_ */
                            1397                                                    /* line_ */
                        )"
                    );
                    break;

                case '2':
                    $omset_tahun_terakhir       = str_replace(",", "", $request->omset_tahun_terakhir);
                    $tot_aset_tahun_terakhr     = str_replace(",", "", $request->tot_aset_tahun_terakhr);
                    $foto_npwp_badan_hukum_val  = $request->foto_npwp_badan_hukum_val;
                    $no_tlp_badan_hukum         = $request->no_tlp_badan_hukum ? '62'.$request->no_tlp_badan_hukum : '';

                    if ($request->hasFile('pic_npwp_bdn_hukum')) {

                        $file        = $request->file('pic_npwp_bdn_hukum');
                        $file_name    = 'npwp_badan_hukum' . '.' . $file->getClientOriginalExtension();

                        $upload_file = Helper::uploadFile($investor_path, $file, $file_name);

                        if ($upload_file) {
                            $foto_npwp_badan_hukum_val = $investor_path. '/' .$file_name;
                        }
                    }

                    $update_provile_data_inv_bdn_hukum = 
                    DB::select(
                        "CALL proc_req_edit_inv_badan_hukum(
                            '".addslashes($investor_id)."',                             /* investor_id int */
                            '".addslashes($old_username)."',                            /* ori account_name varchar(35) */
                            '".addslashes($username)."',                                /* account_name varchar(35) */
                            '".addslashes($email)."',                                   /* email varchar(35) */
                            '".addslashes($acc_status)."',                              /* status varchar(15) */
                            '".addslashes($keterangan)."',                              /* status varchar(15) */
                            '".addslashes($request->nama_badan_hukum)."',               /* nama_perusahaan	varchar(50) */
                            '".addslashes($request->nib_badan_hukum)."',                /* nib	varchar(25) */
                            '".addslashes($request->npwp_bdn_hukum)."',                 /* npwp_perusahaan	varchar(15) */
                            null,                                                       /* nomor_akta_perusahaan varchar(25) */
                            null,                                                       /* tanggal_akta_perusahaan	DATE */
                            '".addslashes($request->no_akta_pendirian_bdn_hukum)."',    /* no_akta_pendirian varchar(25) */
                            '".addslashes($request->tgl_berdiri_badan_hukum)."',        /* tgl_berdiri DATE */
                            '".addslashes($request->no_akta_perubahan_bdn_hukum)."',    /* nomor_akta_perubahan	varchar(25) */
                            '".addslashes($request->tgl_akta_perubahan_bdn_hukum)."',   /* tanggal_akta_perubahan	DATE */
                            '".addslashes($no_tlp_badan_hukum)."',                      /* telpon_perusahaan	varchar(20) */
                            '".addslashes($foto_npwp_badan_hukum_val)."',               /* foto_npwp_perusahaan	varchar(50) */
                            '".addslashes($request->alamat_bdn_hukum)."',               /* alamat_perusahaan	varchar(100) */
                            '".addslashes($request->provinsi_bdn_hukum)."',             /* provinsi_perusahaan	int */
                            '".addslashes($request->kota_bdn_hukum)."',                 /* kota_perusahaan	varchar(5) */
                            '".addslashes($request->kecamatan_bdn_hukum)."',            /* kecamatan_perusahaan	varchar(50) */
                            '".addslashes($request->kelurahan_bdn_hukum)."',            /* kelurahan_perusahaan	varchar(50) */
                            '".addslashes($request->kode_pos_bdn_hukum)."',             /* kode_pos_perusahaan	varchar(5) */
                            '".addslashes($request->rekening_bdn_hukum)."',             /* rekening	varchar(20) */
                            '".addslashes($request->nama_pemilik_rek_bdn_hukum)."',     /* nama_pemilik_rek	varchar(35) */
                            '".addslashes($request->bank_bdn_hukum)."',                 /* bank_investor	varchar(5) */
                            '".addslashes($request->bidang_pekerjaan_bdn_hukum)."',     /* bidang_pekerjaan	varchar(5) */
                            '".addslashes($omset_tahun_terakhir)."',                    /* omset_tahun_terakhir	decimal(15, 2) */
                            '".addslashes($tot_aset_tahun_terakhr)."',                  /* tot_aset_tahun_terakhr	decimal(15, 2) */
                            '".addslashes($request->btn_laporan_keuangan_val)."',       /* laporan_keuangan	varchar(50) */
                            '".addslashes($edit_by)."',                                 /* edit_by			varchar(35) */
                            '".addslashes($va_number_bdn_hukum)."',                     /* va_bni varchar(25) */
                            '".addslashes($va_cimbs_bdn_hukum)."',                      /* va_cimb varchar(25) */
                            '".addslashes($va_bsi_bdn_hukum)."',                        /* va_bsi varchar(25) */
                            '".addslashes($request->kode_ref)."',                       /* ref_number varchar(191) */
                            'InvestorController',                                       /* file varchar(100) */
                            1470                                                        /* line int */
                        )"
                    );

                    if (!empty($update_provile_data_inv_bdn_hukum[0]->sout)) {
                        
                        $total = $request->nama_pengurus ? count($request->nama_pengurus) : 0;
                        for ($i = 0; $i < $total; $i++) {
                            $pengurus_id                    = !empty($request->pengurus_id[$i]) ? $request->pengurus_id[$i] : '';
                            $hist_id                        = $update_provile_data_inv_bdn_hukum[0]->sout;
                            $nama_pengurus                  = !empty($request->nama_pengurus[$i]) ? $request->nama_pengurus[$i] : '';
                            $jns_kelamin_pengurus           = !empty($request->jns_kelamin_pengurus[$i]) ? $request->jns_kelamin_pengurus[$i] : '';
                            $ktp_pengurus                   = !empty($request->ktp_pengurus[$i]) ? $request->ktp_pengurus[$i] : '';
                            $tempat_lahir_pengurus          = !empty($request->tempat_lahir_pengurus[$i]) ? $request->tempat_lahir_pengurus[$i] : '';
                            $tempat_lahir_pengurus          = !empty($request->tempat_lahir_pengurus[$i]) ? $request->tempat_lahir_pengurus[$i] : '';
                            $tanggal_lahir_pengurus         = !empty($request->tanggal_lahir_pengurus[$i]) ? $request->tanggal_lahir_pengurus[$i] : '';
                            $kode_operator_pengurus         = !empty($request->kode_operator_pengurus[$i]) ? $request->kode_operator_pengurus[$i] : '';
                            $no_telp_pengurus               = !empty($request->no_telp_pengurus[$i]) ? $kode_operator_pengurus . $request->no_telp_pengurus[$i] : '';
                            $agama_pengurus                 = !empty($request->agama_pengurus[$i]) ? $request->agama_pengurus[$i] : '';
                            $pendidikan_terakhir_pengurus   = !empty($request->pendidikan_terakhir_pengurus[$i]) ? $request->pendidikan_terakhir_pengurus[$i] : '';
                            $npwp_pengurus                  = !empty($request->npwp_pengurus[$i]) ? $request->npwp_pengurus[$i] : '';
                            $jabatan_pengurus               = !empty($request->jabatan_pengurus[$i]) ? $request->jabatan_pengurus[$i] : '';
                            $alamat_pengurus                = !empty($request->alamat_pengurus[$i]) ? $request->alamat_pengurus[$i] : '';
                            $provinsi_pengurus              = !empty($request->provinsi_pengurus[$i]) ? $request->provinsi_pengurus[$i] : '';
                            $kota_pengurus                  = !empty($request->kota_pengurus[$i]) ? $request->kota_pengurus[$i] : '';
                            $kecamatan_pengurus             = !empty($request->kecamatan_pengurus[$i]) ? $request->kecamatan_pengurus[$i] : '';
                            $kelurahan_pengurus             = !empty($request->kelurahan_pengurus[$i]) ? $request->kelurahan_pengurus[$i] : '';
                            $kode_pos_pengurus              = !empty($request->kode_pos_pengurus[$i]) ? $request->kode_pos_pengurus[$i] : '';

                            $update_profile_data_inve_bdn_hukum_pengrus = 
                            DB::select(
                                "CALL proc_req_edit_inv_bh_pengurus(
                                    '".addslashes($pengurus_id)."',                     /* pengurus_id int */
                                    '".addslashes($hist_id)."',                         /* hist_id int */
                                    '".addslashes($investor_id)."',                     /* investor_id int */
                                    '".addslashes($nama_pengurus)."',                   /* nama_pengurus	varchar(35) */
                                    '".addslashes($jns_kelamin_pengurus)."',            /* jenis_kelamin	tinyint */
                                    '".addslashes($ktp_pengurus)."',                    /* nomor_ktp	varchar(16) */
                                    '".addslashes($tempat_lahir_pengurus)."',           /* tempat_lahir	varchar(50) */
                                    '".addslashes($tanggal_lahir_pengurus)."',          /* tgl_lahir	date */
                                    '".addslashes($kode_operator_pengurus)."',          /* kode_negara	varchar(5) */
                                    '".addslashes($no_telp_pengurus)."',                /* no_hp	varchar(20) */
                                    '".addslashes($agama_pengurus)."',                  /* agama	tinyint */
                                    '".addslashes($pendidikan_terakhir_pengurus)."',    /* pendidikan_terakhir	tinyint */
                                    '".addslashes($npwp_pengurus)."',                   /* npwp	varchar(15) */
                                    '".addslashes($jabatan_pengurus)."',                /* jabatan	varchar(25) */
                                    '".addslashes($alamat_pengurus)."',                 /* alamat	varchar(100) */
                                    '".addslashes($provinsi_pengurus)."',               /* provinsi	tinyint */
                                    '".addslashes($kota_pengurus)."',                   /* kota	varchar(5) */
                                    '".addslashes($kecamatan_pengurus)."',              /* kecamatan	varchar(50) */
                                    '".addslashes($kelurahan_pengurus)."',              /* kelurahan	varchar(50) */
                                    '".addslashes($kode_pos_pengurus)."',               /* kode_pos	varchar(5) */
                                    'InvestorController',                               /* file varchar(100) */
                                    1506                                                /* line int */
                                )"
                            );
                        }
                    }

                    break;
                
                default:
                    # code...
                    break;
            }
        }

        // $response = [
		// 	'status' => 'success',
		// 	'message' => 'perubahan data berhasil dikirim'
		// ];

        // return response()->json($response);
        
        return redirect()->back()->with('updated', "Permintaan perubahan data berhasil dikirim");
        // ======================================================================= //

        if ($request->tipe_proses == 'baru') {
            $investor = Investor::where('id', $request->investor_id)->first();

            if ($investor->username != $request->username) {
                if (Investor::where('username', $request->username)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }
            if ($investor->email != $request->email) {
                if (Investor::where('email', $request->email)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }

            $investor->username = $request->username;
            $investor->email = $request->email;
            $investor->status = 'active';
            $investor->ref_number = $request->kode_ref;

            $investor->save();

            $detil_insert = new DetilInvestor;

            // if ($request->tipe_pengguna == 1)
            // {
            $detil_insert->investor_id = $request->investor_id;
            $detil_insert->tipe_pengguna = null;
            $detil_insert->nama_investor = $request->nama;
            $detil_insert->no_ktp_investor = $request->no_ktp;
            $detil_insert->no_npwp_investor = $request->no_npwp;
            $detil_insert->phone_investor = $request->no_telp;
            $detil_insert->alamat_investor = $request->alamat;
            $detil_insert->provinsi_investor = $request->provinsi;
            $detil_insert->kota_investor = $request->kota;
            $detil_insert->kode_pos_investor = $request->kode_pos;
            $detil_insert->tempat_lahir_investor = $request->tempat_lahir;
            $detil_insert->tgl_lahir_investor = $request->tgl_lahir . '-' . $request->bln_lahir . '-' . $request->thn_lahir;
            $detil_insert->jenis_kelamin_investor = $request->jenis_kelamin;
            $detil_insert->status_kawin_investor = null;
            $detil_insert->status_rumah_investor = null;
            $detil_insert->agama_investor = null;
            $detil_insert->pekerjaan_investor = null;
            $detil_insert->bidang_pekerjaan = null;
            $detil_insert->online_investor = null;
            $detil_insert->pendapatan_investor = null;
            $detil_insert->asset_investor = null;
            $detil_insert->pengalaman_investor = null;
            $detil_insert->pendidikan_investor = null;
            $detil_insert->bank_investor = $request->bank;
            $detil_insert->rekening = $request->rekening;
            $detil_insert->nama_pemilik_rek = $request->nama_pemilik_rek;
            // $detil->job_investor = $request->job_investor;     
            // $detil->pic_investor = $this->upload('foto_diri', $request, $user->id);
            // $detil->pic_ktp_investor = $this->upload('foto_ktp', $request, $user->id);
            // $detil->pic_user_ktp_investor = $this->upload('foto_diri_ktp', $request, $user->id);
            $detil_insert->jenis_badan_hukum = null;
            $detil_insert->nama_perwakilan = null;
            $detil_insert->no_ktp_perwakilan = null;

            // if ($request->kawin == 1)
            // {
            //     $detil->pasangan_investor = $request->nama_pasangan;
            //     $detil->pasangan_email = $request->email_pasangan;
            //     $detil->pasangan_tempat_lhr = $request->tempat_lahir_pasangan;
            //     $detil->pasangan_tgl_lhr = $request->tgl_lahir_pasangan.'-'.$request->bln_lahir_pasangan.'-'.$request->thn_lahir_pasangan;
            //     $detil->pasangan_jenis_kelamin = $request->jenis_kelamin_pasangan;
            //     $detil->pasangan_ktp = $request->no_ktp_pasangan;
            //     $detil->pasangan_npwp = $request->no_npwp_pasangan;
            //     $detil->pasangan_phone = $request->no_telp_pasangan;
            //     $detil->pasangan_alamat = $request->alamat_pasangan;
            //     $detil->pasangan_provinsi = $request->provinsi_pasangan;
            //     $detil->pasangan_kota = $request->kota_pasangan;
            //     $detil->pasangan_kode_pos = $request->kode_pos_pasangan;
            //     $detil->pasangan_agama = $request->agama_pasangan;
            //     $detil->pasangan_pekerjaan = $request->pekerjaan_pasangan;
            //     $detil->pasangan_bidang_pekerjaan = $request->bidang_pekerjaan_pasangan;
            //     $detil->pasangan_online = $request->pekerjaan_online_pasangan;
            //     $detil->pasangan_pendapatan = $request->pendapatan_pasangan;
            //     $detil->pasangan_pengalaman = $request->pengalaman_pasangan;
            //     $detil->pasangan_pendidikan = $request->pendidikan_pasangan;
            // }
            // else
            // {
            //     $detil->pasangan_investor = null;
            //     $detil->pasangan_email = null;
            //     $detil->pasangan_tempat_lhr = null;
            //     $detil->pasangan_tgl_lhr = null;
            //     $detil->pasangan_jenis_kelamin = null;
            //     $detil->pasangan_ktp = null;
            //     $detil->pasangan_npwp = null;
            //     $detil->pasangan_phone = null;
            //     $detil->pasangan_alamat = null;
            //     $detil->pasangan_provinsi = null;
            //     $detil->pasangan_kota = null;
            //     $detil->pasangan_kode_pos = null;
            //     $detil->pasangan_agama = null;
            //     $detil->pasangan_pekerjaan = null;
            //     $detil->pasangan_bidang_pekerjaan = null;
            //     $detil->pasangan_online = null;
            //     $detil->pasangan_pendapatan = null;
            //     $detil->pasangan_pengalaman = null;
            //     $detil->pasangan_pendidikan = null;
            // }
            // $detil->bank = $request->bank;
            // $detil->save();
            // $hasil = $this->generateVA($request->username);
            // }
            // else
            // {
            // $detil->investor_id = $user->id;
            // $detil->tipe_pengguna = $request->tipe_pengguna;
            // $detil->nama_investor = $request->nama;
            // $detil->no_ktp_investor = null;
            // $detil->no_npwp_investor = $request->no_npwp;
            // $detil->phone_investor = $request->no_telp;
            // $detil->alamat_investor = $request->alamat;
            // $detil->provinsi_investor = $request->provinsi;
            // $detil->kota_investor = $request->kota;
            // $detil->kode_pos_investor = $request->kode_pos;
            // $detil->tempat_lahir_investor = null;
            // $detil->tgl_lahir_investor = null;
            // $detil->jenis_kelamin_investor = null;
            // $detil->status_kawin_investor = null;
            // $detil->status_rumah_investor = null;
            // $detil->agama_investor = null;
            // $detil->pekerjaan_investor = null;
            // $detil->bidang_pekerjaan = $request->bidang_pekerjaan;
            // $detil->online_investor = $request->pekerjaan_online;
            // $detil->pendapatan_investor = $request->pendapatan;
            // $detil->asset_investor = $request->asset;
            // $detil->pengalaman_investor = null;
            // $detil->pendidikan_investor = null;
            // $detil->bank_investor = $request->bank;
            // $detil->rekening = $request->rekening;

            // $detil->pasangan_investor = null;
            // $detil->pasangan_email = null;
            // $detil->pasangan_tempat_lhr = null;
            // $detil->pasangan_tgl_lhr = null;
            // $detil->pasangan_jenis_kelamin = null;
            // $detil->pasangan_ktp = null;
            // $detil->pasangan_npwp = null;
            // $detil->pasangan_phone = null;
            // $detil->pasangan_alamat = null;
            // $detil->pasangan_provinsi = null;
            // $detil->pasangan_kota = null;
            // $detil->pasangan_kode_pos = null;
            // $detil->pasangan_agama = null;
            // $detil->pasangan_pekerjaan = null;
            // $detil->pasangan_bidang_pekerjaan = null;
            // $detil->pasangan_online = null;
            // $detil->pasangan_pendapatan = null;
            // $detil->pasangan_pengalaman = null;
            // $detil->pasangan_pendidikan = null;
            // $detil->job_investor = $request->job_investor;     
            // $detil->pic_investor = $this->upload('foto_diri', $request, $user->id);
            // $detil->pic_ktp_investor = $this->upload('foto_ktp', $request, $user->id);
            // $detil->pic_user_ktp_investor = $this->upload('foto_diri_ktp', $request, $user->id);
            // $detil->jenis_badan_hukum = $request->jenis_badan_hukum;
            // $detil->nama_perwakilan = $request->nama_perwakilan;
            // $detil->no_ktp_perwakilan = $request->no_ktp_perwakilan;

            // $detil->save();
            // $hasil = $this->generateVA($request->username);
            // }

            if ($request->hasFile('pic_investor')) {
                // Storage::disk('public')->delete($detil_insert->pic_investor);
                Storage::disk('private')->delete($detil_insert->pic_investor);
                $detil_insert->pic_investor = $this->upload('pic_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_ktp_investor')) {
                // Storage::disk('public')->delete($detil_insert->pic_ktp_investor);
                Storage::disk('private')->delete($detil_insert->pic_ktp_investor);
                $detil_insert->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_user_ktp_investor')) {
                // Storage::disk('public')->delete($detil_insert->pic_user_ktp_investor);
                Storage::disk('private')->delete($detil_insert->pic_user_ktp_investor);
                $detil_insert->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $request->investor_id);
            }

            $detil_insert->save();

            $data_investor = Investor::where('id', $request->investor_id)->first();
            $hasil = $this->generateVA($data_investor->username);
            if (!$hasil) {
                return redirect()->back()->with([
                    'error' => 'Akun anda sudah Aktif tetapi pembuatan VA gagal, harap menghubungi Customer Service kami untuk pembuatan nomor VA agar bisa melakukan Top Up Dana',
                ])
                    ->withInput();
            } else {
                // $investor_id=Auth::user()->id;
                dispatch(new InvestorVerif($data_investor, 1));
                #pesan verifikasi
                $kirimverifikasi = $this->verificationCode($request->investor_id);

                if ($kirimverifikasi === 5) {
                    return redirect()->back()->with('updated', "Insert user Success");
                } else {
                    return redirect()->back()->with('updated', "Insert user Success");
                }

                // return redirect()->back()->with('updated', "Insert user Success");
            }

            // return redirect()->back()->with('updated', "Insert user Success");
        } else {
            $investor = Investor::where('id', $request->investor_id)->first();

            if ($investor->username != $request->username) {
                if (Investor::where('username', $request->username)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }
            if ($investor->email != $request->email) {
                if (Investor::where('email', $request->email)->exists()) {
                    return redirect()->back()->with('exist', "Username or Email already exist");
                }
            }
            // else if ($investor->email != $request->email)
            // {
            //     if(Investor::where('email', $request->email)->exists()){
            //         return redirect()->back()->with('exist', "Username or Email already exist");
            //     }
            // }
            // else
            // {
            //     if(Investor::where('email', $request->email)->exists() || Investor::where('username', $request->username)->exists()){
            //         return redirect()->back()->with('exist', "Username or Email already exist");
            //     }
            // }

            $investor->username = $request->username;
            $investor->email = $request->email;
            $investor->ref_number = $request->kode_ref;

            $investor->save();

            $detil = DetilInvestor::where('investor_id', $request->investor_id)->first();

            // if ($request->tipe_pengguna == 1)
            // {
            // $detil->investor_id = $user->id;
            $detil->tipe_pengguna = null;
            $detil->nama_investor = $request->nama;
            $detil->no_ktp_investor = $request->no_ktp;
            $detil->no_npwp_investor = $request->no_npwp;
            $detil->phone_investor = $request->no_telp;
            $detil->alamat_investor = $request->alamat;
            $detil->provinsi_investor = $request->provinsi;
            $detil->kota_investor = $request->kota;
            $detil->kecamatan = $request->kecamatan;
            $detil->kelurahan = $request->kelurahan;
            $detil->kode_pos_investor = $request->kode_pos;
            $detil->tempat_lahir_investor = $request->tempat_lahir;
            $detil->tgl_lahir_investor = $request->tgl_lahir . '-' . $request->bln_lahir . '-' . $request->thn_lahir;
            $detil->jenis_kelamin_investor = $request->jenis_kelamin;
            $detil->status_kawin_investor = $request->status_kawin;
            $detil->status_rumah_investor = null;
            $detil->agama_investor = null;
            $detil->pekerjaan_investor = $request->pekerjaan;
            $detil->bidang_pekerjaan = null;
            $detil->online_investor = null;
            $detil->pendapatan_investor = $request->pendapatan;
            $detil->asset_investor = null;
            $detil->pengalaman_investor = null;
            $detil->pendidikan_investor = $request->pendidikan;
            $detil->bank_investor = $request->bank;
            $detil->rekening = $request->rekening;
            $detil->nama_pemilik_rek = $request->nama_pemilik_rek;
            $detil->nama_ibu_kandung = $request->nama_ibu_kandung;
            // $detil->job_investor = $request->job_investor;     
            // $detil->pic_investor = $this->upload('foto_diri', $request, $user->id);
            // $detil->pic_ktp_investor = $this->upload('foto_ktp', $request, $user->id);
            // $detil->pic_user_ktp_investor = $this->upload('foto_diri_ktp', $request, $user->id);
            $detil->jenis_badan_hukum = null;
            $detil->nama_perwakilan = null;
            $detil->no_ktp_perwakilan = null;


            if ($request->status == 'suspend') {
                $suspended_by = Auth::guard('admin')->user()->firstname;
                $investor = Investor::where('id', $request->investor_id)->first();
                $investor->status = $request->status;
                $investor->keterangan = $request->alasan_suspend;
                $investor->suspended_by = $suspended_by;
                $investor->save();

                $Log = new LogSuspend;
                $Log->keterangan = $request->alasan_suspend;
                $Log->suspended_by = $suspended_by;
                $Log->save();
            } elseif ($request->status == 'active') {
                $actived_by = Auth::guard('admin')->user()->firstname;
                $investor = Investor::where('id', $request->investor_id)->first();
                $investor->status = $request->status;
                $investor->keterangan = $request->alasan_active;
                $investor->actived_by = $actived_by;
                $investor->save();
            }

            if ($request->hasFile('pic_investor')) {
                // Storage::disk('public')->delete($detil->pic_investor);
                Storage::disk('private')->delete($detil->pic_investor);
                $detil->pic_investor = $this->upload('pic_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_ktp_investor')) {
                // Storage::disk('public')->delete($detil->pic_ktp_investor);
                Storage::disk('private')->delete($detil->pic_ktp_investor);
                $detil->pic_ktp_investor = $this->upload('pic_ktp_investor', $request, $request->investor_id);
            }

            if ($request->hasFile('pic_user_ktp_investor')) {
                // Storage::disk('public')->delete($detil->pic_user_ktp_investor);
                Storage::disk('private')->delete($detil->pic_user_ktp_investor);
                $detil->pic_user_ktp_investor = $this->upload('pic_user_ktp_investor', $request, $request->investor_id);
            }

            $detil->save();

            $rekening = RekeningInvestor::where('investor_id', $request->investor_id)->first();
            if ($rekening == null) {
                if ($request->va_number) {
                    RekeningInvestor::create([
                        'investor_id' => $request->investor_id,
                        'va_number' => $request->va_number,
                        'total_dana' => 0,
                        'unallocated' => 0,
                    ]);
                }
            } else {
                if ($request->va_number !== $rekening->va_number) {
                    $rekening->va_number = $request->va_number;
                    $rekening->save();
                }
            }

            #va lain (cimbs,bsi)
            $va = array('022', '451');
            for ($i = 0; $i < count($va); $i++) {
                $cekva = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE investor_id = $request->investor_id AND kode_bank = " . $va[$i] . "");
                if ($va[$i] == '022') {
                    $value = $request->va_cimbs;
                } elseif ($va[$i] == '451') {
                    $value = $request->va_bsi;
                }
                if ($cekva == NULL) {
                    DB::table('detil_rekening_investor')->insert([
                        'investor_id' => $request->investor_id,
                        'va_number' => $value,
                        'kode_bank' => $va[$i]
                    ]);
                } else {
                    DB::table('detil_rekening_investor')
                        ->where('investor_id', $request->investor_id)
                        ->where('kode_bank', $va[$i])
                        ->update(['va_number' => $value]);
                }
            }

            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Kelola Pendana";
            $audit->description = "Ubah data profil Pendana";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return redirect()->back()->with('updated', "Update user Success");
        }


    }

    public function admin_changepass_investor(Request $request)
    {
        $investor = Investor::where('id', $request->investor_id)
            ->update(['password' => bcrypt($request->newpassword)]);

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pendana";
        $audit->description = "Ubah password Pendana";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('changepass', "Change password user success");
    }

    public function admin_changestatus_investor(Request $request)
    {
        $investor = Investor::where('id', $request->investor_id)
            ->update(['status' => 'notfilled']);

        return redirect()->back()->with('changepass', "Kata sandi pengguna berhasil diubah");
    }

    private function upload($column, Request $request, $investor_id)
    {
        if ($request->hasFile($column)) {
            $file = $request->file($column);
            $resize = $resize = Image::make($file)->resize(480, 640, function ($constraint) {
                $constraint->aspectRatio();
            })->save();
            $filename = Carbon::now()->toDateString() . $column . '.' . $file->getClientOriginalExtension();
            //            save nama file berdasarkan tanggal upload+nama file
            $store_path = 'user/' . $investor_id;
            // $path = $file->storeAs($store_path, $filename, 'public');
            $path = $file->storeAs($store_path, $filename, 'private');
            //            save gambar yang di upload di public storage
            return $path;
        } else {
            return null;
        }
    }

    // Notifikasi Tambah / Penarikan Dana - 18 Maret 2021 - Anas
    public function admin_add_pendanaan_old(Request $request)
    {
        $rekening = RekeningInvestor::where('investor_id', $request->investor_id)->first();
        $proyek = Proyek::where('id', $request->proyek_id)->first();
        $user = Investor::find($request->investor_id);
        $nominal = $request->jumlah_paket * $proyek->harga_paket;

        $jumlahPendanaan = PendanaanAktif::where('proyek_id', $request->proyek_id)->sum('total_dana');
        $selesai = Carbon::parse($proyek->tgl_selesai_penggalangan)->toDateString();
        $sekarang = Carbon::now()->toDateString();
        // echo $jumlahPendanaan+$proyek->terkumpul.','.$proyek->total_need;die;

        $jumlahPenarikan = PenarikanDana::where('investor_id', $request->investor_id)->where('accepted', 0)->sum('jumlah');
        // echo $jumlahPenarikan;die;
        $totalDana = ($proyek->harga_paket * $request->jumlah_paket) + $jumlahPenarikan;
        $jumlahRekening = 0;
        $jumlahRekening += $rekening->unallocated;

        if ($jumlahPendanaan + $proyek->terkumpul < $proyek->total_need && $selesai >= $sekarang) {
            if ($totalDana > $jumlahRekening) {
                if ($jumlahPenarikan > 0) {
                    return response()->json([
                        'status' => '0',
                        'keterangan' => "Dana Tersedia anda sebesar Rp " . number_format($jumlahPenarikan, 0, "", ".") . " sedang kami proses di penarikan dana"
                    ]);
                } else {
                    return response()->json([
                        'status' => '0',
                        'keterangan' => "Dana Tidak Cukup"
                    ]);
                }
            } else {
                $val = $proyek->harga_paket * $request->jumlah_paket;
                $sisatersedia = $proyek->total_need - ($jumlahPendanaan + $proyek->terkumpul);

                if ($val > $sisatersedia) {
                    return response()->json([
                        'status' => '0',
                        'keterangan' => "Gagal Menambah Pendanaan, Pastikan Paket tidak melebihi Sisa Paket !"
                    ]);
                } else {
                    $check = PendanaanAktif::where('investor_id', $request->investor_id)
                        ->where('tanggal_invest', Carbon::now()->toDateString())
                        ->where('proyek_id', $request->proyek_id);


                    if ($check->count() == 0) {
                        $pendanaan = new PendanaanAktif;
                        $pendanaan->investor_id = $request->investor_id;
                        $pendanaan->proyek_id = $request->proyek_id;
                        $pendanaan->total_dana = $val;
                        $pendanaan->nominal_awal = $val;
                        $pendanaan->tanggal_invest = Carbon::now()->toDateString();
                        $pendanaan->last_pay = Carbon::now()->toDateString();;
                        $pendanaan->save();

                        $log = new LogPendanaan;
                        $log->pendanaanAktif_id = $pendanaan->id;
                        $log->nominal = $pendanaan->nominal_awal;
                        $log->tipe = 'add active investation by admin';
                        $log->save();

                        $email = new AdminAddPendanaanEmail($pendanaan, $rekening, $user);
                        Mail::to($user->email)->send($email);

                        $send = 1;
                    } else {
                        $pendanaanAktif = $check->first();
                        $pendanaanAktif->update(['total_dana' => $pendanaanAktif->total_dana + $val, 'nominal_awal' => $pendanaanAktif->nominal_awal + $val, 'status' => 1]);

                        $log = new LogPendanaan;
                        $log->pendanaanAktif_id = $pendanaanAktif->id;
                        $log->nominal = $val;
                        $log->tipe = 'add active investation by admin';
                        $log->save();

                        $email = new AdminAddPendanaanEmail($pendanaanAktif, $rekening, $user);
                        Mail::to($user->email)->send($email);

                        $send = 2;
                    }


                    $rekening->unallocated = $rekening->unallocated - $val;
                    $rekening->save();

                    $audit = new AuditTrail;
                    $username = Auth::guard('admin')->user()->firstname;
                    $audit->fullname = $username;
                    $audit->menu = "Kelola Pendana";
                    $audit->description = "Tambah Pendanaan Proyek";
                    $audit->ip_address =  \Request::ip();
                    $audit->save();

                    if ($send == 1) {
                        return response()->json([
                            'status' => '1',
                            'keterangan' => "Pendanaan Berhasil"
                        ]);
                    } else {
                        return response()->json([
                            'status' => '1',
                            'keterangan' => "Berhasil Menambah Pendanaan"
                        ]);
                    }
                }
            }
        } elseif ($jumlahPendanaan + $proyek->terkumpul < $proyek->total_need && $selesai < $sekarang) {
            return response()->json([
                'status' => '0',
                'keterangan' => "Penggalangan Dana Proyek Sudah Selesai"
            ]);
            // return redirect()->back()->with('error', 'Penggalangan Dana Proyek Sudah Selesai');
        } elseif ($jumlahPendanaan + $proyek->terkumpul >= $proyek->total_need && $selesai >= $sekarang) {
            return response()->json([
                'status' => '0',
                'keterangan' => "Proyek Sudah Penuh"
            ]);
            // return redirect()->back()->with('error', 'Proyek Sudah Penuh');
        } else {
            return response()->json([
                'status' => '0',
                'keterangan' => "Proyek Sudah Penuh & Penggalangan Dana Proyek Sudah Selesai"
            ]);
            // return redirect()->back()->with('error', 'Proyek Sudah Penuh & Penggalangan Dana Proyek Sudah Selesai');
        }
    }

    public function admin_add_pendanaan(Request $request)
    {
        $investor_id = $request->investor_id;
        $proyek_id = $request->proyek_id;
        $jumlah_paket = $request->jumlah_paket;
        $date = date('Ymd');

        $run = DB::SELECT("CALL proc_val_pendanaan_proyek('2'," . $investor_id . "," . $proyek_id . "," . $jumlah_paket . "," . $date . ",'admin_add_pendanaan/InvestorController.php','2093')");
        if ($run[0]->sout == '1') {
            $proyek = Proyek::where('id', $request->proyek_id)->first();
            $user = Investor::find($request->investor_id);


            $val = $proyek->harga_paket * $jumlah_paket;

            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Kelola Pendana";
            $audit->description = "Tambah Pendanaan Proyek";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            try {
                $namaproyek = $proyek->nama;
                $email = new AdminAddPendanaanEmail($user->username, $val, $namaproyek);
                Mail::to($user->email)->send($email);
            } catch (\Swift_RfcComplianceException $e) {
            } catch (\Swift_TransportException $e) {
            }

            return response()->json([
                'status' => '1',
                'keterangan' => "Pendanaan Berhasil"
            ]);
        } else {
            return response()->json([
                'status' => '0',
                'keterangan' => str_replace('#', '', $run[0]->sout)
            ]);
        }
    }

    public function admin_add_va(Request $request)
    {
        $rekening = RekeningInvestor::where('investor_id', $request->investor_id)->first();

        if (preg_match("/^[0-9,]+$/", $request->nominal)) {
            $request->nominal = str_replace(',', '', $request->nominal);
        }
        $rekening->unallocated += $request->nominal;
        $rekening->total_dana += $request->nominal;
        $rekening->save();

        event(new MutasiInvestorEvent($request->investor_id, 'CREDIT', $request->nominal, $request->perihal));

        // $log = new MutasiInvestor;
        // $log->investor_id = $request->investor_id;
        // $log->nominal = $request->nominal;
        // $log->tipe = 'CREDIT';
        // $log->perihal = 'add dana VA oleh admin';
        // $log->save();

        return redirect()->back()->with('success', 'Berhasil menambah dana investor');
    }

    public function admin_minus_dana(Request $request)
    {
        $rekening = RekeningInvestor::where('investor_id', $request->investor_id)->first();

        if (preg_match("/^[0-9,]+$/", $request->nominal)) {
            $request->nominal = str_replace(',', '', $request->nominal);
        }
        $rekening->unallocated -= $request->nominal;
        $rekening->total_dana -= $request->nominal;
        $rekening->save();

        event(new MutasiInvestorEvent($request->investor_id, 'DEBIT', $request->nominal, $request->perihal));

        // $log = new MutasiInvestor;
        // $log->investor_id = $request->investor_id;
        // $log->nominal = $request->nominal;
        // $log->tipe = 'CREDIT';
        // $log->perihal = 'add dana VA oleh admin';
        // $log->save();

        return redirect()->back()->with('success', 'Berhasil mengurangi dana investor');
    }

    public function admin_hapus_pendanaan(Request $request)
    {
        $pendanaan = PendanaanAktif::find($request->pendanaan_id);

        $rekening = RekeningInvestor::where('investor_id', $request->investor_id)->first();
        $rekening->unallocated += $pendanaan->nominal_awal;
        $rekening->save();

        $pendanaan->status = 0;
        $pendanaan->total_dana -= $pendanaan->nominal_awal;
        $pendanaan->nominal_awal = 0;
        $pendanaan->save();

        $log = new LogPendanaan;
        $log->pendanaanAktif_id = $pendanaan->id;
        $log->nominal = $pendanaan->nominal_awal;
        $log->tipe = 'ambil active investation by admin';
        $log->save();


        return redirect()->back()->with('success', 'Pendanaan Berhasil Dihapus');
    }

    public function get_proyek_datatables($id)
    {
        $proyek = DB::select("SELECT 
            IFNULL((SELECT  SUM(total_dana) FROM pendanaan_aktif WHERE proyek_id = a.id AND STATUS = 1),0)+a.terkumpul AS total_terkumpul, 
            IFNULL((SELECT SUM(total_dana) FROM pendanaan_aktif WHERE proyek_id = a.id AND STATUS = 1),0) AS total_danapendanan, 
            a.*
            FROM proyek a
            WHERE a.STATUS = 1 
            GROUP BY a.id");


        $total = RekeningInvestor::where('investor_id', $id)->first();

        $response = ['data_proyek' => $proyek, 'data_total' => $total];

        return response()->json($response);
    }

    public function export_pendanaan_aktif()
    {

        $date = Carbon::now()->toDateString();
        return Excel::download(new DanaInvestorProyek, 'export_pendanaan_aktif-' . $date . '.xlsx');
    }

    public function admin_edit_nominal_pendanaan(Request $request)
    {
        /* $pendanaan = PendanaanAktif::where('id', $request->pendanaan_id)->first();
        
        $pendanaan->total_dana = str_replace(',','',$request->nominal);
        $pendanaan->nominal_awal = str_replace(',','',$request->nominal); 
        $pendanaan->save();

        return redirect()->back()->with('success', 'Berhasil mengubah nominal pendanaan investor'); */
        // update tanggal 14 Januari 2021
        $flag = 1;
        $nominal = str_replace(",", "", $request->nominal);
        $pendanaan = PendanaanAktif::where('id', $request->pendanaan_id)->first();

        if ($nominal > $pendanaan['total_dana']) {
            $flag = 0;
        } elseif ($nominal == $pendanaan['total_dana']) {
            $flag = 2;
        } else {
            if (fmod($nominal, 1000000) == 0) {
                $qty = ($pendanaan['total_dana'] - $nominal) / 1000000;
                $run = DB::select("CALL proc_pengambilandanaproyek($request->pendanaan_id,$qty,'InvestorController.php',2223,@res)");
                $feedback =  $run[0]->{'res'};
                if ($feedback == 1) {
                    $flag = 1;
                } else {
                    $flag = 3;
                }
            } else {
                $flag = 3;
            }
        }

        /*
        ket:
        0 = gagal penarikan gunakan fitur penambahan dana;
        1 = sukses penarikan;
        2 = Tidak Terjadi Penarikan;
        3 = Dana tidak sesuai;
        */

        if ($flag == 0) {
            $status = 'error';
            $ket = 'Gunakan Fitur Kelola Proyek untuk Melakukan Penambahan Dana Proyek';
        } elseif ($flag == 1) {
            $status = 'success';
            $ket = 'Berhasil Melakukan Penarikan Dana Proyek';
        } elseif ($flag == 2) {
            $status = 'success';
            $ket = 'Tidak Terjadi Penarikan Dana';
        } elseif ($flag == 3) {
            $status = 'error';
            $ket = 'Dana yang dimasukan tidak sesuai dengan Paket Proyek, Silahkan Coba Lagi !';
        }

        return redirect()->back()->with($status, $ket);
    }

    public function admin_edit_nominal_pendanaan_selesai(Request $request)
    {
        $pendanaan_selesai = LogPengembalianDana::where('id', $request->pendanaan_selesai_id)->first();

        $pendanaan_selesai->nominal = str_replace(',', '', $request->nominal_selesai);
        $pendanaan_selesai->save();

        return redirect()->back()->with('success', 'Berhasil mengubah nominal pendanaan selesai investor');
    }

    public function verificationCode($investor_id)
    {

        $rekening = RekeningInvestor::join('detil_investor', 'detil_investor.investor_id', '=', 'rekening_investor.investor_id')
            ->select('rekening_investor.va_number', 'detil_investor.nama_investor', 'detil_investor.phone_investor')
            ->where('rekening_investor.investor_id', $investor_id)->first();
        $to =  $rekening->phone_investor;
        // $to = '081318988499';
        $text =  'Terima kasih, akun ' . $rekening->username . ' telah berhasil diverifikasi dengan nomor Virtual Account: ' . $rekening->va_number . ' atas nama ' . $rekening->nama_investor . ' . Silahkan lakukan Top Up dana Anda ke nomor virtual account tersebut.';
        // die();
        $pecah              = explode(",", $to);
        $jumlah             = count($pecah);
        $from               = "DANASYARIAH"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
        $username           = "danasyariahpremium"; //your smsviro username
        $password           = "Dsi701@2019"; //your smsviro password
        $postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS

        for ($i = 0; $i < $jumlah; $i++) {
            if (substr($pecah[$i], 0, 2) == "62" || substr($pecah[$i], 0, 3) == "+62") {
                $pecah = $pecah;
            } elseif (substr($pecah[$i], 0, 1) == "0") {
                $pecah[$i][0] = "X";
                $pecah = str_replace("X", "62", $pecah);
            } else {
                echo "Invalid mobile number format";
            }
            $destination = array("to" => $pecah[$i]);
            $message     = array(
                "from" => $from,
                "destinations" => $destination,
                "text" => $text,
                "smsCount" => 20
            );
            $postData           = array("messages" => array($message));
            $postDataJson       = json_encode($postData);
            $ch                 = curl_init();
            $header             = array("Content-Type:application/json", "Accept:application/json");

            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            // $responseBody = json_decode($response);
            $responseBody = json_decode($response, true);
            curl_close($ch);
        }

        $group_id = $responseBody['messages'][0]['status']['groupId'];

        if ($response) {
            // return response()->json(['success'=>$text]);
            return $group_id;
        } else {
            // return response()->json(['success'=>'gagal']);
            return $group_id;
        }
    }

    public function get_akad_investor()
    {

        return view('pages.admin.investor_akad');
    }

    public function get_json_nama_investor(Request $request)
    {
        if ($request->get('query') != '') {
            $query = $request->get('query');
            $data = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('detil_investor.nama_investor', 'like', '%' . $query . '%')
                ->orderBy('investor.id', 'desc')
                ->get();
            $output = '<ul class="list-group">';
            foreach ($data as $row) {
                $output .= '<li class="list-group-item">&nbsp;<a href="#">' . $row->nama_investor . '</a>&nbsp;</li>';
            }
            $output .= '</ul>';

            echo $output;
        } else {
            $output = '';
            echo $output;
        }
    }

    public function isTreshold($investor_id)
    {
        $privy = new PrivyController;

        $isthreshold = DB::select("select func_check_threshold('$investor_id') as response");

        if ($isthreshold[0]->{"response"} == 1) { // jika dana total asset lebih dari atau sama dengan dari treshold 

            $logAkadDigiSignInvestor = LogAkadDigiSignInvestor::where(\DB::raw('substr(document_id, 1, 15)'), '=', 'investorKontrak')
                ->where('investor_id', $investor_id)
                ->where('status', 'Completed')
                ->orderBy('id_log_akad_investor', 'desc')
                ->first();

            $getRekeningInvestor = RekeningInvestor::where('investor_id', $investor_id)->first();
            // $getLogRekening = LogRekening::whereRaw("id = (select max(`id`) from log_rekening where investor_id = '$investor_id')")->first();
            // $LogDanaRekening    = !empty($getLogRekening) ? $getLogRekening->nominal : '';
            $getTotalDana = !empty($getRekeningInvestor) ? $getRekeningInvestor->total_dana : '';

            if ($logAkadDigiSignInvestor) { // Jika dokumen ada di privy & status sudah di tanda tangani / completed
                if ($getTotalDana != $logAkadDigiSignInvestor->total_pendanaan) { // Jika total dana di privy belum sesuai dengan data terbaru
                    return $privy->createDocAkadWakalahBilUjrohInvestor($investor_id);
                } else { // Jika total dana di privy sesuai dengan data terbaru
                    $docToken = $logAkadDigiSignInvestor->docToken;

                    $client = new Client();
                    $request = $client->get(config('app.privy_url') . '/document/status/' . $docToken, [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                            'charset' => 'utf-8',
                            'Authorization' => 'Basic ' . $privy->base64_usename_password(),
                            'Merchant-Key' => config('app.privy_merchent_key')
                        ]

                    ]);

                    $response = (string)$request->getBody();
                    $response_decode = json_decode($response);

                    return response()->json([
                        'status' => 'privy',
                        'downloadURL' => $response_decode->{'data'}->{'download'}->{'url'}
                    ]);
                }
            } else { // Dokumen tidak ada di privy
                return $privy->createDocAkadWakalahBilUjrohInvestor($investor_id);
            }
        } else { // jika dana total asset kurang dari treshold
            return $privy->createDocAkadWakalahBilUjrohInvestor($investor_id);
        };
    }

    public function inquiryVa()
    {
        $master_kode_operator = DB::table("m_kode_operator_negara")->get();

        return view('pages.admin.inquiry_va', [
            'master_kode_operator' => $master_kode_operator
        ]);
    }

    public function inquiryVaGelAllVaNumber(Request $request)
    {

        $no_hp = $request->in_kode_operator . $request->in_no_hp;

        $investor_id = DB::table('detil_investor')
            ->where('phone_investor', $no_hp)->pluck('investor_id');

        $response = [];

        if ($investor_id->isEmpty()) {
            $response = [
                'status' => 'error',
                'msg' => 'No handphone tidak terdaftar',
                'data' => ''
            ];
        } else {
            $get_all_va_1 = DB::table('rekening_investor AS a')
                ->leftJoin('m_bank AS b', 'b.kode_bank', '=', 'a.kode_bank')
                ->leftJoin('m_bank_partner AS c', 'c.kode_bank', '=', 'a.kode_bank')
                ->distinct()
                ->select([
                    'a.va_number',
                    'a.kode_bank',
                    'b.nama_bank'
                ])
                ->where('investor_id', $investor_id[0])->get();

            $get_all_va_2 = DB::table('detil_rekening_investor AS a')
                ->leftJoin('m_bank AS b', 'b.kode_bank', '=', 'a.kode_bank')
                ->leftJoin('m_bank_partner AS c', 'c.kode_bank', '=', 'a.kode_bank')
                ->distinct()
                ->select([
                    'a.va_number',
                    'a.kode_bank',
                    'b.nama_bank'
                ])
                ->where('investor_id', $investor_id[0])->get();

            $response = [
                'status' => 'success',
                'msg' => 'Data ditemukan',
                'data' => [$get_all_va_1, $get_all_va_2]
            ];
        }

        return $response;
    }
}
