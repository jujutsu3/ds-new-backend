<?php

namespace App\Http\Controllers;

use App\BorrowerUserDetail;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DB;
use App\Http\Middleware\StatusProyek;
use App\Borrower;
use App\AuditTrail;
use Auth;


class BorrowerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private const API_LOGIN = 'http://149.129.250.37/api/auth/login';
    private const HEADER_PUSDAFIl = 'application/json';
    private const API_CEK = 'http://149.129.250.37/api/Inquiry/LoanReport/';

    public function __construct()
    {
        $this->middleware('auth:admin');
        // $this->middleware(NotifikasiProyek::class);
        $this->middleware(StatusProyek::class);
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function show(){
        $master_kode_operator = DB::table("m_kode_operator_negara")->get();
            return view('pages.admin.borrower_manage',['master_kode_operator' => $master_kode_operator]);
    }

    public function data_borrower(Request $request)
    {
        $cek_data = 0;
        $variabelCari = 0;
        if ($request->username != null) {
            $variabelCari = $request->username;
            $cek_data = Borrower::where('username', 'like', '%' . $request->username . '%')
                ->whereIn('status',['Not Active','notfilled','notpreapproved'])
                ->orderBy('brw_id', 'desc')
                ->count();
        }else if ($request->search_email != null) {
            $variabelCari = $request->search_email;
            $cek_data = Borrower::where('email', 'like', '%' . $request->search_email . '%')
                ->whereIn('status',['Not Active','notfilled','notpreapproved'])
                ->orderBy('brw_id', 'desc')
                ->count();
        } else if ($request->phone_number != '') {
            $variabelCari = $request->kode_operator . $request->phone_number;
            $cek_data = BorrowerUserDetail::leftjoin('brw_user','brw_user.brw_id', '=', 'brw_user_detail.brw_id')
                ->where('brw_user_detail.no_tlp', 'like', '%' . $request->kode_operator . $request->phone_number . '%')
                ->whereIn('brw_user.status',['Not Active','notfilled','notpreapproved'])
                ->orderBy('brw_user.brw_id', 'desc')
                ->count();
        }
        // echo $cek_data;die;

        if ($cek_data != 0) {
            $response = ['status' => 'Ada'];
            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Kelola Tautan / OTP Penerima Pembiayaan";
            $audit->description = "Kelola Tautan / OTP Penerima Pembiayaan => pencarian : ".$variabelCari;
            $audit->ip_address =  \Request::ip();
            $audit->save();
        } else {
            $response = ['status' => 'Kosong'];
        }

        return response()->json($response);
    }

    public function get_borrower_datatables(Request $request)
    {
        if ($request->username != '') {
            $mutasi = Borrower::where('brw_user.username', 'like', '%' . $request->username . '%')
                ->whereIn('brw_user.status',['Not Active','notfilled','notpreapproved'])
                ->groupBy([
                    'brw_user.brw_id',
                    'brw_user.email',
                    'brw_user.username',
                    'brw_user.status'
                ])
                ->get([
                    'brw_user.brw_id AS idBrw',
                    'brw_user.username',
                    'brw_user.email',
                    'brw_user.status',
                    'brw_user.email_verif',
                    'brw_user.otp'
                ]);
        } else if ($request->search_email != '') {
            $mutasi = Borrower::where('brw_user.email', 'like', '%' . $request->search_email . '%')
                ->whereIn('brw_user.status',['Not Active','notfilled','notpreapproved'])
                ->groupBy([
                    'brw_user.brw_id',
                    'brw_user.email',
                    'brw_user.username',
                    'brw_user.status'
                ])
                ->get([
                    'brw_user.brw_id AS idBrw',
                    'brw_user.username',
                    'brw_user.email',
                    'brw_user.status',
                    'brw_user.email_verif',
                    'brw_user.otp'
                ]);
        } else if ($request->phone_number != '') {
            $mutasi = BorrowerUserDetail::leftjoin('brw_user','brw_user.brw_id', '=', 'brw_user_detail.brw_id')
                ->where('brw_user_detail.no_tlp', 'like', '%' . $request->kode_operator . $request->phone_number . '%')
                ->whereIn('brw_user.status',['Not Active','notfilled','notpreapproved'])
                ->groupBy([
                    'brw_user.brw_id',
                    'brw_user.email',
                    'brw_user.username',
                    'brw_user.status'
                ])
                ->get([
                    'brw_user.brw_id AS idBrw',
                    'brw_user.username',
                    'brw_user.email',
                    'brw_user.status',
                    'brw_user.email_verif',
                    'brw_user.otp'
                ]);
        }

        // var_dump($mutasi);die;

        $response = ['data' => $mutasi];

        return response()->json($response);
    }

    public function search(Request $pusdafil){
        $client = new Client();
        $data_login = [
                            'email' => 'danasyariah@pusdafil.com',
                            'password' => 'dsi123456'
                      ];
        $raw = json_encode($data_login);

        $connect = $client->post(self::API_LOGIN,[
                    'headers' => ['Content-Type' => self::HEADER_PUSDAFIl],
                    'body'  => $raw
                ]);

        $status = json_decode($connect->getBody());
        if ($status->responsCode == '00')
        {
            $bearer = 'Bearer '.$status->token;
            $data_id = [ 'id' => $pusdafil->nik ];
            $id = json_encode($data_id);
            $cek = $client->get(self::API_CEK,[
                    'headers' => [  'Content-Type' => self::HEADER_PUSDAFIl,
                                    'Authorization' => $bearer
                                ],
                    'body'  => $id
                ]);

            return $cek->getBody();
        }
        else
        {
            return response()->json(['data' => 'gagal']);
        }
    }

}
