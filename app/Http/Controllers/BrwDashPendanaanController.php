<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Mail;
use Response;
use App\Admins;
use App\Proyek;
use App\Borrower;
use Carbon\Carbon;
use App\CheckUserSign;
use GuzzleHttp\Client;
use App\Helpers\Helper;
use App\LogSP3Borrower;
use App\TeksNotifikasi;
use App\BorrowerRekening;
use App\BorrowerPendanaan;
use App\BorrowerPengajuan;
use Illuminate\Http\Request;
use App\BorrowerMaterialTask;
use App\Mail\SetujuiSp3Email;
use App\BorrowerDanaTerkumpul;
use App\BorrowerTipePendanaan;
use App\LogAkadDigiSignBorrower;
use App\BorrowerAnalisaPendanaan;

use App\BorrowerCartMaterialOrder;
use App\Http\Controllers\Controller;
use App\BorrowerVerifikatorPengajuan;
use Illuminate\Support\Facades\Crypt;
use App\Services\MaterialOrderService;
use Illuminate\Support\Facades\Storage;
use App\Mail\NotifPembayaranCicilanEmail;
use App\BorrowerMaterialRequisitionHeader;

use App\Http\Controllers\MaterialOrdersController;
use Illuminate\Contracts\Encryption\DecryptException;

class BrwDashPendanaanController extends Controller
{
	public function __construct()
	{


		$getPendanaan = BorrowerPendanaan::where('id_proyek', '!=', 0)->get();
		if ($getPendanaan) {
			foreach ($getPendanaan as $Pendanaans) {
				$getStatus = Proyek::where('id', $Pendanaans->id_proyek)->first();
				if (empty($getStatus)) {
					$status = 0;
				} else {
					$status = $getStatus->status;
				}
				//cek
				if ($status == 2 && $Pendanaans->status == 1 || $status == 3 && $Pendanaans->status == 1) {
					//echo $Pendanaans->id_proyek;die;
					//echo $status;die;
					BorrowerPendanaan::where('id_proyek', $Pendanaans->id_proyek)->update(['status' => 6]);
					//BorrowerPendanaan::where('id_proyek', $Pendanaans->id_proyek)->update(['status' => $status]);
					//return $update;
					//$this->updatePendanaan($Pendanaans->pendanaan_id,6);

				}
			}
		}


		$this->middleware('auth:borrower');
		$this->middleware(CheckUserSessionBorrower::class)->only(['dashboard']);
		// $this->middleware(brwCheck::class);
		// $this->middleware(UserCheck::class)->except(['lengkapi_profile','view_status_reject']);

		// $this->middleware(StatusPendanaan::class)->only(['dashboard','detilProyek','pendanaanPage','getProyekbyId']);

	}

	public function dashBoard(Request $request)
	{
		$client = new client();

		// $id = Session::get('brw_id');
		$id = Auth::guard('borrower')->user()->brw_id;

		//total keseluruhan
		$response_brwByid = $client->request('GET', config('app.apilink') . "/borrower/brwByid/" . $id);
		$body_BrwByid = json_decode($response_brwByid->getBody()->getContents());

		//pendanaan berjalan
		$response_brwBerjalanByid = $client->request('GET', config('app.apilink') . "/borrower/brwBerjalanById/" . $id);
		$body_BrwBerjalanByid = json_decode($response_brwBerjalanByid->getBody()->getContents());

		//pengajuan
		$response_pengajuan = $client->request('GET', config('app.apilink') . "/borrower/brwPengajuanById/" . $id);
		$body_pengajuan = json_decode($response_pengajuan->getBody()->getContents());

		//finish
		$response_selesai = $client->request('GET', config('app.apilink') . "/borrower/brwSelesaiById/" . $id);
		$body_selesai = json_decode($response_selesai->getBody()->getContents());

		//detailtotalkeseluruhan
		$response_listPro = $client->request('GET', config('app.apilink') . "/borrower/listPro/" . $id);
		$body_listPro = json_decode($response_listPro->getBody()->getContents());

		//plafon
		$response_plafon = $client->request('GET', config('app.apilink') . "/borrower/plaf/" . $id);
		$body_plafon = json_decode($response_plafon->getBody()->getContents());

		//danaditerima
		$response_danaDiterima = $client->request('GET', config('app.apilink') . "/borrower/danatbProyekTerkumpul/" . $id);
		$body_danaDiterima = json_decode($response_danaDiterima->getBody()->getContents());

		//totalImbalHasil
		$response_totalImbalHasil = $client->request('GET', config('app.apilink') . "/borrower/totalImbalHasil/" . $id);
		$body_danaTerkumpul = json_decode($response_totalImbalHasil->getBody()->getContents());


		// var_dump($selesai);
		$totalKeseluruhan = count($body_BrwByid);
		$totalBerjalan = count($body_BrwBerjalanByid);
		$totalPengajuan = count($body_pengajuan);
		$totalselesai = count($body_selesai);
		$danaDiterima = "Rp. " . number_format($body_danaDiterima->danaDiterima);
		$dataPersen = round($body_danaDiterima->dataPersen, 1);
		$danaImbalHasil = "Rp. " . number_format($body_danaTerkumpul->danaImbalHasil);
		$dataPersenImbalHasil = round($body_danaTerkumpul->dataPersenImbalHasil, 1);

		//plafon
		if (empty($body_plafon)) {
			$plafonseluruh = $body_plafon->total_plafon;
			$plafonterpakai = $body_plafon->total_terpakai;
			$plafontersedia = $body_plafon->total_sisa;
		} else {
			$plafonseluruh = "";
			$plafonterpakai = "";
			$plafontersedia = "";
		}



		$hasil = [
			'totalKeseluruhan' => $totalKeseluruhan, 'totalBerjalan' => $totalBerjalan, 'totalPengajuan' => $totalPengajuan, 'totalselesai' => $totalselesai, 'detailKeseluruhan' => $body_listPro,
			'plafonseluruh' => $plafonseluruh, 'plafonterpakai' => $plafonterpakai, 'plafontersedia' => $plafontersedia,
			'danaditerima' => $danaDiterima, 'dataPersen' => $dataPersen,
			'danaImbalHasil' => $danaImbalHasil, 'dataPersenImbalHasil' => $dataPersenImbalHasil
		];

		return view('pages.borrower.dashboard')->with($hasil);
	}

	public function detilProyek(Request $request)
	{

		// $client = new client();
		$id =  $request->id;

		//detail proyek
		// $response_pykid = $client->request('GET', config('app.apilink')."/borrower/pykid/".$id);
		// $body_pykid = json_decode($response_pykid->getBody()->getContents());

		$pykid = Proyek::where('proyek.id', $id)
			->join('brw_pendanaan', 'brw_pendanaan.id_proyek', '=', 'proyek.id')
			->first([
				'proyek.*',
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.pengajuan_id',
				'brw_pendanaan.status as status_pendanaan',
				'brw_pendanaan.status as status_dana',
				'brw_pendanaan.pendanaan_dana_dibutuhkan',
				'brw_pendanaan.mode_pembayaran',
				'brw_pendanaan.metode_pembayaran',
				'brw_pendanaan.dana_dicairkan',
				'brw_pendanaan.pendanaan_akad',
				'brw_pendanaan.pendanaan_tipe',
				'brw_pendanaan.brw_id',
				'brw_pendanaan.lokasi_proyek'
			]);
		$datadanaTerkumpul = BorrowerDanaTerkumpul::where('proyek_id', $id)->sum('nominal_awal');
		$danaTerkumpul = json_encode(array("nominal_awal" => $datadanaTerkumpul));
		$body_danaTerkumpul = json_decode($danaTerkumpul);

		// return response()->json($pykid);




		//gambar proyek

		$response_pykGambar = DB::select("SELECT * FROM gambar_proyek WHERE proyek_id = '" . $id . "'");
		$body_pykGambar = $response_pykGambar;

		$dskPyk = Proyek::where('proyek.id', $id)
			->leftjoin('deskripsi_proyeks', 'deskripsi_proyeks.id', '=', 'proyek.id_deskripsi')
			->first();

		$brwId = Auth::guard('borrower')->user()->brw_id;

		if (!empty($pykid->nama)) {
			$namaproyek = $pykid->nama;
			$danadibutuhkan = $pykid->total_need;
			$durasiproyek = $pykid->tenor_waktu;
			$modepembayaran = $pykid->mode_pembayaran;
			$metodepembayaran = $pykid->metode_pembayaran;
			$danadicairkan = $pykid->dana_dicairkan;
			$hargapaket = $pykid->harga_paket;
			$pendanaanakad = $pykid->pendanaan_akad;
			$imbalhasil = $pykid->profit_margin + 5;

			// progress bar
			// $idProyek = $body_pykid->id;
			$statusproyek = $pykid->status;
			$statusTampil = $pykid->status_tampil;
			$status_pendanaan = $pykid->status_pendanaan;
			$status_dana = $pykid->status_dana;

			$getLogSP3 = LogSP3Borrower::where('id_proyek', $id)->first();
			$statusLogSP3 = !empty($getLogSP3) ? $getLogSP3->status : 0;

			$pengajuan = "";
			$verifikasi = '';
			$penggalangandana = '';
			$ttd = '';
			$proyekberjalan = '';
			$proyekselesai = '';
			$tc_pengajuan = '';
			$icon_pengajuan = '';
			$tc_verifikasi = '';
			$icon_verifikasi = '';
			$tc_penggalangandana = '';
			$icon_penggalangandana = '';
			$tc_ttd = '';
			$icon_ttd =  '';
			$tc_proyekberjalan = '';
			$icon_proyekberjalan = '';
			$tc_proyekselesai = '';
			$icon_proyekselesai = '';


			switch (true) {
				case ($statusproyek == "0"):
					//pengajuan
					$pengajuan = 'active';
					$verifikasi = 'disabled';
					$penggalangandana = 'disabled';
					$ttd = 'disabled';
					$proyekberjalan = 'disabled';
					$proyekselesai = 'disabled';
					$tc_pengajuan = 'text-primary';
					$icon_pengajuan = 'fa-hourglass-half text-primary';
					$tc_verifikasi = 'text-dark';
					$icon_verifikasi = 'fa-times-circle text-secondary';
					$tc_penggalangandana = 'text-dark';
					$icon_penggalangandana = 'fa-times-circle text-secondary';
					$tc_ttd = 'text-dark';
					$icon_ttd =  'fa-times-circle text-secondary';
					$tc_proyekberjalan = 'text-dark';
					$icon_proyekberjalan = 'disabled';
					$tc_proyekselesai = 'text-dark';
					$icon_proyekselesai = 'fa-times-circle text-secondary';
					break;
				case (($statusproyek == "1" && $statusTampil == null) || ($statusproyek == "1" && $statusTampil == "1") || ($statusproyek == "1" && $statusTampil == "2" && $statusLogSP3 == '0') || ($statusproyek == "1" && $statusTampil == "2" && $statusLogSP3 == '1')):
					//verifikasi
					$pengajuan = 'complete';
					$verifikasi = 'active';
					$penggalangandana = 'disabled';
					$ttd = 'disabled';
					$proyekberjalan = 'disabled';
					$proyekselesai = 'disabled';
					$tc_pengajuan = 'text-primary';
					$icon_pengajuan = 'fa-hourglass-half text-primary';
					$tc_verifikasi = 'text-primary';
					$icon_verifikasi = 'fa-hourglass-half text-primary';
					$tc_penggalangandana = 'text-dark';
					$icon_penggalangandana = 'fa-times-circle text-secondary';
					$tc_ttd = 'text-dark';
					$icon_ttd = 'fa-times-circle text-secondary';
					$tc_proyekberjalan = 'text-dark';
					$icon_proyekberjalan = 'fa-times-circle text-secondary';
					$tc_proyekselesai = 'text-dark';
					$icon_proyekselesai = 'fa-times-circle text-secondary';
					break;
				case (($statusproyek == "1" && $statusTampil == "2" && $statusLogSP3 == '2') || ($statusproyek == "2" && $statusTampil == "1" && $statusLogSP3 == '2') || ($statusproyek == "3" && $statusTampil == "1" && $statusLogSP3 == '2') || ($status_pendanaan == "6" && $statusTampil == "1" && $statusLogSP3 == '2')):
					//penggalangandana
					$pengajuan = 'complete';
					$verifikasi = 'complete';
					$penggalangandana = 'active';
					$ttd = 'disabled';
					$proyekberjalan = 'disabled';
					$proyekselesai = 'disabled';
					$tc_pengajuan = 'text-primary';
					$icon_pengajuan = 'fa-check-circle text-primary';
					$tc_verifikasi = 'text-primary';
					$icon_verifikasi = 'fa-check-circle text-primary';
					$tc_penggalangandana = 'text-primary';
					$icon_penggalangandana = 'fa-hourglass-half text-primary';
					$tc_ttd = 'text-dark';
					$icon_ttd = 'fa-times-circle text-secondary';
					$tc_proyekberjalan = 'text-dark';
					$icon_proyekberjalan = 'fa-times-circle text-secondary';
					$tc_proyekselesai = 'text-dark';
					$icon_proyekselesai = 'fa-times-circle text-secondary';
					break;

					//case (($statusproyek == "2" && $statusTampil == "2") || ($statusproyek == "3" && $statusTampil == "2") || ($status_pendanaan == "6" && $statusTampil == "2") || ($status_pendanaan == "7" && $statusTampil == "1")):
				case ($statusproyek == "2" && $status_pendanaan == 6 || $statusproyek == "3" && $status_pendanaan == 6):
					// ttdProyek
					$pengajuan = 'complete';
					$verifikasi = 'complete';
					$penggalangandana = 'complete';
					$ttd = 'active';
					$proyekberjalan = 'disabled';
					$proyekselesai = 'disabled';
					$tc_pengajuan = 'text-primary';
					$icon_pengajuan = 'fa-check-circle text-primary';
					$tc_verifikasi = 'text-primary';
					$icon_verifikasi = 'fa-check-circle text-primary';
					$tc_penggalangandana = 'text-primary';
					$icon_penggalangandana = 'fa-check-circle text-primary';
					$tc_ttd = 'text-primary';
					$icon_ttd = 'fa-check-circle text-primary';
					$tc_proyekberjalan = 'text-dark';
					$icon_proyekberjalan = 'fa-times-circle text-secondary';
					$tc_proyekselesai = 'text-dark';
					$icon_proyekselesai = 'fa-times-circle text-secondary';
					break;
					//case (($status_pendanaan == "7" && $statusTampil == "2") || ($statusproyek == "4" && $statusTampil == "1")):
				case ($statusproyek == "2" && $status_pendanaan == "7" || $statusproyek == "3" && $status_pendanaan == "7"):
					//case (($status_pendanaan == "7" && $statusTampil == "2") || ($statusproyek == "2" && $statusTampil == "2") || ($statusproyek == "3" && $statusTampil == "2")):
					// proyek Berjalan
					$pengajuan = 'complete';
					$verifikasi = 'complete';
					$penggalangandana = 'complete';
					$ttd = 'complete';
					$proyekberjalan = 'active';
					$proyekselesai = 'disabled';
					$tc_pengajuan = 'text-primary';
					$icon_pengajuan = 'fa-check-circle text-primary';
					$tc_verifikasi = 'text-primary';
					$icon_verifikasi = 'fa-check-circle text-primary';
					$tc_penggalangandana = 'text-primary';
					$icon_penggalangandana = 'fa-check-circle text-primary';
					$tc_ttd = 'text-primary';
					$icon_ttd = 'fa fa-check-circle text-primary';
					$tc_proyekberjalan = 'text-dark';
					$icon_proyekberjalan = 'fa-hourglass-half text-primary';
					$tc_proyekselesai = 'text-dark';
					$icon_proyekselesai = 'fa-times-circle text-secondary';
					break;
					//case ($statusproyek == "4" && $statusTampil == "2"):
				case ($statusproyek == "4" && $status_pendanaan == "7"):
					//case (($status_pendanaan == "7" && $statusTampil == "2") || ($statusproyek == "4" && $statusTampil == "2")):
					// proyekselesai
					$pengajuan = 'complete';
					$verifikasi = 'complete';
					$penggalangandana = 'complete';
					$ttd = 'complete';
					$proyekberjalan = 'complete';
					$proyekselesai = 'complete';
					$tc_pengajuan = 'text-primary';
					$icon_pengajuan = 'fa-check-circle text-primary';
					$tc_verifikasi = 'text-primary';
					$icon_verifikasi = 'fa-check-circle text-primary';
					$tc_penggalangandana = 'text-primary';
					$icon_penggalangandana = 'fa-check-circle text-primary';
					$tc_ttd = 'text-primary';
					$icon_ttd = 'fa-check-circle text-primary';
					$tc_proyekberjalan = 'text-primary';
					$icon_proyekberjalan = 'fa-check-circle text-primary';
					$tc_proyekselesai = 'text-primary';
					$icon_proyekselesai = 'fa-check-circle text-primary';
					break;
				default:
					echo "projek anda di reject !";
			}


			$mutasiProyek = [];
			$getRiwayatMutasi = DB::Select("SELECT * FROM brw_log_rekening WHERE brw_id = " . $pykid->brw_id . " AND pendanaan_id= '" . $pykid->pendanaan_id . "'");
			if ($getRiwayatMutasi != null) {
				foreach ($getRiwayatMutasi as $row) {
					$arrayRiwayat = array(
						'pendanaan_id' => $row->pendanaan_id,
						'brw_id' => $row->brw_id,
						'keterangan' => $row->keterangan,
						'debit' => $row->debet,
						'kredit' => $row->credit,
						'total_terpakai' => $row->total_terpakai,
						'total_sisa' => $row->total_sisa,
						'keterangan' => $row->keterangan,
						'tgl' => $row->created_at
					);
					array_push($mutasiProyek, $arrayRiwayat);
				}
			}

			$getpendanaanTipe = DB::select("select * from brw_tipe_pendanaan where tipe_id = '" . $pykid->pendanaan_tipe . "'");
			$pendanaanTipe = $getpendanaanTipe[0]->pendanaan_nama;
			$pengajuanId = $pykid->pengajuan_id;
			$getJaminan = DB::Select("SELECT  a.*,b.jenis_jaminan FROM brw_jaminan a LEFT JOIN m_jenis_jaminan b ON a.jaminan_jenis = b.id_jenis_jaminan WHERE a.pengajuan_id = '" . $pengajuanId . "'");
			$data_pendana = DB::select("SELECT a.proyek_id,a.investor_id,b.nama_investor,a.total_dana FROM pendanaan_aktif a JOIN detil_investor b ON a.investor_id = b.investor_id where a.proyek_id = '" . $id . "' AND a.status = 1");

			$hitungpersendana = (($pykid->terkumpul + $body_danaTerkumpul->nominal_awal) / $pykid->total_need) * 100;
			if ($hitungpersendana == 100.0) {
				$persendana = "100%";
			} else {
				$persendana = round($hitungpersendana, 2) . "%";
			}
			$hasil = [
				'namaproyek' => $namaproyek,
				'dataJaminan' => $getJaminan,
				'dataPendana' => $data_pendana,
				'pendanaanTipe' => $pendanaanTipe,
				'mutasiProyek' => $mutasiProyek,
				'tgl_mulai_proyek' => date('d M Y', strtotime($dskPyk->tgl_mulai)),
				'tgl_selesai_proyek' => date('d M Y', strtotime($dskPyk->tgl_selesai)),
				'lokasi_proyek' => $pykid->lokasi_proyek,
				'gambarProyek' => $body_pykGambar,

				'pengajuan' => $pengajuan,
				'verifikasi' => $verifikasi,
				'penggalangandana' => $penggalangandana,
				'ttd' => $ttd,
				'proyekberjalan' => $proyekberjalan,
				'proyekselesai' => $proyekselesai,
				'tc_pengajuan' =>	$tc_pengajuan,
				'icon_pengajuan' =>    $icon_pengajuan,
				'tc_verifikasi' =>    $tc_verifikasi,
				'icon_verifikasi' =>    $icon_verifikasi,
				'tc_penggalangandana' =>    $tc_penggalangandana,
				'icon_penggalangandana' =>    $icon_penggalangandana,
				'tc_ttd' =>	$tc_ttd,
				'icon_ttd' =>	$icon_ttd,
				'tc_proyekberjalan' =>    $tc_proyekberjalan,
				'icon_proyekberjalan' =>    $icon_proyekberjalan,
				'tc_proyekselesai' =>    $tc_proyekselesai,
				'icon_proyekselesai' =>    $icon_proyekselesai,
				'persendana' => $persendana,
				'imbalhasil' => $imbalhasil . " %",
				'danadibutuhkan' => "Rp. " . number_format($danadibutuhkan, 2, ",", "."),
				'durasiproyek' => $durasiproyek . " Bulan",
				'modepembayaran' => $modepembayaran == 1 ? 'Tiap Bulan' : 'Akhir Proyek',
				'metodepembayaran' => $metodepembayaran == 1 ? 'Full' : 'Parsial',
				'danadicairkan' => $danadicairkan,
				'hargapaket' => "Rp. " . number_format($hargapaket, 2, ",", "."),
				'pendanaanakad' => ($pendanaanakad == 1 ? 'Murabahah' : ($pendanaanakad == 2 ? 'Mudharabah' : ($pendanaanakad == 3 ? 'MMQ' : 'IMBT'))),
				'deskripsi' => $dskPyk->deskripsi,
				'status' => $pykid->status_pendanaan,
				// 'status_log' => $body_pykid->status_pendanaan

			];

			$dataRegDigiSign = CheckUserSign::where('brw_id', Auth::guard('borrower')->user()->brw_id)->first();
			$cekRegDigiSign = !empty($dataRegDigiSign) ? $dataRegDigiSign->tgl_aktifasi : '';

			$dataTeks = '';
			$dataTeks = TeksNotifikasi::where('tipe', 1)->first(['teks_notifikasi']);
			if (!empty($dataTeks)) {
				$teks = $dataTeks->teks_notifikasi;
			} else {
				$teks = '';
			}

			$user = Auth::guard('borrower')->user()->brw_id;
			//$rekening = BorrowerRekening::where('brw_id', $user)->first();
			$dataLogAkad = LogAkadDigiSignBorrower::where('brw_id', $user)
				->where('id_proyek', $id)
				->where(\DB::raw('substr(log_akad_digisign_borrower.document_id, 1, 15)'), '=', 'borrowerKontrak')
				->orderBy('id_log_akad_borrower', 'desc')
				->first();

			//$realTotalAset = !empty($rekening) ? number_format($rekening->total_plafon,0,'','') : 0;
			//$logTotalAset = !empty($dataLogAkad) ? $dataLogAkad->total_pendanaan : 0;
			$logStatus = !empty($dataLogAkad) ? $dataLogAkad->status : '';

			if ($logStatus != '') {
				if ($logStatus == 'kirim') {
					$showKontrak = 'ttd_akhir';
				} elseif ($logStatus == 'waiting' || $logStatus == 'Completed') {

					$showKontrak = 'unduh';
				}
			} else {
				$showKontrak = 'ttd_awal';
			}

			return view('pages.borrower.detail_pendanaan', compact('showKontrak', 'teks', 'statusproyek', 'id', 'brwId', 'statusLogSP3', 'cekRegDigiSign'))->with($hasil);
		} else {
			return redirect()->back();
		}
	}



	public function detailPendanaanRumah($pengajuan_id)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		try {
			$pengajuan_id = Crypt::decrypt($pengajuan_id);
		} catch (DecryptException $e) {
			return redirect('/borrower/pendanaan')->with('error', 'Maaf parameter tidak valid.');
		}
		
		$pengajuan = \DB::table('brw_pengajuan')
			->leftjoin('brw_agunan', 'brw_agunan.id_pengajuan', 'brw_pengajuan.pengajuan_id')
			->leftjoin('brw_dokumen_objek_pendanaan', 'brw_dokumen_objek_pendanaan.id_pengajuan', 'brw_pengajuan.pengajuan_id')
			->leftjoin('brw_dokumen_legalitas_pribadi', 'brw_dokumen_legalitas_pribadi.brw_id', 'brw_pengajuan.brw_id')
			->leftjoin('brw_dokumen_legalitas_badan_hukum', 'brw_dokumen_legalitas_badan_hukum.brw_id', 'brw_pengajuan.brw_id')
			->leftjoin('brw_verifikasi_awal', 'brw_verifikasi_awal.pengajuan_id', 'brw_pengajuan.pengajuan_id')
			->join('brw_user_detail_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_user_detail_pengajuan.pengajuan_id')
			->where('brw_pengajuan.pengajuan_id', $pengajuan_id)
			->where('brw_pengajuan.brw_id', $brw_id)
			->select(['brw_pengajuan.*', 'brw_agunan.*', 'brw_dokumen_legalitas_badan_hukum.*', 'brw_dokumen_objek_pendanaan.*', 'brw_dokumen_legalitas_pribadi.*', 'brw_verifikasi_awal.catatan', 'brw_user_detail_pengajuan.brw_pic_ktp', 'brw_user_detail_pengajuan.brw_pic_npwp', 'brw_user_detail_pengajuan.foto_npwp_perusahaan'])
			->first();

		if (!$pengajuan) return redirect('/borrower/pendanaan')->with('error', 'Maaf data pengajuan tidak ditemukan');

		$borrower_penghasilan =  \DB::table('brw_user_detail_penghasilan')->where('brw_id2', $brw_id)->select(['sumber_pengembalian_dana', 'skema_pembiayaan'])->first();
		$status_kawin = \DB::table('brw_user_detail')->where('brw_id', $brw_id)->value('status_kawin');
		$rumah_ke = \DB::table('brw_pendanaan_rumah_lain')->where('pengajuan_id', $pengajuan_id)->count('rumah_ke');


		$pendanaan_nama = \DB::table('brw_tipe_pendanaan')->where('tipe_id', $pengajuan->pendanaan_tipe)->value('pendanaan_nama');
		$tujuan_pendanaan = \DB::table('m_tujuan_pembiayaan')->where('id', $pengajuan->pendanaan_tujuan)->value('tujuan_pembiayaan');
		$count_dana_non_rumah_lain = \DB::table('brw_pendanaan_non_rumah_lain')->where('pengajuan_id', $pengajuan_id)->count();

		$status_tolak = '';
		$pengajuan_awal = '';
		$verifikasi_awal = '';
		$verifikasi_lanjut = '';
		$penggalangan_dana = '';
		$pencairan_dana = '';
		$pendanaan_berjalan = '';
		$pendanaan_selesai = '';
		$status_pengajuan = '';

		// dd($pengajuan);

		switch (true) {
			case ($pengajuan->status == 0):
				$pengajuan_awal = 'complete';
				$widthProgress = "10";
				break;
			case (in_array($pengajuan->status, [1, 2])):
				$pengajuan_awal = 'complete';
				$verifikasi_awal = 'complete';
				$widthProgress = "20";
				$status_tolak  = ($pengajuan->status == '2') ?  'status-tolak' : '';
				break;
			case (in_array($pengajuan->status, [3, 5, 6, 7, 11, 12])):
				$pengajuan_awal = 'complete';
				$verifikasi_awal = 'complete';
				$verifikasi_lanjut = 'complete';
				$widthProgress = "40";
				$status_tolak  = (in_array($pengajuan->status, ['6','12'])) ? 'status-tolak' : '';
				break;
			case ($pengajuan->status == 8):
				$pengajuan_awal = 'complete';
				$verifikasi_awal = 'complete';
				$verifikasi_lanjut = 'complete';
				$penggalangan_dana = 'complete';
				$widthProgress = "50";
				break;
			case ($pengajuan->status == 9):
				$pengajuan_awal = 'complete';
				$verifikasi_awal = 'complete';
				$verifikasi_lanjut = 'complete';
				$penggalangan_dana = 'complete';
				$pencairan_dana = 'complete';
				$widthProgress = "70";
				break;
			case ($pengajuan->status == 10):
				$pengajuan_awal = 'complete';
				$verifikasi_awal = 'complete';
				$verifikasi_lanjut = 'complete';
				$penggalangan_dana = 'complete';
				$pencairan_dana = 'complete';
				$pendanaan_berjalan = 'complete';
				$widthProgress = "80";
				break;
			case ($pengajuan->status == 4):
				$pengajuan_awal = 'complete';
				$verifikasi_awal = 'complete';
				$verifikasi_lanjut = 'complete';
				$penggalangan_dana = 'complete';
				$pencairan_dana = 'complete';
				$pendanaan_berjalan = 'complete';
				$pendanaan_selesai = 'complete';
				$widthProgress = "100";
				break;
		}

		// if (($pengajuan->status >= 1 && $pengajuan->status != '2')) {
		if (($pengajuan->status  == 1)) {
			$status_pengajuan = 'Pengajuan pendanaan anda sudah terverifikasi, silahkan melanjutkan proses ke tahap selanjutnya';
		}else{
			$status_pengajuan = \DB::table('m_status_pengajuan')->where('id_status_pengajuan', $pengajuan->status)->value('keterangan_pengajuan');
		}

		if (!empty($status_tolak)) {
			$status_pengajuan = 'Mohon maaf pengajuan pendanaan anda tidak dapat dipenuhi';
		}

		$kantor_bpn = \DB::table('m_kantor_bpn')->pluck('nama_kantor', 'id');
		$brw_user_detail_pic = \DB::table('brw_user_detail')->where('brw_id', $brw_id)->select(['brw_pic_ktp', 'brw_type', 'brw_pic_npwp'])->first();

		$borrower_pasangan = \DB::table('brw_pasangan')->where('pasangan_id', $brw_id)->first();
		$status_verifikator = BorrowerAnalisaPendanaan::where('pengajuan_id', $pengajuan_id)->value('status_verifikator');
		$resultDocumentPersyaratan =  Helper::getDokumenList($pengajuan);

		$data_dokumen_persyaratan = [];
        foreach($resultDocumentPersyaratan as $val){
            $data_dokumen_persyaratan[$val->page_title][$val->category_title][] = $val;
        }

		$enable_edit = true;
        $brw_verifikator_pengajuan = BorrowerVerifikatorPengajuan::where('pengajuan_id', $pengajuan_id)->first();
        if ($brw_verifikator_pengajuan) {
            $enable_edit = ($brw_verifikator_pengajuan->status_aktifitas === 3) ? false : true;
        }
		

		$hasil = [
			'brw_id'=> $brw_id,
			'title' => $pendanaan_nama . ' - ' . $tujuan_pendanaan,
			'rumah_ke' => ($rumah_ke + 1),
			'pengajuan' => $pengajuan,
			'pengajuan_awal' => $pengajuan_awal,
			'verifikasi_awal' => $verifikasi_awal,
			'verifikasi_lanjut' => $verifikasi_lanjut,
			'penggalangan_dana' => $penggalangan_dana,
			'pencairan_dana' => $pencairan_dana,
			'pendanaan_berjalan' => $pendanaan_berjalan,
			'pendanaan_selesai' => $pendanaan_selesai,
			'sumber_pengembalian_dana' => $borrower_penghasilan->sumber_pengembalian_dana,
			'skema_pembiayaan'=> $borrower_penghasilan->skema_pembiayaan,
			'status_bank_non_rumah_lain' => ($count_dana_non_rumah_lain > 0) ? '1' : '2',
			'status_kawin' => $status_kawin,
			'pendanaanTipe' => $pendanaan_nama,
			'tujuanPendanaan' => $tujuan_pendanaan,
			'status_tolak' => $status_tolak,
			'count_dana_non_rumah_lain' => $count_dana_non_rumah_lain,
			'status_pengajuan' => $status_pengajuan,
			'kantor_bpn' => $kantor_bpn,
			'widthProgress' => $widthProgress,
			'pengajuan_id'=> $pengajuan_id,
			'brw_user_detail_pic'=> $brw_user_detail_pic,
			'borrower_pasangan'=> $borrower_pasangan,
			'status_verifikator'=> $status_verifikator,
			'data_dokumen_persyaratan'=> $data_dokumen_persyaratan,
			'enable_edit' => $enable_edit
		];

		return view('pages.borrower.detail_pendanaan_rumah')->with($hasil);
	}

	public function queryPengajuan($brw_id, $filter_search = '')
	{

		//Pengajuan 
		$query = \DB::table('brw_pengajuan');
		$query->join('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
		$query->leftJoin('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
		$query->select(['brw_pengajuan.pengajuan_id', 'brw_tipe_pendanaan.pendanaan_nama', 'm_tujuan_pembiayaan.tujuan_pembiayaan', 'brw_tipe_pendanaan.tipe_id', 'brw_pengajuan.status']);
		$query->where('brw_id', $brw_id);
		$query->whereIn('brw_pengajuan.pendanaan_tipe', [1, 2, 3]);

		if (!empty($filter_search)) {
			$query->whereRaw(" (brw_pengajuan.pengajuan_id = ? or brw_tipe_pendanaan.pendanaan_nama like ? or m_tujuan_pembiayaan.tujuan_pembiayaan like ?) ", [$filter_search, '%' . $filter_search . '%', '%' . $filter_search . '%']);
		}


		return $query;
	}


	public function listPengajuan($borrower_id, $filter_search = '')
	{


		$listpengajuanBaru = [];
		$listpengajuanVerifikasiAwal = [];
		$listpengajuanVerifikasiLanjut = [];
		$listPenggalanganDana = [];
		$listPencairanDana = [];
		$listPendanaanBerjalan = [];
		$listPendanaanSelesai = [];

		$queryPengajuan = $this->queryPengajuan($borrower_id, $filter_search)->get();

		foreach ($queryPengajuan as  $val) {

			if ($val->status == 0) array_push($listpengajuanBaru, $val);

			if (in_array($val->status, [1, 2])) array_push($listpengajuanVerifikasiAwal, $val);
			if (in_array($val->status, [3, 5, 6, 7, 11, 12])) array_push($listpengajuanVerifikasiLanjut, $val);

			if ($val->status == 8) array_push($listPenggalanganDana, $val);
			if ($val->status == 9) array_push($listPencairanDana, $val);
			if ($val->status == 10) array_push($listPendanaanBerjalan, $val);
			if ($val->status == 4) array_push($listPendanaanSelesai, $val);
		}

		$listPendanaan = [
			'list_pengajuan_baru' => $listpengajuanBaru,
			'list_verifikasi_awal' => $listpengajuanVerifikasiAwal,
			'list_verifikasi_lanjut' => $listpengajuanVerifikasiLanjut,
			'list_penggalangan_dana' => $listPenggalanganDana,
			'list_pencairan_dana' => $listPencairanDana,
			'list_pendanaan_berjalan' => $listPendanaanBerjalan,
			'list_pendanaan_selesai' => $listPendanaanSelesai
		];


		return $listPendanaan;
	}



	public function cari_pendanaan(Request $request)
	{

		try {
			$output = '';
			$borrower_id = Auth::guard('borrower')->user()->brw_id;

			$filter_search = isset($request->txtSearch) && !empty($request->txtSearch) ? $request->txtSearch : '';
			$listPendanaan = $this->listPengajuan($borrower_id, $filter_search);

			foreach ($listPendanaan['list_pengajuan_baru'] as $rowPengajuan) {
				$output .= '<div class="DivPengajuanBaru col-md-4 d-flex flex-row mb-20">
							<div class="p-2 bg-success"><img src="' . asset("img/logoOnly.png") . '"
									style="max-width: 60px;"></div>
							<div class="block-content block-content-full block-content-sm bg-light">
								<span class="font-w500 text-black"> ' . $rowPengajuan->pengajuan_id . ' -
									' . $rowPengajuan->pendanaan_nama . ' -
									' . $rowPengajuan->tujuan_pembiayaan . '</span><br><br>';

				$output .=  '<a href="' . route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) . '">Lihat Detail</a>';
				$output .= '</div><div class="w-25 p-2 bg-secondary">&nbsp;</div>
						</div>';
			}

			foreach ($listPendanaan['list_verifikasi_awal'] as $rowPengajuan) {
			
				$output .= '<div class="DivVerifikasiAwal col-md-4 d-flex flex-row mb-20">';	
				if ($rowPengajuan->status == '1') {
					$output .= '<div class="position-absolute  card-badge"><span
					class="badge badge-success text-white">LOLOS TAHAP VERIFIKASI AWAL</span></div>';

				}elseif($rowPengajuan->status == '2'){
					$output .= '<div class="position-absolute card-badge"><span
					class="badge badge-success text-white">GAGAL TAHAP VERIFIKASI AWAL</span></div>';
				}elseif($rowPengajuan->status == '3'){
					$output .= '<div class="position-absolute card-badge"><span
					class="badge badge-success text-white">PROSES VERIFIKASI</span>
			        </div>';
				}elseif($rowPengajuan->status == '12'){
					$output .= '<div class="position-absolute card-badge"><span
					class="badge badge-success text-white">GAGAL TAHAP 2</span></div>';
				}
			
				$output .= '<div class="p-2 bg-success"><img src="' . asset("img/logoOnly.png") . '"
									style="max-width: 60px;"></div>
							<div class="block-content block-content-full block-content-sm bg-light">
								<span class="font-w500 text-black"> ' . $rowPengajuan->pengajuan_id . ' -
									' . $rowPengajuan->pendanaan_nama . ' -
									' . $rowPengajuan->tujuan_pembiayaan . '</span><br><br>';

			    $output .=  '<a href="' . route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) . '">Lihat Detail</a>';
				$output .= '</div><div class="w-25 p-2" style="background-color: yellow;">&nbsp;</div>
						</div>';
			}


			foreach ($listPendanaan['list_verifikasi_lanjut'] as $rowPengajuan) {

				$output .= '<div class="DivVerifikasiLanjut col-md-4 d-flex flex-row mb-20">';	
				if ($rowPengajuan->status == '1') {
					$output .= '<div class="position-absolute  card-badge"><span
					class="badge badge-success text-white">LOLOS TAHAP VERIFIKASI AWAL</span></div>';

				}elseif($rowPengajuan->status == '2'){
					$output .= '<div class="position-absolute card-badge"><span
					class="badge badge-success text-white">GAGAL TAHAP VERIFIKASI AWAL</span></div>';
				}elseif($rowPengajuan->status == '3'){
					$output .= '<div class="position-absolute card-badge"><span
					class="badge badge-success text-white">PROSES VERIFIKASI</span>
			        </div>';
				}elseif($rowPengajuan->status == '12'){
					$output .= '<div class="position-absolute card-badge"><span
					class="badge badge-success text-white">GAGAL TAHAP VERIFIKASI LANJUTAN</span></div>';
				}

				$output .= '<div class="p-2 bg-success"><img src="' . asset("img/logoOnly.png") . '"
									style="max-width: 60px;"></div>
							<div class="block-content block-content-full block-content-sm bg-light">
								<span class="font-w500 text-black"> ' . $rowPengajuan->pengajuan_id . ' -
									' . $rowPengajuan->pendanaan_nama . ' -
									' . $rowPengajuan->tujuan_pembiayaan . '</span><br><br>';

				$output .=  '<a href="' . route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) . '">Lihat Detail</a>';
				$output .= '</div><div class="w-25 p-2" style="background-color: #f39c12;">&nbsp;</div>
						</div>';
			}



			foreach ($listPendanaan['list_penggalangan_dana'] as $rowPengajuan) {
				$output .= '<div class="DivPenggalanganDana col-md-4 d-flex flex-row mb-20">
							<div class="p-2 bg-success"><img src="' . asset("img/logoOnly.png") . '"
									style="max-width: 60px;"></div>
							<div class="block-content block-content-full block-content-sm bg-light">
								<span class="font-w500 text-black"> ' . $rowPengajuan->pengajuan_id . ' -
									' . $rowPengajuan->pendanaan_nama . ' -
									' . $rowPengajuan->tujuan_pembiayaan . '</span><br><br>';

				$output .=  '<a href="' . route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) . '">Lihat Detail</a>';
				$output .= '</div><div class="w-25 p-2" style="background-color: #3498db;">&nbsp;</div>
						</div>';
			}

			foreach ($listPendanaan['list_pencairan_dana'] as $rowPengajuan) {
				$output .= '<div class="DivPencairanDana col-md-4 d-flex flex-row mb-20">
							<div class="p-2 bg-success"><img src="' . asset("img/logoOnly.png") . '"
									style="max-width: 60px;"></div>
							<div class="block-content block-content-full block-content-sm bg-light">
								<span class="font-w500 text-black"> ' . $rowPengajuan->pengajuan_id . ' -
									' . $rowPengajuan->pendanaan_nama . ' -
									' . $rowPengajuan->tujuan_pembiayaan . '</span><br><br>';

				if ($rowPengajuan->pendanaan_nama == 'Dana Rumah') {
					$output .=  '<a href="' . route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) . '">Lihat Detail</a>';
				} else {

					$output .= '<a href="/borrower/detilProyek/' . $rowPengajuan->pengajuan_id . '">Lihat Detail</a>';
				}

				$output .= '</div><div class="w-25 p-2" style="background-color: #9b59b6;">&nbsp;</div>
						</div>';
			}

			foreach ($listPendanaan['list_pendanaan_berjalan'] as $rowPengajuan) {
				$output .= '<div class="DivPendanaanBerjalan col-md-4 d-flex flex-row mb-20">
							<div class="p-2 bg-success"><img src="' . asset("img/logoOnly.png") . '"
									style="max-width: 60px;"></div>
							<div class="block-content block-content-full block-content-sm bg-light">
								<span class="font-w500 text-black"> ' . $rowPengajuan->pengajuan_id . ' -
									' . $rowPengajuan->pendanaan_nama . ' -
									' . $rowPengajuan->tujuan_pembiayaan . '</span><br><br>';

				$output .=  '<a href="' . route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) . '">Lihat Detail</a>';
				$output .= '</div><div class="w-25 p-2" style="background-color: #55efc4;">&nbsp;</div>
						</div>';
			}



			foreach ($listPendanaan['list_pendanaan_selesai'] as $rowPengajuan) {
				$output .= '<div class="DivPendanaanSelesai col-md-4 d-flex flex-row mb-20">
							<div class="p-2 bg-success"><img src="' . asset("img/logoOnly.png") . '"
									style="max-width: 60px;"></div>
							<div class="block-content block-content-full block-content-sm bg-light">
								<span class="font-w500 text-black"> ' . $rowPengajuan->pengajuan_id . ' -
									' . $rowPengajuan->pendanaan_nama . ' -
									' . $rowPengajuan->tujuan_pembiayaan . '</span><br><br>';

				$output .=  '<a href="' . route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) . '">Lihat Detail</a>';
				$output .= '</div><div class="w-25 p-2 bg-success">&nbsp;</div>
						</div>';
			}


			return $output;
		} catch (\Exception $e) {
			return response(['status_code' => 500, 'errors' => 'Error. Please contact the administrator of website'], 500);
		}
	}


	public function pendanaan()
	{

		$borrower_id = Auth::guard('borrower')->user()->brw_id;
		$listPendanaan = $this->listPengajuan($borrower_id);
		return view('pages.borrower.all_pendanaan_page')->with($listPendanaan);
	}


	public function pendanaanPage()
	{

		$id_user = Auth::guard('borrower')->user()->brw_id;

		// list proyek pengajuan
		$getProyekPengajuan = BorrowerPengajuan::where('brw_id', $id_user)->whereIn('status', [0, 3])
			->get([
				'brw_pengajuan.pengajuan_id',
				'brw_pengajuan.status',
				'brw_pengajuan.pendanaan_nama',
				'brw_pengajuan.gambar_utama'
			]);

		// list proyek pengajuan
		$getProyekDitolak = BorrowerPengajuan::where('brw_id', $id_user)->whereIn('status', [2, 5])
			->get([
				'brw_pengajuan.pengajuan_id',
				'brw_pengajuan.status',
				'brw_pengajuan.pendanaan_nama',
				'brw_pengajuan.gambar_utama'
			]);

		// list proyek terverifikasi
		$getProyekTerverifikasi = BorrowerPendanaan::where('brw_id', $id_user)->where('status', 0)
			->get([
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.status',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.gambar_utama'
			]);

		// list proyek Penggalangan
		$getProyekPenggalangan = BorrowerPendanaan::where('brw_id', $id_user)
			->leftjoin('proyek', 'proyek.id', '=', 'brw_pendanaan.id_proyek')
			->where('brw_pendanaan.status', 1)
			->whereIn('proyek.status', [1, 3])
			->where('proyek.status_tampil', 2)
			->where('proyek.waktu_bagi', 1)
			->get([
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.status as status_pendanaan',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.gambar_utama',
				'proyek.status as status_proyek',
				'proyek.id as proyek_id',
			]);

		// list proyek pencairan
		$getProyekPencairan = BorrowerPendanaan::where('brw_id', $id_user)
			->leftjoin('proyek', 'proyek.id', '=', 'brw_pendanaan.id_proyek')
			->where('brw_pendanaan.status', 6)
			->where('proyek.status_tampil', 1)
			->where('proyek.status', 2)
			->get([
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.status as status_pendanaan',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.gambar_utama',
				'proyek.status as status_proyek',
				'proyek.id as proyek_id',
			]);

		// list proyek berjalan
		$getProyekBerjalan = BorrowerPendanaan::where('brw_id', $id_user)
			->leftjoin('proyek', 'proyek.id', '=', 'brw_pendanaan.id_proyek')
			->where('brw_pendanaan.status', 2)
			->where('proyek.status_tampil', 1)
			->where('proyek.status', 2)
			->get([
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.status as status_pendanaan',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.gambar_utama',
				'proyek.status as status_proyek',
				'proyek.id as proyek_id',
			]);

		// list proyek selesai
		$getProyekSelesai = BorrowerPendanaan::where('brw_id', $id_user)
			->leftjoin('proyek', 'proyek.id', '=', 'brw_pendanaan.id_proyek')
			->where('brw_pendanaan.status', 4)
			->where('proyek.status', 4)
			->get([
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.status as status_pendanaan',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.gambar_utama',
				'proyek.status as status_proyek',
				'proyek.id as proyek_id',
			]);


		$listProyek = [
			'list_pengajuan' => $getProyekPengajuan, 'list_ditolak' => $getProyekDitolak,  'list_aktif' => $getProyekTerverifikasi,
			'list_penggalangan' => $getProyekPenggalangan, 'list_pencairan' => $getProyekPencairan,
			'list_berjalan' => $getProyekBerjalan, 'list_selesai' => $getProyekSelesai
		];

		return view('pages.borrower.all_pendanaan')->with($listProyek);
	}

	public function getProyekbyId(Request $request)
	{
		$client = new client();
		$id = $request->id;

		//detailtotalkeseluruhan
		// $response_pykid = $client->request('GET', config('app.apilink')."/borrower/pykid/".$id);
		// $body_pykid = json_decode($response_pykid->getBody()->getContents());

		$pykid = Proyek::where('proyek.id', $id)
			->join('brw_pendanaan', 'brw_pendanaan.id_proyek', '=', 'proyek.id')
			->first([
				'proyek.*',
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.status as status_pendanaan',
				'brw_pendanaan.status as status_dana',
				'brw_pendanaan.pendanaan_dana_dibutuhkan',
				'brw_pendanaan.mode_pembayaran',
				'brw_pendanaan.metode_pembayaran',
				'brw_pendanaan.dana_dicairkan',
				'brw_pendanaan.pendanaan_akad'
			]);

		// $response_danaTerkumpul = $client->request('GET', config('app.apilink')."/borrower/danaTerkumpul/".$id);
		// $body_danaTerkumpul = json_decode($response_danaTerkumpul->getBody()->getContents());

		$datadanaTerkumpul = BorrowerDanaTerkumpul::where('proyek_id', $id)->sum('nominal_awal');
		$body_danaTerkumpul = array("nominal_awal" => $datadanaTerkumpul);

		return response()->json(['data' => $pykid, 'danaTerkumpul' => $body_danaTerkumpul]);
		//return response()->json(['data' => $body_pykid, 'danaTerkumpul' => $body_danaTerkumpul]);
	}

	public function getlastproyekapproved(Request $request)
	{
		#dana terkumpul dan proyek akatif terakhir borrower
		// $client = new client();
		//   	//data
		//   	$response_glpa = $client->request('GET', config('app.apilink')."/borrower/getlastproyekapp/".$id);
		// $body_glpa = json_decode($response_glpa->getBody());

		// $idpyk = $body_glpa->id_proyek;

		// $response_danaTerkumpul = $client->request('GET', config('app.apilink')."/borrower/danaTerkumpul/".$idpyk);
		//   	$body_danaTerkumpul = json_decode($response_danaTerkumpul->getBody()->getContents());

		$id = $request->id;
		$dataPlafons =  BorrowerRekening::where('brw_id', $id)->first();
		$dataPlafon = !empty($dataPlafons->total_plafon) ? $dataPlafons->total_plafon : '0';
		$dtotal_pinjaman = DB::select("select get_tot_pinjaman($id,'BrwDashPendanaanController.php',609) AS total_pinjaman");
		$total_pinjaman = $dtotal_pinjaman[0]->{'total_pinjaman'};
		$dtotal_pokok =  DB::select("select get_tot_pokok($id,'BrwDashPendanaanController.php',611) AS total_pokok");
		$total_pokok = $dtotal_pokok[0]->{'total_pokok'};
		$dtagihan = DB::select("select get_tagihan_jatuh_tempo($id,'BrwDashPendanaanController.php',613) AS tagihan_jatuh_tempo");
		$tagihan = $dtagihan[0]->{'tagihan_jatuh_tempo'};
		$total_imbal = $total_pinjaman - $total_pokok;

		// $dataPlafon = $dataPlafons[0]->{'brw_id'};

		// return response()->json(['data' => $body_glpa, 'danaTerkumpul' => $body_danaTerkumpul, 'dataPlafon' => $dataPlafon]);
		return response()->json(['dataPlafon' => $dataPlafon, 'total_pinjaman' => $total_pinjaman, 'total_pokok' => $total_pokok, 'tagihan' => $tagihan, 'total_imbal' => $total_imbal]);
	}

	public function add_pendanaan()
	{
		
		return view('pages.borrower.add_pendanaan');
	}
	public function lengkapi_profile_pendanaan_simple()
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		$data_individu = DB::table("brw_user_detail")->where('brw_id', $brw_id)->first();
		$data_rekening = DB::table("brw_rekening")->where('brw_id', $brw_id)->first();
		$data_pasangan = DB::table("brw_pasangan")->where('pasangan_id', $brw_id)->first();
		$data_penghasilan = DB::table('brw_user_detail_penghasilan')->where('brw_id2', $brw_id)->first();
		$data_penghasilan_pasangan = DB::table('brw_pasangan')->where('pasangan_id', $brw_id)->first();

		$brw_pengurus = DB::table("brw_pengurus")->where('brw_id', $brw_id);
		$pengurus_satu = null;
		$pengurus_dua = null;
		if (count($brw_pengurus->get()) >= 2) {
			$pengurus_satu = $brw_pengurus->get()[0];
			$pengurus_dua = $brw_pengurus->get()[1];
		} else if (count($brw_pengurus->get()) == 1) {
			$pengurus_satu = $brw_pengurus->get()[0];
			$pengurus_dua = null;
		}
		return view('pages.borrower.lengkapi_profile_pendanaan_simple', [
			'data_individu' => $data_individu,
			'data_rekening' => $data_rekening,
			'data_pasangan' => $data_pasangan,
			'data_penghasilan' => $data_penghasilan,
			'data_penghasilan_pasangan' => $data_penghasilan_pasangan,
			'data_pengurus_1' => $pengurus_satu,
			'data_pengurus_2' => $pengurus_dua
		]);

	}



	/************** Halaman Tambah Pendanaan KPR **************/
	public function add_pendanaan_kpr()
	{
		return view('pages.borrower.add_pendanaan_kpr');
	}

	public function lengkapi_profile()
	{
		$brw_id = Auth::guard('borrower')->user()->brw_id;
		$data_individu = DB::table("brw_user_detail")->where('brw_id', $brw_id)->first();
		$data_rekening = DB::table("brw_rekening")->where('brw_id', $brw_id)->first();
		$data_pasangan = DB::table("brw_pasangan")->where('pasangan_id', $brw_id)->first();
		$data_penghasilan = DB::table('brw_user_detail_penghasilan')->where('brw_id2', $brw_id)->first();;
		$data_penghasilan_pasangan = DB::table('brw_pasangan')->where('pasangan_id', $brw_id)->first();;
		return view('pages.borrower.lengkapi_profile', [
			'data_individu' => $data_individu,
			'data_rekening' => $data_rekening,
			'data_pasangan' => $data_pasangan,
			'data_penghasilan' => $data_penghasilan,
			'data_penghasilan_pasangan' => $data_penghasilan_pasangan
		]);
		return view('pages.borrower.lengkapi_profile');
	}

	public function view_status_pending()
	{
		return view('pages.borrower.view_pending_brw');
	}

	public function view_status_reject()
	{
		return view('pages.borrower.view_reject_brw');
	}

	public function otpCode(Request $req)
	{
		$client = new client();
		$to = $req->hp;

		$otp = rand(100000, 999999);
		//$text =  'Kode OTP : '.$otp.' Silahkan masukan kode ini untuk melanjutkan proses melengkapi data anda.';
		$text =  'DANASYARIAH-JANGAN MEMBERITAHU KODE INI KE SIAPAPUN termasuk pihak DANASYARIAH Kode OTP : ' . $otp . ' Silahkan masukan kode ini untuk melanjutkan proses melengkapi data anda.';
		$id_user = Auth::guard('borrower')->user()->brw_id;
		//send to db
		$Borrower = Borrower::where('brw_id', Auth::guard('borrower')->user()->brw_id)->update(['otp' => $otp]);

		$pecah              = explode(",", $req->hp);
		$jumlah             = count($pecah);
		// $from               = "SMSVIRO"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
		// $username           = "smsvirodemo";
		// $password           = "qwerty@123";
		$from               = "DANASYARIAH";
		$username           = "danasyariahpremium"; //your smsviro username
		$password           = "Dsi701@2019"; //your smsviro password
		$postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS

		for ($i = 0; $i < $jumlah; $i++) {
			if (substr($pecah[$i], 0, 2) == "62" || substr($pecah[$i], 0, 3) == "+62") {
				$pecah = $pecah;
			} elseif (substr($pecah[$i], 0, 1) == "0") {
				$pecah[$i][0] = "X";
				$pecah = str_replace("X", "62", $pecah);
			} else {
				echo "Invalid mobile number format";
			}
			$destination = array("to" => $pecah[$i]);
			$message     = array(
				"from" => $from,
				"destinations" => $destination,
				"text" => $text,
				"smsCount" => 20
			);
			$postData           = array("messages" => array($message));
			$postDataJson       = json_encode($postData);
			$ch                 = curl_init();
			$header             = array("Content-Type:application/json", "Accept:application/json");

			curl_setopt($ch, CURLOPT_URL, $postUrl);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($ch);
			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$responseBody = json_decode($response);
			curl_close($ch);
		}

		if ($Borrower) {
			$data = ['status' => true, 'message' => 'Silahkan masukan kode ini untuk melanjutkan proses melengkapi data anda.'];
			return response()->json($data);
		} else {
			$data = ['status' => false, 'message' => 'Data Telepon tidak benar.'];
			return response()->json($data);
		}
	}

	public function cekOTP(Request $req)
	{
		$dataOTP = Borrower::where('brw_id', Auth::guard('borrower')->user()->brw_id)->first();
		$otpDB = $dataOTP->otp;
		if ($req->otp == $otpDB) {
			$data = ['status' => '00', 'message' => 'OTP sama'];
			return response()->json($data);
		} else {
			$data = ['status' => '01', 'message' => 'OTP tidak sama'];
			return response()->json($data);
		}
	}

	public function updateSP3(Request $req)
	{
		$getDataPendanaan   = BorrowerPendanaan::where('brw_pendanaan.brw_id', $req->idbrw)
			->where('brw_pendanaan.pendanaan_id', $req->pendanaan_id)
			->first();
		$date = date('Y-m-d');
		$penggalangandate = date('Y-m-d', strtotime($date . "+1 day"));
		$penggalanganselesaidate = date('Y-m-d', strtotime($penggalangandate . "+14 day"));
		$tgl_mulai = date('Y-m-d', strtotime($penggalanganselesaidate . "+1 day"));
		$selisihTenor = $tgl_mulai . "+" . $getDataPendanaan->durasi_proyek . " month";
		$tgl_selesai = date('Y-m-d', strtotime($selisihTenor));








		//insert proyek
		$id = DB::table('proyek')->insertGetId(
			array(
				'nama' => $getDataPendanaan->pendanaan_nama,
				'akad' => (string)$getDataPendanaan->pendanaan_akad,
				'total_need' => $getDataPendanaan->pendanaan_dana_dibutuhkan,
				'tgl_mulai' => $getDataPendanaan->estimasi_mulai,
				'profit_margin' => $getDataPendanaan->estimasi_imbal_hasil - 5,
				'tenor_waktu' => $getDataPendanaan->durasi_proyek,
				'status' => '1',
				'geocode' => $getDataPendanaan->geocode,
				'gambar_utama' => $getDataPendanaan->gambar_utama,
				'tgl_mulai_penggalangan' => $penggalangandate,
				'tgl_selesai_penggalangan' => $penggalanganselesaidate,
				'tgl_mulai' => $tgl_mulai,
				'tgl_selesai' => $tgl_selesai,
				'alamat' => $getDataPendanaan->lokasi_proyek,
				'harga_paket' => 1000000,
				'status_tampil' => 2,
				'waktu_bagi' => 1


			)
		);

		// insert LogSP3Borrower
		DB::table('brw_log_sp3')->insert([
			[
				'no_sp3' => $req->sp3,
				'brw_id' => $req->idbrw,
				'id_proyek' => $id,
				'total_pendanaan' => $getDataPendanaan->pendanaan_dana_dibutuhkan,
				'status' => 2

				// ,
				// 'created_at' => date('Y-m-d h:m:i')
			],
		]);





		//update pendanaan
		$updatePendanaan = BorrowerPendanaan::where('brw_id', $req->idbrw)
			->where('pendanaan_id', $req->pendanaan_id)
			->first();
		$updatePendanaan->status = 1;
		$updatePendanaan->id_proyek = $id;
		$updatePendanaan->save();

		/*kirim email*/
		$getPengguna = DB::Select("SELECT b.nama,a.email FROM brw_user a JOIN brw_user_detail b ON a.brw_id=b.brw_id where a.brw_id = '" . $req->idbrw . "'");

		$nama_pengguna = $getPengguna[0]->{'nama'};
		$pendanaan = $getDataPendanaan->pendanaan_nama;
		$dana_dibutuhkan = $getDataPendanaan->pendanaan_dana_dibutuhkan;
		$imbal_hasil = $getDataPendanaan->estimasi_imbal_hasil - 5;
		$harga_paket = 'Rp. 1.000.000';
		$tgl_mulai_penggalangan = $penggalangandate;
		$tgl_selesai_penggalangan = $penggalanganselesaidate;

		$data_email = array(
			'nama_pengguna' => $nama_pengguna,
			'nama_analis' => 'Analis Proyek',
			'pendanaan' => $pendanaan,
			'dana_dibutuhkan' => "Rp. " . number_format($dana_dibutuhkan),
			'imbal_hasil' => $imbal_hasil,
			'harga_paket' => $harga_paket,
			'tgl_mulai_penggalangan' => date("d F Y", strtotime($tgl_mulai_penggalangan)),
			'tgl_selesai_penggalangan' => date("d F Y", strtotime($tgl_selesai_penggalangan))
		);

		$array_email_to = [];
		$email_to = Admins::select('email')->where('role', '=', 8)->get();
		foreach ($email_to as $data) {
			array_push($array_email_to, $data->email);
		}
		$email = new SetujuiSp3Email($data_email);
		Mail::to($array_email_to)->send($email);

		if ($updatePendanaan) {









			$data = ['status' => '00', 'message' => 'Sukses'];
			return response()->json(['jsonFile' => $data]);
		} else {
			$data = ['status' => '01', 'message' => 'Gagal'];
			return response()->json(['jsonFile' => $data]);
		}
	}

	public function listInvoice($brw_id, $proyek_id)
	{
		$client = new client();
		$list_invoice = $client->request('GET', config('app.apilink') . "/borrower/list_invoice/" . $brw_id) . "/" . $proyek_id;
		$body_list_invoice = json_decode($list_invoice->getBody());

		// $response = json_decode($request->getBody()->getContents());

		// print_r($request->getBody()->getContents());die;
		return response()->json(['data' => $response]);
	}


	public function portofolio_detail()
	{
		$id = Auth::guard('borrower')->user()->brw_id;
		$hasil = ['idbrw' => $id];
		return view('pages.borrower.detail_portofolio')->with($hasil);
	}


	public function portofolio()
	{
		$id = Auth::guard('borrower')->user()->brw_id;
		$hasil = ['idbrw' => $id];


		return view('pages.borrower.portofolio')->with($hasil);
	}

	public function beranda()
	{

		$client = new client();
		$id = Auth::guard('borrower')->user()->brw_id;

		$pendanaanAktif = BorrowerPendanaan::where('brw_pendanaan.status', 2)
			// ->where('brw_pendanaan.id_proyek','=',0)
			->leftJoin('proyek', 'proyek.id', '=', 'brw_pendanaan.id_proyek')
			->where('brw_pendanaan.brw_id', $id)
			->limit(5)
			->orderby('pendanaan_id', 'DESC')
			->get([
				'pendanaan_id',
				'id_proyek',
				'proyek.nama as nama',
				'proyek.tenor_waktu as tenor',
				'proyek.total_need as jumlah_dana',
				'estimasi_imbal_hasil',
				'proyek.tgl_mulai',
				'proyek.tgl_selesai',
				DB::raw("timestampdiff(MONTH,proyek.tgl_mulai,NOW())+1 as bulanberjalan")
			]);

		$sp3 = BorrowerPendanaan::leftjoin('brw_pengajuan', 'brw_pengajuan.pengajuan_id', '=', 'brw_pendanaan.pengajuan_id')
			->where('brw_pendanaan.status', 0)
			->where('brw_pendanaan.id_proyek', '=', 0)
			->where('brw_pendanaan.brw_id', $id)
			->limit(5)
			->orderby('pendanaan_id', 'DESC')
			->get([
				'pendanaan_id',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.durasi_proyek',
				'brw_pendanaan.pendanaan_dana_dibutuhkan as v_dana',
				'brw_pendanaan.estimasi_imbal_hasil as v_imbal',
				'brw_pendanaan.id_proyek',
				'brw_pengajuan.pendanaan_dana_dibutuhkan as p_dana',
				'brw_pengajuan.estimasi_imbal_hasil as p_imbal'
			]);

		$listPendanaan = [];
		$pendanaan = BorrowerPendanaan::leftJoin('proyek', 'brw_pendanaan.id_proyek', '=', 'proyek.id')
			// ->where('brw_pendanaan.id_proyek','!=',0)
			->where('brw_pendanaan.brw_id', $id)
			->limit(5)
			->orderby('pendanaan_id', 'DESC')
			->get([
				'brw_pendanaan.pendanaan_id',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.durasi_proyek',
				'brw_pendanaan.pendanaan_dana_dibutuhkan',
				'brw_pendanaan.estimasi_imbal_hasil',
				'proyek.id as id',
				'proyek.nama',
				'proyek.tenor_waktu',
				'proyek.total_need',
				'proyek.profit_margin',

				'brw_pendanaan.status as status'
			]);
		if ($pendanaan != null) {
			foreach ($pendanaan as $row) {
				if ($row->id == 0) {
					$pendanaanSaya = array(
						'pendanaanId' => $row->pendanaan_id,
						'id' => $row->id,
						'pendanaan_nama' => $row->pendanaan_nama,
						'durasi_proyek' => $row->durasi_proyek,
						'dana_dibutuhkan' => $row->pendanaan_dana_dibutuhkan,
						'estimasi_imbal_hasil' => (int) $row->estimasi_imbal_hasil,
						'status' => $row->status

					);
				} else {
					$pendanaanSaya = array(
						'pendanaanId' => $row->pendanaan_id,
						'id' => $row->id,
						'pendanaan_nama' => $row->nama,
						'durasi_proyek' => $row->tenor_waktu,
						'dana_dibutuhkan' => $row->total_need,
						'estimasi_imbal_hasil' => (int) $row->profit_margin,
						'status' => $row->status

					);
				}

				array_push($listPendanaan, $pendanaanSaya);
			}
		}

		$pengajuan = BorrowerPengajuan::where('brw_pengajuan.brw_id', $id)
			->where('brw_pengajuan.status', '!=', 1)
			->limit(5)
			->orderby('pengajuan_id', 'DESC')
			->get([
				'pengajuan_id',
				'pendanaan_nama',
				'durasi_proyek',
				'pendanaan_dana_dibutuhkan',
				'estimasi_imbal_hasil',
				'status'
			]);

		if ($pengajuan != null) {
			foreach ($pengajuan as $row) {
				$pengajuanSaya = array(
					'pendanaanId' => 1000,
					'id' => $row->pengajuan_id,
					'pendanaan_nama' => $row->pendanaan_nama,
					'durasi_proyek' => $row->durasi_proyek,
					'dana_dibutuhkan' => $row->pendanaan_dana_dibutuhkan,
					'estimasi_imbal_hasil' => (int) $row->estimasi_imbal_hasil,
					'status' => $row->status

				);
				array_push($listPendanaan, $pengajuanSaya);
			}
		}


		$hasil = ['sp3' => $sp3, 'pendanaanAktif' => $pendanaanAktif, 'pendanaan' => $listPendanaan,  'idbrw' => $id];

		return view('pages.borrower.beranda_page')->with($hasil);
		// return view('pages.borrower.beranda')->with($hasil);
	}

	public function sppp($idbrw)
	{
		$hasil = ['idbrw' => $idbrw];
		return view('pages.borrower.sppp')->with($hasil);
	}

	public function sppp_data($idbrw)
	{
		// $sp3 = BorrowerPendanaan::where('brw_pendanaan.status',0)
		// 						->where('brw_pendanaan.id_proyek','=',0)
		// 						->where('brw_pendanaan.brw_id',$idbrw)
		// 						->orderby('pendanaan_id', 'DESC')
		// 						->get([
		// 							'pendanaan_id',
		// 							'pendanaan_nama',
		//                                      'durasi_proyek',
		//                                      'pendanaan_dana_dibutuhkan',
		// 							'estimasi_imbal_hasil',
		// 							'id_proyek'
		// 					]);
		$sp3 = BorrowerPendanaan::leftjoin('brw_pengajuan', 'brw_pengajuan.pengajuan_id', '=', 'brw_pendanaan.pengajuan_id')
			->where('brw_pendanaan.status', 0)
			->where('brw_pendanaan.id_proyek', '=', 0)
			->where('brw_pendanaan.brw_id', $idbrw)
			->orderby('pendanaan_id', 'DESC')
			->get([
				'pendanaan_id',
				'brw_pendanaan.pendanaan_nama',
				'brw_pendanaan.durasi_proyek',
				'brw_pendanaan.pendanaan_dana_dibutuhkan as v_dana',
				'brw_pendanaan.estimasi_imbal_hasil as v_imbal',
				'brw_pendanaan.id_proyek',
				'brw_pengajuan.pendanaan_dana_dibutuhkan as p_dana',
				'brw_pengajuan.estimasi_imbal_hasil as p_imbal'
			]);

		$response = ['data' => $sp3];
		return response()->json($response);
	}

	public function pencairan_dana()
	{
		$idbrw = Auth::guard('borrower')->user()->brw_id;
		$hasil = ['idbrw' => $idbrw];
		return view('pages.borrower.pencairan_dana')->with($hasil);
	}

	public function pencairan_dana_data($idbrw)
	{
		$pencairan_dana = BorrowerPendanaan::leftJoin('proyek', 'brw_pendanaan.id_proyek', '=', 'proyek.id')
			->where('brw_pendanaan.status', 6)
			->where('proyek.status', 2)
			->where('brw_pendanaan.brw_id', $idbrw)
			->orderby('pendanaan_id', 'DESC')
			->get([
				'brw_pendanaan.pendanaan_id',
				'proyek.id',
				'proyek.nama',
				'proyek.tenor_waktu',
				'proyek.total_need',
				'proyek.profit_margin',
				'brw_pendanaan.status'
			]);

		$response = ['data' => $pencairan_dana];
		return response()->json($response);
	}

	public function pembayaran_cicilan()
	{
		date_default_timezone_set("Asia/Jakarta");
		// $date = date('Y-m-d');

		$idbrw = Auth::guard('borrower')->user()->brw_id;
		$hasil = ['idbrw' => $idbrw];
		return view('pages.borrower.pembayaran_cicilan')->with($hasil);
	}

	public function list_pembayaran_cicilan($idbrw)
	{
		$list_proyek = DB::select("SELECT * FROM brw_pencairan a, brw_pendanaan b where a.pendanaan_id = b.pendanaan_id AND a.brw_id = '$idbrw' AND a.status = 2");

		$response = ['data' => $list_proyek];
		return response()->json($response);
	}

	public function list_tagihan($idpencairan)
	{
		$list_tagihan = DB::select("SELECT a.*,(SELECT status FROM brw_bukti_pembayaran WHERE invoice_id = a.invoice_id ORDER BY bukti_id DESC limit 1) as statusBukti,(SELECT ref_no FROM brw_bukti_pembayaran WHERE invoice_id = a.invoice_id ORDER BY bukti_id DESC limit 1) as ref_no,(SELECT tgl_bukti_bayar FROM brw_bukti_pembayaran WHERE invoice_id = a.invoice_id ORDER BY bukti_id DESC limit 1) as tgl_bukti_bayar,(SELECT pic_pembayaran FROM brw_bukti_pembayaran WHERE invoice_id = a.invoice_id ORDER BY bukti_id DESC limit 1) as pic_pembayaran, (SELECT keterangan FROM brw_bukti_pembayaran WHERE invoice_id = a.invoice_id ORDER BY bukti_id DESC limit 1) as ket FROM brw_invoice a  WHERE a.pencairan_id = '" . $idpencairan . "'");


		$response = ['data' => $list_tagihan];
		return response()->json($response);
	}

	public function getGambarBT(Request $request)
	{

		$invoiceId = $request->invoiceId;
		$get_data = DB::select("SELECT * FROM brw_bukti_pembayaran where invoice_id = '" . $invoiceId . "' AND status = 1 ORDER BY bukti_id DESC");
		return json_encode($get_data);
	}

	public function createInvoice($getId)
	{
		$id = explode('-', $getId);
		$pencairanId = $id[0];
		$invId = $id[1];
		$getInvId = $this->getInvoiceId($pencairanId);

		if ($invId == $getInvId) {
			$invoiceId = $getInvId;
			$flag = 1;
		} else {
			$invoiceId = $invId;
			$flag = 0;
		}
		$templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/public/invoice/template/invoice_brw.docx'));

		$idbrw = Auth::guard('borrower')->user()->brw_id;
		$user = DB::table('brw_user_detail')->where('brw_id', $idbrw)->select('nama', 'alamat')->first();
		// $dataBrw = BorrowerPendanaan::join('m_no_akad_borrower','m_no_akad_borrower.proyek_id','=','brw_pendanaan.id_proyek')
		// 		->join('brw_invoice','brw_invoice.pendanaan_id','=','brw_pendanaan.pendanaan_id')
		// 		->where('invoice_id',$invoiceId)
		// 		->select('no_invoice','no_akad_bor','bln_akad_bor','thn_akad_bor','proyek_id','brw_pendanaan.pendanaan_id','nominal_tagihan_perbulan','tgl_jatuh_tempo','durasi_proyek','bulan_ke','nominal_transfer_tagihan')->first();
		$dataBrw = DB::SELECT("SELECT b.no_invoice, a.id_proyek, a.pendanaan_id, b.nominal_tagihan_perbulan, b.tgl_jatuh_tempo, a.durasi_proyek, b.bulan_ke,b.nominal_transfer_tagihan,(SELECT CONCAT(no_akad_bor,'/',bln_akad_bor,'/',thn_akad_bor) FROM m_no_akad_borrower WHERE proyek_id = a.id_proyek AND id_no_akad_bor = (SELECT MAX(id_no_akad_bor) FROM m_no_akad_borrower WHERE proyek_id = a.id_proyek)) AS no_akad,(SELECT SUM(nominal_tagihan_perbulan)-SUM(nominal_transfer_tagihan) FROM brw_invoice WHERE pendanaan_id = a.pendanaan_id AND STATUS IN (3,4) AND DATE_FORMAT(tgl_jatuh_tempo,'%Y%m%d%h%i%s') < DATE_FORMAT(NOW(),'%Y%m%d%h%i%s')) AS tagihan, (SELECT SUM(nominal_transfer_tagihan) FROM brw_invoice WHERE pendanaan_id = a.pendanaan_id AND STATUS IN (3,4) AND DATE_FORMAT(tgl_jatuh_tempo,'%Y%m%d%h%i%s') < DATE_FORMAT(NOW(),'%Y%m%d%h%i%s') ) AS uangmuka, (SELECT (nominal_tagihan_perbulan - nominal_transfer_tagihan) FROM brw_invoice WHERE invoice_id = '" . $invoiceId . "') AS selfTagihan, (SELECT SUM(nominal_tagihan_perbulan) FROM brw_invoice WHERE pendanaan_id = a.pendanaan_id AND STATUS IN (3,4) AND DATE_FORMAT(tgl_jatuh_tempo,'%Y%m%d%h%i%s') < DATE_FORMAT(NOW(),'%Y%m%d%h%i%s')) as sum_nominal_tagihan_perbulan FROM brw_pendanaan a LEFT JOIN brw_invoice b ON a.pendanaan_id = b.pendanaan_id WHERE b.invoice_id = '" . $invoiceId . "'");
		// dd($dataBrw);
		$tgl_sekarang = date('d-n-Y');
		$getnoAkad = $dataBrw[0]->{'no_akad'};
		$noAkadSplit = explode('/', $getnoAkad);
		$no_akad = $noAkadSplit[0];
		$blnakad = $noAkadSplit[1];
		$thnakad = $noAkadSplit[2];
		if (strlen($no_akad) == 1) {
			$NoAkad = '00' . $no_akad;
		} elseif (strlen($no_akad) == 2) {
			$NoAkad = '0' . $no_akad;
		} else {
			$NoAkad = $no_akad;
		}
		$noamrb = $NoAkad . '/DSI/AMRB/' . $blnakad . '/' . $thnakad;
		$no_invoice = $dataBrw[0]->{'no_invoice'};

		$nama = $user->nama;
		$alamat = $user->alamat;
		// if($dataBrw[0]->{'durasi_proyek'} == $dataBrw[0]->{'bulan_ke'}){
		// 	$dataujroh = DB::select("select nominal_imbal_hasil from brw_pencairan where brw_id = $idbrw and pendanaan_id = $dataBrw[0]->{'pendanaan_id'}"); 
		// 	// $ujroh = $dataujroh[0]->{'nominal_imbal_hasil'};
		// 	// $nominal_tagihan_perbulan = $dataBrw[0]->{'tagihan'};
		// }else{
		// 	$ujroh = 0;
		// 	// $nominal_tagihan_perbulan = $dataBrw[0]->{'tagihan'};
		// }

		$biaya_admin = 0;
		$tgl_jatuh_tempo = date('d-n-Y', strtotime($dataBrw[0]->{'tgl_jatuh_tempo'}));
		$nominal_uang_muka = $dataBrw[0]->{'uangmuka'};

		// $sum_total = $nominal_tagihan_perbulan+$ujroh+$biaya_admin;
		// $sum_total = $dataBrw[0]->{'tagihan'}+$ujroh+$biaya_admin;
		if ($flag == 1) {
			$nominal_tagihan_perbulan = $dataBrw[0]->{'nominal_tagihan_perbulan'};
			$sum_total = $dataBrw[0]->{'nominal_tagihan_perbulan'} + $biaya_admin;
			$totaltagihan = $dataBrw[0]->{'selfTagihan'};
			// $totaltagihan = $dataBrw[0]->{'tagihan'};
		} else {
			$nominal_tagihan_perbulan = $dataBrw[0]->{'sum_nominal_tagihan_perbulan'};
			$sum_total = $dataBrw[0]->{'sum_nominal_tagihan_perbulan'} + $biaya_admin;
			$totaltagihan = $dataBrw[0]->{'tagihan'};
		}



		if (strlen($no_akad) == 1) {
			$NoAkad = '00' . $no_akad;
		} elseif (strlen($no_akad) == 2) {
			$NoAkad = '0' . $no_akad;
		} else {
			$NoAkad = $no_akad;
		}
		$terbilangData = DB::select("SELECT Terbilang(" . $totaltagihan . ") as terbilang");
		$terbilang = $terbilangData[0]->{'terbilang'} . 'Rupiah';


		$templateProcessor->setValue(
			[
				'invoice_id',
				'tgl_sekarang',
				'no_akad',
				'nama',
				'alamat',
				'nominal_tagihan_perbulan',
				// 'ujroh',
				'biaya_admin',
				'sum_total',
				'tgl_jatuh_tempo',
				'terbilang',
				'nominal_uang_muka',
				'totalminuangmuka'
			],
		
				$no_invoice,
				$tgl_sekarang,
				$noamrb,
				$nama,
				$alamat,
				"Rp. " . number_format($nominal_tagihan_perbulan),
				// "Rp. ".number_format($ujroh),
				"Rp. " . number_format($biaya_admin),
				"Rp. " . number_format($sum_total),
				$tgl_jatuh_tempo,
				$terbilang,
				"Rp. " . number_format($nominal_uang_muka),
				"Rp. " . number_format($totaltagihan)
			
		);


		$tenor = $dataBrw[0]->{'durasi_proyek'};
		$noInvget = DB::select("SELECT invoice_id,no_invoice,nominal_transfer_tagihan FROM brw_invoice WHERE pencairan_id = '" . $pencairanId . "' AND status IN (1,3)");
		$getSk = DB::select("SELECT sum(nominal_pencairan + nominal_imbal_hasil) as SK FROM brw_pencairan WHERE pencairan_id = '" . $pencairanId . "'");
		$SKget = $getSk[0]->{'SK'};
		$bulan = [0];
		$nomorinvoice = ['-'];
		$noref = ['-'];
		$tglbyr = ['-'];
		$nominalbyr = ['Rp. 0,-'];
		$totalnom = ['RP. ' . number_format($SKget)];
		$noo = 1;


		if (!empty($noInvget)) {
			foreach ($noInvget as $row) {
				$getInvNo = $row->no_invoice;
				$nomBayar = $row->nominal_transfer_tagihan;
				$getNomByr = 'Rp. ' . number_format($nomBayar);



				$RefNoGet = DB::select("SELECT ref_no, tgl_bukti_bayar FROM brw_bukti_pembayaran WHERE invoice_id = '" . $row->invoice_id . "' AND bukti_id = (SELECT max(bukti_id) FROM brw_bukti_pembayaran WHERE invoice_id = '" . $row->invoice_id . "')");
				$getRefNo = $RefNoGet[0]->{'ref_no'};
				$gettglBbayar = $RefNoGet[0]->{'tgl_bukti_bayar'};
				$SKget = $SKget - $nomBayar;
				array_push($nomorinvoice, $getInvNo);
				array_push($noref, $getRefNo);
				array_push($bulan, $noo++);
				array_push($tglbyr, $gettglBbayar);
				array_push($nominalbyr, $getNomByr);
				array_push($totalnom, 'Rp. ' . number_format($SKget));
			}
		}

		if (count($bulan) != 13) {
			$ulang = 13 - count($bulan);
			for ($a = 1; $a <= $ulang; $a++) {
				array_push($bulan, " ");
			}
		}

		if (count($nomorinvoice) < 13) {
			$ulang = 13 - count($nomorinvoice);
			for ($a = 1; $a <= $ulang; $a++) {
				array_push($nomorinvoice, " ");
				array_push($bulan, " ");
				array_push($tglbyr, " ");
				array_push($nominalbyr, " ");
				array_push($totalnom, " ");
			}
		}

		if (count($noref) < 13) {
			$ulang = 13 - count($noref);
			for ($a = 1; $a <= $ulang; $a++) {
				array_push($noref, " ");
			}
		}
		// dd($nomorinvoice);
		// dd($bulan);

		// foreach ($tenor as $row) {

		//     $bulanGet = $noo++;
		//     array_push($bulan, $bulanGet);
		// }
		// if(count($bulan) < 12){
		//     $ulang = 12-count($bulan);
		//     for($i=1;$i<=$ulang;$i++){
		//         array_push($bulan," ");
		//     }    
		// }

		$templateProcessor->setValue(['bulan0', 'nomorinvoice0', 'noref0', 'tglbyr0', 'nominalbyr0', 'totalnom0'], [$bulan[0], $nomorinvoice[0], $noref[0], $tglbyr[0], $nominalbyr[0], $totalnom[0]]);
		$templateProcessor->setValue(['bulan1', 'nomorinvoice1', 'noref1', 'tglbyr1', 'nominalbyr1', 'totalnom1'], [$bulan[1], $nomorinvoice[1], $noref[1], $tglbyr[1], $nominalbyr[1], $totalnom[1]]);
		$templateProcessor->setValue(['bulan2', 'nomorinvoice2', 'noref2', 'tglbyr2', 'nominalbyr2', 'totalnom2'], [$bulan[2], $nomorinvoice[2], $noref[2], $tglbyr[2], $nominalbyr[2], $totalnom[2]]);
		$templateProcessor->setValue(['bulan3', 'nomorinvoice3', 'noref3', 'tglbyr3', 'nominalbyr3', 'totalnom3'], [$bulan[3], $nomorinvoice[3], $noref[3], $tglbyr[3], $nominalbyr[3], $totalnom[3]]);
		$templateProcessor->setValue(['bulan4', 'nomorinvoice4', 'noref4', 'tglbyr4', 'nominalbyr4', 'totalnom4'], [$bulan[4], $nomorinvoice[4], $noref[4], $tglbyr[4], $nominalbyr[4], $totalnom[4]]);
		$templateProcessor->setValue(['bulan5', 'nomorinvoice5', 'noref5', 'tglbyr5', 'nominalbyr5', 'totalnom5'], [$bulan[5], $nomorinvoice[5], $noref[5], $tglbyr[5], $nominalbyr[5], $totalnom[5]]);
		$templateProcessor->setValue(['bulan6', 'nomorinvoice6', 'noref6', 'tglbyr6', 'nominalbyr6', 'totalnom6'], [$bulan[6], $nomorinvoice[6], $noref[6], $tglbyr[6], $nominalbyr[6], $totalnom[6]]);
		$templateProcessor->setValue(['bulan7', 'nomorinvoice7', 'noref7', 'tglbyr7', 'nominalbyr7', 'totalnom7'], [$bulan[7], $nomorinvoice[7], $noref[7], $tglbyr[7], $nominalbyr[7], $totalnom[7]]);
		$templateProcessor->setValue(['bulan8', 'nomorinvoice8', 'noref8', 'tglbyr8', 'nominalbyr8', 'totalnom8'], [$bulan[8], $nomorinvoice[8], $noref[8], $tglbyr[8], $nominalbyr[8], $totalnom[8]]);
		$templateProcessor->setValue(['bulan9', 'nomorinvoice9', 'noref9', 'tglbyr9', 'nominalbyr9', 'totalnom9'], [$bulan[9], $nomorinvoice[9], $noref[9], $tglbyr[9], $nominalbyr[9], $totalnom[9]]);
		$templateProcessor->setValue(
			['bulan10', 'nomorinvoice10', 'noref10', 'tglbyr10', 'nominalbyr10', 'totalnom10'],
			[$bulan[10], $nomorinvoice[10], $noref[10], $tglbyr[10], $nominalbyr[10], $totalnom[10]]
		);
		$templateProcessor->setValue(
			['bulan11', 'nomorinvoice11', 'noref11', 'tglbyr11', 'nominalbyr11', 'totalnom11'],
			[$bulan[11], $nomorinvoice[11], $noref[11], $tglbyr[11], $nominalbyr[11], $totalnom[11]]
		);
		$templateProcessor->setValue(
			['bulan12', 'nomorinvoice12', 'noref12', 'tglbyr12', 'nominalbyr12', 'totalnom12'],
			[$bulan[12], $nomorinvoice[12], $noref[12], $tglbyr[12], $nominalbyr[12], $totalnom[12]]
		);


		Storage::disk('public')->makeDirectory('invoice/hasil/' . $invoiceId);
		$templateProcessor->saveAs(storage_path('app/public/invoice/hasil/' . $invoiceId . '/invoice_brw_' . $invoiceId . '.docx'));
		shell_exec('unoconv -f pdf ' . base_path('storage/app/public/invoice/hasil/' . $invoiceId . '/invoice_brw_' . $invoiceId . '.docx') . ' ' . base_path('storage/app/public/invoice/hasil/' . $invoiceId));


		$response = ['data' => $invoiceId];
		return response()->json($response);
	}

	public function getInvoiceId($pencairanId)
	{
		$getInvoiceLastId = DB::select("SELECT MIN(invoice_id) as invoiceId FROM brw_invoice WHERE pencairan_id = " . $pencairanId . " AND STATUS IN (3,5,4) limit 1");
		return $getInvoiceLastId[0]->{'invoiceId'};
	}

	public function uploadBT(Request $request)
	{
		$idBrw = $request->idBrw;
		$pencairanId = $request->pencairanId;
		$invoiceId = $this->getInvoiceId($pencairanId);

		if ($request->hasFile('file_name')) {
			$file = $request->file('file_name');
			$filename = 'Bukti_trf_' . Carbon::now()->format('Ymd-his') . '.' . $file->getClientOriginalExtension();
			// save nama file berdasarkan tanggal upload+nama file
			// $store_path = 'invoice/buktibayar'.$brw_id;
			$store_path = 'invoice/buktibayar/' . $idBrw . '/' . $invoiceId;
			$path = $file->storeAs($store_path, $filename, 'public');
			if ($path) {
				return response()->json([
					'Sukses' => 'Berhasil di upload',
					'filename' => $filename
				]);
			} else {
				return response()->json([
					'failed' => 'File gagal di upload'
				]);
			}
			// save gambar yang di upload di public storage
		} else {
			return response()->json([
				'failed' => 'File gagal di upload'
			]);
		}
	}

	public function updateBT(Request $request)
	{
		$idBrw = $request->idBrw;
		$invoiceId = $this->getInvoiceId($request->pencairanId);

		$file_name = $request->file_name;
		$refealnumb = $request->refealnumb;
		$brw_invoice = DB::table("brw_invoice")
			->where('invoice_id', $invoiceId)
			->where('brw_id', $idBrw);
		$select_brw_invoice = $brw_invoice->select('pendanaan_id', 'status', 'keterangan', 'no_invoice')->first();
		$update_brw_invoice	= $brw_invoice->update(array('status' => 5, 'updated_at' => date('Y-m-d h:m:s')));
		// var_dump($select_brw_invoice->pendanaan_id);die();
		$brw_bukti_pembayaran = DB::table('brw_bukti_pembayaran')->insert(array(
			'invoice_id' => $invoiceId,
			'brw_id' => $idBrw,
			'pendanaan_id' => $select_brw_invoice->pendanaan_id,
			'pic_pembayaran' => $idBrw . "/" . $invoiceId . "/" . $file_name,
			'status' => 5,
			'ref_no' => $refealnumb,
			'tgl_bukti_bayar' => date('Y-m-d h:m:s'),
			'created_at' => date('Y-m-d h:m:s'),
			'updated_at' => date('Y-m-d h:m:s')
		));

		$brw_log_invoice = DB::table('brw_log_invoice')->insert(array(
			'invoice_id' => $invoiceId,
			'brw_id' => $idBrw,
			'pendanaan_id' => $select_brw_invoice->pendanaan_id,
			'status' => 5,
			'keterangan' => $select_brw_invoice->keterangan,
			'created_at' => date('Y-m-d h:m:s'),
			'updated_at' => date('Y-m-d h:m:s')
		));
		if ($brw_log_invoice) {
			$response = ['status' => '1'];
		} else {
			$response = ['status' => '0'];
		}

		// email notif to analis
		$getDataEmail = DB::select("SELECT a.pendanaan_id,a.pendanaan_nama, b.nama,(SELECT tgl_bukti_bayar FROM brw_bukti_pembayaran WHERE invoice_id = '" . $invoiceId . "' AND pendanaan_id = a.pendanaan_id AND bukti_id = (SELECT max(bukti_id) FROM brw_bukti_pembayaran WHERE invoice_id = '" . $invoiceId . "' AND pendanaan_id = a.pendanaan_id)) AS  tgl_bayar,(SELECT nominal FROM brw_bukti_pembayaran WHERE invoice_id = '" . $invoiceId . "' AND pendanaan_id = a.pendanaan_id AND bukti_id = (SELECT max(bukti_id) FROM brw_bukti_pembayaran WHERE invoice_id = '" . $invoiceId . "' AND pendanaan_id = a.pendanaan_id)) AS  nominal FROM brw_pendanaan a, brw_user_detail b WHERE a.brw_id = b.brw_id AND a.pendanaan_id = '" . $select_brw_invoice->pendanaan_id . "'");

		$data_email = array(
			'nama_analis'	=> 'Analis',
			'no_invoice'	=> $select_brw_invoice->no_invoice,
			'pendanaan'	=> $getDataEmail[0]->{'pendanaan_nama'},
			'nama_pengguna'	=> $getDataEmail[0]->{'nama'},
			'tgl_bayar'	=> $getDataEmail[0]->{'tgl_bayar'}
			// 'nominal_bayar'	=> 
		);

		// email tujuan
		$array_email_to = [];
		$email_to = Admins::select('email')->where('role', '=', 8)->get();
		foreach ($email_to as $data) {
			array_push($array_email_to, $data->email);
		}

		$email = new NotifPembayaranCicilanEmail($data_email);
		Mail::to($array_email_to)->send($email);

		return response()->json($response);
	}

	public function dinvoice($brwid, $pencairanId)
	{
		$invoiceId = $this->getInvoiceId($pencairanId);
		$file = "../storage/app/public/invoice/hasil/" . $invoiceId . "/invoice_brw_" . $invoiceId . ".pdf";
		$headers = array(
			'Content-Type: application/pdf',
		);
		return response()->download($file, 'invoice-doc.pdf', $headers);
	}

	public function get_id_bang()
	{

		$result = DB::table('ih_list_imbal_user as b')->select(
			DB::raw('CASE WHEN SUBSTR(proyek.tgl_mulai,6,2) = SUBSTR(b.tanggal_payout,6,2)-1 THEN ih_detil_imbal_user.proposional+b.imbal_payout ELSE b.imbal_payout END as total_payout'),
			DB::raw('CONCAT("",detil_investor.rekening) as rekening'),
			'm_bank.kode_bank',
			'm_bank.nama_bank',
			'detil_investor.warganegara',
			'detil_investor.nama_pemilik_rek',
			'investor.email',
			DB::raw('CASE WHEN b.keterangan_payout = 1 THEN CONCAT("IH ",proyek.nama) WHEN b.keterangan_payout = 2 THEN CONCAT("Sisa IH ",proyek.nama) END AS keterangan')
		)
			->rightJoin('ih_detil_imbal_user', 'b.detilimbaluser_id', '=', 'ih_detil_imbal_user.id')
			->rightJoin('pendanaan_aktif', 'ih_detil_imbal_user.pendanaan_id', '=', 'pendanaan_aktif.id')
			->rightJoin('proyek', 'ih_detil_imbal_user.proyek_id', '=', 'proyek.id')
			->leftJoin('detil_investor', 'pendanaan_aktif.investor_id', '=', 'detil_investor.investor_id')
			->leftJoin('m_bank', 'detil_investor.bank_investor', '=', 'm_bank.kode_bank')
			->leftJoin('investor', 'pendanaan_aktif.investor_id', '=', 'investor.id')
			->where('b.tanggal_payout', '2021-03-08')
			->where('pendanaan_aktif.status', 1)
			->wherein('b.keterangan_payout', [1, 2])
			->orderBy('ih_detil_imbal_user.proyek_id', 'ASC')
			->orderBy('ih_detil_imbal_user.id', 'ASC')->get();

		$fileText = "";


		foreach ($result as $r) {

			$rekening = $r->rekening;
			$sender = "PT. DANA SYARIAH INDONESIA";
			$bene_name = $r->nama_pemilik_rek;
			$berita_transfer = $r->keterangan;
			$amount_transfer = $r->total_payout;
			$bene_bank_code = $r->kode_bank;
			$bene_bank_code_SKN = $r->kode_bank; // $r->kode_bank_SKN_BSI;
			$bene_bank_name = $r->nama_bank;
			if ($r->warganegara == "0")
				$bene_citizenship = 1;
			else
				$bene_citizenship = 2;
			$ref = $r->keterangan;
			if ($bene_bank_code == "451")
				$is_SKN = "N";
			else
				$is_SKN = "Y";


			$fileText .= sprintf("%-30s", $rekening);
			$fileText .= sprintf("%-30s", $sender);
			$fileText .= sprintf("%-30s", $bene_name);
			$fileText .= sprintf("%-30s", $berita_transfer);
			$fileText .= sprintf("%012ld", $amount_transfer);
			$fileText .= sprintf("%03d", $bene_bank_code);
			$fileText .= sprintf("%8s", $bene_bank_code_SKN);
			$fileText .= sprintf("%-30s11", $bene_bank_name);
			$fileText .= sprintf("%1s", $bene_citizenship);
			$fileText .= sprintf("%-20s", $ref);
			$fileText .= sprintf("%s\r\n", $is_SKN);
		}
		$myName = "BSINet_payoutexport_.txt";
		$headers = [
			'Content-type' => 'text/plain',   'Content-Disposition' => sprintf('attachment; filename="%s"', $myName),
			'Content-Length' => strlen($fileText)
		];
		return Response::make($fileText, 200, $headers);
	}

	public function plafon()
	{
		$plafonBorrower 	= DB::table('brw_rekening')->where('brw_id', Auth::guard('borrower')->user()->brw_id)->first();
		$Plafon     		= $plafonBorrower->total_plafon;
		$Plafon_sisa     	= $plafonBorrower->total_sisa;
		$Plafon_terpakai    = $plafonBorrower->total_terpakai;
		$sumDanaPendanaan 	= 250000000;
		//$sumDanaPendanaan = DB::table("pendanaan_aktif")->where('proyek_id', $dataLogMurobahahBorrower->id_proyek)->get()->sum("total_dana");


		$updatePlafonSisa 			= $Plafon_sisa == 0 ?  $sumDanaPendanaan : $Plafon_sisa - $sumDanaPendanaan;
		$updatePlafonTerpakai 		= $sumDanaPendanaan + $Plafon_terpakai;

		return $Plafon . '|' . $updatePlafonTerpakai . '|' . $updatePlafonSisa;
	}

	public function getNotifPendanaan($brw_id)
	{

		$status_user = DB::table('brw_user')->where('brw_id', $brw_id)->get()[0]->status;

		$data = DB::table('brw_pengajuan as a')
			->where('a.brw_id', $brw_id)
			->where('a.status', '1') //status pengajuan lolos
			->count();

		$response = [
			'status' => 'success',
			'msg' => 'Data berhasil disimpan',
			'data' => [
				'total_pengajuan' => $data,
				'status_user' => $status_user
			]
		];

		return $response;
	}

	public function getListPendanaan($brw_id)
	{
		
		$data = DB::table('brw_pengajuan as a')
			->leftjoin('brw_tipe_pendanaan as b', 'b.tipe_id', 'a.pendanaan_tipe')
			->leftjoin('m_tujuan_pembiayaan as c', 'c.id', 'a.pendanaan_tujuan')
			->select([
				'a.*',
				'b.pendanaan_nama',
				'c.tujuan_pembiayaan'
			])
			->where('a.brw_id', $brw_id)
			->whereIn('a.status', [0, 1, 3, 11, 5])
			->get();


		$danaKonstruksi = BorrowerTipePendanaan::DANA_KONSTRUKSI;		
	
		return \DataTables::of($data)
			->addColumn('detail_pendanaan', function ($data) use ($danaKonstruksi) {
				$pengajuan_id = $data->pengajuan_id;
				$en_pengajuan_id = Crypt::encrypt($pengajuan_id);
				$detail_route = route('detail_pendanaan_rumah', ['pengajuan_id' => $en_pengajuan_id]);
				$btn = '<a href="' . $detail_route . '" class="btn btn-sm bg-gradient-hijau text-white" id="detailPendanaanSaya">Lihat Detil</a>';

				if($data->pendanaan_tipe === $danaKonstruksi) {
					$btn .= '<a href="'.route('borrower.rab.detail', $pengajuan_id).'" class="btn btn-sm btn-secondary text-black mt-10"> RAB </a>';
				} 
				
				return $btn;
			})
			->rawColumns(['detail_pendanaan'])

			->make(true);
	}


	public function detailPageMaterialOrders($pengajuanId)
    {
		
		$pengajuan = $this->checkPengajuanDetail($pengajuanId);
		if(!$pengajuan) return redirect('/borrower/beranda')->with('error','Pengajuan anda tidak ditemukan');
		
        $queryMaterialItems = MaterialOrdersController::queryMaterialItemList();
        $requisitionHeader  = $queryMaterialItems->orderBy('brw_material_requisition_dtl.task_id', 'asc')
                        ->orderBy('creation_date', 'asc')
                        ->where('brw_material_requisition_hdr.pengajuan_id', $pengajuanId)->get();

        $countItems = BorrowerCartMaterialOrder::where('pengajuan_id', $pengajuanId)->count();                    
        return view('pages.borrower.rab.detail_rab', compact('pengajuan', 'requisitionHeader', 'countItems'));
    }


	public function detailPageAddProducts($pengajuanId)
    {

		$pengajuan = $this->checkPengajuanDetail($pengajuanId);
		if(!$pengajuan) return redirect('/borrower/beranda')->with('error','Pengajuan anda tidak ditemukan');
		
        $listCategory = MaterialOrderService::productCategories();

        if (count($listCategory['data']) > 0) {
            $categories[''] = 'Pilih';
            foreach ($listCategory['data'] as $key => $val) {
                $categories[$val['id']] = $val['name'];
            }
        }

        $materialTaskList = BorrowerMaterialTask::pluck('task_description', 'task_id');
        $countItems = BorrowerCartMaterialOrder::where('pengajuan_id', $pengajuanId)->count();

        return view('pages.borrower.rab.add_products', compact('pengajuanId', 'categories', 'materialTaskList', 'countItems'));
    }

	public function addToCart(Request $request){
        return MaterialOrderService::addToCart($request);
    }

	public function detailCartPage($pengajuanId){
		$pengajuan = $this->checkPengajuanDetail($pengajuanId);
		if(!$pengajuan) return redirect('/borrower/beranda')->with('error','Pengajuan anda tidak ditemukan');
        return view('pages.borrower.rab.cart_products', compact('pengajuanId'));
    }

	public function detailCheckoutPage($pengajuanId){
        $pengajuan = BorrowerPengajuan::where('pengajuan_id', $pengajuanId)->first();
        $cartOrder = DB::table('brw_cart_material_order')->select(DB::raw('SUM(qty * price) as total_belanja'))->where('pengajuan_id', $pengajuanId)->first();

        $materialHeader = BorrowerMaterialRequisitionHeader::where('pengajuan_id', $pengajuanId)->first();

        return view('pages.borrower.rab.checkout', compact('pengajuan', 'cartOrder', 'materialHeader'));
    }

	public function checkPengajuanDetail($pengajuanId){
		$query = MaterialOrdersController::queryMaterialOrders();
        $query->where('brw_pengajuan.pengajuan_id', $pengajuanId);
		$query->where('brw_pengajuan.brw_id', Auth::guard('borrower')->user()->brw_id);
		$query->where('brw_pengajuan.pendanaan_tipe', 1); //dana konstruksi
        return $query->orderBy('brw_pengajuan.created_at', 'desc')->first();
	}



}
