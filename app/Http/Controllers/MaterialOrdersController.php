<?php
// privy
namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\BorrowerPengajuan;
use Illuminate\Http\Request;
use App\BorrowerMaterialTask;
use App\Services\AiqqonService;
use App\BorrowerCartMaterialOrder;
use Illuminate\Support\Facades\DB;
use App\BorrowerMaterialRequisitionDetail;
use App\BorrowerMaterialRequisitionHeader;
use Auth;

use App\Services\MaterialOrderService;

class MaterialOrdersController extends Controller
{

    public function __construct()
    {

        //$this->middleware('auth:api-providers',['only' => ['callbackResponse']]);
        $this->middleware('auth:admin')->only(['listRab', 'detailPageMaterialOrders', 'detailPageAddProducts', 'detailCartPage', 'detailCheckoutPage']);
    }

    public function listConstructionTask()
    {
        $getConstructionTask = DB::select("SELECT *
                                             FROM brw_material_task
                                            WHERE enabled_flag = 'Y'");

        return json_encode($getConstructionTask);
    }

    public function listKategoriBarang(Request $request)
    {
        return response(AiqqonService::call("v2/list-category-product"));
    }

    public function listProduk(Request $request)
    {
        return response(AiqqonService::call("v3/list-product", $request->only('offset', 'limit', 'category_id', 'name', 'product_id')));
    }

    public function listProdukPerCategory(Request $request)
    {
        return response(AiqqonService::call("v2/list-product-by-category"));
    }

    /*public function purchaseRequisition () {
        //
    }*/

    //public function historyTransactions ($start_date, $end_date, $limit, $offset) {
    public function historyTransactions(Request $requestParam)
    {
        //public function historyTransactions () {
        $response = AiqqonService::getVendor();
        $response2 = json_decode($response);

        $token = $response2->token;
        $client_key = $response2->client_key;
        $merchant_id = $response2->merchant_id;
        $endpoint_url = $response2->endpoint_url;

        $client = new Client();

        $data = $requestParam->all();
        $start_date = (string) $data['start_date'];
        $end_date = (string) $data['end_date'];
        $limit = (int) $data['limit'];
        $offset = (int) $data['offset'];

        $request = $client->post($endpoint_url . '/v2/transaction/list', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Client-Key' => $client_key,
                'Authorization' => 'Bearer ' . $token,
            ],
            'body' => '{
                "start_date" : "' . $start_date . '",
                "end_date"   : "' . $end_date . '",
                "limit"      : "' . $limit . '",
                "offset"     : "' . $offset . '"
            }',
        ]);

        /*$request = $client->post($endpoint_url . '/v2/transaction/list', [
            'headers' => [
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'Client-Key'    => $client_key,
                'Authorization' => 'Bearer ' . $token
            ],
            'body' => '{
                "start_date" : "2018-07-04",
                "end_date"   : "2022-08-25",
                "limit"      : "1",
                "offset"     : "0"
            }'
        ]);*/

        $response = $request->getBody()->getContents();

        $json_encode = json_encode($response);

        return $response;
    }

    public function listRab()
    {
        return view('pages.admin.rab.list_rab');
    }

    public static function queryMaterialOrders()
    {

        $query = \DB::table('brw_pengajuan');
        $query->leftjoin('brw_material_requisition_hdr', 'brw_pengajuan.pengajuan_id', 'brw_material_requisition_hdr.pengajuan_id');
        $query->join('brw_user_detail_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_user_detail_pengajuan.pengajuan_id');
        $query->join('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
        $query->join('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
        $query->selectRaw('brw_pengajuan.brw_id, brw_pengajuan.pengajuan_id, brw_pengajuan.pendanaan_tipe,  
                    IF(brw_user_detail_pengajuan.brw_type = 1, UPPER(brw_user_detail_pengajuan.nama), UPPER(brw_user_detail_pengajuan.nm_bdn_hukum)) as nama_cust, 
                    brw_pengajuan.pengajuan_id,  brw_pengajuan.created_at AS tgl_pengajuan, 
                    brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan, m_tujuan_pembiayaan.tujuan_pembiayaan,  brw_pengajuan.pendanaan_dana_dibutuhkan AS nilai_pengajuan, 
                    brw_pengajuan.status as stts, brw_pengajuan.pendanaan_dana_dibutuhkan, brw_pengajuan.estimasi_mulai,
                    IF(brw_user_detail_pengajuan.brw_type = 1, brw_user_detail_pengajuan.no_tlp, UPPER(brw_user_detail_pengajuan.telpon_perusahaan)) as no_telp, 
                    brw_material_requisition_hdr.requisition_date, brw_material_requisition_hdr.delivery_to_address ');

        return $query;
    }

    public static function queryMaterialItemList()
    {
        $query = \DB::table('brw_material_requisition_hdr');
        $query->join('brw_material_requisition_dtl', 'brw_material_requisition_hdr.requisition_hdr_id', 'brw_material_requisition_dtl.requisition_hdr_id');
        $query->join('brw_material_task', 'brw_material_requisition_dtl.task_id', 'brw_material_task.task_id');
        $query->leftjoin('admins', 'brw_material_requisition_dtl.created_by', 'admins.id');
        $query->leftjoin('brw_user', 'brw_material_requisition_dtl.created_by', 'brw_user.brw_id');
        $query->selectRaw('brw_material_requisition_dtl.requisition_line_id, brw_material_requisition_dtl.material_item_name, brw_material_requisition_dtl.quantity,
                     brw_material_requisition_dtl.unit_price, brw_material_task.task_description,  brw_material_requisition_dtl.creation_date, 
                     COALESCE(admins.email, brw_user.email) as created_by, brw_material_requisition_hdr.requisition_date');

        return $query;

    }


    public function dataMaterialOrders()
    {
        //Pengajuan 
        $query = $this->queryMaterialOrders();
        $pengajuan = $query->where('brw_pengajuan.pendanaan_tipe', 1)->orderBy('brw_pengajuan.created_at', 'desc')->get();

        $response = ['data' => $pengajuan];
        return response()->json($response);
    }

    public function detailPageMaterialOrders($pengajuanId)
    {
        $query  = $this->queryMaterialOrders();
        $query->where('brw_pengajuan.pengajuan_id', $pengajuanId);
        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->first();


        $queryMaterialItems = $this->queryMaterialItemList();
        $requisitionHeader  = $queryMaterialItems->orderBy('brw_material_requisition_dtl.task_id', 'asc')
                        ->orderBy('creation_date', 'asc')
                        ->where('brw_material_requisition_hdr.pengajuan_id', $pengajuanId)->get();

        $countItems = BorrowerCartMaterialOrder::where('pengajuan_id', $pengajuanId)->count();                    
        return view('pages.admin.rab.detail_rab', compact('pengajuan', 'requisitionHeader', 'countItems'));
    }

    public function detailPageAddProducts($pengajuanId)
    {

        $query  = $this->queryMaterialOrders();
        $query->where('brw_pengajuan.pengajuan_id', $pengajuanId);
        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->first();

        $listCategory = MaterialOrderService::productCategories();

        if (count($listCategory['data']) > 0) {
            $categories[''] = 'Pilih';
            foreach ($listCategory['data'] as $key => $val) {
                $categories[$val['id']] = $val['name'];
            }
        }

        $materialTaskList = BorrowerMaterialTask::pluck('task_description', 'task_id');
        $countItems = BorrowerCartMaterialOrder::where('pengajuan_id', $pengajuanId)->count();

        return view('pages.admin.rab.add_products', compact('pengajuan', 'categories', 'materialTaskList', 'countItems'));
    }

    public function ajaxProductListing(Request $request)
    {
        return MaterialOrderService::productListing($request);
    }

    public function ajaxBorrowerCartMaterialOrder(Request $request){
        
        $returnHTML = '';

        $query  = \DB::table('brw_cart_material_order');
        $query->join('brw_material_task', 'brw_cart_material_order.task_id', 'brw_material_task.task_id');
        $query->where('brw_cart_material_order.pengajuan_id', $request->pengajuan_id);

        $brwCartMaterialOrder = $query->orderBy('brw_cart_material_order.task_id', 'asc')->selectRaw('brw_cart_material_order.*, brw_material_task.task_description')->get();

        if (isset($brwCartMaterialOrder) && count($brwCartMaterialOrder) > 0) {
           
            foreach ($brwCartMaterialOrder as $val) {
                $returnHTML .= "
                    <div class='row justify-content-center mb-3'>
                        <div class='col-md-12 col-xl-10'>
                            <div class='card shadow-0 border rounded-3'>
                                <div class='card-body'>
                                    <div class='row'>
                                        <div class='col-md-12 col-lg-3 col-xl-3 mb-4 mb-lg-0'>
                                            <div class='bg-image  ripple rounded ripple-surface'>
                                                <img src='".$val->material_item_url."'
                                                    class='w-100' />
                                            </div>
                                        </div>
                                        <div class='col-md-6 col-lg-6 col-xl-6'>
                                            <h6>".$val->material_item_name."</h6>
                                    
                                            <div class='mt-1 mb-0 font-italic'>
                                                Tahap : ".$val->task_description."
                                            </div>     
                                            <div class='mt-1 mb-0 text-bold'>
                                                Harga : Rp. ".number_format($val->price, 0, ",", ".")."

                                            </div>
                                        </div>";

                                if($request->page =='checkout'){
                                    $returnHTML .= "
                                    <div class='col-md-6 col-lg-3 col-xl-3 border-sm-start-none border-start'>
                                        
                                        <div class='d-flex flex-column'>
                                            <label class='text-center font-weight-bold'>Qty  </label>
                                            <input type='text' name='qty' id='qty' value='".$val->qty."' class='form-control-plaintext text-center'/> 
                                        </div>
                                </div>
                                ";

                                }else{
                                    $returnHTML .= "
                                            <div class='col-md-6 col-lg-3 col-xl-3 border-sm-start-none border-start'>
                                                
                                                <div class='d-flex flex-column'>
                                                    <label class='text-center font-weight-bold'>Qty </label>
                                                    <input type='number' name='qty' id='qty' value='".$val->qty."' class='form-control text-center' onkeyup='updateCart(".$val->id.", this.value, \"update\");' oninput='updateCart(".$val->id.", this.value, \"update\");' min='1'/> 
                                                </div>

                                                <div class='d-flex flex-column'>
                                                    <button class='btn  btn-sm mt-2' type='button' style='background-color: #ff7675;color: white;' onclick='updateCart(".$val->id.", 0, \"delete\");'>
                                                        <i class='fa fa-trash'></i> Delete
                                                    </button>
                                            </div>
                                        </div>
                                    ";

                                }
                                

                                $returnHTML .="     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>";

            }
           // return $returnHTML;

            return response()->json(['status' => 'success', 'message' =>  $returnHTML, 'count'=>  count($brwCartMaterialOrder)]);
        }else{
            $returnHTML = "  <div class='row justify-content-center mb-3'>
                    <div class='col-md-12 col-xl-10 text-center'>
                        Tidak ada produk/barang 
                    </div>
            
            </div>";

            return response()->json(['status' => 'success', 'message' =>  $returnHTML, 'count'=>  0]);
        }

    }

    public function detailCartPage($pengajuanId){
        $pengajuan = BorrowerPengajuan::where('pengajuan_id', $pengajuanId)->first();
        return view('pages.admin.rab.cart_products', compact('pengajuan'));
    }

    public function detailCheckoutPage($pengajuanId){
        $pengajuan = BorrowerPengajuan::where('pengajuan_id', $pengajuanId)->first();
        $cartOrder = DB::table('brw_cart_material_order')->select(DB::raw('SUM(qty * price) as total_belanja'))->where('pengajuan_id', $pengajuanId)->first();

        $materialHeader = BorrowerMaterialRequisitionHeader::where('pengajuan_id', $pengajuanId)->first();

        return view('pages.admin.rab.checkout', compact('pengajuan', 'cartOrder', 'materialHeader'));
    }


    public function addToCart(Request $request){

        BorrowerCartMaterialOrder::updateOrCreate(
            ['pengajuan_id' => $request->pengajuan_id, 'material_item_id'=> $request->material_item_id, 'task_id'=> $request->task_id],
            [
                'qty' => 1,
                'price' => $request->price,
                'material_item_name'=> $request->material_item_name,
                'material_item_url'=> $request->material_item_url,
                'material_item_desc'=> $request->material_item_desc,
                'item_category_id'=> $request->item_category_id
            ]
        );

        $countItems = BorrowerCartMaterialOrder::where('pengajuan_id', $request->pengajuan_id)->count();
        return response()->json(['status' => 'success', 'message' => 'Berhasil di simpan', 'countItemCart'=> '('.$countItems.')']);
    }


    public function ajaxUpdateCart(Request $request){

        if($request->action == 'update'){
            BorrowerCartMaterialOrder::where('id', $request->cart_id)->update(['qty'=> $request->qty]);
        }else{
            BorrowerCartMaterialOrder::where('id', $request->cart_id)->delete();
        }
        return response()->json(['status' => 'success', 'message' => 'qty updated']);
    }

    public function konfirmasiCheckout(Request $request){
        
        $cartItems  = BorrowerCartMaterialOrder::where('pengajuan_id', $request->pengajuanId)->orderBy('task_id', 'asc')->get();

        if (Auth::guard('borrower')->check()) {
            $user_id = Auth::guard('borrower')->user()->brw_id;
        }

        if (Auth::guard('admin')->check()) {
            $user_id = Auth::guard('admin')->user()->id;
        }
      
        $dataMaterialHdr = BorrowerMaterialRequisitionHeader::updateOrCreate(
            ['pengajuan_id'=> $request->pengajuanId, 'brw_id'=> $request->brwId],
            [
              'requisition_date'=> date('Y-m-d', strtotime($request->tglPengiriman)),
              'delivery_to_address' => $request->alamatPengiriman,
              'created_by'=>$user_id, 
              'last_updated_by'=> $user_id
            ]
         );
   

        $requisition_hdr_id = $dataMaterialHdr->requisition_hdr_id;
        $maxLineNumber = DB::table('brw_material_requisition_dtl')->where('requisition_hdr_id', $requisition_hdr_id)->max('line_number');
        
        $lineNumber = $maxLineNumber + 1;

        foreach($cartItems as $val) {


            $dataExistMaterialItem = BorrowerMaterialRequisitionDetail::where('requisition_hdr_id', $requisition_hdr_id)
                 ->where('material_item_id', $val->material_item_id)
                 ->where('task_id', $val->task_id)->where('creation_date', date('Y-m-d'))->first();

            if($dataExistMaterialItem){
                $dataExistMaterialItem->material_item_id = $val->material_item_id;
                $dataExistMaterialItem->quantity = $dataExistMaterialItem->quantity + $val->qty;
                $dataExistMaterialItem->unit_price = $val->price;
                $dataExistMaterialItem->last_update_date = date('Y-m-d H:i:s');
                $dataExistMaterialItem->last_updated_by = $user_id;
                $dataExistMaterialItem->update();

            }else{

                $dataExistMaterialItem = new BorrowerMaterialRequisitionDetail();
                $dataExistMaterialItem->requisition_hdr_id = $requisition_hdr_id;
                $dataExistMaterialItem->line_number = $lineNumber;
                $dataExistMaterialItem->task_id = $val->task_id;
                $dataExistMaterialItem->item_category_id = $val->item_category_id;
                $dataExistMaterialItem->material_item_id = $val->material_item_id;
                $dataExistMaterialItem->material_item_name = $val->material_item_name;
                $dataExistMaterialItem->item_description = $val->material_item_desc;
                $dataExistMaterialItem->quantity = $val->qty;
                $dataExistMaterialItem->unit_price = $val->price;
                $dataExistMaterialItem->created_by = $user_id;
                $dataExistMaterialItem->save();
            }

    
            $lineNumber++;
        }


        BorrowerCartMaterialOrder::where('pengajuan_id', $request->pengajuanId)->delete();
        return response()->json(['status' => 'success', 'pengajuan_id' => $request->pengajuanId]);
      

    }

   

    
}
