<?php

namespace App\Http\Controllers\Mobile;

use App\Borrower;
use App\BorrowerAgunan;
use App\BorrowerAnalisaPendanaan;
use App\BorrowerDetailPenghasilan;
use App\BorrowerPendanaanNonRumahLain;
use App\BorrowerPendanaanRumahLain;
use App\BorrowerPengajuan;
use App\BorrowerRekening;
use App\Constants\KYCStatus;
use App\Http\Controllers\Controller;
use App\MasterKawin;
use App\MasterPekerjaan;
use App\Services\BorrowerService;
use App\Services\DSICoreService;
use App\Services\KYCService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class BorrowerPendanaanVerifikasi1ControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['only' => [
            'currentStatus',
            'getProfession', 'updateProfession',
            'getProfile', 'updateProfile',
            'getAddress', 'updateAddress',
            'objekPendanaan', 'updateObjekPendanaan',
            'pemilikPendanaan', 'updatePemilikPendanaan',
            'pendanaanBerjalan', 'updatePendanaanBerjalan',
            'agunan', 'updateAgunan',
            'rekening', 'updateRekening',
        ]]);
    }

    public function currentStatus(Request $request, BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $borrowerDetil = BorrowerService::getDetilBorrower($borrower);
        $borrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        $pasanganCheck = KYCStatus::COMPLETE_TRUE;
        if ($borrowerDetil->status_kawin === MasterKawin::SUDAH) {
            $pasanganCheck = [
                BorrowerPendanaanVerifikasi1PasanganControllerV1::checkProfile($pengajuan),
                BorrowerPendanaanVerifikasi1PasanganControllerV1::checkAddress($pengajuan),
            ];

            if ($borrowerDetil->skema_pembiayaan === BorrowerDetailPenghasilan::SKEMA_JOINT) {
                $pasanganCheck[] = BorrowerPendanaanVerifikasi1PasanganControllerV1::checkProfession($pengajuan);
            }
            
            $pasanganCheck = KYCService::multipleCheck($pasanganCheck);
        }
       
        $incompleteFields = [
            'data_diri' => [],
            'objek_pendanaan' => [],
            'pemilik_pendanaan' => [],
            'pekerjaan' => [],
            'pendanaan_berjalan' => [],
            'agunan' => [],
            'rekening' => [],
        ];
        $status = [
            'data_diri' => self::checkProfile($pengajuan, $incompleteFields['data_diri']),
            'objek_pendanaan' => self::objekPendanaanCheck($pengajuan, $incompleteFields['objek_pendanaan']),
            'pemilik_pendanaan' => self::pemilikPendanaanCheck($pengajuan, $incompleteFields['pemilik_pendanaan']),
            'pekerjaan' => self::checkProfession($pengajuan, $incompleteFields['pekerjaan']),
            'pendanaan_berjalan' => self::pendanaanBerjalanCheck($pengajuan, $incompleteFields['pendanaan_berjalan']),
            'agunan' => self::agunanCheck($pengajuan, $incompleteFields['agunan']),
            'rekening' => self::rekeningCheck($pengajuan, $incompleteFields['rekening']),
            'pasangan' => $pasanganCheck,
            'status_pernikahan' => $borrowerDetil->status_kawin,
            'is_locked' => self::isPengajuanLocked($pengajuan),
            'skema_pembiayaan' => $borrowerPenghasilan->skema_pembiayaan,
            'sumber_pengembalian_dana' => $borrowerPenghasilan->sumber_pengembalian_dana,
        ];

        if (config('app.env') !== 'production') {
            $status['incomplete_fields'] = $incompleteFields;
        }

        return response()->json($status);
    }

    public function submit(Request $request, BorrowerPengajuan $pengajuan)
    {
        if (KYCService::multipleCheck([
                self::checkProfile($pengajuan),
                self::rekeningCheck($pengajuan),
                self::checkProfession($pengajuan),
            ]) !== KYCStatus::COMPLETE_TRUE) {
            return response()->json([
                'error' => 'Pengisian data belum lengkap',
            ]);
        }

        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $borrowerDetil = BorrowerService::getDetilBorrower(BorrowerService::getBorrower($pengajuan->brw_id));
        $borrowerDetilPenghasilan = BorrowerService::getDetilBorrowerPenghasilan(BorrowerService::getBorrower($pengajuan->brw_id));

        if ($borrowerDetil->status_kawin === MasterKawin::SUDAH) {
            $checks = [
                BorrowerPendanaanVerifikasi1PasanganControllerV1::checkProfile($pengajuan),
                BorrowerPendanaanVerifikasi1PasanganControllerV1::checkAddress($pengajuan),
            ];

            if ($borrowerDetil->skema_pembiayaan === BorrowerDetailPenghasilan::SKEMA_JOINT) {
                $checks[] = BorrowerPendanaanVerifikasi1PasanganControllerV1::checkProfession($pengajuan);
            }

            if (KYCService::multipleCheck($checks) !== KYCStatus::COMPLETE_TRUE) {
                return response()->json([
                    'error' => 'Pengisian data pasangan belum lengkap',
                ]);
            }
        }

        $borrower->forceFill([
            'status' => Borrower::STATUS_ACTIVE,
        ])
            ->save();

        return response()->json([
            'success' => 'Data berhasil disubmit',
        ]);
    }

    public function getProfile(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        $borrowerDetil = BorrowerService::getDetilBorrower(BorrowerService::getBorrower($pengajuan->brw_id));

        return response()->json([
            'nama_ibu_kandung' => $borrowerDetil->nm_ibu,
            'provinsi' => $borrowerDetil->provinsi,
            'kota' => $borrowerDetil->kota,
            'kecamatan' => $borrowerDetil->kecamatan,
            'kelurahan' => $borrowerDetil->kelurahan,
            'kode_pos' => $borrowerDetil->kode_pos,
            'alamat' => $borrowerDetil->alamat,
            'status_rumah' => $borrowerDetil->status_rumah,
            'domisili_provinsi' => $borrowerDetil->domisili_provinsi,
            'domisili_kota' => $borrowerDetil->domisili_kota,
            'domisili_kecamatan' => $borrowerDetil->domisili_kecamatan,
            'domisili_kelurahan' => $borrowerDetil->domisili_kelurahan,
            'domisili_kode_pos' => $borrowerDetil->domisili_kd_pos,
            'domisili_alamat' => $borrowerDetil->domisili_alamat,
            'domisili_status_rumah' => $borrowerDetil->domisili_status_rumah,
            'no_kk' => $borrowerDetil->nomor_kk_pribadi,
        ]);
    }

    public function updateProfile(Request $request, BorrowerPengajuan $pengajuan)
    {
        DB::beginTransaction();
        $borrowerDetil = BorrowerService::getDetilBorrower(BorrowerService::getBorrower($pengajuan->brw_id));

        try {
            Validator::make($request->all(), [
                'nama_ibu_kandung' => ['max:50'],
                'domisili_provinsi' => [Rule::exists('m_kode_pos', 'Provinsi')],
                'domisili_kota' => [Rule::exists('m_kode_pos', 'Kota')],
                'domisili_kecamatan' => [Rule::exists('m_kode_pos', 'kecamatan')],
                'domisili_kelurahan' => [Rule::exists('m_kode_pos', 'kelurahan')],
                'domisili_kode_pos' => [Rule::exists('m_kode_pos', 'kode_pos')],
                'domisili_alamat' => ['max:100'],
                'domisili_status_rumah' => [Rule::exists('m_kepemilikan_rumah', 'id_kepemilikan_rumah')],
            ])
                ->validate();

            $borrowerDetil->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'nama_ibu_kandung' => 'nm_ibu',
                'no_kk' => 'nomor_kk_pribadi',
                'domisili_provinsi' => 'domisili_provinsi',
                'domisili_kota' => 'domisili_kota',
                'domisili_kecamatan' => 'domisili_kecamatan',
                'domisili_kelurahan' => 'domisili_kelurahan',
                'domisili_kode_pos' => 'domisili_kd_pos',
                'domisili_alamat' => 'domisili_alamat',
                'domisili_status_rumah' => 'domisili_status_rumah',
            ]))
                ->save();

            DB::commit();

            DSICoreService::updatePengajuan($pengajuan->pengajuan_id);

            return response()->json(['success' => 'Data diri berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public static function checkProfile(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $borrowerDetil = BorrowerService::getDetilBorrower(BorrowerService::getBorrower($pengajuan->brw_id));
        $columns = [
            'nm_ibu',
            'nomor_kk_pribadi',
            'domisili_provinsi',
            'domisili_kota',
            'domisili_kecamatan',
            'domisili_kelurahan',
            'domisili_kd_pos',
            'domisili_alamat',
            'domisili_status_rumah',
        ];

        return KYCService::baseCheck($columns, $borrowerDetil->toArray(), $incompleteFields);
    }

    public function getProfession()
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        return response()->json([
            'pekerjaan' => $detilBorrower->pekerjaan,
            'status_pernikahan' => $detilBorrower->status_kawin,
            'bidang_pekerjaan' => $detilBorrower->bidang_pekerjaan,
            'bidang_online' => $detilBorrower->bidang_online,
            'npwp' => $detilBorrower->npwp,
            'sumber_pengembalian_dana' => $detilBorrowerPenghasilan->sumber_pengembalian_dana,
            'skema_pembiayaan' => $detilBorrowerPenghasilan->skema_pembiayaan,
            'nama_perusahaan' => $detilBorrowerPenghasilan->nama_perusahaan,
            'alamat_perusahaan' => $detilBorrowerPenghasilan->alamat_perusahaan,
            'rt' => $detilBorrowerPenghasilan->rt,
            'rw' => $detilBorrowerPenghasilan->rw,
            'provinsi' => $detilBorrowerPenghasilan->provinsi,
            'kabupaten' => $detilBorrowerPenghasilan->kab_kota,
            'kecamatan' => $detilBorrowerPenghasilan->kecamatan,
            'kelurahan' => $detilBorrowerPenghasilan->kelurahan,
            'kode_pos' => $detilBorrowerPenghasilan->kode_pos,
            'no_tlp_usaha' => $detilBorrowerPenghasilan->no_telp,
            'no_hp_usaha' => $detilBorrowerPenghasilan->no_hp,
            'penghasilan_perbulan' => $detilBorrowerPenghasilan->pendapatan_borrower,
            'biaya_hidup_perbulan' => $detilBorrowerPenghasilan->biaya_hidup,
            'nilai_spt' => $detilBorrowerPenghasilan->nilai_spt,
            'detail_penghasilan_lain' => $detilBorrowerPenghasilan->detail_penghasilan_lain_lain,
            'total_penghasilan_lain' => $detilBorrowerPenghasilan->total_penghasilan_lain_lain,
            'surat_izin' => $detilBorrowerPenghasilan->surat_ijin,
            'no_surat_izin' => $detilBorrowerPenghasilan->no_surat_ijin,
            'lama_usaha' => $detilBorrowerPenghasilan->usia_perusahaan,
            'lama_tempat_usaha' => $detilBorrowerPenghasilan->usia_tempat_usaha,
            'usia_perusahaan' => $detilBorrowerPenghasilan->usia_perusahaan,
            'departemen' => $detilBorrowerPenghasilan->departemen,
            'jabatan' => $detilBorrowerPenghasilan->jabatan,
            'masa_kerja_tahun' => $detilBorrowerPenghasilan->masa_kerja_tahun,
            'masa_kerja_bulan' => $detilBorrowerPenghasilan->masa_kerja_bulan,
            'nip' => $detilBorrowerPenghasilan->nip_nrp_nik,
            'nama_hrd' => $detilBorrowerPenghasilan->nama_hrd,
            'no_fixed_line_hrd' => $detilBorrowerPenghasilan->no_fixed_line_hrd,
            'pengalaman_kerja_di_tempat_lain_tahun' => $detilBorrowerPenghasilan->pengalaman_kerja_tahun,
            'pengalaman_kerja_di_tempat_lain_bulan' => $detilBorrowerPenghasilan->pengalaman_kerja_bulan,
            'status_pekerjaan' => $detilBorrowerPenghasilan->status_pekerjaan,
            'bentuk_badan_usaha' => $detilBorrowerPenghasilan->bentuk_badan_usaha,
        ]);
    }

    public function updateProfession(Request $request)
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        DB::beginTransaction();
        try {
            $single = in_array($detilBorrower->status_kawin, [MasterKawin::BELUM, MasterKawin::JANDADUDA]);
            $isWiraswasta = ($request->pekerjaan === MasterPekerjaan::query()->where('pekerjaan', MasterPekerjaan::WIRASWASTA)->first()->id);
            Validator::make(array_merge($request->all(), [
                '_jenis_pekerjaan' => $isWiraswasta ? 'wiraswasta' : 'non-wiraswasta',
            ]), [
                'pekerjaan' => [Rule::exists('m_pekerjaan', 'id_pekerjaan')],
                'bidang_pekerjaan' => [Rule::exists('m_bidang_pekerjaan', 'id_bidang_pekerjaan')],
                'bidang_online' => [Rule::exists('m_online', 'id_online')],

                // baru
                'npwp' => ['numeric', 'digits_between:15,16'],
                'sumber_pengembalian_dana' => [Rule::in([1, 2])],
                'skema_pembiayaan' => [function ($attribute, $value, $fail) use ($single) {
                    if ($single && $value === BorrowerDetailPenghasilan::SKEMA_JOINT) {
                        $fail("Skema pembiayaan tidak valid");
                    }
                }],
                'nama_perusahaan' => ['max:50'],
                'alamat_perusahaan' => ['max:100'],
                'rt' => ['max:5'],
                'rw' => ['max:5'],
                'provinsi' => [Rule::exists('m_provinsi_kota', 'nama_provinsi')],
                'kabupaten' => [Rule::exists('m_kode_pos', 'Kota')],
                'kecamatan' => [Rule::exists('m_kode_pos', 'kecamatan')],
                'kode_pos' => [Rule::exists('m_kode_pos', 'kode_pos')],
                'no_tlp_usaha' => ['max:15'],
                'penghasilan_perbulan' => ['numeric'],
                'biaya_hidup_perbulan' => ['numeric'],
                'nilai_spt' => ['numeric'],
                'penghasilan_lain_lain' => [Rule::in(0, 1)],
                'detail_penghasilan_lain_lain' => ['max:200', 'required_if:penghasilan_lain_lain,1'],
                'total_penghasilan_lain' => ['numeric', 'required_if:penghasilan_lain_lain,1'],
                'bentuk_badan_usaha' => [Rule::exists('m_bentuk_badan_usaha', 'id')],

                // non wiraswasta
                'surat_izin' => [Rule::in([0, 1])],
                'no_surat_izin' => ['max:50'],
                'lama_usaha' => ['numeric', 'digits_between:0,5'],
                'lama_tempat_usaha' => ['numeric', 'digits_between:0,5'],
                'status_pekerjaan' => ['numeric', Rule::in(array_keys(BorrowerDetailPenghasilan::$statusPegawaiNames))],

                // wiraswasta
                'usia_perusahaan' => ['numeric', 'digits_between:0,5'],
                'nama_departemen' => ['max:50'],
                'no_hp_usaha' => ['max:15'],
                'jabatan' => ['max:50'],
                'masa_kerja_tahun' => ['numeric', 'digits_between:0,5'],
                'masa_kerja_bulan' => ['numeric', 'digits_between:0,5', 'between:0,11'],
                'nik_nrp' => ['max:35'],
                'nama_hrd' => ['max:50'],
                'no_fixed_line_hrd' => ['max:20'],
                'pengalaman_kerja_di_tempat_lain_tahun' => ['numeric', 'digits_between:0,5'],
                'pengalaman_kerja_di_tempat_lain_bulan' => ['numeric', 'digits_between:0,5', 'between:0,11'],
            ])
                ->validate();

            $detilBorrower->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'pekerjaan' => 'pekerjaan',
                'bidang_pekerjaan' => 'bidang_pekerjaan',
                'bidang_online' => 'bidang_online',
                'npwp' => 'npwp',
            ]))
                ->save();

            $detilBorrowerPenghasilan->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'sumber_pengembalian_dana' => 'sumber_pengembalian_dana',
                'skema_pembiayaan' => 'skema_pembiayaan',
                'nama_perusahaan' => 'nama_perusahaan',
                'alamat_perusahaan' => 'alamat_perusahaan',
                'rt' => 'rt',
                'rw' => 'rw',
                'provinsi' => 'provinsi',
                'kabupaten' => 'kab_kota',
                'kecamatan' => 'kecamatan',
                'kelurahan' => 'kelurahan',
                'kode_pos' => 'kode_pos',
                'no_tlp_usaha' => 'no_telp',
                'no_hp_usaha' => 'no_hp',
                'penghasilan_perbulan' => 'pendapatan_borrower',
                'biaya_hidup_perbulan' => 'biaya_hidup',
                'nilai_spt' => 'nilai_spt',
                'detail_penghasilan_lain' => 'detail_penghasilan_lain_lain',
                'total_penghasilan_lain' => 'total_penghasilan_lain_lain',
                // non wiraswasta
                'surat_izin' => 'surat_ijin',
                'no_surat_izin' => 'no_surat_ijin',
                'lama_usaha' => 'usia_perusahaan',
                'lama_tempat_usaha' => 'usia_tempat_usaha',
                // wiraswasta
                'usia_perusahaan' => 'usia_perusahaan',
                'departemen' => 'departemen',
                'jabatan' => 'jabatan',
                'masa_kerja_tahun' => 'masa_kerja_tahun',
                'masa_kerja_bulan' => 'masa_kerja_bulan',
                'nip' => 'nip_nrp_nik',
                'nama_hrd' => 'nama_hrd',
                'no_fixed_line_hrd' => 'no_fixed_line_hrd',
                'status_pekerjaan' => 'status_pekerjaan',
                'bentuk_badan_usaha' => 'bentuk_badan_usaha',
                'pengalaman_kerja_di_tempat_lain_tahun' => 'pengalaman_kerja_tahun',
                'pengalaman_kerja_di_tempat_lain_bulan' => 'pengalaman_kerja_bulan',
            ]))
                ->save();

            DB::commit();

            return response()->json(['success' => 'Data pekerjaan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private static function checkProfession(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        $columns = collect([
            'sumber_pengembalian_dana',
            'skema_pembiayaan',
            'nama_perusahaan',
            'alamat_perusahaan',
            'rt',
            'rw',
            'provinsi',
            'kab_kota',
            'kecamatan',
            'kelurahan',
            'kode_pos',
            'no_telp',
            'pendapatan_borrower',
            'biaya_hidup',
            'nilai_spt',
            'bentuk_badan_usaha',
        ]);

        $isWiraswasta = ($detilBorrower->pekerjaan === MasterPekerjaan::query()->where('pekerjaan', MasterPekerjaan::WIRASWASTA)->first()->id);
        $isTidakBekerja = ($detilBorrower->pekerjaan === MasterPekerjaan::query()->whereIn('pekerjaan', [MasterPekerjaan::PELAJAR, MasterPekerjaan::TIDAK_BEKERJA])->first()->id);
        if ($isWiraswasta) {
            $columns->push([
                'surat_ijin',
                'no_hp_usaha',
            ]);

            if ($detilBorrowerPenghasilan->surat_ijin !== 0) {
                $columns->merge([
                    'no_surat_ijin',
                    'usia_perusahaan',
                    'usia_tempat_usaha',
                ]);
            }
        } elseif (! $isTidakBekerja) {
            $columns->merge([
                'usia_perusahaan',
                'departemen',
                'jabatan',
                'masa_kerja_tahun',
                'masa_kerja_bulan',
                'nip_nrp_nik',
                'nama_hrd',
                'no_fixed_line_hrd',
                'status_pekerjaan',
            ]);
        }

        return KYCService::baseCheck($columns->toArray(), $detilBorrowerPenghasilan, $incompleteFields);
    }

    public function objekPendanaan(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);

        return response()->json([
            'provinsi' => $pengajuan->provinsi,
            'kota' => $pengajuan->kota,
            'kecamatan' => $pengajuan->kecamatan,
            'kelurahan' => $pengajuan->kelurahan,
            'alamat' => $pengajuan->lokasi_proyek,
            'kode_pos' => $pengajuan->kode_pos,
            'detail' => $pengajuan->detail_pendanaan,
        ]);
    }

    public function updateObjekPendanaan(Request $request, BorrowerPengajuan $pengajuan)
    {
        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'provinsi' => [Rule::exists('m_kode_pos', 'Provinsi')],
                'kota' => [Rule::exists('m_kode_pos', 'Kota')],
                'kecamatan' => [Rule::exists('m_kode_pos', 'kecamatan')],
                'kelurahan' => [Rule::exists('m_kode_pos', 'kelurahan')],
                'kode_pos' => [Rule::exists('m_kode_pos', 'kode_pos')],
                'detail' => ['max:2048'],
                'alamat' => ['max:2048'],
            ])
                ->validate();

            $pengajuan->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'provinsi' => 'provinsi',
                'kota' => 'kota',
                'kecamatan' => 'kecamatan',
                'kelurahan' => 'kelurahan',
                'kode_pos' => 'kode_pos',
                'detail' => 'detail_pendanaan',
                'alamat' => 'lokasi_proyek',
            ]))
                ->save();

            DB::commit();

            return response()->json(['success' => 'Data objek pendanaan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private static function objekPendanaanCheck(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $columns = [
            'provinsi',
            'kota',
            'kecamatan',
            'kode_pos',
            'kelurahan',
            'lokasi_proyek',
            'detail_pendanaan',
        ];

        return KYCService::baseCheck($columns, $pengajuan->toArray(), $incompleteFields);
    }

    public function pemilikPendanaan(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);

        return response()->json([
            'nama' => $pengajuan->nm_pemilik,
            'telp' => $pengajuan->no_tlp_pemilik,
            'alamat' => $pengajuan->alamat_pemilik,
            'provinsi' => $pengajuan->provinsi_pemilik,
            'kota' => $pengajuan->kota_pemilik,
            'kecamatan' => $pengajuan->kecamatan_pemilik,
            'kode_pos' => $pengajuan->kd_pos_pemilik,
            'kelurahan' => $pengajuan->kelurahan_pemilik,
        ]);
    }

    public function updatePemilikPendanaan(Request $request, BorrowerPengajuan $pengajuan)
    {
        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'nama' => ['max:200'],
                'alamat' => [],
                'telp' =>   ['min:9', 'max:15'],
                'provinsi' => [Rule::exists('m_kode_pos', 'Provinsi')],
                'kota' => [Rule::exists('m_kode_pos', 'Kota')],
                'kecamatan' => [Rule::exists('m_kode_pos', 'kecamatan')],
                'kelurahan' => [Rule::exists('m_kode_pos', 'kelurahan')],
                'kode_pos' => [Rule::exists('m_kode_pos', 'kode_pos')],
            ])
                ->validate();

            $pengajuan->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'nama' => 'nm_pemilik',
                'alamat' => 'alamat_pemilik',
                'telp' => 'no_tlp_pemilik',
                'provinsi' => 'provinsi_pemilik',
                'kota' => 'kota_pemilik',
                'kecamatan' => 'kecamatan_pemilik',
                'kelurahan' => 'kelurahan_pemilik',
                'kode_pos' => 'kd_pos_pemilik',
            ]))->save();

            DB::commit();

            DSICoreService::updatePengajuan($pengajuan->pengajuan_id);

            return response()->json(['success' => 'Data pemilik objek pendanaan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private static function pemilikPendanaanCheck(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $columns = [
            'nm_pemilik',
            'alamat_pemilik',
            'kd_pos_pemilik',
            'no_tlp_pemilik',
            'provinsi_pemilik',
            'kota_pemilik',
            'kecamatan_pemilik',
            'kelurahan_pemilik',
        ];

        return KYCService::baseCheck($columns, $pengajuan->toArray(), $incompleteFields);
    }

    public function pendanaanBerjalan(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        $pendanaanRumah = BorrowerPendanaanRumahLain::query()
            ->where('pengajuan_id', $pengajuan->pengajuan_id)
            ->get();
        $pendanaanNonRumah = BorrowerPendanaanNonRumahLain::query()
            ->where('pengajuan_id', $pengajuan->pengajuan_id)
            ->get();

        return response()->json([
            'rumah_ke' => $pendanaanRumah->count() + 1,
            'daftar_rumah' => $pendanaanRumah->map(function ($item) {
                $item['plafond'] = (float) $item['plafond'];
                $item['outstanding'] = (float) $item['outstanding'];
                $item['angsuran'] = (float) $item['angsuran'];

                return $item;
            }),
            'pendanaan_lain' => $pendanaanNonRumah->count() > 0 ? 1 : 0,
            'daftar_pendanaan' => $pendanaanNonRumah->map(function ($item) {
                $item['plafond'] = (float) $item['plafond'];
                $item['outstanding'] = (float) $item['outstanding'];
                $item['angsuran'] = (float) $item['angsuran'];

                return $item;
            }),
        ]);
    }

    public function updatePendanaanBerjalan(Request $request, BorrowerPengajuan $pengajuan)
    {
        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'rumah_ke' => ['numeric', 'min:1', 'max:3'],
                'daftar_rumah' => [
                    'array',
                    Rule::requiredIf(function () use ($request) {
                        return $request->get('rumah_ke') > 1;
                    }),
                    function ($attribute, $value, $fail) use ($request) {
                        if (count($value) > ((int) $request->get('rumah_ke') - 1)) {
                            $fail('Daftar rumah boleh diisi jika bukan rumah pertama (rumah pendanaan)');
                        }
                    }],
                'daftar_rumah.*.status' => ['numeric', Rule::in([
                    BorrowerPendanaanRumahLain::STATUS_LUNAS,
                    BorrowerPendanaanRumahLain::STATUS_BELUM_LUNAS,
                ])],
                'daftar_rumah.*.bank' => [],
                'daftar_rumah.*.plafond' => ['numeric'],
                'daftar_rumah.*.jangka_waktu' => [],
                'daftar_rumah.*.outstanding' => ['numeric'],
                'daftar_rumah.*.angsuran' => ['numeric'],
                'pendanaan_lainnya' => ['numeric', 'in:0,1'],
                'daftar_pendanaan' => ['array', Rule::requiredIf(function () use ($request) {
                    return (int) $request->get('pendanaan_lainnya') === 1;
                })],
                'daftar_pendanaan.*.bank' => [],
                'daftar_pendanaan.*.plafond' => ['numeric'],
                'daftar_pendanaan.*.jangka_waktu' => [],
                'daftar_pendanaan.*.outstanding' => ['numeric'],
                'daftar_pendanaan.*.angsuran' => ['numeric'],
            ])
                ->validate();

            BorrowerPendanaanRumahLain::query()->where('pengajuan_id', $pengajuan->pengajuan_id)->delete();
            foreach ($request->get('daftar_rumah') ?? [] as $key => $house) {
                $pendanaan = new BorrowerPendanaanRumahLain();

                $rumahKe = $key + 1;
                if ($rumahKe > ((int) $request->get('rumah_ke') - 1)) {
                    break;
                }

                if ((int) $house['status'] === BorrowerPendanaanRumahLain::STATUS_LUNAS) {
                    $house = [
                        'status' => BorrowerPendanaanRumahLain::STATUS_LUNAS,
                        'bank' => null,
                        'rumah_ke' => $rumahKe,
                        'plafond' => 0,
                        'jangka_waktu' => null,
                        'outstanding' => 0,
                        'angsuran' => 0,
                    ];
                }

                $pendanaan->forceFill(BorrowerService::mapToDatabase($house, [
                    'status' => 'status',
                    'bank' => 'bank',
                    'plafond' => 'plafond',
                    'jangka_waktu' => 'jangka_waktu',
                    'outstanding' => 'outstanding',
                    'angsuran' => 'angsuran',
                ]))
                    ->forceFill([
                        'rumah_ke' => $rumahKe,
                        'pengajuan_id' => $pengajuan->pengajuan_id,
                    ])
                    ->save();
            }
            BorrowerPendanaanNonRumahLain::query()->where('pengajuan_id', $pengajuan->pengajuan_id)->delete();
            foreach ($request->get('daftar_pendanaan') ?? [] as $others) {
                $pendanaan = new BorrowerPendanaanNonRumahLain();
                $pendanaan->forceFill(BorrowerService::mapToDatabase($others, [
                    'bank' => 'bank',
                    'plafond' => 'plafond',
                    'jangka_waktu' => 'jangka_waktu',
                    'outstanding' => 'outstanding',
                    'angsuran' => 'angsuran',
                ]))
                    ->forceFill([
                        'pengajuan_id' => $pengajuan->pengajuan_id,
                    ])
                    ->save();
            }
            DB::commit();

            DSICoreService::updatePengajuan($pengajuan->pengajuan_id);
            return response()->json(['success' => 'Data pendanaan berjalan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private static function pendanaanBerjalanCheck(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        if (
            BorrowerPendanaanRumahLain::query()->where('pengajuan_id', $pengajuan->pengajuan_id)->exists()
            || BorrowerPendanaanNonRumahLain::query()->where('pengajuan_id', $pengajuan->pengajuan_id)->exists()
        ) {
            return KYCStatus::COMPLETE_TRUE;
        }

        return KYCStatus::COMPLETE_FALSE;
    }

    public function agunan(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);

        $agunan = self::getAgunan($pengajuan);

        return response()->json([
            'provinsi_agunan' => $pengajuan->provinsi,
            'kota_agunan' => $pengajuan->kota,
            'kecamatan_agunan' => $pengajuan->kecamatan,
            'kelurahan_agunan' => $pengajuan->kelurahan,
            'alamat_agunan' => $pengajuan->lokasi_proyek,
            'kode_pos_agunan' => $pengajuan->kode_pos,
            'jenis_agunan' => $agunan->jenis_agunan,
            'nomor_agunan' => $agunan->nomor_agunan,
            'tanggal_terbit' => optional($agunan->tanggal_terbit, function ($value) {
                return Carbon::parse($value)->format('d-m-Y');
            }),
            'tanggal_jatuh_tempo' => optional($agunan->tanggal_jatuh_tempo, function ($value) {
                return Carbon::parse($value)->format('d-m-Y');
            }),
            'kantor_penerbit' => $agunan->kantor_penerbit,
            'rt_agunan' => $agunan->RT,
            'rw_agunan' => $agunan->RW,
            'blok_agunan' => $agunan->blok_nomor,
            'atas_nama_agunan' => $agunan->atas_nama_agunan,
            'luas_bangunan' => $agunan->luas_bangunan,
            'luas_tanah' => $agunan->luas_tanah,
            'nomor_imb' => $agunan->no_imb,
            'tanggal_terbit_imb' => optional($agunan->tanggal_terbit_imb, function ($value) {
                return Carbon::parse($value)->format('d-m-Y');
            }),
            'status_imb' => $agunan->status_imb,
            'nomor_surat_ukur' => $agunan->nomor_surat_ukur,
            'tanggal_surat_ukur' => optional($agunan->tanggal_surat_ukur, function ($value) {
                return Carbon::parse($value)->format('d-m-Y');
            }),
        ]);
    }

    public function updateAgunan(Request $request, BorrowerPengajuan $pengajuan)
    {
        DB::beginTransaction();
        try {
            $agunan = self::getAgunan($pengajuan);
            Validator::make($request->all(), [
                'jenis_agunan' => [Rule::in(array_keys(BorrowerAgunan::$jenisNames))],
                'nomor_agunan' => ['max:25'],
                'tanggal_terbit' => ['date_format:d-m-Y'],
                'tanggal_jatuh_tempo' => ['date_format:d-m-Y'],
                'atas_nama_agunan' => ['max:30'],
                'kantor_penerbit' => [Rule::exists('m_kantor_bpn', 'id')],
                'luas_bangunan' => ['numeric'],
                'luas_tanah' => ['numeric'],
                'rt_agunan' => ['numeric', 'digits_between:0,5'],
                'rw_agunan' => ['numeric', 'digits_between:0,5'],
                'blok_agunan' => ['numeric', 'digits_between:0,5'],
                'nomor_imb' => ['max:25'],
                'tanggal_terbit_imb' => ['date_format:d-m-Y'],
                'nomor_surat_ukur' => ['max:25'],
                'tanggal_surat_ukur' => ['date_format:d-m-Y'],
                'status_imb' => [Rule::in(array_keys(BorrowerAgunan::$statusIMBNames))],
            ])
                ->validate();

            $agunan->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'jenis_agunan' => 'jenis_agunan',
                'nomor_agunan' => 'nomor_agunan',
                'atas_nama_agunan' => 'atas_nama_agunan',
                'kantor_penerbit' => 'kantor_penerbit',
                'rt_agunan' => 'RT',
                'rw_agunan' => 'RW',
                'blok_agunan' => 'blok_nomor',
                'luas_bangunan' => 'luas_bangunan',
                'luas_tanah' => 'luas_tanah',
                'nomor_surat_ukur' => 'nomor_surat_ukur',
                'nomor_imb' => 'no_imb',
                'status_imb' => 'status_imb',
            ]))
                ->forceFill([
                    'tanggal_surat_ukur' => $request->filled('tanggal_surat_ukur') ? Carbon::parse($request->get('tanggal_surat_ukur')) : $agunan->tanggal_surat_ukur,
                    'tanggal_terbit_imb' => $request->filled('tanggal_terbit_imb') ? Carbon::parse($request->get('tanggal_terbit_imb')) : $agunan->tanggal_terbit_imb,
                    'tanggal_terbit' => $request->filled('tanggal_terbit') ? Carbon::parse($request->get('tanggal_terbit')) : $agunan->tanggal_terbit,
                    'tanggal_jatuh_tempo' => $request->filled('tanggal_jatuh_tempo') ? Carbon::parse($request->get('tanggal_jatuh_tempo')) : $agunan->tanggal_jatuh_tempo,
                ])
                ->save();

            DB::commit();

            DSICoreService::updatePengajuan($pengajuan->pengajuan_id);

            return response()->json(['success' => 'Data agunan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private static function agunanCheck(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $agunan = self::getAgunan($pengajuan);

        $columns = [
            'RT',
            'RW',
            'blok_nomor',
            'jenis_agunan',
            'nomor_agunan',
            'atas_nama_agunan',
            'luas_bangunan',
            'luas_tanah',
            'nomor_surat_ukur',
            'tanggal_surat_ukur',
            'tanggal_terbit',
            'kantor_penerbit',
            'status_imb',
        ];

        if ($agunan->status_imb === BorrowerAgunan::STATUS_IMB_ADA) {
            $columns[] = 'no_imb';
            $columns[] = 'tanggal_terbit_imb';
        }

        if ($agunan->jenis_agunan === BorrowerAgunan::JENIS_HGB) {
            $columns[] = 'tanggal_jatuh_tempo';
        }

        return KYCService::baseCheck($columns, $agunan->toArray(), $incompleteFields);
    }

    public function rekening(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);

        $rekening = self::getRekening($pengajuan);

        return response()->json([
            'no_rekening' => $rekening->brw_norek,
            'nama_pemilik_rekening' => $rekening->brw_nm_pemilik,
            'bank' => $rekening->brw_kd_bank,
            'kantor_cabang_pembuka' => $rekening->kantor_cabang_pembuka,
        ]);
    }

    public function updateRekening(Request $request, BorrowerPengajuan $pengajuan)
    {
        DB::beginTransaction();
        try {
            $rekening = self::getRekening($pengajuan);
            Validator::make($request->all(), [
                'no_rekening' => ['max:25'],
                'nama_pemilik_rekening' => ['max:50'],
                'bank' => [Rule::exists('m_bank', 'kode_bank')],
                'kantor_cabang_pembuka' => ['max:50'],
            ])
                ->validate();

            $rekening->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'no_rekening' => 'brw_norek',
                'nama_pemilik_rekening' => 'brw_nm_pemilik',
                'bank' => 'brw_kd_bank',
                'kantor_cabang_pembuka' => 'kantor_cabang_pembuka',
            ]))
                ->save();

            DB::commit();

            return response()->json(['success' => 'Data rekening berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private static function rekeningCheck(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $rekening = self::getRekening($pengajuan);

        $columns = [
            'brw_norek',
            'brw_nm_pemilik',
            'brw_kd_bank',
            'kantor_cabang_pembuka',
        ];

        return KYCService::baseCheck($columns, $rekening->toArray(), $incompleteFields);
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @return void
     */
    protected static function validateOwnershipAndLock(BorrowerPengajuan $pengajuan): void
    {
        if ($pengajuan->brw_id !== BorrowerService::getLoggedInBorrowerId()) {
            abort(403);
        }

        if (self::isPengajuanLocked($pengajuan)) {
            abort(403, 'Pengajuan sudah terkunci');
        }
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    private static function getAgunan(BorrowerPengajuan $pengajuan)
    {
        return BorrowerAgunan::query()->where('id_pengajuan', $pengajuan->pengajuan_id)
            ->firstOrCreate([
                'id_pengajuan' => $pengajuan->pengajuan_id,
            ]);
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    private static function getRekening(BorrowerPengajuan $pengajuan)
    {
        return BorrowerRekening::query()->where('brw_id', $pengajuan->brw_id)
            ->firstOrCreate([
                'brw_id' => $pengajuan->brw_id,
            ]);
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @return bool
     */
    protected static function isPengajuanLocked(BorrowerPengajuan $pengajuan)
    {
        $analisanPendanaan = BorrowerAnalisaPendanaan::query()
            ->where('pengajuan_id', $pengajuan->pengajuan_id)
            ->first();

        return ($analisanPendanaan !== null && $analisanPendanaan->status_verifikator >= 3);
    }
}
