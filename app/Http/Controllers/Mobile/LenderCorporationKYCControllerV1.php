<?php

namespace App\Http\Controllers\Mobile;

use App\BniEnc;
use App\Constants\KYCStatus;
use App\DetilInvestor;
use App\Events\GenerateVABankEvent;
use App\Http\Controllers\Controller;
use App\Investor;
use App\InvestorAdministrator;
use App\InvestorAdministratorInterface;
use App\InvestorContacts;
use App\InvestorCorporation;
use App\InvestorKYCStep;
use App\InvestorPemegangSaham;
use App\InvestorPengurus;
use App\MasterParameter;
use App\MasterProvinsi;
use App\Services\InvestorService;
use App\Services\KYCService;
use App\Services\OTPService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class LenderCorporationKYCControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth:api',
            'lender_type:' . DetilInvestor::TYPE_CORPORATION,
        ], [
            'only' => [
                'currentStatus',
                'getCorporation',
                'updateCorporation',
                'getAdministrator',
                'updateAdministrator',
                'getShareholder',
                'updateShareholder',
                'getContacts',
                'updateContacts',
                'profilePic',
                'updateProfilePic',
                'sendOtp',
                'updateOtp',
            ],
        ]);
    }

    public function allStatus()
    {
        return response()->json(
            InvestorKYCStep::corporation()
                ->orderBy('sort')
                ->pluck('name', 'sort')
        );
    }

    private function getCurrentStatus(Investor $investor)
    {
        if ($investor->kyc_step === null) {
            /** @var InvestorKYCStep $first */
            $first = InvestorKYCStep::corporation()->orderBy('sort', 'ASC')->first();
            $investor->forceFill([
                'kyc_step' => $first->name,
            ])
                ->save();
        }

        /** @var InvestorKYCStep $status */
        $status = InvestorKYCStep::corporation()->where('name', $investor->kyc_step)->first();
        
        if ($status === null) {
            $investor->forceFill([
                'kyc_step' => InvestorKYCStep::STEP_COR_AWAL
            ]);
            
            return  [-1, InvestorKYCStep::STEP_COR_AWAL];
        }

        return [$status->sort, $status->name];
    }

    public function currentStatus(Request $request)
    {
        $investor = InvestorService::getInvestor();

        if ($investor->status === Investor::STATUS_ACTIVE) {
            $investor->forceFill(['kyc_step' => InvestorKYCStep::STEP_COR_ACTIVE,])->save();
        }

        [$stepOrder, $stepName] = $this->getCurrentStatus($investor);
        $status['verify_status'] = $investor->status;

        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $corporationIncompleteFields = $administratorIncompleteFields = $shareholderIncompleteFields = $contactIncompleteFields = [];
        $status = [
            'corporation' => $this->checkCorporation($detilInvestor, $corporationIncompleteFields),
            'pengurus' => $this->checkAdministrator($investor, $administratorIncompleteFields),
            'pemegang_saham' => $this->checkShareholder($investor, $shareholderIncompleteFields),
            'contact' => $this->checkContact($investor, $contactIncompleteFields),
            'step_order' => $stepOrder,
            'step' => $stepName,
            'account_status' => $investor->status,
            'incomplete_fields' => [
                'corporation' => $corporationIncompleteFields,
                'pengurus' => $administratorIncompleteFields,
                'pemegang_saham' => $shareholderIncompleteFields,
                'contact' => $contactIncompleteFields,
            ],
        ];

        return response()->json($status);
    }

    private function checkCorporation(DetilInvestor $detilInvestor, &$incompleteFields = [])
    {
        $neededColumns = [
            InvestorCorporation::NAMA,
            InvestorCorporation::NO_SURAT_IZIN,
            InvestorCorporation::NPWP,
            InvestorCorporation::NO_AKTA_PENDIRIAN,
            InvestorCorporation::TELEPON,
            InvestorCorporation::ALAMAT,
            InvestorCorporation::PROVINSI,
            InvestorCorporation::KABUPATEN,
            InvestorCorporation::KECAMATAN,
            InvestorCorporation::KELURAHAN,
            InvestorCorporation::KODE_POS,
            InvestorCorporation::REKENING,
            InvestorCorporation::BANK,
            InvestorCorporation::NAMA_PEMILIK_REKENING,
            InvestorCorporation::BIDANG_PEKERJAAN,
            InvestorCorporation::OMSET_TAHUN_TERAKHIR,
            InvestorCorporation::TOTAL_ASET_TAHUN_TERAKHIR,
            InvestorCorporation::FOTO_NPWP,
            InvestorCorporation::TANGGAL_AKTA_PENDIRIAN,
            InvestorCorporation::BERKEDUDUKAN,
            InvestorCorporation::NAMA_NOTARIS_AKTA_PENDIRIAN,
            InvestorCorporation::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN,
            InvestorCorporation::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
        ];

        $columns = collect(InvestorCorporation::getDetilInvestorMap())
            ->intersectByKeys(array_flip($neededColumns))
            ->values()
            ->toArray();

        return KYCService::baseCheck($columns, $detilInvestor, $incompleteFields);
    }

    public function getCorporation(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();

        $investorIdOriginalOwner = $investor->id;
        $check = self::picCheck($investorIdOriginalOwner);

        $data = $investor->getData([
            InvestorCorporation::NAMA,
            InvestorCorporation::NO_SURAT_IZIN,
            InvestorCorporation::NPWP,
            InvestorCorporation::NO_AKTA_PENDIRIAN,
            InvestorCorporation::TANGGAL_AKTA_PENDIRIAN,
            InvestorCorporation::NO_AKTA_PERUBAHAN,
            InvestorCorporation::TANGGAL_AKTA_PERUBAHAN,
            InvestorCorporation::TELEPON,
            InvestorCorporation::ALAMAT,
            InvestorCorporation::PROVINSI,
            InvestorCorporation::KABUPATEN,
            InvestorCorporation::KECAMATAN,
            InvestorCorporation::KELURAHAN,
            InvestorCorporation::KODE_POS,
            InvestorCorporation::REKENING,
            InvestorCorporation::BANK,
            InvestorCorporation::NAMA_PEMILIK_REKENING,
            InvestorCorporation::BIDANG_PEKERJAAN,
            InvestorCorporation::OMSET_TAHUN_TERAKHIR,
            InvestorCorporation::TOTAL_ASET_TAHUN_TERAKHIR,
            InvestorCorporation::FOTO_NPWP,
            InvestorCorporation::LAPORAN_KEUANGAN,
            InvestorCorporation::LONG,
            InvestorCorporation::LAT,
            InvestorCorporation::ALT,
            InvestorCorporation::BERKEDUDUKAN,
            InvestorCorporation::NAMA_NOTARIS_AKTA_PENDIRIAN,
            InvestorCorporation::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN,
            InvestorCorporation::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            InvestorCorporation::NAMA_NOTARIS_AKTA_PERUBAHAN,
            InvestorCorporation::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN,
            InvestorCorporation::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
            InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
        ])
            ->toArray();

        InvestorCorporation::fieldFileAccess($data, InvestorCorporation::FOTO_NPWP, $check);
        InvestorCorporation::fieldFileAccess($data, InvestorCorporation::LAPORAN_KEUANGAN, $check);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_AKTA_PENDIRIAN);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_AKTA_PERUBAHAN);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN);

        return response()->json(
            array_merge($data, [
                'provinsi_nama' => optional(
                    MasterProvinsi::query()
                        ->where('kode_provinsi', $investor->detilInvestor->provinsi_perusahaan)
                        ->first(),
                    function ($value) {
                        return $value->nama_provinsi;
                    }
                ),
                'kabupaten_nama' => optional(
                    MasterProvinsi::query()->where('kode_kota', $investor->detilInvestor->kota_perusahaan)->first(),
                    function ($value) {
                        return $value->nama_kota;
                    }
                ),
            ])
        );
    }

    public function updateCorporation(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $detilInvestorLokasi = InvestorService::getDetilInvestorLokasi($investor);

        DB::beginTransaction();
        try {
            Validator::make(
                $request->all(),
                InvestorCorporation::getRules($investor->id),
                [],
                InvestorCorporation::getAttributeName()
            )
                ->validate();

            InvestorCorporation::additionalValidator($request, $investor);

            $detilInvestorMap = InvestorCorporation::getDetilInvestorMap();
            $detilInvestor
                ->forceFill(InvestorService::mapToDatabase($request->toArray(), $detilInvestorMap))
                ->forceFill([
                    $detilInvestorMap[InvestorCorporation::TANGGAL_AKTA_PENDIRIAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_AKTA_PENDIRIAN),
                        $detilInvestor->tanggal_akta_pendirian
                    ),
                    $detilInvestorMap[InvestorCorporation::TANGGAL_AKTA_PERUBAHAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_AKTA_PERUBAHAN),
                        $detilInvestor->tanggal_akta_perubahan
                    ),
                    $detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN),
                        $detilInvestor->{$detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN]}
                    ),
                    $detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN),
                        $detilInvestor->{$detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN]}
                    ),
                    'tipe_pengguna' => DetilInvestor::TYPE_CORPORATION,
                ])
                ->save();

            $detilInvestorLokasi
                ->forceFill(InvestorService::mapToDatabase($request->toArray(), InvestorCorporation::getLokasiMap()))
                ->save();

            $photoMessages = [];
            if ($file = $request->file(InvestorCorporation::FOTO_NPWP)) {
                $photoMessages[InvestorCorporation::FOTO_NPWP] = InvestorCorporation::upload(
                    $detilInvestorMap[InvestorCorporation::FOTO_NPWP],
                    $file,
                    $detilInvestor,
                    'pic_user_npwp_investor'
                );
            }

            if ($file = $request->file(InvestorCorporation::LAPORAN_KEUANGAN)) {
                $photoMessages[InvestorCorporation::LAPORAN_KEUANGAN] = InvestorCorporation::uploadLapKeuangan(
                    $detilInvestorMap[InvestorCorporation::LAPORAN_KEUANGAN],
                    $file,
                    $detilInvestor,
                    'laporan_keuangan'
                );
            }

            self::bumpStatus($investor, InvestorKYCStep::STEP_COR_DATA);

            DB::commit();

            return response()->json([
                'success' => 'Data diri berhasil disimpan',
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();
            Log::critical($exception);

            return response()->json([
                'error' => config('app.env') !== 'production'
                    ? $exception->getMessage()
                    : 'Terjadi kesalahan, silahkan coba lagi',
            ]);
        }
    }

    /**
     * @param \App\Investor $investor
     * @param \App\InvestorAdministratorInterface $administrator
     * @param $incompleteFields
     * @return string
     */
    private function checkGenericPerson(
        Investor $investor,
        InvestorAdministratorInterface $administrator,
        &$incompleteFields
    ) {
        $neededColumns = [
            $administrator::NAMA,
            $administrator::JENIS_KELAMIN,
            $administrator::JENIS_IDENTITAS,
            $administrator::IDENTITAS,
            $administrator::TEMPAT_LAHIR,
            $administrator::TANGGAL_LAHIR,
            $administrator::KODE_OPERATOR,
            $administrator::NO_TELEPON,
            $administrator::AGAMA,
            $administrator::PENDIDIKAN,
            $administrator::NPWP,
            $administrator::JABATAN,
            $administrator::ALAMAT,
            $administrator::PROVINSI,
            $administrator::KABUPATEN,
            $administrator::KECAMATAN,
            $administrator::KELURAHAN,
            $administrator::KODE_POS,
            $administrator::FOTO_KTP,
            $administrator::FOTO_PROFILE,
            $administrator::FOTO_PROFILE_KTP,
            $administrator::FOTO_NPWP,
        ];

        $rel = $administrator::getRequestIdentifier();

        $incompleteFields = [];
        $checks = [];
        foreach ($investor->$rel()->oldest()->get() as $key => $admin) {
            $neededColumns = $this->getPemegangSahamModifier($admin, $neededColumns);

            $columns = collect($admin::getAdministratorDbMap())
                ->intersectByKeys(array_flip($neededColumns))
                ->values()
                ->toArray();

            $incompleteFields[$key] = [];
            $checks[] = KYCService::baseCheck($columns, $admin, $incompleteFields[$key]);
        }

        return KYCService::multipleCheck($checks);
    }

    private function checkAdministrator(Investor $investor, &$incompleteFields = [])
    {
        return $this->checkGenericPerson($investor, new InvestorPengurus(), $incompleteFields);
    }

    private function checkShareholder(Investor $investor, &$incompleteFields = [])
    {
        return $this->checkGenericPerson($investor, new InvestorPemegangSaham(), $incompleteFields);
    }

    private function checkContact(Investor $investor, &$incompleteFields = [])
    {
        return $this->checkGenericPerson($investor, new InvestorContacts(), $incompleteFields);
    }

    private function getGenericPerson(InvestorCorporation $investor, Collection $investorAdmins)
    {
        $investorIdOriginalOwner = $investor->id;

        $check = self::picCheck($investorIdOriginalOwner);

        $data = $investorAdmins->map(function (InvestorAdministratorInterface $administrator) use ($check) {
            $neededColumns = [
                $administrator::NAMA,
                $administrator::JENIS_KELAMIN,
                $administrator::JENIS_IDENTITAS,
                $administrator::IDENTITAS,
                $administrator::TEMPAT_LAHIR,
                $administrator::TANGGAL_LAHIR,
                $administrator::KODE_OPERATOR,
                $administrator::NO_TELEPON,
                $administrator::AGAMA,
                $administrator::PENDIDIKAN,
                $administrator::NPWP,
                $administrator::JABATAN,
                $administrator::ALAMAT,
                $administrator::PROVINSI,
                $administrator::KABUPATEN,
                $administrator::KECAMATAN,
                $administrator::KELURAHAN,
                $administrator::KODE_POS,
                $administrator::FOTO_KTP,
                $administrator::FOTO_PROFILE_KTP,
                $administrator::FOTO_PROFILE,
                $administrator::FOTO_NPWP,
            ];

            $neededColumns = $this->getPemegangSahamModifier($administrator, $neededColumns);
            $datum = $administrator->getData($neededColumns);

            $administrator::fieldFileAccess($datum, $administrator::FOTO_NPWP, $check);
            $administrator::fieldFileAccess($datum, $administrator::FOTO_PROFILE, $check);
            $administrator::fieldFileAccess($datum, $administrator::FOTO_PROFILE_KTP, $check);
            $administrator::fieldFileAccess($datum, $administrator::FOTO_KTP, $check);
            $administrator::fieldDateAccess($datum, $administrator::TANGGAL_LAHIR);
            $administrator::fieldPhoneAccess($datum, $administrator::NO_TELEPON, $administrator::KODE_OPERATOR);

            return array_merge($datum->toArray(), [
                'provinsi_nama' => optional(
                    MasterProvinsi::query()->where('kode_provinsi', $datum[InvestorAdministrator::PROVINSI])->first(),
                    function ($value) {
                        return $value->nama_provinsi;
                    }
                ),
                'kabupaten_nama' => optional(
                    MasterProvinsi::query()->where('kode_kota', $datum[InvestorAdministrator::KABUPATEN])->first(),
                    function ($value) {
                        return $value->nama_kota;
                    }
                ),
            ]);
        });

        return response()->json($data);
    }

    public function getAdministrator(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $investorAdmins = $investor->admins;

        return $this->getGenericPerson($investor, $investorAdmins);
    }

    public function getShareholder(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $shareHolders = $investor->shareholders;

        return $this->getGenericPerson($investor, $shareHolders);
    }

    public function getContacts(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $contacts = $investor->contacts;

        return $this->getGenericPerson($investor, $contacts);
    }

    private function updateGenericPerson(
        Request $request,
        Investor $investor,
        InvestorAdministratorInterface $administrator
    ) {
        DB::beginTransaction();
        $requestKey = $administrator::getRequestIdentifier();
        $administratorMap = $administrator::getAdministratorDbMap();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        try {
            $detilInvestor->forceFill([
                'tipe_pengguna' => DetilInvestor::TYPE_CORPORATION,
            ])
                ->save();

            $data = $request->all();
            $neededRules = [];
            InvestorCorporation::validator($data, $administrator, $neededRules, $investor);

            $createdAtOld = $investor->$requestKey()->pluck('created_at');
            $investor->$requestKey()->delete();
            $photoMessages = [];
            foreach ($request->get($requestKey) ?? [] as $key => $adminRequest) {
                $administrator::additionalValidator($adminRequest, $administrator, $requestKey, $key);

                $admin = $investor->$requestKey()->make()
                    ->forceFill(InvestorService::mapToDatabase($adminRequest, $administratorMap))
                    ->forceFill([
                        'investor_id' => $investor->id,
                        $administratorMap[$administrator::TANGGAL_LAHIR] => $administrator::fieldDateMutate(
                            $adminRequest[$administrator::TANGGAL_LAHIR] ?? null,
                            $administrator->tanggal_lahir
                        ),
                        $administratorMap[$administrator::NO_TELEPON] => $administrator::fieldPhoneMutate(
                            $adminRequest[$administrator::NO_TELEPON] ?? null,
                            $adminRequest[$administrator::KODE_OPERATOR] ?? null
                        ),
                    ]);

                $mapRequestToColumnPhoto = array_intersect_key(
                    $administratorMap,
                    array_flip([
                        $administrator::FOTO_KTP,
                        $administrator::FOTO_PROFILE_KTP,
                        $administrator::FOTO_PROFILE,
                        $administrator::FOTO_NPWP,
                    ])
                );

                $photoMessages[$key] = [];
                foreach ($mapRequestToColumnPhoto as $requestField => $columnKey) {
                    if ($file = $request->file("$requestKey.$key.$requestField")) {
                        $photoMessages[$key][$requestField] = InvestorCorporation::upload(
                            $columnKey,
                            $file,
                            $admin,
                            "pic_corp_{$requestKey}_{$key}_$columnKey"
                        );
                    }
                }

                /** @var \Illuminate\Database\Eloquent\Model $admin */
                $admin->timestamps = false;
                $admin->forceFill([
                    $admin::CREATED_AT => $createdAtOld[$key] ?? Carbon::now(),
                    $admin::UPDATED_AT => $admin->{$admin->getUpdatedAtColumn()} ?? Carbon::now(),
                ])
                    ->save();
            }

            self::bumpStatus($investor, InvestorKYCStep::STEP_COR_DATA);

            DB::commit();

            $readableKey = $administrator::READABLE_KEY;

            return response()->json([
                'success' => "Data $readableKey berhasil disimpan",
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();
            Log::critical($exception);

            return response()->json([
                'error' => config('app.env') !== 'production'
                    ? $exception->getMessage()
                    : 'Terjadi kesalahan, silahkan coba lagi',
            ]);
        }
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateAdministrator(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();

        return $this->updateGenericPerson($request, $investor, new InvestorPengurus());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateShareholder(Request $request)
    {
        $investor = InvestorService::getInvestor();

        return $this->updateGenericPerson($request, $investor, new InvestorPemegangSaham());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateContacts(Request $request)
    {
        $investor = InvestorService::getInvestor();

        return $this->updateGenericPerson($request, $investor, new InvestorContacts());
    }

    public function sendOtp(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        if (KYCService::multipleCheck([
                $this->checkCorporation($detilInvestor),
                $this->checkAdministrator($investor),
                $this->checkShareholder($investor),
                $this->checkContact($investor),
            ]) !== KYCStatus::COMPLETE_TRUE) {
            return response()->json(['error' => 'pengisian KYC harus selesai']);
        }

        $investor = InvestorService::getInvestor();
        $phone = OTPService::trim($request->no_telp);
        
        if (App::isLocale('en')) {
            $message = 'Phone :input is already registered';
        } else {
            $message = 'Nomor ponsel :input sudah terdaftar';
        }

        Validator::make([
            'no_telp' => $phone,
        ], [
            'no_telp' => [
                Rule::unique('investor_corp_contact', 'no_tlp')->ignore(
                    $detilInvestor->investor_id,
                    'investor_id'
                ),
                Rule::unique('detil_investor', 'phone_investor'),
            ],
        ], [
            'no_telp.unique' => $message
        ])
            ->validate();

        [$status, $message] = OTPService::investorSend($investor, $request->no_telp);
        // [$status, $message] = [true, 'test'];

        if ($status) {
            self::bumpStatus($investor, InvestorKYCStep::STEP_IND_OTP);

            return response()->json(['success' => $message]);
        }

        return response()->json(['error' => $message]);
    }

    public function updateOtp(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();

        if ((string) $request->no_otp === (string) $investor->otp) {
            if (MasterParameter::getParam(MasterParameter::INVESTOR_KYC_MODE) === 'automatic') {

                $this->generateVA($investor);

                $investor->forceFill(['status' => Investor::STATUS_ACTIVE])->save();
                self::bumpStatus($investor, InvestorKYCStep::STEP_IND_ACTIVE);
            } else {
                self::bumpStatus($investor, InvestorKYCStep::STEP_IND_VERIFICATION);
            }

            return response()->json(['status' => 'success']);
        }

        return response()->json(['error' => 'OTP yang anda masukan salah']);
    }

    private function generateVA(InvestorCorporation $investor)
    {
        $date = Carbon::now()->addYear(4);
        $user = $investor;

        event(new GenerateVABankEvent($user));

        $data = [
            'type' => 'createbilling',
            'client_id' => config('app.bni_id'),
            'trx_id' => $user->id,
            'trx_amount' => '0',
            'customer_name' => $user->getDataSingle(InvestorCorporation::NAMA),
            'customer_email' => $user->email,
            'virtual_account' => '8' . config('app.bni_id') . $user->getVANumber(),
            'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
            'billing_type' => 'o',
        ];

        $encrypted = BniEnc::encrypt($data, config('app.bni_id'), config('app.bni_key'));

        $client = new \GuzzleHttp\Client(); //GuzzleHttp\Client
        $result = $client->post(config('app.bni_url'), [
            'json' => [
                'client_id' => config('app.bni_id'),
                'data' => $encrypted,
            ],
        ]);

        $result = json_decode($result->getBody()->getContents());
        if ($result->status !== '000') {
            $user->RekeningInvestor()->create([
                'investor_id' => $user->id,
                'total_dana' => 0,
                'va_number' => "",
                'kode_bank' => '009',
                'unallocated' => 0,
            ]);

            // insert log db app
            $insertLog = DB::table("log_db_app")->insert([
                "file_name" => "Mobile/LenderKYCControllerV1.php",
                "line" => "621",
                "description" => "Gagal Generate VA BNIK" . $user->id . " " . json_encode($result) . "",
                "created_at" => date("Y-m-d H:i:s"),
            ]);

            return false;
        }

        $decrypted = BniEnc::decrypt($result->data, config('app.bni_id'), config('app.bni_key'));
        //return json_encode($decrypted);
        $user->RekeningInvestor()->create([
            'investor_id' => $user->id,
            'total_dana' => 0,
            'va_number' => $decrypted['virtual_account'],
            'kode_bank' => '009',
            'unallocated' => 0,
        ]);

        // insert log db app
        $insertLog = DB::table("log_db_app")->insert([
            "file_name" => "mobile/newapiauthcontroller.php",
            "line" => "1971",
            "description" => "Sukses Generate VA BNIK" . $user->id . " " . json_encode($decrypted) . "",
            "created_at" => date("Y-m-d H:i:s"),
        ]);

        return true;
        // return view('pages.user.add_funds')->with('message','VA Generate Success!');
    }

    private static function bumpStatus(Investor $investor, string $newStatus)
    {
        $statuses = InvestorKYCStep::corporation()->whereIn('name', [$newStatus, $investor->kyc_step])->pluck(
            'sort',
            'name'
        );

        $current = $statuses[$investor->kyc_step];
        $new = $statuses[$newStatus];

        if ($new > $current) {
            $investor->forceFill([
                'kyc_step' => $newStatus,
            ])
                ->save();
        }
    }

    private static function picCheck($investorIdOriginalOwner)
    {
        return function ($investorId) use ($investorIdOriginalOwner) {
            return $investorId === $investorIdOriginalOwner;
        };
    }

    /**
     * @param \App\InvestorAdministratorInterface $administrator
     * @param array $neededColumns
     * @return array|int[]|string[]
     */
    private function getPemegangSahamModifier(
        InvestorAdministratorInterface $administrator,
        array $neededColumns
    ): array {
        if (! $administrator instanceof InvestorPemegangSaham) {
            return $neededColumns;
        }

        $neededColumns[] = InvestorPemegangSaham::JENIS_PEMEGANG_SAHAM;
        $neededColumns[] = InvestorPemegangSaham::NILAI_SAHAM;
        $neededColumns[] = InvestorPemegangSaham::LEMBAR_SAHAM;

        if ($administrator->{InvestorPemegangSaham::JENIS_PEMEGANG_SAHAM} === InvestorPemegangSaham::PMG_SHM_BDN_HKM) {
            $temp = array_flip($neededColumns);
            unset(
                $temp[$administrator::AGAMA],
                $temp[$administrator::JENIS_KELAMIN],
                $temp[$administrator::TEMPAT_LAHIR],
                $temp[$administrator::TANGGAL_LAHIR],
                $temp[$administrator::PENDIDIKAN],
                $temp[$administrator::JABATAN],
                $temp[$administrator::FOTO_KTP],
                $temp[$administrator::FOTO_PROFILE_KTP],
                $temp[$administrator::FOTO_PROFILE]
            );

            $neededColumns = array_flip($temp);
        }

        return $neededColumns;
    }
}
