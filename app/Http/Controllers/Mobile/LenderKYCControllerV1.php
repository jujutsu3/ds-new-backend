<?php

namespace App\Http\Controllers\Mobile;

use App\AhliWarisInvestor;
use App\BniEnc;
use App\Constants\JenisKelamin;
use App\Constants\KYCStatus;
use App\DetilInvestor;
use App\Events\GenerateVABankEvent;
use App\Http\Controllers\Controller;
use App\Investor;
use App\InvestorContacts;
use App\InvestorIndividu;
use App\InvestorKYCStep;
use App\MasterBank;
use App\MasterNegara;
use App\MasterParameter;
use App\MasterProvinsi;
use App\Services\InvestorService;
use App\Services\KYCService;
use App\Services\LenderFileService;
use App\Services\OTPService;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class LenderKYCControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth:api',
            'lender_type:' . DetilInvestor::TYPE_INDIVIDU,
        ], [
            'only' => [
                'currentStatus',
                'profile', 'updateProfile',
                'profilePic', 'updateProfilePic',
                'education', 'updateEducation',
                'inheritance', 'updateInheritance',
                'sendOtp', 'updateOtp',
            ],
        ]);
    }

    public function allStatus()
    {
        return response()->json(
            InvestorKYCStep::individu()
                ->orderBy('sort')
                ->pluck('name', 'sort')
        );
    }

    private function getCurrentStatus(Investor $investor)
    {
        if ($investor->kyc_step === null) {
            /** @var InvestorKYCStep $first */
            $first = InvestorKYCStep::individu()->orderBy('sort', 'ASC')->first();
            $investor->forceFill([
                'kyc_step' => $first->name,
            ])
                ->save();
        }

        /** @var InvestorKYCStep $status */
        $status = InvestorKYCStep::individu()->where('name', $investor->kyc_step)->first();

        return [$status->sort, $status->name];
    }

    public function currentStatus(Request $request)
    {
        $investor = InvestorService::getInvestor();

        if ($investor->status === Investor::STATUS_ACTIVE) {
            $investor->forceFill([
                'kyc_step' => InvestorKYCStep::STEP_IND_ACTIVE,
            ])->save();
        }

        [$stepOrder, $stepName] = $this->getCurrentStatus($investor);
        $status['verify_status'] = $investor->status;

        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $ahliWaris = InvestorService::getAhliWaris();
        $profileIncompleteFields = $educationIncompleteFields = $inheritanceIncompleteFields = [];
        $status = [
            'profile' => $this->profileCheck($detilInvestor, $profileIncompleteFields),
            // 'profilePic' => $this->profilePicCheck($detilInvestor),
            'education' => $this->educationCheck($detilInvestor, $educationIncompleteFields),
            'inheritance' => $this->inheritanceCheck($detilInvestor, $ahliWaris, $inheritanceIncompleteFields),
            'step_order' => $stepOrder,
            'step' => $stepName,
            'account_status' => $investor->status,
            'incomplete_fields' => [
                'profile' => $profileIncompleteFields,
                'education' => $educationIncompleteFields,
                'inheritance' => $inheritanceIncompleteFields,
            ],
        ];

        return response()->json($status);
    }

    private function profileCheck(DetilInvestor $detilInvestor, &$incompleteFields = [])
    {
        $columns = [
            ['no_ktp_investor', 'no_passpor_investor'],
            'jenis_kelamin_investor',
            'nama_investor',
            'tempat_lahir_investor',
            'tgl_lahir_investor',
            'agama_investor',
            'status_kawin_investor',
            'warganegara',
            'domisili_negara',
            'alamat_investor',
            'nama_ibu_kandung',
        ];

        // check if not indonesian
        if ((int) $detilInvestor->domisili_negara === MasterNegara::INDONESIA) {
            $columns = array_merge($columns, [
                'provinsi_investor',
                'kecamatan',
                'kota_investor',
                'kelurahan',
                'kode_pos_investor',
            ]);
        }

        return KYCService::multipleCheck([
            KYCService::baseCheck($columns, $detilInvestor, $incompleteFields),
            $this->profilePicCheck($detilInvestor, $incompleteFields),
        ]);
    }

    public function profile(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $detilInvestorLokasi = InvestorService::getDetilInvestorLokasi($investor);

        return response()->json([
            'jenis_identitas' => $detilInvestor->jenis_identitas,
            'nik' => $detilInvestor->no_ktp_investor,
            'no_paspor' => $detilInvestor->no_passpor_investor,
            'nama' => $detilInvestor->nama_investor,
            'tempat_lahir' => $detilInvestor->tempat_lahir_investor,
            'tanggal_lahir' => $detilInvestor->tgl_lahir_investor,
            'jenis_kelamin' => $detilInvestor->jenis_kelamin_investor,
            'agama' => $detilInvestor->agama_investor,
            'status_pernikahan' => $detilInvestor->status_kawin_investor,
            'kewarganegaraan' => $detilInvestor->domisili_negara,
            'warga_negara' => $detilInvestor->warganegara,
            'alamat' => $detilInvestor->alamat_investor,
            'provinsi' => $detilInvestor->provinsi_investor,
            /** provinsi dan kabupaten menggunakan kode */
            'provinsi_nama' => optional(MasterProvinsi::query()->where('kode_provinsi', $detilInvestor->provinsi_investor)->first(), function ($value) {
                return $value->nama_provinsi;
            }),
            'kecamatan' => $detilInvestor->kecamatan,
            'kabupaten' => $detilInvestor->kota_investor,
            'kabupaten_nama' => optional(MasterProvinsi::query()->where('kode_kota', $detilInvestor->kota_investor)->first(), function ($value) {
                return $value->nama_kota;
            }),
            'kelurahan' => $detilInvestor->kelurahan,
            'kode_pos' => $detilInvestor->kode_pos_investor ?? "",
            'long' => $detilInvestorLokasi->longitude,
            'lat' => $detilInvestorLokasi->latitude,
            'alt' => $detilInvestorLokasi->altitude,
            'ibu_kandung' => $detilInvestor->nama_ibu_kandung,
        ]);
    }

    public function updateProfile(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $detilInvestorLokasi = InvestorService::getDetilInvestorLokasi($investor);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                InvestorIndividu::NIK => ($request->filled('is_passport') && $request->get('is_passport') !== false)
                    ? ['alpha_num', 'max:9', Rule::unique('detil_investor', 'no_passpor_investor')->ignore($detilInvestor->investor_id, 'investor_id')]
                    : ['digits:16', Rule::unique('detil_investor', 'no_ktp_investor')->ignore($detilInvestor->investor_id, 'investor_id')],
                InvestorIndividu::NAMA => ['max:191'],
                InvestorIndividu::JENIS_KELAMIN => [Rule::in([JenisKelamin::LAKILAKI, JenisKelamin::PEREMPUAN])],
                InvestorIndividu::TEMPAT_LAHIR => ['max:191'],
                InvestorIndividu::TANGGAL_LAHIR => ['date_format:d-m-Y'],
                InvestorIndividu::AGAMA => [Rule::exists('m_agama', 'id_agama')],
                InvestorIndividu::STATUS_PERNIKAHAN => [Rule::exists('m_kawin', 'id_kawin')],
                InvestorIndividu::KEWARGANEGARAAN => [Rule::exists('m_negara', 'id_negara')],
                InvestorIndividu::WARGA_NEGARA => [Rule::exists('m_negara', 'id_negara')],
                InvestorIndividu::ALAMAT => ['max:191'],
                InvestorIndividu::PROVINSI => [Rule::exists('m_provinsi_kota', 'kode_provinsi')],
                InvestorIndividu::KABUPATEN => [Rule::exists('m_provinsi_kota', 'kode_kota')],
                InvestorIndividu::KECAMATAN => [],
                InvestorIndividu::KELURAHAN => [],
                InvestorIndividu::KODE_POS => ['nullable', 'numeric', 'digits:5', Rule::exists('m_kode_pos', 'kode_pos')],
                InvestorIndividu::LONG => [],
                InvestorIndividu::LAT => [],
                InvestorIndividu::ALT => [],
            ], [
                'nik.unique' => 'No NIK Sudah Terdaftar',
                'nik.digits' => 'Harap masukkan setidaknya 16 digit',
            ], InvestorIndividu::getAttributeName())
                ->validate();

            if ($request->filled('nik')) {
                if (! $request->filled('is_passport')) {
                    $nikData = KYCService::parseNik($request->get('nik'));
                    $dob = Carbon::parse($nikData['dob'])->format('d-m-Y');
                    $gender = $nikData['gender'];
                }
                $detilInvestor->forceFill([
                    'no_ktp_investor' => null,
                    'no_passpor_investor' => null,
                ])->save();
            }

            $detilInvestor->forceFill(InvestorService::mapToDatabase($request->toArray(), [
                'nik' => $request->filled('is_passport')
                    ? 'no_passpor_investor'
                    : 'no_ktp_investor',
                'nama' => 'nama_investor',
                'tempat_lahir' => 'tempat_lahir_investor',
                'agama' => 'agama_investor',
                'status_pernikahan' => 'status_kawin_investor',
                'alamat' => 'alamat_investor',
                'provinsi' => 'provinsi_investor',
                'kecamatan' => 'kecamatan',
                'kabupaten' => 'kota_investor',
                'kelurahan' => 'kelurahan',
                'kode_pos' => 'kode_pos_investor',
                'ibu_kandung' => 'nama_ibu_kandung',
            ]))
                ->forceFill([
                    'tgl_lahir_investor' => $dob ?? ($request->filled('tanggal_lahir') ? Carbon::createFromFormat('d-m-Y', $request->get('tanggal_lahir'))->format('d-m-Y') : $detilInvestor->tgl_lahir_investor),
                    'tipe_pengguna' => DetilInvestor::TYPE_INDIVIDU,
                    'jenis_kelamin_investor' => $gender ?? $request->get('jenis_kelamin') ?: $detilInvestor->jenis_kelamin_investor,
                    'jenis_identitas' => $request->filled('is_passport') ? DetilInvestor::JENIS_PASPORT : DetilInvestor::JENIS_NIK,
                    'domisili_negara' => $request->filled('kewarganegaraan') ? $request->get('kewarganegaraan') : $detilInvestor->domisili_negara,
                    'warganegara' => $request->filled('warga_negara')
                        ? $request->get('warga_negara')
                        : ($detilInvestor->warganegara ?: $request->get('kewarganegaraan')),
                ])
                ->save();

            if ((int) $detilInvestor->domisili_negara !== MasterNegara::INDONESIA) {
                $detilInvestor->forceFill([
                    'kelurahan' => null,
                    'kecamatan' => null,
                    'kode_pos_investor' => null,
                    'kota_investor' => MasterProvinsi::KOTA_LAINLAIN,
                    'provinsi_investor' => MasterProvinsi::PROVINSI_LAINLAIN,
                ])
                    ->save();
            }

            $detilInvestorLokasi->forceFill(InvestorService::mapToDatabase($request->toArray(), [
                'long' => 'longitude',
                'lat' => 'latitude',
                'alt' => 'altitude',
            ]))->save();

            self::bumpStatus($investor, InvestorKYCStep::STEP_IND_DATA);

            DB::commit();

            return response()->json(['success' => 'Data diri berhasil disimpan']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public function profilePic()
    {
        $detilInvestor = InvestorService::getDetilInvestor(InvestorService::getInvestor());
        $investorIdOriginalOwner = $detilInvestor->investor_id;

        $check = static function ($investorId) use ($investorIdOriginalOwner) {
            return $investorId === $investorIdOriginalOwner;
        };

        return response()->json([
            'ktp' => $detilInvestor->pic_ktp_investor,
            'ktp_full' => LenderFileService::makeUrl($detilInvestor->pic_ktp_investor, $check),
            'profile_ktp' => $detilInvestor->pic_user_ktp_investor,
            'profile_ktp_full' => LenderFileService::makeUrl($detilInvestor->pic_user_ktp_investor, $check),
            'profile' => $detilInvestor->pic_investor,
            'profile_full' => LenderFileService::makeUrl($detilInvestor->pic_investor, $check),
        ]);
    }

    public function updateProfilePic(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        Validator::make($request->all(), [
            'ktp' => ['mimes:jpeg,jpg,bmp,png'],
            'profile_ktp' => ['mimes:jpeg,jpg,bmp,png'],
            'profile' => ['mimes:jpeg,jpg,bmp,png'],
        ])
            ->validate();

        $messages = [];
        if ($request->hasFile('profile')) {
            $detilInvestor->forceFill(['pic_investor' =>
                LenderFileService::uploadFile($request->file('profile'), 'pic_investor', $detilInvestor->investor_id),
            ])->save();
            $messages['profile'] = 'success';
        }

        if ($request->hasFile('ktp')) {
            $detilInvestor->forceFill(['pic_ktp_investor' =>
                LenderFileService::uploadFile($request->file('ktp'), 'pic_ktp_investor', $detilInvestor->investor_id),
            ])->save();
            $messages['ktp'] = 'success';
        }

        if ($request->hasFile('profile_ktp')) {
            $detilInvestor->forceFill(['pic_user_ktp_investor' =>
                LenderFileService::uploadFile($request->file('profile_ktp'), 'pic_user_ktp_investor', $detilInvestor->investor_id),
            ])->save();
            $messages['profile_ktp'] = 'success';
        }

        $detilInvestor->forceFill([
            'tipe_pengguna' => DetilInvestor::TYPE_INDIVIDU,
        ]);

        self::bumpStatus($investor, InvestorKYCStep::STEP_IND_DATA);

        return response()->json($messages);
    }

    private function profilePicCheck(DetilInvestor $detilInvestor, &$incompleteFields = [])
    {
        $columns = [
            'pic_ktp_investor',
            'pic_user_ktp_investor',
            'pic_investor',
        ];

        return KYCService::baseCheck($columns, $detilInvestor, $incompleteFields);
    }

    public function education(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        return response()->json([
            'pendidikan' => $detilInvestor->pendidikan_investor,
            'pekerjaan' => $detilInvestor->pekerjaan_investor,
            'bidang_pekerjaan' => $detilInvestor->bidang_pekerjaan,
            'bidang_online' => $detilInvestor->online_investor,
            'lama_kerja' => $detilInvestor->pengalaman_investor,
            'estimasi_penghasilan' => $detilInvestor->pendapatan_investor,
            'sumber_penghasilan' => $detilInvestor->sumber_dana,
            'npwp' => $detilInvestor->no_npwp_investor,
            'rekening' => $detilInvestor->rekening,
            'bank' => $detilInvestor->bank_investor,
            'is_valid_npwp' => $detilInvestor->is_valid_npwp,
            'nama_pemilik' => $detilInvestor->nama_pemilik_rek,
        ]);
    }

    public function updateEducation(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $noNpwpInvestor = $detilInvestor->no_npwp_investor;

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                InvestorIndividu::PENDIDIKAN => [Rule::exists('m_pendidikan', 'id_pendidikan')],
                InvestorIndividu::PEKERJAAN => [Rule::exists('m_pekerjaan', 'id_pekerjaan')],
                InvestorIndividu::BIDANG_PEKERJAAN => [Rule::exists('m_bidang_pekerjaan', 'kode_bidang_pekerjaan')],
                InvestorIndividu::BIDANG_ONLINE => [Rule::exists('m_online', 'id_online')],
                InvestorIndividu::LAMA_KERJA => [Rule::exists('m_pengalaman_kerja', 'id_pengalaman_kerja')],
                InvestorIndividu::ESTIMASI_PENGHASILAN => [Rule::exists('m_pendapatan', 'id_pendapatan')],
                InvestorIndividu::SUMBER_PENGHASILAN => ['max:191'],
                InvestorIndividu::NPWP => ['nullable', 'digits_between:15,16', Rule::unique('detil_investor', 'no_npwp_investor')->ignore($detilInvestor->investor_id, 'investor_id')],
                InvestorIndividu::REKENING => ['numeric', 'digits_between:0,191'],
                InvestorIndividu::BANK => [Rule::exists('m_bank', 'kode_bank')],
                InvestorIndividu::NAMA_PEMILIK => ['max:191'],
            ], [], InvestorIndividu::getAttributeName())
                ->validate();

            $detilInvestor->forceFill(InvestorService::mapToDatabase($request->toArray(), [
                'pendidikan' => 'pendidikan_investor',
                'pekerjaan' => 'pekerjaan_investor',
                'bidang_pekerjaan' => 'bidang_pekerjaan',
                'bidang_online' => 'online_investor',
                'lama_kerja' => 'pengalaman_investor',
                'estimasi_penghasilan' => 'pendapatan_investor',
                'sumber_penghasilan' => 'sumber_dana',
                'npwp' => 'no_npwp_investor',
                'rekening' => 'rekening',
                'nama_pemilik' => 'nama_pemilik_rek',
                'tipe_pengguna' => DetilInvestor::TYPE_INDIVIDU,
            ]))
                ->forceFill([
                    'bank_investor' => $request->has('bank')
                        ? MasterBank::query()->where('kode_bank', $request->get('bank'))->first()->getAttribute('kode_bank')
                        : $detilInvestor->bank_investor,
                ])
                ->save();

            self::bumpStatus($investor, InvestorKYCStep::STEP_IND_DATA);

            //[11/05/2022] - set is_valid_npwp = null if lender update current npwp. purpose : for bridge apex get the data to re-send mitrapajak
            if (trim($noNpwpInvestor) !== trim($request->npwp)) {
                $detilInvestor->forceFill([
                    'is_valid_npwp' => null,
                ])->save();
            }

            DB::commit();

            return response()->json(['success' => 'Data pendidikan/pekerjaan berhasil disimpan']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private function educationCheck(DetilInvestor $detilInvestor, &$incompleteFields = [])
    {
        $columns = [
            'pendidikan_investor',
            'pekerjaan_investor',
            'bidang_pekerjaan',
            'online_investor',
            'pengalaman_investor',
            'pendapatan_investor',
            'sumber_dana',
            // 'no_npwp_investor',
            'rekening',
            'bank_investor',
            'nama_pemilik_rek',
        ];

        return KYCService::baseCheck($columns, $detilInvestor, $incompleteFields);
    }

    public function inheritance(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $ahliWaris = InvestorService::getAhliWaris();

        AhliWarisInvestor::fieldPhoneAccess($ahliWaris, 'no_hp_ahli_waris', 'kode_operator');

        return response()->json([
            'ahli_waris' => $ahliWaris->nama_ahli_waris,
            'nik' => $ahliWaris->nik_ahli_waris,
            'hubungan' => $ahliWaris->hubungan_keluarga_ahli_waris,
            'kode_operator' => $ahliWaris->kode_operator,
            'telp' => $ahliWaris->no_hp_ahli_waris,
            'alamat' => $ahliWaris->alamat_ahli_waris,
        ]);
    }

    public function updateInheritance(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $ahliWaris = InvestorService::getAhliWaris();

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'ibu_kandung' => ['max:191'],
                'ahli_waris' => ['max:191'],
                'nik' => ['numeric', 'digits:16'],
                'hubungan' => [Rule::exists('m_hub_ahli_waris', 'id_hub_ahli_waris')],
                'kode_operator' => [],
                'telp' => ['numeric', 'digits_between:9,15'],
                'alamat' => ['max:100'],
            ])
                ->validate();

            $ahliWaris->forceFill(InvestorService::mapToDatabase($request->toArray(), [
                'ahli_waris' => 'nama_ahli_waris',
                'nik' => 'nik_ahli_waris',
                'hubungan' => 'hubungan_keluarga_ahli_waris',
                'alamat' => 'alamat_ahli_waris',
                'kode_operator' => 'kode_operator',
            ]))
                ->forceFill([
                    'no_hp_ahli_waris' => AhliWarisInvestor::fieldPhoneMutate($request->get('telp'), $request->get('kode_operator')),
                ])
                ->save();

            $detilInvestor->forceFill([
                'tipe_pengguna' => DetilInvestor::TYPE_INDIVIDU,
            ]);

            self::bumpStatus($investor, InvestorKYCStep::STEP_IND_DATA);

            DB::commit();

            return response()->json(['success' => 'Data ahli waris berhasil disimpan']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private function inheritanceCheck(DetilInvestor $detilInvestor, AhliWarisInvestor $ahliWaris, &$incompleteFields = [])
    {
        $columns = [
            'nama_ahli_waris',
            'nik_ahli_waris',
            'hubungan_keluarga_ahli_waris',
           // 'kode_operator',
            'no_hp_ahli_waris',
            'alamat_ahli_waris',
        ];

        return KYCService::multipleCheck([
            // KYCService::baseCheck($detilColumns, $detilInvestor),
            KYCService::baseCheck($columns, $ahliWaris, $incompleteFields),
        ]);
    }

    public function sendOtp(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $ahliWaris = InvestorService::getAhliWaris();

        if (KYCService::multipleCheck([
                $this->profileCheck($detilInvestor),
                $this->educationCheck($detilInvestor),
                $this->inheritanceCheck($detilInvestor, $ahliWaris),
            ]) !== KYCStatus::COMPLETE_TRUE) {
            return response()->json(['error' => 'pengisian KYC harus selesai']);
        }

        $investor = InvestorService::getInvestor();
        $phone = OTPService::trim($request->no_telp);
        
        if (App::isLocale('en')) {
            $message = 'Phone :input is already registered';
        } else {
            $message = 'Nomor ponsel :input sudah terdaftar';
        }

        /**
         * @todo mungkin tetep harus validate kode negara
         * @see https://stackoverflow.com/questions/68540349/laravel-country-code-and-phone-number-validation
         * @see https://github.com/giggsey/libphonenumber-for-php
         */
        Validator::make([
            'no_telp' => $phone,
        ], [
            'no_telp' => [
                Rule::unique('detil_investor', 'phone_investor')->ignore($detilInvestor->investor_id, 'investor_id'),
                Rule::unique(InvestorContacts::TABLE, InvestorContacts::getAdministratorDbMap()[InvestorContacts::NO_TELEPON])
                    ->ignore($detilInvestor->investor_id, 'investor_id')
            ],
        ], [
            'no_telp.unique' => $message
        ])
            ->validate();

        [$status, $message] = OTPService::investorSend($investor, $request->no_telp);

        if ($status) {
            $detilInvestor->forceFill([
                'phone_investor' => $phone,
            ])->save();
            self::bumpStatus($investor, InvestorKYCStep::STEP_IND_OTP);

            return response()->json(['success' => $message]);
        }

        return response()->json(['error' => $message]);
    }

    public function updateOtp(Request $request)
    {
        $investor = InvestorService::getInvestor();

        if ((string) $request->no_otp === (string) $investor->otp) {
            if (MasterParameter::getParam(MasterParameter::INVESTOR_KYC_MODE) === 'automatic') {

                $this->generateVA($investor->username);

                $investor->forceFill(['status' => Investor::STATUS_ACTIVE])->save();
                self::bumpStatus($investor, InvestorKYCStep::STEP_IND_ACTIVE);
            } else {
                self::bumpStatus($investor, InvestorKYCStep::STEP_IND_VERIFICATION);
            }

            return response()->json(['status' => 'success']);
        }

        return response()->json(['error' => 'OTP yang anda masukan salah']);
    }

    public function generateVA($username)
    {
        $date = Carbon::now()->addYear(4);
        /** @var Investor $user */
        $user = Investor::query()->where('username', $username)->first();
        $data = DB::select("select distinct investor_id id from rekening_investor where investor_id = $user->id");
        if ($data) {
            return false;
        }

        event(new GenerateVABankEvent($user));

        $data = [
            'type' => 'createbilling',
            'client_id' => config('app.bni_id'),
            'trx_id' => $user->id,
            'trx_amount' => '0',
            'customer_name' => $user->detilInvestor->nama_investor,
            'customer_email' => $user->email,
            'virtual_account' => '8' . config('app.bni_id') . $user->detilInvestor->getVa(),
            'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
            'billing_type' => 'o',
        ];

        $encrypted = BniEnc::encrypt($data, config('app.bni_id'), config('app.bni_key'));

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post(config('app.bni_url'), [
            'json' => [
                'client_id' => config('app.bni_id'),
                'data' => $encrypted,
            ],
        ]);

        $result = json_decode($result->getBody()->getContents());
        if ($result->status !== '000') {
            $user->RekeningInvestor()->create([
                'investor_id' => $user->id,
                'total_dana' => 0,
                'va_number' => "",
                'kode_bank' => '009',
                'unallocated' => 0,
            ]);

            // insert log db app
            $insertLog = DB::table("log_db_app")->insert([
                "file_name" => "Mobile/LenderKYCControllerV1.php",
                "line" => "621",
                "description" => "Gagal Generate VA BNIK" . $user->id . " " . json_encode($result) . "",
                "created_at" => date("Y-m-d H:i:s"),
            ]);

            return false;
        }

        $decrypted = BniEnc::decrypt($result->data, config('app.bni_id'), config('app.bni_key'));
        //return json_encode($decrypted);
        $user->RekeningInvestor()->create([
            'investor_id' => $user->id,
            'total_dana' => 0,
            'va_number' => $decrypted['virtual_account'],
            'kode_bank' => '009',
            'unallocated' => 0,
        ]);

        // insert log db app
        $insertLog = DB::table("log_db_app")->insert([
            "file_name" => "mobile/newapiauthcontroller.php",
            "line" => "1971",
            "description" => "Sukses Generate VA BNIK" . $user->id . " " . json_encode($decrypted) . "",
            "created_at" => date("Y-m-d H:i:s"),
        ]);

        return true;
        // return view('pages.user.add_funds')->with('message','VA Generate Success!');
    }

    private static function bumpStatus(Investor $investor, string $newStatus)
    {
        $statuses = InvestorKYCStep::individu()->whereIn('name', [$newStatus, $investor->kyc_step])->pluck('sort', 'name');

        $current = $statuses[$investor->kyc_step];
        $new = $statuses[$newStatus];

        if ($new > $current) {
            $investor->forceFill([
                'kyc_step' => $newStatus,
            ])
                ->save();
        }
    }
}
