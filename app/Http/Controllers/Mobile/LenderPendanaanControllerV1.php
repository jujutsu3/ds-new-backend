<?php

namespace App\Http\Controllers\Mobile;

use Carbon\Carbon;
use App\IhListImbalUser;
use App\MasterParameter;
use App\IhDetilImbalUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LenderPendanaanControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['only' => [
            'getFunding',
        ]]);
    }

    public function getYield(Request $request)
    {
        $pendanaanId = $request->pendanaan_id;

        $proporsional = IhDetilImbalUser::query()
            ->join('pendanaan_aktif', 'ih_detil_imbal_user.pendanaan_id', '=', 'pendanaan_aktif.id')
            ->where('ih_detil_imbal_user.pendanaan_id', $pendanaanId)
            ->orderby('ih_detil_imbal_user.id', 'DESC')
            ->first();

        $results = [];
        /**
         * Probably need ordering kalau ingin menggunakan key pada foreach
         */
        $listImbalHasil = IhListImbalUser::query()
            ->where('pendanaan_id', $pendanaanId)
            ->with('detilImbalUser')
            ->get();

        if (! isset($listImbalHasil[0])) {
            return response()->json(['data' => [], 'item' => $proporsional]);
        }

        $imbalPayoutFields = [
            1 => function ($imbalHasil) {
                return $imbalHasil->detilImbalUser->imbal_hasil_bulanan;
            },
            3 => function ($imbalHasil) {
                return $imbalHasil->imbal_payout;
            },
            2 => function ($imbalHasil) {
                return $imbalHasil->detilImbalUser->sisa_imbal;
            },
        ];
        $textsNominal = [
            1 => 'Imbal Hasil',
            2 => 'Imbal Hasil',
            3 => 'Dana Pokok',
        ];
        $keteranganPayoutNames = [
            1 => 'Imbal Hasil Proyek',
            2 => 'Sisa Imbal Hasil Akhir',
            3 => 'Dana Pokok',
        ];
        $statusKeteranganPayoutNames = [
            2 => 'bold',
            3 => 'bold',
        ];
        $keteranganPayoutNamesFromStatus = [
            2 => 'Berhasil Ditransfer',
            3 => 'Berhasil Disimpan',
        ];
        $countImbalHasil = count($listImbalHasil);
        foreach ($listImbalHasil as $key => $imbalHasil) {
            $imbalPayout = $imbalPayoutFields[$imbalHasil->keterangan_payout]($imbalHasil);
            $keteranganPayout = $keteranganPayoutNames[$imbalHasil->keterangan_payout];
            $statusKeteranganPayout = $statusKeteranganPayoutNames[$imbalHasil->status_payout] ?? 'normal';

            if (in_array($imbalHasil->status_payout, [3, 2], true)) {
                $keteranganPayout = $keteranganPayoutNamesFromStatus[$imbalHasil->status_payout];
            }

            $tglPayout = Carbon::parse($imbalHasil->tanggal_payout)->format('d-m-Y');

            if ($key === $countImbalHasil - 1) {
                //BULAN UNTUK DANA POKOK
                $bulan = $key - 1;
            } elseif ($key === $countImbalHasil - 2) {
                //BULAN UNTUK SISA IMBAL HASIL
                $bulan = $key;
            } else {
                //BULAN BIASA
                $bulan = $key + 1;
            }

            $tax_tarif_persen = MasterParameter::query()->whereIn('deskripsi', ['pph23_no_npwp', 'pph23_npwp'])->pluck('value', 'deskripsi');

            $result = [
                "id" => $key,
                "bulan" => $bulan,
                'imbal_payout' => $imbalPayout,
                'proporsional' => 0,
                "ket_weekend" => trim(strip_tags($imbalHasil->ket_libur)),
                "status_keterangan_payout" => $statusKeteranganPayout,
                "keterangan_payout" => $keteranganPayout,
                "tanggal_payout" => $tglPayout,
                "reg_digital_signature_fee" => (float) $imbalHasil->keterangan,
                "tax" => $imbalHasil->jenis_pajak,
                "tax_tariff_persen" => $imbalHasil->tarif, // ($imbalHasil->npwp_lender === null ? (float) $tax_tarif_persen['pph23_no_npwp'] : (float) $tax_tarif_persen['pph23_npwp']) * 100,
                "tax_tariff_nominal" => (float) $imbalHasil->nominal_pajak,
                'text_nominal' => $textsNominal[$imbalHasil->keterangan_payout],
                'text_total' => "Total " . $textsNominal[$imbalHasil->keterangan_payout] . " Diterima",
            ];

            // Khusus bulan pertama
            if ($key === 0) {
                $result["status_keterangan_payout"] = $statusKeteranganPayout;
                $result["proporsional"] = (float) $proporsional->proposional;
                $result["keterangan_payout"] = "Proporsional & Imbal Hasil " . $keteranganPayout;
            }

            $result['total'] = $result['imbal_payout'] + $result['proporsional'] - $result['reg_digital_signature_fee'] - $result['tax_tariff_nominal'];

            $results[] = $result;
        }

        return response()->json(['data' => $results, 'item' => $proporsional]);
    }
}
