<?php

namespace App\Http\Controllers\Mobile;

use App\BorrowerDetails;
use App\DetilInvestor;
use App\Http\Controllers\Controller;
use App\Services\BorrowerService;
use App\Services\InvestorService;

class InfoControllerV1 extends Controller
{
    public function borrower()
    {
        $borrower = BorrowerService::getBorrower();

        $type = null;
        if ($borrower->detail !== null) {
            $type = $borrower->detail->brw_type;
        }

        $typeNames = [
            BorrowerDetails::TYPE_INDIVIDU => 'Individu',
            BorrowerDetails::TYPE_CORPORATION => 'Badan Hukum',
        ];

        $typeNamesAlt = [
            BorrowerDetails::TYPE_INDIVIDU => 'ind',
            BorrowerDetails::TYPE_CORPORATION => 'cor',
        ];

        return response([
            'type' => $type,
            'type_name' => $typeNames[$type] ?? null,
            'type_name_alt' => $typeNamesAlt[$type] ?? null,
        ]);
    }

    public function lender()
    {
        $investor = InvestorService::getInvestor();

        $type = null;
        if ($investor->detilInvestor !== null) {
            $type = $investor->detilInvestor->tipe_pengguna;
        }

        $typeNames = [
            DetilInvestor::TYPE_INDIVIDU => 'Individu',
            DetilInvestor::TYPE_CORPORATION => 'Badan Hukum',
        ];

        $typeNamesAlt = [
            DetilInvestor::TYPE_INDIVIDU => 'ind',
            DetilInvestor::TYPE_CORPORATION => 'cor',
        ];

        return response([
            'type' => $type,
            'type_name' => $typeNames[$type] ?? null,
            'type_name_alt' => $typeNamesAlt[$type] ?? null,
        ]);
    }
}
