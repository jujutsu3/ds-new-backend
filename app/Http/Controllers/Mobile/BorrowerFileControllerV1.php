<?php

namespace App\Http\Controllers\Mobile;

use App\Helpers\BorrowerFile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BorrowerFileControllerV1 extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['only' => [
            'getFile',
        ]]);
    }

    public function getFile(Request $request)
    {
        Validator::make($request->all(), [
            'path' => ['required'],
            'scope' => ['required', Rule::in(BorrowerFile::$scopes)],
            'column' => ['required'],
        ])
            ->validate();

        $path = $request->get('path');
        $column = $request->get('column');
        $scopesMap = BorrowerFile::verifyScope($column, $path);

        if ($scopesMap[$request->get('scope')]()) {
            return $this->returnFile($request->get('path'));
        }

        return response(null, 403);
    }

    private function returnFile($file)
    {
        if (! Storage::disk('private')->exists($file)) {
            return response(null, 404);
        }
        $path = storage_path('app/private/' . $file);

        return response()->file($path);
    }

}
