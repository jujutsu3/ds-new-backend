<?php

namespace App\Http\Controllers\Mobile;

use App\Borrower;
use App\BorrowerContact;
use App\BorrowerDetails;
use App\BorrowerIndividu;
use App\BorrowerKYCStep;
use App\Constants\KYCStatus;
use App\Http\Controllers\Controller;
use App\MasterKodePos;
use App\MasterPekerjaan;
use App\Services\BorrowerFileService;
use App\Services\BorrowerService;
use App\Services\KYCService;
use App\Services\OTPService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class BorrowerKYCControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', [
            'only' => [
                'currentStatus',
                'profile',
                'updateProfile',
                'profilePic',
                'updateProfilePic',
                'education',
                'updateEducation',
                'getEducationPic',
                'updateEducationPic',
                'sendOtp',
                'updateOtp',
            ],
        ]);
    }

    public function allStatus()
    {
        return response()->json(
            BorrowerKYCStep::query()
                ->where('type', BorrowerKYCStep::TYPE_INDIVIDU)
                ->orderBy('sort')
                ->pluck('name', 'sort')
        );
    }

    private function getCurrentStatus(Borrower $borrower)
    {
        if ($borrower->kyc_step === null) {
            /** @var BorrowerKYCStep $first */
            $first = BorrowerKYCStep::query()->orderBy('sort', 'ASC')->first();
            $borrower->forceFill([
                'kyc_step' => $first->name,
            ])
                ->save();
        }

        /** @var BorrowerKYCStep $status */
        $status = BorrowerKYCStep::query()->where('name', $borrower->kyc_step)->first();

        if ($status === null) {
            $borrower->forceFill([
                'kyc_step' => BorrowerKYCStep::STEP_COR_AWAL,
            ]);

            return [-1, BorrowerKYCStep::STEP_COR_AWAL];
        }

        return [$status->sort, $status->name];
    }

    public function currentStatus(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        if (in_array($borrower->status, [Borrower::STATUS_ACTIVE, Borrower::STATUS_NOTFILLED])) {
            $borrower->forceFill([
                'kyc_step' => BorrowerKYCStep::STEP_IND_ACTIVE,
            ])->save();
        }

        [$stepOrder, $stepName] = $this->getCurrentStatus($borrower);
        $status['verify_status'] = $borrower->status;

        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $status = [
            'profile' => $this->profileCheck($detilBorrower),
            'education' => $this->educationCheck($detilBorrower, $borrower),
            // 'profilePic' => $this->profilePicCheck($detilInvestor),
            // 'inheritance' => $this->inheritanceCheck($detilBorrower, $ahliWaris),
            'step_order' => $stepOrder,
            'step' => $stepName,
            'account_status' => $borrower->status,
        ];

        return response()->json($status);
    }

    public function profile()
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);

        return response()->json([
            'nama' => $detilBorrower->nama,
            'nik' => $detilBorrower->ktp,
            'email' => $detilBorrower->email,
            'jenis_kelamin' => $detilBorrower->jns_kelamin,
            'tempat_lahir' => $detilBorrower->tempat_lahir,
            'tanggal_lahir' => optional($detilBorrower->tgl_lahir, function ($value) {
                return Carbon::parse($value)->format('d-m-Y');
            }),
            'agama' => $detilBorrower->agama,
            'status_pernikahan' => $detilBorrower->status_kawin,
            'provinsi' => $detilBorrower->provinsi,
            'kabupaten' => $detilBorrower->kota,
            'kecamatan' => $detilBorrower->kecamatan,
            'kelurahan' => $detilBorrower->kelurahan,
            'kode_pos' => $detilBorrower->kode_pos,
            'alamat' => $detilBorrower->alamat,
            // 'domisili_status_rumah' => $detilBorrower->domisili_status_rumah,
            'status_rumah' => $detilBorrower->status_rumah,
            'pendidikan' => $detilBorrower->pendidikan_terakhir,
        ]);
    }

    public function updateProfile(Request $request)
    {
        self::checkIsLocked(BorrowerKYCStep::STEP_IND_DATA);
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                BorrowerIndividu::NAMA => ['max:50'],
                BorrowerIndividu::NIK => [
                    'digits:16',
                    Rule::unique('brw_user_detail', 'ktp')->ignore($borrower->brw_id, 'brw_id'),
                ],
                'brw_type' => [''],
                BorrowerIndividu::TEMPAT_LAHIR => ['max:100'],
                BorrowerIndividu::TANGGAL_LAHIR => ['date_format:d-m-Y'],
                BorrowerIndividu::AGAMA => [Rule::exists('m_agama', 'id_agama')],
                BorrowerIndividu::STATUS_PERNIKAHAN => [Rule::exists('m_kawin', 'id_kawin')],
                BorrowerIndividu::PROVINSI => [Rule::exists('m_kode_pos', 'Provinsi')],
                BorrowerIndividu::KABUPATEN => [
                    function ($attribute, $value, $fail) {
                        if (! MasterKodePos::query()->where([
                            ['Jenis', explode(' ', trim($value))[0] ?? null],
                            [
                                'Kota',
                                substr(strstr($value, " "), 1),
                            ],
                        ])->exists()) {
                            $fail("Kabupaten tidak valid");
                        }
                    },
                ],
                BorrowerIndividu::KECAMATAN => [Rule::exists('m_kode_pos', 'kecamatan')],
                BorrowerIndividu::KELURAHAN => [Rule::exists('m_kode_pos', 'kelurahan')],
                BorrowerIndividu::KODE_POS => [Rule::exists('m_kode_pos', 'kode_pos')],
                BorrowerIndividu::ALAMAT => ['max:100'],
                // 'domisili_status_rumah' => [Rule::exists('m_negara', 'id_negara')],
                BorrowerIndividu::STATUS_RUMAH => [Rule::exists('m_kepemilikan_rumah', 'id_kepemilikan_rumah')],
                BorrowerIndividu::PENDIDIKAN => [Rule::exists('m_pendidikan', 'id_pendidikan')],
            ], [], BorrowerIndividu::getAttributeName())
                ->validate();

            $detilBorrower->forceFill(
                BorrowerService::mapToDatabase($request->toArray(), [
                    BorrowerIndividu::NAMA => 'nama',
                    BorrowerIndividu::NIK => 'ktp',
                    BorrowerIndividu::TEMPAT_LAHIR => 'tempat_lahir',
                    BorrowerIndividu::AGAMA => 'agama',
                    BorrowerIndividu::STATUS_PERNIKAHAN => 'status_kawin',
                    BorrowerIndividu::PROVINSI => 'provinsi',
                    // BorrowerIndividu::KABUPATEN => 'kota',
                    BorrowerIndividu::KECAMATAN => 'kecamatan',
                    BorrowerIndividu::KELURAHAN => 'kelurahan',
                    BorrowerIndividu::KODE_POS => 'kode_pos',
                    BorrowerIndividu::ALAMAT => 'alamat',
                    BorrowerIndividu::STATUS_RUMAH => 'status_rumah',
                    BorrowerIndividu::PENDIDIKAN => 'pendidikan_terakhir',
                ])
            )->save();

            if ($request->filled('nik')) {
                $nikData = KYCService::parseNik($request->get('nik'));
                $dob = $nikData['dob'];
                $gender = $nikData['gender'];
            }

            $detilBorrower->forceFill([
                'kota' => $request->filled('kabupaten') ? substr(
                    strstr($request->get('kabupaten'), " "),
                    1
                ) : $detilBorrower->kota,
                'tgl_lahir' => $dob ?? $detilBorrower->tgl_lahir,
                'brw_type' => BorrowerDetails::TYPE_INDIVIDU,
                'jns_kelamin' => $gender ?? $detilBorrower->jns_kelamin,
            ])->save();

            $borrower = BorrowerService::getBorrower();
            self::bumpStatus($borrower, BorrowerKYCStep::STEP_IND_DATA);

            DB::commit();

            return response()->json(['success' => 'Data diri berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'error' => config('app.env') !== 'production' ? $exception->getMessage(
                    ) : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }
    }

    private function profileCheck(BorrowerDetails $detilBorrower)
    {
        $columns = [
            'nama',
            'ktp',
            'tempat_lahir',
            'tgl_lahir',
            'agama',
            'status_kawin',
            'provinsi',
            'kota',
            // 'kecamatan',
            'alamat',
            // 'pendidikan_terakhir',
        ];

        return KYCService::multipleCheck([
            KYCService::baseCheck($columns, $detilBorrower),
            $this->profilePicCheck($detilBorrower),
        ]);
    }

    public function education()
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        return response()->json([
            'pendidikan' => $detilBorrower->pendidikan_terakhir,
            'pekerjaan' => $detilBorrower->pekerjaan,
            'bidang_pekerjaan' => $detilBorrower->bidang_pekerjaan,
            'bidang_online' => $detilBorrower->bidang_online,
            'nama_perusahaan' => $detilBorrowerPenghasilan->nama_perusahaan,
            'no_tlp_usaha' => $detilBorrowerPenghasilan->no_telp,
            // 'lama_kerja' => $detilBorrower->pengalaman_pekerjaan,
            'penghasilan_perbulan' => $detilBorrowerPenghasilan->pendapatan_borrower,
            'masa_kerja_tahun' => $detilBorrowerPenghasilan->masa_kerja_tahun,
            'masa_kerja_bulan' => $detilBorrowerPenghasilan->masa_kerja_bulan,
            'lama_usaha' => $detilBorrowerPenghasilan->usia_perusahaan,
            // 'estimasi_penghasilan' => $detilBorrower->pendapatan,
            // 'sumber_penghasilan' => $detilBorrower->sumber_dana,
            'npwp' => $detilBorrower->npwp,
        ]);
    }

    public function updateEducation(Request $request)
    {
        self::checkIsLocked(BorrowerKYCStep::STEP_IND_DATA);
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                BorrowerIndividu::PENDIDIKAN => [Rule::exists('m_pendidikan', 'id_pendidikan')],
                BorrowerIndividu::PEKERJAAN => [Rule::exists('m_pekerjaan', 'id_pekerjaan')],
                BorrowerIndividu::BIDANG_PEKERJAAN => [Rule::exists('m_bidang_pekerjaan', 'id_bidang_pekerjaan')],
                BorrowerIndividu::BIDANG_ONLINE => [Rule::exists('m_online', 'id_online')],
                // 'lama_kerja' => [Rule::exists('m_pengalaman_kerja', 'id_pengalaman_kerja')],
                BorrowerIndividu::PENGHASILAN_PERBULAN => ['numeric'],
                // 'estimasi_penghasilan' => [Rule::exists('m_pendapatan', 'id_pendapatan')],
                // 'sumber_penghasilan' => ['max:191'],
                BorrowerIndividu::NPWP => [
                    'numeric',
                    'digits_between:15,16',
                    Rule::unique('brw_user_detail', 'npwp')->ignore($borrower->brw_id, 'brw_id'),
                ],
                BorrowerIndividu::LAMA_USAHA => ['numeric', 'digits_between:0,5'],
                BorrowerIndividu::MASA_KERJA_TAHUN => ['numeric', 'digits_between:0,5'],
                BorrowerIndividu::MASA_KERJA_BULAN => ['numeric', 'digits_between:0,5', 'between:0,11'],
                BorrowerIndividu::NAMA_PERUSAHAAN => ['max:50'],
                BorrowerIndividu::NO_TLP_USAHA => ['max:15'],
            ], [], BorrowerIndividu::getAttributeName())
                ->validate();

            $detilBorrower->forceFill(
                BorrowerService::mapToDatabase($request->toArray(), [
                    BorrowerIndividu::PENDIDIKAN => 'pendidikan_terakhir',
                    BorrowerIndividu::PEKERJAAN => 'pekerjaan',
                    BorrowerIndividu::BIDANG_PEKERJAAN => 'bidang_pekerjaan',
                    BorrowerIndividu::BIDANG_ONLINE => 'bidang_online',
                    // BorrowerIndividu::LAMA_KERJA => 'pengalaman_pekerjaan',
                    // BorrowerIndividu::ESTIMASI_PENGHASILAN => 'pendapatan',
                    BorrowerIndividu::NPWP => 'npwp',
                ])
            )->save();

            $detilBorrowerPenghasilan->forceFill(
                BorrowerService::mapToDatabase($request->toArray(), [
                    BorrowerIndividu::PENGHASILAN_PERBULAN => 'pendapatan_borrower',
                    BorrowerIndividu::MASA_KERJA_TAHUN => 'masa_kerja_tahun',
                    BorrowerIndividu::MASA_KERJA_BULAN => 'masa_kerja_bulan',
                    BorrowerIndividu::LAMA_USAHA => 'usia_perusahaan',
                    BorrowerIndividu::NAMA_PERUSAHAAN => 'nama_perusahaan',
                    BorrowerIndividu::NO_TLP_USAHA => 'no_telp',
                ])
            )->save();

            $borrower = BorrowerService::getBorrower();
            self::bumpStatus($borrower, BorrowerKYCStep::STEP_IND_DATA);

            DB::commit();

            return response()->json(['success' => 'Data pendidikan/pekerjaan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'error' => config('app.env') !== 'production' ? $exception->getMessage(
                    ) : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }
    }

    private function educationCheck(BorrowerDetails $detilBorrower, Borrower $borrower)
    {
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);
        $columns = [
            'pendidikan_terakhir',
            'pekerjaan',
            'bidang_pekerjaan',
            'bidang_online',
            // 'pengalaman_pekerjaan',
            // 'pendapatan',
            // 'sumber_dana',
            'npwp',
            'brw_pic_npwp',
        ];

        $isWiraswasta = ($detilBorrower->pekerjaan === MasterPekerjaan::query()->where(
                'pekerjaan',
                MasterPekerjaan::WIRASWASTA
            )->first()->id_pekerjaan);
        $base = [
            'nama_perusahaan',
            'no_telp',
            'pendapatan_borrower',
        ];
        $add = $isWiraswasta ? [
            'usia_perusahaan',
        ] : [
            'masa_kerja_tahun',
            'masa_kerja_bulan',
        ];

        return KYCService::multipleCheck([
            KYCService::baseCheck($columns, $detilBorrower),
            KYCService::baseCheck(array_merge($base, $add), $detilBorrowerPenghasilan),
        ]);
    }

    public static function detilBorrowerPicValidateHandler($borrowerId, $column, $path)
    {
        /**
         * @todo Admin privilege, atau bisa aja kalo admin langsung pake returnFile() tanpa getFile()
         * @see \App\Services\FileService::returnFile()
         */
        $detil = BorrowerService::getDetilBorrower(BorrowerService::getBorrower($borrowerId))->toArray();

        return $detil[$column] === $path;
    }

    public function profilePic()
    {
        $borrower = BorrowerService::getBorrower();
        $borrowerIdOriginalOwner = $borrower->brw_id;
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);

        $check = static function ($borrowerId) use ($borrowerIdOriginalOwner) {
            return $borrowerId === $borrowerIdOriginalOwner;
        };

        return response()->json([
            'ktp' => $detilBorrower->brw_pic_ktp,
            'ktp_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic_ktp, $check),
            'profile_ktp' => $detilBorrower->brw_pic_user_ktp,
            'profile_ktp_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic_user_ktp, $check),
            'profile' => $detilBorrower->brw_pic,
            'profile_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic, $check),
        ]);
    }

    public function updateProfilePic(Request $request)
    {
        self::checkIsLocked(BorrowerKYCStep::STEP_IND_DATA);
        $detilBorrower = BorrowerService::getDetilBorrower(BorrowerService::getBorrower());

        Validator::make($request->all(), [
            'ktp' => ['mimes:jpeg,jpg,bmp,png'],
            'profile_ktp' => ['mimes:jpeg,jpg,bmp,png'],
            'profile' => ['mimes:jpeg,jpg,bmp,png'],
        ])
            ->validate();

        $messages = [];
        if ($request->hasFile('ktp')) {
            $detilBorrower->forceFill([
                'brw_pic_ktp' =>
                    BorrowerFileService::uploadFile($request->file('ktp'), 'brw_pic_ktp', $detilBorrower->brw_id),
            ])->save();
            $messages['ktp'] = 'success';
        }

        if ($request->hasFile('profile_ktp')) {
            $detilBorrower->forceFill([
                'brw_pic_user_ktp' =>
                    BorrowerFileService::uploadFile(
                        $request->file('profile_ktp'),
                        'brw_pic_user_ktp',
                        $detilBorrower->brw_id
                    ),
            ])->save();
            $messages['profile_ktp'] = 'success';
        }

        if ($request->hasFile('profile')) {
            $detilBorrower->forceFill([
                'brw_pic' =>
                    BorrowerFileService::uploadFile($request->file('profile'), 'brw_pic', $detilBorrower->brw_id),
            ])->save();
            $messages['profile'] = 'success';
        }

        return response()->json($messages);
    }

    private function profilePicCheck(BorrowerDetails $detilBorrower)
    {
        $columns = [
            'brw_pic_ktp',
            'brw_pic_user_ktp',
            'brw_pic',
        ];

        return KYCService::baseCheck($columns, $detilBorrower);
    }

    public function sendOtp(Request $request)
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        // $ahliWaris = BorrowerService::getAhliWaris();
        if (KYCService::multipleCheck([
                $this->profileCheck($detilBorrower),
                $this->educationCheck($detilBorrower, $borrower),
                // $this->inheritanceCheck($detilBorrower, $ahliWaris),
            ]) !== KYCStatus::COMPLETE_TRUE) {
            return response()->json(['error' => 'pengisian KYC harus selesai']);
        }

        $phone = OTPService::trim($request->no_telp);
        
        if (App::isLocale('en')) {
            $message = 'Phone :input is already registered';
        } else {
            $message = 'Nomor ponsel :input sudah terdaftar';
        }

        Validator::make([
            'no_telp' => $phone,
        ], [
            'no_telp' => [
                Rule::unique('brw_user_detail', 'no_tlp')->ignore($borrower->brw_id, 'brw_id'),
                Rule::unique(
                    BorrowerContact::TABLE,
                    BorrowerContact::getAdministratorDbMap()[BorrowerContact::NO_TELEPON]
                )
                    ->ignore($detilBorrower->brw_id, 'brw_id'),
            ],
        ], [
            'no_telp.unique' => $message
        ])
            ->validate();
        [$status, $message] = OTPService::borrowerSend($borrower, $request->no_telp);

        if ($status) {
            self::bumpStatus($borrower, BorrowerKYCStep::STEP_IND_OTP);

            $detilBorrower->forceFill([
                'no_tlp' => $phone,
            ])->save();

            return response()->json(['success' => $message]);
        }

        return response()->json(['error' => $message]);
    }

    public function getEducationPic()
    {
        $detilBorrower = BorrowerService::getDetilBorrower(BorrowerService::getBorrower());
        $borrowerIdOriginalOwner = BorrowerService::getLoggedInBorrowerId();

        $check = static function ($borrowerId) use ($borrowerIdOriginalOwner) {
            return $borrowerId === $borrowerIdOriginalOwner;
        };

        return response()->json([
            'npwp' => $detilBorrower->brw_pic_npwp,
            'npwp_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic_npwp, $check),
        ]);
    }

    public function updateEducationPic(Request $request)
    {
        self::checkIsLocked(BorrowerKYCStep::STEP_IND_DATA);
        $detilBorrower = BorrowerService::getDetilBorrower(BorrowerService::getBorrower());

        Validator::make($request->all(), [
            'npwp' => ['mimes:jpeg,jpg,bmp,png'],
        ])
            ->validate();

        $messages = [];

        if ($request->hasFile('npwp')) {
            $detilBorrower->forceFill([
                'brw_pic_npwp' =>
                    BorrowerFileService::uploadFile($request->file('npwp'), 'brw_pic_npwp', $detilBorrower->brw_id),
            ])->save();
            $messages['npwp'] = 'success';
        }

        return response()->json($messages);
    }

    private function checkEducationPic(BorrowerDetails $detilBorrower)
    {
        $columns = [
            'brw_pic_npwp',
        ];

        return KYCService::baseCheck($columns, $detilBorrower);
    }

    public function updateOtp(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        if ((string) $request->no_otp === (string) $borrower->otp) {
            // commented karena harusnya pake borrower kyc mode dari master parameter
            // if (MasterParameter::getParam(MasterParameter::INVESTOR_KYC_MODE === 'automatic')) {
            // mungkin harusnya flush juga otp nya
            $borrower->forceFill(['status' => Borrower::STATUS_NOTPREAPPROVED])->save();
            self::bumpStatus($borrower, BorrowerKYCStep::STEP_IND_ACTIVE);
            // } else {
            //     self::bumpStatus($borrower, BorrowerKYCStep::STEP_IND_VERIFICATION);
            // }

            return response()->json(['status' => 'success']);
        }

        return response()->json(['error' => 'OTP yang anda masukan salah']);
    }

    private static function bumpStatus(Borrower $borrower, string $newStatus)
    {
        $statuses = BorrowerKYCStep::query()->whereIn('name', [$newStatus, $borrower->kyc_step])->pluck('sort', 'name');

        $current = $statuses[$borrower->kyc_step];
        $new = $statuses[$newStatus];

        if ($new > $current) {
            $borrower->forceFill([
                'kyc_step' => $newStatus,
            ])
                ->save();
        }
    }

    private static function checkIsLocked($step)
    {
        /**
         * @todo Maybe next
         */
        // $borrower = BorrowerService::getBorrower();
        // if ($borrower->kyc_step !== $step) {
        //     abort(403, 'Tahapan KYC tersebut sudah terkunci');
        // }
    }
}
