<?php

namespace App\Http\Controllers\Mobile;

use App\DetilInvestor;
use App\Http\Controllers\Controller;
use App\Investor;
use App\InvestorAdministrator;
use App\InvestorAdministratorInterface;
use App\InvestorContacts;
use App\InvestorCorporation;
use App\InvestorPemegangSaham;
use App\InvestorPengurus;
use App\MasterProvinsi;
use App\Services\InvestorService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LenderCorporationProfileControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth:api',
            'lender_type:' . DetilInvestor::TYPE_CORPORATION,
        ], [
            'only' => [
                'getCorporation',
                'updateCorporation',
                'getAdministrator',
                'updateAdministrator',
                'getShareholder',
                'updateShareholder',
                'getContacts',
                'updateContacts',
                'verifyData',
            ],
        ]);
    }

    public function getCorporation(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();

        $investorIdOriginalOwner = $investor->id;
        $check = self::picCheck($investorIdOriginalOwner);

        $data = $investor->getData([
            InvestorCorporation::NAMA,
            InvestorCorporation::USERNAME,
            InvestorCorporation::EMAIL,
            InvestorCorporation::NO_SURAT_IZIN,
            InvestorCorporation::NPWP,
            InvestorCorporation::NO_AKTA_PENDIRIAN,
            InvestorCorporation::TANGGAL_AKTA_PENDIRIAN,
            InvestorCorporation::NO_AKTA_PERUBAHAN,
            InvestorCorporation::TANGGAL_AKTA_PERUBAHAN,
            InvestorCorporation::TELEPON,
            InvestorCorporation::ALAMAT,
            InvestorCorporation::PROVINSI,
            InvestorCorporation::KABUPATEN,
            InvestorCorporation::KECAMATAN,
            InvestorCorporation::KELURAHAN,
            InvestorCorporation::KODE_POS,
            InvestorCorporation::REKENING,
            InvestorCorporation::BANK,
            InvestorCorporation::NAMA_PEMILIK_REKENING,
            InvestorCorporation::BIDANG_PEKERJAAN,
            InvestorCorporation::OMSET_TAHUN_TERAKHIR,
            InvestorCorporation::TOTAL_ASET_TAHUN_TERAKHIR,
            InvestorCorporation::FOTO_NPWP,
            InvestorCorporation::LAPORAN_KEUANGAN,
            InvestorCorporation::LONG,
            InvestorCorporation::LAT,
            InvestorCorporation::ALT,
            InvestorCorporation::BERKEDUDUKAN,
            InvestorCorporation::NAMA_NOTARIS_AKTA_PENDIRIAN,
            InvestorCorporation::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN,
            InvestorCorporation::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            InvestorCorporation::NAMA_NOTARIS_AKTA_PERUBAHAN,
            InvestorCorporation::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN,
            InvestorCorporation::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
            InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
        ])
            ->toArray();

        InvestorCorporation::fieldFileAccess($data, InvestorCorporation::FOTO_NPWP, $check);
        InvestorCorporation::fieldFileAccess($data, InvestorCorporation::LAPORAN_KEUANGAN, $check);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_AKTA_PENDIRIAN);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_AKTA_PERUBAHAN);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN);
        InvestorCorporation::fieldDateAccess($data, InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN);

        return response()->json(
            array_merge($data, [
                'provinsi_nama' => optional(
                    MasterProvinsi::query()
                        ->where('kode_provinsi', $investor->detilInvestor->provinsi_perusahaan)
                        ->first(),
                    function ($value) {
                        return $value->nama_provinsi;
                    }
                ),
                'kabupaten_nama' => optional(
                    MasterProvinsi::query()->where('kode_kota', $investor->detilInvestor->kota_perusahaan)->first(),
                    function ($value) {
                        return $value->nama_kota;
                    }
                ),
            ])
        );
    }

    public function updateCorporation(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        DB::beginTransaction();
        try {
            // Ignore beberapa kolom sehingga tidak boleh di update
            $detilInvestorMap = array_diff(InvestorCorporation::getDetilInvestorMap(), [
                InvestorCorporation::NAMA,
                InvestorCorporation::NO_AKTA_PENDIRIAN,
                InvestorCorporation::NO_AKTA_PERUBAHAN,
                InvestorCorporation::TANGGAL_AKTA_PENDIRIAN,
                InvestorCorporation::TELEPON,
                InvestorCorporation::REKENING,
                InvestorCorporation::NAMA_PEMILIK_REKENING,
                InvestorCorporation::BANK,
            ]);

            Validator::make(
                $request->all(),
                InvestorCorporation::getRules($investor->id, $detilInvestorMap),
                [],
                InvestorCorporation::getAttributeName()
            )
                ->validate();

            InvestorCorporation::additionalValidator($request, $investor);

            $detilInvestorMap = InvestorCorporation::getDetilInvestorMap();
            $detilInvestor
                ->forceFill(InvestorService::mapToDatabase($request->toArray(), $detilInvestorMap))
                ->forceFill([
                    $detilInvestorMap[InvestorCorporation::TANGGAL_AKTA_PENDIRIAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_AKTA_PENDIRIAN),
                        $detilInvestor->tanggal_akta_pendirian
                    ),
                    $detilInvestorMap[InvestorCorporation::TANGGAL_AKTA_PERUBAHAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_AKTA_PERUBAHAN),
                        $detilInvestor->tanggal_akta_perubahan
                    ),
                    $detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN),
                        $detilInvestor->{$detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN]}
                    ),
                    $detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN] => InvestorCorporation::fieldDateMutate(
                        $request->get(InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN),
                        $detilInvestor->{$detilInvestorMap[InvestorCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN]}
                    ),
                    'tipe_pengguna' => DetilInvestor::TYPE_CORPORATION,
                ])
                ->save();

            $photoMessages = [];
            if ($file = $request->file(InvestorCorporation::FOTO_NPWP)) {
                $photoMessages[InvestorCorporation::FOTO_NPWP] = InvestorCorporation::upload(
                    $detilInvestorMap[InvestorCorporation::FOTO_NPWP],
                    $file,
                    $detilInvestor,
                    'pic_user_npwp_investor'
                );
            }

            if ($file = $request->file(InvestorCorporation::LAPORAN_KEUANGAN)) {
                $photoMessages[InvestorCorporation::LAPORAN_KEUANGAN] = InvestorCorporation::uploadLapKeuangan(
                    $detilInvestorMap[InvestorCorporation::LAPORAN_KEUANGAN],
                    $file,
                    $detilInvestor,
                    'laporan_keuangan'
                );
            }

            DB::commit();

            return response()->json([
                'success' => 'Data perusahaan berhasil disimpan',
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => config('app.env') !== 'production'
                    ? $exception->getMessage()
                    : 'Terjadi kesalahan, silahkan coba lagi',
            ]);
        }
    }

    private function getGenericPerson(InvestorCorporation $investor, Collection $investorAdmins)
    {
        $investorIdOriginalOwner = $investor->id;

        $check = self::picCheck($investorIdOriginalOwner);

        $data = $investorAdmins->map(function (InvestorAdministratorInterface $administrator) use ($check) {
            $neededColumns = [
                $administrator::NAMA,
                $administrator::JENIS_KELAMIN,
                $administrator::JENIS_IDENTITAS,
                $administrator::IDENTITAS,
                $administrator::TEMPAT_LAHIR,
                $administrator::TANGGAL_LAHIR,
                $administrator::KODE_OPERATOR,
                $administrator::NO_TELEPON,
                $administrator::AGAMA,
                $administrator::PENDIDIKAN,
                $administrator::NPWP,
                $administrator::JABATAN,
                $administrator::ALAMAT,
                $administrator::PROVINSI,
                $administrator::KABUPATEN,
                $administrator::KECAMATAN,
                $administrator::KELURAHAN,
                $administrator::KODE_POS,
                $administrator::FOTO_KTP,
                $administrator::FOTO_PROFILE_KTP,
                $administrator::FOTO_PROFILE,
                $administrator::FOTO_NPWP,
            ];

            $neededColumns = $this->getPemegangSahamModifier($administrator, $neededColumns);
            $datum = $administrator->getData($neededColumns);

            $administrator::fieldFileAccess($datum, $administrator::FOTO_NPWP, $check);
            $administrator::fieldFileAccess($datum, $administrator::FOTO_PROFILE, $check);
            $administrator::fieldFileAccess($datum, $administrator::FOTO_PROFILE_KTP, $check);
            $administrator::fieldFileAccess($datum, $administrator::FOTO_KTP, $check);
            $administrator::fieldDateAccess($datum, $administrator::TANGGAL_LAHIR);
            $administrator::fieldPhoneAccess($datum, $administrator::NO_TELEPON, $administrator::KODE_OPERATOR);

            return array_merge($datum->toArray(), [
                'provinsi_nama' => optional(
                    MasterProvinsi::query()->where('kode_provinsi', $datum[InvestorAdministrator::PROVINSI])->first(),
                    function ($value) {
                        return $value->nama_provinsi;
                    }
                ),
                'kabupaten_nama' => optional(
                    MasterProvinsi::query()->where('kode_kota', $datum[InvestorAdministrator::KABUPATEN])->first(),
                    function ($value) {
                        return $value->nama_kota;
                    }
                ),
            ]);
        });

        return response()->json($data);
    }

    public function getAdministrator(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $investorAdmins = $investor->admins;

        return $this->getGenericPerson($investor, $investorAdmins);
    }

    public function getShareholder(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $shareHolders = $investor->shareholders;

        return $this->getGenericPerson($investor, $shareHolders);
    }

    public function getContacts(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();
        $contacts = $investor->contacts;

        return $this->getGenericPerson($investor, $contacts);
    }

    private function updateGenericPerson(
        Request $request,
        Investor $investor,
        InvestorAdministratorInterface $administrator
    ) {
        DB::beginTransaction();
        $requestKey = $administrator::getRequestIdentifier();
        $administratorMap = $administrator::getAdministratorDbMap();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        try {
            $data = $request->all();
            $neededRules = [];
            InvestorCorporation::validator($data, $administrator, $neededRules, $investor);

            $createdAtOld = $investor->$requestKey()->pluck('created_at');
            $investor->$requestKey()->delete();
            $photoMessages = [];
            $map = $administratorMap;
            foreach ($request->get($requestKey) ?? [] as $key => $adminRequest) {
                $administrator::additionalValidator($adminRequest, $administrator, $requestKey, $key);

                // Ignore beberapa kolom untuk pengurus pertama 
                if ($key === 0 && $administrator instanceof InvestorPengurus) {
                    $map = array_diff($map, [
                        InvestorAdministrator::IDENTITAS,
                        InvestorAdministrator::NO_TELEPON,
                        InvestorAdministrator::NPWP,
                    ]);
                }
                $admin = $investor->$requestKey()->make()
                    ->forceFill(InvestorService::mapToDatabase($adminRequest, $map))
                    ->forceFill([
                        'investor_id' => $investor->id,
                        $map[$administrator::TANGGAL_LAHIR] => $administrator::fieldDateMutate(
                            $adminRequest[$administrator::TANGGAL_LAHIR] ?? null,
                            $administrator->tanggal_lahir
                        ),
                        $map[$administrator::NO_TELEPON] => $administrator::fieldPhoneMutate(
                            $adminRequest[$administrator::NO_TELEPON] ?? null,
                            $adminRequest[$administrator::KODE_OPERATOR] ?? null
                        ),
                    ]);

                $mapRequestToColumnPhoto = array_intersect_key(
                    $map,
                    array_flip([
                        $administrator::FOTO_KTP,
                        $administrator::FOTO_PROFILE_KTP,
                        $administrator::FOTO_PROFILE,
                        $administrator::FOTO_NPWP,
                    ])
                );

                $photoMessages[$key] = [];
                foreach ($mapRequestToColumnPhoto as $requestField => $columnKey) {
                    if ($file = $request->file("$requestKey.$key.$requestField")) {
                        $photoMessages[$key][$requestField] = InvestorCorporation::upload(
                            $columnKey,
                            $file,
                            $admin,
                            "pic_corp_{$requestKey}_{$key}_$columnKey"
                        );
                    }
                }

                /** @var \Illuminate\Database\Eloquent\Model $admin */
                $admin->timestamps = false;
                $admin->forceFill([
                    $admin::CREATED_AT => $createdAtOld[$key] ?? Carbon::now(),
                    $admin::UPDATED_AT => $admin->{$admin->getUpdatedAtColumn()} ?? Carbon::now(),
                ])
                    ->save();
            }

            DB::commit();

            $readableKey = $administrator::READABLE_KEY;

            return response()->json([
                'success' => "Data $readableKey berhasil disimpan",
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'error' => config('app.env') !== 'production' ? $exception->getMessage(
                    ) : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateAdministrator(Request $request)
    {
        $investor = InvestorService::getInvestorCorporation();

        return $this->updateGenericPerson($request, $investor, new InvestorPengurus());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateShareholder(Request $request)
    {
        $investor = InvestorService::getInvestor();

        return $this->updateGenericPerson($request, $investor, new InvestorPemegangSaham());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateContacts(Request $request)
    {
        $investor = InvestorService::getInvestor();

        return $this->updateGenericPerson($request, $investor, new InvestorContacts());
    }

    public function verifyData(Request $request)
    {
        $investor = InvestorService::getInvestor();
        if ($request->has('email') && $request->email === $investor->email) {
            return response()->json([
                'success' => 'Email Terverifikasi',
            ]);
        }

        if ($request->has('phone') && $request->get('phone') !== null) {
            $contact = $investor->contacts()->first();
            if ($contact !== null && ltrim($request->phone, '0') === (string) $contact->no_tlp) {
                return response()->json([
                    'success' => 'No HP Terverifikasi',
                ]);
            }
        }

        return response()->json([
            'errors' => 'Data tidak sesuai',
        ]);
    }

    private static function picCheck($investorIdOriginalOwner)
    {
        return function ($investorId) use ($investorIdOriginalOwner) {
            return $investorId === $investorIdOriginalOwner;
        };
    }

    /**
     * @param \App\InvestorAdministratorInterface $administrator
     * @param array $neededColumns
     * @return array|int[]|string[]
     */
    private function getPemegangSahamModifier(
        InvestorAdministratorInterface $administrator,
        array $neededColumns
    ): array {
        if (! $administrator instanceof InvestorPemegangSaham) {
            return $neededColumns;
        }

        $neededColumns[] = InvestorPemegangSaham::JENIS_PEMEGANG_SAHAM;
        $neededColumns[] = InvestorPemegangSaham::NILAI_SAHAM;
        $neededColumns[] = InvestorPemegangSaham::LEMBAR_SAHAM;

        if ($administrator->{InvestorPemegangSaham::JENIS_PEMEGANG_SAHAM} === InvestorPemegangSaham::PMG_SHM_BDN_HKM) {
            $temp = array_flip($neededColumns);
            unset(
                $temp[$administrator::AGAMA],
                $temp[$administrator::JENIS_KELAMIN],
                $temp[$administrator::TEMPAT_LAHIR],
                $temp[$administrator::TANGGAL_LAHIR],
                $temp[$administrator::PENDIDIKAN],
                $temp[$administrator::JABATAN]
            );

            $neededColumns = array_flip($temp);
        }

        return $neededColumns;
    }
}
