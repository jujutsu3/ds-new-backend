<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Services\BorrowerService;
use App\Services\InvestorService;
use Illuminate\Support\Facades\Auth;

class UserInfoControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['only' => ['lender']]);
        $this->middleware('auth:borrower-api-mobile', ['only' => ['borrower']]);
    }

    public function lender()
    {
        $lender = InvestorService::getInvestor(Auth::id());
        $detail = InvestorService::getDetilInvestor($lender);

        return response()->json([
            'tipe' => $detail->tipe_pengguna,
        ]);
    }

    public function borrower($param)
    {
        $this->middleware('auth:borrower-api-mobile');
        $borrower = BorrowerService::getBorrower(Auth::id());
        $detail = BorrowerService::getDetilBorrower($borrower);

        return response()->json([
            'tipe' => $detail->brw_type,
        ]);
    }
}
