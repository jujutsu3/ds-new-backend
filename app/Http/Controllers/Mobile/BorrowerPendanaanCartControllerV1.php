<?php

namespace App\Http\Controllers\Mobile;

use App\Borrower;
use App\BorrowerAnalisaPendanaan;
use App\BorrowerPengajuan;
use App\BorrowerTipePendanaan;
use App\Constants\BorrowerPengajuanStatus;
use App\Http\Controllers\Controller;
use App\MasterTujuanPembiayaan;
use App\Services\BorrowerService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class BorrowerPendanaanCartControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['only' => [
            'getFunding',
            'submitApplication',
        ]]);
    }

    public function getCart(Request $request, BorrowerPengajuan $pengajuan)
    {
        $materialRequisition = $pengajuan->materialRequisition->first();
        
        if ($materialRequisition === null || ! $materialRequisition->items()->exist()) {
            return response([], 204);
        }
        
        $items = $materialRequisition->items();
    }
}
