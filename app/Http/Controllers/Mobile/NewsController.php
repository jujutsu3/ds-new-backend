<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['getNews', 'getDetil']]);
    }

    public function getNews(Request $request)
    {
        $news = News::query()
            ->orderBy('updated_at', 'desc')
            ->paginate($request->get('per_page') ?: 4);

        return response()->json([
            'metadata' => [
                'total' => $news->total(),
                'per_page' => $news->perPage(),
                'current_page' => $news->currentPage(),
                'last_page' => $news->lastPage(),
                'next_page_url' => $news->nextPageUrl(),
                'prev_page_url' => $news->previousPageUrl(),
            ],
            'data' => collect($news->items())->map(function ($item) {
                return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'image' => '/storage/' . $item->image,
                    'writer' => $item->writer,
                    'updated_at' => $item->updated_at->toDateString(),
                ];
            }),
        ]);
    }

    public function getDetil(Request $request)
    {
        $detils = News::where('id', $request->id_news)->first();

        return [
            'id' => $detils->id,
            'title' => $detils->title,
            'image' => '/storage/' . $detils->image,
            'writer' => $detils->writer,
            'deskripsi' => $detils->deskripsi,
            'updated_at' => $detils->updated_at->toDateString(),
        ];
    }
}
