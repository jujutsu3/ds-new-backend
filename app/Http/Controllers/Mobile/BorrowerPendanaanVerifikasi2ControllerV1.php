<?php

namespace App\Http\Controllers\Mobile;

use App\BorrowerPengajuan;
use App\BorrowerPersyaratanPendanaan;
use App\Http\Controllers\Controller;
use App\Services\BorrowerFileService;
use App\Services\BorrowerService;
use App\Services\DSICoreService;
use App\Services\KYCService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;

class BorrowerPendanaanVerifikasi2ControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['only' => [
            'currentStatus',
            'getDocument', 'getDocumentForm',
            'updateDocument', 'updateDocumentPdf',
        ]]);
    }

    public function currentStatus(Request $request, BorrowerPengajuan $pengajuan)
    {
        BorrowerService::validatePengajuanOwnershipAndLock($pengajuan);

        $status = [
            'all' => self::allCheck($pengajuan),
        ];

        return response()->json($status);
    }

    private static function allCheck(BorrowerPengajuan $pengajuan)
    {
        $fillingDocuments = self::getDokumenList($pengajuan)
            ->filter(function ($document) {
                return $document->persyaratan_mandatory === 1;
            })
            ->map(function ($document) {
                return $document->field_name;
            })
            ->values()
            ->toArray();
        $existingDocuments = self::getExistingDocument($pengajuan)
            ->toArray();

        return KYCService::baseCheck($fillingDocuments, $existingDocuments);
    }

    public function getDocumentForm(BorrowerPengajuan $pengajuan)
    {
        BorrowerService::validatePengajuanOwnershipAndLock($pengajuan);
        $documents = self::getDokumenList($pengajuan);

        if ($documents->isEmpty()) {
            return response([]);
        }

        $data = $documents->map(function ($document) {
            return [
                'id' => $document->persyaratan_id,
                'judul' => $document->persyaratan_nama,
                'is_required' => $document->persyaratan_mandatory,
                'halaman' => $document->page_title,
                'kategori' => $document->category_title,
                'sort' => $document->sort,
                'field' => $document->field_name,
            ];
        });

        return response()->json($data->groupBy('halaman')
            ->sortBy(function ($category) {
                return $category[0]['sort'];
            })
            ->map(function ($documents, $key) {
                return [
                    'title' => $key,
                    'children' => $documents->groupBy('kategori')
                        ->sortKeys()
                        ->map(function ($documents, $key) {
                            return [
                                'title' => $key,
                                'children' => $documents->sortBy('sort'),
                            ];
                        })
                        ->values(),
                ];
            })
            ->values()
        );
    }

    public function getDocument(BorrowerPengajuan $pengajuan, Request $request)
    {
        BorrowerService::validatePengajuanOwnershipAndLock($pengajuan);
        $responses = self::getExistingDocument($pengajuan);
        $documents = self::getDokumenList($pengajuan)->pluck('field_name')->flip()->map(function () {
            return null;
        });

        $borrowerIdOriginalOwner = $pengajuan->brw_id;
        $check = static function ($borrowerId) use ($borrowerIdOriginalOwner) {
            return $borrowerId === $borrowerIdOriginalOwner;
        };

        $responses = $documents->merge($responses);
        $responses->each(function ($item, $key) use ($check, $responses) {
            $responses->put("{$key}_full", BorrowerFileService::makeUrl($item, $check));
        });

        return response()->json($responses);
    }

    public function updateDocument(Request $request, BorrowerPengajuan $pengajuan)
    {
        BorrowerService::validatePengajuanOwnershipAndLock($pengajuan);
        $documents = self::getDokumenList($pengajuan);

       
        DB::beginTransaction();
        try {
            Validator::make(
                $request->all(),
                $documents->mapWithKeys(function ($document) {
                    return [
                        $document->field_name => ['array'],
                        $document->field_name . '.*' => ['mimes:jpeg,jpg,bmp,png,pdf'],
                    ];
                })->toArray()
            )
                ->validate();

            $messages = [];
            //collect document id
            $sendDocuments = [];
            foreach ($documents as $document) {
                $fieldName = $document->field_name;
                if ($request->hasFile($fieldName)) {
                    $sendDocuments[] = $document->persyaratan_id; 
                    $mime = optional($request->file($fieldName)[0] ?? null, function ($item) {
                        return $item->getMimeType();
                    });
                    if (strpos($mime, 'image/') === 0) {
                        $messages[$fieldName] = $this->uploadImage($document, $request->file($fieldName), $fieldName, $pengajuan);
                    }

                    if (strpos($mime, 'application/pdf') === 0) {
                        $messages[$fieldName] = $this->uploadPdf($document, $request->file($fieldName), $fieldName, $pengajuan);
                    }
                }
            }

            DB::commit();

            DSICoreService::sendDocument($sendDocuments , $pengajuan->pengajuan_id);

            return response()->json($messages);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    /**
     * Ceritanya tadinya tetep image yang di upload tapi dijadiin pdf output nya
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\BorrowerPengajuan $pengajuan
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @deprecated
     */
    public function updateDocumentPdf(Request $request, BorrowerPengajuan $pengajuan)
    {
        BorrowerService::validatePengajuanOwnershipAndLock($pengajuan);
        $documents = self::getDokumenList($pengajuan);

        DB::beginTransaction();
        try {
            Validator::make(
                $request->all(),
                $documents->mapWithKeys(function ($document) {
                    return [
                        $document->field_name => ['array'],
                        $document->field_name . '.*' => ['image'],
                    ];
                })->toArray()
            )
                ->validate();

            $messages = [];
            foreach ($documents as $document) {
                $fieldName = $document->field_name;
                if ($request->hasFile($fieldName)) {
                    $path = sys_get_temp_dir() . '/' . Str::random();

                    $pdf = App::make('dompdf.wrapper');
                    $html = "
                        <style>
                            .page-break {
                                page-break-after: always;
                            }
                        </style>";
                    $count = count($request->file($fieldName));
                    foreach ($request->file($fieldName) as $key => $file) {
                        $html .= "
                            <img src='data:image/png;base64, " . base64_encode(file_get_contents($file->path())) . "'>
                        ";
                        if ($key !== ($count - 1)) {
                            $html .= "
                                <div class='page-break'></div>
                            ";
                        }
                    }

                    $pdf->loadHTML($html)
                        ->save($path);

                    DB::table($document->table_name)->update([$fieldName =>
                        BorrowerFileService::uploadFile(
                            new UploadedFile($path, "$fieldName.pdf"),
                            "verify2_{$fieldName}",
                            $pengajuan->brw_id,
                            BorrowerFileService::getDiskPath($pengajuan->brw_id) . "/$pengajuan->pengajuan_id"
                        ),
                    ]);
                    $messages[$fieldName] = 'success';
                }
            }

            DB::commit();

            return response()->json($messages);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    private static function getDokumenList(BorrowerPengajuan $pengajuan)
    {
        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $borrowerDetail = BorrowerService::getDetilBorrower($borrower);
        $borrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);
        $borrowerPasangan = BorrowerService::getDetilBorrowerPasangan($borrower);

        return BorrowerPersyaratanPendanaan::query()->where([
            'tipe_id' => $pengajuan->pendanaan_tipe,
            'user_type' => $borrowerDetail->brw_type,
        ])
            ->orderBy('category')
            ->orderBy('sort')
            ->get()
            ->filter(function ($doc) use ($borrowerPasangan, $borrowerPenghasilan, $pengajuan, $borrowerDetail) {
                if ($doc->status_kawin !== null
                    && $doc->status_kawin !== $borrowerDetail->status_kawin) {
                    return false;
                }
                if ($doc->skema_pembiayaan !== null
                    && $doc->skema_pembiayaan !== $borrowerPenghasilan->skema_pembiayaan) {
                    return false;
                }
                if ($doc->sumber_pengembalian_dana !== null
                    && $doc->sumber_pengembalian_dana !== $borrowerPenghasilan->sumber_pengembalian_dana) {
                    return false;
                }
                if ($doc->sumber_pengembalian_dana_pasangan !== null
                    && $doc->sumber_pengembalian_dana_pasangan !== $borrowerPasangan->sumber_pengembalian_dana) {
                    return false;
                }

                return true;
            });
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @return \Illuminate\Support\Collection
     */
    private static function getExistingDocument(BorrowerPengajuan $pengajuan)
    {
        $documents = self::getDokumenList($pengajuan);

        $responses = collect();
        $tables = $documents->groupBy('table_name');
        foreach ($tables as $tableName => $table) {
            $result = DB::table($tableName)
                ->select($table->pluck('field_name')->toArray())
                ->where(function ($query) use ($pengajuan, $tableName) {
                    $primaryField = BorrowerPendanaanVerifikasi2ControllerV1::getPrimaryFieldForDoc($pengajuan, $tableName);

                    $query->where($primaryField);
                    // if (array_key_exists($tableName, $primaryFieldMap)) {
                    //     return 
                    // }

                    return true;
                })
                ->first();
            $responses = $responses->merge($result);
        }

        return $responses;
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @return array[]
     */
    protected static function getPrimaryFieldForDoc(BorrowerPengajuan $pengajuan, $tableName): array
    {
        $arr = [
            'brw_dokumen_legalitas_pribadi' => ['brw_id' => $pengajuan->brw_id],
            'brw_user_detail' => ['brw_id' => $pengajuan->brw_id],
            'brw_dokumen_objek_pendanaan' => ['id_pengajuan' => $pengajuan->pengajuan_id],
        ];

        /**
         * Fallback in case of something happen
         *
         * @see \App\Http\Controllers\Borrower\BorrowerDokumenController::uploadDokumenPendukung()
         */
        return $arr[$tableName] ?? ['brw_id' => $pengajuan->brw_id,];
    }

    /**
     * @param \App\BorrowerPersyaratanPendanaan $document
     * @param array $files
     * @param $columnName
     * @param \App\BorrowerPengajuan $pengajuan
     * @return string
     */
    private function uploadImage(BorrowerPersyaratanPendanaan $document, array $files, $columnName, BorrowerPengajuan $pengajuan)
    {
        // calculate height and width
        $totalHeight = $totalWidth = 0;
        $heights = [];
        /** @var UploadedFile $file */
        foreach ($files as $file) {
            $height = Image::make($file)->height();
            $heights[] = $height;
            $totalHeight += $height + 10;
            $totalWidth = max($totalWidth, Image::make($file)->width());
        }

        $path = sys_get_temp_dir() . '/' . Str::random();

        $image = Image::canvas($totalWidth, $totalHeight);
        $height = 0;
        foreach ($files as $key => $file) {
            $image->insert($file, 'top-left', 0, ($height += ($heights[$key - 1] ?? 0) + 10));
        }

        $image->save($path);

        $primaryField = self::getPrimaryFieldForDoc($pengajuan, $document->table_name);
        DB::table($document->table_name)->updateOrInsert($primaryField, [$columnName =>
            BorrowerFileService::uploadFile(
                new UploadedFile($path, "$columnName.jpeg"),
                "verify2_{$columnName}",
                $pengajuan->brw_id,
                BorrowerFileService::getDiskPath($pengajuan->brw_id) . "/$pengajuan->pengajuan_id"
            ),
        ]);

        return 'success';
    }

    /**
     * @param \App\BorrowerPersyaratanPendanaan $document
     * @param array $files
     * @param $columnName
     * @param \App\BorrowerPengajuan $pengajuan
     * @return string
     */
    private function uploadPdf(BorrowerPersyaratanPendanaan $document, array $files, $columnName, BorrowerPengajuan $pengajuan)
    {
        /** @var UploadedFile $file */
        $file = $files[0];
        $primaryField = self::getPrimaryFieldForDoc($pengajuan, $document->table_name);
        DB::table($document->table_name)->updateOrInsert($primaryField, [$columnName =>
            BorrowerFileService::uploadFile(
                $file,
                "verify2_{$columnName}",
                $pengajuan->brw_id,
                BorrowerFileService::getDiskPath($pengajuan->brw_id) . "/$pengajuan->pengajuan_id"
            ),
        ]);

        return 'success';
    }
}
