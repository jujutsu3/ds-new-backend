<?php

namespace App\Http\Controllers\Mobile;

use App\Borrower;
use App\BorrowerAdministrator;
use App\BorrowerAdministratorInterface;
use App\BorrowerContact;
use App\BorrowerCorporation;
use App\BorrowerDetails;
use App\BorrowerPemegangSaham;
use App\BorrowerPengurus;
use App\Http\Controllers\Controller;
use App\MasterProvinsi;
use App\Services\BorrowerService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

/**
 * @property \App\BorrowerDetails $detail
 */
class BorrowerCorporationProfileControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth:borrower-api-mobile',
            'borrower_type:' . BorrowerDetails::TYPE_CORPORATION,
        ], [
            'only' => [
                'getCorporation',
                'updateCorporation',
                'getAdministrator',
                'updateAdministrator',
                'getShareholder',
                'updateShareholder',
                'getContacts',
                'updateContacts',
                'verifyData',
            ],
        ]);
    }

    public function getCorporation(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        BorrowerService::getDetilBorrower($borrower);
        BorrowerService::getBorrowerRekening($borrower);

        $borrowerIdOriginalOwner = $borrower->brw_id;
        $check = self::picCheck($borrowerIdOriginalOwner);

        $data = $borrower->getData([
            BorrowerCorporation::NAMA,
            BorrowerCorporation::NO_SURAT_IZIN,
            BorrowerCorporation::NPWP,
            BorrowerCorporation::FOTO_NPWP,
            BorrowerCorporation::NO_AKTA_PENDIRIAN,
            BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN,
            BorrowerCorporation::NO_AKTA_PERUBAHAN,
            BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN,
            BorrowerCorporation::TELEPON,
            BorrowerCorporation::ALAMAT,
            BorrowerCorporation::PROVINSI,
            BorrowerCorporation::KABUPATEN,
            BorrowerCorporation::KECAMATAN,
            BorrowerCorporation::KELURAHAN,
            BorrowerCorporation::KODE_POS,
            BorrowerCorporation::REKENING,
            BorrowerCorporation::KODE_BANK,
            BorrowerCorporation::NAMA_PEMILIK_REKENING,
            BorrowerCorporation::BIDANG_USAHA,
            BorrowerCorporation::OMSET_TAHUN_TERAKHIR,
            BorrowerCorporation::TOTAL_ASET_TAHUN_TERAKHIR,
            BorrowerCorporation::LAPORAN_KEUANGAN,
        ])
            ->toArray();

        BorrowerCorporation::fieldFileAccess($data, BorrowerCorporation::FOTO_NPWP, $check);
        BorrowerCorporation::fieldFileAccess($data, BorrowerCorporation::LAPORAN_KEUANGAN, $check);
        BorrowerCorporation::fieldDateAccess($data, BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN);
        BorrowerCorporation::fieldDateAccess($data, BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN);

        return response()->json(
            array_merge($data, [
                'provinsi_nama' => optional(
                    MasterProvinsi::query()->where('kode_provinsi', $borrower->detail->provinsi)->first(),
                    function ($value) {
                        return $value->nama_provinsi;
                    }
                ),
                'kabupaten_nama' => optional(
                    MasterProvinsi::query()->where('kode_kota', $borrower->detail->kota)->first(),
                    function ($value) {
                        return $value->nama_kota;
                    }
                ),
            ])
        );
    }

    public function updateCorporation(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $borrowerDetails = BorrowerService::getDetilBorrower($borrower);
        $borrowerRekening = BorrowerService::getBorrowerRekening($borrower);

        DB::beginTransaction();
        try {
            // Ignore beberapa kolom sehingga tidak boleh di update
            $borrowerDetailDBMap = array_diff(BorrowerCorporation::getBorrowerDetailDbMap(), [
                BorrowerCorporation::NAMA,
                BorrowerCorporation::NO_AKTA_PENDIRIAN,
                BorrowerCorporation::NO_AKTA_PERUBAHAN,
                BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN,
                BorrowerCorporation::TELEPON,
                BorrowerCorporation::REKENING,
                BorrowerCorporation::NAMA_PEMILIK_REKENING,
                BorrowerCorporation::KODE_BANK,
            ]);

            Validator::make(
                $request->all(),
                BorrowerCorporation::getRules($borrower->brw_id),
                [],
                BorrowerCorporation::getAttributeName()
            )
                ->validate();

            $borrowerDetails
                ->forceFill(BorrowerService::mapToDatabase($request->toArray(), $borrowerDetailDBMap))
                ->forceFill([
                    $borrowerDetailDBMap[BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN] => BorrowerCorporation::fieldDateMutate(
                        $request->get(BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN),
                        $borrowerDetails->{BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN}
                    ),
                    $borrowerDetailDBMap[BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN] => BorrowerCorporation::fieldDateMutate(
                        $request->get(BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN),
                        $borrowerDetails->{BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN}
                    ),
                    'brw_type' => BorrowerDetails::TYPE_CORPORATION,
                ])
                ->save();

            $photoMessages = [];
            if ($file = $request->file(BorrowerCorporation::FOTO_NPWP)) {
                $photoMessages[BorrowerCorporation::FOTO_NPWP] = BorrowerCorporation::upload(
                    $borrowerDetailDBMap[BorrowerCorporation::FOTO_NPWP],
                    $file,
                    $borrowerDetails,
                    'pic_user_npwp_corporation'
                );
            }

            if ($file = $request->file(BorrowerCorporation::LAPORAN_KEUANGAN)) {
                $photoMessages[BorrowerCorporation::LAPORAN_KEUANGAN] = BorrowerCorporation::uploadLapKeuangan(
                    $borrowerDetailDBMap[BorrowerCorporation::LAPORAN_KEUANGAN],
                    $file,
                    $borrowerDetails,
                    'laporan_keuangan'
                );
            }

            DB::commit();

            return response()->json([
                'success' => 'Data perusahaan berhasil disimpan',
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => config('app.env') !== 'production'
                    ? $exception->getMessage()
                    : 'Terjadi kesalahan, silahkan coba lagi',
            ]);
        }
    }

    private function getGenericPerson(BorrowerCorporation $borrower, Collection $borrowerAdmins)
    {
        $borrowerIdOriginalOwner = $borrower->brw_id;

        $check = self::picCheck($borrowerIdOriginalOwner);

        $data = $borrowerAdmins->map(function (BorrowerAdministratorInterface $administrator) use ($check) {
            $neededColumns = [
                BorrowerAdministrator::NAMA,
                BorrowerAdministrator::JENIS_KELAMIN,
                BorrowerAdministrator::IDENTITAS,
                BorrowerAdministrator::TEMPAT_LAHIR,
                BorrowerAdministrator::TANGGAL_LAHIR,
                BorrowerAdministrator::NO_TELEPON,
                BorrowerAdministrator::AGAMA,
                BorrowerAdministrator::PENDIDIKAN,
                BorrowerAdministrator::NPWP,
                BorrowerAdministrator::JABATAN,
                BorrowerAdministrator::ALAMAT,
                BorrowerAdministrator::PROVINSI,
                BorrowerAdministrator::KABUPATEN,
                BorrowerAdministrator::KECAMATAN,
                BorrowerAdministrator::KELURAHAN,
                BorrowerAdministrator::KODE_POS,
                BorrowerAdministrator::FOTO_KTP,
                BorrowerAdministrator::FOTO_PROFILE_KTP,
                BorrowerAdministrator::FOTO_PROFILE,
                BorrowerAdministrator::FOTO_NPWP,
            ];

            $neededColumns = $this->getPemegangSahamModifier($administrator, $neededColumns);

            $datum = $administrator->getData($neededColumns);

            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_NPWP, $check);
            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_PROFILE, $check);
            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_PROFILE_KTP, $check);
            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_KTP, $check);
            $administrator::fieldDateAccess($datum, BorrowerAdministrator::TANGGAL_LAHIR);

            return array_merge($datum->toArray(), [
                'provinsi_nama' => optional(
                    MasterProvinsi::query()->where('kode_provinsi', $datum[BorrowerAdministrator::PROVINSI])->first(),
                    function ($value) {
                        return $value->nama_provinsi;
                    }
                ),
                'kabupaten_nama' => optional(
                    MasterProvinsi::query()->where('kode_kota', $datum[BorrowerAdministrator::KABUPATEN])->first(),
                    function ($value) {
                        return $value->nama_kota;
                    }
                ),
            ]);
        });

        return response()->json($data);
    }

    public function getAdministrator(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $admins = $borrower->admins;

        return $this->getGenericPerson($borrower, $admins);
    }

    public function getShareholder(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $shareHolders = $borrower->shareholders;

        return $this->getGenericPerson($borrower, $shareHolders);
    }

    public function getContacts(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $contacts = $borrower->contacts;

        return $this->getGenericPerson($borrower, $contacts);
    }

    private function updateGenericPerson(
        Request $request,
        Borrower $borrower,
        BorrowerAdministratorInterface $administrator
    ) {
        DB::beginTransaction();
        $requestKey = $administrator::getRequestIdentifier();
        $administratorMap = $administrator::getAdministratorDbMap();
        try {
            Validator::make(
                $request->all(),
                collect(
                    $administrator::getRules([],
                        $borrower->brw_id,
                        $borrower->$requestKey()->pluck($administrator->getKeyName())->toArray())
                )
                    ->mapWithKeys(function ($rule, $key) use ($administrator, $requestKey) {
                        if ($key === $administrator::JENIS_IDENTITAS) {
                            $rule[] = 'required_with:' . "$requestKey.*." . $administrator::IDENTITAS;
                        }

                        return ["{$requestKey}.*.$key" => $rule,];
                    })
                    ->merge([
                        $administrator::getRequestIdentifier() => ['array', 'min:1'],
                    ])
                    ->toArray(),
                [
                    "$requestKey.*." . $administrator::NPWP . ".digits_between" => 'Harap masukkan NPWP setidaknya :min digit',
                ],
                collect($administrator::getAttributeName())
                    ->mapWithKeys(function ($rule, $key) use ($requestKey) {
                        return ["{$requestKey}.*.$key" => $rule,];
                    })
                    ->toArray()
            )
                ->validate();

            $createdAtOld = $borrower->$requestKey()->pluck('created_at');
            $borrower->$requestKey()->delete();
            $photoMessages = [];
            foreach ($request->get($requestKey) ?? [] as $key => $adminRequest) {
                $administrator::additionalValidator($adminRequest, $administrator, $requestKey, $key);

                $admin = $borrower->$requestKey()->make()
                    ->forceFill(BorrowerService::mapToDatabase($adminRequest, $administratorMap))
                    ->forceFill([
                        'brw_id' => $borrower->brw_id,
                        $administratorMap[$administrator::TANGGAL_LAHIR] => $administrator::fieldDateMutate(
                            $adminRequest[$administrator::TANGGAL_LAHIR] ?? null,
                            $administrator->{$administrator::TANGGAL_LAHIR}
                        ),
                        $administratorMap[$administrator::NO_TELEPON] => $administrator::fieldPhoneMutate(
                            $adminRequest[$administrator::NO_TELEPON] ?? null,
                            $adminRequest[$administrator::KODE_OPERATOR] ?? null
                        ),
                    ]);

                $mapRequestToColumnPhoto = array_intersect_key(
                    $administratorMap,
                    array_flip([
                        $administrator::FOTO_KTP,
                        $administrator::FOTO_PROFILE_KTP,
                        $administrator::FOTO_PROFILE,
                        $administrator::FOTO_NPWP,
                    ])
                );

                $photoMessages[$key] = [];
                foreach ($mapRequestToColumnPhoto as $requestField => $columnKey) {
                    if ($file = $request->file("$requestKey.$key.$requestField")) {
                        $photoMessages[$key][$requestField] = BorrowerCorporation::upload(
                            $columnKey,
                            $file,
                            $admin,
                            "pic_corp_{$requestKey}_{$key}_$columnKey"
                        );
                    }
                }

                /** @var \Illuminate\Database\Eloquent\Model $admin */
                $admin->timestamps = false;
                $admin->forceFill([
                    $admin::CREATED_AT => $createdAtOld[$key] ?? Carbon::now(),
                    $admin::UPDATED_AT => $admin->{$admin->getUpdatedAtColumn()} ?? Carbon::now(),
                ])
                    ->save();
            }

            DB::commit();

            return response()->json([
                'success' => "Data $requestKey berhasil disimpan",
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'error' => config('app.env') !== 'production' ? $exception->getMessage(
                    ) : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateAdministrator(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        return $this->updateGenericPerson($request, $borrower, new BorrowerPengurus());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateShareholder(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        return $this->updateGenericPerson($request, $borrower, new BorrowerPemegangSaham());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateContacts(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        return $this->updateGenericPerson($request, $borrower, new BorrowerContact());
    }

    public function verifyData(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        if ($request->has('email') && $request->email === $borrower->email) {
            return response()->json([
                'success' => 'Email Terverifikasi',
            ]);
        }
        $phoneCheck = function () use ($request, $borrower) {
            if (! $request->has('phone')) {
                return false;
            }

            $contact = $borrower->contacts()->oldest()->first();
            if (! $contact) {
                return false;
            }

            $map = BorrowerContact::getAdministratorDbMap();
            $phone = $contact->{$map[BorrowerContact::NO_TELEPON]};

            $requestPhone = $request->get('kode_operator') . $request->get('phone');
            if ($phone !== $requestPhone) {
                return false;
            }
            
            return true;
        };
        
        if ($phoneCheck()) {
            return response()->json([
                'success' => 'No HP Terverifikasi',
            ]);
        }


        return response()->json([
            'errors' => 'Data tidak sesuai',
        ]);
    }

    private static function picCheck($borrowerIdOriginalOwner)
    {
        return static function ($borrowerId) use ($borrowerIdOriginalOwner) {
            return $borrowerId === $borrowerIdOriginalOwner;
        };
    }

    /**
     * @param \App\BorrowerAdministratorInterface $administrator
     * @param array $neededColumns
     * @return array|int[]|string[]
     */
    private function getPemegangSahamModifier(
        BorrowerAdministratorInterface $administrator,
        array $neededColumns
    ): array {
        if (! $administrator instanceof BorrowerPemegangSaham) {
            return $neededColumns;
        }

        $neededColumns[] = BorrowerPemegangSaham::JENIS_PEMEGANG_SAHAM;
        $neededColumns[] = BorrowerPemegangSaham::NILAI_SAHAM;
        $neededColumns[] = BorrowerPemegangSaham::LEMBAR_SAHAM;

        if ($administrator->{BorrowerPemegangSaham::JENIS_PEMEGANG_SAHAM} === BorrowerPemegangSaham::PMG_SHM_BDN_HKM) {
            $temp = array_flip($neededColumns);
            unset(
                $temp[$administrator::AGAMA],
                $temp[$administrator::JENIS_KELAMIN],
                $temp[$administrator::TEMPAT_LAHIR],
                $temp[$administrator::TANGGAL_LAHIR],
                $temp[$administrator::PENDIDIKAN],
                $temp[$administrator::JABATAN]
            );
            $neededColumns = array_flip($temp);
        }

        return $neededColumns;
    }
}
