<?php

namespace App\Http\Controllers\Mobile;

use App\Borrower;
use App\BorrowerAdministrator;
use App\BorrowerAdministratorInterface;
use App\BorrowerContact;
use App\BorrowerCorporation;
use App\BorrowerDetails;
use App\BorrowerKYCStep;
use App\BorrowerPemegangSaham;
use App\BorrowerPengurus;
use App\Constants\KYCStatus;
use App\Http\Controllers\Controller;
use App\Services\BorrowerService;
use App\Services\KYCService;
use App\Services\OTPService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

/**
 * @property \App\BorrowerDetails $detail
 */
class BorrowerCorporationKYCControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware([
            'auth:borrower-api-mobile',
            'borrower_type:' . BorrowerDetails::TYPE_CORPORATION,
        ], [
            'only' => [
                'currentStatus',
                'getCorporation',
                'updateCorporation',
                'getAdministrator',
                'updateAdministrator',
                'getShareholder',
                'updateShareholder',
                'getContacts',
                'updateContacts',
                'profilePic',
                'updateProfilePic',
                'sendOtp',
                'updateOtp',
            ],
        ]);
    }

    public function allStatus()
    {
        return response()->json(
            BorrowerKYCStep::corporation()
                ->orderBy('sort')
                ->pluck('name', 'sort')
        );
    }

    private function getCurrentStatus(Borrower $borrower)
    {
        if ($borrower->kyc_step === null) {
            /** @var BorrowerKYCStep $first */
            $first = BorrowerKYCStep::corporation()->orderBy('sort', 'ASC')->first();
            $borrower->forceFill([
                'kyc_step' => $first->name,
            ])
                ->save();
        }

        /** @var BorrowerKYCStep $status */
        $status = BorrowerKYCStep::corporation()->where('name', $borrower->kyc_step)->first();

        return [$status->sort, $status->name];
    }

    public function currentStatus(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        if ($borrower->status === Borrower::STATUS_ACTIVE) {
            $borrower->forceFill(['kyc_step' => BorrowerKYCStep::STEP_IND_ACTIVE,])->save();
        }

        [$stepOrder, $stepName] = $this->getCurrentStatus($borrower);
        $status['verify_status'] = $borrower->status;

        $borrowerDetails = BorrowerService::getDetilBorrower($borrower);
        $corporationIncompleteFields = $administratorIncompleteFields = $shareholderIncompleteFields = $contactIncompleteFields = [];
        $status = [
            'corporation' => $this->checkCorporation($borrowerDetails, $corporationIncompleteFields),
            'pengurus' => $this->checkAdministrator($borrower, $administratorIncompleteFields),
            'pemegang_saham' => $this->checkShareholder($borrower, $shareholderIncompleteFields),
            'contact' => $this->checkContact($borrower, $contactIncompleteFields),
            'step_order' => $stepOrder,
            'step' => $stepName,
            'account_status' => $borrower->status,
            'incomplete_fields' => [
                'corporation' => $corporationIncompleteFields,
                'pengurus' => $administratorIncompleteFields,
                'pemegang_saham' => $shareholderIncompleteFields,
                'contact' => $contactIncompleteFields,
            ],
        ];

        return response()->json($status);
    }

    private function checkCorporation(BorrowerDetails $borrowerDetails, &$incompleteFields = [])
    {
        $neededColumns = [
            BorrowerCorporation::NAMA,
            BorrowerCorporation::NO_SURAT_IZIN,
            BorrowerCorporation::NPWP,
            BorrowerCorporation::NO_AKTA_PENDIRIAN,
            BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN,
            BorrowerCorporation::NO_AKTA_PERUBAHAN,
            BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN,
            BorrowerCorporation::TELEPON,
            BorrowerCorporation::ALAMAT,
            BorrowerCorporation::PROVINSI,
            BorrowerCorporation::KABUPATEN,
            BorrowerCorporation::KECAMATAN,
            BorrowerCorporation::KELURAHAN,
            BorrowerCorporation::KODE_POS,
            BorrowerCorporation::REKENING,
            BorrowerCorporation::KODE_BANK,
            BorrowerCorporation::NAMA_PEMILIK_REKENING,
            BorrowerCorporation::BIDANG_USAHA,
            BorrowerCorporation::OMSET_TAHUN_TERAKHIR,
            BorrowerCorporation::TOTAL_ASET_TAHUN_TERAKHIR,
            BorrowerCorporation::FOTO_NPWP,
            BorrowerCorporation::NAMA_NOTARIS_AKTA_PENDIRIAN,
            // BorrowerCorporation::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN,
            BorrowerCorporation::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            // BorrowerCorporation::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
            // BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
        ];

        $columns = collect(BorrowerCorporation::getBorrowerDetailDbMap())
            ->intersectByKeys(array_flip($neededColumns))
            ->values()
            ->toArray();

        return KYCService::baseCheck($columns, $borrowerDetails, $incompleteFields);
    }

    public function getCorporation(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        BorrowerService::getDetilBorrower($borrower);
        BorrowerService::getBorrowerRekening($borrower);

        $borrowerIdOriginalOwner = $borrower->brw_id;
        $check = self::picCheck($borrowerIdOriginalOwner);

        $data = $borrower->getData([
            BorrowerCorporation::NAMA,
            BorrowerCorporation::NO_SURAT_IZIN,
            BorrowerCorporation::NPWP,
            BorrowerCorporation::FOTO_NPWP,
            BorrowerCorporation::NO_AKTA_PENDIRIAN,
            BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN,
            BorrowerCorporation::NO_AKTA_PERUBAHAN,
            BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN,
            BorrowerCorporation::TELEPON,
            BorrowerCorporation::ALAMAT,
            BorrowerCorporation::PROVINSI,
            BorrowerCorporation::KABUPATEN,
            BorrowerCorporation::KECAMATAN,
            BorrowerCorporation::KELURAHAN,
            BorrowerCorporation::KODE_POS,
            BorrowerCorporation::REKENING,
            BorrowerCorporation::KODE_BANK,
            BorrowerCorporation::NAMA_PEMILIK_REKENING,
            BorrowerCorporation::BIDANG_USAHA,
            BorrowerCorporation::OMSET_TAHUN_TERAKHIR,
            BorrowerCorporation::TOTAL_ASET_TAHUN_TERAKHIR,
            BorrowerCorporation::LAPORAN_KEUANGAN,
            BorrowerCorporation::NAMA_NOTARIS_AKTA_PENDIRIAN,
            BorrowerCorporation::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN,
            BorrowerCorporation::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            BorrowerCorporation::NAMA_NOTARIS_AKTA_PERUBAHAN,
            BorrowerCorporation::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN,
            BorrowerCorporation::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
            BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN,
        ])
            ->toArray();

        BorrowerCorporation::fieldFileAccess($data, BorrowerCorporation::FOTO_NPWP, $check);
        BorrowerCorporation::fieldFileAccess($data, BorrowerCorporation::LAPORAN_KEUANGAN, $check);
        BorrowerCorporation::fieldDateAccess($data, BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN);
        BorrowerCorporation::fieldDateAccess($data, BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN);
        BorrowerCorporation::fieldDateAccess($data, BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN);
        BorrowerCorporation::fieldDateAccess($data, BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN);

        return response()->json(
            array_merge($data, [])
        );
    }

    public function updateCorporation(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $borrowerDetails = BorrowerService::getDetilBorrower($borrower);
        $borrowerRekening = BorrowerService::getBorrowerRekening($borrower);

        DB::beginTransaction();
        try {
            Validator::make(
                $request->all(),
                BorrowerCorporation::getRules($borrower->brw_id),
                [],
                BorrowerCorporation::getAttributeName()
            )
                ->validate();

            $borrowerDetailDBMap = BorrowerCorporation::getBorrowerDetailDbMap();
            $borrowerDetails
                ->forceFill(BorrowerService::mapToDatabase($request->toArray(), $borrowerDetailDBMap))
                ->forceFill([
                    $borrowerDetailDBMap[BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN] => BorrowerCorporation::fieldDateMutate(
                        $request->get(BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN),
                        $borrowerDetails->{BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN}
                    ),
                    $borrowerDetailDBMap[BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN] => BorrowerCorporation::fieldDateMutate(
                        $request->get(BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN),
                        $borrowerDetails->{BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN}
                    ),

                    $borrowerDetailDBMap[BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN] => BorrowerCorporation::fieldDateMutate(
                        $request->get(BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN),
                        $borrowerDetails->{$borrowerDetailDBMap[BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN]}
                    ),

                    $borrowerDetailDBMap[BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN] => BorrowerCorporation::fieldDateMutate(
                        $request->get(BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN),
                        $borrowerDetails->{$borrowerDetailDBMap[BorrowerCorporation::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN]}
                    ),

                    'brw_type' => BorrowerDetails::TYPE_CORPORATION,
                ])
                ->save();

            $borrowerRekening
                ->forceFill(
                    BorrowerService::mapToDatabase($request->toArray(), BorrowerCorporation::getBorrowerRekeningDbMap())
                )
                ->save();

            $photoMessages = [];
            if ($file = $request->file(BorrowerCorporation::FOTO_NPWP)) {
                $photoMessages[BorrowerCorporation::FOTO_NPWP] = BorrowerCorporation::upload(
                    $borrowerDetailDBMap[BorrowerCorporation::FOTO_NPWP],
                    $file,
                    $borrowerDetails,
                    'pic_user_npwp_corporation'
                );
            }

            if ($file = $request->file(BorrowerCorporation::LAPORAN_KEUANGAN)) {
                $photoMessages[BorrowerCorporation::LAPORAN_KEUANGAN] = BorrowerCorporation::uploadLapKeuangan(
                    $borrowerDetailDBMap[BorrowerCorporation::LAPORAN_KEUANGAN],
                    $file,
                    $borrowerDetails,
                    'laporan_keuangan'
                );
            }

            self::bumpStatus($borrower, BorrowerKYCStep::STEP_COR_DATA);

            DB::commit();

            return response()->json([
                'success' => 'Data perusahaan berhasil disimpan',
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'error' => config('app.env') !== 'production' ? $exception->getMessage(
                    ) : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }
    }

    /**
     * @param \App\Borrower $borrower
     * @param \App\BorrowerAdministratorInterface $administrator
     * @param $incompleteFields
     * @return string
     */
    private function checkGenericPerson(
        Borrower $borrower,
        BorrowerAdministratorInterface $administrator,
        &$incompleteFields
    ) {
        $neededColumns = [
            BorrowerAdministrator::NAMA,
            BorrowerAdministrator::JENIS_KELAMIN,
            BorrowerAdministrator::JENIS_IDENTITAS,
            BorrowerAdministrator::IDENTITAS,
            BorrowerAdministrator::TEMPAT_LAHIR,
            BorrowerAdministrator::TANGGAL_LAHIR,
            BorrowerAdministrator::NO_TELEPON,
            BorrowerAdministrator::AGAMA,
            BorrowerAdministrator::PENDIDIKAN,
            BorrowerAdministrator::NPWP,
            BorrowerAdministrator::JABATAN,
            BorrowerAdministrator::ALAMAT,
            BorrowerAdministrator::PROVINSI,
            BorrowerAdministrator::KABUPATEN,
            BorrowerAdministrator::KECAMATAN,
            BorrowerAdministrator::KELURAHAN,
            BorrowerAdministrator::KODE_POS,
            BorrowerAdministrator::FOTO_KTP,
            BorrowerAdministrator::FOTO_PROFILE,
            BorrowerAdministrator::FOTO_PROFILE_KTP,
            BorrowerAdministrator::FOTO_NPWP,

        ];

        $rel = $administrator::getRequestIdentifier();

        $incompleteFields = [];
        $checks = [];
        $administrators = $borrower->$rel()->oldest()->get();
        foreach ($administrators as $key => $admin) {
            $neededColumns = $this->getPemegangSahamModifier($admin, $neededColumns);

            $columns = collect($administrator::getAdministratorDbMap())
                ->intersectByKeys(array_flip($neededColumns))
                ->values()
                ->toArray();

            $incompleteFields[$key] = [];
            $checks[] = KYCService::baseCheck($columns, $admin, $incompleteFields[$key]);
        }

        return KYCService::multipleCheck($checks);
    }

    private function checkAdministrator(Borrower $borrower, &$incompleteFields = [])
    {
        return $this->checkGenericPerson($borrower, new BorrowerPengurus(), $incompleteFields);
    }

    private function checkShareholder(Borrower $borrower, &$incompleteFields = [])
    {
        return $this->checkGenericPerson($borrower, new BorrowerPemegangSaham(), $incompleteFields);
    }

    private function checkContact(Borrower $borrower, &$incompleteFields = [])
    {
        return $this->checkGenericPerson($borrower, new BorrowerContact(), $incompleteFields);
    }

    private function getGenericPerson(BorrowerCorporation $borrower, Collection $borrowerAdmins)
    {
        $borrowerIdOriginalOwner = $borrower->brw_id;

        $check = self::picCheck($borrowerIdOriginalOwner);

        $data = $borrowerAdmins->map(function (BorrowerAdministratorInterface $administrator) use ($check) {
            $neededColumns = [
                BorrowerAdministrator::NAMA,
                BorrowerAdministrator::JENIS_KELAMIN,
                BorrowerAdministrator::IDENTITAS,
                BorrowerAdministrator::JENIS_IDENTITAS,
                BorrowerAdministrator::TEMPAT_LAHIR,
                BorrowerAdministrator::TANGGAL_LAHIR,
                BorrowerAdministrator::NO_TELEPON,
                BorrowerAdministrator::AGAMA,
                BorrowerAdministrator::PENDIDIKAN,
                BorrowerAdministrator::NPWP,
                BorrowerAdministrator::JABATAN,
                BorrowerAdministrator::ALAMAT,
                BorrowerAdministrator::PROVINSI,
                BorrowerAdministrator::KABUPATEN,
                BorrowerAdministrator::KECAMATAN,
                BorrowerAdministrator::KELURAHAN,
                BorrowerAdministrator::KODE_POS,
                BorrowerAdministrator::FOTO_KTP,
                BorrowerAdministrator::FOTO_PROFILE_KTP,
                BorrowerAdministrator::FOTO_PROFILE,
                BorrowerAdministrator::FOTO_NPWP,
            ];

            $neededColumns = $this->getPemegangSahamModifier($administrator, $neededColumns);

            $datum = $administrator->getData($neededColumns);

            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_NPWP, $check);
            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_PROFILE, $check);
            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_PROFILE_KTP, $check);
            $administrator::fieldFileAccess($datum, BorrowerAdministrator::FOTO_KTP, $check);
            $administrator::fieldDateAccess($datum, BorrowerAdministrator::TANGGAL_LAHIR);
            $administrator::fieldPhoneAccess($datum, BorrowerAdministrator::NO_TELEPON);

            return array_merge($datum->toArray(), []);
        });

        return response()->json($data);
    }

    public function getAdministrator(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $admins = $borrower->admins;

        return $this->getGenericPerson($borrower, $admins);
    }

    public function getShareholder(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $shareHolders = $borrower->shareholders;

        return $this->getGenericPerson($borrower, $shareHolders);
    }

    public function getContacts(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $contacts = $borrower->contacts;

        return $this->getGenericPerson($borrower, $contacts);
    }

    private function updateGenericPerson(
        Request $request,
        Borrower $borrower,
        BorrowerAdministratorInterface $administrator
    ) {
        DB::beginTransaction();
        $requestKey = $administrator::getRequestIdentifier();
        $administratorMap = $administrator::getAdministratorDbMap();
        try {
            Validator::make(
                $request->all(),
                collect(
                    $administrator::getRules([],
                        $borrower->brw_id,
                        $borrower->$requestKey()->pluck($administrator->getKeyName())->toArray())
                )
                    ->mapWithKeys(function ($rule, $key) use ($administrator, $requestKey) {
                        if ($key === $administrator::JENIS_IDENTITAS) {
                            $rule[] = 'required_with:' . "$requestKey.*." . $administrator::IDENTITAS;
                        }

                        return ["{$requestKey}.*.$key" => $rule,];
                    })
                    ->merge([
                        $administrator::getRequestIdentifier() => ['array', 'min:1'],
                    ])
                    ->toArray(),
                [
                    "$requestKey.*." . $administrator::NPWP . ".digits_between" => 'Harap masukkan NPWP setidaknya :min digit',
                ],
                collect($administrator::getAttributeName())
                    ->mapWithKeys(function ($rule, $key) use ($requestKey) {
                        return ["{$requestKey}.*.$key" => $rule,];
                    })
                    ->toArray()
            )
                ->validate();

            $borrower->$requestKey()->delete();
            $photoMessages = [];
            foreach ($request->get($requestKey) ?? [] as $key => $adminRequest) {
                $administrator::additionalValidator($adminRequest, $administrator, $requestKey, $key);

                $admin = $borrower->$requestKey()->make()
                    ->forceFill(BorrowerService::mapToDatabase($adminRequest, $administratorMap))
                    ->forceFill([
                        'brw_id' => $borrower->brw_id,
                        $administratorMap[$administrator::TANGGAL_LAHIR] => $administrator::fieldDateMutate(
                            $adminRequest[$administrator::TANGGAL_LAHIR] ?? null,
                            $administrator->{$administrator::TANGGAL_LAHIR}
                        ),
                        $administratorMap[$administrator::NO_TELEPON] => $administrator::fieldPhoneMutate(
                            $adminRequest[$administrator::NO_TELEPON] ?? null,
                            $adminRequest[$administrator::KODE_OPERATOR] ?? null
                        ),
                    ]);

                $mapRequestToColumnPhoto = array_intersect_key(
                    $administratorMap,
                    array_flip([
                        $administrator::FOTO_KTP,
                        $administrator::FOTO_PROFILE_KTP,
                        $administrator::FOTO_PROFILE,
                        $administrator::FOTO_NPWP,
                    ])
                );

                $photoMessages[$key] = [];
                foreach ($mapRequestToColumnPhoto as $requestField => $columnKey) {
                    if ($file = $request->file("$requestKey.$key.$requestField")) {
                        $photoMessages[$key][$requestField] = BorrowerCorporation::upload(
                            $columnKey,
                            $file,
                            $admin,
                            "pic_corp_{$requestKey}_{$key}_$columnKey"
                        );
                    }
                }

                $admin->save();
            }

            self::bumpStatus($borrower, BorrowerKYCStep::STEP_COR_DATA);

            DB::commit();

            return response()->json([
                'success' => "Data $requestKey berhasil disimpan",
                'photos' => $photoMessages,
            ]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'error' => config('app.env') !== 'production' ? $exception->getMessage(
                    ) : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateAdministrator(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        return $this->updateGenericPerson($request, $borrower, new BorrowerPengurus());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateShareholder(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        return $this->updateGenericPerson($request, $borrower, new BorrowerPemegangSaham());
    }

    /**
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateContacts(Request $request)
    {
        $borrower = BorrowerService::getBorrower();

        return $this->updateGenericPerson($request, $borrower, new BorrowerContact());
    }

    public function sendOtp(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();
        $borrowerDetail = BorrowerService::getDetilBorrower($borrower);

        if (KYCService::multipleCheck([
                $this->checkCorporation($borrowerDetail),
                $this->checkAdministrator($borrower),
                $this->checkShareholder($borrower),
                $this->checkContact($borrower),
            ]) !== KYCStatus::COMPLETE_TRUE) {
            return response()->json(['error' => 'pengisian KYC harus selesai']);
        }

        $phone = OTPService::trim($request->no_telp);
        
        if (App::isLocale('en')) {
            $message = 'Phone :input is already registered';
        } else {
            $message = 'Nomor ponsel :input sudah terdaftar';
        }

        Validator::make([
            'no_telp' => $phone,
        ], [
            'no_telp' => [
                Rule::unique('brw_user_detail', 'no_tlp'),
                Rule::unique('brw_corp_contact', 'no_tlp')->ignore($borrowerDetail->brw_id, 'brw_id')
            ],
        ], [
            'no_telp.unique' => $message
        ])
            ->validate();

        [$status, $message] = OTPService::borrowerSend($borrower, $request->no_telp);
        // [$status, $message] = [true, 'test'];

        if ($status) {
            self::bumpStatus($borrower, BorrowerKYCStep::STEP_COR_OTP);

            return response()->json(['success' => $message]);
        }

        return response()->json(['error' => $message]);
    }

    public function updateOtp(Request $request)
    {
        $borrower = BorrowerService::getBorrowerCorporation();

        if ((string) $request->no_otp === (string) $borrower->otp) {
            // if (MasterParameter::getParam(MasterParameter::INVESTOR_KYC_MODE) === 'automatic') {
            //
            //     $this->generateVA($investor);
            //
            //     $investor->forceFill(['status' => Investor::STATUS_ACTIVE])->save();
            self::bumpStatus($borrower, BorrowerKYCStep::STEP_IND_ACTIVE);
            // } else {
            //     self::bumpStatus($investor, InvestorKYCStep::STEP_IND_VERIFICATION);
            // }

            return response()->json(['status' => 'success']);
        }

        return response()->json(['error' => 'OTP yang anda masukan salah']);
    }

    private static function bumpStatus(Borrower $borrower, string $newStatus)
    {
        $statuses = BorrowerKYCStep::corporation()->whereIn('name', [$newStatus, $borrower->kyc_step])->pluck(
            'sort',
            'name'
        );

        $current = $statuses[$borrower->kyc_step] ?? null;
        $new = $statuses[$newStatus] ?? null;

        if ($current === null || $new === null) {
            return response()->json(
                [
                    'error' => config(
                        'app.env'
                    ) !== 'production' ? 'Master Data Step KYC Tidak Valid' : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }

        if ($new > $current) {
            $borrower->forceFill([
                'kyc_step' => $newStatus,
            ])
                ->save();
        }
    }

    private static function picCheck($borrowerIdOriginalOwner)
    {
        return static function ($borrowerId) use ($borrowerIdOriginalOwner) {
            return $borrowerId === $borrowerIdOriginalOwner;
        };
    }

    /**
     * @param \App\BorrowerAdministratorInterface $administrator
     * @param array $neededColumns
     * @return array|int[]|string[]
     */
    private function getPemegangSahamModifier(
        BorrowerAdministratorInterface $administrator,
        array $neededColumns
    ): array {
        if (! $administrator instanceof BorrowerPemegangSaham) {
            return $neededColumns;
        }

        $neededColumns[] = BorrowerPemegangSaham::JENIS_PEMEGANG_SAHAM;
        $neededColumns[] = BorrowerPemegangSaham::NILAI_SAHAM;
        $neededColumns[] = BorrowerPemegangSaham::LEMBAR_SAHAM;

        if ($administrator->{BorrowerPemegangSaham::JENIS_PEMEGANG_SAHAM} === BorrowerPemegangSaham::PMG_SHM_BDN_HKM) {
            $temp = array_flip($neededColumns);
            unset(
                $temp[$administrator::AGAMA],
                $temp[$administrator::JENIS_KELAMIN],
                $temp[$administrator::TEMPAT_LAHIR],
                $temp[$administrator::TANGGAL_LAHIR],
                $temp[$administrator::PENDIDIKAN],
                $temp[$administrator::JABATAN],
                $temp[$administrator::FOTO_KTP],
                $temp[$administrator::FOTO_PROFILE_KTP],
                $temp[$administrator::FOTO_PROFILE]
            );
            $neededColumns = array_flip($temp);
        }

        return $neededColumns;
    }
}
