<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FileControllerV1 extends Controller
{
    public function getFile(Request $request)
    {
        $owner = Auth::id();
        return FileService::getFile($owner, $request->get('token'));
    }
}
