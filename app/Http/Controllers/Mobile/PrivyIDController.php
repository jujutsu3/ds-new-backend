<?php

namespace App\Http\Controllers\Mobile;

use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PrivyController;
use App\CheckUserSign;
use App\LogAkadDigiSignInvestor;
use Auth;
use DB;

// ARL 2022-04-23
use App\DetilInvestor;
use App\BorrowerDetails;

class PrivyIDController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['checkRegistrasi']]);
    }

    public function checkRegistrasi(Request $request)
    {

        $user_id = Auth::guard('api')->user()->id;
        $user_type = $request->user_type;
        $privyID = new PrivyController;

        $check_threshold = DB::select("select func_check_threshold('$user_id') as data_return");

        if ($check_threshold[0]->data_return == 1) {
            $return_check_register = $privyID->checkRegistrasi($user_id, $user_type);
        } else {
            $return_check_register = response()->json(['under_threshold' => 'Total Dana dibawah Threshold']);
        };

        return $return_check_register;
    }

    public function checkDocumentStatus()
    {

        $user_id = Auth::guard('api')->user()->id;

        $privyID = new PrivyController;

        $return_check_doc_status = $privyID->checkDocumentStatus($user_id);

        return $return_check_doc_status;
    }

    public function checkStatusDocumentPrivy(Request $request)
    {

        $privyID = new PrivyController;

        $return_check_status = $privyID->checkStatusDocumentPrivy($request->docToken);

        return $return_check_status;
    }

    public function sendDocMurobahahInvestorBorrower(Request $request)
    {

        $user_id = Auth::guard('api')->user()->id;
        $id_proyek = $request->id_proyek;

        $privyID = new PrivyController;

        $return_send_doc_morobahah = $privyID->sendDocMurobahahInvestorBorrower($id_proyek, $user_id);

        return $return_send_doc_morobahah;
    }

    public function createDocMurobahahInvestorBorrower(Request $request)
    {

        $user_id = Auth::guard('api')->user()->id;
        $id_proyek = $request->id_proyek;

        $privyID = new PrivyController;

        $return_create_doc_morobahah = $privyID->createDocMurobahahInvestorBorrower($user_id, $id_proyek);

        return $return_create_doc_morobahah;
    }

    public function sendDocAkadWakalahBilUjrohInvestor()
    {

        $user_id = Auth::guard('api')->user()->id;

        $privyID = new PrivyController;

        $return_send_akad_wakalah_bilujroh = $privyID->sendDocAkadWakalahBilUjrohInvestor($user_id, 2);

        return $return_send_akad_wakalah_bilujroh;
    }

    public function viewTTD(Request $request)
    {

        $user_id = Auth::guard('api')->user()->id;

        $privyID = new PrivyController;

        $link = "/user/viewTTD" + $request->privyID + "/user/viewTTD/" + $request->token + '/' + $request->proyek_id + '/' + $request->qty + '/' + $user_id;

        $return_viewTTD = $privyID->viewTTD($request->privyID, $request->x, $request->y, $request->page, $request->token, $request->proyek_id, $request->qty, $user_id);

        return $link;
    }
    public function checkDocumentStatusWakalah()
    {

        $user_id = Auth::guard('api')->user()->id;

        $privyID = new PrivyController;

        $return_status_wakalah = $privyID->checkDocumentStatusWakalah($user_id);

        return $return_status_wakalah;
    }
    public function RegisterPrivyID(Request $request)
    {

        $user_id = Auth::guard('api')->user()->id;

        $privyID = new PrivyController;

        // ARL 2022-04-23
        if ($request->user_type == 1) { // user type 1 = investor & 2 = borrower

            $DataUsers = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
                ->where('investor.id', $user_id)
                ->first();
        } else {

            $DataUsers = BorrowerDetails::leftJoin('brw_user', 'brw_user.brw_id', '=', 'brw_user_detail.brw_id')
                ->where('brw_user.brw_id', $user_id)
                ->first();
        }

        // dd($DataUsers);

        $fotoDiri = $privyID->cekFotoDiriExist($user_id, $request->user_type);
        if ($fotoDiri == NULL) {

            // $err = array(
            //      ['field' => 'ID', 'messages' => 'Ambil Foto Diri Anda / Pleaes take your selfie photo']

            // );

            $err = array(
                ['field' => 'selfie', 'messages' => array('cannot be blank')]

           );

            return response()->json(['code' => 422, 'errors' => $err]);

        }

        $fotoKtp = $privyID->cekFotoKtpExist($user_id, $request->user_type);
        if ($fotoKtp == NULL) {
            // $err = array(
            //     ['field' => 'ID', 'messages' => 'Ambil foto Identitas Anda yang masih berlaku / Please take your valid ID Card']
            // );

            $err = array(
                ['field' => 'selfie', 'messages' => array('cannot be blank')]

           );

            return response()->json(['code' => 422, 'errors' => $err]);
        }
        // ARL -===== 2022-04-03


        try {
            $return_register = $privyID->RegisterPrivyID($user_id, $request->user_type);
        } catch (ClientException $clientException) {
            if ($clientException->getResponse()->getStatusCode() === 422) {
                $message = json_decode($clientException->getResponse()->getBody()->getContents());
                $return_register = response()->json($message, 422);
            }
        }

        return $return_register;
    }
    public function generateFormRDL()
    {

        $user_id = Auth::guard('api')->user()->id;

        $privyID = new PrivyController;

        $return_generateFormRDL = $privyID->generateFormRDL($user_id);

        return $return_generateFormRDL;
    }
    public function getDocTokenWakalahInvestor()
    {

        $user_id = Auth::guard('api')->user()->id;

        $privyID = new PrivyController;

        $return_getDocTokenWakalahInvestor = $privyID->getDocTokenWakalahInvestor($user_id);

        return $return_getDocTokenWakalahInvestor;
    }
}
