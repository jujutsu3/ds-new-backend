<?php

namespace App\Http\Controllers\Mobile;

use App\BorrowerDetails;
use App\BorrowerIndividu;
use App\Constants\JenisKelamin;
use App\Http\Controllers\Controller;
use App\MasterKodePos;
use App\Services\BorrowerFileService;
use App\Services\BorrowerService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class BorrowerProfileControllerV2 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', [
            'only' => [
                'getProfile',
                'updateProfile',
                'getEducation',
                'updateEducation',
                'verifyPassword',
                'changePassword',
                'changePasswordExpired',
                'verifyData',
            ],
        ]);
    }

    public function getProfile()
    {
        $borrower = BorrowerService::getBorrower();
        $borrowerIdOriginalOwner = $borrower->brw_id;
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);

        $check = static function ($borrowerId) use ($borrowerIdOriginalOwner) {
            return $borrowerId === $borrowerIdOriginalOwner;
        };

        return response()->json([
            'nama' => $detilBorrower->nama,
            'username' => $borrower->username,
            'email' => $borrower->email,
            'nik' => $detilBorrower->ktp,
            'tempat_lahir' => $detilBorrower->tempat_lahir,
            'tanggal_lahir' => optional($detilBorrower->tgl_lahir, function ($value) {
                return Carbon::parse($value)->format('d-m-Y');
            }),
            'agama' => $detilBorrower->agama,
            'status_pernikahan' => $detilBorrower->status_kawin,
            'provinsi' => $detilBorrower->provinsi,
            'kabupaten' => $detilBorrower->kota,
            'kecamatan' => $detilBorrower->kecamatan,
            'kelurahan' => $detilBorrower->kelurahan,
            'kode_pos' => $detilBorrower->kode_pos,
            'alamat' => $detilBorrower->alamat,
            // 'domisili_status_rumah' => $detilBorrower->domisili_status_rumah,
            'status_rumah' => $detilBorrower->status_rumah,
            'pendidikan' => $detilBorrower->pendidikan_terakhir,
            'no_telp' => $detilBorrower->no_tlp,
            'jenis_kelamin' => $detilBorrower->jns_kelamin,
            'ktp' => $detilBorrower->brw_pic_ktp,
            'ktp_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic_ktp, $check),
            'profile_ktp' => $detilBorrower->brw_pic_user_ktp,
            'profile_ktp_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic_user_ktp, $check),
            'profile' => $detilBorrower->brw_pic,
            'profile_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic, $check),
            'npwp' => $detilBorrower->brw_pic_npwp,
            'npwp_full' => BorrowerFileService::makeUrl($detilBorrower->brw_pic_npwp, $check),
            'borrower_type' => $detilBorrower->brw_type,
        ]);
    }

    public function updateProfile(Request $request)
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                BorrowerIndividu::NAMA => ['max:50'],
                // 'nik' => ['digits:16', Rule::unique('brw_user_detail', 'ktp')->ignore($borrower->brw_id, 'brw_id')],
                'brw_type' => [''],
                BorrowerIndividu::EMAIL => ['email'],
                BorrowerIndividu::USERNAME => [''],
                BorrowerIndividu::TEMPAT_LAHIR => ['max:100'],
                BorrowerIndividu::TANGGAL_LAHIR => ['date_format:d-m-Y'],
                BorrowerIndividu::AGAMA => [Rule::exists('m_agama', 'id_agama')],
                BorrowerIndividu::STATUS_PERNIKAHAN => [Rule::exists('m_kawin', 'id_kawin')],
                BorrowerIndividu::PROVINSI => [Rule::exists('m_kode_pos', 'Provinsi')],
                BorrowerIndividu::KABUPATEN => [
                    function ($attribute, $value, $fail) {
                        if (! MasterKodePos::query()->where([
                            ['Jenis', explode(' ', trim($value))[0] ?? null],
                            [
                                'Kota',
                                substr(strstr($value, " "), 1),
                            ],
                        ])->exists()) {
                            $fail("Kabupaten tidak valid");
                        }
                    },
                ],
                BorrowerIndividu::KECAMATAN => [Rule::exists('m_kode_pos', 'kecamatan')],
                BorrowerIndividu::KELURAHAN => [Rule::exists('m_kode_pos', 'kelurahan')],
                BorrowerIndividu::KODE_POS => [Rule::exists('m_kode_pos', 'kode_pos')],
                BorrowerIndividu::ALAMAT => ['max:100'],
                // 'domisili_status_rumah' => [Rule::exists('m_negara', 'id_negara')],
                BorrowerIndividu::STATUS_RUMAH => [Rule::exists('m_kepemilikan_rumah', 'id_kepemilikan_rumah')],
                BorrowerIndividu::PENDIDIKAN => [Rule::exists('m_pendidikan', 'id_pendidikan')],
                'ktp' => ['mimes:jpeg,jpg,bmp,png'],
                'profile_ktp' => ['mimes:jpeg,jpg,bmp,png'],
                'profile' => ['mimes:jpeg,jpg,bmp,png'],
            ], [], BorrowerIndividu::getAttributeName())
                ->validate();

            // $detilBorrower->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
            //     'username' => 'username',
            // ]))->save();

            $detilBorrower->forceFill(
                BorrowerService::mapToDatabase($request->toArray(), [
                    'nama' => 'nama',
                    'nik' => 'ktp',
                    'tempat_lahir' => 'tempat_lahir',
                    'agama' => 'agama',
                    'status_pernikahan' => 'status_kawin',
                    'provinsi' => 'provinsi',
                    // 'kabupaten' => 'kota',
                    'kecamatan' => 'kecamatan',
                    'kelurahan' => 'kelurahan',
                    'kode_pos' => 'kode_pos',
                    'alamat' => 'alamat',
                    'status_rumah' => 'status_rumah',
                    'pendidikan' => 'pendidikan_terakhir',
                ])
            )->save();

            if ($request->filled('nik')) {
                $gender = ((int) substr($request->get('nik'), 6, 2)) <= 31
                    ? JenisKelamin::LAKILAKI
                    : JenisKelamin::PEREMPUAN;
            }

            if ($request->hasFile('ktp')) {
                $detilBorrower->forceFill([
                    'brw_pic_ktp' =>
                        BorrowerFileService::uploadFile($request->file('ktp'), 'brw_pic_ktp', $detilBorrower->brw_id),
                ])->save();
                $messages['ktp'] = 'success';
            }

            if ($request->hasFile('profile_ktp')) {
                $detilBorrower->forceFill([
                    'brw_pic_user_ktp' =>
                        BorrowerFileService::uploadFile(
                            $request->file('profile_ktp'),
                            'brw_pic_user_ktp',
                            $detilBorrower->brw_id
                        ),
                ])->save();
                $messages['profile_ktp'] = 'success';
            }

            if ($request->hasFile('profile')) {
                $detilBorrower->forceFill([
                    'brw_pic' =>
                        BorrowerFileService::uploadFile($request->file('profile'), 'brw_pic', $detilBorrower->brw_id),
                ])->save();
                $messages['profile'] = 'success';
            }

            if ($request->hasFile('npwp')) {
                $detilBorrower->forceFill([
                    'brw_pic_npwp' =>
                        BorrowerFileService::uploadFile($request->file('npwp'), 'brw_pic_npwp', $detilBorrower->brw_id),
                ])->save();
                $messages['npwp'] = 'success';
            }

            $detilBorrower->forceFill([
                'kota' => $request->filled('kabupaten') ? substr(
                    strstr($request->get('kabupaten'), " "),
                    1
                ) : $detilBorrower->kota,
                'tgl_lahir' => $request->filled('tanggal_lahir') ? Carbon::parse(
                    $request->get('tanggal_lahir')
                ) : $detilBorrower->tgl_lahir,
                'brw_type' => BorrowerDetails::TYPE_INDIVIDU,
                'jns_kelamin' => $gender ?? $detilBorrower->jns_kelamin,
            ])->save();

            $borrower = BorrowerService::getBorrower();

            DB::commit();

            return response()->json(['success' => 'Data diri berhasil diperbaharui', 'files' => $messages ?? []]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'error' => config('app.env') !== 'production' ? $exception->getMessage(
                    ) : 'Terjadi kesalahan, silahkan coba lagi',
                ]
            );
        }
    }

    public function getEducation()
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        return response()->json([
            'pendidikan' => $detilBorrower->pendidikan_terakhir,
            'pekerjaan' => $detilBorrower->pekerjaan,
            'bidang_pekerjaan' => $detilBorrower->bidang_pekerjaan,
            'bidang_online' => $detilBorrower->bidang_online,
            'nama_perusahaan' => $detilBorrowerPenghasilan->nama_perusahaan,
            'no_tlp_usaha' => $detilBorrowerPenghasilan->no_telp,
            // 'lama_kerja' => $detilBorrower->pengalaman_pekerjaan,
            'penghasilan_perbulan' => $detilBorrowerPenghasilan->pendapatan_borrower,
            'masa_kerja_tahun' => $detilBorrowerPenghasilan->masa_kerja_tahun,
            'masa_kerja_bulan' => $detilBorrowerPenghasilan->masa_kerja_bulan,
            'lama_usaha' => $detilBorrowerPenghasilan->usia_perusahaan,
            // 'estimasi_penghasilan' => $detilBorrower->pendapatan,
            // 'sumber_penghasilan' => $detilBorrower->sumber_dana,
            'npwp' => $detilBorrower->npwp,
        ]);
    }

    public function updateEducation(Request $request)
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $detilBorrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                // 'lama_kerja' => [Rule::exists('m_pengalaman_kerja', 'id_pengalaman_kerja')],
                // 'estimasi_penghasilan' => [Rule::exists('m_pendapatan', 'id_pendapatan')],
                // 'sumber_penghasilan' => ['max:191'],
                BorrowerIndividu::PENDIDIKAN => [Rule::exists('m_pendidikan', 'id_pendidikan')],
                BorrowerIndividu::PEKERJAAN => [Rule::exists('m_pekerjaan', 'id_pekerjaan')],
                BorrowerIndividu::BIDANG_PEKERJAAN => [Rule::exists('m_bidang_pekerjaan', 'id_bidang_pekerjaan')],
                BorrowerIndividu::BIDANG_ONLINE => [Rule::exists('m_online', 'id_online')],
                BorrowerIndividu::PENGHASILAN_PERBULAN => ['numeric'],
                BorrowerIndividu::NPWP => [
                    'numeric',
                    'digits_between:15,16',
                    Rule::unique('brw_user_detail', 'npwp')->ignore($borrower->brw_id, 'brw_id'),
                ],
                BorrowerIndividu::LAMA_USAHA => ['numeric', 'digits_between:0,5'],
                BorrowerIndividu::MASA_KERJA_TAHUN => ['numeric', 'digits_between:0,5'],
                BorrowerIndividu::MASA_KERJA_BULAN => ['numeric', 'digits_between:0,5', 'between:0,11'],
                BorrowerIndividu::NAMA_PERUSAHAAN => ['max:50'],
                BorrowerIndividu::NO_TLP_USAHA => ['max:15'],
            ], [], BorrowerIndividu::getAttributeName())
                ->validate();

            $detilBorrower->forceFill(
                BorrowerService::mapToDatabase($request->toArray(), [
                    BorrowerIndividu::PENDIDIKAN => 'pendidikan_terakhir',
                    BorrowerIndividu::PEKERJAAN => 'pekerjaan',
                    BorrowerIndividu::BIDANG_PEKERJAAN => 'bidang_pekerjaan',
                    BorrowerIndividu::BIDANG_ONLINE => 'bidang_online',
                    BorrowerIndividu::NPWP => 'npwp',
                    // 'lama_kerja' => 'pengalaman_pekerjaan',
                    // 'estimasi_penghasilan' => 'pendapatan',
                ])
            )->save();

            $detilBorrowerPenghasilan->forceFill(
                BorrowerService::mapToDatabase($request->toArray(), [
                    BorrowerIndividu::PENGHASILAN_PERBULAN => 'pendapatan_borrower',
                    BorrowerIndividu::MASA_KERJA_TAHUN => 'masa_kerja_tahun',
                    BorrowerIndividu::MASA_KERJA_BULAN => 'masa_kerja_bulan',
                    BorrowerIndividu::LAMA_USAHA => 'usia_perusahaan',
                    BorrowerIndividu::NAMA_PERUSAHAAN => 'nama_perusahaan',
                    BorrowerIndividu::NO_TLP_USAHA => 'no_telp',
                ])
            )->save();

            $borrower = BorrowerService::getBorrower();

            DB::commit();

            return response()->json(['success' => 'Data pendidikan/pekerjaan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json([
                'error' => config('app.env') !== 'production'
                    ? $exception->getMessage()
                    : 'Terjadi kesalahan, silahkan coba lagi',
            ]);
        }
    }

    public function changePassword(Request $request)
    {
        $old = $request->get('old_password');
        if (! (Hash::check($old, $request->user()->password))) {
            return response()->json(["error" => "Kata sandi lama anda salah"]);
        }

        if ($request->get('old_password') === $request->get('new_password')) {
            return response()->json(["error" => "Kata sandi baru harus berbeda dengan kata sandi lama"]);
        }

        if (strlen($request->get('new_password')) < 6) {
            return response()->json(["error" => "Panjang kata sandi harus lebih dari 6 karakter"]);
        }

        //Change Password
        $user = $request->user();
        $user->password = Hash::make($request->get('new_password'));
        $user->save();

        return response()->json(["success" => "Perubahan kata sandi berhasil "]);
    }

    public function changePasswordExpired(Request $request)
    {
        $brw_id = Auth::guard('borrower-api-mobile')->user()->id;
        $username = Auth::guard('borrower-api-mobile')->user()->username;
        $status = Auth::guard('borrower-api-mobile')->user()->status;

        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);

        if (! $detilBorrower) {
            $status = 'notfilled';
        } else {
            $status = 'active';
        }

        $borrower->password = Hash::make($request->get('new_password'));
        $borrower->password_updated_at = Carbon::now();
        $borrower->status = $status;
        $borrower->save();

        $credentials = (['username' => $username, 'password' => $request->get('new_password')]);

        if (! $token = Auth::guard('borrower-api-mobile')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized']);
        }

        //AMBIL STATUS LG TERBARU KARENA UDAH UPDATE ACTIVE
        $status = Auth::guard('borrower-api-mobile')->user()->status;

        return $this->respondWithTokenExpiredPassword($token, $status, $username);
    }

    public function verifyData(Request $request)
    {
        $borrower = BorrowerService::getBorrower();
        if ($request->has('email') && $request->email === $borrower->email) {
            return response()->json([
                'success' => 'Email Terverifikasi',
            ]);
        }

        if ($request->has('phone') && $request->phone === $borrower->detail->no_tlp) {
            return response()->json([
                'success' => 'No HP Terverifikasi',
            ]);
        }

        return response()->json([
            'errors' => 'Data tidak sesuai',
        ]);
    }

    public function verifyPassword(Request $request)
    {
        $user = $request->user();

        Validator::make($request->all(), ['password' => ['required']])->validate();

        if (Hash::check($request->get('password'), $user->password)) {
            return response()->json(['success' => 'Password terverifikasi']);
        }

        return response()->json(['error' => 'Password Salah']);
    }

    protected function respondWithTokenExpiredPassword($token, $status, $username)
    {
        return response()->json([
            'expired' => 'no',
            'username' => $username,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL(),
            'status' => $status,
        ]);
    }

}
