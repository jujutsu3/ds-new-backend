<?php
namespace App\Http\Controllers\Mobile\Kpr;

use App\Borrower;
use App\LogSuspend;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use JWTFactory;
use Hash;
use Carbon\Carbon;
use Mail;
use Image;
use GuzzleHttp\Client;
use DB;
use Exception;
use Storage;

use App\Jobs\ProcessEmail;
use App\Mail\EmailAktifasiKpr;

use App\LoginBorrower;
use App\Token;

class NewKprAuthController extends Controller
{
    use ThrottlesLogins;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['except' => ['login', 'resendEmail', 'newnewCheckToken',  'register_new_new_new','hasTooManyLoginAttempts','sendLockoutResponse']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $temp_username=$request->username;
        
        if(is_numeric($temp_username)){
            if(substr($temp_username,0,2) == "62"){
                $no_hp = substr($temp_username, 2);
            }elseif(substr($temp_username,0,1) == "0"){
                $no_hp = substr($temp_username, 1);
            }
            $data = DB::table('brw_user_detail')->join('brw_user', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')->select('username')->where('brw_user_detail.no_tlp', 'like', '%'.$no_hp.'%')->first();
            if($data){
                $username = $data->username;
            }else{
                $username = $temp_username;
            }
        }elseif (strpos($temp_username, '@')) {
            $data = DB::table('brw_user')->select('username')->where('brw_user.email', '=', $temp_username)->first();
            if($data){
                $username = $data->username;
            }else{
                $username = $temp_username;
            }
        }
        else{
            $username = $temp_username;
        }
        $credentials =  (['username' => $username,'password' => $request->password]);

        if (! $token = Auth::guard('borrower-api-mobile')->attempt($credentials)) {
            if ($this->hasTooManyLoginAttempts($request)) {
                return $this->sendLockoutResponse($request);
            }

            if (Borrower::query()->where('username', $username)->where('status', Borrower::STATUS_ACTIVE)->exists()) {
                $this->incrementLoginAttempts($request);
            }

            return response()->json(['error' => 'Unauthorized']);
        }
        
        $password_updated_at = Auth::guard('borrower-api-mobile')->user()->password_updated_at;
        $password_expiry_days = Auth::guard('borrower-api-mobile')->user()->password_expiry_days; 

        if ($password_updated_at != null) {

            $password_expiry_at = Carbon::parse($password_updated_at)->addDays($password_expiry_days);

            if(Auth::guard('borrower-api-mobile')->user()->status == 'Not Active'){
                $status = Auth::guard('borrower-api-mobile')->user()->status;

                if ($status === Borrower::STATUS_EXPIRED) {
                    return response()->json(['error' => 'Kata sandi Anda harus segera diganti. / Your password has been expired. ', 'status' => 'expired']);
                }

                if ($status === Borrower::STATUS_SUSPENDED) {
                    return response()->json(['error' => 'Anda salah memasukkan kata sandi tiga kali. Aakun anda terblokir. Silahkan hubungi Customer Service / You have inputted invalid password three times. Your account is suspended. Please call Customser Service.', 'status' => 'suspend' ]);
                }

                return $this->respondWithToken($token, $status);
            }else if($password_expiry_at->lessThan(Carbon::now())){

                $update_status_expired = LoginBorrower::where('brw_id', Auth::guard('borrower-api-mobile')->user()->brw_id)->update(['status' => 'expired']);

                return response()->json([
                    'id_user' => Auth::guard('borrower-api-mobile')->user()->brw_id,
                    'expired' => 'yes',
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => Auth::guard('borrower-api-mobile')->factory()->getTTL(),
                    'status' => 'expired',
                    'status_reg' => Auth::guard('borrower-api-mobile')->user()->status_reg
                ]);
            } else {
                $status = Auth::guard('borrower-api-mobile')->user()->status;
            
                return $this->respondWithToken($token, $status);
            }
        } else {
            $status = Auth::guard('borrower-api-mobile')->user()->status;
            
            return $this->respondWithToken($token, $status);
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(Auth::guard('borrower-api-mobile')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        // $token = JWTAuth::getToken();
        
        // $user_token = Token::where('investor_id', Auth::guard('borrower-api-mobile')->user()->brw_id)->where('login_token', $token)->first();
        // $user_token->delete();

        Auth::guard('borrower-api-mobile')->logout();

        return response()->json(['message' => 'Anda berhasil keluar']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::guard('borrower-api-mobile')->refresh(), Auth::guard('borrower-api-mobile')->user()->status);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */

    protected function respondWithToken($token, $status)
    {
        return response()->json([
            'id_user' => Auth::guard('borrower-api-mobile')->user()->brw_id,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('borrower-api-mobile')->factory()->getTTL(),
            'status' => $status,
            'expired' => 'no',
            'status_reg' => Auth::guard('borrower-api-mobile')->user()->status_reg
        ]);
    }

    public function newnewCheckToken (Request $request) {

        $token = $request->token;
        
        try {
            // attempt to verify the credentials
            $token = JWTAuth::getToken();
            $apy = JWTAuth::getPayload($token)->toArray();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
    
            return response()->json(['error'=>'Token Expired, Silahkan login kembali'], 500);
    
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
    
            return response()->json(['error'=>'Token Invalid, Silahkan login dengan akun Danasyariah'], 500);
    
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
    
            return response()->json(['error_token_absent'=>'token_absent', 'msg' => $e->getMessage()], 500);
    
        }

        $status = Auth::guard('borrower-api-mobile')->user()->status;

        return $this->respondWithToken($token, $status);
    }

    public function register_new_new_new(Request $request) {

        if (LoginBorrower::where('username', $request['username'])->first()!== null) {
            return [
                'error' => 'Nama Akun Sudah Digunakan, Silahkan gunakan Nama Akun yang lain'
            ];
        }
        if (LoginBorrower::where('email', $request['email'])->first()!== null) {
            return [
                'error' => 'Email Sudah Digunakan,  Silahkan gunakan Email yang lain'
            ];
        }

        if (!filter_var($request['email'], FILTER_VALIDATE_EMAIL)) {

            return [
                'error' => 'Format email tidak valid'
            ];
        }

        $ar=preg_split("/@/",$request['email']);
        if ( !checkdnsrr($ar[1], 'ANY') ) {

            return [
                'error' => 'Format email tidak valid'
            ];
        }

        $user = LoginBorrower::create([
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'email_verif' => str_random(30),
            'status'=>'Not Active',
            'ref_number' => $request['referal_code'],
            'password_expiry_days' => 180,
        ]);

        $credentials = request(['username', 'password']);

        if (! $token = Auth::guard('borrower-api-mobile')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized']);
        }
    
        $email = new EmailAktifasiKpr($user);
        Mail::to($user->email)->send($email);

        $brw_id = LoginBorrower::select('brw_id')->where('username', $request['username'])->first();

        // if (Mail::failures()) {
        //     $insert_log_db = DB::table('log_db_app')->insert([
        //         'file_name' => 'KprAuthController.php',
        //         'line' => 735,
        //         'description' => 'Gagal mengirim email pendaftaran user KPR',
        //     ]);
        //     return response()->json(['email_failed' => 'Gagal Kirim Email', 'brw_id' => $brw_id]);
        // }

        return response()->json(['status' => 'Not Active', 'brw_id' => $brw_id->brw_id]);
    }

    public function resendEmail(Request $request){
        $email = $request->email ? $request->email : 'null';
        if($request->email){
            $user = LoginBorrower::where('email', $email)->first();

            if($user->status !== 'Not Active'){
                return response()->json(['status' => 'Sudah_Terdaftar']);
            }else{
                $email = new EmailAktifasiKpr($user);
                $send_mail = Mail::to($user->email)->send($email);
                // $send_mail = dispatch(new ProcessEmail($user, 'regis'));
            }
        }else{
            $user = LoginBorrower::where('brw_id', Auth::guard('borrower-api-mobile')->user()->brw_id)->first();
            if($user->status !== 'Not Active'){
                return response()->json(['status' => 'Sudah_Terdaftar']);
            }else{
                $email = new EmailAktifasiKpr($user);
                $send_mail = Mail::to($user->email)->send($email);
                // $send_mail = dispatch(new ProcessEmail($user, 'regis'));
            }
        }
        if(!$send_mail){
            return response()->json(['status' => 'Success']);
        }else{
            return response()->json(['status' => 'Failed']);
        }
    }

    public function username()
    {
        return 'username';
    }

    public function maxAttempts()
    {
        return 3;
    }
	
	public static function hasTooManyLoginAttempts(Request $request)
    {
        // dd($request);
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts()
        );
    }

    public function sendLockoutResponse(Request $request)
    {
        $borrower = Borrower::query()->where('username', $request->username)->orWhere('email', $request->username)->first();

        if ($borrower === null) {
            return response()->json(['error' => 'Unauthorized']);
        }

        $borrower->forceFill([
            'status' => Borrower::STATUS_SUSPENDED,
        ])
            ->save();
        $Log = new LogSuspend;
        $Log->keterangan = 'Salah masukkan password lebih dari 3x';
        $Log->suspended_by = 'Sistem';
        $Log->save();

        return response()->json(['error' => 'Anda salah memasukkan kata sandi tiga kali. Akun anda terblokir. Silahkan hubungi Customer Service / You have inputted invalid password three times. Your account is suspended. Please call Customser Service.']);
    }
}

