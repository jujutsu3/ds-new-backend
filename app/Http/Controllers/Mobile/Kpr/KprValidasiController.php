<?php
namespace App\Http\Controllers\Mobile\Kpr;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;
use GuzzleHttp\Client;
use DB;
use Exception;
use Storage;


use App\LoginBorrower;
use App\BorrowerDetails;

class KprValidasiController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['except' => ['validateOTP', 'checkPhoneNumber']]);
    }

    public function validateOTP(Request $request){

        // $id = 52215;
        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $input_otp = $request->no_otp;

        $query = LoginBorrower::where('brw_id', $brw_id)->first();
        $otp = $query->otp;

        if($input_otp == $otp){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'failed']);
        }
    }  

    public function sendOtp(Request $request){

        $id=Auth::guard('borrower-api-mobile')->user()->brw_id;
        $to = $request->no_telp;

        // $phone_get = BorrowerDetails::where('brw_id',$id)->first(['no_tlp']);
        // $to =  $phone_get;
        $otp = rand(100000, 999999);

        $text =  "<#> DANASYARIAH-JANGAN MEMBERITAHU KODE INI KE SIAPAPUN termasuk pihak DANASYARIAH. Kode OTP : $otp Silahkan masukan kode ini untuk melanjutkan proses pendaftaran anda.\nX96Ckidjiws"; 

        //send to db
        $detil = LoginBorrower::where('brw_id', $id)->update(['otp' => $otp]);

        $pecah              = explode(",",$to);
        $jumlah             = count($pecah);
        $from               = "DANASYARIAH"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
        $username           = "danasyariahpremium"; //your smsviro username
        $password           = "Dsi701@2019"; //your smsviro password
        $postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS
        
        for($i=0; $i<$jumlah; $i++){
            if(substr($pecah[$i],0,2) == "62" || substr($pecah[$i],0,3) == "+62"){
                $pecah = $pecah;
            }elseif(substr($pecah[$i],0,1) == "0"){
                $pecah[$i][0] = "X";
                $pecah = str_replace("X", "62", $pecah);
            }else{
                echo "Invalid mobile number format";
            }
            $destination = array("to" => $pecah[$i]);
            $message     = array("from" => $from,
                                 "destinations" => $destination,
                                 "text" => $text,
                                 "smsCount" => 20);
            $postData           = array("messages" => array($message));
            $postDataJson       = json_encode($postData);
            $ch                 = curl_init();
            $header             = array("Content-Type:application/json", "Accept:application/json");
            
            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $responseBody = json_decode($response);
            curl_close($ch);
        }   

        if($detil){
            $data = ['success' => true, 'message' => 'Silahkan masukan kode ini untuk melanjutkan proses penarikan tunai.'];
            return response()->json($data);
        }else{
          $data = ['failed' => false, 'message' => 'Data Telepon tidak benar.'];
          return response()->json($data);
        }
    }

    public function checkPhoneNumber(Request $request){

        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;
        $no_hp_temp = BorrowerDetails::select('no_tlp')->where('brw_id', '=', $brw_id)->first();

        if(isset($no_hp_temp)){
            if($no_hp_temp->no_tlp == '62'.$request->no_hp){
                return response()->json(['success'=> 'Nomer Telpon Sama Seperti Sebelumnya']);
            }
        }
        
        if(empty($request->no_hp)){
            return response()->json(['success'=> 'Nomer Telpon Belum Pernah Terdaftar']);
        }
        else
            if (BorrowerDetails::whereIn('no_tlp', ['62'.$request->no_hp, '0'.$request->no_hp])->first()) {
                return response()->json(['error'=> 'Nomer Telpon Sudah Pernah Terdaftar']);
            }
            else
            return response()->json(['success'=> 'Nomer Telpon Belum Pernah Terdaftar']);
    }

    public function check_KTP(Request $request){
        if (BorrowerDetails::where('ktp', $request->no_ktp)->first()) {
            return response()->json(['error'=> 'Nomor KTP Sudah Pernah Terdaftar']);
        }else return response()->json(['success'=> 'Nomer KTP Belum Pernah Terdaftar']);
    }

    public function checkNpwp(Request $request){
        if (BorrowerDetails::where('npwp', $request->no_npwp)->first()) {
            return response()->json(['error'=> 'Nomor NPWP Sudah Pernah Terdaftar']);
        }else return response()->json(['success'=> 'Nomer NPWP Belum Pernah Terdaftar']);
    }

    public function cekBiodataPribadi(Request $request){

        $npwp = $request->npwp;
        $ktp = $request->ktp;
        $no_hp = $request->no_hp;

        $cek_validasi = DB::select("SELECT val_register_user(
            '1',
            '1',
            '".$npwp."',
            '2',
            '".$ktp."',
            '3',
            '".$no_hp."',
            'KprAuthController.php',
            1845) as result;"
        );
        return response()->json(['error' => $cek_validasi[0]->result]);

        if($cek_validasi[0]->result !=='1'){
            return response()->json(['error' => $cek_validasi[0]->result]);
        }else{
            return response()->json(['success' => 'Berhasil Checkout Pendanaan', 'jumlah_investasi'=>$total_investation]);
        }
    }

}

