<?php
namespace App\Http\Controllers\Mobile\Kpr;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Image;
use GuzzleHttp\Client;
use DB;
use Exception;
use Storage;
use Illuminate\Support\Facades\File; 

use App\BorrowerDetails;
use App\LoginBorrower;
use App\BorrowerDanaRumahLain;
use App\BorrowerDanaNonRumahLain;

class KprProcessDataController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['except' => []]);
    }

    public function uploadFoto(Request $request)
    {
        $jenis_foto_upload = $request->jenis_foto_upload;
        
        if($jenis_foto_upload == 'pic_user'){
            $path_jenis_foto_upload = 'pic_brw';
        }else if($jenis_foto_upload == 'pic_ktp_investor'){
            $path_jenis_foto_upload = 'pic_brw_ktp';
        }else if($jenis_foto_upload == 'pic_user_ktp_investor'){
            $path_jenis_foto_upload = 'pic_brw_dan_ktp';
        }else{
            $path_jenis_foto_upload = 'pic_brw_npwp';
        }
        
        // $path_jenis_foto_upload = $jenis_foto_upload == 'pic_user' ? 'pic_brw' :  $jenis_foto_upload == 'pic_ktp_investor' ? 'pic_brw_ktp' : $jenis_foto_upload == 'pic_user_ktp_investor' ? 'pic_brw_dan_ktp' : 'pic_brw_npwp';

        
        if ($request->hasFile($jenis_foto_upload)) {
            $file = $request->file($jenis_foto_upload);
            $resize = Image::make($file)->save();
            $filename = $path_jenis_foto_upload. '.' . $file->getClientOriginalExtension();
            //  save nama file berdasarkan tanggal upload+nama file
            $store_path = 'borrower/' . Auth::guard('borrower-api-mobile')->user()->brw_id;
            $path = $file->storeAs($store_path, $filename, 'private');
            //  save gambar yang di upload di private storage

            // Storage::disk('private')->delete('brw_user/'.Auth::guard('borrower-api-mobile')->user()->id.'/'.$filename);

            if(Storage::disk('private')->exists('borrower/'.Auth::guard('borrower-api-mobile')->user()->brw_id.'/'.$filename)){
                return response()->json([
                    'success' => 'Berhasil di upload'
                ]);
            }else{
                return response()->json([
                    'failed' => 'File gagal di upload'
                ]);
            }
        }
        else {
            return [
                'failed' => 'File Kosong'
            ];
        }
    }

    # ichal_sl, Mar 02, 2022
	# bikin versi baru dengan men take-out data2 yg tidak perlu
	public function saveKprAll (Request $request) {

        $id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        if (BorrowerDetails::where('no_tlp', $request->no_hp)->first()) {
            return response()->json(['error'=> 'Nomer Telpon Sudah Pernah Terdaftar']);
        }
        if (BorrowerDetails::where('id', $id)->first()) {
            return response()->json(['error'=> 'Data ini sudah terdaftar']);
        }

        $data_pribadi = json_decode($request->data_pribadi);
        $data_alamat = json_decode($request->data_alamat);
        $data_rekening = json_decode($request->data_rekening);
        $data_pekerjaan = json_decode($request->data_pekerjaan);
        $data_pasangan = json_decode($request->data_pasangan);
        $data_pekerjaan_pasangan = json_decode($request->data_pekerjaan_pasangan);
        $data_ajukan_pendanaan = json_decode($request->data_ajukan_pendanaan);
        $data_foto = json_decode($request->data_foto);

        $bidang_pekerjaan_pasangan = $data_pekerjaan_pasangan ? $data_pekerjaan_pasangan->bidang_pekerjaan : '';
        $bidang_online_pasangan = $data_pekerjaan_pasangan ? $data_pekerjaan_pasangan->bidang_online : '';
        $pekerjaan_pasangan = $data_pekerjaan_pasangan ? $data_pekerjaan_pasangan->pekerjaan : '';

        $jenis_kelamin = $data_pribadi->jenis_kelamin == '0' ? '1' : '2'; 
        $no_hp = '62'.$data_pribadi->no_hp; 

        $format_foto_diri = $data_foto->uri_pic_user_investor == '' ? $data_foto->uri_pic_user_investor : 'JPEG';
        $format_foto_ktp = $data_foto->uri_pic_ktp_investor == '' ? $data_foto->uri_pic_ktp_investor : 'JPEG';
        $format_foto_diri_dan_ktp = $data_foto->uri_pic_user_ktp_investor == '' ? $data_foto->uri_pic_user_ktp_investor : 'JPEG';
        $format_foto_npwp = $data_foto->uri_pic_npwp_investor == '' ? $data_foto->uri_pic_npwp_investor : 'JPEG';

        $path_foto_diri = "/borrower/$id/pic_brw.JPEG";
        $path_foto_ktp = "/borrower/$id/pic_brw_ktp.JPEG";
        $path_foto_diri_dan_ktp = "/borrower/$id/pic_brw_dan_ktp.JPEG";
        $path_foto_npwp = "/borrower/$id/pic_brw_npwp.JPEG";

        // $path_foto_diri ='';
        // $path_foto_ktp ='';
        // $path_foto_diri_dan_ktp ='';
        // $path_foto_npwp ='';

        // DB::beginTransaction();

        //!INSERT INTO BRW_USER_DETAIL
        $insertDetailBorrower = DB::select("SELECT put_into_brw_user_detail(
            '".$id."',
            '".$data_pribadi->nama."',
            '', /*nm_bdn_hukum*/ 
            '', /*nib*/ 
            '', /*npwp_perusahaan*/ 
            '', /*akta*/
            '', /*tgl berdiri*/
            '', /*tlp_perusahaan*/
            '', /*foto_npwp_perusahaan*/
            '', /*bidang usaha*/
            '', /*omset*/
            '', /*tot aset*/
            '', /*jabatan*/
            '1', /*brw_tipe*/
            '".$data_pribadi->nama_ibu_kandung."', /*nama ibu*/
            '".$data_pribadi->no_ktp."', /*ktp*/
            '".$data_pribadi->no_npwp."', /*npwp*/
            '".$data_pribadi->tanggal_lahir."',/*tgl lahir*/
            '".$no_hp."', /*no_tlp*/
            '".$jenis_kelamin."', /*jenis_kelamin*/
            '".$data_pribadi->status_pernikahan."', /*status_kawin*/
            '".$data_alamat->status_kepemilikan_rumah."', /*status rumah*/
            '".$data_alamat->alamat_sesuai_ktp."', /*alamat*/
            '".$data_alamat->alamat_sesuai_ktp_domisili."',  /*domisili*/
            '".$data_alamat->provinsi_domisili."', /*domisili prov*/
            '".$data_alamat->kota_domisili."', /*domisili kota*/
            '".$data_alamat->kecamatan_domisili."', /*domisili kec*/
            '".$data_alamat->kelurahan_domisili."', /*domisili kel*/
            '".$data_alamat->kode_pos_domisili."', /*domisili kd pos*/
            '".$data_alamat->status_kepemilikan_rumah_domisili."',  /*status rumah domisili*/
            '".$data_alamat->provinsi."', /*provinsi*/
            '".$data_alamat->kota."', /*kota*/
            '".$data_alamat->kecamatan."', /*kec*/
            '".$data_alamat->kelurahan."', /*kel*/
            '".$data_alamat->kode_pos."', /*kd pos*/
            '".$data_pribadi->agama."', /*agama*/
            '".$data_pribadi->tempat_lahir."', /*tempat lahir*/
            '".$data_pribadi->pendidikan."', /*pendidikan terakhi*/
            '".$data_pekerjaan->pekerjaan."', /*pekerjaan*/
            '', /*bidang perusahaan*/
            '".$data_pekerjaan->bidang_pekerjaan."', /*bidang pekerjaan*/
            '".$data_pekerjaan->bidang_online."', /*bidang online*/
            '', /*pengalaman pekerjaan*/
            '', /*pendapatan*/
            '', /*total aset*/
            '', /*warganegara*/
            '', /*brw online*/
            '".$path_foto_diri."',
            '".$path_foto_ktp."',
            '".$path_foto_diri_dan_ktp."',
            '".$path_foto_npwp."',
            '".$data_pribadi->no_kk."', /*no kk*/
            '', /*lama menempati*/
            '', /*tlp rumah*/
            '', /*kartu kredit*/
            '', /*usia pensiun*/
            '', /*alamat penagihan rumah*/
            '', /*alamat penagihan kantor*/
            '', /*npwp pasangan*/
            '', /*nomor kk pasangan*/
            '', /*tmpt lahir pasangan*/
            '', /*tgl lahir pasangan*/
            '', /*pendidikan pasangan*/
            '', /*sd pasangan*/
            '', /*smp pasangan*/
            '', /*sma pasangan*/
            '', /*pt pasangan*/
            '".$pekerjaan_pasangan."', /*pekerjaan pasangan*/
            '".$bidang_pekerjaan_pasangan."', /*bd pekerjaan pasangan*/
            '".$bidang_online_pasangan."', /*bd pekerjaanO pasangan*/
            '',  /*pengalaman pasangan*/
            '', /*pendapatan pasangan*/
            '', /*kartu kredit*/
            '', /*jmlasettdkbergerak_pasangan*/
            '', /*mlasetbergerak_pasangan*/
            '', /*agama pasangan*/
            '', /*pic kk*/
            '', /*pic ktp pasangan*/
            '', /*pic surat nikah*/
            '', /*pic spt*/
            '', /*pic rek koran*/
            '', /*pic slip gaji*/
            '', /*pic lap keuangan*/
            '".date('Y-m-d H:i:s')."', /*createtd at*/
            '".date('Y-m-d H:i:s')."',  /*update at*/
            'KprAuthController',
            '523'
            ) as response;"
        );

        //!JIKA SUKSES INSERT BRW_USER_DETAIL MAKA INSERT INTO BRW_REKENING
        if($insertDetailBorrower[0]->response == 1){
            $insertRekening = DB::select("select put_into_brw_rekening(
                '".$id."', 
                '',  
                '".$data_rekening->nomor_rekening."', 
                '".$data_rekening->nama_pemilik_rekening."', 
                '".$data_rekening->nama_bank."',
			    '2000000000',
                '0',
                '2000000000',
                '".date('Y-m-d H:i:s')."',
                '".date('Y-m-d H:i:s')."',
                'KprAuthController', 
                '641', 
                '".$data_rekening->kantor_cabang_pembuka."'
				) as response;"
			);

            // return $insertRekening;

            //!JIKA SUKSES INSERT BRW_REKENING MAKA INSERT INTO BRW_USER_DETAIL_PENGHASILAN
            if($insertRekening[0]->response == 1){

                //VARIABEL KONDISI PEKERJAAN
                $skema_pembiayaan = $data_pekerjaan->skema_pembiayaan == 'single_income' ? 1 : 2;
                $sumber_pengembalian_dana = $data_pekerjaan->sumber_pengembalian_dana == 'fixed_income' ? 1 : 2;

                //JIKA PEKERJAANNYA PELAJAR, TIDAK BEKERJA, DLL
                if($data_pekerjaan->pekerjaan == 6 ||  $data_pekerjaan->pekerjaan == 7 || $data_pekerjaan->pekerjaan == 8){
                    $insertPenghasilan = DB::select("SELECT put_into_brw_user_detail_penghasilan(
                        '".$id."',
                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                        '".$skema_pembiayaan."', /*skema_pembiayaan*/
                        '', /*nama_perusahaan*/
                        '', /*alamat_perusahaan*/
                        '', /*rt*/
                        '', /*rw*/
                        '', /*provinsi*/
                        '', /*kab_kota*/
                        '', /*kecamatan*/
                        '', /*kelurahan*/
                        '', /*kode_pos*/
                        '', /*no_telp*/
                        '', /*no_hp*/
                        '', /*surat_ijin*/
                        '', /*no_surat_ijin*/
                        '', /*bentuk_badan_usaha*/
                        '', /*status_pekerjaan*/
                        '', /*usia_perusahaan*/
                        '', /*usia_tempat_usaha*/
                        '', /*departemen*/
                        '', /*jabatan*/
                        '', /*masa_kerja_tahun*/
                        '', /*masa_kerja_bulan*/
                        '', /*nip_nrp_nik*/
                        '', /*nama_hrd*/
                        '', /*no_fixed_line_hrd*/
                        '', /*pengalaman_kerja_tahun*/
                        '', /*pengalaman_kerja_bulan*/
                        '".$data_pekerjaan->penghasilan_perbulan."', /*pendapatan_borrower*/
                        '".$data_pekerjaan->biaya_hidup."', /*biaya_hidup*/
                        '".$data_pekerjaan->detil_pekerjaan_lain."', /*detail_penghasilan_lain_lain*/
                        '', /*total_penghasilan_lain_lain*/
                        '', /*nilai_spt*/
                        'KprAuthController',
                        '654',
                        '".date('Y-m-d H:i:s')."', /*createtd at*/
                        '".date('Y-m-d H:i:s')."'
                        ) as response;"
                    );

                //!JIKA PEKERJAANNYA FIXED INCOME
                }elseif($sumber_pengembalian_dana == 1){

                    $temp_surat_ijin = 0;
                    $temp_no_telpon_perusahaan = '62'.$data_pekerjaan->no_telpon_perusahaan_fixed_income;
                    $temp_nomor_hrd_fixed_income = '62'.$data_pekerjaan->nomor_hrd_fixed_income;
                    
                    $insertPenghasilan = DB::select("SELECT put_into_brw_user_detail_penghasilan(
                        '".$id."',
                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                        '".$skema_pembiayaan."', /*skema_pembiayaan*/
                        '".$data_pekerjaan->nama_perusahaan_fixed_income."', /*nama_perusahaan*/
                        '".$data_pekerjaan->alamat_perusahaan_fixed_income."', /*alamat_perusahaan*/
                        '".$data_pekerjaan->rt."', /*rt*/
                        '".$data_pekerjaan->rw."', /*rw*/
                        '".$data_pekerjaan->provinsi."', /*provinsi*/
                        '".$data_pekerjaan->kota."', /*kab_kota*/
                        '".$data_pekerjaan->kecamatan."', /*kecamatan*/
                        '".$data_pekerjaan->kelurahan."', /*kelurahan*/
                        '".$data_pekerjaan->kode_pos."', /*kode_pos*/
                        '".$temp_no_telpon_perusahaan."', /*no_telp*/
                        '', /*no_hp*/
                        '".$temp_surat_ijin."', /*surat_ijin*/
                        '', /*no_surat_ijin*/
                        '".$data_pekerjaan->bentuk_badan_usaha_fixed_income."', /*bentuk_badan_usaha*/
                        '".$data_pekerjaan->status_pekerjaan_fixed_income."', /*status_pekerjaan*/
                        '".$data_pekerjaan->lama_bekerja_fixed_income."', /*usia_perusahaan*/
                        '', /*usia_tempat_usaha*/
                        '".$data_pekerjaan->departemen_fixed_income."', /*departemen*/
                        '".$data_pekerjaan->jabatan_fixed_income."', /*jabatan*/
                        '".$data_pekerjaan->pengalaman_kerja_tahun_fixed_income."', /*masa_kerja_tahun*/
                        '".$data_pekerjaan->pengalaman_kerja_bulan_fixed_income."', /*masa_kerja_bulan*/
                        '".$data_pekerjaan->nip_fixed_income."', /*nip_nrp_nik*/
                        '".$data_pekerjaan->nama_hrd_fixed_income."', /*nama_hrd*/
                        '".$temp_nomor_hrd_fixed_income."', /*no_fixed_line_hrd*/
                        '".$data_pekerjaan->pengalaman_kerja_tempat_lain_tahun_fixed_income."', /*pengalaman_kerja_tahun*/
                        '".$data_pekerjaan->pengalaman_kerja_tempat_lain_bulan_fixed_income."', /*pengalaman_kerja_bulan*/
                        '".$data_pekerjaan->penghasilan_perbulan."', /*pendapatan_borrower*/
                        '".$data_pekerjaan->biaya_hidup."', /*biaya_hidup*/
                        '".$data_pekerjaan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                        '".$data_pekerjaan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                        '".$data_pekerjaan->nilai_spt."', /*nilai_spt*/
                        'KprAuthController',
                        '696',
                        '".date('Y-m-d H:i:s')."', /*createtd at*/
                        '".date('Y-m-d H:i:s')."'
                        ) as response;"
                    );

                //!JIKA PEKERJAANNYA NOT FIXED INCOME
                }else{

                    $temp_bentuk_badan_usaha = 0;
                    $temp_status_pekerjaan = 0;

                    $temp_no_telpon_usaha_non_fixed_income = '62'.$data_pekerjaan->no_telpon_usaha_non_fixed_income;
                    $temp_no_hp_non_fixed_income = '62'.$data_pekerjaan->no_hp_non_fixed_income;

                    $insertPenghasilan = DB::select("SELECT put_into_brw_user_detail_penghasilan(
                        '".$id."',
                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                        '".$skema_pembiayaan."', /*skema_pembiayaan*/
                        '".$data_pekerjaan->nama_usaha_non_fixed_income."', /*nama_perusahaan*/
                        '".$data_pekerjaan->alamat_usaha_non_fixed_income."', /*alamat_perusahaan*/
                        '".$data_pekerjaan->rt."', /*rt*/
                        '".$data_pekerjaan->rw."', /*rw*/
                        '".$data_pekerjaan->provinsi."', /*provinsi*/
                        '".$data_pekerjaan->kota."', /*kab_kota*/
                        '".$data_pekerjaan->kecamatan."', /*kecamatan*/
                        '".$data_pekerjaan->kelurahan."', /*kelurahan*/
                        '".$data_pekerjaan->kode_pos."', /*kode_pos*/
                        '".$temp_no_telpon_usaha_non_fixed_income."', /*no_telp*/
                        '".$temp_no_hp_non_fixed_income."', /*no_hp*/
                        '".$data_pekerjaan->surat_ijin_usaha_non_fixed_income."', /*surat_ijin*/
                        '".$data_pekerjaan->nomor_ijin_usaha_non_fixed_income."', /*no_surat_ijin*/
                        '".$temp_bentuk_badan_usaha."', /*bentuk_badan_usaha*/
                        '".$temp_status_pekerjaan."', /*status_pekerjaan*/
                        '".$data_pekerjaan->lama_usaha_non_fixed_income."', /*usia_perusahaan*/
                        '".$data_pekerjaan->lama_tempat_usaha_non_fixed_income."', /*usia_tempat_usaha*/
                        '', /*departemen*/
                        '', /*jabatan*/
                        '', /*masa_kerja_tahun*/
                        '', /*masa_kerja_bulan*/
                        '', /*nip_nrp_nik*/
                        '', /*nama_hrd*/
                        '', /*no_fixed_line_hrd*/
                        '', /*pengalaman_kerja_tahun*/
                        '', /*pengalaman_kerja_bulan*/
                        '".$data_pekerjaan->penghasilan_perbulan."', /*pendapatan_borrower*/
                        '".$data_pekerjaan->biaya_hidup."', /*biaya_hidup*/
                        '".$data_pekerjaan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                        '".$data_pekerjaan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                        '".$data_pekerjaan->nilai_spt."', /*nilai_spt*/
                        'KprAuthController',
                        '738',
                        '".date('Y-m-d H:i:s')."', /*createtd at*/
                        '".date('Y-m-d H:i:s')."'
                        ) as response;"
                    );
                }

                //!JIKA SUKSES INSERT BRW_USER_DETAIL_PENGHASILAN MAKA INSERT INTO BRW_PASANGAN
                if($insertPenghasilan[0]->response == 1){

                    if($data_pribadi->status_pernikahan == 1){
                        // $tanggal_lahir_pasangan = $data_pasangan->tgl_lahir_pasangan;
                        // $bulan_lahir_pasangan = $data_pasangan->bulan_lahir_pasangan;
                        // $tahun_lahir_pasangan = $data_pasangan->tahun_lahir_pasangan;

                        // $tgl_lengkap_pasangan = $tahun_lahir_pasangan.'-'.$bulan_lahir_pasangan.'-'.$tanggal_lahir_pasangan;

                        $tgl_lengkap_pasangan = $data_pasangan->tanggal_lahir_pasangan;
                        $jenis_kelamin_pasangan = $data_pasangan->jenis_kelamin_pasangan == '0' ? '1' : '2';
                        $no_hp_pasangan = '62'.$data_pasangan->no_hp_pasangan;

                        $insertPasangan = DB::select("SELECT put_into_brw_pasangan(
                                '".$id."', 
                                '".$data_pasangan->nama_pasangan."', 
                                '".$jenis_kelamin_pasangan."', 
                                '".$data_pasangan->no_ktp_pasangan."', 
                                '".$data_pasangan->tempat_lahir_pasangan."', 
                                '".$tgl_lengkap_pasangan."', 
                                '".$no_hp_pasangan."', 
                                '".$data_pasangan->agama_pasangan."',
                                '".$data_pasangan->pendidikan_pasangan."', 
                                '".$data_pasangan->no_npwp_pasangan."', 
                                '".$data_pasangan->alamat_pasangan."', 
                                '".$data_pasangan->provinsi_pasangan."' , 
                                '".$data_pasangan->kota_pasangan."', 
                                '".$data_pasangan->kecamatan_pasangan."', 
                                '".$data_pasangan->kelurahan_pasangan."', 
                                '".$data_pasangan->kode_pos_pasangan."',
                                '".date('Y-m-d H:i:s')."',
                                '".date('Y-m-d H:i:s')."', 
                                'KprAuthController', 
                                '790',
                                '".$data_pasangan->no_kk_pasangan."'
                            ) as response;"
                        );

                        if($insertPasangan[0]->response == 1){

                            //! JIKA SUKSES BERHASIL COMMIT PASANGAN  
                            // DB::commit();

                            //! JIKA JOINT INCOME
                            if($data_pekerjaan->skema_pembiayaan == 'joint_income'){

                                //VARIABEL KONDISI PEKERJAAN
                                $sumber_pengembalian_dana = $data_pekerjaan_pasangan->sumber_pengembalian_dana == 'fixed_income' ? 1 : 2;

                                //JIKA PEKERJAANNYA PELAJAR, TIDAK BEKERJA, DLL
                                if($data_pekerjaan_pasangan->pekerjaan == 6 ||  $data_pekerjaan_pasangan->pekerjaan == 7 || $data_pekerjaan_pasangan->pekerjaan == 8){
                                    $insertPekerjaanPasangan = DB::select("SELECT put_into_brw_pekerjaan_pasangan(
                                        '".$id."',
                                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                                        '', /*nama_perusahaan*/
                                        '', /*alamat_perusahaan*/
                                        '', /*rt*/
                                        '', /*rw*/
                                        '', /*provinsi*/
                                        '', /*kab_kota*/
                                        '', /*kecamatan*/
                                        '', /*kelurahan*/
                                        '', /*kode_pos*/
                                        '', /*no_telp*/
                                        '', /*no_hp*/
                                        '', /*surat_ijin*/
                                        '', /*no_surat_ijin*/
                                        '', /*bentuk_badan_usaha*/
                                        '', /*status_pekerjaan*/
                                        '', /*usia_perusahaan*/
                                        '', /*usia_tempat_usaha*/
                                        '', /*departemen*/
                                        '', /*jabatan*/
                                        '', /*masa_kerja_tahun*/
                                        '', /*masa_kerja_bulan*/
                                        '', /*nip_nrp_nik*/
                                        '', /*nama_hrd*/
                                        '', /*no_fixed_line_hrd*/
                                        '', /*pengalaman_kerja_tahun*/
                                        '', /*pengalaman_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->penghasilan_perbulan."', /*pendapatan_borrower*/
                                        '".$data_pekerjaan_pasangan->biaya_hidup."', /*biaya_hidup*/
                                        '".$data_pekerjaan_pasangan->detil_pekerjaan_lain."', /*detail_penghasilan_lain_lain*/
                                        '', /*total_penghasilan_lain_lain*/
                                        '', /*nilai_spt*/
                                        'KprAuthController',
                                        '826'
                                        ) as response;"
                                    );

                                //!JIKA PEKERJAANNYA FIXED INCOME
                                }elseif($sumber_pengembalian_dana == 1){

                                    $temp_surat_ijin = 0;
                                    $temp_no_telpon_perusahaan = '62'.$data_pekerjaan_pasangan->no_telpon_perusahaan_fixed_income;
                                    $temp_nomor_hrd_fixed_income = '62'.$data_pekerjaan_pasangan->nomor_hrd_fixed_income;

                                    $insertPekerjaanPasangan = DB::select("SELECT put_into_brw_pekerjaan_pasangan(
                                        '".$id."',
                                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                                        '".$data_pekerjaan_pasangan->nama_perusahaan_fixed_income."', /*nama_perusahaan*/
                                        '".$data_pekerjaan_pasangan->alamat_perusahaan_fixed_income."', /*alamat_perusahaan*/
                                        '".$data_pekerjaan_pasangan->rt."', /*rt*/
                                        '".$data_pekerjaan_pasangan->rw."', /*rw*/
                                        '".$data_pekerjaan_pasangan->provinsi."', /*provinsi*/
                                        '".$data_pekerjaan_pasangan->kota."', /*kab_kota*/
                                        '".$data_pekerjaan_pasangan->kecamatan."', /*kecamatan*/
                                        '".$data_pekerjaan_pasangan->kelurahan."', /*kelurahan*/
                                        '".$data_pekerjaan_pasangan->kode_pos."', /*kode_pos*/
                                        '".$temp_no_telpon_perusahaan."', /*no_telp*/
                                        '', /*no_hp*/
                                        '".$temp_surat_ijin."', /*surat_ijin*/
                                        '', /*no_surat_ijin*/
                                        '".$data_pekerjaan_pasangan->bentuk_badan_usaha_fixed_income."', /*bentuk_badan_usaha*/
                                        '".$data_pekerjaan_pasangan->status_pekerjaan_fixed_income."', /*status_pekerjaan*/
                                        '".$data_pekerjaan_pasangan->lama_bekerja_fixed_income."', /*usia_perusahaan*/
                                        '', /*usia_tempat_usaha*/
                                        '".$data_pekerjaan_pasangan->departemen_fixed_income."', /*departemen*/
                                        '".$data_pekerjaan_pasangan->jabatan_fixed_income."', /*jabatan*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_tahun_fixed_income."', /*masa_kerja_tahun*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_bulan_fixed_income."', /*masa_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->nip_fixed_income."', /*nip_nrp_nik*/
                                        '".$data_pekerjaan_pasangan->nama_hrd_fixed_income."', /*nama_hrd*/
                                        '".$temp_nomor_hrd_fixed_income."', /*no_fixed_line_hrd*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_tempat_lain_tahun_fixed_income."', /*pengalaman_kerja_tahun*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_tempat_lain_bulan_fixed_income."', /*pengalaman_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->penghasilan_perbulan."', /*pendapatan_borrower*/
                                        '".$data_pekerjaan_pasangan->biaya_hidup."', /*biaya_hidup*/
                                        '".$data_pekerjaan_pasangan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->nilai_spt."', /*nilai_spt*/
                                        'KprAuthController',
                                        '870'
                                        ) as response;"
                                    );

                                //!JIKA PEKERJAANNYA NOT FIXED INCOME
                                }else{

                                    $temp_bentuk_badan_usaha = 0;
                                    $temp_status_pekerjaan = 0;

                                    $temp_no_telpon_usaha_non_fixed_income = '62'.$data_pekerjaan_pasangan->no_telpon_usaha_non_fixed_income;
                                    $temp_no_hp_non_fixed_income = '62'.$data_pekerjaan_pasangan->no_hp_non_fixed_income;

                                    $insertPekerjaanPasangan = DB::select("SELECT put_into_brw_pekerjaan_pasangan(
                                        '".$id."',
                                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                                        '".$data_pekerjaan_pasangan->nama_usaha_non_fixed_income."', /*nama_perusahaan*/
                                        '".$data_pekerjaan_pasangan->alamat_usaha_non_fixed_income."', /*alamat_perusahaan*/
                                        '".$data_pekerjaan_pasangan->rt."', /*rt*/
                                        '".$data_pekerjaan_pasangan->rw."', /*rw*/
                                        '".$data_pekerjaan_pasangan->provinsi."', /*provinsi*/
                                        '".$data_pekerjaan_pasangan->kota."', /*kab_kota*/
                                        '".$data_pekerjaan_pasangan->kecamatan."', /*kecamatan*/
                                        '".$data_pekerjaan_pasangan->kelurahan."', /*kelurahan*/
                                        '".$data_pekerjaan_pasangan->kode_pos."', /*kode_pos*/
                                        '".$temp_no_telpon_usaha_non_fixed_income."', /*no_telp*/
                                        '".$temp_no_hp_non_fixed_income."', /*no_hp*/
                                        '".$data_pekerjaan_pasangan->surat_ijin_usaha_non_fixed_income."', /*surat_ijin*/
                                        '".$data_pekerjaan_pasangan->nomor_ijin_usaha_non_fixed_income."', /*no_surat_ijin*/
                                        '".$temp_bentuk_badan_usaha."', /*bentuk_badan_usaha*/
                                        '".$temp_status_pekerjaan."', /*status_pekerjaan*/
                                        '".$data_pekerjaan_pasangan->lama_usaha_non_fixed_income."', /*usia_perusahaan*/
                                        '".$data_pekerjaan_pasangan->lama_tempat_usaha_non_fixed_income."', /*usia_tempat_usaha*/
                                        '', /*departemen*/
                                        '', /*jabatan*/
                                        '', /*masa_kerja_tahun*/
                                        '', /*masa_kerja_bulan*/
                                        '', /*nip_nrp_nik*/
                                        '', /*nama_hrd*/
                                        '', /*no_fixed_line_hrd*/
                                        '', /*pengalaman_kerja_tahun*/
                                        '', /*pengalaman_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->penghasilan_perbulan."', /*pendapatan_borrower*/
                                        '".$data_pekerjaan_pasangan->biaya_hidup."', /*biaya_hidup*/
                                        '".$data_pekerjaan_pasangan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->nilai_spt."', /*nilai_spt*/
                                        'KprAuthController',
                                        '914'
                                        ) as response;"
                                    );
                                }

                                if($insertPekerjaanPasangan[0]->response == 1){
                                    //! SUKSES SIMPAN DENGAN PEKERJAAN PASANGAN

                                    // $addPendanaanKpr = $this->addPendanaanKprNew($data_ajukan_pendanaan);

                                    // if($addPendanaanKpr['response']=='sukses'){

                                        $updateStatusBorrower = DB::table('brw_user')
                                        ->where('brw_id', $id)
                                        ->update(['status' => "active"]);

                                        // DB::commit();
                                        return response()->json(['success'=> 'Berhasil Simpan Data']);
                                    // }else{
                                    //     return response()->json(['error'=> 'Gagal Simpan Data pengajuan pendanaan']);
                                    // }
                                }else{
                                    // DB::rollback();
                                    return response()->json(['error'=> 'Gagal Simpan Data brw_pekerjaan_pasangan']);
                                }
                            }else{
                                //! SUKSES SIMPAN TANPA PEKERJAAN PASANGAN

                                // $addPendanaanKpr = $this->addPendanaanKprNew($data_ajukan_pendanaan);

                                // if($addPendanaanKpr['response']=='sukses'){

                                    $updateStatusBorrower = DB::table('brw_user')
                                    ->where('brw_id', $id)
                                    ->update(['status' => "active"]);

                                    // DB::commit();
                                    return response()->json(['success'=> 'Berhasil Simpan Data']);
                                // }else{
                                //     return response()->json(['error'=> 'Gagal Simpan Data pengajuan pendanaan']);
                                // }
                            }
                            
                        }else{
                            // DB::rollback();
                            return response()->json(['error'=> 'Gagal Simpan brw_pasangan Data']);
                        }

                    }else{
                        //! SUKSES SIMPAN TANPA PASANGAN
                        // $addPendanaanKpr = $this->addPendanaanKprNew($data_ajukan_pendanaan);

                        // if($addPendanaanKpr['response']=='sukses'){

                            $updateStatusBorrower = DB::table('brw_user')
                            ->where('brw_id', $id)
                            ->update(['status' => "active"]);

                            // DB::commit();
                            return response()->json(['success'=> 'Berhasil Simpan Data']);
                        // }else{
                        //     return response()->json(['error'=> 'Gagal Simpan Data pengajuan pendanaan']);
                        // }
                    }
                    
                }else{
                    // DB::rollback();
                    return response()->json(['error'=> 'Gagal Simpan Data brw_user_detail_penghasilan']);
                }
            }else{
                // DB::rollback();
                return response()->json(['error'=> 'Gagal Simpan Data brw_rekening']);
            }
        }else{
            // DB::rollback();
            return response()->json(['error'=> 'Gagal Simpan Data brw_user_detail']);
        }
    }

    public function addPendanaanKprNew($request){
        $id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $insert_brw_pengajuan = DB::table('brw_pengajuan')->insert([
            'pendanaan_tipe' => $request->tipe_pendanaan,
            'pendanaan_tujuan' => $request->tujuan_pendanaan,
            'brw_id' => $id,
            'lokasi_proyek' => $request->alamat_pendanaan,
            'provinsi' => $request->provinsi,
            'kota' => $request->kota,
            'kecamatan' => $request->kecamatan,
            'kelurahan' => $request->kelurahan,
            'kode_pos' => $request->kode_pos,
            'harga_objek_pendanaan' => $request->harga_objek_pendanaan,
            'uang_muka' => $request->uang_muka,
            'pendanaan_dana_dibutuhkan' => $request->nilai_pengajuan_pendanaan,
            'durasi_proyek' => $request->jangka_waktu,
            'detail_pendanaan' => $request->detail_pendanaan,
            'status' => 0,
            'nm_pemilik' => $request->nama_pengaju,
            'no_tlp_pemilik' => $request->no_telpon_pengaju,
            'alamat_pemilik' => $request->alamat_pengaju,
            'provinsi_pemilik' => $request->provinsi_pengaju,
            'kota_pemilik' => $request->kota_pengaju,
            'kecamatan_pemilik' => $request->kecamatan_pengaju,
            'kelurahan_pemilik' => $request->kelurahan_pengaju,
            'kd_pos_pemilik' => $request->kode_pos_pengaju
        ]);

        if($insert_brw_pengajuan){
            $insert_deskripsi_proyek = DB::table('deskripsi_proyeks')->insert([
                'deskripsi' => $request->detail_pendanaan,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s")
            ]);

            if($insert_deskripsi_proyek){

                $select_id_pengajuan = DB::table('brw_pengajuan')->select('pengajuan_id')->orderBy('pengajuan_id', 'desc')->limit(1)->first();

                return [
                    'response'=> 'sukses',
                    'id_pengajuan'=>$select_id_pengajuan->pengajuan_id
                ];
            }else{
                return ['response'=> 'failed', 'response_error'=>'Gagal Simpan Data'];
            }
        }
    }


    public function addPendanaanKpr(Request $request){
        $id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $check_input_validation = DB::select("SELECT val_ajukan_kpr(
            '".$request->tujuan_pendanaan."',
            '1',
            '".$request->harga_objek_pendanaan."',
            '2',
            '".$request->uang_muka."',
            '3',
            '".$request->nilai_pengajuan_pendanaan."',
            'KprAuthController.php',
            '999'
            ) as response;"
        );

        if($check_input_validation[0]->response !== '1'){
            $data = $check_input_validation[0]->response;
            $response_error = explode('#', $data);

            $array = [];
            for($i=0; $i<sizeof($response_error)-1; $i++){
                $string_tenor = explode(';', $response_error[$i]);
                $id = $string_tenor[0];
                $deskripsi_error = $string_tenor[1];

                $array[$i] = [
                    'no'=> $i+1,
                    'id' => $id,
                    'deskripsi_error' => $deskripsi_error
                ];            
            }

            return response()->json(['error'=> 'error_validasi', 'response_error'=>$array]);
        }else{
            $insert_brw_pengajuan = DB::table('brw_pengajuan')->insert([
                'pendanaan_tipe' => $request->tipe_pendanaan,
                'pendanaan_tujuan' => $request->tujuan_pendanaan,
                'brw_id' => $id,
                'lokasi_proyek' => $request->alamat_pendanaan,
                'provinsi' => $request->provinsi,
                'kota' => $request->kota,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'kode_pos' => $request->kode_pos,
                'harga_objek_pendanaan' => $request->harga_objek_pendanaan,
                'uang_muka' => $request->uang_muka,
                'pendanaan_dana_dibutuhkan' => $request->nilai_pengajuan_pendanaan,
                'durasi_proyek' => $request->jangka_waktu,
                'detail_pendanaan' => $request->detail_pendanaan,
                'status' => 0,
                'nm_pemilik' => $request->nama_pengaju,
                'no_tlp_pemilik' => $request->no_telpon_pengaju,
                'alamat_pemilik' => $request->alamat_pengaju,
                'provinsi_pemilik' => $request->provinsi_pengaju,
                'kota_pemilik' => $request->kota_pengaju,
                'kecamatan_pemilik' => $request->kecamatan_pengaju,
                'kelurahan_pemilik' => $request->kelurahan_pengaju,
                'kd_pos_pemilik' => $request->kode_pos_pengaju
            ]);

            if($insert_brw_pengajuan){
                $insert_deskripsi_proyek = DB::table('deskripsi_proyeks')->insert([
                    'deskripsi' => $request->detail_pendanaan,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ]);

                if($insert_deskripsi_proyek){

                    $select_id_pengajuan = DB::table('brw_pengajuan')->select('pengajuan_id')->orderBy('pengajuan_id', 'desc')->limit(1)->first();

                    return response()->json([
                        'success'=> 'Sukses Simpan',
                        'id_pengajuan'=>$select_id_pengajuan->pengajuan_id
                    ]);
                }else{
                    return response()->json(['error'=> 'failed_save_db', 'response_error'=>'Gagal Simpan Data']);
                }
            }
        }
    }

    public function addPendanaanKprRpc(Request $request){

        //DATA PRIBADI
        $data_pribadi_rpc = json_decode($request->data_pribadi_rpc);

        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $status = Auth::guard('borrower-api-mobile')->user()->status;

        $cekGender = (int) substr($data_pribadi_rpc->no_ktp, 6, 2);
        if ($cekGender <= 31) {
            $gender = 1;
        } else {
            $gender = 2;
        }

        $cekTglLahir = (int) substr($data_pribadi_rpc->no_ktp, 6, 2);
        $tahun = (int) substr($data_pribadi_rpc->no_ktp, 10, 2);

        if ($cekTglLahir <= 31) {
            if($tahun > 40 && $tahun < 99){
                $tahun_fix = '19';
            }else{
                $tahun_fix = '20';
            }
            $bulan = substr($data_pribadi_rpc->no_ktp, 8, 2);
            $tgl = substr($data_pribadi_rpc->no_ktp, 6, 2);
            $tanggal_lahir = $tahun_fix.substr($data_pribadi_rpc->no_ktp, 10, 2) . "-" . $bulan . "-" . $tgl;
        } else {
            if($tahun > 40 && $tahun < 99){
                $tahun_fix = '19';
            }else{
                $tahun_fix = '20';
            }
            $tahun = (int) substr($data_pribadi_rpc->no_ktp, 10, 2);
            $bulan = substr($data_pribadi_rpc->no_ktp, 8, 2);
            $tgl = $cekTglLahir - 40;
            $tanggal_lahir = $tahun_fix.substr($data_pribadi_rpc->no_ktp, 10, 2) . "-" . $bulan . "-" . $tgl;
        }

        $nama = $data_pribadi_rpc->nama;
        $no_hp = '62'.$data_pribadi_rpc->no_hp;
        $no_ktp = $data_pribadi_rpc->no_ktp;
        $tempat_lahir = $data_pribadi_rpc->tempat_lahir;
        $no_npwp = $data_pribadi_rpc->no_npwp;
        $status_pernikahan = $data_pribadi_rpc->status_pernikahan[0];
        $agama = $data_pribadi_rpc->agama[0];
        $alamat = $data_pribadi_rpc->alamat;
        $provinsi = $data_pribadi_rpc->provinsi;
        $kota = $data_pribadi_rpc->kota;
        $kecamatan = $data_pribadi_rpc->kecamatan;
        $kelurahan = $data_pribadi_rpc->kelurahan;
        $kode_pos = $data_pribadi_rpc->kode_pos;
        $status_kepemilikan_rumah = $data_pribadi_rpc->status_kepemilikan_rumah[0];
        $bidang_online = $data_pribadi_rpc->bidang_online[0];
        $bidang_pekerjaan = $data_pribadi_rpc->bidang_pekerjaan[0];
        $pekerjaan = $data_pribadi_rpc->pekerjaan[0];
        $pendidikan = $data_pribadi_rpc->pendidikan[0];

        $penghasilan_perbulan = $data_pribadi_rpc->penghasilan_perbulan;
        $sumber_pengembalian_dana_ = $data_pribadi_rpc->sumber_pengembalian_dana;
        if($sumber_pengembalian_dana_ == 'fixed_income'){
            $sumber_pengembalian_dana = 1;
        }else{
            $sumber_pengembalian_dana = 2;
        };
        $nama_perusahaan_fixed_income = isset($data_pribadi_rpc->nama_perusahaan_fixed_income) ? $data_pribadi_rpc->nama_perusahaan_fixed_income : '';
        $pengalaman_kerja_bulan_fixed_income = isset($data_pribadi_rpc->pengalaman_kerja_bulan_fixed_income) ? $data_pribadi_rpc->pengalaman_kerja_bulan_fixed_income : '';
        $pengalaman_kerja_tahun_fixed_income = isset($data_pribadi_rpc->pengalaman_kerja_tahun_fixed_income) ? $data_pribadi_rpc->pengalaman_kerja_tahun_fixed_income : '';
        $no_telpon_perusahaan_fixed_income = isset($data_pribadi_rpc->no_telpon_perusahaan_fixed_income) ? $data_pribadi_rpc->no_telpon_perusahaan_fixed_income : '';

        $lama_usaha_non_fixed_income = isset($data_pribadi_rpc->lama_usaha_non_fixed_income) ? $data_pribadi_rpc->lama_usaha_non_fixed_income : '';
        $nama_usaha_non_fixed_income = isset($data_pribadi_rpc->nama_usaha_non_fixed_income) ? $data_pribadi_rpc->nama_usaha_non_fixed_income : '';
        $no_telpon_usaha_non_fixed_income = isset($data_pribadi_rpc->no_telpon_usaha_non_fixed_income) ? $data_pribadi_rpc->no_telpon_usaha_non_fixed_income : '';

        //DATA PENDANAAN
        $data_objek_pendanaan_rpc = json_decode($request->data_objek_pendanaan_rpc);

        $harga_objek_pendanaan = $data_objek_pendanaan_rpc->harga_objek_pendanaan;
        $jangka_waktu = $data_objek_pendanaan_rpc->jangka_waktu;
        $jenis_properti = $data_objek_pendanaan_rpc->jenis_properti;
        $nilai_pengajuan_pendanaan = $data_objek_pendanaan_rpc->nilai_pengajuan_pendanaan;
        $tujuan_pendanaan = $data_objek_pendanaan_rpc->tujuan_pendanaan;
        $tipe_pendanaan = $data_objek_pendanaan_rpc->tipe_pendanaan;
        $uang_muka = $data_objek_pendanaan_rpc->uang_muka;


        //DATA FOTO
        $data_foto = json_decode($request->data_foto);

        $format_foto_diri = $data_foto->uri_pic_user_investor =='' ? $data_foto->uri_pic_user_investor : 'JPEG';
        $format_foto_ktp = $data_foto->uri_pic_ktp_investor =='' ? $data_foto->uri_pic_ktp_investor : 'JPEG';
        $format_foto_diri_dan_ktp = $data_foto->uri_pic_user_ktp_investor =='' ? $data_foto->uri_pic_user_ktp_investor : 'JPEG';
        $format_foto_npwp = $data_foto->uri_pic_npwp_investor =='' ? $data_foto->uri_pic_npwp_investor : 'JPEG';
        
        $path_foto_diri = "/borrower/$brw_id/pic_brw.JPEG";
        $path_foto_ktp = "/borrower/$brw_id/pic_brw_ktp.JPEG";
        $path_foto_diri_dan_ktp = "/borrower/$brw_id/pic_brw_dan_ktp.JPEG";
        $path_foto_npwp = "/borrower/$brw_id/pic_brw_npwp.JPEG";

        // DB::beginTransaction();
        $insert_data_pribadi_rpc = DB::table('brw_user_detail')->updateOrInsert(
            ['brw_id' => $brw_id],
            [
                'brw_id' => $brw_id,
                'brw_type' => 1, //INDIVIDU
                'nama' => $nama,
                'no_tlp' => $no_hp,
                'brw_pic' => $path_foto_diri,
                'brw_pic_ktp' => $path_foto_ktp,
                'brw_pic_user_ktp' => $path_foto_diri_dan_ktp,
                'brw_pic_npwp' => $path_foto_npwp,
                'ktp' => $no_ktp,
                'tempat_lahir' => $tempat_lahir,
                'jns_kelamin' => $gender,
                'tgl_lahir' => $tanggal_lahir,
                'npwp' => $no_npwp,
                'status_kawin' => $status_pernikahan,
                'agama' => $agama,
                'alamat' => $alamat,
                'provinsi' => $provinsi,
                'kota' => $kota,
                'kecamatan' => $kecamatan,
                'kelurahan' => $kelurahan,
                'kode_pos' => $kode_pos,
                'status_rumah' => $status_kepemilikan_rumah,
                'bidang_online' => $bidang_online,
                'bidang_pekerjaan' => $bidang_pekerjaan,
                'pekerjaan' => $pekerjaan,
                'pendidikan_terakhir' => $pendidikan
            ]
        );

            if($sumber_pengembalian_dana_ == 'fixed_income'){
                $nama_perusahaan_usaha = $nama_perusahaan_fixed_income;
                $no_telp = '62'.$no_telpon_perusahaan_fixed_income;
            }else{
                $nama_perusahaan_usaha = $nama_usaha_non_fixed_income;
                $no_telp = '62'.$no_telpon_usaha_non_fixed_income;
            }
            $insert_data_pekerjaan_rpc = DB::table('brw_user_detail_penghasilan')->updateOrInsert([
                'brw_id2' => $brw_id
            ],[
                'brw_id2' => $brw_id,
                'pendapatan_borrower' => $penghasilan_perbulan,
                'sumber_pengembalian_dana' => $sumber_pengembalian_dana,
                'nama_perusahaan' => $nama_perusahaan_usaha,
                'masa_kerja_tahun' => $pengalaman_kerja_tahun_fixed_income,
                'masa_kerja_bulan' => $pengalaman_kerja_bulan_fixed_income,
                'no_telp' => $no_telp,
                'usia_perusahaan' => $lama_usaha_non_fixed_income
            ]);

                $insert_brw_pendanaan = DB::table('brw_pengajuan')->insert([
                    'brw_id' => $brw_id,
                    'pendanaan_tipe' => $tipe_pendanaan,
                    'pendanaan_tujuan' => $tujuan_pendanaan,
                    'jenis_properti'=>$jenis_properti,
                    'harga_objek_pendanaan' => $harga_objek_pendanaan,
                    'durasi_proyek' => $jangka_waktu,
                    'pendanaan_dana_dibutuhkan' => $nilai_pengajuan_pendanaan,
                    'uang_muka' => $uang_muka,
                    'status'=> '0' //0 status pengajuan baru
                ]);

                if($status=='notpreapproved'){
                    $user = LoginBorrower::where('brw_id', $brw_id)->first();
                    $user->status = 'notfilled';
                    $user->save();
                }

                $select_id_pengajuan = DB::table('brw_pengajuan')->select('pengajuan_id')->orderBy('pengajuan_id', 'desc')->limit(1)->first();

                // DB::commit();

                return response()->json([
                    'success'=> 'Sukses Simpan',
                    'id_pengajuan'=>$select_id_pengajuan->pengajuan_id
                ]);            
    }

    public function saveKprDataPribadi(Request $request){

        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;
        $data_pribadi = json_decode($request->data_pribadi);

        $nama = $data_pribadi->nama;
        $jenis_kelamin = $data_pribadi->jenis_kelamin == '0' ? '1' : '2';
        $no_ktp = $data_pribadi->no_ktp;
        $tempat_lahir = $data_pribadi->tempat_lahir;
        $tanggal_lahir = $data_pribadi->tanggal_lahir;
        $no_hp = $data_pribadi->no_hp;
        $agama = $data_pribadi->agama;
        $pendidikan = $data_pribadi->pendidikan;
        $nama_ibu = $data_pribadi->nama_ibu_kandung;
        $no_npwp = $data_pribadi->no_npwp;
        $status_pernikahan = $data_pribadi->status_pernikahan;

        // DB::beginTransaction();
        $insert_data_pribadi_rpc = DB::table('brw_user_detail')->updateOrInsert(
            ['brw_id' => $brw_id],
            [
                'brw_id' => $brw_id,
                'nama' => $nama,
                'jns_kelamin' => $jenis_kelamin,
                'ktp' => $no_ktp,
                'tempat_lahir' => $tempat_lahir,
                'tgl_lahir' => $tanggal_lahir,
                'no_tlp' => $no_hp,
                'agama' => $agama,
                'pendidikan_terakhir' => $pendidikan,
                'nm_ibu' => $nama_ibu,
                'npwp' => $no_npwp,
                'status_kawin' => $status_pernikahan,
                'status_isi_data_pribadi' => 1
            ]
        );

        return response()->json(['success' => 'Sukses Insert']);
    }

    public function saveKprDataAlamat(Request $request){
        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;
        $data_alamat = json_decode($request->data_alamat);

        $alamat_sesuai_ktp = $data_alamat->alamat_sesuai_ktp; 
        $provinsi = $data_alamat->provinsi; 
        $kota = $data_alamat->kota; 
        $kecamatan = $data_alamat->kecamatan; 
        $kelurahan = $data_alamat->kelurahan; 
        $kode_pos = $data_alamat->kode_pos; 
        $status_kepemilikan_rumah = $data_alamat->status_kepemilikan_rumah; 
        $alamat_sesuai_ktp_domisili = $data_alamat->alamat_sesuai_ktp_domisili; 
        $provinsi_domisili = $data_alamat->provinsi_domisili; 
        $kota_domisili = $data_alamat->kota_domisili; 
        $kecamatan_domisili = $data_alamat->kecamatan_domisili; 
        $kelurahan_domisili = $data_alamat->kelurahan_domisili; 
        $kode_pos_domisili = $data_alamat->kode_pos_domisili; 
        $status_kepemilikan_rumah_domisili = $data_alamat->status_kepemilikan_rumah_domisili; 

        // DB::beginTransaction();
        $insert_data_pribadi_rpc = DB::table('brw_user_detail')->updateOrInsert(
            ['brw_id' => $brw_id],
            [
                'brw_id' => $brw_id,
                'alamat'=> $alamat_sesuai_ktp,
                'provinsi'=> $provinsi,
                'kota'=> $kota,
                'kecamatan'=> $kecamatan,
                'kelurahan'=> $kelurahan,
                'kode_pos'=> $kode_pos,
                'status_rumah'=> $status_kepemilikan_rumah,
                'domisili_alamat'=> $alamat_sesuai_ktp_domisili,
                'domisili_provinsi'=> $provinsi_domisili,
                'domisili_kota'=> $kota_domisili,
                'domisili_kecamatan'=> $kecamatan_domisili,
                'domisili_kelurahan'=> $kelurahan_domisili,
                'domisili_kd_pos'=> $kode_pos_domisili,
                'domisili_status_rumah'=> $status_kepemilikan_rumah_domisili,
                'status_isi_data_alamat' => 1
            ]
        );

        return response()->json(['success' => 'Sukses Insert']);
    }

    public function saveKprDataRekening(Request $request){
        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;
        $data_rekening = json_decode($request->data_rekening);

        $nomor_rekening = $data_rekening->nomor_rekening;
        $nama_pemilik_rekening = $data_rekening->nama_pemilik_rekening;
        $nama_bank = $data_rekening->nama_bank;
        $kantor_cabang_pembuka = $data_rekening->kantor_cabang_pembuka;

        $insert_data_pribadi_rpc = DB::table('brw_rekening')->updateOrInsert(
            ['brw_id' => $brw_id],
            [
                'brw_id' => $brw_id,
                'brw_norek' => $nomor_rekening,
                'brw_nm_pemilik' => $nama_pemilik_rekening,
                'brw_kd_bank' => $nama_bank,
                'kantor_cabang_pembuka' => $kantor_cabang_pembuka,
                'total_plafon' => '2000000000',
                'total_terpakai' => '0',
                'total_sisa' => '2000000000',
                'status_isi_data_rekening' => 1
            ]
        );

        return response()->json(['success' => 'Sukses Insert']);
    }

    public function pengajuanPendanaanRumah(Request $request)
	{

		try {
            $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

            $data_page1 = json_decode($request->dataPage1);
            $data_page2 = json_decode($request->dataPage2);
            $data_page3 = json_decode($request->dataPage3);

            $insert_brw_pengajuan = DB::table('brw_pengajuan')->updateOrInsert(
                ['pengajuan_id' => $data_page1->id_pengajuan],
                [
                    'pengajuan_id' => $data_page1->id_pengajuan,
                    'lokasi_proyek' => $data_page1->alamat_pendanaan,
					'provinsi' => $data_page1->provinsi,
					'kota' => $data_page1->kota,
					'kecamatan' => $data_page1->kecamatan,
					'kelurahan' => $data_page1->kelurahan,
					'kode_pos' => $data_page1->kode_pos,
					// 'detail_pendanaan' => $request->in_dtl_obj_pendanaan,

					'nm_pemilik' => $data_page1->nama_pengaju,
					'no_tlp_pemilik' => '62' . $data_page1->no_telpon_pengaju,
					'alamat_pemilik' => $data_page1->alamat_pengaju,
					'provinsi_pemilik' => $data_page1->provinsi_pengaju,
					'kota_pemilik' => $data_page1->kota_pengaju,
					'kecamatan_pemilik' => $data_page1->kecamatan_pengaju,
					'kelurahan_pemilik' => $data_page1->kelurahan_pengaju,
					'kd_pos_pemilik' => $data_page1->kode_pos_pengaju,
					'status' => 11
                ]
            );

            $insert_brw_agunan = DB::table('brw_agunan')->updateOrInsert(
                ['id_pengajuan' => $data_page1->id_pengajuan],
                [
                    'id_pengajuan' => $data_page1->id_pengajuan,
                    'jenis_agunan' => $data_page3->jenis_agunan == 'hgb' ? 2 : 1,
                    'nomor_agunan' => $data_page3->nomor_agunan,
                    'atas_nama_agunan' => $data_page3->atas_nama_agunan,
                    'luas_bangunan' => $data_page3->luas_bangunan,
                    'luas_tanah' => $data_page3->luas_tanah,
                    'nomor_surat_ukur' => $data_page3->nomor_surat_ukur,
                    'tanggal_surat_ukur' => (isset($data_page3->tanggal_surat_ukur) && !empty($data_page3->tanggal_surat_ukur))  ? date('Y-m-d', strtotime($data_page3->tanggal_surat_ukur)) : '',
                    'tanggal_terbit' => (isset($data_page3->tanggal_terbit) && !empty($data_page3->tanggal_terbit))  ? date('Y-m-d', strtotime($data_page3->tanggal_terbit)) : '',
                    'tanggal_jatuh_tempo' => (isset($data_page3->tanggal_jatuh_tempo) && !empty($data_page3->tanggal_jatuh_tempo))  ? date('Y-m-d', strtotime($data_page3->tanggal_jatuh_tempo)) : '',
                    'kantor_penerbit' => $data_page3->kantor_penerbit,
                    'blok_nomor' => $data_page3->blok_agunan,
                    'RT' => $data_page3->rt_agunan,
                    'RW' => $data_page3->rw_agunan,
                    'no_imb' => $data_page3->nomor_imb,
                    'tanggal_terbit_imb' => $data_page3->tanggal_terbit_imb,
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );

			//Pendanaan Rumah Lain
			$this->pendanaanRumahLain($data_page2, $data_page1->id_pengajuan);

			//Pendanaan Non Rumah Lain 
			$this->pendanaanNonRumahLain($data_page2, $data_page1->id_pengajuan);

			return response()->json(['status' => 'success', 'message' => 'Berhasil di simpan']);
		} catch (\Exception $e) {
			return response()->json(['status' => 'failed', 'message' => $e->getMessage()]);
		}
	}

    public function pengajuanPendanaanRumahDibantu(Request $request){
        try {
            $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

            $data_page1 = json_decode($request->dataPage1);

            $insert_brw_pengajuan = DB::table('brw_pengajuan')->updateOrInsert(
                ['pengajuan_id' => $data_page1->id_pengajuan],
                [
                    'pengajuan_id' => $data_page1->id_pengajuan,
                    'lokasi_proyek' => $data_page1->alamat_pendanaan,
					'provinsi' => $data_page1->provinsi,
					'kota' => $data_page1->kota,
					'kecamatan' => $data_page1->kecamatan,
					'kelurahan' => $data_page1->kelurahan,
					'kode_pos' => $data_page1->kode_pos,
					// 'detail_pendanaan' => $request->in_dtl_obj_pendanaan,

					'nm_pemilik' => $data_page1->nama_pengaju,
					'no_tlp_pemilik' => '62' . $data_page1->no_telpon_pengaju,
					'alamat_pemilik' => $data_page1->alamat_pengaju,
					'provinsi_pemilik' => $data_page1->provinsi_pengaju,
					'kota_pemilik' => $data_page1->kota_pengaju,
					'kecamatan_pemilik' => $data_page1->kecamatan_pengaju,
					'kelurahan_pemilik' => $data_page1->kelurahan_pengaju,
					'kd_pos_pemilik' => $data_page1->kode_pos_pengaju,
					'status' => 11,
                    'setuju_verifikator_pengajuan' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );

			return response()->json(['status' => 'success', 'message' => 'Berhasil di simpan']);
		} catch (\Exception $e) {
			return response()->json(['status' => 'failed', 'message' => $e->getMessage()]);
		}
    }

    public function pengajuanPendanaanRumahDibantuDokumen(Request $request){
        try {
            $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

            DB::table('brw_pengajuan')->where('pengajuan_id', $request->id_pengajuan)->update(['setuju_verifikator_dokumen' => 1, 'updated_at' => date('Y-m-d H:i:s')]);

			return response()->json(['status' => 'success', 'message' => 'Berhasil di simpan']);
		} catch (\Exception $e) {
			return response()->json(['status' => 'failed', 'message' => $e->getMessage()]);
		}
    }

	public function pendanaanRumahLain($request, $id_pengajuan)
	{

		BorrowerDanaRumahLain::where('pengajuan_id', $id_pengajuan)->delete();

		if (isset($request->rumah_lainnya_sebanyak) && $request->rumah_lainnya_sebanyak > 0) {

			//Pendanaan Rumah lain
			for ($i = 1; $i <= $request->rumah_lainnya_sebanyak; $i++) {
				// $rumah_ke =  ($request->rumah_lainnya_sebanyak == 2) ? $request->rumah_lainnya_sebanyak - 1 : $request->rumah_lainnya_sebanyak - $i;
				$rumah_ke =  $i;
                $status = 'status'.$i;
                $bank = 'bank'.$i;
                $plafond = 'plafond'.$i;
                $jangka_waktu = 'jangka_waktu'.$i;
                $outstanding = 'outstanding'.$i;
                $angsuran = 'angsuran'.$i;

				$data_rumah_lain = [
					'pengajuan_id' => $id_pengajuan,
					'rumah_ke' => $rumah_ke,
					'status' => isset($request->$status) ? $request->$status : '',
					'bank'  => isset($request->$bank) ? $request->$bank : '',
					'plafond' => isset($request->$plafond) ? $request->$plafond : '',
					'jangka_waktu' => isset($request->$jangka_waktu) ? $request->$jangka_waktu : '',
					'outstanding' => isset($request->$outstanding) ? $request->$outstanding : '',
					'angsuran' => isset($request->$angsuran) ? $request->$angsuran : ''
				];
				BorrowerDanaRumahLain::create($data_rumah_lain);
			}
		}
	}



    public function pendanaanNonRumahLain($request, $id_pengajuan)
	{

		BorrowerDanaNonRumahLain::where('pengajuan_id', $id_pengajuan)->delete();

		if (isset($request->jumlah_fasilitas_pembiayaan_berjalan) && $request->jumlah_fasilitas_pembiayaan_berjalan > 0 && $request->fasilitas_pembiayaan_berjalan == 'Ya') {
			for ($i = 1; $i <= $request->jumlah_fasilitas_pembiayaan_berjalan; $i++) {

                $bank = 'bank_pembiayaan_lain'.$i;
                $plafond = 'plafond_pembiayaan_lain'.$i;
                $jangka_waktu = 'jangka_waktu_pembiayaan_lain'.$i;
                $outstanding = 'outstanding_pembiayaan_lain'.$i;
                $angsuran = 'angsuran_pembiayaan_lain'.$i;

				$data_non_rumah_lain = [
					'pengajuan_id' => $id_pengajuan,
					'bank'  => isset($request->$bank) ? $request->$bank : '',
					'plafond' => isset($request->$plafond) ? $request->$plafond : '',
					'jangka_waktu' => isset($request->$jangka_waktu) ? $request->$jangka_waktu : '',
					'outstanding' => isset($request->$outstanding) ? $request->$outstanding : '',
					'angsuran' => isset($request->$angsuran) ? $request->$angsuran : ''
				];
				BorrowerDanaNonRumahLain::create($data_non_rumah_lain);
			}
		}
	}

    public function getDocumentValue($tableName, $whereFilter,  $id, $fieldName)
    {
        return  \DB::table($tableName)->where($whereFilter, $id)->value($fieldName);
    }

    public function pengajuanExists($tableName, $whereFilter,  $id)
    {
        return \DB::table($tableName)->where($whereFilter, $id)->first();
    }

    public function uploadDokumen(Request $request)
    {
        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;
        $id_pengajuan = $request->id_pengajuan;
        $jenis_foto_upload = $request->jenis_foto_upload;
        $table_name = $request->table_name;

        //cek exists 
        if($table_name == 'brw_dokumen_objek_pendanaan'){

            $fileSaved = $this->getDocumentValue($table_name, 'id_pengajuan', $id_pengajuan, $jenis_foto_upload);
            $store_path = 'borrower/'.$brw_id.'/'.$id_pengajuan;

            if ($fileSaved) {
                if (file_exists(storage_path('app/private/' . $fileSaved))) {

                    File::delete(storage_path('app/private/' . $fileSaved));
                }
            }
        }else if($table_name == 'brw_dokumen_legalitas_pribadi'){
            
            $fileSaved = $this->getDocumentValue($table_name, 'brw_id', $brw_id, $jenis_foto_upload);
            $store_path = 'borrower/'.$brw_id;

            if ($fileSaved) {
                if (file_exists(storage_path('app/private/' . $fileSaved))) {

                    File::delete(storage_path('app/private/' . $fileSaved));
                }
            }
        }else if($table_name == 'brw_user_detail'){
            
            $fileSaved = $this->getDocumentValue($table_name, 'brw_id', $brw_id, $jenis_foto_upload);
            $store_path = 'borrower/'.$brw_id;

            if ($fileSaved) {
                if (file_exists(storage_path('app/private/' . $fileSaved))) {

                    File::delete(storage_path('app/private/' . $fileSaved));
                }
            }
        }
        
        if ($request->hasFile($jenis_foto_upload)) {
            $file = $request->file($jenis_foto_upload);
            $resize = Image::make($file)->save();
            $filename = $jenis_foto_upload. '_'.date('YmdHis'). '.' . $file->getClientOriginalExtension();
            $upload_file = $file->storeAs($store_path, $filename, 'private');
            $fileToSave = $store_path .  '/' . $filename;

            if($upload_file){

                if($table_name == 'brw_dokumen_objek_pendanaan'){
                    $pengajuanExists = $this->pengajuanExists($table_name, 'id_pengajuan', $id_pengajuan);
                    if ($pengajuanExists) {
                        DB::table($table_name)->where('id_pengajuan', $id_pengajuan)->update([$jenis_foto_upload => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                    } else {
                        DB::table($table_name)->insert(['id_pengajuan' => $id_pengajuan, $jenis_foto_upload => $fileToSave]);
                    }
                }else if($table_name == 'brw_dokumen_legalitas_pribadi'){
                    $pengajuanExists = $this->pengajuanExists($table_name, 'brw_id', $brw_id);
                    if ($pengajuanExists) {
                        DB::table($table_name)->where('brw_id', $brw_id)->update([$jenis_foto_upload => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                    } else {
                        DB::table($table_name)->insert(['brw_id' => $brw_id, $jenis_foto_upload => $fileToSave]);
                    }
                }else if($table_name == 'brw_user_detail'){
                    $pengajuanExists = $this->pengajuanExists($table_name, 'brw_id', $brw_id);
                    if ($pengajuanExists) {
                        DB::table($table_name)->where('brw_id', $brw_id)->update([$jenis_foto_upload => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                    } else {
                        DB::table($table_name)->insert(['brw_id' => $brw_id, $jenis_foto_upload => $fileToSave]);
                    }
                }

                return response()->json([
                    'success' => 'Berhasil di upload'
                ]);
            }else{
                return response()->json([
                    'failed' => 'File gagal di upload'
                ]);
            }
        }
        else {
            return [
                'failed' => 'File Kosong'
            ];
        }
    }

}