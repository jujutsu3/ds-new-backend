<?php
namespace App\Http\Controllers\Mobile\Kpr;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;
use Image;
use GuzzleHttp\Client;
use DB;
use Exception;
use Storage;
use File;
use Response;

use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;

class KprGetDataController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
	 * ichal_sl            01-Mar-2022              add getDataUserDetail
     */

    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['except' => [ 'getProvinsi', 'getKota', 'getKecamatan', 'getKelurahan', 'getKodePos', 'getProvinsiPendanaan', 'getKotaPendanaan', 'getKecamatanPendanaan', 'getKelurahanPendanaan', 'masterSimulasiPengajuan', 'getResultSimulationKepemilikan', 'getResultSimulationPraKepemilikan']]);
    }

    public function masterSimulasiPengajuan(){

        $master_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->where('pendanaan_nama', 'Dana Rumah')->get();
        $x=0;
        if (!isset($master_tipe_pendanaan[0])) {
            $master_tipe_pendanaan = null;
        }
        else {
                foreach ($master_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan', 'max_tenor', 'min_pembiayaan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan
                ];
                $x++;
            }
        }

        return [
            'data_tipe_pendanaan' => $data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan,
        ];
    }

    public function allMasterPengajuan(){

        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $master_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama', 'pendanaan_keterangan')->whereIn('pendanaan_nama', ['Dana Rumah', 'Dana Konstruksi'])->get();
        $x=0;
        if (!isset($master_tipe_pendanaan[0])) {
            $master_tipe_pendanaan = null;
        }
        else {
                foreach ($master_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
                    'detil' => $item->pendanaan_keterangan,
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan', 'max_tenor', 'min_pembiayaan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan,
                    'max_tenor' => $item->max_tenor,
                    'min_pembiayaan' => $item->min_pembiayaan,
                ];
                $x++;
            }
        }

        return [
            'data_tipe_pendanaan' => $data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan
        ];
    }

    public function allMasterFormRpc(){

        $master_kawin = MasterKawin::all();
        $x=0;
        if (!isset($master_kawin[0])) {
            $master_kawin = null;
        }else {
                foreach ($master_kawin as $item){
                $data_kawin[$x] = [
                    'id_kawin'=>$item->id_kawin,
                    'jenis_kawin'=>$item->jenis_kawin,

                ];
                $x++;
            }
        }

        $master_pendidikan = MasterPendidikan::all();
        $x=0;
        if (!isset($master_pendidikan[0])) {
            $master_pendidikan = null;
        }else {
                foreach ($master_pendidikan as $item){
                $data_pendidikan[$x] = [
                    'id_pendidikan'=>$item->id_pendidikan,
                    'pendidikan'=>$item->pendidikan,

                ];
                $x++;
            }
        }

        $master_pekerjaan = MasterPekerjaan::whereNotIn('id_pekerjaan', [6,7,8])->get();
        $x=0;
        if (!isset($master_pekerjaan[0])) {
            $master_pekerjaan = null;
        }else {
                foreach ($master_pekerjaan as $item){
                $data_pekerjaan[$x] = [
                    'id_pekerjaan'=>$item->id_pekerjaan,
                    'pekerjaan'=>$item->pekerjaan,

                ];
                $x++;
            }
        }

        $master_agama = MasterAgama::all();
        $x=0;
        if (!isset($master_agama[0])) {
            $master_agama = null;
        }else {
                foreach ($master_agama as $item){
                $data_agama[$x] = [
                    'id_agama'=>$item->id_agama,
                    'agama'=>$item->agama,
                ];
                $x++;
            }
        }

        $master_bidang_pekerjaan = MasterBidangPekerjaan::whereNotIn('kode_bidang_pekerjaan', ['e00', 'e01'])->get();
        $x=0;
        if (!isset($master_bidang_pekerjaan[0])) {
            $master_bidang_pekerjaan = null;
        }else {
                foreach ($master_bidang_pekerjaan as $item){
                $data_bidang_pekerjaan[$x] = [
                    'id_bidang_pekerjaan'=>$item->id_bidang_pekerjaan,
                    'kode_bidang_pekerjaan'=>$item->kode_bidang_pekerjaan,
                    'bidang_pekerjaan'=>$item->bidang_pekerjaan,
                ];
                $x++;
            }
        }

        $master_online = MasterOnline::all();
        $x=0;
        if (!isset($master_online[0])) {
            $master_online = null;
        }else {
                foreach ($master_online as $item){
                $data_online[$x] = [
                    'id_online'=>$item->id_online,
                    'tipe_online'=>$item->tipe_online,
                ];
                $x++;
            }
        }

        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        $data_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->whereIn('tipe_id', [2])->get(); //Dana Konstruksi, Dana Rumah
        $x=0;
        if (!isset($data_tipe_pendanaan[0])) {
            $data_tipe_pendanaan = null;
        }
        else {
                foreach ($data_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan
                ];
                $x++;
            }
        }

        $master_bentuk_badan_usaha = DB::table('m_bentuk_badan_usaha')->select('id', 'bentuk_badan_usaha')->get();
        $x=0;
        if (!isset($master_bentuk_badan_usaha[0])) {
            $master_bentuk_badan_usaha = null;
        }
        else{
                foreach ($master_bentuk_badan_usaha as $item){
                $data_master_bentuk_badan_usaha[$x] = [
                    'id'=>$item->id,
                    'bentuk_badan_usaha'=>$item->bentuk_badan_usaha
                ];
                $x++;
            }
        }

        $get_kepemilikan_rumah = DB::table('m_kepemilikan_rumah')->select('id_kepemilikan_rumah', 'kepemilikan_rumah')->get();
        $x=0;
        if (!isset($get_kepemilikan_rumah[0])) {
            $data_kepemilikan_rumah = [];
        }
        else {
                foreach ($get_kepemilikan_rumah as $item){
                $data_kepemilikan_rumah[$x] = [
                    'id'=>$item->id_kepemilikan_rumah,
                    'kepemilikan_rumah'=>$item->kepemilikan_rumah,
                ];
                $x++;
            }
        }

        $data_jenis_rumah=array();
        $data_1 = [
            'id'=> '1', 'jenis_properti'=>'Rumah Baru',
        ];
        array_push($data_jenis_rumah, $data_1);

        $data_2 = [
            'id'=> '2', 'jenis_properti'=>'Rumah Bekas'
        ];
        array_push($data_jenis_rumah, $data_2);

        return [
            'data_agama' => $data_agama,
            'data_kawin' => $data_kawin,
            'data_pendidikan' => $data_pendidikan,
            'data_pekerjaan' => $data_pekerjaan,
            'data_bidang_pekerjaan' => $data_bidang_pekerjaan,
            'data_online' => $data_online,
            'data_provinsi' => $data_master_provinsi,
            'data_tipe_pendanaan' =>$data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan,
            'data_master_bentuk_badan_usaha' => $data_master_bentuk_badan_usaha,
            'data_kepemilikan_rumah' => $data_kepemilikan_rumah,
            'data_jenis_rumah'=>$data_jenis_rumah
        ];
    }

    public function allMasterBaru() {

        $master_jenis_kelamin = MasterJenisKelamin::all();
        $x=0;
        if (!isset($master_jenis_kelamin[0])) {
            $master_jenis_kelamin = null;
        }
        else {
                foreach ($master_jenis_kelamin as $item){
                $data_jenis_kelamin[$x] = [
                    'id'=>$item->id_jenis_kelamin,
                    'jenis_kelamin'=>$item->jenis_kelamin,
                ];
                $x++;
            }
        }

        $master_bank = MasterBank::all();
        $x=0;
        if (!isset($master_bank[0])) {
            $master_bank = null;
        }
        else {
                foreach ($master_bank as $item){
                $data_bank[$x] = [
                    'kode'=>$item->kode_bank,
                    'nama_bank'=>$item->nama_bank,
                ];
                $x++;
            }
        }

        $master_kawin = MasterKawin::all();
        $x=0;
        if (!isset($master_kawin[0])) {
            $master_kawin = null;
        }
        else {
                foreach ($master_kawin as $item){
                $data_kawin[$x] = [
                    'id_kawin'=>$item->id_kawin,
                    'jenis_kawin'=>$item->jenis_kawin,

                ];
                $x++;
            }
        }

        $master_pendapatan = MasterPendapatan::all();
        $x=0;
        if (!isset($master_pendapatan[0])) {
            $master_pendapatan = null;
        }
        else {
                foreach ($master_pendapatan as $item){
                $data_pendapatan[$x] = [
                    'id_pendapatan'=>$item->id_pendapatan,
                    'pendapatan'=>$item->pendapatan,

                ];
                $x++;
            }
        }

        $master_pendidikan = MasterPendidikan::all();
        $x=0;
        if (!isset($master_pendidikan[0])) {
            $master_pendidikan = null;
        }
        else {
                foreach ($master_pendidikan as $item){
                $data_pendidikan[$x] = [
                    'id_pendidikan'=>$item->id_pendidikan,
                    'pendidikan'=>$item->pendidikan,

                ];
                $x++;
            }
        }

        $master_pekerjaan = MasterPekerjaan::whereNotIn('id_pekerjaan', [6,7,8])->get();
        $x=0;
        if (!isset($master_pekerjaan[0])) {
            $master_pekerjaan = null;
        }
        else {
                foreach ($master_pekerjaan as $item){
                $data_pekerjaan[$x] = [
                    'id_pekerjaan'=>$item->id_pekerjaan,
                    'pekerjaan'=>$item->pekerjaan,

                ];
                $x++;
            }
        }

        $master_jenis_pengguna = MasterJenisPengguna::all();
        $x=0;
        if (!isset($master_jenis_pengguna[0])) {
            $master_jenis_pengguna = null;
        }
        else {
                foreach ($master_jenis_pengguna as $item){
                $data_jenis_pengguna[$x] = [
                    'id_jenis_pengguna'=>$item->id_jenis_pengguna,
                    'jenis_pengguna'=>$item->jenis_pengguna,
                ];
                $x++;
            }
        }

        $master_agama = MasterAgama::all();
        $x=0;
        if (!isset($master_agama[0])) {
            $master_agama = null;
        }
        else {
                foreach ($master_agama as $item){
                $data_agama[$x] = [
                    'id_agama'=>$item->id_agama,
                    'agama'=>$item->agama,
                ];
                $x++;
            }
        }

        $master_bidang_pekerjaan = MasterBidangPekerjaan::whereNotIn('kode_bidang_pekerjaan', ['e00', 'e01'])->get();
        $x=0;
        if (!isset($master_bidang_pekerjaan[0])) {
            $master_bidang_pekerjaan = null;
        }
        else {
                foreach ($master_bidang_pekerjaan as $item){
                $data_bidang_pekerjaan[$x] = [
                    'id_bidang_pekerjaan'=>$item->id_bidang_pekerjaan,
                    'kode_bidang_pekerjaan'=>$item->kode_bidang_pekerjaan,
                    'bidang_pekerjaan'=>$item->bidang_pekerjaan,
                ];
                $x++;
            }
        }

        $master_pengalaman_kerja = MasterPengalamanKerja::all();
        $x=0;
        if (!isset($master_pengalaman_kerja[0])) {
            $master_pengalaman_kerja = null;
        }
        else {
                foreach ($master_pengalaman_kerja as $item){
                $data_pengalaman_kerja[$x] = [
                    'id_pengalaman_kerja'=>$item->id_pengalaman_kerja,
                    'pengalaman_kerja'=>$item->pengalaman_kerja,
                ];
                $x++;
            }
        }

        $master_online = MasterOnline::all();
        $x=0;
        if (!isset($master_online[0])) {
            $master_online = null;
        }
        else {
                foreach ($master_online as $item){
                $data_online[$x] = [
                    'id_online'=>$item->id_online,
                    'tipe_online'=>$item->tipe_online,
                ];
                $x++;
            }
        }

        $master_hub_waris = DB::table('m_hub_ahli_waris')->select('id_hub_ahli_waris', 'jenis_hubungan')->get();
        $x=0;
        if (!isset($master_hub_waris[0])) {
            $master_hub_waris = null;
        }
        else {
                foreach ($master_hub_waris as $item){
                $data_hub_waris[$x] = [
                    'id_hub_ahli_waris'=>$item->id_hub_ahli_waris,
                    'jenis_hubungan'=>$item->jenis_hubungan,
                ];
                $x++;
            }
        }

        $master_bentuk_badan_usaha = DB::table('m_bentuk_badan_usaha')->select('id', 'bentuk_badan_usaha')->get();
        $x=0;
        if (!isset($master_bentuk_badan_usaha[0])) {
            $master_bentuk_badan_usaha = null;
        }
        else{
                foreach ($master_bentuk_badan_usaha as $item){
                $data_master_bentuk_badan_usaha[$x] = [
                    'id'=>$item->id,
                    'bentuk_badan_usaha'=>$item->bentuk_badan_usaha
                ];
                $x++;
            }
        }

        return [
            'data_jenis_kelamin' => $data_jenis_kelamin,
            'data_bank' => $data_bank,
            'data_kawin' => $data_kawin,
            'data_pendapatan' => $data_pendapatan,
            'data_pendidikan' => $data_pendidikan,
            'data_pekerjaan' => $data_pekerjaan,
            'data_jenis_pengguna' => $data_jenis_pengguna,
            'data_agama' => $data_agama,
            'data_bidang_pekerjaan' => $data_bidang_pekerjaan,
            'data_pengalaman_kerja' => $data_pengalaman_kerja,
            'data_online' => $data_online,
            'data_hub_waris' => $data_hub_waris,
            'data_master_bentuk_badan_usaha'=>$data_master_bentuk_badan_usaha
        ];
    }

    public function getProvinsi(){

        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }

        else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        return [
            'data_provinsi' => $data_master_provinsi
        ];
    }

    public function getKota($provinsi){

        $getKota = DB::table('m_kode_pos')
            ->select('id_kode_pos', 'Jenis', 'Kota')
            ->where('Provinsi', $provinsi)
            ->groupBy('Kota')
            ->get();

        $x=0;
        if (!isset($getKota[0])) {
            $getKota = null;
        }

        else{
                foreach ($getKota as $item){
                $data_master_kota[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'jenis'=>$item->Jenis,
                    'kota'=>$item->Kota,
                ];
                $x++;
            }
        }
	if ($getKota == NULL) return [  'data_master_kota' => NULL ];
        return [
            'data_master_kota' => $data_master_kota
        ];
    }

    public function getKecamatan($kota){

        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'kecamatan')->where('Kota', $kota)->groupBy('kecamatan')->get();

        $x=0;
        if (!isset($getKecamatan[0])) {
            $getKecamatan = null;
        }

        else{
                foreach ($getKecamatan as $item){
                $data_master_kecamatan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kecamatan'=>$item->kecamatan,
                ];
                $x++;
            }
        }
        if ($getKecamatan == NULL) return [  'data_master_kecamatan' => NULL ];
        return [
            'data_master_kecamatan' => $data_master_kecamatan
        ];
    }

    public function getKelurahan($kecamatan){

        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'kelurahan', 'kode_pos')->where('kecamatan', $kecamatan)->groupBy('kelurahan')->get();

        $x=0;
        if (!isset($getKelurahan[0])) {
            $getKelurahan = null;
        }

        else{
                foreach ($getKelurahan as $item){
                $data_master_kelurahan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kode_pos'=>$item->kode_pos,
                    'kelurahan'=>$item->kelurahan,
                ];
                $x++;
            }
        }
        if ($getKelurahan == NULL) return [  'data_master_kelurahan' => NULL ];
        return [
            'data_master_kelurahan' => $data_master_kelurahan
        ];
    }

    public function getKodePos($kelurahan, $kecamatan){

        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')->where('kelurahan', $kelurahan)->where('kecamatan', $kecamatan)->first();

        if($getKodePos){
            return [
                'kode_pos' => $getKodePos->kode_pos
            ];
        }else{
            return [
                'kode_pos' => ''
            ];
        }
    }

    public function getProvinsiPendanaan(){

        $data_prov = [31, 32, 36];
        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->whereIn('kode_provinsi', $data_prov)->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }

        else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        return [
            'data_provinsi' => $data_master_provinsi
        ];
    }

    public function getKotaPendanaan($provinsi){


        // $kota_banten = ['Tangerang', 'Tangerang Selatan'];
        // $kota_jakarta = ['Kepulauan Seribu'];
        // $kota_jawa_barat = ['Bandung', 'Bandung Barat', 'Bogor', 'Bekasi', 'Depok'];

        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')->where('Provinsi', $provinsi)->groupBy('Kota')->get();

        // if($provinsi == 'Banten'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereIn('Kota', $kota_banten)->groupBy('Kota')->get();
        // }else if($provinsi == 'DKI Jakarta'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereNotIn('Kota', $kota_jakarta)->groupBy('Kota')->get();
        // }else if($provinsi == 'Jawa Barat'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereIn('Kota', $kota_jawa_barat)->groupBy('Kota')->get();
        // }else{
        //     $getKota = $getKota->where('Provinsi', $provinsi)->groupBy('Kota')->get();
        // }

        $x=0;
        if (!isset($getKota[0])) {
            $getKota = null;
        }

        else{
                foreach ($getKota as $item){
                $data_master_kota[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kota'=>$item->Kota,
                ];
                $x++;
            }
        }

        return [
            'data_master_kota' => $data_master_kota
        ];
    }

    public function getKecamatanPendanaan($kota){

        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'kecamatan')->where('Kota', $kota)->groupBy('kecamatan')->get();

        $x=0;
        if (!isset($getKecamatan[0])) {
            $getKecamatan = null;
        }

        else{
                foreach ($getKecamatan as $item){
                $data_master_kecamatan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kecamatan'=>$item->kecamatan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kecamatan' => $data_master_kecamatan
        ];
    }

    public function getKelurahanPendanaan($kecamatan){

        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'kelurahan')->where('kecamatan', $kecamatan)->groupBy('kelurahan')->get();

        $x=0;
        if (!isset($getKelurahan[0])) {
            $getKelurahan = null;
        }

        else{
                foreach ($getKelurahan as $item){
                $data_master_kelurahan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kelurahan'=>$item->kelurahan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kelurahan' => $data_master_kelurahan
        ];
    }

    public function getKodePosPendanaan($kelurahan, $kecamatan){

        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')->where('kelurahan', $kelurahan)->where('kecamatan', $kecamatan)->first();

        if($getKodePos){
            return [
                'kode_pos' => $getKodePos->kode_pos
            ];
        }else{
            return [
                'kode_pos' => ''
            ];
        }
    }

    public function getResultSimulationKepemilikan(Request $request){

        $total_pinjaman = $request->nilai_pengajuan_pendanaan;
        $tenor = $request->jangka_waktu;
        $tenor_bulan = $tenor*12;

        // $call_log_db = DB::select("select get_simulation_kpr( '$total_pinjaman','$tenor', 'KprGetDataController.php', '744') as response;");
        $margin_efektif = DB::select("SELECT margin_efektif FROM m_param_kalkulator WHERE tenor = 999")[0]->margin_efektif;
        $call_log_db = DB::select("SELECT get_new_simulation_kpr('".$total_pinjaman."', '".$tenor_bulan."','".$margin_efektif."', 'KprGetDataController.php', '".__LINE__."') as response;");
        
        $data = $call_log_db[0]->response;
        $final = explode('#', $data);
        $array = [];
        // $total_cicilan = str_replace('^', '', $final[sizeof($final)-1]);
        $total_cicilan = 0;

        for($i=0; $i<sizeof($final)-1; $i++){
            $string_tenor = explode(';', $final[$i]);
            $tenor = $string_tenor[0];
            $nilai_cicilan = number_format($string_tenor[1],0,'.',',');
            $total_cicilan_periode = $string_tenor[2];
            $angsuran_pokok = number_format($string_tenor[3],0,'.',',');
            $angsuran_margin = number_format($string_tenor[4],0,'.',',');

            $array[$i] = [
                'id'=> $i,
                'tenor' => $tenor,
                'nilai_cicilan' => $nilai_cicilan,
                'total_cicilan_periode' => $total_cicilan_periode,
                'angsuran_pokok' => $angsuran_pokok,
                'angsuran_margin' => $angsuran_margin
            ];            

            $total_cicilan = $total_cicilan+$string_tenor[3]+$string_tenor[4];

            $bulanakhir = $i+1;
            $tenorakhir = $tenor+1;
            $nilai_cicilan_akhir = $nilai_cicilan;
            $total_cicilan_periode_akhir = $total_cicilan_periode;
            $angsuran_pokok_akhir = $string_tenor[5];
            $angsuran_margin_akhir = $string_tenor[5]-$string_tenor[3];
        }

        $array[$bulanakhir] = [
            'id'=> $bulanakhir,
            'tenor' => $tenorakhir,
            'nilai_cicilan' => $nilai_cicilan_akhir,
            'total_cicilan_periode' => $total_cicilan_periode_akhir,
            'angsuran_pokok' => $angsuran_pokok_akhir,
            'angsuran_margin' => number_format($angsuran_margin_akhir,0,'.',',')
        ]; 

        return response()->json(['data' => $array, 'total_cicilan'=> number_format($total_cicilan+$angsuran_pokok_akhir+$angsuran_margin_akhir,0,'.',',')]);
    }

    public function getResultSimulationPraKepemilikan(Request $request){

        $total_pinjaman = $request->nilai_pengajuan_pendanaan;
        $tenor = $request->jangka_waktu;
        $margin = 0.12;

        $nominal_cicilan_perbulan = ($total_pinjaman + ($total_pinjaman*$margin))/$tenor;

        $total_cicilan = 0;
        for($i=0; $i<$tenor; $i++){
            $total_cicilan += $nominal_cicilan_perbulan;
        }

        return response()->json(['nominal_cicilan_perbulan' => floor($nominal_cicilan_perbulan), 'total_cicilan'=> floor($total_cicilan) ]);
    }

    public function getDataBerandaKpr(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $brw_status = Auth::guard('borrower-api-mobile')->user()->status;

        $brw_rekening = DB::table('brw_rekening')->select('total_plafon', 'total_terpakai', 'total_sisa')->where('brw_id', '=', $brw_id)->first();
        $brw_user_detail = DB::table('brw_user_detail')->select('nama', 'tempat_lahir', 'tgl_lahir', 'alamat', 'ktp')->where('brw_id', '=', $brw_id)->first();
        $status_pengajuan = DB::table('brw_pengajuan')->where('brw_id', '=', $brw_id)->where('status', '=', 1)->exists();

        if($brw_status == 'notfilled' && $status_pengajuan){
            $force_update_data = true;
        }else{
            $force_update_data = false;
        }

        $data_cicilan = [];
        $total_pinjaman_pokok = '';
        $total_margin = '';
        $total_pinjaman = '';
        $total_tagihan_bulan_berjalan = '';
        $total_tagihan_bulan_lalu = '';

        return response()->json([
            'total_pinjaman_pokok' => $total_pinjaman_pokok,
            'total_margin' => $total_margin,
            'total_pinjaman' => $total_pinjaman,
            'total_tagihan_bulan_berjalan' => $total_tagihan_bulan_berjalan,
            'total_tagihan_bulan_lalu' => $total_tagihan_bulan_lalu,
            'brw_user_detail' => $brw_user_detail,
            'data_cicilan' => $data_cicilan,
            'force_update_data' => $force_update_data,
        ]);
    }

    public function getDataPendanaanKpr(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $brw_pengajuan = DB::table('brw_pengajuan')->select('pengajuan_id', 'status', 'pendanaan_tipe', 'pendanaan_tujuan')->where('brw_id', '=', $brw_id)->orderby('pengajuan_id', 'desc')->get();

        if(!isset($brw_pengajuan[0])){
            $data_brw_pengajuan=[];
        }else{
            $x=0;
            foreach($brw_pengajuan as $item){

                if($item->pendanaan_tipe == '2'){
                    $pendanaan_tipe = 'Dana Rumah';
                }else if($item->pendanaan_tipe == '1'){
                    $pendanaan_tipe = 'Dana Konstruksi';
                }else{
                    $pendanaan_tipe = ' ';
                };

                if($item->pendanaan_tujuan  == '1'){
                    $pendanaan_tujuan = 'Pra Kepemilikan Rumah';
                }else if($item->pendanaan_tujuan == '2'){
                    $pendanaan_tujuan = 'Kepemilikan Rumah';
                }else{
                    $pendanaan_tujuan = ' ';
                };

                $pengajuan_id = $item->pengajuan_id;

                if($item->status == '0'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Pengajuan Baru';
                    $color = '#02613C';
                }else if($item->status == '1'){
                    $status = 'Lengkapi Data';
                    $status_isi = 'Lolos tahap 1';
                    $color = '#02613C';
                }else if($item->status == '2'){
                    $status = 'Ditolak';
                    $status_isi = 'Gagal tahap 1';
                    $color = '#db1414';
                }else if($item->status == '5'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Diterima';
                    $color = '#02613C';
                }
                else if($item->status == '3' || $item->status == '11'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Proses Verifikasi';
                    $color = '#02613C';
                }
                else if($item->status == '6'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Ditolak';
                    $color = '#db1414';
                }
                else{
                    $status = '-';
                }

                $data_brw_pengajuan[$x]=[
                    'pengajuan_id' => $pengajuan_id,
                    'nama_pendanaan' => $pengajuan_id.' '.$pendanaan_tipe.' - '.$pendanaan_tujuan,
                    'status_pendanaan' => $status,
                    'status_isi' => $status_isi,
                    'color' => $color,
                ];

                $x++;
            }
        }

        return response()->json([
            'data_brw_pengajuan' => $data_brw_pengajuan,
        ]);
    }

    public function getDataProfileKpr(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $username = Auth::guard('borrower-api-mobile')->user()->username;
        $brw_pengajuan = DB::table('brw_pengajuan')->select('pengajuan_id', 'status', 'pendanaan_tipe', 'pendanaan_tujuan')->where('brw_id', '=', $brw_id)->orderby('pengajuan_id', 'desc')->get();
        $brw_user_detail = DB::table('brw_user_detail')->select('nama', 'brw_pic')->where('brw_id', '=', $brw_id)->first();

        $brw_status = Auth::guard('borrower-api-mobile')->user()->status;
        $status_pengajuan = DB::table('brw_pengajuan')->where('brw_id', '=', $brw_id)->where('status', '=', 1)->exists();
        if($brw_status == 'notfilled' && $status_pengajuan){
            $force_update_data = true;
        }else{
            $force_update_data = false;
        }

        if(!isset($brw_pengajuan[0])){
            $data_brw_pengajuan=[];
        }else{
            $x=0;
            foreach($brw_pengajuan as $item){
                if($item->pendanaan_tipe == '2'){
                    $pendanaan_tipe = 'Dana Rumah';
                }else if($item->pendanaan_tipe == '1'){
                    $pendanaan_tipe = 'Dana Konstruksi';
                }else{
                    $pendanaan_tipe = ' ';
                };

                if($item->pendanaan_tujuan  == '1'){
                    $pendanaan_tujuan = 'Pra Kepemilikan Rumah';
                }else if($item->pendanaan_tujuan == '2'){
                    $pendanaan_tujuan = 'Kepemilikan Rumah';
                }else if($item->pendanaan_tujuan == '15'){
                    $pendanaan_tujuan = 'Dana Konstruksi Rumah Hunian';
                }else{
                    $pendanaan_tujuan = ' ';
                };

                $pengajuan_id = $item->pengajuan_id;

                if($item->status == '0'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Pengajuan Baru';
                    $color = '#02613C';
                }else if($item->status == '1'){
                    $status = 'Lengkapi Data';
                    $status_isi = 'Lolos tahap 1';
                    $color = '#02613C';
                }else if($item->status == '2'){
                    $status = 'Ditolak';
                    $status_isi = 'Gagal tahap 1';
                    $color = '#db1414';
                }else if($item->status == '5'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Diterima';
                    $color = '#02613C';
                }
                else if($item->status == '3' || $item->status == '11'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Proses Verifikasi';
                    $color = '#02613C';
                }
                else if($item->status == '6'){
                    $status = 'Lihat Detail';
                    $status_isi = 'Ditolak';
                    $color = '#db1414';
                }
                else{
                    $status = '-';
                }

                $data_brw_pengajuan[$x]=[
                    'pengajuan_id' => $pengajuan_id,
                    'nama_pendanaan' => $pengajuan_id.' '.$pendanaan_tipe.' - '.$pendanaan_tujuan,
                    'status_pendanaan' => $status,
                    'status_isi' => $status_isi,
                    'color' => $color,
                ];
                $x++;
            }
        }

        if(isset($brw_user_detail->brw_pic)){
            $brw_pic = $this->licenceFileShow($brw_user_detail->brw_pic);
        }else{
            $brw_pic = '';
        };

        return response()->json([
            'brw_id' => $brw_id,
            'brw_nama'=>isset($brw_user_detail->nama) ? $brw_user_detail->nama : '',
            'brw_pic'=>$brw_pic,
            'username'=> $username,
            'data_brw_pengajuan' => $data_brw_pengajuan,
            'force_update_data' => $force_update_data
        ]);

    }

    public function listMenuKpr(){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        //!DATA PRIBADI DAN ALAMAT
        $get_status_isi_user_detail = DB::table('brw_user_detail')->select('status_isi_data_pribadi', 'status_isi_data_alamat', 'status_kawin')->where('brw_id', '=', $brw_id)->first();
        $data = [];

        $status_isi_data_pribadi = $get_status_isi_user_detail->status_isi_data_pribadi == 1 ? 'done' : 'not_done';
        $data_informasi_pribadi = [
            'label' => 'Informasi Pribadi',
            'status' => $status_isi_data_pribadi,
            'disable' => false
        ];
        array_push($data, $data_informasi_pribadi);

        if($status_isi_data_pribadi == 'done'){
            $status_disable_alamat = false;
        }else{
            $status_disable_alamat = true;
        }
        $status_isi_data_alamat = $get_status_isi_user_detail->status_isi_data_alamat == 1 ? 'done' : 'not_done';
        $data_informasi_alamat = [
            'label' => 'Informasi Alamat',
            'status' => $status_isi_data_alamat,
            'disable' => $status_disable_alamat
        ];
        array_push($data, $data_informasi_alamat);
        //!-------------

        //!DATA REKENING
        if($status_isi_data_alamat == 'done'){
            $status_disable_rekening = false;
        }else{
            $status_disable_rekening = true;
        }
        $get_status_isi_brw_rekening = DB::table('brw_rekening')->select('status_isi_data_rekening')->where('brw_id', '=', $brw_id)->first();
        $status_isi_data_rekening = $get_status_isi_brw_rekening->status_isi_data_rekening == 1 ? 'done' : 'not_done';
        $data_rekening = [
            'label' => 'Informasi Rekening',
            'status' => $status_isi_data_rekening,
            'disable' => $status_disable_rekening
        ];
        array_push($data, $data_rekening);
        //!-------------


        //!DATA PEKERJAAN
        if($status_isi_data_rekening == 'done'){
            $status_disable_pekerjaan = false;
        }else{
            $status_disable_pekerjaan = true;
        }
        $get_status_isi_brw_penghasilan = DB::table('brw_user_detail_penghasilan')->select('status_isi_pekerjaan')->where('brw_id2', '=', $brw_id)->first();
        $status_isi_data_pekerjaan = $get_status_isi_brw_penghasilan->status_isi_pekerjaan == 1 ? 'done' : 'not_done';
        $data_pekerjaan = [
            'label' => 'Informasi Pekerjaan',
            'status' => $status_isi_data_pekerjaan,
            'disable' => $status_disable_pekerjaan
        ];
        array_push($data, $data_pekerjaan);
        //!-------------

        //!DATA PASANGAN
        if($get_status_isi_user_detail->status_kawin == 1){ //jika status nya menikah
            $get_status_isi_brw_rekening = DB::table('brw_rekening')->select('status_isi_data_rekening')->where('brw_id', '=', $brw_id)->first();
            if($status_isi_data_rekening == 'done'){
                $status_disable_data_pasangan = false;
            }else{
                $status_disable_data_pasangan = true;
            }
            $data_pasangan = [
                'label' => 'Informasi Pasangan',
                'status' => 'not_done',
                'disable' => $status_disable_data_pasangan
            ];
            array_push($data, $data_pasangan);

            $data_pekerjaan_pasangan = [
                'label' => 'Informasi Pekerjaan Pasangan',
                'status' => 'not_done',
                'disable' => false
            ];
            array_push($data, $data_pekerjaan_pasangan);
        }
        //!-------------

        $data_objek_pendanaan = [
            'label' => 'Informasi Objek Pendanaan',
            'status' => 'not_done',
            'disable' => false
        ];
        array_push($data, $data_objek_pendanaan);

        $data_pengajuan_pendanaan = [
            'label' => 'Data Pengajuan Pendanaan',
            'status' => 'not_done',
            'disable' => false
        ];
        array_push($data, $data_pengajuan_pendanaan);

        // array_push($data, [$data_informasi_pribadi, $data_informasi_alamat, $data_rekening, $data_pasangan, $data_pekerjaan, $data_objek_pendanaan, $data_pengajuan_pendanaan]);

        return ['data'=>$data];
    }

    public function listMenuKprPengajuan(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $data = [];
        $id_pengajuan = $request->id_pengajuan;

        //!DATA PENGAJUAN 1
        $data_objek_pendanaan = [
            'label' => 'Data Pengajuan (1)',
            'status' => 'done',
            'disable' => false
        ];
        array_push($data, $data_objek_pendanaan);

         //!DATA PENGAJUAN 2

         $check_brw_pengajuan = DB::table('brw_pengajuan')->select('status')->where('pengajuan_id', '=', $id_pengajuan)->first();

         if($check_brw_pengajuan->status == 0 || $check_brw_pengajuan->status == 1 || $check_brw_pengajuan->status == 2){
             $status_pengajuan = 'not_done';
         }else{
            $status_pengajuan = 'done';
         }

         $data_objek_pendanaan = [
            'label' => 'Data Pengajuan (2)',
            'status' => $status_pengajuan,
            'disable' => false
        ];
        array_push($data, $data_objek_pendanaan);

         //!DATA DOKUMEN PENDUKUNG
         $data_objek_pendanaan = [
            'label' => 'Dokumen Pendukung',
            'status' => 'not_done',
            'disable' => false
        ];
        array_push($data, $data_objek_pendanaan);

        return ['data'=>$data];
    }

    public function getDataAllRpc(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $select_rpc_user_detail = DB::table('brw_user_detail')->select(
            'brw_id',
            'brw_type',
            'nama',
            'no_tlp',
            'ktp',
            'tempat_lahir',
			'tgl_lahir',
            'npwp',
            'status_kawin',
            'status_rumah',
            'agama',
            'alamat',
            'provinsi',
            'kota',
            'kecamatan',
            'kelurahan',
            'kode_pos',
			DB::raw('(CASE status_rumah
			          WHEN 1 THEN "Rumah Sendiri"
			          WHEN 2 THEN "Sewa"
			          WHEN 3 THEN "Rumah Keluarga"
					  ELSE ""
					  END) AS status_kepemilikan_rumah'),
            'bidang_online',
            'bidang_pekerjaan',
            'pekerjaan',
            'pendidikan_terakhir',
        )->where('brw_id', $brw_id)->first();

        $label_kawin = MasterKawin::select('jenis_kawin')->where('id_kawin', $select_rpc_user_detail->status_kawin)->first();

        $label_pendidikan = MasterPendidikan::select('pendidikan')->where('id_pendidikan', $select_rpc_user_detail->pendidikan_terakhir)->first();

        $label_pekerjaan = MasterPekerjaan::select('pekerjaan')->where('id_pekerjaan', $select_rpc_user_detail->pekerjaan)->first();

        $label_agama = MasterAgama::select('agama')->where('id_agama', $select_rpc_user_detail->agama)->first();

        $label_bidang_pekerjaan = MasterBidangPekerjaan::select('bidang_pekerjaan')->where('id_bidang_pekerjaan', $select_rpc_user_detail->bidang_pekerjaan)->first();

        $label_online = MasterOnline::select('tipe_online')->where('id_online', $select_rpc_user_detail->bidang_online)->first();

        $label_kepemilikan_rumah = DB::table('m_kepemilikan_rumah')->select('kepemilikan_rumah')->where('id_kepemilikan_rumah', $select_rpc_user_detail->status_rumah)->first();

        $select_rpc_penghasilan = DB::table('brw_user_detail_penghasilan')->select(
            'brw_id2',
            'pendapatan_borrower',
            'sumber_pengembalian_dana',
            'nama_perusahaan',
            'masa_kerja_tahun as pengalaman_kerja_tahun',
            'masa_kerja_bulan as pengalaman_kerja_bulan',
            'usia_perusahaan',
            'no_telp',
        )->where('brw_id2', $brw_id)->first();

        return [
            'data_user_detail' => $select_rpc_user_detail,
            'data_penghasilan' => $select_rpc_penghasilan,
            'label_kawin' => $label_kawin,
            'label_pendidikan' => $label_pendidikan,
            'label_pekerjaan' => $label_pekerjaan,
            'label_agama' => $label_agama,
            'label_bidang_pekerjaan' => $label_bidang_pekerjaan,
            'label_online' => $label_online,
            'label_kepemilikan_rumah' => $label_kepemilikan_rumah,
        ];
    }

	# add by ichal_sl, March 04, 2022
	public function getTipePendanaan(Request $request){
		$brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select(
            'tipe_id',
            'pendanaan_nama',
            'pendanaan_keterangan'
        )->first();

		return [
            'tipe_pendanaan' => $tipe_pendanaan
        ];
	}

	# add by ichal_sl, March 04, 2022
	public function getTujuanPendanaan(Request $request){
		$brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select(
            'id',
			'tipe_id',
            'tujuan_pembiayaan',
            'Keterangan'
        )->get();

		return [
            'tujuan_pendanaan' => $tujuan_pendanaan
        ];
	}

	# add by ichal_sl, March 01, 2022
	public function getAllMasterPengajuan(){

        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $master_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama', 'pendanaan_keterangan', 'max_period')->where('pendanaan_nama', 'Dana Rumah')->get();
        $x=0;
        if (!isset($master_tipe_pendanaan[0])) {
            $master_tipe_pendanaan = null;
        }
        else {
                foreach ($master_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
					'pendanaan_keterangan'=>$item->pendanaan_keterangan,
					'max_period'=>$item->max_period
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan', 'keterangan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan,
					'keterangan'=>$item->keterangan
                ];
                $x++;
            }
        }

		/*$tenor = DB::select(DB::raw('SELECT 24 tenor,
		                                    "Bulan" tenor_waktu,
											"Dana Konstruksi" tenor_keterangan
                                     UNION
							         SELECT 180,
									        "Bulan" tenor_waktu,
											"Dana Rumah"') );*/

		return [
            'data_tipe_pendanaan' => $data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan
			//'tenor_pendanaan' => $tenor
        ];
    }

	# add by ichal_sl, March 04, 2022
	public function getAllMasterFormRpc(){

        $master_kawin = MasterKawin::all();
        $x=0;
        if (!isset($master_kawin[0])) {
            $master_kawin = null;
        }else {
                foreach ($master_kawin as $item){
                $data_kawin[$x] = [
                    'id_kawin'=>$item->id_kawin,
                    'jenis_kawin'=>$item->jenis_kawin,

                ];
                $x++;
            }
        }

        $master_pendidikan = MasterPendidikan::all();
        $x=0;
        if (!isset($master_pendidikan[0])) {
            $master_pendidikan = null;
        }else {
                foreach ($master_pendidikan as $item){
                $data_pendidikan[$x] = [
                    'id_pendidikan'=>$item->id_pendidikan,
                    'pendidikan'=>$item->pendidikan,

                ];
                $x++;
            }
        }

        $master_pekerjaan = MasterPekerjaan::whereNotIn('id_pekerjaan', [6,7,8])->get();
        $x=0;
        if (!isset($master_pekerjaan[0])) {
            $master_pekerjaan = null;
        }else {
                foreach ($master_pekerjaan as $item){
                $data_pekerjaan[$x] = [
                    'id_pekerjaan'=>$item->id_pekerjaan,
                    'pekerjaan'=>$item->pekerjaan,

                ];
                $x++;
            }
        }

        $master_agama = MasterAgama::all();
        $x=0;
        if (!isset($master_agama[0])) {
            $master_agama = null;
        }else {
                foreach ($master_agama as $item){
                $data_agama[$x] = [
                    'id_agama'=>$item->id_agama,
                    'agama'=>$item->agama,
                ];
                $x++;
            }
        }

        $master_bidang_pekerjaan = MasterBidangPekerjaan::whereNotIn('kode_bidang_pekerjaan', ['e00', 'e01'])->get();
        $x=0;
        if (!isset($master_bidang_pekerjaan[0])) {
            $master_bidang_pekerjaan = null;
        }else {
                foreach ($master_bidang_pekerjaan as $item){
                $data_bidang_pekerjaan[$x] = [
                    'id_bidang_pekerjaan'=>$item->id_bidang_pekerjaan,
                    'kode_bidang_pekerjaan'=>$item->kode_bidang_pekerjaan,
                    'bidang_pekerjaan'=>$item->bidang_pekerjaan,
                ];
                $x++;
            }
        }

        $master_online = MasterOnline::all();
        $x=0;
        if (!isset($master_online[0])) {
            $master_online = null;
        }else {
                foreach ($master_online as $item){
                $data_online[$x] = [
                    'id_online'=>$item->id_online,
                    'tipe_online'=>$item->tipe_online,
                ];
                $x++;
            }
        }

        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        $data_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->whereIn('tipe_id', [2])->get(); //Dana Konstruksi, Dana Rumah
        $x=0;
        if (!isset($data_tipe_pendanaan[0])) {
            $data_tipe_pendanaan = null;
        }
        else {
                foreach ($data_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan
                ];
                $x++;
            }
        }

        $master_bentuk_badan_usaha = DB::table('m_bentuk_badan_usaha')->select('id', 'bentuk_badan_usaha')->get();
        $x=0;
        if (!isset($master_bentuk_badan_usaha[0])) {
            $master_bentuk_badan_usaha = null;
        }
        else{
                foreach ($master_bentuk_badan_usaha as $item){
                $data_master_bentuk_badan_usaha[$x] = [
                    'id'=>$item->id,
                    'bentuk_badan_usaha'=>$item->bentuk_badan_usaha
                ];
                $x++;
            }
        }

        $get_kepemilikan_rumah = DB::table('m_kepemilikan_rumah')->select('id_kepemilikan_rumah', 'kepemilikan_rumah')->get();
        $x=0;
        if (!isset($get_kepemilikan_rumah[0])) {
            $data_kepemilikan_rumah = [];
        }
        else {
                foreach ($get_kepemilikan_rumah as $item){
                $data_kepemilikan_rumah[$x] = [
                    'id'=>$item->id_kepemilikan_rumah,
                    'kepemilikan_rumah'=>$item->kepemilikan_rumah,
                ];
                $x++;
            }
        }

        $data_jenis_rumah=array();
        $data_1 = [
            'id'=> '1', 'jenis_properti'=>'Rumah Baru',
        ];
        array_push($data_jenis_rumah, $data_1);

        $data_2 = [
            'id'=> '2', 'jenis_properti'=>'Rumah Bekas'
        ];
        array_push($data_jenis_rumah, $data_2);

        return [
            'data_agama' => $data_agama,
            'data_kawin' => $data_kawin,
            'data_pendidikan' => $data_pendidikan,
            'data_pekerjaan' => $data_pekerjaan,
            'data_bidang_pekerjaan' => $data_bidang_pekerjaan,
            'data_online' => $data_online,
            #'data_provinsi' => $data_master_provinsi,
            'data_tipe_pendanaan' =>$data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan,
            'data_master_bentuk_badan_usaha' => $data_master_bentuk_badan_usaha,
            'data_kepemilikan_rumah' => $data_kepemilikan_rumah,
            'data_jenis_rumah'=>$data_jenis_rumah
        ];
    }

	# add by ichal_sl, March 01, 2022
	public function getDataUserDetail(Request $request){
		$brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $select_rpc_user_detail = DB::table('brw_user_detail')->select(
            'brw_id',
            'brw_type',
            'nama',
            'no_tlp',
            'ktp',
            'tempat_lahir',
            'npwp',
            'status_kawin',
            'status_rumah',
            'agama',
            'alamat',
            'provinsi',
            'kota',
            'kecamatan',
            'kelurahan',
            'kode_pos',
            'bidang_online',
            'bidang_pekerjaan',
            'pekerjaan',
            'pendidikan_terakhir',
        )->where('brw_id', $brw_id)->first();

		return [
            'data_user_detail' => $select_rpc_user_detail
        ];
	}

	# add by ichal_sl, March 01, 2022
	public function getDataPenghasilan(Request $request){
		$brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

		$select_rpc_penghasilan = DB::table('brw_user_detail_penghasilan')->select(
            'brw_id2',
            'pendapatan_borrower',
            'sumber_pengembalian_dana',
            'nama_perusahaan',
            'masa_kerja_tahun as pengalaman_kerja_tahun',
            'masa_kerja_bulan as pengalaman_kerja_bulan',
            'usia_tempat_usaha',
            'no_telp',
        )->where('brw_id2', $brw_id)->first();

		return [
            'data_penghasilan' => $select_rpc_penghasilan
        ];
	}

	# add by ichal_sl, March 01, 2022
	public function getLabelKawin(Request $request){
		$brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

		$select_rpc_user_detail = DB::table('brw_user_detail')->select(
            'status_kawin',
        )->where('brw_id', $brw_id)->first();

		$label_kawin = MasterKawin::select('jenis_kawin')->where('id_kawin', $select_rpc_user_detail->status_kawin)->first();

		return [
            'label_kawin' => $label_kawin
        ];
	}

    public function getDataAllRpcAndPengajuan(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $id_pengajuan = $request->id_pengajuan;

        $select_rpc_user_detail = DB::table('brw_user_detail')->select(
            'brw_id',
            'brw_type',
            'nama',
            'no_tlp',
            'ktp',
            'npwp',
            'status_kawin',
            'status_rumah',
            'agama',
            'alamat',
            'provinsi',
            'kota',
            'kecamatan',
            'kelurahan',
            'kode_pos',
            'bidang_online',
            'bidang_pekerjaan',
            'pekerjaan',
            'pendidikan_terakhir',
        )->where('brw_id', $brw_id)->first();

        $select_rpc_user_detail = DB::table('brw_user_detail')->select(
            'brw_id',
            'pendanaan_tipe',
            'pendanaan_tujuan',
            'jenis_properti',
            'harga_objek_pendanaan',
            'durasi_proyek',
            'pendanaan_dana_dibutuhkan',
            'uang_muka',
            'status',
        )->where('brw_id', $brw_id)->where('pengajuan_id', $id_pengajuan)->first();

        $label_kawin = MasterKawin::select('jenis_kawin')->where('id_kawin', $select_rpc_user_detail->status_kawin)->first();

        $label_pendidikan = MasterPendidikan::select('pendidikan')->where('id_pendidikan', $select_rpc_user_detail->pendidikan_terakhir)->first();

        $label_pekerjaan = MasterPekerjaan::select('pekerjaan')->where('id_pekerjaan', $select_rpc_user_detail->pekerjaan)->first();

        $label_agama = MasterAgama::select('agama')->where('id_agama', $select_rpc_user_detail->agama)->first();

        $label_bidang_pekerjaan = MasterBidangPekerjaan::select('bidang_pekerjaan')->where('id_bidang_pekerjaan', $select_rpc_user_detail->bidang_pekerjaan)->first();

        $label_online = MasterOnline::select('tipe_online')->where('id_online', $select_rpc_user_detail->bidang_online)->first();

        $label_kepemilikan_rumah = DB::table('m_kepemilikan_rumah')->select('kepemilikan_rumah')->where('id_kepemilikan_rumah', $select_rpc_user_detail->status_rumah)->first();

        $select_rpc_penghasilan = DB::table('brw_user_detail_penghasilan')->select(
            'brw_id2',
            'pendapatan_borrower',
            'sumber_pengembalian_dana',
            'nama_perusahaan',
            'masa_kerja_tahun as pengalaman_kerja_tahun',
            'masa_kerja_bulan as pengalaman_kerja_bulan',
            'usia_tempat_usaha',
            'no_telp',
        )->where('brw_id2', $brw_id)->first();

        return [
            'data_user_detail' => $select_rpc_user_detail,
            'data_penghasilan' => $select_rpc_penghasilan,
            'label_kawin' => $label_kawin,
            'label_pendidikan' => $label_pendidikan,
            'label_pekerjaan' => $label_pekerjaan,
            'label_agama' => $label_agama,
            'label_bidang_pekerjaan' => $label_bidang_pekerjaan,
            'label_online' => $label_online,
            'label_kepemilikan_rumah' => $label_kepemilikan_rumah,
        ];
    }

    public function getDataBiodataPribadi(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $master_agama = MasterAgama::all();
        $x=0;
        if (!isset($master_agama[0])) {
            $master_agama = [];
        }
        else {
                foreach ($master_agama as $item){
                $data_agama[$x] = [
                    'id_agama'=>$item->id_agama,
                    'agama'=>$item->agama,
                ];
                $x++;
            }
        }

        $master_pendidikan = MasterPendidikan::all();
        $x=0;
        if (!isset($master_pendidikan[0])) {
            $master_pendidikan = [];
        }else {
                foreach ($master_pendidikan as $item){
                $data_pendidikan[$x] = [
                    'id_pendidikan'=>$item->id_pendidikan,
                    'pendidikan'=>$item->pendidikan,

                ];
                $x++;
            }
        }

        $data_tipe_pendanaan = DB::table('m_kawin')->select('id_kawin', 'jenis_kawin')->get();
        $x=0;
        if (!isset($data_tipe_pendanaan[0])) {
            $data_kawin = [];
        }
        else {
                foreach ($data_tipe_pendanaan as $item){
                $data_kawin[$x] = [
                    'id'=>$item->id_kawin,
                    'jenis_kawin'=>$item->jenis_kawin,
                ];
                $x++;
            }
        }

        $get_exist_data_pribadi = DB::table('brw_user_detail')->select('nama', 'jns_kelamin', 'ktp', 'tempat_lahir', 'tgl_lahir', 'no_tlp', 'agama', 'pendidikan_terakhir', 'status_kawin', 'npwp', 'nm_ibu', 'status_kawin')->where('brw_id', '=', $brw_id)->first();

        return [
            'data_agama' => $data_agama,
            'data_pendidikan' => $data_pendidikan,
            'data_kawin' => $data_kawin,
            'data_exist_pribadi' => $get_exist_data_pribadi
        ];
    }

    public function getDataBiodataAlamat(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $get_exist_data_alamat = DB::table('brw_user_detail')->select('status_rumah', 'alamat', 'provinsi', 'kota', 'kecamatan', 'kelurahan', 'kode_pos', 'domisili_status_rumah', 'domisili_alamat', 'domisili_provinsi', 'domisili_kota', 'domisili_kecamatan', 'domisili_kelurahan', 'domisili_kd_pos')->where('brw_id', '=', $brw_id)->first();

        return [
            'get_exist_data_alamat' => $get_exist_data_alamat
        ];
    }

    public function getDataRekening(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $get_exist_data_alamat = DB::table('brw_rekening')->select('brw_norek', 'brw_nm_pemilik', 'brw_kd_bank', 'kantor_cabang_pembuka')->where('brw_id', '=', $brw_id)->first();

        $master_bank = MasterBank::all();
        $x=0;
        if (!isset($master_bank[0])) {
            $master_bank = null;
        }
        else {
                foreach ($master_bank as $item){
                $data_bank[$x] = [
                    'kode'=>$item->kode_bank,
                    'nama_bank'=>$item->nama_bank,
                ];
                $x++;
            }
        }

        return [
            'get_exist_data_alamat' => $get_exist_data_alamat,
            'data_bank' => $data_bank
        ];
    }

    public function getDataPekerjaan(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $get_exist_data_pekerjaan = DB::table('brw_user_detail_penghasilan')->select('pendapatan_borrower', 'sumber_pengembalian_dana', 'nama_perusahaan', 'masa_kerja_tahun as pengalaman_kerja_tahun', 'masa_kerja_bulan as pengalaman_kerja_bulan', 'no_telp', 'usia_perusahaan')->where('brw_id2', '=', $brw_id)->first();
        $get_exist_data_pekerjaan2 = DB::table('brw_user_detail')->select('status_kawin', 'bidang_online', 'bidang_pekerjaan', 'pekerjaan')->where('brw_id', '=', $brw_id)->first();

        $master_pekerjaan = MasterPekerjaan::whereNotIn('id_pekerjaan', [6,7,8])->get();
        $x=0;
        if (!isset($master_pekerjaan[0])) {
            $master_pekerjaan = null;
        }
        else {
                foreach ($master_pekerjaan as $item){
                $data_pekerjaan[$x] = [
                    'id_pekerjaan'=>$item->id_pekerjaan,
                    'pekerjaan'=>$item->pekerjaan,

                ];
                $x++;
            }
        }

        $master_bidang_pekerjaan = MasterBidangPekerjaan::whereNotIn('kode_bidang_pekerjaan', ['e00', 'e01'])->get();
        $x=0;
        if (!isset($master_bidang_pekerjaan[0])) {
            $master_bidang_pekerjaan = null;
        }
        else {
                foreach ($master_bidang_pekerjaan as $item){
                $data_bidang_pekerjaan[$x] = [
                    'id_bidang_pekerjaan'=>$item->id_bidang_pekerjaan,
                    'kode_bidang_pekerjaan'=>$item->kode_bidang_pekerjaan,
                    'bidang_pekerjaan'=>$item->bidang_pekerjaan,
                ];
                $x++;
            }
        }

        $master_online = MasterOnline::all();
        $x=0;
        if (!isset($master_online[0])) {
            $master_online = null;
        }
        else {
                foreach ($master_online as $item){
                $data_online[$x] = [
                    'id_online'=>$item->id_online,
                    'tipe_online'=>$item->tipe_online,
                ];
                $x++;
            }
        }

        $master_bentuk_badan_usaha = DB::table('m_bentuk_badan_usaha')->select('id', 'bentuk_badan_usaha')->get();
        $x=0;
        if (!isset($master_bentuk_badan_usaha[0])) {
            $master_bentuk_badan_usaha = null;
        }
        else{
                foreach ($master_bentuk_badan_usaha as $item){
                $data_master_bentuk_badan_usaha[$x] = [
                    'id'=>$item->id,
                    'bentuk_badan_usaha'=>$item->bentuk_badan_usaha
                ];
                $x++;
            }
        }

        return [
            'get_exist_data_pekerjaan' => $get_exist_data_pekerjaan,
            'get_exist_data_pekerjaan_user_detail' => $get_exist_data_pekerjaan2,
            'data_pekerjaan' => $data_pekerjaan,
            'data_bidang_pekerjaan' => $data_bidang_pekerjaan,
            'data_online' => $data_online,
            'data_master_bentuk_badan_usaha' => $data_master_bentuk_badan_usaha,
        ];
    }

    public function getDataAjukanPendanaanRpc(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $get_pengajuan = DB::table('brw_pengajuan')->select(
            'brw_id',
            'pendanaan_tipe',
            'pendanaan_tujuan',
            'jenis_properti',
            'harga_objek_pendanaan',
            'durasi_proyek',
            'pendanaan_dana_dibutuhkan',
            'uang_muka',
            'status',
            )->where('brw_id', '=', $brw_id)->where('pengajuan_id', '=', $request->id_pengajuan)->first();

        return [
            'get_pengajuan' => $get_pengajuan
        ];
    }

    public function getKantorPenerbit(){

        $get_kantor_penerbit = DB::table('m_kantor_bpn')->select('id', 'nama_kantor')->get();

        return [
            'data_kantor_penerbit' => $get_kantor_penerbit
        ];
    }

    public function getDataPengajuan2(){

        $jumlah_rumah = 3;
        $jumlah_fasilitas = 5;
        $sertifikat_halaman1 = asset('img/Sertifikat_Halaman_1.png');
        $sertifikat_halaman2 = asset('img/Sertifikat_Halaman_2.png');

        return [
            'jumlah_rumah' => $jumlah_rumah,
            'jumlah_fasilitas' => $jumlah_fasilitas,
            'sertifikat_halaman1' => $sertifikat_halaman1,
            'sertifikat_halaman2' => $sertifikat_halaman2,
        ];
    }

    public function getDataPengajuan2Page3(){

        $sertifikat_halaman1 = asset('img/Sertifikat_Halaman_1.png');
        $sertifikat_halaman2 = asset('img/Sertifikat_Halaman_2.png');

        return [
            'sertifikat_halaman1' => $sertifikat_halaman1,
            'sertifikat_halaman2' => $sertifikat_halaman2,
        ];
    }

    public function getDataFotoRpc(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $select_rpc_user_detail = DB::table('brw_user_detail')->select(
            'brw_pic',
            'brw_pic_ktp',
            'brw_pic_user_ktp',
            'brw_pic_npwp',
        )->where('brw_id', $brw_id)->first();

        if(isset($select_rpc_user_detail->brw_pic)){
            $brw_pic = $this->licenceFileShow($select_rpc_user_detail->brw_pic);
        }else{
            $brw_pic = '';
        };

        if(isset($select_rpc_user_detail->brw_pic_ktp)){
            $brw_pic_ktp = $this->licenceFileShow($select_rpc_user_detail->brw_pic_ktp);
        }else{
            $brw_pic_ktp = '';
        };

        if(isset($select_rpc_user_detail->brw_pic_user_ktp)){
            $brw_pic_user_ktp = $this->licenceFileShow($select_rpc_user_detail->brw_pic_user_ktp);
        }else{
            $brw_pic_user_ktp = '';
        };

        if(isset($select_rpc_user_detail->brw_pic_npwp)){
            $brw_pic_npwp = $this->licenceFileShow($select_rpc_user_detail->brw_pic_npwp);
        }else{
            $brw_pic_npwp = '';
        };

        return [
            'brw_pic' => $brw_pic,
            'brw_pic_ktp' => $brw_pic_ktp,
            'brw_pic_user_ktp' => $brw_pic_user_ktp,
            'brw_pic_npwp' => $brw_pic_npwp,
        ];
    }

    public function getDataFotoDokumenObjekPendanaan(Request $request) {
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $id_pengajuan = $request->id_pengajuan;

        $select_brw_dokumen_objek_pendanaan = DB::table('brw_dokumen_objek_pendanaan')->where('id_pengajuan', $id_pengajuan)->first();

        if(isset($select_brw_dokumen_objek_pendanaan->surat_pemesanan_rumah)){
            $surat_pemesanan_rumah = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->surat_pemesanan_rumah);
        }else{
            $surat_pemesanan_rumah = '';
        };

        if(isset($select_brw_dokumen_objek_pendanaan->daftar_harga_perumahan)){
            $daftar_harga_perumahan = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->daftar_harga_perumahan);
        }else{
            $daftar_harga_perumahan = '';
        };

        if(isset($select_brw_dokumen_objek_pendanaan->gambar_objek_pendanaan)){
            $gambar_objek_pendanaan = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->gambar_objek_pendanaan);
        }else{
            $gambar_objek_pendanaan = '';
        };

        if(isset($select_brw_dokumen_objek_pendanaan->sertifikat)){
            $sertifikat = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->sertifikat);
        }else{
            $sertifikat = '';
        };

        if(isset($select_brw_dokumen_objek_pendanaan->imb)){
            $imb = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->imb);
        }else{
            $imb = '';
        };

        if(isset($select_brw_dokumen_objek_pendanaan->pbb)){
            $pbb = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->pbb);
        }else{
            $pbb = '';
        };

        if(isset($select_brw_dokumen_objek_pendanaan->rab)){
            $rab = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->rab);
        }else{
            $rab = '';
        };

        if(isset($select_brw_dokumen_objek_pendanaan->ktp_pemilik)){
            $ktp_pemilik = $this->licenceFileShow($select_brw_dokumen_objek_pendanaan->ktp_pemilik);
        }else{
            $ktp_pemilik = '';
        };

        $data_page_1 = DB::table('brw_pengajuan')->select('setuju_verifikator_dokumen')->where('pengajuan_id', '=', $id_pengajuan)->first();

        return [
            'surat_pemesanan_rumah' => $surat_pemesanan_rumah,
            'daftar_harga_perumahan' => $daftar_harga_perumahan,
            'gambar_objek_pendanaan' => $gambar_objek_pendanaan,
            'sertifikat' => $sertifikat,
            'imb' => $imb,
            'pbb' => $pbb,
            'rab' => $rab,
            'ktp_pemilik' => $ktp_pemilik,
            'status_verifikator_dokumen' => $data_page_1->setuju_verifikator_dokumen
        ];
    }

    public function getDataExistPengajuan2Page1(Request $request){

        $pengajuan_id = $request->id_pengajuan;

        $data_page_1 = DB::table('brw_pengajuan')->select(
            'lokasi_proyek',
            'provinsi',
            'kota',
            'kecamatan',
            'kelurahan',
            'kode_pos',
            'nm_pemilik',
            'no_tlp_pemilik',
            'alamat_pemilik',
            'provinsi_pemilik',
            'kota_pemilik',
            'kecamatan_pemilik',
            'kelurahan_pemilik',
            'kd_pos_pemilik',
            'status',
            'setuju_verifikator_pengajuan')->where('pengajuan_id', '=', $pengajuan_id)->first();
        return [
            'data_page_1' => $data_page_1
        ];
    }

    public function getDataExistPengajuan2Page2(Request $request){

        $pengajuan_id = $request->id_pengajuan;

        $data_page_2_rumah_lain = DB::table('brw_pendanaan_rumah_lain')->select(
            'rumah_ke',
            'status',
            'bank',
            'plafond',
            'jangka_waktu',
            'outstanding',
            'angsuran')->where('pengajuan_id', '=', $pengajuan_id)->get();

        $data_page_2_pembiayaan_lain = DB::table('brw_pendanaan_non_rumah_lain')->select(
            'bank',
            'plafond',
            'jangka_waktu',
            'outstanding',
            'angsuran')->where('pengajuan_id', '=', $pengajuan_id)->get();

        return [
            'data_page_2_rumah_lain' => $data_page_2_rumah_lain,
            'data_page_2_pembiayaan_lain' => $data_page_2_pembiayaan_lain,
        ];
    }

    public function getDataExistPengajuan2Page3(Request $request){
        $pengajuan_id = $request->id_pengajuan;

        $data_page_3 = DB::table('brw_agunan')->select(
            'id_pengajuan',
            'jenis_agunan',
            'nomor_agunan',
            'atas_nama_agunan',
            'luas_bangunan',
            'luas_tanah',
            'nomor_surat_ukur',
            'tanggal_surat_ukur',
            'tanggal_terbit',
            'tanggal_jatuh_tempo',
            'kantor_penerbit',
            'blok_nomor',
            'RT',
            'RW',
            'no_imb',
            'tanggal_terbit_imb',
            'updated_at')->where('id_pengajuan', '=', $pengajuan_id)->first();

        return [
            'data_page_3' => $data_page_3
        ];
    }

    public function getDataFotoDokumenLegalitasPribadi(Request $request) {
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $id_pengajuan = $request->id_pengajuan;
        $dokumen_untuk = $request->dokumen_untuk;

        $select_brw_dokumen_legalitas_pribadi = DB::table('brw_dokumen_legalitas_pribadi')->where('brw_id', $brw_id)->first();
        $select_brw_user_detail = DB::table('brw_user_detail')->select('brw_pic_ktp', 'brw_pic_npwp', 'status_kawin')->where('brw_id', $brw_id)->first();

        //!DOKUMEN LEGALITAS PRIBADI
        if($dokumen_untuk == 'legalitas_pribadi'){

            if(isset($select_brw_user_detail->brw_pic_ktp)){
                $brw_pic_ktp = $this->licenceFileShow($select_brw_user_detail->brw_pic_ktp);
            }else{
                $brw_pic_ktp = '';
            };

            if(isset($select_brw_user_detail->brw_pic_npwp)){
                $brw_pic_npwp = $this->licenceFileShow($select_brw_user_detail->brw_pic_npwp);
            }else{
                $brw_pic_npwp = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->kartu_keluarga)){
                $kartu_keluarga = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->kartu_keluarga);
            }else{
                $kartu_keluarga = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->surat_keterangan_belum_menikah)){
                $surat_keterangan_belum_menikah = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->surat_keterangan_belum_menikah);
            }else{
                $surat_keterangan_belum_menikah = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->surat_domisili)){
                $surat_domisili = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->surat_domisili);
            }else{
                $surat_domisili = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->surat_beda_nama)){
                $surat_beda_nama = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->surat_beda_nama);
            }else{
                $surat_beda_nama = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->perjanjian_pra_nikah)){
                $perjanjian_pra_nikah = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->perjanjian_pra_nikah);
            }else{
                $perjanjian_pra_nikah = '';
            };

            return [
                'brw_pic_ktp' => $brw_pic_ktp,
                'brw_pic_npwp' => $brw_pic_npwp,
                'kartu_keluarga' => $kartu_keluarga,
                'surat_keterangan_belum_menikah' => $surat_keterangan_belum_menikah,
                'surat_domisili' => $surat_domisili,
                'surat_beda_nama' => $surat_beda_nama,
                'perjanjian_pra_nikah' => $perjanjian_pra_nikah,
                'status_kawin' => $select_brw_user_detail->status_kawin,
            ];
        }
        //!DOKUMEN PENGHASILAN
        else if($dokumen_untuk == 'dokumen_penghasilan'){
            $select_skema_pembiayaan = DB::table('brw_user_detail_penghasilan')->select('skema_pembiayaan')->where('brw_id2', $brw_id)->first();

            if(isset($select_brw_dokumen_legalitas_pribadi->surat_bekerja)){
                $surat_bekerja = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->surat_bekerja);
            }else{
                $surat_bekerja = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->surat_pengalaman_kerja)){
                $surat_pengalaman_kerja = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->surat_pengalaman_kerja);
            }else{
                $surat_pengalaman_kerja = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->slip_gaji)){
                $slip_gaji = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->slip_gaji);
            }else{
                $slip_gaji = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->mutasi_rekening)){
                $mutasi_rekening = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->mutasi_rekening);
            }else{
                $mutasi_rekening = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->spt)){
                $spt = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->spt);
            }else{
                $spt = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->dokumen_pendukung)){
                $dokumen_pendukung = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->dokumen_pendukung);
            }else{
                $dokumen_pendukung = '';
            };

            return [
                'surat_bekerja' => $surat_bekerja,
                'surat_pengalaman_kerja' => $surat_pengalaman_kerja,
                'slip_gaji' => $slip_gaji,
                'mutasi_rekening' => $mutasi_rekening,
                'spt' => $spt,
                'dokumen_pendukung' => $dokumen_pendukung,
                'skema_pembiayaan' => $select_skema_pembiayaan->skema_pembiayaan,
            ];
        }
        //!DOKUMEN PENGHASILAN PASANGAN
        else if($dokumen_untuk == 'dokumen_penghasilan_pasangan'){

            if(isset($select_brw_dokumen_legalitas_pribadi->surat_bekerja_pasangan)){
                $surat_bekerja_pasangan = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->surat_bekerja_pasangan);
            }else{
                $surat_bekerja_pasangan = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->surat_pengalaman_kerja_pasangan)){
                $surat_pengalaman_kerja_pasangan = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->surat_pengalaman_kerja_pasangan);
            }else{
                $surat_pengalaman_kerja_pasangan = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->slip_gaji_pasangan)){
                $slip_gaji_pasangan = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->slip_gaji_pasangan);
            }else{
                $slip_gaji_pasangan = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->mutasi_rekening_pasangan)){
                $mutasi_rekening_pasangan = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->mutasi_rekening_pasangan);
            }else{
                $mutasi_rekening_pasangan = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->spt_pasangan)){
                $spt_pasangan = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->spt_pasangan);
            }else{
                $spt_pasangan = '';
            };

            if(isset($select_brw_dokumen_legalitas_pribadi->dokumen_pendukung_pasangan)){
                $dokumen_pendukung_pasangan = $this->licenceFileShow($select_brw_dokumen_legalitas_pribadi->dokumen_pendukung_pasangan);
            }else{
                $dokumen_pendukung_pasangan = '';
            };

            return [
                'surat_bekerja_pasangan' => $surat_bekerja_pasangan,
                'surat_pengalaman_kerja_pasangan' => $surat_pengalaman_kerja_pasangan,
                'slip_gaji_pasangan' => $slip_gaji_pasangan,
                'mutasi_rekening_pasangan' => $mutasi_rekening_pasangan,
                'spt_pasangan' => $spt_pasangan,
                'dokumen_pendukung_pasangan' => $dokumen_pendukung_pasangan,
            ];
        }

    }

    public function licenceFileShow($file)
    {
        /**
         *Make sure the @param $file has a dot
        * Then check if the user has Admin Role. If true serve else
        */
        if (strpos($file, '.') !== false) {
            if (Auth::check()) {
                /** Serve the file for the Admin*/
                return $this->returnFile($file);
            } else {
                /**Logic to check if the request is from file owner**/
                return $this->returnFile($file);
            }
        } else {
            //Invalid file name given
            return false;
        }
    }

    public function returnFile($file)
    {
        //This method will look for the file and get it from drive
        $path = storage_path('app/private/' . $file);
        try {
            $file = File::get($path);
            $type = File::mimeType($path);
            $response = Response::make("data:$type;base64,".base64_encode(file_get_contents($path)), 200);

            $response->header("Content-Type", $type);
            return $response;
        } catch (\Exception $exception) {
            // $insertLog = DB::table("log_db_app")->insert(array(
			// 	"file_name" => "KprGetDataController.php",
			// 	"line" => "1348",
            //     'description' => "Gagal mengambil file foto $file",
			// 	"created_at" => date("Y-m-d H:i:s")
			// ));
            return false;
        }
    }

}

