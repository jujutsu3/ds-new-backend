<?php
namespace App\Http\Controllers\Mobile\Kpr;

use App\Services\BorrowerService;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
// use App\Http\Controllers\Auth\LoginController;

// use Illuminate\Foundation\Auth\ThrottlesLogins;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Foundation\Auth\ThrottlesLogins;

use Illuminate\Http\Request;
use JWTAuth;
use JWTFactory;
use Hash;
use Carbon\Carbon;
use Mail;
use Image;
use GuzzleHttp\Client;
use DB;
use Exception;
use Storage;

use App\Jobs\ProcessEmail;
use App\Jobs\InvestorVerif;
use App\Mail\EmailAktifasiKpr;

use App\LoginBorrower;
use App\BorrowerDetails;
use App\AhliWarisInvestor;
use App\TermCondition;

use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;

use App\Token;
use App\Helpers\Helper;

class KprController_Backup extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */

     use AuthenticatesUsers;
    // use ThrottlesLogins;

    public $maxAttempts = 2;

    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['except' => ['login', 'checkToken', 'resendEmail', 'validateOTP', 'newCheckToken', 'newnewCheckToken', 'verificationCode', 'verificationOtp', 'checkVersion', 'checkPhoneNumber', 'getTermCondition', 'register_new_new_new', 'getProvinsi', 'getKota', 'getKecamatan', 'getKelurahan', 'getKodePos', 'getProvinsiPendanaan', 'getKotaPendanaan', 'getKecamatanPendanaan', 'getKelurahanPendanaan', 'masterSimulasiPengajuan', 'getResultSimulationKepemilikan', 'getResultSimulationPraKepemilikan']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $temp_username=trim($request->username);

        if(is_numeric($temp_username))
		{
			$hp_length = strlen($temp_username);
			if ( 9 > $hp_length )
				return response()->json(['error'=> 'Panjang minimal No HP 9 Digit']);
			elseif ( 13 < $hp_length )
				return response()->json(['error'=> 'Panjang maksimal No HP 13 Digit']);

			if( "62" == substr($temp_username,0,2) )
			{
                $no_hp = substr($temp_username, 2);
				$ilength = 2;
            }elseif( "0" == substr($temp_username,0,1) )
			{
                $no_hp = substr($temp_username, 1);
				$ilength = 1;
            }
			else
			{
				$no_hp = $temp_username;
				$ilength = 0;
			}

            // $data = DB::table('brw_user_detail')->join('brw_user', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')->select('username')->where('brw_user_detail.no_tlp', 'like', '%'.$no_hp.'%')->first();
            $data = DB::SELECT("SELECT a.brw_id , a.username , a.status, b.brw_type FROM brw_user a, brw_user_detail b WHERE a.brw_id = b.brw_id AND right(trim(b.no_tlp),$hp_length - $ilength) = $no_hp limit 1");
			if($data)
			{
                //[11/05/2022] cek if borrower <> individu. return error messsage
                if($data[0]->brw_type === 2) {
                    if(!$request->has('channel') && $request->channel  !== 'web'){
                        return response()->json(['error'=> 'Untuk login user badan hukum, harap login melalui web danasyariah']);
                    }
                }

                $username = $data[0]->username;
				$status_user = $data[0]->status;
				$borrower_id = $data[0]->brw_id ;

            }else{
				$data = DB::table('brw_user')->select('status','brw_id')->where('brw_user.username', '=', $temp_username)->first();


                if ($data === null && $no_hp[0] === '8')  {
                    $contact = \Illuminate\Support\Facades\DB::table('brw_corp_contact')
                        ->whereRaw("no_tlp like ?", ["%$no_hp"])
                        ->first();
                    if ($contact !== null) {
                        $borrower = BorrowerService::getBorrower($contact->brw_id);
                        $allContacts = $borrower->contacts()->oldest()->get();

                        if (
                            isset($allContacts[0])
                            && $allContacts[0] !== null
                            && ($allContacts[0]->no_tlp === $no_hp || str_contains($allContacts[0]->no_tlp, $no_hp))
                        ) {
                            $isPhone = true;
                            $data = $borrower;
                        }
                    }
                }

                if($data)
                {
                    if ($isPhone ?? false) {
                        $username = $data['username'];
                    } else {
                        $username = $temp_username;
                    }
                    $status_user = $data ? $data->status : '';
                    $borrower_id = $data->brw_id ;
                }
                else return response()->json(['error'=> 'No Hp Tidak Terdaftar']);
            }
        }
		elseif (strpos($temp_username, '@'))
		{
            $data = DB::table('brw_user')->select('username', 'status', 'brw_id')->where('brw_user.email', '=', $temp_username)->first();
            if($data){

                $detil_borrower = BorrowerDetails::query()->where('brw_id', $data->brw_id)->first();
                if($detil_borrower){
                    if($detil_borrower->brw_type ===  2) {
                        if(!$request->has('channel') && $request->channel  !== 'web'){
                            return response()->json(['error'=> 'Untuk login user badan hukum, harap login melalui web danasyariah']);
                        }
                    }
                }

                $username = $data->username;
                $status_user = $data->status;
                $borrower_id = $data->brw_id;

            }else return response()->json(['error'=> 'Email Tidak Terdaftar']);
        }
        else{
            //$username = $temp_username;

			$data = DB::table('brw_user')->select('username', 'status', 'brw_id')->where('brw_user.username', '=', $temp_username)->first();

            if($data){

                $detil_borrower = BorrowerDetails::query()->where('brw_id', $data->brw_id)->first();
                if($detil_borrower){
                    if($detil_borrower->brw_type ===  2) {
                        if(!$request->has('channel') && $request->channel  !== 'web'){
                            return response()->json(['error'=> 'Untuk login user badan hukum, harap login melalui web danasyariah']);
                        }
                    }
                }

                 $username = $temp_username;
                 $status_user = $data ? $data->status : '';
                 $borrower_id = $data->brw_id ;

            }else return response()->json(['error' => 'Username tidak terdaftar']);
        }


        $credentials =  (['username' => $username,'password' => $request->password]);

        //[16/11/2022] Add Custom jwt payload
        $claimsPayload  = [];
        if($borrower_id){
            $borrowerDetail = BorrowerDetails::query()->where('brw_id', $borrower_id)->first();
            $userType = "-";

            if($borrowerDetail){
               if($borrowerDetail->brw_type){
                   $userType = $borrowerDetail->brw_type == '1' ? 'Individu': 'Badan Hukum';
                }
             }

            $claimsPayload = ['user_id'=> $borrower_id, 'user_login'=> 'borrower', 'user_type'=> $userType];
        }


		if ('suspend' == $status_user )
		{
			return response()->json(['error' => 'Salah Password 3X , Akun Anda diblokir Silahkan Hubungi Customer Service',
				                         'status' => 'suspend'
										]);
		}

        if (! $token = Auth::guard('borrower-api-mobile')->claims($claimsPayload)->attempt($credentials)) {

			if ($this->hasTooManyLoginAttempts($request) and 'active' == $status_user )
			{
				$data = DB::SELECT("update brw_user set status = 'suspend' where brw_id = $borrower_id ");
				//return $this->sendLockoutResponse($request);
				return response()->json(['error' => 'Anda salah memasukkan kata sandi tiga kali. Akun anda terblokir. Silahkan hubungi Customer Service','status' => 'suspend']);
            }

			if ( 'active' == $status_user )
			{
				$this->incrementLoginAttempts($request);
            }
			return response()->json(['error' => 'User Name atau Kata Sandi Salah ']);

        }

        if('Not Active' == $status_user )
		{
			return response()->json([
									'id_user' => Auth::guard('borrower-api-mobile')->user()->brw_id,
									'expired' => 'no',
									'Not Active' => 'yes',
									'expires_in' => Auth::guard('borrower-api-mobile')->factory()->getTTL(),
									'error' => 'Not Active User',
									'status' => 'Not Active',
									'status_reg' => Auth::guard('borrower-api-mobile')->user()->status_reg
							]);

        }

        $password_updated_at = Auth::guard('borrower-api-mobile')->user()->password_updated_at;
        $password_expiry_days = Auth::guard('borrower-api-mobile')->user()->password_expiry_days;

        if ($password_updated_at != null )
		{

            $password_expiry_at = Carbon::parse($password_updated_at)->addDays($password_expiry_days);

            if(Auth::guard('borrower-api-mobile')->user()->status == 'Not Active'){
                $status = Auth::guard('borrower-api-mobile')->user()->status;

                return $this->respondWithToken($token, $status);
            }
			else if($password_expiry_at->lessThan(Carbon::now()))
			{

                if('active' == $status_user)
				{
					$update_status_expired = LoginBorrower::where('brw_id', Auth::guard('borrower-api-mobile')->user()->brw_id)->update(['status' => 'expired']);
					return response()->json([
						'id_user' => Auth::guard('borrower-api-mobile')->user()->brw_id,
						'expired' => 'yes',
						'access_token' => $token,
						'token_type' => 'bearer',
						'expires_in' => Auth::guard('borrower-api-mobile')->factory()->getTTL(),
						'status' => 'expired',
						'status_reg' => Auth::guard('borrower-api-mobile')->user()->status_reg
					]);
				}
				else
					return response()->json([
						'id_user' => Auth::guard('borrower-api-mobile')->user()->brw_id,
						'expired' => 'no',
						'access_token' => $token,
						'token_type' => 'bearer',
						'expires_in' => Auth::guard('borrower-api-mobile')->factory()->getTTL(),
						'status' => $status_user,
						'status_reg' => Auth::guard('borrower-api-mobile')->user()->status_reg
					]);
            }
			else
			{
                $status = Auth::guard('borrower-api-mobile')->user()->status;

                return $this->respondWithToken($token, $status);
            }
        } else {
            $status = Auth::guard('borrower-api-mobile')->user()->status;

            return $this->respondWithToken($token, $status);
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(Auth::guard('borrower-api-mobile')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        // $token = JWTAuth::getToken();

        // $user_token = Token::where('investor_id', Auth::guard('borrower-api-mobile')->user()->brw_id)->where('login_token', $token)->first();
        // $user_token->delete();

        Auth::guard('borrower-api-mobile')->logout();

        return response()->json(['message' => 'Anda berhasil keluar']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(Auth::guard('borrower-api-mobile')->refresh(), Auth::guard('borrower-api-mobile')->user()->status);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /*
    protected function respondWithToken($token, $status)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('borrower-api-mobile')->factory()->getTTL(),
            'status' => $status
        ]);
    }
    */


    protected function respondWithToken($token, $status)
    {

        $query = Helper::getProfileUser();
        $borrower = $query->where('brw_user.brw_id', Auth::guard('borrower-api-mobile')->user()->brw_id)->first();
        $birthdate = null;

        if(isset($borrower->tgl_lahir) && !empty($borrower->tgl_lahir)){
            $birthdate =  Carbon::parse($borrower->tgl_lahir, 'UTC')->toISOString();
         }

        return response()->json([
            'id_user' => Auth::guard('borrower-api-mobile')->user()->brw_id,
            'email' => $borrower->email,
            'full_name'=> $borrower->full_name,
            'gender'=>  $borrower->gender,
            'contact_number'=> $borrower->contact_number,
            'tipe_pengguna'=> $borrower->tipe_pengguna,
            'birthdate'=> $birthdate,
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('borrower-api-mobile')->factory()->getTTL(),
            'status' => $status,
            'expired' => 'no',
            'status_reg' => Auth::guard('borrower-api-mobile')->user()->status_reg
        ]);
    }

    public function newnewCheckToken (Request $request) {

        $token = $request->token;

        try {
            // attempt to verify the credentials
            $token = JWTAuth::getToken();
            $apy = JWTAuth::getPayload($token)->toArray();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['error'=>'Token Expired, Silahkan login kembali'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['error'=>'Token Invalid, Silahkan login dengan akun Danasyariah'], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['error_token_absent'=>'token_absent', 'msg' => $e->getMessage()], 500);

        }

        $status = Auth::guard('borrower-api-mobile')->user()->status;

        return $this->respondWithToken($token, $status);
    }


    private function upload($column,Request $request, $investor_id)
    {
        if ($request->hasFile($column)) {
            $file = $request->file($column);
            $filename = Carbon::now()->toDateString() . $column . '.' . $file->getClientOriginalExtension();
//            save nama file berdasarkan tanggal upload+nama file
            $store_path = 'brw_user/' . $investor_id;
            $path = $file->storeAs($store_path, $filename, 'public');
//            save gambar yang di upload di public storage
            return $path;
        }
        else {
            return null;
        }

    }

    public function uploadFoto(Request $request)
    {
        $jenis_foto_upload = $request->jenis_foto_upload;

        if($jenis_foto_upload == 'pic_user'){
            $path_jenis_foto_upload = 'pic_brw';
        }else if($jenis_foto_upload == 'pic_ktp_investor'){
            $path_jenis_foto_upload = 'pic_brw_ktp';
        }else if($jenis_foto_upload == 'pic_user_ktp_investor'){
            $path_jenis_foto_upload = 'pic_brw_dan_ktp';
        }else{
            $path_jenis_foto_upload = 'pic_brw_npwp';
        }

        // $path_jenis_foto_upload = $jenis_foto_upload == 'pic_user' ? 'pic_brw' :  $jenis_foto_upload == 'pic_ktp_investor' ? 'pic_brw_ktp' : $jenis_foto_upload == 'pic_user_ktp_investor' ? 'pic_brw_dan_ktp' : 'pic_brw_npwp';


        if ($request->hasFile($jenis_foto_upload)) {
            $file = $request->file($jenis_foto_upload);
            $resize = Image::make($file)->save();
            $filename = $path_jenis_foto_upload. '.' . $file->getClientOriginalExtension();
            //  save nama file berdasarkan tanggal upload+nama file
            $store_path = 'borrower/' . Auth::guard('borrower-api-mobile')->user()->brw_id;
            $path = $file->storeAs($store_path, $filename, 'private');
            //  save gambar yang di upload di private storage

            // Storage::disk('private')->delete('brw_user/'.Auth::guard('borrower-api-mobile')->user()->id.'/'.$filename);

            if(Storage::disk('private')->exists('borrower/'.Auth::guard('borrower-api-mobile')->user()->brw_id.'/'.$filename)){
                return response()->json([
                    'success' => 'Berhasil di upload'
                ]);
            }else{
                return response()->json([
                    'failed' => 'File gagal di upload'
                ]);
            }
        }
        else {
            return [
                'failed' => 'File Kosong'
            ];
        }
    }

    public function register_new_new_new(Request $request) {

        if (LoginBorrower::where('username', $request['username'])->first()!== null) {
            return [
                'error' => 'Nama Akun Sudah Digunakan, Silahkan memakai Nama Akun yang lain'
            ];
        }
        if (LoginBorrower::where('email', $request['email'])->first()!== null) {
            return [
                'error' => 'Email Sudah Digunakan,  Silahkan memakai Email yang lain'
            ];
        }

        if (!filter_var($request['email'], FILTER_VALIDATE_EMAIL)) {
            return [
               'error' => 'Format email tidak valid'
           ];
       }

        $user = LoginBorrower::create([
            'username' => $request['username'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'email_verif' => str_random(30),
            'status'=>'Not Active',
            'ref_number' => $request['referal_code'],
            'password_expiry_days' => 180,
        ]);

        $credentials = request(['username', 'password']);

        if (! $token = Auth::guard('borrower-api-mobile')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized']);
        }

        $email = new EmailAktifasiKpr($user);
        Mail::to($user->email)->send($email);

        if (Mail::failures()) {
            $insert_log_db = DB::table('log_db_app')->insert([
                'file_name' => 'KprAuthController.php',
                'line' => 735,
                'description' => 'Gagal mengirim email pendaftaran user KPR',
            ]);
            return response()->json(['email_failed' => 'Gagal Kirim Email']);
        }

        return response()->json(['status' => 'Not Active']);
    }

    public function resendEmail(Request $request){
        $email = $request->email ? $request->email : 'null';
        if($request->email){
            $user = LoginBorrower::where('email', $email)->first();

            if($user->status !== 'Not Active'){
                return response()->json(['status' => 'Sudah_Terdaftar']);
            }else{
                $email = new EmailAktifasiKpr($user);
                $send_mail = Mail::to($user->email)->send($email);
                // $send_mail = dispatch(new ProcessEmail($user, 'regis'));
            }
        }else{
            $user = LoginBorrower::where('brw_id', Auth::guard('borrower-api-mobile')->user()->brw_id)->first();
            if($user->status !== 'Not Active'){
                return response()->json(['status' => 'Sudah_Terdaftar']);
            }else{
                $email = new EmailAktifasiKpr($user);
                $send_mail = Mail::to($user->email)->send($email);
                // $send_mail = dispatch(new ProcessEmail($user, 'regis'));
            }
        }
        if(!$send_mail){
            return response()->json(['status' => 'Success']);
        }else{
            return response()->json(['status' => 'Failed']);
        }
    }

    public function validateOTP(Request $request){

        // $id = 52215;
        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $input_otp = $request->no_otp;

        $query = LoginBorrower::where('brw_id', $brw_id)->first();
        $otp = $query->otp;

        if($input_otp == $otp){
            return response()->json(['status' => 'success']);
        }else{
            return response()->json(['status' => 'failed']);
        }
    }

    public function sendOtp(Request $request){

        $id=Auth::guard('borrower-api-mobile')->user()->brw_id;
        $to = $request->no_telp;

        // $phone_get = BorrowerDetails::where('brw_id',$id)->first(['no_tlp']);
        // $to =  $phone_get;
        $otp = rand(100000, 999999);

        $text =  "<#> DANASYARIAH-JANGAN MEMBERITAHU KODE INI KE SIAPAPUN termasuk pihak DANASYARIAH. Kode OTP : $otp Silahkan masukan kode ini untuk melanjutkan proses pendaftaran anda.\nX96Ckidjiws";

        //send to db
        $detil = LoginBorrower::where('brw_id', $id)->update(['otp' => $otp]);

        $pecah              = explode(",",$to);
        $jumlah             = count($pecah);
        $from               = "DANASYARIAH"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
        $username           = "danasyariahpremium"; //your smsviro username
        $password           = "Dsi701@2019"; //your smsviro password
        $postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS

        for($i=0; $i<$jumlah; $i++){
            if(substr($pecah[$i],0,2) == "62" || substr($pecah[$i],0,3) == "+62"){
                $pecah = $pecah;
            }elseif(substr($pecah[$i],0,1) == "0"){
                $pecah[$i][0] = "X";
                $pecah = str_replace("X", "62", $pecah);
            }else{
                echo "Invalid mobile number format";
            }
            $destination = array("to" => $pecah[$i]);
            $message     = array("from" => $from,
                                 "destinations" => $destination,
                                 "text" => $text,
                                 "smsCount" => 20);
            $postData           = array("messages" => array($message));
            $postDataJson       = json_encode($postData);
            $ch                 = curl_init();
            $header             = array("Content-Type:application/json", "Accept:application/json");

            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $responseBody = json_decode($response);
            curl_close($ch);
        }

        if($detil){
            $data = ['success' => true, 'message' => 'Silahkan masukan kode ini untuk melanjutkan proses penarikan tunai.'];
            return response()->json($data);
        }else{
          $data = ['failed' => false, 'message' => 'Data Telepon tidak benar.'];
          return response()->json($data);
        }
    }

    public function saveKprAll (Request $request) {

        $id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        if (BorrowerDetails::where('no_tlp', $request->no_hp)->first()) {
            return response()->json(['error'=> 'Nomer Telpon Sudah Pernah Terdaftar']);
        }
        if (BorrowerDetails::where('id', $id)->first()) {
            return response()->json(['error'=> 'Data ini sudah terdaftar']);
        }

        $data_pribadi = json_decode($request->data_pribadi);
        $data_alamat = json_decode($request->data_alamat);
        $data_rekening = json_decode($request->data_rekening);
        $data_pekerjaan = json_decode($request->data_pekerjaan);
        $data_pasangan = json_decode($request->data_pasangan);
        $data_pekerjaan_pasangan = json_decode($request->data_pekerjaan_pasangan);
        $data_foto = json_decode($request->data_foto);

        $bidang_pekerjaan_pasangan = $data_pekerjaan_pasangan ? $data_pekerjaan_pasangan->bidang_pekerjaan : '';
        $bidang_online_pasangan = $data_pekerjaan_pasangan ? $data_pekerjaan_pasangan->bidang_online : '';
        $pekerjaan_pasangan = $data_pekerjaan_pasangan ? $data_pekerjaan_pasangan->pekerjaan : '';

        $jenis_kelamin = $data_pribadi->jenis_kelamin == '0' ? '1' : '2';
        $no_hp = '62'.$data_pribadi->no_hp;

        $path_foto_diri = "/borrower/$id/pic_brw.$data_foto->uri_pic_user_investor";
        $path_foto_ktp = "/borrower/$id/pic_brw_ktp.$data_foto->uri_pic_ktp_investor";
        $path_foto_diri_dan_ktp = "/borrower/$id/pic_brw_dan_ktp.$data_foto->uri_pic_user_ktp_investor";
        $path_foto_npwp = "/borrower/$id/pic_brw_npwp.$data_foto->uri_pic_npwp_investor";

        // DB::beginTransaction();

        //!INSERT INTO BRW_USER_DETAIL
        $insertDetailBorrower = DB::select("SELECT put_into_brw_user_detail(
            '".$id."',
            '".$data_pribadi->nama."',
            '', /*nm_bdn_hukum*/
            '', /*nib*/
            '', /*npwp_perusahaan*/
            '', /*akta*/
            '', /*tgl berdiri*/
            '', /*tlp_perusahaan*/
            '', /*foto_npwp_perusahaan*/
            '', /*bidang usaha*/
            '', /*omset*/
            '', /*tot aset*/
            '', /*jabatan*/
            '1', /*brw_tipe*/
            '".$data_pribadi->nama_ibu_kandung."', /*nama ibu*/
            '".$data_pribadi->no_ktp."', /*ktp*/
            '".$data_pribadi->no_npwp."', /*npwp*/
            '".$data_pribadi->tanggal_lahir."',/*tgl lahir*/
            '".$no_hp."', /*no_tlp*/
            '".$jenis_kelamin."', /*jenis_kelamin*/
            '".$data_pribadi->status_pernikahan."', /*status_kawin*/
            '".$data_alamat->status_kepemilikan_rumah."', /*status rumah*/
            '".$data_alamat->alamat_sesuai_ktp."', /*alamat*/
            '".$data_alamat->alamat_sesuai_ktp_domisili."',  /*domisili*/
            '".$data_alamat->provinsi_domisili."', /*domisili prov*/
            '".$data_alamat->kota_domisili."', /*domisili kota*/
            '".$data_alamat->kecamatan_domisili."', /*domisili kec*/
            '".$data_alamat->kelurahan_domisili."', /*domisili kel*/
            '".$data_alamat->kode_pos_domisili."', /*domisili kd pos*/
            '".$data_alamat->status_kepemilikan_rumah_domisili."',  /*status rumah domisili*/
            '".$data_alamat->provinsi."', /*provinsi*/
            '".$data_alamat->kota."', /*kota*/
            '".$data_alamat->kecamatan."', /*kec*/
            '".$data_alamat->kelurahan."', /*kel*/
            '".$data_alamat->kode_pos."', /*kd pos*/
            '".$data_pribadi->agama."', /*agama*/
            '".$data_pribadi->tempat_lahir."', /*tempat lahir*/
            '".$data_pribadi->pendidikan."', /*pendidikan terakhi*/
            '".$data_pekerjaan->pekerjaan."', /*pekerjaan*/
            '', /*bidang perusahaan*/
            '".$data_pekerjaan->bidang_pekerjaan."', /*bidang pekerjaan*/
            '".$data_pekerjaan->bidang_online."', /*bidang online*/
            '', /*pengalaman pekerjaan*/
            '', /*pendapatan*/
            '', /*total aset*/
            '', /*warganegara*/
            '', /*brw online*/
            '".$path_foto_diri."',
            '".$path_foto_ktp."',
            '".$path_foto_diri_dan_ktp."',
            '".$path_foto_npwp."',
            '".$data_pribadi->no_kk."', /*no kk*/
            '', /*lama menempati*/
            '', /*tlp rumah*/
            '', /*kartu kredit*/
            '', /*usia pensiun*/
            '', /*alamat penagihan rumah*/
            '', /*alamat penagihan kantor*/
            '', /*npwp pasangan*/
            '', /*nomor kk pasangan*/
            '', /*tmpt lahir pasangan*/
            '', /*tgl lahir pasangan*/
            '', /*pendidikan pasangan*/
            '', /*sd pasangan*/
            '', /*smp pasangan*/
            '', /*sma pasangan*/
            '', /*pt pasangan*/
            '".$pekerjaan_pasangan."', /*pekerjaan pasangan*/
            '".$bidang_pekerjaan_pasangan."', /*bd pekerjaan pasangan*/
            '".$bidang_online_pasangan."', /*bd pekerjaanO pasangan*/
            '',  /*pengalaman pasangan*/
            '', /*pendapatan pasangan*/
            '', /*kartu kredit*/
            '', /*jmlasettdkbergerak_pasangan*/
            '', /*mlasetbergerak_pasangan*/
            '', /*agama pasangan*/
            '', /*pic kk*/
            '', /*pic ktp pasangan*/
            '', /*pic surat nikah*/
            '', /*pic spt*/
            '', /*pic rek koran*/
            '', /*pic slip gaji*/
            '', /*pic lap keuangan*/
            '".date('Y-m-d H:i:s')."', /*createtd at*/
            '".date('Y-m-d H:i:s')."',  /*update at*/
            'KprAuthController',
            '523'
            ) as response;"
        );

        //!JIKA SUKSES INSERT BRW_USER_DETAIL MAKA INSERT INTO BRW_REKENING
        if($insertDetailBorrower[0]->response == 1){
            $insertRekening = DB::select("select put_into_brw_rekening(
                '".$id."',
                '',
                '".$data_rekening->nomor_rekening."',
                '".$data_rekening->nama_pemilik_rekening."',
                '".$data_rekening->nama_bank."',
			    '2000000000',
                '0',
                '2000000000',
                '".date('Y-m-d H:i:s')."',
                '".date('Y-m-d H:i:s')."',
                'KprAuthController',
                '641',
                '".$data_rekening->kantor_cabang_pembuka."'
				) as response;"
			);

            // return $insertRekening;

            //!JIKA SUKSES INSERT BRW_REKENING MAKA INSERT INTO BRW_USER_DETAIL_PENGHASILAN
            if($insertRekening[0]->response == 1){

                //VARIABEL KONDISI PEKERJAAN
                $skema_pembiayaan = $data_pekerjaan->skema_pembiayaan == 'single_income' ? 1 : 2;
                $sumber_pengembalian_dana = $data_pekerjaan->sumber_pengembalian_dana == 'fixed_income' ? 1 : 2;

                //JIKA PEKERJAANNYA PELAJAR, TIDAK BEKERJA, DLL
                if($data_pekerjaan->pekerjaan == 6 ||  $data_pekerjaan->pekerjaan == 7 || $data_pekerjaan->pekerjaan == 8){
                    $insertPenghasilan = DB::select("SELECT put_into_brw_user_detail_penghasilan(
                        '".$id."',
                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                        '".$skema_pembiayaan."', /*skema_pembiayaan*/
                        '', /*nama_perusahaan*/
                        '', /*alamat_perusahaan*/
                        '', /*rt*/
                        '', /*rw*/
                        '', /*provinsi*/
                        '', /*kab_kota*/
                        '', /*kecamatan*/
                        '', /*kelurahan*/
                        '', /*kode_pos*/
                        '', /*no_telp*/
                        '', /*no_hp*/
                        '', /*surat_ijin*/
                        '', /*no_surat_ijin*/
                        '', /*bentuk_badan_usaha*/
                        '', /*status_pekerjaan*/
                        '', /*usia_perusahaan*/
                        '', /*usia_tempat_usaha*/
                        '', /*departemen*/
                        '', /*jabatan*/
                        '', /*masa_kerja_tahun*/
                        '', /*masa_kerja_bulan*/
                        '', /*nip_nrp_nik*/
                        '', /*nama_hrd*/
                        '', /*no_fixed_line_hrd*/
                        '', /*pengalaman_kerja_tahun*/
                        '', /*pengalaman_kerja_bulan*/
                        '".$data_pekerjaan->penghasilan_perbulan."', /*pendapatan_borrower*/
                        '".$data_pekerjaan->biaya_hidup."', /*biaya_hidup*/
                        '".$data_pekerjaan->detil_pekerjaan_lain."', /*detail_penghasilan_lain_lain*/
                        '', /*total_penghasilan_lain_lain*/
                        '', /*nilai_spt*/
                        'KprAuthController',
                        '654',
                        '".date('Y-m-d H:i:s')."', /*createtd at*/
                        '".date('Y-m-d H:i:s')."'
                        ) as response;"
                    );

                //!JIKA PEKERJAANNYA FIXED INCOME
                }elseif($sumber_pengembalian_dana == 1){

                    $temp_surat_ijin = 0;
                    $temp_no_telpon_perusahaan = '62'.$data_pekerjaan->no_telpon_perusahaan_fixed_income;
                    $temp_nomor_hrd_fixed_income = '62'.$data_pekerjaan->nomor_hrd_fixed_income;

                    $insertPenghasilan = DB::select("SELECT put_into_brw_user_detail_penghasilan(
                        '".$id."',
                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                        '".$skema_pembiayaan."', /*skema_pembiayaan*/
                        '".$data_pekerjaan->nama_perusahaan_fixed_income."', /*nama_perusahaan*/
                        '".$data_pekerjaan->alamat_perusahaan_fixed_income."', /*alamat_perusahaan*/
                        '".$data_pekerjaan->rt."', /*rt*/
                        '".$data_pekerjaan->rw."', /*rw*/
                        '".$data_pekerjaan->provinsi."', /*provinsi*/
                        '".$data_pekerjaan->kota."', /*kab_kota*/
                        '".$data_pekerjaan->kecamatan."', /*kecamatan*/
                        '".$data_pekerjaan->kelurahan."', /*kelurahan*/
                        '".$data_pekerjaan->kode_pos."', /*kode_pos*/
                        '".$temp_no_telpon_perusahaan."', /*no_telp*/
                        '', /*no_hp*/
                        '".$temp_surat_ijin."', /*surat_ijin*/
                        '', /*no_surat_ijin*/
                        '".$data_pekerjaan->bentuk_badan_usaha_fixed_income."', /*bentuk_badan_usaha*/
                        '".$data_pekerjaan->status_pekerjaan_fixed_income."', /*status_pekerjaan*/
                        '".$data_pekerjaan->lama_bekerja_fixed_income."', /*usia_perusahaan*/
                        '', /*usia_tempat_usaha*/
                        '".$data_pekerjaan->departemen_fixed_income."', /*departemen*/
                        '".$data_pekerjaan->jabatan_fixed_income."', /*jabatan*/
                        '".$data_pekerjaan->pengalaman_kerja_tahun_fixed_income."', /*masa_kerja_tahun*/
                        '".$data_pekerjaan->pengalaman_kerja_bulan_fixed_income."', /*masa_kerja_bulan*/
                        '".$data_pekerjaan->nip_fixed_income."', /*nip_nrp_nik*/
                        '".$data_pekerjaan->nama_hrd_fixed_income."', /*nama_hrd*/
                        '".$temp_nomor_hrd_fixed_income."', /*no_fixed_line_hrd*/
                        '".$data_pekerjaan->pengalaman_kerja_tempat_lain_tahun_fixed_income."', /*pengalaman_kerja_tahun*/
                        '".$data_pekerjaan->pengalaman_kerja_tempat_lain_bulan_fixed_income."', /*pengalaman_kerja_bulan*/
                        '".$data_pekerjaan->penghasilan_perbulan."', /*pendapatan_borrower*/
                        '".$data_pekerjaan->biaya_hidup."', /*biaya_hidup*/
                        '".$data_pekerjaan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                        '".$data_pekerjaan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                        '".$data_pekerjaan->nilai_spt."', /*nilai_spt*/
                        'KprAuthController',
                        '696',
                        '".date('Y-m-d H:i:s')."', /*createtd at*/
                        '".date('Y-m-d H:i:s')."'
                        ) as response;"
                    );

                //!JIKA PEKERJAANNYA NOT FIXED INCOME
                }else{

                    $temp_bentuk_badan_usaha = 0;
                    $temp_status_pekerjaan = 0;

                    $temp_no_telpon_usaha_non_fixed_income = '62'.$data_pekerjaan->no_telpon_usaha_non_fixed_income;
                    $temp_no_hp_non_fixed_income = '62'.$data_pekerjaan->no_hp_non_fixed_income;

                    $insertPenghasilan = DB::select("SELECT put_into_brw_user_detail_penghasilan(
                        '".$id."',
                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                        '".$skema_pembiayaan."', /*skema_pembiayaan*/
                        '".$data_pekerjaan->nama_usaha_non_fixed_income."', /*nama_perusahaan*/
                        '".$data_pekerjaan->alamat_usaha_non_fixed_income."', /*alamat_perusahaan*/
                        '".$data_pekerjaan->rt."', /*rt*/
                        '".$data_pekerjaan->rw."', /*rw*/
                        '".$data_pekerjaan->provinsi."', /*provinsi*/
                        '".$data_pekerjaan->kota."', /*kab_kota*/
                        '".$data_pekerjaan->kecamatan."', /*kecamatan*/
                        '".$data_pekerjaan->kelurahan."', /*kelurahan*/
                        '".$data_pekerjaan->kode_pos."', /*kode_pos*/
                        '".$temp_no_telpon_usaha_non_fixed_income."', /*no_telp*/
                        '".$temp_no_hp_non_fixed_income."', /*no_hp*/
                        '".$data_pekerjaan->surat_ijin_usaha_non_fixed_income."', /*surat_ijin*/
                        '".$data_pekerjaan->nomor_ijin_usaha_non_fixed_income."', /*no_surat_ijin*/
                        '".$temp_bentuk_badan_usaha."', /*bentuk_badan_usaha*/
                        '".$temp_status_pekerjaan."', /*status_pekerjaan*/
                        '".$data_pekerjaan->lama_usaha_non_fixed_income."', /*usia_perusahaan*/
                        '".$data_pekerjaan->lama_tempat_usaha_non_fixed_income."', /*usia_tempat_usaha*/
                        '', /*departemen*/
                        '', /*jabatan*/
                        '', /*masa_kerja_tahun*/
                        '', /*masa_kerja_bulan*/
                        '', /*nip_nrp_nik*/
                        '', /*nama_hrd*/
                        '', /*no_fixed_line_hrd*/
                        '', /*pengalaman_kerja_tahun*/
                        '', /*pengalaman_kerja_bulan*/
                        '".$data_pekerjaan->penghasilan_perbulan."', /*pendapatan_borrower*/
                        '".$data_pekerjaan->biaya_hidup."', /*biaya_hidup*/
                        '".$data_pekerjaan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                        '".$data_pekerjaan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                        '".$data_pekerjaan->nilai_spt."', /*nilai_spt*/
                        'KprAuthController',
                        '738',
                        '".date('Y-m-d H:i:s')."', /*createtd at*/
                        '".date('Y-m-d H:i:s')."'
                        ) as response;"
                    );
                }

                //!JIKA SUKSES INSERT BRW_USER_DETAIL_PENGHASILAN MAKA INSERT INTO BRW_PASANGAN
                if($insertPenghasilan[0]->response == 1){

                    if($data_pribadi->status_pernikahan == 1){
                        // $tanggal_lahir_pasangan = $data_pasangan->tgl_lahir_pasangan;
                        // $bulan_lahir_pasangan = $data_pasangan->bulan_lahir_pasangan;
                        // $tahun_lahir_pasangan = $data_pasangan->tahun_lahir_pasangan;

                        // $tgl_lengkap_pasangan = $tahun_lahir_pasangan.'-'.$bulan_lahir_pasangan.'-'.$tanggal_lahir_pasangan;

                        $tgl_lengkap_pasangan = $data_pasangan->tanggal_lahir_pasangan;
                        $jenis_kelamin_pasangan = $data_pasangan->jenis_kelamin_pasangan == '0' ? '1' : '2';
                        $no_hp_pasangan = '62'.$data_pasangan->no_hp_pasangan;

                        $insertPasangan = DB::select("SELECT put_into_brw_pasangan(
                                '".$id."',
                                '".$data_pasangan->nama_pasangan."',
                                '".$jenis_kelamin_pasangan."',
                                '".$data_pasangan->no_ktp_pasangan."',
                                '".$data_pasangan->tempat_lahir_pasangan."',
                                '".$tgl_lengkap_pasangan."',
                                '".$no_hp_pasangan."',
                                '".$data_pasangan->agama_pasangan."',
                                '".$data_pasangan->pendidikan_pasangan."',
                                '".$data_pasangan->no_npwp_pasangan."',
                                '".$data_pasangan->alamat_pasangan."',
                                '".$data_pasangan->provinsi_pasangan."' ,
                                '".$data_pasangan->kota_pasangan."',
                                '".$data_pasangan->kecamatan_pasangan."',
                                '".$data_pasangan->kelurahan_pasangan."',
                                '".$data_pasangan->kode_pos_pasangan."',
                                '".date('Y-m-d H:i:s')."',
                                '".date('Y-m-d H:i:s')."',
                                'KprAuthController',
                                '790',
                                '".$data_pasangan->no_kk_pasangan."'
                            ) as response;"
                        );

                        if($insertPasangan[0]->response == 1){

                            //! JIKA SUKSES BERHASIL COMMIT PASANGAN
                            // DB::commit();

                            //! JIKA JOINT INCOME
                            if($data_pekerjaan->skema_pembiayaan == 'joint_income'){

                                //VARIABEL KONDISI PEKERJAAN
                                $sumber_pengembalian_dana = $data_pekerjaan_pasangan->sumber_pengembalian_dana == 'fixed_income' ? 1 : 2;

                                //JIKA PEKERJAANNYA PELAJAR, TIDAK BEKERJA, DLL
                                if($data_pekerjaan_pasangan->pekerjaan == 6 ||  $data_pekerjaan_pasangan->pekerjaan == 7 || $data_pekerjaan_pasangan->pekerjaan == 8){
                                    $insertPekerjaanPasangan = DB::select("SELECT put_into_brw_pekerjaan_pasangan(
                                        '".$id."',
                                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                                        '', /*nama_perusahaan*/
                                        '', /*alamat_perusahaan*/
                                        '', /*rt*/
                                        '', /*rw*/
                                        '', /*provinsi*/
                                        '', /*kab_kota*/
                                        '', /*kecamatan*/
                                        '', /*kelurahan*/
                                        '', /*kode_pos*/
                                        '', /*no_telp*/
                                        '', /*no_hp*/
                                        '', /*surat_ijin*/
                                        '', /*no_surat_ijin*/
                                        '', /*bentuk_badan_usaha*/
                                        '', /*status_pekerjaan*/
                                        '', /*usia_perusahaan*/
                                        '', /*usia_tempat_usaha*/
                                        '', /*departemen*/
                                        '', /*jabatan*/
                                        '', /*masa_kerja_tahun*/
                                        '', /*masa_kerja_bulan*/
                                        '', /*nip_nrp_nik*/
                                        '', /*nama_hrd*/
                                        '', /*no_fixed_line_hrd*/
                                        '', /*pengalaman_kerja_tahun*/
                                        '', /*pengalaman_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->penghasilan_perbulan."', /*pendapatan_borrower*/
                                        '".$data_pekerjaan_pasangan->biaya_hidup."', /*biaya_hidup*/
                                        '".$data_pekerjaan_pasangan->detil_pekerjaan_lain."', /*detail_penghasilan_lain_lain*/
                                        '', /*total_penghasilan_lain_lain*/
                                        '', /*nilai_spt*/
                                        'KprAuthController',
                                        '826'
                                        ) as response;"
                                    );

                                //!JIKA PEKERJAANNYA FIXED INCOME
                                }elseif($sumber_pengembalian_dana == 1){

                                    $temp_surat_ijin = 0;
                                    $temp_no_telpon_perusahaan = '62'.$data_pekerjaan_pasangan->no_telpon_perusahaan_fixed_income;
                                    $temp_nomor_hrd_fixed_income = '62'.$data_pekerjaan_pasangan->nomor_hrd_fixed_income;

                                    $insertPekerjaanPasangan = DB::select("SELECT put_into_brw_pekerjaan_pasangan(
                                        '".$id."',
                                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                                        '".$data_pekerjaan_pasangan->nama_perusahaan_fixed_income."', /*nama_perusahaan*/
                                        '".$data_pekerjaan_pasangan->alamat_perusahaan_fixed_income."', /*alamat_perusahaan*/
                                        '".$data_pekerjaan_pasangan->rt."', /*rt*/
                                        '".$data_pekerjaan_pasangan->rw."', /*rw*/
                                        '".$data_pekerjaan_pasangan->provinsi."', /*provinsi*/
                                        '".$data_pekerjaan_pasangan->kota."', /*kab_kota*/
                                        '".$data_pekerjaan_pasangan->kecamatan."', /*kecamatan*/
                                        '".$data_pekerjaan_pasangan->kelurahan."', /*kelurahan*/
                                        '".$data_pekerjaan_pasangan->kode_pos."', /*kode_pos*/
                                        '".$temp_no_telpon_perusahaan."', /*no_telp*/
                                        '', /*no_hp*/
                                        '".$temp_surat_ijin."', /*surat_ijin*/
                                        '', /*no_surat_ijin*/
                                        '".$data_pekerjaan_pasangan->bentuk_badan_usaha_fixed_income."', /*bentuk_badan_usaha*/
                                        '".$data_pekerjaan_pasangan->status_pekerjaan_fixed_income."', /*status_pekerjaan*/
                                        '".$data_pekerjaan_pasangan->lama_bekerja_fixed_income."', /*usia_perusahaan*/
                                        '', /*usia_tempat_usaha*/
                                        '".$data_pekerjaan_pasangan->departemen_fixed_income."', /*departemen*/
                                        '".$data_pekerjaan_pasangan->jabatan_fixed_income."', /*jabatan*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_tahun_fixed_income."', /*masa_kerja_tahun*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_bulan_fixed_income."', /*masa_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->nip_fixed_income."', /*nip_nrp_nik*/
                                        '".$data_pekerjaan_pasangan->nama_hrd_fixed_income."', /*nama_hrd*/
                                        '".$temp_nomor_hrd_fixed_income."', /*no_fixed_line_hrd*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_tempat_lain_tahun_fixed_income."', /*pengalaman_kerja_tahun*/
                                        '".$data_pekerjaan_pasangan->pengalaman_kerja_tempat_lain_bulan_fixed_income."', /*pengalaman_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->penghasilan_perbulan."', /*pendapatan_borrower*/
                                        '".$data_pekerjaan_pasangan->biaya_hidup."', /*biaya_hidup*/
                                        '".$data_pekerjaan_pasangan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->nilai_spt."', /*nilai_spt*/
                                        'KprAuthController',
                                        '870'
                                        ) as response;"
                                    );

                                //!JIKA PEKERJAANNYA NOT FIXED INCOME
                                }else{

                                    $temp_bentuk_badan_usaha = 0;
                                    $temp_status_pekerjaan = 0;

                                    $temp_no_telpon_usaha_non_fixed_income = '62'.$data_pekerjaan_pasangan->no_telpon_usaha_non_fixed_income;
                                    $temp_no_hp_non_fixed_income = '62'.$data_pekerjaan_pasangan->no_hp_non_fixed_income;

                                    $insertPekerjaanPasangan = DB::select("SELECT put_into_brw_pekerjaan_pasangan(
                                        '".$id."',
                                        '".$sumber_pengembalian_dana."', /*sumber_pengembalian_dana*/
                                        '".$data_pekerjaan_pasangan->nama_usaha_non_fixed_income."', /*nama_perusahaan*/
                                        '".$data_pekerjaan_pasangan->alamat_usaha_non_fixed_income."', /*alamat_perusahaan*/
                                        '".$data_pekerjaan_pasangan->rt."', /*rt*/
                                        '".$data_pekerjaan_pasangan->rw."', /*rw*/
                                        '".$data_pekerjaan_pasangan->provinsi."', /*provinsi*/
                                        '".$data_pekerjaan_pasangan->kota."', /*kab_kota*/
                                        '".$data_pekerjaan_pasangan->kecamatan."', /*kecamatan*/
                                        '".$data_pekerjaan_pasangan->kelurahan."', /*kelurahan*/
                                        '".$data_pekerjaan_pasangan->kode_pos."', /*kode_pos*/
                                        '".$temp_no_telpon_usaha_non_fixed_income."', /*no_telp*/
                                        '".$temp_no_hp_non_fixed_income."', /*no_hp*/
                                        '".$data_pekerjaan_pasangan->surat_ijin_usaha_non_fixed_income."', /*surat_ijin*/
                                        '".$data_pekerjaan_pasangan->nomor_ijin_usaha_non_fixed_income."', /*no_surat_ijin*/
                                        '".$temp_bentuk_badan_usaha."', /*bentuk_badan_usaha*/
                                        '".$temp_status_pekerjaan."', /*status_pekerjaan*/
                                        '".$data_pekerjaan_pasangan->lama_usaha_non_fixed_income."', /*usia_perusahaan*/
                                        '".$data_pekerjaan_pasangan->lama_tempat_usaha_non_fixed_income."', /*usia_tempat_usaha*/
                                        '', /*departemen*/
                                        '', /*jabatan*/
                                        '', /*masa_kerja_tahun*/
                                        '', /*masa_kerja_bulan*/
                                        '', /*nip_nrp_nik*/
                                        '', /*nama_hrd*/
                                        '', /*no_fixed_line_hrd*/
                                        '', /*pengalaman_kerja_tahun*/
                                        '', /*pengalaman_kerja_bulan*/
                                        '".$data_pekerjaan_pasangan->penghasilan_perbulan."', /*pendapatan_borrower*/
                                        '".$data_pekerjaan_pasangan->biaya_hidup."', /*biaya_hidup*/
                                        '".$data_pekerjaan_pasangan->detil_penghasilan_lain."', /*detail_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->penghasilan_lain_perbulan."', /*total_penghasilan_lain_lain*/
                                        '".$data_pekerjaan_pasangan->nilai_spt."', /*nilai_spt*/
                                        'KprAuthController',
                                        '914'
                                        ) as response;"
                                    );
                                }

                                if($insertPekerjaanPasangan[0]->response == 1){
                                    //! SUKSES SIMPAN DENGAN PEKERJAAN PASANGAN

                                    $updateStatusBorrower = DB::table('brw_user')
                                    ->where('brw_id', $id)
                                    ->update(['status' => "active"]);

                                    // DB::commit();
                                    return response()->json(['success'=> 'Berhasil Simpan Data']);
                                }else{
                                    // DB::rollback();
                                    return response()->json(['error'=> 'Gagal Simpan Data brw_pekerjaan_pasangan']);
                                }
                            }else{
                                //! SUKSES SIMPAN TANPA PEKERJAAN PASANGAN

                                $updateStatusBorrower = DB::table('brw_user')
                                ->where('brw_id', $id)
                                ->update(['status' => "active"]);

                                // DB::commit();
                                return response()->json(['success'=> 'Berhasil Simpan Data']);
                            }

                        }else{
                            // DB::rollback();
                            return response()->json(['error'=> 'Gagal Simpan brw_pasangan Data']);
                        }

                    }else{
                        //! SUKSES SIMPAN TANPA PASANGAN

                        $updateStatusBorrower = DB::table('brw_user')
                        ->where('brw_id', $id)
                        ->update(['status' => "active"]);

                        // DB::commit();
                        return response()->json(['success'=> 'Berhasil Simpan Data']);
                    }

                }else{
                    // DB::rollback();
                    return response()->json(['error'=> 'Gagal Simpan Data brw_user_detail_penghasilan']);
                }
            }else{
                // DB::rollback();
                return response()->json(['error'=> 'Gagal Simpan Data brw_rekening']);
            }
        }else{
            // DB::rollback();
            return response()->json(['error'=> 'Gagal Simpan Data brw_user_detail']);
        }
    }

    public function addPendanaanKpr(Request $request){
        $id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $check_input_validation = DB::select("SELECT val_ajukan_kpr(
            '".$request->tujuan_pendanaan."',
            '1',
            '".$request->harga_objek_pendanaan."',
            '2',
            '".$request->uang_muka."',
            '3',
            '".$request->nilai_pengajuan_pendanaan."',
            'KprAuthController.php',
            '999'
            ) as response;"
        );

        if($check_input_validation[0]->response !== '1'){
            $data = $check_input_validation[0]->response;
            $response_error = explode('#', $data);

            $array = [];
            for($i=0; $i<sizeof($response_error)-1; $i++){
                $string_tenor = explode(';', $response_error[$i]);
                $id = $string_tenor[0];
                $deskripsi_error = $string_tenor[1];

                $array[$i] = [
                    'no'=> $i+1,
                    'id' => $id,
                    'deskripsi_error' => $deskripsi_error
                ];
            }

            return response()->json(['error'=> 'error_validasi', 'response_error'=>$array]);
        }else{
            $insert_brw_pengajuan = DB::table('brw_pengajuan')->insert([
                'pendanaan_tipe' => $request->tipe_pendanaan,
                'pendanaan_tujuan' => $request->tujuan_pendanaan,
                'brw_id' => $id,
                'lokasi_proyek' => $request->alamat_pendanaan,
                'provinsi' => $request->provinsi,
                'kota' => $request->kota,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'kode_pos' => $request->kode_pos,
                'harga_objek_pendanaan' => $request->harga_objek_pendanaan,
                'uang_muka' => $request->uang_muka,
                'pendanaan_dana_dibutuhkan' => $request->nilai_pengajuan_pendanaan,
                'durasi_proyek' => $request->jangka_waktu,
                'detail_pendanaan' => $request->detail_pendanaan,
                'status' => 0,
                'nm_pemilik' => $request->nama_pengaju,
                'no_tlp_pemilik' => $request->no_telpon_pengaju,
                'alamat_pemilik' => $request->alamat_pengaju,
                'provinsi_pemilik' => $request->provinsi_pengaju,
                'kota_pemilik' => $request->kota_pengaju,
                'kecamatan_pemilik' => $request->kecamatan_pengaju,
                'kelurahan_pemilik' => $request->kelurahan_pengaju,
                'kd_pos_pemilik' => $request->kode_pos_pengaju
            ]);

            if($insert_brw_pengajuan){
                $insert_deskripsi_proyek = DB::table('deskripsi_proyeks')->insert([
                    'deskripsi' => $request->detail_pendanaan,
                    "created_at" => date("Y-m-d H:i:s"),
                    "updated_at" => date("Y-m-d H:i:s")
                ]);

                if($insert_deskripsi_proyek){

                    $select_id_pengajuan = DB::table('brw_pengajuan')->select('pengajuan_id')->orderBy('pengajuan_id', 'desc')->limit(1)->first();

                    return response()->json([
                        'success'=> 'Sukses Simpan',
                        'id_pengajuan'=>$select_id_pengajuan->pengajuan_id
                    ]);
                }else{
                    return response()->json(['error'=> 'failed_save_db', 'response_error'=>'Gagal Simpan Data']);
                }
            }
        }
    }

    public function checkVersion(){
        $users = DB::table('mobile_version')->orderBy('id', 'desc')->limit(1)->get();

        return ['id'=>$users[0]->id,
                'version'=>$users[0]->version,
                'version_code'=>$users[0]->version_code,
                'created_date'=>$users[0]->created_at,
                'location'=>'/storage/'.$users[0]->location
            ];
    }

    public function checkPhoneNumber(Request $request){

        if(empty($request->no_hp)){
            return response()->json(['success'=> 'Nomer Telpon Belum Pernah Terdaftar']);
        }
        else
            if (BorrowerDetails::whereIn('no_tlp', ['62'.$request->no_hp, '0'.$request->no_hp])->first()) {
                return response()->json(['error'=> 'Nomer Telpon Sudah Pernah Terdaftar']);
            }
            else
            return response()->json(['success'=> 'Nomer Telpon Belum Pernah Terdaftar']);
    }

    public function check_KTP(Request $request){
        if (BorrowerDetails::where('ktp', $request->no_ktp)->first()) {
            return response()->json(['error'=> 'Nomor KTP Sudah Pernah Terdaftar']);
        }else return response()->json(['success'=> 'Nomer KTP Belum Pernah Terdaftar']);
    }

    public function checkNpwp(Request $request){
        if (BorrowerDetails::where('npwp', $request->no_npwp)->first()) {
            return response()->json(['error'=> 'Nomor NPWP Sudah Pernah Terdaftar']);
        }else return response()->json(['success'=> 'Nomer NPWP Belum Pernah Terdaftar']);
    }

    public function getTermCondition(Request $request){
        $detils = TermCondition::orderBy('id', 'desc')->first();

        return [
            'id'=>$detils->id,
            'title'=>$detils->title,
            'writer'=>$detils->writer,
            'deskripsi'=>$detils->deskripsi,
        ];
    }

    public function masterSimulasiPengajuan(){

        $master_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->where('pendanaan_nama', 'Dana Rumah')->get();
        $x=0;
        if (!isset($master_tipe_pendanaan[0])) {
            $master_tipe_pendanaan = null;
        }
        else {
                foreach ($master_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan
                ];
                $x++;
            }
        }

        return [
            'data_tipe_pendanaan' => $data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan,
        ];
    }

    public function allMasterPengajuan(){

        $brw_id=Auth::guard('borrower-api-mobile')->user()->brw_id;

        $master_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->where('pendanaan_nama', 'Dana Rumah')->get();
        $x=0;
        if (!isset($master_tipe_pendanaan[0])) {
            $master_tipe_pendanaan = null;
        }
        else {
                foreach ($master_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan
                ];
                $x++;
            }
        }

        return [
            'data_tipe_pendanaan' => $data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan
        ];
    }

    public function allMasterFormBaru(){

        $master_kawin = MasterKawin::all();
        $x=0;
        if (!isset($master_kawin[0])) {
            $master_kawin = null;
        }else {
                foreach ($master_kawin as $item){
                $data_kawin[$x] = [
                    'id_kawin'=>$item->id_kawin,
                    'jenis_kawin'=>$item->jenis_kawin,

                ];
                $x++;
            }
        }

        $master_pendidikan = MasterPendidikan::all();
        $x=0;
        if (!isset($master_pendidikan[0])) {
            $master_pendidikan = null;
        }else {
                foreach ($master_pendidikan as $item){
                $data_pendidikan[$x] = [
                    'id_pendidikan'=>$item->id_pendidikan,
                    'pendidikan'=>$item->pendidikan,

                ];
                $x++;
            }
        }

        $master_pekerjaan = MasterPekerjaan::whereNotIn('id_pekerjaan', [6,7,8])->get();
        $x=0;
        if (!isset($master_pekerjaan[0])) {
            $master_pekerjaan = null;
        }else {
                foreach ($master_pekerjaan as $item){
                $data_pekerjaan[$x] = [
                    'id_pekerjaan'=>$item->id_pekerjaan,
                    'pekerjaan'=>$item->pekerjaan,

                ];
                $x++;
            }
        }

        $master_agama = MasterAgama::all();
        $x=0;
        if (!isset($master_agama[0])) {
            $master_agama = null;
        }else {
                foreach ($master_agama as $item){
                $data_agama[$x] = [
                    'id_agama'=>$item->id_agama,
                    'agama'=>$item->agama,
                ];
                $x++;
            }
        }

        $master_bidang_pekerjaan = MasterBidangPekerjaan::whereNotIn('kode_bidang_pekerjaan', ['e00', 'e01'])->get();
        $x=0;
        if (!isset($master_bidang_pekerjaan[0])) {
            $master_bidang_pekerjaan = null;
        }else {
                foreach ($master_bidang_pekerjaan as $item){
                $data_bidang_pekerjaan[$x] = [
                    'id_bidang_pekerjaan'=>$item->id_bidang_pekerjaan,
                    'kode_bidang_pekerjaan'=>$item->kode_bidang_pekerjaan,
                    'bidang_pekerjaan'=>$item->bidang_pekerjaan,
                ];
                $x++;
            }
        }

        $master_online = MasterOnline::all();
        $x=0;
        if (!isset($master_online[0])) {
            $master_online = null;
        }else {
                foreach ($master_online as $item){
                $data_online[$x] = [
                    'id_online'=>$item->id_online,
                    'tipe_online'=>$item->tipe_online,
                ];
                $x++;
            }
        }

        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        $data_tipe_pendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->where('pendanaan_nama', 'Dana Rumah')->get();
        $x=0;
        if (!isset($data_tipe_pendanaan[0])) {
            $data_tipe_pendanaan = null;
        }
        else {
                foreach ($data_tipe_pendanaan as $item){
                $data_tipe_pendanaan[$x] = [
                    'id'=>$item->tipe_id,
                    'pendanaan_nama'=>$item->pendanaan_nama,
                ];
                $x++;
            }
        }

        $master_tujuan_pendanaan = DB::table('m_tujuan_pembiayaan')->select('id', 'tujuan_pembiayaan')->where('tipe_id', '2')->get();
        $x=0;
        if (!isset($master_tujuan_pendanaan[0])) {
            $master_tujuan_pendanaan = null;
        }
        else{
                foreach ($master_tujuan_pendanaan as $item){
                $data_tujuan_pendanaan[$x] = [
                    'id'=>$item->id,
                    'tujuan_pembiayaan'=>$item->tujuan_pembiayaan
                ];
                $x++;
            }
        }

        $data_jenis_rumah=array();
        $data_1 = [
            'id'=> '1', 'jenis_rumah'=>'Rumah Baru',
        ];
        array_push($data_jenis_rumah, $data_1);

        $data_2 = [
            'id'=> '2', 'jenis_rumah'=>'Rumah Lama'
        ];
        array_push($data_jenis_rumah, $data_2);

        return [
            'data_agama' => $data_agama,
            'data_kawin' => $data_kawin,
            'data_pendidikan' => $data_pendidikan,
            'data_pekerjaan' => $data_pekerjaan,
            'data_bidang_pekerjaan' => $data_bidang_pekerjaan,
            'data_online' => $data_online,
            'data_provinsi' => $data_master_provinsi,
            'data_tipe_pendanaan' =>$data_tipe_pendanaan,
            'data_tujuan_pendanaan' => $data_tujuan_pendanaan,
            'data_jenis_rumah'=>$data_jenis_rumah
        ];
    }

    public function allMasterBaru() {

        $master_jenis_kelamin = MasterJenisKelamin::all();
        $x=0;
        if (!isset($master_jenis_kelamin[0])) {
            $master_jenis_kelamin = null;
        }
        else {
                foreach ($master_jenis_kelamin as $item){
                $data_jenis_kelamin[$x] = [
                    'id'=>$item->id_jenis_kelamin,
                    'jenis_kelamin'=>$item->jenis_kelamin,
                ];
                $x++;
            }
        }

        $master_bank = MasterBank::all();
        $x=0;
        if (!isset($master_bank[0])) {
            $master_bank = null;
        }
        else {
                foreach ($master_bank as $item){
                $data_bank[$x] = [
                    'kode'=>$item->kode_bank,
                    'nama_bank'=>$item->nama_bank,
                ];
                $x++;
            }
        }

        $master_kawin = MasterKawin::all();
        $x=0;
        if (!isset($master_kawin[0])) {
            $master_kawin = null;
        }
        else {
                foreach ($master_kawin as $item){
                $data_kawin[$x] = [
                    'id_kawin'=>$item->id_kawin,
                    'jenis_kawin'=>$item->jenis_kawin,

                ];
                $x++;
            }
        }

        $master_pendapatan = MasterPendapatan::all();
        $x=0;
        if (!isset($master_pendapatan[0])) {
            $master_pendapatan = null;
        }
        else {
                foreach ($master_pendapatan as $item){
                $data_pendapatan[$x] = [
                    'id_pendapatan'=>$item->id_pendapatan,
                    'pendapatan'=>$item->pendapatan,

                ];
                $x++;
            }
        }

        $master_pendidikan = MasterPendidikan::all();
        $x=0;
        if (!isset($master_pendidikan[0])) {
            $master_pendidikan = null;
        }
        else {
                foreach ($master_pendidikan as $item){
                $data_pendidikan[$x] = [
                    'id_pendidikan'=>$item->id_pendidikan,
                    'pendidikan'=>$item->pendidikan,

                ];
                $x++;
            }
        }

        $master_pekerjaan = MasterPekerjaan::whereNotIn('id_pekerjaan', [6,7,8])->get();
        $x=0;
        if (!isset($master_pekerjaan[0])) {
            $master_pekerjaan = null;
        }
        else {
                foreach ($master_pekerjaan as $item){
                $data_pekerjaan[$x] = [
                    'id_pekerjaan'=>$item->id_pekerjaan,
                    'pekerjaan'=>$item->pekerjaan,

                ];
                $x++;
            }
        }

        $master_jenis_pengguna = MasterJenisPengguna::all();
        $x=0;
        if (!isset($master_jenis_pengguna[0])) {
            $master_jenis_pengguna = null;
        }
        else {
                foreach ($master_jenis_pengguna as $item){
                $data_jenis_pengguna[$x] = [
                    'id_jenis_pengguna'=>$item->id_jenis_pengguna,
                    'jenis_pengguna'=>$item->jenis_pengguna,
                ];
                $x++;
            }
        }

        $master_agama = MasterAgama::all();
        $x=0;
        if (!isset($master_agama[0])) {
            $master_agama = null;
        }
        else {
                foreach ($master_agama as $item){
                $data_agama[$x] = [
                    'id_agama'=>$item->id_agama,
                    'agama'=>$item->agama,
                ];
                $x++;
            }
        }

        $master_bidang_pekerjaan = MasterBidangPekerjaan::whereNotIn('kode_bidang_pekerjaan', ['e00', 'e01'])->get();
        $x=0;
        if (!isset($master_bidang_pekerjaan[0])) {
            $master_bidang_pekerjaan = null;
        }
        else {
                foreach ($master_bidang_pekerjaan as $item){
                $data_bidang_pekerjaan[$x] = [
                    'id_bidang_pekerjaan'=>$item->id_bidang_pekerjaan,
                    'kode_bidang_pekerjaan'=>$item->kode_bidang_pekerjaan,
                    'bidang_pekerjaan'=>$item->bidang_pekerjaan,
                ];
                $x++;
            }
        }

        $master_pengalaman_kerja = MasterPengalamanKerja::all();
        $x=0;
        if (!isset($master_pengalaman_kerja[0])) {
            $master_pengalaman_kerja = null;
        }
        else {
                foreach ($master_pengalaman_kerja as $item){
                $data_pengalaman_kerja[$x] = [
                    'id_pengalaman_kerja'=>$item->id_pengalaman_kerja,
                    'pengalaman_kerja'=>$item->pengalaman_kerja,
                ];
                $x++;
            }
        }

        $master_online = MasterOnline::all();
        $x=0;
        if (!isset($master_online[0])) {
            $master_online = null;
        }
        else {
                foreach ($master_online as $item){
                $data_online[$x] = [
                    'id_online'=>$item->id_online,
                    'tipe_online'=>$item->tipe_online,
                ];
                $x++;
            }
        }

        $master_hub_waris = DB::table('m_hub_ahli_waris')->select('id_hub_ahli_waris', 'jenis_hubungan')->get();
        $x=0;
        if (!isset($master_hub_waris[0])) {
            $master_hub_waris = null;
        }
        else {
                foreach ($master_hub_waris as $item){
                $data_hub_waris[$x] = [
                    'id_hub_ahli_waris'=>$item->id_hub_ahli_waris,
                    'jenis_hubungan'=>$item->jenis_hubungan,
                ];
                $x++;
            }
        }

        $master_bentuk_badan_usaha = DB::table('m_bentuk_badan_usaha')->select('id', 'bentuk_badan_usaha')->get();
        $x=0;
        if (!isset($master_bentuk_badan_usaha[0])) {
            $master_bentuk_badan_usaha = null;
        }
        else{
                foreach ($master_bentuk_badan_usaha as $item){
                $data_master_bentuk_badan_usaha[$x] = [
                    'id'=>$item->id,
                    'bentuk_badan_usaha'=>$item->bentuk_badan_usaha
                ];
                $x++;
            }
        }

        return [
            'data_jenis_kelamin' => $data_jenis_kelamin,
            'data_bank' => $data_bank,
            'data_kawin' => $data_kawin,
            'data_pendapatan' => $data_pendapatan,
            'data_pendidikan' => $data_pendidikan,
            'data_pekerjaan' => $data_pekerjaan,
            'data_jenis_pengguna' => $data_jenis_pengguna,
            'data_agama' => $data_agama,
            'data_bidang_pekerjaan' => $data_bidang_pekerjaan,
            'data_pengalaman_kerja' => $data_pengalaman_kerja,
            'data_online' => $data_online,
            'data_hub_waris' => $data_hub_waris,
            'data_master_bentuk_badan_usaha'=>$data_master_bentuk_badan_usaha
        ];
    }

    public function getProvinsi(){

        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }

        else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        return [
            'data_provinsi' => $data_master_provinsi
        ];
    }

    public function getKota($provinsi){

        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')->where('Provinsi', $provinsi)->groupBy('Kota')->get();

        $x=0;
        if (!isset($getKota[0])) {
            $getKota = null;
        }

        else{
                foreach ($getKota as $item){
                $data_master_kota[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kota'=>$item->Kota,
                ];
                $x++;
            }
        }

        return [
            'data_master_kota' => $data_master_kota
        ];
    }

    public function getKecamatan($kota){

        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'kecamatan')->where('Kota', $kota)->groupBy('kecamatan')->get();

        $x=0;
        if (!isset($getKecamatan[0])) {
            $getKecamatan = null;
        }

        else{
                foreach ($getKecamatan as $item){
                $data_master_kecamatan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kecamatan'=>$item->kecamatan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kecamatan' => $data_master_kecamatan
        ];
    }

    public function getKelurahan($kecamatan){

        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'kelurahan')->where('kecamatan', $kecamatan)->groupBy('kelurahan')->get();

        $x=0;
        if (!isset($getKelurahan[0])) {
            $getKelurahan = null;
        }

        else{
                foreach ($getKelurahan as $item){
                $data_master_kelurahan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kelurahan'=>$item->kelurahan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kelurahan' => $data_master_kelurahan
        ];
    }

    public function getKodePos($kelurahan, $kecamatan){

        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')->where('kelurahan', $kelurahan)->where('kecamatan', $kecamatan)->first();

        if($getKodePos){
            return [
                'kode_pos' => $getKodePos->kode_pos
            ];
        }else{
            return [
                'kode_pos' => ''
            ];
        }
    }

    public function getProvinsiPendanaan(){

        $data_prov = [31, 32, 36];
        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->whereIn('kode_provinsi', $data_prov)->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }

        else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        return [
            'data_provinsi' => $data_master_provinsi
        ];
    }

    public function getKotaPendanaan($provinsi){


        // $kota_banten = ['Tangerang', 'Tangerang Selatan'];
        // $kota_jakarta = ['Kepulauan Seribu'];
        // $kota_jawa_barat = ['Bandung', 'Bandung Barat', 'Bogor', 'Bekasi', 'Depok'];

        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')->where('Provinsi', $provinsi)->groupBy('Kota')->get();

        // if($provinsi == 'Banten'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereIn('Kota', $kota_banten)->groupBy('Kota')->get();
        // }else if($provinsi == 'DKI Jakarta'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereNotIn('Kota', $kota_jakarta)->groupBy('Kota')->get();
        // }else if($provinsi == 'Jawa Barat'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereIn('Kota', $kota_jawa_barat)->groupBy('Kota')->get();
        // }else{
        //     $getKota = $getKota->where('Provinsi', $provinsi)->groupBy('Kota')->get();
        // }

        $x=0;
        if (!isset($getKota[0])) {
            $getKota = null;
        }

        else{
                foreach ($getKota as $item){
                $data_master_kota[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kota'=>$item->Kota,
                ];
                $x++;
            }
        }

        return [
            'data_master_kota' => $data_master_kota
        ];
    }

    public function getKecamatanPendanaan($kota){

        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'kecamatan')->where('Kota', $kota)->groupBy('kecamatan')->get();

        $x=0;
        if (!isset($getKecamatan[0])) {
            $getKecamatan = null;
        }

        else{
                foreach ($getKecamatan as $item){
                $data_master_kecamatan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kecamatan'=>$item->kecamatan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kecamatan' => $data_master_kecamatan
        ];
    }

    public function getKelurahanPendanaan($kecamatan){

        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'kelurahan')->where('kecamatan', $kecamatan)->groupBy('kelurahan')->get();

        $x=0;
        if (!isset($getKelurahan[0])) {
            $getKelurahan = null;
        }

        else{
                foreach ($getKelurahan as $item){
                $data_master_kelurahan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kelurahan'=>$item->kelurahan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kelurahan' => $data_master_kelurahan
        ];
    }

    public function getKodePosPendanaan($kelurahan, $kecamatan){

        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')->where('kelurahan', $kelurahan)->where('kecamatan', $kecamatan)->first();

        if($getKodePos){
            return [
                'kode_pos' => $getKodePos->kode_pos
            ];
        }else{
            return [
                'kode_pos' => ''
            ];
        }
    }

    public function getResultSimulationKepemilikan(Request $request){

        $total_pinjaman = $request->nilai_pengajuan_pendanaan;
        $tenor = $request->jangka_waktu;

        $call_log_db = DB::select("select get_simulation_kpr( '$total_pinjaman','$tenor', 'KprAuthController.php', '1620') as response;");

        $data = $call_log_db[0]->response;
        $final = explode('#', $data);
        $array = [];
        $total_cicilan = str_replace('^', '', $final[sizeof($final)-1]);

        for($i=0; $i<sizeof($final)-1; $i++){
            $string_tenor = explode(';', $final[$i]);
            $tenor = $string_tenor[0];
            $nilai_cicilan = $string_tenor[1];
            $total_cicilan_periode = $string_tenor[2];
            $angsuran_pokok = $string_tenor[3];
            $angsuran_margin = $string_tenor[4];

            $array[$i] = [
                'id'=> $i,
                'tenor' => $tenor,
                'nilai_cicilan' => $nilai_cicilan,
                'total_cicilan_periode' => $total_cicilan_periode,
                'angsuran_pokok' => $angsuran_pokok,
                'angsuran_margin' => $angsuran_margin
            ];
        }

        return response()->json(['data' => $array, 'total_cicilan'=> $total_cicilan]);
    }

    public function getResultSimulationPraKepemilikan(Request $request){

        $total_pinjaman = $request->nilai_pengajuan_pendanaan;
        $tenor = $request->jangka_waktu;
        $margin = 0.12;

        $nominal_cicilan_perbulan = ($total_pinjaman + ($total_pinjaman*$margin))/$tenor;

        $total_cicilan = 0;
        for($i=0; $i<$tenor; $i++){
            $total_cicilan += $nominal_cicilan_perbulan;
        }

        return response()->json(['nominal_cicilan_perbulan' => floor($nominal_cicilan_perbulan), 'total_cicilan'=> floor($total_cicilan) ]);
    }

    public function cekBiodataPribadi(Request $request){

        $npwp = $request->npwp;
        $ktp = $request->ktp;
        $no_hp = $request->no_hp;

        $cek_validasi = DB::select("SELECT val_register_user(
            '1',
            '1',
            '".$npwp."',
            '2',
            '".$ktp."',
            '3',
            '".$no_hp."',
            'KprAuthController.php',
            1845) as result;"
        );
        return response()->json(['error' => $cek_validasi[0]->result]);

        if($cek_validasi[0]->result !=='1'){
            return response()->json(['error' => $cek_validasi[0]->result]);
        }else{
            return response()->json(['success' => 'Berhasil Checkout Pendanaan', 'jumlah_investasi'=>$total_investation]);
        }
    }

    public function addPendanaanKprRpc(Request $request){

        $data_pribadi_rpc = json_decode($request->data_pribadi_rpc);

        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $nama = $data_pribadi_rpc->nama;
        $no_hp = '62'.$data_pribadi_rpc->no_hp;
        $no_ktp = $data_pribadi_rpc->no_ktp;
        $no_npwp = $data_pribadi_rpc->no_npwp;
        $status_pernikahan = $data_pribadi_rpc->status_pernikahan[0];
        $agama = $data_pribadi_rpc->agama[0];
        $alamat_domisili = $data_pribadi_rpc->alamat;
        $provinsi_domisili = $data_pribadi_rpc->provinsi;
        $kota_domisili = $data_pribadi_rpc->kota;
        $kecamatan_domisili = $data_pribadi_rpc->kecamatan;
        $kelurahan_domisili = $data_pribadi_rpc->kelurahan;
        $kode_pos_domisili = $data_pribadi_rpc->kode_pos;
        $bidang_online = $data_pribadi_rpc->bidang_online[0];
        $bidang_pekerjaan = $data_pribadi_rpc->bidang_pekerjaan[0];
        $pekerjaan = $data_pribadi_rpc->pekerjaan[0];
        $pendidikan = $data_pribadi_rpc->pendidikan[0];

        $penghasilan_perbulan = $data_pribadi_rpc->penghasilan_perbulan;
        $sumber_pengembalian_dana_ = $data_pribadi_rpc->sumber_pengembalian_dana;
        if($sumber_pengembalian_dana_ == 'fixed_income'){
            $sumber_pengembalian_dana = 1;
        }else{
            $sumber_pengembalian_dana = 2;
        };
        $nama_perusahaan_fixed_income = isset($data_pribadi_rpc->nama_perusahaan_fixed_income) ? $data_pribadi_rpc->nama_perusahaan_fixed_income : '';
        $pengalaman_kerja_bulan_fixed_income = isset($data_pribadi_rpc->pengalaman_kerja_bulan_fixed_income) ? $data_pribadi_rpc->pengalaman_kerja_bulan_fixed_income : '';
        $pengalaman_kerja_tahun_fixed_income = isset($data_pribadi_rpc->pengalaman_kerja_tahun_fixed_income) ? $data_pribadi_rpc->pengalaman_kerja_tahun_fixed_income : '';
        $no_telpon_perusahaan_fixed_income = isset($data_pribadi_rpc->no_telpon_perusahaan_fixed_income) ? $data_pribadi_rpc->no_telpon_perusahaan_fixed_income : '';

        $lama_usaha_non_fixed_income = isset($data_pribadi_rpc->lama_usaha_non_fixed_income) ? $data_pribadi_rpc->lama_usaha_non_fixed_income : '';
        $nama_usaha_non_fixed_income = isset($data_pribadi_rpc->nama_usaha_non_fixed_income) ? $data_pribadi_rpc->nama_usaha_non_fixed_income : '';
        $no_hp_non_fixed_income = isset($data_pribadi_rpc->no_hp_non_fixed_income) ? $data_pribadi_rpc->no_hp_non_fixed_income : '';

        $data_objek_pendanaan_rpc = json_decode($request->data_objek_pendanaan_rpc);
        $harga_objek_pendanaan = $data_objek_pendanaan_rpc->harga_objek_pendanaan;
        $jangka_waktu = $data_objek_pendanaan_rpc->jangka_waktu;
        $jenis_rumah = $data_objek_pendanaan_rpc->jenis_rumah;
        $nilai_pengajuan_pendanaan = $data_objek_pendanaan_rpc->nilai_pengajuan_pendanaan;
        $tujuan_pendanaan = $data_objek_pendanaan_rpc->tujuan_pendanaan;
        $tipe_pendanaan = $data_objek_pendanaan_rpc->tipe_pendanaan;
        $uang_muka = $data_objek_pendanaan_rpc->uang_muka;

        // DB::beginTransaction();
        $insert_data_pribadi_rpc = DB::table('brw_user_detail')->updateOrInsert(
            ['brw_id' => $brw_id],
            [
                'brw_id' => $brw_id,
                'nama' => $nama,
                'no_tlp' => $no_hp,
                'ktp' => $no_ktp,
                'npwp' => $no_npwp,
                'status_kawin' => $status_pernikahan,
                'agama' => $agama,
                'domisili_alamat' => $alamat_domisili,
                'domisili_provinsi' => $provinsi_domisili,
                'domisili_kota' => $kota_domisili,
                'domisili_kecamatan' => $kecamatan_domisili,
                'domisili_kelurahan' => $kelurahan_domisili,
                'domisili_kd_pos' => $kode_pos_domisili,
                'bidang_online' => $bidang_online,
                'bidang_pekerjaan' => $bidang_pekerjaan,
                'pekerjaan' => $pekerjaan,
                'pendidikan_terakhir' => $pendidikan
            ]
        );

            $insert_data_pekerjaan_rpc = DB::table('brw_user_detail_penghasilan')->updateOrInsert([
                'brw_id2' => $brw_id
            ],[
                'brw_id2' => $brw_id,
                'pendapatan_borrower' => $penghasilan_perbulan,
                'sumber_pengembalian_dana' => $sumber_pengembalian_dana,
                'nama_perusahaan' => $nama_perusahaan_fixed_income,
                'pengalaman_kerja_tahun' => $pengalaman_kerja_tahun_fixed_income,
                'pengalaman_kerja_bulan' => $pengalaman_kerja_bulan_fixed_income,
                'no_telp' => $no_telpon_perusahaan_fixed_income,

                'usia_tempat_usaha' => $lama_usaha_non_fixed_income,
                'nama_perusahaan' => $nama_usaha_non_fixed_income,
                'no_telp' => $no_hp_non_fixed_income,
            ]);

                $insert_brw_pendanaan = DB::table('brw_pengajuan')->insert([
                    'brw_id' => $brw_id,
                    'pendanaan_tipe' => $tipe_pendanaan,
                    'pendanaan_tujuan' => $tujuan_pendanaan,
                    // 'jenis_rumah'=>$jenis_rumah,
                    'harga_objek_pendanaan' => $harga_objek_pendanaan,
                    'durasi_proyek' => $jangka_waktu,
                    'pendanaan_dana_dibutuhkan' => $nilai_pengajuan_pendanaan,
                    'uang_muka' => $uang_muka,
                    'status'=> '0' //0 status pengajuan baru
                ]);


                // DB::commit();
                return response()->json(['success' => 'Sukses Insert']);

    }

    public function getDataBerandaKpr(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $brw_rekening = DB::table('brw_rekening')->select('total_plafon', 'total_terpakai', 'total_sisa')->where('brw_id', '=', $brw_id)->first();
        $brw_user_detail = DB::table('brw_user_detail')->select('nama', 'tempat_lahir', 'tgl_lahir', 'alamat', 'ktp')->where('brw_id', '=', $brw_id)->first();

        return response()->json([
            'brw_rekening' => $brw_rekening,
            'brw_user_detail' => $brw_user_detail
        ]);
    }

    public function getDataPendanaanKpr(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;

        $brw_pengajuan = DB::table('brw_pengajuan')->select('pengajuan_id', 'status', 'pendanaan_tipe', 'pendanaan_tujuan')->where('brw_id', '=', $brw_id)->get();

        if(!isset($brw_pengajuan[0])){
            $data_brw_pengajuan=[];
        }else{
            $x=0;
            foreach($brw_pengajuan as $item){
                $pendanaan_tipe = $item->pendanaan_tipe == '1' ? 'Dana Rumah' : 'Dana Konstruksi';
                $pendanaan_tujuan = $item->pendanaan_tujuan  == '1' ? 'Pra Kepemilikan Rumah' : 'Kepemilikan Rumah';
                $pengajuan_id = $item->pengajuan_id;

                if($item->status == '0'){
                    $status = 'Proses';
                    $color = '#02613C';
                }else if($item->status == '1'){
                    $status = 'Lanjut';
                    $color = '#02613C';
                }else if($item->status == '2'){
                    $status = 'Ditolak';
                    $color = '#db1414';
                }else{
                    $status = '-';
                }

                $data_brw_pengajuan[$x]=[
                    'pengajuan_id' => $pengajuan_id,
                    'nama_pendanaan' => $pengajuan_id.' '.$pendanaan_tipe.' - '.$pendanaan_tujuan,
                    'status_pendanaan' => $status,
                    'color' => $color
                ];
                $x++;
            }
        }

        return response()->json([
            'data_brw_pengajuan' => $data_brw_pengajuan,
        ]);
    }

    public function getDataProfileKpr(Request $request){
        $brw_id = Auth::guard('borrower-api-mobile')->user()->brw_id;
        $username = Auth::guard('borrower-api-mobile')->user()->username;

        return response()->json([
            'brw_id' => $brw_id,
            'username'=> $username
        ]);

    }

}
