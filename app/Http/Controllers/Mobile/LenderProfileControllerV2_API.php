<?php

namespace App\Http\Controllers\Mobile;

use App\Constants\JenisKelamin;
use App\DetilInvestor;
use App\Http\Controllers\Controller;
use App\MasterNegara;
use App\MasterProvinsi;
use App\Services\InvestorService;
use App\Services\KYCService;
use App\Services\LenderFileService;
use App\Services\OTPService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class LenderProfileControllerV2_API extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['only' => [
            'profile', 'updateProfile',
            'inheritance', 'updateInheritance',
            'education', 'updateEducation',
        ]]);
    }

    public function profile()
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $detilInvestorLokasi = InvestorService::getDetilInvestorLokasi($investor);
        $investorIdOriginalOwner = $detilInvestor->investor_id;

        $check = static function ($investorId) use ($investorIdOriginalOwner) {
            return $investorId === $investorIdOriginalOwner;
        };

        $noPassporLender = $detilInvestor->no_passpor_investor;
        if (trim($noPassporLender) === '') {
            $noPassporLender = null;
        }

        return response()->json([
            'jenis_identitas' => $detilInvestor->jenis_identitas,
            'username' => $investor->username,
            'nik' => $detilInvestor->no_ktp_investor,
            'no_paspor' => $noPassporLender,
            'nama' => $detilInvestor->nama_investor,
            'tempat_lahir' => $detilInvestor->tempat_lahir_investor,
            'jenis_kelamin' => $detilInvestor->jenis_kelamin_investor,
            'tanggal_lahir' => $detilInvestor->tgl_lahir_investor,
            'agama' => $detilInvestor->agama_investor,
            'status_pernikahan' => $detilInvestor->status_kawin_investor,
            'kewarganegaraan' => $detilInvestor->domisili_negara,
            'warga_negara' => $detilInvestor->warganegara,
            'alamat' => $detilInvestor->alamat_investor,
            'provinsi' => $detilInvestor->provinsi_investor,
            'provinsi_nama' => optional(MasterProvinsi::query()->where('kode_provinsi', $detilInvestor->provinsi_investor)->first(), function ($value) {
                return $value->nama_provinsi;
            }),
            'kecamatan' => $detilInvestor->kecamatan,
            'kabupaten' => $detilInvestor->kota_investor,
            'kabupaten_nama' => optional(MasterProvinsi::query()->where('kode_kota', $detilInvestor->kota_investor)->first(), function ($value) {
                return $value->nama_kota;
            }),
            'kelurahan' => $detilInvestor->kelurahan,
            'kode_pos' => $detilInvestor->kode_pos_investor ?? "",
            'long' => $detilInvestorLokasi->longitude,
            'lat' => $detilInvestorLokasi->latitude,
            'alt' => $detilInvestorLokasi->altitude,
            'email' => $investor->email,
            'no_telp' => $detilInvestor->phone_investor,
            'ktp' => $detilInvestor->pic_ktp_investor,
            'ktp_full' => LenderFileService::makeUrl($detilInvestor->pic_ktp_investor, $check),
            'profile_ktp' => $detilInvestor->pic_user_ktp_investor,
            'profile_ktp_full' => LenderFileService::makeUrl($detilInvestor->pic_user_ktp_investor, $check),
            'profile' => $detilInvestor->pic_investor,
            'profile_full' => LenderFileService::makeUrl($detilInvestor->pic_investor, $check),
            'nama_ibu_kandung' => $detilInvestor->nama_ibu_kandung,
            'lender_type' => $detilInvestor->tipe_pengguna,
        ]);
    }

    public function updateProfile(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'nik' => $request->filled('is_passport')
                    ? [Rule::unique('detil_investor', 'no_passpor_investor')->ignore($detilInvestor->investor_id, 'investor_id')]
                    : ['digits:16', Rule::unique('detil_investor', 'no_ktp_investor')->ignore($detilInvestor->investor_id, 'investor_id')],
                // 'nama' => ['max:191'],
                'username' => ['max:191'],
                // 'tempat_lahir' => ['max:191'],
                'jenis_kelamin' => [Rule::in([JenisKelamin::LAKILAKI, JenisKelamin::PEREMPUAN])],
                'tanggal_lahir' => ['date_format:d-m-Y'],
                'agama' => [Rule::exists('m_agama', 'id_agama')],
                'status_pernikahan' => [Rule::exists('m_kawin', 'id_kawin')],
                'kewarganegaraan' => [Rule::exists('m_negara', 'id_negara')],
                'warga_negara' => [Rule::exists('m_negara', 'id_negara')],
                'alamat' => ['max:191'],
                'provinsi' => ['required', Rule::exists('m_provinsi_kota', 'kode_provinsi')],
                'kabupaten' => ['required', Rule::exists('m_provinsi_kota', 'kode_kota')],
                'kecamatan' => [Rule::requiredIf(function () use ($request) {
                    return (int) $request->kewarganegaraan === MasterNegara::INDONESIA;
                })],
                'kelurahan' => [Rule::requiredIf(function () use ($request) {
                    return (int) $request->kewarganegaraan === MasterNegara::INDONESIA;
                })],
                'kode_pos' => ['nullable', 'numeric', 'digits:5', Rule::exists('m_kode_pos', 'kode_pos'), Rule::requiredIf(function () use ($request) {
                    return (int) $request->kewarganegaraan === MasterNegara::INDONESIA;
                })],
                // 'long' => [],
                // 'lat' => [],
                // 'alt' => [],
                'email' => ['email'],
                'no_telp' => [Rule::unique('detil_investor', 'phone_investor')->ignore($detilInvestor->investor_id, 'investor_id')],
                'ktp' => ['mimes:jpeg,jpg,bmp,png'],
                'profile_ktp' => ['mimes:jpeg,jpg,bmp,png'],
                'profile' => ['mimes:jpeg,jpg,bmp,png'],
            ])
                ->validate();

            if ($request->filled('nik')) {
                if (! $request->filled('is_passport')) {
                    $nikData = KYCService::parseNik($request->get('nik'));
                    $dob = Carbon::parse($nikData['dob'])->format('d-m-Y');
                    $gender = $nikData['gender'];
                }
                $detilInvestor->forceFill([
                    'no_ktp_investor' => null,
                    'no_passpor_investor' => null,
                ])->save();
            }
            $isPassport = strlen($request->get('nik')) < 16;

            $data = $request->all();
            $investor->forceFill([
                'username' => $data['username'] ?? $investor['username'],
            ])
                ->save();
            $detilInvestor->forceFill(InvestorService::mapToDatabase($request->toArray(), [
                'nik' => $isPassport
                    ? 'no_passpor_investor'
                    : 'no_ktp_investor',
                'nama' => 'nama_investor',
                'jenis_kelamin' => 'jenis_kelamin_investor',
                'tempat_lahir' => 'tempat_lahir_investor',
                'agama' => 'agama_investor',
                'status_pernikahan' => 'status_kawin_investor',
                // 'kewarganegaraan' => 'warganegara',
                'alamat' => 'alamat_investor',
                'provinsi' => 'provinsi_investor',
                'kecamatan' => 'kecamatan',
                'kabupaten' => 'kota_investor',
                'kelurahan' => 'kelurahan',
                'kode_pos' => 'kode_pos_investor',
                'email' => 'email',
                'no_telp' => 'phone_investor',
                'nama_ibu_kandung' => 'nama_ibu_kandung'
            ]))
                ->forceFill([
                    'tgl_lahir_investor' => $dob ?? ($request->filled('tanggal_lahir') ? Carbon::createFromFormat('d-m-Y', $request->get('tanggal_lahir'))->format('d-m-Y') : $detilInvestor->tgl_lahir_investor),
                    'jenis_kelamin_investor' => $gender ?? $request->get('jenis_kelamin') ?: $detilInvestor->jenis_kelamin_investor,
                    'jenis_identitas' => $isPassport ? DetilInvestor::JENIS_PASPORT : DetilInvestor::JENIS_NIK,
                    'domisili_negara' => $request->filled('kewarganegaraan') ? $request->get('kewarganegaraan') : $detilInvestor->domisili_negara,
                    'warganegara' => $request->filled('warga_negara')
                        ? $request->get('warga_negara')
                        : ($detilInvestor->warganegara ?: $request->get('kewarganegaraan')),
                ])
                ->save();

            if ((int) $detilInvestor->domisili_negara !== MasterNegara::INDONESIA) {
                $detilInvestor->forceFill([
                    'kelurahan' => null,
                    'kecamatan' => null,
                    'kode_pos_investor' => null,
                    'kota_investor' => MasterProvinsi::KOTA_LAINLAIN,
                    'provinsi_investor' => MasterProvinsi::PROVINSI_LAINLAIN,
                ])
                    ->save();
            }

            if ($request->hasFile('profile')) {
                $detilInvestor->forceFill(['pic_investor' =>
                    LenderFileService::uploadFile($request->file('profile'), 'pic_investor', $detilInvestor->investor_id),
                ])->save();
                $messages['profile'] = 'success';
            }

            if ($request->hasFile('ktp')) {
                $detilInvestor->forceFill(['pic_ktp_investor' =>
                    LenderFileService::uploadFile($request->file('ktp'), 'pic_ktp_investor', $detilInvestor->investor_id),
                ])->save();
                $messages['ktp'] = 'success';
            }

            if ($request->hasFile('profile_ktp')) {
                $detilInvestor->forceFill(['pic_user_ktp_investor' =>
                    LenderFileService::uploadFile($request->file('profile_ktp'), 'pic_user_ktp_investor', $detilInvestor->investor_id),
                ])->save();
                $messages['profile_ktp'] = 'success';
            }

            DB::commit();

            return response()->json(['success' => 'Data diri berhasil diperbaharui', 'files' => $messages ?? []]);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public function inheritance(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $ahliWaris = InvestorService::getAhliWaris();

        return response()->json([
            // 'ibu_kandung' => $detilInvestor->nama_ibu_kandung,
            'ahli_waris' => $ahliWaris->nama_ahli_waris,
            'nik' => $ahliWaris->nik_ahli_waris,
            'hubungan' => $ahliWaris->hubungan_keluarga_ahli_waris,
            'kode_operator' => $ahliWaris->kode_operator,
            'telp' => $ahliWaris->no_hp_ahli_waris,
            'alamat' => $ahliWaris->alamat_ahli_waris,
        ]);
    }

    public function updateInheritance(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);
        $ahliWaris = InvestorService::getAhliWaris();

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                // 'ibu_kandung' => ['required', 'max:191'],
                'ahli_waris' => ['required', 'max:191'],
                'nik' => ['required', 'digits:16'],
                'hubungan' => ['required', Rule::exists('m_hub_ahli_waris', 'id_hub_ahli_waris')],
                'telp' => ['required', 'numeric'],
                'alamat' => ['required', 'max:100'],
            ])
                ->validate();

            // $detilInvestor->forceFill(InvestorService::mapToDatabase($request->toArray(), [
            //     'ibu_kandung' => 'nama_ibu_kandung',
            // ]))->save();
            $ahliWaris->forceFill(InvestorService::mapToDatabase($request->toArray(), [
                'ahli_waris' => 'nama_ahli_waris',
                'nik' => 'nik_ahli_waris',
                'hubungan' => 'hubungan_keluarga_ahli_waris',
                'alamat' => 'alamat_ahli_waris',
                'kode_operator' => 'kode_operator',
            ]))
                ->forceFill([
                    'no_hp_ahli_waris' => OTPService::trim($request->get('telp')),
                ])
                ->save();

            DB::commit();

            return response()->json(['success' => 'Data ahli waris berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public function education(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        return response()->json([
            'pendidikan' => $detilInvestor->pendidikan_investor,
            'pekerjaan' => $detilInvestor->pekerjaan_investor,
            'bidang_pekerjaan' => $detilInvestor->bidang_pekerjaan,
            'bidang_online' => $detilInvestor->online_investor,
            'lama_kerja' => $detilInvestor->pengalaman_investor,
            'estimasi_penghasilan' => $detilInvestor->pendapatan_investor,
            'sumber_penghasilan' => $detilInvestor->sumber_dana,
            'npwp' => $detilInvestor->no_npwp_investor,
        ]);
    }

    public function updateEducation(Request $request)
    {
        $investor = InvestorService::getInvestor();
        $detilInvestor = InvestorService::getDetilInvestor($investor);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'pendidikan' => [Rule::exists('m_pendidikan', 'id_pendidikan')],
                'pekerjaan' => [Rule::exists('m_pekerjaan', 'id_pekerjaan')],
                'bidang_pekerjaan' => [Rule::exists('m_bidang_pekerjaan', 'kode_bidang_pekerjaan')],
                'bidang_online' => [Rule::exists('m_online', 'id_online')],
                'lama_kerja' => [Rule::exists('m_pengalaman_kerja', 'id_pengalaman_kerja')],
                'estimasi_penghasilan' => [Rule::exists('m_pendapatan', 'id_pendapatan')],
                'sumber_penghasilan' => ['max:191'],
                'npwp' => ['numeric', 'digits_between:15,16'],
            ])
                ->validate();

            $detilInvestor->forceFill(InvestorService::mapToDatabase($request->toArray(), [
                'pendidikan' => 'pendidikan_investor',
                'pekerjaan' => 'pekerjaan_investor',
                'bidang_pekerjaan' => 'bidang_pekerjaan',
                'bidang_online' => 'online_investor',
                'lama_kerja' => 'pengalaman_investor',
                'estimasi_penghasilan' => 'pendapatan_investor',
                'sumber_penghasilan' => 'sumber_dana',
                'npwp' => 'no_npwp_investor',
            ]))->save();

            DB::commit();

            return response()->json(['success' => 'Data pendidikan/pekerjaan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }
}
