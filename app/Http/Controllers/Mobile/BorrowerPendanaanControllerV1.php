<?php

namespace App\Http\Controllers\Mobile;

use Exception;
use App\Borrower;
use Carbon\Carbon;
use App\BorrowerDetails;
use App\BorrowerPengajuan;
use Illuminate\Http\Request;
use App\BorrowerTipePendanaan;
use App\MasterTujuanPembiayaan;
use App\Services\AiqqonService;
use Illuminate\Validation\Rule;
use App\Services\DSICoreService;
use App\Services\BorrowerService;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;

use App\Constants\BorrowerPengajuanStatus;
use Illuminate\Validation\ValidationException;
use App\Helpers\Helper;

class BorrowerPendanaanControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['only' => [
            'getFunding',
            'getFundingNew',
            'submitApplication',
            'submitApplicationNew'
        ]]);
    }

     public function getFunding()
    {
        $brwId = BorrowerService::getLoggedInBorrowerId();
        $listPengajuan = BorrowerPengajuan::query()
            ->select([
                'pengajuan_id',
                'status',
                'pendanaan_tipe',
                'pendanaan_tujuan',
                'pendanaan_dana_dibutuhkan',
                'durasi_proyek',
            ])
            ->where('brw_id', $brwId)
            ->get();

        if ($listPengajuan->isEmpty()) {
            return response([
                'error' => 'Data Kosong',
            ], 204);
        }

        $listTipePendanaan = BorrowerTipePendanaan::query()->pluck('pendanaan_nama', 'tipe_id');
        $listTujuanPengajuan = MasterTujuanPembiayaan::query()->pluck('tujuan_pembiayaan', 'id');
        $listStatusVerifikator = BorrowerAnalisaPendanaan::query()->whereIn('pengajuan_id', $listPengajuan->pluck('pengajuan_id'))
            ->pluck('status_verifikator', 'pengajuan_id');

        $response = collect();
        $listPengajuan->each(function (BorrowerPengajuan $pengajuan) use ($listTujuanPengajuan, $listStatusVerifikator, $listTipePendanaan, $response) {
            $response->push([
                'pengajuan_id' => $pengajuan->pengajuan_id,
                /**
                 * @todo Nama mungkin bisa jadi lebih bagus dengan apa ?
                 */
                'nama' => sprintf("%s %s - %s",
                    $pengajuan->pengajuan_id,
                    $listTipePendanaan[$pengajuan->pendanaan_tipe] ?? '**',
                    $listTujuanPengajuan[$pengajuan->pendanaan_tujuan] ?? '**'),
                'status_kode' => $pengajuan->status,
                'status' => BorrowerPengajuanStatus::$statusNames[$pengajuan->status] ?? null,
                'status_alternate' => BorrowerPengajuanStatus::$alternateStatusNames[$pengajuan->status] ?? null,
                'nominal' => $pengajuan->pendanaan_dana_dibutuhkan,
                'tipe' => $pengajuan->pendanaan_tipe,
                'tujuan' => $pengajuan->pendanaan_tujuan,
                'jangka_waktu' => $pengajuan->durasi_proyek,
                /**
                 * @todo Hilangkan magic number 3, ganti dengan const
                 */
                'is_locked' => ($listStatusVerifikator[$pengajuan->pengajuan_id] ?? null) >= 3,
                // 'jenis_properti' => $pengajuan->jenis_properti,
            ]);
        });

        return response($response);
    }

    public function getFundingNew()
    {
        return response(DSICoreService::getPengajuan());
    }

    public function getType(Request $request)
    {
        $enabledPembiayaan = BorrowerTipePendanaan::$enabledPembiayaan[BorrowerDetails::TYPE_INDIVIDU];

        if ($request->bearerToken() !== null && Auth::guard('borrower-api-mobile')->check()) {
            Auth::shouldUse('borrower-api-mobile');

            $borrower = BorrowerService::getBorrower();
            $borrowerDetail = BorrowerService::getDetilBorrower($borrower);

            $borrowerDetail->brw_type !== null
            && $enabledPembiayaan = (BorrowerTipePendanaan::$enabledPembiayaan[$borrowerDetail->brw_type] ?? $enabledPembiayaan);
        }

        $tipe = BorrowerTipePendanaan::query()
            ->select(['pendanaan_nama', 'pendanaan_keterangan', 'tipe_id'])
            ->whereIn('tipe_id', array_keys($enabledPembiayaan))
            ->with(['tujuanPembiayaan' => function ($query) {
                $query->select(['id', 'tujuan_pembiayaan', 'tipe_id', 'keterangan']);
            }])
            ->get();

        return response($tipe);
    }

    public function show(Request $request, BorrowerPengajuan $pengajuan)
    {
        $handler = [
            BorrowerTipePendanaan::DANA_RUMAH => function (BorrowerPengajuan $pengajuan) {
                return [
                    'brw_id' => $pengajuan->brw_id,
                    'tipe_id' => $pengajuan->pendanaan_tipe,
                    'status' => $pengajuan->status,
                    'tujuan' => $pengajuan->pendanaan_tujuan,
                    'harga_objek' => $pengajuan->harga_objek_pendanaan,
                    'uang_muka' => $pengajuan->uang_muka,
                    'jenis_properti' => $pengajuan->jenis_properti,
                    'nilai_pengajuan' => $pengajuan->pendanaan_dana_dibutuhkan,
                    'jangka_waktu' => $pengajuan->durasi_proyek,
                ];
            },
            BorrowerTipePendanaan::DANA_KONSTRUKSI => function (BorrowerPengajuan $pengajuan) {
                $pengajuan->load('materialRequisition');

                return [
                    'brw_id' => $pengajuan->brw_id,
                    'tipe_id' => $pengajuan->pendanaan_tipe,
                    'status' => $pengajuan->status,
                    'tujuan' => $pengajuan->pendanaan_tujuan,
                    'estimasi_mulai' => optional($pengajuan->estimasi_mulai, function ($value) {
                        return Carbon::parse($value)->format('d-m-Y');
                    }),
                    'kebutuhan_dana' => $pengajuan->pendanaan_dana_dibutuhkan,
                    'jangka_waktu' => $pengajuan->durasi_proyek,
                    'purchase_order' => $pengajuan->materialRequisition ? [
                        'nomor' => $pengajuan->materialRequisition->requisition_number,
                        'tanggal' => optional($pengajuan->materialRequisition->requisition_date, function ($value) {
                            return Carbon::parse($value)->format('d-m-Y');
                        }),
                        'status' => $pengajuan->materialRequisition->requisition_status,
                        'alamat_pengiriman' => $pengajuan->materialRequisition->delivery_to_address,
                    ] : null,
                ];
            },

        ];

        return response($handler[$pengajuan->pendanaan_tipe]($pengajuan));
    }


    public function submitApplicationNew(Request $request)
    {

        
          $mapToTipeId = $this->mapTujuanToTipeId();
            
          Validator::make($request->all(), [
            'tujuan' => ['required', Rule::in(array_keys($mapToTipeId->toArray()))],
          ], [], [
            'tujuan' => 'Tujuan Pembiayaan',
          ])->validate();

         try {
    
            $tipeHandler = [
                    BorrowerTipePendanaan::DANA_KONSTRUKSI => function (Request $request) {
                        return $this->danaKonstruksiHandler($request);
                    },
                    BorrowerTipePendanaan::DANA_RUMAH => function (Request $request) {
                        return DSICoreService::entryPengajuan($request);
                    },
            ];

            $response = $tipeHandler[$mapToTipeId[$request->tujuan]]($request);
            return $response;

         }catch (ValidationException $exception) {
            throw $exception;
          
         }catch (Exception $exception) {
             Helper::logDbApp('Error Entry Pengajuan', '200', $exception->getMessage());
             return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
         }

    }

    public function submitApplication(Request $request)
    {
        $mapToTipeId = $this->mapTujuanToTipeId();

        Validator::make($request->all(), [
            'tujuan' => ['required', Rule::in(array_keys($mapToTipeId->toArray()))],
        ], [], [
            'tujuan' => 'Tujuan Pembiayaan',
        ])
            ->validate();

        //DB::beginTransaction();
        try {
            $tipeHandler = [
                BorrowerTipePendanaan::DANA_KONSTRUKSI => function (Request $request) {
                    return $this->danaKonstruksiHandler($request);
                },
                BorrowerTipePendanaan::DANA_RUMAH => function (Request $request) {
                    return $borrowerPengajuan = $this->danaRumahHandler($request);
                },
            ];

            $response = $tipeHandler[$mapToTipeId[$request->tujuan]]($request);

            //DB::commit();

            return $response;
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            //DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    protected function danaRumahHandler(Request $request)
    {
        $borrower = BorrowerService::getBorrower();
        $result = BorrowerService::canSubmitPengajuan($borrower);
        if($result["hasil"] == 0 ) return response()->json($result['responseMessage'], 422);
        
        $tujuan = MasterTujuanPembiayaan::query()->find($request->get('tujuan'));
        Validator::make($request->all(), [
            // Tujuan bailed untuk menghindari salah validasi jangka waktu
            'tujuan' => ['required', 'bail', Rule::exists('m_tujuan_pembiayaan', 'id')],
            'harga_objek' => ['required', 'numeric'],
            'uang_muka' => ['required', 'numeric', 'lte:harga_objek'],
            'jenis_properti' => [Rule::in(array_keys(BorrowerPengajuan::$jenisNames))],
            'nilai_pengajuan' => ['required', 'numeric', 'lte:harga_objek', function ($attribute, $value, $fail) use ($request, $tujuan) {
                if ((int) $value !== ((int) $request->get('harga_objek') - (int) $request->get('uang_muka'))) {
                    $fail('Nilai pengajuan tidak valid');
                }

                if ($value < $tujuan->min_pembiayaan) {
                    $fail('Minimal nilai pengajuan adalah ' . number_format($tujuan->min_pembiayaan, 2));
                }

                
                if ($value > $tujuan->max_pembiayaan) {
                    $fail('Maksimal nilai pengajuan adalah ' . number_format($tujuan->max_pembiayaan, 2));
                }


            }],
            'jangka_waktu' => ['required', 'numeric', function ($attribute, $value, $fail) use ($tujuan) {
                if ($tujuan->max_tenor !== null && (int) $value > $tujuan->max_tenor) {
                    $fail('Jangka waktu tidak valid');
                }
            }],
        ])
            ->validate();

        /** @var BorrowerPengajuan $borrowerPengajuan */
        $borrowerPengajuan = BorrowerPengajuan::query()->make();
        $borrowerPengajuan
            ->forceFill([
                'brw_id' => $borrower->brw_id,
                'pendanaan_tipe' => $tujuan->tipe_id,
                'status' => BorrowerPengajuanStatus::STATUS_BARU,
            ])
            ->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'tujuan' => 'pendanaan_tujuan',
                // 'harga_objek' => 'jenis_properti',
                'harga_objek' => 'harga_objek_pendanaan',
                'uang_muka' => 'uang_muka',
                'jenis_properti' => 'jenis_properti',
                'nilai_pengajuan' => 'pendanaan_dana_dibutuhkan',
                'jangka_waktu' => 'durasi_proyek',
            ]))->save();

        DB::select('select update_user_profile_pengajuan(?, ?) as response', [$borrowerPengajuan->pengajuan_id, 'create']);

        //Create LOS Event
        // event(new SendDataEntryLOSEvent($borrowerPengajuan->pengajuan_id));

        if ($borrower->status === Borrower::STATUS_NOTPREAPPROVED) {
            $borrower->forceFill(['status' => Borrower::STATUS_NOTFILLED])->save();
        }

        return response()->json(['success' => 'Data berhasil diajukan', 'pengajuan_id' => $borrowerPengajuan->pengajuan_id]);
    }

    protected function danaKonstruksiHandler(Request $request)
    {

       
        $isFinal = function () use ($request) {
            return ! $request->filled('draft');
        };
        $borrower = BorrowerService::getBorrower();

        if (! $request->filled('draft')) {
            $result = BorrowerService::canSubmitPengajuan($borrower);
            if ($result["hasil"] === 0) {
                return response()->json($result['responseMessage'], 422);
            }
        }

        $tujuan = MasterTujuanPembiayaan::query()->find($request->get('tujuan'));

        Validator::make($request->all(), [
            'tujuan' => [Rule::requiredIf($isFinal), Rule::exists('m_tujuan_pembiayaan', 'id')],
            'kebutuhan_dana' => [Rule::requiredIf($isFinal), 'numeric', function ($attribute, $value, $fail) use ($tujuan) {
                if ($value < $tujuan->min_pembiayaan) {
                    $fail('Minimal Kebutuhan Dana adalah ' . number_format($tujuan->min_pembiayaan, 2));
                }

                if ($value > $tujuan->max_pembiayaan) {
                    $fail('Maksimal Kebutuhan Dana adalah ' . number_format($tujuan->max_pembiayaan, 2));
                }
            }],
            'estimasi_mulai' => [Rule::requiredIf($isFinal), 'date_format:d-m-Y'],
            'jangka_waktu' => [Rule::requiredIf($isFinal), 'bail', 'numeric', function ($attribute, $value, $fail) use ($tujuan) {
                if ($tujuan->max_tenor !== null && (int) $value > $tujuan->max_tenor) {
                    $fail('Jangka waktu tidak valid');
                }
            }],
            'pengajuan_id' => [function ($attribute, $value, $fail) use ($borrower) {
                $borrowerPengajuan = BorrowerPengajuan::query()->find($value);
                if (! $borrowerPengajuan
                    || ($borrowerPengajuan->brw_id !== $borrower->brw_id)
                    // || $borrowerPengajuan->status !== BorrowerPengajuanStatus::STATUS_DRAFT
                    || $borrowerPengajuan->pendanaan_tipe !== BorrowerTipePendanaan::DANA_KONSTRUKSI
                ) {
                    $fail('pengajuan_id tidak valid');
                }
            }],
            'items' => ['array'],
            'alamat_pengiriman' => ['max:150'],
        ])
            ->validate();

        /** @var BorrowerPengajuan $borrowerPengajuan */
        $borrowerPengajuan = BorrowerPengajuan::query()->find($request->pengajuan_id);

        $isCreation = $borrowerPengajuan === null;
        if ($isCreation) {
            $borrowerPengajuan = BorrowerPengajuan::query()->make();
        } elseif ($borrowerPengajuan->status !== BorrowerPengajuanStatus::STATUS_DRAFT) {
            return response()->json(['error' => 'Data pengajuan tidak dapat diubah karena sudah diajukan', 'pengajuan_id' => $borrowerPengajuan->pengajuan_id]);
        }

        $borrowerPengajuan
            ->forceFill([
                'brw_id' => $borrower->brw_id,
                'pendanaan_tipe' => $tujuan->tipe_id,
                'status' => $request->filled('draft') ? BorrowerPengajuanStatus::STATUS_DRAFT : BorrowerPengajuanStatus::STATUS_BARU,
                'estimasi_mulai' => $request->filled('estimasi_mulai') ? Carbon::createFromFormat('d-m-Y', $request->estimasi_mulai) : null,
            ])
            ->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'tujuan' => 'pendanaan_tujuan',
                'kebutuhan_dana' => 'pendanaan_dana_dibutuhkan',
                'jangka_waktu' => 'durasi_proyek',
            ]))
            ->save();

        self::syncCart($request, $borrowerPengajuan, $request->items);

        if ($isCreation) {
            DB::select('select update_user_profile_pengajuan(?, ?) as response', [$borrowerPengajuan->pengajuan_id, 'create']);
        }

        $message = 'Draft data pengajuan sudah di update';
        if ($isFinal()) {
            $message = 'Data pengajuan berhasil diajukan';
            if ($borrower->status === Borrower::STATUS_NOTPREAPPROVED) {
                $borrower->forceFill(['status' => Borrower::STATUS_NOTFILLED])->save();
            }
        }

        return response()->json(['success' => $message, 'pengajuan_id' => $borrowerPengajuan->pengajuan_id]);
    }

    public function getItems(BorrowerPengajuan $pengajuan)
    {
        /** @var \App\BorrowerMaterialRequisitionHeader $materialRequisition */
        $materialRequisition = $pengajuan->materialRequisition;
        if ($materialRequisition === null) {
            return response([
                'error' => 'Tidak ada barang',
            ], 400);
        }

        $items = $materialRequisition->items;
        if ($items->isEmpty()) {
            return response([
                'error' => 'Tidak ada barang',
            ], 400);
        }

        $images = collect(AiqqonService::call("v3/list-product", [
            'product_id' => $items->pluck('material_item_id')->implode(' '),
        ])['data'])
            ->pluck('image', 'id');

        return $materialRequisition->items()
            ->orderBy('line_number')
            ->get()
            ->map(function ($item) use ($images) {
                return [
                    'id' => $item->material_item_id,
                    'name' => $item->material_item_name,
                    'image' => $images[$item->material_item_id],
                    'category_id' => $item->item_category_id,
                    'description' => $item->item_description,
                    'quantity' => $item->quantity,
                    'unit_price' => $item->unit_price,
                    'task_id' => $item->task_id,
                    'unit' => $item->unit_of_measure,
                    'line_number' => $item->line_number,
                ];
            });
    }

    private static function syncCart(Request $request, BorrowerPengajuan $pengajuan, $items)
    {
        if (empty($items)) {
            return;
        }

        $requestedItems = collect($items)->pluck('id')->unique()->implode(' ');
        $products = collect(AiqqonService::call("v3/list-product", [
            'product_id' => $requestedItems,
        ])['data'])
            ->keyBy('id');

        Validator::make($items, [
            '*.id' => ['required', Rule::in($products->keys())],
            '*.quantity' => ['required', 'numeric'],
            '*.task_id' => ['required', Rule::exists('brw_material_task', 'task_id')],
        ])
            ->validate();

        /** @var \App\BorrowerMaterialRequisitionHeader $materialRequisition */
        $materialRequisition = $pengajuan->materialRequisition;
        if ($materialRequisition === null) {
            $materialRequisition = $pengajuan->materialRequisition()->make();
        }

        $materialRequisition->forceFill([
            'requisition_date' => Carbon::now(),
            'delivery_to_address' => $request->get('alamat_pengiriman') ?: $materialRequisition->delivery_to_address,
            'brw_id' => $pengajuan->brw_id,
            'pengajuan_id' => $pengajuan->pengajuan_id,
            'created_by' => $pengajuan->brw_id,
            'last_updated_by' => $pengajuan->brw_id,
        ])
            ->save();

        $materialRequisition->items()->delete();
        foreach ($items as $key => $item) {
            $itemSrc = $products[$item['id']];
            $itemObj = $materialRequisition->items()->make();
            $itemObj->forceFill(BorrowerService::mapToDatabase($item, [
                'id' => 'material_item_id',
                'quantity' => 'quantity',
                'task_id' => 'task_id',
            ]))
                ->forceFill([
                    'requisition_hdr_id' => $materialRequisition->requisition_hdr_id,
                    'line_number' => $key + 1,
                    'material_item_name' => $itemSrc['nama'],
                    'item_category_id' => $itemSrc['category_id'],
                    'item_description' => $itemSrc['deskripsi'],
                    'unit_price' => $itemSrc['harga'],
                    'created_by' => $pengajuan->brw_id,
                    'last_updated_by' => $pengajuan->brw_id,
                    // 'unit' => $itemSrc[''],
                ])
                ->save();
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function mapTujuanToTipeId(): \Illuminate\Support\Collection
    {
        return MasterTujuanPembiayaan::query()
            // ->whereIn('tipe_id', array_keys(BorrowerTipePendanaan::$enabledPembiayaan))
            ->pluck('tipe_id', 'id');
    }
}
