<?php

namespace App\Http\Controllers\Mobile;

use App\Borrower;
use App\BorrowerDetails;
use App\BorrowerKYCStep;
use App\BorrowerPengurus;
use App\BorrowerRekening;
use App\Helpers\BorrowerFile;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class BorrowerCompanyKYCControllerV1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['only' => [
            'currentStatus',
            'profile', 'updateProfile',
            'profilePic', 'updateProfilePic',
            'address', 'updateAddress',
            'bank', 'updateBank',
            'manager1', 'updateManager1',
            'manager2', 'updateManager2',
            'manager1Pic', 'updateManager1Pic',
            'manager2Pic', 'updateManager2Pic',
        ]]);
    }

    public function allStatus()
    {
        return response()->json(BorrowerKYCStep::query()
            ->where('type', BorrowerKYCStep::TYPE_COMPANY)
            ->orderBy('sort')
            ->pluck('name', 'sort')
        );
    }

    public function currentStatus(Request $request)
    {
        /** @var Borrower $borrower */
        $borrower = Borrower::query()->where('brw_id', $this->getBrwId())->first();

        $status = BorrowerKYCStep::query()
            ->where('name', $borrower->kyc_status)
            ->pluck('name', 'sort');

        if ($status->isEmpty()) {
            /** @var BorrowerKYCStep $firstStatus */
            $firstStatus = BorrowerKYCStep::query()->orderBy('sort')->first();
            $borrower->forceFill([
                'kyc_status' => $firstStatus->name,
            ])
                ->save();
            $status = [(int) $firstStatus->sort => $firstStatus->name];
        }

        return response()->json($status);
    }

    public function profile()
    {
        $detilBorrower = $this->getDetilBorrower();

        return response()->json([
            'nama_perusahaan' => $detilBorrower->nm_bdn_hukum,
            'nib' => $detilBorrower->nib,
            'npwp_perusahaan' => $detilBorrower->npwp_perusahaan,
            'tanggal_berdiri' => Carbon::parse($detilBorrower->tgl_berdiri)->format('d-m-Y'),
            'telp' => $detilBorrower->telpon_perusahaan,
            'bidang_usaha' => $detilBorrower->bidang_usaha,
            'omset_tahun_terakhir' => $detilBorrower->omset_tahun_terakhir,
            'tot_aset_tahun_terakhr' => $detilBorrower->tot_aset_tahun_terakhr,
        ]);
    }

    public function updateProfile(Request $request)
    {
        $detilBorrower = $this->getDetilBorrower();

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'nama_perusahaan' => ['required', 'max:50'],
                'nib' => ['required', 'max:25'],
                'npwp_perusahaan' => ['required', 'max:15'],
                'tanggal_berdiri' => ['required', 'date_format:d-m-Y'],
                'telp' => ['required', 'max:25'],
                'bidang_usaha' => ['required', Rule::exists('m_bidang_pekerjaan', 'id_bidang_pekerjaan')],
                'omset_tahun_terakhir' => ['required'],
                'tot_aset_tahun_terakhr' => ['required'],])
                ->validate();

            $data = $request->all();
            $detilBorrower->forceFill([
                'nm_bdn_hukum' => $data['nama_perusahaan'],
                'nib' => $data['nib'],
                'npwp_perusahaan' => $data['npwp_perusahaan'],
                'tgl_berdiri' => $data['tanggal_berdiri'],
                'telpon_perusahaan' => $data['telp'],
                'bidang_usaha' => $data['bidang_usaha'],
                'omset_tahun_terakhir' => $data['omset_tahun_terakhir'],
                'tot_aset_tahun_terakhr' => $data['tot_aset_tahun_terakhr'],
            ])
                ->save();

            $this->bumpStatus(BorrowerKYCStep::STEP_COM_DATA);

            DB::commit();

            return response()->json(['success' => 'Data perusahaan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public function profilePic()
    {
        $detilBorrower = $this->getDetilBorrower();

        return response()->json([
            'npwp' => $detilBorrower->foto_npwp_perusahaan,
            'npwp_full' => BorrowerFile::makeUrl($detilBorrower->foto_npwp_perusahaan, BorrowerFile::S_KYC_COMPANY_FOTO, 'npwp'),
        ]);
    }

    public function updateProfilePic(Request $request)
    {
        $detilBorrower = $this->getDetilBorrower();

        Validator::make($request->all(), [
            'npwp' => ['mimes:jpeg,jpg,bmp,png'],
        ])
            ->validate();

        $messages = [];
        if ($request->hasFile('npwp')) {
            Storage::disk('private')->exists($detilBorrower->npwp) && Storage::disk('private')->delete($detilBorrower->npwp);
            $detilBorrower->forceFill([
                'foto_npwp_perusahaan' => $this->upload('npwp', $request->file('npwp'), $detilBorrower->brw_id),
            ])
                ->save();
            $messages['npwp'] = 'success';
        }

        return response()->json($messages);
    }

    public function address(Request $request)
    {
        $detilBorrower = $this->getDetilBorrower();

        return response()->json([
            'provinsi' => $detilBorrower->provinsi,
            'kabupaten' => $detilBorrower->kota,
            'alamat' => $detilBorrower->alamat,
            'domisili_provinsi' => $detilBorrower->domisili_provinsi,
            'domisili_kabupaten' => $detilBorrower->domisili_kota,
            'domisili_alamat' => $detilBorrower->domisili_alamat,
        ]);
    }

    public function updateAddress(Request $request)
    {
        $detilBorrower = $this->getDetilBorrower();

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'provinsi' => ['required', Rule::exists('m_provinsi_kota', 'nama_provinsi')],
                'kabupaten' => ['required', Rule::exists('m_provinsi_kota', 'nama_kota')],
                'alamat' => ['required'],
                'domisili_sama' => ['required', 'boolean'],
                'domisili_provinsi' => ['required_if:domisili_sama,0', Rule::exists('m_provinsi_kota', 'nama_provinsi')],
                'domisili_kabupaten' => ['required_if:domisili_sama,0', Rule::exists('m_provinsi_kota', 'nama_kota')],
                'domisili_alamat' => ['required_if:domisili_sama,0'],
            ])
                ->validate();

            $data = $request->all();
            $detilBorrower->forceFill([
                'provinsi' => $data['provinsi'],
                'kota' => $data['kabupaten'],
                'alamat' => $data['alamat'],
                'domisili_provinsi' => $data['domisili_sama'] ? $data['provinsi'] : $data['domisili_provinsi'],
                'domisili_kota' => $data['domisili_sama'] ? $data['kabupaten'] : $data['domisili_kabupaten'],
                'domisili_alamat' => $data['domisili_sama'] ? $data['alamat'] : $data['domisili_alamat'],
            ])
                ->save();

            $this->bumpStatus(BorrowerKYCStep::STEP_COM_ALAMAT);

            DB::commit();

            return response()->json(['success' => 'Data alamat berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public function bank()
    {
        $brwId = $this->getBrwId();
        $rekening = $this->getRekening($brwId);

        return response()->json([
            'kode_bank' => $rekening->brw_kd_bank,
            'nomor' => $rekening->brw_norek,
            'nama_pemilik' => $rekening->brw_nm_pemilik,
            'kantor_cabang_pembuka' => $rekening->kantor_cabang_pembuka,
        ]);
    }

    public function updateBank(Request $request)
    {
        $brwId = $this->getBrwId();
        $rekening = $this->getRekening($brwId);

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'kode_bank' => ['required', Rule::exists('m_bank', 'kode_bank')],
                'nomor' => ['required', 'numeric'],
                'nama_pemilik' => ['required'],
                'kantor_cabang_pembuka' => ['required'],
            ])
                ->validate();

            $data = $request->all();
            $rekening->forceFill([
                'brw_kd_bank' => $data['kode_bank'],
                'brw_norek' => $data['nomor'],
                'brw_nm_pemilik' => $data['nama_pemilik'],
                'kantor_cabang_pembuka' => $data['kantor_cabang_pembuka'],
            ])
                ->save();

            $this->bumpStatus(BorrowerKYCStep::STEP_COM_REKENING);

            DB::commit();

            return response()->json(['success' => 'Data rekening berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public function manager1()
    {
        return $this->manager($n = 0);
    }

    public function updateManager1(Request $request)
    {
        return $this->updateManager($request, $n = 0);
    }

    public function manager2()
    {
        return $this->manager($n = 1);
    }

    public function updateManager2(Request $request)
    {
        return $this->updateManager($request, $n = 1);
    }

    public function manager1Pic()
    {
        return $this->managerPic($n = 0);
    }

    public function updateManager1Pic(Request $request)
    {
        return $this->updateManagerPic($request, $n = 0);
    }

    public function manager2Pic()
    {
        return $this->managerPic($n = 1);
    }

    public function updateManager2Pic(Request $request)
    {
        return $this->updateManagerPic($request, $n = 1);
    }

    private function manager($n)
    {
        $brwId = $this->getBrwId();
        $pengurus = $this->getPIC($brwId, $n) ?: new BorrowerPengurus();

        return response()->json([
            'nama' => $pengurus->nm_pengurus,
            'nik' => $pengurus->nik_pengurus,
            'tempat_lahir' => $pengurus->tempat_lahir,
            'tanggal_lahir' => Carbon::parse($pengurus->tgl_lahir ?: '1970-01-01')->format('d-m-Y'),
            'agama' => $pengurus->agama,
            'pendidikan' => $pengurus->pendidikan_terakhir,
            'kode_pos' => $pengurus->kode_pos,
            'alamat' => $pengurus->alamat,
        ]);
    }

    public function updateManager(Request $request, $n)
    {
        $brwId = $this->getBrwId();
        $pengurus = $this->getPIC($brwId, $n) ?: new BorrowerPengurus();

        DB::beginTransaction();
        try {
            Validator::make($request->all(), [
                'nama' => ['required', 'max:50'],
                'nik' => ['required', 'digits:16'],
                'tempat_lahir' => ['required', 'max:100'],
                'tanggal_lahir' => ['required', 'date_format:d-m-Y'],
                'agama' => ['required', Rule::exists('m_agama', 'id_agama')],
                'pendidikan' => ['required', Rule::exists('m_pendidikan', 'id_pendidikan')],
                'kode_pos' => ['required', Rule::exists('m_kode_pos', 'id_kode_pos')],
                'alamat' => ['required'],
            ])
                ->validate();

            $data = $request->all();
            $pengurus->forceFill([
                'brw_id' => $brwId,
                'nm_pengurus' => $data['nama'],
                'nik_pengurus' => $data['nik'],
                'tempat_lahir' => $data['tempat_lahir'],
                'tgl_lahir' => Carbon::parse($data['tanggal_lahir']),
                'agama' => $data['agama'],
                'pendidikan_terakhir' => $data['pendidikan'],
                'kode_pos' => $data['kode_pos'],
                'alamat' => $data['alamat'],
            ])
                ->save();

            $this->bumpStatus(BorrowerKYCStep::STEP_COM_PJ . ($n + 1));

            DB::commit();

            return response()->json(['success' => 'Data Penanggung Jawab berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public function managerPic($n)
    {
        $brwId = $this->getBrwId();
        $pengurus = $this->getPIC($brwId, $n) ?: new BorrowerPengurus();

        return response()->json([
            'ktp' => $pengurus->foto_ktp,
            'ktp_full' => BorrowerFile::makeUrl($pengurus->foto_ktp, BorrowerFile::S_KYC_COMPANY_PENGURUS_FOTO, 'ktp'),
            'profile_ktp' => $pengurus->foto_diri_ktp,
            'profile_ktp_full' => BorrowerFile::makeUrl($pengurus->foto_diri_ktp, BorrowerFile::S_KYC_COMPANY_PENGURUS_FOTO, 'profile_ktp'),
            'npwp' => $pengurus->foto_npwp,
            'npwp_full' => BorrowerFile::makeUrl($pengurus->foto_npwp, BorrowerFile::S_KYC_COMPANY_PENGURUS_FOTO, 'npwp'),
        ]);
    }

    public function updateManagerPic(Request $request, $n)
    {
        $brwId = $this->getBrwId();
        $pengurus = $this->getPIC($brwId, $n) ?: new BorrowerPengurus();

        Validator::make($request->all(), [
            'ktp' => ['mimes:jpeg,jpg,bmp,png'],
            'profile_ktp' => ['mimes:jpeg,jpg,bmp,png'],
            'npwp' => ['mimes:jpeg,jpg,bmp,png'],
        ])
            ->validate();

        $messages = [];
        if ($request->hasFile('ktp')) {
            Storage::disk('private')->exists($pengurus->foto_ktp) && Storage::disk('private')->delete($pengurus->foto_ktp);
            $pengurus->forceFill([
                'foto_ktp' => $this->uploadManager('ktp' . $pengurus->pengurus_id, $request->file('ktp'), $pengurus->brw_id),
            ])
                ->save();
            $messages['ktp'] = 'success';
        }

        if ($request->hasFile('profile_ktp')) {
            Storage::disk('private')->exists($pengurus->foto_diri_ktp) && Storage::disk('private')->delete($pengurus->foto_diri_ktp);
            $pengurus->forceFill([
                'foto_diri_ktp' => $this->uploadManager('diri_ktp' . $pengurus->pengurus_id, $request->file('ktp'), $pengurus->brw_id),
            ])
                ->save();
            $messages['profile_ktp'] = 'success';
        }

        if ($request->hasFile('npwp')) {
            Storage::disk('private')->exists($pengurus->foto_npwp) && Storage::disk('private')->delete($pengurus->foto_npwp);
            $pengurus->forceFill([
                'foto_npwp' => $this->uploadManager('npwp' . $pengurus->pengurus_id, $request->file('npwp'), $pengurus->brw_id),
            ])
                ->save();
            $messages['npwp'] = 'success';
        }

        return response()->json($messages);
    }

    private function getBrwId()
    {
        return request()->user()->brw_id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    private function getDetilBorrower()
    {
        $brwId = $this->getBrwId();
        $detilBorrower = BorrowerDetails::query()
            ->where('brw_id', $brwId)
            ->first();

        if ($detilBorrower === null) {
            $detilBorrower = BorrowerDetails::query()->make()->forceFill([
                'brw_id' => $brwId,
            ]);
            $detilBorrower->save();
            $detilBorrower = BorrowerDetails::query()
                ->where('brw_id', $brwId)
                ->first();
        }

        return $detilBorrower;
    }

    /**
     * @param $brwId
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getRekening($brwId)
    {
        $getRekening = function ($brwId) {
            return BorrowerRekening::query()
                ->where('brw_id', $brwId)
                ->first();
        };
        $rekening = $getRekening($brwId);

        if ($rekening === null) {
            BorrowerRekening::query()->make()->forceFill([
                'brw_id' => $brwId,
            ])
                ->save();

            $rekening = $getRekening($brwId);
        }

        return $rekening;
    }

    /**
     * @param $brwId
     * @param $n
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getPIC($brwId, $n)
    {
        $getPICs = function ($brwId) {
            return BorrowerPengurus::query()
                ->where('brw_id', $brwId)
                ->orderBy('pengurus_id')
                ->get();
        };

        return $getPICs($brwId)[$n] ?? null;
    }

    private function upload($column, UploadedFile $file, $brw_id)
    {
        return $file->storeAs("borrower/$brw_id",
            $column . '_kyc_bdn_hkm' . '.' . $file->getClientOriginalExtension(),
            'private');
    }

    private function uploadManager($column, UploadedFile $file, $brw_id)
    {
        return $file->storeAs("borrower/$brw_id",
            $column . '_pengurus_kyc_bdn_hkm' . '.' . $file->getClientOriginalExtension(),
            'private');
    }

    private function bumpStatus(string $newStatus)
    {
        /** @var Borrower $borrower */
        $borrower = Borrower::query()->where('brw_id', $this->getBrwId())->first();

        $statuses = BorrowerKYCStep::query()
            ->where('type', BorrowerKYCStep::TYPE_COMPANY)
            ->whereIn('name', [$newStatus, $borrower->kyc_status])
            ->pluck('sort', 'name');

        $current = $statuses[$borrower->kyc_status];
        $new = $statuses[$newStatus];

        if ($new > $current) {
            $borrower->forceFill([
                'kyc_status' => $newStatus,
            ])
                ->save();
        }
    }
}
