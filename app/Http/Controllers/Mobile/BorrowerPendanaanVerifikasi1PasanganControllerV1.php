<?php

namespace App\Http\Controllers\Mobile;

use App\BorrowerDetailPenghasilan;
use App\BorrowerPengajuan;
use App\Constants\KYCStatus;
use App\MasterKawin;
use App\MasterPekerjaan;
use App\Services\BorrowerService;
use App\Services\KYCService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class BorrowerPendanaanVerifikasi1PasanganControllerV1 extends BorrowerPendanaanVerifikasi1ControllerV1
{
    public function currentStatus(Request $request, BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $borrowerDetil = BorrowerService::getDetilBorrower($borrower);
        $borrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);
        $borrowerPasangan = BorrowerService::getDetilBorrowerPasangan($borrower);

        $incompleteFields = [
            'data_diri' => [],
            'alamat' => [],
            'pekerjaan' => [],
        ];

        $status = [
            'data_diri' => self::checkProfile($pengajuan, $incompleteFields['data_diri']),
            'alamat' => self::checkAddress($pengajuan, $incompleteFields['alamat']),
            'pekerjaan' => $borrowerDetil->skema_pembiayaan === BorrowerDetailPenghasilan::SKEMA_JOINT ? self::checkProfession($pengajuan, $incompleteFields['pekerjaan']) : KYCStatus::COMPLETE_TRUE,
            // 'rekening' => self::pemilikPendanaanCheck($pengajuan),
            'is_locked' => self::isPengajuanLocked($pengajuan),
            'skema_pembiayaan' => $borrowerPenghasilan->skema_pembiayaan,
            'sumber_pengembalian_dana' => $borrowerPenghasilan->sumber_pengembalian_dana,
            'sumber_pengembalian_dana_pasangan' => $borrowerPasangan->sumber_pengembalian_dana,
        ];

        if (config('app.env') !== 'production') {
            $status['incomplete_fields'] = $incompleteFields;
        }

        return response()->json($status);
    }

    public function getProfile(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $borrowerDetil = BorrowerService::getDetilBorrower($borrower);
        $pasangan = BorrowerService::getDetilBorrowerPasangan($borrower);

        return response()->json([
            'jenis_kelamin_borrower' => $borrowerDetil->jns_kelamin,
            'nama' => $pasangan->nama,
            'jenis_kelamin' => $pasangan->jenis_kelamin,
            'nik' => $pasangan->ktp,
            'tempat_lahir' => $pasangan->tempat_lahir,
            'tanggal_lahir' => optional($pasangan->tgl_lahir, function ($value) {
                return Carbon::parse($value)->format('d-m-Y');
            }),
            'no_hp' => $pasangan->no_hp,
            'no_kk' => $pasangan->no_kk,
            'agama' => $pasangan->agama,
            'pendidikan_terakhir' => $pasangan->pendidikan,
            'no_npwp' => $pasangan->npwp,
        ]);
    }

    public function updateProfile(Request $request, BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        DB::beginTransaction();
        $pasangan = BorrowerService::getDetilBorrowerPasangan(BorrowerService::getBorrower($pengajuan->brw_id));
        try {
            Validator::make($request->all(), [
                'nama' => ['max:25'],
                'jenis_kelamin' => [Rule::exists('m_jenis_kelamin', 'id_jenis_kelamin')],
                'nik' => ['digits:16', 'numeric'],
                'tempat_lahir' => ['max:50'],
                'tanggal_lahir' => ['date_format:d-m-Y'],
                'no_hp' => ['digits_between:0,25', 'numeric'],
                'no_kk' => ['digits_between:0,20', 'numeric'],
                'agama' => [Rule::exists('m_agama', 'id_agama')],
                'pendidikan_terakhir' => [Rule::exists('m_pendidikan', 'id_pendidikan')],
                'no_npwp' => ['numeric', 'digits_between:15,16'],
            ])
                ->validate();

            $pasangan->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'nama' => 'nama',
                'jenis_kelamin' => 'jenis_kelamin',
                'nik' => 'ktp',
                'tempat_lahir' => 'tempat_lahir',
                'no_hp' => 'no_hp',
                'no_kk' => 'no_kk',
                'agama' => 'agama',
                'pendidikan_terakhir' => 'pendidikan',
                'no_npwp' => 'npwp',
            ]))
                ->forceFill([
                    'tgl_lahir' => $request->filled('tanggal_lahir') ? Carbon::createFromFormat('d-m-Y', $request->get('tanggal_lahir')) : $pasangan->tgl_lahir,
                ])
                ->save();

            if ($request->filled('nik')) {
                $nikData = KYCService::parseNik($request->get('nik'));
                $dob = $nikData['dob'];
                $gender = $nikData['gender'];
                $pasangan->forceFill([
                    'tgl_lahir' => Carbon::parse($dob),
                    'jenis_kelamin' => $gender,
                ])->save();
            }

            DB::commit();

            return response()->json(['success' => 'Data diri pasangan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public static function checkProfile(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $borrowerDetil = BorrowerService::getDetilBorrowerPasangan(BorrowerService::getBorrower($pengajuan->brw_id));
        $columns = [
            'nama',
            'jenis_kelamin',
            'ktp',
            'tempat_lahir',
            'tgl_lahir',
            'no_hp',
            'no_kk',
            'agama',
            'pendidikan',
            'npwp',
        ];

        return KYCService::baseCheck($columns, $borrowerDetil->toArray(), $incompleteFields);
    }

    public function getAddress(BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        $pasangan = BorrowerService::getDetilBorrowerPasangan(BorrowerService::getBorrower($pengajuan->brw_id));

        return response()->json([
            'provinsi' => $pasangan->provinsi,
            'kota' => $pasangan->kota,
            'kecamatan' => $pasangan->kecamatan,
            'kelurahan' => $pasangan->kelurahan,
            'kode_pos' => $pasangan->kode_pos,
            'alamat' => $pasangan->alamat,
        ]);
    }

    public function updateAddress(Request $request, BorrowerPengajuan $pengajuan)
    {
        self::validateOwnershipAndLock($pengajuan);
        DB::beginTransaction();
        $pasangan = BorrowerService::getDetilBorrowerPasangan(BorrowerService::getBorrower($pengajuan->brw_id));
        try {
            Validator::make($request->all(), [
                'nama_ibu_kandung' => ['max:50'],
                'domisili_provinsi' => [Rule::exists('m_provinsi_kota', 'nama_provinsi')],
                'domisili_kota' => [Rule::exists('m_provinsi_kota', 'nama_kota')],
                'domisili_kecamatan' => [Rule::exists('m_kode_pos', 'kecamatan')],
                'domisili_kelurahan' => [Rule::exists('m_kode_pos', 'kelurahan')],
                'domisili_kode_pos' => [Rule::exists('m_kode_pos', 'kode_pos')],
                'domisili_alamat' => ['max:100'],
                'domisili_status_rumah' => [Rule::exists('m_kepemilikan_rumah', 'id_kepemilikan_rumah')],
            ])
                ->validate();

            $pasangan->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'provinsi' => 'provinsi',
                'kota' => 'kota',
                'kecamatan' => 'kecamatan',
                'kelurahan' => 'kelurahan',
                'kode_pos' => 'kode_pos',
                'alamat' => 'alamat',
            ]))->save();

            DB::commit();

            return response()->json(['success' => 'Data alamat pasangan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public static function checkAddress(BorrowerPengajuan $pengajuan)
    {
        $pasangan = BorrowerService::getDetilBorrowerPasangan(BorrowerService::getBorrower($pengajuan->brw_id));
        $columns = [
            'provinsi',
            'kota',
            'kecamatan',
            'kelurahan',
            'kode_pos',
            'alamat',
        ];

        return KYCService::baseCheck($columns, $pasangan->toArray());
    }

    public function getProfession()
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $pasangan = BorrowerService::getDetilBorrowerPasangan($borrower);

        return response()->json([
            'pekerjaan' => $detilBorrower->pekerjaan_pasangan,
            'bidang_pekerjaan' => $detilBorrower->bd_pekerjaan_pasangan,
            'bidang_online' => $detilBorrower->bd_pekerjaanO_pasangan,
            'sumber_pengembalian_dana' => $pasangan->sumber_pengembalian_dana,
            'nama_perusahaan' => $pasangan->nama_perusahaan,
            'alamat_perusahaan' => $pasangan->alamat_perusahaan,
            'rt' => $pasangan->rt_pekerjaan_pasangan,
            'rw' => $pasangan->rw_pekerjaan_pasangan,
            'provinsi' => $pasangan->provinsi_pekerjaan_pasangan,
            'kabupaten' => $pasangan->kab_kota_pekerjaan_pasangan,
            'kecamatan' => $pasangan->kecamatan_pekerjaan_pasangan,
            'kelurahan' => $pasangan->kelurahan_pekerjaan_pasangan,
            'kode_pos' => $pasangan->kode_pos_pekerjaan_pasangan,
            'no_tlp_usaha' => $pasangan->no_telp_pekerjaan_pasangan,
            'no_hp_usaha' => $pasangan->no_hp_pekerjaan_pasangan,
            'penghasilan_perbulan' => $pasangan->pendapatan_borrower,
            'biaya_hidup_perbulan' => $pasangan->biaya_hidup,
            'nilai_spt' => $pasangan->nilai_spt,
            'detail_penghasilan_lain' => $pasangan->detail_penghasilan_lain_lain,
            'total_penghasilan_lain' => $pasangan->total_penghasilan_lain_lain,
            'surat_izin' => $pasangan->surat_ijin,
            'no_surat_izin' => $pasangan->no_surat_ijin,
            'lama_usaha' => $pasangan->usia_perusahaan,
            'lama_tempat_usaha' => $pasangan->usia_tempat_usaha,
            'usia_perusahaan' => $pasangan->usia_perusahaan,
            'departemen' => $pasangan->departemen,
            'jabatan' => $pasangan->jabatan,
            'masa_kerja_tahun' => $pasangan->masa_kerja_tahun,
            'masa_kerja_bulan' => $pasangan->masa_kerja_bulan,
            'nip' => $pasangan->nip_nrp_nik,
            'nama_hrd' => $pasangan->nama_hrd,
            'no_fixed_line_hrd' => $pasangan->no_fixed_line_hrd,
            'pengalaman_kerja_di_tempat_lain_tahun' => $pasangan->pengalaman_kerja_tahun,
            'pengalaman_kerja_di_tempat_lain_bulan' => $pasangan->pengalaman_kerja_bulan,
            'status_pekerjaan' => $pasangan->status_pekerjaan,
            'bentuk_badan_usaha' => $pasangan->bentuk_badan_usaha,
        ]);
    }

    public function updateProfession(Request $request)
    {
        $borrower = BorrowerService::getBorrower();
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $pasangan = BorrowerService::getDetilBorrowerPasangan($borrower);

        DB::beginTransaction();
        try {
            $single = in_array($detilBorrower->status_kawin, [MasterKawin::BELUM, MasterKawin::JANDADUDA]);
            $isWiraswasta = ($request->pekerjaan === MasterPekerjaan::query()->where('pekerjaan', 'Wiraswasta')->first()->id);
            Validator::make(array_merge($request->all(), [
                '_jenis_pekerjaan' => $isWiraswasta ? 'wiraswasta' : 'non-wiraswasta',
            ]), [
                'pekerjaan' => [Rule::exists('m_pekerjaan', 'id_pekerjaan')],
                'bidang_pekerjaan' => [Rule::exists('m_bidang_pekerjaan', 'id_bidang_pekerjaan')],
                'bidang_online' => [Rule::exists('m_online', 'id_online')],

                // baru
                'npwp' => ['numeric', 'digits_between:15,16'],
                'sumber_pengembalian_dana' => [Rule::in([1, 2])],
                'nama_perusahaan' => ['max:50'],
                'alamat_perusahaan' => ['max:100'],
                'rt' => ['max:5'],
                'rw' => ['max:5'],
                'provinsi' => [Rule::exists('m_provinsi_kota', 'nama_provinsi')],
                'kabupaten' => [Rule::exists('m_kode_pos', 'kota')],
                'kecamatan' => [Rule::exists('m_kode_pos', 'kecamatan')],
                'kode_pos' => [Rule::exists('m_kode_pos', 'kode_pos')],
                'no_tlp_usaha' => ['max:15'],
                'no_hp_usaha' => ['max:15'],
                'penghasilan_perbulan' => ['numeric'],
                'biaya_hidup_perbulan' => ['numeric'],
                'nilai_spt' => ['numeric'],
                'penghasilan_lain_lain' => [Rule::in(0, 1)],
                'detail_penghasilan_lain_lain' => ['max:200', 'required_if:penghasilan_lain_lain,1'],
                'total_penghasilan_lain' => ['numeric', 'required_if:penghasilan_lain_lain,1'],
                'bentuk_badan_usaha' => [Rule::exists('m_bentuk_badan_usaha', 'id')],

                // non wiraswasta
                'surat_izin' => [Rule::in([0, 1])],
                'no_surat_izin' => ['max:50'],
                'lama_usaha' => ['numeric', 'digits_between:0,5'],
                'lama_tempat_usaha' => ['numeric', 'digits_between:0,5'],
                'status_pekerjaan' => ['numeric', Rule::in(array_keys(BorrowerDetailPenghasilan::$statusPegawaiNames))],

                // wiraswasta
                'usia_perusahaan' => ['numeric', 'digits_between:0,5'],
                'nama_departemen' => ['max:50'],
                'jabatan' => ['max:50'],
                'masa_kerja_tahun' => ['numeric', 'digits_between:0,5'],
                'masa_kerja_bulan' => ['numeric', 'digits_between:0,5', 'between:0,11'],
                'nik_nrp' => ['max:35'],
                'nama_hrd' => ['max:50'],
                'no_fixed_line_hrd' => ['max:20'],
                'pengalaman_kerja_di_tempat_lain_tahun' => ['numeric'],
                'pengalaman_kerja_di_tempat_lain_bulan' => ['numeric', 'between:0,11'],
            ])
                ->validate();

            $detilBorrower->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'pekerjaan' => 'pekerjaan_pasangan',
                'bidang_pekerjaan' => 'bd_pekerjaan_pasangan',
                'bidang_online' => 'bd_pekerjaanO_pasangan',
            ]))
                ->save();

            $pasangan->forceFill(BorrowerService::mapToDatabase($request->toArray(), [
                'sumber_pengembalian_dana' => 'sumber_pengembalian_dana',
                'nama_perusahaan' => 'nama_perusahaan',
                'alamat_perusahaan' => 'alamat_perusahaan',
                'rt' => 'rt_pekerjaan_pasangan',
                'rw' => 'rw_pekerjaan_pasangan',
                'provinsi' => 'provinsi_pekerjaan_pasangan',
                'kabupaten' => 'kab_kota_pekerjaan_pasangan',
                'kecamatan' => 'kecamatan_pekerjaan_pasangan',
                'kelurahan' => 'kelurahan_pekerjaan_pasangan',
                'kode_pos' => 'kode_pos_pekerjaan_pasangan',
                'no_tlp_usaha' => 'no_telp_pekerjaan_pasangan',
                'no_hp_usaha' => 'no_hp_pekerjaan_pasangan',
                'penghasilan_perbulan' => 'pendapatan_borrower',
                'biaya_hidup_perbulan' => 'biaya_hidup',
                'nilai_spt' => 'nilai_spt',
                'detail_penghasilan_lain' => 'detail_penghasilan_lain_lain',
                'total_penghasilan_lain' => 'total_penghasilan_lain_lain',
                // non wiraswasta
                'surat_izin' => 'surat_ijin',
                'no_surat_izin' => 'no_surat_ijin',
                'lama_usaha' => 'usia_perusahaan',
                'lama_tempat_usaha' => 'usia_tempat_usaha',
                // wiraswasta
                'usia_perusahaan' => 'usia_perusahaan',
                'departemen' => 'departemen',
                'jabatan' => 'jabatan',
                'masa_kerja_tahun' => 'masa_kerja_tahun',
                'masa_kerja_bulan' => 'masa_kerja_bulan',
                'nip' => 'nip_nrp_nik',
                'nama_hrd' => 'nama_hrd',
                'no_fixed_line_hrd' => 'no_fixed_line_hrd',
                'status_pekerjaan' => 'status_pekerjaan',
                'bentuk_badan_usaha' => 'bentuk_badan_usaha',
                'pengalaman_kerja_di_tempat_lain_tahun' => 'pengalaman_kerja_tahun',
                'pengalaman_kerja_di_tempat_lain_bulan' => 'pengalaman_kerja_bulan',
            ]))
                ->save();

            DB::commit();

            return response()->json(['success' => 'Data pekerjaan pasangan berhasil diperbaharui']);
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }

    public static function checkProfession(BorrowerPengajuan $pengajuan, &$incompleteFields = [])
    {
        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $detilBorrower = BorrowerService::getDetilBorrower($borrower);
        $pasangan = BorrowerService::getDetilBorrowerPasangan($borrower);

        $columns = collect([
            'nama_perusahaan',
            'alamat_perusahaan',
            'rt_pekerjaan_pasangan',
            'rw_pekerjaan_pasangan',
            'provinsi_pekerjaan_pasangan',
            'kab_kota_pekerjaan_pasangan',
            'kecamatan_pekerjaan_pasangan',
            'kelurahan_pekerjaan_pasangan',
            'kode_pos_pekerjaan_pasangan',
            'no_telp_pekerjaan_pasangan',
            'pendapatan_borrower',
            'biaya_hidup',
            'nilai_spt',
            'bentuk_badan_usaha',
        ]);

        $isWiraswasta = ($detilBorrower->pekerjaan === MasterPekerjaan::query()->where('pekerjaan', MasterPekerjaan::WIRASWASTA)->first()->id);
        $isTidakBekerja = ($detilBorrower->pekerjaan === MasterPekerjaan::query()->whereIn('pekerjaan', [MasterPekerjaan::PELAJAR, MasterPekerjaan::TIDAK_BEKERJA])->first()->id);
        if ($isWiraswasta) {
            $columns->push('surat_ijin');

            if ($pasangan->surat_ijin !== 0) {
                $columns->merge([
                    'no_surat_ijin',
                    'usia_perusahaan',
                    'usia_tempat_usaha',
                    'no_hp_pekerjaan_pasangan',
                ]);
            }
        } elseif (! $isTidakBekerja) {
            $columns->merge([
                'usia_perusahaan',
                'departemen',
                'jabatan',
                'masa_kerja_tahun',
                'masa_kerja_bulan',
                'nip_nrp_nik',
                'nama_hrd',
                'no_fixed_line_hrd',
                'status_pekerjaan',
            ]);
        }

        return KYCService::baseCheck($columns->toArray(), $pasangan, $incompleteFields);
    }
}
