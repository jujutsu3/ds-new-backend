<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\Rule;

class LenderForgotPasswordControllerV1 extends Controller
{
    use SendsPasswordResetEmails;

    public function broker()
    {
        return Password::broker('users');
    }

    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => ['required', 'email', Rule::exists('investor', 'email')]]);
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->json([
            'success' => 'Email reset password telah dikirimkan jika email tersebut terdaftar dalam sistem kami.',
        ]);
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param \Illuminate\Http\Request $request
     * @param string $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        if ($response === PasswordBroker::INVALID_USER) {
            Log::info('Email "' . $request->email . '" tidak ditemukan ketika reset password');
        }

        return response()->json([
            'error' => 'Email tersebut tidak terdaftar dalam sistem kami.',
        ]);
    }
}
