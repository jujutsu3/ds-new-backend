<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App;
use DB;

use function GuzzleHttp\json_encode;

class PusdafilController extends Controller
{
    public function get_all_data_pusdafil()
    {

        $data_pusdafil_reg_pengguna = [];
        $data_pusdafil_reg_borrower = [];
        $data_pusdafil_reg_lender = [];
        $data_pusdafil_pengajuan_pinjaman = [];
        $data_pusdafil_pengajuan_pemberi_pinjaman = [];
        $data_pusdafil_transaksi_pinjam_meminjam = [];
        $data_pusdafil_pembayaran_pinjaman = [];

        //REG PENGGUNA
        $select_reg_pengguna = DB::table('rpt_pusdafil_reg_pengguna')->whereNull('date_sent')->get();

        if ($select_reg_pengguna->isEmpty()) {
            $data_pusdafil_reg_pengguna = [];
        } else {
            for ($i = 0; $i < sizeof($select_reg_pengguna); $i++) {

                $ktp = isset($select_reg_pengguna[$i]->no_identitas) ?  substr($select_reg_pengguna[$i]->no_identitas, 0, -3) . 'xxx' : '';
                $npwp = isset($select_reg_pengguna[$i]->no_npwp) ? substr($select_reg_pengguna[$i]->no_npwp, 0, -3) . 'xxx' : '';

                $data_pusdafil_reg_pengguna[$i] = [
                    'id_penyelenggara' => isset($select_reg_pengguna[$i]->id_penyelenggara) ? $select_reg_pengguna[$i]->id_penyelenggara : '',
                    'id_pengguna' => isset($select_reg_pengguna[$i]->id_pengguna) ? $select_reg_pengguna[$i]->id_pengguna : '',
                    'jenis_pengguna' => isset($select_reg_pengguna[$i]->jenis_pengguna) ? $select_reg_pengguna[$i]->jenis_pengguna : '',
                    'tgl_registrasi' => isset($select_reg_pengguna[$i]->tgl_registrasi) ? $select_reg_pengguna[$i]->tgl_registrasi : '',
                    'nama_pengguna' => isset($select_reg_pengguna[$i]->nama_pengguna) ? $select_reg_pengguna[$i]->nama_pengguna : '',
                    'jenis_identitas' => isset($select_reg_pengguna[$i]->jenis_identitas) ? $select_reg_pengguna[$i]->jenis_identitas : '',
                    'no_identitas' => $ktp,
                    'no_npwp' => $npwp,                                                //TIDAK MANDATORY
                    'id_jenis_badan_hukum' => isset($select_reg_pengguna[$i]->id_jenis_badan_hukum) ? $select_reg_pengguna[$i]->id_jenis_badan_hukum : '',
                    'tempat_lahir' => isset($select_reg_pengguna[$i]->tempat_lahir) ? $select_reg_pengguna[$i]->tempat_lahir : '',                                 //TIDAK MANDATORY
                    'tgl_lahir' => isset($select_reg_pengguna[$i]->tgl_lahir) ? $select_reg_pengguna[$i]->tgl_lahir : '',                                          //TIDAK MANDATORY
                    'id_jenis_kelamin' => isset($select_reg_pengguna[$i]->id_jenis_kelamin) ? $select_reg_pengguna[$i]->id_jenis_kelamin : '',
                    'alamat' => isset($select_reg_pengguna[$i]->alamat) ? $select_reg_pengguna[$i]->alamat : '',
                    'id_kota' => isset($select_reg_pengguna[$i]->id_kota) ? $select_reg_pengguna[$i]->id_kota : '',
                    'id_provinsi' => isset($select_reg_pengguna[$i]->id_provinsi) ? $select_reg_pengguna[$i]->id_provinsi : '',
                    'kode_pos' => isset($select_reg_pengguna[$i]->kode_pos) ? $select_reg_pengguna[$i]->kode_pos : '',                                             //TIDAK MANDATORY
                    'id_agama' => isset($select_reg_pengguna[$i]->id_agama) ? $select_reg_pengguna[$i]->id_agama : '',
                    'id_status_perkawinan' => isset($select_reg_pengguna[$i]->id_status_perkawinan) ? $select_reg_pengguna[$i]->id_status_perkawinan : '',
                    'id_pekerjaan' => isset($select_reg_pengguna[$i]->id_pekerjaan) ? $select_reg_pengguna[$i]->id_pekerjaan : '',
                    'id_bidang_pekerjaan' => isset($select_reg_pengguna[$i]->id_bidang_pekerjaan) ? $select_reg_pengguna[$i]->id_bidang_pekerjaan : '',
                    'id_pekerjaan_online' => isset($select_reg_pengguna[$i]->id_pekerjaan_online) ? $select_reg_pengguna[$i]->id_pekerjaan_online : '',
                    'pendapatan' => isset($select_reg_pengguna[$i]->pendapatan) ? $select_reg_pengguna[$i]->pendapatan : '',
                    'pengalaman_kerja' => isset($select_reg_pengguna[$i]->pengalaman_kerja) ? $select_reg_pengguna[$i]->pengalaman_kerja : '',
                    'id_pendidikan' => isset($select_reg_pengguna[$i]->id_pendidikan) ? $select_reg_pengguna[$i]->id_pendidikan : '',
                    'nama_perwakilan' => isset($select_reg_pengguna[$i]->nama_perwakilan) ? $select_reg_pengguna[$i]->nama_perwakilan : '',                        //TIDAK MANDATORY
                    'no_identitas_perwakilan' => isset($select_reg_pengguna[$i]->no_identitas_perwakilan) ? $select_reg_pengguna[$i]->no_identitas_perwakilan : '' //TIDAK MANDATORY
                ];
            };
        }

        //REG BORROWER
        $select_reg_borrower = DB::table('rpt_pusdafil_reg_borrower')->whereNull('date_sent')->get();

        if ($select_reg_borrower->isEmpty()) {
            $data_pusdafil_reg_borrower = [];
        } else {
            for ($i = 0; $i < sizeof($select_reg_borrower); $i++) {
                $data_pusdafil_reg_borrower[$i] = [
                    'id_penyelenggara' => isset($select_reg_borrower[$i]->id_penyelenggara) ? $select_reg_borrower[$i]->id_penyelenggara : '',
                    'id_pengguna' => isset($select_reg_borrower[$i]->id_pengguna) ? $select_reg_borrower[$i]->id_pengguna : '',
                    'id_borrower' => isset($select_reg_borrower[$i]->id_borrower) ? $select_reg_borrower[$i]->id_borrower : '',
                    'total_aset' => isset($select_reg_borrower[$i]->total_aset) ? $select_reg_borrower[$i]->total_aset : '',
                    'status_kepemilikan_rumah' => isset($select_reg_borrower[$i]->status_kepemilikan_rumah) ? $select_reg_borrower[$i]->status_kepemilikan_rumah : '',
                ];
            };
        }

        //REG LENDER
        $select_reg_lender = DB::table('rpt_pusdafil_reg_lender')->whereNull('date_sent')->get();

        if ($select_reg_lender->isEmpty()) {
            $data_pusdafil_reg_lender = [];
        } else {
            for ($i = 0; $i < sizeof($select_reg_lender); $i++) {
                $data_pusdafil_reg_lender[$i] = [
                    'id_penyelenggara' => isset($select_reg_lender[$i]->id_penyelenggara) ? $select_reg_lender[$i]->id_penyelenggara : '',
                    'id_pengguna' => isset($select_reg_lender[$i]->id_pengguna) ? $select_reg_lender[$i]->id_pengguna : '',
                    'id_lender' => isset($select_reg_lender[$i]->id_lender) ? $select_reg_lender[$i]->id_lender : '',
                    'id_negara_domisili' => isset($select_reg_lender[$i]->id_negara_domisili) ? $select_reg_lender[$i]->id_negara_domisili : '',
                    'id_kewarganegaraan' => isset($select_reg_lender[$i]->id_kewarganegaraan) ? $select_reg_lender[$i]->id_kewarganegaraan : '', //HANYA UNTUK LENDER PERORANGAN
                    'sumber_dana' => isset($select_reg_lender[$i]->sumber_dana) ? $select_reg_lender[$i]->sumber_dana : '',
                ];
            };
        }

        //PENGAJUAN PINJAMAN
        $select_pengajuan_pinjaman = DB::table('rpt_pusdafil_pengajuan_pinjaman')->whereNull('date_sent')->get();

        if ($select_pengajuan_pinjaman->isEmpty()) {
            $data_pusdafil_pengajuan_pinjaman = [];
        } else {
            for ($i = 0; $i < sizeof($select_pengajuan_pinjaman); $i++) {
                $data_pusdafil_pengajuan_pinjaman[$i] = [
                    'id_penyelenggara' => isset($select_pengajuan_pinjaman[$i]->id_penyelenggara) ? $select_pengajuan_pinjaman[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_pengajuan_pinjaman[$i]->id_pinjaman) ? $select_pengajuan_pinjaman[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_pengajuan_pinjaman[$i]->id_borrower) ? $select_pengajuan_pinjaman[$i]->id_borrower : '',
                    'id_syariah' => isset($select_pengajuan_pinjaman[$i]->id_syariah) ? $select_pengajuan_pinjaman[$i]->id_syariah : '',
                    'id_status_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->id_status_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->id_status_pengajuan_pinjaman : '',
                    'nama_pinjaman' => isset($select_pengajuan_pinjaman[$i]->nama_pinjaman) ? $select_pengajuan_pinjaman[$i]->nama_pinjaman : '', //TIDAK MANDATORY
                    'tgl_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->tgl_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->tgl_pengajuan_pinjaman : '',
                    'nilai_permohonan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->nilai_permohonan_pinjaman) ? $select_pengajuan_pinjaman[$i]->nilai_permohonan_pinjaman : '',
                    'jangka_waktu_pinjaman' => isset($select_pengajuan_pinjaman[$i]->jangka_waktu_pinjaman) ? $select_pengajuan_pinjaman[$i]->jangka_waktu_pinjaman : '',
                    'satuan_jangka_waktu_pinjaman' => isset($select_pengajuan_pinjaman[$i]->satuan_jangka_waktu_pinjaman) ? $select_pengajuan_pinjaman[$i]->satuan_jangka_waktu_pinjaman : '',
                    'penggunaan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->penggunaan_pinjaman) ? $select_pengajuan_pinjaman[$i]->penggunaan_pinjaman : '',
                    'agunan' => isset($select_pengajuan_pinjaman[$i]->agunan) ? $select_pengajuan_pinjaman[$i]->agunan : '',
                    'jenis_agunan' => isset($select_pengajuan_pinjaman[$i]->jenis_agunan) ? $select_pengajuan_pinjaman[$i]->jenis_agunan : '',
                    'rasio_pinjaman_nilai_agunan' => isset($select_pengajuan_pinjaman[$i]->rasio_pinjaman_nilai_agunan) ? $select_pengajuan_pinjaman[$i]->rasio_pinjaman_nilai_agunan : '', //TIDAK MANDATORY
                    'permintaan_jaminan' => isset($select_pengajuan_pinjaman[$i]->permintaan_jaminan) ? $select_pengajuan_pinjaman[$i]->permintaan_jaminan : '', //TIDAK MANDATORY
                    'rasio_pinjaman_aset' => isset($select_pengajuan_pinjaman[$i]->rasio_pinjaman_aset) ? $select_pengajuan_pinjaman[$i]->rasio_pinjaman_aset : '', //TIDAK MANDATORY
                    'cicilan_bulan' => isset($select_pengajuan_pinjaman[$i]->cicilan_bulan) ? $select_pengajuan_pinjaman[$i]->cicilan_bulan : '', //TIDAK MANDATORY
                    'rating_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->rating_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->rating_pengajuan_pinjaman : '',
                    'nilai_plafond' => isset($select_pengajuan_pinjaman[$i]->nilai_plafond) ? $select_pengajuan_pinjaman[$i]->nilai_plafond : '', //TIDAK MANDATORY
                    'nilai_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->nilai_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->nilai_pengajuan_pinjaman : '',
                    'suku_bunga_pinjaman' => isset($select_pengajuan_pinjaman[$i]->suku_bunga_pinjaman) ? $select_pengajuan_pinjaman[$i]->suku_bunga_pinjaman : '',
                    'satuan_suku_bunga_pinjaman' => isset($select_pengajuan_pinjaman[$i]->satuan_suku_bunga_pinjaman) ? $select_pengajuan_pinjaman[$i]->satuan_suku_bunga_pinjaman : '',
                    'jenis_bunga' => isset($select_pengajuan_pinjaman[$i]->jenis_bunga) ? $select_pengajuan_pinjaman[$i]->jenis_bunga : '',
                    'tgl_mulai_publikasi_pinjaman' => isset($select_pengajuan_pinjaman[$i]->tgl_mulai_publikasi_pinjaman) ? $select_pengajuan_pinjaman[$i]->tgl_mulai_publikasi_pinjaman : '',
                    'rencana_jangka_waktu_publikasi' => isset($select_pengajuan_pinjaman[$i]->rencana_jangka_waktu_publikasi) ? $select_pengajuan_pinjaman[$i]->rencana_jangka_waktu_publikasi : '',
                    'realisasi_jangka_waktu_publikasi' => isset($select_pengajuan_pinjaman[$i]->realisasi_jangka_waktu_publikasi) ? $select_pengajuan_pinjaman[$i]->realisasi_jangka_waktu_publikasi : '',
                    'tgl_mulai_pendanaan' => isset($select_pengajuan_pinjaman[$i]->tgl_mulai_pendanaan) ? $select_pengajuan_pinjaman[$i]->tgl_mulai_pendanaan : '',
                    'frekuensi_pinjaman' => isset($select_pengajuan_pinjaman[$i]->frekuensi_pinjaman) ? $select_pengajuan_pinjaman[$i]->frekuensi_pinjaman : '',
                ];
            };
        }


        //PENGAJUAN PEMBERIAN PINJAMAN
        $select_pengajuan_pemberian_pinjaman = DB::table('rpt_pusdafil_pengajuan_pemberian_pinjaman')->whereNull('date_sent')->get();

        if ($select_pengajuan_pemberian_pinjaman->isEmpty()) {
            $data_pusdafil_pengajuan_pemberi_pinjaman = [];
        } else {
            for ($i = 0; $i < sizeof($select_pengajuan_pemberian_pinjaman); $i++) {
                $data_pusdafil_pengajuan_pemberi_pinjaman[$i] = [
                    'id_penyelenggara' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_penyelenggara) ? $select_pengajuan_pemberian_pinjaman[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_pinjaman) ? $select_pengajuan_pemberian_pinjaman[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_borrower) ? $select_pengajuan_pemberian_pinjaman[$i]->id_borrower : '',
                    'id_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->id_lender : '',
                    'no_perjanjian_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->no_perjanjian_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->no_perjanjian_lender : '',
                    'tgl_perjanjian_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->tgl_perjanjian_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->tgl_perjanjian_lender : '',
                    'tgl_penawaran_pemberian_pinjaman' => isset($select_pengajuan_pemberian_pinjaman[$i]->tgl_penawaran_pemberian_pinjaman) ? $select_pengajuan_pemberian_pinjaman[$i]->tgl_penawaran_pemberian_pinjaman : '',
                    'nilai_penawaran_pinjaman' => isset($select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_pinjaman) ? $select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_pinjaman : '',
                    'nilai_penawaran_disetujui' => isset($select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_disetujui) ? $select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_disetujui : '',
                    'no_va_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->no_va_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->no_va_lender : '',
                ];
            };
        }

        //TRANSAKSI PINJAM MEMINJAM
        $select_transaksi_pinjam_meminjam = DB::table('rpt_pusdafil_transaksi_pinjam_meminjam')->whereNull('date_sent')->get();

        if ($select_transaksi_pinjam_meminjam->isEmpty()) {
            $data_pusdafil_transaksi_pinjam_meminjam = [];
        } else {
            for ($i = 0; $i < sizeof($select_transaksi_pinjam_meminjam); $i++) {
                $data_pusdafil_transaksi_pinjam_meminjam[$i] = [
                    'id_penyelenggara' => isset($select_transaksi_pinjam_meminjam[$i]->id_penyelenggara) ? $select_transaksi_pinjam_meminjam[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->id_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_transaksi_pinjam_meminjam[$i]->id_borrower) ? $select_transaksi_pinjam_meminjam[$i]->id_borrower : '',
                    'id_lender' => isset($select_transaksi_pinjam_meminjam[$i]->id_lender) ? $select_transaksi_pinjam_meminjam[$i]->id_lender : '',
                    'id_transaksi' => isset($select_transaksi_pinjam_meminjam[$i]->id_transaksi) ? $select_transaksi_pinjam_meminjam[$i]->id_transaksi : '',
                    'no_perjanjian_borrower' => isset($select_transaksi_pinjam_meminjam[$i]->no_perjanjian_borrower) ? $select_transaksi_pinjam_meminjam[$i]->no_perjanjian_borrower : '',
                    'tgl_perjanjian_borrower' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_perjanjian_borrower) ? $select_transaksi_pinjam_meminjam[$i]->tgl_perjanjian_borrower : '',
                    'nilai_pendanaan' => isset($select_transaksi_pinjam_meminjam[$i]->nilai_pendanaan) ? $select_transaksi_pinjam_meminjam[$i]->nilai_pendanaan : '',
                    'suku_bunga_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->suku_bunga_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->suku_bunga_pinjaman : '',
                    'satuan_suku_bunga_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->satuan_suku_bunga_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->satuan_suku_bunga_pinjaman : '',
                    'id_jenis_pembayaran' => isset($select_transaksi_pinjam_meminjam[$i]->id_jenis_pembayaran) ? $select_transaksi_pinjam_meminjam[$i]->id_jenis_pembayaran : '',
                    'id_frekuensi_pembayaran' => isset($select_transaksi_pinjam_meminjam[$i]->id_frekuensi_pembayaran) ? $select_transaksi_pinjam_meminjam[$i]->id_frekuensi_pembayaran : '',
                    'nilai_angsuran' => isset($select_transaksi_pinjam_meminjam[$i]->nilai_angsuran) ? $select_transaksi_pinjam_meminjam[$i]->nilai_angsuran : '',
                    'objek_jaminan' => isset($select_transaksi_pinjam_meminjam[$i]->objek_jaminan) ? $select_transaksi_pinjam_meminjam[$i]->objek_jaminan : '', //TIDAK MANDATORY
                    'jangka_waktu_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->jangka_waktu_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->jangka_waktu_pinjaman : '',
                    'satuan_jangka_waktu_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->satuan_jangka_waktu_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->satuan_jangka_waktu_pinjaman : '',
                    'tgl_jatuh_tempo' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_jatuh_tempo) ? $select_transaksi_pinjam_meminjam[$i]->tgl_jatuh_tempo : '',
                    'tgl_pendanaan' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_pendanaan) ? $select_transaksi_pinjam_meminjam[$i]->tgl_pendanaan : '',
                    'tgl_penyaluran_dana' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_penyaluran_dana) ? $select_transaksi_pinjam_meminjam[$i]->tgl_penyaluran_dana : '',
                    'no_ea_transaksi' => isset($select_transaksi_pinjam_meminjam[$i]->no_ea_transaksi) ? $select_transaksi_pinjam_meminjam[$i]->no_ea_transaksi : '',
                    'frekuensi_pendanaan' => isset($select_transaksi_pinjam_meminjam[$i]->frekuensi_pendanaan) ? $select_transaksi_pinjam_meminjam[$i]->frekuensi_pendanaan : '',
                ];
            };
        }

        $select_pembayaran_pinjaman = DB::table('rpt_pusdafil_pembayaran_pinjaman')->whereNull('date_sent')->get();

        if ($select_pembayaran_pinjaman->isEmpty()) {
            $data_pusdafil_pembayaran_pinjaman = [];
        } else {
            for ($i = 0; $i < sizeof($select_pembayaran_pinjaman); $i++) {
                $data_pusdafil_pembayaran_pinjaman[$i] = [
                    'id_penyelenggara' => isset($select_pembayaran_pinjaman[$i]->id_penyelenggara) ? $select_pembayaran_pinjaman[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_pembayaran_pinjaman[$i]->id_pinjaman) ? $select_pembayaran_pinjaman[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_pembayaran_pinjaman[$i]->id_borrower) ? $select_pembayaran_pinjaman[$i]->id_borrower : '',
                    'id_lender' => isset($select_pembayaran_pinjaman[$i]->id_lender) ? $select_pembayaran_pinjaman[$i]->id_lender : '',
                    'id_transaksi' => isset($select_pembayaran_pinjaman[$i]->id_transaksi) ? $select_pembayaran_pinjaman[$i]->id_transaksi : '',
                    'id_pembayaran' => isset($select_pembayaran_pinjaman[$i]->id_pembayaran) ? $select_pembayaran_pinjaman[$i]->id_pembayaran : '',
                    'tgl_jatuh_tempo' => isset($select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo) ? $select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo : '',
                    'tgl_jatuh_tempo_selanjutnya' => isset($select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo_selanjutnya) ? $select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo_selanjutnya : '',
                    'tgl_pembayaran_borrower' => isset($select_pembayaran_pinjaman[$i]->tgl_pembayaran_borrower) ? $select_pembayaran_pinjaman[$i]->tgl_pembayaran_borrower : '', //TIDAK MANDATORY      
                    'tgl_pembayaran_penyelenggara' => isset($select_pembayaran_pinjaman[$i]->tgl_pembayaran_penyelenggara) ? $select_pembayaran_pinjaman[$i]->tgl_pembayaran_penyelenggara : '', //TIDAK MANDATORY
                    'sisa_pinjaman_berjalan' => isset($select_pembayaran_pinjaman[$i]->sisa_pinjaman_berjalan) ? $select_pembayaran_pinjaman[$i]->sisa_pinjaman_berjalan : '',
                    'id_status_pinjaman' => isset($select_pembayaran_pinjaman[$i]->id_status_pinjaman) ? $select_pembayaran_pinjaman[$i]->id_status_pinjaman : '',
                    'tgl_pelunasan_borrower' => isset($select_pembayaran_pinjaman[$i]->tgl_pelunasan_borrower) ? $select_pembayaran_pinjaman[$i]->tgl_pelunasan_borrower : '0000-00-00', //TIDAK MANDATORY
                    'tgl_pelunasan_penyelenggara' => isset($select_pembayaran_pinjaman[$i]->tgl_pelunasan_penyelenggara) ? $select_pembayaran_pinjaman[$i]->tgl_pelunasan_penyelenggara : '0000-00-00', //TIDAK MANDATORY
                    'denda' => isset($select_pembayaran_pinjaman[$i]->denda) ? $select_pembayaran_pinjaman[$i]->denda : '', //TIDAK MANDATORY
                    'nilai_pembayaran' => isset($select_pembayaran_pinjaman[$i]->nilai_pembayaran) ? $select_pembayaran_pinjaman[$i]->nilai_pembayaran : '',
                ];
            };
        }


        $url = 'https://pusdafil.ojk.go.id/PusdafilAPI/pelaporanharian';

        // $client = new Client();
        $client = new Client([
            'verify' => false
        ]);

        $request = $client->post($url, [
            'auth' => [
                config('app.pusdafil_username'),
                config('app.pusdafil_password')
            ],
            'form_params' => [
                'reg_pengguna' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_pengguna),
                'reg_lender' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_lender),
                'reg_borrower' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_borrower),
                'pengajuan_pinjaman' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pinjaman),
                'pengajuan_pemberian_pinjaman' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pemberi_pinjaman),
                'transaksi_pinjam_meminjam' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_transaksi_pinjam_meminjam),
                'pembayaran_pinjaman' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pembayaran_pinjaman)
            ]
        ]);

        $response = json_decode($request->getBody(), true);

        $return_response = $this->pusdafil_return_response($response);

        return $return_response;
    }


    public function get_register_data_pusdafil()
    {

        $data_pusdafil_reg_pengguna = [];
        $array_id_reg_pengguna = [];

        $data_pusdafil_reg_borrower = [];
        $array_id_reg_borrower = [];

        $data_pusdafil_reg_lender = [];
        $array_id_reg_lender = [];

        //REG PENGGUNA
        $select_reg_pengguna = DB::table('rpt_pusdafil_reg_pengguna')->whereNull('date_sent')->get();

        if (sizeof($select_reg_pengguna) > 300) {
            $loop_to_reg_pengguna = 300;
        } else {
            $loop_to_reg_pengguna = sizeof($select_reg_pengguna);
        }

        if ($select_reg_pengguna->isEmpty()) {
            $data_pusdafil_reg_pengguna = [];
            return 'data reg pengguna pusdafil kosong';
        } else {
            for ($i = 0; $i < $loop_to_reg_pengguna; $i++) {

                $ktp = isset($select_reg_pengguna[$i]->no_identitas) ?  substr($select_reg_pengguna[$i]->no_identitas, 0, -3) . 'xxx' : '';
                $npwp = isset($select_reg_pengguna[$i]->no_npwp) ? substr($select_reg_pengguna[$i]->no_npwp, 0, -3) . 'xxx' : '';


                array_push($array_id_reg_pengguna, $select_reg_pengguna[$i]->id_rpt_pusdafil_reg_pengguna);

                $data_pusdafil_reg_pengguna[$i] = [
                    'id_penyelenggara' => isset($select_reg_pengguna[$i]->id_penyelenggara) ? $select_reg_pengguna[$i]->id_penyelenggara : '',
                    'id_pengguna' => isset($select_reg_pengguna[$i]->id_pengguna) ? $select_reg_pengguna[$i]->id_pengguna : '',
                    'jenis_pengguna' => isset($select_reg_pengguna[$i]->jenis_pengguna) ? $select_reg_pengguna[$i]->jenis_pengguna : '',
                    'tgl_registrasi' => isset($select_reg_pengguna[$i]->tgl_registrasi) ? $select_reg_pengguna[$i]->tgl_registrasi : '',
                    'nama_pengguna' => isset($select_reg_pengguna[$i]->nama_pengguna) ? $select_reg_pengguna[$i]->nama_pengguna : '',
                    'jenis_identitas' => isset($select_reg_pengguna[$i]->jenis_identitas) ? $select_reg_pengguna[$i]->jenis_identitas : '',
                    'no_identitas' => $ktp,
                    'no_npwp' => $npwp,                                                //TIDAK MANDATORY
                    'id_jenis_badan_hukum' => isset($select_reg_pengguna[$i]->id_jenis_badan_hukum) ? $select_reg_pengguna[$i]->id_jenis_badan_hukum : '',
                    'tempat_lahir' => isset($select_reg_pengguna[$i]->tempat_lahir) ? $select_reg_pengguna[$i]->tempat_lahir : '',                                 //TIDAK MANDATORY
                    'tgl_lahir' => isset($select_reg_pengguna[$i]->tgl_lahir) ? $select_reg_pengguna[$i]->tgl_lahir : '',                                          //TIDAK MANDATORY
                    'id_jenis_kelamin' => isset($select_reg_pengguna[$i]->id_jenis_kelamin) ? $select_reg_pengguna[$i]->id_jenis_kelamin : '',
                    'alamat' => isset($select_reg_pengguna[$i]->alamat) ? $select_reg_pengguna[$i]->alamat : '',
                    'id_kota' => isset($select_reg_pengguna[$i]->id_kota) ? $select_reg_pengguna[$i]->id_kota : '',
                    'id_provinsi' => isset($select_reg_pengguna[$i]->id_provinsi) ? $select_reg_pengguna[$i]->id_provinsi : '',
                    'kode_pos' => isset($select_reg_pengguna[$i]->kode_pos) ? $select_reg_pengguna[$i]->kode_pos : '',                                             //TIDAK MANDATORY
                    'id_agama' => isset($select_reg_pengguna[$i]->id_agama) ? $select_reg_pengguna[$i]->id_agama : '',
                    'id_status_perkawinan' => isset($select_reg_pengguna[$i]->id_status_perkawinan) ? $select_reg_pengguna[$i]->id_status_perkawinan : '',
                    'id_pekerjaan' => isset($select_reg_pengguna[$i]->id_pekerjaan) ? $select_reg_pengguna[$i]->id_pekerjaan : '',
                    'id_bidang_pekerjaan' => isset($select_reg_pengguna[$i]->id_bidang_pekerjaan) ? $select_reg_pengguna[$i]->id_bidang_pekerjaan : '',
                    'id_pekerjaan_online' => isset($select_reg_pengguna[$i]->id_pekerjaan_online) ? $select_reg_pengguna[$i]->id_pekerjaan_online : '',
                    'pendapatan' => isset($select_reg_pengguna[$i]->pendapatan) ? $select_reg_pengguna[$i]->pendapatan : '',
                    'pengalaman_kerja' => isset($select_reg_pengguna[$i]->pengalaman_kerja) ? $select_reg_pengguna[$i]->pengalaman_kerja : '',
                    'id_pendidikan' => isset($select_reg_pengguna[$i]->id_pendidikan) ? $select_reg_pengguna[$i]->id_pendidikan : '',
                    'nama_perwakilan' => isset($select_reg_pengguna[$i]->nama_perwakilan) ? $select_reg_pengguna[$i]->nama_perwakilan : '',                        //TIDAK MANDATORY
                    'no_identitas_perwakilan' => isset($select_reg_pengguna[$i]->no_identitas_perwakilan) ? $select_reg_pengguna[$i]->no_identitas_perwakilan : '' //TIDAK MANDATORY
                ];
            };
        }

        //REG BORROWER
        // $select_reg_borrower = DB::table('rpt_pusdafil_reg_borrower')->whereNull('date_sent')->get();

        // if(sizeof($select_reg_borrower)>300){
        //     $loop_to_reg_borrower = 300;
        // }else{
        //     $loop_to_reg_borrower = sizeof($select_reg_borrower);
        // }

        // if($select_reg_borrower->isEmpty()){
        //     $data_pusdafil_reg_borrower = [];
        //      return 'data reg borrower kosong';
        // }else{
        //     for($i=0; $i<sizeof($select_reg_borrower); $i++){

        //         array_push($array_id_reg_borrower, $select_reg_borrower[$i]->id_rpt_pusdafil_reg_borrower);


        //         $data_pusdafil_reg_borrower[$i] = [
        //             'id_penyelenggara'=>isset($select_reg_borrower[$i]->id_penyelenggara) ? $select_reg_borrower[$i]->id_penyelenggara : '',
        //             'id_pengguna'=>isset($select_reg_borrower[$i]->id_pengguna) ? $select_reg_borrower[$i]->id_pengguna : '',
        //             'id_borrower'=>isset($select_reg_borrower[$i]->id_borrower) ? $select_reg_borrower[$i]->id_borrower : '',
        //             'total_aset'=>isset($select_reg_borrower[$i]->total_aset) ? $select_reg_borrower[$i]->total_aset : '',
        //             'status_kepemilikan_rumah'=>isset($select_reg_borrower[$i]->status_kepemilikan_rumah) ? $select_reg_borrower[$i]->status_kepemilikan_rumah : '',
        //         ];
        //     };
        // }

        //REG LENDER
        $select_reg_lender = DB::table('rpt_pusdafil_reg_lender')->whereNull('date_sent')->get();

        if (sizeof($select_reg_lender) > 300) {
            $loop_to_reg_lender = 300;
        } else {
            $loop_to_reg_lender = sizeof($select_reg_lender);
        }

        if ($select_reg_lender->isEmpty()) {
            $data_pusdafil_reg_lender = [];
            return 'data reg lender pusdafil kosong';
        } else {
            for ($i = 0; $i < $loop_to_reg_lender; $i++) {

                array_push($array_id_reg_lender, $select_reg_lender[$i]->id_rpt_pusdafil_reg_lender);

                $data_pusdafil_reg_lender[$i] = [
                    'id_penyelenggara' => isset($select_reg_lender[$i]->id_penyelenggara) ? $select_reg_lender[$i]->id_penyelenggara : '',
                    'id_pengguna' => isset($select_reg_lender[$i]->id_pengguna) ? $select_reg_lender[$i]->id_pengguna : '',
                    'id_lender' => isset($select_reg_lender[$i]->id_lender) ? $select_reg_lender[$i]->id_lender : '',
                    'id_negara_domisili' => isset($select_reg_lender[$i]->id_negara_domisili) ? $select_reg_lender[$i]->id_negara_domisili : '',
                    'id_kewarganegaraan' => isset($select_reg_lender[$i]->id_kewarganegaraan) ? $select_reg_lender[$i]->id_kewarganegaraan : '', //HANYA UNTUK LENDER PERORANGAN
                    'sumber_dana' => isset($select_reg_lender[$i]->sumber_dana) ? $select_reg_lender[$i]->sumber_dana : '',
                ];
            };
        }

        $url = 'https://pusdafil.ojk.go.id/PusdafilAPI/pelaporanharian';

        // $client = new Client();
        $client = new Client([
            'verify' => false
        ]);

        $request = $client->post($url, [
            'auth' => [
                config('app.pusdafil_username'),
                config('app.pusdafil_password')
            ],
            'form_params' => [
                'reg_pengguna' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_pengguna),
                'reg_lender' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_lender),
                // 'reg_borrower'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_borrower),
            ]
        ]);

        $response = json_decode($request->getBody(), true);

        $return_response = $this->pusdafil_return_response($response, $array_id_reg_pengguna, $array_id_reg_lender, $array_id_reg_borrower);

        return $return_response;
    }


    public function get_transaction_data_pusdafil()
    {

        //PENGAJUAN PINJAMAN
        $select_pengajuan_pinjaman = DB::table('rpt_pusdafil_pengajuan_pinjaman')->whereNull('date_sent')->get();
        $data_pusdafil_pengajuan_pinjaman = [];

        if ($select_pengajuan_pinjaman->isEmpty()) {
            $data_pusdafil_pengajuan_pinjaman = [];
        } else {
            for ($i = 0; $i < sizeof($select_pengajuan_pinjaman); $i++) {
                $data_pusdafil_pengajuan_pinjaman[$i] = [
                    'id_penyelenggara' => isset($select_pengajuan_pinjaman[$i]->id_penyelenggara) ? $select_pengajuan_pinjaman[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_pengajuan_pinjaman[$i]->id_pinjaman) ? $select_pengajuan_pinjaman[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_pengajuan_pinjaman[$i]->id_borrower) ? $select_pengajuan_pinjaman[$i]->id_borrower : '',
                    'id_syariah' => isset($select_pengajuan_pinjaman[$i]->id_syariah) ? $select_pengajuan_pinjaman[$i]->id_syariah : '',
                    'id_status_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->id_status_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->id_status_pengajuan_pinjaman : '',
                    'nama_pinjaman' => isset($select_pengajuan_pinjaman[$i]->nama_pinjaman) ? $select_pengajuan_pinjaman[$i]->nama_pinjaman : '', //TIDAK MANDATORY
                    'tgl_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->tgl_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->tgl_pengajuan_pinjaman : '',
                    'nilai_permohonan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->nilai_permohonan_pinjaman) ? $select_pengajuan_pinjaman[$i]->nilai_permohonan_pinjaman : '',
                    'jangka_waktu_pinjaman' => isset($select_pengajuan_pinjaman[$i]->jangka_waktu_pinjaman) ? $select_pengajuan_pinjaman[$i]->jangka_waktu_pinjaman : '',
                    'satuan_jangka_waktu_pinjaman' => isset($select_pengajuan_pinjaman[$i]->satuan_jangka_waktu_pinjaman) ? $select_pengajuan_pinjaman[$i]->satuan_jangka_waktu_pinjaman : '',
                    'penggunaan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->penggunaan_pinjaman) ? $select_pengajuan_pinjaman[$i]->penggunaan_pinjaman : '',
                    'agunan' => isset($select_pengajuan_pinjaman[$i]->agunan) ? $select_pengajuan_pinjaman[$i]->agunan : '',
                    'jenis_agunan' => isset($select_pengajuan_pinjaman[$i]->jenis_agunan) ? $select_pengajuan_pinjaman[$i]->jenis_agunan : '',
                    'rasio_pinjaman_nilai_agunan' => isset($select_pengajuan_pinjaman[$i]->rasio_pinjaman_nilai_agunan) ? $select_pengajuan_pinjaman[$i]->rasio_pinjaman_nilai_agunan : '', //TIDAK MANDATORY
                    'permintaan_jaminan' => isset($select_pengajuan_pinjaman[$i]->permintaan_jaminan) ? $select_pengajuan_pinjaman[$i]->permintaan_jaminan : '', //TIDAK MANDATORY
                    'rasio_pinjaman_aset' => isset($select_pengajuan_pinjaman[$i]->rasio_pinjaman_aset) ? $select_pengajuan_pinjaman[$i]->rasio_pinjaman_aset : '', //TIDAK MANDATORY
                    'cicilan_bulan' => isset($select_pengajuan_pinjaman[$i]->cicilan_bulan) ? $select_pengajuan_pinjaman[$i]->cicilan_bulan : '', //TIDAK MANDATORY
                    'rating_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->rating_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->rating_pengajuan_pinjaman : '',
                    'nilai_plafond' => isset($select_pengajuan_pinjaman[$i]->nilai_plafond) ? $select_pengajuan_pinjaman[$i]->nilai_plafond : '', //TIDAK MANDATORY
                    'nilai_pengajuan_pinjaman' => isset($select_pengajuan_pinjaman[$i]->nilai_pengajuan_pinjaman) ? $select_pengajuan_pinjaman[$i]->nilai_pengajuan_pinjaman : '',
                    'suku_bunga_pinjaman' => isset($select_pengajuan_pinjaman[$i]->suku_bunga_pinjaman) ? $select_pengajuan_pinjaman[$i]->suku_bunga_pinjaman : '',
                    'satuan_suku_bunga_pinjaman' => isset($select_pengajuan_pinjaman[$i]->satuan_suku_bunga_pinjaman) ? $select_pengajuan_pinjaman[$i]->satuan_suku_bunga_pinjaman : '',
                    'jenis_bunga' => isset($select_pengajuan_pinjaman[$i]->jenis_bunga) ? $select_pengajuan_pinjaman[$i]->jenis_bunga : '',
                    'tgl_mulai_publikasi_pinjaman' => isset($select_pengajuan_pinjaman[$i]->tgl_mulai_publikasi_pinjaman) ? $select_pengajuan_pinjaman[$i]->tgl_mulai_publikasi_pinjaman : '',
                    'rencana_jangka_waktu_publikasi' => isset($select_pengajuan_pinjaman[$i]->rencana_jangka_waktu_publikasi) ? $select_pengajuan_pinjaman[$i]->rencana_jangka_waktu_publikasi : '',
                    'realisasi_jangka_waktu_publikasi' => isset($select_pengajuan_pinjaman[$i]->realisasi_jangka_waktu_publikasi) ? $select_pengajuan_pinjaman[$i]->realisasi_jangka_waktu_publikasi : '',
                    'tgl_mulai_pendanaan' => isset($select_pengajuan_pinjaman[$i]->tgl_mulai_pendanaan) ? $select_pengajuan_pinjaman[$i]->tgl_mulai_pendanaan : '',
                    'frekuensi_pinjaman' => isset($select_pengajuan_pinjaman[$i]->frekuensi_pinjaman) ? $select_pengajuan_pinjaman[$i]->frekuensi_pinjaman : '',
                ];
            };
        }


        //PENGAJUAN PEMBERIAN PINJAMAN
        $select_pengajuan_pemberian_pinjaman = DB::table('rpt_pusdafil_pengajuan_pemberian_pinjaman')->whereNull('date_sent')->get();
        $data_pusdafil_pengajuan_pemberi_pinjaman = [];

        if ($select_pengajuan_pemberian_pinjaman->isEmpty()) {
            $data_pusdafil_pengajuan_pemberi_pinjaman = [];
        } else {
            for ($i = 0; $i < sizeof($select_pengajuan_pemberian_pinjaman); $i++) {
                $data_pusdafil_pengajuan_pemberi_pinjaman[$i] = [
                    'id_penyelenggara' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_penyelenggara) ? $select_pengajuan_pemberian_pinjaman[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_pinjaman) ? $select_pengajuan_pemberian_pinjaman[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_borrower) ? $select_pengajuan_pemberian_pinjaman[$i]->id_borrower : '',
                    'id_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->id_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->id_lender : '',
                    'no_perjanjian_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->no_perjanjian_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->no_perjanjian_lender : '',
                    'tgl_perjanjian_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->tgl_perjanjian_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->tgl_perjanjian_lender : '',
                    'tgl_penawaran_pemberian_pinjaman' => isset($select_pengajuan_pemberian_pinjaman[$i]->tgl_penawaran_pemberian_pinjaman) ? $select_pengajuan_pemberian_pinjaman[$i]->tgl_penawaran_pemberian_pinjaman : '',
                    'nilai_penawaran_pinjaman' => isset($select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_pinjaman) ? $select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_pinjaman : '',
                    'nilai_penawaran_disetujui' => isset($select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_disetujui) ? $select_pengajuan_pemberian_pinjaman[$i]->nilai_penawaran_disetujui : '',
                    'no_va_lender' => isset($select_pengajuan_pemberian_pinjaman[$i]->no_va_lender) ? $select_pengajuan_pemberian_pinjaman[$i]->no_va_lender : '',
                ];
            };
        }

        //TRANSAKSI PINJAM MEMINJAM
        $select_transaksi_pinjam_meminjam = DB::table('rpt_pusdafil_transaksi_pinjam_meminjam')->whereNull('date_sent')->get();
        $data_pusdafil_transaksi_pinjam_meminjam = [];

        if ($select_transaksi_pinjam_meminjam->isEmpty()) {
            $data_pusdafil_transaksi_pinjam_meminjam = [];
        } else {
            for ($i = 0; $i < sizeof($select_transaksi_pinjam_meminjam); $i++) {
                $data_pusdafil_transaksi_pinjam_meminjam[$i] = [
                    'id_penyelenggara' => isset($select_transaksi_pinjam_meminjam[$i]->id_penyelenggara) ? $select_transaksi_pinjam_meminjam[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->id_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_transaksi_pinjam_meminjam[$i]->id_borrower) ? $select_transaksi_pinjam_meminjam[$i]->id_borrower : '',
                    'id_lender' => isset($select_transaksi_pinjam_meminjam[$i]->id_lender) ? $select_transaksi_pinjam_meminjam[$i]->id_lender : '',
                    'id_transaksi' => isset($select_transaksi_pinjam_meminjam[$i]->id_transaksi) ? $select_transaksi_pinjam_meminjam[$i]->id_transaksi : '',
                    'no_perjanjian_borrower' => isset($select_transaksi_pinjam_meminjam[$i]->no_perjanjian_borrower) ? $select_transaksi_pinjam_meminjam[$i]->no_perjanjian_borrower : '',
                    'tgl_perjanjian_borrower' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_perjanjian_borrower) ? $select_transaksi_pinjam_meminjam[$i]->tgl_perjanjian_borrower : '',
                    'nilai_pendanaan' => isset($select_transaksi_pinjam_meminjam[$i]->nilai_pendanaan) ? $select_transaksi_pinjam_meminjam[$i]->nilai_pendanaan : '',
                    'suku_bunga_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->suku_bunga_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->suku_bunga_pinjaman : '',
                    'satuan_suku_bunga_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->satuan_suku_bunga_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->satuan_suku_bunga_pinjaman : '',
                    'id_jenis_pembayaran' => isset($select_transaksi_pinjam_meminjam[$i]->id_jenis_pembayaran) ? $select_transaksi_pinjam_meminjam[$i]->id_jenis_pembayaran : '',
                    'id_frekuensi_pembayaran' => isset($select_transaksi_pinjam_meminjam[$i]->id_frekuensi_pembayaran) ? $select_transaksi_pinjam_meminjam[$i]->id_frekuensi_pembayaran : '',
                    'nilai_angsuran' => isset($select_transaksi_pinjam_meminjam[$i]->nilai_angsuran) ? $select_transaksi_pinjam_meminjam[$i]->nilai_angsuran : '',
                    'objek_jaminan' => isset($select_transaksi_pinjam_meminjam[$i]->objek_jaminan) ? $select_transaksi_pinjam_meminjam[$i]->objek_jaminan : '', //TIDAK MANDATORY
                    'jangka_waktu_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->jangka_waktu_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->jangka_waktu_pinjaman : '',
                    'satuan_jangka_waktu_pinjaman' => isset($select_transaksi_pinjam_meminjam[$i]->satuan_jangka_waktu_pinjaman) ? $select_transaksi_pinjam_meminjam[$i]->satuan_jangka_waktu_pinjaman : '',
                    'tgl_jatuh_tempo' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_jatuh_tempo) ? $select_transaksi_pinjam_meminjam[$i]->tgl_jatuh_tempo : '',
                    'tgl_pendanaan' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_pendanaan) ? $select_transaksi_pinjam_meminjam[$i]->tgl_pendanaan : '',
                    'tgl_penyaluran_dana' => isset($select_transaksi_pinjam_meminjam[$i]->tgl_penyaluran_dana) ? $select_transaksi_pinjam_meminjam[$i]->tgl_penyaluran_dana : '',
                    'no_ea_transaksi' => isset($select_transaksi_pinjam_meminjam[$i]->no_ea_transaksi) ? $select_transaksi_pinjam_meminjam[$i]->no_ea_transaksi : '',
                    'frekuensi_pendanaan' => isset($select_transaksi_pinjam_meminjam[$i]->frekuensi_pendanaan) ? $select_transaksi_pinjam_meminjam[$i]->frekuensi_pendanaan : '',
                ];
            };
        }

        $select_pembayaran_pinjaman = DB::table('rpt_pusdafil_pembayaran_pinjaman')->whereNull('date_sent')->get();
        $data_pusdafil_pembayaran_pinjaman = [];

        if ($select_pembayaran_pinjaman->isEmpty()) {
            $data_pusdafil_pembayaran_pinjaman = [];
        } else {
            for ($i = 0; $i < sizeof($select_pembayaran_pinjaman); $i++) {
                $data_pusdafil_pembayaran_pinjaman[$i] = [
                    'id_penyelenggara' => isset($select_pembayaran_pinjaman[$i]->id_penyelenggara) ? $select_pembayaran_pinjaman[$i]->id_penyelenggara : '',
                    'id_pinjaman' => isset($select_pembayaran_pinjaman[$i]->id_pinjaman) ? $select_pembayaran_pinjaman[$i]->id_pinjaman : '',
                    'id_borrower' => isset($select_pembayaran_pinjaman[$i]->id_borrower) ? $select_pembayaran_pinjaman[$i]->id_borrower : '',
                    'id_lender' => isset($select_pembayaran_pinjaman[$i]->id_lender) ? $select_pembayaran_pinjaman[$i]->id_lender : '',
                    'id_transaksi' => isset($select_pembayaran_pinjaman[$i]->id_transaksi) ? $select_pembayaran_pinjaman[$i]->id_transaksi : '',
                    'id_pembayaran' => isset($select_pembayaran_pinjaman[$i]->id_pembayaran) ? $select_pembayaran_pinjaman[$i]->id_pembayaran : '',
                    'tgl_jatuh_tempo' => isset($select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo) ? $select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo : '',
                    'tgl_jatuh_tempo_selanjutnya' => isset($select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo_selanjutnya) ? $select_pembayaran_pinjaman[$i]->tgl_jatuh_tempo_selanjutnya : '',
                    'tgl_pembayaran_borrower' => isset($select_pembayaran_pinjaman[$i]->tgl_pembayaran_borrower) ? $select_pembayaran_pinjaman[$i]->tgl_pembayaran_borrower : '', //TIDAK MANDATORY      
                    'tgl_pembayaran_penyelenggara' => isset($select_pembayaran_pinjaman[$i]->tgl_pembayaran_penyelenggara) ? $select_pembayaran_pinjaman[$i]->tgl_pembayaran_penyelenggara : '', //TIDAK MANDATORY
                    'sisa_pinjaman_berjalan' => isset($select_pembayaran_pinjaman[$i]->sisa_pinjaman_berjalan) ? $select_pembayaran_pinjaman[$i]->sisa_pinjaman_berjalan : '',
                    'id_status_pinjaman' => isset($select_pembayaran_pinjaman[$i]->id_status_pinjaman) ? $select_pembayaran_pinjaman[$i]->id_status_pinjaman : '',
                    'tgl_pelunasan_borrower' => isset($select_pembayaran_pinjaman[$i]->tgl_pelunasan_borrower) ? $select_pembayaran_pinjaman[$i]->tgl_pelunasan_borrower : '0000-00-00', //TIDAK MANDATORY
                    'tgl_pelunasan_penyelenggara' => isset($select_pembayaran_pinjaman[$i]->tgl_pelunasan_penyelenggara) ? $select_pembayaran_pinjaman[$i]->tgl_pelunasan_penyelenggara : '0000-00-00', //TIDAK MANDATORY
                    'denda' => isset($select_pembayaran_pinjaman[$i]->denda) ? $select_pembayaran_pinjaman[$i]->denda : '', //TIDAK MANDATORY
                    'nilai_pembayaran' => isset($select_pembayaran_pinjaman[$i]->nilai_pembayaran) ? $select_pembayaran_pinjaman[$i]->nilai_pembayaran : '',
                ];
            };
        }


        $url = 'https://pusdafil.ojk.go.id/PusdafilAPI/pelaporanharian';

        // $client = new Client();
        $client = new Client([
            'verify' => false
        ]);

        $request = $client->post($url, [
            'auth' => [
                config('app.pusdafil_username'),
                config('app.pusdafil_password')
            ],
            'form_params' => [
                'pengajuan_pinjaman' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pinjaman),
                'pengajuan_pemberian_pinjaman' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pemberi_pinjaman),
                'transaksi_pinjam_meminjam' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_transaksi_pinjam_meminjam),
                'pembayaran_pinjaman' => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pembayaran_pinjaman)
            ]
        ]);

        $response = json_decode($request->getBody(), true);

        $return_response = $this->pusdafil_return_response($response);

        return $return_response;
    }


    private function enkripsi_aes256cbc_64encode($data)
    {
        $data_string = json_encode($data);
        if (!defined('AES_256_CBC')) define('AES_256_CBC', 'aes-256-cbc');

        $encryption_key = config('app.pusdafil_key');
        $iv = 'qwerzxcv1234asdf';

        $encrypted = openssl_encrypt($data_string, AES_256_CBC, $encryption_key, 0, $iv);
        $encrypted_full = $encrypted . '::' . $iv;
        $base64encode = base64_encode($encrypted_full);

        return $base64encode;
    }

    private function pusdafil_return_response($response, $array_id_reg_pengguna, $array_id_reg_lender, $array_id_reg_borrower)
    {
        if ($response['request_status'] == 200) {
            for ($i = 0; $i < sizeof($response['data']); $i++) {
                if ($response['data'][$i]['error'] == false && $response['data'][$i]['table'] == 'Reg Pengguna' && isset($response['data'][$i]['count'])) {
                    $update_reg_pengguna = DB::table('rpt_pusdafil_reg_pengguna')->whereNull('date_sent')->whereIn('id_rpt_pusdafil_reg_pengguna', $array_id_reg_pengguna)->update(['date_sent' => Carbon::now()]);
                } else if ($response['data'][$i]['error'] == false && $response['data'][$i]['table'] == 'Reg Lender' && isset($response['data'][$i]['count'])) {
                    $update_reg_lender = DB::table('rpt_pusdafil_reg_lender')->whereNull('date_sent')->whereIn('id_rpt_pusdafil_reg_lender', $array_id_reg_lender)->update(['date_sent' => Carbon::now()]);
                }
                // else if($response['data'][$i]['error']==false && $response['data'][$i]['table'] == 'Reg Borrower' && isset($response['data'][$i]['count'])){
                //     $update_reg_borrower = DB::table('rpt_pusdafil_reg_borrower')->whereNull('date_sent')->update(['date_sent' => Carbon::now()]);
                // }
                // else if($response['data'][$i]['error']==false && $response['data'][$i]['table'] == 'Pengajuan Pinjaman' && isset($response['data'][$i]['count'])){
                //     $update_reg_pengajuan_pinjaman = DB::table('rpt_pusdafil_pengajuan_pinjaman')->whereNull('date_sent')->update(['date_sent' => Carbon::now()]);
                // }
                // else if($response['data'][$i]['error']==false && $response['data'][$i]['table'] == 'Pengajuan Pemberian Pinjaman' && isset($response['data'][$i]['count'])){
                //     $update_pengajuan_pemberian_pinjaman = DB::table('rpt_pusdafil_pengajuan_pemberian_pinjaman')->whereNull('date_sent')->update(['date_sent' => Carbon::now()]);
                // }
                // else if($response['data'][$i]['error']==false && $response['data'][$i]['table'] == 'Transaksi Pinjam Meminjam' && isset($response['data'][$i]['count'])){
                //     $update_transaksi_pinjam_meminjam = DB::table('rpt_pusdafil_transaksi_pinjam_meminjam')->whereNull('date_sent')->update(['date_sent' => Carbon::now()]);
                // }
                // else if($response['data'][$i]['error']==false && $response['data'][$i]['table'] == 'Pembayaran Pinjaman' && isset($response['data'][$i]['count'])){
                //     $update_pembayaran_pinjaman = DB::table('rpt_pusdafil_pembayaran_pinjaman')->whereNull('date_sent')->update(['date_sent' => Carbon::now()]);
                // };
            }
        }

        return $response;
    }














    public function get_all_data_pusdafil_backup()
    {

        $id_all = [
            'id_penyelenggara' => '810059',
            'id_pengguna' => '1',
            'id_borrower' => '33',
            'id_lender' => '444',
            'id_pinjaman' => '1',
            'id_transaksi' => '1'
        ];

        $looping_to = 1;

        $select_reg_pengguna = DB::table('rpt_pusdafil_reg_pengguna')->get();

        $select_reg_pengguna = DB::table('rpt_daily_payout')->get();
        return $select_reg_pengguna[0]->nama;

        for ($i = 0; $i < $looping_to; $i++) {
            $data_pusdafil_reg_pengguna[$i] = [
                'id_penyelenggara' => $id_all['id_penyelenggara'],
                'id_pengguna' => $id_all['id_pengguna'],
                'jenis_pengguna' => 1,
                'tgl_registrasi' => '2020-10-05',
                'nama_pengguna' => 'Suhendri',
                'jenis_identitas' => 1,
                'no_identitas' => '3674070110940003',
                'no_npwp' => '',                              //TIDAK MANDATORY
                'id_jenis_badan_hukum' => 5,
                'tempat_lahir' => '',                         //TIDAK MANDATORY
                'tgl_lahir' => '',                            //TIDAK MANDATORY
                'id_jenis_kelamin' => 1,
                'alamat' => 'Desa Iwul',
                'id_kota' => 'e999',
                'id_provinsi' => 31,
                'kode_pos' => '',                             //TIDAK MANDATORY
                'id_agama' => 1,
                'id_status_perkawinan' => 2,
                'id_pekerjaan' => 4,
                'id_bidang_pekerjaan' => 'e39',
                'id_pekerjaan_online' => 1,
                'pendapatan' => 1,
                'pengalaman_kerja' => 3,
                'id_pendidikan' => 5,
                'nama_perwakilan' => '',                      //TIDAK MANDATORY
                'no_identitas_perwakilan' => ''               //TIDAK MANDATORY
            ];
        };

        $select_reg_borrower = DB::table('rpt_pusdafil_reg_borrower')->get();

        for ($i = 0; $i < $looping_to; $i++) {
            $data_pusdafil_reg_borrower[$i] = [
                'id_penyelenggara' => $id_all['id_penyelenggara'],
                'id_pengguna' => $id_all['id_pengguna'],
                'id_borrower' => $id_all['id_borrower'],
                'total_aset' => 300000000,
                'status_kepemilikan_rumah' => 1,
            ];
        };

        $select_reg_lender = DB::table('rpt_pusdafil_reg_lender')->get();

        for ($i = 0; $i < $looping_to; $i++) {
            $data_pusdafil_reg_lender[$i] = [
                'id_penyelenggara' => $id_all['id_penyelenggara'],
                'id_pengguna' => $id_all['id_pengguna'],
                'id_lender' => $id_all['id_lender'],
                'id_negara_domisili' => 1,
                'id_kewarganegaraan' => 1,        //HANYA UNTUK LENDER PERORANGAN
                'sumber_dana' => 'Pribadi',
            ];
        };

        $select_pengajuan_pinjaman = DB::table('rpt_pusdafil_pengajuan_pinjaman')->get();

        for ($i = 0; $i < $looping_to; $i++) {
            $data_pusdafil_pengajuan_pinjaman[$i] = [
                'id_penyelenggara' => $id_all['id_penyelenggara'],
                'id_pinjaman' => $id_all['id_pinjaman'] + $i,
                'id_borrower' => $id_all['id_borrower'],
                'id_syariah' => 1,
                'id_status_pengajuan_pinjaman' => 1,
                'nama_pinjaman' => '',                             //TIDAK MANDATORY
                'tgl_pengajuan_pinjaman' => '2020-10-30',
                'nilai_permohonan_pinjaman' => 100000000,
                'jangka_waktu_pinjaman' => 3,
                'satuan_jangka_waktu_pinjaman' => 3,
                'penggunaan_pinjaman' => 'e0',
                'agunan' => 1,
                'jenis_agunan' => 1,
                'rasio_pinjaman_nilai_agunan' => '',              //TIDAK MANDATORY
                'permintaan_jaminan' => '',                       //TIDAK MANDATORY
                'rasio_pinjaman_aset' => '',                      //TIDAK MANDATORY
                'cicilan_bulan' => '',                            //TIDAK MANDATORY
                'rating_pengajuan_pinjaman' => 'AA',
                'nilai_plafond' => '',                            //TIDAK MANDATORY
                'nilai_pengajuan_pinjaman' => 500000000,
                'suku_bunga_pinjaman' => 18,
                'satuan_suku_bunga_pinjaman' => 4,
                'jenis_bunga' => 1,
                'tgl_mulai_publikasi_pinjaman' => '2020-11-30',
                'rencana_jangka_waktu_publikasi' => 14,
                'realisasi_jangka_waktu_publikasi' => 14,
                'tgl_mulai_pendanaan' => '2020-11-30',
                'frekuensi_pinjaman' => 80,
            ];
        };

        $select_pengajuan_pemberian_pinjaman = DB::table('rpt_pusdafil_pengajuan_pemberian_pinjaman')->get();

        for ($i = 0; $i < $looping_to; $i++) {
            $data_pusdafil_pengajuan_pemberi_pinjaman[$i] = [
                'id_penyelenggara' => $id_all['id_penyelenggara'],
                'id_pinjaman' => $id_all['id_pinjaman'] + $i,
                'id_borrower' => $id_all['id_borrower'],
                'id_lender' => $id_all['id_lender'],
                'no_perjanjian_lender' => '323123',
                'tgl_perjanjian_lender' => '2020-11-15',
                'tgl_penawaran_pemberian_pinjaman' => '2020-11-15',
                'nilai_penawaran_pinjaman' => 100000000,
                'nilai_penawaran_disetujui' => 50000000,
                'no_va_lender' => '32312323213',
            ];
        };

        $select_transaksi_pinjam_meminjam = DB::table('rpt_pusdafil_transaksi_pinjam_meminjam')->get();

        for ($i = 0; $i < $looping_to; $i++) {
            $data_pusdafil_transaksi_pinjam_meminjam[$i] = [
                'id_penyelenggara' => $id_all['id_penyelenggara'],
                'id_pinjaman' => $id_all['id_pinjaman'] + $i,
                'id_borrower' => $id_all['id_borrower'],
                'id_lender' => $id_all['id_lender'],
                'id_transaksi' => $id_all['id_transaksi'] + $i,
                'no_perjanjian_borrower' => '3213123213',
                'tgl_perjanjian_borrower' => '2020-11-10',
                'nilai_pendanaan' => 100000000,
                'suku_bunga_pinjaman' => 20,
                'satuan_suku_bunga_pinjaman' => 3,
                'id_jenis_pembayaran' => 4,
                'id_frekuensi_pembayaran' => 3,
                'nilai_angsuran' => 100000000,
                'objek_jaminan' => '',                 //TIDAK MANDATORY
                'jangka_waktu_pinjaman' => 3,
                'satuan_jangka_waktu_pinjaman' => 3,
                'tgl_jatuh_tempo' => '2020-11-10',
                'tgl_pendanaan' => '2020-10-10',
                'tgl_penyaluran_dana' => '2020-10-15',
                'no_ea_transaksi' => '32013012302103',
                'frekuensi_pendanaan' => '10',
            ];
        };

        $select_pembayaran_pinjaman = DB::table('rpt_pusdafil_pembayaran_pinjaman')->get();

        for ($i = 0; $i < $looping_to; $i++) {
            $data_pusdafil_pembayaran_pinjaman[$i] = [
                'id_penyelenggara' => $id_all['id_penyelenggara'],
                'id_pinjaman' => $id_all['id_pinjaman'] + $i,
                'id_borrower' => $id_all['id_borrower'],
                'id_lender' => $id_all['id_lender'],
                'id_transaksi' => $id_all['id_transaksi'] + $i,
                'id_pembayaran' => $i,
                'tgl_jatuh_tempo' => '2020-11-10',
                'tgl_jatuh_tempo_selanjutnya' => '2020-12-10',
                'tgl_pembayaran_borrower' => '2020-11-11',       //TIDAK MANDATORY      
                'tgl_pembayaran_penyelenggara' => '2020-11-10',  //TIDAK MANDATORY
                'sisa_pinjaman_berjalan' => 3,
                'id_status_pinjaman' => 1,
                'tgl_pelunasan_borrower' => '0000-00-00',        //TIDAK MANDATORY
                'tgl_pelunasan_penyelenggara' => '0000-00-00',   //TIDAK MANDATORY
                'denda' => '',                                   //TIDAK MANDATORY
                'nilai_pembayaran' => 1000000000,
            ];
        };

        // $url = 'https://pusdafil.ojk.go.id/PusdafilAPI/pelaporanharian';

        // $client = new Client();

        // $request = $client->post($url, [
        //     'auth' => [
        //         'danasyariah', 
        //         'dsi1234'
        //     ],
        //     'form_params' => [
        //         'reg_pengguna'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_pengguna),
        //         'reg_lender'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_lender),
        //         'reg_borrower'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_borrower),
        //         'pengajuan_pinjaman'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pinjaman),
        //         'pengajuan_pemberian_pinjaman'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pemberi_pinjaman),
        //         'transaksi_pinjam_meminjam'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_transaksi_pinjam_meminjam),
        //         'pembayaran_pinjaman'=> $this->enkripsi_aes256cbc_64encode($data_pusdafil_pembayaran_pinjaman)
        //     ]
        // ]);

        // $response = $request->getBody()->getContents();
        // return $response;

        $data2 = [
            'form_params' => [
                'reg_pengguna'                  => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_pengguna),
                'reg_lender'                    => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_lender),
                'reg_borrower'                  => $this->enkripsi_aes256cbc_64encode($data_pusdafil_reg_borrower),
                'pengajuan_pinjaman'            => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pinjaman),
                'pengajuan_pemberian_pinjaman'  => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pengajuan_pemberi_pinjaman),
                'transaksi_pinjam_meminjam'     => $this->enkripsi_aes256cbc_64encode($data_pusdafil_transaksi_pinjam_meminjam),
                'pembayaran_pinjaman'           => $this->enkripsi_aes256cbc_64encode($data_pusdafil_pembayaran_pinjaman)
            ]
        ];
        return $data2;
    }
}
