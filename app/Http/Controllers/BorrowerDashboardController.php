<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Message;
use App\Jobs\ProcessEmail;
use App\Http\Middleware\UserCheck;
use App\Investor;
use App\Proyek;
use App\ManagePenghargaan;
use App\ManageKhazanah;
use App\News;
use App\Borrower;
use GuzzleHttp\Client;
use Auth;
use App\BorrowerDetails;
use App\BorrowerAhliWaris;
use App\BorrowerRekening;
use App\BorrowerPengurus;
use DB;

class BorrowerDashboardController extends Controller
{
    /*
	public function welcome() {
        return view('pages.borrower.welcome');
    } 
    public function dashboard() {
        return view('pages.borrower.dashboard');
    }
    public function all_pendanaan() {
        return view('pages.borrower.all_pendanaan');
    }
    public function detail_pendanaan() {
        return view('pages.borrower.detail_pendanaan');
    }
    public function add_pendanaan() {
        return view('pages.borrower.add_pendanaan');
    }
    public function all_riwayat_mutasi() {
        return view('pages.borrower.all_riwayat_mutasi');
    }
    public function edit_profile() {
        return view('pages.borrower.edit_profile');
    }
    public function notifikasi() {
        return view('pages.borrower.notifikasi');
    }
    public function log() {
        return view('pages.borrower.log');
    }
    public function faq() {
        return view('pages.borrower.faq');
    }
    public function lock() {
        return view('pages.borrower.lockscreen');
    }
	*/

    public function __construct()
    {
        $this->middleware('auth:borrower');
        $this->middleware(UserCheck::class)->except(['lengkapi_profile']);
        $this->middleware(UserCheck::class)->only(['lengkapi_profile']);
    }


    /************** Halaman Berhasil Registrasi **************/
    public function welcome()
    {
        return view('pages.borrower.welcome');
    }

    /************** Halaman Lengkapi Data **************/
    public function lengkapi_profile()
    {
        return view('pages.borrower.lengkapi_profile');
    }


    /************** Halaman Dashboard **************/
    public function dashboard()
    {
        return view('pages.borrower.dashboard');
    }


    /************** Halaman Tambah Pendanaan **************/
    public function add_pendanaan()
    {
        return view('pages.borrower.add_pendanaan');
    }

    /**************  ARL SIMPLIFIKASI - Halaman Tambah Pendanaan Simplifikasi **************/
    public function lengkapi_profile_pendanaan_simple()
    {
        return view('pages.borrower.lengkapi_profile_pendanaan_simple');
    }

    /************** Halaman Ubah Profile **************/
    public function edit_profile()
    {

        $id = Auth::guard('borrower')->user()->brw_id;

        $body_getProfileBrw = DB::table('brw_user_detail')
            ->join('brw_rekening', 'brw_user_detail.brw_id', '=', 'brw_rekening.brw_id')
            ->where('brw_user_detail.brw_id', $id)
            ->first();

        $body_getPasanganBrw = DB::table('brw_pasangan')
            ->where('pasangan_id', $id)
            ->first();

        if (empty($body_getPasanganBrw)) {
            $body_getPasanganBrw = (object) array(
                'nama'   => '',
                'jenis_kelamin'   => 0,
                'ktp'    => '',
                'tempat_lahir'  => '',
                'tgl_lahir'  => '',
                'no_hp' => '',
                'agama' => 0,
                'pendidikan'    => 0,
                'npwp'    => '',
                'alamat'    => '',
                'provinsi'    => '',
                'kota'    => '',
                'kecamatan'    => '',
                'kelurahan'    => '',
                'kode_pos'    => '',
            );
        }

        $body_getPengurusBrw = DB::table('brw_pengurus')->where('brw_pengurus.brw_id', $id)->limit(2)->get();
        $i = 1;
        $pengurusBrw1 = array();
        $pengurusBrw2 = array();
        if (count($body_getPengurusBrw) != 2) {
            $body_getPengurusBrw = (object) array(
                'pengurus_id' => null,
                'brw_id' => null,
                'nm_pengurus' => null,
                'jenis_kelamin' => null,
                'nik_pengurus' => null,
                'tempat_lahir' => null,
                'tgl_lahir' => null,
                'no_tlp' => null,
                'agama' => null,
                'pendidikan_terakhir' => null,
                'npwp' => null,
                'jabatan' => null,
                'alamat' => null,
                'provinsi' => null,
                'kota' => null,
                'kecamatan' => null,
                'kelurahan' => null,
                'kode_pos' => null,
                'foto_diri' => null,
                'foto_ktp' => null,
                'foto_diri_ktp' => null,
                'foto_npwp' => null
            );
            $pengurusBrw1[] = $body_getPengurusBrw;
            $pengurusBrw2[] = $body_getPengurusBrw;
        } else {
            foreach ($body_getPengurusBrw as $pengurus) {
                if ($i == 1) {
                    $pengurusBrw1[] = $pengurus;
                } else if ($i == 2) {
                    $pengurusBrw2[] = $pengurus;
                }
                $i++;
            }
        }

        return view('pages.borrower.edit_profile', [
            'id' => $id,
            'brw_id' => $id,
            "nama" => $body_getProfileBrw->nama,
            "nm_bdn_hukum" => $body_getProfileBrw->nm_bdn_hukum,
            "jabatan" => $body_getProfileBrw->jabatan,
            "brw_type" => $body_getProfileBrw->brw_type,
            "nm_ibu" => $body_getProfileBrw->nm_ibu,
            "ktp" => $body_getProfileBrw->ktp,
            "npwp" => $body_getProfileBrw->npwp,
            "tgl_lahir" => $body_getProfileBrw->tgl_lahir,
            "no_tlp" => $body_getProfileBrw->no_tlp,
            "telpon_perusahaan" => $body_getProfileBrw->telpon_perusahaan,
            "jns_kelamin" => $body_getProfileBrw->jns_kelamin,
            "status_kawin" => $body_getProfileBrw->status_kawin,
            "status_rumah" => $body_getProfileBrw->status_rumah,
            "alamat" => $body_getProfileBrw->alamat,
            "domisili_alamat" => $body_getProfileBrw->domisili_alamat,
            "domisili_provinsi" => $body_getProfileBrw->domisili_provinsi,
            "domisili_kota" => $body_getProfileBrw->domisili_kota,
            "domisili_kecamatan" => $body_getProfileBrw->domisili_kecamatan,
            "domisili_kelurahan" => $body_getProfileBrw->domisili_kelurahan,
            "domisili_kd_pos" => $body_getProfileBrw->domisili_kd_pos,
            "domisili_status_rumah" => $body_getProfileBrw->domisili_status_rumah,
            "provinsi" => $body_getProfileBrw->provinsi,
            "kota" => $body_getProfileBrw->kota,
            "kecamatan" => $body_getProfileBrw->kecamatan,
            "kelurahan" => $body_getProfileBrw->kelurahan,
            "kode_pos" => $body_getProfileBrw->kode_pos,
            "agama" => $body_getProfileBrw->agama,
            "tempat_lahir" => $body_getProfileBrw->tempat_lahir,
            "pendidikan_terakhir" => $body_getProfileBrw->pendidikan_terakhir,
            "pekerjaan" => $body_getProfileBrw->pekerjaan,
            "bidang_perusahaan" => $body_getProfileBrw->bidang_perusahaan,
            "bidang_usaha" => $body_getProfileBrw->bidang_usaha,
            "bidang_pekerjaan" => $body_getProfileBrw->bidang_pekerjaan,
            "bidang_online" => $body_getProfileBrw->bidang_online,
            "pengalaman_pekerjaan" => $body_getProfileBrw->pengalaman_pekerjaan,
            "pendapatan" => $body_getProfileBrw->pendapatan,
            "total_aset" => $body_getProfileBrw->total_aset,
            "kewarganegaraan" => $body_getProfileBrw->kewarganegaraan,
            "brw_online" => $body_getProfileBrw->brw_online,
            "brw_pic" => $body_getProfileBrw->brw_pic,
            "brw_pic_ktp" => $body_getProfileBrw->brw_pic_ktp,
            "brw_pic_user_ktp" => $body_getProfileBrw->brw_pic_user_ktp,
            "brw_pic_npwp" => $body_getProfileBrw->brw_pic_npwp,

            // Informasi Badan Hukum
            "tgl_berdiri" =>  $body_getProfileBrw->tgl_berdiri,
            "no_akta_pendirian" =>  $body_getProfileBrw->no_akta_pendirian,
            "npwp_perusahaan" => $body_getProfileBrw->npwp_perusahaan,
            "foto_npwp_perusahaan" => $body_getProfileBrw->foto_npwp_perusahaan,
            "nib" => $body_getProfileBrw->nib,
            "omset_tahun_terakhir" => $body_getProfileBrw->omset_tahun_terakhir,
            "tot_aset_tahun_terakhir" => $body_getProfileBrw->tot_aset_tahun_terakhr,

            //informasi pasangan
            "pasanganBrw" => $body_getPasanganBrw,

            //informasi rekening
            "brw_norek" => $body_getProfileBrw->brw_norek === null ? null : $body_getProfileBrw->brw_norek,
            "brw_nm_pemilik" => $body_getProfileBrw->brw_nm_pemilik === null ? null : $body_getProfileBrw->brw_nm_pemilik,
            "brw_kd_bank" => $body_getProfileBrw->brw_kd_bank === null ? null : $body_getProfileBrw->brw_kd_bank,

            //pengurus
            "pengurusBrw1" => $pengurusBrw1[0],
            "pengurusBrw2" => $pengurusBrw2[0],
            // 'nm_pengurus' => $body_getPengurusBrw->nm_pengurus === 0 ? null : $body_getPengurusBrw->nm_pengurus,
            // 'nik_pengurus' => $body_getPengurusBrw->nik_pengurus === 0 ? null : $body_getPengurusBrw->nik_pengurus,
            // 'no_tlppengurus' => $body_getPengurusBrw->no_tlp === 0 ? null : $body_getPengurusBrw->no_tlp,
            // 'jabatanpengurus' => $body_getPengurusBrw->jabatan === 0 ? null : $body_getPengurusBrw->jabatan,
        ]);
    }

    public function change_password()
    {
        return view('pages.borrower.change_password');
    }

    public function cek_password(Request $request)
    {
        // $aidi = base64_encode($request->id."*dsi*".$request->matchvalue);

        // $client = new client();
        //cek_password
        // $response_cekpassword = $client->request('GET', config('app.apilink')."/borrower/cek_password/".$aidi);
        // $body_cekpassword = json_decode($response_cekpassword->getBody()->getContents());

        $oldPassword = Auth::guard('borrower')->user()->password;
        if (Hash::check($request->matchvalue, $oldPassword)) {
            return response()->json([
                'status' => 1
            ]);
        } else {
            return response()->json([
                'status' => 0
            ]);
        }
        // return response()->json(['status' => $body_cekpassword->status]);
    }

    public function ubah_password(Request $request)
    {
        // $client = new client();
        // $req = $client->post(config('app.apilink')."/borrower/resetPasswordProses",[
        //     'form_params' =>
        //         [
        //             "aidi"			    => $request->id,
        //             "password"			=> $request->newpwd,
        //         ]
        //     ]);
        // $response = json_decode($req ->getBody()->getContents());
        // return response()->json(['status' => $response->status]);

        // $User = User::where('brw_id',$brw_users->brw_id)->first();

        $newPwd = $request->newpwd;
        //Change Password
        $id = Auth::guard('borrower')->user()->brw_id;
        $brw_user = Borrower::where('brw_id', $id)->first();
        $brw_user->password = Hash::make($newPwd);
        $brw_user->save();

        return response()->json(['status' => 0]);
    }

    // kill
    public function proses_updateprofile(Request $brw_users)
    {
        if ($brw_users->type_borrower == 2) {
            // proses badan hukum
            // $npwp_exist = BorrowerDetails::where('npwp_perusahaan', '=', $brw_users->npwp_bdn_hukum)->where('brw_id', '!=', $brw_users->brw_id)->exists();
            // $npwp_pengurus1_exist = BorrowerPengurus::where('npwp', '=', $brw_users->npwp_pengurus1)->where('pengurus_id', '!=', $brw_users->id_pengurus1)->exists();
            // $npwp_pengurus2_exist = BorrowerPengurus::where('npwp', '=', $brw_users->npwp_pengurus2)->where('pengurus_id', '!=', $brw_users->id_pengurus2)->exists();
            $no_tlp_pengurus1_exist = BorrowerPengurus::where('no_tlp', '=', $brw_users->no_tlp)->where('pengurus_id', '!=', $brw_users->id_pengurus1)->exists();
            $no_tlp_pengurus2_exist = BorrowerPengurus::where('no_tlp', '=', $brw_users->no_tlp)->where('pengurus_id', '!=', $brw_users->id_pengurus2)->exists();

            // if($npwp_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'NPWP badan hukum sudah terdaftar',
            //         'type' => '1'
            //     ]);
            // } else if($npwp_pengurus1_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'NPWP Pengurus 1 sudah terdaftar',
            //         'type' => '2'
            //     ]);
            // } else if($npwp_pengurus2_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'NPWP Pengurus 2 sudah terdaftar',
            //         'type' => '3'
            //     ]);
            // } 
            if ($no_tlp_pengurus1_exist) {
                return response()->json([
                    'error' => true,
                    'msg' => 'Nomor Telepon Pengurus 1 sudah terdaftar',
                    'type' => '4'
                ]);
            } else if ($no_tlp_pengurus2_exist) {
                return response()->json([
                    'error' => true,
                    'msg' => 'Nomor Telepon Pengurus 2 sudah terdaftar',
                    'type' => '5'
                ]);
            }

            $Borrower = BorrowerDetails::where('brw_id', $brw_users->brw_id)->first();
            $Borrower->nib = $brw_users->nib;
            $Borrower->npwp_perusahaan = $brw_users->npwp_bdn_hukum;
            $Borrower->no_akta_pendirian = $brw_users->no_akta_pendirian;
            $Borrower->bidang_usaha = $brw_users->bidang_usaha;
            $Borrower->tgl_berdiri = $brw_users->tgl_berdiri;
            $Borrower->nm_bdn_hukum = $brw_users->nm_bdn_hukum;
            $Borrower->jabatan = $brw_users->jabatanPendaftar;
            $Borrower->brw_type = $brw_users->type_borrower;
            // $Borrower->ktp = $brw_users->nikPendaftar;
            $Borrower->npwp = $brw_users->npwp_bdn_hukum;
            $Borrower->telpon_perusahaan = $brw_users->hpPendaftar;
            $Borrower->foto_npwp_perusahaan = $brw_users->url_pic_brw_npwp;

            $Borrower->alamat = $brw_users->alamat_bdn_hukum;
            $Borrower->provinsi = $brw_users->provinsi_bdn_hukum;
            $Borrower->kota = $brw_users->kota_bdn_hukum;
            $Borrower->kecamatan = $brw_users->kecamatan_bdn_hukum;
            $Borrower->kelurahan = $brw_users->kelurahan_bdn_hukum;
            $Borrower->kode_pos = $brw_users->kode_pos_bdn_hukum;

            if ($brw_users->domisili_status == 1) {
                $Borrower->domisili_kota = $brw_users->kota_bdn_hukum;
                $Borrower->domisili_provinsi = $brw_users->provinsi_bdn_hukum;
                $Borrower->domisili_kecamatan = $brw_users->kecamatan_bdn_hukum;
                $Borrower->domisili_kelurahan = $brw_users->kelurahan_bdn_hukum;
                $Borrower->domisili_kd_pos = $brw_users->kode_pos_bdn_hukum;
                $Borrower->domisili_alamat = $brw_users->alamat_bdn_hukum;
            } else {
                $Borrower->domisili_kota = $brw_users->domisili_kota;
                $Borrower->domisili_provinsi = $brw_users->domisili_provinsi;
                $Borrower->domisili_kecamatan = $brw_users->domisili_kecamatan;
                $Borrower->domisili_kelurahan = $brw_users->domisili_kelurahan;
                $Borrower->domisili_kd_pos = $brw_users->domisili_kd_pos;
                $Borrower->domisili_alamat = $brw_users->domisili_alamat;
            }

            $Borrower->bidang_perusahaan = $brw_users->bidang_pekerjaan_bdn_hukum;
            $Borrower->omset_tahun_terakhir = $brw_users->pendapatan_bdn_hukum;
            $Borrower->tot_aset_tahun_terakhr = $brw_users->total_aset;

            $Borrower->update();

            // insert data pengurus
            // pengurus 1
            $pengurusBrw1_exist = BorrowerPengurus::where('pengurus_id', $brw_users->id_pengurus1)->exists();
            if ($pengurusBrw1_exist) {
                $pengurusBrw1 = BorrowerPengurus::where('pengurus_id', $brw_users->id_pengurus1)->first();
                $pengurusBrw1->nm_pengurus = $brw_users->nm_pengurus1;
                $pengurusBrw1->jenis_kelamin = $brw_users->jns_kelamin_pengurus1;
                $pengurusBrw1->nik_pengurus = $brw_users->nik_pengurus1;
                $pengurusBrw1->tempat_lahir = $brw_users->tmpt_lahir_pengurus1;
                $pengurusBrw1->tgl_lahir = $brw_users->tgl_lahir_pengurus1;
                $pengurusBrw1->no_tlp = $brw_users->tlp_pengurus1;
                $pengurusBrw1->agama = $brw_users->agama_pengurus1;
                $pengurusBrw1->pendidikan_terakhir = $brw_users->pendidikan_terakhir_pengurus1;
                $pengurusBrw1->npwp = $brw_users->npwp_pengurus1;
                $pengurusBrw1->jabatan = $brw_users->jabatan_pengurus1;
                $pengurusBrw1->alamat = $brw_users->alamat_pengurus1;
                $pengurusBrw1->provinsi = $brw_users->provinsi_pengurus1;
                $pengurusBrw1->kota = $brw_users->kota_pengurus1;
                $pengurusBrw1->kecamatan = $brw_users->kecamatan_pengurus1;
                $pengurusBrw1->kelurahan = $brw_users->kelurahan_pengurus1;
                $pengurusBrw1->kode_pos = $brw_users->kode_pos_pengurus1;

                $pengurusBrw1->foto_diri = $brw_users->url_pic_brw_pengurus1;
                $pengurusBrw1->foto_ktp = $brw_users->url_pic_brw_ktp_pengurus1;
                $pengurusBrw1->foto_diri_ktp = $brw_users->url_pic_brw_dengan_ktp_pengurus1;
                $pengurusBrw1->foto_npwp = $brw_users->url_pic_brw_npwp_pengurus1;
                $pengurusBrw1->update();
            }

            // pengurus 2
            $pengurusBrw2_exist = BorrowerPengurus::where('pengurus_id', $brw_users->id_pengurus2)->exists();
            if ($pengurusBrw2_exist) {
                $pengurusBrw2 = BorrowerPengurus::where('pengurus_id', $brw_users->id_pengurus2)->first();
                $pengurusBrw2->nm_pengurus = $brw_users->nm_pengurus2;
                $pengurusBrw2->jenis_kelamin = $brw_users->jns_kelamin_pengurus2;
                $pengurusBrw2->nik_pengurus = $brw_users->nik_pengurus2;
                $pengurusBrw2->tempat_lahir = $brw_users->tmpt_lahir_pengurus2;
                $pengurusBrw2->tgl_lahir = $brw_users->tgl_lahir_pengurus2;
                $pengurusBrw2->no_tlp = $brw_users->tlp_pengurus2;
                $pengurusBrw2->agama = $brw_users->agama_pengurus2;
                $pengurusBrw2->pendidikan_terakhir = $brw_users->pendidikan_terakhir_pengurus2;
                $pengurusBrw2->npwp = $brw_users->npwp_pengurus2;
                $pengurusBrw2->jabatan = $brw_users->jabatan_pengurus2;
                $pengurusBrw2->alamat = $brw_users->alamat_pengurus2;
                $pengurusBrw2->provinsi = $brw_users->provinsi_pengurus2;
                $pengurusBrw2->kota = $brw_users->kota_pengurus2;
                $pengurusBrw2->kecamatan = $brw_users->kecamatan_pengurus2;
                $pengurusBrw2->kelurahan = $brw_users->kelurahan_pengurus2;
                $pengurusBrw2->kode_pos = $brw_users->kode_pos_pengurus2;

                $pengurusBrw2->foto_diri = $brw_users->url_pic_brw_pengurus2;
                $pengurusBrw2->foto_ktp = $brw_users->url_pic_brw_ktp_pengurus2;
                $pengurusBrw2->foto_diri_ktp = $brw_users->url_pic_brw_dengan_ktp_pengurus2;
                $pengurusBrw2->foto_npwp = $brw_users->url_pic_brw_npwp_pengurus2;
                $pengurusBrw2->update();
            }

            return response()->json([
                'error' => false
            ]);
        } else {
            // proses individu
            // $ktp_pribadi_exist = BorrowerDetails::where('ktp', '=', $brw_users->ktp)->where('brw_id', '!=', $brw_users->brw_id)->exists();
            // $npwp_pribadi_exist = BorrowerDetails::where('npwp', '=', $brw_users->npwp)->where('brw_id', '!=', $brw_users->brw_id)->exists();
            // $no_tlp_pasangan_exist = DB::table('brw_pasangan')->where('no_hp', '=', $brw_users->no_tlp_pasangan)->where('pasangan_id', '!=', $brw_users->brw_id)->exists();
            // $ktp_pasangan_exist = DB::table('brw_pasangan')->where('ktp', '=', $brw_users->ktp_pasangan)->where('pasangan_id', '!=', $brw_users->brw_id)->exists();
            // $npwp_pasangan_exist = DB::table('brw_pasangan')->where('npwp', '=', $brw_users->npwp_pasangan)->where('pasangan_id', '!=', $brw_users->brw_id)->exists();

            // if ($ktp_pribadi_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'Nomor KTP sudah terdaftar',
            //         'type' => '4'
            //     ]);
            // } else if ($npwp_pribadi_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'Nomor NPWP sudah terdaftar',
            //         'type' => '5'
            //     ]);
            // } else if ($no_tlp_pasangan_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'Nomor Telepon Pasangan sudah terdaftar',
            //         'type' => '1'
            //     ]);
            // } else if ($ktp_pasangan_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'Nomor KTP Pasangan sudah terdaftar',
            //         'type' => '2'
            //     ]);
            // } else if ($npwp_pasangan_exist) {
            //     return response()->json([
            //         'error' => true,
            //         'msg' => 'Nomor NPWP Pasangan sudah terdaftar',
            //         'type' => '3'
            //     ]);
            // }  
            $Borrower = BorrowerDetails::where('brw_id', $brw_users->brw_id)->first();

            $Borrower->nama = $brw_users->nama;
            $Borrower->nm_ibu = $brw_users->ibukandung;
            $Borrower->ktp = $brw_users->ktp;
            $Borrower->npwp = $brw_users->npwp;
            // $Borrower->tgl_lahir = $brw_users->tgl_lahir_tahun.'-'.$brw_users->tgl_lahir_bulan.'-'.$brw_users->tgl_lahir_hari;
            $Borrower->tgl_lahir = $brw_users->tgl_lahir;
            $Borrower->no_tlp = $brw_users->no_tlp;
            $Borrower->jns_kelamin = $brw_users->jns_kelamin;
            $Borrower->status_kawin = $brw_users->status_kawin;
            $Borrower->alamat = $brw_users->alamat;
            $Borrower->tempat_lahir = $brw_users->tempat_lahir;

            $Borrower->provinsi = $brw_users->provinsi;
            $Borrower->kota = $brw_users->kota;
            $Borrower->kecamatan = $brw_users->kecamatan;
            $Borrower->kelurahan = $brw_users->kelurahan;
            $Borrower->kode_pos = $brw_users->kode_pos;
            $Borrower->status_rumah = $brw_users->status_rumah;

            if ($brw_users->domisili_status == 1) {
                $Borrower->domisili_kota = $brw_users->kota;
                $Borrower->domisili_provinsi = $brw_users->provinsi;
                $Borrower->domisili_kecamatan = $brw_users->kecamatan;
                $Borrower->domisili_kelurahan = $brw_users->kelurahan;
                $Borrower->domisili_kd_pos = $brw_users->kode_pos;
                $Borrower->domisili_alamat = $brw_users->alamat;
                $Borrower->domisili_status_rumah = $brw_users->status_rumah;
            } else {
                $Borrower->domisili_kota = $brw_users->domisili_kota;
                $Borrower->domisili_provinsi = $brw_users->domisili_provinsi;
                $Borrower->domisili_kecamatan = $brw_users->domisili_kecamatan;
                $Borrower->domisili_kelurahan = $brw_users->domisili_kelurahan;
                $Borrower->domisili_kd_pos = $brw_users->domisili_kd_pos;
                $Borrower->domisili_alamat = $brw_users->domisili_alamat;
                $Borrower->domisili_status_rumah = $brw_users->domisili_status_rumah;
            }

            $Borrower->agama = $brw_users->agama;
            $Borrower->pendidikan_terakhir = $brw_users->pendidikan_terakhir;
            $Borrower->pekerjaan = $brw_users->pekerjaan;
            $Borrower->bidang_pekerjaan = $brw_users->bidang_pekerjaan;
            $Borrower->bidang_online = $brw_users->bidang_online;
            $Borrower->pengalaman_pekerjaan = $brw_users->pengalaman_kerja;
            $Borrower->pendapatan = $brw_users->pendapatan_bulanan;

            $Borrower->brw_pic = $brw_users->url_pic_brw;
            $Borrower->brw_pic_ktp = $brw_users->url_pic_brw_ktp;
            $Borrower->brw_pic_user_ktp = $brw_users->url_pic_brw_dengan_ktp;
            $Borrower->brw_pic_npwp = $brw_users->url_pic_brw_npwp;
            $Borrower->update();

            // Insert Pasangan
            $pasanganBrw = DB::table('brw_pasangan')->where('pasangan_id', $brw_users->brw_id)->first();
            if ($pasanganBrw == null) {
                DB::table('brw_pasangan')->insert([
                    'pasangan_id' => $brw_users->brw_id,
                    'nama' => $brw_users->nama_pasangan,
                    'jenis_kelamin' => $brw_users->jns_kelamin_pasangan,
                    'ktp' => $brw_users->ktp_pasangan,
                    'tempat_lahir' => $brw_users->tmpt_lahir_pasangan,
                    'tgl_lahir' => $brw_users->tgl_lahir_pasangan,
                    'no_hp' => $brw_users->no_tlp_pasangan,
                    'agama' => $brw_users->agama_pasangan,
                    'pendidikan' => $brw_users->pendidikan_terakhir_pasangan,
                    'npwp' => $brw_users->npwp_pasangan,
                    'alamat' => $brw_users->alamat_pasangan,
                    'provinsi' => $brw_users->provinsi_pasangan,
                    'kota' => $brw_users->kota_pasangan,
                    'kecamatan' => $brw_users->kecamatan_pasangan,
                    'kelurahan' => $brw_users->kelurahan_pasangan,
                    'kode_pos' => $brw_users->kode_pos_pasangan,
                ]);
            }
            DB::table('brw_pasangan')->where('pasangan_id', $brw_users->brw_id)->update([
                'nama' => $brw_users->nama_pasangan,
                'jenis_kelamin' => $brw_users->jns_kelamin_pasangan,
                'ktp' => $brw_users->ktp_pasangan,
                'tempat_lahir' => $brw_users->tmpt_lahir_pasangan,
                'tgl_lahir' => $brw_users->tgl_lahir_pasangan,
                'no_hp' => $brw_users->no_tlp_pasangan,
                'agama' => $brw_users->agama_pasangan,
                'pendidikan' => $brw_users->pendidikan_terakhir_pasangan,
                'npwp' => $brw_users->npwp_pasangan,
                'alamat' => $brw_users->alamat_pasangan,
                'provinsi' => $brw_users->provinsi_pasangan,
                'kota' => $brw_users->kota_pasangan,
                'kecamatan' => $brw_users->kecamatan_pasangan,
                'kelurahan' => $brw_users->kelurahan_pasangan,
                'kode_pos' => $brw_users->kode_pos_pasangan,
            ]);

            //insert data Rekening
            $BorrowerRek = BorrowerRekening::where('brw_id', $brw_users->brw_id)->first();
            $BorrowerRek->brw_norek = $brw_users->norekening;
            $BorrowerRek->brw_nm_pemilik = $brw_users->namapemilikrekening;
            $BorrowerRek->brw_kd_bank = $brw_users->bank;
            $BorrowerRek->save();

            return response()->json([
                'error' => false
            ]);
        }
    }
}
