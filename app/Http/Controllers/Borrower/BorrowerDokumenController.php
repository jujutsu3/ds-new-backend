<?php

namespace App\Http\Controllers\Borrower;


use App\Admins;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\VerifikatorService;
use App\BorrowerPengajuan;
use Illuminate\Support\Facades\Auth;

class BorrowerDokumenController extends Controller
{

    public function getDocumentValue($tableName, $whereFilter,  $id, $fieldName)
    {
        return  \DB::table($tableName)->where($whereFilter, $id)->value($fieldName);
    }

    public function pengajuanExists($tableName, $whereFilter,  $id)
    {
        return \DB::table($tableName)->where($whereFilter, $id)->first();
    }

    public function validateFileType($mimeType, $allowedType){

        $arrayExtension = ['bmp'=> 'image/x-ms-bmp','pdf'=>'application/pdf', 'jpeg'=> 'image/jpeg', 'jpg'=>'image/jpg', 'png'=>'image/png'];

        $allowTypes = explode('|', $allowedType);
        $checkFileType = [];
        foreach($allowTypes as $type){
             array_push($checkFileType, $arrayExtension[$type]);
        }

        return (in_array($mimeType, $checkFileType)) ? true : false;
    }



    public function uploadDokumenPendukung(Request $request)
    {

        try {

            if($request->has('brw_id')){
                $brw_id = $request->brw_id;
            }else{
                $brw_id = Auth::guard('borrower')->user()->brw_id;
            }

            if ($request->file('file')) {
                $file_temp = $request->file('file');
                $allowedType = $request->filetype;
                $mimeType = $file_temp->getMimeType();
                $extension  = $file_temp->getClientOriginalExtension();
                $size = $file_temp->getSize();
                $maxsize = 1000000; // 1MB

                if($size > $maxsize)  return response()->json(['status' => 'failed', 'message' => 'Maksimum Unggah File Size 1 MB']);                
                $validType = $this->validateFileType($mimeType, $allowedType);
                if(!$validType) return response()->json(['status' => 'failed', 'message' => 'Format File Tidak Diizinkan']);

                $safeName = $request->fieldName . '_' . date('YmdHis') . '.' . $extension;

                if ($request->tableName == 'brw_dokumen_objek_pendanaan') {
                    //cek exists 
                    $fileSaved = $this->getDocumentValue($request->tableName, 'id_pengajuan', $request->pengajuanId, $request->fieldName);
                    // if ($fileSaved) {
                    //     if (file_exists(storage_path('app/private/' . $fileSaved))) {
                    //         unlink(storage_path('app/private/' . $fileSaved));
                    //     }
                    // }
                    $destinationPath = storage_path('app/private/borrower/' . $brw_id . '/' . $request->pengajuanId . '/');
                } else {
                    $fileSaved = $this->getDocumentValue($request->tableName, 'brw_id',  $brw_id, $request->fieldName);
                    // if ($fileSaved) unlink(storage_path('app/private/' . $fileSaved));
                    $destinationPath = storage_path('app/private/borrower/' . $brw_id . '/');
                }


                if ($fileSaved) {
                    if (file_exists(storage_path('app/private/' . $fileSaved))) {
                        unlink(storage_path('app/private/' . $fileSaved));
                    }
                }

                if ($file_temp->move($destinationPath, $safeName)) {

                    if ($request->tableName == 'brw_dokumen_objek_pendanaan') {
                        $fileToSave = 'borrower/' . $brw_id . '/' . $request->pengajuanId . '/' . $safeName;
                        $pengajuanExists = $this->pengajuanExists($request->tableName, 'id_pengajuan', $request->pengajuanId);

                        if ($pengajuanExists) {
                            \DB::table('brw_dokumen_objek_pendanaan')->where('id_pengajuan', $request->pengajuanId)->update([$request->fieldName => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                        } else {
                            \DB::table('brw_dokumen_objek_pendanaan')->insert(['id_pengajuan' => $request->pengajuanId, $request->fieldName => $fileToSave]);
                        }
                    } else if($request->tableName == 'brw_dokumen_legalitas_pribadi') {
                        $fileToSave = 'borrower/' . $brw_id .  '/' . $safeName;
                        $pengajuanExists = $this->pengajuanExists($request->tableName, 'brw_id', $brw_id);

                        if ($pengajuanExists) {
                            \DB::table('brw_dokumen_legalitas_pribadi')->where('brw_id', $brw_id)->update([$request->fieldName => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                        } else {
                            \DB::table('brw_dokumen_legalitas_pribadi')->insert(['brw_id' => $brw_id, $request->fieldName => $fileToSave]);
                        }

                    } else if($request->tableName == 'brw_user_detail_pengajuan'){
                        $fileToSave = 'borrower/' . $brw_id .  '/' . $safeName;
                        \DB::table($request->tableName)->where('pengajuan_id', $request->pengajuanId)->update([$request->fieldName => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                        \DB::table('brw_user_detail')->where('brw_id', $brw_id)->update([$request->fieldName => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                    }else{
                        $fileToSave = 'borrower/' . $brw_id .  '/' . $safeName;

                        $pengajuanExists = $this->pengajuanExists($request->tableName, 'brw_id', $brw_id);

                        if ($pengajuanExists) {
                            \DB::table($request->tableName)->where('brw_id', $brw_id)->update([$request->fieldName => $fileToSave, 'updated_at' => date('Y-m-d H:i:s')]);
                        }else{
                            \DB::table($request->tableName)->insert(['brw_id' => $brw_id, $request->fieldName => $fileToSave]);
                        }

                    }

                    BorrowerPengajuan::where('pengajuan_id', $request->pengajuanId)->where('status', 1)->update(['status'=> 11]);
                    Helper::logAktifitasPengajuan($request->pengajuanId, [$request->fieldName => $fileToSave], $request->tableName);	

                    if($request->has('brw_id')){
                        //verifikator pengajuan
                        VerifikatorService::updateVerifikatorPengajuan($request->pengajuanId);
                        
                        $file_link =  route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $fileToSave)]);
                    }else{
                        $file_link =  route('getUserFile', ['filename' => str_replace('/', ':', $fileToSave)]);
                    }
                    
                   
                    return response()->json(['status' => 'success', 'message' => 'Berhasil di upload', 'field' => $request->fieldName, 'file_link' => $file_link]);
                }
            } else {
                return response()->json(['status' => 'failed', 'message' => 'File Kosong']);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'failed', 'message' => $e->getMessage()]);
        }
    }
}
