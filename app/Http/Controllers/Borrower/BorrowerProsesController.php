<?php

namespace App\Http\Controllers\Borrower;

use DB;
use File;
use Mail;
use Image;
use Storage;
use Response;
use App\Admins;
use App\Proyek;
use App\Borrower;
use App\Investor;
use Carbon\Carbon;
use App\GambarProyek;
use GuzzleHttp\Client;
use App\LogSP3Borrower;
use App\MasterProvinsi;
use App\BorrowerDetails;
use App\BorrowerInvoice;
use App\BorrowerJaminan;
use App\BorrowerPengurus;
use App\BorrowerPengajuan;
use App\BorrowerPembayaran;
use Illuminate\Http\Request;
use App\BorrowerDanaRumahLain;
use App\BorrowerLogPembayaran;
use App\BorrowerDanaNonRumahLain;
use App\BorrowerPersyaratanInsert;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\BorrowerPersyaratanPendanaan;
use App\Mail\SendInfoPengajuanAnalys;
use Illuminate\Support\Facades\Schema;

use App\Services\VerifikatorService;

//use Carbon\Carbon;
//use App\Http\Middleware\UserCheck;
use App\Mail\SendInfoPengajuanBorrower;
use Illuminate\Support\Facades\Session;
use App\Helpers\Helper;


class BorrowerProsesController extends Controller
{
	//private const  url = 'http://103.28.23.203/borrower/';
	private const  url = 'http://127.0.0.1:8000/borrower/';
	public function new_upload_gambar_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;
		//var_dump($_FILES['webcam']['tmp_name']) ;
		if ($request->file('file')) {
			$file = $request->file('file');
			$filename = 'pic_brw' . '.' . $file->getClientOriginalExtension();
			//var_dump($filename);die();
			$resize = Image::make($file)->resize(480, 640, function ($constraint) {
				$constraint->aspectRatio();
			})->save();

			// $resize = Image::make($file)->resize(480,640, function ($constraint) {
			// $constraint->aspectRatio();
			// })->save($filename);

			// save nama file berdasarkan tanggal upload+nama file
			$store_path = 'borrower/' . $brw_id;
			$path = $file->storeAs($store_path, $filename, 'public');
			// save gambar yang di upload di public storage

			// Storage::disk('public')->delete('user/'.$investor_id.'/'.$filename);

			if (Storage::disk('public')->exists('borrower/' . $brw_id . '/' . $filename)) {
				return response()->json([
					'success' => 'Berhasil di upload',
					'url' => "borrower/" . $brw_id . "/" . $filename,
					'filename' => $filename
				]);
			} else {
				return response()->json([
					'failed' => 'File gagal di upload'
				]);
			}
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function new_upload_gambar_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request->file('file')) {
			$file = $request->file('file');
			$filename = 'pic_brw_ktp' . '.' . $file->getClientOriginalExtension();
			$resize = Image::make($file)->resize(480, 640, function ($constraint) {
				$constraint->aspectRatio();
			})->save();

			// $resize = Image::make($file)->resize(480,640, function ($constraint) {
			// $constraint->aspectRatio();
			// })->save($filename);
			// save nama file berdasarkan tanggal upload+nama file
			$store_path = 'borrower/' . $brw_id;
			$path = $file->storeAs($store_path, $filename, 'public');
			// save gambar yang di upload di public storage

			// Storage::disk('public')->delete('user/'.$investor_id.'/'.$filename);

			if (Storage::disk('public')->exists('borrower/' . $brw_id . '/' . $filename)) {
				return response()->json([
					'success' => 'Berhasil di upload',
					'url' => "borrower/" . $brw_id . "/" . $filename,
					'filename' => $filename
				]);
			} else {
				return response()->json([
					'failed' => 'File gagal di upload'
				]);
			}
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function new_upload_gambar_3(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request->file('file')) {
			$file = $request->file('file');
			$filename = 'pic_brw_dan_ktp' . '.' . $file->getClientOriginalExtension();
			$resize = Image::make($file)->resize(480, 640, function ($constraint) {
				$constraint->aspectRatio();
			})->save();

			// $resize = Image::make($file)->resize(480,640, function ($constraint) {
			// $constraint->aspectRatio();
			// })->save();
			// save nama file berdasarkan tanggal upload+nama file
			$store_path = 'borrower/' . $brw_id;
			$path = $file->storeAs($store_path, $filename, 'public');
			// save gambar yang di upload di public storage

			// Storage::disk('public')->delete('user/'.$investor_id.'/'.$filename);

			if (Storage::disk('public')->exists('borrower/' . $brw_id . '/' . $filename)) {
				return response()->json([
					'success' => 'Berhasil di upload',
					'url' => "borrower/" . $brw_id . "/" . $filename,
					'filename' => $filename
				]);
			} else {
				return response()->json([
					'failed' => 'File gagal di upload'
				]);
			}
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}


	public function new_upload_gambar_4(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request->file('file')) {
			$file = $request->file('file');
			$filename = 'pic_brw_npwp' . '.' . $file->getClientOriginalExtension();
			$resize = Image::make($file)->resize(480, 640, function ($constraint) {
				$constraint->aspectRatio();
			})->save();


			// $resize = Image::make($file)->resize(480,640, function ($constraint) {
			// $constraint->aspectRatio();
			// })->save($filename);
			// save nama file berdasarkan tanggal upload+nama file
			$store_path = 'borrower/' . $brw_id;
			$path = $file->storeAs($store_path, $filename, 'public');
			// save gambar yang di upload di public storage

			// Storage::disk('public')->delete('user/'.$investor_id.'/'.$filename);

			if (Storage::disk('public')->exists('borrower/' . $brw_id . '/' . $filename)) {
				return response()->json([
					'success' => 'Berhasil di upload',
					'url' => "borrower/" . $brw_id . "/" . $filename,
					'filename' => $filename
				]);
			} else {
				return response()->json([
					'failed' => 'File gagal di upload'
				]);
			}
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}
	// PROSES UPLOAD FOTO INDIVIDU
	public function webcam_picture_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'pic_brw.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_picture_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp = explode(";base64,", $request);
			$image_decode_foto_ktp = base64_decode($image_foto_ktp[1]);
			$fileName_foto_ktp = uniqid() . '.png';
			$path_foto_ktp = 'borrower/' . $brw_id . '/' . 'pic_brw_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp, $image_decode_foto_ktp);
			$path = Storage::disk('private')->put($path_foto_ktp, $image_decode_foto_ktp);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_ktp,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_picture_3(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp_diri = explode(";base64,", $request);
			$image_decode_foto_ktp_diri = base64_decode($image_foto_ktp_diri[1]);
			$fileName_foto_ktp_diri = uniqid() . '.png';
			$path_foto_ktp_diri = 'borrower/' . $brw_id . '/' . 'pic_brw_dan_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp_diri, $image_decode_foto_ktp_diri);
			$path = Storage::disk('private')->put($path_foto_ktp_diri, $image_decode_foto_ktp_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_ktp_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_picture_4(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_npwp = explode(";base64,", $request);
			$image_decode_foto_npwp = base64_decode($image_foto_npwp[1]);
			$fileName_foto_npwp = uniqid() . '.png';
			$path_foto_npwp = 'borrower/' . $brw_id . '/' . 'pic_brw_npwp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_npwp, $image_decode_foto_npwp);
			$path = Storage::disk('private')->put($path_foto_npwp, $image_decode_foto_npwp);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_npwp,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	// PROSES UPLOAD FOTO BADAN HUKUM
	public function webcam_npwp_bdn_hukum(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_npwp = explode(";base64,", $request);
			$image_decode_foto_npwp = base64_decode($image_foto_npwp[1]);
			$fileName_foto_npwp = uniqid() . '.png';
			$path_foto_npwp = 'borrower/' . $brw_id . '/' . 'npwp_badan_hukum.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_npwp, $image_decode_foto_npwp);
			$path = Storage::disk('private')->put($path_foto_npwp, $image_decode_foto_npwp);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_npwp,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_foto_pengurus_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_pengurus_1.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_foto_ktp_pengurus_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_ktp_pengurus_1.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}
	public function webcam_foto_ktp_diri_pengurus_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_ktp_diri_pengurus_1.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}
	public function webcam_npwp_pengurus_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_npwp_pengurus_1.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_foto_pengurus_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_pengurus_2.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_foto_ktp_pengurus_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_ktp_pengurus_2.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}
	public function webcam_foto_ktp_diri_pengurus_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_ktp_diri_pengurus_2.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}
	public function webcam_npwp_pengurus_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'foto_npwp_pengurus_2.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	// END PROSES UPLOAD FOTO BADAN HUKUM

	public function update_webcam_picture_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri = explode(";base64,", $request);
			$image_decode_foto_diri = base64_decode($image_foto_diri[1]);
			$fileName_foto_diri = uniqid() . '.png';
			$path_foto_diri = 'borrower/' . $brw_id . '/' . 'pic_brw.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri, $image_decode_foto_diri);
			$path = Storage::disk('private')->put($path_foto_diri, $image_decode_foto_diri);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong',
			]);
		}
	}

	public function update_webcam_picture_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp = explode(";base64,", $request);
			$image_decode_foto_ktp = base64_decode($image_foto_ktp[1]);
			$fileName_foto_ktp = uniqid() . '.png';
			$path_foto_ktp = 'borrower/' . $brw_id . '/' . 'pic_brw_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp, $image_decode_foto_ktp);
			$path = Storage::disk('private')->put($path_foto_ktp, $image_decode_foto_ktp);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_ktp,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function update_webcam_picture_3(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp_diri = explode(";base64,", $request);
			$image_decode_foto_ktp_diri = base64_decode($image_foto_ktp_diri[1]);
			$fileName_foto_ktp_diri = uniqid() . '.png';
			$path_foto_ktp_diri = 'borrower/' . $brw_id . '/' . 'pic_brw_dan_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp_diri, $image_decode_foto_ktp_diri);
			$path = Storage::disk('private')->put($path_foto_ktp_diri, $image_decode_foto_ktp_diri);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_ktp_diri,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function update_webcam_picture_4(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_npwp = explode(";base64,", $request);
			$image_decode_foto_npwp = base64_decode($image_foto_npwp[1]);
			$fileName_foto_npwp = uniqid() . '.png';
			$path_foto_npwp = 'borrower/' . $brw_id . '/' . 'pic_brw_npwp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_npwp, $image_decode_foto_npwp);
			$path = Storage::disk('private')->put($path_foto_npwp, $image_decode_foto_npwp);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_npwp,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_picture_hukum_1(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri_hukum = explode(";base64,", $request);
			$image_decode_foto_diri_hukum = base64_decode($image_foto_diri_hukum[1]);
			$fileName_foto_diri_hukum = uniqid() . '.png';
			$path_foto_diri_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri_hukum, $image_decode_foto_diri_hukum);
			$path = Storage::disk('private')->put($path_foto_diri_hukum, $image_decode_foto_diri_hukum);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_diri_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_picture_hukum_2(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp_hukum = explode(";base64,", $request);
			$image_decode_foto_ktp_hukum = base64_decode($image_foto_ktp_hukum[1]);
			$fileName_foto_ktp_hukum = uniqid() . '.png';
			$path_foto_ktp_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp_hukum, $image_decode_foto_ktp_hukum);
			$path = Storage::disk('private')->put($path_foto_ktp_hukum, $image_decode_foto_ktp_hukum);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_ktp_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_picture_hukum_3(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp_diri_hukum = explode(";base64,", $request);
			$image_decode_foto_ktp_diri_hukum = base64_decode($image_foto_ktp_diri_hukum[1]);
			$fileName_foto_ktp_diri_hukum = uniqid() . '.png';
			$path_foto_ktp_diri_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw_dan_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp_diri_hukum, $image_decode_foto_ktp_diri_hukum);
			$path = Storage::disk('private')->put($path_foto_ktp_diri_hukum, $image_decode_foto_ktp_diri_hukum);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_ktp_diri_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function webcam_picture_hukum_4(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_npwp_hukum = explode(";base64,", $request);
			$image_decode_foto_npwp_hukum = base64_decode($image_foto_npwp_hukum[1]);
			$fileName_foto_npwp_hukum = uniqid() . '.png';
			$path_foto_npwp_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw_npwp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_npwp_hukum, $image_decode_foto_npwp_hukum);
			$path = Storage::disk('private')->put($path_foto_npwp_hukum, $image_decode_foto_npwp_hukum);

			return response()->json([
				'success' => 'Berhasil di upload',
				'url' => $path_foto_npwp_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function update_webcam_picture_1_bdn_hukum(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_diri_bdn_hukum = explode(";base64,", $request);
			$image_decode_foto_diri_bdn_hukum = base64_decode($image_foto_diri_bdn_hukum[1]);
			$fileName_foto_diri_bdn_hukum = uniqid() . '.png';
			$path_foto_diri_bdn_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_diri_bdn_hukum, $image_decode_foto_diri_bdn_hukum);
			$path = Storage::disk('private')->put($path_foto_diri_bdn_hukum, $image_decode_foto_diri_bdn_hukum);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_diri_bdn_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong',
			]);
		}
	}

	public function update_webcam_picture_2_bdn_hukum(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp_bdn_hukum = explode(";base64,", $request);
			$image_decode_foto_ktp_bdn_hukum = base64_decode($image_foto_ktp_bdn_hukum[1]);
			$fileName_foto_ktp_bdn_hukum = uniqid() . '.png';
			$path_foto_ktp_bdn_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp_bdn_hukum, $image_decode_foto_ktp_bdn_hukum);
			$path = Storage::disk('private')->put($path_foto_ktp_bdn_hukum, $image_decode_foto_ktp_bdn_hukum);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_ktp_bdn_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function update_webcam_picture_3_bdn_hukum(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_ktp_diri_bdn_hukum = explode(";base64,", $request);
			$image_decode_foto_ktp_diri_bdn_hukum = base64_decode($image_foto_ktp_diri_bdn_hukum[1]);
			$fileName_foto_ktp_diri_bdn_hukum = uniqid() . '.png';
			$path_foto_ktp_diri_bdn_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw_dan_ktp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_ktp_diri_bdn_hukum, $image_decode_foto_ktp_diri_bdn_hukum);
			$path = Storage::disk('private')->put($path_foto_ktp_diri_bdn_hukum, $image_decode_foto_ktp_diri_bdn_hukum);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_ktp_diri_bdn_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function update_webcam_picture_4_bdn_hukum(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request) {
			$image_foto_npwp_bdn_hukum = explode(";base64,", $request);
			$image_decode_foto_npwp_bdn_hukum = base64_decode($image_foto_npwp_bdn_hukum[1]);
			$fileName_foto_npwp_bdn_hukum = uniqid() . '.png';
			$path_foto_npwp_bdn_hukum = 'borrower/' . $brw_id . '/' . 'pic_brw_npwp.' . 'png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			// $path = Storage::disk('public')->put($path_foto_npwp_bdn_hukum, $image_decode_foto_npwp_bdn_hukum);
			$path = Storage::disk('private')->put($path_foto_npwp_bdn_hukum, $image_decode_foto_npwp_bdn_hukum);

			return response()->json([
				'success' => 'Berhasil di update',
				'url' => $path_foto_npwp_bdn_hukum,
			]);
		} else {
			return response()->json([
				'failed' => 'File Kosong'
			]);
		}
	}

	public function action_lengkapi_profile(Request $request)
	{

        $result = Helper::checkLimitPengajuan(Auth::guard('borrower')->user()->brw_id, 'web');
		if($result['hasil'] == 0) return response()->json(['status' => 'limit', 'message' => $result['responseMessage']['errors']['pengajuan'][0], 'data_borrower'=>null]);
	   
		$type = $request->borrower_type;
		$historyDataStatus = 0;

		switch ($type) {
			case 1: // perorangan pegawai
				// insert borrower details

				$domisili_status = $request->domisili_status;
				// Alamat
				$alamat = $request->alamat;
				$provinsi = $request->provinsi;
				$kota = $request->kota;
				$kecamatan = $request->kecamatan;
				$kelurahan = $request->kelurahan;
				$kode_pos = $request->kode_pos;
				$status_rumah = $request->status_rumah;

				// Domisili Alamat
				$alamat_domisili = $request->alamat_domisili;
				$provinsi_domisili = $request->provinsi_domisili;
				$kota_domisili = $request->kota_domisili;
				$kecamatan_domisili = $request->kecamatan_domisili;
				$kelurahan_domisili = $request->kelurahan_domisili;
				$kode_pos_domisili = $request->kode_pos_domisili;
				$domisili_status_rumah = $request->domisili_status_rumah;

				if ($domisili_status) {
					$alamat_domisili = $alamat;
					$provinsi_domisili = $provinsi;
					$kota_domisili = $kota;
					$kecamatan_domisili = $kecamatan;
					$kelurahan_domisili = $kelurahan;
					$kode_pos_domisili = $kode_pos;
					$domisili_status_rumah = $request->status_rumah;
				}

				$no_telpon_usaha = $request->no_telpon_usaha ? "62" . $request->no_telpon_usaha : '';
				$no_hp_usaha = $request->no_hp_usaha ? "62" . $request->no_hp_usaha : '';
				$no_fixed_line_hrd = $request->no_hrd ? '62' . $request->no_hrd : '';

				$no_telpon_usaha_pasangan = $request->no_telpon_usaha_pasangan ? "62" . $request->no_telpon_usaha_pasangan : '';
				$no_hp_usaha_pasangan = $request->no_hp_usaha_pasangan ? "62" . $request->no_hp_usaha_pasangan : '';
				$no_hrd_pasangan = $request->no_hrd_pasangan ? '62' . $request->no_hrd_pasangan : '';

				$nilai_spt = str_replace(".", "", $request->nilai_spt);
				$nilai_spt_pasangan = str_replace(".", "", $request->nilai_spt_pasangan);

				$total_penghasilan_lain_lain = str_replace(".", "", $request->total_penghasilan_lain_lain);
				$total_penghasilan_lain_lain_pasangan = str_replace(".", "", $request->total_penghasilan_lain_lain_pasangan);


				$pekerjaan = $request->pekerjaan;
				$penghasilan = str_replace(".", "", $request->penghasilan);
				$penghasilan_pasangan = str_replace(".", "", $request->penghasilan_pasangan);
				$biaya_hidup = str_replace(".", "", $request->biaya_hidup);
				$detail_penghasilan_lain_lain = $request->detail_penghasilan_lain_lain;
				if ($pekerjaan == '6' || $pekerjaan == '8') {
					$penghasilan = str_replace(".", "", $request->penghasilan2);
					$biaya_hidup = str_replace(".", "", $request->biaya_hidup2);
					$detail_penghasilan_lain_lain = $request->detail_penghasilan_lain_lain2;
				}

				$pekerjaan_pasangan = $request->pekerjaan_pasangan;
				$penghasilan_pasangan = str_replace(".", "", $request->penghasilan_pasangan);
				$biaya_hidup_pasangan = str_replace(".", "", $request->biaya_hidup_pasangan);
				$detail_penghasilan_lain_lain_pasangan = $request->detail_penghasilan_lain_lain_pasangan;

				if ($pekerjaan_pasangan == '6' || $pekerjaan == '8') {
					$penghasilan_pasangan = str_replace(".", "", $request->penghasilan_pasangan2);
					$biaya_hidup_pasangan = str_replace(".", "", $request->biaya_hidup_pasangan2);
					$detail_penghasilan_lain_lain_pasangan = $request->detail_penghasilan_lain_lain_pasangan2;
				}

				$insertDetailBorrower = DB::select(
					"SELECT put_into_brw_user_detail(
					'" . Auth::guard('borrower')->user()->brw_id . "',
					'" . $request->nama_individu . "',
					'', /*nm_bdn_hukum*/ 
					'', /*nib*/ 
					'', /*npwp_perusahaan*/ 
					'', /*akta*/
					'', /*tgl berdiri*/
					'', /*tlp_perusahaan*/
					'', /*foto_npwp_perusahaan*/
					'', /*bidang usaha*/
					'', /*omset*/
					'', /*tot aset*/
					'', /*jabatan*/
					'1', /*brw_tipe*/
					'" . $request->nama_ibu . "', /*nama ibu*/
					'" . $request->ktp . "', /*ktp*/
					'" . $request->npwp . "', /*npwp*/
					'" . $request->tanggal_lahir . "',/*tgl lahir*/
					'62" . $request->telepon . "', /*no_tlp*/
					'" . $request->gender . "', /*jenis_kelamin*/
					'" . $request->status_kawin . "', /*status_kawin*/
					'" . $request->status_rumah . "', /*status rumah*/
					'" . addslashes($request->alamat) . "', /*alamat*/
					'" . addslashes($alamat_domisili) . "',  /*domisili*/
					'" . $provinsi_domisili . "', /*domisili prov*/
					'" . $kota_domisili . "', /*domisili kota*/
					'" . $kecamatan_domisili . "', /*domisili kec*/
					'" . $kelurahan_domisili . "', /*domisili kel*/
					'" . $kode_pos_domisili . "', /*domisili kd pos*/
					'" . $domisili_status_rumah . "',  /*status rumah domisili*/
					'" . $request->provinsi . "', /*provinsi*/
					'" . $request->kota . "', /*kota*/
					'" . $request->kecamatan . "', /*kec*/
					'" . $request->kelurahan . "', /*kel*/
					'" . $request->kode_pos . "', /*kd pos*/
					'" . $request->agama . "', /*agama*/
					'" . $request->tempat_lahir . "', /*tempat lahir*/
					'" . $request->pendidikan_terakhir . "', /*pendidikan terakhi*/
					'" . $request->pekerjaan . "', /*pekerjaan*/
					'', /*bidang perusahaan*/
					'" . $request->bidang_pekerjaan . "', /*bidang pekerjaan*/
					'" . $request->bidang_online . "', /*bidang online*/
					'', /*pengalaman pekerjaan*/
					'', /*pendapatan*/
					'', /*total aset*/
					'', /*warganegara*/
					'', /*brw online*/
					'" . $request->user_camera_diri . "',
					'" . $request->user_camera_ktp . "',
					'" . $request->user_camera_diri_dan_ktp . "',
					'" . $request->user_camera_npwp . "',
					'" . $request->kk . "', /*no kk*/
					'', /*lama menempati*/
					'', /*tlp rumah*/
					'', /*kartu kredit*/
					'', /*usia pensiun*/
					'', /*alamat penagihan rumah*/
					'', /*alamat penagihan kantor*/
					'', /*npwp pasangan*/
					'', /*nomor kk pasangan*/
					'', /*tmpt lahir pasangan*/
					'', /*tgl lahir pasangan*/
					'', /*pendidikan pasangan*/
					'', /*sd pasangan*/
					'', /*smp pasangan*/
					'', /*sma pasangan*/
					'', /*pt pasangan*/
					'" . $pekerjaan_pasangan . "', /*pekerjaan pasangan*/
					'" . $request->bidang_pekerjaan_pasangan . "', /*bd pekerjaan pasangan*/
					'" . $request->bidang_online_pasangan . "', /*bd pekerjaanO pasangan*/
					'',  /*pengalaman pasangan*/
					'', /*pendapatan pasangan*/
					'', /*kartu kredit*/
					'', /*jmlasettdkbergerak_pasangan*/
					'', /*mlasetbergerak_pasangan*/
					'', /*agama pasangan*/
					'', /*pic kk*/
					'', /*pic ktp pasangan*/
					'', /*pic surat nikah*/
					'', /*pic spt*/
					'', /*pic rek koran*/
					'', /*pic slip gaji*/
					'', /*pic lap keuangan*/
					'" . date('Y-m-d H:i:s') . "', /*createtd at*/
					'" . date('Y-m-d H:i:s') . "',  /*update at*/
					'BorrowerProsesController',
					'843'
					) as response;"
				);


				$insertPenghasilan = DB::select(
					"SELECT put_into_brw_user_detail_penghasilan(
					'" . Auth::guard('borrower')->user()->brw_id . "',
					'" . $request->sumber_penghasilan_dana . "', /*sumber_pengembalian_dana*/
					'" . $request->skema_pembiayaan . "', /*skema_pembiayaan*/
					'" . addslashes($request->nama_perusahaan) . "', /*nama_perusahaan*/
					'" . addslashes($request->alamat_perusahaan) . "', /*alamat_perusahaan*/
					'" . addslashes($request->rt_perusahaan) . "', /*rt*/
					'" . addslashes($request->rw_perusahaan) . "', /*rw*/
					'" . $request->provinsi_perusahaan . "', /*provinsi*/
					'" . $request->kota_perusahaan . "', /*kab_kota*/
					'" . $request->kecamatan_perusahaan . "', /*kecamatan*/
					'" . $request->kelurahan_perusahaan . "', /*kelurahan*/
					'" . $request->kode_pos_perusahaan . "', /*kode_pos*/
					'" . $no_telpon_usaha . "', /*no_telp*/
					'" . $no_hp_usaha . "', /*no_hp*/
					'" . $request->surat_ijin_usaha . "', /*surat_ijin*/
					'" . addslashes($request->no_ijin_usaha) . "', /*no_surat_ijin*/
					'" . $request->badan_usaha . "', /*bentuk_badan_usaha*/
					'" . $request->status_kepegawaian . "', /*status_pekerjaan*/
					'" . $request->usia_perusahaan . "', /*usia_perusahaan*/
					'" . $request->usia_perusahaan . "', /*usia_tempat_usaha*/
					'" . addslashes($request->departemen) . "', /*departemen*/
					'" . addslashes($request->jabatan) . "', /*jabatan*/
					'" . $request->tahun_bekerja . "', /*masa_kerja_tahun*/
					'" . $request->bulan_bekerja . "', /*masa_kerja_bulan*/
					'" . addslashes($request->nip) . "', /*nip_nrp_nik*/
					'" . $request->nama_hrd . "', /*nama_hrd*/
					'" . $no_fixed_line_hrd . "', /*no_fixed_line_hrd*/
					'" . $request->tahun_bekerja_ditempat_lain . "', /*pengalaman_kerja_tahun*/
					'" . $request->bulan_bekerja_ditempat_lain . "', /*pengalaman_kerja_bulan*/
					'" . $penghasilan . "', /*pendapatan_borrower*/
					'" . $biaya_hidup . "', /*biaya_hidup*/
					'" . addslashes($detail_penghasilan_lain_lain) . "', /*detail_penghasilan_lain_lain*/
					'" . $total_penghasilan_lain_lain . "', /*total_penghasilan_lain_lain*/
					'" . $nilai_spt . "', /*nilai_spt*/
					'BorrowerProsesController',
					'994',
					'" . date('Y-m-d H:i:s') . "', /*createtd at*/
					'" . date('Y-m-d H:i:s') . "'
					) as response;"
				);

				if ($request->status_kawin == 1) {

					// insert pasangan			 
					$insertPasangan = DB::select(
						"SELECT put_into_brw_pasangan('" . Auth::guard('borrower')->user()->brw_id . "', '" . $request->nama_pasangan . "', '" . $request->jns_kelamin_pasangan . "', 
							'" . $request->ktp_pasangan . "', '" . $request->tempat_lahir_pasangan . "', '" . $request->tanggal_lahir_pasangan . "', '62" . $request->telepon_pasangan . "', '" . $request->agama_pasangan . "',
							'" . $request->pendidikan_terakhir_pasangan . "',  '" . $request->npwp_pasangan . "', '" . addslashes($request->alamat_pasangan) . "', '" . $request->provinsi_pasangan . "' , '" . $request->kota_pasangan . "', 
							'" . $request->kecamatan_pasangan . "', '" . $request->kelurahan_pasangan . "', '" . $request->kode_pos_pasangan . "','" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "', 'BorrowerProsesController', '938',
							'" . $request->kk_pasangan . "'
						) as responsePasangan;"
					);

					$insertPenghasilanPasangan = DB::select(
						"SELECT put_into_brw_pekerjaan_pasangan(
						'" . Auth::guard('borrower')->user()->brw_id . "',
						'" . $request->sumber_penghasilan_dana_pasangan . "', /*sumber_pengembalian_dana*/
						'" . addslashes($request->nama_perusahaan_pasangan) . "', /*nama_perusahaan*/
						'" . addslashes($request->alamat_perusahaan_pasangan) . "', /*alamat_perusahaan*/
						'" . addslashes($request->rt_perusahaan_pasangan) . "', /*rt*/
						'" . addslashes($request->rw_perusahaan_pasangan) . "', /*rw*/
						'" . $request->provinsi_perusahaan_pasangan . "', /*provinsi*/
						'" . $request->kota_perusahaan_pasangan . "', /*kab_kota*/
						'" . $request->kecamatan_perusahaan_pasangan . "', /*kecamatan*/
						'" . $request->kelurahan_perusahaan_pasangan . "', /*kelurahan*/
						'" . $request->kode_pos_perusahaan_pasangan . "', /*kode_pos*/
						'" . $no_telpon_usaha_pasangan . "', /*no_telp*/
						'" . $no_hp_usaha_pasangan . "', /*no_hp*/
						'" . $request->surat_ijin_usaha_pasangan . "', /*surat_ijin*/
						'" . addslashes($request->no_ijin_usaha_pasangan) . "', /*no_surat_ijin*/
						'" . $request->badan_usaha_pasangan . "', /*bentuk_badan_usaha*/
						'" . $request->status_kepegawaian_pasangan . "', /*status_pekerjaan*/
						'" . $request->usia_perusahaan_pasangan . "', /*usia_perusahaan*/
						'" . $request->usia_perusahaan_pasangan . "', /*usia_tempat_usaha*/
						'" . addslashes($request->departemen_pasangan) . "', /*departemen*/
						'" . addslashes($request->jabatan_pasangan) . "', /*jabatan*/
						'" . $request->tahun_bekerja_pasangan . "', /*masa_kerja_tahun*/
						'" . $request->bulan_bekerja_pasangan . "', /*masa_kerja_bulan*/
						'" . addslashes($request->nip_pasangan) . "', /*nip_nrp_nik*/
						'" . $request->nama_hrd_pasangan . "', /*nama_hrd*/
						'" . $no_hrd_pasangan . "', /*no_fixed_line_hrd*/
						'" . $request->tahun_bekerja_ditempat_lain_pasangan . "', /*pengalaman_kerja_tahun*/
						'" . $request->bulan_bekerja_ditempat_lain_pasangan . "', /*pengalaman_kerja_bulan*/
						'" . $penghasilan_pasangan . "', /*pendapatan_borrower*/
						'" . $biaya_hidup_pasangan . "', /*biaya_hidup*/
						'" . addslashes($detail_penghasilan_lain_lain_pasangan) . "', /*detail_penghasilan_lain_lain*/
						'" . $total_penghasilan_lain_lain_pasangan . "', /*total_penghasilan_lain_lain*/
						'" . $nilai_spt_pasangan . "', /*nilai_spt*/
						'BorrowerProsesController',
						'1141'
						) as insertPenghasilanPasangan;"
					);
				} else {
					DB::table('brw_pasangan')->where('pasangan_id', Auth::guard('borrower')->user()->brw_id)->delete();
				}


				// insert data Ahli Waris			 
				// $insertAhliWaris= DB::select("select put_into_brw_ahli_waris('".Auth::guard('borrower')->user()->brw_id."', '".$request->txt_nm_ahli_waris."', '".$request->txt_hubungan_ahli_waris."', 
				// 		'".$request->txt_jns_kelamin_ahli_waris."', '".$request->txt_no_ktp_ahli_waris."', '".$request->txt_tmpt_lahir_ahli_waris."', '".$request->txt_tgl_lahir_ahli_waris."',
				// 		'".$request->txt_noHP_ahli_waris."',  '".$request->txt_agama_ahli_waris."', '".$request->txt_pendidikanT_ahli_waris."', '".$request->txt_alamat_ahli_waris."',
				// 		'".$request->txt_provinsi_ahli_waris."', '".$request->txt_kota_ahli_waris."', '".$request->txt_kecamatan_ahli_waris."', 
				// 		'".$request->txt_kelurahan_ahli_waris."', '".$request->txt_kd_pos_ahli_waris."', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."', 'BorrowerProsesController', '950'
				//     ) as responseAhliWaris;"

				// );

				// update status borrower
				if ($request->status_kawin == 1 && $request->status == 2) {
					$updateStatusBorrower = DB::table('brw_user')
						->where('brw_id', Auth::guard('borrower')->user()->brw_id)
						->update(['status' => "active"]);

					$historyDataStatus = 1;
				}

				// insert data rekening
				$insertRekening = DB::select(
					"select put_into_brw_rekening('" . Auth::guard('borrower')->user()->brw_id . "', '',  '" . $request->no_rekening . "', '" . $request->nama_pemilik_rekening . "', '" . $request->bank . "',
						'2000000000','0','2000000000','" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "','BorrowerProsesController', '639', '" . $request->kantor_cabang . "'
					) as responseRekening;"
				);
				break;

			default: // badan hukum
				$txt_omset_thn_akhir = str_replace(".", "", $request->txt_omset_thn_akhir);
				$txt_aset_thn_akhir = str_replace(".", "", $request->txt_aset_thn_akhir);

				$exPengurus = explode('^~', $request->pengurus);

				$splitTglBerdiri = explode("-", $request->txt_tgl_berdiri_badan_hukum);
				$mergeTglBerdiri = $splitTglBerdiri[2] . '-' . $splitTglBerdiri[1] . '-' . $splitTglBerdiri[0];
				for ($ex = 0; $ex < count($exPengurus); $ex++) {

					$dataPengurus = explode('@,@', $exPengurus[$ex]);
					$splitTglLahirPengurus = explode("-", $dataPengurus[4]);
					$mergeTglLahirPengurus = $splitTglLahirPengurus[2] . '-' . $splitTglLahirPengurus[1] . '-' . $splitTglLahirPengurus[0];


					// $dataPengurus[0] nama pengurus 1
					// $dataPengurus[1] nama jenis kelamin
					// $dataPengurus[2] nama KTP
					// $dataPengurus[3] Tempat Lahir
					// $dataPengurus[4] Tanggal Lahir
					// $dataPengurus[5] Nomor HP
					// $dataPengurus[6] Agama
					// $dataPengurus[7] Pendidikan
					// $dataPengurus[8] Npwp
					// $dataPengurus[9] Jabatan
					// $dataPengurus[10] Alamat
					// $dataPengurus[11] Prov
					// $dataPengurus[12] Kota
					// $dataPengurus[13] Kecamatan
					// $dataPengurus[14] Kelurahan
					// $dataPengurus[15] Kd pos

					// dd($dataPengurus[13]);
					if ($ex == 0) {
						$insertDetailBorrower = DB::select(
							"select put_into_brw_user_detail(
									'" . Auth::guard('borrower')->user()->brw_id . "',
									'" . $dataPengurus[0] . "',
									'" . $request->txt_nm_badan_hukum . "', /*nm_bdn_hukum*/ 
									'" . $request->txt_nib_badan_hukum . "', /*nib*/ 
									'" . $request->txt_npwp_badan_hukum . "', /*npwp_perusahaan*/ 
									'" . $request->txt_akta_badan_hukum . "', /*akta*/
									'" . $request->txt_tgl_berdiri_badan_hukum . "', /*tgl berdiri*/
									'" . $request->txt_tlp_badan_hukum . "', /*tlp_perusahaan*/
									'" . $request->url_npwp_badan_hukum . "', /*foto_npwp_perusahaan*/
									'" . $request->txt_bd_pekerjaan_badan_hukum . "', /*bidang usaha*/
									'" . $txt_omset_thn_akhir . "', /*omset*/
									'" . $txt_aset_thn_akhir . "', /*tot aset*/
									'" . $dataPengurus[9] . "', /*jabatan*/
									'2', /*brw_tipe*/
									'', /*nama ibu*/
									'" . $dataPengurus[2] . "', /*ktp*/
									'" . $dataPengurus[8] . "', /*npwp*/
									'" . $dataPengurus[4] . "',/*tgl lahir*/
									'" . $dataPengurus[5] . "', /*no_tlp*/
									'" . $dataPengurus[1] . "', /*jenis_kelamin*/
									'', /*status_kawin*/
									'', /*status rumah*/
									'" . addslashes($request->txt_alamat_badan_hukum) . "', /*alamat*/
									'" . addslashes($request->txt_alamat_domisili_badan_hukum) . "',  /*domisili*/
									'" . $request->txt_provinsi_domisili_badan_hukum . "', /*domisili prov*/
									'" . $request->txt_kota_domisili_badan_hukum . "', /*domisili kota*/
									'" . $request->txt_kecamatan_domisili_badan_hukum . "', /*domisili kec*/
									'" . $request->txt_kelurahan_domisili_badan_hukum . "', /*domisili kel*/
									'" . $request->txt_kd_pos_domisili_badan_hukum . "', /*domisili kd pos*/
									'',  /*status rumah*/
									'" . $request->txt_provinsi_badan_hukum . "', /*provinsi*/
									'" . $request->txt_kota_badan_hukum . "', /*kota*/
									'" . $request->txt_kecamatan_badan_hukum . "', /*kec*/
									'" . $request->txt_kelurahan_badan_hukum . "', /*kel*/
									'" . $request->txt_kd_pos_badan_hukum . "', /*kd pos*/
									'" . $dataPengurus[6] . "', /*agama*/
									'" . $request->txt_tmpt_lahir_pribadi . "', /*tempat lahir*/
									'" . $dataPengurus[7] . "', /*pendidikan terakhir*/
									'', /*pekerjaan*/
									'', /*bidang perusahaan*/
									'', /*bidang pekerjaan*/
									'', /*bidang online*/
									'', /*pengalaman pekerjaan*/
									'', /*pendapatan*/
									'', /*total aset*/
									'', /*warganegara*/
									'', /*brw online*/
									'" . $dataPengurus[16] . "', 
									'" . $dataPengurus[17] . "',
									'" . $dataPengurus[18] . "',
									'" . $dataPengurus[19] . "',
									'', /*no kk*/
									'', /*lama menempati*/
									'', /*tlp rumah*/
									'', /*kartu kredit*/
									'', /*usia pensiun*/
									'', /*alamat penagihan rumah*/
									'', /*alamat penagihan kantor*/
									'', /*npwp pasangan*/
									'', /*nomor kk pasangan*/
									'', /*tmpt lahir pasangan*/
									'', /*tgl lahir pasangan*/
									'', /*pendidikan pasangan*/
									'', /*sd pasangan*/
									'', /*smp pasangan*/
									'', /*sma pasangan*/
									'', /*pt pasangan*/
									'', /*pekerjaan pasangan*/
									'', /*bd pekerjaan pasangan*/
									'', /*bd pekerjaanO pasangan*/
									'',  /*pengalaman pasangan*/
									'', /*pendapatan pasangan*/
									'', /*kartu kredit*/
									'', /*jmlasettdkbergerak_pasangan*/
									'', /*mlasetbergerak_pasangan*/
									'', /*agama pasangan*/
									'', /*pic kk*/
									'', /*pic ktp pasangan*/
									'', /*pic surat nikah*/
									'', /*pic spt*/
									'', /*pic rek koran*/
									'', /*pic slip gaji*/
									'', /*pic lap keuangan*/
									'" . date('Y-m-d H:i:s') . "', /*createtd at*/
									'" . date('Y-m-d H:i:s') . "',  /*update at*/
									'BorrowerProsesController',
									'916'
									) as response;"
						);

						$insertjaminan = DB::select(
							"select put_into_brw_pengurus('" . Auth::guard('borrower')->user()->brw_id . "',
								'" . $dataPengurus[0] . "', '" . $dataPengurus[1] . "', '" . $dataPengurus[2] . "', '" . $dataPengurus[3] . "', '" . $dataPengurus[4] . "', '" . $dataPengurus[5] . "', 
								'" . $dataPengurus[6] . "', '" . $dataPengurus[7] . "', '" . $dataPengurus[8] . "', '" . $dataPengurus[9] . "', '" . addslashes($dataPengurus[10]) . "', '" . $dataPengurus[11] . "', 
								'" . $dataPengurus[12] . "', '" . $dataPengurus[13] . "', '" . $dataPengurus[14] . "', '" . $dataPengurus[15] . "', '" . $dataPengurus[16] . "', '" . $dataPengurus[17] . "', 
								'" . $dataPengurus[18] . "', '" . $dataPengurus[19] . "', '', '', 'BorrowerProsesController', '906') as responseJaminan;"
						);


						continue;
					} else {
						// insert data pengurus
						$insertPengurus = DB::select(
							"select put_into_brw_pengurus('" . Auth::guard('borrower')->user()->brw_id . "',
								'" . $dataPengurus[0] . "', '" . $dataPengurus[1] . "', '" . $dataPengurus[2] . "', '" . $dataPengurus[3] . "', '" . $dataPengurus[4] . "', '" . $dataPengurus[5] . "', 
								'" . $dataPengurus[6] . "', '" . $dataPengurus[7] . "', '" . $dataPengurus[8] . "', '" . $dataPengurus[9] . "', '" . addslashes($dataPengurus[10]) . "', '" . $dataPengurus[11] . "', 
								'" . $dataPengurus[12] . "', '" . $dataPengurus[13] . "', '" . $dataPengurus[14] . "', '" . $dataPengurus[15] . "', '" . $dataPengurus[16] . "', '" . $dataPengurus[17] . "', 
								'" . $dataPengurus[18] . "', '" . $dataPengurus[19] . "', '', '', 'BorrowerProsesController', '906') as responseJaminan;"
						);
						// dd($insertPengurus);
					}
				}

				// insert data rekening
				$insertRekening = DB::select(
					"select put_into_brw_rekening('" . Auth::guard('borrower')->user()->brw_id . "', '',  '" . $request->txt_no_rekening_badan_hukum . "', 
					'" . $request->txt_nm_pemilik_badan_hukum . "', '" . $request->txt_bank_badan_hukum . "', '2000000000','0','2000000000','','','BorrowerProsesController', '688', '" . $request->txt_kantor_cabang_pembuka_badan_hukum . "'
                    ) as responseRekening;"
				);

				$updateStatusBorrower = DB::table('brw_user')
					->where('brw_id', Auth::guard('borrower')->user()->brw_id)
					->update(['status' => "active"]);
		}

		$borrowerDetails = DB::table('brw_user_detail')
			->where('brw_id', Auth::guard('borrower')->user()->brw_id)->first();

		Session::put('brw_type', $borrowerDetails === null ? null : $borrowerDetails->brw_type);

		if ($request->status_kawin != 1 && $request->status == 1) {
			$updateStatusBorrower = DB::table('brw_user')
				->where('brw_id', Auth::guard('borrower')->user()->brw_id)
				->update(['status' => "active"]);
			$historyDataStatus = 1;
		}

		$response = [
			'status' => 'sukses',
			'message' => 'borrower berhasil ditambahkan',
			'history_data_status' => $historyDataStatus,
			'data_borrower' => 'null'

		];

		return response()->json($response);
	}

	/************** ARL SIMPLIFIKASI **********************/
	public function action_lengkapi_profile_pendanaan_simple(Request $request)
	{

		$result = Helper::checkLimitPengajuan(Auth::guard('borrower')->user()->brw_id, 'web');
		if($result['hasil'] == 0) return response()->json(['status' => 'limit', 'message' => $result['responseMessage']['errors']['pengajuan'][0], 'data_borrower'=>null]);

		// return $request->all();
		$ktp = $request->ktp;
		$nama = $request->nama;
		$agama = $request->agama;
		$alamat = addslashes($request->alamat);
		$telepon = $request->telepon;

		$bidang_online = $request->bidang_online;
		$bidang_pekerjaan = $request->bidang_pekerjaan;

		$kecamatan = $request->kecamatan;
		$kelurahan = $request->kelurahan;
		$kode_pos = $request->kode_pos;
		$kota = $request->kota;
		$provinsi = $request->provinsi;

		$npwp = $request->npwp;
		$pekerjaan = $request->pekerjaan;
		$pendidikan = $request->pendidikan;
		$penghasilan = str_replace(".", "", $request->penghasilan);

		$status_kepemilikan_rumah = $request->status_kepemilikan_rumah;
		$status_kawin = $request->status_pernikahan;

		// $borrower_type = $request->borrower_type; 
		$nama_perusahaan = $request->nama_perusahaan;
		$bulan_bekerja = $request->bulan_bekerja;
		$tahun_bekerja = $request->tahun_bekerja;
		$usia_perusahaan = $request->usia_perusahaan;
		$no_telpon_usaha = $request->no_telpon_usaha;

		$tipe_pendanaan = $request->type_pendanaan;
		$text_tipe_pendanaan = $request->text_type_pendanaan;
		$type_tujuan_pendanaan = $request->tujuan_pendanaan;

		$harga_objek_pendanaan = $request->harga_objek_pendanaan;
		$uang_muka = $request->uang_muka;
		$persen = $request->persen;
		$nilai_pengajuan = $request->nilai_pengajuan;
		$jangka_waktu = $request->jangka_waktu;

		$jenis_property = $request->jenis_property;

		$type = $request->borrower_type;
		$historyDataStatus = 0;

		$tempat_lahir = addslashes($request->tempat_lahir);
		if (isset($request->txt_estimasi_proyek)) {
			$estimasi_mulai = $request->txt_estimasi_proyek;
		} else {
			$estimasi_mulai = "";
		}
		switch ($type) {
			case 1: // individu

				$cekGender = (int) substr($request->ktp, 6, 2);
				if ($cekGender <= 31) {
					$gender = 1;
				} else {
					$gender = 2;
				}
				if (substr($ktp, 10, 1) > substr(date('Y'), 2, 1)) {
					$tahunawalan = '19';
				} else {
					$tahunawalan = '20';
				}
				$cekTglLahir = (int) substr($ktp, 6, 2);
				if ($cekTglLahir <= 31) {
					$tanggal_lahir = $tahunawalan . substr($ktp, 10, 2) . "-" . substr($ktp, 8, 2) . "-" . substr($ktp, 6, 2);
				} else {
					$cekTglLahir = $cekTglLahir - 40;
					$tanggal_lahir = $tahunawalan . substr($ktp, 10, 2) . "-" . substr($ktp, 8, 2) . "-" . $cekTglLahir;
				}

				DB::select("SELECT put_into_brw_user_detail_simplify('" . Auth::guard('borrower')->user()->brw_id . "','" . $nama . "','','','','','','','','','','','','1','','" . $ktp . "','" . $npwp . "','" . $tanggal_lahir . "','62" . $telepon . "','" . $gender . "','" . $status_kawin . "','" . $status_kepemilikan_rumah . "', '".$alamat."' ,'', '','','','','" . $kode_pos . "','', '" . $provinsi . "','" . $kota . "','" . $kecamatan . "','" . $kelurahan . "','" . $kode_pos . "','" . $agama . "','" . $tempat_lahir . "','" . $pendidikan . "','" . $pekerjaan . "','','" . $bidang_pekerjaan . "','" . $bidang_online . "','','" . $penghasilan . "','','','','" . $request->user_camera_diri . "','" . $request->user_camera_ktp . "','" . $request->user_camera_diri_dan_ktp . "','" . $request->user_camera_npwp . "','','','','','','','','','','','','','','','','','','','','', '','','','','','','','','','','','','" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "', 'BorrowerProsesController','" . __LINE__ . "') as response;");

				$brwUserDetailPenghasilan = DB::table('brw_user_detail_penghasilan')->where('brw_id2', Auth::guard('borrower')->user()->brw_id)->first();

				if($brwUserDetailPenghasilan){
					$updateData = array(
							'pendapatan_borrower' => $penghasilan,
							'nama_perusahaan'=> $nama_perusahaan,
							'no_telp'=> '62'.$no_telpon_usaha,
							'masa_kerja_tahun'=> $tahun_bekerja,
							'masa_kerja_bulan'=> $bulan_bekerja,
							'updated_at'=> date('Y-m-d H:i:s')
					);
					DB::table('brw_user_detail_penghasilan')->where('brw_id2', Auth::guard('borrower')->user()->brw_id)->update($updateData);

				}else{

					// syslog(0,"insertDetailBorrower = ".json_encode($insertDetailBorrower));
				    $insertPenghasilan = DB::select("SELECT put_into_brw_user_detail_penghasilan_simplify('" . Auth::guard('borrower')->user()->brw_id . "','', '', '" . $nama_perusahaan . "', '', '', '', '', '', '', '', '', '62" . $no_telpon_usaha . "', '', '', '', '', '', '" . $usia_perusahaan . "', '', '', '', '" . $tahun_bekerja . "', '" . $bulan_bekerja . "', '', '', '', '', '', '" . $penghasilan . "', '', '', '', '', 'BorrowerProsesController','994','" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "') as response;");
				}	


				if ($status_kawin != 1) {
					DB::table('brw_pasangan')->where('pasangan_id', Auth::guard('borrower')->user()->brw_id)->delete();
				}

				// ************** PENGAJUAN ***********************
				if ($tipe_pendanaan == '2') {
					$check_input_validation = DB::select("SELECT val_ajukan_kpr('" . $tipe_pendanaan . "','1','" . str_replace(".", "", $harga_objek_pendanaan) . "','2','" . str_replace(".", "", $uang_muka) . "','3','" . str_replace(".", "", $nilai_pengajuan) . "','BorrowerProsesController.php','" . __LINE__ . "') as response;");

					$data = $check_input_validation[0]->response;

					if ($data != "1") {
						return [
							'status' => 'gagal',
							'message' => $data,
							'data_borrower' => 'null'
						];
					}
				}

				$pengajuan = DB::table('brw_pengajuan')->insert(array(
					"id_proyek" => "",
					"brw_id" => Auth::guard('borrower')->user()->brw_id,
					"pendanaan_nama" => "",
					"pendanaan_tipe" => $tipe_pendanaan,
					"pendanaan_tujuan" => $type_tujuan_pendanaan,
					"pendanaan_akad" => "",
					"pendanaan_dana_dibutuhkan" => str_replace(".", "", $nilai_pengajuan),
					"pendanaan_dana_disetujui" => "0",
					"harga_objek_pendanaan" => str_replace(".", "", $harga_objek_pendanaan),
					"uang_muka" => str_replace(".", "", $uang_muka),
					"estimasi_mulai" => $estimasi_mulai,
					"estimasi_imbal_hasil" => "",
					"mode_pembayaran" => "0",
					"metode_pembayaran" => "0",
					"durasi_proyek" => $jangka_waktu,
					"detail_pendanaan" => "",
					"dana_dicairkan" => "0",
					"status" => "0",
					"status_dana" => "0",
					"lokasi_proyek" => "",
					"geocode" => "",
					"provinsi" => "",
					"kota" => "",
					"kecamatan" => "",
					"kelurahan" => "",
					"kode_pos" => "",
					"va_number" => "",
					"gambar_utama" => "gambar",
					"nm_pemilik" => "",
					"no_tlp_pemilik" => "",
					"alamat_pemilik" => "",
					"provinsi_pemilik" => "",
					"kota_pemilik" => "",
					"kecamatan_pemilik" => "",
					"kelurahan_pemilik" => "",
					"kd_pos_pemilik" => "",
					"status_tampil_pengajuan" => "tampil",
					"keterangan" => "",
					"jenis_properti" => $jenis_property,
					"keterangan_approval" => "",
					"created_at" => date('Y-m-d H:i:s'),
					"updated_at" => date('Y-m-d H:i:s'),
				));

				$response = '';

				if (!$pengajuan) {

					$log_db_app = DB::table('log_db_app')->insert(array(
						"filename" => "BorrowerProsesController",
						"line" => __LINE__,
						"description" => "Proses Ajukan Dana Rumah BRW ID" . Auth::guard('borrower')->user()->brw_id . " Gagal",
						"created_at" => date('Y-m-d H:i:s')
					));

					$response = [
						'status' => 'gagal',
						'message' => 'pengajuan pendanaan DANA RUMAH gagal',
						'data_borrower' => 'null'
					];
				} else {

					$pengajuan_id   = DB::getPDO()->lastInsertId();
					$insertLogPengajuan = DB::select("select put_into_brw_log_pengajuan('" . $pengajuan_id . "', '" . Auth::guard('borrower')->user()->brw_id . "', '0', 'Pendanaan DANA RUMAH Anda Akan kami Proses', '','',  'BorrowerProsesController', '" . __LINE__ . "') as responseLogPengajuan;");

					$updateStatusBorrower = DB::table('brw_user')
						->where('brw_id', Auth::guard('borrower')->user()->brw_id)
						->where('status', 'notpreapproved')
						->update(['status' => "notfilled"]);

					$historyDataStatus = 1;
					$response = [
						'stts' => "00",
						'status' => 'sukses',
						'message' => 'pengajuan pendanaan ' . $text_tipe_pendanaan . ' berhasil',
						'history_data_status' => 'simplify',
						'data_borrower' => 'null',
						'pengajuan_id' =>  Auth::guard('borrower')->user()->brw_id . '_' . $pengajuan_id,
					];


					//create profile pengajuan 
					$responseProfilePengajuan = \DB::select('select update_user_profile_pengajuan(?, ?) as response', [$pengajuan_id,  'create']);

				}




				break;

			default: // badan hukum
				//         pengurus: "123123123123@,@1@,@1231231231231231@,@123123123123@,@2021-10-15@,@628723424234234@,@2@,@2@,@123123123123123@,@4@,@egwergwergweerg@,@Bangka Belitung@,@Bangka Barat@,@Kelapa@,@Dendang@,@20811@,@borrower/6702/foto_pengurus_1.png@,@borrower/6702/foto_ktp_pengurus_1.png@,@borrower/6702/foto_ktp_diri_pengurus_1.png@,@borrower/6702/foto_npwp_pengurus_1.png^~123123123123@,@2@,@1231231231231231@,@123123123123@,@2021-10-05@,@628723223232323@,@3@,@3@,@123123123123123@,@2@,@32r23r23r2r23r@,@Bangka Belitung@,@Bangka Selatan@,@Pulau Besar@,@Sukajaya@,@16610@,@borrower/6702/foto_pengurus_2.png@,@borrower/6702/foto_ktp_pengurus_2.png@,@borrower/6702/foto_ktp_diri_pengurus_2.png@,@borrower/6702/foto_npwp_pengurus_2.png"
				// txt_akta_badan_hukum: "23423"
				// txt_alamat_badan_hukum: "weqfqefqrfqf"
				// txt_alamat_domisili_badan_hukum: "weqfqefqrfqf"
				// txt_aset_thn_akhir: "123.123.123.123"
				// txt_bank_badan_hukum: "013"
				// txt_bd_pekerjaan_badan_hukum: "4"
				// txt_kantor_cabang_pembuka_badan_hukum: "123123123123"
				// txt_kd_pos_badan_hukum: "21183"
				// txt_kd_pos_domisili_badan_hukum: "21183"
				// txt_kecamatan_badan_hukum: "Simpang Teritip"
				// txt_kecamatan_domisili_badan_hukum: "Simpang Teritip"
				// txt_kelurahan_badan_hukum: "Mayang"
				// txt_kelurahan_domisili_badan_hukum: "Mayang"
				// txt_kota_badan_hukum: "Bangka Barat"
				// txt_kota_domisili_badan_hukum: "Bangka Barat"
				// txt_nib_badan_hukum: "wrerwer"
				// txt_nm_badan_hukum: "123412341234"
				// txt_nm_pemilik_badan_hukum: "131231231231"
				// txt_no_rekening_badan_hukum: "2213123123123"
				// txt_npwp_badan_hukum: "234234234234222"
				// txt_omset_thn_akhir: "13.123.123.123"
				// txt_provinsi_badan_hukum: "Bangka Belitung"
				// txt_provinsi_domisili_badan_hukum: "Bangka Belitung"
				// txt_tgl_berdiri_badan_hukum: "2021-10-12"
				// txt_tlp_badan_hukum: "0218742342334234"
				// type_borrower: "2"
				// url_npwp_badan_hukum: "borrower/6702/npwp_badan_hukum.png"
				// _token: "7Xi0A7nl1PcnQgfe0DGwN3cwFjsvYXownOq2FITL"
				// return $request->all();

				$txt_omset_thn_akhir = str_replace(".", "", $request->txt_omset_thn_akhir);
				$txt_aset_thn_akhir = str_replace(".", "", $request->txt_aset_thn_akhir);

				$exPengurus = explode('^~', $request->pengurus);

				$splitTglBerdiri = explode("-", $request->txt_tgl_berdiri_badan_hukum);
				$mergeTglBerdiri = $splitTglBerdiri[2] . '-' . $splitTglBerdiri[1] . '-' . $splitTglBerdiri[0];

				$tgl_akta_perubahan = Carbon::parse($request->tgl_akta_perubahan)->format('Y-m-d');
				for ($ex = 0; $ex < count($exPengurus); $ex++) {

					$dataPengurus = explode('@,@', $exPengurus[$ex]);
					$splitTglLahirPengurus = explode("-", $dataPengurus[4]);
					$mergeTglLahirPengurus = $splitTglLahirPengurus[2] . '-' . $splitTglLahirPengurus[1] . '-' . $splitTglLahirPengurus[0];


					// $dataPengurus[0] nama pengurus 1
					// $dataPengurus[1] nama jenis kelamin
					// $dataPengurus[2] nama KTP
					// $dataPengurus[3] Tempat Lahir
					// $dataPengurus[4] Tanggal Lahir
					// $dataPengurus[5] Nomor HP
					// $dataPengurus[6] Agama
					// $dataPengurus[7] Pendidikan
					// $dataPengurus[8] Npwp
					// $dataPengurus[9] Jabatan
					// $dataPengurus[10] Alamat
					// $dataPengurus[11] Prov
					// $dataPengurus[12] Kota
					// $dataPengurus[13] Kecamatan
					// $dataPengurus[14] Kelurahan
					// $dataPengurus[15] Kd pos

					// dd($dataPengurus[13]);
					if ($ex == 0) {
						$insertDetailBorrower = DB::select(
							"select put_into_brw_user_detail(
									'" . Auth::guard('borrower')->user()->brw_id . "',
									'" . $dataPengurus[0] . "',
									'" . $request->txt_nm_badan_hukum . "', /*nm_bdn_hukum*/ 
									'" . $request->txt_nib_badan_hukum . "', /*nib*/ 
									'" . $request->txt_npwp_badan_hukum . "', /*npwp_perusahaan*/ 
									'" . $request->txt_akta_badan_hukum . "', /*akta*/
									'" . $request->no_akta_perubahan. "', /* akta perubahan */
									'" . $tgl_akta_perubahan. "', /* tgl akta perubahan */
									'" . $request->txt_tgl_berdiri_badan_hukum . "', /*tgl berdiri*/
									'" . $request->kode_telepon . "', /*kode_telepon*/
									'" . $request->txt_tlp_badan_hukum . "', /*tlp_perusahaan*/
									'" . $request->url_npwp_badan_hukum . "', /*foto_npwp_perusahaan*/
									'" . $request->txt_bd_pekerjaan_badan_hukum . "', /*bidang usaha*/
									'" . $txt_omset_thn_akhir . "', /*omset*/
									'" . $txt_aset_thn_akhir . "', /*tot aset*/
									'" . $dataPengurus[9] . "', /*jabatan*/
									'2', /*brw_tipe*/
									'', /*nama ibu*/
									'" . $dataPengurus[2] . "', /*ktp*/
									'" . $dataPengurus[8] . "', /*npwp*/
									'" . $dataPengurus[4] . "',/*tgl lahir*/
									'" . $dataPengurus[5] . "', /*no_tlp*/
									'" . $dataPengurus[1] . "', /*jenis_kelamin*/
									'', /*status_kawin*/
									'', /*status rumah*/
									'" . addslashes($request->txt_alamat_badan_hukum) . "', /*alamat*/
									'" . addslashes($request->txt_alamat_domisili_badan_hukum) . "',  /*domisili*/
									'" . $request->txt_provinsi_domisili_badan_hukum . "', /*domisili prov*/
									'" . $request->txt_kota_domisili_badan_hukum . "', /*domisili kota*/
									'" . $request->txt_kecamatan_domisili_badan_hukum . "', /*domisili kec*/
									'" . $request->txt_kelurahan_domisili_badan_hukum . "', /*domisili kel*/
									'" . $request->txt_kd_pos_domisili_badan_hukum . "', /*domisili kd pos*/
									'',  /*status rumah*/
									'" . $request->txt_provinsi_badan_hukum . "', /*provinsi*/
									'" . $request->txt_kota_badan_hukum . "', /*kota*/
									'" . $request->txt_kecamatan_badan_hukum . "', /*kec*/
									'" . $request->txt_kelurahan_badan_hukum . "', /*kel*/
									'" . $request->txt_kd_pos_badan_hukum . "', /*kd pos*/
									'" . $dataPengurus[6] . "', /*agama*/
									'" . $request->txt_tmpt_lahir_pribadi . "', /*tempat lahir*/
									'" . $dataPengurus[7] . "', /*pendidikan terakhir*/
									'', /*pekerjaan*/
									'', /*bidang perusahaan*/
									'', /*bidang pekerjaan*/
									'', /*bidang online*/
									'', /*pengalaman pekerjaan*/
									'', /*pendapatan*/
									'', /*total aset*/
									'', /*warganegara*/
									'', /*brw online*/
									'" . $dataPengurus[16] . "', 
									'" . $dataPengurus[17] . "',
									'" . $dataPengurus[18] . "',
									'" . $dataPengurus[19] . "',
									'', /*no kk*/
									'', /*lama menempati*/
									'', /*tlp rumah*/
									'', /*kartu kredit*/
									'', /*usia pensiun*/
									'', /*alamat penagihan rumah*/
									'', /*alamat penagihan kantor*/
									'', /*npwp pasangan*/
									'', /*nomor kk pasangan*/
									'', /*tmpt lahir pasangan*/
									'', /*tgl lahir pasangan*/
									'', /*pendidikan pasangan*/
									'', /*sd pasangan*/
									'', /*smp pasangan*/
									'', /*sma pasangan*/
									'', /*pt pasangan*/
									'', /*pekerjaan pasangan*/
									'', /*bd pekerjaan pasangan*/
									'', /*bd pekerjaanO pasangan*/
									'',  /*pengalaman pasangan*/
									'', /*pendapatan pasangan*/
									'', /*kartu kredit*/
									'', /*jmlasettdkbergerak_pasangan*/
									'', /*mlasetbergerak_pasangan*/
									'', /*agama pasangan*/
									'', /*pic kk*/
									'', /*pic ktp pasangan*/
									'', /*pic surat nikah*/
									'', /*pic spt*/
									'', /*pic rek koran*/
									'', /*pic slip gaji*/
									'', /*pic lap keuangan*/
									'" . $request->is_tbk . "', /*is tbk*/
									'" . date('Y-m-d H:i:s') . "', /*createtd at*/
									'" . date('Y-m-d H:i:s') . "',  /*update at*/
									'BorrowerProsesController',
									'916'
									) as response;"
						);

						$insertpengurus = DB::select(
							"select put_into_brw_pengurus('" . Auth::guard('borrower')->user()->brw_id . "','1',
								'" . $dataPengurus[0] . "', '" . $dataPengurus[1] . "', '" . $dataPengurus[2] . "', '" . $dataPengurus[3] . "', '" . $dataPengurus[4] . "', '" . $dataPengurus[5] . "', 
								'" . $dataPengurus[6] . "', '" . $dataPengurus[7] . "', '" . $dataPengurus[8] . "', '" . $dataPengurus[9] . "', '" . addslashes($dataPengurus[10]) . "', '" . $dataPengurus[11] . "', 
								'" . $dataPengurus[12] . "', '" . $dataPengurus[13] . "', '" . $dataPengurus[14] . "', '" . $dataPengurus[15] . "', '" . $dataPengurus[16] . "', '" . $dataPengurus[17] . "', 
								'" . $dataPengurus[18] . "', '" . $dataPengurus[19] . "', NOW(), NOW(), 'BorrowerProsesController', '906') as responseJaminan;"
						);

						continue;
					} else {
						// insert data pengurus
						$insertPengurus = DB::select(
							"select put_into_brw_pengurus('" . Auth::guard('borrower')->user()->brw_id . "','2',
								'" . $dataPengurus[0] . "', '" . $dataPengurus[1] . "', '" . $dataPengurus[2] . "', '" . $dataPengurus[3] . "', '" . $dataPengurus[4] . "', '" . $dataPengurus[5] . "', 
								'" . $dataPengurus[6] . "', '" . $dataPengurus[7] . "', '" . $dataPengurus[8] . "', '" . $dataPengurus[9] . "', '" . addslashes($dataPengurus[10]) . "', '" . $dataPengurus[11] . "', 
								'" . $dataPengurus[12] . "', '" . $dataPengurus[13] . "', '" . $dataPengurus[14] . "', '" . $dataPengurus[15] . "', '" . $dataPengurus[16] . "', '" . $dataPengurus[17] . "', 
								'" . $dataPengurus[18] . "', '" . $dataPengurus[19] . "', NOW(), NOW(), 'BorrowerProsesController', '906') as responseJaminan;"
						);
						// dd($insertPengurus);
					}
				}

				// insert data rekening
				$insertRekening = DB::select(
					"select put_into_brw_rekening('" . Auth::guard('borrower')->user()->brw_id . "', '',  '" . $request->txt_no_rekening_badan_hukum . "','" . $request->txt_nm_pemilik_badan_hukum . "', '" . $request->txt_bank_badan_hukum . "', '2000000000','0','2000000000',NOW(),NOW(),'BorrowerProsesController', '688', '" . $request->txt_kantor_cabang_pembuka_badan_hukum . "') as responseRekening;"
				);

				

				try {

					DB::table('brw_user_detail_penghasilan')->insert([
						'brw_id2' => Auth::guard('borrower')->user()->brw_id,
						'updated_at' => date('Y-m-d H:i:s'),
						'created_at' => date('Y-m-d H:i:s')
					]);
				} catch (\Illuminate\Database\QueryException $e) {
		
					DB::table('brw_user_detail_penghasilan')->where('brw_id2',Auth::guard('borrower')->user()->brw_id)->update([
						'updated_at' => date('Y-m-d H:i:s'),
						'created_at' => date('Y-m-d H:i:s')
					]);
				}

				$updateStatusBorrower = DB::table('brw_user')
					->where('brw_id', Auth::guard('borrower')->user()->brw_id)
					->update(['status' => "notpreapproved"]);

				$response = [
					'status' => 'sukses',
					'message' => 'borrower dan pengajuan baru berhasil ditambahkan',
					'history_data_status' => $historyDataStatus,
					'data_borrower' => 'null',
					'pengajuan_id' =>  Auth::guard('borrower')->user()->brw_id

				];
		}

		$borrowerDetails = DB::table('brw_user_detail')
			->where('brw_id', Auth::guard('borrower')->user()->brw_id)->first();

		Session::put('brw_type', $borrowerDetails === null ? null : $borrowerDetails->brw_type);

		return response()->json($response);
	}

	public function proses_pendanaan(Request $request)
	{

		$result = Helper::checkLimitPengajuan(Auth::guard('borrower')->user()->brw_id , 'web');
		if($result['hasil'] == 0) return response()->json(['status' => 'limit', 'message' => $result['responseMessage']['errors']['pengajuan'][0]]);

		//die($request->txt_jenis_akad_pendanaan);
		// return $request->all();
		$type = $request->type_borrower;
		$brw_id = $request->brw_id;

		$explode = explode(',', $request->persyaratan_arr);
		$countPersyaratan =  count($explode);
		$array = array($request->persyaratan_arr);

		// insert data pengajuan
		// $insertPengajuan = DB::select("select put_into_brw_pengajuan('".Auth::guard('borrower')->user()->brw_id."', '".$request->txt_nm_pendanaan."', '".$request->type_pendanaan_select."',
		// 	'".$request->txt_jenis_akad_pendanaan."', '".$request->txt_dana_pendanaan."', '".Carbon::parse($request->txt_estimasi_proyek)->toDateString()."', '".$request->txt_estimasi_imbal_hasil."', '0', '0', 
		// 	'".$request->txt_durasi_pendanaan."','".$request->txt_detail_pendanaan."', '0', '0', '0','0', '".$request->txt_lokasi_proyek."','".$request->txt_geocode."',
		// 	'','gambar', 'tampil', '','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','BorrowerProsesController', '843') as pengajuan_id;"
		// );

		$pengajuan = DB::table('brw_pengajuan')->insert(array(

			// $pengajuan = DB::table('brw_pengajuan')->insertGetId(array(
			"id_proyek" => "",
			"brw_id" => Auth::guard('borrower')->user()->brw_id,
			"pendanaan_nama" => $request->txt_nm_pendanaan,
			"pendanaan_tipe" => $request->type_pendanaan_select,
			"pendanaan_tujuan" => $request->type_tujuan_pendanaan,
			"pendanaan_akad" => $request->txt_jenis_akad_pendanaan,
			"pendanaan_dana_dibutuhkan" => $request->txt_dana_pendanaan,
			"harga_objek_pendanaan" => $request->txt_dana_pendanaan,
			"pendanaan_dana_disetujui" => "0",
			"estimasi_mulai" => Carbon::parse($request->txt_estimasi_proyek)->toDateString(),
			"estimasi_imbal_hasil" => $request->txt_estimasi_imbal_hasil,
			"mode_pembayaran" => "0",
			"metode_pembayaran" => "0",
			"durasi_proyek" => $request->txt_durasi_pendanaan,
			"detail_pendanaan" => $request->txt_detail_pendanaan,
			"dana_dicairkan" => "0",
			"status" => "0",
			"status_dana" => "0",
			"lokasi_proyek" => $request->txt_lokasi_proyek,
			"geocode" => "",
			"va_number" => "",
			"gambar_utama" => "gambar",
			"status_tampil_pengajuan" => "tampil",
			"keterangan" => "",
			"keterangan_approval" => "",
			"created_at" => date('Y-m-d H:i:s'),
			"updated_at" => date('Y-m-d H:i:s'),
		));


		$pengajuan_id = "";
		if (!$pengajuan) {

			$insertLogPengajuan = DB::select(
				"select put_into_brw_log_pengajuan('', '" . Auth::guard('borrower')->user()->brw_id . "', 
					'0', 'Proses Insert Pengajuan Gagal', '','',  'BorrowerProsesController', '1167') as responseLogPengajuan;"
			);

			$log_db_app = DB::table('log_db_app')->insert(array(
				"filename" => "BorrowerProsesController",
				"line" => "1167",
				"description" => "Proses " . Auth::guard('borrower')->user()->brw_id . " Insert Pengajuan Gagal",
				"created_at" => date('Y-m-d H:i:s')
			));

			$response = [
				'status' => 'gagal',
				'message' => 'Maaf Proses Simpan Gagal, Silahkan Hubungi CSO Kami'

			];

			return response()->json($response);
		} else {
			$pengajuan_id   = DB::getPDO()->lastInsertId();;
		}


		// simpan gambar utama

		// $gambar_utama_path = 'proyek/TEMP_PROYEK/' . $insertPengajuan[0]->{'pengajuan_id'} .'/projectpic350x233';
		// $gambar_utama_path = 'proyek/TEMP_PROYEK/' . $pengajuan_id . '/projectpic350x233';
		// $uploadGambarUtama = $this->uploadPotoProyek('gambar_utama', $request->gambar_utama, $gambar_utama_path);
		// $updatePathGambar = BorrowerPengajuan::where('pengajuan_id',$insertPengajuan[0]->{'pengajuan_id'})->update(['gambar_utama' => $uploadGambarUtama]);
		// $updatePathGambar = BorrowerPengajuan::where('pengajuan_id', $pengajuan_id)->update(['gambar_utama' => $uploadGambarUtama]);

		// $gambar_proyek = $request->gambar_slider;
		if ($request->hasFile('gambar_slider')) {
			// $gambarpath = 'proyek/TEMP_PROYEK/' . $insertPengajuan[0]->{'pengajuan_id'} . '/projectpic730x486';
			$gambarpath = 'proyek/TEMP_PROYEK/' . $pengajuan_id . '/projectpic730x486';
			$i = count($gambar_proyek) + 1;
			foreach ($request->gambar_slider as $gambar) {


				// $this->uploadPotoProyek('gambar'.$i ,   $gambar, $gambarpath);
				$filename = Carbon::now()->toDateString() . 'gambar' . $i . '.' . $gambar->getClientOriginalExtension();

				$path = $gambar->storeAs($gambarpath, $filename, 'public');

				DB::table('gambar_proyek')->insert(
					//['pengajuan_id' => $insertPengajuan[0]->{'pengajuan_id'}, 'proyek_id' => '', 'gambar' => $path]
					['pengajuan_id' => $pengajuan_id, 'proyek_id' => '', 'gambar' => $path]
				);

				$i += 1;
			}
		}


		// cek persyaratan insert
		$cekpersyaratan = BorrowerPersyaratanInsert::where('brw_id', Auth::guard('borrower')->user()->brw_id)->where('persyaratan_id', $explode[0])->get();
		if (count($cekpersyaratan) == 0) {
			$tipe_id = $request->type_pendanaan_select;
			$user_type = $type;
			$getPersyaratan = BorrowerPersyaratanPendanaan::select('persyaratan_id', 'persyaratan_mandatory')->where('tipe_id', $tipe_id)->where('user_type', $user_type)->get();


			for ($i = 0; $i < count($getPersyaratan); $i++) {
				$masukanPersyaratan = new BorrowerPersyaratanInsert();
				$masukanPersyaratan->brw_id = Auth::guard('borrower')->user()->brw_id;
				$masukanPersyaratan->tipe_id = $tipe_id;
				$masukanPersyaratan->user_type = $user_type;
				$masukanPersyaratan->persyaratan_id = $getPersyaratan[$i]['persyaratan_id'];
				$masukanPersyaratan->checked = $getPersyaratan[$i]['persyaratan_mandatory'];
				$masukanPersyaratan->save();
			}
		}

		// update persyaratan
		for ($i = 0; $i < $countPersyaratan; $i++) {

			$persyaratan_id = $explode[$i];
			$Pengajuan = BorrowerPersyaratanInsert::where('brw_id', Auth::guard('borrower')->user()->brw_id)
				->where('tipe_id', $request->type_pendanaan_select)
				->where('user_type', $type)
				->where('persyaratan_id', $persyaratan_id)
				->update(['checked' => 1]);
		}



		$exjaminan = explode('^~', $request->jaminan);
		$gambar_proyek = $request->gambar_slider;
		for ($ex = 0; $ex < count($exjaminan); $ex++) {
			$datajaminan = explode('@,@', $exjaminan[$ex]);

			$pathFileJaminan = "";
			$file_jaminan = $request->file_jaminan;
			if ($request->hasFile('file_jaminan')) {
				//$jaminanPath = 'jaminan/' . $insertPengajuan[0]->{'pengajuan_id'};
				$jaminanPath = 'jaminan/' . $pengajuan_id;
				// $i = count($file_jaminan) + 1;
				foreach ($request->file_jaminan as $file) {
					$pathFileJaminan  = $this->uploadJaminan($datajaminan[0],   $file, $jaminanPath);
				}
			}



			//$insertjaminan = DB::select("select put_into_brw_jaminan('".$insertPengajuan[0]->{'pengajuan_id'}."', '".$datajaminan[0]."', '".$datajaminan[2]."', '".$datajaminan[1]."', '".$datajaminan[5]."', 
			$insertjaminan = DB::select(
				"select put_into_brw_jaminan('" . $pengajuan_id . "', '" . $datajaminan[0] . "', '" . $datajaminan[2] . "', '" . $datajaminan[1] . "', '" . $datajaminan[5] . "', 
					'" . $datajaminan[6] . "', 'BPN " . $datajaminan[3] . "','" . $datajaminan[4] . "','" . $pathFileJaminan . "' , '0', '" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "','BorrowerProsesController', '906') as responseJaminan;"
			);
		}


		//$insertLogPengajuan = DB::select("select put_into_brw_log_pengajuan('".$insertPengajuan[0]->{'pengajuan_id'}."', '".Auth::guard('borrower')->user()->brw_id."', 
		$insertLogPengajuan = DB::select(
			"select put_into_brw_log_pengajuan('" . $pengajuan_id . "', '" . Auth::guard('borrower')->user()->brw_id . "', 
            '0', 'Pendanaan Anda Akan kami Proses', '','',  'BorrowerProsesController', '912') as responseLogPengajuan;"
		);

		$dataPengajuan = DB::table('brw_pengajuan')->select('brw_user_detail.nama', 'brw_pengajuan.*', 'brw_tipe_pendanaan.pendanaan_nama as tipe_pendanaan_nama')
			->join('brw_tipe_pendanaan', 'brw_tipe_pendanaan.tipe_id', '=', 'brw_pengajuan.pendanaan_tipe')
			->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_pengajuan.brw_id')
			//->where('brw_pengajuan.pengajuan_id','=', $insertPengajuan[0]->{'pengajuan_id'})
			->where('brw_pengajuan.pengajuan_id', '=', $pengajuan_id)
			->first();


		try {
			$SendMailBorrower = new SendInfoPengajuanBorrower($dataPengajuan);
			Mail::to(Auth::guard('borrower')->user()->email)->send($SendMailBorrower);
		} catch (\Swift_TransportException $e) {
			// echo $e->getMessage();
		} catch (\Swift_RfcComplianceException $e) {
			// echo $e->getMessage();
		}

		$email_to = Admins::select('email', 'firstname', 'lastname')->where('role', '=', 10)->get();

		$i = 0;
		foreach ($email_to as $data) {
			try {
				$SendMailAnalis = new SendInfoPengajuanAnalys($dataPengajuan, $email_to[$i]);
				Mail::to($email_to[$i]->email)->send($SendMailAnalis);
			} catch (\Swift_TransportException $e) {
				// echo $e->getMessage();
			} catch (\Swift_RfcComplianceException $e) {
				// echo $e->getMessage();
			}
			$i++;
		}


		//create profile pengajuan 
		$responseProfilePengajuan = \DB::select('select update_user_profile_pengajuan(?, ?) as response', [$pengajuan_id,  'create']);

		$response = [
			'status' => 'sukses',
			'message' => 'borrower berhasil ditambahkan',
			'data_borrower' => 'null'

		];

		return response()->json($response);
	}

	public function getDanaNonRumahLain(Request $request)
	{

		$output = "";
		$dataNonRumahLain = BorrowerDanaNonRumahLain::where('pengajuan_id', $request->pengajuan_id)->get();
		$no = 1;
		foreach ($dataNonRumahLain as $key => $val) {
			$output .= '<tr id="row' . $no . '">';
			$output .= '<td> <input type="text" value="' . $val->bank . '" name="bank_non_rumah_lain[]" id="bank_non_rumah_lain_' . $no . '"
			placeholder="ketik disini" class="form-control" maxlength="30" /></td>';

			$output .= '<td><input type="text" value="' .  number_format($val->plafond, 0, '.', '.') . '" name="plafond_non_rumah_lain[]"
			id="plafond_non_rumah_lain_' . $no . '" placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';


			$output .= '<td><input type="text" value="' . $val->jangka_waktu . '" name="jangkawaktu_non_rumah_lain[]"
			id="jangkawaktu_non_rumah_lain_' . $no . '" placeholder="ketik disini" class="form-control"  /></td>';


			$output .= '<td><input type="text" value="' .  number_format($val->outstanding, 0, '.', '.') . '" name="outstanding_non_rumah_lain[]"
			id="outstanding_non_rumah_lain_' . $no . '" placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';

			$output .= '<td><input type="text"  value="' .  number_format($val->angsuran, 0, '.', '.') . '" name="angsuran_non_rumah_lain[]"
			id="angsuran_non_rumah_lain_' . $no . '" placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';

			if ($no == 1) {
				$output .= ' <td><button type="button" class="btn btn-primary" id="add_dana_non_rumahlain"><i
				class="fa fa-plus"></i></button></td>';
			} else {
				$output .= '<td> <button type="button" name="remove" id="' . $no . '" class="btn btn-danger btn_remove_non_rumah_lain"><i class="fa fa-trash"></i></button></td>';
			}

			$output .= '</tr>';

			$no++;
		}

		return $output;
	}

	public function getDanaRumahLain(Request $request)
	{

		$output = "";
		$dataRumahLain = BorrowerDanaRumahLain::where('pengajuan_id', $request->pengajuan_id)->get();
		$no = 1;
		foreach ($dataRumahLain as $key => $val) {
			$disabled = ($val->status == "2") ? "readonly" : "";

			$output .= '<tr><td>' . $no . '</td>';
			$output .= '<td width="200"> <select id="status_rumah_lain_' . $no . '" name="status_rumah_lain[]" class="form-control custom-select">';
			$output .= '<option value="1" ' .  (($val->status == "1") ? 'selected' : '') . '>Sedang Berjalan</option>';
			$output .= '<option value="2" ' . (($val->status == "2") ? 'selected' : '') . '>Lunas</option></select></td>';
			$output .= '<td><input type="text" id="bank_rumah_lain_' . $no . '" value="' . $val->bank . '" name="bank_rumah_lain[]" class="form-control" maxlength="30" ' . $disabled . ' /></td>';
			$output .= '<td><input type="text" id="plafond_rumah_lain_' . $no . '" value="' . number_format($val->plafond, 0, '.', '.') . '" name="plafond_rumah_lain[]"  class="form-control"  onkeyup="this.value = formatRupiah(this.value);" ' . $disabled . '/></td>';
			$output .= '<td><input type="text" id="jangka_waktu_rumah_lain_' . $no . '" value="' . $val->jangka_waktu . '" name="jangka_waktu_rumah_lain[]"  class="form-control" ' . $disabled . '/></td>';
			$output .= '<td><input type="text" id="outstanding_rumah_lain_' . $no . '"  value="' . number_format($val->outstanding, 0, '.', '.') . '" name="outstanding_rumah_lain[]"  class="form-control" onkeyup="this.value = formatRupiah(this.value);" ' . $disabled . ' /></td>';
			$output .= '<td><input type="text" id="angsuran_rumah_lain_' . $no . '" value="' . number_format($val->angsuran, 0, '.', '.') . '" name="angsuran_rumah_lain[]"  class="form-control"  onkeyup="this.value = formatRupiah(this.value);" ' . $disabled . ' /></td>';
			$output .= '</tr>';

			$no++;
		}

		return $output;
	}


	public function pendanaanNonRumahLain($request): void
	{

		BorrowerDanaNonRumahLain::where('pengajuan_id', $request->id_pengajuan)->delete();
		if (isset($request->bank_non_rumah_lain) && count($request->bank_non_rumah_lain) > 0 && $request->status_bank == "1") {
			for ($count = 0; $count < count($request->bank_non_rumah_lain); $count++) {
				$plafond = str_replace(".", "", $request->plafond_non_rumah_lain[$count]);
				$outstanding = str_replace(".", "", $request->outstanding_non_rumah_lain[$count]);
				$angsuran = str_replace(".", "", $request->angsuran_non_rumah_lain[$count]);
				$data_non_rumah_lain = [
					'pengajuan_id' => $request->id_pengajuan,
					'bank'  => $request->bank_non_rumah_lain[$count],
					'plafond' => $plafond,
					'jangka_waktu' => $request->jangkawaktu_non_rumah_lain[$count],
					'outstanding' => $outstanding,
					'angsuran' => $angsuran
				];
				BorrowerDanaNonRumahLain::create($data_non_rumah_lain);
			}

			//Log Aktifitas Dana Rumah Lain
			Helper::logAktifitasPengajuan($request->id_pengajuan, $data_non_rumah_lain, 'brw_pendanaan_non_rumah_lain');	
	
		}
	}


	public function pendanaanRumahLain($request): void
	{

		BorrowerDanaRumahLain::where('pengajuan_id', $request->id_pengajuan)->delete();

		if (isset($request->status_rumah_lain) && count($request->status_rumah_lain) > 0) {

			//Pendanaan Rumah lain
			for ($count = 0; $count < count($request->status_rumah_lain); $count++) {
				// $rumah_ke =  ($request->rumah_ke == 2) ? $request->rumah_ke - 1 : $request->rumah_ke - $count;
				$rumah_ke =  $count+1;
				$plafond = str_replace(".", "", $request->plafond_rumah_lain[$count]);
				$outstanding = str_replace(".", "", $request->outstanding_rumah_lain[$count]);
				$angsuran = str_replace(".", "", $request->angsuran_rumah_lain[$count]);

				$data_rumah_lain = [
					'pengajuan_id' => $request->id_pengajuan,
					'rumah_ke' => $rumah_ke,
					'status' => $request->status_rumah_lain[$count],
					'bank'  => $request->bank_rumah_lain[$count],
					'plafond' => $plafond,
					'jangka_waktu' => $request->jangka_waktu_rumah_lain[$count],
					'outstanding' => $outstanding,
					'angsuran' => $angsuran
				];
				BorrowerDanaRumahLain::create($data_rumah_lain);
			}
			//Log Aktifitas Dana Rumah Lain
			Helper::logAktifitasPengajuan($request->id_pengajuan, $data_rumah_lain, 'brw_pendanaan_rumah_lain');	
	
		}
	}

	public function pengajuanPendanaanRumah(Request $request)
	{

		try {

			$dataExists = \DB::table('brw_agunan')->where('id_pengajuan', $request->id_pengajuan)->first();

			$data_agunan = [
				'id_pengajuan' => $request->id_pengajuan,
				'jenis_agunan' => $request->jenis_agunan,
				'nomor_agunan' => $request->nomor_agunan,
				'atas_nama_agunan' => $request->atas_nama_agunan,
				'luas_bangunan' => $request->luas_bangunan,
				'luas_tanah' => $request->luas_tanah,
				'nomor_surat_ukur' => $request->nomor_surat_ukur,
				'tanggal_surat_ukur' => (isset($request->tanggal_surat_ukur) && !empty($request->tanggal_surat_ukur))  ? date('Y-m-d', strtotime($request->tanggal_surat_ukur)) : '',
				'tanggal_terbit' => (isset($request->tanggal_terbit) && !empty($request->tanggal_terbit))  ? date('Y-m-d', strtotime($request->tanggal_terbit)) : '',
				'tanggal_jatuh_tempo' => (isset($request->tanggal_jatuh_tempo) && !empty($request->tanggal_jatuh_tempo))  ? date('Y-m-d', strtotime($request->tanggal_jatuh_tempo)) : '',
				'kantor_penerbit' => $request->kantor_penerbit,
				'blok_nomor' => $request->blok_nomor,
				'RT' => $request->RT,
				'RW' => $request->RW,
				'no_imb' => $request->no_imb,
				'tanggal_terbit_imb' => $request->tanggal_terbit_imb

			];

			if (!$dataExists) {
				$data_agunan['created_at'] = date('Y-m-d H:i:s');
				\DB::table('brw_agunan')->insert($data_agunan);
			} else {
				$data_agunan['updated_at'] = date('Y-m-d H:i:s');
				\DB::table('brw_agunan')->where('id_pengajuan', $request->id_pengajuan)->update($data_agunan);
			}

			//Log Aktifitas Agunan
			Helper::logAktifitasPengajuan($request->id_pengajuan, $data_agunan, 'brw_agunan', $request->created_by);	


			//update data brw_pengajuan provinsi, kota, kec, kel, kode pos 
			$data_pengajuan = [
				'lokasi_proyek' => $request->in_alamat_obj_pendanaan,
				'provinsi' => $request->in_provinsi_obj_pendanaan,
				'kota' => $request->in_kota_obj_pendanaan,
				'kecamatan' => $request->in_kecamatan_obj_pendanaan,
				'kelurahan' => $request->in_kelurahan_obj_pendanaan,
				'kode_pos' => $request->in_kode_pos_obj_pendanaan,
				'detail_pendanaan' => $request->in_dtl_obj_pendanaan,
				'nm_pemilik' => $request->in_nama_pemilik_obj,
				'no_tlp_pemilik' => '62' . $request->in_no_hp_pemilik_obj,
				'alamat_pemilik' => $request->in_alamat_pemilik_obj,
				'provinsi_pemilik' => $request->in_provinsi_pemilik_obj,
				'kota_pemilik' => $request->in_kota_pemilik_obj,
				'kecamatan_pemilik' => $request->in_kecamatan_pemilik_obj,
				'kelurahan_pemilik' => $request->in_kelurahan_pemilik_obj,
				'kd_pos_pemilik' => $request->in_kode_pos_pemilik_obj,
				'setuju_verifikator_pengajuan' => 1,
				'setuju_verifikator_dokumen' => 1,
				'status' => 11
			];

			\DB::table('brw_pengajuan')
				->where('pengajuan_id', $request->id_pengajuan)
				->update($data_pengajuan);

			//Log Aktifitas Pengajuan
			Helper::logAktifitasPengajuan($request->id_pengajuan, $data_pengajuan, 'brw_pengajuan');	

			//Pendanaan Rumah Lain
			$this->pendanaanRumahLain($request);

			//Pendanaan Non Rumah Lain 
			$this->pendanaanNonRumahLain($request);

			//return result object pendanaan 
			$in_obj_pendanaan = ['in_alamat_obj_pendanaan'=> $request->in_alamat_obj_pendanaan, 
			                    'in_provinsi_obj_pendanaan'=> $request->in_provinsi_obj_pendanaan,
					            'in_kota_obj_pendanaan' => $request->in_kota_obj_pendanaan,
							    'in_kecamatan_obj_pendanaan'=> $request->in_kecamatan_obj_pendanaan,
								'in_kelurahan_obj_pendanaan'=> $request->in_kelurahan_obj_pendanaan,
								'in_kode_pos_obj_pendanaan'=> $request->in_kode_pos_obj_pendanaan,
								'in_dtl_obj_pendanaan'=> $request->in_dtl_obj_pendanaan,
								'in_nama_pemilik_obj'=> $request->in_nama_pemilik_obj,
								'in_no_hp_pemilik_obj'=> '62' . $request->in_no_hp_pemilik_obj,
								'in_alamat_pemilik_obj' => $request->in_alamat_pemilik_obj,
								'in_provinsi_pemilik_obj' => $request->in_provinsi_pemilik_obj,
								'in_kota_pemilik_obj' => $request->in_kota_pemilik_obj,
								'in_kecamatan_pemilik_obj' => $request->in_kecamatan_pemilik_obj,
								'in_kelurahan_pemilik_obj' => $request->in_kelurahan_pemilik_obj,
								'in_kode_pos_pemilik_obj' => $request->in_kode_pos_pemilik_obj,
							];


			//update user profile pengajuan 
			$updateUserProfilePengajuan = \DB::select('select update_user_profile_pengajuan (?, ?) as response ', [$request->id_pengajuan, 'update']);

	
			if (Auth::guard('admin')->check()) {
				//verifikator pengajuan
				VerifikatorService::updateVerifikatorPengajuan($request->id_pengajuan);
			}

			return response()->json(['status' => 'success', 'message' => 'Berhasil di simpan', 'in_obj_pendanaan'=> $in_obj_pendanaan]);
		} catch (\Exception $e) {
			return response()->json(['status' => 'failed', 'message' => $e->getMessage()]);
		}
	}

	public function proses_pendanaan_kpr(Request $request)
	{

		$check_input_validation = DB::select(
			"SELECT val_ajukan_kpr(
			'" . $request->type_tujuan_pendanaan . "',
            '1',
            '" . str_replace(".", "", $request->txt_harga_objek_pendanaan) . "',
            '2',
            '" . str_replace(".", "", $request->txt_uang_muka) . "',
            '3',
            '" . str_replace(".", "", $request->txt_nilai_pengajuan) . "',
			'BorrowerProsesControllre.php',
			'999'
            ) as response;"
		);

		$data = $check_input_validation[0]->response;

		if ($data != "1") {
			return [
				'status' => 'gagal',
				'message' => $data,
				'data_borrower' => 'null'
			];
		}

		$pengajuan = DB::table('brw_pengajuan')->insert(array(

			"id_proyek" => "",
			"brw_id" => Auth::guard('borrower')->user()->brw_id,
			"pendanaan_nama" => "",
			"pendanaan_tipe" => $request->type_pendanaan_select,
			"pendanaan_tujuan" => $request->type_tujuan_pendanaan,
			"pendanaan_akad" => "",
			"pendanaan_dana_dibutuhkan" => str_replace(".", "", $request->txt_nilai_pengajuan),
			"pendanaan_dana_disetujui" => "0",
			"harga_objek_pendanaan" => str_replace(".", "", $request->txt_harga_objek_pendanaan),
			"uang_muka" => str_replace(".", "", $request->txt_uang_muka),
			"estimasi_mulai" => "",
			"estimasi_imbal_hasil" => "",
			"mode_pembayaran" => "0",
			"metode_pembayaran" => "0",
			"durasi_proyek" => $request->txt_jangka_waktu,
			"detail_pendanaan" => $request->txt_detail_pendanaan,
			"dana_dicairkan" => "0",
			"status" => "0",
			"status_dana" => "0",
			"lokasi_proyek" => $request->txt_alamat,
			"geocode" => "",
			"provinsi" => $request->txt_provinsi,
			"kota" => $request->txt_kota,
			"kecamatan" => $request->txt_kecamatan,
			"kelurahan" => $request->txt_kelurahan,
			"kode_pos" => $request->txt_kd_pos,
			"va_number" => "",
			"gambar_utama" => "gambar",
			"nm_pemilik" => $request->txt_nm_pemilik,
			"no_tlp_pemilik" => "62" . $request->txt_hp_pemilik,
			"alamat_pemilik" => $request->txt_alamat_pemilik,
			"provinsi_pemilik" => $request->txt_provinsi_pemilik,
			"kota_pemilik" => $request->txt_kota_pemilik,
			"kecamatan_pemilik" => $request->txt_kecamatan_pemilik,
			"kelurahan_pemilik" => $request->txt_kelurahan_pemilik,
			"kd_pos_pemilik" => $request->txt_kd_pos_pemilik,
			"status_tampil_pengajuan" => "tampil",
			"keterangan" => "",
			"keterangan_approval" => "",
			"created_at" => date('Y-m-d H:i:s'),
			"updated_at" => date('Y-m-d H:i:s'),
		));

		$response = '';

		if (!$pengajuan) {

			$log_db_app = DB::table('log_db_app')->insert(array(
				"filename" => "BorrowerProsesController",
				"line" => "1381",
				"description" => "Proses Ajukan KPR BRW ID" . Auth::guard('borrower')->user()->brw_id . " Gagal",
				"created_at" => date('Y-m-d H:i:s')
			));

			$response = [
				'status' => 'gagal',
				'message' => 'pengajuan pendanaan kpr gagal',
				'data_borrower' => 'null'
			];
		} else {


			$pengajuan_id   = DB::getPDO()->lastInsertId();
			$insertLogPengajuan = DB::select(
				"select put_into_brw_log_pengajuan('" . $pengajuan_id . "', '" . Auth::guard('borrower')->user()->brw_id . "', 
				'0', 'Pendanaan KPR Anda Akan kami Proses', '','',  'BorrowerProsesController', '1390') as responseLogPengajuan;"
			);

			$response = [
				'status' => 'sukses',
				'message' => 'pengajuan pendanaan kpr berhasil',
				'data_borrower' => 'null',
				'pengajuan_id' =>  Auth::guard('borrower')->user()->brw_id . '_' . $pengajuan_id,
			];
		}

		return $response;
		// return redirect('/borrower/ajukanPendanaanDanaRumah/')->with('status', 'proses_berhasil');		
	}
	private function upload($column, $request, $store_path)
	{
		if ($request->file('file')) {
			$file = $request->file('file');
			$filename = 'pic_brw' . '.' . $file->getClientOriginalExtension();
			//var_dump($filename);die();
			$resize = Image::make($file)->resize(480, 640, function ($constraint) {
				$constraint->aspectRatio();
			})->save();

			// $resize = Image::make($file)->resize(480,640, function ($constraint) {
			// $constraint->aspectRatio();
			// })->save($filename);

			// save nama file berdasarkan tanggal upload+nama file
			$store_path = 'borrower/' . $brw_id;
			$path = $file->storeAs($store_path, $filename, 'public');
		}
	}

	private function uploadJaminan($column, $request, $store_path)
	{
		$file = $request;
		$filename = $column . '.' . $file->getClientOriginalExtension();

		$path = $file->storeAs($store_path, $filename, 'public');
		//save gambar yang di upload di public storage
		return $path;
	}

	private function uploadPotoProyek($column, $request, $store_path)
	{
		$file = $request;
		$filename = Carbon::now()->toDateString() . $column . '.' . $file->getClientOriginalExtension();

		$path = $file->storeAs($store_path, $filename, 'public');
		//save gambar yang di upload di public storage
		return $path;
	}

	public function licenceFileShow($file)
	{
		if (strpos($file, '.') !== false) { // check if the file exists

			if (Auth::check() || Auth::guard('borrower')->check()) { // check who is logged in, whether user or borrower

				// get user_id or borrower_id
				$user_id = Auth::user() ? Auth::user()->id : Auth::guard('borrower')->user()->brw_id;

				// get id from path
				$get_id = substr($file, 1);
				$path_id = explode(':', $get_id);

				if ($user_id == $path_id[1]) { // check if the id is the same
					return $this->returnPhoto(str_replace(':', '/', $file));
				} else {
					return redirect('/');
				}
			} else {

				return redirect('/');
			}
		} else {
			//Invalid file name given or the file doesn't exist
			return redirect('/');
		}
	}

	public function licenceFileShowForAdmin($file)
	{
		if (strpos($file, '.') !== false) { // check if the file exists

			if (Auth::guard('admin')->check()) { // check who is logged in, whether user or borrower

				return $this->returnPhoto(str_replace(':', '/', $file));
			} else {

				return redirect('/');
			}
		} else {
			//Invalid file name given or the file doesn't exist
			return redirect('/');
		}
	}

	public function returnPhoto($file)
	{
		$path = storage_path('app/private/' . $file);

		try {
			$file = File::get($path);
			$type = File::mimeType($path);
			$response = Response::make($file, 200);
			$response->header("Content-Type", $type);
			return $response;
		} catch (\Exception $exception) {

			DB::table('log_db_app')->insert([
				'file_name' => 'BorrowerProsesController.php',
				'line' => 1512,
				'description' => "Gagal Retrieve Foto $file , karena path tidak valid",
				"created_at" => date("Y-m-d H:i:s")
			]);

			return false;
		}
	}

	public function upload_foto(Request $request)
	{

		$brw_id = Auth::guard('borrower')->user()->brw_id;

		if ($request->img) {
			$image = explode(";base64,", $request->img);
			$image_decode = base64_decode($image[1]);
			$path_foto = 'borrower/' . $brw_id . '/' . $request->file_name . '.png';

			// ! PERUBAHAN UPLOAD FOTO PATH KE PRIVATE 20210517
			$path = Storage::disk('private')->put($path_foto, $image_decode);

			if (Storage::disk('private')->exists($path_foto)) {
				return response()->json([
					'success' => 'Berhasil di upload',
					'url' => $path_foto,
				]);
			} else {
				$insert_log = DB::table('log_db_app')->insert([
					'file_name' => 'BorrowerProssesController.php',
					'line' => 1808,
					'description' => "Unggah file $path_foto gagal",
					'created_at' => date("Y-m-d H:i:s")
				]);
				return response()->json([
					'failed' => "File gagal di upload"
				]);
			}
		} else {
			return response()->json([
				'failed' => "File Kosong"
			]);
		}
	}

	public function getFilePath($tableName, $fieldName, $pengajuan_id) {

		$data = DB::table('brw_pengajuan')->where('pengajuan_id', $pengajuan_id)
		->select(['brw_id', 'pengajuan_id'])->get();

		if (Auth::guard('admin')->check()) { 
			if (Schema::hasColumn($tableName, 'brw_id')){
				$doc_path = DB::table($tableName)->where('brw_id', $data[0]->brw_id)
				->pluck($fieldName);

			} 
			else if (Schema::hasColumn($tableName, 'id_pengajuan')) {
				$doc_path = DB::table($tableName)->where('id_pengajuan', $data[0]->pengajuan_id)
				->pluck($fieldName);
			}
		}

		return $doc_path[0];
	}

	
}
