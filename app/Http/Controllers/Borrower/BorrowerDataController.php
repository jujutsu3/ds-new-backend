<?php

namespace App\Http\Controllers\Borrower;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Investor;
use App\Proyek;
use App\MasterProvinsi;
use DB;
use Illuminate\Support\Facades\Auth;

// estension custom
use Illuminate\Support\Facades\Config;
//use App\Http\Middleware\UserCheck;

class BorrowerDataController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth:api', ['except'=>['DataProvinsi', 'DataKota', 'DataPekerjaan', 'DataBidangPekerjaan', 'DataPengalamanPekerjaan', 'DataPendapatan','DataPendidikan', 'CheckNIK', 'CheckNIKBH', 'TipePendanaan','TipePendanaanDanaRumah', 'PersyaratanPendanaan', 'JenisKelamin', 'KepemilikanRumah', 'Agama', 'StatusPerkawinan', 'BidangPekerjaanOnline', 'DataBank', 'CheckNoTlp','PersyaratanPendanaanProsesPengajuan','JenisJaminan']]);
    }

    public function DataPendidikan()
    {


        $getPendidikan = DB::table('m_pendidikan')->select('id_pendidikan', 'pendidikan')->get();
        //$borrower = Borrower::where('username', $request->username)->first();

        echo "[";
        $i = 0;
        foreach ($getPendidikan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_pendidikan\", \"text\":\"$data->pendidikan\"}";
            $i++;
        }

        echo "]";
    }

    public function CheckNIK($nik)
    {
        $response = '';
        $brw_id = Auth::guard('borrower')->user()->brw_id;
        $checkNIK = DB::table('brw_user_detail')
            //->whereIn('brw_type',[1,3])
            ->where('brw_id', '!=', $brw_id)
            ->where('ktp', '=', $nik)
            ->first();

        if (!$checkNIK) {

            $response = [
                'status' => 'kosong',
                'message' => 'KTP Tidak Ditemukan'
            ];
        } else {
            $response = [
                'status' => 'ada',
                'message' => 'KTP Ditemukan'
            ];
        }

        return response()->json($response);
    }

    public function CheckNPWP($npwp, $type)
    {
        $brw_id =  Auth::guard('borrower')->check() ? Auth::guard('borrower')->user()->brw_id : '';
        $npwp_column = "";
        $response = "";
        if ($type == 1) {
            $npwp_column = "npwp";
        } else {
            $npwp_column = "npwp_perusahaan";
        }
        $checkNPWP = DB::table('brw_user_detail')
            //->whereIn('brw_type',[1,3])
            ->where('brw_id', '!=', $brw_id)
            ->where($npwp_column, '=', $npwp)
            ->first();

        if (!$checkNPWP) {
            $response = [
                'status' => 'kosong',
                'message' => 'NPWP Tidak Ditemukan'
            ];
        } else {
            $response = [
                'status' => 'ada',
                'message' => 'NPWP Ditemukan'
            ];
        }

        return response()->json($response);
    }
    public function CheckNoTlp($noTLP)
    {
        $response = "";
        $brw_id = Auth::guard('borrower')->user()->brw_id;
        $CheckNOTLP = DB::table('brw_user_detail')
            //->whereIn('brw_type',[1,3])
            ->where('brw_id', '!=', $brw_id)
            ->where('no_tlp', '=', "62$noTLP")
            ->orWhere('no_tlp', '=', "0$noTLP")
            //->where('no_tlp', '=', $noTLP)
            ->first();

        if (!$CheckNOTLP) {
            $response = [
                'status' => 'kosong',
                'message' => 'No TLP Tidak Ditemukan'
            ];
        } else {
            $response = [
                'status' => 'ada',
                'message' => 'No TLP Ditemukan'
            ];
        }

        return response()->json($response);
    }

    public function kodeTelepon()
    {
        $getKode = DB::table('m_kode_telpon_regional')->select('id_telpon', 'kode_operator')->get();
        echo "[";
        $i = 0;
        foreach ($getKode as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_telpon\", \"text\":\"0$data->kode_operator\"}";
            $i++;
        }

        echo "]";
    }

    public function DataProvinsi(Request $request)
    {

        if ($request->flag_jabodetabek == "1") {
            $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')
                ->whereIn('Provinsi', ['Banten', 'DKI Jakarta', 'Jawa Barat'])
                ->groupBy('provinsi')
                ->get();
        } else {
            $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')
                ->groupBy('provinsi')
                ->get();
        }

        echo "[";
        $i = 0;
        foreach ($getProv as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->Provinsi\"}";
            $i++;
        }

        echo "]";
    }

    public function Datajabatan()
    {

        $getJabatan = DB::table('m_jabatan')->select('id', 'jabatan')->get();
        echo "[";
        $i = 0;
        foreach ($getJabatan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id\", \"text\":\"$data->jabatan\"}";
            $i++;
        }

        echo "]";
    }

    public function DataKota($provinsi)
    {
        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')
            ->where('Provinsi', $provinsi)
            ->groupBy('Kota')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKota as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->Kota\"}";
            $i++;
        }

        echo "]";
    }

    public function DataKotaList()
    {
        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')
            ->groupBy('Kota')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKota as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->Kota\"}";
            $i++;
        }

        echo "]";
    }

    public function DataKecamatan($kota)
    {

        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'Kecamatan')
            ->where('Kota', $kota)
            ->groupBy('Kecamatan')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKecamatan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->Kecamatan\"}";
            $i++;
        }

        echo "]";
    }

    public function DataKelurahan($kecamatan)
    {


        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'Kelurahan')
            ->where('Kecamatan', $kecamatan)
            ->groupBy('Kelurahan')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKelurahan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->Kelurahan\"}";
            $i++;
        }

        echo "]";
    }

    public function DataKodePos($kelurahan)
    {


        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')
            ->where('Kelurahan', $kelurahan)
            ->groupBy('kode_pos')
            ->first();
        echo "[{\"id\"  : \"$getKodePos->id_kode_pos\", \"text\":\"$getKodePos->kode_pos\"}]";
    }

    public function DataKodePosv2($kelurahan, $kecamatan, $kota, $prov)
    {


        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')
            ->where('kelurahan', $kelurahan)
            ->where('kecamatan', $kecamatan)
            ->where('Kota', $kota)
            ->where('Provinsi', $prov)
            ->groupBy('kode_pos')
            ->first();
        echo "[{\"id\"  : \"$getKodePos->id_kode_pos\", \"text\":\"$getKodePos->kode_pos\"}]";
    }


    public function DataPekerjaan()
    {

        $getPekerjaan = DB::table('m_pekerjaan')->select('id_pekerjaan', 'pekerjaan')->get();


        $i = 0;
        echo "[";
        foreach ($getPekerjaan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_pekerjaan\", \"text\":\"$data->pekerjaan\"}";
            $i++;
        }

        echo "]";
    }

    public function DataBidangPekerjaan()
    {


        $getBidangPekerjaan = DB::table('m_bidang_pekerjaan')->select('id_bidang_pekerjaan', 'bidang_pekerjaan')->get();

        $i = 0;
        foreach ($getBidangPekerjaan as $data) {
            $listBidangPekerjaan[$i] = [
                'id' => $data->id_bidang_pekerjaan,
                'text' => $data->bidang_pekerjaan,
            ];
            $i++;
        }


        return json_encode($listBidangPekerjaan);
    }

    public function DataPengalamanPekerjaan()
    {


        $getpengalamanPekerjaan = DB::table('m_pengalaman_kerja')->select('id_pengalaman_kerja', 'pengalaman_kerja')->get();


        $i = 0;
        echo "[";
        foreach ($getpengalamanPekerjaan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_pengalaman_kerja\", \"text\":\"$data->pengalaman_kerja\"}";
            $i++;
        }

        echo "]";
    }
    public function DataBank()
    {


        $getBank = DB::table('m_bank')->select('kode_bank', 'nama_bank')->get();


        $i = 0;
        echo "[";
        foreach ($getBank as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->kode_bank\", \"text\":\"$data->nama_bank\"}";
            $i++;
        }

        echo "]";
    }
    public function DataPendapatan()
    {

        $getPendapatan = DB::table('m_pendapatan')->select('id_pendapatan', 'pendapatan')->get();

        $i = 0;
        echo "[";
        foreach ($getPendapatan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_pendapatan\", \"text\":\"$data->pendapatan\"}";
            $i++;
        }

        echo "]";
    }

    public function JenisKelamin()
    {

        $getJenisKelamin = DB::table('m_jenis_kelamin')->select('id_jenis_kelamin', 'jenis_kelamin')->get();

        $i = 0;
        echo "[";
        foreach ($getJenisKelamin as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_jenis_kelamin\", \"text\":\"$data->jenis_kelamin\"}";
            $i++;
        }

        echo "]";
    }

    public function Agama()
    {

        $getTipeAgama = DB::table('m_agama')->select('id_agama', 'agama')->get();

        $i = 0;
        echo "[";
        foreach ($getTipeAgama as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_agama\", \"text\":\"$data->agama\"}";
            $i++;
        }

        echo "]";
    }

    public function StatusPerkawinan()
    {

        $getStatusKawin = DB::table('m_kawin')->select('id_kawin', 'jenis_kawin')->get();

        $i = 0;
        echo "[";
        foreach ($getStatusKawin as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kawin\", \"text\":\"$data->jenis_kawin\"}";
            $i++;
        }

        echo "]";
    }
    public function KepemilikanRumah()
    {

        $getTipeRumah = DB::table('m_kepemilikan_rumah')->select('id_kepemilikan_rumah', 'kepemilikan_rumah')->get();


        $i = 0;
        echo "[";
        foreach ($getTipeRumah as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kepemilikan_rumah\", \"text\":\"$data->kepemilikan_rumah\"}";
            $i++;
        }

        echo "]";
    }

    public function BidangPekerjaanOnline()
    {

        $getTipeBidangOnline = DB::table('m_online')->select('id_online', 'tipe_online')->get();


        $i = 0;
        echo "[";
        foreach ($getTipeBidangOnline as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_online\", \"text\":\"$data->tipe_online\"}";
            $i++;
        }

        echo "]";
    }

    public function TujuanPendanaan()
    {

        $getTujuanPendanaan = DB::table('m_tujuan_pembiayaan as a')
            ->select('a.id', 'a.tujuan_pembiayaan')->get();

        $i = 0;
        echo "[";
        foreach ($getTujuanPendanaan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id\", \"text\":\"$data->tujuan_pembiayaan\"}";
            $i++;
        }

        echo "]";
    }

    public function TujuanPendanaanKpr(Request $request)
    {
        $tipe_pendanaan = $request->tipe_pendanaan ? $request->tipe_pendanaan : 2;
        $getTujuanPendanaan = DB::table('m_tujuan_pembiayaan as a')
            ->where('tipe_id', '=', $tipe_pendanaan)
            ->select('a.id', 'a.tujuan_pembiayaan')->get();

        $i = 0;
        echo "[";
        foreach ($getTujuanPendanaan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id\", \"text\":\"$data->tujuan_pembiayaan\"}";
            $i++;
        }

        echo "]";
    }

    public function TipePendanaan($user_type)
    {

        $getTipePendanaan = DB::table('brw_persyaratan_pendanaan')
            ->join('brw_tipe_pendanaan', 'brw_persyaratan_pendanaan.tipe_id', '=', 'brw_tipe_pendanaan.tipe_id')
            ->where('brw_persyaratan_pendanaan.user_type', $user_type)
            ->whereIn('brw_tipe_pendanaan.tipe_id', ['1'])
            ->groupBy('brw_tipe_pendanaan.pendanaan_nama')
            ->select('brw_tipe_pendanaan.tipe_id', 'brw_tipe_pendanaan.pendanaan_nama')->get();
        // $getTipePendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->get();


        $i = 0;
        echo "[";
        foreach ($getTipePendanaan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->tipe_id\", \"text\":\"$data->pendanaan_nama\"}";
            $i++;
        }

        echo "]";
    }

    public function TipePendanaanDanaRumah($user_type)
    {

        $getTipePendanaan = DB::table('brw_persyaratan_pendanaan')
            ->join('brw_tipe_pendanaan', 'brw_persyaratan_pendanaan.tipe_id', '=', 'brw_tipe_pendanaan.tipe_id')
            ->where('brw_persyaratan_pendanaan.user_type', $user_type)
            ->whereIn('brw_tipe_pendanaan.tipe_id', ['1', '2'])
            ->groupBy('brw_tipe_pendanaan.pendanaan_nama')
            ->select('brw_tipe_pendanaan.tipe_id', 'brw_tipe_pendanaan.pendanaan_nama')->get();
        // $getTipePendanaan = DB::table('brw_tipe_pendanaan')->select('tipe_id', 'pendanaan_nama')->get();

        $i = 0;
        echo "[";
        foreach ($getTipePendanaan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->tipe_id\", \"text\":\"$data->pendanaan_nama\"}";
            $i++;
        }

        echo "]";
    }

    public function PersyaratanPendanaan($tipe_borrower, $tipe_pendanaan)
    {

        $getPersyaratan = DB::table('brw_persyaratan_pendanaan')->select('persyaratan_id', 'tipe_id', 'user_type', 'persyaratan_nama', 'persyaratan_mandatory')
            ->where('tipe_id', '=', $tipe_pendanaan)
            ->where('user_type', '=', $tipe_borrower)
            ->get();
        echo "[";
        $i = 0;
        foreach ($getPersyaratan as $data) {

            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"persyaratan_id\"  : \"$data->persyaratan_id\", \"tipe_id\"  : \"$data->tipe_id\", \"user_type\":\"$data->user_type\", \"persyaratan_nama\":\"$data->persyaratan_nama\", \"persyaratan_mandatory\":\"$data->persyaratan_mandatory\"}";
            $i++;
        }
        echo "]";
    }

    public function JenisJaminan()
    {
        $getJenisJaminan = DB::table('m_jenis_jaminan')->select('id_jenis_jaminan', 'jenis_jaminan')->get();

        $i = 0;
        echo "[";
        foreach ($getJenisJaminan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_jenis_jaminan\", \"text\":\"$data->jenis_jaminan\"}";
            $i++;
        }

        echo "]";
    }

    public function CheckNIKBH($nik)
    {
        $checkNIK = DB::table('brw_user_detail')
            //->whereIn('brw_type',[1,3])
            ->where('ktp', '=', $nik)
            ->first();

        if (!$checkNIK) {
            $response = [
                'status' => 'kosong',
                'message' => 'KTP Tidak Ditemukan'
            ];
        } else {
            $response = [
                'status' => 'ada',
                'message' => 'KTP Ditemukan'
            ];
        }

        return response()->json($response);
    }

    public function PersyaratanPendanaanProsesPengajuan($brw_id, $tipe_borrower, $tipe_pendanaan)
    {
        $getPersyaratan = DB::table('brw_persyaratan_pendanaan')->select('persyaratan_id', 'tipe_id', 'user_type', 'persyaratan_nama', 'persyaratan_mandatory')
            ->where('tipe_id', '=', $tipe_pendanaan)
            ->where('user_type', '=', $tipe_borrower)
            ->get();
        echo "[";
        $i = 0;
        foreach ($getPersyaratan as $data) {

            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"persyaratan_id\"  : \"$data->persyaratan_id\", \"tipe_id\"  : \"$data->tipe_id\", \"user_type\":\"$data->user_type\", \"persyaratan_nama\":\"$data->persyaratan_nama\", \"persyaratan_mandatory\":\"$data->persyaratan_mandatory\"}";
            $i++;
        }
        echo "]";
    }

    public function DataAsset()
    {

        $getJenisJaminan = DB::table('m_asset')->select('id_asset', 'asset')->get();

        $i = 0;
        echo "[";
        foreach ($getJenisJaminan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_asset\", \"text\":\"$data->asset\"}";
            $i++;
        }

        echo "]";
    }
    public function DataOmset()
    {

        $getJenisJaminan = DB::table('m_asset')->select('id_asset', 'asset')->get();

        $i = 0;
        echo "[";
        foreach ($getJenisJaminan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_asset\", \"text\":\"$data->asset\"}";
            $i++;
        }

        echo "]";
    }

    public function DataHubungan()
    {

        $getJenisJaminan = DB::table('m_hub_ahli_waris')->select('id_hub_ahli_waris', 'jenis_hubungan')->get();

        $i = 0;
        echo "[";
        foreach ($getJenisJaminan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_hub_ahli_waris\", \"text\":\"$data->jenis_hubungan\"}";
            $i++;
        }

        echo "]";
    }

    public function DataKotaPengurus($id_kode_pos)
    {
        $getProvinsi = DB::table('m_kode_pos')->where('id_kode_pos', $id_kode_pos)->first();
        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')
            ->where('Provinsi', $getProvinsi->Provinsi)
            ->groupBy('Kota')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKota as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->Kota\"}";
            $i++;
        }

        echo "]";
    }
    public function DataKecamatanPengurus($id_kode_pos)
    {

        $getKota = DB::table('m_kode_pos')->where('id_kode_pos', $id_kode_pos)->first();
        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'kecamatan')
            ->where('Kota', $getKota->Kota)
            ->groupBy('kecamatan')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKecamatan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->kecamatan\"}";
            $i++;
        }

        echo "]";
    }
    public function DataKelurahanPengurus($id_kode_pos)
    {

        $getKecamatan = DB::table('m_kode_pos')->where('id_kode_pos', $id_kode_pos)->first();
        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'kelurahan')
            ->where('kecamatan', $getKecamatan->kecamatan)
            ->groupBy('kelurahan')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKelurahan as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->kelurahan\"}";
            $i++;
        }

        echo "]";
    }

    public function DataKodePosPengurus($id_kode_pos)
    {

        $getKelurahan = DB::table('m_kode_pos')->where('id_kode_pos', $id_kode_pos)->select('id_kode_pos', 'kelurahan')->first();
        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')
            ->where('kelurahan', $getKelurahan->kelurahan)
            ->groupBy('kode_pos')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getKodePos as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id_kode_pos\", \"text\":\"$data->kode_pos\"}";
            $i++;
        }

        echo "]";
    }

    public function BentukBadanUsaha()
    {
        $getBentukBadanUsaha = DB::table('m_bentuk_badan_usaha')->select('id', 'bentuk_badan_usaha')
            ->groupBy('bentuk_badan_usaha')
            ->get();
        echo "[";
        $i = 0;
        foreach ($getBentukBadanUsaha as $data) {
            if ($i > 0) {
                echo ",\r\n";
            }
            echo "{\"id\"  : \"$data->id\", \"text\":\"$data->bentuk_badan_usaha\"}";
            $i++;
        }

        echo "]";
    }

    public function pengikatanJaminan()
    {
        $getPengikatJaminan = \DB::table("m_pengikatan_jaminan")
        ->pluck("jenis_pengikatan", "id");
        return response()->json($getPengikatJaminan);

    }

    public function pengikatanPendanaan()
    {
        $getPengikatPendanaan = \DB::table("m_pengikatan_pendanaan")
        ->pluck("jenis_pengikatan", "id");
        return response()->json($getPengikatPendanaan);

    }

    public function maxTenorPembiayaan($tujuan_pendanaan){
        $maxTenor = \DB::table('m_tujuan_pembiayaan')->where('id', $tujuan_pendanaan)->value('max_tenor');
        echo $maxTenor ? $maxTenor : 12;
    }
}
