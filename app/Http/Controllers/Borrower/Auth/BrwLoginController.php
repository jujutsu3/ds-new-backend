<?php

namespace App\Http\Controllers\Borrower\Auth;

use App\Borrower;
use App\BorrowerDetails;
use App\BorrowerRekening;
use App\BorrowerPendanaan;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Lang;
use App\AuditTrail;
use App\Http\Controllers\BorrowerDashboardController;
use DB;

use Carbon\Carbon;

class BrwLoginController extends Controller
{
	//use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */

	//private const  url = 'http://103.28.23.203/borrower/';
	private const  url = 'http://127.0.0.1:8001/borrower/';

	// protected $redirectTo = '/';
	protected $redirectTomail = '/resendMailborrower';

	public function username()
	{
		return 'username';
	}
	public function password()
	{
		return 'password';
	}
	public function showLoginForm()
	{
		return redirect('/#loginModalAs');
	}

	// public function login(Request $request)
	// {  

	// $password = $request->password;

	// // $borrower = Borrower::where('username', $request->username)->first();

	// $username = $request->username;
	// $password = $request->password;

	// // $borrower = Borrower::where('username', $request->username)->first();
	// $no_hp = "";
	// if(substr($request->username,0,2) == "62"){
	// $no_hp .= $request->username;
	// }else{
	// $no_hp .= "62".substr($request->username,1);
	// }


	// // $borrower = Borrower::where('username', $request->username)
	// // ->orWhere('email', $request->username)->first();
	// $borrower = DB::table('brw_user as a')
	// ->selectRaw('a.*, b.brw_id, b.no_tlp')
	// ->join('brw_user_detail AS b', 'a.brw_id', '=', 'b.brw_id')
	// ->where('a.username', $request->username)
	// ->orWhere('a.email', $request->username)
	// ->orWhere('b.no_tlp', 'like', $no_hp)
	// ->first();
	// // dd($borrower);


	// if (is_null($borrower)) {

	// return redirect()->to('/#modal_login_borrower')->with('status_kosong','Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
	// }

	// else if(!Hash::check($request->password, $borrower->password)) {   


	// return redirect()->to('/#modal_login_borrower')->with('status_password','Nama pengguna & kata sandi yang anda masukan salah ...')->withInput($request->only($this->username(), 'remember'));

	// }
	// else if ($borrower->status == "suspend"){

	// return redirect()->to('/#modal_login_borrower')->with('status_suspend','Nama Pengguna anda kami suspend ...')->withInput($request->only($this->username(), 'remember'));
	// }
	// else{

	// Auth::guard('borrower')->LoginUsingId($borrower->brw_id);

	// // $password_updated_at =  Auth::guard('borrower')->user()->password_updated_at;

	// // if ($password_updated_at != null) {

	// //     $password_expiry_days = Auth::guard('borrower')->user()->password_expiry_days;

	// // 	$password_expiry_at = Carbon::parse($password_updated_at)->addDays($password_expiry_days);

	// //     if($password_expiry_at->lessThan(Carbon::now())){

	// // 		$update_status_expired = Borrower::where('username',$request->username)->update(['status' => 'expired']);

	// // 		return redirect('borrower/ubahPasswordExpiredBorrower');

	// //     } else {
	// //     //      syslog(0,"normal login");
	// //     }
	// // } else {

	// // }

	// $borrowerDetails = BorrowerDetails::where('brw_id', $borrower->brw_id)->first();
	// $plafon = BorrowerRekening::where('brw_id',$borrower->brw_id)->first();

	// $pendanaan = BorrowerPendanaan::select('brw_pendanaan.*', 'proyek.nama','brw_invoice.tgl_jatuh_tempo')
	// ->leftjoin('brw_invoice','brw_pendanaan.pendanaan_id', '=' ,'brw_invoice.pendanaan_id')
	// ->leftjoin('proyek','brw_pendanaan.id_proyek', '=' ,'proyek.id')
	// ->where('brw_pendanaan.brw_id',$borrower->brw_id)
	// ->whereIn('brw_pendanaan.status',array(0,1,2,3))->get();
	// $pnd = array();

	// $username = Auth::guard('borrower')->user()->username;

	// Session::put('brw_type', $borrowerDetails === null ? null : $borrowerDetails->brw_type);
	// Session::put('brw_nama', $borrower === null ? null : $borrower->username);
	// Session::put('brw_ptotal', $plafon === null ? null : $plafon->total_plafon);
	// Session::put('brw_ppake', $plafon === null ? null : $plafon->total_terpakai);
	// Session::put('brw_psisa', $plafon === null ? null : $plafon->total_sisa);
	// Session::put('pendanaan',$pnd);
	// Session::put('brw_id',$borrower->brw_id);

	// $audit = new AuditTrail;
	// $audit->fullname = $username;
	// $audit->description = "Login";
	// $audit->ip_address =  \Request::ip();
	// $audit->save();

	// if(Auth::guard('borrower')->user()->status == 'notfilled'){
	// $arahan = '/borrower/lengkapi_profile';
	// }
	// elseif(Auth::guard('borrower')->user()->status == 'Not Active'){
	// return redirect($this->redirectTomail)->with('reg_sukses', 'Pendaftaran Sukses, Cek Email step selanjutnya')
	// ->with('nama', $username)
	// ->with('email', $borrower->email)
	// ->with('email_verif',$borrower->email_verif);
	// }
	// else{
	// $arahan = '/';
	// }
	// return redirect($arahan);

	// }


	// }

	public function login(Request $request)
	{

		$username = $request->username;
		$password = $request->password;

		$borrower = "";
		if (is_numeric($request->username)) {

			if (substr($request->username, 0, 2) == "62") {
				$no_hp = substr($request->username, 2);
				$borrower = DB::table('brw_user as a')->join('brw_user_detail as b', 'a.brw_id', '=', 'b.brw_id')->where('b.no_tlp', 'like', '%' . $no_hp . '%')->first();

				if (is_null($borrower)) {

					return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
				} elseif (is_null($borrower)) {

					return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
				} else if ($borrower->status == "suspend") {

					return redirect()->to('/#modal_login_borrower')->with('status_suspend', 'Nama Pengguna anda kami suspend ...')->withInput($request->only($this->username(), 'remember'));
				} else if (!Hash::check($request->password, $borrower->password)) {


					return redirect()->to('/#modal_login_borrower')->with('status_password', 'Nama pengguna & kata sandi yang anda masukan salah ...')->withInput($request->only($this->username(), 'remember'));
				}
			} else if (substr($request->username, 0, 1) == "0") {

				$no_hp = substr($request->username, 1);
				$borrower = DB::table('brw_user as a')->join('brw_user_detail as b', 'a.brw_id', '=', 'b.brw_id')->where('b.no_tlp', 'like', '%' . $no_hp . '%')->first();

				if (is_null($borrower)) {

					return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
				} else if ($borrower->status == "suspend") {

					return redirect()->to('/#modal_login_borrower')->with('status_suspend', 'Nama Pengguna anda kami suspend ...')->withInput($request->only($this->username(), 'remember'));
				} elseif (is_null($borrower)) {

					return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
				} else if (!Hash::check($request->password, $borrower->password)) {


					return redirect()->to('/#modal_login_borrower')->with('status_password', 'Nama pengguna & kata sandi yang anda masukan salah ...')->withInput($request->only($this->username(), 'remember'));
				}
			}
		} else if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {

			$borrower = DB::table('brw_user as a')->where('a.email', $request->username)->first();

			if (is_null($borrower)) {

				return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
			} elseif (is_null($borrower)) {

				return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
			} else if ($borrower->status == "suspend") {

				return redirect()->to('/#modal_login_borrower')->with('status_suspend', 'Nama Pengguna anda kami suspend ...')->withInput($request->only($this->username(), 'remember'));
			} else if (!Hash::check($request->password, $borrower->password)) {


				return redirect()->to('/#modal_login_borrower')->with('status_password', 'Nama pengguna & kata sandi yang anda masukan salah ...')->withInput($request->only($this->username(), 'remember'));
			}
		} else {

			$borrower = DB::table('brw_user as a')->where('a.username', $request->username)->first();

			if (is_null($borrower)) {

				return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
			} elseif (is_null($borrower)) {

				return redirect()->to('/#modal_login_borrower')->with('status_kosong', 'Nama pengguna & kata sandi yang anda masukan salah  ...')->withInput($request->only($this->username(), 'remember'));
			} else if ($borrower->status == "suspend") {

				return redirect()->to('/#modal_login_borrower')->with('status_suspend', 'Nama Pengguna anda kami suspend ...')->withInput($request->only($this->username(), 'remember'));
			} else if (!Hash::check($request->password, $borrower->password)) {


				return redirect()->to('/#modal_login_borrower')->with('status_password', 'Nama pengguna & kata sandi yang anda masukan salah ...')->withInput($request->only($this->username(), 'remember'));
			}
		}

		Auth::guard('borrower')->LoginUsingId($borrower->brw_id);

		$borrowerDetails = BorrowerDetails::where('brw_id', $borrower->brw_id)->first();
		$plafon = BorrowerRekening::where('brw_id', $borrower->brw_id)->first();

		$pendanaan = BorrowerPendanaan::select('brw_pendanaan.*', 'proyek.nama', 'brw_invoice.tgl_jatuh_tempo')
			->leftjoin('brw_invoice', 'brw_pendanaan.pendanaan_id', '=', 'brw_invoice.pendanaan_id')
			->leftjoin('proyek', 'brw_pendanaan.id_proyek', '=', 'proyek.id')
			->where('brw_pendanaan.brw_id', $borrower->brw_id)
			->whereIn('brw_pendanaan.status', array(0, 1, 2, 3))->get();
		$pnd = array();

		$username = Auth::guard('borrower')->user()->username;

		Session::put('brw_type', $borrowerDetails === null ? null : $borrowerDetails->brw_type);
		Session::put('brw_nama', $borrower === null ? null : $borrower->username);
		Session::put('brw_ptotal', $plafon === null ? null : $plafon->total_plafon);
		Session::put('brw_ppake', $plafon === null ? null : $plafon->total_terpakai);
		Session::put('brw_psisa', $plafon === null ? null : $plafon->total_sisa);
		Session::put('pendanaan', $pnd);
		Session::put('brw_id', $borrower->brw_id);

		$audit = new AuditTrail;
		$audit->fullname = $borrower->username;
		$audit->description = "Login";
		$audit->ip_address =  \Request::ip();
		$audit->save();

		if (Auth::guard('borrower')->user()->status == 'notfilled') {
			$arahan = '/borrower/beranda';
		}
		/* ARL SIMPLIFIKASI */ elseif (Auth::guard('borrower')->user()->status == 'notpreapproved') {
			$arahan = '/borrower/beranda';
		} elseif (Auth::guard('borrower')->user()->status == 'Not Active') {
			return redirect($this->redirectTomail)->with('reg_sukses', 'Pendaftaran Sukses, Cek Email step selanjutnya')
				->with('nama', $borrower->username)
				->with('email', $borrower->email)
				->with('email_verif', $borrower->email_verif);
		} else {
			$arahan = '/';
		}
		return redirect($arahan);
	}

	public function logout()
	{

		//$borrowerDetails === null ? null : $borrowerDetails->nama;
		//dd(Auth::guard('borrower')->user()->username);
		//$username = Auth::guard('borrower')->user()->username === null ? "Null" : Auth::guard('borrower')->user()->username;

		if (Auth::guard('borrower')->check()) {
			$username = Auth::guard('borrower')->user()->username;
			Auth::guard('borrower')->logout();
			\Session::forget('key');
			\Session::flush();


			$audit = new AuditTrail;
			$audit->fullname = $username;
			$audit->description = "Logout";
			$audit->ip_address =  \Request::ip();
			$audit->save();
		}

		return redirect('/');
	}

	public function loginKpr(Request $request)
	{

		$client = new client();
		$response = $client->post(config('app.apilink') . "/borrower/login", [
			//$response = $client->post(self::url.'login', [
			//'headers' => ['Content-Type' => 'application/json'],
			'form_params' => [
				'username' => $request->username,
				'password' => $request->password
			]
		]);


		$body = json_decode($response->getBody()->getContents());
		// dd($body);
		if ($body->status == "sukses") {

			Auth::guard('borrower')->LoginUsingId($body->data_borrower->brw_id);
			$username = Auth::guard('borrower')->user()->username;
			Session::put('brw_type', $body->brw_type);
			Session::put('brw_nama', $body->brw_nama);
			Session::put('brw_ptotal', $body->brw_ptotal);
			Session::put('brw_ppake', $body->brw_ppake);
			Session::put('brw_psisa', $body->brw_psisa);
			Session::put('pendanaan', $body->data_pendanaan);
			Session::put('brw_id', $body->data_borrower->brw_id);

			$audit = new AuditTrail;
			$audit->fullname = $username;
			$audit->description = "Login";
			$audit->ip_address =  \Request::ip();
			$audit->save();

			return redirect('/borrower/lengkapi_kpr_profile');
		} elseif ($body->status == "gagal_data") {
			return redirect()->to('/#modal_login_kpr')->with('status_kosong', 'Username & Password Anda Salah ...')->withInput($request->only($this->username(), 'remember'));
		} elseif ($body->status == "gagal_password") {
			return redirect()->to('/#modal_login_kpr')->with('status_password', 'Username & Password Anda Salah ...')->withInput($request->only($this->username(), 'remember'));
		} elseif ($body->status == "suspend") {
			return redirect()->to('/#modal_login_kpr')->with('status_suspend', 'Username anda kami suspend ...')->withInput($request->only($this->username(), 'remember'));
		}
	}
}
