<?php

namespace App\Http\Controllers\Borrower\Auth;

use App\Borrower;
//use App\DetilInvestor;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Mail;
use App\Jobs\ProcessEmailBorrower;
use App\Mail\EmailAktifasiPenerimaPendanaan;
use Lang;
use DB;
//use Auth;

class BrwRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    //private const  url = 'http://103.28.23.203/borrower/';
    // private const  url = 'http://127.0.0.1:8001/borrower/';

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    //protected $redirectTo = '/';
    protected $redirectTomail = '/resendMailborrower';

    public function username_borrower()
    {
        return 'username_borrower';
    }
    public function email()
    {
        return 'email';
    }
    public function password()
    {
        return 'password';
    }
    public function showRegisterForm()
    {
        return redirect('/#modal_register_borrower');
    }

    protected function validator(array $data)
    {
        Validator::extend('without_spaces', function ($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        }, 'Tidak boleh ada spasi');

        $validator =  Validator::make($data, [
            'username' => 'required|without_spaces|string|max:50|unique:brw_user',
            'email' => 'required|string|email|max:75|unique:brw_user',
            'password' => 'required|string|min:8|confirmed',
        ]);
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $Borrower = Borrower::create([
            'username' => $data['username_borrower'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'email_verif' => str_random(30),
            'status' => 'notfilled',
            'password_expiry_days' => 30,
            'password_updated_at' => Carbon::now()
            //'ref_number' => $data['ref_number']

        ]);
        //dispatch(new ProcessEmail($user, 'regis'));
        // $email = new EmailAktifasiPenerimaPendanaan($Borrower);
        // Mail::to($Borrower->email)->send($email);

        return $Borrower;
    }

    public function register(Request $request)
    {
        $lengthUsername     = strlen($request->username_borrower);
        $lengthPass         = strlen($request->password);
        $email                 = $request->email;

        if ($lengthUsername < 6) {
            return redirect()->to('/#modal_register_borrower')->with('brw_status_username', 'Nama pengguna harus minimal 6 karakter ...')->withInput($request->only($this->username_borrower(), 'remember'));
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

            return redirect()->to('/#modal_register_borrower')->with('brw_status_email', 'Format Email Tidak Valid ...')->withInput($request->only($this->email(), 'remember'));
        }

        // new validation email 28 sept 2021
        $ar = preg_split("/@/", $email);
        if (!checkdnsrr($ar[1], 'ANY')) {

            return redirect()->to('/#modal_register_borrower')->with('brw_status_email', 'Email Anda Tidak Valid ...')->withInput($request->only($this->email(), 'remember'));
        }
        // end validation email

        if ($lengthPass < 8) {

            return redirect()->to('/#modal_register_borrower')->with('brw_status_length_password', 'Password Kurang Dari 8 Digit, Minimal Harus 8 Digit ...');
        }

        if ($request->password != $request->password_confirmation) {

            return redirect()->to('/#modal_register_borrower')->with('brw_status_same_password', 'Password Tidak Sama, Pastikan Password Harus Sama ...');
        }

        if (strlen($request->password_confirmation) < 8) {

            return redirect()->to('/#modal_register_borrower')->with('brw_status_length_confirm_password', 'Password Tidak Sama, Pastikan Password Harus Sama ...')->withInput($request->only($this->password(), 'remember'));
        } else {

            $email_verif = $this->str_random();
            $validator = $this->validator($request->all());
            $username = Borrower::where('username', $request->username_borrower)->first(); // cek username
            $email = Borrower::where('email', $request->email)->first(); // cek email

            if ($username['username'] == $request->username_borrower) {

                return redirect()->to('/#modal_register_borrower')->with('brw_status_username', 'Nama Pengguna Anda Sudah Terdaftar ...')->withInput($request->only($this->username_borrower(), 'remember'));
            }
            if ($email['email'] == $request->email) {

                return redirect()->to('/#modal_register_borrower')->with('brw_status_email', 'Email Anda Sudah Terdaftar ...')->withInput($request->only($request->email, 'remember'))->withInput($request->only($this->email(), 'remember'));
            } else {

                $createBorrower = DB::select("select put_into_brw_user('" . $request->username_borrower . "', '" . $request->email . "', '" . Hash::make($request->password) . "', '', '" . $email_verif . "', '', 'Not Active', '', 180, '" . date('Y-m-d H:i:s') . "', '" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "', 'BrwRegisterController', '141') as brw_id;");
                //$createBorrower = DB::select("select put_into_brw_user('".$request->username_borrower."', '".$request->email."', '".Hash::make($request->password)."', '', '".$email_verif."', '', 'notfilled', '', 180, '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."', 'BrwRegisterController', '141') as brw_id;");

                if ($createBorrower == true) {

                    //Auth::guard('borrower')->LoginUsingId($createBorrower[0]->{'brw_id'});

                    //return redirect('/borrower/lengkapi_profile');

                    $req_array = array("username" => $request->username_borrower, "email" => $request->email, "password" => $request->password, "remember_token" => "", "email_verif" => $email_verif, "ref_number" => "", "status" => "notfilled");

                    $email = new EmailAktifasiPenerimaPendanaan($req_array);
                    Mail::to($request->email)->send($email);

                    // return view('/redirectTomail', compact('req_array'));
                    // return redirect($this->redirectTomail)->with('email', $request->email);

                    return redirect($this->redirectTomail)->with('reg_sukses', 'Pendaftaran Sukses, Cek Email step selanjutnya')
                        ->with('nama', $request->username_borrower)
                        ->with('email', $request->email)
                        ->with('email_verif', $email_verif);
                } else {

                    //return redirect('/borrower/lengkapi_profile');

                }
            }
        }
    }

    private function str_random()
    {
        return (str_random(30));
    }

    public function emailConfirm($code)
    {

        if (empty($code)) {
            return view('errors.email-verif-false_penerima_pendanaan');
        }
        $user = Borrower::where('email_verif', $code)->first();

        if (!$user) {
            return view('errors.email-verif-false_penerima_pendanaan');
        } else {
            if ($user->email_verif === $code) {
                $user->status = 'notpreapproved'; // ARL SIMPLIFIKASI. Previous value: 'notfilled';
                // $user->status = 'notfilled';
                $user->email_verif = Null;
                $user->save();

                return view('errors.email-verif-ok_penerima_pendanaan');
            } else {
                return redirect('/#modal_login_borrower');
            }
        }
    }

    // public function emailConfirm($code) {
    // $user = Borrower::where('email_verif', $code)->first();

    // if(!$user){
    // return "False";
    // }
    // else{
    // if($user->email_verif === $code){
    // $user->status = 'notfilled';
    // $user->email_verif = Null;
    // $user->save();

    // return redirect('/');

    // }
    // else{
    // return redirect('/#loginModalAs');
    // }
    // }
    // }

}
