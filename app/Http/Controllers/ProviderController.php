<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Provider;
use App\LogAkadDigiSignInvestor;
use App\LogAkadDigiSignBorrower;
use App\CheckUserSign;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Str;
use App\BorrowerPendanaan;
use App\BorrowerPencairan;
use DB;

class ProviderController extends Controller
{
    
    public function __construct(){
		
        // $this->middleware('auth:api-providers',['except' => ['login','CallBack', 'register']]);
    
    }

    public function login(Request $request)
    {	
		
		$response = $request;
        
		// die($response->username);
		
        $credentials = $request->only('username', 'password');
		
		try {

            if (! $token = Auth::guard('api-providers')->attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'],400);
            }

            // if (! $token = JWTAuth::attempt($credentials)) {
            //     return response()->json(['error' => 'invalid_credentials'], 400);
            // }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
		// die()
        return response()->json(compact('token'));
    }
	
	public function CallBack(Request $request)
	{	
		
        $content = $request->getContent();
		$response_decode = json_decode($content);
		$checkUser = Provider::where('username', $request->username)->first(); // get username provider
		$response = array();
		if($checkUser){ // jika user privy ada
			
			if(Hash::check($request->password, $checkUser->password)){
				
				if($response_decode == null){ // payload kosong
					
					$response["status"] = "invalid payload";
					// return response()->json($response);
				}
				else{
					
					$eventName = $response_decode->{'eventName'};
					
					$docToken = !empty($response_decode->{'data'}->{'docToken'}) ? $response_decode->{'data'}->{'docToken'} : '';
					$docStatus = !empty($response_decode->{'data'}->{'documentStatus'}) ? $response_decode->{'data'}->{'documentStatus'} : '';
					if($eventName == "document-completed"){ // jika eventnya document
						
						

							
						// handel doc wakalah investor & dsi
						$dataLogWakalahInvestor = LogAkadDigiSignInvestor::where(\DB::raw('substr(document_id, 1, 15)'), '=' , 'investorKontrak')
										->where('docToken',$docToken)
										->first();
						
						if($dataLogWakalahInvestor){ // jika document akad wakalah bil ujroh investor
							
							$logAkadDigiSignInvestor = logAkadDigiSignInvestor::where('docToken', $docToken)->update( 
								[
									'status' => $response_decode->{'data'}->{'recipients'}[1]->{'signatoryStatus'},
									// 'tgl_sign' => $date->format('Y-m-d'),
									'downloadURL' => $response_decode->{'data'}->{'download'}->{'url'}
								]
							);

							// update log status log akad wakalah investor
							$updateLogAkadInvestor = DB::table("log_akad_wakalah_investor")->where("docToken",$response_decode->{'data'}->{'docToken'})->update(["status"=>2]);
							
							
							$selectParam = DB::table("m_param")->where("id", 2)->first();
							$valueParam = $selectParam->value;
							
							$insertLogPrivy = DB::table("log_privy_investor")->insert(array(
								"investor_id"=> $dataLogWakalahInvestor->investor_id,
								"transaction_id"=> $response_decode->{'data'}->{'docToken'},
								"nominal"=> $valueParam,
								"status"=> 0,
								"created_at"=> date("Y-m-d H:i:s")
							));

							$response["status"] = $docStatus;
							$response["message"] = $response_decode->{'message'};

						}else{ // jika akad murobahah borrower
							
							$dataLogMurobahahBorrower = LogAkadDigiSignBorrower::where('docToken', $docToken)
								->where(\DB::raw('substr(document_id, 1, 15)'), '=' , 'borrowerKontrak')
								->orderBy('id_log_akad_borrower','desc')
								->first();

							if($dataLogMurobahahBorrower){

								$UpdateAkadDigiSignBorrower = LogAkadDigiSignBorrower::where('docToken', $docToken)->update( 
									[
										'status' => $response_decode->{'data'}->{'recipients'}[1]->{'signatoryStatus'},
										'downloadURL' => $response_decode->{'data'}->{'download'}->{'url'}
									]
								);

							

								$getPendanaan = DB::table('brw_pendanaan')
									->where('brw_id', $dataLogMurobahahBorrower->brw_id)
									->where('id_proyek', $dataLogMurobahahBorrower->id_proyek)
									->first();

								$updateStatusPendanaan = BorrowerPendanaan::where('brw_id', $dataLogMurobahahBorrower->brw_id)
									->where('id_proyek', $dataLogMurobahahBorrower->id_proyek)
									->update(
										[
											'status' => 2
										]
									);
								
								$sumDanaPendanaan = DB::table("pendanaan_aktif")->where('proyek_id', $dataLogMurobahahBorrower->id_proyek)->get()->sum("total_dana");
								
								$plafonBorrower 	= DB::table('brw_rekening')->where('brw_id', $dataLogMurobahahBorrower->brw_id)->first();
								$Plafon     		= $plafonBorrower->total_plafon;
								$Plafon_sisa     	= $plafonBorrower->total_sisa;
								$Plafon_terpakai    = $plafonBorrower->total_terpakai;
								
														
								
								$updatePlafonSisa 			= $Plafon_sisa == 0 ?  $sumDanaPendanaan : $Plafon_sisa - $sumDanaPendanaan;
								$updatePlafonTerpakai 		= $Plafon_terpakai == 0 ? $Plafon  - $sumDanaPendanaan : $sumDanaPendanaan + $Plafon_terpakai;

								// UPDATE PLAFON
								// $updatePlafon = DB::table('brw_rekening')
								// ->where('brw_id', $dataLogMurobahahBorrower->brw_id)
								// ->update(['total_terpakai' => $updatePlafonTerpakai, 'total_sisa' => $updatePlafonSisa]);

								
								// $cair = 250000000;
								$imbal = $getPendanaan->estimasi_imbal_hasil;
								$nominal = ($sumDanaPendanaan*($imbal/100));
								$insertPencairan = DB::table('brw_pencairan')->insertGetId(array(
									'brw_id' => $dataLogMurobahahBorrower->brw_id,
									'pendanaan_id' => $getPendanaan->pendanaan_id,
									'nominal_pencairan' => $sumDanaPendanaan,
									'nominal_imbal_hasil' => $nominal,
									'tgl_req_pencairan' => date('Y-m-d'),
									'tgl_pencairan' => '',
									'dicairkan_oleh' => '',
									'status' => 1,
									'bukti_transfer' => ''
								));
								
								
								$insertLogPencairan = DB::table('brw_log_pencairan')->insert([
									[
										'pencairan_id' => $insertPencairan,
										'brw_id' => $dataLogMurobahahBorrower->brw_id,
										'pendanaan_id' => $getPendanaan->pendanaan_id,
										'status' => 1,
										'keterangan' => 'Proses Pencairan'
										//'created_at' => date('Y-m-d h:m:i')
									]
								]);
								
								$insertLogPendanaan = DB::table('brw_log_pendanaan')->insert([
									[
										'pendanaan_id' => $getPendanaan->pendanaan_id,
										'brw_id' => $dataLogMurobahahBorrower->brw_id,
										'status' => 2,
										'keterangan' => 'Proses Pencairan'
									]
								]);
								
								$response["status"] = $docStatus;
								$response["message"] = $response_decode->{'message'};

							}
						}
					} // end if event document completed
					
					else if($eventName == "document-signed"){ // jika eventnya document  signed
							
						// di hide sementara
						
						// cek document investor
						// $cekDocAkadWakalahInvestor = LogAkadDigiSignInvestor::where('docToken', $docToken)
						// 	->where(\DB::raw('substr(document_id, 1, 15)'), '=' , 'investorKontrak')
						// 	->orderBy('id_log_akad_investor','desc')
						// 	->first();
							
							
						// if($cekDocAkadWakalahInvestor){ // jika doc ditemukan/ atau docToken ada di table
							
						// 	// update status
						// 	$logAkadDigiSignInvestor = logAkadDigiSignInvestor::where('docToken', $docToken)->update( 
						// 		[
						// 			'status' => $response_decode->{'data'}->{'recipients'}[1]->{'signatoryStatus'},
						// 			'downloadURL' => $response_decode->{'data'}->{'download'}->{'url'}
						// 		]
						// 	);
							
						// 	$response["status"] = $docStatus;
						// 	$response["message"] = $response_decode->{'message'};
							
						// }else{
							
							
						// 	// cek document borrower
						// 	$cekDocMurobahahBorrower = LogAkadDigiSignBorrower::where('docToken', $docToken)
						// 		->where(\DB::raw('substr(document_id, 1, 15)'), '=' , 'borrowerKontrak')
						// 		->orderBy('id_log_akad_borrower','desc')
						// 		->first();
							
						// 	//dd($cekDocMurobahahBorrower);
						// 	if($cekDocMurobahahBorrower){ // jika ada data wakalah borrower
						// 		$LogAkadDigiSignBorrower = LogAkadDigiSignBorrower::where('docToken', $docToken)->update( 
						// 			[
						// 				'status' => $response_decode->{'data'}->{'recipients'}[1]->{'signatoryStatus'},
						// 			]
						// 		);
								
						// 		$updateStatusPendanaan = DB::table('brw_pendanaan')
						// 			->where('brw_id', $cekDocMurobahahBorrower->brw_id)
						// 			->where('id_proyek', $cekDocMurobahahBorrower->id_proyek);
						// 			$a = $updateStatusPendanaan->update(['status' => 2]);
						// 			$b = $updateStatusPendanaan->select('pendanaan_id', 'estimasi_imbal_hasil')->get();
						// 		$bb = $b[0]->pendanaan_id;
								
								
						// 		$sumDanaPendanaan = DB::table("pendanaan_aktif")->where('proyek_id', $cekDocMurobahahBorrower->id_proyek)->get()->sum("total_dana");
								
						// 		$cair = 250000000;
						// 		$imbal = 20;
						// 		$nominal = ($sumDanaPendanaan*(20/100));
						// 		$insertPencairan = DB::table('brw_pencairan')->insertGetId(array(
						// 			'brw_id' => $cekDocMurobahahBorrower->brw_id,
						// 			'pendanaan_id' => $bb,
						// 			//'nominal_pencairan' => $sumDanaPendanaan->total_dana,
						// 			'nominal_pencairan' => '250000000',
						// 			'nominal_imbal_hasil' => $nominal,
						// 			'tgl_req_pencairan' => date('Y-m-d'),
						// 			'tgl_pencairan' => '',
						// 			'dicairkan_oleh' => '',
						// 			'status' => 1,
						// 			'bukti_transfer' => ''
						// 		));
								
								
						// 		$insertLogPencairan = DB::table('brw_log_pencairan')->insert([
						// 			[
						// 				'pencairan_id' => $insertPencairan,
						// 				'brw_id' => $cekDocMurobahahBorrower->brw_id,
						// 				'pendanaan_id' => $bb,
						// 				'status' => 1,
						// 				'keterangan' => 'Proses Pencairan'
						// 				//'created_at' => date('Y-m-d h:m:i')
						// 			]
						// 		]);
								
						// 		$insertLogPendanaan = DB::table('brw_log_pendanaan')->insert([
						// 			[
						// 				'pendanaan_id' => $bb,
						// 				'brw_id' => $cekDocMurobahahBorrower->brw_id,
						// 				'status' => 2,
						// 				'keterangan' => 'Proses Pencairan'
						// 			]
						// 		]);
						// 		// dd($insertPencairan);
						// 		$response["status"] = $docStatus;
						// 		$response["message"] = $response_decode->{'message'};
								
						// 	}else{
								
						// 		// jika document tidak dtemukan
						// 		$response["status"] = "invalid_doc";
						// 		$response["message"] = "Document Tidak Ditemukan";
								
						// 	}
						// }
							
					}// end if event document signed
					else if($eventName == "register"){ // jika eventnya user

						$statusUser 	= $response_decode->{'data'}->{'status'};
						$message    	= $response_decode->{'message'};
						$privyID      	= !empty($response_decode->{'data'}->{'privyId'}) ? $response_decode->{'data'}->{'privyId'} : null;
						$userToken      = !empty($response_decode->{'data'}->{'userToken'}) ? $response_decode->{'data'}->{'userToken'} : null;

						if($statusUser == "verified" ){

							$date = \Carbon\Carbon::parse($response_decode->{'data'}->{'processedAt'});
							
							$updateStatusReg = CheckUserSign::where('userToken', $userToken)->update(
								[
									'status' => $statusUser,
									'privyID' => $privyID,
									'tgl_aktifasi'=>$date->format('Y-m-d')
								]
							);
							
							$response["status"] = $statusUser;
							$response["message"] = $message;
							
							
						}
						else if($statusUser == "rejected" || $statusUser == "invalid"){
							
							$date = \Carbon\Carbon::parse($response_decode->{'data'}->{'processedAt'});
							
							$updateStatusReg = CheckUserSign::where('userToken', $userToken)->update(
								[
									'status' => $statusUser,
									'privyID' => "",
								]
							);
							
							$response["status"] = $statusUser;
							$response["message"] = $message;
							
						}
						
						else{
				
							$response["status"] = $statusUser;
							$response["message"] = $message;
				
						}
					} // end if event register
				} // end if payload valid
			} //end if jika password valid
			else{
				$response["status"] = "invalid account";
			}
		} // end jika user privy ada
		else{
			$response["status"] = "invalid account";
		}
		return response()->json($response);
	}

    public function register(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|string|max:60',
            'username' => 'required|string|max:50',
            'password' => 'required|string|min:20',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = Provider::create([
            'client_id' => $request->get('client_id'),
            'username' => $request->get('username'),
            'password' => Hash::make($request->get('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user','token'),201);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }
    
}
