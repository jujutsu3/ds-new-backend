<?php
// privy
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\CheckUserSign;
use App\Investor;
use App\DetilInvestor;
use App\MasterProvinsi;
use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;
use App\RekeningInvestor;
use App\PendanaanAktif;
use App\Proyek;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\LogAkadDigiSignInvestor;
use App\MasterNoAkadInvestor;
use Illuminate\Support\Facades\Storage;
use App\MasterNoAkadBorrower;
use App\BorrowerJaminan;
use App\MasterJenisJaminan;
use App\Borrower;
use App\BorrowerDetails;
use App\BorrowerPendanaan;
use App\BorrowerTipePendanaan;
use App\BorrowerRekening;
use App\LogAkadDigiSignBorrower;
use App\AhliWarisInvestor;
use App\MasterNoSP3;
use App\LogSP3Borrower;
use App\LogRekening;
use App\LogDigiSignResponse;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007\Element\Section;
use Terbilang;
use DB;


class FaqController extends Controller
{

    public function __construct()
    {

        //$this->middleware('auth:api-providers',['only' => ['callbackResponse']]);

    }

    public function listKategoriGroupFaq () {
		$getKategoryGroup = DB::select("SELECT  fc.category_name
                                               ,fg.group_name
                                               ,fc.category_id
                                               ,fg.group_id
                                          FROM  m_faq_category fc
                                               ,m_faq_group    fg
                                         WHERE fc.category_id = fg.category_id");
	    return json_encode($getKategoryGroup);
	}
	
	public function listContentFaq ($category_id, $group_id) {
		$getKategoryGroup = DB::select("SELECT  fc.category_name
       ,fg.group_name
       ,ft.*
  FROM  m_faq_category fc
       ,m_faq_group    fg
       ,m_faq_content  ft
 WHERE fc.category_id = fg.category_id
   AND fg.group_id    = ft.group_id
   AND ft.category_id = " . $category_id . "
   AND ft.group_id    = " . $group_id );
	    return json_encode($getKategoryGroup);
	}

}
