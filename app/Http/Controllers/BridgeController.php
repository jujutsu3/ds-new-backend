<?php
// privy
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\CheckUserSign;
use App\DetilInvestor;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\LogAkadDigiSignInvestor;
use App\MasterNoAkadInvestor;
use Illuminate\Support\Facades\Storage;
use App\MasterNoAkadBorrower;
use App\BorrowerJaminan;
use App\MasterJenisJaminan;
use App\Borrower;
use App\BorrowerDetails;
use App\BorrowerPendanaan;
use App\BorrowerTipePendanaan;
use App\BorrowerRekening;
use App\LogAkadDigiSignBorrower;
use App\AhliWarisInvestor;
use App\MasterNoSP3;
use App\LogSP3Borrower;
use App\LogRekening;
use App\LogDigiSignResponse;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007\Element\Section;
use Terbilang;
use DB;
use DateTime;
use App\Proyek;
use App\PendanaanAktif;
use App\RekeningInvestor;
use App\Investor;
use App\LogPendanaan;
use App\PenarikanDana;
use App\Pemilik_Proyek;
use App\TmpSelectedProyek;
use App\IhDetilImbalUser;
use App\IhListImbalUser;
use App\Http\Controllers\RekeningController;


class BridgeController extends Controller
{

    public function __construct()
    {

        //$this->middleware('auth:api-providers',['only' => ['callbackResponse']]);

    }

    public function listProyek() {

        //return false;
        $proyekAktif = Proyek::where('status',1)->orderBy('proyek.profit_margin', 'desc')->get();
        $i = 0;
        foreach ($proyekAktif as $item){
            $data_pendana = PendanaanAktif::where('proyek_id',$item->id)->get();
            $all_dana = 0 ;
            foreach($data_pendana as $d){
                $all_dana += $d['nominal_awal'];
            }
            $profit_explode = (explode('.',$item->profit_margin));
            if($profit_explode[1]=='00'){
               $profit_margin=$profit_explode[0];
            }else{
                $profit_margin=$item->profit_margin;
            }
            $dayLeft = ($item->status == 3 ? 'Full' : ($item->status == 2 ? 'Closed' : (date_diff(date_create(Carbon::now()->format('Y-m-d')),date_create(Carbon::parse($item->tgl_selesai_penggalangan)->format('Y-m-d')))->format('%d') + 1).' hari'));
            

            $dataProyekAktif[$i] = [
                'id'=>$item->id, 
                'nama'=>$item->nama, 
                'imbal_hasil'=>$profit_margin, 
                'harga_paket'=>number_format($item->harga_paket,0,',','.'),
                'interval'=>$item->interval,
                'dayleft'=>$dayLeft,
                'terkumpul'=> $item->status == 3 || $item->status == 2 ? 100 : number_format((($item->terkumpul+$all_dana)/$item->total_need)*100,2,'.',','),
                'image_url'=>'/storage/'.$item->gambar_utama,
                // 'tenor'=>$item->tgl_mulai->diffInMonths($item->tgl_selesai)];
                'tenor' => $item->tenor_waktu,
                'akad' => ($item->akad == 1 ? 'Murabahah' : ($item->akad == 2 ? 'Mudharabah' : ($item->akad == 3 ? 'MMQ' : 'IMBT'))),
                'alamat' => $item->alamat,
                'butuh' => number_format($item->total_need,0,',','.')
            ];
                $i++;
        }

        $proyekClosed = Proyek::where('status',2)
                                ->orderBy('proyek.id', 'desc')->get();
 
        $i = 0;
        foreach ($proyekClosed as $item){
            $data_pendana = PendanaanAktif::where('proyek_id',$item->id)->get();
            $all_dana = 0 ;
            foreach($data_pendana as $d){
                $all_dana += $d['nominal_awal'];
            }
            $dayLeft = ($item->status == 3 ? 'Full' : ($item->status == 2 ? 'Closed' : (date_diff(date_create(Carbon::now()->format('Y-m-d')),date_create(Carbon::parse($item->tgl_selesai_penggalangan)->format('Y-m-d')))->format('%d') + 1).' hari'));
        
            $dataProyekClosed[$i] = [
                'id'=>$item->id, 
                'nama'=>$item->nama, 
                'imbal_hasil'=>$item->profit_margin, 
                'harga_paket'=>number_format($item->harga_paket,0,',','.'),
                'interval'=>$item->interval,
                'dayleft'=>$dayLeft,
                'terkumpul'=> $item->status == 3 || $item->status == 2 ? 100 : number_format((($item->terkumpul+$all_dana)/$item->total_need)*100,2,'.',','),
                'image_url'=>'/storage/'.$item->gambar_utama,
                // 'tenor'=>$item->tgl_mulai->diffInMonths($item->tgl_selesai)];
                'tenor' => $item->tenor_waktu,
                'akad' => ($item->akad == 1 ? 'Murabahah' : ($item->akad == 2 ? 'Mudharabah' : ($item->akad == 3 ? 'MMQ' : 'IMBT'))),
                'alamat' => $item->alamat,
                'butuh' => number_format($item->total_need,0,',','.')
            ];
                $i++;
        }

        $proyekFull = Proyek::where('status',3)
                                ->orderBy('proyek.id', 'desc')->get();
 
        $i = 0;
        foreach ($proyekFull as $item){
            $data_pendana = PendanaanAktif::where('proyek_id',$item->id)->get();
            $all_dana = 0 ;
            foreach($data_pendana as $d){
                $all_dana += $d['nominal_awal'];
            }
            $dayLeft = ($item->status == 3 ? 'Full' : ($item->status == 2 ? 'Closed' : (date_diff(date_create(Carbon::now()->format('Y-m-d')),date_create(Carbon::parse($item->tgl_selesai_penggalangan)->format('Y-m-d')))->format('%d') + 1).' hari'));
        
            $dataProyekFull[$i] = [
                'id'=>$item->id, 
                'nama'=>$item->nama, 
                'imbal_hasil'=>$item->profit_margin, 
                'harga_paket'=>number_format($item->harga_paket,0,',','.'),
                'interval'=>$item->interval,
                'dayleft'=>$dayLeft,
                'terkumpul'=> $item->status == 3 || $item->status == 2 ? 100 : number_format((($item->terkumpul+$all_dana)/$item->total_need)*100,2,'.',','),
                'image_url'=>'/storage/'.$item->gambar_utama,
                // 'tenor'=>$item->tgl_mulai->diffInMonths($item->tgl_selesai)];
                'tenor' => $item->tenor_waktu,
                'akad' => ($item->akad == 1 ? 'Murabahah' : ($item->akad == 2 ? 'Mudharabah' : ($item->akad == 3 ? 'MMQ' : 'IMBT'))),
                'alamat' => $item->alamat,
                'butuh' => number_format($item->total_need,0,',','.')
            ];
                $i++;
        }

        return response()->json(['dataProyekAktif' => isset($dataProyekAktif) ? $dataProyekAktif : null,'dataProyekFull' => isset($dataProyekFull) ? $dataProyekFull : null,'dataProyekClosed' => isset($dataProyekClosed) ? $dataProyekClosed : null]);
    }
	
	public function lenderWithFund ($limit) {
		$lenderWithFund = DB::select("SELECT  a.investor_id
                                             ,a.no_ktp_investor
                                             ,a.no_npwp_investor
                                             ,a.is_valid_npwp
                                             ,a.tgl_validasi_npwp
                                             ,a.tipe_pengguna
                                             ,a.npwp_perusahaan
                                             ,a.nama_investor
                                             ,a.jenis_identitas
                                        FROM  detil_investor a,
                                             (SELECT DISTINCT a.investor_id
                                                FROM  pendanaan_aktif a
                                                     ,proyek          b
                                               WHERE a.proyek_id = b.id
                                                 AND b.status IN (2,3,4)
                                                 AND a.total_dana > 0
                                              ) b
                                       WHERE a.investor_id = b.investor_id
                                         AND (is_valid_npwp IS NULL OR TRIM(is_valid_npwp) = '')
                                         AND (TRIM(a.no_ktp_investor) != '' and a.no_ktp_investor IS NOT NULL )
                                         AND LENGTH(a.no_ktp_investor) = 16
                                       LIMIT ". $limit);

		return json_encode($lenderWithFund);
	}
	
	public function konfirmasiMPajakResponse(Request $request)
    {
		$investor_id       = $request->input('investor_id');
		$is_valid_npwp     = $request->input('is_valid_npwp');
		$tgl_validasi_npwp = $request->input('tgl_validasi_npwp');
		
		$nama_wp           = $request->input('nama');
		$nik_wp            = $request->input('no_ktp_investor');
		$npwp_wp           = $request->input('no_npwp_investor');
		$alamat_wp         = $request->input('alamat');
		$status_wp         = $request->input('status_wp');
		$status_spt        = $request->input('status_spt');
		$tipe_pengguna     = $request->input('tipe_pengguna');
		$tipe_user         = $request->input('tipe_user');
		$response_message  = $request->input('response_message');
		
		$now = date("Y-m-d H:i:s");
		
		$is_exists = DB::select(DB::raw("SELECT COUNT(id) as cnt
		                                   FROM validasi_npwp
								          WHERE tipe_user = 'L'
										    AND id        = ". $investor_id));
		
		//syslog(0,"req=".json_encode($is_exists));
		//syslog(0,"req=".json_encode($request));
		//syslog(0,"investor_id=".$investor_id);
		//syslog(0,"is_exists=".$is_exists[0]->cnt);
		
		DB::table('detil_investor')
                ->where('investor_id', $investor_id)
                ->update(['is_valid_npwp'     => $is_valid_npwp,
				          'tgl_validasi_npwp' => $tgl_validasi_npwp
				]);
        
		if ($is_exists[0]->cnt == 0) {
		   //syslog(0,"...INSERT...");
		   $insertLog = DB::table("validasi_npwp")->insert(array(
                    "id"               => $investor_id,
					"Nama_wp"          => $nama_wp,
                    "Nik_wp"           => $nik_wp,
                    "Npwp_wp"          => $npwp_wp,
					"Alamat_wp"        => $alamat_wp,
					"status_wp"        => $status_wp,
                    "status_spt"       => $status_spt,
					"tipe_pengguna"    => $tipe_pengguna,
					"tipe_user"        => $tipe_user,
					"response_message" => $response_message
                ));
		} else {
			//syslog(0,"...UPDATE...");
			DB::table('validasi_npwp')
                ->where('id', $investor_id)
				->where('tipe_user', 'L')
				->update(['Nama_wp'          => $nama_wp,
				          'Nik_wp'           => $nik_wp,
						  'Npwp_wp'          => $npwp_wp,
						  'Alamat_wp'        => $alamat_wp,
						  'status_wp'        => $status_wp,
						  'status_spt'       => $status_spt,
						  'tipe_pengguna'    => $tipe_pengguna,
						  'tipe_user'        => $tipe_user,
						  'response_message' => $response_message,
						  'Action_date'      => $now
				]);
		}
		
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function pajakDataInfo () {
		$pajakDataInfo = DB::select("SELECT COUNT(id) Total, 'Dont Have NIK'
                                       FROM detil_investor
                                      WHERE no_ktp_investor IS NULL OR TRIM(no_ktp_investor) = ''
                                     UNION ALL
                                     SELECT COUNT(id), 'Have NIK'
                                       FROM detil_investor
                                      WHERE no_ktp_investor IS NOT NULL
                                     UNION ALL
                                     SELECT COUNT(id), 'Dont Have NPWP'
                                       FROM detil_investor
                                      WHERE no_npwp_investor IS NULL OR TRIM(no_npwp_investor) = ''
                                     UNION ALL
                                     SELECT COUNT(id), 'Have NPWP'
                                       FROM detil_investor
                                      WHERE no_npwp_investor IS NOT NULL");
	    
		return json_encode($pajakDataInfo);
	}
	
	public function validNPWP () {
		$validNPWP = DB::select("SELECT COUNT(id) Total, 'Valid NPWP'
                                   FROM detil_investor
                                  WHERE is_valid_npwp = 1
                                 UNION ALL
                                 SELECT COUNT(id) Total, 'Invalid NPWP'
                                   FROM detil_investor
                                  WHERE (is_valid_npwp IS NULL OR TRIM(is_valid_npwp) = '' OR is_valid_npwp = 0)");
	    
		return json_encode($validNPWP);

	}
	
	public function lenderWithFundSingle ($investor_id) {
		$lenderWithFund = DB::select("SELECT  a.investor_id
                                             ,a.no_ktp_investor
                                             ,a.no_npwp_investor
                                             ,a.is_valid_npwp
                                             ,a.tgl_validasi_npwp
                                             ,a.tipe_pengguna
                                             ,a.npwp_perusahaan
                                             ,a.nama_investor
                                             ,a.jenis_identitas
                                        FROM detil_investor a
                                       WHERE a.investor_id = ". $investor_id);
	    
		return json_encode($lenderWithFund);
	}
	
	public function validasiNpwp ($investor_id, $tipe_user) {
		$validasiNpwp = DB::select("SELECT  id
                                           ,Nama_wp
                                           ,Nik_wp
                                           ,Npwp_wp
                                           ,Alamat_wp
                                           ,status_wp
                                           ,status_spt
                                           ,tipe_pengguna
                                           ,tipe_user
                                           ,response_message
                                           ,Action_date
                                      FROM validasi_npwp
                                     WHERE id        = ". $investor_id ."
                                       AND tipe_user = '". $tipe_user ."'");
	    
		return json_encode($validasiNpwp);
	}
	
	public function lenderWithFundAll () {
		$lenderWithFundAll = DB::select("SELECT  a.investor_id
                                                ,a.no_ktp_investor
                                                ,a.no_npwp_investor
                                                ,a.is_valid_npwp
                                                ,a.tgl_validasi_npwp
                                                ,a.tipe_pengguna
                                                ,a.npwp_perusahaan
                                                ,a.nama_investor
                                                ,a.jenis_identitas
                                           FROM  detil_investor a,
                                                (SELECT DISTINCT a.investor_id
                                                   FROM  pendanaan_aktif a
                                                        ,proyek          b
                                                  WHERE a.proyek_id = b.id
                                                    AND b.status IN (2,3,4)
                                                    AND a.total_dana > 0
                                                 ) b
                                          WHERE a.investor_id = b.investor_id
                                            AND (is_valid_npwp IS NULL OR TRIM(is_valid_npwp) = '')
                                            AND (TRIM(a.no_ktp_investor) != '' and a.no_ktp_investor IS NOT NULL )
                                            AND LENGTH(a.no_ktp_investor) = 16");

		return json_encode($lenderWithFundAll);
	}
	
	public function countLenderWithFundAll () {
		$countLenderWithFundAll = DB::select("SELECT COUNT(a.investor_id) all_count
                                                FROM  detil_investor a,
                                                     (SELECT DISTINCT a.investor_id
                                                        FROM  pendanaan_aktif a
                                                             ,proyek          b
                                                       WHERE a.proyek_id = b.id
                                                         AND b.status IN (2,3,4)
                                                         AND a.total_dana > 0
                                                      ) b
                                               WHERE a.investor_id = b.investor_id
                                                 AND (is_valid_npwp IS NULL OR TRIM(is_valid_npwp) = '')
                                                 AND (TRIM(a.no_ktp_investor) != '' and a.no_ktp_investor IS NOT NULL )
                                                 AND LENGTH(a.no_ktp_investor) = 16");

		return json_encode($countLenderWithFundAll);
	}
	
	public function borrowerDataLimit ($limit) {
		$borrowerDataLimit = DB::select("SELECT  id
                                                ,brw_id
                                                ,brw_type
                                                ,nama
                                                ,nm_bdn_hukum
                                                ,npwp_perusahaan
                                                ,ktp
                                                ,npwp
                                                ,npwp_pasangan
                                                ,is_valid_npwp
                                                ,tgl_validasi_npwp
                                           FROM brw_user_detail
                                          LIMIT ". $limit);

		return json_encode($borrowerDataLimit);
	}
	
	public function borrowerDataSingle ($brw_id) {
		$borrowerDataSingle = DB::select("SELECT  id
                                                 ,brw_id
                                                 ,brw_type
                                                 ,nama
                                                 ,nm_bdn_hukum
                                                 ,npwp_perusahaan
                                                 ,ktp
                                                 ,npwp
                                                 ,npwp_pasangan
                                                 ,is_valid_npwp
                                                 ,tgl_validasi_npwp
                                            FROM brw_user_detail
                                           WHERE brw_id = ". $brw_id);

		return json_encode($borrowerDataSingle);
	}
	
	public function konfirmasiMPajakBResponse(Request $request)
    {
		$brw_id            = $request->input('brw_id');
		$is_valid_npwp     = $request->input('is_valid_npwp');
		$tgl_validasi_npwp = $request->input('tgl_validasi_npwp');
		
		$nama_wp           = $request->input('nama');
		$nik_wp            = $request->input('no_ktp');
		$npwp_wp           = $request->input('no_npwp');
		$alamat_wp         = $request->input('alamat');
		$status_wp         = $request->input('status_wp');
		$status_spt        = $request->input('status_spt');
		$tipe_pengguna     = $request->input('tipe_pengguna');
		$tipe_user         = $request->input('tipe_user');
		$response_message  = $request->input('response_message');
		
		$now = date("Y-m-d H:i:s");
		
		$is_exists = DB::select(DB::raw("SELECT COUNT(id) as cnt
		                                   FROM validasi_npwp
								          WHERE tipe_user = 'B'
										    AND id        = ". $brw_id));
		
		//syslog(0,"req=".json_encode($request));
		//syslog(0,"brw_id=".$brw_id);
		//syslog(0,"is_exists=".$is_exists->count());
		
		DB::table('brw_user_detail')
                ->where('brw_id', $brw_id)
                ->update(['is_valid_npwp'     => $is_valid_npwp,
				          'tgl_validasi_npwp' => $tgl_validasi_npwp
				]);
        
		if ($is_exists[0]->cnt == 0) {
		   //syslog(0,"...INSERT...");
		   $insertLog = DB::table("validasi_npwp")->insert(array(
                    "id"               => $brw_id,
					"Nama_wp"          => $nama_wp,
                    "Nik_wp"           => $nik_wp,
                    "Npwp_wp"          => $npwp_wp,
					"Alamat_wp"        => $alamat_wp,
					"status_wp"        => $status_wp,
                    "status_spt"       => $status_spt,
					"tipe_pengguna"    => $tipe_pengguna,
					"tipe_user"        => $tipe_user,
					"response_message" => $response_message
                ));
		} else {
			//syslog(0,"...UPDATE...");
			DB::table('validasi_npwp')
                ->where('id', $brw_id)
				->where('tipe_user', 'B')
                ->update(['Nama_wp'          => $nama_wp,
				          'Nik_wp'           => $nik_wp,
						  'Npwp_wp'          => $npwp_wp,
						  'Alamat_wp'        => $alamat_wp,
						  'status_wp'        => $status_wp,
						  'status_spt'       => $status_spt,
						  'tipe_pengguna'    => $tipe_pengguna,
						  'tipe_user'        => $tipe_user,
						  'response_message' => $response_message,
						  'Action_date'      => $now
				]);
		}
		
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function listImbalUserLimit ($masa, $tahun, $isvalid, $limit) {
		$listImbalUserLimit = DB::select("SELECT  bpi.Investor_id
                                                 ,bpi.nama_wp
												 ,bpi.nama_investor
                                                 ,UPPER(bpi.jenis_pajak) jenis_pajak
                                                 ,bpi.masa_pajak
                                                 ,bpi.tahun_pajak
                                                 ,bpi.tarif
                                                 ,bpi.no_npwp_investor
												 ,bpi.no_ktp_investor
                                                 ,bpi.imbal_payout
                                                 ,bpi.potongan_pajak
                                                 ,bpi.alamat_investor
                                                 ,bpi.phone_investor
                                                 ,bpi.email
                                                 ,bpi.tgl_lahir_investor
                                                 ,bpi.tempat_lahir_investor
                                                 ,bpi.no_passpor_investor
                                                 ,bpi.warganegara
                                                 ,bpi.is_taken
												 ,bpi.tgl_pemotongan
                                            FROM bukti_potong_investor bpi
                                           WHERE 1=1
                                             AND (bpi.no_npwp_investor IS NOT NULL OR TRIM(bpi.no_npwp_investor) <> '')
                                             AND bpi.tahun_pajak = ". $tahun ."
                                             AND bpi.masa_pajak  = ". $masa ."
                                             AND bpi.is_taken    = 0
                                             AND bpi.tarif       = CASE ". $isvalid ."
                                                                      WHEN 1 THEN 15
                                                                      WHEN 0 THEN 30
                                                                      WHEN 2 THEN bpi.tarif
                                                                      ELSE bpi.tarif
                                          		                   END
		                                  LIMIT ". $limit);

		return json_encode($listImbalUserLimit);
	}
	
	public function listImbalUserAll ($masa, $tahun) {
		$listImbalUserAll = DB::select("SELECT  bpi.Investor_id
                                               ,bpi.nama_wp
											   ,bpi.nama_investor
                                               ,UPPER(bpi.jenis_pajak) jenis_pajak
                                               ,bpi.masa_pajak
                                               ,bpi.tahun_pajak
                                               ,bpi.tarif
                                               ,bpi.no_npwp_investor
											   ,bpi.no_ktp_investor
                                               ,bpi.imbal_payout
                                               ,bpi.potongan_pajak
                                               ,bpi.alamat_investor
                                               ,bpi.phone_investor
                                               ,bpi.email
                                               ,bpi.tgl_lahir_investor
                                               ,bpi.tempat_lahir_investor
                                               ,bpi.no_passpor_investor
                                               ,bpi.warganegara
                                               ,bpi.is_taken
											   ,bpi.tgl_pemotongan
                                          FROM bukti_potong_investor bpi
                                         WHERE 1=1
                                           AND bpi.tahun_pajak = ". $tahun ."
                                           AND bpi.masa_pajak  = ". $masa ."
                                           AND bpi.is_taken    = 0");
        
		return json_encode($listImbalUserAll);
	}
	
	public function listImbalUserSingle ($masa, $tahun, $investor_id) {
		$listImbalUserSingle = DB::select("SELECT  bpi.Investor_id
                                                  ,bpi.nama_wp
												  ,bpi.nama_investor
                                                  ,UPPER(bpi.jenis_pajak) jenis_pajak
                                                  ,bpi.masa_pajak
                                                  ,bpi.tahun_pajak
                                                  ,bpi.tarif
                                                  ,bpi.no_npwp_investor
												  ,bpi.no_ktp_investor
                                                  ,bpi.imbal_payout
                                                  ,bpi.potongan_pajak
                                                  ,bpi.alamat_investor
                                                  ,bpi.phone_investor
                                                  ,bpi.email
                                                  ,bpi.tgl_lahir_investor
                                                  ,bpi.tempat_lahir_investor
                                                  ,bpi.no_passpor_investor
                                                  ,bpi.warganegara
                                                  ,bpi.is_taken
												  ,bpi.tgl_pemotongan
                                             FROM bukti_potong_investor bpi
                                            WHERE 1=1
                                              AND bpi.tahun_pajak = ". $tahun ."
                                              AND bpi.masa_pajak  = ". $masa ."
                                              AND bpi.Investor_id = ". $investor_id);

		return json_encode($listImbalUserSingle);
	}
	
	public function imbalIsTakenResponse(Request $request)
    {
		$investor_id       = $request->input('investor_id');
		$masa_pajak        = $request->input('masa_pajak');
		$tahun_pajak       = $request->input('tahun_pajak');
		$response_message  = $request->input('response_message');
		
		$now = date("Y-m-d H:i:s");
		
		DB::table('bukti_potong_investor')
                ->where('investor_id', $investor_id)
                ->where('masa_pajak', $masa_pajak)
                ->where('tahun_pajak', $tahun_pajak)
                ->update(['is_taken'   => 1]);
        
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function infoBuktiPotongResponse(Request $request)
    {
		$investor_id       = $request->input('investor_id');
		$no_bukti_potong   = $request->input('no_bukti_potong');
		$response_message  = $request->input('response_message');
		
		$now = date("Y-m-d H:i:s");
		
		DB::table('bukti_potong_investor')
                ->where('investor_id', $investor_id)
                ->where('masa_pajak', $masa_pajak)
                ->where('tahun_pajak', $tahun_pajak)
				->where('is_taken', 0)
                ->update(['no_bukti_potong' => $no_bukti_potong]);
        
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function countImbalUserAll ($masa, $tahun) {
		$countImbalUserAll = DB::select("SELECT COUNT(bpi.Investor_id) all_count
                                           FROM bukti_potong_investor bpi
                                          WHERE 1=1
                                            AND bpi.tahun_pajak = ". $tahun ."
                                            AND bpi.masa_pajak  = ". $masa ."
                                            AND bpi.is_taken    = 0");
        
		return json_encode($countImbalUserAll);
	}
	
	public function imbalIsTakenAllResponse(Request $request)
    {
		$masa_pajak        = $request->input('masa_pajak');
		$tahun_pajak       = $request->input('tahun_pajak');
		$response_message  = $request->input('response_message');
		
		$now = date("Y-m-d H:i:s");
		
		DB::table('bukti_potong_investor')
                ->where('masa_pajak', $masa_pajak)
                ->where('tahun_pajak', $tahun_pajak)
				->where('is_taken', 0)
                ->update(['is_taken'   => 1]);
        
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function cekRekeningSingle ($investor_id) {
        $cekRekening = DB::select("SELECT  investor_id
                                          ,TRIM(bank_investor) bank_investor
                                          ,bank
                                          ,TRIM(rekening) no_rekening
                                          ,nama_pemilik_rek
                                          ,is_valid_rekening
                                          ,tgl_validasi_rekening
                                          ,pemilik_rekening_asli
                                          ,is_verified_rekening
                                          ,tipe_pengguna
                                          ,nama_investor
                                     FROM detil_investor
                                    WHERE (rekening IS NOT NULL OR TRIM(rekening) != '' )
                                      AND LENGTH(rekening) > 7
                                      AND investor_id = ". $investor_id);
	    
		return json_encode($cekRekening);
	}
	
	public function cekRekeningLimit ($limit) {
        $cekRekening = DB::select("SELECT  investor_id
                                          ,TRIM(bank_investor) bank_investor
                                          ,bank
                                          ,TRIM(rekening) no_rekening
                                          ,nama_pemilik_rek
                                          ,is_valid_rekening
                                          ,tgl_validasi_rekening
                                          ,pemilik_rekening_asli
                                          ,is_verified_rekening
                                          ,tipe_pengguna
                                          ,nama_investor
                                     FROM detil_investor
                                    WHERE (rekening IS NOT NULL OR TRIM(rekening) != '' )
                                      AND LENGTH(rekening) > 7
                                   LIMIT ". $limit);
	    
		return json_encode($cekRekening);
	}
	
	public function cekRekeningResponse(Request $request)
    {
		$investor_id           = $request->input('investor_id');
		$no_rekening           = $request->input('no_rekening');
		$is_valid_rekening     = $request->input('is_valid_rekening');
		$tgl_validasi_rekening = $request->input('tgl_validasi_rekening');
		$bank_id               = $request->input('bank_id');
		$bank_investor         = $request->input('bank_investor');
		
		$status                = $request->input('status');
		$bank_name             = $request->input('bank_name');
		$account_num           = $request->input('account_num');
		$is_verified           = $request->input('is_verified');
		$is_dispute            = $request->input('is_dispute');
		$is_rejected           = $request->input('is_rejected');
		$tipe_pengguna         = $request->input('tipe_pengguna');
		$tipe_user             = $request->input('tipe_user');
		$response_message      = $request->input('response_message');
		
		//syslog(0,"req=".json_encode($request));
		//syslog(0,"investor_id=".$investor_id);
		
		$now = date("Y-m-d H:i:s");
		
		$is_exists = DB::select(DB::raw("SELECT COUNT(id) as cnt
		                                   FROM cek_rekening
								          WHERE tipe_user = 'L'
										    AND id        = ". $investor_id));
		
		DB::table('detil_investor')
                ->where('investor_id', $investor_id)
                ->update(['is_verified_rekening'     => $is_valid_rekening,
				          'tgl_verified_rekening' => $tgl_validasi_rekening]);
        
		if ($is_exists[0]->cnt == 0) {
		   $insertLog = DB::table("cek_rekening")->insert(array(
                    "id"               => $investor_id,
					"status"           => $status,
					"bank_id"          => $bank_id,
                    "bank_name"        => $bank_name,
                    "account_num"      => $account_num,
					"is_verified"      => $is_verified,
					"is_dispute"       => $is_dispute,
                    "is_rejected"      => $is_rejected,
					"tipe_pengguna"    => $tipe_pengguna,
					"tipe_user"        => $tipe_user,
					"response_message" => $response_message
                ));
		} else {
			DB::table('cek_rekening')
                ->where('id', $investor_id)
				->where('tipe_user', 'L')
                ->update(['status'           => $status,
				          'bank_id'          => $bank_id,
						  'bank_name'        => $bank_name,
						  'account_num'      => $account_num,
						  'is_verified'      => $is_verified,
						  'is_dispute'       => $is_dispute,
						  'is_rejected'      => $is_rejected,
						  'tipe_pengguna'    => $tipe_pengguna,
						  'tipe_user'        => $tipe_user,
						  'response_message' => $response_message,
						  'Action_date'      => $now
				]);
		}
		
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function validasiRekening ($investor_id, $tipe_user) {
		$validasiRekening = DB::select("SELECT  id
                                               ,status
                                               ,bank_id
                                               ,bank_name
                                               ,account_num
                                               ,is_verified
                                               ,is_dispute
                                               ,is_rejected
                                               ,tipe_pengguna
                                               ,tipe_user
                                               ,response_message
                                               ,Action_date
                                          FROM cek_rekening
                                         WHERE id        = ". $investor_id ."
                                           AND tipe_user = '". $tipe_user ."'");
	    
		return json_encode($validasiRekening);
	}
	
	// ichal_sl, Jul 07, 2022
	// function for Inquiry Rekening through BNI P2PL Web Service
	// LENDER
	public function inqRekeningSingleL ($investor_id) {
        $inqRekening = DB::select("SELECT  investor_id
                                          ,TRIM(bank_investor) bank_number
                                          ,bank                bank_name
                                          ,TRIM(rekening)      no_rekening
                                          ,nama_pemilik_rek
                                          ,is_valid_rekening
                                          ,tgl_validasi_rekening
                                          ,pemilik_rekening_asli
                                          ,tipe_pengguna
                                          ,nama_investor
                                     FROM detil_investor
                                    WHERE investor_id          = ". $investor_id);
	    
		return json_encode($inqRekening);
	}
	
	public function inqRekeningLimitL ($limit) {                           
        $inqRekening = DB::select("SELECT investor_id
                ,TRIM(bank_investor) bank_number
                ,bank                bank_name
                ,TRIM(rekening) no_rekening
                ,nama_pemilik_rek
                ,is_valid_rekening
                ,tgl_validasi_rekening
                ,pemilik_rekening_asli 
                ,tipe_pengguna
                ,nama_investor
                FROM detil_investor
                WHERE investor_id IN (SELECT investor_id FROM rekening_investor WHERE total_dana > 999000)
                AND (is_valid_rekening IS NULL OR TRIM(is_valid_rekening) = '')
                                   LIMIT ". $limit);
	    
		return json_encode($inqRekening);
	}
	
	public function inqRekeningResponseL(Request $request)
    {
		$investor_id             = $request->input('investor_id');
		$tipe_user               = $request->input('tipe_user');
		$tipe_pengguna           = $request->input('tipe_pengguna');
		$bank_number             = $request->input('bank_number');
		$bank_account_number     = $request->input('bank_account_number');
		$bank_account_name       = $request->input('bank_account_name');
		//$is_inquiry_rekening     = $request->input('is_inquiry_rekening');
		//$tgl_inquiry_rekening    = $request->input('tgl_inquiry_rekening');
		$customer_account_number = $request->input('customer_account_number');
		$customer_account_name   = $request->input('customer_account_name');
		$beneficiary_bank_name   = $request->input('beneficiary_bank_name');
		$response_code           = $request->input('response_code');
		$response_message        = $request->input('response_message');
		$account_type            = $request->input('account_type');
		$currency                = $request->input('currency');
		$account_status          = $request->input('account_status');
		$is_valid                = $request->input('is_valid');
		$http_status             = $request->input('http_status');
		
		//syslog(0,"req=".json_encode($request));
		//syslog(0,"investor_id=".$investor_id);
		// inquiry_rekening
		
		$now = date("Y-m-d H:i:s");
		
		$is_exists = DB::select(DB::raw("SELECT COUNT(id) as cnt
		                                   FROM validasi_rekening
								          WHERE tipe_user = 'L'
										    AND id        = ". $investor_id));
		
		DB::table('detil_investor')
                ->where('investor_id', $investor_id)
                ->update(['is_valid_rekening'  => $is_valid,   //$is_inquiry_rekening,
				          'tgl_validasi_rekening' => $now, //$tgl_inquiry_rekening
						  'pemilik_rekening_asli' => $customer_account_name
						 ]);
        
		if ($is_exists[0]->cnt == 0) {
		   $insertLog = DB::table("validasi_rekening")->insert(array(
					"id"                      => $investor_id,
					"tipe_user"               => 'L',
					"tipe_pengguna"           => $tipe_pengguna,
					"bank_number"             => $bank_number,
					"bank_account_number"     => $bank_account_number,
					"bank_account_name"       => $bank_account_name,
					"customer_account_number" => $customer_account_number,
					"customer_account_name"   => $customer_account_name,
					"beneficiary_bank_name"   => $beneficiary_bank_name,
					"response_code"           => $response_code,
					"response_message"        => $response_message,
					"account_type"            => $account_type,
					"currency"                => $currency,
					"account_status"          => $account_status,
					"is_valid"                => $is_valid,
					"http_status"             => $http_status,
					"action_date"             => $now
                ));
		} else {
			DB::table('validasi_rekening')
                ->where('id', $investor_id)
				->where('tipe_user', 'L')
                ->update(['tipe_pengguna'           => $tipe_pengguna,
				          'bank_number'             => $bank_number,
				          'bank_account_number'     => $bank_account_number,
				          'bank_account_name'       => $bank_account_name,
				          'customer_account_number' => $customer_account_number,
				          'customer_account_name'   => $customer_account_name,
				          'beneficiary_bank_name'   => $beneficiary_bank_name,
				          'response_code'           => $response_code,
				          'response_message'        => $response_message,
				          'account_type'            => $account_type,
				          'currency'                => $currency,
				          'account_status'          => $account_status,
				          'is_valid'                => $is_valid,
				          'http_status'             => $http_status,
				          'action_date'             => $now
				]);
		}
		
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function inqRekeningL ($investor_id, $tipe_user) {
		$inqRekening = DB::select("SELECT  id
                                          ,rekening_id
                                          ,tipe_pengguna
                                          ,tipe_user
                                          ,bank_number
                                          ,bank_account_number
                                          ,bank_account_name
                                          ,customer_account_number
                                          ,customer_account_name
                                          ,beneficiary_bank_name
                                          ,response_code
                                          ,response_message
                                          ,account_type
                                          ,currency
                                          ,account_status
                                          ,is_valid
                                          ,http_status
                                          ,action_date
                                     FROM validasi_rekening
                                    WHERE id        = ". $investor_id ."
                                      AND tipe_user = '". $tipe_user ."'");
	    
		return json_encode($inqRekening);
	}
	
	// ichal_sl, Jul 07, 2022
	// function for Inquiry Rekening through BNI P2PL Web Service
	// BORROWER
	public function inqRekeningSingleB ($brw_id) {
        $inqRekening = DB::select("SELECT  bud.brw_id
                                          ,br.rekening_id
                                          ,br.brw_kd_bank  bank_number
                                          ,br.brw_norek
                                          ,br.brw_nm_pemilik
                                          ,br.is_valid_rekening
                                          ,br.tgl_validasi_rekening
                                          ,bud.nama
                                          ,bud.brw_type
                                     FROM  brw_user_detail bud
                                          ,brw_rekening    br
                                    WHERE bud.brw_id            = br.brw_id
                                      AND TRIM(br.brw_kd_bank) != ''
                                      AND TRIM(br.brw_norek)   != ''
									  AND LENGTH(br.brw_norek)  > 7
                                      AND bud.brw_id            = ". $brw_id);
	    
		return json_encode($inqRekening);
	}
	
	public function inqRekeningLimitB ($limit) {
        $inqRekening = DB::select("SELECT  bud.brw_id
                                          ,br.rekening_id
                                          ,br.brw_kd_bank  bank_number
                                          ,br.brw_norek
                                          ,br.brw_nm_pemilik
                                          ,br.is_valid_rekening
                                          ,br.tgl_validasi_rekening
                                          ,bud.nama
                                          ,bud.brw_type
                                     FROM  brw_user_detail bud
                                          ,brw_rekening    br
                                    WHERE bud.brw_id            = br.brw_id
                                      AND TRIM(br.brw_kd_bank) != ''
                                      AND TRIM(br.brw_norek)   != ''
									  AND LENGTH(br.brw_norek)  > 7
									  AND (is_valid_rekening IS NULL OR TRIM(is_valid_rekening) = '')
                                   LIMIT ". $limit);
	    
		return json_encode($inqRekening);
	}
	
	public function inqRekeningResponseB(Request $request)
    {
		$brw_id                  = $request->input('brw_id');
		$rekening_id             = $request->input('rekening_id');
		$bank_account_number     = $request->input('bank_account_number');
		$bank_account_name       = $request->input('bank_account_name');
		//$is_inquiry_rekening     = $request->input('is_inquiry_rekening');
		//$tgl_inquiry_rekening    = $request->input('tgl_inquiry_rekening');
		$customer_account_number = $request->input('customer_account_number');
		$customer_account_name   = $request->input('customer_account_name');
		$beneficiary_bank_name   = $request->input('beneficiary_bank_name');
		$response_code           = $request->input('response_code');
		$response_message        = $request->input('response_message');
		$account_type            = $request->input('account_type');
		$currency                = $request->input('currency');
		$account_status          = $request->input('account_status');
		$is_valid                = $request->input('is_valid');
		$http_status             = $request->input('http_status');
		
		//syslog(0,"req=".json_encode($request));
		//syslog(0,"brw_id=".$brw_id);
		
		$now = date("Y-m-d H:i:s");
		
		$is_exists = DB::select(DB::raw("SELECT COUNT(id) as cnt
		                                   FROM validasi_rekening
								          WHERE tipe_user   = 'B'
										    AND id          = ". $brw_id ."
											AND rekening_id = ". $rekening_id ));
		
		DB::table('brw_rekening')
                ->where('brw_id', $brw_id)
				->where('rekening_id', $rekening_id)
                ->update(['is_valid_rekening'  => $is_valid,   //$is_inquiry_rekening,
				          'tgl_validasi_rekening' => $now //$tgl_inquiry_rekening
						 ]);
        
		if ($is_exists[0]->cnt == 0) {
		   $insertLog = DB::table("validasi_rekening")->insert(array(
					"id"                      => $brw_id,
					"rekening_id"             => $rekening_id,
					"tipe_user"               => 'B',
					"tipe_pengguna"           => $tipe_pengguna,
					"bank_number"             => $bank_number,
					"bank_account_number"     => $bank_account_number,
					"bank_account_name"       => $bank_account_name,
					"customer_account_number" => $customer_account_number,
					"customer_account_name"   => $customer_account_name,
					"beneficiary_bank_name"   => $beneficiary_bank_name,
					"response_code"           => $response_code,
					"response_message"        => $response_message,
					"account_type"            => $account_type,
					"currency"                => $currency,
					"account_status"          => $account_status,
					"is_valid"                => $is_valid,
					"http_status"             => $http_status,
					"action_date"             => $now
                ));
		} else {
			DB::table('validasi_rekening')
                ->where('id', $brw_id)
                ->where('rekening_id', $rekening_id)
				->where('tipe_user', 'B')
                ->update(['tipe_pengguna'           => $tipe_pengguna,
				          'bank_number'             => $bank_number,
				          'bank_account_number'     => $bank_account_number,
				          'bank_account_name'       => $bank_account_name,
				          'customer_account_number' => $customer_account_number,
				          'customer_account_name'   => $customer_account_name,
				          'beneficiary_bank_name'   => $beneficiary_bank_name,
				          'response_code'           => $response_code,
				          'response_message'        => $response_message,
				          'account_type'            => $account_type,
				          'currency'                => $currency,
				          'account_status'          => $account_status,
				          'is_valid'                => $is_valid,
				          'http_status'             => $http_status,
				          'action_date'             => $now
				]);
		}
		
		return response()->json([
            'ResponseCode' => '001',
            'ResponseDescription' => 'Success'
        ]);
	}
	
	public function inqRekeningB ($brw_id, $tipe_user) {
		$inqRekening = DB::select("SELECT  id
                                          ,rekening_id
                                          ,tipe_pengguna
                                          ,tipe_user
                                          ,bank_number
                                          ,bank_account_number
                                          ,bank_account_name
                                          ,customer_account_number
                                          ,customer_account_name
                                          ,beneficiary_bank_name
                                          ,response_code
                                          ,response_message
                                          ,account_type
                                          ,currency
                                          ,account_status
                                          ,is_valid
                                          ,http_status
                                          ,action_date
                                     FROM validasi_rekening
                                    WHERE id        = ". $brw_id ."
                                      AND tipe_user = '". $tipe_user ."'");
	    
		return json_encode($inqRekening);
	}

}
