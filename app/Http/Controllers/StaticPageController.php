<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Borrower;
use App\Message;
use App\Jobs\ProcessEmail;
use App\Jobs\ProcessEmailBorrower;
use App\Investor;
use App\Proyek;
use App\ManagePenghargaan;
use App\ManageKhazanah;
use App\News;
use App\TestimoniPendana;
use App\Mail\EmailAktifasiPendana;
use App\Mail\EmailAktifasiPenerimaPendanaan;
use Mail;
use DB;

class StaticPageController extends Controller
{

    protected $redirectTomail = '/resendMailborrower';
    // fungsi menampilkan halaman khazanah
    public function khazanah()
    {
        $data = ManageKhazanah::all();

        return view('pages.guest.khazanah', compact('data'));
    }
    // fungsi menampilkan halaman timkami
    public function timKami()
    {
        $proyeks = ManagePenghargaan::all();
        return view('pages.guest.tim_kami', compact('proyeks'));
    }
    //fungsi menampilkan halaman kontak
    public function kontak()
    {
        $address= DB::table('cms')->where('type',3)->first();
        return view('pages.guest.kontak', compact('address'));
    }
    public function kalkulator()
    {
        return view('pages.guest.kalkulator');
    }


    public function kalkulator_kpr()
    {
        return view('pages.guest.kalkulator_kpr');
    }

    public function resultKalkulatorKpr(Request $request)
    {

        $nilai_pengajuan = $request->nilai_pengajuan;
        $tenor = $request->tenor;
        $tipe_pendanaan = $request->tipe_pendanaan;
        $returnHTML = "";

        try {

            if ($tipe_pendanaan == '2') {
                $cicilanPerBulan =  ($nilai_pengajuan + ($nilai_pengajuan * 0.12)) / $tenor;
                $returnHTML .= "  <thead>
                                    <tr>
                                        <th>Bulan ke-</th>
                                        <th>Nominal per Bulan</th>
                                    </tr>
                                </thead> ";
                $returnHTML .= "<tbody><tr><td>1-" . $tenor . " </td>";
                $returnHTML .= '<td>Rp.' . number_format($cicilanPerBulan, 0, '.', '.') . '</td>';
                $returnHTML .= "</tr>";

                /*$returnHTML .= "<tr>
                                <td>Total </td>
                                <td>Rp." .   number_format(($cicilanPerBulan * $tenor), 0, '.', '.') . "</td>
                            </tr></tbody>";*/

                return $returnHTML;
            } else {

                // $data = \DB::select("SELECT get_simulation_kpr($nilai_pengajuan, $tenor, 'StaticPageController.php', 82) as result");
                $tenorBulan = $tenor * 12;
                // $margin_efektif = \DB::select("SELECT margin_efektif FROM m_param_kalkulator WHERE tenor = 999")[0]->margin_efektif;
                $margin_efektif = floatval($request->margin);
                $data = \DB::select("SELECT get_new_simulation_kpr($nilai_pengajuan, $tenorBulan,$margin_efektif, 'StaticPageController.php', 87) as result");

                $resultSimulasi = $data[0]->result;

                if ($resultSimulasi) {
                    $dataShowResult = [];

                    $dataRow = explode("#", $resultSimulasi);
                    $dataLastRow = array_pop($dataRow);
                    $totalAllBayar = str_replace('^', '', $dataLastRow);

                    $returnHTML .= "  <thead>
                        <tr>
                            <th>Bulan ke </th>
                            <th>Total Angsuran</th>
                            <th>Angsuran Pokok</th>
                            <th>Angsuran Margin</th>
                            <th>Sisa Pokok</th>
                         </tr>
                       </thead>";
                    foreach ($dataRow as $key => $val) {
                        $dataColumn = explode(";", $val);
                        // $dataShowResult[$dataColumn[0]] = array('Angsuran Pokok' => $dataColumn[3], 'Angsuran Margin' => $dataColumn[4], 'Total Angsuran' => $dataColumn[1]);

                        // foreach ($val as $key2 => $val2) {
                            $returnHTML .= "<tr>";
                            $returnHTML .= "<td align='center'>" . $dataColumn[0] . "</td>";
                            $returnHTML .= "<td>Rp" . number_format($dataColumn[1], 0, '.', '.') . "</td>";
                            $returnHTML .= "<td>Rp" . number_format($dataColumn[3], 2, ',', '.') . "</td>";
                            $returnHTML .= "<td>Rp" . number_format($dataColumn[4], 0, '.', '.') . "</td>";
                            $returnHTML .= "<td>Rp" . number_format($dataColumn[5], 0, '.', '.') . "</td>";
                            $returnHTML .= "</tr>";

                            $end0 = $dataColumn[0] + 1;
                            $end1 = $dataColumn[1];
                            $end3 = $dataColumn[3];
                            $end5 = $dataColumn[5];
                            $end4 = $end5-$end3;
                        // }
                    }
                            $returnHTML .= "<tr>";
                            $returnHTML .= "<td align='center'>" . $end0 . "</td>";
                            $returnHTML .= "<td>Rp" . number_format($end1, 0, '.', '.') . "</td>";
                            $returnHTML .= "<td>Rp" . number_format($end5, 2, ',', '.') . "</td>";
                            $returnHTML .= "<td>Rp" . number_format($end4, 0, '.', '.') . "</td>";
                            $returnHTML .= "<td align='center'> - </td>";
                            $returnHTML .= "</tr>";
                    // LastRow of tabel angsuran

                    // die();

                       
// ^ array:6 [▼
// 0 => "1"
// 1 => "9734040"
// 2 => "9734040"
// 3 => "2806951.5"
// 4 => "6927080"
// 5 => "662193000"
                    // foreach ($dataShowResult as $key1 => $val1) {
                    //     $returnHTML .= "  <thead>
                    //     <tr>
                    //         <th>Bulan ke </th>
                    //         <th>" . $key1 . "</th>
                    //      </tr>
                    //    </thead>";
                        // foreach ($val1 as $key2 => $val2) {
                        //     $returnHTML .= "<tr>";
                        //     $returnHTML .= "<td>" . $key2 . "</td>";
                        //     $returnHTML .= "<td>Rp. " . number_format($val2, 0, '.', '.') . "</td>";
                        //     $returnHTML .= "</tr>";
                        // }
                    // }

                    /*$returnHTML .= "  
                    <tr><td></td></tr>
                    <tr>
                        <td><b>TOTAL</b> </td>
                        <td><b>Rp." .   number_format($totalAllBayar, 0, '.', '.') . "</b></td>
                    </tr>";*/

                    $returnHTML .= "</tbody>";
                    return $returnHTML;



                } else {
                    $returnHTML .= "  <tbody>
                            <tr>
                                <td>Tidak ada hasil</td>
                                <td>0</td>
                            </tr>
                        </thead> ";

                    return $returnHTML;
                }
            }
        } catch (\Exception $e) {
            return response(['status_code' => 500, 'errors' => 'Error. Please contact the administrator of website'], 500);
        }
    }
    public function get_data_kal($id)
    {

        $proyek = Proyek::where('status', 1)->get();
        // echo $proyek;die;
        $response = ['datakal' => $proyek];

        return response()->json($response);
    }
    public function tataCaraPendana()
    {
        return view('pages.guest.tatacara_pendana');
    }
    public function tataCaraPenerima()
    {
        return view('pages.guest.tatacara_penerima');
    }
    public function tataCaraPengaduan()
    {
        return view('pages.guest.pengaduan');
    }
    public function modalusahaProperty()
    {
        return view('pages.guest.modal_usaha_property');
    }
    public function faq()
    {
        return view('pages.guest.faq');
    }
    public function forgotpassword()
    {
        return view('pages.guest.forgotpassword');
    }
    public function message(Request $request)
    {

        $message = new Message;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->number = $request->phone;
        $message->message = $request->message;

        $message->save();


        return view('pages.guest.kontak')->with('status', 'Pesan sudah terkirim, silahkan tunggu balasan dari Dana Syariah Indonesia');
    }

    public function termcondition()
    {
        return view('pages.guest.termcondition');
    }

    public function privacypolicy()
    {
        return view('pages.guest.privacypolicy');
    }

    public function resendMailPost(Request $req)
    {
        $email = $req->email;

        if ($email == null) {
            return view('errors.email-verif-false');
        }

        $user = Investor::where('email', $email)->first();
        if (empty($user->email_verif)) {
            return view('errors.email-verif-false');
        }

        $isiEmail = new EmailAktifasiPendana($user);
        Mail::to($email)->send($isiEmail);

        return redirect('/resendMail')->with('email', $email);
    }

    public function resendMailPostborrower(Request $req)
    {

        $email = $req->email;
        // $user = Borrower::where('email', $email)->first();
        // $isiEmail = new EmailAktifasiPenerimaPendanaan($user->username,$user->email,$user->email_verif);
        // Mail::to($email)->send($isiEmail);

        // return redirect('/resendMailborrower')->with('email',$email);


        if ($email == null) {
            return view('errors.email-verif-false');
        }

        $user = Borrower::where('email', $email)->first();
        if (empty($user->email_verif)) {
            return view('errors.email-verif-false');
        }

        $isiEmail = new EmailAktifasiPenerimaPendanaan($user);
        Mail::to($email)->send($isiEmail);

        return redirect($this->redirectTomail)->with('reg_sukses', 'Pendaftaran Sukses, Cek Email step selanjutnya')
            ->with('nama', $user->username)
            ->with('email', $user->email)
            ->with('email_verif', $user->email_verif);
    }

    public function all_news()
    {

        $all_news = News::orderBy('id', 'desc')->paginate(6);

        return view('pages.guest.news', ['all_news' => $all_news]);
    }

    public function news_detil($id)
    {

        $news_detil = News::where('id', $id)->first();

        $news_others = News::whereNotIn('id', [$id])->orderBy('id', 'desc')->limit(3)->get();

        return view('pages.guest.news_detail', ['news_detil' => $news_detil, 'news_others' => $news_others]);
    }
}
