<?php
namespace App\Http\Controllers\Helpdesk;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use JWTFactory;
use Hash;
use Carbon\Carbon;
use Mail;
use Image;
use GuzzleHttp\Client;
use DB;
use Exception;
use Storage;

use App\Jobs\ProcessEmail;
use App\Jobs\InvestorVerif;
use App\Mail\EmailAktifasiKpr;

use App\LoginBorrower;
use App\BorrowerDetails;
use App\AhliWarisInvestor;
use App\TermCondition;

use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;

use App\Token;

class HelpdeskAuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('auth:borrower-api-mobile', ['except' => ['login',   'getProvinsi' ]]);
    }

               

    public function checkPhoneNumber(Request $request){
        
        if(empty($request->no_hp)){
            return response()->json(['success'=> 'Nomer Telpon Belum Pernah Terdaftar']);
        }
        else
            if (BorrowerDetails::whereIn('no_tlp', ['62'.$request->no_hp, '0'.$request->no_hp])->first()) {
                return response()->json(['error'=> 'Nomer Telpon Sudah Pernah Terdaftar']);
            }
            else
            return response()->json(['success'=> 'Nomer Telpon Belum Pernah Terdaftar']);
    }

    public function check_KTP(Request $request){
        if (BorrowerDetails::where('ktp', $request->no_ktp)->first()) {
            return response()->json(['error'=> 'Nomor KTP Sudah Pernah Terdaftar']);
        }else return response()->json(['success'=> 'Nomer KTP Belum Pernah Terdaftar']);
    }

    public function checkNpwp(Request $request){
        if (BorrowerDetails::where('npwp', $request->no_npwp)->first()) {
            return response()->json(['error'=> 'Nomor NPWP Sudah Pernah Terdaftar']);
        }else return response()->json(['success'=> 'Nomer NPWP Belum Pernah Terdaftar']);
    }
   

    public function getProvinsi(){

        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }

        else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        return [
            'data_provinsi' => $data_master_provinsi
        ];
    }

    public function getKota($provinsi){

        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')->where('Provinsi', $provinsi)->groupBy('Kota')->get();

        $x=0;
        if (!isset($getKota[0])) {
            $getKota = null;
        }
        
        else{
                foreach ($getKota as $item){
                $data_master_kota[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kota'=>$item->Kota,
                ];
                $x++;
            }
        }

        return [
            'data_master_kota' => $data_master_kota
        ];
    }

    public function getKecamatan($kota){

        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'kecamatan')->where('Kota', $kota)->groupBy('kecamatan')->get();

        $x=0;
        if (!isset($getKecamatan[0])) {
            $getKecamatan = null;
        }
        
        else{
                foreach ($getKecamatan as $item){
                $data_master_kecamatan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kecamatan'=>$item->kecamatan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kecamatan' => $data_master_kecamatan
        ];
    }

    public function getKelurahan($kecamatan){

        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'kelurahan')->where('kecamatan', $kecamatan)->groupBy('kelurahan')->get();

        $x=0;
        if (!isset($getKelurahan[0])) {
            $getKelurahan = null;
        }
        
        else{
                foreach ($getKelurahan as $item){
                $data_master_kelurahan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kelurahan'=>$item->kelurahan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kelurahan' => $data_master_kelurahan
        ];
    }

    public function getKodePos($kelurahan, $kecamatan){

        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')->where('kelurahan', $kelurahan)->where('kecamatan', $kecamatan)->first();

        if($getKodePos){
            return [
                'kode_pos' => $getKodePos->kode_pos
            ];
        }else{
            return [
                'kode_pos' => ''
            ];
        }
    }

    public function getProvinsiPendanaan(){

        $data_prov = [31, 32, 36];
        $getProv = DB::table('m_kode_pos')->select('id_kode_pos', 'Provinsi')->whereIn('kode_provinsi', $data_prov)->groupBy('provinsi')->get();

        $x=0;
        if (!isset($getProv[0])) {
            $getProv = null;
        }

        else{
                foreach ($getProv as $item){
                $data_master_provinsi[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'provinsi'=>$item->Provinsi,
                ];
                $x++;
            }
        }

        return [
            'data_provinsi' => $data_master_provinsi
        ];
    }

    public function getKotaPendanaan($provinsi){


        // $kota_banten = ['Tangerang', 'Tangerang Selatan'];
        // $kota_jakarta = ['Kepulauan Seribu'];
        // $kota_jawa_barat = ['Bandung', 'Bandung Barat', 'Bogor', 'Bekasi', 'Depok'];

        $getKota = DB::table('m_kode_pos')->select('id_kode_pos', 'Kota')->where('Provinsi', $provinsi)->groupBy('Kota')->get();

        // if($provinsi == 'Banten'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereIn('Kota', $kota_banten)->groupBy('Kota')->get();
        // }else if($provinsi == 'DKI Jakarta'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereNotIn('Kota', $kota_jakarta)->groupBy('Kota')->get();
        // }else if($provinsi == 'Jawa Barat'){
        //     $getKota = $getKota->where('Provinsi', $provinsi)->whereIn('Kota', $kota_jawa_barat)->groupBy('Kota')->get();
        // }else{
        //     $getKota = $getKota->where('Provinsi', $provinsi)->groupBy('Kota')->get();
        // }

        $x=0;
        if (!isset($getKota[0])) {
            $getKota = null;
        }
        
        else{
                foreach ($getKota as $item){
                $data_master_kota[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kota'=>$item->Kota,
                ];
                $x++;
            }
        }

        return [
            'data_master_kota' => $data_master_kota
        ];
    }

    public function getKecamatanPendanaan($kota){

        $getKecamatan = DB::table('m_kode_pos')->select('id_kode_pos', 'kecamatan')->where('Kota', $kota)->groupBy('kecamatan')->get();

        $x=0;
        if (!isset($getKecamatan[0])) {
            $getKecamatan = null;
        }
        
        else{
                foreach ($getKecamatan as $item){
                $data_master_kecamatan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kecamatan'=>$item->kecamatan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kecamatan' => $data_master_kecamatan
        ];
    }

    public function getKelurahanPendanaan($kecamatan){

        $getKelurahan = DB::table('m_kode_pos')->select('id_kode_pos', 'kelurahan')->where('kecamatan', $kecamatan)->groupBy('kelurahan')->get();

        $x=0;
        if (!isset($getKelurahan[0])) {
            $getKelurahan = null;
        }
        
        else{
                foreach ($getKelurahan as $item){
                $data_master_kelurahan[$x] = [
                    'id_kode_pos'=>$item->id_kode_pos,
                    'kelurahan'=>$item->kelurahan,
                ];
                $x++;
            }
        }

        return [
            'data_master_kelurahan' => $data_master_kelurahan
        ];
    }

    public function getKodePosPendanaan($kelurahan, $kecamatan){

        $getKodePos = DB::table('m_kode_pos')->select('id_kode_pos', 'kode_pos')->where('kelurahan', $kelurahan)->where('kecamatan', $kecamatan)->first();

        if($getKodePos){
            return [
                'kode_pos' => $getKodePos->kode_pos
            ];
        }else{
            return [
                'kode_pos' => ''
            ];
        }
    }
 
 	 public function get_detil_investor_by_cellphone($cell_phone)
    {

		  syslog(0,"cell=".$cell_phone);
        $detil_investor = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
            ->leftJoin('marketer', 'marketer.ref_code', '=', 'investor.ref_number')
            ->leftJoin('detil_marketer', 'detil_marketer.marketer_id', '=', 'marketer.id')
            ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'investor.id')
            ->leftJoin('ahli_waris_investor', 'ahli_waris_investor.id_investor', '=', 'investor.id')
            ->where('detil_investor.phone_investor', $cell_phone)
            ->first([
                'detil_investor.*',
                'investor.keterangan as keteranganSuspend',
                'investor.suspended_by as namaSuspended',
                'investor.actived_by as namaActived',
                'investor.updated_at as tanggalSuspend',
                'marketer.ref_code',
                'detil_marketer.nama_lengkap as nama_marketer',
                'rekening_investor.va_number',
                'ahli_waris_investor.nama_ahli_waris as nama_ahli_waris',
                'ahli_waris_investor.hubungan_keluarga_ahli_waris as hub_ahli_waris',
                'ahli_waris_investor.nik_ahli_waris as nik_ahli_waris',
                'ahli_waris_investor.no_hp_ahli_waris as no_hp_ahli_waris',
                'ahli_waris_investor.alamat_ahli_waris as alamat_ahli_waris',

            ]);
        $id = $detil_investor['id'];
        syslog(0,"id=".json_encode($detil_investor));
        $getva = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE investor_id = ".$id);
        $va = array();
        if ($getva != NULL) {
            foreach ($getva as $item) {
                $index = "bank" . $item->kode_bank;
                $va[$index] = $item->va_number;
            }
        }

        $investor_pengurus = InvestorPengurus::where('investor_id', $id)->get();
        $response = ['detil_investor' => $detil_investor, 'investor_pengurus' => $investor_pengurus, 'va' => $va];

        return response()->json($response);
    }

}

