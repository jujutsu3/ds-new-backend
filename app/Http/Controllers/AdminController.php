<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admins;
use App\Investor;
use App\PendanaanAktif;
use App\Proyek;
use App\Marketer;
use App\MasterProvinsi;
use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;
use App\ManageCarousel;
use App\ECollBni;
use App\ManagePenghargaan;
use App\ManageKhazanah;
use App\TermCondition;
use App\Jobs\BroadcastEmail;
use App\News;
use App\BorrowerTipePendanaan;
use App\BorrowerDetails;
use App\BorrowerAhliWaris;
use App\BorrowerRekening;
use App\BorrowerPengurus;
use App\LogPengembalianDana;
use Carbon\Carbon;
use Storage;
use League\Csv\Reader;
use App\ManageRole;
use App\AdminMenuItem;
use App\ListFileFDC;
use App\RBAC;
use DB;
use App\Http\Middleware\NotifikasiProyek;
use Auth;
use App\Http\Middleware\StatusProyek;
use App\MobileVersion;
use App\TeksNotifikasi;
use App\ThresholdKontrak;
use App\BorrowerPendanaan;
use App\Http\Controllers\KirimEmailController;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\AuditTrail;
use App\TestimoniPendana;
use App\DokumenBorrower;
use App\RekeningInvestor;
use App\Http\Controllers\DMSController;
use App\Http\Controllers\RDLController;
use App\LogBankRDLTransaction;
use League\Csv\Statement;
use League\Csv\Writer;
use ZipArchive;
use App\Tindikator;
use Image;

use File;
use App\Http\Controllers\FDCController;

use Illuminate\Support\Facades\Validator;
use App\Mail\EmailPencairanDana;
use App\Mail\EmailPencairanDanaFinance;
use App\Mail\EmailKonfirmasiCicilan;
use App\Mail\EmailSPKOccasio;
use Mail;
use DateTime;
use App\Mail\EmailVerifikasiPenilaian;
use App\Mail\EmailVerifikasiPenilaianKeBorrower;
use App\Borrower;
use App\BorrowerInvoice;
use App\BorrowerBuktiPembayaran;
use App\BorrowerPembayaran;
use App\BorrowerLogPembayaran;
use App\BorrowerPengajuan;
use Excel;
use App\Exports\ReportImbalHasilBorrower;
use App\Helpers\Helper;
use App\Services\VerifikatorService;
use App\BorrowerDokumenLegalitas;
use App\BorrowerHdVerifikator;
use App\BorrowerDokumenCeklis;
use App\BorrowerDetailCeklistAnalisa;
use App\BorrowerAnalisaPendanaan;
use App\BorrowerFormInterviewHrd;
use App\BorrowerFormKunjunganDomisili;
use App\BorrowerAgunan;
use App\BorrowerPendanaanRumahLain;
use App\BorrowerPendanaanNonRumahLain;
use App\AdminPermintaanPerubahan;
use App\DetilInvestor;
use App\InvestorPengurus;
use App\AdminRiwayatInvestor;
use App\AdminRiwayatDetilInvestorIndividu;
use App\AdminRiwayatDetilInvestorBadanHukum;
use App\AdminRiwayatAhliWarisInvestor;
use App\AdminRiwayatNomorVa;
use App\AdminRiwayatPengurusInvestorBadanHukum;

use App\AdminRiwayatInvestorOri; 
use App\AdminRiwayatDetilInvestorIndividuOri; 
use App\AdminRiwayatDetilInvestorBadanHukumOri; 
use App\AdminRiwayatAhliWarisInvestorOri;
use App\AdminRiwayatNomorVaOri;
use App\AdminRiwayatPengurusInvestorBadanHukumOri;
use DataTables;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    private $data_json;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        // $this->middleware(NotifikasiProyek::class);
        $this->middleware(StatusProyek::class);
    }
    /**
     * show dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    // public function dashboard(){
    // $jumlah_investor = Investor::all()->count();
    // $total_dana = PendanaanAktif::where('status',1)->sum('total_dana');
    // $jumlah_marketer = Marketer::all()->count();
    // $jumlah_proyek = Proyek::all()->count();
    // $initial = Proyek::all()->sum('terkumpul');
    // $terkumpul = $initial + $total_dana;

    // return view('pages.admin.admin_dashboard')->with(compact('jumlah_investor', 'total_dana', 'jumlah_marketer', 'jumlah_proyek','terkumpul'));
    // }

    public function dashboard()
    {
        $jumlah_investor = Investor::all()->count();

        $data = Tindikator::orderby('id', 'DESC')->first();
        $jumlah_investor        = $data->jumlah_investor;
        $total_dana             = $data->dana_pinjaman;
        $terkumpul              = $data->dana_pinjaman_2;

        $jumlah_marketer = Marketer::all()->count();
        $jumlah_proyek = Proyek::all()->count();

        return view('pages.admin.admin_dashboard')->with(compact('jumlah_investor', 'total_dana', 'jumlah_marketer', 'jumlah_proyek', 'terkumpul'));
    }

    public function manageAdmin()
    {
        $admins = Admins::all();
        $role = ManageRole::all();
        $menuItem = AdminMenuItem::whereNotIn('id', [\DB::raw('select parent from admin_menu_items where parent != 0')])
            ->get();
        $cekUser = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')->where('admins.id', \Auth::id())->first(['roles.name', 'admins.id']);

        return view('pages.admin.admin_manage')->with(['admins' => $admins, 'role' => $role, 'menuItem' => $menuItem, 'cekUser' => $cekUser]);
    }

    public function add_investor()
    {
        $master_provinsi = MasterProvinsi::distinct('kode_provinsi', 'nama_provinsi')->get(['kode_provinsi', 'nama_provinsi']);
        $master_agama = MasterAgama::all();
        $master_asset = MasterAsset::all();
        $master_badan_hukum = MasterBadanHukum::all();
        $master_bank = MasterBank::all();
        $master_bidang_pekerjaan = MasterBidangPekerjaan::all();
        $master_jenis_kelamin = MasterJenisKelamin::all();
        $master_jenis_pengguna = MasterJenisPengguna::all();
        $master_kawin = MasterKawin::all();
        $master_kepemilikan_rumah = MasterKepemilikanRumah::all();
        $master_negara = MasterNegara::all();
        $master_online = MasterOnline::all();
        $master_pekerjaan = MasterPekerjaan::all();
        $master_pendapatan = MasterPendapatan::all();
        $master_pendidikan = MasterPendidikan::all();
        $master_pengalaman_kerja = MasterPengalamanKerja::all();

        return view('pages.admin.add_investor', [
            'master_provinsi' => $master_provinsi,
            'master_agama' => $master_agama,
            'master_asset' => $master_asset,
            'master_badan_hukum' => $master_badan_hukum,
            'master_bank' => $master_bank,
            'master_bidang_pekerjaan' => $master_bidang_pekerjaan,
            'master_jenis_kelamin' => $master_jenis_kelamin,
            'master_jenis_pengguna' => $master_jenis_pengguna,
            'master_kawin' => $master_kawin,
            'master_kepemilikan_rumah' => $master_kepemilikan_rumah,
            'master_negara' => $master_negara,
            'master_online' => $master_online,
            'master_pekerjaan' => $master_pekerjaan,
            'master_pendapatan' => $master_pendapatan,
            'master_pendidikan' => $master_pendidikan,
            'master_pengalaman_kerja' => $master_pengalaman_kerja,
            'master_provinsi' => $master_provinsi,
        ]);
    }

    public function get_kota($id_provinsi)
    {
        $kota = MasterProvinsi::where('kode_provinsi', $id_provinsi)
            ->orderBy('kode_kota', 'asc')
            ->get(['kode_kota', 'nama_kota']);

        return response()->json(['kota' => $kota]);
    }

    public function deleteAdmin($id)
    {
        $deleteUser = Admins::where('id', $id)->delete();

        if ($deleteUser) {
            $response = ['status' => 'Sukses'];
        }


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pengguna";
        $audit->description = "Hapus pengguna";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function createAdmin(Request $request)
    {
        if (Admins::where('email', $request->email)->exists()) {
            return redirect()->route('admin.manage')->with('exists', 'User exists!');
        }

        $admin = new Admins;

        $admin->firstname = $request->firstname;
        $admin->lastname = $request->lastname;
        $admin->email = $request->email;
        $admin->address = $request->address;
        $admin->password = bcrypt('$request->password');
        $admin->role = $request->role;

        $admin->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pengguna";
        $audit->description = "Tambah Pengguna";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->route('admin.manage')->with('createdone', 'Create User successfull');
    }

    public function changepassAdmin(Request $request)
    {

        $newhash = bcrypt($request->password);

        Admins::where('id', $request->id_user)
            ->update(['password' => $newhash]);


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pengguna";
        $audit->description = "Ubah password pengguna";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->route('admin.manage')->with('changepassdone', 'Change password successfull');
    }

    public function showBroadcastForm()
    {
        $data = app('App\Http\Controllers\KirimEmailController')->ListGroup();
        return view('pages.admin.broadcast_email')->with('data', $data);
    }



    public function broadcastEmail(Request $request)
    {
        $data = [
            'title' => $request->judul_email,
            'content' => $request->deskripsi,
            'list_id' => $request->list_email,

        ];

        app('App\Http\Controllers\KirimEmailController')->BroadcastEmail($data);

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pesan";
        $audit->description = "Broadcast Email";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->route('admin.broadcast')->with('broadcastdone', 'Broadcast success');
    }

    public function CreateListGroup()
    {
        $data = app('App\Http\Controllers\KirimEmailController')->ListGroup();

        return view('pages.admin.broadcast_list', compact('data'));
    }
    public function listNew(Request $request)
    {
        $data = [
            'title' => $request->list_kirim,
        ];
        app('App\Http\Controllers\KirimEmailController')->CreateList($data);
        return redirect()->back()->with('broadcastdone', 'Sukses membuat group list');;
    }
    public function postList(Request $request)
    {
        $data = [
            'id' => $request->id_list,
            'list_id' => $request->nominal_id,
        ];
        app('App\Http\Controllers\KirimEmailController')->CreateSubscriber($data);
        return redirect()->back()->with('broadcastdone', 'Sukses Import Email');;
    }

    public function showSingleMail()
    {
        // $user = Investor::all();

        return view('pages.admin.single_email')/*->with('user', $user)*/;
    }

    public function get_email_datatables(Request $request)
    {
        $email = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
            ->where('detil_investor.nama_investor', 'like', '%' . $request->name_email . '%')
            ->orderBy('investor.id', 'desc')
            ->get([
                'investor.id',
                'investor.username',
                'investor.email',
                'investor.status',
                'detil_investor.nama_investor'
            ]);

        $response = ['data_email' => $email];

        return response()->json($response);
    }

    public function sendSingleMail(Request $request)
    {
        $user = Investor::whereIn('id', $request->checked)->get();
        $data = [
            'judul_email' => $request->judul_email,
            'deskripsi' => $request->deskripsi
        ];
        foreach ($user as $users) {
            dispatch(new BroadcastEmail($data, $users));
        }
        return redirect()->route('admin.singleMail')->with('singlemail', 'Send Email success');
    }

    public function showNews()
    {
        return view('pages.admin.news');
    }

    public function postNews(Request $request)
    {
        $news = new News;
        $news->title = $request->judul;
        $news->writer = $request->writer;
        $news->deskripsi = $request->deskripsi;
        $news->save();
        $news->image = $this->upload('news' . $news->id, $request->image, 'admin/news');
        $news->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Berita";
        $audit->description = "Tambah berita baru";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Create News success');
    }

    private function upload($column, $request, $store_path)
    {
        $file = $request;
        $filename = Carbon::now()->toDateString() . $column . '.' . $file->getClientOriginalExtension();

        $path = $file->storeAs($store_path, $filename, 'public');
        //            save gambar yang di upload di public storage
        return $path;
    }

    public function listNews()
    {
        $news = News::all();
        return view('pages.admin.listNews')->with('news', $news);
    }

    public function deleteNews(Request $request)
    {
        $news = News::find($request->id);
        Storage::disk('public')->delete($news->image);
        $news->delete();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Berita";
        $audit->description = "Hapus berita";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('delete', 'News Deleted');
    }

    public function updateNews(Request $request)
    {
        $news = News::find($request->id);
        $news->title = $request->judul;
        $news->writer = $request->writer;
        $news->deskripsi = $request->deskripsi;
        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($news->image);
            $news->image = $this->upload('news' . $news->id, $request->image, 'admin/news');
        }
        $news->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Berita";
        $audit->description = "Ubah berita";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('delete', 'News Edited');
    }

    private function upload_carousel($column, Request $request)
    {
        if ($request->hasFile($column)) {
            $file = $request->file($column);
            $filename = Carbon::now()->toDateString() . $request->gambar_carousel->getClientOriginalName();
            $resize = Image::make($file)->resize(1657, 690)->save($filename);
            $store_path = 'carousel';
            $path = $file->storeAs($store_path, $filename, 'public');
            //            save gambar yang di upload di public storage
            return $path;
        } else {
            return null;
        }
    }

    public function manageCarousel()
    {

        return view('pages.admin.manage_carousel');
    }

    public function admin_add_carousel(Request $request)
    {

        $carousel = new ManageCarousel;

        $carousel->gambar = $this->upload_carousel('gambar_carousel', $request);

        $carousel->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Karosel";

        $audit->description = "Tambah gambar carausel";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Add Gambar Success');
    }

    public function admin_update_carousel(Request $request)
    {

        $carousel = ManageCarousel::where('id_carousel', $request->gambar_id)->first();

        if ($request->hasFile('gambar_carousel')) {
            Storage::disk('public')->delete($carousel->gambar);
            $carousel->gambar   = $this->upload_carousel('gambar_carousel', $request);

            
        }
        $carousel->judul        = $request->title_carousel;
        $carousel->isi          = $request->isi_carousel;
        $carousel->textalign    = $request->txtalign;
        if($request->has('buttonleft')){
            $carousel->buttonleft   = '1';
        }else{
            $carousel->buttonleft   = '0';
        }
        if($request->has('buttonright')){
            $carousel->buttonright   = '1';
        }else{
            $carousel->buttonright  = '0';
        }
        $carousel->left_button_label = $request->left_button_label;
        $carousel->right_button_label= $request->right_button_label;
        $carousel->left_button_link = $request->left_button_link;
        $carousel->right_button_link= $request->right_button_link;


        $carousel->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Karosel";
        $audit->description = "Ubah gambar Karosel";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('updated', "Update Success");
    }

    public function admin_delete_carousel($id)
    {

        $carousel = ManageCarousel::where('id_carousel', $id)->first();

        Storage::disk('public')->delete($carousel->gambar);

        $carousel_delete = ManageCarousel::where('id_carousel', $id)->delete();

        if ($carousel_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "kelola Karosel";
        $audit->description = "Hapus gambar Karosel";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function get_carousel()
    {
        $gambar = ManageCarousel::all();

        $response = ['data' => $gambar];

        return response()->json($response);
    }

    public function e_coll_bni()
    {
        return view('pages.admin.e_coll_bni');
    }

    public function get_ecoll()
    {
        $ecoll = ECollBni::orderBy('id_ecoll', 'desc')->get();

        $response = ['data' => $ecoll];

        return response()->json($response);
    }

    public function import(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->storeAs(
                'public/import',
                $filename
            );

            // READ DATA DARI FILE CSV YANG DISIMPAN DIDALAM FOLDER
            // STORAGE > APP > PUBLIC > IMPORT > NAMAFILE.CSV
            $csv = Reader::createFromPath(storage_path('app/public/import/' . $filename), 'r');
            $csv->setDelimiter(';');
            //BARIS PERTAMA DI-SET SEBAGAI KEY DARI ARRAY YANG DIHASILKAN
            $csv->setHeaderOffset(0);

            //LOOPING DATA YANG TELAH DI-LOAD
            foreach ($csv as $row) {
                //SIMPAN KE DALAM TABLE USER
                ECollBni::create([
                    'nama' => preg_replace('/[=""]/', '', $row['Customer Name']),
                    'no_va' => preg_replace('/[=""]/', '', $row['VA Number']),
                    'tgl_payment' => preg_replace('/[=""]/', '', $row['Payment Date']),
                ]);
            }
            return redirect()->back()->with(['success' => 'Upload success']);
        }
        return redirect()->back()->with(['error' => 'Failed to upload file']);
    }

    public function converter()
    {
        return view('pages.admin.convertCSVToJSON', ['data_json' => $this->data_json]);
    }

    public function actConvert(Request $request)
    {
        $this->validate($request, [
            'file' => 'required'
        ]);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->storeAs(
                'public/import',
                $filename
            );

            // READ DATA DARI FILE CSV YANG DISIMPAN DIDALAM FOLDER
            // STORAGE > APP > PUBLIC > IMPORT > NAMAFILE.CSV
            $csv = Reader::createFromPath(storage_path('app/public/import/' . $filename), 'r');
            $csv->setDelimiter('|');

            //Nama File
            $dataFile = explode(".", $filename);
            $namaFile = $dataFile[0];

            //Inisiasi Array
            $csvData = array();

            $csvData[] = [];
            //LOOPING DATA YANG TELAH DI-LOAD
            foreach ($csv as $row) {
                //Convert To JSON
                if ($namaFile == 'Pembayaran_Pinjaman') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[0],
                            'id_pinjaman' => (int)$row[1],
                            'id_borrower' => (int)$row[2],
                            'id_lender' => (int)$row[3],
                            'id_transaksi' => (int)$row[4],
                            'id_pembayaran' => (int)$row[5],
                            'tgl_pembayaran' => $row[6],
                            'tgl_pembayaran_borrower' => $row[7],
                            'tgl_pembayaran_penyelenggara' => $row[8],
                            'sisa_pinjaman_berjalan' => (int)$row[9],
                            'id_status_pinjaman' => (int)$row[10],
                            'tgl_pelunasan_borrower' => $row[11],
                            'tgl_pelunasan_penyelenggara' => $row[12],
                            'denda' => (int)$row[13],
                            'nilai_pembayaran' => (int)$row[14],
                            'id_jenis_pembayaran' => (int)$row[15],
                        ];
                } elseif ($namaFile == 'Pengajuan_Pemberian_Pinjaman') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[0],
                            'id_pinjaman' => (int)$row[1],
                            'id_borrower' => (int)$row[2],
                            'id_lender' => (int)$row[3],
                            'no_perjanjian_lender' => $row[4],
                            'tgl_perjanjian_lender' => $row[5],
                            'tgl_penawaran_pemberian_pinjaman' => $row[6],
                            'nilai_penawaran_pinjaman' => (int)$row[7],
                            'nilai_penawaran_disetujui' => (int)$row[8],
                            'no_va_lender' => $row[9],
                        ];
                } elseif ($namaFile == 'Pengajuan_Pinjaman') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[0],
                            'id_pinjaman' => (int)$row[1],
                            'id_borrower' => (int)$row[2],
                            'id_syariah' => (int)$row[3],
                            'id_status_pengajuan_pinjaman' => $row[4],
                            'nama_pinjaman' => (int)$row[5],
                            'tgl_pengajuan_pinjaman' => (int)$row[6],
                            'nilai_permohonan_pinjaman' => (int)$row[7],
                            'jangka_waktu_pinjaman' => (int)$row[8],
                            'satuan_jangka_waktu_pinjaman' => $row[9],
                            'penggunaan_pinjaman' => $row[10],
                            'agunan' => (int)$row[11],
                            'jenis_agunan' => $row[12],
                            'rasio_pinjaman_nilai_agunan' => (int)$row[13],
                            'permintaan_jaminan' => (int)$row[14],
                            'rasio_pinjaman_aset' => (int)$row[15],
                            'cicilan_bulan' => (int)$row[16],
                            'rating_pengajuan_pinjaman' => (int)$row[17],
                            'nilai_plafond' => (int)$row[18],
                            'nilai_pengajuan_pinjaman' => (int)$row[19],
                            'suku_bunga_pinjaman' => (int)$row[20],
                            'satuan_suku_bunga_pinjaman' => (int)$row[21],
                            'jenis_bunga' => (int)$row[22],
                            'tgl_mulai_publikasi_pinjaman' => (int)$row[23],
                            'rencana_jangka_waktu_publikasi' => (int)$row[24],
                            'realisasi_jangka_waktu_publikasi' => $row[25],
                            'tgl_mulai_pendanaan' => (int)$row[26],
                            'frekuensi_pinjaman' => (int)$row[27],
                        ];
                } elseif ($namaFile == 'profile_penyelenggara') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[0],
                            'nama_penyelenggara' => $row[1],
                            'layanan_pinjaman' => (int)$row[2],
                            'jumlah_tenaga_kerja_pria' => (int)$row[3],
                            'jumlah_tenaga_kerja_wanita' => (int)$row[4],
                            'jumlah_kantor_cabang' => (int)$row[5],
                        ];
                } elseif ($namaFile == 'Reg_Borrower') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[0],
                            'id_borrower' => (int)$row[1],
                            'kode_penguna' => (int)$row[2],
                            'kode_jenis_pengguna' => (int)$row[3],
                            'nama_borrower' => $row[4],
                            'no_ktp' => (int)$row[5],
                            'no_npwp' => $row[6],
                            'id_jenis_badan_hukum' => (int)$row[7],
                            'tempat_lahir' => $row[8],
                            'tgl_lahir' => $row[9],
                            'id_jenis_kelamin' => (int)$row[10],
                            'alamat' => $row[11],
                            'id_kota' => (int)$row[12],
                            'id_provinsi' => (int)$row[13],
                            'kode_pos' => (int)$row[14],
                            'id_agama' => (int)$row[15],
                            'id_status_perkawinan' => (int)$row[16],
                            'id_pekerjaan' => (int)$row[17],
                            'id_bidang_pekerjaan' => (int)$row[18],
                            'id_pekerjaan_online' => (int)$row[19],
                            'pendapatan' => (int)$row[20],
                            'total_aset' => (int)$row[21],
                            'pengalaman_kerja' => (int)$row[22],
                            'id_pendidikan' => (int)$row[23],
                            'status_kepemilikan_rumah' => (int)$row[24],
                            'nama_perwakilan' => $row[25],
                            'no_ktp_perwakilan' => (int)$row[26],
                        ];
                } elseif ($namaFile == 'Reg_Lender') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[0],
                            'id_lender' => (int)$row[1],
                            'kode_penguna' => (int)$row[2],
                            'jenis_pengguna' => (int)$row[3],
                            'nama_lender' => $row[4],
                            'no_ktp' => (int)$row[5],
                            'no_npwp' => $row[6],
                            'id_jenis_badan_hukum' => (int)$row[7],
                            'id_negara_domisili' => (int)$row[8],
                            'tempat_lahir' => $row[9],
                            'tgl_lahir' => $row[10],
                            'id_jenis_kelamin' => (int)$row[11],
                            'alamat' => $row[12],
                            'id_kota' => (int)$row[13],
                            'id_provinsi' => (int)$row[14],
                            'kode_pos' => (int)$row[15],
                            'id_agama' => (int)$row[16],
                            'id_status_perkawinan' => (int)$row[17],
                            'id_pekerjaan' => (int)$row[18],
                            'id_bidang_pekerjaan' => (int)$row[19],
                            'pendapatan' => (int)$row[20],
                            'pengalaman_kerja' => (int)$row[21],
                            'id_pendidikan' => (int)$row[22],
                            'id_kewarganegaraan' => (int)$row[23],
                            'sumber_dana' => (int)$row[24],
                            'nama_perwakilan' => $row[25],
                            'no_ktp_perwakilan' => (int)$row[26],
                        ];
                } elseif ($namaFile == 'Reg_Pengguna') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[0],
                            'id_pengguna' => (int)$row[1],
                            'kode_penguna' => $row[2],
                            'jenis_pengguna' => (int)$row[3],
                            'tgl_registrasi' => $row[4],
                            'nama_pengguna' => $row[5],
                            'no_ktp' => (int)$row[6],
                            'no_npwp' => $row[7],
                        ];
                } elseif ($namaFile == 'Rincian_Direksi_Komisaris_PS') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => (int)$row[2],
                            'nama' => $row[3],
                            'no_ktp' => (int)$row[4],
                            'jabatan' => $row[5],
                            'masa_jabatan' => $row[6],
                            'npwp/tin' => $row[7],
                            'jumlah_nilai_saham' => (int)$row[8],
                            'jumlah_lebar_saham' => (int)$row[9],
                            'presentase_nilai_saham' => $row[10],
                        ];
                } elseif ($namaFile == 'Rincian_Escrow') {
                    $csvData[] =
                        [
                            'kode_bank' => $row[1],
                            'no_rekening_escrow' => (int)$row[2],
                            'id_penyelenggara' => (int)$row[3],
                        ];
                } elseif ($namaFile == 'Rincian_Kerjasama_LJK') {
                    $csvData[] =
                        [
                            'nama_ljk' => $row[0],
                            'jenis_ljk' => $row[1],
                            'domisili_ljk' => $row[2],
                            'id_penyelenggara' => $row[3],
                        ];
                } elseif ($namaFile == 'Rincian_Layanan_Pendukung') {
                    $csvData[] =
                        [
                            'nama_layanan_pendukung' => $row[1],
                            'jenis_layanan_pendukung' => (int)$row[2],
                            'domisili_layanan_pendukung' => $row[3],
                        ];
                } elseif ($namaFile == 'Transaksi_Pinjam_Meminjam') {
                    $csvData[] =
                        [
                            'id_penyelenggara' => $row[0],
                            'id_pinjaman' => $row[1],
                            'id_borrower' => $row[2],
                            'id_lender' => $row[3],
                            'id_transaksi' => $row[4],
                            'no_perjanjian_borrower' => $row[5],
                            'tgl_perjanjian_borrower' => $row[6],
                            'nilai_pendanaan' => $row[7],
                            'suku_bunga_pinjaman' => $row[8],
                            'satuan_suku_bunga_pinjaman' => $row[9],
                            'id_jenis_pembayaran' => $row[10],
                            'id_frekuensi_pembayaran' => $row[11],
                            'nilai_angsuran' => $row[12],
                            'objek_jaminan' => $row[13],
                            'id_provinsi' => $row[14],
                            'jangka_waktu_pinjaman' => $row[15],
                            'satuan_jangka_waktu_pinjaman' => $row[16],
                            'tgl_jatuh_tempo' => $row[17],
                            'tgl_pendanaan' => $row[18],
                            'tgl_penyaluran_dana' => $row[19],
                            'no_ea_transaksi' => $row[20],
                            'frekuensi_pendanaan' => $row[21],
                        ];
                } else {
                    $csvData[] = [];
                }
            }
            $this->data_json = $csvData;
            return view('pages.admin.convertCSVToJSON', ['success' => 'Upload success', 'data_json' => $this->data_json]);
        }
        return redirect()->back()->with(['error' => 'Failed to upload file']);
    }

    public function showMenu()
    {
        return view('pages.admin.edit_menu');
    }

    public function add_role(Request $request)
    {
        $role = new ManageRole;
        $role->name = $request->role;
        $role->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pengguna";
        $audit->description = "Tambah Peran";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        $getRole = ManageRole::orderBy('id', 'desc')->first();

        for ($i = 0; $i < sizeof($request->dataMenu); $i++) {
            $rbac = new RBAC;
            $rbac->id_role = $getRole->id;
            $rbac->id_menu = $request->dataMenu[$i];

            $rbac->save();
        }

        return redirect()->back()->with('success', 'Create Success');
    }

    public function get_role()
    {
        $role = ManageRole::orderBy('id', 'asc')->get();

        $response = ['data' => $role];

        return response()->json($response);
    }

    public function edit_role(Request $request)
    {
        $role_edit = ManageRole::where('id', $request->role_id)->first();
        $role_edit->name = $request->role_edit;
        $role_edit->save();

        $rbacDelete = RBAC::where('id_role', $request->role_id)->delete();
        for ($i = 0; $i < sizeof($request->dataMenuEdit); $i++) {
            $rbac_edit = new RBAC;
            $rbac_edit->id_role = $request->role_id;
            $rbac_edit->id_menu = $request->dataMenuEdit[$i];

            $rbac_edit->save();
        }


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pengguna";
        $audit->description = "Ubah data peran";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('updated', 'Update Success');
    }

    public function delete_role($id)
    {
        $role_delete = ManageRole::where('id', $id)->delete();
        $rbacDelete = RBAC::where('id_role', $id)->delete();
        if ($role_delete && $rbacDelete) {
            $response = ['status' => 'Sukses'];
        }


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pengguna";
        $audit->description = "Hapus data peran";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function edit_menu($id)
    {
        $dataMenu = AdminMenuItem::leftJoin('rbac', 'rbac.id_menu', '=', 'admin_menu_items.id')
            ->where('rbac.id_role', $id)
            ->orderBy('admin_menu_items.id', 'asc')
            ->get(['rbac.id_role', 'rbac.id_menu', 'admin_menu_items.label']);
        $cekMenu = array();
        foreach ($dataMenu as $menu) {
            $cekMenu[] = $menu->id_menu;
        }

        $dataMenuElse = AdminMenuItem::whereNotIn('id', $cekMenu)
            ->whereNotIn('id', [\DB::raw('select parent from admin_menu_items where parent != 0')])
            ->orderBy('id', 'asc')
            ->get(['id']);

        return response()->json(['data' => $dataMenu, 'dataElse' => $dataMenuElse]);
    }

    public function get_users_datatables()
    {
        $users = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')
            ->get(['admins.firstname', 'admins.lastname', 'admins.email', 'admins.id', 'admins.role', 'roles.name']);

        return response()->json(['data' => $users]);
    }

    public function editAdmins(Request $request)
    {

        $admin_edit = Admins::where('id', $request->id_user)->first();

        if (Auth::user()->email == $request->email) {
            return redirect()->route('admin.manage')->with('exists', 'User exists!');
        }

        $admin_edit->firstname = $request->firstname;
        $admin_edit->lastname = $request->lastname;
        $admin_edit->email = $request->email;
        $admin_edit->address = $admin_edit->address;
        $admin_edit->password = $admin_edit->password;
        $admin_edit->role = $request->role;

        $admin_edit->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Pengguna";
        $audit->description = "Ubah pengguna";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->route('admin.manage')->with('editdone', 'Edit User successfull');
    }

    public function showLink()
    {
        $master_kode_operator = DB::table("m_kode_operator_negara")->get();
        return view('pages.admin.link_verifikasi_register',['master_kode_operator' => $master_kode_operator]);
    }

    public function get_link_investor()
    {
        $link = Investor::where('status', 'Not Active')
            ->orderBy('id', 'desc')
            ->get();
        return response()->json(['data' => $link]);
    }

    public function upload_penghargaan($column, Request $request)
    {
        if ($request->hasFile($column)) {
            $file = $request->file($column);
            $filename = Carbon::now()->toDateString() . $request->gambar->getClientOriginalName();
            $store_path = 'penghargaan';
            $path = $file->storeAs($store_path, $filename, 'public');
            return $path;
        } else {
            return null;
        }
    }

    public function managePenghargaan()
    {
        return view('pages.admin.manage_penghargaan');
    }

    public function get_penghargaan()
    {
        $gambar = ManagePenghargaan::all();

        $response = ['data' => $gambar];

        return response()->json($response);
    }

    public function admin_add_penghargaan(Request $request)
    {
        $new = new ManagePenghargaan;
        $new->title = $request->title;
        $new->keterangan = $request->keterangan;
        $new->tgl_publish = Carbon::now()->toDateString();
        $new->author = 'Admin';
        $new->gambar = $this->upload_penghargaan('gambar', $request);

        $new->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Penghargaan";
        $audit->description = "Tambah penghargaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Add Penghargaan Sukses');
    }

    public function admin_delete_penghargaan($id)
    {

        $carousel = ManagePenghargaan::where('id', $id)->first();

        Storage::disk('public')->delete($carousel->gambar);

        $carousel_delete = ManagePenghargaan::where('id', $id)->delete();

        if ($carousel_delete) {
            $response = ['status' => 'Sukses'];
        }


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Penghargaan";
        $audit->description = "Hapus penghargaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function admin_update_penghargaan(Request $request)
    {

        $update = ManagePenghargaan::where('id', $request->id)->first();
        // var_dump($update);
        $update->title = $request->title;
        $update->keterangan = $request->keterangan;
        $update->tgl_publish = Carbon::now()->toDateString();
        $update->author = 'Admin';

        if ($request->hasFile('gambar')) {
            Storage::disk('public')->delete($update->gambar);
            $update->gambar = $this->upload_penghargaan('gambar', $request);
            $update->save();
        }

        $update->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Penghargaan";
        $audit->description = "Ubah penghargaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('updated', "Update Gambar Success");
    }



    public function upload_khazanah($column, Request $request)
    {
        if ($request->hasFile($column)) {
            $file = $request->file($column);
            $filename = Carbon::now()->toDateString() . $request->gambar->getClientOriginalName();
            $store_path = 'Khazanah';
            $path = $file->storeAs($store_path, $filename, 'public');

            return $path;
        }
    }

    public function manageKhazanah()
    {
        return view('pages.admin.manage_khazanah');
    }

    public function get_khazanah()
    {
        $data = ManageKhazanah::all();
        $response = ['data' => $data];

        return response()->json($response);
    }

    public function addKhazanah(Request $request)
    {
        $new = new ManageKhazanah();

        $size_file = $request->file('gambar')->getSize();
        if ($size_file > 1000000) { //jika file lebih dari 2MB
            return redirect()->back()->with('error', 'Gambar Yang Anda Unggah Tidak Boleh Lebih Dari 1MB');
        } else {
            $new->title = $request->title;
            $new->author = 'Admin';
            $new->tgl_publish = Carbon::now()->toDateString();
            $new->keterangan = $request->keterangan;
            $new->gambar = $this->upload_khazanah('gambar', $request);
            $new->save();


            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Kelola Khazanah";
            $audit->description = "Tambah Gambar Khazanah";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return redirect()->back()->with('success', 'Tambah Khazanah Sukses');
        }
    }

    public function admin_update_khazanah(Request $request)
    {
        $update = ManageKhazanah::where('id', $request->id)->first();

        if ($request->hasFile('gambar')) {
            $size_file = $request->file('gambar')->getSize();
            if ($size_file > 1000000) { //jika file lebih dari 2MB
                return redirect()->back()->with('error', 'Gambar Yang Anda Unggah Tidak Boleh Lebih Dari 1MB');
            } else {
                Storage::disk('public')->delete($update->gambar);
                $update->gambar = $this->upload_khazanah('gambar', $request);
                $update->save();
            }
        }
        $update->title = $request->title;
        $update->keterangan = $request->keterangan;
        $update->tgl_publish = Carbon::now()->toDateString();
        $update->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Khazanah";
        $audit->description = "Ubah Gambar Khazanah";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('updated', "Update Gambar Success");
    }

    public function admin_delete_khazanah($id)
    {
        $delete = ManageKhazanah::where('id', $id)->first();
        Storage::disk('public')->delete($delete->gambar);

        $delete_khazanah = ManageKhazanah::where('id', $id)->delete();
        if ($delete_khazanah) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Khazanah";
        $audit->description = "Hapus Gambar Khazanah";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function list_table_mutasi()
    { // admin/manage proyek mutasi

        $mutasi = DB::table('proyek AS a')
            ->selectRaw('a.id, a.nama, a.total_need, a.harga_paket, a.terkumpul + sum(b.nominal_awal) as terkumpul, sum(b.nominal_awal) as terkumpul_dari_investor, count(b.investor_id) as jumlah_investor, sum(b.nominal_awal)/a.total_need*100 as presentasi, b.proyek_id, b.investor_id, b.id as id_pendanaan, b.total_dana, b.nominal_awal')
            ->join('pendanaan_aktif AS b', 'a.id', '=', 'b.proyek_id')
            ->groupBy('b.proyek_id')
            ->where('b.status', '=', 1)
            ->get();

        $data = array();
        $no = 1;
        if (!empty($mutasi)) {
            foreach ($mutasi as $row) {
                $column['ID Proyek']             = $row->id;
                $column['Nama Proyek']             = (string)$row->nama;
                $column['Total Dibutuhkan']     = number_format($row->total_need);
                $column['Terkumpul']                 = number_format($row->terkumpul);
                $column['Terkumpul dari Pendana']     = number_format($row->terkumpul_dari_investor);
                $column['Jumlah Pendana']         = $row->jumlah_investor;
                $column['Detail Mutasi']         = $row->id_pendanaan;
                $column['Presentasi Terkumpul'] = number_format($row->presentasi, '2');

                $data[] = $column;
            }
        }
        $parsingJSON = array(
            "data" => $data
        );
        echo json_encode($parsingJSON);
    }

    public function detail_table_mutasi($id)
    { // admin/manage proyek detail mutasi

        //$mutasi = DB::table('proyek')->get();
        $detail_mutasi = DB::table('investor as a')
            ->selectRaw('a.id, a.email, b.investor_id, b.nama_investor, c.investor_id, c.proyek_id, c.tanggal_invest, d.pendanaanAktif_id, d.nominal')
            ->join('detil_investor AS b', 'a.id', '=', 'b.investor_id')
            ->join('pendanaan_aktif AS c', 'c.investor_id', '=', 'a.id')
            ->join('log_pendanaan AS d', 'd.pendanaanAktif_id', '=', 'c.id')
            //->groupBy('b.proyek_id')
            ->where('c.proyek_id', '=', $id)
            ->where('c.status', 1)
            ->get();
        $data = array();
        $no = 1;
        if (!empty($detail_mutasi)) {
            foreach ($detail_mutasi as $row) {
                //var_dump();
                $column['Proyek_id']                = (string)$row->proyek_id;
                $column['Investor_id']              = (string)$row->investor_id;
                $column['Nama Pendana']     = (string)$row->nama_investor;
                $column['Email Pendana']     = (string)$row->email;
                $column['Nominal Pendanaan']         = (string)number_format($row->nominal);
                $column['Tanggal Pendanaan']         = (string)$row->tanggal_invest;

                $data[] = $column;
            }
        }
        $parsingJSON = array(
            "data" => $data
        );



        echo json_encode($parsingJSON);
    }

    public function manageVersion()
    {
        return view('pages.admin.manage_mobile');
    }

    public function tableManageVersion()
    {
        $dataGet = MobileVersion::all();

        $dataArray = array();
        $i = 1;

        if (!empty($dataGet)) {
            foreach ($dataGet as $item) {
                $column['no'] = (string) $i++;
                $column['id'] = (string) $item->id;
                $column['versionMobile'] = (string) $item->version_code;
                $column['kodeVersionMobile'] = (string) $item->version;
                $column['tanggalMobile'] = (string)Carbon::parse($item->created_at)->toDateString();
                $column['gambarMobile'] = (string)$item->location;

                $dataArray[] = $column;
            }
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function postVersionMobile(Request $request)
    {

        $versi = new MobileVersion;

        $versi->version = $request->version;
        $versi->version_code = $request->version_code;
        // $versi->created_date = $request->created_date;
        $versi->save();

        $versi->location = $this->upload('version' . $versi->id, $request->location, 'admin');
        $versi->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Versi";
        $audit->description = "Tambah mobile version";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Upload New Version Berhasil');
    }

    public function editVersionMobile(Request $request)
    {
        // echo $request->editGmbr;die;
        $updateVersi = MobileVersion::where('id', $request->idVersion)->first();

        $updateVersi->version = $request->editVersion;
        $updateVersi->version_code = $request->editVersion_code;
        // $updateVersi->created_date = $request->editCreated_date;
        // if (Storage::exists($updateVersi->location)) {
        //     Storage::disk('public')->delete($updateVersi->location);
        // }\
        if (!empty($request->editLocation)) {
            Storage::disk('public')->delete($updateVersi->location);
            $updateVersi->location = $this->uploadVersion('version' . $updateVersi->id, $request->editLocation, 'admin');
        } else {
        }
        $updateVersi->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Versi";
        $audit->description = "Ubah mobile version";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Update Version Berhasil');
    }

    public function deleteVersion($id)

    {
        $deleteVersi = MobileVersion::find($id);
        Storage::disk('public')->delete($deleteVersi->location);
        $deleteVersi->delete();

        $response = ['data' => 'sukses'];

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Versi";
        $audit->description = "Hapus mobile version";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    private function uploadVersion($column, $request, $store_path)
    {
        $file = $request;
        if (!empty($file)) {
            $filename = Carbon::now()->toDateString() . $column . '.' . $file->getClientOriginalExtension();

            $path = $file->storeAs($store_path, $filename, 'public');
            return $path;
        } else {
            return true;
        }
    }

    public function show_menu_teks()
    {
        return view('pages.admin.teks_notifikasi');
    }

    public function datatables_teks()
    {
        $teks = TeksNotifikasi::all();

        $response = ['data' => $teks];

        return response()->json($response);
    }

    public function admin_add_teks(Request $request)
    {

        $teks = new TeksNotifikasi;

        $teks->teks_notifikasi = $request->teks_notif;
        $teks->tipe = $request->tipe;

        $teks->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Teks Notifikasi";
        $audit->description = "Tambah teks notifikasi data kontrak";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Tambah Teks Berhasil');
    }

    public function admin_update_teks(Request $request)
    {

        $teks = TeksNotifikasi::where('id_teks_notifikasi', $request->id_teks_notifikasi)->first();
        // echo $request->edit_tipe;die;

        $teks->teks_notifikasi = $request->edit_teks_notif;
        $teks->tipe = $request->edit_tipe;

        $teks->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Teks Notifikasi";
        $audit->description = "Ubah teks notifikasi data kontrak";
        $audit->ip_address =  \Request::ip();
        $audit->save();


        return redirect()->back()->with('updated', "Ubah Teks Berhasil");
    }

    public function admin_delete_teks($id)
    {

        $teks = TeksNotifikasi::where('id_teks_notifikasi', $id)->first();

        $teks_delete = TeksNotifikasi::where('id_teks_notifikasi', $id)->delete();

        if ($teks_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Teks Notifikasi";
        $audit->description = "Hapus teks notifikasi data kontrak";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function show_menu_threshold()
    {
        return view('pages.admin.threshold_kontrak');
    }

    public function datatables_threshold()
    {
        $threshold = ThresholdKontrak::all();

        $response = ['data' => $threshold];

        return response()->json($response);
    }

    public function admin_add_threshold(Request $request)
    {

        $threshold = new ThresholdKontrak;

        $threshold->threshold_kontrak = $request->threshold;
        $threshold->tipe = $request->tipe;

        $threshold->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Threshold Kontrak";
        $audit->description = "Tambah threshold kontrak";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Tambah Threshold Berhasil');
    }

    public function admin_update_threshold(Request $request)
    {

        $threshold = ThresholdKontrak::where('id_threshold', $request->id_threshold)->first();
        // echo $request->edit_tipe;die;

        $threshold->threshold_kontrak = $request->edit_threshold;
        $threshold->tipe = $request->edit_tipe;

        $threshold->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Threshold Kontrak";
        $audit->description = "ubah threshold kontrak";
        $audit->ip_address =  \Request::ip();
        $audit->save();


        return redirect()->back()->with('updated', "Ubah Threshold Berhasil");
    }

    public function admin_delete_threshold($id)
    {

        $threshold = ThresholdKontrak::where('id_threshold', $id)->first();

        $threshold_delete = ThresholdKontrak::where('id_threshold', $id)->delete();

        if ($threshold_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Threshold Kontrak";
        $audit->description = "Hapus threshold kontrak";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function showaddSyaratKetentuan()
    {
        return view('pages.admin.add_syarat_ketentuan');
    }

    public function listSyaratKetentuan()
    {
        $term_condition = TermCondition::all();
        return view('pages.admin.listSyaratKetentuan')->with('term_condition', $term_condition);
    }

    public function postSyaratKetentuan(Request $request)
    {
        $news = new TermCondition;
        $news->title = $request->judul;
        $news->writer = $request->writer;
        $news->deskripsi = $request->deskripsi;
        $news->save();

        return redirect()->back()->with('success', 'Sukses menyimpan data');
    }

    public function updateSyaratKetentuan(Request $request)
    {
        $term_condition = TermCondition::find($request->id);
        $term_condition->title = $request->judul;
        $term_condition->writer = $request->writer;
        $term_condition->deskripsi = $request->deskripsi;

        $term_condition->save();

        return redirect()->back()->with('Edit', 'Edit berhasil');
    }

    public function deleteSyaratKetentuan(Request $request)
    {
        $delete = TermCondition::find($request->id)->delete();
        return redirect()->back()->with('delete', 'Berhasil hapus Syarat Ketentuan');
    }

    public function managePendanaanBorower()
    {
        // echo Config::get('constan.url');die;
        return view('pages.admin.pendanaanBorrower');
    }

    public function listBorrower()
    {
        // echo Config::get('constan.url');die;
        return view('pages.admin.list_borrower');
    }
    public function list_pendanaan_borrower($id)
    {
        $dataGet = BorrowerPendanaan::leftJoin('brw_users_details', 'brw_users_details.brw_id', '=', 'brw_pendanaan.brw_id')
            ->where('brw_pendanaan.brw_id', $id)
            ->get([
                'brw_pendanaan.*',
                'brw_users_details.brw_type'
            ]);

        $dataArray = array();

        if (!empty($dataGet)) {
            foreach ($dataGet as $item) {
                $column = array();
                $column[] = (string)$item->pendanaan_id;
                $column[] = (string)$item->brw_id;
                $column[] = (string)$item->brw_type;
                $column[] = (string)$item->pendanaan_nama;
                $column[] = (string)$item->pendanaan_dana_dibutuhkan;
                $column[] = (string)Carbon::parse($item->estimasi_mulai)->toDateString();
                $column[] = (string)$item->status;
                $column[] = (string)$item->id_proyek;
                $column[] = '';

                $dataArray[] = $column;
            }
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function manageScorringBorower()
    {
        return view('pages.admin.scorringBorrower');
    }

    public function manageVerifikasiBorrower()
    {
        return view('pages.admin.verifikasiBorrower');
    }

    public function edit_poto_1(Request $request)
    {

        if ($request->file('file')) {
            $file = $request->file('file');
            $filename = 'pic_brw' . '.' . $file->getClientOriginalExtension();
            // $resize = Image::make($file)->resize(480,640, function ($constraint) {
            // $constraint->aspectRatio();
            // })->save();


            $resize = Image::make($file)->resize(480, 640, function ($constraint) {
                $constraint->aspectRatio();
            })->save($filename);
            // save nama file berdasarkan tanggal upload+nama file
            $store_path = 'borrower/' . $brw_id;
            // $path = $file->storeAs($store_path, $filename, 'public');
            $path = $file->storeAs($store_path, $filename, 'private');
            // save gambar yang di upload di public storage

            // Storage::disk('public')->delete('user/'.$investor_id.'/'.$filename);

            // if(Storage::disk('public')->exists('borrower/'.$brw_id.'/'.$filename)){
            if (Storage::disk('private')->exists('borrower/' . $brw_id . '/' . $filename)) {
                return response()->json([
                    'success' => 'Berhasil di upload',
                    'url' => "borrower/" . $brw_id . "/" . $filename,
                    'filename' => $filename
                ]);
            } else {
                return response()->json([
                    'failed' => 'File gagal di upload'
                ]);
            }
        } else {
            return response()->json([
                'failed' => 'File Kosong'
            ]);
        }
    }

    public function update_borrower(Request $request)
    {

        if ($brw_users->type_borrower == 1 || $brw_users->type_borrower == 3) {
            // proses individu
            $Borrower = BorrowerDetails::where('brw_id', $brw_users->brw_id)->first();

            $Borrower->nama = $brw_users->txt_nm_individu;
            $Borrower->nm_ibu = $brw_users->txt_nm_ibu_individu;
            $Borrower->ktp = $brw_users->txt_no_ktp_individu;
            $Borrower->npwp = $brw_users->txt_no_npwp_individu;
            $Borrower->tgl_lahir = $brw_users->txt_thn_lahir . '-' . $brw_users->txt_bln_lahir . '-' . $brw_users->txt_tgl_lahir;
            $Borrower->no_tlp = $brw_users->txt_no_telp_individu;
            $Borrower->jns_kelamin = $brw_users->txt_jns_kelamin;
            $Borrower->status_kawin = $brw_users->txt_sts_nikah;
            $Borrower->alamat = $brw_users->txt_alamat_individu;

            $Borrower->domisili_alamat = $brw_users->txt_alamat_domisili_individu;
            $Borrower->domisili_provinsi = $brw_users->txt_provinsi_domisili_individu;
            $Borrower->domisili_kota = $brw_users->txt_kota_domisili_individu;
            $Borrower->domisili_kecamatan = $brw_users->txt_kecamatan_domisili_individu;
            $Borrower->domisili_kelurahan = $brw_users->txt_kelurahan_domisili_individu;
            $Borrower->domisili_kd_pos = $brw_users->txt_kd_pos_domisili_individu;

            $Borrower->provinsi = $brw_users->txt_provinsi_individu;
            $Borrower->kota = $brw_users->txt_kota_individu;
            $Borrower->kecamatan = $brw_users->txt_kecamatan_individu;
            $Borrower->kelurahan = $brw_users->txt_kelurahan_individu;
            $Borrower->kode_pos = $brw_users->txt_kd_pos_individu;
            $Borrower->status_rumah = $brw_users->txt_pemilik_rumah;

            $Borrower->agama = $brw_users->txt_agama;
            $Borrower->tempat_lahir = $brw_users->txt_tmpt_lahir_individu;
            $Borrower->pendidikan_terakhir = $brw_users->txt_pendidikan_pribadi;
            $Borrower->pekerjaan = $brw_users->txt_pekerjaan_individu;
            $Borrower->bidang_pekerjaan = $brw_users->txt_bidang_pekerjaan_individu;
            $Borrower->bidang_online = $brw_users->txt_bidang_online_individu;
            $Borrower->pengalaman_pekerjaan = $brw_users->txt_pengalaman_individu;
            $Borrower->pendapatan = $brw_users->txt_pendapatan_bulan_individu;

            // poto
            // $Borrower->brw_pic = $brw_users->url_pic_brw;
            // $Borrower->brw_pic_ktp = $brw_users->url_pic_brw_ktp;
            // $Borrower->brw_pic_user_ktp = $brw_users->url_pic_brw_dengan_ktp;
            // $Borrower->brw_pic_npwp = $brw_users->url_pic_brw_npwp;

            $Borrower->update();

            // insert data Ahli Waris
            $BorrowerAW = BorrowerAhliWaris::where('brw_id', $brw_users->brw_id)->first();
            $BorrowerAW->nama_ahli_waris = $brw_users->txt_nm_aw;
            $BorrowerAW->nik = $brw_users->txt_nik_aw;
            $BorrowerAW->no_tlp = $brw_users->txt_no_hp_aw;
            $BorrowerAW->email = $brw_users->txt_email_aw;
            $BorrowerAW->alamat = $brw_users->txt_alamat_aw;
            $BorrowerAW->provinsi = $brw_users->txt_provinsi_aw;
            $BorrowerAW->kota = $brw_users->txt_kota_aw;
            $BorrowerAW->kecamatan = $brw_users->txt_kecamatan_aw;
            $BorrowerAW->kelurahan = $brw_users->txt_kelurahan_aw_pribadi;
            $BorrowerAW->kd_pos = $brw_users->txt_kd_pos_aw;
            $BorrowerAW->update();

            //insert data REkening
            $BorrowerRek = BorrowerRekening::where('brw_id', $brw_users->brw_id)->first();
            $BorrowerRek->brw_norek = $brw_users->txt_no_rek_individu;
            $BorrowerRek->brw_nm_pemilik = $brw_users->txt_nm_pemilik_individu;
            $BorrowerRek->brw_kd_bank = $brw_users->txt_bank_individu;
            $BorrowerRek->update();

            echo "sukses";
        } else {

            // proses badan hukum
            $Borrower = BorrowerDetails::where('brw_id', $brw_users->brw_id)->first();
            $Borrower->nama = $brw_users->txt_nm_anda_bdn_hukum;
            $Borrower->nm_bdn_hukum = $brw_users->txt_nm_bdn_hukum;
            $Borrower->jabatan = $brw_users->txt_jabatan_bdn_hukum;
            $Borrower->brw_type = $brw_users->type_borrower;
            $Borrower->ktp = $brw_users->txt_nik_bdn_hukum;
            $Borrower->npwp = $brw_users->txt_npwp_bdn_hukum;
            $Borrower->no_tlp = $brw_users->txt_no_hp_bdn_hukum;
            $Borrower->alamat = $brw_users->txt_alamat_bdn_hukum;
            $Borrower->provinsi = $brw_users->txt_provinsi_bdn_hukum;
            $Borrower->kota = $brw_users->txt_kota_bdn_hukum;
            $Borrower->kecamatan = $brw_users->txt_kecamatan_bdn_hukum;
            $Borrower->kelurahan = $brw_users->txt_kelurahan_bdn_hukum;
            $Borrower->kode_pos = $brw_users->txt_kd_pos_bdn_hukum;
            $Borrower->bidang_perusahaan = $brw_users->txt_bidang_pekerjaan_bdn_hukum;
            $Borrower->bidang_online = $brw_users->txt_bidang_online_bdn_hukum;
            $Borrower->pendapatan = $brw_users->txt_revenue_bulanan_bdn_hukum;
            $Borrower->total_aset = $brw_users->txt_total_asset_bdn_hukum;
            // $Borrower->brw_pic = $brw_users->url_pic_brw_bdn_hukum;
            // $Borrower->brw_pic_ktp = $brw_users->url_pic_brw_ktp_bdn_hukum;
            // $Borrower->brw_pic_user_ktp = $brw_users->url_pic_brw_dengan_ktp_bdn_hukum;
            // $Borrower->brw_pic_npwp = $brw_users->url_pic_brw_npwp_bdn_hukum;
            $Borrower->update();

            // insert data pengurus
            $ahliWaris = BorrowerPengurus::where('brw_id', $brw_users->brw_id)->first();
            $ahliWaris->nm_pengurus = $brw_users->txt_nm_pengurus;
            $ahliWaris->nik_pengurus = $brw_users->txt_nik_pengurus;
            $ahliWaris->no_tlp = $brw_users->txt_no_hp_pengurus;
            $ahliWaris->jabatan = $brw_users->txt_jabatan_pengurus;
            $ahliWaris->update();

            // insert data Rekening
            $ahliWaris =  BorrowerRekening::where('brw_id', $brw_users->brw_id)->first();
            $ahliWaris->brw_norek = $brw_users->txt_no_rek_bdn_hukum;
            $ahliWaris->brw_nm_pemilik = $brw_users->txt_nm_pemilik_bdn_hukum;
            $ahliWaris->brw_kd_bank = $brw_users->txt_bank_bdn_hukum;
            $ahliWaris->update();

            echo "sukses";
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Daftar Penerima Pendanaan";
        $audit->description = "Ubah data Penerima pendanaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();
    }

    public function ListTableApproveDana()
    {
        return view('pages.admin.list_approve_dana');
    }

    public function listApproveDana()
    {

        // $listApproveDana = DB::table('brw_pendanaan AS a')
        // ->join('log_akad_digisign_borrower AS b', 'a.id_proyek', '=', 'b.id_proyek')
        // ->join('brw_rekening AS c', 'a.brw_id', '=', 'c.brw_id')
        // //->selectRaw('a.brw_id, a.id_proyek, a.pendanaan_nama, a.pendanaan_dana_dibutuhkan, a.status as status_pendanaan, a.metode_pembayaran, a.dana_dicairkan, a.status_dana, b.brw_id, b.id_proyek, b.status as status_akad, c.brw_norek, c.brw_kd_bank, sum(b.total_pendanaan) as total_dana_terkumpul')
        // //->join('log_akad_digisign_borrower AS b', 'a.id_proyek', '=', 'b.id_proyek')
        // ->join('pendanaan_aktif AS d', 'd.proyek_id', '=', 'a.id_proyek')
        // ->selectRaw('a.brw_id, a.id_proyek, a.pendanaan_nama, a.pendanaan_dana_dibutuhkan, a.status as status_pendanaan, a.status_dana, a.dana_dicairkan, b.status as status_akad, c.brw_norek, c.brw_kd_bank, sum(d.total_dana) as total_dana_terkumpul')
        // ->where('b.status','=', "complete")
        // ->where('a.status_dana','=', 2)
        // ->get();

        $listApproveDana = DB::table('brw_pendanaan AS a')
            ->join('log_akad_digisign_borrower AS b', 'a.id_proyek', '=', 'b.id_proyek')
            //->join('pendanaan_aktif AS c', 'c.proyek_id', '=', 'a.id_proyek')
            ->selectRaw('a.brw_id, a.id_proyek, a.pendanaan_nama, a.pendanaan_dana_dibutuhkan, a.status_dana, a.dana_dicairkan, b.status as status_akad')
            ->where(\DB::raw('substr(b.document_id, 1, 10)'), '=', 'kontrakAll')
            ->where('b.status', '=', "complete")
            ->where('a.status_dana', '=', 2)->get();


        $dataArray = array();

        if (!empty($listApproveDana)) {
            foreach ($listApproveDana as $item) {
                $sumPendanaanAktif = PendanaanAktif::where('proyek_id', $item->id_proyek)->sum('total_dana');

                $column = array();
                $column[] = (string)$item->brw_id;
                $column[] = (string)$item->id_proyek;
                $column[] = (string)$item->pendanaan_nama;
                $column[] = (string)$item->pendanaan_dana_dibutuhkan;
                //$column[] = (string)$item->metode_pembayaran;
                $column[] = (string)$sumPendanaanAktif;
                //$column[] = (string)$item->status_pendanaan;
                //$column[] = (string)$item->status_dana;
                //$column[] = (string)$item->status_akad;
                //$column[] = (string)$item->dana_dicairkan;
                //$column[] = (string)$item->brw_norek;
                //$column[] = (string)$item->brw_kd_bank;

                $dataArray[] = $column;
            }
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function prosesCairDana(Request $request)
    {

        $Rekening = DB::table('brw_rekening')
            ->where('brw_rekening.brw_id', '=', $request->brw_id)
            ->first();

        $total_plafon = $Rekening->total_plafon;
        $total_sisa = $Rekening->total_sisa;
        $total_terpakai = $Rekening->total_terpakai;

        // insert data pengurus
        $pendanaan = BorrowerPendanaan::where('brw_id', $request->brw_id)->where('id_proyek', $request->id_proyek)->first();
        $pendanaan->status = 7;
        $pendanaan->status_dana = 1;
        $pendanaan->update();

        // summary pendaan
        $pendanaanAktif = DB::table('pendanaan_aktif')
            ->where('proyek_id', $request->id_proyek)->sum('total_dana');

        // update plafon
        $plafon_terpakai         = $total_terpakai + $pendanaanAktif;
        $plafon_sisa             = $total_plafon -  $plafon_terpakai;

        // update plafon
        $Rekening_update = BorrowerRekening::where('brw_id', $request->brw_id)->first();
        $Rekening_update->total_terpakai = $plafon_terpakai;
        $Rekening_update->total_sisa = $plafon_sisa;
        $Rekening_update->update();

        return response()->json(['status' => "sukses"]);

        //$client = new Client();
        //$request = $client->post(config('app.apilink')."/borrower/proses_pendanaan",[
        //	'form_params' =>
        //	[
        //		"brw_id"			=> $request->brw_id,
        //		"proyek_id"			=> $request->proyek_id
        //  ]
        //]);

        //$response = json_decode($request->getBody()->getContents());

        //dd($response);
        //echo $request->getBody();



    }

    // public function inquiryTransfer($bank_rdl_code,$accno_src,$accno_dest,$currency,$amount,$berita_transfer,$bank_code_dest){
    public function inquiryTransfer(Request $request)
    {



        $accno_src = config('app.bnik_main_account');
        // proses update
        $Rekening = DB::table('brw_rekening')
            ->where('brw_rekening.brw_id', '=', $request->brw_id)
            ->first();

        $total_plafon = $Rekening->total_plafon;
        $total_sisa = $Rekening->total_sisa;
        $total_terpakai = $Rekening->total_terpakai;

        $accno_dest = $Rekening->brw_norek;
        $amount = $request->dana_dicairkan;
        $currency = 'IDR';
        $berita_transfer = 'test';
        $bank_code_dest = $Rekening->brw_kd_bank;
        $bank_rdl_code = '009';

        $client = new RDLController();

        if ($bank_rdl_code == $bank_code_dest) {

            // insert data pengurus
            $pendanaan = BorrowerPendanaan::where('brw_id', $request->brw_id)->where('id_proyek', $request->id_proyek)->first();
            $pendanaan->status = 7;
            $pendanaan->status_dana = 1;
            $pendanaan->update();


            // update plafon
            $plafon_terpakai         = $total_terpakai + $amount;
            $plafon_sisa             = $total_plafon -  $plafon_terpakai;

            // update plafon
            BorrowerRekening::where('brw_id', $request->brw_id)->update(['total_terpakai' => $plafon_terpakai, 'total_sisa' => $plafon_sisa]);

            return $client->inquiryAccountInfo($bank_rdl_code, $accno_dest); // proses transfer

        } else {

            // insert data pengurus
            $pendanaan = BorrowerPendanaan::where('brw_id', $request->brw_id)->where('id_proyek', $request->id_proyek)->first();
            $pendanaan->status = 7;
            $pendanaan->status_dana = 1;
            $pendanaan->update();


            // update plafon
            $plafon_terpakai         = $total_terpakai + $amount;
            $plafon_sisa             = $total_plafon -  $plafon_terpakai;

            // update plafon
            BorrowerRekening::where('brw_id', $request->brw_id)->update(['total_terpakai' => $plafon_terpakai, 'total_sisa' => $plafon_sisa]);

            return $client->inquiryTransferTransactionOnline($bank_rdl_code, $accno_src, $bank_code_dest, $accno_dest);
        }
    }

    public function executeTransfer($bank_rdl_code, $accno_src, $accno_dest, $currency, $amount, $berita_transfer, $bank_code_dest)
    {
        $client = new RDLController();
        if ($bank_rdl_code == $bank_code_dest) {
            return $client->executeTransferTransactionOverbooking($bank_rdl_code, $accno_src, $accno_dest, $currency, $amount, $berita_transfer);
        } else {
            // 0 sd 20jt online
            // 20jt > 1m Sistem Kliring Nasional
            // 1m > RTGS
            $limit1 = 20000000;
            $limit2 = 1000000000;

            if ($amount <= $limit1) {
                return $client->executeTransferTransactionOnline($bank_rdl_code, $accno_src, $bank_code_dest, $accno_dest, $accname_dest, $currency, $amount);
            } elseif ($amount > $limit1 && $amount <= $limit2) {
                return $client->executeTransferTransactionKliring($bank_rdl_code, $accno_src, $bank_code_dest, $accno_dest, $accname_dest, $beneficiaryAddress1, $beneficiaryAddress2, $beneficiaryAddress3, $currency, $amount, $berita_transfer);
            } elseif ($amount > $limit2) {
                return $client->executeTransferTransactionRTGS($bank_rdl_code, $accno_src, $bank_code_dest, $accno_dest, $accname_dest, $beneficiaryAddress1, $beneficiaryAddress2, $currency, $amount, $berita_transfer);
            }
        }
    }

    public function show_menu_audit_trail()
    {
        return view('pages.admin.audit_trail');
    }

    public function datatables_audit_trail()
    {
        $audit = AuditTrail::all();

        $response = ['data' => $audit];

        return response()->json($response);
    }

    public function ManageTestimoniPendana()
    {
        return view('pages.admin.manage_testimoni_pendana');
    }

    public function ManageMediaOnline()
    {
        return view('pages.admin.manage_media_online');
    }

    public function ManageInformasiPerusahaan()
    {
        return view('pages.admin.manage_informasi_perusahaan');
    }

    public function ManageDisclaimerOJK()
    {
        return view('pages.admin.manage_disclaimer_ojk');
    }

    public function ManageTermCondition()
    {
        return view('pages.admin.manage_term_condition');
    }

    public function ManagePrivacyPolicy()
    {
        return view('pages.admin.manage_privacy_policy');
    }

    public function ManageFAQ()
    {
        return view('pages.admin.manage_faq');
    }

    public function admin_add_testimoni(Request $request)
    {

        $testimoni = new TestimoniPendana;

        $testimoni->nama = $request->nama_pendana;
        $testimoni->content = $request->testimoni;
        //$testimoni->link = $request->link_testimoni;
        $testimoni->gambar = $this->upload_foto_pendana('foto_pendana', $request);
        $testimoni->type = 1;
        $testimoni->author = Auth::guard('admin')->user()->firstname;
        
        if ($testimoni->gambar) {
            $testimoni->save();
        //$testimoni->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Tambah Testimoni Pendana";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        //return redirect()->back()->with('success', 'Tambah Testimoni Pendana Berhasil');
            return redirect()->back()->with('success', 'Tambah Testimoni Berhasil');
        }else{
            return redirect()->back()->with('Failed', 'Tambah Testimoni gagal');    
        }
    }

    private function upload_foto_pendana($column, Request $request)
    {
        $this->validate($request, [

            'foto_pendana.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ],
        [
            'foto_pendana.image' => 'gunakan file gambar',
            'foto_pendana.mimes' => 'gunakan file jpeg,png,jpg,gif,svg',
        ]
        );
        if ($request->hasFile($column)) {            
            $file = $request->file($column);
            $filename = Carbon::now()->format('Y-m-d H_i_s') . $request->foto_pendana->getClientOriginalName();
            $store_path = 'pendana';
            $path = $file->storeAs($store_path, $filename, 'public');
            return $path;
        } else {
            return null;
        }
    }

    public function datatables_testimoni()
    {
        $testimoni = TestimoniPendana::where('type', 1)->get();

        $response = ['data' => $testimoni];

        return response()->json($response);
    }

    public function admin_update_testimoni(Request $request)
    {

        $testimoni = TestimoniPendana::where('id', $request->id)->first();
        // echo $request->edit_tipe;die;

        $testimoni->nama = $request->edit_nama;
        $testimoni->content = $request->edit_content;
        //$testimoni->link = $request->edit_link;
        //$testimoni->gambar = $this->upload_foto_pendana('foto_pendana', $request);
        if ($request->hasFile('foto_pendana')) {
            Storage::disk('public')->delete($testimoni->gambar);
            $testimoni->gambar = $this->upload_foto_pendana('foto_pendana', $request);
            if ($testimoni->gambar) {
                $testimoni->save();
            }
            //$testimoni->save();
        }else{
            $testimoni->save();
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Edit Testimoni Pendana";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('updated', "Ubah Testimoni Berhasil");
    }

    public function admin_delete_testimoni($id)
    {

        $testimoni = TestimoniPendana::where('id', $id)->first();

        $testimoni_delete = TestimoniPendana::where('id', $id)->delete();

        if ($testimoni_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Hapus Testimoni Pendana";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function admin_add_media_online(Request $request)
    {

        $media = new TestimoniPendana;
        $media->link = $request->add_link;
        $media->gambar = $this->upload_media_online('gambar', $request);
        $media->type = 2;
        $media->author = Auth::guard('admin')->user()->firstname;
        //dd($media->link);
        if ($media->gambar) {
            $media->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Tambah Media Online";
        $audit->ip_address =  \Request::ip();
        $audit->save();

            return redirect()->back()->with('success', 'Tambah Media Online Berhasil');
        }else{
            return redirect()->back()->with('Failed', 'Tambah Media Online gagal');    
        }
    }

    private function upload_media_online($column, Request $request)
    {
        $this->validate($request, [

            'gambar.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ],
        [
            'gambar.image' => 'gunakan file gambar',
            'gambar.mimes' => 'gunakan file jpeg,png,jpg,gif,svg',
        ]
        );
        if ($request->hasFile($column)) {            
            $file = $request->file($column);
            // $filename = Carbon::now()->toDateString() . $request->gambar->getClientOriginalName();
            $filename = Carbon::now()->format('Y-m-d H_i_s') . $request->gambar->getClientOriginalName();
            $store_path = 'media';
            $path = $file->storeAs($store_path, $filename, 'public');
            return $path;
        } else {
            return null;
        }
    }

    public function datatables_media_online()
    {
        $media = DB::table('cms')->where('type', 2)->get();
        $response = ['data' => $media];

        return response()->json($response);
    }

    public function admin_update_media_online(Request $request)
    {
        //return ($request);
        $media = TestimoniPendana::where('id', $request->id)->first();
        if ($request->hasFile('gambar')) {
            Storage::disk('public')->delete($media->gambar);
            $media->link = $request->edit_link;
            $media->gambar = $this->upload_media_online('gambar', $request);
            if ($media->gambar) {
                $media->save();
            }
        }else{
            $media->link = $request->edit_link;
            $media->save();
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Edit Media Online";
        $audit->ip_address =  \Request::ip();
        $audit->save();


        return redirect()->back()->with('updated', "Ubah Media Online Berhasil");
    }

    public function admin_delete_media_online($id)
    {

        $media = TestimoniPendana::where('id', $id)->first();

        $media_delete = TestimoniPendana::where('id', $id)->delete();

        if ($media_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Hapus Media Online";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function admin_add_company(Request $request)
    {

        $company = new TestimoniPendana;

        $company->nama = $request->nama_perusahaan;
        $company->content = $request->deskripsi;
        $company->alamat = $request->alamat;
        $company->email = $request->email;
        $company->phone = $request->phone;
        $count_whatsapp = implode('|', $request->whatsapp);
        $company->handphone .= '|' . $count_whatsapp;
        $company->type = 3;
        $company->author = Auth::guard('admin')->user()->firstname;
        //echo"<pre>";print_r($company);"</pre>";die();
        $company->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Tambah Informasi Perusahaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Tambah Informasi Perusahaan Berhasil');
    }

    public function datatables_company()
    {
        $company = DB::table('cms')->where('type', 3)->get();

        $response = ['data' => $company];

        return response()->json($response);
    }

    public function admin_delete_company($id)
    {

        $company = TestimoniPendana::where('id', $id)->first();

        $company_delete = TestimoniPendana::where('id', $id)->delete();

        if ($company_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Hapus Informasi Perusahaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function admin_update_company(Request $request)
    {

        $company = TestimoniPendana::where('id', $request->id)->first();
        $company->nama = $request->edit_nama_perusahaan;
        $company->content = $request->edit_deskripsi;
        $company->alamat = $request->edit_alamat;
        $company->email = $request->edit_email;
        $company->phone = $request->edit_phone;
        $company->handphone = $request->edit_handphone;
        $company->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Edit Informasi Perusahaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('updated', "Ubah Informasi Perusahaan Berhasil");
    }

    public function admin_add_disclaimer(Request $request)
    {

        $disclaimer = new TestimoniPendana;

        $disclaimer->content = $request->deskripsi;
        $disclaimer->type = 4;
        $disclaimer->author = Auth::guard('admin')->user()->firstname;;
        $disclaimer->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Tambah Disclaimer";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Tambah Disclaimer Berhasil');
    }

    public function datatables_disclaimer()
    {
        $disclaimer = DB::table('cms')->where('type', 4)->get();

        $response = ['data' => $disclaimer];

        return response()->json($response);
    }

    public function admin_update_disclaimer(Request $request)
    {

        $disclaimer = TestimoniPendana::where('id', $request->id)->first();
        $disclaimer->content = $request->edit_deskripsi;
        $disclaimer->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Edit Disclaimer";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('updated', "Ubah Disclaimer Berhasil");
    }

    public function admin_delete_disclaimer($id)
    {

        $disclaimer = TestimoniPendana::where('id', $id)->first();

        $disclaimer_delete = TestimoniPendana::where('id', $id)->delete();

        if ($disclaimer_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Kelola Kontent Manajemen Website";
        $audit->description = "Hapus Disclaimer";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function admin_add_term_condition(Request $request)
    {

        $term = new TestimoniPendana;
        $term->title = $request->title;
        $term->content = $request->deskripsi;
        $term->type = 5;

        $term->save();

        return redirect()->back()->with('success', 'Tambah Term & Condition Berhasil');
    }

    public function datatables_termcondition()
    {
        $term = DB::table('cms')->where('type', 5)->get();

        $response = ['data' => $term];

        return response()->json($response);
    }

    public function admin_update_term_condition(Request $request)
    {

        $term = TestimoniPendana::where('id', $request->id)->first();
        $term->title = $request->edit_title;
        $term->content = $request->edit_deskripsi;
        $term->save();
        return redirect()->back()->with('updated', "Ubah Term & Condition Berhasil");
    }

    public function admin_delete_term_condition($id)
    {

        $term = TestimoniPendana::where('id', $id)->first();

        $term_delete = TestimoniPendana::where('id', $id)->delete();

        if ($term_delete) {
            $response = ['status' => 'Sukses'];
        }

        return response()->json($response);
    }

    public function VerifikasiPembayaran()
    {
        $client = new client();

        $response_getDataPendana = $client->request('GET', config('app.apilink') . "/borrower/dataCair");
        $body_getDataPendana = json_decode($response_getDataPendana->getBody());
        $data = (array) $body_getDataPendana->data;
        // dd($body_getDataPendana);

        return view('pages.admin.verifikasipembayaran')->with('dataPendanaan', $data);;
    }

    // public function pendanaanVerifikasiPembayaran(){

    // }

    public function admin_add_privacy(Request $request)
    {

        $privacy = new TestimoniPendana;
        $privacy->title = $request->title;
        $privacy->file = $this->upload_kebijakan_privacy('file', $request);
        $privacy->type = 6;
        $privacy->publish = $request->publish;
        $privacy->author = Auth::guard('admin')->user()->firstname;
        //echo"<pre>";print_r($privacy);echo"</pre>";die();
        $privacy->save();

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Konten Manajemen Website";
        $audit->description = "Tambah Kebijakan Privacy";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Tambah Kebijakan Privacy Berhasil');
    }

    public function datatables_privacy()
    {
        $privacy = DB::table('cms')->where('type', 6)->get();

        $response = ['data' => $privacy];

        return response()->json($response);
    }

    private function upload_kebijakan_privacy($column, Request $request)
    {
        if ($request->hasFile($column)) {
            $file = $request->file($column);
            $filename = $request->file->getClientOriginalName();
            $store_path = 'footer';
            $path = $file->storeAs($store_path, $filename, 'public');
            return $path;
        } else {
            return null;
        }
    }

    public function admin_update_privacy(Request $request)
    {

        $privacy = TestimoniPendana::where('id', $request->id)->first();
        $privacy->title = $request->title;
        $privacy->publish = $request->publish;
        if ($request->hasFile('file')) {
            Storage::disk('public')->delete($privacy->file);
            $privacy->file = $this->upload_kebijakan_privacy('file', $request);

            $privacy->save();
        }
        $privacy->save();


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Konten Manajemen Website";
        $audit->description = "Edit Kebijakan Privacy";
        $audit->ip_address =  \Request::ip();
        $audit->save();


        return redirect()->back()->with('updated', "Ubah Kebijakan Privacy Berhasil");
    }

    public function admin_delete_privacy($id)
    {

        $privacy = TestimoniPendana::where('id', $id)->first();

        $privacy_delete = TestimoniPendana::where('id', $id)->delete();

        if ($privacy_delete) {
            $response = ['status' => 'Sukses'];
        }

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Konten Manajemen Website";
        $audit->description = "Hapus Kebijakan Privacy";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return response()->json($response);
    }

    public function admin_upload_dokumen(Request $request)
    {



        $a = count($request->dokumen);
        $author = Auth::guard('admin')->user()->firstname;
        $uploads = $this->upload_dokumen_brw('file', $request);

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Data Penerima Pendanaan";
        $audit->description = "Unggah Dokumen Penerima Pendanaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        if ($uploads['status_code'] == "00") {
            return redirect()->back()->with('success', 'Unggah ' . $uploads['num_uploaded_file'] . ' Dokumen Penerima Pendanaan Berhasil');
        } else {
            return redirect()->back()->with('success', 'Unggah Dokumen Penerima Pendanaan Gagal');
        }
    }


    private function upload_dokumen_brw($column, Request $request)
    {

        $cnt = 0;

        $return1 = array(
            "status_code" => "00",
            "status_message" => 'Upload File',
            "num_uploaded_file" => $cnt

        );

        if ($request->hasFile(($column))) {

            $files = $request->file($column);
            $client = new DMSController();
            $id_brw = $request->brw_id[0];
            $author = Auth::guard('admin')->user()->firstname;
            $brw_type =  $request->brw_type;

            if ($brw_type == 1) {
                /* METHODE UNTUK FOLDER */
                $client->createFolder("BORROWER", "PERSONAL", "WNI", $request->ktp);
                $client->createFolder("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$id_brw");
                $client->createFolder("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$id_brw/DOKUMEN");
                $jmlhFile = count($files);
            } elseif ($brw_type == 2) {
                /* METHODE UNTUK FOLDER */
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", $request->ktp_bdn_hukum);
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", "$request->ktp_bdn_hukum/BRW_$id_brw");
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", "$request->ktp_bdn_hukum/BRW_$id_brw/DOKUMEN");
                $jmlhFile = count($files);
            }

            // foreach($files as $file)
            for ($x = 0; $x < $jmlhFile; $x++) {
                if ($brw_type == 1) {
                    $result_uploads = $client->uploadFile("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$id_brw/DOKUMEN", $files[$x]->getClientOriginalName(), file_get_contents($files[$x]->path()));
                    $r = json_decode($result_uploads);
                } elseif ($brw_type == 2) {
                    $result_uploads = $client->uploadFile("BORROWER", "CORPORATE", "LOCAL", "$request->ktp_bdn_hukum/BRW_$id_brw/DOKUMEN", $files[$x]->getClientOriginalName(), file_get_contents($files[$x]->path()));
                    $r = json_decode($result_uploads);
                }


                if ($r->status_code == '00') {

                    $upload = new DokumenBorrower;
                    $upload->brw_id =  $request->brw_id[0];
                    $upload->jenis_dokumen =  $request->dokumen[$x];
                    $upload->nama_dokumen = $files[$x]->getClientOriginalName();
                    $upload->path_file =  $r->uuid;
                    $upload->author = $author;
                    //echo"<pre>";print_r($upload);echo"</pre>";die();
                    $upload->save();
                    $cnt++;

                    $return1 = array(
                        "status_code" => "00",
                        "status_message" => 'Upload File',
                        "num_uploaded_file" => $cnt

                    );
                }
            }

            return $return1;
        } else {
            return null;
        }
    }

    public function datatables_list_dokumen($id)
    {


        $dokumen = DokumenBorrower::where('brw_id', $id)->get();
        $response = ['data' => $dokumen];

        return response()->json($response);
    }

    public function admin_get_dokumen($id)
    {
        $client = new DMSController();
        $response = json_decode($client->downloadFile($id));
        $content = $response->content;
        $content_decode = base64_decode($content);

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Data Penerima Pendanaan";
        $audit->description = "Download Dokumen Penerima Pendanaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();


        return response($content_decode)->header('Content-Type', 'application/pdf');
    }

    public function tarik_dana_investor()
    {
        return view('pages.admin.list_tarik_dana');
    }

    public function list_tarik_dana_investor()
    { // admin/manage proyek mutasi				

        // $list_proyek = Proyek::whereIn('status', [2,3])->orderBy('tgl_selesai_penggalangan', 'desc')->get();

        $list_proyek = Proyek::select('proyek.id', 'proyek.nama', 'proyek.total_need', 'proyek.tgl_mulai_penggalangan', 'proyek.tgl_selesai_penggalangan', 'proyek.status')
            ->join('brw_pendanaan', 'brw_pendanaan.id_proyek', '=', 'proyek.id')
            ->whereIn('proyek.status', [2, 3])
            ->where('brw_pendanaan.status_dana', 0)
            ->orderBy('tgl_selesai_penggalangan', 'desc')->get();


        $data = array();
        $no = 1;
        if (!empty($list_proyek)) {
            foreach ($list_proyek as $row) {
                if ($row->status == 3) {
                    $status_proyek = 'Pendanaan Terpenuhi';
                } else {
                    $status_proyek = 'Penggalangan Selesai';
                }

                $column['ID Proyek']                     = $row->id;
                $column['Nama Proyek']                     = (string)$row->nama;
                $column['Total Dibutuhkan']             = number_format($row->total_need);
                $column['Tanggal Mulai Penggalangan']     = $row->tgl_mulai_penggalangan;
                $column['Tanggal Selesai Penggalangan'] = $row->tgl_selesai_penggalangan;
                $column['Status Proyek']                 = $status_proyek;
                $column['Detil Tarik Dana']             = $row->id;

                $data[] = $column;
            }
        }

        $parsingJSON = array(
            "data" => $data
        );
        echo json_encode($parsingJSON);
    }

    public function list_detail_investor($id_proyek)
    {

        $detail_investor = DB::table('pendanaan_aktif as a')
            ->selectRaw('a.investor_id, a.proyek_id, SUM(a.total_dana) as total_investasi, b.nama_investor, c.email')
            ->join('detil_investor AS b', 'a.investor_id', '=', 'b.investor_id')
            ->join('investor AS c', 'c.id', '=', 'a.investor_id')
            ->where('proyek_id', '=', $id_proyek)
            ->groupBy('investor_id')
            ->get();

        $data = array();
        $no = 1;
        if (!empty($detail_investor)) {
            foreach ($detail_investor as $row) {
                //var_dump();
                $column['Proyek_id']                = (string)$row->proyek_id;
                $column['Investor_id']              = (string)$row->investor_id;
                $column['Nama Investor Pendana']     = (string)$row->nama_investor;
                $column['Email Pendana']             = (string)$row->email;
                $column['Total Pendanaan']          = (string)number_format($row->total_investasi);

                $data[] = $column;
            }
        }
        $parsingJSON = array(
            "data" => $data
        );

        echo json_encode($parsingJSON);
    }

    public function tarik_dana(Request $request, $id)
    {
        $client = new RDLController();

        $detail_investor = DB::table('pendanaan_aktif as a')
            ->selectRaw('a.investor_id, a.proyek_id, SUM(a.total_dana) as total_investasi, b.nama_investor, c.va_number, d.account_number')
            ->join('detil_investor AS b', 'a.investor_id', '=', 'b.investor_id')
            ->join('rekening_investor AS c', 'c.investor_id', '=', 'a.investor_id')
            ->join('rdl_acount_number AS d', 'd.investor_id', '=', 'a.investor_id')
            ->where('proyek_id', '=', $id)
            ->groupBy('investor_id')
            ->get();

        $berita_transfer = 'Penarikan dana Pendana Proyek ' . $id;

        for ($i = 0; $i < sizeof($detail_investor); $i++) {
            $withdraw_investor = $client->ExecuteTransferTransactionOverbooking((string)$detail_investor[$i]->account_number, (string)'009', (string)$detail_investor[$i]->va_number, (string)number_format($detail_investor[$i]->total_investasi, 0, '', ''), (string)$berita_transfer);
        }

        // $withdraw_investor = $client->ExecuteTransferTransactionOverbooking((string)$detail_investor[0]->account_number, (string)'009', (string)$detail_investor[0]->va_number, (string)number_format($detail_investor[0]->total_investasi,0,'',''), (string)$berita_transfer);

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Tarik Dana Pendana";
        $audit->description = "Tarik Dana Pendana ke VA Proyek " . $id;
        $audit->ip_address =  \Request::ip();
        $audit->save();

        $pendanaan = BorrowerPendanaan::where('id_proyek', $id)->first();
        $pendanaan->status_dana = 2;
        $pendanaan->update();

        return redirect()->back()->with('success', 'Tarik Dana Berhasil');
        // $response = [
        //     'status' => 'Sukses'
        // ];
        // return response()->json($response);
    }

    public function transfer_dana_pendana()
    {
        return view('pages.admin.list_transfer_dana');
    }

    public function list_transfer_dana_investor()
    { // admin/manage proyek mutasi				

        $list_proyek = Proyek::where('status', 4)->orderBy('tgl_selesai_penggalangan', 'desc')
            ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana group by proyek_id')])
            ->get();


        $data = array();
        $no = 1;
        if (!empty($list_proyek)) {
            foreach ($list_proyek as $row) {
                $status_proyek = 'Proyek Selesai';

                $column['ID Proyek']                     = $row->id;
                $column['Nama Proyek']                     = (string)$row->nama;
                $column['Total Dibutuhkan']             = number_format($row->total_need);
                $column['Tanggal Mulai Penggalangan']     = $row->tgl_mulai_penggalangan;
                $column['Tanggal Selesai Penggalangan'] = $row->tgl_selesai_penggalangan;
                $column['Status Proyek']                 = $status_proyek;
                $column['Detil Tarik Dana']             = $row->id;

                $data[] = $column;
            }
        }

        $parsingJSON = array(
            "data" => $data
        );
        echo json_encode($parsingJSON);
    }

    public function list_detail_transfer_investor($id_proyek)
    {

        $detail_investor = DB::table('pendanaan_aktif as a')
            ->selectRaw('a.investor_id, a.proyek_id, SUM(a.total_dana) as total_investasi, b.nama_investor, c.email')
            ->join('detil_investor AS b', 'a.investor_id', '=', 'b.investor_id')
            ->join('investor AS c', 'c.id', '=', 'a.investor_id')
            ->where('proyek_id', '=', $id_proyek)
            ->groupBy('investor_id')
            ->get();

        $data = array();
        $no = 1;
        if (!empty($detail_investor)) {
            foreach ($detail_investor as $row) {
                //var_dump();
                $column['Proyek_id']                = (string)$row->proyek_id;
                $column['Investor_id']              = (string)$row->investor_id;
                $column['Nama Investor Pendana']     = (string)$row->nama_investor;
                $column['Email Pendana']             = (string)$row->email;
                $column['Total Pendanaan']          = (string)number_format($row->total_investasi);

                $data[] = $column;
            }
        }
        $parsingJSON = array(
            "data" => $data
        );

        echo json_encode($parsingJSON);
    }

    public function transfer_dana(Request $request, $id)
    {
        $client = new RDLController();
        $accno_src = config('app.bnik_main_account');

        $detail_investor = DB::table('pendanaan_aktif as a')
            ->selectRaw('a.investor_id, a.proyek_id, SUM(a.total_dana) as total_investasi, b.nama_investor, c.va_number, d.account_number')
            ->join('detil_investor AS b', 'a.investor_id', '=', 'b.investor_id')
            ->join('rekening_investor AS c', 'c.investor_id', '=', 'a.investor_id')
            ->join('rdl_acount_number AS d', 'd.investor_id', '=', 'a.investor_id')
            ->where('proyek_id', '=', $id)
            ->groupBy('investor_id')
            ->get();

        $berita_transfer = 'Transfer dana ke Account Number Pendana Proyek ' . $id;

        for ($i = 0; $i < sizeof($detail_investor); $i++) {
            $withdraw_investor = $client->ExecuteTransferTransactionOverbooking((string)$accno_src,  (string)'009', (string)$detail_investor[$i]->account_number, (string)number_format($detail_investor[$i]->total_investasi, 0, '', ''), (string)$berita_transfer);

            $log_pengembalian_dana = new LogPengembalianDana;
            $log_pengembalian_dana->proyek_id = $id;
            $log_pengembalian_dana->investor_id = $detail_investor[$i]->investor_id;
            $log_pengembalian_dana->nominal = $detail_investor[$i]->total_investasi;
            $log_pengembalian_dana->save();

            $update_rekening = RekeningInvestor::where('investor_id', $detail_investor[$i]->investor_id)->first();
            $update_rekening->unallocated += $detail_investor[$i]->total_investasi;
            $update_rekening->update();
        }

        // $withdraw_investor = $client->ExecuteTransferTransactionOverbooking((string)$detail_investor[0]->account_number, (string)'009', (string)$detail_investor[0]->va_number, (string)number_format($detail_investor[0]->total_investasi,0,'',''), (string)$berita_transfer);

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Transfer Dana Pendana";
        $audit->description = "Transfer dana ke Account Number Pendana Proyek " . $id;
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return redirect()->back()->with('success', 'Transfer Dana Berhasil');
        // $response = [
        //     'status' => 'Sukses'
        // ];
        // return response()->json($response);
    }

    public function admin_upload_dokumen_scoring_pendanaan(Request $request)
    {


        $author = Auth::guard('admin')->user()->firstname;
        $uploads = $this->upload_dokumen_scoring_pendanaan('file', $request);


        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Penilaian Penerima Pendanaan";
        $audit->description = "Unggah Dokumen Penilaian Pendanaan Penerima Pendana";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        if ($uploads['status_code'] == "00") {
            return redirect()->back()->with('success', 'Unggah Dokumen Penilaian Pendanaan Berhasil');
        } else {
            return redirect()->back()->with('success', 'Unggah Dokumen Penilaian Pendanaan Gagal');
        }
    }


    private function upload_dokumen_scoring_pendanaan($column, Request $request)
    {
        $return1 = array(
            "status_code" => "00",
            "status_message" => 'Upload File'

        );

        if ($request->hasFile(($column))) {

            $client = new DMSController();
            $file = $request->file($column);
            $author = Auth::guard('admin')->user()->firstname;
            $brw_type = $request->brw_type;
            if ($brw_type == 1) {
                /* METHODE UNTUK CREATE FOLDER */
                $client->createFolder("BORROWER", "PERSONAL", "WNI", $request->ktp);
                $client->createFolder("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$request->brw_id");
                $client->createFolder("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$request->brw_id/SCORING_PENDANAAN_$request->pendanaan_id");

                $upload = $client->uploadFile("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$request->brw_id/SCORING_PENDANAAN_$request->pendanaan_id", $file->getClientOriginalName(), file_get_contents($file->path()));
                $r = json_decode($upload);
            } else {
                /* METHODE UNTUK CREATE FOLDER */
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", $request->ktp);
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", "$request->ktp/BRW_$request->brw_id");
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", "$request->ktp/BRW_$request->brw_id/SCORING_PENDANAAN_$request->pendanaan_id");

                $upload = $client->uploadFile("BORROWER", "CORPORATE", "LOCAL", "$request->ktp/BRW_$request->brw_id/SCORING_PENDANAAN_$request->pendanaan_id", $file->getClientOriginalName(), file_get_contents($file->path()));
                $r = json_decode($upload);
            }

            if ($r->status_code == '00') {

                $upload = new DokumenBorrower;
                $upload->brw_id =  $request->brw_id;
                $upload->pendanaan_id =  $request->pendanaan_id;
                $upload->scoring_type =  2;
                $upload->jenis_dokumen =  $request->nama_dokumen;
                $upload->nama_dokumen = $file->getClientOriginalName();
                $upload->path_file =  $r->uuid;
                $upload->author = $author;
                $upload->save();

                $return1 = array(
                    "status_code" => "00",
                    "status_message" => 'Upload File'

                );
            }

            return $return1;
        } else {
            return null;
        }
    }

    public function admin_get_dokumen_scoring($id)
    {
        $check_file_scoring = DB::table('brw_dokumen as a')
            ->selectRaw('a.brw_id, a.pendanaan_id, a.path_file')
            ->where('pendanaan_id', '=', $id)
            ->where('scoring_type', '=', 2)
            ->count();

        if ($check_file_scoring > 0) {
            $file_scoring = DB::table('brw_dokumen as a')
                ->selectRaw('a.brw_id, a.pendanaan_id, a.path_file')
                ->where('pendanaan_id', '=', $id)
                ->where('scoring_type', '=', 2)
                ->get();

            $scoring_file = $file_scoring[0]->path_file;

            $client = new DMSController();
            $response = json_decode($client->downloadFile($scoring_file));
            $content = $response->content;
            $content_decode = base64_decode($content);

            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Penilaian Penerima Pendanaan";
            $audit->description = "Unduh Dokumen Penilaian Pendanaan Penerima Pendana";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return response($content_decode)->header('Content-Type', 'application/pdf');
        } else {
            return redirect()->back()->with('error', 'Maaf, tidak ada data yang tersedia. Silahkan anda unggah kembali');
        }
    }

    public function admin_upload_dokumen_scoring_personal(Request $request)
    {


        $author = Auth::guard('admin')->user()->firstname;
        //$uploads = json_decode($this->upload_dokumen_scoring_personal('file', $request));
        //$result_uploads = $uploads->uuid;
        $uploads = $this->upload_dokumen_scoring_personal('file', $request);

        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "Penilaian Penerima Pendanaan";
        $audit->description = "Unggah Dokumen Penilaian Personal Penerima Pendana";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        //return redirect()->back()->with('success', 'Unggah Dokumen Penilaian Personal Berhasil');
        if ($uploads['status_code'] == "00") {
            return redirect()->back()->with('success', 'Unggah Dokumen Penilaian Personal Berhasil');
        } else {
            return redirect()->back()->with('success', 'Unggah Dokumen Penilaian Personal Gagal');
        }
    }

    private function upload_dokumen_scoring_personal($column, Request $request)
    {
        $return1 = array(
            "status_code" => "00",
            "status_message" => 'Upload File'

        );

        if ($request->hasFile(($column))) {

            $client = new DMSController();
            $file = $request->file($column);
            $author = Auth::guard('admin')->user()->firstname;
            $brw_type = $request->brw_type;

            if ($brw_type == 1) {

                /* METHODE UNTUK CREATE FOLDER */
                $client->createFolder("BORROWER", "PERSONAL", "WNI", $request->ktp);
                $client->createFolder("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$request->brw_id");
                $client->createFolder("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$request->brw_id/SCORING_PERSONAL_$request->pendanaan_id");

                $upload = $client->uploadFile("BORROWER", "PERSONAL", "WNI", "$request->ktp/BRW_$request->brw_id/SCORING_PERSONAL_$request->pendanaan_id", $file->getClientOriginalName(), file_get_contents($file->path()));
                $r = json_decode($upload);
            } else {
                /* METHODE UNTUK CREATE FOLDER */
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", $request->ktp);
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", "$request->ktp/BRW_$request->brw_id");
                $client->createFolder("BORROWER", "CORPORATE", "LOCAL", "$request->ktp/BRW_$request->brw_id/SCORING_PERSONAL_$request->pendanaan_id");

                $upload = $client->uploadFile("BORROWER", "CORPORATE", "LOCAL", "$request->ktp/BRW_$request->brw_id/SCORING_PERSONAL_$request->pendanaan_id", $file->getClientOriginalName(), file_get_contents($file->path()));
                $r = json_decode($upload);
            }

            if ($r->status_code == '00') {

                $upload = new DokumenBorrower;
                $upload->brw_id =  $request->brw_id;
                $upload->pendanaan_id =  $request->pendanaan_id;
                $upload->scoring_type =  1;
                $upload->jenis_dokumen =  $request->nama_dokumen;
                $upload->nama_dokumen = $file->getClientOriginalName();
                $upload->path_file =  $r->uuid;
                $upload->author = $author;
                $upload->save();

                $return1 = array(
                    "status_code" => "00",
                    "status_message" => 'Upload File'

                );
            }

            return $return1;
        } else {
            return null;
        }
    }

    public function admin_get_dokumen_scoring_personal($id)
    {
        $check_file_scoring = DB::table('brw_dokumen as a')
            ->selectRaw('a.brw_id, a.pendanaan_id, a.path_file')
            ->where('pendanaan_id', '=', $id)
            ->where('scoring_type', '=', 1)
            ->count();

        if ($check_file_scoring > 0) {
            $file_scoring = DB::table('brw_dokumen as a')
                ->selectRaw('a.brw_id, a.pendanaan_id, a.path_file')
                ->where('pendanaan_id', '=', $id)
                ->where('scoring_type', '=', 1)
                ->get();

            $scoring_file = $file_scoring[0]->path_file;

            $client = new DMSController();
            $response = json_decode($client->downloadFile($scoring_file));
            $content = $response->content;
            $content_decode = base64_decode($content);

            $audit = new AuditTrail;
            $username = Auth::guard('admin')->user()->firstname;
            $audit->fullname = $username;
            $audit->menu = "Penilaian Penerima Pendanaan";
            $audit->description = "Unduh Dokumen Penilaian Personal Penerima Pendana";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return response($content_decode)->header('Content-Type', 'application/pdf');
        } else {
            return redirect()->back()->with('error', 'Maaf, tidak ada data yang tersedia. Silahkan anda unggah kembali');
        }
    }

    public function show_menu_RDLBank_transaction()
    {
        return view('pages.admin.LogBankRDLTransaction');
    }

    public function datatables_RDLBank_transaction()
    {
        $log = LogBankRDLTransaction::all();

        $data = array();
        $no = 1;
        if (!empty($log)) {
            foreach ($log as $row) {

                $column['id']                     = (string)$row->id;
                $column['bank_rdl_code']         = (string)$row->bank_rdl_code;
                $column['category']             = (string)$row->category;
                $column['request_content']         = (string)$row->request_content;
                $column['request_response']     = (string)$row->request_response;
                $column['nominal_transaction']     = (string)$row->nominal_transaction;
                $column['status']                 = (string)$row->status;
                $column['bank_reference']         = (string)$row->bank_reference;
                $column['created_at']             = (string)$row->created_at;

                $data[] = $column;
            }
        }

        $parsingJSON = array(
            "data" => $data
        );
        echo json_encode($parsingJSON);
    }

    // view upload data borrower FDC
    public function view_upload_fdc()
    {
        return view('pages.admin.view_data_fdc');
    }

    // proses upload file ke internal storage
    public function upload_file_fdc(Request $request)
    {


        $tgl_format_hari_ini = date('Ymd');
        $tgl_format_kemarin = date('Ymd', strtotime('-1 days'));
        $tglHariini = date('Y-m-d');
        $tglkemarin = date('Y-m-d', strtotime('-1 days'));
        $bulan = date('M');
        $tahun = date('Y');
        $message = array();



        if ($request->hasFile('file_fdc')) {


            $file = $request->file('file_fdc');
            $filename = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $renameFile = "";
            $version    = "";

            //$selectFileTable = ListFileFDC::where('filename', $filename)->first();

            $zip = new ZipArchive();

            // cek ext file
            if ($ext !== "csv") {

                $message['status'] = "gagal_ext";
                $message['keterangan'] = "file bukan csv";
            } else {
                // check backdate file
                $selectFileTable = ListFileFDC::where('tanggal_file', $tglkemarin)->where('status_pengiriman', 1)->where('status_file', 0)->orderby('version', 'DESC')->first();

                // kondisi jika file backdate masi ada kesalahan
                if ($selectFileTable) {

                    $message['status'] = "file_error";
                    $message['keterangan'] = $selectFileTable->filename . " pada Tanggal " . $selectFileTable->tanggal_file . " isi data tidak sesuai, silahkan diperbaiki dulu";
                } else {

                    // check file backdate status pengiriman 0 dan status file 0
                    $selectFileTable = ListFileFDC::where('tanggal_file', $tglkemarin)->where('status_pengiriman', 0)->where('status_file', 0)->orderby('version', 'DESC')->first();

                    // jika user mengupload file backdate kembali tetapi status pengiriman masi 0 dan status file 0, maka sistem akan menolaknya
                    if ($selectFileTable) {

                        $message['status'] = "file_error";
                        $message['keterangan'] = $selectFileTable->filename . " pada Tanggal " . $selectFileTable->tanggal_file . " File Belum Di upload ke server AFPI";
                    } else {

                        // check tanggal hari ini dengan status pengiriman 0
                        $selectFileTable = ListFileFDC::where('tanggal_file', $tglHariini)->where('status_pengiriman', 0)->first();

                        // apabila user sudah mengupload file untuk hari ini, maka sistem akan menolaknya, karena file sudah ada dan status pengiriman 0 dan status file 0
                        if ($selectFileTable) {

                            $message['status'] = "status_pengiriman";
                            $message['keterangan'] = "Anda Sudah Mengunggah File Pada Hari ini, File " . $selectFileTable->filename . " Sedang Menunggu Unggah File Ke Server AFPI";
                        } else {

                            // check tanggal hari ini dengan statusu pengiriman 1 dan status file 0 (file hari ini yg bermasalah)
                            $selectFileTable = ListFileFDC::where('tanggal_file', $tglHariini)->where('status_pengiriman', 1)->where('status_file', 0)->first();

                            // jika file hari ini bermasalah dengan status pengiriman 1 dan status file 0 dan user ingin mengupload file baru maka sistem akan menolaknya
                            if ($selectFileTable) {

                                $message['status'] = "file_error";
                                $message['keterangan'] = $selectFileTable->filename . " pada Tanggal " . $selectFileTable->tanggal_file . " isi data tidak sesuai, silahkan diperbaiki dulu";
                            } else {
                                // check file hari ini status pengiriman 1 dan status file 1 
                                $selectFileTable = ListFileFDC::where('tanggal_file', $tglHariini)->where('status_pengiriman', 1)->where('status_file', 1)->first();

                                // jika hari ini status pengiriman 1 dan status file 1 maka sistem akan menolak untuk mengupload file, karna file sudah ada dengan status sukses  
                                if ($selectFileTable) {

                                    $message['status'] = "file_hari_ini";
                                    $message['keterangan'] = "Untuk File Hari Ini Sudah Terupload Ke Server AFPI Dan Status File Valid";
                                } else {

                                    // proses upload file untuk hari ini
                                    $version .= "1";
                                    // utk ngecek status pengiriman file 0 belum dikirim, 1 sudah dikirim ke fdc

                                    $formatFilename = config('app.sftp_id') . $tgl_format_hari_ini . 'SIK' . '01';
                                    //$formatFilename = config('app.sftp_id').'20200617'.'SIK'.'0';
                                    $filezip        = $formatFilename . '.zip';
                                    $renameFile     = $formatFilename . '.' . $ext;


                                    $file->storeAs(
                                        'public/fdc',
                                        $renameFile
                                    );


                                    if ($zip->open(storage_path('app/public/fdc/' . $filezip), ZipArchive::CREATE) === TRUE) {
                                        $fdc        = new FDCController();
                                        $decodeJsonPass = $fdc->GeneratepasswordZIP();

                                        $zip->addFile(storage_path('app/public/fdc/' . $renameFile), $renameFile);
                                        $zip->setEncryptionName($renameFile, ZipArchive::EM_AES_256, $decodeJsonPass); //$decodeJsonPass->{'zipPwd'});
                                        $zip->close();
                                    }
                                    //File::put(storage_path('app/public/fdc/'.$formatFilename.'_pass.txt'), $decodeJsonPass->{'zipPwd'});
                                    File::put(storage_path('app/public/fdc/' . $formatFilename . '_pass.txt'), $decodeJsonPass);


                                    $insertDB = DB::table('list_file_fdc')->insert(
                                        [
                                            'user_upload' => Auth::guard('admin')->user()->firstname, 'filename' => $filezip, 'version' => $version,
                                            'status_pengiriman' => 0, 'status_file' => 0, 'parent' => '1', 'tanggal_file' => $tglHariini, 'tanggal_upload' => "", 'bulan' => $bulan, 'tahun' => $tahun
                                        ]
                                    );

                                    // set response
                                    $message['status'] = "sukses";
                                    $message['keterangan'] = "Berhasil Diupload";
                                }
                            }
                        }
                    }
                }
            }
        }
        return redirect('/admin/view_upload_fdc')->with($message);
        //return response()->json($message);

    }

    public function upload_data_perbaikan(Request $request)
    {


        $tgl_format_hari_ini = date('Ymd');
        $tgl_format_kemarin = date('Ymd', strtotime('-1 days'));
        $tglHariini = date('Y-m-d');
        $tglkemarin = date('Y-m-d', strtotime('-1 days'));
        $bulan = date('M');
        $tahun = date('Y');
        $message = array();

        if ($request->hasFile('file_perbaikan')) {

            $file = $request->file('file_perbaikan');
            $filename = $file->getClientOriginalName();
            $ext = $file->getClientOriginalExtension();
            $renameFile = "";
            $version    = "";


            $zip = new ZipArchive();

            // cek ext file
            if ($ext != "csv") {

                $message['status'] = "gagal_ext_2";
                $message['keterangan'] = "file bukan csv";
            } else {

                $getVersion = ListFileFDC::where('id_fdc', $request->text_id_data)->first();
                $setVersion = $getVersion->version + 1;

                $TanggalFile = explode('-', $getVersion->tanggal_file);
                $setTanggal  = $TanggalFile[0] . $TanggalFile[1] . $TanggalFile[2];

                //$formatFilename = config('app.sftp_id').$tgl_format_hari_ini.'SIK'.'01';
                $formatFilename = config('app.sftp_id') . $setTanggal . 'SIK0' . $setVersion;
                $filezip        = $formatFilename . '.zip';
                $renameFile     = $formatFilename . '.' . $ext;


                $file->storeAs(
                    'public/fdc',
                    $renameFile
                );


                if ($zip->open(storage_path('app/public/fdc/' . $filezip), ZipArchive::CREATE) === TRUE) {
                    $fdc        = new FDCController();
                    $decodeJsonPass = $fdc->GeneratepasswordZIP();
                    $zip->addFile(storage_path('app/public/fdc/' . $renameFile), $renameFile);
                    $zip->setEncryptionName($renameFile, ZipArchive::EM_AES_256, $decodeJsonPass);
                    $zip->close();
                }
                File::put(storage_path('app/public/fdc/' . $formatFilename . '_pass.txt'), $decodeJsonPass);


                $updateStatus = DB::table('list_file_fdc')
                    ->where('id_fdc', $getVersion->id_fdc)
                    ->update(['status_pengiriman' => 1, 'status_file' => 1,  'tanggal_upload' => date('Y-m-d')]);

                $insertDB = DB::table('list_file_fdc')->insert(
                    [
                        'user_upload' => Auth::guard('admin')->user()->firstname, 'filename' => $filezip, 'version' => $setVersion,
                        'status_pengiriman' => 0, 'status_file' => 0, 'parent' => '1', 'tanggal_file' => $getVersion->tanggal_file, 'tanggal_upload' => "", 'bulan' => $bulan, 'tahun' => $tahun
                    ]
                );

                // set response
                $message['status'] = "sukses";
                $message['keterangan'] = "Berhasil Diupload";
                // utk ngecek status pengiriman file 0 belum dikirim, 1 sudah dikirim ke fdc

            }
        }

        return redirect('/admin/view_upload_fdc')->with($message);
    }


    public function bulan($bulan)
    {
        switch ($bulan) {

            case "01":
                echo "Januari";
                break;
            case "02":
                echo "Febuari";
                break;
            case "03":
                echo "Maret";
                break;
            case "04":
                echo "April";
                break;
            case "05":
                echo "Mei";
                break;
            case "06":
                echo "Juni";
                break;
            case "07":
                echo "Juli";
                break;
            case "08":
                echo "Agustus";
                break;
            case "09":
                echo "September";
                break;
            case "10":
                echo "Oktober";
                break;
            case "11":
                echo "November";
                break;
            case "12":
                echo "Desember";
                break;
        }
    }



    public function list_file_fdc()
    {


        $getData     = DB::table('list_file_fdc')
            ->get();

        $no = 1;
        $data = array();


        foreach ($getData as $value) {


            $column['No'] = (string) $no;
            $column['Id_FDC'] = (string) $value->id_fdc;
            $column['Tanggal_File'] = (string) $value->tanggal_file;
            $column['Tanggal_Upload'] = (string) $value->tanggal_upload;
            $column['Filename'] = (string) $value->filename;
            $column['Version'] = (string) $value->version;
            $column['Status_Pengiriman'] = (string) $value->status_pengiriman;
            $column['Status_File'] = (string) $value->status_file;
            $column['Action'] = "";
            $data[] = $column;

            $no++;
        }

        $parsingJSON = array('data' => $data);

        echo json_encode($parsingJSON);
    }

    public function delete_file_fdc(Request $request)
    {
        $file = DB::table('list_file_fdc')
            ->where('id_fdc', $request->text_delete_id_file)
            ->first();
        $file_fdc = $file->filename . '.out';
        // $file_fdc = '81005920200713SIK01.zip.out';
        // $test = Storage::disk('fdc')->exists('/response/'.$file_fdc);
        // dd($file_fdc);

        if (Storage::disk('fdc')->exists('/response/' . $file_fdc)) {
            $data = Storage::disk('fdc')->get('/response/' . $file_fdc);
            $result = trim($data);
            // dd($result);
            if ($result == 'RowNo|Errors|id_borrower|id_pinjaman') {
                DB::table('list_file_fdc')
                    ->where('id_fdc', $request->text_delete_id_file)
                    ->update([
                        'status_pengiriman' => 1,
                        'status_file' => 1,
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);
                $message['status'] = "sukses_hapus";
                $message['keterangan'] = "Response : Berhasil Diterima";
                return redirect('/admin/view_upload_fdc')->with($message);
            } else {
                $message['status'] = "sukses_hapus";
                $message['keterangan'] = "Responses : Gagal " . $result;
                return redirect('/admin/view_upload_fdc')->with($message);
            }
        } else {
            $message['status'] = "sukses_hapus";
            $message['keterangan'] = "Responses : File Tidak Ditemukan";
            return redirect('/admin/view_upload_fdc')->with($message);
        }
    }

    public function pencairanListPenerima()
    {
        return view('pages.admin.borrower.pencairan_dana');
    }

    public function listPencairanDana()
    {

        $listApproveDana = DB::table('brw_pencairan AS a')
            ->join('brw_user_detail AS b', 'a.brw_id', '=', 'b.brw_id')
            ->join('brw_pendanaan AS c', 'a.pendanaan_id', '=', 'c.pendanaan_id')
            ->join('brw_rekening AS d', 'a.brw_id', '=', 'd.brw_id')
            ->join('m_bank AS e', 'd.brw_kd_bank', '=', 'e.kode_bank')
            ->join('m_no_akad_borrower AS f', 'f.proyek_id', '=', 'c.id_proyek')
            ->selectRaw('b.nama, a.pencairan_id, a.brw_id, a.pendanaan_id, a.nominal_pencairan, a.tgl_req_pencairan, 
            a.tgl_pencairan, a.status, a.dicairkan_oleh, a.bukti_transfer, a.keterangan, c.pendanaan_nama, c.id_proyek as proyek_id, d.brw_norek, d.brw_nm_pemilik, 
            e.nama_bank, f.no_akad_bor, f.bln_akad_bor, f.thn_akad_bor, f.created_at')
            ->whereIn('a.status', [2, 3])
            ->orderBy('pencairan_id', 'desc')
            ->get();

        $dataArray = array();
        $i = 1;
        if (!empty($listApproveDana)) {
            foreach ($listApproveDana as $item) {
                $column = array();
                $column[0] = $i++;
                $column[1] = (string)$item->pencairan_id;
                $column[2] = (string)$item->brw_id;
                $column[3] = (string)$item->proyek_id;
                $column[4] = (string)$item->nama;
                $column[5] = (string)$item->pendanaan_nama;
                $column[6] = (string)$item->nominal_pencairan;
                $column[7] = (string)$item->no_akad_bor . '/AMRB/DSI/' . (string)$item->bln_akad_bor . '/' . (string)$item->thn_akad_bor;
                $column[8] = (string)date('d-m-Y H:i:s', strtotime($item->created_at));
                $column[9] = (string)$item->brw_norek;
                $column[10] = $item->tgl_req_pencairan ? (string)date('d-m-Y H:i:s', strtotime($item->tgl_req_pencairan)) : '-';
                $column[11] = $item->tgl_pencairan ? (string)date('d-m-Y H:i:s', strtotime($item->tgl_pencairan)) : '-';
                $column[12] = $item->dicairkan_oleh ? (string)$item->dicairkan_oleh : '-';
                $column[13] = $item->status;
                $column[14] = ($item->bukti_transfer == null || $item->bukti_transfer == '') ? '-' : (string)$item->bukti_transfer;
                $column[15] = ($item->keterangan == null || $item->keterangan == '') ? '-' : (string)$item->keterangan;
                $column[16] = (string)$item->pendanaan_id;
                $column[17] = (string)$item->brw_nm_pemilik;
                $column[18] = (string)$item->nama_bank;

                $dataArray[] = $column;
            }
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function admin_pencairan_dana_penerima(Request $request)
    {

        $pencairan_id = $request->pencairan_id;
        $brw_id = $request->brw_id;
        $pendanaan_id = $request->pendanaan_id;
        $nominal_pencairan = $request->nominal_pencairan;
        $author = Auth::guard('admin')->user()->firstname;
        $file_name = 'AdminController.php';
        $line = 3889;

        DB::beginTransaction();

        try {

            $uploads = $this->upload_bukti_transfer_pencairan('file', $request);

            if ($uploads) {
                $call_db = DB::select(
                    "call proc_brw_pencairan('$pencairan_id','$brw_id','$pendanaan_id','$nominal_pencairan','$author','$uploads', '$file_name', '$line')"
                );

                if ($call_db[0]->res == 1) {

                    $audit = new AuditTrail;
                    $username = Auth::guard('admin')->user()->firstname;
                    $audit->fullname = $username;
                    $audit->menu = "skor Penerima Pendanaan";
                    $audit->description = "Menyetujui Pendanaan";
                    $audit->ip_address =  \Request::ip();
                    $audit->save();

                    DB::commit();

                    $email_to = Admins::select('email', 'firstname', 'lastname')->where('role', '=', 3)->get();

                    $data_email = Borrower::select(
                        'brw_user.email',
                        'brw_user_detail.nama',
                        'brw_pencairan.nominal_pencairan',
                        'brw_pencairan.tgl_pencairan',
                        'brw_pencairan.dicairkan_oleh',
                        'brw_rekening.brw_norek',
                        'brw_rekening.brw_nm_pemilik',
                        'brw_pendanaan.pendanaan_nama',
                        'm_bank.nama_bank',
                        'm_no_akad_borrower.no_akad_bor',
                        'm_no_akad_borrower.bln_akad_bor',
                        'm_no_akad_borrower.thn_akad_bor',
                        'm_no_akad_borrower.created_at'
                    )
                        ->addSelect(DB::raw("'Disetujui' as status"))
                        ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                        ->join('brw_pencairan', 'brw_user.brw_id', '=', 'brw_pencairan.brw_id')
                        ->join('brw_rekening', 'brw_rekening.brw_id', '=', 'brw_user.brw_id')
                        ->join('m_bank', 'brw_rekening.brw_kd_bank', '=', 'm_bank.kode_bank')
                        ->join('brw_pendanaan', 'brw_pendanaan.pendanaan_id', '=', 'brw_pencairan.pendanaan_id')
                        ->join('m_no_akad_borrower', 'm_no_akad_borrower.proyek_id', '=', 'brw_pendanaan.id_proyek')
                        ->where('brw_pencairan.pencairan_id', $pencairan_id)->get();

                    //BUAT KE BORROWER
                    $email = new EmailPencairanDanaFinance($data_email[0], '', 'borrower');
                    Mail::to($data_email[0]->email)->send($email);

                    //BUAT KE ADMIN
                    foreach ($email_to as $data) {
                        $email = new EmailPencairanDanaFinance($data_email[0], $data, 'admin');
                        Mail::to($data->email)->send($email);
                    }

                    return redirect()->back()->with('success', 'Unggah Bukti Transfer Berhasil');
                } else {
                    DB::rollback();
                    return redirect()->back()->with('error', 'Unggah Bukti Transfer Gagal');
                }
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Unggah Bukti Transfer Gagal');
            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('success', 'Unggah Bukti Transfer Gagal');
        }
    }

    private function upload_bukti_transfer_pencairan($column, Request $request)
    {
        if ($request->hasFile($column)) {
            $file = $request->file($column);
            $filename = $request->pencairan_id . $request->file->getClientOriginalName();
            $store_path = 'bukti_pencairan/' . $request->brw_id;
            $path = $file->storeAs($store_path, $filename, 'public');
            //save gambar yang di upload di public storage
            if ($path) {
                return $path;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function admin_unduh_bukti_pencairan(Request $request)
    {
        return response()->download(storage_path("app/public/{$request->routes_url}"));
    }

    public function verifikasiCicilan()
    {
        return view('pages.admin.borrower.verifikasi_cicilan');
    }

    public function listVerifikasiCicilan()
    {

        $listVerifikasi = DB::table('brw_invoice AS b')
            ->join('brw_pendanaan AS d', 'b.pendanaan_id', '=', 'd.pendanaan_id')
            ->join('brw_user_detail AS c', 'c.brw_id', '=', 'b.brw_id')
            ->selectRaw('b.no_invoice, b.invoice_id, b.brw_id, b.pendanaan_id, c.nama, d.pendanaan_nama, b.nominal_tagihan_perbulan, 
        b.bulan_ke, b.tgl_jatuh_tempo, b.tgl_pembayaran, b.tgl_konfirmasi, b.konfirmasi_oleh, b.status, b.no_referal, b.nominal_transfer_tagihan')
            ->whereIn('b.status', [1, 3, 5, 6])
            ->orderBy('invoice_id', 'desc')
            ->get();

        $dataArray = array();

        $i = 1;
        if (!empty($listVerifikasi)) {

            $now = date_create(date("Y-m-d"));

            foreach ($listVerifikasi as $item) {
                $tanggal = date_format(new DateTime($item->tgl_jatuh_tempo), 'Y-m-d');
                $tgl_jatuh_tempo = date_create($tanggal);
                $hari = date_diff($tgl_jatuh_tempo, $now)->format("%R%a");
                if (strpos($hari, '-') !== false) {
                    $hari = 0;
                } else if (strpos($hari, '+') !== false) {
                    $hari = date_diff($tgl_jatuh_tempo, $now)->format("%a");
                }

                $column = array();
                $column[0] = (string)$i++;
                $column[1] = $item->invoice_id ? (string)$item->invoice_id : '-';
                $column[2] = $item->brw_id ? (string)$item->brw_id : '-';
                $column[3] = $item->pendanaan_id ? (string)$item->pendanaan_id : '-';
                $column[4] = $item->no_invoice ? (string)$item->no_invoice : '-';
                $column[5] = $item->nama ? (string)$item->nama : '-';
                $column[6] = $item->pendanaan_nama ? (string)$item->pendanaan_nama : '-';
                $column[7] = $item->nominal_tagihan_perbulan ? (string)$item->nominal_tagihan_perbulan : '-';
                $column[8] = $item->bulan_ke ? 'Bulan ke ' . (string)$item->bulan_ke : '-';
                $column[9] = $item->tgl_jatuh_tempo ? (string)date('d-m-Y H:i:s', strtotime($item->tgl_jatuh_tempo)) : '-';
                $column[10] = $hari ? (string)$hari : '0';
                $column[11] = $item->nominal_transfer_tagihan ? (string)$item->nominal_transfer_tagihan : '-';
                $column[12] = $item->no_referal ? (string)$item->no_referal : '-';
                $column[13] = $item->tgl_pembayaran ? (string)$item->tgl_pembayaran : '-';
                $column[14] = $item->tgl_konfirmasi ? (string)$item->tgl_konfirmasi : '-';
                $column[15] = $item->konfirmasi_oleh ? (string)$item->konfirmasi_oleh : '-';
                $column[16] = $item->status ? (string)$item->status : '-';


                $dataArray[] = $column;
            }
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function getPicturePembayaran(Request $request)
    {

        $invoice_id = $request->invoice_id;
        $pict_status = $request->pict_status;

        $where = BorrowerBuktiPembayaran::select('bukti_id', 'invoice_id', 'pic_pembayaran', 'status')->where('invoice_id', '=', $invoice_id)->orderBy('bukti_id', 'desc');

        if ($pict_status == 'tolak') {
            $get_data = $where->where('status', '=', 6)->get();
        } else if ($pict_status == 'konfirm') {
            $get_data = $where->whereIn('status', [5, 3])->get();
        } else {
            $get_data = $where->where('status', '=', 1)->get();
        }

        return json_encode($get_data);
    }

    public function konfirmasiCicilan(Request $request)
    {

        $status_confirm = $request->status_confirm;
        $invoice_id_confirm = $request->invoice_id_confirm;
        $bukti_id_confirm = $request->bukti_id_confirm;
        $brw_id_confirm = $request->brw_id_confirm;
        $pendanaan_id_confirm = $request->pendanaan_id_confirm;
        $keterangan_confirm = ($request->keterangan_confirm) ? $request->keterangan_confirm : '';
        $nominal_confirm_real = ($request->nominal_confirm) ? $request->nominal_confirm : 0;
        $nominal_confirm_real = (int)str_replace(array('.', ','), '', $nominal_confirm_real);

        $author = Auth::guard('admin')->user()->firstname;
        $file_name = 'AdminControlller.php';
        $line = 4013;

        $call_db = DB::select(
            "call proc_brw_konfirmasi_cicilan('$status_confirm', '$invoice_id_confirm', '$brw_id_confirm', '$pendanaan_id_confirm', $bukti_id_confirm, '$keterangan_confirm', '$nominal_confirm_real', '$author', '$file_name', '$line')"
        );

        if ($call_db[0]->res == 1) {

            $audit = new AuditTrail;
            $audit->fullname = $author;
            $audit->menu = "Kelola Cicilan";
            $audit->description = "Menyetujui Cicilan Borrower";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            DB::commit();

            $data_email = Borrower::select(
                'brw_user.email',
                'brw_user_detail.nama',
                'brw_invoice.no_invoice',
                'brw_invoice.bulan_ke',
                'brw_invoice.nominal_tagihan_perbulan',
                'brw_invoice.tgl_pembayaran',
                'brw_invoice.tgl_konfirmasi',
                'brw_invoice.keterangan',
                DB::raw('(CASE WHEN brw_invoice.status = 6 THEN  "Gagal" ELSE "Berhasil" END) AS status_pembayaran')
            )
                ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                ->join('brw_invoice', 'brw_user.brw_id', '=', 'brw_invoice.brw_id')
                ->where('brw_invoice.invoice_id', $request->invoice_id_confirm)->get();

            if (!empty($data_email)) {
                $email = new EmailKonfirmasiCicilan($data_email[0]);
                Mail::to($data_email[0]->email)->send($email);
            }

            return redirect()->back()->with('success', 'Sukses Menyimpan Data');
        } else {
            DB::rollback();
            return redirect()->back()->with('error', 'Gagal Menyimpan Data, Silahkan coba lagi');
        }
    }

    public function tableGetBuktiBayar($id_invoice)
    {

        $bukti_bayar = BorrowerBuktiPembayaran::select(
            'brw_bukti_pembayaran.bukti_id',
            'brw_bukti_pembayaran.invoice_id',
            'brw_bukti_pembayaran.brw_id',
            'brw_bukti_pembayaran.nominal',
            'brw_bukti_pembayaran.ref_no',
            'brw_bukti_pembayaran.pic_pembayaran',
            'brw_bukti_pembayaran.tgl_bukti_bayar',
            'brw_bukti_pembayaran.status',
            'brw_bukti_pembayaran.confirmed_by',
            'brw_bukti_pembayaran.updated_at',
            'brw_bukti_pembayaran.keterangan',
            'brw_invoice.no_invoice',
            'brw_invoice.pendanaan_id'
        )
            ->join('brw_invoice', 'brw_invoice.invoice_id', '=', 'brw_bukti_pembayaran.invoice_id')
            ->where('brw_bukti_pembayaran.invoice_id', '=', $id_invoice)->get();

        $i = 1;
        $data = array();

        foreach ($bukti_bayar as $value) {
            $column['no'] = (string) $i++;
            $column['bukti_id'] = (string)$value->bukti_id;
            $column['invoice_id'] = (string)$value->invoice_id;
            $column['brw_id'] = (string)$value->brw_id;
            $column['no_invoice'] = (string)$value->no_invoice;
            $column['tgl_bukti_bayar'] = (string)date('d-m-Y H:i:s', strtotime($value->tgl_bukti_bayar));
            $column['ref_no'] = (string)$value->ref_no;
            $column['nominal'] = (string)$value->nominal;
            $column['tgl_konfirmasi'] = (string)date('d-m-Y H:i:s', strtotime($value->updated_at));
            $column['confirmed_by'] = (string)$value->confirmed_by;
            $column['keterangan'] = (string)$value->keterangan;
            $column['status'] = (string)$value->status;
            $column['pic_pembayaran'] = (string)$value->pic_pembayaran;
            $column['pendanaan_id'] = (string)$value->pendanaan_id;
            $data[] = $column;
        }

        $data_bukti_bayar = array('data' => $data);
        return response($data_bukti_bayar);
    }

    public function admin_unduh_bukti_transfer(Request $request)
    {

        $brw_id = $request->brw_id_lihat;
        $invoice_id = $request->invoice_id_lihat;
        $bukti_id = $request->bukti_id_lihat;

        $get_data = BorrowerBuktiPembayaran::select('bukti_id', 'invoice_id', 'pic_pembayaran', 'status')->where('bukti_id', '=', $bukti_id)->get();

        if (sizeof($get_data) > 1) {
            $zip_file = "bukti-transfer-$invoice_id.zip";

            $zip = new ZipArchive();
            $zip->open($zip_file, ZipArchive::CREATE);

            foreach ($get_data as $item) {
                $invoice_file = "app/public/invoice/buktibayar/$item->pic_pembayaran";
                $file_name = "bukti-bayar/$item->pic_pembayaran";

                $zip->addFile(storage_path($invoice_file), $file_name);
            }

            $zip->close();

            return response()->download($zip_file);
        } else {
            $file = $get_data[0]->pic_pembayaran;
            return response()->download(storage_path("app/public/invoice/buktibayar/$file"));
        }
    }

    public function viewPersetujuanPendanaan()
    {
        return view('pages.admin.borrower.persetujuan_pendanaan');
    }

    public function listPersetujuanPendanaan()
    {
        $statususer = array('active', 'pending');
        $getData = BorrowerPengajuan::select(
            'brw_pengajuan.brw_id',
            'brw_pengajuan.status',
            'brw_pengajuan.pendanaan_tipe',
            'brw_pengajuan.pengajuan_id',
            'brw_pengajuan.pendanaan_nama',
            'brw_pengajuan.pendanaan_dana_dibutuhkan',
            'brw_pengajuan.keterangan',
            'brw_user_detail.nama',
            'brw_user_detail.brw_type',
            'brw_user_detail.nm_bdn_hukum',
            'brw_pengajuan.created_at',
            'brw_pengajuan.detail_pendanaan',
            'brw_user_detail.ktp',
            'brw_scorring_personal.nilai as nilai_personal',
            'brw_scorring_pendanaan.scorring_nilai as nilai_pendanaan',
            'brw_scorring_pendanaan.scorring_judul as scorring_judul'
        )
            ->leftjoin('brw_user', 'brw_user.brw_id', '=', 'brw_pengajuan.brw_id')
            ->leftjoin('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_pengajuan.brw_id')
            ->leftjoin('brw_scorring_personal', 'brw_scorring_personal.brw_id', '=', 'brw_pengajuan.brw_id')
            ->leftjoin('brw_scorring_pendanaan', 'brw_scorring_pendanaan.pengajuan_id', '=', 'brw_pengajuan.pengajuan_id')
            ->wherein('brw_pengajuan.status', [1, 3, 5])
            ->wherein('brw_user.status', $statususer)->orderBy('pengajuan_id', 'desc')
            ->get();

        $i = 1;
        $data = array();

        foreach ($getData as $item) {
            if ($item->created_at == NULL) {
                $created_at = "-";
            } else {
                $date = date_create($item->created_at);
                $created_at = date_format($date, "d-m-Y H:i:s");
            }

            if ($item->brw_type == 2) {
                $judulnama = $item->nm_bdn_hukum;
            } else {
                $judulnama = $item->nama;
            }
            $column['no'] = (string) $i++;
            $column['id'] = (string) $item->brw_id;
            $column['idPengajuan'] = (string) $item->pengajuan_id;
            $column['pendanaanBorrower'] = (string) $item->pendanaan_nama;
            $column['namaBorrower'] = (string) $judulnama;
            $column['pendanaan_dana_dibutuhkan'] = (string) $item->pendanaan_dana_dibutuhkan;
            $column['tgl_pengajuan'] = (string) $created_at;
            $column['ktp'] = (string) $item->ktp;
            $column['nilaiBorrower'] = (string) $item->nilai_personal;
            $column['nilaiPendanaan'] = (string) $item->nilai_pendanaan;
            $column['gradeNilaiPendanaan'] = (string) $item->scorring_judul;
            $column['brw_type'] = (string) $item->brw_type;
            $column['keterangan'] = (string) $item->keterangan;
            $column['status'] = (string) $item->status;
            $column['pendanaan_tipe'] = (string) $item->pendanaan_tipe;

            $data[] = $column;
        }

        $parsingJSON = array('data' => $data);

        // echo json_encode($parsingJSON);

        return response($parsingJSON);
    }

    public function save_persetujuan_pendanaan(Request $request)
    {

        $id_brw = $request->brw_id_edit;
        $idPengajuan = $request->pengajuan_id_edit;
        $scorePersonal = $request->penilaian_personal;
        $scorePendanaan =  $request->penilaian_pendanaan;
        $status_verif =  $request->status_setuju;
        $keterangan =  $request->keterangan_confirm ? $request->keterangan_confirm : '';
        $file = 'AdminController.php';
        $line = 4136;

        try {

            DB::beginTransaction();

            $call_db = DB::select(
                "call proc_brw_analyst_verif('$id_brw','$idPengajuan','$scorePersonal','$scorePendanaan','$status_verif','$keterangan', '$file', '$line')"
            );

            if ($call_db[0]->res == 1) {
                $audit = new AuditTrail;
                $username = Auth::guard('admin')->user()->firstname;
                $audit->fullname = $username;
                $audit->menu = "skor Penerima Pendanaan";
                $audit->description = "Menyetujui Pendanaan";
                $audit->ip_address =  \Request::ip();
                $audit->save();

                DB::commit();

                $email_to = Admins::select('email', 'firstname', 'lastname')->where('role', '=', 8)->get();

                if ($status_verif == 'setuju') {
                    $data_email = Borrower::select(
                        'brw_user.email',
                        'brw_user.brw_id',
                        'brw_user_detail.nama',
                        'brw_pengajuan.pendanaan_nama',
                        'brw_pengajuan.keterangan',
                        'brw_pengajuan.created_at',
                        'brw_pengajuan.pendanaan_dana_dibutuhkan'
                    )
                        ->addSelect(DB::raw("'Diterima' as status"))
                        ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                        ->join('brw_pengajuan', 'brw_pengajuan.brw_id', '=', 'brw_user.brw_id')
                        ->where('brw_pengajuan.pengajuan_id', '=', $idPengajuan)->get();

                    //BUAT KE ANALIS
                    $i = 0;
                    foreach ($email_to as $data) {
                        $email = new EmailVerifikasiPenilaian($data_email[0], $data, 'analis');
                        Mail::to($data->email)->send($email);
                        $i++;
                    }

                    //BUAT KE BORROWER
                    $email2 = new EmailVerifikasiPenilaianKeBorrower($data_email[0]);
                    Mail::to($data_email[0])->send($email2);
                } else {
                    $data_email = Borrower::select(
                        'brw_user.email',
                        'brw_user_detail.nama',
                        'brw_pengajuan.pendanaan_nama',
                        'brw_pengajuan.keterangan',
                        'brw_pengajuan.created_at',
                        'brw_pengajuan.pendanaan_dana_dibutuhkan'
                    )
                        ->addSelect(DB::raw("'Ditolak' as status"))
                        ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                        ->join('brw_pengajuan', 'brw_pengajuan.brw_id', '=', 'brw_user.brw_id')
                        ->where('brw_pengajuan.pengajuan_id', '=', $idPengajuan)->get();

                    //BUAT KE BORROWER
                    $email = new EmailVerifikasiPenilaian($data_email[0], '', 'borrower');
                    Mail::to($data_email[0]->email)->send($email);

                    //BUAT KE ANALIS
                    $i = 0;
                    foreach ($email_to as $data) {
                        $email = new EmailVerifikasiPenilaian($data_email[0], $data, 'analis');
                        Mail::to($data->email)->send($email);
                        $i++;
                    }
                }

                return redirect()->back()->with('success', 'Konfirmasi Berhasil');
            } else {
                DB::rollback();
                return redirect()->back()->with('error', 'Konfirmasi Gagal');
            }
        } catch (Throwable $e) {
            DB::rollback();
            return $e;
        }
    }

    public function viewPersetujuanPencairan()
    {
        return view('pages.admin.borrower.persetujuan_pencairan');
    }

    public function listPersetujuanPencairan()
    {

        $listApproveDana = DB::table('brw_pencairan AS a')
            ->join('brw_user_detail AS b', 'a.brw_id', '=', 'b.brw_id')
            ->join('brw_pendanaan AS c', 'a.pendanaan_id', '=', 'c.pendanaan_id')
            ->join('brw_rekening AS d', 'a.brw_id', '=', 'd.brw_id')
            ->join('m_bank AS e', 'd.brw_kd_bank', '=', 'e.kode_bank')
            ->join('m_no_akad_borrower AS f', 'f.proyek_id', '=', 'c.id_proyek')
            ->selectRaw('b.nama, a.pencairan_id, a.brw_id, a.pendanaan_id, a.nominal_pencairan, a.tgl_req_pencairan, 
            a.tgl_pencairan, a.status, a.dicairkan_oleh, a.bukti_transfer, a.keterangan, c.pendanaan_nama, c.id_proyek as proyek_id, d.brw_norek, d.brw_nm_pemilik, 
            e.nama_bank, f.no_akad_bor, f.bln_akad_bor, f.thn_akad_bor, f.created_at')
            ->orderBy('pencairan_id', 'desc')
            ->get();

        $dataArray = array();
        $i = 1;
        if (!empty($listApproveDana)) {
            foreach ($listApproveDana as $item) {
                $column = array();
                $column[0] = $i++;
                $column[1] = (string)$item->pencairan_id;
                $column[2] = (string)$item->brw_id;
                $column[3] = (string)$item->proyek_id;
                $column[4] = (string)$item->nama;
                $column[5] = (string)$item->pendanaan_nama;
                $column[6] = (string)$item->nominal_pencairan;
                $column[7] = (string)$item->no_akad_bor . '/AMRB/DSI/' . (string)$item->bln_akad_bor . '/' . (string)$item->thn_akad_bor;
                $column[8] =  date('d-m-Y H:i:s', strtotime($item->created_at));
                $column[9] = (string)$item->brw_norek;
                $column[10] = $item->tgl_req_pencairan ? date('d-m-Y H:i:s', strtotime($item->tgl_req_pencairan)) : '-';
                $column[11] = $item->tgl_pencairan !== '' ? date('d-m-Y H:i:s', strtotime($item->tgl_pencairan)) : '-';
                $column[12] = $item->dicairkan_oleh ? (string)$item->dicairkan_oleh : '-';
                $column[13] = $item->status;
                $column[14] = ($item->bukti_transfer == null || $item->bukti_transfer == '') ? '-' : (string)$item->bukti_transfer;
                $column[15] = ($item->keterangan == null || $item->keterangan == '') ? '-' : (string)$item->keterangan;
                $column[16] = (string)$item->pendanaan_id;
                $column[17] = (string)$item->brw_nm_pemilik;
                $column[18] = (string)$item->nama_bank;

                $dataArray[] = $column;
            }
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function admin_persetujuan_pencairan(Request $request)
    {

        $status_confirm = $request->status_confirm;
        $pencairan_id = $request->pencairan_id;
        $brw_id_confirm = $request->brw_id;
        $pendanaan_id_confirm = $request->pendanaan_id;
        $keterangan_confirm = ($request->keterangan_confirm) ? $request->keterangan_confirm : '';
        $author = Auth::guard('admin')->user()->firstname;
        $file_name = 'AdminControlller.php';
        $line = 4246;

        $call_db = DB::select(
            "call proc_brw_admin_konfirmasi_cicilan('$pencairan_id', '$status_confirm', '$brw_id_confirm', '$pendanaan_id_confirm', '$keterangan_confirm', '$author', '$file_name', '$line')"
        );

        if ($call_db[0]->res == 1) {

            $audit = new AuditTrail;
            $audit->fullname = $author;
            $audit->menu = "Persetujuan Pencairan";
            $audit->description = "Admin menyetujui pencairan";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            DB::commit();

            $email_to = Admins::select('email', 'firstname', 'lastname')->where('role', '=', 4)->get();

            if ($status_confirm == 'setuju') {
                $data_email = Borrower::select(
                    'brw_user.email',
                    'brw_user_detail.nama',
                    'brw_pencairan.nominal_pencairan',
                    'brw_pencairan.keterangan',
                    'brw_rekening.brw_norek',
                    'brw_rekening.brw_nm_pemilik',
                    'm_bank.nama_bank',
                    'm_no_akad_borrower.no_akad_bor',
                    'm_no_akad_borrower.bln_akad_bor',
                    'm_no_akad_borrower.thn_akad_bor',
                    'm_no_akad_borrower.created_at'
                )
                    ->addSelect(DB::raw("'Disetujui' as status"))
                    ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                    ->join('brw_pencairan', 'brw_user.brw_id', '=', 'brw_pencairan.brw_id')
                    ->join('brw_rekening', 'brw_rekening.brw_id', '=', 'brw_user.brw_id')
                    ->join('m_bank', 'brw_rekening.brw_kd_bank', '=', 'm_bank.kode_bank')
                    ->join('brw_pendanaan', 'brw_pendanaan.pendanaan_id', '=', 'brw_pencairan.pendanaan_id')
                    ->join('m_no_akad_borrower', 'm_no_akad_borrower.proyek_id', '=', 'brw_pendanaan.id_proyek')
                    ->where('brw_pencairan.pencairan_id', $pencairan_id)->get();

                //EMAIL KE FINANCE
                foreach ($email_to as $data) {
                    $email = new EmailPencairanDana($data_email[0], $data, 'finance');
                    Mail::to($data->email)->send($email);
                }
            } else {
                $data_email = Borrower::select(
                    'brw_user.email',
                    'brw_user_detail.nama',
                    'brw_pencairan.nominal_pencairan',
                    'brw_pencairan.keterangan',
                    'brw_rekening.brw_norek',
                    'brw_rekening.brw_nm_pemilik',
                    'm_bank.nama_bank',
                    'm_no_akad_borrower.no_akad_bor',
                    'm_no_akad_borrower.bln_akad_bor',
                    'm_no_akad_borrower.thn_akad_bor',
                    'm_no_akad_borrower.created_at'
                )
                    ->addSelect(DB::raw("'Ditolak' as status"))
                    ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                    ->join('brw_pencairan', 'brw_user.brw_id', '=', 'brw_pencairan.brw_id')
                    ->join('brw_rekening', 'brw_rekening.brw_id', '=', 'brw_user.brw_id')
                    ->join('m_bank', 'brw_rekening.brw_kd_bank', '=', 'm_bank.kode_bank')
                    ->join('brw_pendanaan', 'brw_pendanaan.pendanaan_id', '=', 'brw_pencairan.pendanaan_id')
                    ->join('m_no_akad_borrower', 'm_no_akad_borrower.proyek_id', '=', 'brw_pendanaan.id_proyek')
                    ->where('brw_pencairan.pencairan_id', $pencairan_id)->get();

                //EMAIL KE BORROWER
                $email = new EmailPencairanDana($data_email[0], '', 'borrower');
                Mail::to($data_email[0]->email)->send($email);

                //EMAIL KE FINANCE
                foreach ($email_to as $data) {
                    $email = new EmailPencairanDana($data_email[0], $data, 'finance');
                    Mail::to($data->email)->send($email);
                }
            }

            return redirect()->back()->with('success', 'Sukses Menyimpan Data');
        } else {
            DB::rollback();
            return redirect()->back()->with('error', 'Gagal Menyimpan Data, Silahkan coba lagi');
        }
    }

    public function viewLaporanImbalHasil()
    {
        $data = Proyek::all('id', 'nama');
        return view('pages.admin.borrower.laporan_imbal_hasil', compact('data'));
        // return view('pages.admin.borrower.persetujuan_pencairan');
    }

    public function generateReportImbalHasilBorrower()
    {
        return Excel::download(new ReportImbalHasilBorrower(), 'Test.xlsx');
    }

    public function view_verifikasi_occasio()
    {
        return view('pages.admin.verifikator.analisa_pendanaan.daftar_pendanaan');
    }

    public function new_tahap_verifikasi($pengajuan_id)
    {
        return view('pages.admin.verifikator.analisa_pendanaan.detail_pendanaan', ['pengajuan_id'=> $pengajuan_id]);
    }

    public function list_verifikasi_occasio()
    {

        try {

            $list_verifikasi = DB::table('brw_analisa_pendanaan')
                ->join('brw_tipe_pendanaan', 'brw_analisa_pendanaan.jenis_pendanaan', 'brw_tipe_pendanaan.tipe_id')
                ->join('m_tujuan_pembiayaan',  'brw_analisa_pendanaan.tujuan_pendanaan', 'm_tujuan_pembiayaan.id')
                ->leftJoin('brw_hd_verifikator', 'brw_analisa_pendanaan.pengajuan_id', 'brw_hd_verifikator.pengajuan_id')
                ->whereIn('brw_analisa_pendanaan.status_verifikator', [1,2,3])
                ->select(['brw_analisa_pendanaan.pengajuan_id', 'tanggal_pengajuan', 'tanggal_verifikasi', 'penerima_pendanaan', 'nilai_pengajuan_pendanaan', 'pendanaan_nama', 'tujuan_pembiayaan', 'nama_verifikator', 'status_verifikator'])->orderBy('brw_analisa_pendanaan.pengajuan_id', 'desc')
                ->get();

            $dataArray = array();
            $i = 1;
            if (!empty($list_verifikasi)) {
                foreach ($list_verifikasi as $item) {
                    $column = array();
                    $column[0] = $i++;
                    $column[1] = (string)$item->pengajuan_id;
                    $column[2] = date('d-m-Y', strtotime($item->tanggal_pengajuan));
                    $column[3] = date('d-m-Y', strtotime($item->tanggal_verifikasi));
                    $column[4] = (string)$item->penerima_pendanaan;
                    $column[5] = (string)$item->pendanaan_nama;
                    $column[6] = (string)$item->tujuan_pembiayaan;
                    $column[7] = (string)$item->nilai_pengajuan_pendanaan;
                    $column[8] = (string)$item->nama_verifikator;
                    $column[9] = (string)$item->status_verifikator;

                    $dataArray[] = $column;
                }
            }

            $parsingJson = array('data' => $dataArray);

            echo json_encode($parsingJson);
        } catch (\Exception $e) {
        }
    }

    public function jumlah_taskverifikator_selesai($pengajuan_id){
        $occasio = new VerifikatorService();
        return $occasio->checkAllStatusPekerjaan($pengajuan_id);
    }

    public function list_daftar_pekerjaan($pengajuan_id)
    {
      
        $data = [
            'Surat Perintah Kerja(SPK)',
            'Dokumen Ceklis',
            'Verifikasi RAC',
            'Form Interview HRD',
            'Form Kunjungan Domisili',
            'Form Kunjungan Objek Pendanaan',
            'Form Informasi Pendanaan Berjalan',
            'Laporan Appraisal (KJPP)'
        ];

        $dataArray = array();
        $i = 1;

        $query = DB::table('brw_hd_verifikator')->where('pengajuan_id', $pengajuan_id);

        foreach ($data as $item) {
            $column = array();
            $column['no'] = $i;
            $column['keterangan'] = (string)$item; 
            $file_data_spk = '';

            if($i == 1){
                $result = $query->select('flag_spk', 'spk', 'created_at_spk', 'updated_at_spk')->first();
                $flag = $result->flag_spk;
                $data_created_at = $result->created_at_spk ? date('d-m-Y H:i:s', strtotime($result->created_at_spk)) : '';
                $data_updated_at = $result->updated_at_spk ? date('d-m-Y H:i:s', strtotime($result->updated_at_spk)) : '';
                $file_data_spk = route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $result->spk)]);
            }

            if($i == 2){
                $result = $query->select('flag_dokumen_ceklis', 'created_at_dokumen_ceklis', 'updated_at_dokumen_ceklis')->first();
                $flag = $result->flag_dokumen_ceklis ;
                $data_created_at = $result->created_at_dokumen_ceklis ? date('d-m-Y H:i:s', strtotime($result->created_at_dokumen_ceklis)) : '';
                $data_updated_at = $result->updated_at_dokumen_ceklis ? date('d-m-Y H:i:s', strtotime($result->updated_at_dokumen_ceklis)) : '';
            }

            
            if($i == 3){
                $result = $query->select('flag_rac', 'created_at_rac', 'updated_at_rac')->first();
                $flag = $result->flag_rac;
                $data_created_at = $result->created_at_rac ? date('d-m-Y H:i:s', strtotime($result->created_at_rac)) : '';
                $data_updated_at = $result->updated_at_rac ? date('d-m-Y H:i:s', strtotime($result->updated_at_rac)) : '';
            }

            if($i == 4){
                $result = $query->select('flag_interview_hrd', 'created_at_interview_hrd', 'updated_at_interview_hrd')->first();
                $flag = $result->flag_interview_hrd;
                $data_created_at = $result->created_at_interview_hrd ? date('d-m-Y H:i:s', strtotime($result->created_at_interview_hrd)) : '';
                $data_updated_at = $result->updated_at_interview_hrd ? date('d-m-Y H:i:s', strtotime($result->updated_at_interview_hrd)) : '';
            }

    
            if($i == 5){
                $result = $query->select('flag_kunjungan_domisili', 'created_at_kunjungan_domisili', 'updated_at_kunjungan_domisili')->first();
                $flag = $result->flag_kunjungan_domisili;
                $data_created_at = $result->created_at_kunjungan_domisili ? date('d-m-Y H:i:s', strtotime($result->created_at_kunjungan_domisili)) : '';
                $data_updated_at = $result->updated_at_kunjungan_domisili ? date('d-m-Y H:i:s', strtotime($result->updated_at_kunjungan_domisili)) : '';
            }

            if($i == 6){
                $result = $query->select('flag_kunjungan_objek_pendanaan', 'created_at_kunjungan_objek_pendanaan', 'updated_at_kunjungan_objek_pendanaan')->first();
                $flag = $result->flag_kunjungan_objek_pendanaan;
                $data_created_at = $result->created_at_kunjungan_objek_pendanaan ? date('d-m-Y H:i:s', strtotime($result->created_at_kunjungan_objek_pendanaan)) : '';
                $data_updated_at = $result->updated_at_kunjungan_objek_pendanaan ? date('d-m-Y H:i:s', strtotime($result->updated_at_kunjungan_objek_pendanaan)) : '';
            }

            if($i == 7){
                $result = $query->select('flag_informasi_pendanaan_berjalan', 'created_at_informasi_pendanaan_berjalan', 'updated_at_informasi_pendanaan_berjalan')->first();
                $flag = $result->flag_informasi_pendanaan_berjalan;
                $data_created_at = $result->created_at_informasi_pendanaan_berjalan ? date('d-m-Y H:i:s', strtotime($result->created_at_informasi_pendanaan_berjalan)) : '';
                $data_updated_at = $result->updated_at_informasi_pendanaan_berjalan ? date('d-m-Y H:i:s', strtotime($result->updated_at_informasi_pendanaan_berjalan)) : '';
            }

            if($i == 8){
                $result = $query->select('laporan_appraisal', 'created_at_laporan_appraisal', 'updated_at_laporan_appraisal')->first();
                $flag = $result->laporan_appraisal;
                $data_created_at = $result->created_at_laporan_appraisal ? date('d-m-Y H:i:s', strtotime($result->created_at_laporan_appraisal)) : '';
                $data_updated_at = $result->updated_at_laporan_appraisal ? date('d-m-Y H:i:s', strtotime($result->updated_at_laporan_appraisal)) : '';
            }

            $column['terakhir_diperbaharui'] =  $data_updated_at ? $data_updated_at : $data_created_at;
            $column['item'] = array("no"=>$i, "status"=> $flag, "file_spk"=> $file_data_spk);

            $i++;
            $dataArray[] = $column;
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function get_proses_verif_data($id_pengajuan)
    {
       
        $getData = BorrowerPengajuan::select()->where('brw_pengajuan.pengajuan_id', $id_pengajuan)->first();
        $pendanaan_nama = \DB::table('brw_tipe_pendanaan')->where('tipe_id', $getData->pendanaan_tipe)->value('pendanaan_nama');
		$tujuan_pendanaan = \DB::table('m_tujuan_pembiayaan')->where('id', $getData->pendanaan_tujuan)->value('tujuan_pembiayaan');

        return response()->json(['data_pengajuan' => $getData, 'jenis_pendanaan'=> $pendanaan_nama, 'tujuan_pendanaan' => $tujuan_pendanaan]);
    }

    public function save_process_verif_occasio(Request $request)
    {
        $pengajuan_id = $request->pengajuan_id;

        return redirect()->to('admin/borrower/verifikator/detail/' . $pengajuan_id);
    }

    public function view_detil_verif_occasio(
        BorrowerPengajuan $brw_pengajuan,
        BorrowerAnalisaPendanaan $borrower_analisa_pendanaan, 
        $pengajuan_id)
    {
        $data_borrower_analisa_pendanaan =  $borrower_analisa_pendanaan::where('pengajuan_id', $pengajuan_id)->first();
        $data_borrower = $brw_pengajuan::from('brw_pengajuan as a')
        ->leftJoin('brw_user_detail_pengajuan as b', 'b.pengajuan_id', 'a.pengajuan_id')
        ->select([
            'a.brw_id',
            'b.nama',
            'b.ktp',
            'b.jns_kelamin',
            'b.brw_pic'
        ])
        ->where('a.pengajuan_id', $pengajuan_id)
        ->first();

        return view('pages.admin.verifikator.analisa_pendanaan.daftar_pekerjaan', [
            'pengajuan_id'=> $pengajuan_id, 
            'data_borrower_analisa_pendanaan' => $data_borrower_analisa_pendanaan,
            'data_borrower' => $data_borrower
        ]);
    }

    public function next_page_verif_occasio(
        Request $request,
        BorrowerPengajuan $brw_pengajuan,
        BorrowerDokumenCeklis $brw_doc_ceklis,
        BorrowerDetailCeklistAnalisa $brw_dtl_ceklist_analisa,
        BorrowerFormInterviewHrd $brw_form_interview_hrd,
        BorrowerFormKunjunganDomisili $brw_form_kunjungan_domisili,
        BorrowerAgunan $brw_agunan,
        BorrowerPendanaanRumahLain $brw_pendanaan_rumah_lain,
        BorrowerPendanaanNonRumahLain $brw_pendanaan_non_rumah_lain
    )
    {
        $page_name = $request->page_name;
        $pengajuan_id = $request->pengajuan_id;

        $data = $brw_pengajuan::from('brw_pengajuan as a')
        // ->leftJoin('brw_hd_verifikator as b', 'b.pengajuan_id', 'a.pengajuan_id')
        ->leftJoin('brw_user_detail_penghasilan_pengajuan as c', 'c.pengajuan_id', 'a.pengajuan_id')
        ->leftJoin('brw_user_detail_pengajuan as d', 'd.pengajuan_id', 'a.pengajuan_id')
        ->leftJoin('brw_hd_verifikator as e', 'e.pengajuan_id', 'a.pengajuan_id')
        ->leftjoin('brw_tipe_pendanaan AS f', 'f.tipe_id', 'a.pendanaan_tipe')
        ->leftjoin('brw_analisa_pendanaan AS g', 'g.pengajuan_id', 'a.pengajuan_id')
        ->select([
            // 'a.brw_id',
            // 'a.pendanaan_dana_dibutuhkan',
            // 'a.pendanaan_tipe',
            // 'a.durasi_proyek',
            // 'b.nama_occasio',
            // 'b.created_at_dokumen_ceklis',
            'c.skema_pembiayaan',
            'c.sumber_pengembalian_dana',
            'c.nama_perusahaan',
            'c.nama_hrd',
            'c.no_fixed_line_hrd',
            'c.alamat_perusahaan as c_alamat_perusahaan',
            'c.provinsi as c_provinsi',
            'c.kelurahan as c_kelurahan',
            'c.kecamatan as c_kecamatan',
            'c.kab_kota as c_kab_kota',
            'c.kode_pos as c_kode_pos',
            'c.rt',
            'c.rw',
            // 'a.pengajuan_id',
            'a.*',
            'd.nama',
            'd.bidang_pekerjaan',
            'd.status_kawin',
            'e.*',
            'f.pendanaan_nama',
            'g.status_verifikator'
        ])
        ->where('a.pengajuan_id', $pengajuan_id)
        ->first();

        switch ($page_name) {

            case 'dokumen_ceklis_verifikasi':
                
                $data_dokumen_ceklist = $brw_doc_ceklis::where('pengajuan_id', $pengajuan_id)->first();

                return view('pages.admin.verifikator.analisa_pendanaan.detail_pekerjaan_occasio', [
                    'data' => $data,
                    'data_dokumen_ceklist' => $data_dokumen_ceklist
                ]);
                break;

            case 'dokumen_verifikasi_rac':
                $data_rac = $brw_dtl_ceklist_analisa::where('id_pengajuan', $pengajuan_id)->first();
                $data_verifikasi = DB::SELECT("SELECT * FROM brw_verifikasi_awal WHERE pengajuan_id = " . $pengajuan_id . " AND brw_id = " . $data->brw_id . "");
                
                return view('pages.admin.verifikator.analisa_pendanaan.detail_pekerjaan_occasio', [
                    'data' => $data,
                    'data_rac' => $data_rac,
                    'data_verifikasi' => ($data_verifikasi == NULL) ? 0 : $data_verifikasi
                ]);
                break;
            case 'form_interview_hrd':
                $data_brw_form_interview_hrd = $brw_form_interview_hrd::where('pengajuan_id', $pengajuan_id)->first();
                $m_bidang_pekerjaan = DB::table('m_bidang_pekerjaan')->get();
                $m_bank = MasterBank::all();

                return view('pages.admin.verifikator.analisa_pendanaan.detail_pekerjaan_occasio', [
                    'data' => $data,
                    'data_brw_form_interview_hrd' => $data_brw_form_interview_hrd,
                    'm_bidang_pekerjaan' => $m_bidang_pekerjaan,
                    'm_bank' => $m_bank,
                ]);
                break;
            case 'form_kunjungan_domisili':
                $data_brw_form_kunjungan_domisili = $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->first();
                $m_hub = DB::table('m_hub_ahli_waris')->get();
                return view('pages.admin.verifikator.analisa_pendanaan.detail_pekerjaan_occasio', [
                    'data' => $data,
                    'data_brw_form_kunjungan_domisili' => $data_brw_form_kunjungan_domisili,
                    'm_hub' => $m_hub
                ]);
                break;
            case 'form_kunjungan_objek_pendanaan':
                $data_brw_agunan = $brw_agunan::where('id_pengajuan', $pengajuan_id)->first();
                $m_hub = DB::table('m_hub_ahli_waris')->get();
                
                return view('pages.admin.verifikator.analisa_pendanaan.detail_pekerjaan_occasio', [
                    'data' => $data,
                    'data_brw_agunan' => $data_brw_agunan,
                    'm_hub' => $m_hub
                ]);
                break;
            case 'form_informasi_pendanaan_berjalan':
                $data_brw_pendanaan_rumah_lain = $brw_pendanaan_rumah_lain::where('pengajuan_id', $pengajuan_id)->get();
                $data_brw_pendanaan_non_rumah_lain = $brw_pendanaan_non_rumah_lain::where('pengajuan_id', $pengajuan_id)->get();
                $m_bank = DB::table('m_bank')->get();
                return view('pages.admin.verifikator.analisa_pendanaan.detail_pekerjaan_occasio', [
                    'data' => $data,
                    'data_brw_pendanaan_rumah_lain' => $data_brw_pendanaan_rumah_lain,
                    'data_brw_pendanaan_non_rumah_lain' => $data_brw_pendanaan_non_rumah_lain,
                    'm_banks' => $m_bank
                ]);
                break;
            default:
                # code...
                break;
        }

    }

    public function detailPekerjaanVerifikatorUpdate(
        Request $request,
        BorrowerDokumenLegalitas $brw_doc_legal,
        BorrowerHdVerifikator $brw_hd_verifikator,
        BorrowerDetailCeklistAnalisa $brw_dtl_ceklist_analisa,
        BorrowerAnalisaPendanaan $brw_analisa_pendanaan,
        BorrowerFormInterviewHrd $brw_form_interview_hrd,
        BorrowerFormKunjunganDomisili $brw_form_kunjungan_domisili,
        BorrowerAgunan $brw_agunan,
        BorrowerPengajuan $brw_pengajuan,
        BorrowerPendanaanNonRumahLain $brw_pendanaan_non_rumah_lain,
        BorrowerPendanaanRumahLain $brw_pendanaan_rumah_lain
    ) {
        $keterangan = $request->keterangan;
        $pengajuan_id = $request->pengajuan_id;
        $brw_id = $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->pluck('brw_id')->first();
        $result_update_status = 0;

        // START: Update status analis and Create brw_hd_analis
        $brw_analisa_pendanaan->where('pengajuan_id', $pengajuan_id)->update(["status_verifikator" => 2]);

        $update_brw_hd_analis = $brw_hd_verifikator::updateOrCreate([
            'pengajuan_id' => $pengajuan_id
        ], [
            'pengajuan_id' => $pengajuan_id,
            'nama_verifikator' => Auth::user()->firstname.' '.Auth::user()->lastname,
        ]);
        // END: START: Update status analis and Create brw_hd_analis


        switch ($keterangan) {

            case 'dokumen_ceklis_verifikasi':
                
                $update_brw_doc_legal = $brw_doc_legal::updateOrCreate([
                    'pengajuan_id' => $pengajuan_id
                ], [
                    'dokumen_legalitas_1' => $request->doc_legal_1 ? $request->doc_legal_1 : 0,
                    'dokumen_legalitas_2' => $request->doc_legal_2 ? $request->doc_legal_2 : 0,
                    'dokumen_legalitas_3' => $request->doc_legal_3 ? $request->doc_legal_3 : 0,
                    'dokumen_legalitas_4' => $request->doc_legal_4 ? $request->doc_legal_4 : 0,
                    'dokumen_legalitas_5' => $request->doc_legal_5 ? $request->doc_legal_5 : 0,
                    'dokumen_legalitas_6' => $request->doc_legal_6 ? $request->doc_legal_6 : 0,
                    'dokumen_legalitas_7' => $request->doc_legal_7 ? $request->doc_legal_7 : 0,
                    'dokumen_legalitas_8' => $request->doc_legal_8 ? $request->doc_legal_8 : 0,

                    'dokumen_legalitas_1_hasil' => $request->doc_legal_1_hasil ? $request->doc_legal_1_hasil : '',
                    'dokumen_legalitas_2_hasil' => $request->doc_legal_2_hasil ? $request->doc_legal_2_hasil : '',
                    'dokumen_legalitas_3_hasil' => $request->doc_legal_3_hasil ? $request->doc_legal_3_hasil : '',
                    'dokumen_legalitas_4_hasil' => $request->doc_legal_4_hasil ? $request->doc_legal_4_hasil : '',
                    'dokumen_legalitas_5_hasil' => $request->doc_legal_5_hasil ? $request->doc_legal_5_hasil : '',
                    'dokumen_legalitas_6_hasil' => $request->doc_legal_6_hasil ? $request->doc_legal_6_hasil : '',
                    'dokumen_legalitas_7_hasil' => $request->doc_legal_7_hasil ? $request->doc_legal_7_hasil : '',
                    'dokumen_legalitas_8_hasil' => $request->doc_legal_8_hasil ? $request->doc_legal_8_hasil : '',

                    'dokumen_pekerjaan_1' => $request->doc_pekerjaan_1 ? $request->doc_pekerjaan_1 : 0,

                    'dokumen_pekerjaan_1_hasil' => $request->doc_pekerjaan_1_hasil ? $request->doc_pekerjaan_1_hasil : 0,

                    'dokumen_penghasilan_1' => $request->doc_penghasilan_1 ? $request->doc_penghasilan_1 : 0,
                    'dokumen_penghasilan_2' => $request->doc_penghasilan_2 ? $request->doc_penghasilan_2 : 0,
                    'dokumen_penghasilan_3' => $request->doc_penghasilan_3 ? $request->doc_penghasilan_3 : 0,

                    'dokumen_penghasilan_1_hasil' => $request->doc_penghasilan_1_hasil ? $request->doc_penghasilan_1_hasil : '',
                    'dokumen_penghasilan_2_hasil' => $request->doc_penghasilan_2_hasil ? $request->doc_penghasilan_2_hasil : '',
                    'dokumen_penghasilan_3_hasil' => $request->doc_penghasilan_3_hasil ? $request->doc_penghasilan_3_hasil : '',

                    'dokumen_jaminan_1' => $request->doc_jaminan_1 ? $request->doc_jaminan_1 : 0,
                    'dokumen_jaminan_2' => $request->doc_jaminan_2 ? $request->doc_jaminan_2 : 0,
                    'dokumen_jaminan_3' => $request->doc_jaminan_3 ? $request->doc_jaminan_3 : 0,
                    'dokumen_jaminan_4' => $request->doc_jaminan_4 ? $request->doc_jaminan_4 : 0,

                    'dokumen_jaminan_1_hasil' => $request->doc_jaminan_1_hasil ? $request->doc_jaminan_1_hasil : '',
                    'dokumen_jaminan_2_hasil' => $request->doc_jaminan_2_hasil ? $request->doc_jaminan_2_hasil : '',
                    'dokumen_jaminan_3_hasil' => $request->doc_jaminan_3_hasil ? $request->doc_jaminan_3_hasil : '',
                    'dokumen_jaminan_4_hasil' => $request->doc_jaminan_4_hasil ? $request->doc_jaminan_4_hasil : '',
                ]);

                // START: Update dokumen ceklis
                $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        19, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AdminController.php', /* file varchar(100) */
                        4604 /* line int */
                    )"
                );

                $result_update_status = $update_status[0]->sout;
                // END: Update dokumen ceklis

                break;
            case 'dokumen_verifikasi_rac':
                
                // START: Detail ceklist analisa
                $update_brw_dtl_ceklist_analisa = $brw_dtl_ceklist_analisa::updateOrCreate([
                    'id_pengajuan' => $pengajuan_id
                ], [
                    // Doc Legalitas
                    'dok_legal_1_verif'  => $request->dok_legal_1_verif ? $request->dok_legal_1_verif : 0,
                    'dok_legal_2_verif'  => $request->dok_legal_2_verif ? $request->dok_legal_2_verif : 0,
                    'dok_legal_3_verif'  => $request->dok_legal_3_verif ? $request->dok_legal_3_verif : 0,
                    'dok_legal_4_verif'  => $request->dok_legal_4_verif ? $request->dok_legal_4_verif : 0,
                    'dok_legal_5_verif'  => $request->dok_legal_5_verif ? $request->dok_legal_5_verif : 0,
                    'dok_legal_6_verif'  => $request->dok_legal_6_verif ? $request->dok_legal_6_verif : 0,
                    'dok_legal_7_verif'  => $request->dok_legal_7_verif ? $request->dok_legal_7_verif : 0,
                    'dok_legal_8_verif'  => $request->dok_legal_8_verif ? $request->dok_legal_8_verif : 0,
                    'dok_legal_9_verif'  => $request->dok_legal_9_verif ? $request->dok_legal_9_verif : 0,
                    'dok_legal_10_verif' => $request->dok_legal_10_verif ? $request->dok_legal_10_verif : 0,
                    'dok_legal_11_verif' => $request->dok_legal_11_verif ? $request->dok_legal_11_verif : 0,

                    'dok_legal_1_verif_hasil'  => $request->dok_legal_1_verif_hasil ? $request->dok_legal_1_verif_hasil : '',
                    'dok_legal_2_verif_hasil'  => $request->dok_legal_2_verif_hasil ? $request->dok_legal_2_verif_hasil : '',
                    'dok_legal_3_verif_hasil'  => $request->dok_legal_3_verif_hasil ? $request->dok_legal_3_verif_hasil : '',
                    'dok_legal_4_verif_hasil'  => $request->dok_legal_4_verif_hasil ? $request->dok_legal_4_verif_hasil : '',
                    'dok_legal_5_verif_hasil'  => $request->dok_legal_5_verif_hasil ? $request->dok_legal_5_verif_hasil : '',
                    'dok_legal_6_verif_hasil'  => $request->dok_legal_6_verif_hasil ? $request->dok_legal_6_verif_hasil : '',
                    'dok_legal_7_verif_hasil'  => $request->dok_legal_7_verif_hasil ? $request->dok_legal_7_verif_hasil : '',
                    'dok_legal_8_verif_hasil'  => $request->dok_legal_8_verif_hasil ? $request->dok_legal_8_verif_hasil : '',
                    'dok_legal_9_verif_hasil'  => $request->dok_legal_9_verif_hasil ? $request->dok_legal_9_verif_hasil : '',
                    'dok_legal_10_verif_hasil' => $request->dok_legal_10_verif_hasil ? $request->dok_legal_10_verif_hasil : '',
                    'dok_legal_11_verif_hasil' => $request->dok_legal_11_verif_hasil ? $request->dok_legal_11_verif_hasil : '',

                    // Usia
                    'usia_1_verif' => $request->usia_1_verif ? $request->usia_1_verif : 0,
                    'usia_1_verif_hasil' => $request->usia_1_verif_hasil ? $request->usia_1_verif_hasil : '',

                    // Pekerjaan
                    'pekerjaan_1_verif' => $request->pekerjaan_1_verif ? $request->pekerjaan_1_verif : 0,
                    'pekerjaan_2_verif' => $request->pekerjaan_2_verif ? $request->pekerjaan_2_verif : 0,
                    'pekerjaan_3_verif' => $request->pekerjaan_3_verif ? $request->pekerjaan_3_verif : 0,
                    'pekerjaan_4_verif' => $request->pekerjaan_4_verif ? $request->pekerjaan_4_verif : 0,
                    'pekerjaan_5_verif' => $request->pekerjaan_5_verif ? $request->pekerjaan_5_verif : 0,
                    'pekerjaan_6_verif' => $request->pekerjaan_6_verif ? $request->pekerjaan_6_verif : 0,

                    'pekerjaan_1_verif_hasil' => $request->pekerjaan_1_verif_hasil ? $request->pekerjaan_1_verif_hasil : '',
                    'pekerjaan_2_verif_hasil' => $request->pekerjaan_2_verif_hasil ? $request->pekerjaan_2_verif_hasil : '',
                    'pekerjaan_3_verif_hasil' => $request->pekerjaan_3_verif_hasil ? $request->pekerjaan_3_verif_hasil : '',
                    'pekerjaan_4_verif_hasil' => $request->pekerjaan_4_verif_hasil ? $request->pekerjaan_4_verif_hasil : '',
                    'pekerjaan_5_verif_hasil' => $request->pekerjaan_5_verif_hasil ? $request->pekerjaan_5_verif_hasil : '',
                    'pekerjaan_6_verif_hasil' => $request->pekerjaan_6_verif_hasil ? $request->pekerjaan_6_verif_hasil : '',

                    // Verifikasi
                    'verifikasi_1_verif' => $request->verifikasi_1_verif ? $request->verifikasi_1_verif : 0,
                    'verifikasi_2_verif' => $request->verifikasi_2_verif ? $request->verifikasi_2_verif : 0,
                    'verifikasi_3_verif' => $request->verifikasi_3_verif ? $request->verifikasi_3_verif : 0,
                    'verifikasi_4_verif' => $request->verifikasi_4_verif ? $request->verifikasi_4_verif : 0,
                    'verifikasi_5_verif' => $request->verifikasi_5_verif ? $request->verifikasi_5_verif : 0,
                    'verifikasi_6_verif' => $request->verifikasi_6_verif ? $request->verifikasi_6_verif : 0,

                    'verifikasi_1_verif_hasil' => $request->verifikasi_1_verif_hasil ? $request->verifikasi_1_verif_hasil : '',
                    'verifikasi_2_verif_hasil' => $request->verifikasi_2_verif_hasil ? $request->verifikasi_2_verif_hasil : '',
                    'verifikasi_3_verif_hasil' => $request->verifikasi_3_verif_hasil ? $request->verifikasi_3_verif_hasil : '',
                    'verifikasi_4_verif_hasil' => $request->verifikasi_4_verif_hasil ? $request->verifikasi_4_verif_hasil : '',
                    'verifikasi_5_verif_hasil' => $request->verifikasi_5_verif_hasil ? $request->verifikasi_5_verif_hasil : '',
                    'verifikasi_6_verif_hasil' => $request->verifikasi_6_verif_hasil ? $request->verifikasi_6_verif_hasil : '',

                    // Pendapatan
                    'pendapatan_1_verif' => $request->pendapatan_1_verif ? $request->pendapatan_1_verif : 0,
                    'pendapatan_2_verif' => $request->pendapatan_2_verif ? $request->pendapatan_2_verif : 0,
                    'pendapatan_3_verif' => $request->pendapatan_3_verif ? $request->pendapatan_3_verif : 0,
                    'pendapatan_4_verif' => $request->pendapatan_4_verif ? $request->pendapatan_4_verif : 0,
                    'pendapatan_5_verif' => $request->pendapatan_5_verif ? $request->pendapatan_5_verif : 0,
                    'pendapatan_6_verif' => $request->pendapatan_6_verif ? $request->pendapatan_6_verif : 0,
                    'pendapatan_7_verif' => $request->pendapatan_7_verif ? $request->pendapatan_7_verif : 0,

                    'pendapatan_1_verif_hasil' => $request->pendapatan_1_verif_hasil ? $request->pendapatan_1_verif_hasil : '',
                    'pendapatan_2_verif_hasil' => $request->pendapatan_2_verif_hasil ? $request->pendapatan_2_verif_hasil : '',
                    'pendapatan_3_verif_hasil' => $request->pendapatan_3_verif_hasil ? $request->pendapatan_3_verif_hasil : '',
                    'pendapatan_4_verif_hasil' => $request->pendapatan_4_verif_hasil ? $request->pendapatan_4_verif_hasil : '',
                    'pendapatan_5_verif_hasil' => $request->pendapatan_5_verif_hasil ? $request->pendapatan_5_verif_hasil : '',
                    'pendapatan_6_verif_hasil' => $request->pendapatan_6_verif_hasil ? $request->pendapatan_6_verif_hasil : '',
                    'pendapatan_7_verif_hasil' => $request->pendapatan_7_verif_hasil ? $request->pendapatan_7_verif_hasil : '',

                    // Jaminan
                    'jaminan_1_verif' => $request->jaminan_1_verif ? $request->jaminan_1_verif : 0,
                    'jaminan_2_verif' => $request->jaminan_2_verif ? $request->jaminan_2_verif : 0,
                    'jaminan_3_verif' => $request->jaminan_3_verif ? $request->jaminan_3_verif : 0,
                    'jaminan_4_verif' => $request->jaminan_4_verif ? $request->jaminan_4_verif : 0,
                    'jaminan_5_verif' => $request->jaminan_5_verif ? $request->jaminan_5_verif : 0,
                    'jaminan_6_verif' => $request->jaminan_6_verif ? $request->jaminan_6_verif : 0,
                    'jaminan_7_verif' => $request->jaminan_7_verif ? $request->jaminan_7_verif : 0,
                    'jaminan_8_verif' => $request->jaminan_8_verif ? $request->jaminan_8_verif : 0,
                    'jaminan_9_verif' => $request->jaminan_9_verif ? $request->jaminan_9_verif : 0,

                    'jaminan_1_verif_hasil' => $request->jaminan_1_verif_hasil ? $request->jaminan_1_verif_hasil : '',
                    'jaminan_2_verif_hasil' => $request->jaminan_2_verif_hasil ? $request->jaminan_2_verif_hasil : '',
                    'jaminan_3_verif_hasil' => $request->jaminan_3_verif_hasil ? $request->jaminan_3_verif_hasil : '',
                    'jaminan_4_verif_hasil' => $request->jaminan_4_verif_hasil ? $request->jaminan_4_verif_hasil : '',
                    'jaminan_5_verif_hasil' => $request->jaminan_5_verif_hasil ? $request->jaminan_5_verif_hasil : '',
                    'jaminan_6_verif_hasil' => $request->jaminan_6_verif_hasil ? $request->jaminan_6_verif_hasil : '',
                    'jaminan_7_verif_hasil' => $request->jaminan_7_verif_hasil ? $request->jaminan_7_verif_hasil : '',
                    'jaminan_8_verif_hasil' => $request->jaminan_8_verif_hasil ? $request->jaminan_8_verif_hasil : '',
                    'jaminan_9_verif_hasil' => $request->jaminan_9_verif_hasil ? $request->jaminan_9_verif_hasil : '',
                ]);
                // END: Detail ceklist analisa

                // START: Update RAC Verifikator
                $rac_verif = $brw_dtl_ceklist_analisa::where('id_pengajuan', $request->pengajuan_id)->first();
                if (empty($rac_verif->created_at_verif) || $rac_verif->created_at_verif == 0) {
                    $brw_dtl_ceklist_analisa::where('id_pengajuan', $pengajuan_id)->update([
                        'created_at_verif' => Carbon::now()
                    ]);
                } else {
                    $brw_dtl_ceklist_analisa::where('id_pengajuan', $pengajuan_id)->update([
                        'updated_at_verif' => Carbon::now()
                    ]);
                }
                // END: Update RAC Verifikator

                // START: Update dokumen RAC
                $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        20, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AdminController.php', /* file varchar(100) */
                        4724 /* line int */
                    )"
                );

                $result_update_status = $update_status[0]->sout;
                // END: Update dokumen RAC

                break;
            case 'form_interview_hrd':
                $update_brw_form_interview_hrd = $brw_form_interview_hrd::updateOrCreate([
                    'pengajuan_id' => $pengajuan_id
                ], [
                    'tanggal_interview'                 => $request->tanggal_interview,
                    'jam_interview'                     => $request->jam_interview,
                    'verifikasi_melalui'                => $request->verifikasi_melalui,
                    'jabatan_hrd'                       => $request->jabatan_hrd,
                    'jumlah_total_karyawan'             => $request->jumlah_total_karyawan,

                    'pertanyaan_1_jawaban'              => $request->pertanyaan_1_jawaban,
                    'pertanyaan_1_deskripsi'            => $request->pertanyaan_1_deskripsi,

                    'pertanyaan_2'                      => $request->pertanyaan_2,

                    'pertanyaan_3_jawaban'              => $request->pertanyaan_3_jawaban,
                    'pertanyaan_3_deskripsi'            => $request->pertanyaan_3_deskripsi,

                    'pertanyaan_4_jawaban'              => $request->pertanyaan_4_jawaban,
                    'pertanyaan_4_deskripsi'            => $request->pertanyaan_4_deskripsi,

                    'pertanyaan_5'                      => preg_replace('/[^0-9]/', '', $request->pertanyaan_5),

                    'pertanyaan_6_gaji_pokok'           => $request->pertanyaan_6_gaji_pokok,
                    'pertanyaan_6_tunjangan_jabatan'    => $request->pertanyaan_6_tunjangan_jabatan,
                    'pertanyaan_6_lembur'               => $request->pertanyaan_6_lembur,
                    'pertanyaan_6_transport'            => $request->pertanyaan_6_transport,
                    'pertanyaan_6_uang_makan'           => $request->pertanyaan_6_uang_makan,
                    'pertanyaan_6_bonus'                => $request->pertanyaan_6_bonus,
                    'pertanyaan_6_bpjs'                 => $request->pertanyaan_6_bpjs,
                    'pertanyaan_6_jht'                  => $request->pertanyaan_6_jht,
                    'pertanyaan_6_jkk'                  => $request->pertanyaan_6_jkk,
                    'pertanyaan_6_jpn'                  => $request->pertanyaan_6_jpn,
                    'pertanyaan_6_pph21'                => $request->pertanyaan_6_pph21,

                    'pertanyaan_7_tanggal'              => $request->pertanyaan_7_jawaban,
                    'pertanyaan_7_bank'                 => $request->pertanyaan_7_bank,

                    'pertanyaan_8_jawaban'              => $request->pertanyaan_8_jawaban,
                    'pertanyaan_8_deskripsi'            => $request->pertanyaan_8_deskripsi,

                    'pertanyaan_9_jawaban'              => $request->pertanyaan_9_jawaban,
                    'pertanyaan_9_deskripsi'            => $request->pertanyaan_9_deskripsi,

                    'pertanyaan_10_jawaban'             => $request->pertanyaan_10_jawaban,
                    'pertanyaan_10_deskripsi'           => $request->pertanyaan_10_deskripsi,

                    'pertanyaan_11_jawaban'             => $request->pertanyaan_11_jawaban,
                    'pertanyaan_11_plafond'             => preg_replace('/[^0-9]/', '', $request->pertanyaan_11_plafond),
                    'pertanyaan_11_outstanding'         => preg_replace('/[^0-9]/', '', $request->pertanyaan_11_outstanding),
                    'pertanyaan_11_jangka_waktu'        => $request->pertanyaan_11_jangka_waktu,
                    'pertanyaan_11_margin'              => $request->pertanyaan_11_margin,
                    'pertanyaan_11_angsuran'            => preg_replace('/[^0-9]/', '', $request->pertanyaan_11_angsuran),
                ]);

                $store_path = 'verifikator/' . $brw_id . '/' . $pengajuan_id .'/form_interview_hrd';

                // Foto Bukti Telpon
                if ($request->hasFile('bukti_telpon')) {

                    $file = $request->file('bukti_telpon');
                    $filename = 'bukti_telpon' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();

                    $old_file = $brw_form_interview_hrd::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->bukti_interview)) {
                        Storage::disk('private')->delete($old_file->bukti_interview);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        $err_msg = [
                            'pengajuan_id' => $pengajuan_id,
                            'desc' => 'Proses unggah file bukti telpon',
                        ];
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 4821,
                            'description' => $err_msg
                        ]);
                    } else {
                        $bukti_telpon = $path;

                        $brw_form_interview_hrd::where('pengajuan_id', $pengajuan_id)->update(['bukti_interview' => $bukti_telpon]);
                    }
                }
                
                // Foto Verifikator Kantor
                if ($request->hasFile('foto_verifikator_kantor')) {

                    $file = $request->file('foto_verifikator_kantor');
                    $filename = 'foto_verifikator_kantor' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();

                    $old_file = $brw_form_interview_hrd::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->foto_didepan_kantor_lobby)) {
                        Storage::disk('private')->delete($old_file->foto_didepan_kantor_lobby);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        $err_msg = [
                            'pengajuan_id' => $pengajuan_id,
                            'desc' => 'Proses unggah file bukti bersama hrd gagal',
                        ];
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 4853,
                            'description' => $err_msg
                        ]);
                    } else {
                        $foto_didepan_kantor_lobby = $path;

                        $brw_form_interview_hrd::where('pengajuan_id', $pengajuan_id)->update(['foto_didepan_kantor_lobby' => $foto_didepan_kantor_lobby]);
                    }
                } 
                
                // Foto Verifikator HRD
                if ($request->hasFile('foto_verifikator_hrd')) {

                    $file = $request->file('foto_verifikator_hrd');
                    $filename = 'foto_verifikator_hrd' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $old_file = $brw_form_interview_hrd::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->foto_bersama_hrd)) {
                        Storage::disk('private')->delete($old_file->foto_bersama_hrd);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        $err_msg = [
                            'pengajuan_id' => $pengajuan_id,
                            'desc' => 'Proses unggah file bukti bersama hrd gagal',
                        ];
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 4884,
                            'description' => $err_msg
                        ]);
                    } else {
                        $foto_bersama_hrd = $path;

                        $brw_form_interview_hrd::where('pengajuan_id', $pengajuan_id)->update(['foto_bersama_hrd' => $foto_bersama_hrd]);
                    }
                } 

                // START: Update dokumen interview
                $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        22, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AdminController.php', /* file varchar(100) */
                        4888 /* line int */
                    )"
                );

                $result_update_status = $update_status[0]->sout;
                // END: Update dokumen interview

                break;
            case 'form_kunjungan_domisili':
                
                $update_brw_form_kunjungan_domisili = $brw_form_kunjungan_domisili::updateOrCreate([
                    'pengajuan_id' => $pengajuan_id
                ], [
                    'tanggal_kunjungan'             => $request->tanggal_kunjungan,
                    'jam_kunjungan'                 => $request->jam_kunjungan,
                    'nama_yang_ditemui'             => $request->nama_yang_ditemui,
                    'selaku'                        => $request->selaku,
                    'alamat_rumah_saat_ini'         => $request->alamat_rumah_saat_ini,
                    'provinsi'                      => $request->provinsi,
                    'kota_kabupaten'                => $request->kota_kabupaten,
                    'kecamatan'                     => $request->kecamatan,
                    'kelurahan'                     => $request->kelurahan,
                    'kode_pos'                      => $request->kode_pos,
                    'status_rumah_saat_ini'         => $request->status_rumah,
                    'lama_tinggal'                  => $request->lama_tinggal,
                    'kondisi_lingkungan'            => $request->kondisi_lingkungan,
                    'informasi_negatif'             => $request->informasi_negatif,
                    'nama_pemberi_informasi'        => $request->nama_pemberi_informasi,
                    'tlp_hp_pemberi_informasi'      => $request->tlp_hp_pemberi_informasi,
                    'penjelasan_terkait_pengajuan'  => $request->penjelasan_terkait_pengajuan
                ]);

                // Foto rumah tampak depan
                if ($request->hasFile('rumah_depan')) {

                    $file = $request->file('rumah_depan');
                    $filename = 'rumah_depan' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $store_path = 'verifikator/' . $pengajuan_id .'/kunjungan_domisili';

                    $old_file = $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->foto_tampak_depan_rumah)) {
                        Storage::disk('private')->delete($old_file->foto_tampak_depan_rumah);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 4816,
                            'description' => "Proses unggah file bukti telpon berhasil"
                        ]);
                    } else {
                        $foto_tampak_depan_rumah = $path;

                        $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->update(['foto_tampak_depan_rumah' => $foto_tampak_depan_rumah]);
                    }
                }

                // Foto rumah tampak dalam
                if ($request->hasFile('rumah_dalam')) {

                    $file = $request->file('rumah_dalam');
                    $filename = 'rumah_dalam' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $store_path = 'verifikator/' . $pengajuan_id .'/kunjungan_domisili';

                    $old_file = $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->foto_tampak_dalam_rumah)) {
                        Storage::disk('private')->delete($old_file->foto_tampak_dalam_rumah);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 4816,
                            'description' => "Proses unggah foto rumah tampak dalam gagal"
                        ]);
                    } else {
                        $foto_tampak_dalam_rumah = $path;

                        $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->update(['foto_tampak_dalam_rumah' => $foto_tampak_dalam_rumah]);
                    }
                }

                // Foto lingkungan rumah
                if ($request->hasFile('rumah_lingkungan')) {

                    $file = $request->file('rumah_lingkungan');
                    $filename = 'rumah_lingkungan' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $store_path = 'verifikator/' . $pengajuan_id .'/kunjungan_domisili';

                    $old_file = $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->foto_lingkungan_rumah)) {
                        Storage::disk('private')->delete($old_file->foto_lingkungan_rumah);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 4816,
                            'description' => "Proses unggah foto lingkungan rumah gagal"
                        ]);
                    } else {
                        $foto_lingkungan_rumah = $path;

                        $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->update(['foto_lingkungan_rumah' => $foto_lingkungan_rumah]);
                    }
                }

                // Foto petugas
                if ($request->hasFile('foto_petugas_depan_rumah')) {

                    $file = $request->file('foto_petugas_depan_rumah');
                    $filename = 'foto_petugas_depan_rumah' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $store_path = 'verifikator/' . $pengajuan_id .'/kunjungan_domisili';

                    $old_file = $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->foto_petugas_depan_rumah)) {
                        Storage::disk('private')->delete($old_file->foto_petugas_depan_rumah);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 5028,
                            'description' => "Proses unggah foto petugas gagal"
                        ]);
                    } else {
                        $foto_petugas_depan_rumah = $path;

                        $brw_form_kunjungan_domisili::where('pengajuan_id', $pengajuan_id)->update(['foto_petugas_depan_rumah' => $foto_petugas_depan_rumah]);
                    }
                }

                // START: Update form kunjungan domisili
                $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        23, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AdminController.php', /* file varchar(100) */
                        5041 /* line int */
                    )"
                );

                $result_update_status = $update_status[0]->sout;
                // END: Update form kunjungan domisili

                break;
            case 'form_kunjungan_objek_pendanaan' :
                $update_brw_agunan = $brw_agunan::updateOrCreate([
                    'id_pengajuan' => $pengajuan_id
                ], [
                    'jenis_objek_pendanaan' => $request->jenis_objek_pendanaan,
                    'jenis_agunan'          => $request->jenis_agunan,
                    'nomor_agunan'          => $request->nomor_agunan,
                    'atas_nama_agunan'      => $request->atas_nama_agunan,
                    'luas_tanah'            => $request->luas_tanah,
                    'luas_bangunan'         => $request->luas_bangunan,
                    'nomor_surat_ukur'      => $request->nomor_surat_ukur,
                    'tanggal_surat_ukur'    => $request->tanggal_surat_ukur,

                    // 'tanggal_terbit'        => $request->tanggal_terbit,
                    // 'tanggal_jatuh_tempo'   => $request->tanggal_jatuh_tempo,
                    // 'kantor_penerbit'       => $request->kantor_penerbit,
                    // 'blok_nomor'            => $request->blok_nomor,
                    'tanggal_terbit_imb'    => $request->tanggal_terbit_imb,
                    'tipe_agunan'           => $request->tipe_agunan,

                    // If status IMB Checked
                    'status_imb'            => $request->is_status_imb == '1' ? $request->status_imb : '',
                    'no_imb'                => $request->is_status_imb == '1' ? $request->no_imb : '',
                    'luas_tanah_imb'        => $request->is_status_imb == '1' ? $request->luas_tanah_imb : '',
                    'luas_bangunan_imb'     => $request->is_status_imb == '1' ? $request->luas_bangunan_imb : '',
                    'tanggal_terbit_imb'    => $request->is_status_imb == '1' ? $request->tanggal_terbit_imb : '',
                    'atas_nama_imb'         => $request->is_status_imb == '1' ? $request->atas_nama_imb : '',
                ]);

                $update_brw_pengajuan = $brw_pengajuan::updateOrCreate([
                    'pengajuan_id'              => $pengajuan_id
                ], [
                    
                    'tanggal_kunjungan'         => $request->tanggal_kunjungan,
                    'geocode'                   => $request->geocode,
                    'telepon_rumah'             => $request->telepon_rumah,
                    'daya_listrik'              => $request->daya_listrik,
                    'air'                       => $request->air,
                    'internet_wifi'             => $request->internet_wifi,

                    'jumlah_lantai'             => $request->is_bangunan_bertingkat == '1' ? $request->jumlah_lantai : '',
                    'jenis_properti'            => $request->jenis_properti,
                    'dibangun_tahun'            => $request->jenis_properti == '1' ? $request->dibangun_tahun : '', //Jenis bangunan baru
                    'renovasi_tahun'            => $request->jenis_properti == '2' && $request->is_renovasi == '1' ? $request->renovasi_tahun : '', //Jenis bangunan bekas
                    'akses_jalan'               => $request->akses_jalan,
                    'lebar_jalan'               => $request->lebar_jalan,
                    'lingkungan_sekitar'        => $request->lingkungan_sekitar,

                    'dihuni_oleh'               => $request->dihuni_oleh,
                    'hubungan_sebagai'          => $request->dihuni_oleh == '3' ? $request->hubungan_sebagai : '', //Jika dihuni orang lain
                    'dikontrak_atau_sewa'       => $request->dihuni_oleh == '3' ? $request->dikontrak_atau_sewa : '', //Jika dihuni orang lain
                    'tanggal_berakhir_kontrak'  => $request->dikontrak_atau_sewa == '1' ? $request->tanggal_berakhir_kontrak : '', //Jika disewa, masukkan tangal berakhir sewa
                    'informasi_lain_lain'       => $request->informasi_lain_lain,
                    'narasumber_nama'           => $request->narasumber_nama,
                    'narasumber_selaku'         => $request->narasumber_selaku,
                    'narasumber_tlp_hp'         => $request->narasumber_tlp_hp,
                ]);

                $store_path = 'verifikator/' . $brw_id . '/' . $pengajuan_id .'/kunjungan_objek_pendanaan';
                $err_msg = [];
                // Foto objek tampak depan
                if ($request->hasFile('objek_depan')) {

                    $file = $request->file('objek_depan');
                    $filename = 'objek_depan_' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $old_file = $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->pic_tampak_depan_objek)) {
                        Storage::disk('private')->delete($old_file->pic_tampak_depan_objek);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        $err_msg = [
                            'pengajuan_id' => $pengajuan_id,
                            'desc' => 'Proses unggah foto objek tampak depan gagal',
                        ];
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 5147,
                            'description' => $err_msg
                        ]);
                    } else {
                        $pic_tampak_depan_objek = $path;

                        $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->update(['pic_tampak_depan_objek' => $pic_tampak_depan_objek]);
                    }
                }

                // Foto objek tampak dalam
                if ($request->hasFile('objek_dalam')) {

                    $file = $request->file('objek_dalam');
                    $filename = 'objek_dalam_' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $old_file = $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->pic_tampak_dalam_objek)) {
                        Storage::disk('private')->delete($old_file->pic_tampak_dalam_objek);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        $err_msg = [
                            'pengajuan_id' => $pengajuan_id,
                            'desc' => 'Proses unggah foto objek tampak depan gagal',
                        ];
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 5176,
                            'description' => $err_msg
                        ]);
                    } else {
                        $pic_tampak_dalam_objek = $path;

                        $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->update(['pic_tampak_dalam_objek' => $pic_tampak_dalam_objek]);
                    }
                }

                // Foto lingkungan objek
                if ($request->hasFile('objek_lingkungan')) {

                    $file = $request->file('objek_lingkungan');
                    $filename = 'objek_lingkungan_' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();
                    $old_file = $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->pic_tampak_kondisi_lingkungan_objek)) {
                        Storage::disk('private')->delete($old_file->pic_tampak_kondisi_lingkungan_objek);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        $err_msg = [
                            'pengajuan_id' => $pengajuan_id,
                            'desc' => 'Proses unggah foto lingkungan objek gagal',
                        ];
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 5207,
                            'description' => $err_msg
                        ]);
                    } else {
                        $pic_tampak_kondisi_lingkungan_objek = $path;

                        $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->update(['pic_tampak_kondisi_lingkungan_objek' => $pic_tampak_kondisi_lingkungan_objek]);
                    }
                }

                // Foto petugas
                if ($request->hasFile('foto_petugas_depan_objek')) {

                    $file = $request->file('foto_petugas_depan_objek');
                    $filename = 'foto_petugas_depan_objek_' . Carbon::now()->format('d-m-Y') . '.' . $file->getClientOriginalExtension();

                    $old_file = $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->first();

                    if (Storage::disk('private')->exists($old_file->pic_petugas)) {
                        Storage::disk('private')->delete($old_file->pic_petugas);
                    }

                    $path = $file->storeAs($store_path, $filename, 'private');

                    if (!$path) {
                        $err_msg = [
                            'pengajuan_id' => $pengajuan_id,
                            'desc' => 'Proses unggah foto petugas gagal',
                        ];
                        DB::table('log_db_app')->insert([
                            'file_name' => 'AdminController.php',
                            'line' => 5028,
                            'description' => $err_msg
                        ]);
                    } else {
                        $pic_petugas = $path;

                        $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->update(['pic_petugas' => $pic_petugas]);
                    }
                }

                // START: Update form kunjungan objek pendanaan
                $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        24, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AdminController.php', /* file varchar(100) */
                        5239 /* line int */
                    )"
                );

                $result_update_status = $update_status[0]->sout;
                // END: Update form kunjungan objek pendanaan

                break;
            case 'form_informasi_pendanaan_berjalan' :
                // return $request->all();

                // START: Pendanaan Rumah Lain
                $delete_old_data_pendanaan_rumah_lain = $brw_pendanaan_rumah_lain::where('pengajuan_id', $request->pengajuan_id)->delete();

                $total_item_pendanaan_rumah_lain = $request->text_status_rumah_lain ? count($request->text_status_rumah_lain) : 0;
                for ($i=0; $i < $total_item_pendanaan_rumah_lain; $i++) { 
                    $update_brw_pendanaan_rumah_lain = $brw_pendanaan_rumah_lain::create([
                        'pengajuan_id'  => $pengajuan_id,
                        'rumah_ke'      => $request->rumah_ke[$i],
                        'status'        => $request->text_status_rumah_lain[$i],
                        'bank'          => $request->text_bank_rumah_lain[$i],
                        'plafond'       => preg_replace('/[^0-9]/', '', $request->text_plafond_rumah_lain[$i]),
                        'jangka_waktu'  => $request->text_jangka_waktu_rumah_lain[$i],
                        'outstanding'   => preg_replace('/[^0-9]/', '', $request->text_outstanding_rumah_lain[$i]),
                        'angsuran'      => preg_replace('/[^0-9]/', '', $request->text_angsuran_rumah_lain[$i])
                    ]);
                }
                // END: Pendanaan Rumah Lain

                // START: Pendanaan No Rumah Lain
                if ($request->status_kepemilikan_non_rumah_lain == '1') { // Jika memiliki pendanaan non rumah lain
                    $total_item = $request->text_id ? count($request->text_id) : 0;
                    for ($i=0; $i < $total_item; $i++) { 
                        if ($request->text_bank_non_rumah_lain[$i] || $request->text_plafond_non_rumah_lain[$i] || $request->text_jangka_waktu_non_rumah_lain[$i] || $request->text_outstanding[$i] || $request->text_angsuran_non_rumah_lain[$i]) {
                            $update_brw_pendanaan_non_rumah_lain = $brw_pendanaan_non_rumah_lain::updateOrCreate([
                                'id'  => $request->text_id[$i]
                            ], [
                                'pengajuan_id'  => $pengajuan_id,
                                'bank'          => $request->text_bank_non_rumah_lain[$i],
                                'plafond'       => preg_replace('/[^0-9]/', '', $request->text_plafond_non_rumah_lain[$i]),
                                'jangka_waktu'  => $request->text_jangka_waktu_non_rumah_lain[$i],
                                'outstanding'   => preg_replace('/[^0-9]/', '', $request->text_outstanding[$i]),
                                'angsuran'      => preg_replace('/[^0-9]/', '', $request->text_angsuran_non_rumah_lain[$i])
                            ]);
                        }

                    }

                    $total_item_deleted = $request->deleted_data_non_rumah_lain ? count($request->deleted_data_non_rumah_lain) : 0;
                    for ($i=0; $i < $total_item_deleted; $i++) { 
                        $brw_pendanaan_non_rumah_lain::where('id', $request->deleted_data_non_rumah_lain[$i])->delete();
                    }
                } else { // Jika tidak memiliki pendanaan non rumah lain
                    $brw_pendanaan_non_rumah_lain::where('pengajuan_id', $pengajuan_id)->delete();
                }
                // END: Pendanaan No Rumah Lain

                // START: Update form pendanaan berjalan
                $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        25, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AdminController.php', /* file varchar(100) */
                        5319 /* line int */
                    )"
                );

                $result_update_status = $update_status[0]->sout;
                // END: Update form pendanaan berjalan

                break;
            default:
                $response = [
                    'status' => 'error',
                    'msg' => 'Data form tidak terdaftar, silahkan coba lagi',
                ];

                return $response;
                break;
        }

        if ($result_update_status == 1) {
            $response = [
                'status' => 'success',
                'msg' => 'Data berhasil disimpan',
            ];

            return $response;
        } else {
            $response = [
                'status' => 'error',
                'msg' => 'Data gagal disimpan, silahkan coba lagi',
            ];

            return $response;
        }

    }

    public function uploadLaporanAppraisal(
        BorrowerPengajuan $brw_pengajuan, 
        BorrowerHdVerifikator $brw_hd_verifikator, 
        Request $request
    ) {
        $pengajuan_id   = $request->pengajuan_id;
        $brw_id         = $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->pluck('brw_id')->first();
        $store_path     = 'verifikator/' . $brw_id . '/' . $pengajuan_id .'/laporan_appraisal';

        if ($request->hasFile('file_laporan_keuangan')) {
            $file = $request->file('file_laporan_keuangan');
            $file_size = $file->getSize();
            $file_extension = $file->getClientOriginalExtension();
            $filename = 'laporan_appraisal_' . Carbon::now()->format('d-m-Y') . '.' . $file_extension;
            $maxsize = 1000000; // 1MB

            if ($file_size > $maxsize) {
                return response()->json(['status' => 'failed', 'message' => 'Ukuran File Melebihi 1 MB']); 
            } else {

                $old_file = $brw_hd_verifikator::where('pengajuan_id', $pengajuan_id)->first();

                if (Storage::disk('private')->exists($old_file->laporan_appraisal)) {
                    Storage::disk('private')->delete($old_file->laporan_appraisal);
                }

                $path = $file->storeAs($store_path, $filename, 'private');

                if (!$path) {
                    $err_msg = [
                        'pengajuan_id' => $pengajuan_id,
                        'desc' => 'Proses unggah file appraisal gagal',
                    ];
                    DB::table('log_db_app')->insert([
                        'file_name' => 'AdminController.php',
                        'line' => 5363,
                        'description' => $err_msg
                    ]);
                } else {
                    $file_appraisal = $path;

                    $brw_hd_verifikator::where('pengajuan_id', $pengajuan_id)->update(['laporan_appraisal' => $file_appraisal]);

                    // START: Update form pendanaan berjalan
                    $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                    $update_status = DB::select(
                        "CALL proc_click_button_borrower(
                            26, /* form_id int */
                            '$user_login', /* user_login varchar(35) */
                            '$pengajuan_id', /* pengajuan_id int */
                            'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                            'AdminController.php', /* file varchar(100) */
                            5386 /* line int */
                        )"
                    );

                    $result_update_status = $update_status[0]->sout;
                    // END: Update form pendanaan berjalan

                    return response()->json(['status' => 'success', 'message' => 'File berhasil di upload', 'img_url' => $file_appraisal]);
                }
            }
        } else {
            return response()->json(['status' => 'failed', 'message' => 'File kosong']);
        }
    }

    public function kirimVerifikator(Request $request){
        $verifikator = new VerifikatorService();
        $result = $verifikator->kirimVerifikasiData($request);        
        return ($result) ? '1' : '0';
    }

    public function getListPerubahanProfile(AdminPermintaanPerubahan $admin_permintaan_perubahan) {
        if(Auth::user()->role == '19'){
            $data  = DB::SELECT("SELECT admin_permintaan_perubahan.* FROM admin_permintaan_perubahan 
						LEFT JOIN admin_riwayat_investor_ori ON admin_riwayat_investor_ori.hist_investor_id = admin_permintaan_perubahan.hist_id 
						LEFT JOIN admin_riwayat_investor ON admin_riwayat_investor.hist_investor_id = admin_permintaan_perubahan.hist_id 
						WHERE admin_permintaan_perubahan.status = 1 
						AND (admin_riwayat_investor_ori.status = 'suspend' 
						OR admin_riwayat_investor.status = 'suspend') ORDER BY admin_permintaan_perubahan.request_id DESC");
        }else{
            $data = $admin_permintaan_perubahan::all();
        }
        
        return \DataTables::of($data)
        ->addColumn('edit_date', function($data) {
            $edit_date = $data->edit_date;
            if ($edit_date) {
                return date("d-m-Y", strtotime($data->edit_date));
            } else {
                return '';
            }
        })
        ->addColumn('approve_date', function($data) {
            $approve_date = $data->approve_date;
            if ($approve_date) {
                return date("d-m-Y", strtotime($data->approve_date));
            } else {
                return '';
            }
        })
        ->addColumn('badge_status', function($data) {
            $status = $data->status;
            $request_id = $data->request_id;
            $user_roles =  (Auth::user()->role == '4' || Auth::user()->role == '19') ? 'Baru' : 'Proses';
            $badge_status = '';
            switch ($status) {
                case 1: // in_progress for editor / CS , new for approval
                    $badge_status = '<span class="badge badge-info p-2">'.$user_roles.'</span>';
                    break;

                case 2: // done , both for editor and approval 
                    $badge_status = '<span class="badge badge-success p-2">Selesai</span>';
                    break;
                case 3: // reject , both for editor and approval	
                    $badge_status = '<span class="badge badge-danger p-2">Ditolak</span>';
                    break;
                default:
                    $badge_status = '<span class="badge badge-info p-2">'.$user_roles.'</span>';
                    break;
            }
            return $badge_status;
        })
        ->addColumn('btn_detail', function($data) {
            $status = $data->status;
            $request_id = $data->request_id;
            $btn_status = '';
            switch ($status) {
                case 1: // in_progress for editor / CS , new for approval
                    $btn_status = '<button data-id="'.$request_id.'" class="btn btn-primary rounded" onclick="getJsonDetailProfile('.$request_id.')">Detail</button>';
                    break;

                case 2: // done , both for editor and approval 
                    $btn_status = '<button data-id="'.$request_id.'" class="btn btn-primary rounded" onclick="getJsonDetailProfile('.$request_id.')">Detail</button>';
                    break;
                case 3: // reject , both for editor and approval	
                    $btn_status = '<button data-id="'.$request_id.'" class="btn btn-primary rounded" onclick="getJsonDetailProfile('.$request_id.')">Detail</button>';
                    break;
                default:
                    $btn_status = '<button data-id="'.$request_id.'" class="btn btn-primary rounded" onclick="getJsonDetailProfile('.$request_id.')">Detail</button>';
                    break;
            }
            return $btn_status;
        })
        ->rawColumns(['badge_status', 'edit_date', 'approve_date', 'btn_detail'])
        ->make(true);
    }

    public function listPermintaanPerubahanProfilePengguna(Request $request) {
        $master_provinsi = MasterProvinsi::distinct('kode_provinsi', 'nama_provinsi')->get(['kode_provinsi', 'nama_provinsi']);
        $master_agama = MasterAgama::all();
        $master_asset = MasterAsset::all();
        $master_badan_hukum = MasterBadanHukum::all();
        $master_bank = MasterBank::all();
        $master_bidang_pekerjaan = MasterBidangPekerjaan::all();
        $master_jenis_kelamin = MasterJenisKelamin::all();
        $master_jenis_pengguna = MasterJenisPengguna::all();
        $master_kawin = MasterKawin::all();
        $master_kepemilikan_rumah = MasterKepemilikanRumah::all();
        $master_negara = MasterNegara::all();
        $master_online = MasterOnline::all();
        $master_pekerjaan = MasterPekerjaan::all();
        $master_pendapatan = MasterPendapatan::all();
        $master_pendidikan = MasterPendidikan::all();
        $master_pengalaman_kerja = MasterPengalamanKerja::all();
        $master_hub_ahli_waris = DB::table('m_hub_ahli_waris')->select('id_hub_ahli_waris', 'jenis_hubungan')->get();
        $master_kode_operator = DB::table("m_kode_operator_negara")->get();
        $master_jabatan = DB::table('m_jabatan')->get();

        return view('pages.admin.riwayat_perubahan_profile', [
            'master_provinsi' => $master_provinsi,
            'master_agama' => $master_agama,
            'master_asset' => $master_asset,
            'master_badan_hukum' => $master_badan_hukum,
            'master_bank' => $master_bank,
            'master_bidang_pekerjaan' => $master_bidang_pekerjaan,
            'master_jenis_kelamin' => $master_jenis_kelamin,
            'master_jenis_pengguna' => $master_jenis_pengguna,
            'master_kawin' => $master_kawin,
            'master_kepemilikan_rumah' => $master_kepemilikan_rumah,
            'master_negara' => $master_negara,
            'master_online' => $master_online,
            'master_pekerjaan' => $master_pekerjaan,
            'master_pendapatan' => $master_pendapatan,
            'master_pendidikan' => $master_pendidikan,
            'master_pengalaman_kerja' => $master_pengalaman_kerja,
            'master_provinsi' => $master_provinsi,
            'master_hub_ahli_waris' => $master_hub_ahli_waris,
            'master_kode_operator' => $master_kode_operator,
            'master_jabatan' => $master_jabatan
        ]);
    }

    public function getJsonDetailProfile(
        Request $request,
        AdminPermintaanPerubahan $admin_permintaan_perubahan,
        // DetilInvestor $detil_investor,
        // InvestorPengurus $investor_pengurus,

        // Riwayat Ori
        AdminRiwayatInvestorOri $admin_riwayat_investor_ori,
        AdminRiwayatDetilInvestorIndividuOri $admin_riwayat_detil_investor_individu_ori,
        AdminRiwayatDetilInvestorBadanHukumOri $admin_riwayat_detil_investor_bdn_hukum_ori,
        AdminRiwayatAhliWarisInvestorOri $admin_riwayat_ahliwaris_investor_ori,
        AdminRiwayatNomorVaOri $admin_riwayat_nomor_va_ori,
        AdminRiwayatPengurusInvestorBadanHukumOri $admin_riwayat_pengurus_investor_ori,

        // Riwayat
        AdminRiwayatInvestor $admin_riwayat_investor,
        AdminRiwayatDetilInvestorIndividu $admin_riwayat_detil_investor_individu,
        AdminRiwayatDetilInvestorBadanHukum $admin_riwayat_detil_investor_bdn_hukum,
        AdminRiwayatAhliWarisInvestor $admin_riwayat_ahliwaris_investor,
        AdminRiwayatNomorVa $admin_riwayat_nomor_va,
        AdminRiwayatPengurusInvestorBadanHukum $admin_riwayat_pengurus_investor
    ) {
        $request_id                 = $request->request_id;
        $data_permintaan_perubahan  = $admin_permintaan_perubahan::where('request_id', $request_id)->first();
        $client_type                = $admin_permintaan_perubahan ? $data_permintaan_perubahan->client_type : 0; // 1 = investor, 2 = borrower
        $user_type                  = $admin_permintaan_perubahan ? $data_permintaan_perubahan->user_type : 0; // 1 = individu, 2 = badan hukum
        $client_id                  = $admin_permintaan_perubahan ? $data_permintaan_perubahan->client_id : 0; // investor_id atau brw_id
        $hist_inv_id                = $admin_permintaan_perubahan ? $data_permintaan_perubahan->hist_id : 0; // hist inv ID
        $response                   = [];

        switch ($client_type) {
            case '1': // Investor
                $data_investor = '';
                // $data_investor = $detil_investor::from('detil_investor as a')
                //     ->leftJoin('investor as b', 'b.id', '=', 'a.investor_id')
                //     ->where('a.investor_id', $client_id)
                //     ->first();

                // Detil Investor ORI

                if ($user_type == 2) { //Badan Hukum
                    $data_investor = $admin_riwayat_investor_ori::from('admin_riwayat_investor_ori as a')
                    ->leftJoin('admin_riwayat_detil_investor_badan_hukum_ori as c', 'c.hist_investor_id', '=', 'a.hist_investor_id')
                    ->where('a.hist_investor_id', $hist_inv_id)
                    ->first();
                } else { //Individu
                    $data_investor = $admin_riwayat_investor_ori::from('admin_riwayat_investor_ori as a')
                    ->leftJoin('admin_riwayat_detil_investor_individu_ori as b', 'b.hist_investor_id', '=', 'a.hist_investor_id')
                    ->where('a.hist_investor_id', $hist_inv_id)
                    ->first();
                }

                // Header Riwayat Investor
                $data_riwayat_investor = $admin_riwayat_investor::where('hist_investor_id', $hist_inv_id)->first();

                // Detail Riwayat Investor Individu
                $data_riwayat_investor_individu = $admin_riwayat_investor::from('admin_riwayat_investor as a')
                    ->leftJoin('admin_riwayat_detil_investor_individu as b', 'b.hist_investor_id', '=', 'a.hist_investor_id')
                    ->where('a.hist_investor_id', $hist_inv_id)
                    ->first();

                // Detail Riwayat Investor Badan Hukum
                $data_riwayat_detil_investor_bdn_hukum = $admin_riwayat_investor::from('admin_riwayat_investor as a')
                    ->leftJoin('admin_riwayat_detil_investor_badan_hukum as b', 'b.hist_investor_id', '=', 'a.hist_investor_id')
                    ->where('a.hist_investor_id', $hist_inv_id)
                    ->first();
                
                // Pengurus
                $data_pengurus_investor         = $user_type == 2 ? $admin_riwayat_pengurus_investor_ori::where('hist_investor_id', $hist_inv_id)->orderby('pengurus_id', 'ASC')->get() : '';
                $data_riwayat_pengurus_investor = $user_type == 2 ? $admin_riwayat_pengurus_investor::where('hist_investor_id', $hist_inv_id)->orderby('pengurus_id', 'ASC')->get() : '';
                $total_pengurus                 = $user_type == 2 ? $data_pengurus_investor->count() : '';
                
                // Ahli Waris
                $data_ahli_waris                = $admin_riwayat_ahliwaris_investor_ori::where("hist_investor_id", $hist_inv_id)->first();
                $data_riwayat_ahli_waris        = $admin_riwayat_ahliwaris_investor::where("hist_investor_id", $hist_inv_id)->first();

                // Nomor VA
                // $data_rekengin_investor         = DB::table('rekening_investor')->where('investor_id', $client_id)->get();
                // $data_detil_rekengin_investor   = DB::table('detil_rekening_investor')->where('investor_id', $client_id)->get();
                $data_nomor_va                  = $admin_riwayat_nomor_va_ori::where('hist_id', $hist_inv_id)->get();
                $data_riwayat_nomor_va          = $admin_riwayat_nomor_va::where('hist_id', $hist_inv_id)->get();

                $response = [
                    'status' => 'success',
                    'message' => 'data ditemukan',
                    'data' => [
                        'user_type'                         => $user_type,
                        'client_id'                         => $client_id,
                        'client_type'                       => $client_type,
                        'hist_investor_id'                  => $hist_inv_id,
                        'status_data'                       => $data_permintaan_perubahan->status,

                        'data_investor'                     => $data_investor,
                        'data_riwayat_investor'             => $data_riwayat_investor,
                        'data_riwayat_investor_individu'    => $data_riwayat_investor_individu,
                        'data_riwayat_investor_bdn_hukum'   => $data_riwayat_detil_investor_bdn_hukum,

                        'data_pengurus_investor'            => $data_pengurus_investor,
                        'data_riwayat_pengurus_investor'    => $data_riwayat_pengurus_investor,
                        'total_pengurus'                    => $total_pengurus,

                        'data_ahli_waris'                   => $data_ahli_waris,
                        'data_riwayat_ahli_waris'           => $data_riwayat_ahli_waris,

                        'data_nomor_va'                     => $data_nomor_va,
                        'data_riwayat_nomor_va'             => $data_riwayat_nomor_va
                    ]
                ];
                return response()->json($response);
                break;
            
            default:
                # code...
                break;
        }
    }

    public function savePermintaanPerubahanData(Request $request) {
        $approve_by     = Auth::user()->firstname . ' ' . Auth::user()->lastname;
        $status         = $request->status; // 2 diterima, 3 ditolak
        $response       = '';
        $user_type      = $request->user_type;
        $client_type    = $request->client_type;
        $total_pengurus = $request->total_pengurus;

        // set null is_valid_rekening ketika ada perubahan rekening
        if($status == '2'){
            $getRek = DB::SELECT("SELECT 
                    a.rekening AS 'rek_ori', b.rekening AS 'rek_ubah' 
                    FROM admin_riwayat_detil_investor_individu_ori a,
                        admin_riwayat_detil_investor_individu b
                    WHERE a.investor_id = $request->investor_id
                        AND a.hist_investor_id =  $request->hist_investor_id
                        AND a.hist_investor_id = b.hist_investor_id");
            if(count($getRek) > 0){
                if($getRek[0]->rek_ori != $getRek[0]->rek_ubah){
                    $detilinvestor = new DetilInvestor;
                    $detilinvestor->updateIsValidrekeningNull($request->investor_id);
                }
            }
        }

        if ($client_type == '1' && $user_type == '1') { //Investor Individu
            $save_permintaan_perubahan_data_inv_individu = DB::select(
                "CALL proc_update_inv_individu(
                    '$request->hist_investor_id', /* hist_id */ 
                    '$request->investor_id', /* investor_id */ 
                    '$approve_by', /* approve_by */ 
                    '$status', /* status */ 
                    'AdminController', /* file */ 
                    5321 /* line */
                )"
            );

            if (!empty($save_permintaan_perubahan_data_inv_individu[0]->sout) && $save_permintaan_perubahan_data_inv_individu[0]->sout == '1') {
                if ($status == '2') { // 2 diterima, 3 ditolak
                    $response = [
                        'status' => 'success',
                        'message' => 'Perubahan berhasil disimpan'
                    ];
                } else {
                    $response = [
                        'status' => 'success',
                        'message' => 'Perubahan data ditolak'
                    ];
                }
                
            } else {
                $response = [
                    'status' => 'error',
                    'message' => !empty($save_permintaan_perubahan_data_inv_individu[0]->sout) ? $save_permintaan_perubahan_data_inv_individu[0]->sout : $save_permintaan_perubahan_data_inv_individu[0]
                ];
            }
        } else if($client_type == '1' && $user_type == '2') { //Investor Badan Hukum
            $save_permintaan_perubahan_data_inv_bdn_hukum = DB::select(
                "CALL proc_update_inv_badan_hukum(
                    '$request->hist_investor_id', /* hist_id */ 
                    '$request->investor_id', /* investor_id */ 
                    '$approve_by', /* approve_by */ 
                    '$status', /* status */ 
                    'AdminController', /* file */ 
                    5378 /* line */
                )"
            );

            for ($i=1; $i <= $total_pengurus; $i++) { 
                $is_pengurus_satu = $i == 1 ? 1 : 0 ;
                $save_permintaan_perubahan_data_inv_pengurus = DB::select(
                    
                    "CALL proc_update_inv_badan_hukum_pengurus(
                        '$is_pengurus_satu', /* _header_pengurus tinyint,#0 = pengurus biasa , #1 = pengurus kepala	*/
                        '$request->hist_investor_id', /*_hist_id int */
                        '$request->investor_id', /*_investor_id int */
                        '$approve_by', /*_approve_by varchar(35) */
                        '$status', /* _status int */
                        'AdminController', /* _file varchar(100) */
                        5422 /* _line int */
                    )"
                );

                if (empty($save_permintaan_perubahan_data_inv_pengurus[0]->sout)) {
                    $response = [
                        'status' => 'error',
                        'message' => $save_permintaan_perubahan_data_inv_pengurus[0]
                    ];

                    return response()->json($response);

                }
            }


            if (!empty($save_permintaan_perubahan_data_inv_bdn_hukum[0]->sout) && $save_permintaan_perubahan_data_inv_bdn_hukum[0]->sout == '1') {
                if ($status == '2') { // 2 diterima, 3 ditolak
                    $response = [
                        'status' => 'success',
                        'message' => 'Perubahan berhasil disimpan'
                    ];
                } else {
                    $response = [
                        'status' => 'success',
                        'message' => 'Perubahan data ditolak'
                    ];
                }
            } else {
                $response = [
                    'status' => 'error',
                    'message' => !empty($save_permintaan_perubahan_data_inv_bdn_hukum[0]->sout) ? $save_permintaan_perubahan_data_inv_bdn_hukum[0]->sout : $save_permintaan_perubahan_data_inv_bdn_hukum[0]
                ];
            }
        }

        // approved by or suspended by tabel investor
        $getStatus = DB::SELECT("SELECT 
                    a.status as status_ubah, b.status AS status_ori
                    FROM admin_riwayat_investor a,
                        admin_riwayat_investor_ori b
                    WHERE a.hist_investor_id = b.hist_investor_id
                        AND a.hist_investor_id =  $request->hist_investor_id");
        if($getStatus[0]->status_ubah == 'active' && $getStatus[0]->status_ori == 'suspend'){
            $actived_by = $approve_by;
            $suspended_by = '/N';
        }elseif($getStatus[0]->status_ubah == 'suspend' && $getStatus[0]->status_ori == 'active'){
            $actived_by = '/N';
            $suspended_by = $approve_by;
        }

        DB::table('investor')->where('id',$request->investor_id)->update(array(
              'actived_by'=>$approve_by,
              'suspended_by'=>$approve_by,
            ));
        // end
        return response()->json($response);
    }       


    
    public function listUserDanaMaterialSubmit()
    {
        return view('pages.admin.list_dana_material_submit');
    }


    public function dataUserDanaMaterial(Request $request){

        $input = Input::all();

        $query= \DB::table('brw_user_material_submit');

        if(isset($input['search_filter'])){

            $filter_data = json_decode($input['search_filter']);

            if (!empty($filter_data->filter_start_date) && !empty($filter_data->filter_end_date)) {
                $query->whereBetween('created_at', [$filter_data->filter_start_date." 00:00:00", $filter_data->filter_end_date." 23:59:59"]);
            }     
        }


        $userDanaMaterial = $query->orderBy('created_at', 'desc')->get();
        $response = ['data' => $userDanaMaterial];
        return response()->json($response);
    }
}
