<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use throttle;
use Lang;
use Illuminate\Http\Request;
use Auth;
use App\Investor;
use App\LogSuspend;
use DB;

use Carbon\Carbon;
use App\DetilInvestor;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    public $maxAttempts = 2;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->middleware('throttle:2,1')->only('login');
    }
    public function username()
    {
        return 'username';
    }
    public function password()
    {
        return 'password';
    }
    public function showLoginForm()
    {
        return redirect()->to('/#loginModalAs')->with(['skinvestor' => 'skinniinni']);
    }

    public function login(Request $request)
    {
        // $this->validateLogin($request);

        if (is_numeric($request->username)) {
            \Auth::logoutOtherDevices(request('password'));
            $first = $request->username;
            $pass = $request->password;
            $token = $request->_token;
            if (substr($request->username, 0, 2) == "62") {

                $no_hp = substr($request->username, 2);
                $data = DB::table('investor')->join('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')->select(['username'])->where('detil_investor.phone_investor', 'like', '%' . $no_hp . '%')->first();
                if ($data) {
                    $request->replace(['_token' => $token, 'username' => $data->username, 'password' => $pass, 'first' => $first]);
                } else {
                    $dd = DB::select("CALL get_desc_master('m_notif',129,'LoginController.php',78,@result)");
                    return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
                }
            }

            if (substr($request->username, 0, 1) == "0") {
                $no_hp = substr($request->username, 1);
                $data = DB::table('investor')->join('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')->select(['username'])->where('detil_investor.phone_investor', 'like', '%' . $no_hp . '%')->first();
                if ($data) {
                    $request->replace(['_token' => $token, 'username' => $data->username, 'password' => $pass, 'first' => $first]);
                } else {
                    $dd = DB::select("CALL get_desc_master('m_notif',129,'LoginController.php',93,@result)");
                    return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
                }
            }
        }

        if (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            $first = $request->username;
            $pass = $request->password;
            $token = $request->_token;

            $data = DB::table('investor')->where('email', 'like', '%' . $request->username . '%')->first();
            if ($data) {
                $request->replace(['_token' => $token, 'username' => $data->username, 'password' => $pass, 'first' => $first]);
            } else {
                $dd = DB::select("CALL get_desc_master('m_notif',131,'LoginController.php',115,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            }
        }

        $data = Investor::where('username', $request->username)->first();
        $data_suspend = $data['status'];
        if ($data_suspend == 'suspend') {
            $keterangan_suspend = $data['keterangan'] ? ' karena ' .$data['keterangan'] : '';
            return redirect()->to('/#modal_login_investor')->with('status_max_login', 'Akun anda di Suspend' .$keterangan_suspend. '. Silahkan hubungi admin untuk mengaktifkan kembali.');
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            \Auth::logoutOtherDevices(request('password'));
            $password_updated_at = $data['password_updated_at'];
            if ($password_updated_at != null) {

                $password_expiry_days = $data['password_expiry_days'];
                $password_expiry_at = Carbon::parse($password_updated_at)->addDays($password_expiry_days);
                if ($password_expiry_at->lessThan(Carbon::now())) {
                    // syslog(0,"must change password");
                    $cekData = DetilInvestor::where('investor_id', $data['id'])->first();

                    $update_status_expired = Investor::where('username',$request->username)->update(['status' => 'expired']);
                    // return redirect('user/ubahPassword')->with('passwordExpired',$cekData->phone_investor);
                } else {
                    //	syslog(0,"normal login");
                }
            } else {
            }
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    protected function sendFailedLoginResponse(Request $request)
    {
        $user = $request->only($this->username(), 'remember');
        $password = $request->only($this->password());

        $data = Investor::where('username', $user)->first();
        $data_user = $data['email_verif'];
        $data_username = $data['username'];
        $data_password = $data['password'];

        // var_dump($data_user);die;
        if (is_numeric($request->first)) {
            if ($data_username == null) {
                $dd = DB::select("CALL get_desc_master('m_notif',129,'LoginController.php',181,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            } elseif ($data_password != $password) {
                $dd = DB::select("CALL get_desc_master('m_notif',130,'LoginController.php',187,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            } elseif ($data_user != null) {
                $dd = DB::select("CALL get_desc_master('m_notif',128,'LoginController.php',195,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            }
            return $redirect;
        }
        if (filter_var($request->first, FILTER_VALIDATE_EMAIL)) {
            if ($data_username == null) {
                $dd = DB::select("CALL get_desc_master('m_notif',131,'LoginController.php',205,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            } elseif ($data_password != $password) {
                $dd = DB::select("CALL get_desc_master('m_notif',130,'LoginController.php',213,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            } elseif ($data_user != null) {
                $dd = DB::select("CALL get_desc_master('m_notif',128,'LoginController.php',221,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            }
            return $redirect;
        } else {
            if ($data_username == null) {
                $dd = DB::select("CALL get_desc_master('m_notif',132,'LoginController.php',232,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            } elseif ($data_password != $password) {
                $dd = DB::select("CALL get_desc_master('m_notif',130,'LoginController.php',239,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            } elseif ($data_user != null) {
                $dd = DB::select("CALL get_desc_master('m_notif',128,'LoginController.php',246,@result)");
                return redirect()->to('/#modal_login_investor')->with('status_kosong', $dd[0]->{'res'});
            }
        }
    }

    public function hasTooManyLoginAttempts(Request $request)
    {
        // dd($request);
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts()
        );
    }

    public function sendLockoutResponse(Request $request)
    {
        $cek_username = Investor::where('username', $request->username)->first();

        if (isset($cek_username)) {
            $status = 'suspend';
            $keterangan = 'Salah masukkan password lebih dari 3x';
            $suspended_by = 'Sistem';
            $updateDetails = [
                'status' => $status,
                'keterangan' =>  $keterangan,
                'suspended_by' =>  $suspended_by
            ];
            //Investor::where('username',$request->username)->update(['status' => $status]);
            Investor::where('username', $request->username)->update($updateDetails);
            $Log = new LogSuspend;
            $Log->keterangan = 'Salah masukkan password lebih dari 3x';
            $Log->suspended_by = 'Sistem';
            $Log->save();
            return redirect()->to('/#modal_login_investor')->with('status_max_login', 'Akun Anda sudah kami suspend. Silahkan hubungi admin untuk mengaktifkan kembali.');
        }
    }
}
