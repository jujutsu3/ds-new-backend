<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BniEnc;
use App\LogdbApp;
use App\Investor;
use App\MutasiInvestor;
use App\Events\MutasiInvestorEvent;
use App\PenarikanDana;
use App\RekeningInvestor;
use App\DetilInvestor;
use App\MasterBank;
use App\PendanaanAktif;
use Storage;
use PDF;
use Terbilang;
use App\LogSertifikat;
use Response;
//use App\Jobs\DepositEmail;
use Illuminate\Support\Facades\Auth;
use App\Http\Middleware\UserCheck;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\Http\Middleware\StatusProyek;
use phpDocumentor\Reflection\Types\String_;
use App\AuditTrail;
use App\TmpSelectedProyek;
use App\LoginBorrower;
use App\Proyek;
use App\LogAkadDigiSignInvestor;
use App\BorrowerPembayaran;
use App\BorrowerRekening;
use App\FDCPassword;
use App\BorrowerPendanaan;
use App\RDLAccountNumber;
use App\LogGenerateVaRdl;
use App\Http\Controllers\RDLController;
use App\Http\Controllers\PrivyController;
use DateTime;
use ZipArchive;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use DB;
use Mail;
use App\Mail\DepositEmail;
use App\Events\GenerateVABankEvent;
use Illuminate\Support\Facades\Hash;


class RekeningController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except(['bankResponseTest', 'bankResponse', 'bankResponseKonven', 'generateVABNI_Investor_test', 'generateVABNI_Investor', 'generateVABNI_Borrower', 'checkStatusUser', 'checkStatusUserInvest', 'generatePasswordZIP', 'bankResponse022_inquiry', 'bankResponse022_payment', 'bankResponse451_payment']);
        $this->middleware(UserCheck::class)->only(['tambahDana', 'mutasiInvestor', 'generateVA', 'penarikanDana', 'showPenarikanDana', 'get_list_mutasi_history', 'get_params_mutasi_history', 'verificationCode', 'sendVerifikasi', 'cekSertifikat', 'convertCSV_AFPI', 'generateVAtest']);
        $this->middleware(StatusProyek::class);
    }

    //Generate VA for user MASTER
    // public function generateVA(){
    // $date = \Carbon\Carbon::now()->addYear(4);
    // $user = Auth::user();
    // $data = [
    // 'type' => 'createbilling',
    // 'client_id' => config('app.bni_id'),
    // 'trx_id' => '999',
    // 'trx_amount' => '0',
    // 'customer_name' => $user->username,
    // 'customer_email' => $user->email,
    // 'virtual_account' => '8'.config('app.bni_id').'000000000999',
    // 'datetime_expired' => $date->format('Y-m-d').'T'.$date->format('H:i:sP'),
    // 'billing_type' => 'o',
    // ];

    // $encrypted = BniEnc::encrypt($data,config('app.bni_id'),config('app.bni_key'));

    // $client = new Client(); //GuzzleHttp\Client
    // $result = $client->post(config('app.bni_url'), [
    // 'json' => [
    // 'client_id' => config('app.bni_id'),
    // 'data' => $encrypted,
    // ]
    // ]);

    // $result = json_decode($result->getBody()->getContents());
    // if($result->status !== '000'){
    // return $result->message;
    // }
    // else{
    // $decrypted = BniEnc::decrypt($result->data,config('app.bni_id'),config('app.bni_key'));
    // $dana = RekeningInvestor::create([
    // 'investor_id' => $user->id,
    // 'jumlah_dana' => 0,
    // 'va_number' => $decrypted['virtual_account'],
    // 'unallocated' => 0,
    // ]);
    // return view('pages.user.add_funds')->with('message','VA Generate Success!');
    // }
    // }

    public function generateVA()
    {
        $date = \Carbon\Carbon::now()->addYear(4);
        $user = Auth::user();
        $data_user = Investor::where('username', $user->username)->first();


        event(new GenerateVABankEvent($data_user));

        if (strlen(config('app.bni_url')) != 0) {
            $data = [
                'type' => 'createbilling',
                'client_id' => config('app.bni_id'),
                'trx_id' => $data_user->id,
                'trx_amount' => '0',
                'customer_name' => $data_user->detilInvestor->nama_investor,
                'customer_email' => $user->email,
                'virtual_account' => '8' . config('app.bni_id') . $data_user->detilInvestor->getVa(),
                'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
                'billing_type' => 'o',
            ];
            $encrypted = BniEnc::encrypt($data, config('app.bni_id'), config('app.bni_key'));

            $client = new Client(); //GuzzleHttp\Client
            $result = $client->post(config('app.bni_url'), [
                'json' => [
                    'client_id' => config('app.bni_id'),
                    'data' => $encrypted,
                ]
            ]);

            $result = json_decode($result->getBody()->getContents());
            if ($result->status !== '000') {
                // insert rekening investor
                $dana = RekeningInvestor::create([
                    'investor_id' => $user->id,
                    'jumlah_dana' => 0,
                    // 'va_number' => "",
                    'unallocated' => 0,
                    'kode_bank' => '009',
                ]);

                // insert log db app
                $insertLog = DB::table("log_db_app")->insert(array(
                    "file_name" => __FILE__,
                    "line" => __LINE__,
                    "description" => "Gagal Generate VA " . $user->id . " " . json_encode($result) . "",
                    "created_at" => date("Y-m-d H:i:s")
                ));
                // return $result->message;
                return 'VA Generate Success!';
            } else {
                $decrypted = BniEnc::decrypt($result->data, config('app.bni_id'), config('app.bni_key'));
                $dana = RekeningInvestor::create([
                    'investor_id' => $user->id,
                    'jumlah_dana' => 0,
                    'va_number' => $decrypted['virtual_account'],
                    'unallocated' => 0,
                    'kode_bank' => '009',
                ]);

                // insert log db app
                $insertLog = DB::table("log_db_app")->insert(array(
                    "file_name" => __FILE__,
                    "line" => __LINE__,
                    "description" => "Sukses Generate VA " . $user->id . " " . json_encode($result) . "",
                    "created_at" => date("Y-m-d H:i:s")
                ));
                return 'VA Generate Success!';
            }
        } else {
            #-------------------------------------------------------------------------------- update nonaktif 06sep2021 jika tanpa BNI
            $cekRekening = RekeningInvestor::where('investor_id', $user->id)->first();
            if ($cekRekening == NULL) {
                RekeningInvestor::create([
                    'investor_id' => $user->id,
                    'jumlah_dana' => 0,
                    'unallocated' => 0,
                    'kode_bank' => '427',
                ]);
                return 'VA Generate Success!';
            } else {
                DB::table("log_db_app")->insert(array(
                    "file_name" => __FILE__,
                    "line" => __LINE__,
                    "description" => "Gagal Insert Lender Baru pada Rekeninginvestor '" . $user->id . "' sudah terdaftar sebelumnya",
                    "created_at" => date("Y-m-d H:i:s")
                ));
                return 'VA Generate Gagal!';
            }
            #-------------------------------------------------------------------------------- update nonaktif 06sep2021
        }
    }

    // Function to show help page for adding cash
    public function tambahDana()
    {
        $rekening = RekeningInvestor::where('investor_id', Auth::user()->id)->first();
        return view('pages.user.add_funds')->with('rekening', $rekening);
    }

    // REQUEST OJK 
    public function bankResponse(Request $request)
    {
        $data = $request->input('data');
        $decrypted = BniEnc::decrypt($data, config('app.bni_id'), config('app.bni_key'));

        $va_investor = $decrypted['virtual_account'];
        $payment_ntb = $decrypted['payment_ntb'];
        $trxNtb = $decrypted['trx_id'] . $decrypted['payment_ntb'];

        if ($request->input('client_id') != config('app.bni_id')) {
            $insertLogCallback = DB::table("log_db_app")->insert(array("file_name" => 'RekeningController', "line" => 150, "description" => 'Credential tidak sesuai ' . $va_investor . '', "created_at" => date('Y-m-d H:i:s')));
            return response()->json([
                'status' => '999',
                'message' => 'Access Denied',
            ]);
        }

        //$decrypted = BniEnc::decrypt($data,config('app.bni_id'),config('app.bni_key'));
        //$va_investor = $decrypted['virtual_account'];
        //$payment_ntb = $decrypted['payment_ntb'];

        if (strstr($decrypted['trx_id'], '/')) {

            // OJK VERSION
            $explodeTRX_id = explode('/', $decrypted['trx_id']);
            $user_id        = $explodeTRX_id[0];
            $proyek_id      = $explodeTRX_id[1];
            $trx_date_va    = $explodeTRX_id[2];


            $rekening = RekeningInvestor::where('investor_id', $user_id)->first();
            //$rekening = RekeningInvestor::where('i', $va_investor)->first();
            $payment_ntb_exist = $rekening['payment_ntb'];

            // get the incoming amount
            $amount = (int)$decrypted['payment_amount'];

            // update the rekening record
            // $rekening = $user->rekeningInvestor;
            $getTemporer = TmpSelectedProyek::where('no_va', $va_investor)
                ->where('investor_id', $user_id)
                ->where('proyek_id', $proyek_id)
                ->where('amount', $amount)
                ->where('trx_date_va', $trx_date_va)->first();
            //syslog(0, 'log getTemporerPendanaan'.$getTemporer);

            $insert_pendanaan = new \App\PendanaanAktif();
            $insert_pendanaan->investor_id  = $getTemporer->investor_id;
            $insert_pendanaan->proyek_id    = $getTemporer->proyek_id;
            $insert_pendanaan->total_dana   = $getTemporer->total_price;
            $insert_pendanaan->nominal_awal = $getTemporer->total_price;
            $insert_pendanaan->tanggal_invest = date('Y-m-d');
            $insert_pendanaan->save();

            // hapus row table temporer
            TmpSelectedProyek::where('investor_id', $user_id)
                ->where('proyek_id', $proyek_id)
                ->where('no_va', $va_investor)->delete();
            //syslog(0, 'Proyek ID / '.$getTemporer->proyek_id.' - Investor ID /'.$getTemporer->investor_id);

            $run = DB::SELECT("CALL proc_update_rek_investor($user_id,$va_investor,$amount,$trxNtb,'427','" . __FILE__ . "','" . __LINE__ . "')");
            if ($run[0]->sout != '1') {
                return response()->json([
                    'status' => '888',
                    'message' => 'Payment NTB double',
                ]);
            }
        } else {

            $rekening = RekeningInvestor::where('va_number', $va_investor)->first();
            $amount = (int)$decrypted['payment_amount'];
            // $payment_ntb_exist = $rekening['payment_ntb'];
            $user_id = $rekening['investor_id'];

            $run = DB::SELECT("CALL proc_update_rek_investor($user_id,$va_investor,$amount,$trxNtb,'427','" . __FILE__ . "','" . __LINE__ . "')");
            if ($run[0]->sout != '1') {
                return response()->json([
                    'status' => '888',
                    'message' => 'Payment NTB double',
                ]);
            }
        }
        // call event that will log the cash flow
        event(new MutasiInvestorEvent($rekening->investor->id, 'CREDIT', $amount, 'Transfer Rekening'));

        return response()->json([
            'status' => '000'
        ]);
    }

    // Show mutasi rekening milik investor
    public function mutasiInvestor()
    {
        $user = Auth::user();
        $rekening = RekeningInvestor::where('investor_id', $user->id)->first();
        $totalDana = (!empty($rekening->total_dana) ? $rekening->total_dana : 0);
        return view('pages.user.mutation_history', compact('totalDana'));
    }

    public function get_list_mutasi_history()
    {
        $user = Auth::user();
        $date = Carbon::now()->subDays(30);
        // $date_now = Carbon::now();
        // $data_mutasi_awal = MutasiInvestor::where('investor_id', $user->id)
        //                             ->where('created_at', '<', $date)
        //                             ->where('perihal', 'not like', '%pengembalian dana pokok%')
        //                             ->where('perihal', 'not like', '%pengembalian pokok%')
        //                             ->where('perihal', 'not like', '%sisa imbal hasil%')
        //                             ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
        //                             ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
        //                             //  ->orderBy('created_at', 'ASC')
        //                             ->whereIn('tipe', ['CREDIT', 'DEBIT'])
        //                             ->get();

        // $data_mutasi = "";
        $data_mutasi = MutasiInvestor::where('investor_id', $user->id)
            //  ->where('created_at','>=', Carbon::now()->subDays(30))
            ->where('perihal', 'not like', '%pengembalian dana pokok%')
            ->where('perihal', 'not like', '%pengembalian pokok%')
            ->where('perihal', 'not like', '%sisa imbal hasil%')
            ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
            ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
            ->where('perihal', 'not like', '%Akumulasi Keseluruhan Imbal Hasil%')
            ->where('perihal', 'not like', '%DANA POKOK : %')
            ->whereIn('tipe', ['CREDIT', 'DEBIT'])
            ->orderBy('created_at', 'ASC')
            //  ->limit(10)
            ->get();

        // dd($data_mutasi);die;

        $valuefirst = 0;
        $valuesecond = 0;
        $total = 0;
        $nul = 0;
        $i = 0;
        // if(!empty($data_mutasi_awal))
        // {
        //     foreach($data_mutasi_awal as $dt_a)
        //     {
        //         if($dt_a->tipe == "DEBIT")
        //           {
        //             $total_first = $dt_a->nominal - $valuefirst;
        //             $valuesecond =  $total -= $total_first;
        //           }
        //           elseif($dt_a->tipe == "CREDIT")
        //           {
        //             $total_first = $dt_a->nominal + $valuefirst;
        //             $valuesecond =  $total += $total_first;
        //           }
        //     }
        // }
        if (!empty($data_mutasi)) {
            $data = array();
            foreach ($data_mutasi as $dt) {

                $i++;
                $column['Keterangan'] = (string) $dt->perihal . ' ' . $dt->kode_bank;
                $column['Tanggal'] = (string) Carbon::parse($dt->created_at)->format('d-m-Y');
                if ($dt->tipe == "DEBIT") {
                    $total_first = $dt->nominal - $valuefirst;
                    $valuesecond =  $total -= $total_first;

                    $column['Debit'] = (string) number_format($dt->nominal);
                    $column['Kredit'] = (string) $nul;
                } elseif ($dt->tipe == "CREDIT") {
                    $total_first = $dt->nominal + $valuefirst;
                    $valuesecond =  $total += $total_first;

                    $column['Debit'] = (string) $nul;
                    $column['Kredit'] = (string) number_format($dt->nominal);
                }
                $column['Saldo'] = (string) number_format($valuesecond);

                $data[] = $column;
            }
        }

        $parsingJSON = array("data" => $data);
        echo json_encode($parsingJSON);
    }

    public function get_params_mutasi_history($startDate, $endDate)
    {
        $user = Auth::user();
        // $date = Carbon::now()->subDays(30);
        $date_now = Carbon::now();
        $data_mutasi_awal = MutasiInvestor::where('investor_id', $user->id)
            ->where('created_at', '<', $startDate)
            ->where('perihal', 'not like', '%pengembalian dana pokok%')
            ->where('perihal', 'not like', '%pengembalian pokok%')
            ->where('perihal', 'not like', '%sisa imbal hasil%')
            ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
            ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
            ->where('perihal', 'not like', '%DANA POKOK : %')
            ->orderBy('created_at', 'ASC')
            ->whereIn('tipe', ['CREDIT', 'DEBIT'])
            ->get();
        $data_mutasi = MutasiInvestor::where('investor_id', $user->id)
            ->whereBetween('created_at', [$startDate . " 00:00:00", $endDate . " 23:59:59"])
            ->where('perihal', 'not like', '%pengembalian dana pokok%')
            ->where('perihal', 'not like', '%pengembalian pokok%')
            ->where('perihal', 'not like', '%sisa imbal hasil%')
            ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
            ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
            ->where('perihal', 'not like', '%DANA POKOK : %')
            ->orderBy('created_at', 'ASC')
            ->whereIn('tipe', ['CREDIT', 'DEBIT'])
            ->get();
        $data_mutasi_akhir = MutasiInvestor::where('investor_id', $user->id)
            ->where('perihal', 'not like', '%pengembalian dana pokok%')
            ->where('perihal', 'not like', '%pengembalian pokok%')
            ->where('perihal', 'not like', '%sisa imbal hasil%')
            ->where('perihal', 'not like', '%pengembalian sisa imbal hasil%')
            ->where('perihal', 'not like', '%transfer sisa imbal hasil%')
            ->where('perihal', 'not like', '%DANA POKOK : %')
            ->orderBy('created_at', 'ASC')
            ->whereIn('tipe', ['CREDIT', 'DEBIT'])
            ->get();
        // dd($data_mutasi);die;

        $valuefirst = 0;
        $valuesecond = 0;
        $valueLast = 0;
        $total = 0;
        $nul = 0;
        $i = 0;
        if (!empty($data_mutasi_awal)) {
            foreach ($data_mutasi_awal as $dt_a) {
                if ($dt_a->tipe == "DEBIT") {
                    $total_first = $dt_a->nominal - $valuefirst;
                    $valuesecond =  $total -= $total_first;
                } elseif ($dt_a->tipe == "CREDIT") {
                    $total_first = $dt_a->nominal + $valuefirst;
                    $valuesecond =  $total += $total_first;
                }
            }
        }
        if (!empty($data_mutasi)) {
            $data = array();
            foreach ($data_mutasi as $dt) {

                $i++;
                $column['Keterangan'] = (string) $dt->perihal . ' ' . $dt->kode_bank;
                $column['Tanggal'] = (string) Carbon::parse($dt->created_at)->format('d-m-Y');
                if ($dt->tipe == "DEBIT") {
                    $total_first = $dt->nominal - $valuefirst;
                    $valuesecond =  $total -= $total_first;

                    $column['Debit'] = (string) number_format($dt->nominal);
                    $column['Kredit'] = (string) $nul;
                } elseif ($dt->tipe == "CREDIT") {
                    $total_first = $dt->nominal + $valuefirst;
                    $valuesecond =  $total += $total_first;

                    $column['Debit'] = (string) $nul;
                    $column['Kredit'] = (string) number_format($dt->nominal);
                }
                $column['Saldo'] = (string) number_format($valuesecond);

                $data[] = $column;
            }
        }
        $parsingJSON = array("data" => $data);

        // dd($parsingJSON);die;

        echo json_encode($parsingJSON);
    }

    public function cetakulangsertifikat($id)
    {

        $data_rek = RekeningInvestor::where('investor_id', $id)->first();
        $data_user = DetilInvestor::where('investor_id', $id)->first();
        $reprint = LogSertifikat::where('investor_id', $id)->orderBy('id', 'desc')->limit(1)->first();
        $id_s = $reprint['id'];
        $all_random = $reprint['seri_sertifikat'];
        $total_s = $reprint['total_dana'];
        $dana = number_format($total_s, 0, ",", ".");

        // mengambil nama investor
        $nama_investor = $data_user['nama_investor'];
        $rek_investor = $data_user['rekening'];
        $bank_investor = $data_user['bank_investor'];
        $alamat_investor = $data_user['alamat_investor'];

        // mengambil dana

        // mengambil VA 
        $va_bni = !empty($data_rek['va_number']) ? $data_rek['va_number'] . ' (BNI)' : '';

        $getvaBSI  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $id . " AND kode_bank = '451' LIMIT 1");
        $getvaCIMB  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $id . " AND kode_bank = '022' LIMIT 1");

        $va_cimbs = !empty($getvaCIMB[0]->va_number) ? $getvaCIMB[0]->va_number . ' (CIMBS)' : '';
        $va_bsi =  !empty($getvaBSI[0]->va_number) ? '900' . $getvaBSI[0]->va_number . ' (BSI)' : '';

        // mengambil nama bank 
        $master_bank = MasterBank::where('kode_bank', $bank_investor)->first();
        $bankinvestor = $master_bank['nama_bank'];


        // merubah dana menjadi terbilang Words   
        $terbilang = Terbilang::make($total_s, ' rupiah');

        // menjadi huruf kapital depannya
        $hasil_bilangan = ucwords($terbilang);

        $title = 'Sertifikat - ' . $nama_investor;

        $pdf = PDF::loadView(
            'pages.user.e_sertifikat_user',
            [
                // 'iduser'=>$iduser,
                'title' => $title,
                'dana' => $dana,
                'all_random' => $all_random,
                'hasil_bilangan' => $hasil_bilangan,
                // 'nomer_va' => $nomer_va,
                'va_bni' => $va_bni,
                'va_cimbs' => $va_cimbs,
                'va_bsi' => $va_bsi,
                'nama_investor' => $nama_investor,
                'rek_investor' => $rek_investor,
                'bankinvestor' => $bankinvestor,
                'alamat_investor' => $alamat_investor,


            ]
        );



        $pdf->setPaper('A4', 'landscape');
        // $pdf->setEncryption('copy');
        return $pdf->download('Sertifikat - ' . $nama_investor . '.pdf');
    }

    public function printsertifikat($id, $no_sertifikat)
    {

        $data_rek = RekeningInvestor::where('investor_id', $id)
            ->first(['total_dana', 'va_number', 'updated_at']);

        $data_user = DetilInvestor::where('investor_id', $id)->first();

        setlocale(LC_TIME, 'id_ID.utf8');
        $vardate_change = new DateTime(substr($no_sertifikat, -10, 2) . '-' . substr($no_sertifikat, -8, 2) . '-' . substr($no_sertifikat, -6, 2));
        $date_change = strftime('%d %B %Y', $vardate_change->getTimestamp());
        // dd([$no_sertifikat,$date_change,date("d F Y", strtotime($date_change))]);

        // mengambil nama investor
        $nama_investor = ($data_user['tipe_pengguna'] == 2) ? $data_user['nama_perusahaan'] : $data_user['nama_investor'];
        $rek_investor = $data_user['rekening'];
        $bank_investor = $data_user['bank_investor'];
        $alamat_investor = ($data_user['tipe_pengguna'] == 2) ? $data_user['alamat_perusahaan'] : $data_user['alamat_investor'];

        // mengambil dana
        $total = $data_rek->total_dana;

        // mengambil VA 
        $va_bni = !empty($data_rek->va_number) ? $data_rek->va_number . ' (BNI)' : '';

        $getvaBSI  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $id . " AND kode_bank = '451' LIMIT 1");
        $getvaCIMB  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $id . " AND kode_bank = '022' LIMIT 1");

        $va_cimbs = !empty($getvaCIMB[0]->va_number) ? $getvaCIMB[0]->va_number . ' (CIMBS)' : '';
        $va_bsi =  !empty($getvaBSI[0]->va_number) ? '900' . $getvaBSI[0]->va_number . ' (BSI)' : '';

        // mengambil nama bank 
        $master_bank = MasterBank::where('kode_bank', $bank_investor)->first();
        $bankinvestor = $master_bank['nama_bank'];

        // merubah dana menjadi terbilang Words   
        $terbilang = Terbilang::make($total, ' rupiah');

        // menjadi huruf kapital depannya
        $hasil_bilangan = ucwords($terbilang);

        $title = 'Sertifikat - ' . $nama_investor;
        $dana = number_format($total, 0, ",", ".");

        $check_log = LogSertifikat::where('total_dana', $total)->where('investor_id', $id)->limit(1)->first();
        $total_cek = $check_log['total_dana'];

        $data_log = new LogSertifikat;
        $data_log->investor_id = $id;
        $data_log->seri_sertifikat = $no_sertifikat;
        $data_log->total_dana = $total;

        $data_log->save();
        //}
        $pdf = PDF::loadView(
            'pages.user.e_sertifikat_user',
            [
                'title' => $title,
                'dana' => $dana,
                'all_random' => $no_sertifikat, // no sertifikat
                'hasil_bilangan' => $hasil_bilangan,
                // 'nomer_va' => $nomer_va,
                'va_bni' => $va_bni,
                'va_cimbs' => $va_cimbs,
                'va_bsi' => $va_bsi,
                'nama_investor' => $nama_investor,
                'rek_investor' => $rek_investor,
                'bankinvestor' => $bankinvestor,
                'alamat_investor' => $alamat_investor,
                'padatanggal' => $date_change
            ]
        );

        $pdf->setPaper('A4', 'landscape');
        // $pdf->setEncryption('copy');
        $path = storage_path('app/public/sertifikat');
        $fileName =  $data_log['title'] . 'Sertifikat - ' . $id . '.pdf';
        $pdf->save($path . '/' . $fileName);

        $data_log->save();

        if ($pdf) {
            return ('success');
        } else {
            return ('failed');
        }

        // return $pdf->download('Sertifikat - '.$nama_investor.'.pdf');
    }

    public function cekSertifikat($id)
    {
        $cId = date('Y') . Auth::user()->id . date('md');
        if ($id != $cId) {
            abort(404);
        }

        $id = Auth::user()->id;
        $getNo_sertifikat = DB::SELECT("SELECT func_generate_serial_number(" . $id . ",'func_generate_serial_number',@res) AS no_seri");
        $no_sertifikat = $getNo_sertifikat[0]->no_seri;
        $log_sertifikat = LogSertifikat::where('seri_sertifikat', $no_sertifikat)->orderby('id', 'desc')->first();
        // dd($log_sertifikat);

        if ($log_sertifikat == null) {
            // dd(1);
            $cetak_sertifikat = $this->printsertifikat($id, $no_sertifikat);
            $filename = 'Sertifikat - ' . $id . '.pdf';
            $path = storage_path("app/public/sertifikat/Sertifikat - " . $id . '.pdf');

            return Response::make(file_get_contents($path), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $filename . '"'
            ]);
        } else {
            // dd(2);
            $filename = 'Sertifikat - ' . $id . '.pdf';
            $path = storage_path("app/public/sertifikat/Sertifikat - " . $id . '.pdf');

            return Response::make(file_get_contents($path), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . $filename . '"'
            ]);
        }
    }

    // Show Penarikan dana form
    public function showPenarikanDana()
    {
        // $rekening = Auth::user()->rekeningInvestor;
        $pendanaan = Auth::user()->pendanaanAktif;
        $unallocated = Auth::user()->rekeningInvestor;
        $nama = Auth::user()->detilInvestor;
        $master_bank = MasterBank::where('kode_bank', $nama->bank_investor)->first(['nama_bank']);
        $getAlasan = DB::table('m_alasan_penarikan')
            ->orderBy('id', 'asc')
            ->get();

        return view('pages.user.withdraw_request')->with('pendanaan', $pendanaan)->with('unallocated', $unallocated)->with('nama', $nama)->with('master_bank', $master_bank)->with('alasan', $getAlasan);
    }

    // Process the cash checkout request
    public function penarikanDana(Request $request)
    {
        // $rekening = Auth::user()->rekeningInvestor;
        // $requestAmount = $request->nominal;

        // use laravel collection method SUM()
        // $sumAvailable = $rekening->unallocated;

        // if($requestAmount > $sumAvailable || $requestAmount <= 0){
        //     // Throw error, cant  request more than available sum
        //     return redirect()->back()->with('error','Penarikan dana anda lebih dari dana tersedia, silahkan mengambil uang pada pendanaan anda terlebih dahulu');
        // }
        $cekDanaTersedia = RekeningInvestor::where('investor_id', Auth::user()->id)->first();
        $jumlahPenarikan = PenarikanDana::where('investor_id', Auth::user()->id)->where('accepted', 0)->sum('jumlah');
        // dd($jumlahPenarikan);die;
        $totalDana = $request->nominal + $jumlahPenarikan;
        $note_alasan_penarikan = ($request->alasan_penarikan == 9) ? $request->note_alasan_penarikan : NULL;
        if ($totalDana <= $cekDanaTersedia->unallocated) {
            // Create new record penarikan dana
            PenarikanDana::create([
                'investor_id' => Auth::user()->id,
                'jumlah' => $request->nominal,
                'no_rekening' => $request->rekening,
                'bank' => $request->bank,
                'accepted' => 0,
                'perihal' => 'Pengajuan Penarikan Dana',
                'alasan_penarikan' => $request->alasan_penarikan,
                'note_alasan_penarikan' => $note_alasan_penarikan
            ]);

            // event(new MutasiInvestorEvent(Auth::user()->id,'request DEBIT',-$request->nominal,'Penarikan dana sedang diproses'));
            $audit = new AuditTrail;
            $username = Auth::guard()->user()->username;
            $audit->fullname = $username;
            $audit->description = "Permohonan penarikan dana";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return redirect('user/dashboard')->with('success', 'Penarikan dana anda akan kami proses. Terima kasih');
        } else {
            $audit = new AuditTrail;
            $username = Auth::guard()->user()->username;
            $audit->fullname = $username;
            $audit->description = "Permohonan penarikan dana1";
            $audit->ip_address =  \Request::ip();
            $audit->save();

            return redirect('user/dashboard')->with('success', 'Penarikan dana anda akan kami proses. Terima kasih');
        }
    }

    public function msgVerification($data)
    {

        $rekening = RekeningInvestor::join('detil_investor', 'detil_investor.investor_id', '=', 'rekening_investor.investor_id')
            ->select('detil_investor.nama_investor', 'detil_investor.phone_investor', 'rekening_investor.total_dana')
            ->where('rekening_investor.investor_id', $data['investor_id'])->first();
        $to =  $rekening->phone_investor;
        $amount = "Rp." . number_format($data['amount'], 0, ',', '.');
        $total_dana = "Rp." . number_format($rekening->total_dana, 0, ',', '.');
        // $to = '081318988499';
        $text =  'Top up dana sebesar ' . $amount . ' BERHASIL ditambahkan ke Akun ' . strtoupper($rekening->nama_investor) . '. Total Aset anda saat ini ' . $total_dana . '.';
        // die();
        $pecah              = explode(",", $to);
        $jumlah             = count($pecah);
        $from               = "DANASYARIAH"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
        $username           = "danasyariahpremium"; //your smsviro username
        $password           = "Dsi701@2019"; //your smsviro password
        $postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS

        for ($i = 0; $i < $jumlah; $i++) {
            if (substr($pecah[$i], 0, 2) == "62" || substr($pecah[$i], 0, 3) == "+62") {
                $pecah = $pecah;
            } elseif (substr($pecah[$i], 0, 1) == "0") {
                $pecah[$i][0] = "X";
                $pecah = str_replace("X", "62", $pecah);
            } else {
                echo "Invalid mobile number format";
            }
            $destination = array("to" => $pecah[$i]);
            $message     = array(
                "from" => $from,
                "destinations" => $destination,
                "text" => $text,
                "smsCount" => 20
            );
            $postData           = array("messages" => array($message));
            $postDataJson       = json_encode($postData);
            $ch                 = curl_init();
            $header             = array("Content-Type:application/json", "Accept:application/json");

            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $responseBody = json_decode($response);
            curl_close($ch);
        }

        if ($response) {
            return response()->json(['success' => $text]);
        } else {
            return response()->json(['success' => 'gagal']);
        }
    }

    public function testing()
    {
        $data = array(
            'amount' => '2000000',
            'investor_id' => '5358'
        );
        $this->msgVerification($data);
    }

    public function penarikanDanaNew($id, $dana)
    {
        // $rekening = Auth::user()->rekeningInvestor;
        // $requestAmount = $request->nominal;

        // use laravel collection method SUM()
        // $sumAvailable = $rekening->unallocated;

        // if($requestAmount > $sumAvailable || $requestAmount <= 0){
        //     // Throw error, cant  request more than available sum
        //     return redirect()->back()->with('error','Penarikan dana anda lebih dari dana tersedia, silahkan mengambil uang pada pendanaan anda terlebih dahulu');
        // }
        $cekDanaTersedia = RekeningInvestor::where('investor_id', $id)->first();
        $data_rekening = DetilInvestor::where('investor_id', $id)->first();
        $jumlahPenarikan = PenarikanDana::where('investor_id', $id)->where('accepted', 0)->sum('jumlah');
        // dd($jumlahPenarikan);die;
        $totalDana = $dana + $jumlahPenarikan;

        if ($totalDana <= $cekDanaTersedia->unallocated) {
            // Create new record penarikan dana
            PenarikanDana::create([
                'investor_id' => $id,
                'jumlah' => $dana,
                'no_rekening' => $data_rekening->rekening,
                'bank' => $data_rekening->bank,
                'accepted' => 0,
                'perihal' => 'Pengajuan Penarikan Dana',
            ]);

            // event(new MutasiInvestorEvent(Auth::user()->id,'request DEBIT',-$request->nominal,'Penarikan dana sedang diproses'));
            return redirect('user/dashboard')->with('success', 'Penarikan dana anda akan kami proses. Terima kasih');
        } else {
            return redirect('user/dashboard')->with('success', 'Penarikan dana anda akan kami proses. Terima kasih');
        }
    }

    public function verificationCode($id)
    {

        $phone_get = DetilInvestor::where('investor_id', $id)->first(['phone_investor']);
        // $to =  $phone_get;
        $to = '082213953400';
        $otp = rand(100000, 999999);
        $text =  'Kode OTP : ' . $otp . ' Silahkan masukan kode ini untuk melanjutkan proses penarikan tunai.';

        //send to db
        $detil = DetilInvestor::where('investor_id', $id)->update(['OTP' => $otp]);

        $pecah              = explode(",", $to);
        $jumlah             = count($pecah);
        $from               = "SMSVIRO"; //Sender ID or SMS Masking Name, if leave blank, it will use default from telco
        // $username           = "smsvirodemo";
        // $password           = "qwerty@123";
        // $from               = "DANASYARIAH";
        $username           = "danasyariahpremium"; //your smsviro username
        $password           = "Dsi701@2019"; //your smsviro password
        $postUrl            = "http://107.20.199.106/restapi/sms/1/text/advanced"; # DO NOT CHANGE THIS

        for ($i = 0; $i < $jumlah; $i++) {
            if (substr($pecah[$i], 0, 2) == "62" || substr($pecah[$i], 0, 3) == "+62") {
                $pecah = $pecah;
            } elseif (substr($pecah[$i], 0, 1) == "0") {
                $pecah[$i][0] = "X";
                $pecah = str_replace("X", "62", $pecah);
            } else {
                echo "Invalid mobile number format";
            }
            $destination = array("to" => $pecah[$i]);
            $message     = array(
                "from" => $from,
                "destinations" => $destination,
                "text" => $text,
                "smsCount" => 20
            );
            $postData           = array("messages" => array($message));
            $postDataJson       = json_encode($postData);
            $ch                 = curl_init();
            $header             = array("Content-Type:application/json", "Accept:application/json");

            curl_setopt($ch, CURLOPT_URL, $postUrl);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $responseBody = json_decode($response);
            curl_close($ch);
        }

        if ($detil) {
            $data = ['success' => true, 'message' => 'Silahkan masukan kode ini untuk melanjutkan proses penarikan tunai.'];
            return response()->json($data);
        } else {
            $data = ['success' => false, 'message' => 'Data Telepon tidak benar.'];
            return response()->json($data);
        }
    }

    public function sendVerifikasi($id, $otp, $dana)
    {

        $cek = DetilInvestor::where('investor_id', $id)->where('OTP', $otp)->count();
        if ($cek == 1) {
            $detil = DetilInvestor::where('investor_id', $id)->update(['OTP' => '']);
            $this->penarikanDanaNew($id, $dana);
            $data = ['success' => true, 'message' => 'Kode OTP Authtentication.'];
            return response()->json($data);
        } else {
            $data = ['success' => false, 'message' => ' Kode Salah, Masukan Kembali. '];
            return response()->json($data);
        }
    }

    // public function enkripsi(Request $request){
    //     $username = $request->username;
    //     $date = \Carbon\Carbon::now()->addYear(4);
    //     // $user = Investor::where('username', $username)->first();
    //     $data = [
    //         // 'type' => 'createbilling',
    //         // 'client_id' => self::CLIENT_ID,
    //         // 'trx_id' => $user->id,
    //         // 'trx_amount' => '0',
    //         // 'customer_name' => $user->detilInvestor->nama_investor,
    //         // 'customer_email' => $user->email,
    //         // 'virtual_account' => '8'.self::CLIENT_ID.$user->detilInvestor->getVa(),
    //         // 'datetime_expired' => $date->format('Y-m-d').'T'.$date->format('H:i:sP'),
    //         // 'billing_type' => 'o',

    //         "trx_id" => "1230000001",
    //         "virtual_account" => "8805085123234345",
    //         "customer_name" => "Mr. X",
    //         "trx_amount" => "100000",
    //         "payment_amount" => "100000",
    //         "cumulative_payment_amount" => "100000",
    //         "payment_ntb" => "233171",
    //         "datetime_payment" => "2016-03-01 14:00:00", 
    //         "datetime_payment_iso8601" => "2016-03-01T14:00:00+07:00"
    //     ];


    //     $encrypted = BniEnc::encrypt($data,self::CLIENT_ID,self::KEY);

    //     return $encrypted;
    // }

    public function checkStatusUser($id_proyek, $qty)
    {
        $investor_id = Auth::user()->id;
        $username    = Auth::user()->username;

        $log_akad   = LogAkadDigiSignInvestor::where('investor_id', $investor_id)->where('proyek_id', $id_proyek)->whereIn('status', ['waiting', 'complete'])->first();
        if (isset($log_akad)) {
            return response()->json([
                'status'        => 'sudah_ttd',
                'keterangan'    => "Sudah TTD Akad, Lanjut ke proses Pendanaan"
            ]);
        } else {
            return response()->json([
                'status'        => 'belum TTD',
                'keterangan'    => "Belum TTD Akad"
            ]);
        }
    }

    public function checkStatusUserInvest($id_proyek, $qty)
    {

        // $investor_id = 52257;
        // $username = 'dudu';
        $investor_id = Auth::user()->id;
        $username    = Auth::user()->username;
        $client      = new RDLController;
        $log_generate = new logGenerateVaRdl;

        $rekening   = RekeningInvestor::where('investor_id', $investor_id)->first();
        $rdl        = RDLAccountNumber::where('investor_id', $investor_id)->first();
        $log_akad   = LogAkadDigiSignInvestor::where('investor_id', $investor_id)->where('proyek_id', $id_proyek)->whereIn('status', ['waiting', 'complete'])->first();
        $proyek     = Proyek::where('id', '=', $id_proyek)->first();

        //Jika sudah punya rekening
        if (isset($rekening) and isset($rdl)) {
            if ($rekening->va_number == "" || $rekening->va_number == null) {
                // generate VA
                $generate_va = $this->generateVABNI_Investor_test($username);

                if (!$generate_va) {
                    $generate_cif_number = $client->RegisterInvestor($investor_id, '009');
                    $response_decode = json_decode($generate_cif_number);
                    $cif_number = $response_decode->{'response'}->{'cifNumber'};
                    if ($response_decode->{'response'}->{'responseCode'} == "0001") {
                        $generate_account_number = $client->RegisterInvestorAccount($investor_id, $cif_number, '009');
                        $response_decode_account = json_decode($generate_account_number);
                        if ($response_decode_account->{'response'}->{'responseCode'} == "0001") {
                            $log_generate->status = 011;
                            $log_generate->investor_id = $investor_id;
                            $log_generate->status_number = 'VA Gagal, CIF Sukses, ACN Sukses';
                            $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                            $log_generate->save();

                            return response()->json([
                                'status'        => '011',
                                'status_number' => 'VA Gagal, CIF Sukses, ACN Sukses',
                                'keterangan'    => "Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA"
                            ]);
                        } else {
                            $log_generate->status = 010;
                            $log_generate->investor_id = $investor_id;
                            $log_generate->status_number = 'VA Gagal, CIF Sukses, ACN Gagal';
                            $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                            $log_generate->save();

                            return response()->json([
                                'status'        => '010',
                                'status_number' => 'VA Gagal, CIF Sukses, ACN Gagal',
                                'keterangan'    => 'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA'
                            ]);
                        }
                    } else {

                        $log_generate->status = 000;
                        $log_generate->investor_id = $investor_id;
                        $log_generate->status_number = 'VA Gagal, CIF Gagal, ACN Gagal';
                        $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                        $log_generate->save();

                        return response()->json([
                            'status'        => '000',
                            'status_number' => 'VA Gagal, CIF Gagal, ACN Gagal',
                            'keterangan'    => "Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA"
                        ]);
                    }
                } else {
                    $generate_cif_number = $client->RegisterInvestor($investor_id, '009');
                    $response_decode = json_decode($generate_cif_number);
                    $cif_number = $response_decode->{'response'}->{'cifNumber'};
                    if ($response_decode->{'response'}->{'responseCode'} == "0001") {
                        $generate_account_number = $client->RegisterInvestorAccount($investor_id, $cif_number, '009');
                        $response_decode_account = json_decode($generate_account_number);
                        if ($response_decode_account->{'response'}->{'responseCode'} == "0001") {

                            $log_generate->status = 111;
                            $log_generate->investor_id = $investor_id;
                            $log_generate->status_number = 'VA Sukses, CIF Sukses, ACN Sukses';
                            $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                            $log_generate->save();

                            return response()->json([
                                'status'        => '111',
                                'status_number' => 'VA Sukses, CIF Sukses, ACN Sukses',
                                'keterangan'    => "Maaf Dana Anda Tidak Cukup, Silahkan Top Up Dahulu ke Rekening anda"
                            ]);
                        } else {

                            $log_generate->status = 110;
                            $log_generate->investor_id = $investor_id;
                            $log_generate->status_number = 'VA Sukses, CIF Sukses, ACN Gagal';
                            $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                            $log_generate->save();

                            return response()->json([
                                'status'        => '110',
                                'status_number' => 'VA Sukses, CIF Sukses, ACN Gagal',
                                'keterangan'    => "Maaf Pembuatan Akun Number anda gagal, Harap menghubungi Customer Service kami untuk pembuatan Akun Number"
                            ]);
                        }
                    } else {

                        $log_generate->status = 100;
                        $log_generate->investor_id = $investor_id;
                        $log_generate->status_number = 'VA Sukses, CIF Gagal, ACN Gagal';
                        $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                        $log_generate->save();

                        return response()->json([
                            'status'        => '100',
                            'status_number' => 'VA Sukses, CIF Gagal, ACN Gagal',
                            'keterangan'    => "Maaf Pembuatan CIF dan Akun Number anda gagal, Harap menghubungi Customer Service kami untuk pembuatan CIF dan Akun Number"
                        ]);
                    }
                }
                // return redirect()->back()->with('msg_error', 'Maaf Dana Anda Tidak Mencukupi. Silahkan Top up terlebih dahulu'); // error validasi investasi
            } else {
                if ($rekening->unallocated < $proyek->harga_paket * $qty) {
                    return response()->json([
                        'status' => 'gagal_dana',
                        'keterangan' => "Maaf Dana Anda Tidak Cukup, Silahkan Top Up Dahulu ke Rekening anda"
                    ]);
                    // return redirect()->back()->with('msg_error', 'Maaf Dana Anda Tidak Mencukupi. Silahkan Top up terlebih dahulu'); // error validasi investasi
                } else {
                    return response()->json([
                        'status'        => 'lanjut_pendanaan',
                        'keterangan'    => "Lanjut ke proses Pendanaan"
                    ]);
                }
            }
        } elseif (isset($rekening)) {
            $generate_cif_number = $client->RegisterInvestor($investor_id, '009');
            $response_decode = json_decode($generate_cif_number);
            $cif_number = $response_decode->{'response'}->{'cifNumber'};
            if ($response_decode->{'response'}->{'responseCode'} == "0001") {
                $generate_account_number = $client->RegisterInvestorAccount($investor_id, $cif_number, '009');
                $response_decode_account = json_decode($generate_account_number);
                if ($response_decode_account->{'response'}->{'responseCode'} == "0001") {

                    $log_generate->status = 011;
                    $log_generate->investor_id = $investor_id;
                    $log_generate->status_number = 'VA Gagal, CIF Sukses, ACN Sukses masuk elseif';
                    $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                    $log_generate->save();

                    return response()->json([
                        'status'        => '011',
                        'status_number' => 'VA Gagal, CIF Sukses, ACN Sukses',
                        'keterangan'    => "Maaf Dana Anda Tidak Cukup, Silahkan Top Up Dahulu ke Rekening anda"
                    ]);
                } else {
                    $log_generate->status = 010;
                    $log_generate->investor_id = $investor_id;
                    $log_generate->status_number = 'VA Gagal, CIF Sukses, ACN Gagal';
                    $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                    $log_generate->save();


                    return response()->json([
                        'status'        => '010',
                        'status_number' => 'VA Gagal, CIF Sukses, ACN Gagal',
                        'keterangan'    => "Maaf Dana Anda Tidak Cukup, Silahkan Top Up Dahulu ke Rekening anda"
                    ]);
                }
            } else {

                $log_generate->status = 000;
                $log_generate->investor_id = $investor_id;
                $log_generate->status_number = 'VA Gagal, CIF Gagal, ACN Gagal';
                $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                $log_generate->save();

                return response()->json([
                    'status'        => '000',
                    'status_number' => 'VA Gagal, CIF Gagal, ACN Gagal',
                    'keterangan'    => "Maaf Dana Anda Tidak Cukup, Silahkan Top Up Dahulu ke Rekening anda"
                ]);
            }
        } else {
            $generate_va = $this->generateVABNI_Investor_test($username);

            if (!$generate_va) {
                $generate_cif_number = $client->RegisterInvestor($investor_id, '009');
                $response_decode = json_decode($generate_cif_number);
                $cif_number = $response_decode->{'response'}->{'cifNumber'};
                if ($response_decode->{'response'}->{'responseCode'} == "0001") {
                    $generate_account_number = $client->RegisterInvestorAccount($investor_id, $cif_number, '009');
                    $response_decode_account = json_decode($generate_account_number);
                    if ($response_decode_account->{'response'}->{'responseCode'} == "0001") {

                        $log_generate->status = 011;
                        $log_generate->investor_id = $investor_id;
                        $log_generate->status_number = 'VA Gagal, CIF Sukses, ACN Sukses masuk else';
                        $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                        $log_generate->save();

                        return response()->json([
                            'status'        => '011',
                            'status_number' => 'VA Gagal, CIF Sukses, ACN Sukses',
                            'keterangan'    => "Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA"
                        ]);
                    } else {

                        $log_generate->status = 010;
                        $log_generate->investor_id = $investor_id;
                        $log_generate->status_number = 'VA Gagal, CIF Sukses, ACN Sukses';
                        $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                        $log_generate->save();

                        return response()->json([
                            'status'        => '010',
                            'status_number' => 'VA Gagal, CIF Sukses, ACN Sukses',
                            'keterangan'    => "Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA"
                        ]);
                    }
                } else {

                    $log_generate->status = 000;
                    $log_generate->investor_id = $investor_id;
                    $log_generate->status_number = 'VA Gagal, CIF Gagal, ACN Gagal';
                    $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                    $log_generate->save();

                    return response()->json([
                        'status'        => '000',
                        'status_number' => 'VA Gagal, CIF Gagal, ACN Gagal',
                        'keterangan'    => "Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA"
                    ]);
                }
            } else {
                $generate_cif_number = $client->RegisterInvestor($investor_id, '009');
                $response_decode = json_decode($generate_cif_number);
                $cif_number = $response_decode->{'response'}->{'cifNumber'};
                if ($response_decode->{'response'}->{'responseCode'} == "0001") {
                    $generate_account_number = $client->RegisterInvestorAccount($investor_id, $cif_number, '009');
                    $response_decode_account = json_decode($generate_account_number);
                    if ($response_decode_account->{'response'}->{'responseCode'} == "0001") {

                        $log_generate->status = 111;
                        $log_generate->investor_id = $investor_id;
                        $log_generate->status_number = 'VA Sukses, CIF Sukses, ACN Sukses';
                        $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                        $log_generate->save();

                        return response()->json([
                            'status'        => '111',
                            'status_number' => 'VA Sukses, CIF Sukses, ACN Sukses',
                            'keterangan'    => "Maaf Dana Anda Tidak Cukup, Silahkan Top Up Dahulu ke Rekening anda"
                        ]);
                    } else {

                        $log_generate->status = 110;
                        $log_generate->investor_id = $investor_id;
                        $log_generate->status_number = 'VA Sukses, CIF Sukses, ACN Gagal';
                        $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                        $log_generate->save();

                        return response()->json([
                            'status'        => '110',
                            'status_number' => 'VA Sukses, CIF Sukses, ACN Gagal',
                            'keterangan'    => "Maaf Pembuatan Akun Number anda gagal, Harap menghubungi Customer Service kami untuk pembuatan Akun Number"
                        ]);
                    }
                } else {

                    $log_generate->status = 100;
                    $log_generate->investor_id = $investor_id;
                    $log_generate->status_number = 'VA Sukses, CIF Gagal, ACN Gagal';
                    $log_generate->keterangan =  'Maaf Pembuatan VA anda gagal, Harap menghubungi Customer Service kami untuk pembuatan VA';
                    $log_generate->save();

                    return response()->json([
                        'status'        => '100',
                        'status_number' => 'VA Sukses, CIF Gagal, ACN Gagal',
                        'keterangan'    => "Maaf Pembuatan CIF dan Akun Number anda gagal, Harap menghubungi Customer Service kami untuk pembuatan CIF dan Akun Number"
                    ]);
                }
            }
        }
    }

    // check limit dana threshold investor 
    public function checkStatusTreshold()
    {

        $investor_id = Auth::user()->id;
        $username   = Auth::user()->username;
        $privyID    = new PrivyController;

        $check_threshold = DB::select("select func_check_threshold('$investor_id') as response");
        $return_check_register = response()->json(['data' => $check_threshold]);

        if ($check_threshold[0]->{"response"} == 1) { // jika dana total asset lebih dari atau sama dengan dari treshold 

            $return_check_register = $privyID->checkRegistrasi($investor_id, 1);
            return $return_check_register;
        } else { // jika dana total asset kurang dari treshold
            $return_check_register = array('status' => 'under_threshold');
            return response()->json($return_check_register);
        };
    }

    //GENERATE VA BNI KONVENSIONAL
    public function generateVABNI_Investor($username)
    {
        $user = Investor::where('username', $username)->first();
        $date = \Carbon\Carbon::now()->addYear(4);

        echo '8' . config('app.bnik_id') . $user->detilInvestor->getVa();
        die;

        $data = [
            'type' => 'createbilling',
            'client_id' => config('app.bnik_id'),
            'trx_id' => $user->id,
            'trx_amount' => '0',
            'customer_name' => $user->detilInvestor->nama_investor,
            'customer_email' => $user->email,
            'virtual_account' => '8' . config('app.bnik_id') . $user->detilInvestor->getVa(),
            'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
            'billing_type' => 'o',
        ];


        $encrypted = BniEnc::encrypt($data, config('app.bnik_id'), config('app.bnik_key'));

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post(config('app.bnik_url'), [
            'json' => [
                'client_id' => config('app.bnik_id'),
                'data' => $encrypted,
            ]
        ]);

        $result = json_decode($result->getBody()->getContents());
        print_r($result);
        die;
        if ($result->status !== '000') {
            return false;
        } else {
            $decrypted = BniEnc::decrypt($result->data, config('app.bnik_id'), config('app.bnik_key'));
            //return json_encode($decrypted);
            $user->RekeningInvestor()->create([
                'investor_id' => $user->id,
                'total_dana' => 0,
                'va_number' => $decrypted['virtual_account'],
                'unallocated' => 0,
            ]);

            return true;
            // return view('pages.user.add_funds')->with('message','VA Generate Success!');
        }
    }

    // generate va proyek
    public function generateVABNI_Borrower($username, $id_proyek)
    {

        $date = \Carbon\Carbon::now()->addYear(4);
        $data_proyek = Proyek::select('tgl_mulai', 'id')->where('id', $id_proyek)->first();
        $year =  substr($data_proyek->tgl_mulai, 2, 2);
        $last_digit = sprintf("%04d", $data_proyek->id);


        $user = LoginBorrower::where('username', $username)->first();
        $data = [
            'type' => 'createbilling',
            'client_id' => config('app.bnik_id'),
            'trx_id' => $id_proyek,
            'trx_amount' => '0',
            'customer_name' => $user->username,
            'customer_email' => $user->email,
            'virtual_account' => '988' . config('app.bnik_id') . '02' . $year . $last_digit,
            'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
            'billing_type' => 'o',
        ];

        $encrypted = BniEnc::encrypt($data, config('app.bnik_id'), config('app.bnik_key'));

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post(config('app.bnik_url'), [
            'json' => [
                'client_id' => config('app.bnik_id'),
                'data' => $encrypted,
            ]
        ]);

        $result = json_decode($result->getBody()->getContents());
        //print_r($result);

        if ($result->status !== '000') {
            print_r($result);
        } else {

            $decrypted = BniEnc::decrypt($result->data, config('app.bnik_id'), config('app.bnik_key'));
            //return json_encode($decrypted);
            $updateDetails = [
                'va_number' =>  $decrypted['virtual_account']
            ];
            $updateVaNumberProyek = [
                'va_number' =>  $decrypted['virtual_account']
            ];


            // BorrowerRekening::where('brw_id',$user->brw_id)
            // ->update($updateDetails);

            // 

            BorrowerPendanaan::where('id_proyek', $id_proyek)
                ->update($updateVaNumberProyek);



            return $decrypted['virtual_account'];
            // return view('pages.user.add_funds')->with('message','VA Generate Success!');
        }
    }

    //GENERATE VA BNI FOR TESTING
    public function generateVABNI_Investor_test($username)
    {

        $log_generate = new logGenerateVaRdl;

        $user = Investor::where('username', $username)->first();
        $date = \Carbon\Carbon::now()->addYear(4);

        $data = [
            'type' => 'createbilling',
            'client_id' => config('app.bnik_id'),
            'trx_id' => $user->id,
            'trx_amount' => '0',
            'customer_name' => $user->detilInvestor->nama_investor,
            'customer_email' => $user->email,
            'virtual_account' => '988' . config('app.bnik_id') . $user->detilInvestor->getVA_konv(),
            'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
            'billing_type' => 'o',
        ];

        $encrypted = BniEnc::encrypt($data, config('app.bnik_id'), config('app.bnik_key'));

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post(config('app.bnik_url'), [
            'json' => [
                'client_id' => config('app.bnik_id'),
                'data' => $encrypted,
            ]
        ]);

        $result = json_decode($result->getBody()->getContents());

        if ($result->status !== '000') {
            $log_generate->status = 010;
            $log_generate->status_number = $username;
            $log_generate->keterangan =  $result->message;
            $log_generate->save();

            return false;
        } else {
            $decrypted = BniEnc::decrypt($result->data, config('app.bnik_id'), config('app.bnik_key'));
            //return json_encode($decrypted);
            $user->RekeningInvestor()->create([
                'investor_id' => $user->id,
                'total_dana' => 0,
                'va_number' => $decrypted['virtual_account'],
                'unallocated' => 0,
            ]);

            return true;
            // return view('pages.user.add_funds')->with('message','VA Generate Success!');
        }
    }

    public function bankResponseKonven(Request $request)
    {

        $data = $request->input('data');
        if ($request->input('client_id') != config('app.bnik_id')) {
            return response()->json([
                'status' => '999',
                'message' => 'Access Denied',
            ]);
        }

        $decrypted         = BniEnc::decrypt($data, config('app.bnik_id'), config('app.bnik_key'));
        $va_investor     = $decrypted['virtual_account'];
        $payment_ntb     = $decrypted['payment_ntb'];

        $rekening         = RekeningInvestor::where('va_number', $va_investor)->first();
        $amount         = (int)$decrypted['payment_amount'];
        $payment_ntb_exist = $rekening['payment_ntb'];
        $investor_id = $rekening['investor_id'];

        $type_user = substr($va_investor, 8, 2);

        if ($payment_ntb != $payment_ntb_exist) {
            if ($type_user == 01) {
                // $rekening->total_dana += $amount;
                // $rekening->unallocated += $amount;
                // $rekening->payment_ntb = $payment_ntb;
                // $rekening->save();
                DB::SELECT("call proc_update_rek_investor($investor_id,$amount,$payment_ntb,__FILE__,__LINE__)");
            } else {

                $BorrowerPendanaan     = BorrowerPendanaan::where('va_number', $va_investor)->first();
                $BorrowerRekening     = BorrowerRekening::where('brw_id', $BorrowerPendanaan->brw_id)->first();
                $BorrowerRekening->total_sisa += $amount;
                $BorrowerRekening->total_terpakai -= $amount;
                $BorrowerRekening->save();
            }
        } else {
            return response()->json([
                'status' => '888',
                'message' => 'Payment NTB double',
            ]);
        }
    }

    public function convertCSV_AFPI()
    {

        $tanggal = date('Ymd');
        //BIKIN FILE CSV SIMPEN KE STORAGE
        $records = [
            [1, 2, 3],
            ['foo', 'bar', 'baz'],
            ['john', 'doe', 'john.doe@example.com'],
        ];


        $filename = config('app.sft_id') . '20200328' . 'SIK' . '01';
        $writer = Writer::createFromPath(storage_path('app/public/fdc/' . $filename . '.csv'), 'w+');
        $writer->setDelimiter('|');
        $writer->insertAll($records); //using an array

        //$path_ktp = storage_path('app/public/fdc/'.$filename.'.csv');
        $path = Storage::disk('public')->put('app/public/fdc/' . $filename . '.csv', $writer);
        //FINISH CREATE CSV


        //AMBIL FILE CSV YG UDAH DIBIKIN DIATAS, DIMASUKIN KE ZIP BUAT PASSWORD
        $zip = new ZipArchive;
        if ($zip->open($filename . '.zip', ZipArchive::CREATE) === TRUE) {


            $zip->setPassword($this->GeneratepasswordZIP()); // set password
            $path_ktp = 'user/88887777/' . Carbon::now()->toDateString() . 'mihihi.' . 'csv';
            $zip->addFile(storage_path('app/public/fdc/' . $filename . '.csv'));
            //$zip->addFile(storage_path('app/public/fdc/'.$filename.'.csv'), 'test.csv');
            $zip->setEncryptionName('app/public/fdc/' . $filename . '.csv', ZipArchive::EM_AES_256); //Add file name and password dynamically

            // All files are added, so close the zip file.
            $zip->close();
        }

        //SELESAI ZIP

        // $path_ktp = 'user/88887777/'.Carbon::now()->toDateString() . 'mahaha.'.'zip';
        // $path = Storage::disk('public')->put($path_ktp, $zip);

        $path_afpi = 'in/' . Carbon::now()->toDateString() . 'testing.' . 'csv';
        $path = Storage::disk('ftp')->put($path_afpi, $writer);

        if ($path) {
            echo 'sukses';
        } else {
            echo 'gagal';
        }
    }

    // consume inquiry fdc
    public function inquiry_fdc()
    {


        $date    = date('dmY');
        $client = new Client();
        $res = $client->request('GET', config('app.sftp_host') . '/api/v1/Inquiry', [
            'headers' => [
                'Content-Type'    => 'application/json'
            ],
            'auth' => [config('app.sftp_account_username'), config('app.sftp_account_password')],
            'query' =>
            [
                "id"        => "3172032803910005",
                "reason"    => "1",
                "reffid"    => ""
            ]
        ]);

        $response = $res->getBody();
        $response_decode = json_decode($response);
        return $response;
    }

    // consume password fdc
    public function passwordZIP()
    {

        $client = new Client();
        $credentials = base64_encode(config('app.sftp_account_username') . ':' . config('app.sftp_account_password'));

        $request       = $client->post(config('app.sftp_host') . '/api/v1/ZipPassword', [
            'headers' => [
                'Content-Type'    => 'application/json',
                //'Authorization' => 'Basic ' . (string)config('app.sftp_account_username').':'.config('app.sftp_account_password'),
            ],
            'auth' => [config('app.sftp_account_username'), config('app.sftp_account_password')],
            'body' => json_encode(
                ["zipPwd" => $this->GeneratepasswordZIP()]
            )

        ]);

        $response = $request->getBody();
        $response_decode = json_decode($response);
        return $response;
    }


    // generate password zip
    function GeneratepasswordZIP()
    {

        $getMonth = date('m'); // get bulan

        // select password in table
        $passwordZip = DB::table('fdc_zip_password')
            ->orderBy('tanggal', 'asc')
            ->first();

        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;

        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        $password = implode($pass); // set password

        $getMonthPass = explode('-', $passwordZip->tanggal); // get bulan

        if ($passwordZip) {
            if ($getMonth == $getMonthPass[1]) {

                return $passwordZip->password; //turn the array into a string

            } else {

                $updatePassword = FDCPassword::where('id', 1)->update(['tanggal' => date('Y-m-d'), 'password' => $password]);

                return $updatePassword->password; //turn the array into a string
            }
        } else {

            $fdc_passsword = new \App\FDCPassword();
            $fdc_passsword->tanggal        = date('Y-m-d');
            $fdc_passsword->password    = $password;
            $insert_pendanaan->save();

            return $password; //turn the array into a string
        }
    }



    public function generateVA_AdditionalBankCIMBS($no_va, $userid, $invid)
    {

        // CIMB syariah, BCA Syariah, Mega Syariah, Muamalat, Panin Dubai Syariah, BTPN Syariah, Bukopin Syariah

        // $codebankArray = [config('app.cimbs_id'), "536", "506", "147", "517","547", "521"];
        // $corp_codeArray = [config('app.cimbs_id_comp_code'), "001", "001", "001", "001","001", "001"];
        $codebankArray = [config('app.cimbs_id')];
        $corp_codeArray = [config('app.cimbs_id_comp_code')];

        $arrayLength = count($codebankArray);

        $i = 0;
        while ($i < $arrayLength) {

            $bank = $codebankArray[$i];
            $corp_code = $corp_codeArray[$i];

            $cekva = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE investor_id = " . $invid . " AND kode_bank = " . $bank . "");
            if ($cekva == NULL) {
                DB::table("detil_rekening_investor")->insert(array(
                    'investor_id' => $invid,
                    'va_number' => $corp_code . $no_va,
                    'kode_bank' => $bank
                ));
            }

            $insertLog = DB::table("log_db_app")->insert(array(
                "file_name" => "RekeningController.php",
                "line" => __LINE__,
                "description" => "Sukses Generate VA " . $no_va . " Bank " . $bank . " " . $userid,
                "created_at" => date("Y-m-d H:i:s")
            ));

            $i++;
        }
    }



    public function bankResponse022_inquiry(Request $request)
    {


        $BillDetail = [

            'BillCurrency' => "",
            'BillCode' => "",
            'BillAmount' => "",
            'BillReference' => ""
        ];
        $BillDetailList = ['BillDetail' => $BillDetail];

        $va_investor = config('app.cimbs_id_comp_code') . $request->input('CustomerKey1');
        $transactionID =  $request->input('TransactionID');
        $trx_date_va = $request->input('TransactionDate');
        $channelid = $request->input('ChannelID');
        $terminalID = $request->input('TerminalID');
        if (is_null($terminalID))
            $terminalID = "";
        $companyCode = $request->input('CompanyCode');

        if ($companyCode != config('app.cimbs_id_comp_code')) {
            return response()->json([

                'TransactionID' => $transactionID,
                'ChannelID' =>  $channelid,
                'TerminalID' => $terminalID,
                'TransactionDate' => $request->input('TransactionDate'),
                'CompanyCode' => $companyCode,
                'CustomerKey1' => $request->input('CustomerKey1'),
                'CustomerName' => '',
                'CustomerKey2' => '',
                'CustomerKey3' => '',
                'Currency' => 'IDR',
                'Amount' => 0,
                'Fee' => 0,
                'PaidAmount' => 0,
                'AdditionalData1' => '',
                'AdditionalData2' => '',
                'AdditionalData3' => '',
                'AdditionalData4' => '',
                'FlagPayment' => '1',
                'BillDetailList' => $BillDetailList,

                'ResponseCode' => '999',
                'ResponseDescription' => 'Access Denied',
            ]);
        }

        // syslog(0,"NO VA=".$va_investor);

        // inquiry 
        $bank = config('app.cimbs_id');
        // $rekening = RekeningInvestor::where('va_number', $va_investor)
        //     ->where('kode_bank', config('app.cimbs_id'))
        //     ->first();
        $rekening = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE va_number = " . $va_investor . " AND kode_bank = " . config('app.cimbs_id') . "");

        if ($rekening == null) {

            //  check first BNIS/BSI
            // syslog(0,"CHECK BNIS FIRST");

            $rekening = RekeningInvestor::where(\DB::raw('substr(va_number, 5)'), '=', substr($va_investor, 4))
                ->where('kode_bank', '427')
                ->first();
            $data_user = DetilInvestor::where('investor_id', $rekening['investor_id'])->first();

            if ($rekening == null) {
                // syslog(0," va not found bnis cimb");

                $insertLog = DB::table("log_db_app")->insert(array(
                    "file_name" => "RekeningController.php",
                    "line" => __LINE__,
                    "description" => "Inquiry not found: VA " . $va_investor . " Bank " . $bank,
                    "created_at" => date("Y-m-d H:i:s")
                ));




                return response()->json([

                    'TransactionID' => $transactionID,
                    'ChannelID' =>  $channelid,
                    'TerminalID' => $terminalID,
                    'TransactionDate' => $request->input('TransactionDate'),
                    'CompanyCode' => $companyCode,
                    'CustomerKey1' => $request->input('CustomerKey1'),
                    'CustomerName' => '',
                    'CustomerKey2' => '',
                    'CustomerKey3' => '',
                    'Currency' => 'IDR',
                    'Amount' => 0,
                    'Fee' => 0,
                    'PaidAmount' => 0,
                    'AdditionalData1' => '',
                    'AdditionalData2' => '',
                    'AdditionalData3' => '',
                    'AdditionalData4' => '',
                    'FlagPayment' => '1',
                    'BillDetailList' => $BillDetailList,

                    'ResponseCode' => '16',
                    'ResponseDescription' => 'Customer Not Found',
                ]);
            } else {
                $data_user = Investor::where('id', $rekening['investor_id'])->first();
                $userid = $data_user['username'];
                $this->generateVA_AdditionalBankCIMBS($request->input('CustomerKey1'), $userid, $rekening['investor_id']);
            }
        } else {
            $data_user = DetilInvestor::where('investor_id', $rekening[0]->investor_id)->first();
        }


        $nama_rekening = $data_user['nama_investor'];

        DB::table("log_db_app")->insert(array(
            "file_name" => "RekeningController.php",
            "line" => __LINE__,
            "description" => "Inquiry found: VA " . $va_investor . " Bank " . $bank . " " . $nama_rekening,
            "created_at" => date("Y-m-d H:i:s")
        ));



        return response()->json([

            'TransactionID' => $transactionID,
            'ChannelID' =>  $channelid,
            'TerminalID' => $terminalID,
            'TransactionDate' => $request->input('TransactionDate'),
            'CompanyCode' => $companyCode,
            'CustomerKey1' => $request->input('CustomerKey1'),
            'CustomerName' => $nama_rekening,
            'CustomerKey2' => '',
            'CustomerKey3' => '',
            'Currency' => 'IDR',
            'Amount' => 0,
            'Fee' => 0,
            'PaidAmount' => 0,
            'AdditionalData1' => '',
            'AdditionalData2' => '',
            'AdditionalData3' => '',
            'AdditionalData4' => '',
            'FlagPayment' => '1',
            'BillDetailList' => $BillDetailList,
            'ResponseCode' => '00',
            'ResponseDescription' => 'Transaction Success'
        ]);
    }


    public function bankResponse022_payment(Request $request)
    {

        $request2 = $request->input('PaymentRq');

        $va_investor = config('app.cimbs_id_comp_code') . $request2['CustomerKey1'];
        $transactionID =  $request2['TransactionID'];
        $trx_date_va = $request2['TransactionDate'];
        $channelid = $request2['ChannelID'];
        $terminalID = $request2['TerminalID'];

        if (is_null($terminalID))
            $terminalID = "";

        if (!isset($request2['CustomerKey2']))
            $request2['CustomerKey2'] = "";
        else {
            if (is_null($request2['CustomerKey2']))
                $request2['CustomerKey2'] = "";
        }
        if (!isset($request2['CustomerKey3']))
            $request2['CustomerKey3'] = "";
        else {
            if (is_null($request2['CustomerKey3']))
                $request2['CustomerKey3'] = "";
        }
        if (!isset($request2['AdditionalData1']))
            $request2['AdditionalData1'] = "";
        else {
            if (is_null($request2['AdditionalData1']))
                $request2['AdditionalData1'] = "";
        }
        if (!isset($request2['AdditionalData2']))
            $request2['AdditionalData2'] = "";
        else {
            if (is_null($request2['AdditionalData2']))
                $request2['AdditionalData2'] = "";
        }
        if (!isset($request2['AdditionalData3']))
            $request2['AdditionalData3'] = "";
        else {
            if (is_null($request2['AdditionalData3']))
                $request2['AdditionalData3'] = "";
        }
        if (!isset($request2['AdditionalData4']))
            $request2['AdditionalData4'] = "";
        else {
            if (is_null($request2['AdditionalData4']))
                $request2['AdditionalData4'] = "";
        }



        $companyCode = $request2['CompanyCode'];
        $amount = $request2['Amount'];
        $paidamount  = $request2['PaidAmount'];
        $ref_number_trx = $request2['ReferenceNumberTransaction'];
        $nama_rekening = $request2['CustomerName'];
        $bank = config('app.cimbs_id');

        // syslog(0,"comparing ".$companyCode." with ".config('app.cimbs_id_comp_code'));

        if ($companyCode != config('app.cimbs_id_comp_code')) {
            return response()->json([

                'TransactionID' => $transactionID,
                'ChannelID' =>  $channelid,
                'TerminalID' => $terminalID,
                'TransactionDate' => $request2['TransactionDate'],
                'CompanyCode' =>  $request2['CompanyCode'],
                'CustomerKey1' => $request2['CustomerKey1'],
                'CustomerName' => $request2['CustomerName'],
                'CustomerKey2' => $request2['CustomerKey2'],
                'CustomerKey3' => $request2['CustomerKey3'],
                'Currency' => $request2['Currency'],
                'Amount' => $request2['Amount'],
                'Fee' => $request2['Fee'],
                'PaidAmount' => $request2['PaidAmount'],
                'AdditionalData1' => $request2['AdditionalData1'],
                'AdditionalData2' => $request2['AdditionalData2'],
                'AdditionalData3' => $request2['AdditionalData3'],
                'AdditionalData4' => $request2['AdditionalData4'],
                'PaymentFlag' => '100000',
                'ReferenceNumberTransaction' => $ref_number_trx,
                'ResponseCode' => '999',
                'ResponseDescription' => 'Access Denied',
            ]);
        }

        if ($amount < config('app.cimbs_lender_min_amount')) {
            DB::table("log_db_app")->insert(array(
                "file_name" => "RekeningController.php",
                "line" => __LINE__,
                "description" => "Minimum Amount IDR " . config('app.cimbs_lender_min_amount') . " vaNumber " . $va_investor . " amount " . $amount . " bank " . $bank,
                "created_at" => date("Y-m-d H:i:s")
            ));
            return response()->json([
                'TransactionID' => $transactionID,
                'ChannelID' =>  $channelid,
                'TerminalID' => $terminalID,
                'TransactionDate' => $request2['TransactionDate'],
                'CompanyCode' =>  $request2['CompanyCode'],
                'CustomerKey1' => $request2['CustomerKey1'],
                'CustomerName' => $request2['CustomerName'],
                'CustomerKey2' => $request2['CustomerKey2'],
                'CustomerKey3' => $request2['CustomerKey3'],
                'Currency' => $request2['Currency'],
                'Amount' => $request2['Amount'],
                'Fee' => $request2['Fee'],
                'PaidAmount' => $request2['PaidAmount'],
                'AdditionalData1' => $request2['AdditionalData1'],
                'AdditionalData2' => $request2['AdditionalData2'],
                'AdditionalData3' => $request2['AdditionalData3'],
                'AdditionalData4' => $request2['AdditionalData4'],
                'PaymentFlag' => '100000',
                'ReferenceNumberTransaction' => $ref_number_trx,
                'ResponseCode' => '38',
                'ResponseDescription' => 'Minimum Amount IDR ' . config('app.cimbs_lender_min_amount')
            ]);
        } else {

            if ($amount % config('app.cimbs_lender_multiplication_amount') != 0) {
                DB::table("log_db_app")->insert(array(
                    "file_name" => "RekeningController.php",
                    "line" => __LINE__,
                    "description" => "Multiplication Amount IDR " . config('app.cimbs_lender_min_amount') . " vaNumber " . $va_investor . " amount " . $amount . " bank " . $bank,
                    "created_at" => date("Y-m-d H:i:s")
                ));
                return response()->json([
                    'TransactionID' => $transactionID,
                    'ChannelID' =>  $channelid,
                    'TerminalID' => $terminalID,
                    'TransactionDate' => $request2['TransactionDate'],
                    'CompanyCode' =>  $request2['CompanyCode'],
                    'CustomerKey1' => $request2['CustomerKey1'],
                    'CustomerName' => $request2['CustomerName'],
                    'CustomerKey2' => $request2['CustomerKey2'],
                    'CustomerKey3' => $request2['CustomerKey3'],
                    'Currency' => $request2['Currency'],
                    'Amount' => $request2['Amount'],
                    'Fee' => $request2['Fee'],
                    'PaidAmount' => $request2['PaidAmount'],
                    'AdditionalData1' => $request2['AdditionalData1'],
                    'AdditionalData2' => $request2['AdditionalData2'],
                    'AdditionalData3' => $request2['AdditionalData3'],
                    'AdditionalData4' => $request2['AdditionalData4'],
                    'PaymentFlag' => '100000',
                    'ReferenceNumberTransaction' => $ref_number_trx,
                    'ResponseCode' => '38',
                    'ResponseDescription' => 'Multiplication Amount IDR ' . config('app.cimbs_lender_min_amount')
                ]);
            }
        }


        $payment_ntb = $ref_number_trx;

        // $phone = str_replace('01', '', $request2['CustomerKey1']);
        // $getinvId = DB::SELECT("SELECT phone_investor, investor_id FROM detil_investor WHERE phone_investor LIKE '%" . $phone . "%'");
        // if (isset($getinvId)) {
        //     $invId = $getinvId[0]->investor_id;
        //     $detilRekening = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE investor_id=" . $invId . " AND kode_bank=" . $bank . "");
        // }

        $detilRekening = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE kode_bank = " . $bank . " AND va_number = " . $va_investor . "");
        if ($detilRekening == null) {
            DB::table("log_db_app")->insert(array(
                "file_name" => __FILE__,
                "line" => __LINE__,
                "description" => "Top UP CIMBS Gagal : Data detil dan Rekening Investor " . $va_investor . " Bank " . $bank . " Tidak Lengkap",
                "created_at" => date("Y-m-d H:i:s")
            ));
        } else {
            $payment_ntb_exist = $detilRekening[0]->last_payment_ntb;
            $invId = $detilRekening[0]->investor_id;
        }
        $rekening = RekeningInvestor::where('investor_id', $invId)->first();

        // syslog(0,"comparing ".$payment_ntb." with ".$payment_ntb_exist);

        if ($payment_ntb != $payment_ntb_exist) {
            $rekening->total_dana += $amount;
            $rekening->unallocated += $amount;
            // $rekening->payment_ntb = $payment_ntb;
            $rekening->save();

            DB::table('detil_rekening_investor')
                ->where('va_number', $va_investor)
                ->update(['last_amount' => $amount, 'last_payment_ntb' => $payment_ntb]);
        } else {
            $insertLog = DB::table("log_db_app")->insert(array(
                "file_name" => "RekeningController.php",
                "line" => __LINE__,
                "description" => "Flagging failed : Duplicate NTB - VA " . $va_investor . " Bank " . $bank . " " . $nama_rekening . " IDR " . $amount,
                "created_at" => date("Y-m-d H:i:s")
            ));
            return response()->json([
                'TransactionID' => $transactionID,
                'ChannelID' =>  $channelid,
                'TerminalID' => $terminalID,
                'TransactionDate' => $request2['TransactionDate'],
                'CompanyCode' =>  $request2['CompanyCode'],
                'CustomerKey1' => $request2['CustomerKey1'],
                'CustomerName' => $request2['CustomerName'],
                'CustomerKey2' => $request2['CustomerKey2'],
                'CustomerKey3' => $request2['CustomerKey3'],
                'Currency' => $request2['Currency'],
                'Amount' => $request2['Amount'],
                'Fee' => $request2['Fee'],
                'PaidAmount' => $request2['PaidAmount'],
                'AdditionalData1' => $request2['AdditionalData1'],
                'AdditionalData2' => $request2['AdditionalData2'],
                'AdditionalData3' => $request2['AdditionalData3'],
                'AdditionalData4' => $request2['AdditionalData4'],
                'PaymentFlag' => '100000',
                'ReferenceNumberTransaction' => $ref_number_trx,
                'ResponseCode' => '06',
                'ResponseDescription' => 'Payment NTB double'
            ]);
        }



        /* call event that will log the cash flow */
        event(new MutasiInvestorEvent($rekening->investor->id, 'CREDIT', $amount, 'Transfer Rekening', null, $bank, 'V', $va_investor, $ref_number_trx));

        $insertLog = DB::table("log_db_app")->insert(array(
            "file_name" => "RekeningController.php",
            "line" => __LINE__,
            "description" => "Flagging success : VA " . $va_investor . " Bank " . $bank . " " . $nama_rekening . " IDR " . $amount,
            "created_at" => date("Y-m-d H:i:s")
        ));

        return response()->json([

            'TransactionID' => $transactionID,
            'ChannelID' =>  $channelid,
            'TerminalID' => $terminalID,
            'TransactionDate' => $request2['TransactionDate'],
            'CompanyCode' =>  $request2['CompanyCode'],
            'CustomerKey1' => $request2['CustomerKey1'],
            'CustomerName' => $request2['CustomerName'],
            'CustomerKey2' => $request2['CustomerKey2'],
            'CustomerKey3' => $request2['CustomerKey3'],
            'Currency' => $request2['Currency'],
            'Amount' => $request2['Amount'],
            'Fee' => $request2['Fee'],
            'PaidAmount' => $request2['PaidAmount'],
            'AdditionalData1' => $request2['AdditionalData1'],
            'AdditionalData2' => $request2['AdditionalData2'],
            'AdditionalData3' => $request2['AdditionalData3'],
            'AdditionalData4' => $request2['AdditionalData4'],
            'PaymentFlag' => '100000',
            'ReferenceNumberTransaction' => $ref_number_trx,
            'ResponseCode' => '00',
            'ResponseDescription' => 'Success'

        ]);
    }

    public function bankResponse451_payment(Request $request)
    {
        $data = $request->input('data');

        $decrypted = BniEnc::decrypt($data, config('app.bsi_id'), config('app.bsi_key'));
        $va_investor = $decrypted['virtual_account'];
        $payment_ntb = $decrypted['payment_ntb'];

        if ($request->input('client_id') != config('app.bsi_id')) {
            $insertLogCallback = DB::table("log_db_app")->insert(
                array(
                    "file_name" => __FILE__,
                    "line" => __LINE__,
                    "description" => 'Credential BSI tidak sesuai ' . $va_investor . '', "created_at" => date('Y-m-d H:i:s')
                )
            );
            return response()->json([
                'status' => '999',
                'message' => 'Access Denied',
            ]);
        }

        $amount = (int)$decrypted['payment_amount'];
        $vanumber = config('app.bsi_id') . $va_investor;

        $getDetilrekening = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE va_number = " . $vanumber . " AND kode_bank = " . config('app.bsi_code_bank') . "");
        if ($getDetilrekening == NULL) {
            DB::table("log_db_app")->insert(array(
                "file_name" => __FILE__,
                "line" => __LINE__,
                "description" => 'Va Number BSI Tidak Terdaftar' . $vanumber,
                "created_at" => date('Y-m-d H:i:s')
            ));
            return response()->json([
                'status' => '888',
                'message' => 'VA tidak Terdaftar',
            ]);
        }

        $user_id = $getDetilrekening[0]->investor_id;

        $run = DB::SELECT("CALL proc_update_rek_investor($user_id,$vanumber,$amount,$payment_ntb,'451','" . __FILE__ . "','" . __LINE__ . "')");
        if ($run[0]->sout != '1') {
            return response()->json([
                'status' => '888',
                'message' => 'Payment NTB double',
            ]);
        }

        // call event that will log the cash flow
        event(new MutasiInvestorEvent($user_id, 'CREDIT', $amount, 'Transfer Rekening', null, config('app.bsi_code_bank'), 'V', $vanumber, $payment_ntb));

        return response()->json([
            'status' => '000'
        ]);
    }

    public function generateVAtest(Request $request)
    {
        // $date = \Carbon\Carbon::now()->addYear(4);
        $user = Auth::user();
        $data_user = Investor::where('id', $request->invid)->first();
        // $cekRekening = RekeningInvestor::where('investor_id', $user->id)->first();



        event(new GenerateVABankEvent($data_user));
        // if ($cekRekening == NULL) {
        //     RekeningInvestor::create([
        //         'investor_id' => $user->id,
        //         'jumlah_dana' => 0,
        //         'unallocated' => 0,
        //     ]);
        // } else {
        //     DB::table("log_db_app")->insert(array(
        //         "file_name" => __FILE__,
        //         "line" => __LINE__,
        //         "description" => "Gagal Insert Lender Baru pada Rekeninginvestor '" . $user->id . "' sudah terdaftar sebelumnya",
        //         "created_at" => date("Y-m-d H:i:s")
        //     ));
        // }

        dd($data_user);


        // $va_investor = '011122554433';
        // $getrekening = RekeningInvestor::where('va_number', 'LIKE', '%' . $va_investor . '%')->first();
        // if ($getrekening == NULL) {
        //     $phone = str_replace('01', '', $va_investor);
        //     $getinvId = DB::SELECT("SELECT phone_investor, investor_id FROM detil_investor WHERE phone_investor LIKE '%" . $phone . "%'");
        //     if (isset($getinvId)) {
        //         $invId = $getinvId[0]->investor_id;
        //         $rekening = RekeningInvestor::where('investor_id', $invId)->first();

        //         // dd($invId);

        //         DB::table('rekening_investor')
        //             ->where('investor_id', $invId)
        //             ->update(['va_number' => '8757' . $va_investor]);
        //     }
        //     echo 1;
        // } else {
        //     $rekening = RekeningInvestor::where('va_number', 'LIKE', '%' . $va_investor . '%')->first();
        //     echo 2;
        // }
        // dd($rekening);

        // $rekening = DB::SELECT("SELECT * FROM detil_rekening_investor WHERE va_number = " . $va_investor . " AND kode_bank = " . config('app.cimbs_id') . "");
    }

    public function bankResponseTest($user_id, $va_investor, $payment_amount, $payment_ntb)
    {

        if (0 != $user_id) {
            $rekening = RekeningInvestor::where('investor_id', $user_id)->first();
            $vanumber = $rekening['va_number'];

            $run = DB::SELECT("CALL proc_update_rek_investor($user_id,$vanumber,$payment_amount,$payment_ntb,'427','" . __FILE__ . "','" . __LINE__ . "')");
            // dd($run);
            if ($run[0]->sout != '1') {
                return response()->json([
                    'status' => '888',
                    'message' => 'Payment NTB double',
                ]);
            }
        } else {

            $rekening = RekeningInvestor::where('va_number', $va_investor)->first();
            $vanumber = $rekening['va_number'];
            $user_id = $rekening['investor_id'];

            $run = DB::SELECT("CALL proc_update_rek_investor($user_id,$vanumber,$payment_amount,$payment_ntb,'427','" . __FILE__ . "','" . __LINE__ . "')");
            if ($run[0]->sout != '1') {
                return response()->json([
                    'status' => '888',
                    'message' => 'Payment NTB double',
                ]);
            }
        }
        // call event that will log the cash flow
        event(new MutasiInvestorEvent($rekening->investor->id, 'CREDIT', $payment_amount, 'Transfer Rekening'));

        return response()->json([
            'status' => '000'
        ]);
    }
}
