<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use DateTime;
use ZipArchive;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use DB;
use App\Proyek;
use Illuminate\Http\File;
use App\FDCPassword;
use League\Flysystem\Filesystem;
use League\Flysystem\Sftp\SftpAdapter;
use App\ListFileFDC;

class FDCController extends Controller
{
    
    
    public function upload_data() {

        $message        = array();
        $tglkemarin     = date('Y-m-d', strtotime( '-1 days' ));
        $GetData        = DB::table('list_file_fdc')  
        ->where('tanggal_file', '=',$tglkemarin) 
        ->where('status_pengiriman', '=','0')
        ->where('status_file', '=','0')
        ->first();

        if($GetData){
            $exists      = Storage::disk('fdc')->exists($GetData->filename);
            

            $message['keterangan'] = 'Proses Upload Berhasil';
            $message['status'] = "Sukses";

            
            
            $fileZip_path = Storage::disk('fdc')->get($GetData->filename);
            Storage::disk('sftp')->put($GetData->filename,$fileZip_path);
 
 
            $updateStatusFile = DB::table('list_file_fdc')
              ->where('id_fdc', $GetData->id_fdc)
              ->update(['status_pengiriman' => 1, 'status_file' => 1, 'tanggal_upload' => date('Y-m-d')]);

            $insertLog = DB::table('log_fdc')->insert(
            [
                
                'id_fdc' => $GetData->id_fdc == null ? null : $GetData->id_fdc, 'filename' => $GetData->filename == null ? null : $GetData->filename, 
                'version' => $GetData->version == null ? null : $GetData->version, 'tanggal_file' => $GetData->tanggal_file == null ? null : $GetData->tanggal_file, 
                'tanggal_upload' => date('Y-m-d H:i:s'),'status_kirim' => 1, 'status_file' => 1, 'Keterangan' => $message['keterangan']
            ]);
        

        }else{

            $message['keterangan'] = 'File tidak ditemukan, Silahkan dicoba kembali';
            $message['status'] = "Gagal";

            $insertLog = DB::table('log_fdc')->insert(
            [
                'id_fdc' => "", 'filename' => "-", 
                'version' => "", 'tanggal_file' => "", 
                'tanggal_upload' => date('Y-m-d H:i:s'), 'status_kirim' => 1,'status_file' => 1, 'Keterangan' => $message['keterangan']
            ]);

        }

        return json_encode($message);

        
    }

    // fungsi untuk upload manual by admin
    public function upload_data_manual_ke_afpi(Request $request) {
        
        //die($req->text_filename_afpi);
        
        $message = array();
        $fileZip_new = Storage::disk('fdc')->get($request->text_filename_afpi);
        
        
        if($fileZip_new){
            
            Storage::disk('sftp')->put($request->text_filename_afpi,$fileZip_new);

            $updateStatus = DB::table('list_file_fdc')
            ->where('filename', $request->text_filename_afpi)
            ->update(['status_pengiriman' => 1, 'status_file' => 1, 'tanggal_upload' => date('Y-m-d')]);

            $selectFile = DB::table('list_file_fdc')
            ->where('filename', $request->text_filename_afpi)->first();

            $message['status'] = "sukses_upload_afpi";
            $message['keterangan'] = 'Proses Upload File ke AFPI Berhasil';

            
            
            $insertLog = DB::table('log_fdc')->insert(
            [
                
                'id_fdc' => $selectFile->id_fdc == null ? null : $selectFile->id_fdc, 'filename' => $selectFile->filename == null ? null : $selectFile->filename, 
                'version' => $selectFile->version == null ? null : $selectFile->version, 'tanggal_file' => $selectFile->tanggal_file == null ? null : $selectFile->tanggal_file, 
                'tanggal_upload' => date('Y-m-d H:i:s'), 'status_kirim' => 0, 'Keterangan' => $message['keterangan']
            ]);
            
            return redirect('/admin/view_upload_fdc')->with($message);
            
        }else{

            $message['status'] = "Gagal";
            $message['keterangan'] = 'File tidak ditemukan, Silahkan dicoba kembali';

            $insertLog = DB::table('log_fdc')->insert(
                [
                    'id_fdc' => "", 'filename' => $request->text_filename_afpi, 
                    'version' => "", 'tanggal_file' => "", 
                    'tanggal_upload' => date('Y-m-d H:i:s'), 'status_kirim' => 0, 'Keterangan' => $message['keterangan']
                    
                ]);
        }

        return redirect('/admin/view_upload_fdc')->with($message);
        
            
            
    }
    
    
    
    public function download_file_response(){

       $fileZip_new = Storage::disk('fdc')->get("81005920200612SIK01.zip.out");
        
        // if($fileZip_new){
            
       $file =  Storage::disk('sftp')->put("81005920200612SIK01.zip.out","in/"."81005920200612SIK01.zip.out");
        // $file = Storage::disk('sftp')->get(storage_path('app/public/fdc/'), "out/81005920200612SIK01.zip.out");

        if($file){
            die("sukses");
        }else{
            die("gagal");
        }
        

    }


    
    // consume inquiry fdc
    public function inquiry_fdc(Request $req){
     //public function inquiry_fdc(){
	
         //$id = "843261462421000";
         //$reason = "1";
        //$reffid = "1";
	
        $id = $req->txt_id;
        $reason = $req->txt_reason;
        $reffid = $req->txt_reffid;
        
        $date    = date('dmY');
        $client = new Client();
        $credentials = base64_encode(config('app.sftp_account_username').':'.config('app.sftp_account_password'));
	//$credentials = base64_encode('taufiqaljufri@danasyariah.id'.':'.'NSkjasxmLO7HV69hhsd66');
	
        $res = $client->request('GET', 'https://pusdafil.afpi.or.id'.'/api/v3.5/Inquiry', [
            'headers' => [
               'Content-Type'    => 'application/json',
               'Authorization' => 'Basic '.$credentials,
            ],
            'query' => 
            [
                "id"        => $id, 
                "reason"    => $reason, 
                "reffid"    => $reffid
            ],
            'http_errors' => false
        ]);
        
        $response = $res->getBody();
        $response_decode = json_decode($response);

        
        return $response;
        //return redirect('/admin/view_upload_fdc')->with($response_decode);
        
    }
    
    // consume password fdc
    public function passwordZIP(){
        
        $zipPass = $this->GeneratepasswordZIP();
        
        $client = new Client();
        $credentials = base64_encode(config('app.sftp_account_username').':'.config('app.sftp_account_password'));
        
        $request       = $client->post(config('app.sftp_host').'/api/ZipPassword',[
            'headers' => [
               'Content-Type'    => 'application/json',
               'Authorization' => 'Basic '.$credentials,
            ],
            'body' => json_encode(['zipPwd' => $zipPass]),
            //'http_errors' => false
            
        ]);
        
        $response = $request->getBody()->getContents();
        return $response;
        
    }
    
    public function ganti_password($password){
        
        $updatePassword = FDCPassword::where('id', 2)->update(['password' => $password]);
        $message = array();
        if($updatePassword){

            $message['status'] = "Sukses";
            
        }else{

            $message['status'] = "Gagal";
            
        }

        return json_encode($message);

    }
    
    // generate password zip
    public function GeneratepasswordZIP() {
        
        $getMonth = date('m'); // get bulan
        
        // select password in table
        $passwordZip = DB::table('fdc_zip_password')
        ->orderBy('tanggal', 'asc')
        ->first();
        
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        
        $password = implode($pass); // set password
        
        $message = array();
        
        if($passwordZip){
            
            $getMonthPass = explode('-',$passwordZip->tanggal); // get bulan
            // jika masi dbulan yang sama
            if($getMonth == $getMonthPass[1]){
                
                $message['password'] = $passwordZip->password;
                $message['status'] = "Sukses";
                
                // return $message; //turn the array into a string
                return $passwordZip->password;
                
            }else{
                
                // jika sudah ganti bulan password akan di update
                $updatePassword = FDCPassword::where('id',$passwordZip->id)->update(['tanggal' => date('Y-m-d'), 'password' => $password]);
                $passwordZip = DB::table('fdc_zip_password')
                ->orderBy('tanggal', 'asc')
                ->first();

                $message['password'] = $passwordZip;
                $message['status'] = "Update Password Sukses";
                
                // return $message;
                return $passwordZip->password;
                
            }
        
        }else{

            // jika belum ada datanya
            $fdc_passsword = new \App\FDCPassword();
            $fdc_passsword->tanggal     = date('Y-m-d');
            $fdc_passsword->password    = $password;
            $fdc_passsword->save();
            
            $message['password'] = $fdc_passsword->password;
            $message['status'] = "Sukses";
            
            // return $message; //turn the array into a string
            return $fdc_passsword->password;

        }
        
    }

   
}
