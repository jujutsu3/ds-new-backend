<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Admins;
use App\ManageRole;
use App\AuditTrail;

use Illuminate\Http\Request;

use App\Http\Middleware\UserCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use function GuzzleHttp\json_encode;
use Cart;
use Auth;
use App;
use App\BorrowerPengajuan;
use DB;

use Mail;


class ResumePendanaan extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->only(['listproyek', 'compliance_resumependanaan']);
    }

    public function ListPengajuanCompliance()
    {
        // Resume
        $query = \DB::table('brw_analisa_pendanaan');
        $query->leftJoin('m_tujuan_pembiayaan','m_tujuan_pembiayaan.id','brw_analisa_pendanaan.tujuan_pendanaan');
        $query->leftJoin('brw_tipe_pendanaan','brw_tipe_pendanaan.tipe_id','brw_analisa_pendanaan.jenis_pendanaan');
        $query->leftJoin('m_status_analis','m_status_analis.id_status_analis','brw_analisa_pendanaan.status_compliance');
		$query->where('m_status_analis.id_vendor', 11);
		//$query->where('brw_analisa_pendanaan.status_komite', '==',0);
        $query->OrderBy('brw_analisa_pendanaan.status_compliance','asc');
        $query->OrderBy('brw_analisa_pendanaan.tanggal_verifikasi', 'asc');
        $query->Select([
            'pengajuan_id',
            DB::raw('DATE_FORMAT(tanggal_pengajuan,"%d-%m-%Y") as tanggal_pengajuan'),
            DB::raw('DATE_FORMAT(tanggal_verifikasi,"%d-%m-%Y") as tanggal_verifikasi'),
            'penerima_pendanaan',
            'jenis_pendanaan',
            'tujuan_pembiayaan',
            'nilai_pengajuan_pendanaan',
            'status_compliance',
            'keterangan_status'
        ]);
        $pengajuan = $query->get();

        $response = ['data' => $pengajuan];
        return response()->json($response);
    }

    public function detilPengajuan($idPengajuan)
	{
        //Pengajuan 
		$query = \DB::table('brw_pengajuan');
		$query->leftJoin('brw_user_detail_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_user_detail_pengajuan.pengajuan_id');
		$query->leftJoin('brw_pasangan', 'brw_pasangan.pasangan_id', 'brw_pengajuan.brw_id');
		$query->leftJoin('brw_rekening', 'brw_rekening.brw_id', 'brw_pengajuan.brw_id');
		$query->leftJoin('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
		$query->leftJoin('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
		$query->leftJoin('brw_user_detail_penghasilan_pengajuan', 'brw_user_detail_penghasilan_pengajuan.pengajuan_id', 'brw_pengajuan.pengajuan_id');
		$query->leftJoin('brw_dtl_lembar_pendapatan', 'brw_dtl_lembar_pendapatan.id_pengajuan', 'brw_pengajuan.pengajuan_id');
		$query->leftJoin('brw_agunan', 'brw_agunan.id_pengajuan', 'brw_pengajuan.pengajuan_id');
		$query->select([
            'brw_pengajuan.pengajuan_id',
            'brw_pengajuan.brw_id',
            'brw_user_detail_pengajuan.nama as nama',
            'brw_user_detail_pengajuan.tgl_lahir',
            'brw_user_detail_penghasilan_pengajuan.status_pekerjaan',
            'brw_user_detail_penghasilan_pengajuan.jabatan',
            'brw_user_detail_penghasilan_pengajuan.masa_kerja_tahun',
            'brw_user_detail_penghasilan_pengajuan.usia_perusahaan',
            'brw_user_detail_penghasilan_pengajuan.usia_tempat_usaha',
            'brw_pasangan.nama as nama_pasangan',
            'brw_pasangan.tgl_lahir as tgl_lahir_pasangan',
            'brw_pasangan.status_pekerjaan as status_pekerjaan_pasangan',
            'brw_pasangan.jabatan as jabatan_pasangan',
            'brw_pasangan.nama_perusahaan',
            'brw_dtl_lembar_pendapatan.cr_maks',
            'brw_agunan.maks_ftv_persentase',
            'brw_agunan.maks_ftv_nominal',
            'brw_pengajuan.nm_pemilik',
            'brw_pengajuan.pendanaan_dana_dibutuhkan',
            'brw_pengajuan.pendanaan_tipe',
            'brw_pengajuan.durasi_proyek',
            'brw_tipe_pendanaan.pendanaan_nama',
            'm_tujuan_pembiayaan.tujuan_pembiayaan',
            'brw_user_detail_penghasilan_pengajuan.id',
            'brw_user_detail_penghasilan_pengajuan.sumber_pengembalian_dana',
            'brw_user_detail_penghasilan_pengajuan.skema_pembiayaan',
            'brw_tipe_pendanaan.tipe_id',
            'brw_pengajuan.status']);
		$query->where('brw_pengajuan.pengajuan_id', $idPengajuan);
		$query->whereIn('brw_pengajuan.pendanaan_tipe', [1, 2, 3]);
        $pengajuan = $query->get();
/*
        $queryresume = \DB::table('brw_resume_pendanaan');
        $resume = $queryresume->get();*/
        $response = ['data' => $pengajuan];
        return response()->json($response);
	}

    public function listproyek()
    {
        
        //$idp =$idPengajuan;
        $admins = Admins::all();
        $role = ManageRole::all();
        $cekUser = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')->where('admins.id', \Auth::id())->first(['roles.name', 'admins.id']);

        $qstatus = \DB::table('m_status_analis');
		$qstatus->where('m_status_analis.id_vendor', 11);
        $status_analis = $qstatus->get();
        return view('pages.admin.adrCompliance_listproyek')->with(['cekUser' => $cekUser,'status_analis' => $status_analis]);
    }

    public function naups($idPengajuan='')
    {
        $Qnaup = \DB::table('brw_dtl_naup');
        $Qnaup ->where('brw_dtl_naup.pengajuan_id', $idPengajuan);
        $naup = $Qnaup->get();
        $response = ['data' => $naup];
        return response()->json($response); 
    }

    public function resume($idPengajuan='')
    {
        // Resume
        $queryresume = \DB::table('brw_resume_pendanaan');
        $queryresume ->where('brw_resume_pendanaan.pengajuan_id', $idPengajuan); 
        $resume = $queryresume->get();

        $response = ['data' => $resume];
        return response()->json($response); 
    }

    public function insertUpdateResume(Request $request)
    {
        // return $request->all();
        $resume = DB::table('brw_resume_pendanaan')->where('pengajuan_id', $request->pengajuan_id);
        $role           = $request->role;
        $user_login     = $request->nama_user;
        $pengajuan_id   = $request->pengajuan_id;

        if($resume->first() == null){
            $resume->insert([
                'pengajuan_id' => $request->pengajuan_id,
                'nama'.$role => $request->nama_user,
                'rekomendasi'.$role => $request->rekomendasi,
                'catatan'.$role => $request->catatan,
                'tgl_keputusan'.$role => $request->tanggal_keputusan,
                'created_at'.$role => $request->tanggal_buat,
                'updated_at'.$role => date("Y-m-d H:i:s")
            ]);
        } else {
                $resume->update([
                    'nama'.$role => $request->nama_user,
                    'rekomendasi'.$role => $request->rekomendasi,
                    'catatan'.$role => $request->catatan,
                    'tgl_keputusan'.$role => $request->tanggal_keputusan,
                    'created_at'.$role => $request->tanggal_buat,
                    'updated_at'.$role => date("Y-m-d H:i:s")
                ]);
        };

        $Simpan = '';
        if ($role == '_risk_management'){
            $Simpan = 'SIMPAN_RISK';
        }else if ($role == '_kredit_analis'){
            $Simpan = 'SIMPAN_ANALIS';
        }else if ($role == '_legal'){
            $Simpan = 'SIMPAN_LEGAL';
        }else if($role == '_compliance'){
            $Simpan = 'SIMPAN_COMPLIANCE';
        }
        // dd($simpan);
            $update_status = DB::select(
                "CALL proc_click_button_borrower(
                    28, /* form_id int */
                    '$request->nama_user',
                    $request->pengajuan_id, /* pengajuan_id int */
                    '$Simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                    'ResumePendanaan.php', /* file varchar(100) */
                    166 /* line int */
               )"
           );
        
//        $checking = DB::table('brw_resume_pendanaan')->where('pengajuan_id', $request->pengajuan_id);
//        $checking ->whereNotNull('nama_kredit_analis');
//        $checking ->whereNotNull('nama_compliance');
//        $checking ->whereNotNull('nama_risk_management');
//        $checking ->whereNotNull('nama_legal');
//        if(!$checking->first() == null){

//            $update_status = DB::select(
//                "CALL proc_click_button_borrower(
//                    28, /* form_id int */
//                    '$user_login',
//                    $pengajuan_id, /* pengajuan_id int */
//                    'KIRIM', /* method varchar(10) ['simpan' , 'kirim'] */
//                    'ResumePendanaan.php', /* file varchar(100) */
//                    296 /* line int */
//                )"
//            );
            //dd ($update_status);
//            return 1;
//        }else{
            $audit = new AuditTrail;
            $audit->fullname = $request->nama_user;
            $audit->menu = "Resume Pendanaan";
            $audit->description = $request->nama_user." on pengajuan ".$request->pengajuan_id ;
            $audit->ip_address =  \Request::ip();
            $audit->save();
//             dd(2);
            return 1;
//        }

    }

    public function compliance_resumependanaan($idPengajuan='')
    {
        //$idp =$idPengajuan;
        $admins = Admins::all();
        $role = ManageRole::all();
        $cekUser = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')->where('admins.id', \Auth::id())->first(['roles.name', 'admins.id']);

        //Pengajuan 
		$query = \DB::table('brw_pengajuan');
		$query->leftJoin('brw_user_detail_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_user_detail_pengajuan.pengajuan_id');
		$query->leftJoin('brw_pasangan', 'brw_pasangan.pasangan_id', 'brw_pengajuan.brw_id');
		$query->leftJoin('brw_rekening', 'brw_rekening.brw_id', 'brw_pengajuan.brw_id');
		$query->leftJoin('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
		$query->leftJoin('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
		$query->leftJoin('brw_user_detail_penghasilan_pengajuan', 'brw_user_detail_penghasilan_pengajuan.pengajuan_id', 'brw_pengajuan.pengajuan_id');
		$query->leftJoin('brw_dtl_lembar_pendapatan', 'brw_dtl_lembar_pendapatan.id_pengajuan', 'brw_pengajuan.pengajuan_id');
		$query->leftJoin('brw_agunan', 'brw_agunan.id_pengajuan', 'brw_pengajuan.pengajuan_id');
		$query->select([
            'brw_pengajuan.pengajuan_id',
            'brw_pengajuan.brw_id',
            'brw_user_detail_pengajuan.nama as nama',
            'brw_user_detail_pengajuan.tgl_lahir',
            'brw_user_detail_penghasilan_pengajuan.status_pekerjaan',
            'brw_user_detail_penghasilan_pengajuan.jabatan',
            'brw_user_detail_penghasilan_pengajuan.masa_kerja_tahun',
            'brw_user_detail_penghasilan_pengajuan.masa_kerja_bulan',
            'brw_user_detail_penghasilan_pengajuan.usia_perusahaan',
            'brw_user_detail_penghasilan_pengajuan.usia_tempat_usaha',
            'brw_pasangan.nama as nama_pasangan',
            'brw_pasangan.tgl_lahir as tgl_lahir_pasangan',
            'brw_pasangan.status_pekerjaan as status_pekerjaan_pasangan',
            'brw_pasangan.jabatan as jabatan_pasangan',
            'brw_pasangan.nama_perusahaan',
            'brw_dtl_lembar_pendapatan.cr_maks as cr_maks',
            'brw_agunan.maks_ftv_persentase',
            'brw_agunan.maks_ftv_nominal',
            'brw_pengajuan.nm_pemilik',
            'brw_pengajuan.pendanaan_dana_dibutuhkan',
            'brw_pengajuan.pendanaan_tipe',
            'brw_pengajuan.durasi_proyek',
            'brw_tipe_pendanaan.pendanaan_nama',
            'm_tujuan_pembiayaan.tujuan_pembiayaan',
            'brw_user_detail_penghasilan_pengajuan.id',
            'brw_user_detail_penghasilan_pengajuan.sumber_pengembalian_dana',
            'brw_user_detail_penghasilan_pengajuan.skema_pembiayaan',
            'brw_tipe_pendanaan.tipe_id',
            'brw_pengajuan.status']);
		$query->where('brw_pengajuan.pengajuan_id', $idPengajuan);
		$query->whereIn('brw_pengajuan.pendanaan_tipe', [1, 2, 3]);
        $pengajuan = $query->get();
/*
		if (!empty($filter_search)) {
			$query->whereRaw(" (brw_pengajuan.pengajuan_id = ? or brw_tipe_pendanaan.pendanaan_nama like ? or m_tujuan_pembiayaan.tujuan_pembiayaan like ?) ", [$filter_search, '%' . $filter_search . '%', '%' . $filter_search . '%']);
		}
*/
        $Qanalisa_pendanaan = \DB::table('brw_analisa_pendanaan')
        ->where('brw_analisa_pendanaan.pengajuan_id',$idPengajuan)
        ->first();
        $data['status_analis'] =$Qanalisa_pendanaan->status_analis?$Qanalisa_pendanaan->status_analis: 0;
        $data['status_legal'] =$Qanalisa_pendanaan->status_legal?$Qanalisa_pendanaan->status_legal: 0;
        $data['status_risk'] =$Qanalisa_pendanaan->status_risk?$Qanalisa_pendanaan->status_risk: 0;
        
        $data['status_pendanaan'] =$Qanalisa_pendanaan->status_compliance?$Qanalisa_pendanaan->status_compliance: 0;

        $Qnaup = \DB::table('brw_dtl_naup')
		->leftJoin('m_pengikatan_pendanaan AS mpp_wakalah', 'mpp_wakalah.id', 'brw_dtl_naup.pengikatan_pendanaan_wakalah')
		->leftJoin('m_pengikatan_jaminan AS mpj_wakalah', 'mpj_wakalah.id', 'brw_dtl_naup.pengikatan_jaminan_wakalah')
        ->where('brw_dtl_naup.pengajuan_id', $idPengajuan)
        /*->select([
            'brw_dtl_naup.*',
            'mpp_wakalah.jenis_pengikatan AS pengikatan_pendanaan',
            'mpj_wakalah.jenis_pengikatan AS pengikatan_jaminan',
        ])*/;
        $naup = $Qnaup->get();

        $Qpengikat = \DB::table('m_pengikatan_pendanaan');
        $pengikat  = $Qpengikat->get();

        $Qagunan = \DB::table('brw_agunan');
        $Qagunan ->leftJoin('brw_dtl_naup','brw_dtl_naup.pengajuan_id','brw_agunan.id_pengajuan');
		$Qagunan->leftJoin('m_pengikatan_jaminan AS mpj', 'mpj.id', 'brw_dtl_naup.jenis_pengikatan_agunan');
        $Qagunan->leftJoin('brw_pengajuan AS bp', 'bp.pengajuan_id', 'brw_agunan.id_pengajuan');
        $Qagunan ->where('brw_agunan.id_pengajuan', $idPengajuan);
        $agunan = $Qagunan->get();

        $Qbiaya = \DB::table('brw_dtl_biaya');
        $Qbiaya ->where('brw_dtl_biaya.pengajuan_id', $idPengajuan);
        $biaya = $Qbiaya->get();

        // Resume
        $queryresume = \DB::table('brw_resume_pendanaan')
        ->where('brw_resume_pendanaan.pengajuan_id', $idPengajuan) 
        ->first();
        if ($cekUser->name == 'Analis'){
            $data['nama_kredit_analis']             = !empty($queryresume->nama_kredit_analis) ? $queryresume->nama_kredit_analis : auth::user()->firstname.' '.auth::user()->lastname;
            $data['nama_compliance']                = !empty($queryresume->nama_compliance) ? $queryresume->nama_compliance : '';
            $data['nama_risk_management']           = !empty($queryresume->nama_risk_management) ? $queryresume->nama_risk_management : '';
            $data['nama_legal']                     = !empty($queryresume->nama_legal) ? $queryresume->nama_legal : '';
        }elseif ($cekUser->name == 'Compliance'){
            $data['nama_kredit_analis']             = !empty($queryresume->nama_kredit_analis) ? $queryresume->nama_kredit_analis : '';
            $data['nama_compliance']                = !empty($queryresume->nama_compliance) ? $queryresume->nama_compliance : auth::user()->firstname.' '.auth::user()->lastname;
            $data['nama_risk_management']           = !empty($queryresume->nama_risk_management) ? $queryresume->nama_risk_management : '';
            $data['nama_legal']                     = !empty($queryresume->nama_legal) ? $queryresume->nama_legal : '';
        }elseif ($cekUser->name == 'Risk'){      
            $data['nama_kredit_analis']             = !empty($queryresume->nama_kredit_analis) ? $queryresume->nama_kredit_analis : '';
            $data['nama_compliance']                = !empty($queryresume->nama_compliance) ? $queryresume->nama_compliance : '';
            $data['nama_risk_management']           = !empty($queryresume->nama_risk_management) ? $queryresume->nama_risk_management : auth::user()->firstname.' '.auth::user()->lastname; 
            $data['nama_legal']                     = !empty($queryresume->nama_legal) ? $queryresume->nama_legal : '';     
        }elseif ($cekUser->name == 'Legal'){      
            $data['nama_kredit_analis']             = !empty($queryresume->nama_kredit_analis) ? $queryresume->nama_kredit_analis : '';
            $data['nama_compliance']                = !empty($queryresume->nama_compliance) ? $queryresume->nama_compliance : '';
            $data['nama_risk_management']           = !empty($queryresume->nama_risk_management) ? $queryresume->nama_risk_management : ''; 
            $data['nama_legal']                     = !empty($queryresume->nama_legal) ? $queryresume->nama_legal :  auth::user()->firstname.' '.auth::user()->lastname;     
        }else{      
            $data['nama_kredit_analis']             = !empty($queryresume->nama_kredit_analis) ? $queryresume->nama_kredit_analis : '';
            $data['nama_compliance']                = !empty($queryresume->nama_compliance) ? $queryresume->nama_compliance : '';
            $data['nama_risk_management']           = !empty($queryresume->nama_risk_management) ? $queryresume->nama_risk_management : ''; 
            $data['nama_legal']                     = !empty($queryresume->nama_legal) ? $queryresume->nama_legal : '';     

        }
                $data['rekomendasi_kredit_analis']      = !empty($queryresume->rekomendasi_kredit_analis) ? $queryresume->rekomendasi_kredit_analis : 0 ;
                $data['catatan_kredit_analis']          = !empty($queryresume->catatan_kredit_analis) ? $queryresume->catatan_kredit_analis : '';
                $data['tgl_keputusan_kredit_analis']    = !empty($queryresume->tgl_keputusan_kredit_analis) ? date('Y-m-d',strtotime($queryresume->tgl_keputusan_kredit_analis)) : date('Y-m-d',strtotime(now()));
                $data['created_at_kredit_analis']       = !empty($queryresume->created_at_kredit_analis) ? date('Y-m-d',strtotime($queryresume->created_at_kredit_analis)) : date('Y-m-d',strtotime(now()));
                $data['updated_at_kredit_analis']       = !empty($queryresume->updated_at_kredit_analis) ? date('Y-m-d',strtotime($queryresume->updated_at_kredit_analis)) : date('Y-m-d',strtotime(now()));

                $data['rekomendasi_compliance']         = !empty($queryresume->rekomendasi_compliance) ? $queryresume->rekomendasi_compliance : 0;
                $data['catatan_compliance']             = !empty($queryresume->catatan_compliance) ? $queryresume->catatan_compliance : '';
                $data['tgl_keputusan_compliance']       = !empty($queryresume->tgl_keputusan_compliance) ? date('Y-m-d',strtotime($queryresume->tgl_keputusan_compliance)) : date('Y-m-d',strtotime(now()));
                $data['created_at_compliance']          = !empty($queryresume->created_at_compliance) ? date('Y-m-d',strtotime($queryresume->created_at_compliance)) : date('Y-m-d',strtotime(now()));
                $data['updated_at_compliance']          = !empty($queryresume->updated_at_compliance) ? date('Y-m-d',strtotime($queryresume->updated_at_compliance)) : date('Y-m-d',strtotime(now()));

                $data['rekomendasi_risk_management']    = !empty($queryresume->rekomendasi_risk_management) ? $queryresume->rekomendasi_risk_management : 0;
                $data['catatan_risk_management']        = !empty($queryresume->catatan_risk_management) ? $queryresume->catatan_risk_management : '';
                $data['tgl_keputusan_risk_management']  = !empty($queryresume->tgl_keputusan_risk_management) ? date('Y-m-d',strtotime($queryresume->tgl_keputusan_risk_management)) : date('Y-m-d',strtotime(now()));
                $data['created_at_risk_management']     = !empty($queryresume->created_at_risk_management) ? date('Y-m-d',strtotime($queryresume->created_at_risk_management)) : date('Y-m-d',strtotime(now()));
                $data['updated_at_risk_management']     = !empty($queryresume->updated_at_risk_management) ? date('Y-m-d',strtotime($queryresume->updated_at_risk_management)) : date('Y-m-d',strtotime(now()));

                $data['rekomendasi_legal']              = !empty($queryresume->rekomendasi_legal) ? $queryresume->rekomendasi_legal : 0;
                $data['catatan_legal']                  = !empty($queryresume->catatan_legal) ? $queryresume->catatan_legal : '';
                $data['tgl_keputusan_legal']            = !empty($queryresume->tgl_keputusan_legal) ? date('Y-m-d',strtotime($queryresume->tgl_keputusan_legal)) : date('Y-m-d',strtotime(now()));
                $data['created_at_legal']               = !empty($queryresume->created_at_legal) ? date('Y-m-d',strtotime($queryresume->created_at_legal)) : date('Y-m-d',strtotime(now()));
                $data['updated_at_legal']               = !empty($queryresume->updated_at_legal) ? date('Y-m-d',strtotime($queryresume->updated_at_legal)) : date('Y-m-d',strtotime(now()));
//            }

    

        return view('pages.admin.adrCompliance_resumependanaan')->with(['data'=> $data,'admins' => $admins, 'role' => $role, 'cekUser' => $cekUser, 'pengajuan' => $pengajuan, 'pengajuan1' => $pengajuan, 'naup' => $naup, 'naup1' => $naup, 'agunan' => $agunan, 'biaya' => $biaya, /*'resume' => $resume,*/ 'pengikat' => $pengikat]);
    }

    public function kirimResume(Request $request)
    {
        // return $request->all();
        $idPengajuan            = $request->idPengajuan;
        $user_login             = $request->user_login;       

        $checking = DB::table('brw_resume_pendanaan')->where('pengajuan_id', $request->idPengajuan);
        $checking ->whereNotNull('nama_kredit_analis');
        $checking ->whereNotNull('nama_compliance');
        $checking ->whereNotNull('nama_risk_management');
        $checking ->whereNotNull('nama_legal');
        if(!$checking->first() == null){
            $update_status = DB::select(
                "CALL proc_click_button_borrower(
                    28, /* form_id int */
                    '$user_login',
                    $idPengajuan, /* pengajuan_id int */
                    'KIRIM', /* method varchar(10) ['simpan' , 'kirim'] */
                    'ResumePendanaan.php', /* file varchar(100) */
                    296 /* line int */
                )"
            );
            //dd ($update_status);
            return 1;
        }else{
            // dd(2);
            return 2;
        }
    }
}
