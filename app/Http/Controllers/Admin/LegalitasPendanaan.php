<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\BorrowerPengajuan;

use App\BorrowerDetailNaup;
use App\BorrowerHdLegalitas;
use App\BorrowerDetailLembarAnalisa;

use Illuminate\Http\Request;
use App\BorrowerDetailBiaya;
use App\Http\Controllers\Controller;
use Auth;

class LegalitasPendanaan extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->only(['listproyek', 'dplegalitas', 'legalnaup', 'legalbiaya', 'analisapendanaan', 'next_page_verif_legal']);
    }

    public function listproyek()
    {
        return view('pages.admin.adrLegal_listproyek');
    }
    public function dplegalitas($pengajuan_id)
    {
        $data_borrower = \DB::table('brw_pengajuan as a')
        ->leftJoin('brw_user_detail_pengajuan as b', 'b.pengajuan_id', 'a.pengajuan_id')
        ->select(['a.brw_id', 'b.nama','b.ktp', 'b.jns_kelamin','b.brw_pic'])
        ->where('a.pengajuan_id', $pengajuan_id)->first();

        $status_legal = \DB::table('brw_analisa_pendanaan')->where('pengajuan_id', $pengajuan_id)->value('status_legal');
        return view('pages.admin.adrLegal_daftarpekerjaan', compact('pengajuan_id', 'status_legal', 'data_borrower'));
    }
    public function legalnaup()
    {
        // return view('pages.admin.kpr_dashboard');
        return view('pages.admin.adrLegal_naup');
    }
    public function legalbiaya()
    {
        // return view('pages.admin.kpr_dashboard');
        return view('pages.admin.adrLegal_biaya');
    }
    public function analisapendanaan($pengajuan_id)
    {
        // return view('pages.admin.kpr_dashboard');
        return view('pages.admin.adrLegal_analisapendanaan', ['pengajuan_id' => $pengajuan_id]);
    }

    public function ListPengajuan()
    {

        $query = \DB::table('brw_analisa_pendanaan');
        $query->join('brw_pengajuan', 'brw_analisa_pendanaan.pengajuan_id', 'brw_pengajuan.pengajuan_id');
        $query->join('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
        $query->leftJoin('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
        $query->selectRaw('brw_pengajuan.brw_id, brw_analisa_pendanaan.pengajuan_id, tanggal_pengajuan AS tgl_pengajuan, tanggal_verifikasi, penerima_pendanaan,  
                            brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan, m_tujuan_pembiayaan.tujuan_pembiayaan,  brw_pengajuan.pendanaan_dana_dibutuhkan AS nilai_pengajuan, status_legal');
        $query->whereIn('brw_analisa_pendanaan.status_legal', [7, 8, 9]);


        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->get();
        $response = ['data' => $pengajuan];
        return response()->json($response);
    }

    public function next_page_verif_legal(
        Request $request,
        BorrowerPengajuan $brw_pengajuan,
        BorrowerHdLegalitas  $brw_hd_legalitas
    ) {

        $page_name = $request->page_name;
        $pengajuan_id = $request->pengajuan_id;

        $data = $brw_pengajuan::from('brw_pengajuan as a')
            ->leftJoin('brw_tipe_pendanaan', 'a.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id')
            ->leftJoin('brw_user_detail_penghasilan_pengajuan as c', 'c.pengajuan_id', 'a.pengajuan_id')
            ->leftJoin('brw_user_detail_pengajuan as d', 'd.pengajuan_id', 'a.pengajuan_id')
            ->leftJoin('brw_hd_legalitas as e', 'e.pengajuan_id', 'a.pengajuan_id')
            ->leftJoin('brw_pasangan as f', 'f.pasangan_id', 'a.brw_id')
            ->select([
                'c.sumber_pengembalian_dana',
                'c.nama_perusahaan',
                'c.nama_hrd',
                'c.no_fixed_line_hrd',
                'c.skema_pembiayaan',
                'c.sumber_pengembalian_dana',
                'c.alamat_perusahaan as c_alamat_perusahaan',
                'c.provinsi as c_provinsi',
                'c.kelurahan as c_kelurahan',
                'c.kecamatan as c_kecamatan',
                'c.kab_kota as c_kab_kota',
                'c.kode_pos as c_kode_pos',
                'c.rt',
                'c.rw',
                'a.*',
                'd.nama',
                'd.bidang_pekerjaan',
                'e.*',
                'f.nama as nama_pasangan',
                'brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan',
            ])
            ->where('a.pengajuan_id', $pengajuan_id)
            ->first();

        $status_legal = \DB::table('brw_analisa_pendanaan')->where('pengajuan_id', $pengajuan_id)->value('status_legal');

        switch ($page_name) {

            case 'form_naup':
                $data_naup = $brw_hd_legalitas::where('pengajuan_id', $pengajuan_id)->first();
                return view('pages.admin.detil_pekerjaan_legal', [
                    'data' => $data,
                    'data_naup' => $data_naup,
                    'status_legal' => $status_legal,
                    'tanggal_legalitas' => (isset($data->created_at_naup)) ? date('d/m/Y', strtotime($data->created_at_naup)) : '-'
                ]);
                break;

            case 'form_biaya':
                $i = 1;

                $data_biaya_list = [
                    'Biaya Administrasi',
                    'Biaya KJPP',
                    'Biaya Notaris',
                    'Biaya Asuransi Kebakaran',
                    'Biaya Asuransi Jiwa',
                    'Biaya Asuransi Pembiayaan',
                    'Biaya Materai',
                    'Biaya Lain-lain'
                ];

                $data_rekanan_list = ['Biaya KJPP' => 'm_kjpp', 'Biaya Notaris' => 'm_notarisppat'];
                foreach ($data_biaya_list as $item) {
                    $column = array();
                    $column['no'] = $i;
                    $column['rincian_biaya'] = (string)$item;

                    $column['value_id'] = '';
                    $column['value_keterangan'] = '';
                    $column['value_jumlah'] = '';
                    $column['value_rekanan'] = '';

                    $get_biaya = BorrowerDetailBiaya::where('pengajuan_id', $pengajuan_id)->where('rincian_biaya', (string)$item)->first();

                    if ($get_biaya) {
                        $column['value_id'] = $get_biaya->id;
                        $column['value_keterangan'] = $get_biaya->keterangan;
                        $column['value_jumlah'] =  number_format($get_biaya->jumlah, 0, '.', '.');
                        $column['value_rekanan'] = $get_biaya->nama_rekanan;
                    }

                    if (in_array($item, ['Biaya KJPP', 'Biaya Notaris'])) {
                        $column['show_nama_rekanan'] = 1;
                        $now = date('Y-m-d');
                        $column['list_rekanan'] = \DB::table($data_rekanan_list[$item])->where('tanggal_berlaku', '<=', $now)->where('tanggal_berakhir', '>=', $now)->pluck('nama', 'id');
                    } else {
                        $column['show_nama_rekanan'] = 0;
                    }

                    $dataArray[] = $column;
                    $i++;
                }

                $data_sub_biaya_list = BorrowerDetailBiaya::where('pengajuan_id', $pengajuan_id)->whereNotIn('rincian_biaya', $data_biaya_list)->get();
                $i = 9;

                $dataSubBiaya = [];
                foreach ($data_sub_biaya_list as $key => $val) {
                    $column = array();
                    $column['no'] = $i;
                    $column['rincian_biaya'] = $val['rincian_biaya'];

                    $get_biaya = BorrowerDetailBiaya::where('pengajuan_id', $pengajuan_id)->where('rincian_biaya', $val['rincian_biaya'])->first();
                    $column['value_id'] = $get_biaya->id;
                    $column['value_keterangan'] = $get_biaya->keterangan;
                    $column['value_jumlah'] = number_format($get_biaya->jumlah, 0, '.', '.');
                    $column['show_nama_rekanan'] = 0;

                    $dataSubBiaya[] = $column;
                    $i++;
                }

                //get data naup
                $data_naup = BorrowerDetailNaup::where('pengajuan_id', $pengajuan_id)->first();
                $nama_rekanan = \DB::table("m_bank")
                    ->pluck("nama_bank", "id_bank");

                return view('pages.admin.detil_pekerjaan_legal', [
                    'data' => $data,
                    'data_biaya' => $dataArray,
                    'data_sub_biaya' => $dataSubBiaya,
                    'row_number_sub_biaya' => $i,
                    'data_naup' => $data_naup,
                    'tanggal_legalitas' =>  isset($data->created_at_biaya2) ? date('d/m/Y', strtotime($data->created_at_biaya2)) : '-',
                    'status_legal' => $status_legal,
                    'nama_rekanan' => $nama_rekanan
                ]);
                break;

            default:
                # code...
                break;
        }
    }

    public static function kalkulatorKpr($nilai_pengajuan, $margin, $tenor)
    {

        $data = \DB::select("SELECT get_new_simulation_kpr($nilai_pengajuan, $tenor, $margin, 'LegalitasPendanaan.php', 212) as result");
        return $data;
    }


    public function ajaxGetTotalAngsuran(Request $request){

        $nilai_pengajuan = $request->plafond_rekomendasi;
        $margin = $request->margin;
        $tenor = $request->tenor;
        $data_kalkulator_kpr = $this->kalkulatorKpr($nilai_pengajuan,  $margin , $tenor);
        $result_simulasi =  $this->newSimulasiAngsuran($data_kalkulator_kpr); 
        
        return response()->json(['total_angsuran'=> $result_simulasi['angsuran']]);
    }


    public function showDetailRekanan(Request $request)
    {
        $returnHTML = '';


        $result = \DB::table($request->table_rekanan)->where('id', $request->id)->first();

        if ($result) {
            $returnHTML .= '<table border=1 cellspacing="2" cellpadding="10" class="table table-responsive font-size-sm">
            <thead class="thead-dark">
                <tr>
                 <th>Nomor /Nama </th>
                 <th>Alamat</th>
                 <th>Tanggal Berlaku</th>
                 <th>No Telp/Email</th>
                 ';

            if ($request->table_rekanan == 'm_kjpp') {
                $returnHTML .= '     
                <th>Fee Tanah Kosong</th>
                <th>Fee Rumah Tinggal</th>
                <th>Fee Ruko</th>
                ';
            }


            $returnHTML .= '</tr>    
           </thead>';

            $returnHTML .= '    
            <tbody>
               <tr>
                <td>' . $result->nomor . ' <br/> ' . $result->nama . ' </td>
                <td>' . $result->alamat . '</td>
                <td>' . date('d-m-Y', strtotime($result->tanggal_berlaku)) . ' s/d ' . date('d-m-Y', strtotime($result->tanggal_berakhir)) . '</td>
                <td>' . $result->no_telp . '/' . $result->email . '</td>';

            if ($request->table_rekanan == 'm_kjpp') {
                $returnHTML .= '    
                    <td>Rp' . number_format($result->fee_tanah_kosong, 0, '.', '.') . '</td>
                    <td>Rp' . number_format($result->fee_rumah_tinggal, 0, '.', '.') . '</td>
                    <td>Rp' . number_format($result->fee_ruko, 0, '.', '.') . '</td>';
            }

            $returnHTML .= '
                </tr>   
            </tbody>
            </table>';
        }

        return $returnHTML;
    }


    public static function newSimulasiAngsuran($data){

        $resultSimulasi = $data[0]->result;
        $dataRow = explode("#", rtrim($resultSimulasi,'#'));

        if($dataRow[0] == '')  return ['angsuran_margin' => '', 'total_margin'=> 0, 'total_angsuran'=> 0, 'angsuran'=> 0]; 
        $totalMargin = 0; 
        $totalAngsuran  = 0;
    
        $returnPerBulan = '';
        $returnTotalMargin = '';
        $returnTotalAngsuran = '';

        $returnPerBulan .= "<thead>
            <tr>
                <th>Bulan ke </th>             
                <th>Angsuran Margin</th>
            </tr>
        </thead>";

        foreach ($dataRow as $key => $val) {
                $dataColumn = explode(";", $val); 

                $returnPerBulan .= "<tr>";
                $returnPerBulan .= "<td align='left'>" . $dataColumn[0] . "</td>";
                $returnPerBulan .= "<td>Rp. " . number_format($dataColumn[4], 0, '.', '.') . "</td>";
                $returnPerBulan .= "</tr>";

                $totalMargin += $dataColumn[4];
                $totalAngsuran = $dataColumn[1];
                
                $end0 = $dataColumn[0] + 1;
                $end3 = $dataColumn[3];
                $end5 = $dataColumn[5];
                $end4 = $end5-$end3;
        }

        $returnTotalMargin .= "<thead><tr><th>Total Margin </th></tr></thead>";
        $returnTotalMargin .= "<tr>";
        $returnTotalMargin .= "<td>Rp. " . number_format($totalMargin, 0, '.', '.') . "</td>";
        $returnTotalMargin .= "</tr>";


        $returnPerBulan .= "<tr>";
        $returnPerBulan .= "<td>" . $end0 . "</td>";
        $returnPerBulan .= "<td>Rp. " . number_format($end4, 0, '.', '.') . "</td>";
        $returnPerBulan .= "</tr>";

        $returnTotalAngsuran .= "<thead><tr><th>Total Angsuran </th></tr></thead>";
        $returnTotalAngsuran .= "<tr>";
        $returnTotalAngsuran .= "<td>Rp. " . number_format($totalAngsuran, 0, '.', '.') . "</td>";
        $returnTotalAngsuran .= "</tr>";
    

        return ['angsuran_margin' => $returnPerBulan, 'total_margin'=> $returnTotalMargin, 'total_angsuran'=> $returnTotalAngsuran, 'angsuran'=> $totalAngsuran];
    }

    // public function simulasiAngsuranMargin($data)
    // {

    //     $resultSimulasi = $data[0]->result;
    //     $returnHTML = '';

    //     if ($resultSimulasi) {
    //         $dataShowResult = [];
    //         $dataRow = explode("#", $resultSimulasi);
    //         $dataLastRow = array_pop($dataRow);
    //         $totalAllBayar = str_replace('^', '', $dataLastRow);

    //         foreach ($dataRow as $key => $val) {
    //             $dataColumn = explode(";", $val);
    //             $dataShowResult[$dataColumn[0]] = array('Angsuran Margin' => $dataColumn[4]);
    //         }

    //         foreach ($dataShowResult as $key1 => $val1) {
    //             $returnHTML .= "  <thead>
    //             <tr>
    //                 <th>Bulan ke </th>
    //                 <th>" . $key1 . "</th>
    //                 </tr>
    //             </thead>";

    //             foreach ($val1 as $key2 => $val2) {
    //                 $returnHTML .= "<tr>";
    //                 $returnHTML .= "<td>" . $key2 . "</td>";
    //                 $returnHTML .= "<td>Rp. " . number_format($val2, 0, '.', '.') . "</td>";
    //                 $returnHTML .= "</tr>";
    //             }
    //         }
    //     }
    //     return $returnHTML;
    // }

    // public function simulasiTotalAngsuran($data)
    // {

    //     $resultSimulasi = $data[0]->result;
    //     $returnHTML = '';

    //     if ($resultSimulasi) {
    //         $dataShowResult = [];
    //         $dataRow = explode("#", $resultSimulasi);
    //         $dataLastRow = array_pop($dataRow);
    //         $totalAllBayar = str_replace('^', '', $dataLastRow);

    //         foreach ($dataRow as $key => $val) {
    //             $dataColumn = explode(";", $val);
    //             $dataShowResult[$dataColumn[0]] = array('Total Angsuran' => $dataColumn[1]);
    //         }

    //         foreach ($dataShowResult as $key1 => $val1) {
    //             $returnHTML .= "  <thead>
    //             <tr>
    //                 <th>Bulan ke </th>
    //                 <th>" . $key1 . "</th>
    //                 </tr>
    //             </thead>";

    //             foreach ($val1 as $key2 => $val2) {
    //                 $returnHTML .= "<tr>";
    //                 $returnHTML .= "<td>" . $key2 . "</td>";
    //                 $returnHTML .= "<td>Rp. " . number_format($val2, 0, '.', '.') . "</td>";
    //                 $returnHTML .= "</tr>";
    //             }
    //         }
    //     }
    //     return $returnHTML;
    // }


    // public function simulasiTotalMargin($data)
    // {

    //     $resultSimulasi = $data[0]->result;
    //     $returnHTML = '';

    //     if ($resultSimulasi) {
    //         $dataShowResult = [];
    //         $dataRow = explode("#", $resultSimulasi);
    //         $dataLastRow = array_pop($dataRow);
    //         $totalAllBayar = str_replace('^', '', $dataLastRow);
    //         $total_margin = 0;

    //         foreach ($dataRow as $key => $val) {
    //             $dataColumn = explode(";", $val);
    //             $dataJangkaWaktu = explode("-", $dataColumn[0]);
    //             $periode =  (int) ($dataJangkaWaktu[1] - $dataJangkaWaktu[0]) + 1;
    //             $total_margin = $dataColumn[4] * $periode;


    //             $dataShowResult[$dataColumn[0]] = array('Angsuran Margin' => $dataColumn[4], 'Total Margin' => $total_margin);
    //         }

    //         foreach ($dataShowResult as $key1 => $val1) {
    //             $returnHTML .= "  <thead>
    //             <tr>
    //                 <th>Bulan ke </th>
    //                 <th>" . $key1 . "</th>
    //                 </tr>
    //             </thead>";

    //             foreach ($val1 as $key2 => $val2) {
    //                 $returnHTML .= "<tr>";
    //                 $returnHTML .= "<td>" . $key2 . "</td>";
    //                 $returnHTML .= "<td>Rp. " . number_format($val2, 0, '.', '.') . "</td>";
    //                 $returnHTML .= "</tr>";
    //             }
    //         }
    //     }
    //     return $returnHTML;
    // }


    public function getDetailNaup($id_pengajuan)
    {

        $getData = BorrowerPengajuan::join('brw_user_detail', 'brw_user_detail.brw_id', 'brw_pengajuan.brw_id')
                 ->leftjoin('brw_tipe_pendanaan', 'brw_tipe_pendanaan.tipe_id', 'brw_pengajuan.pendanaan_tipe')
                 ->leftjoin('m_tujuan_pembiayaan', 'm_tujuan_pembiayaan.id', 'brw_pengajuan.pendanaan_tujuan')
                 ->leftjoin('brw_dtl_naup', 'brw_dtl_naup.pengajuan_id', 'brw_pengajuan.pengajuan_id')
                 ->leftjoin('brw_agunan', 'brw_agunan.id_pengajuan', 'brw_pengajuan.pengajuan_id')
                 ->leftjoin('brw_dtl_lembar_hasil_analisa', 'brw_dtl_lembar_hasil_analisa.id_pengajuan', 'brw_pengajuan.pengajuan_id')
                 ->leftjoin('brw_verifikasi_awal', 'brw_verifikasi_awal.pengajuan_id', 'brw_pengajuan.pengajuan_id')
                 ->where('brw_pengajuan.pengajuan_id', $id_pengajuan)->first();        

        $getData->alamat_sertifikat = $getData->lokasi_proyek . ', No. ' . $getData->blok_nomor . ' RT. ' .
            $getData->RT . '/ RW.' . $getData->RW . ', Ds./Kel.' . $getData->kelurahan . ', Kec. ' . $getData->kecamatan . ', Kab/Kota. ' . $getData->kota;
        $getData->jenis_agunan = ($getData->jenis_agunan == '1') ? 'SHM' : 'HGB';
      
        $kesimpulan_resume_akad = \DB::table('brw_dtl_resume_akad')->where('pengajuan_id', $id_pengajuan)->value('kesimpulan');
        $atas_nama_imb = ($getData->imbt_atas_nama ? $getData->imbt_atas_nama : $getData->atas_nama_imb);

        if ($getData->pendanaan_tipe == '2') {
            $data_kalkulator_kpr = $this->kalkulatorKpr($getData->pendanaan_dana_dibutuhkan,  $getData->persentase_margin , $getData->durasi_proyek);
            $result_simulasi =  $this->newSimulasiAngsuran($data_kalkulator_kpr);

            $simulasi_angsuran = $result_simulasi['total_angsuran']; 
            $simulasi_margin = $result_simulasi['angsuran_margin'];
            $simulasi_total_margin = $result_simulasi['total_margin'];
        }

        return response()->json([
            'pengajuan' => $getData,
            'atas_nama_imb'=> $atas_nama_imb,
            'simulasi_margin' => $simulasi_margin,
            'simulasi_angsuran' => $simulasi_angsuran,
            'simulasi_total_margin' => $simulasi_total_margin,
            'usulan_analis_kredit' => $kesimpulan_resume_akad,
            'brw_nama' => $getData->nama,
            'brw_id' => $getData->brw_id,
        ]);
    }

    public function statusLegalitas($pengajuan_id)
    {
        return DB::table('brw_analisa_pendanaan')->where('pengajuan_id', $pengajuan_id)->value('status_legal');
    }

    public function kirimLegalitasData(Request $request)
    {
        try {

            $nama_legal = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
            $data = \DB::select('call proc_click_button_borrower(?, ?, ?, ?, ?, ?)', ['2', $nama_legal, $request->pengajuan_id, 'kirim', 'LegalitasPendanaan.php', '391']);
            $result_status = $data[0]->sout;

            return $result_status;
        } catch (\Exception $e) {
            return ['status' => 'failed', 'msg' => $e->getMessage()];
        }
    }

    public function checkAllStatusPekerjaan($pengajuan_id)
    {
        $jumlah_selesai_task = DB::table('brw_hd_legalitas')->where('pengajuan_id', $pengajuan_id)
            ->selectRaw('(flag_naup+flag_biaya2) as total_task')
            ->value('total_task');
        $status_legalitas = $this->statusLegalitas($pengajuan_id);

        $parsingJson = array('data' => ['jumlah_selesai' => $jumlah_selesai_task, 'status_legal' => $status_legalitas]);
        echo json_encode($parsingJson);
    }

    public function doPekerjaanLegalUpdate(
        Request $request,
        BorrowerPengajuan $brw_pengajuan
    ) {


        $keterangan = $request->keterangan;
        $pengajuan_id = $request->pengajuan_id;
        $brw_id = $brw_pengajuan::where('pengajuan_id', $pengajuan_id)->pluck('brw_id')->first();


        if ($keterangan == "form_naup") {

            $result_naup = BorrowerDetailNaup::updateOrCreate(
                ['pengajuan_id' => $pengajuan_id],
                [
                    'no_naup' => $request->nomor_naup,
                    'wakalah' => (isset($request->wakalah) && !empty($request->wakalah)) ? '1' : '0',
                    'qardh' => (isset($request->qard) && !empty($request->qard)) ? '1' : '0',
                    'murabahah' => (isset($request->murabahah) && !empty($request->murabahah)) ? '1' : '0',
                    'imbt' => (isset($request->imbt) && !empty($request->imbt)) ? '1' : '0',
                    'mmq' => (isset($request->mmq) && !empty($request->mmq)) ? '1' : '0',
                    'ijarah' => (isset($request->ijarah) && !empty($request->ijarah)) ? '1' : '0',
                    'jumlah_wakalah' => (isset($request->wakalah) && !empty($request->wakalah)) ? str_replace(".", "", $request->jumlah_wakalah) : NULL,
                    'tujuan_penggunaan_wakalah' => (isset($request->wakalah) && !empty($request->wakalah)) ? $request->tujuan_penggunaan_wakalah : NULL,
                    'tujuan_penggunaan_qardh' => (isset($request->qard) && !empty($request->qard)) ? $request->tujuan_penggunaan_qardh : NULL,
                    'tujuan_penggunaan_murabahah' => (isset($request->murabahah) && !empty($request->murabahah)) ? $request->tujuan_penggunaan_murabahah : NULL,
                    'tujuan_penggunaan_imbt' => (isset($request->imbt) && !empty($request->imbt)) ? $request->tujuan_penggunaan_imbt : NULL,
                    'tujuan_penggunaan_mmq' => (isset($request->mmq) && !empty($request->mmq)) ? $request->tujuan_penggunaan_mmq : NULL,
                    'pengikatan_pendanaan_wakalah' => (isset($request->wakalah) && !empty($request->wakalah)) ? $request->pengikatan_pendanaan_wakalah : NULL,
                    'pengikatan_jaminan_wakalah' => (isset($request->wakalah) && !empty($request->wakalah)) ? $request->pengikatan_jaminan_wakalah : NULL,
                    'pengikatan_pendanaan_qardh' => (isset($request->qard) && !empty($request->qard)) ? $request->pengikatan_pendanaan_qardh : NULL,
                    'pengikatan_jaminan_qardh' => (isset($request->qard) && !empty($request->qard)) ? $request->pengikatan_jaminan_qardh : NULL,
                    'pengikatan_pendanaan_murabahah' => (isset($request->murabahah) && !empty($request->murabahah)) ? $request->pengikatan_pendanaan_murabahah : NULL,
                    'pengikatan_jaminan_murabahah' => (isset($request->murabahah) && !empty($request->murabahah)) ? $request->pengikatan_jaminan_murabahah : NULL,
                    'pengikatan_pendanaan_imbt' => (isset($request->imbt) && !empty($request->imbt)) ? $request->pengikatan_pendanaan_imbt : NULL,
                    'pengikatan_jaminan_imbt' => (isset($request->imbt) && !empty($request->imbt)) ? $request->pengikatan_jaminan_imbt : NULL,
                    'take_over_dari_qardh' => (isset($request->qard) && !empty($request->qard)) ? $request->take_over_dari_qardh : NULL,
                    'jumlah_qardh' => (isset($request->qard) && !empty($request->qard)) ? str_replace(".", "", $request->jumlah_qardh) : NULL,
                    'jangka_waktu_wakalah' => (isset($request->wakalah) && !empty($request->wakalah)) ? $request->jangka_waktu_wakalah : NULL,
                    'jangka_waktu_qardh' => (isset($request->qard) && !empty($request->qard)) ? $request->jangka_waktu_qardh : NULL,
                    'jangka_waktu_penyerahan_objek_mmq' => (isset($request->mmq) && !empty($request->mmq)) ? $request->jangka_waktu_penyerahan_objek_mmq : NULL,
                    'jangka_waktu_penyerahan_ijarah' => (isset($request->ijarah) && !empty($request->ijarah)) ? $request->jangka_waktu_penyerahan_ijarah : NULL,
                    'uang_muka_murabahah' => (isset($request->murabahah) && !empty($request->murabahah)) ? str_replace(".", "", $request->uang_muka_murabahah) : NULL,
                    'denda_murabahah' => (isset($request->murabahah) && !empty($request->murabahah)) ? str_replace(".", "", $request->denda_murabahah) : NULL,
                    'uang_muka_mmq' => (isset($request->mmq) && !empty($request->mmq)) ? str_replace(".", "", $request->uang_muka_mmq) : NULL,
                    'uang_muka_imbt' => (isset($request->imbt) && !empty($request->imbt)) ? str_replace(".", "", $request->uang_muka_imbt) : NULL,
                    'take_over_dari_qardh' => (isset($request->qard) && !empty($request->qard)) ? $request->take_over_dari_qardh : NULL,
                    'penetapan_nilai_ujroh_selanjutnya' => (isset($request->imbt) && !empty($request->imbt)) ? $request->penetapan_nilai_ujroh_selanjutnya : NULL,
                    'denda_imbt' => (isset($request->imbt) && !empty($request->imbt)) ? str_replace(".", "", $request->denda_imbt) : NULL,
                    'denda_mmq' => (isset($request->mmq) && !empty($request->mmq)) ? str_replace(".", "", $request->denda_mmq) : NULL,
                    'objek_bagi_hasil_mmq' => (isset($request->mmq) && !empty($request->mmq)) ? $request->objek_bagi_hasil_mmq  : NULL,
                    'nisbah_penyelenggara' => (isset($request->mmq) && !empty($request->mmq)) ? $request->nisbah_penyelenggara : NULL,
                    'nisbah_penerima_dana' => (isset($request->mmq) && !empty($request->mmq)) ? $request->nisbah_penerima_dana : NULL,
                    'hishshah_per_unit' => (isset($request->mmq) && !empty($request->mmq)) ? str_replace(".", "", $request->hishshah_per_unit) : NULL,
                    'objek_sewa' => (isset($request->ijarah) && !empty($request->ijarah)) ? str_replace(".", "", $request->objek_sewa) : NULL,
                    'penyesuaian_nilai_sewa' => (isset($request->ijarah) && !empty($request->ijarah)) ? $request->penyesuaian_nilai_sewa  : NULL,
                    'imbt_atas_nama' => $request->imbt_atas_nama,
                    'appraisal' => $request->appraisal,
                    'rekomendasi_appraisal' => $request->rekomendasi_appraisal,
                    'tanggal_penilaian_agunan' => isset($request->tanggal_penilaian_agunan) ? date('Y-m-d', strtotime($request->tanggal_penilaian_agunan)) : NULL,
                    'nilai_pengikatan_agunan' => str_replace(".", "", $request->nilai_pengikatan_agunan),
                    'hasil_scoring_akhir' => $request->hasil_scoring_akhir,
                    'jenis_pengikatan_agunan' => $request->jenis_pengikatan_agunan,
                    'hasil_scoring_akhir' =>  $request->hasil_scoring_akhir,
                    'plafon_pembiayaan'=> isset($request->plafon_pembiayaan) ? $request->plafon_pembiayaan : NULL
                ]

            );


            if ($result_naup) {

                $nama_legal = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
                $data = \DB::select('call proc_click_button_borrower(?, ?, ?, ?, ?, ?)', ['4', $nama_legal, $pengajuan_id, 'simpan', 'LegalitasPendanaan.php', '498']);
                $result_status = $data[0]->sout;
                if ($result_status == '1') {
                    $response = ['status' => 'success', 'msg' => 'Data berhasil disimpan'];
                } else {
                    $response = ['status' => 'failed', 'msg' => 'Data status gagal disimpan'];
                }
                return $response;
            }
        } else {
            $biaya = $request->biaya;
            $biayaToDelete = BorrowerDetailBiaya::where('pengajuan_id', $pengajuan_id)->pluck('id', 'id');

            foreach ($biaya as $key => $val) {
                $createdUpdated =  BorrowerDetailBiaya::updateOrCreate(
                    [
                        'pengajuan_id' => $request->pengajuan_id,
                        'rincian_biaya' => $val['rincian_biaya']
                    ],

                    [
                        'keterangan' => $val['keterangan'],
                        'jumlah' => preg_replace("/[^0-9]/i", "", $val['jumlah']),
                        'nama_rekanan' => $val['nama_rekanan']
                    ]
                );


                if (!empty($biayaToDelete[$createdUpdated->id])) {
                    unset($biayaToDelete[$createdUpdated->id]);
                }
            }

            if (count($biayaToDelete) > 0) {
                BorrowerDetailBiaya::where('pengajuan_id', $pengajuan_id)->whereIn('id', $biayaToDelete)->delete();
            }


            $nama_legal = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;

            $data = \DB::select('call proc_click_button_borrower(?, ?, ?, ?, ?, ?)', ['5', $nama_legal, $pengajuan_id, 'simpan', 'LegalitasPendanaan.php', '542']);
            $result_status = $data[0]->sout;
            if ($result_status == '1') {
                $response = ['status' => 'success', 'msg' => 'Data berhasil disimpan'];
            } else {
                $response = ['status' => 'failed', 'msg' => 'Data status gagal disimpan'];
            }

            return $response;
        }
    }

    public function list_daftar_pekerjaan($pengajuan_id)
    {

        $data = [
            'NAUP',
            'Biaya - biaya'
        ];

        $dataArray = array();
        $i = 1;

        $query = DB::table('brw_hd_legalitas')->where('pengajuan_id', $pengajuan_id);

        $flag = '';
        $data_updated_at = NULL;

        foreach ($data as $item) {
            $column = array();
            $column['no'] = $i;
            $column['keterangan'] = (string)$item;

            if ($i == 1) {
                $result = $query->select('flag_naup', 'created_at_naup', 'updated_at_naup')->first();

                if ($result) {
                    $flag = $result->flag_naup;
                    $data_created_at = $result->created_at_naup ? date('d-m-Y H:i:s', strtotime($result->created_at_naup)) : '';
                    $data_updated_at = $result->updated_at_naup ? date('d-m-Y H:i:s', strtotime($result->updated_at_naup)) : '';
                }
            }

            if ($i == 2) {
                $result = $query->select('flag_biaya2', 'created_at_biaya2', 'updated_at_biaya2')->first();
                if ($result) {
                    $flag = $result->flag_biaya2;
                    $data_created_at = $result->created_at_biaya2 ? date('d-m-Y H:i:s', strtotime($result->created_at_biaya2)) : '';
                    $data_updated_at = $result->updated_at_biaya2 ? date('d-m-Y H:i:s', strtotime($result->updated_at_biaya2)) : '';
                }
            }


            $column['terakhir_diperbaharui'] =  $data_updated_at ? $data_updated_at : $data_created_at;
            $column['item'] = array("no" => $i, "status" => $flag);

            $i++;
            $dataArray[] = $column;
        }

        $parsingJson = array('data' => $dataArray);

        echo json_encode($parsingJson);
    }

    public function cekNaup($pengajuan_id){
        $naup = BorrowerHdLegalitas::where('pengajuan_id', $pengajuan_id)->first();

        if($naup){
          if($naup->flag_naup != null && $naup->flag_biaya2 != null){
              echo 'both_exits';
          }else if($naup->flag_naup != null && $naup->flag_biaya2 == null){
              echo 'biaya_not_exists';
          }else if($naup->flag_naup == null && $naup->flag_biaya2 != null){
              echo 'naup_not_exists';
          }

        }else{
            echo 'null';
        }
    }
}
