<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Guzzle
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

// Model
use App\BorrowerTipePendanaan;

// estension custom
use Illuminate\Support\Facades\Config;

//create VA Proyek
use App\Http\Controllers\RekeningController;

use App\LoginBorrower;
// create log
use App\AuditTrail;
use Auth;
use App\Admins;
use App\Borrower;
use App\BorrowerLogScorring;
use App\BorrowerPersyaratanPendanaan;
use App\BorrowerPengajuan;
use App\BorrowerLogPengajuan;
use App\BorrowerPendanaan;
use App\BorrowerLogPendanaan;
use App\BorrowerScorringTotal;
use App\BorrowerScorringPersonal;
use App\BorrowerScorringPendanaan;
use App\Borrowerdeskripsiproyek;
use App\BorrowerInvoice;
use App\BorrowerPembayaran;
use App\BorrowerLogPembayaran;
use App\BorrowerJaminan;
use App\BorrowerPersyaratanInsert;
use App\Proyek;
use App\GambarProyek;
use DB;
use Mail;
use App\Mail\EmailVerifikasiPenilaian;
use App\Mail\EmailApproveAnalisToAdmin;


class AdminProsessController extends Controller
{
  
  public function __construct()
  {
      // $this->middleware('auth:api', ['except'=>[]]);
  }
  
  // Jenis Pendanaan Prosess Start Here
  public function postNewJenis(Request $request)
  {    
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/postJenisPendanaan",[
      'form_params' =>
      [
        'pendanaanNama' => $request->pendanaanJenis,
        'pendanaanKeterangan' => $request->pendanaanKeterangan,
      ]
    ]);

    $response = $request->getBody()->getContents();

    $audit = new AuditTrail;
    $username = Auth::guard('admin')->user()->firstname;
    $audit->fullname = $username;
    $audit->menu = "Persyaratan Penerima Pendanaan";
    $audit->description = "Tambah Jenis Pendanaan";
    $audit->ip_address =  \Request::ip();
    $audit->save();

    if($response == 'Success')
    {
      return redirect()->back()->with('success','Telah menambahakan jenis Pendanaan');
    }
    else
    {
      return redirect()->back()->with('error','Ada yang error nih.');      
    }

  }

  public function updateJenis(Request $request)
  {
    $id = $request->idPendanaanJenis;
    $jenis = $request->editPendanaanJenis;
    $ket = $request->editPendanaanKeterangan;

    $idList = array();
    $valueList = array();
    $list = array();
    $deleteList = array();

    $idList[] = $request->idList;
    $valueList[] = $request->valueList;
    $list[] = $request->list;
    $deleteList[] = $request->deleteList; 

    // dd($list);



    // dd($response);
    
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/updateTipeJenis",[
      'form_params' =>
      [       
        'pendanaanId' => $id,
        'idList' => $request->idList,
        'listValue' => $request->list,
        'checkBox' => $request->valueList,
        'deleteList' => $request->deleteList,
      ]
    ]);
    $resUpdateTipeJenis = $request->getBody()->getContents();

    
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/updateJenisPendanaan",[
      'form_params' =>
      [
        'pendanaanId' => $id,
        'pendanaanNama' => $jenis,
        'pendanaanKeterangan' => $ket,    
      ]
    ]);
    $response = $request->getBody()->getContents();

    // dd($resUpdateTipeJenis);

     
    $audit = new AuditTrail;
    $username = Auth::guard('admin')->user()->firstname;
    $audit->fullname = $username;
    $audit->menu = "Persyaratan Penerima pendanaan";
    $audit->description = "Edit jenis pendanaan";
    $audit->ip_address =  \Request::ip();
    $audit->save();
    
    if($resUpdateTipeJenis == 'Success')
    {
      return redirect()->back()->with('success','Telah mengupdate jenis Pendanaan');
    }
    else
    {
      return redirect()->back()->with('error','Ada yang error nih.');      
    }
  }

  public function deleteJenis($id)
  {
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/postDeleteJenis",[
      'form_params' =>
      [
        'pendanaanId' => $id,
      ]
    ]);
    $response = $request->getBody()->getContents();

    $audit = new AuditTrail;
    $username = Auth::guard('admin')->user()->firstname;
    $audit->fullname = $username;
    $audit->description = "Hapus jenis pendanaan";
    $audit->ip_address =  \Request::ip();
    $audit->save();
    if($response == 'Success')
    {
      $resData = ['data' => 'sukses'];
      
      return response($resData);
    }
    else
    {

    }
  }
  

  public function getListJenis($id)
  {
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/getListJenis",
                            [
                              'form_params' => 
                              [
                                'tipe_id' => $id,
                              ]
                            ]
                          );
    $response = $request->getBody()->getContents();
    
    return response($response);
    
  }

  
  public function getListJenisA($id)
  {
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/getListJenisA",
                            [
                              'form_params' => 
                              [
                                'tipe_id' => $id,
                              ]
                            ]
                          );
    $response = $request->getBody()->getContents();
    
    return response($response);
    
  }

  
  public function getListJenisB($id)
  {
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/getListJenisB",
                            [
                              'form_params' => 
                              [
                                'tipe_id' => $id,
                              ]
                            ]
                          );
    $response = $request->getBody()->getContents();
    
    return response($response);
    
  }



  public function addJenisList(Request $request)
  {
    $data = [
              'idListNew' => $request->idListNew ,
              'listValue' => $request->addList,
              'addSelected' => $request->addSelect,
              'mandatoryList' => $request->mandatoryList,
            ];
    $dataJson = json_encode($data);
    $client = new Client();
    $request = $client->post(config('app.apilink')."/borrower-admin/server-side/newPostTipeJenis",
                            [
                              'form_params' => 
                              [
                                'data' => $dataJson,
                              ]
                            ]
                          );
    $response = $request->getBody()->getContents();

     
    $audit = new AuditTrail;
    $username = Auth::guard('admin')->user()->firstname;
    $audit->fullname = $username;
    $audit->menu = "Persyaratan Penerima Pendanaan";
    $audit->description = "Tambah daftar jenis pendanaan";
    $audit->ip_address =  \Request::ip();
    $audit->save();

    // dd($response);/
    if($response == 'Succcess')
    {
    return redirect()->back()->with('success','Telah menambah jenis Pendanaan');
    }
    else
    {
      return redirect()->back()->with('error','Ada yang error nih.');      
    }

  }


  // Jenis Pendanaan Prosess End



	public function postScorringBorrower(Request $request)
	{
    
		$id = $request->id;
		$idPengajuan = $request->idPengajuan;
		$scorePersonal = $request->scorePersonal;
    $scorePendanaan =  $request->scorePendanaan;
    $status_verif =  'terima';
    $keterangan =  '';
    $file = 'AdminProsessController.php';
    $line = 316;
    
		// $client = new Client();
		// $request = $client->post(config('app.apilink')."/borrower-admin/server-side/postTotalScoring",
		// 						[
		// 						  'form_params' => 
		// 						  [
		// 							'idBorrower' => $id,
		// 							'idPengajuan' => $idPengajuan,
		// 							'scorePersonal' => $scorePersonal,
		// 							'scorePendanaan' => $scorePendanaan
		// 						  ]
		// 						]
		// 					  );
		// $response = $request->getBody()->getContents();
    // $return = json_decode($response);

    try{

      DB::beginTransaction();

      $call_db = DB::select(
        "call proc_brw_analyst_verif('$id','$idPengajuan','$scorePersonal','$scorePendanaan','$status_verif','$keterangan', '$file', '$line')"
      );

      if($call_db[0]->res == 1){
        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "skor Penerima Pendanaan";
        $audit->description = "Menyetujui Pendanaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        DB::commit();

        $data_email = Borrower::select('brw_user.email', 'brw_user_detail.nama', 'brw_pengajuan.pendanaan_nama', DB::raw('(CASE WHEN brw_pengajuan.pendanaan_akad = 1 THEN "Murabahah" ELSE "Lainnya" END) AS pendanaan_akad'),
            'brw_pengajuan.estimasi_mulai', 'brw_pengajuan.durasi_proyek', 'brw_pengajuan.pendanaan_dana_dibutuhkan', 'brw_pengajuan.lokasi_proyek', 'brw_pengajuan.keterangan')
            ->addSelect(DB::raw("'Berhasil' as status"))
            ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
            ->join('brw_pengajuan', 'brw_pengajuan.brw_id', '=', 'brw_user.brw_id')
            ->where('brw_pengajuan.pengajuan_id', $idPengajuan)->get();

        $email = new EmailVerifikasiPenilaian($data_email[0], '', 'borrower');
        Mail::to($data_email[0]->email)->send($email);

        return response(['data'=>'ok']);
      }else{
        DB::rollback();
        return response(['data'=>'no']);
      }

      
    }catch(Throwable $e){
      DB::rollback();
      return $e;
    }
	}



  public function rejectScorringBorrower(Request $request)
  {

    $id = $request->id;
		$idPengajuan = $request->idPengajuan;
		$scorePersonal = 0;
    $scorePendanaan =  0;
    $status_verif =  'tolak';
    $keterangan =  $request->keterangan;
    $file = 'AdminProsessController.php';
    $line = 374;

		// $client = new Client();
    // $request = $client->post(config('app.apilink')."/borrower-admin/server-side/rejectScorringBorrower",
    //                         [
    //                           'form_params' => 
    //                           [
    //                             'idBorrower' => $request->id,
    //                             'idPengajuan' => $request->idPengajuan,
    //                           ]
    //                         ]
    //                       );
    // $response = $request->getBody()->getContents();

    // dd($response);

    try{
      DB::beginTransaction();

      $call_db = DB::select(
        "call proc_brw_analyst_verif('$id','$idPengajuan','$scorePersonal','$scorePendanaan','$status_verif','$keterangan', '$file', '$line')"
      );
      if($call_db[0]->res == 1){
        $audit = new AuditTrail;
        $username = Auth::guard('admin')->user()->firstname;
        $audit->fullname = $username;
        $audit->menu = "skor Penerima Pendanaan";
        $audit->description = "Menyetujui Pendanaan";
        $audit->ip_address =  \Request::ip();
        $audit->save();

        DB::commit();

        $data_email = Borrower::select('brw_user.email', 'brw_user_detail.nama', 'brw_pengajuan.pendanaan_nama', DB::raw('(CASE WHEN brw_pengajuan.pendanaan_akad = 1 THEN "Murabahah" ELSE "Lainnya" END) AS pendanaan_akad'),
            'brw_pengajuan.estimasi_mulai', 'brw_pengajuan.durasi_proyek', 'brw_pengajuan.pendanaan_dana_dibutuhkan')
            ->addSelect(DB::raw("'Ditolak' as status"))
            ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
            ->join('brw_pengajuan', 'brw_pengajuan.brw_id', '=', 'brw_user.brw_id')
            ->where('brw_pengajuan.pengajuan_id', $idPengajuan)->get();

        $email = new EmailVerifikasiPenilaian($data_email[0], '', 'borrower');
        Mail::to($data_email[0]->email)->send($email);

        return response(['data'=>'ok']);
      }else{
        DB::rollback();
        $data = ['data' => 'no'];
        return response($data);
      }
    }catch(Throwable $e){
      DB::rollback();
      return $e;
    }
    
  }

  public function updateDetailsBorrower(Request $request){
    
    $brw_id = $request->brw_id_edit;
    $brw_type = $request->brw_type_edit;
    $data_dokumen_persyaratan = $request->txt_persyaratan_pendanaan;
    $tipe_pendanaan_edit = $request->tipe_pendanaan_edit;
    $penilaian_personal = $request->penilaian_personal;
    $penilaian_pendanaan = $request->penilaian_pendanaan;
    $deskripsi_proyek = $request->textarea_deskripsi;

    //KONDISI JIKA PENILAIAN PENDANAAN KURANG, MAKA AKAN DITOLAK
    if($penilaian_personal < 677 || $penilaian_pendanaan < 250){
        $updatePengajuan = BorrowerPengajuan::where('pengajuan_id',$request->pengajuan_id_edit)->first();
        $updatePengajuan->status =  2;
        $updatePengajuan->keterangan = $request->keterangan_penilaian;
        $updatePengajuan->save(); 

        //INSERT OR UPDATE brw_scorring_personal
        $brw_scorring_personal = BorrowerScorringPersonal::updateOrCreate(['brw_id'=>$brw_id],['nilai'=>$penilaian_personal, 'brw_id'=>$brw_id]);

        $results = DB::select( DB::raw("SELECT grade_nilai FROM brw_master_grade WHERE $penilaian_pendanaan between min and max"));
        //INSERT OR UPDATE brw_scorring_pendanaan
        $brw_scorring_pendanaan = BorrowerScorringPendanaan::updateOrCreate(['pengajuan_id'=>$request->pengajuan_id_edit],['pengajuan_id'=>$request->pengajuan_id_edit, 'scorring_judul'=>$results[0]->grade_nilai, 'scorring_nilai'=>$penilaian_pendanaan, 'user_create'=> Auth::guard('admin')->user()->firstname]);

        $data_email = Borrower::select('brw_user.email', 'brw_user_detail.nama', 'brw_pengajuan.pendanaan_nama', 'brw_pengajuan.keterangan', 'brw_pengajuan.created_at', 'brw_pengajuan.pendanaan_dana_dibutuhkan')
        ->addSelect(DB::raw("'Ditolak' as status"))
        ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
        ->join('brw_pengajuan', 'brw_pengajuan.brw_id', '=', 'brw_user.brw_id')
        ->where('brw_pengajuan.pengajuan_id', $request->pengajuan_id_edit)->get();
        
        $email = new EmailVerifikasiPenilaian($data_email[0], '', 'borrower');
        Mail::to($data_email[0]->email)->send($email);

        return redirect()->back()->with('error','Pengajuan Pendanaan Ditolak');
    }else{
        $updatePengajuan = BorrowerPengajuan::where('pengajuan_id',$request->pengajuan_id_edit)->first();
        $updatePengajuan->pendanaan_nama = $request->pendanaan_nama_edit;
        $updatePengajuan->pendanaan_dana_dibutuhkan = $request->pendanaan_dana_dibutuhkan_edit;
        $updatePengajuan->estimasi_imbal_hasil = $request->estimasi_imbal_hasil_edit;
        $updatePengajuan->durasi_proyek = $request->durasi_proyek_edit;
        $updatePengajuan->estimasi_mulai = $request->estimasi_mulai_edit;
        $updatePengajuan->lokasi_proyek = $request->alamat_edit;
        $updatePengajuan->geocode = $request->geocode_edit;
        $updatePengajuan->lokasi_proyek = $request->alamat_edit;
        $updatePengajuan->pendanaan_tipe = $request->tipe_pendanaan_edit;
        $updatePengajuan->pendanaan_akad = $request->akad_edit;
        $updatePengajuan->detail_pendanaan = $deskripsi_proyek;
        $updatePengajuan->status = 3;
        $updatePengajuan->keterangan = $request->keterangan_penilaian;
        $updatePengajuan->save(); 

        $check_existing_dokumen = BorrowerPersyaratanInsert::where('brw_id', $brw_id)->where('tipe_id', $tipe_pendanaan_edit)->where('user_type', $brw_type)->count();
        if($check_existing_dokumen>0){

          $update_all = BorrowerPersyaratanInsert::where('brw_id', $brw_id)
              ->where('tipe_id',$tipe_pendanaan_edit)
              ->where('user_type',$brw_type)
              ->update(['checked' => 0]);  

          for($i=0; $i<sizeof($data_dokumen_persyaratan);$i++){
            $update_checked = BorrowerPersyaratanInsert::where('brw_id', $brw_id)
              ->where('tipe_id',$tipe_pendanaan_edit)
              ->where('user_type',$brw_type)
              ->where('persyaratan_id',$data_dokumen_persyaratan[$i])
              ->update(['checked' => 1]);  
          }
        }else{
          $getPersyaratan = BorrowerPersyaratanPendanaan::select('persyaratan_id','persyaratan_mandatory')->where('tipe_id',$tipe_pendanaan_edit)->where('user_type',$brw_type)->get();

          //INSERT dulu ke brw_persyaratan_insert
          for($i=0;$i<sizeof($getPersyaratan);$i++){
            $masukanPersyaratan = new BorrowerPersyaratanInsert(); 
            $masukanPersyaratan->brw_id = $brw_id;
            $masukanPersyaratan->tipe_id = $tipe_pendanaan_edit;
            $masukanPersyaratan->user_type = $brw_type;
            $masukanPersyaratan->persyaratan_id = $getPersyaratan[$i]['persyaratan_id'];
            $masukanPersyaratan->checked = 0;
            $masukanPersyaratan->save(); 
          }

          //UPDATE brw_persyaratan_insert
          for($i=0; $i<sizeof($data_dokumen_persyaratan);$i++){
            $Pengajuan = BorrowerPersyaratanInsert::where('brw_id', $brw_id)
              ->where('tipe_id',$tipe_pendanaan_edit)
              ->where('user_type',$brw_type)
              ->where('persyaratan_id',$data_dokumen_persyaratan[$i])
              ->update(['checked' => 1]);  
          }
          
        }

        //INSERT OR UPDATE brw_scorring_personal
        $brw_scorring_personal = BorrowerScorringPersonal::updateOrCreate(['brw_id'=>$brw_id],['nilai'=>$penilaian_personal, 'brw_id'=>$brw_id]);

        $results = DB::select( DB::raw("SELECT grade_nilai FROM brw_master_grade WHERE $penilaian_pendanaan between min and max"));
        //INSERT OR UPDATE brw_scorring_pendanaan
        $brw_scorring_pendanaan = BorrowerScorringPendanaan::updateOrCreate(['pengajuan_id'=>$request->pengajuan_id_edit],['pengajuan_id'=>$request->pengajuan_id_edit, 'scorring_judul'=>$results[0]->grade_nilai, 'scorring_nilai'=>$penilaian_pendanaan, 'user_create'=> Auth::guard('admin')->user()->firstname]);

        //FILE GAMBAR UTAMA
        if ($request->hasFile('file')) {
          $gambar_utama_path = 'proyek/TEMP_PROYEK/' . $request->pengajuan_id_edit .'/'.$request->pengajuan_id_edit.'_projectpic350x233';
          $file = $request->file('file');
          $filename = $request->pengajuan_id_edit.'/'.$request->pengajuan_id_edit.'_projectpic350x233' . '.' . $file->getClientOriginalExtension();
          
          $store_path = 'proyek/TEMP_PROYEK'; 
          $path = $file->storeAs($store_path, $filename, 'public');
          $updatePathGambar = BorrowerPengajuan::where('pengajuan_id',$request->pengajuan_id_edit)->update(['gambar_utama' => $gambar_utama_path.'.'.$file->getClientOriginalExtension()]);
        }
        //FILE GAMBAR DETIL 1
        if ($request->hasFile('file_detil1')) {
          $gambar_detil1_path = 'proyek/TEMP_PROYEK/' . $request->pengajuan_id_edit .'/'.$request->pengajuan_id_edit.'_projectpic730x486';
          $file = $request->file('file_detil1');
          $filename = $request->pengajuan_id_edit.'/'.$request->pengajuan_id_edit.'_projectpic730x486' . '.' . $file->getClientOriginalExtension();
          
          $store_path = 'proyek/TEMP_PROYEK'; 
          $path = $file->storeAs($store_path, $filename, 'public');
          $updatePathGambar = GambarProyek::where('id',$request->id_image_detil1)->update(['gambar' => $gambar_detil1_path.'.'.$file->getClientOriginalExtension()]);
        }
        //FILE GAMBAR DETIL 2
        if ($request->hasFile('file_detil2')) {
          $gambar_detil2_path = 'proyek/TEMP_PROYEK/' . $request->pengajuan_id_edit .'/'.$request->pengajuan_id_edit.'_projectpic730x486';
          $file = $request->file('file_detil2');
          $filename = $request->pengajuan_id_edit.'/'.$request->pengajuan_id_edit.'_projectpic730x486' . '.' . $file->getClientOriginalExtension();
          
          $store_path = 'proyek/TEMP_PROYEK'; 
          $path = $file->storeAs($store_path, $filename, 'public');
          $updatePathGambar = GambarProyek::where('id',$request->id_image_detil2)->update(['gambar' => $gambar_detil2_path.'.'.$file->getClientOriginalExtension()]);
        }

        $data_email = Borrower::select('brw_user_detail.nama', 'brw_pengajuan.pendanaan_nama', 'brw_pengajuan.keterangan', 'brw_pengajuan.pendanaan_dana_dibutuhkan', 'brw_pengajuan.created_at', 
            'brw_scorring_pendanaan.scorring_nilai', 'brw_scorring_pendanaan.scorring_judul', 'brw_scorring_personal.nilai')
            ->join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
            ->join('brw_pengajuan', 'brw_pengajuan.brw_id', '=', 'brw_user.brw_id')
            ->join('brw_scorring_pendanaan', 'brw_pengajuan.pengajuan_id', '=', 'brw_scorring_pendanaan.pengajuan_id')
            ->join('brw_scorring_personal', 'brw_user_detail.brw_id', '=', 'brw_scorring_personal.brw_id')
            ->where('brw_pengajuan.pengajuan_id', $request->pengajuan_id_edit)->get();
        
        $email_to = Admins::select('email', 'firstname', 'lastname')->where('role', '=', 3)->get();

        $i = 0;
        foreach($email_to as $data){
          $email_analis_to_admin = new EmailApproveAnalisToAdmin($data_email[0], $data);
          Mail::to($data->email)->send($email_analis_to_admin);
          $i++;
        }
        
        return redirect()->back()->with('success','Berhasil Merubah Data');
    }
  }
  
  public function editJaminanPengajuan(Request $request){

    if($request->status_form_jaminan=='add'){
      if ($request->hasFile('file_jaminan')) {
        
        $store_path = 'jaminan/' . $request->id_pengajuan;
        $file = $request->file('file_jaminan');
        $filename = $file->getClientOriginalName();
        $path = $file->storeAs($store_path, $filename, 'public');
      }

      $insertJaminan = new BorrowerJaminan;
      $insertJaminan->pengajuan_id = $request->id_pengajuan;
      $insertJaminan->jaminan_nama = $request->nama_jaminan;
      $insertJaminan->jaminan_nomor = $request->nomor_jaminan;
      $insertJaminan->NOP = $request->nop_jaminan;
      $insertJaminan->kantor_penerbit = $request->kantor_penerbit_jaminan;
      $insertJaminan->jaminan_nilai = $request->nilai_jaminan;
      $insertJaminan->jaminan_detail = $request->detil_jaminan;
      $insertJaminan->jaminan_jenis = $request->jenis_jaminan_edit;
      $insertJaminan->sertifikat = $path;
      $insertJaminan->save(); 
    }else{
      $updateJaminan = BorrowerJaminan::where('jaminan_id',$request->id_jaminan)->where('pengajuan_id',$request->id_pengajuan)->first();

      if ($request->hasFile('file_jaminan')) {
        $store_path = 'jaminan/' . $request->id_pengajuan;
        $file = $request->file('file_jaminan');
        $filename = $file->getClientOriginalName();
        $path = $file->storeAs($store_path, $filename, 'public');

        $updateJaminan->sertifikat = $path;
      }
      $updateJaminan->jaminan_nama = $request->nama_jaminan;
      $updateJaminan->jaminan_nomor = $request->nomor_jaminan;
      $updateJaminan->NOP = $request->nop_jaminan;
      $updateJaminan->kantor_penerbit = $request->kantor_penerbit_jaminan;
      $updateJaminan->jaminan_nilai = $request->nilai_jaminan;
      $updateJaminan->jaminan_detail = $request->detil_jaminan;
      $updateJaminan->jaminan_jenis = $request->jenis_jaminan_edit;
      $updateJaminan->save(); 
    }
		
    return response(['data'=>'ok', 'file_jaminan'=>isset($path)?$path:'']);
  }

  public function deleteJaminanPengajuan(Request $request){

		$updateJaminan = BorrowerJaminan::where('jaminan_id',$request->jaminan_id)->delete();
    
    return response(['data'=>'ok']);
  }

}
