<?php

namespace App\Http\Controllers\Admin;

use App\BorrowerPengajuan;
use App\BorrowerTipePendanaan;
use App\BorrowerUserDetail;
use App\Deskripsi_Proyek;
use App\Exceptions\DataTablesException;
use App\Helpers\Datatable;
use App\Helpers\DataTablesBuilderService;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Middleware\UserCheck;
use App\Legalitas_Proyek;
use App\MasterStatusPengajuan;
use App\Pemilik_Proyek;
use App\Proyek;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Storage;
use Validator;
use Yajra\DataTables\Facades\DataTables;

class PenggalanganDanaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['userfeed']);
        $this->middleware(UserCheck::class)->only(['userfeed']);
        $this->middleware('auth:admin')->only(['manage', 'create', 'show', 'edit']);
    }

    public function manage(DataTablesBuilderService $builder)
    {
        $pengajuanTable = BorrowerPengajuan::TABLE;
        $userDetailTable = BorrowerUserDetail::TABLE;
        $tipeTable = BorrowerTipePendanaan::TABLE;
        $statusTable = MasterStatusPengajuan::TABLE;

        $dataTables = $builder
            ->setOrder(1)
            ->setUrl(route('penggalangan_dana.manage.table_data'))
            ->withIndex()
            ->setColumns([[
                'name' => "$userDetailTable.nama",
                'data' => "user_detail_name",
                'title' => 'Nama Borrower',
            ], [
                'name' => "lokasi_proyek",
                'title' => 'Alamat Pembiayaan',
            ], [
                'name' => "$tipeTable.pendanaan_nama",
                'data' => "tipe_pendanaan_nama",
                'title' => 'Jenis Pembiayaan',
            ], [
                'name' => "pendanaan_dana_dibutuhkan",
                'title' => 'Nominal',
            ], [
                'name' => "durasi_proyek",
                'title' => 'Tenor',
            ], [
                'name' => "$statusTable.keterangan_pengajuan",
                'data' => "status_keterangan_pengajuan",
                'title' => 'Status',
            ]]);

        return view('pages.admin.penggalangan_dana.index')->with([
            'table' => $dataTables,
        ]);
    }

    public function manageTableData()
    {
        $pengajuanTable = BorrowerPengajuan::TABLE;
        $userDetailTable = BorrowerUserDetail::TABLE;
        $tipeTable = BorrowerTipePendanaan::TABLE;
        $statusTable = MasterStatusPengajuan::TABLE;

        $pengajuanList = BorrowerPengajuan::query()
            ->select([
                "$pengajuanTable.pengajuan_id",
                "$pengajuanTable.id_proyek",
                "$pengajuanTable.lokasi_proyek",
                "$pengajuanTable.kelurahan",
                "$pengajuanTable.kecamatan",
                "$pengajuanTable.kota",
                "$pengajuanTable.provinsi",
                "$pengajuanTable.kode_pos",
                "$pengajuanTable.pendanaan_tipe",
                "$pengajuanTable.pendanaan_dana_dibutuhkan",
                "$pengajuanTable.durasi_proyek",
                "$pengajuanTable.status",
                "$userDetailTable.nama as user_detail_name",
                "$tipeTable.pendanaan_nama as tipe_pendanaan_nama",
                "$statusTable.keterangan_pengajuan as status_keterangan_pengajuan",
            ])
            ->leftJoin($userDetailTable, "$pengajuanTable.brw_id", "$userDetailTable.brw_id")
            ->leftJoin($tipeTable, "$pengajuanTable.pendanaan_tipe", "$tipeTable.tipe_id")
            ->leftJoin($statusTable, "$pengajuanTable.status", "$statusTable.id_status_pengajuan");

        try {
            return DataTables::of($pengajuanList)
                ->addIndexColumn()
                ->filterColumn('lokasi_proyek', function ($query, $keyword) use ($pengajuanTable) {
                    $sql = "CONCAT($pengajuanTable.lokasi_proyek,$pengajuanTable.kota,$pengajuanTable.provinsi,$pengajuanTable.kode_pos) like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                })
                ->editColumn('lokasi_proyek', function ($pengajuan) {
                    return nl2br("$pengajuan->lokasi_proyek
                        <small style='color: grey'>Kel.</small> $pengajuan->kelurahan, <small style='color: grey'>Kec.</small> $pengajuan->kecamatan
                        <small style='color: grey'>Kota</small> $pengajuan->kota, <small style='color: grey'>Prov.</small> $pengajuan->provinsi, $pengajuan->kode_pos");
                })
                ->editColumn('pendanaan_dana_dibutuhkan', function ($pengajuan) {
                    $value = Helper::toThousand($pengajuan->pendanaan_dana_dibutuhkan);

                    return "<span class='text-right monospace'>$value</span>";
                })
                ->editColumn('tenor', function ($pengajuan) {
                    $value = Helper::toThousand($pengajuan->tenor);

                    return "<span class='text-right monospace'>$value</span>";
                })
                ->editColumn('status_keterangan_pengajuan', function ($pengajuan) {
                    $html = "<span class='text-right monospace'>$pengajuan->status_keterangan_pengajuan</span>";

                    if ($pengajuan->status === MasterStatusPengajuan::V_PENGGALANGAN && $pengajuan->id_proyek === null) {
                        $route = route('penggalangan_dana.create', ['pengajuan_id' => $pengajuan->pengajuan_id]);
                        $html .= "<br><a href='$route'><button class='btn btn-success'>Buat Proyek</button></a>";
                    } elseif ($pengajuan->status === MasterStatusPengajuan::V_PENGGALANGAN) {
                        $route = route('penggalangan_dana.edit', ['pengajuan_id' => $pengajuan->pengajuan_id]);
                        $html .= "<br><a href='$route' target='_blank'><button class='btn btn-warning'>Ubah Proyek</button></a>";
                    } else {
                        $route = route('penggalangan_dana.show', ['pengajuan_id' => $pengajuan->pengajuan_id]);
                        $html .= "<br><a href='$route' target='_blank'><button class='btn btn-info'>Lihat Proyek</button></a>";
                    }

                    return $html;
                })
                ->rawColumns(['lokasi_proyek', 'pendanaan_dana_dibutuhkan', 'tenor', 'status_keterangan_pengajuan'])
                ->make(true);
        } catch (Exception $e) {
            throw new DataTablesException($e);
        }
    }

    public function show(Request $request, BorrowerPengajuan $pengajuan)
    {
        /** @var Proyek $proyek */
        $proyek = $pengajuan->proyek;

        if ($proyek === null) {
            abort(404);
        }

        $proyek->load([
            'deskripsiProyekRel',
            'pemilikProyekRel',
            'legalitasProyekRel',
        ]);

        return view('pages.admin.penggalangan_dana.show')->with([
            'proyek' => $proyek,
            'pengajuan' => $pengajuan,
        ]);
    }

    public function edit(Request $request, BorrowerPengajuan $pengajuan)
    {
        /** @var Proyek $proyek */
        if ($pengajuan->status !== MasterStatusPengajuan::V_PENGGALANGAN) {
            session()->flash('message_type', 'warning');
            session()->flash('message', "Proyek tidak dapat diubah, karena sudah melewati batas waktu penggalangan");

            return redirect()->route('penggalangan_dana.manage');
        }
        $proyek = $pengajuan->proyek;

        $proyek->load([
            'deskripsiProyekRel',
            'pemilikProyekRel',
            'legalitasProyekRel',
        ]);

        return view('pages.admin.penggalangan_dana.edit')->with([
            'pengajuan' => $pengajuan,
            'proyek' => $proyek,
        ]);
    }

    private function save(Request $request, Proyek $proyek, BorrowerPengajuan $pengajuan)
    {
        $proyek->forceFill([
            'total_need' => $pengajuan->pendanaan_dana_dibutuhkan,
            'tgl_mulai' => Carbon::parse($request->get('tgl_selesai_penggalangan'))->addDay()->toDateString(),
            'tgl_selesai' => Carbon::parse($request->get('tgl_mulai'))->addMonth($request->get('tenor_waktu'))->toDateString(),
        ]);
        $proyek
            ->forceFill($request->except(['approve', 'desc', 'files', '_token', 'pengajuan_id', '_method']))
            ->save();

        $proyek->deskripsiProyekRel()->associate(Deskripsi_Proyek::create(['deskripsi' => $request->get('desc')['deskripsi']]));
        $proyek->legalitasProyekRel()->associate(Legalitas_Proyek::create(['deskripsi_legalitas' => $request->get('desc')['legalitas']]))->save();
        if ($request->get('desc')['pemilik'] ?? null) {
            $proyek->pemilikProyekRel()->associate(Pemilik_Proyek::create(['deskripsi_pemilik' => $request->get('desc')['pemilik']]))->save();
        }

        if ($request->file('files')['gambar_utama'] ?? null) {
            $gambarUtamaPath = Helper::upload(
                $request->file('files')['gambar_utama'],
                'gambar_utama',
                "proyek/$proyek->id/projectpic350x233"
            );
            $deletePaths[] = $gambarUtamaPath;
            $proyek->forceFill(['gambar_utama' => $gambarUtamaPath]);
        }

        if ($request->file('files')['slider'] ?? null) {
            $gambarpath = "proyek/$proyek->id/projectpic730x486";
            $i = $proyek->gambarProyek()->count() + 1;
            foreach ($request->file('files')['slider'] as $key => $slider) {
                $proyek->gambarProyek()->create([
                    'gambar' => $path = Helper::upload($slider, 'gambar' . ($key + $i), $gambarpath),
                ]);
                $deletePaths[] = $path;
            }
        }

        $proyek->save();
    }

    public function update(Request $request, BorrowerPengajuan $pengajuan)
    {
        if ($pengajuan->status !== MasterStatusPengajuan::V_PENGGALANGAN) {
            session()->flash('message_type', 'warning');
            session()->flash('message', "Proyek tidak dapat diubah, karena sudah melewati batas waktu penggalangan");

            return redirect()->route('penggalangan_dana.manage');
        }

        /** @var Proyek $proyek */
        $proyek = $pengajuan->proyek()->firstOrFail();

        $deletePaths = collect([]);
        try {
            DB::beginTransaction();
            $validator = $this->createValidator($request->toArray());
            $validator->validate();
            $validator->addRules([
                'files.gambar_utama' => ['file', 'mimes:jpeg,jpg,png,gif,svg,bmp', 'dimensions:width=350,height=233'],
                'files.slider' => ['array'],
                'files.slider.*' => ['file', 'mimes:jpeg,jpg,png,gif,svg,bmp', 'dimensions:width=730,height=486'],
            ]);
            $validator->addCustomAttributes([
                'files' => '',
                'files.gambar_utama' => 'Thumbnail Profile Proyek',
                'files.slider' => '',
                'files.slider.*' => 'Slider Profile Proyek',
            ]);
            $this->save($request, $proyek, $pengajuan);

            if ($request->has('approve')) {
                $pengajuan->forceFill([
                    'status' => MasterStatusPengajuan::V_PENGGALANGAN,
                ])
                    ->save();

                session()->flash('message_type', 'info');
                session()->flash('message', "Proyek berhasil disetujui");
                DB::commit();

                return redirect()->route('penggalangan_dana.manage');
            }
            session()->flash('message_type', 'info');
            session()->flash('message', "Proyek berhasil diubah");

            DB::commit();

            return redirect()->route('penggalangan_dana.edit', $pengajuan->pengajuan_id);
        } catch (Exception $exception) {
            DB::rollBack();
            $deletePaths->each(function ($path) {
                Storage::delete("public/$path");
            });

            throw $exception;
        }
    }

    public function create(Request $request)
    {
        if (($pengajuanId = $request->get('pengajuan_id')) === null) {
            return redirect()->route('penggalangan_dana.manage');
        }

        /** @var BorrowerPengajuan $pengajuan */
        $pengajuan = BorrowerPengajuan::query()
            ->where('pengajuan_id', $pengajuanId)
            ->firstOrFail();

        if ($pengajuan->proyek()->exists()) {
            session()->flash('message_type', 'warning');
            session()->flash('message', "Pengajuan telah memiliki proyek");

            return redirect()->route('penggalangan_dana.show', $pengajuan);
        }

        return view('pages.admin.penggalangan_dana.create')->with([
            'pengajuan' => $pengajuan,
        ]);
    }

    /**
     * @throws \Throwable
     */
    public function store(Request $request, Proyek $proyek)
    {
        $pengajuan = BorrowerPengajuan::find($request->get('pengajuan_id'));
        if (Proyek::find($pengajuan->id_proyek) !== null) {
            session()->flash('message_type', 'warning');
            session()->flash('message', "Pengajuan telah memiliki proyek");

            return redirect()->route('penggalangan_dana.show', $request->get('pengajuan_id'));
        }
        $deletePaths = collect([]);
        try {
            DB::beginTransaction();
            $validator = $this->createValidator($request->toArray());
            $validator->addRules([
                'pengajuan_id' => ['required', Rule::exists('brw_pengajuan', 'pengajuan_id')],
                'files' => ['required'],
                'files.gambar_utama' => ['required', 'file', 'mimes:jpeg,jpg,png,gif,svg,bmp', 'dimensions:width=350,height=233'],
                'files.slider' => ['required', 'array'],
                'files.slider.*' => ['required', 'file', 'mimes:jpeg,jpg,png,gif,svg,bmp', 'dimensions:width=730,height=486'],
            ]);
            $validator->addCustomAttributes([
                'pengajuan_id' => 'Pengajuan',
                'files' => '',
                'files.gambar_utama' => 'Thumbnail Profile Proyek',
                'files.slider' => '',
                'files.slider.*' => 'Slider Profile Proyek',
            ]);
            $validator->validate();

            $this->save($request, $proyek, $pengajuan);

            BorrowerPengajuan::query()->findOrFail($request->get('pengajuan_id'))
                ->proyek()->associate($proyek)->save();

            if ($request->has('approve')) {
                $pengajuan->forceFill([
                    'status' => MasterStatusPengajuan::V_PENGGALANGAN,
                ])
                    ->save();

                session()->flash('message_type', 'info');
                session()->flash('message', "Proyek berhasil disetujui");
                DB::commit();

                return redirect()->route('penggalangan_dana.manage');
            }

            session()->flash('message_type', 'success');
            session()->flash('message', "Proyek berhasil disimpan");

            DB::commit();

            return redirect()->route('penggalangan_dana.manage');
        } catch (Exception $exception) {
            DB::rollBack();
            $deletePaths->each(function ($path) {
                Storage::delete("public/$path");
            });

            throw $exception;
        }
    }

    private function createValidator($data)
    {
        $validator = Validator::make($data, [
            'nama' => ['required', 'max:191'],
            'alamat' => ['required', 'max:191'],
            'geocode' => ['required', 'max:191'],
            'akad' => ['required', Rule::exists('m_akad', 'id')],
            'profit_margin' => ['required', 'min:1', 'max:99'],
            'harga_paket' => ['required', 'numeric', 'min:1000000', function ($attribute, $value, $fail) {
                if ($value % 1000000 !== 0) {
                    $fail($attribute . ' harus kelipatan 1.000.000'); // your message
                }
            }],
            'terkumpul' => ['required', 'numeric', 'min:0'],
            'tgl_mulai_penggalangan' => ['required', 'date', 'after_or_equal:today'],
            'tgl_selesai_penggalangan' => ['required', 'date', 'after:tgl_mulai_penggalangan'],
            // 'tgl_mulai' => ['required', 'date', 'gt:tgl_selesai_penggalangan'],
            // 'tgl_selesai' => ['required', 'date', 'gt:tgl_mulai'],
            'tenor_waktu' => ['required', 'numeric', 'min:1', 'max:180'],
            'status_tampil' => ['required', 'in:0,1'],
            'desc' => ['required', 'array'],
            'desc.deskripsi' => ['required'],
            'desc.legalitas' => ['required'],
        ], [], [
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'geocode' => 'GeoCode',
            'akad' => 'Akad',
            'profit_margin' => 'Profit Margin',
            'harga_paket' => 'Harga Paket',
            'terkumpul' => 'Dana Terkumpul',
            'tgl_mulai_penggalangan' => 'Tgl Mulai Penggalangan',
            'tgl_selesai_penggalangan' => 'Tgl Selesai Penggalangan',
            'tgl_mulai' => 'Tgl Mulai',
            'tgl_selesai' => 'Tgl Selesai',
            'tenor_waktu' => 'Tenor Waktu',
            'status_tampil' => 'Status Tampil',
            'desc.deskripsi' => 'Deskripsi',
            'desc.pemilik' => 'Pemilik',
            'desc.legalitas' => 'Legalitas',
        ]);

        return $validator;
    }
}
