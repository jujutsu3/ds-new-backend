<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Guzzle
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Borrower;
use App\BorrowerPengajuan;
use App\BorrowerPendanaan;
use App\BorrowerPersyaratanInsert;
use App\BorrowerPersyaratanPendanaan;
use App\BorrowerJaminan;
use App\MasterJenisJaminan;
use DB;

// Model
use App\BorrowerTipePendanaan;
use App\GambarProyek;


class AdminClientController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth:api', ['except'=>[]]);
  }
  
  
  // Start Client Jenis Pendanaan Here
  public function getTableJenisBorrower()
  {
    
    $client = new Client();
    $request = $client->get(config('app.apilink')."/borrower-admin/client-side/jenisPendanaanPage");
    $response = $request->getBody()->getContents();
    // return response()->json($request->getBody()->getContents());
    return response($response);
  }

	public function tableGetBorrower()
	{    
		// $client = new Client();
		// $request = $client->get(config('app.apilink')."/borrower-admin/client-side/tableBorrowerData");
		// $response = $request->getBody()->getContents();

		$getDataPengajuan = BorrowerPengajuan::select('brw_pengajuan.brw_id', 'brw_pengajuan.id_proyek', 'brw_pengajuan.status', 'brw_pengajuan.pendanaan_tipe', 'brw_pengajuan.pengajuan_id',
		DB::raw('(CASE WHEN brw_pengajuan.status = 0 THEN "Pengajuan Baru" WHEN brw_pengajuan.status = 2 THEN "Pengajuan Ditolak" WHEN brw_pengajuan.status = 3 THEN "Menunggu Persetujuan" ELSE "Tidak Disetujui oleh Pak Atis" END) AS status_proyek'),
		'brw_pengajuan.pendanaan_nama', 'brw_pengajuan.keterangan_approval','brw_user_detail.nama','brw_user_detail.brw_type','brw_user_detail.nm_bdn_hukum','brw_pengajuan.created_at',
		'brw_user_detail.ktp','brw_scorring_personal.nilai as nilai_personal', 'brw_scorring_pendanaan.scorring_nilai as nilai_pendanaan')
			->leftjoin('brw_user','brw_user.brw_id','=','brw_pengajuan.brw_id')
			->leftjoin('brw_user_detail','brw_user_detail.brw_id','=','brw_pengajuan.brw_id')
			->leftjoin('brw_scorring_personal','brw_scorring_personal.brw_id','=','brw_pengajuan.brw_id')
			->leftjoin('brw_scorring_pendanaan','brw_scorring_pendanaan.pengajuan_id','=','brw_pengajuan.pengajuan_id')
			->whereNotIn('brw_pengajuan.status', [1])
			->orderBy('pengajuan_id', 'desc');

		$getDataPendanaan = BorrowerPendanaan::select('brw_pendanaan.brw_id', 'brw_pendanaan.id_proyek', 'brw_pendanaan.status', 'brw_pendanaan.pendanaan_tipe', 'brw_pendanaan.pengajuan_id',
		DB::raw('(CASE WHEN brw_pendanaan.status = 0 THEN "Pendanaan Aktif" WHEN brw_pendanaan.status = 1 THEN "Penggalangan Dana" WHEN brw_pendanaan.status = 2 THEN "Proyek Berjalan" WHEN brw_pendanaan.status = 6 THEN "Menunggu TTD Akad" ELSE "Proyek Selesai" END) AS status_proyek'),
		'brw_pendanaan.pendanaan_nama', 'brw_pendanaan.keterangan_approval','brw_user_detail.nama','brw_user_detail.brw_type','brw_user_detail.nm_bdn_hukum','brw_pendanaan.created_at',
		'brw_user_detail.ktp','brw_scorring_personal.nilai as nilai_personal', 'brw_scorring_pendanaan.scorring_nilai as nilai_pendanaan')
			->leftjoin('brw_user','brw_user.brw_id','=','brw_pendanaan.brw_id')
			->leftjoin('brw_user_detail','brw_user_detail.brw_id','=','brw_pendanaan.brw_id')
			->leftjoin('brw_scorring_personal','brw_scorring_personal.brw_id','=','brw_pendanaan.brw_id')
			->leftjoin('brw_scorring_pendanaan','brw_scorring_pendanaan.pengajuan_id','=','brw_pendanaan.pengajuan_id')
			->union($getDataPengajuan)
			->get();

		$i = 1;
		$data= array();

		foreach($getDataPendanaan as $item){

			if($item->created_at == NULL){
				$created_at = "-";
			}else{
				$date=date_create($item->created_at);
				$created_at = date_format($date,"d-m-Y");
			}

			if($item->brw_type == 2){
				$judulnama = $item->nm_bdn_hukum;
			}else{
				$judulnama = $item->nama;
			}
			$column['no'] = (string) $i++;
			$column['id'] = (string) $item->brw_id;
			$column['idPengajuan'] = (string) $item->pengajuan_id;
			$column['pendanaanBorrower'] = (string) $item->pendanaan_nama;
			$column['namaBorrower'] = (string) $judulnama;
			$column['tgl_pengajuan'] = (string) $created_at;
			$column['ktp'] = (string) $item->ktp;
			$column['nilaiBorrower'] = (string) $item->nilai_personal;
			$column['nilaiPendanaan'] = (string) $item->nilai_pendanaan;
			$column['brw_type'] = (string) $item->brw_type;
			$column['keterangan_approval'] = (string) $item->keterangan_approval;
			$column['status_proyek'] = (string) $item->status_proyek;
			$column['status'] = (string) $item->status;
			$column['pendanaan_tipe'] = (string) $item->pendanaan_tipe;
			$column['id_proyek'] = (string) $item->id_proyek;

			$data[] = $column;   
		}
		$parsingJSON = array('data' => $data);

		// echo json_encode($parsingJSON);
    
		return response($parsingJSON);
	}
  
	public function getDataBorrower(){
		
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataBorrower");
		$response = $request->getBody()->getContents();
    
		return response($response);
	}
	public function getDetailsBorrower($borrower_id){
		
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/getDetailsBorrower/".$borrower_id."");
		$response = $request->getBody()->getContents();
    
		return response($response);
	}
	
	/**************************** DATA *********************************************/
	
	public function DataPendidikan(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataPendidikan/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataJenisKelamin(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataJenisKelamin/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataAgama(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataAgama/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataNikah(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataNikah/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataProvinsi(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataProvinsi/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataKota($kota){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataKota/$kota");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function GantiDataKota($kota){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/GantiDataKota/$kota");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	// public function GantiDataKota($kota){
        
		// $client = new Client();
		// $request = $client->get(config('app.apilink')."/borrower-admin/client-side/GantiDataKota/$kota");
		// $response = $request->getBody()->getContents();
    
		// return response($response);
        
    // }
	
	public function DataBank(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataBank/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataPekerjaan(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataPekerjaan/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataBidangPekerjaan(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataBidangPekerjaan/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataBidangOnline(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataBidangOnline/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataPengalaman(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataPengalaman/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
    }
	
	public function DataPendapatan(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataPendapatan/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
	}
	
	public function DataDokumenBorrower(){
        
		$client = new Client();
		$request = $client->get(config('app.apilink')."/borrower-admin/client-side/DataDokumenBorrower/");
		$response = $request->getBody()->getContents();
    
		return response($response);
        
	}
	
	public function GetUbahVerifBorrower($id_pengajuan, $id_brw){

		$getData = BorrowerPengajuan::select('brw_pengajuan.brw_id','brw_pengajuan.pengajuan_id','brw_pengajuan.pendanaan_nama','brw_pengajuan.estimasi_mulai',
		'brw_pengajuan.pendanaan_dana_dibutuhkan','brw_pengajuan.estimasi_imbal_hasil','brw_pengajuan.durasi_proyek', 'brw_pengajuan.pendanaan_tipe', 'brw_pengajuan.status',
		'brw_pengajuan.pendanaan_akad', 'brw_pengajuan.lokasi_proyek', 'brw_pengajuan.geocode', 'brw_pengajuan.gambar_utama', 'brw_pengajuan.keterangan', 'brw_pengajuan.detail_pendanaan')
		->where('brw_pengajuan.pengajuan_id', $id_pengajuan)
		->first();

		$master_tipe_pendanaan = BorrowerTipePendanaan::select('tipe_id', 'pendanaan_nama')->orderBy('tipe_id')->get();

		$master_jenis_jaminan = MasterJenisJaminan::select('id_jenis_jaminan', 'jenis_jaminan')->orderBy('id_jenis_jaminan')->get();

		$gambar_proyek_detil = GambarProyek::select('id', 'pengajuan_id', 'gambar')->where('pengajuan_id', '=', $id_pengajuan)->orderBy('id', 'desc')->limit(2)->get();

		$master_kantor_penerbit = DB::table('m_kode_pos')->select('kota')->distinct()->get();

		$brw_rekening = DB::table('brw_rekening')->select('brw_id', 'total_sisa')->where('brw_id', '=', $id_brw)->first();
    
		return response()->json(['data_edit' => $getData, 'data_master'=>$master_tipe_pendanaan, 'data_jenis_jaminan'=>$master_jenis_jaminan, 'data_kantor_penerbit'=>$master_kantor_penerbit, 'gambar_proyek_detil'=>$gambar_proyek_detil, 'data_rekening'=>$brw_rekening]);
	}

	public function GetLihatDokumen($id_pengajuan, $id_brw, $pendanaan_tipe, $brw_type){

		$check_existing = BorrowerPersyaratanInsert::where('brw_persyaratan_insert.brw_id', $id_brw)->where('brw_persyaratan_insert.tipe_id', $pendanaan_tipe)->where('brw_persyaratan_insert.user_type', $brw_type)->count();

		if($check_existing>0){
			$getData = BorrowerPersyaratanInsert::select('brw_persyaratan_insert.persyaratan_id', 'brw_persyaratan_insert.checked', 'brw_persyaratan_pendanaan.persyaratan_mandatory', 'brw_persyaratan_pendanaan.persyaratan_nama',
			'brw_persyaratan_insert.tipe_id')
			->join('brw_persyaratan_pendanaan', 'brw_persyaratan_insert.persyaratan_id', '=', 'brw_persyaratan_pendanaan.persyaratan_id')
			->where('brw_persyaratan_insert.brw_id', $id_brw)
			->where('brw_persyaratan_insert.tipe_id', $pendanaan_tipe)
			->where('brw_persyaratan_insert.user_type', $brw_type)
			->get();
		}else{
			$getData = BorrowerPersyaratanPendanaan::select('brw_persyaratan_pendanaan.persyaratan_id', 'brw_persyaratan_pendanaan.persyaratan_mandatory', 'brw_persyaratan_pendanaan.persyaratan_mandatory as checked', 'brw_persyaratan_pendanaan.persyaratan_nama',
			'brw_persyaratan_pendanaan.tipe_id')
			->where('brw_persyaratan_pendanaan.tipe_id', $pendanaan_tipe)
			->where('brw_persyaratan_pendanaan.user_type', $brw_type)
			->get();
		}
    
		return response()->json($getData);
	}
	
	public function tableGetJaminan($id_pengajuan){

		$jaminan = BorrowerJaminan::select('brw_jaminan.jaminan_id', 'brw_jaminan.pengajuan_id', 'brw_jaminan.jaminan_nama', 'brw_jaminan.jaminan_nomor', 'brw_jaminan.jaminan_jenis',
		'brw_jaminan.jaminan_nilai', 'brw_jaminan.jaminan_detail', 'm_jenis_jaminan.jenis_jaminan', 'brw_jaminan.kantor_penerbit', 'brw_jaminan.NOP', 'brw_jaminan.sertifikat', 'brw_jaminan.status')
		->leftJoin('m_jenis_jaminan', 'brw_jaminan.jaminan_jenis', '=', 'm_jenis_jaminan.id_jenis_jaminan')
		->where('brw_jaminan.pengajuan_id', '=', $id_pengajuan)->get();

		$i = 1;
		$data = array();

		foreach($jaminan as $value){
			$column['no'] = (string) $i++;
			$column['jaminan_id']=(string)$value->jaminan_id;
			$column['pengajuan_id']=(string)$value->pengajuan_id;
			$column['jaminan_nama']=(string)$value->jaminan_nama;
			$column['jaminan_nomor']=(string)$value->jaminan_nomor;
			$column['jenis_jaminan']=(string)$value->jenis_jaminan;
			$column['jaminan_nilai']=(string)$value->jaminan_nilai;
			$column['jaminan_detail']=(string)$value->jaminan_detail;
			$column['jaminan_jenis']=(string)$value->jaminan_jenis;
			$column['kantor_penerbit']= (string)$value->kantor_penerbit;
			$column['nomor_objek_pajak']=(string)$value->NOP;
			$column['sertifikat']=(string)$value->sertifikat;
			$column['status']=(string)$value->status;
			$data[] = $column;   
		}

		
		$data_jaminan = array('data' => $data);
		return response($data_jaminan);
	}
	
	/**************************** END DATA *****************************************/
	

  
}
