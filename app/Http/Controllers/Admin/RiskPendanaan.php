<?php

namespace App\Http\Controllers\Admin;

use Auth;

use App\BorrowerPengajuan;
use App\BorrowerDetailNaup;
use App\BorrowerDetailBiaya;
use App\BorrowerAnalisaPendanaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RiskPendanaan extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->only(['listproyek', 'risk_deviasi']);
    }

    public function ListPengajuan()
    {

        $query = \DB::table('brw_analisa_pendanaan');
        $query->join('brw_pengajuan', 'brw_analisa_pendanaan.pengajuan_id', 'brw_pengajuan.pengajuan_id');
        $query->join('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
        $query->leftJoin('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
        $query->selectRaw('brw_pengajuan.brw_id, brw_analisa_pendanaan.pengajuan_id, tanggal_pengajuan AS tgl_pengajuan, tanggal_verifikasi, penerima_pendanaan,  
                            brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan, m_tujuan_pembiayaan.tujuan_pembiayaan,  brw_pengajuan.pendanaan_dana_dibutuhkan AS nilai_pengajuan, status_risk');
        $query->whereIn('brw_analisa_pendanaan.status_risk', [10,11,12]);


        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->get();
        $response = ['data' => $pengajuan];
        return response()->json($response);
    }

    public function listproyek()
    {
        return view('pages.admin.adrRisk_listproyek');
    }

    public function doPekerjaanRiskUpdate(Request $request){

       try {
           $deviasi = $request->deviasi;
           if($deviasi) {
                foreach ($deviasi as $key => $val) {
                    \DB::table('brw_dtl_deviasi')->where('pengajuan_id', $request->pengajuan_id)->where('id', $val['id'])->update(['resiko'=>$val['resiko'],'mitigasi'=>$val['mitigasi']]);
               }
           }

            $nama_risk = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;

            $data = \DB::select('call proc_click_button_borrower(?, ?, ?, ?, ?, ?)', ['7', $nama_risk, $request->pengajuan_id, 'simpan', 'RiskPendanaan.php', '61']);
            $result_status = $data[0]->sout;
            if($result_status == '1'){
                $response = ['status' => 'success', 'msg' => 'Data berhasil disimpan'];
            }else{
                $response = ['status' => 'failed', 'msg' => 'Data status gagal disimpan'];
            }
           return $response;

        }catch (\Exception $e) { 
           return ['status' => 'failed', 'msg' => $e->getMessage()];
        }


    }

    public function kirimDeviasiData(Request $request){
        try {

            $nama_risk = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
            $data = \DB::select('call proc_click_button_borrower(?, ?, ?, ?, ?, ?)', ['7', $nama_risk, $request->pengajuan_id, 'kirim', 'RiskPendanaan.php', '81']);
            $result_status = $data[0]->sout;
            return $result_status;

        }catch(\Exception $e){
            return ['status' => 'failed', 'msg' => $e->getMessage()];
        }

    }

    public function risk_deviasi($pengajuan_id)
    {

       
        $pengajuan = BorrowerPengajuan::from('brw_pengajuan as a')
            ->leftJoin('brw_tipe_pendanaan', 'a.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id')
            ->leftJoin('brw_user_detail_penghasilan_pengajuan as c', 'c.pengajuan_id', 'a.pengajuan_id')
            ->leftJoin('brw_user_detail_pengajuan as d', 'd.pengajuan_id', 'a.pengajuan_id')
            ->leftJoin('brw_hd_deviasi as e', 'e.pengajuan_id', 'a.pengajuan_id')
            ->leftJoin('brw_pasangan as f', 'f.pasangan_id', 'a.brw_id')
            ->leftJoin('brw_dtl_lembar_pendapatan as g', 'g.id_pengajuan', 'a.pengajuan_id')
            ->leftJoin('brw_agunan as h', 'h.id_pengajuan', 'a.pengajuan_id')
            ->leftJoin('m_tujuan_pembiayaan as m_tujuan_pembiayaan', 'a.pendanaan_tujuan', 'm_tujuan_pembiayaan.id')
            ->selectRaw(
                'c.sumber_pengembalian_dana,
                c.nama_perusahaan,
                f.nama_perusahaan as nama_perusahaan_pasangan,
                c.nama_hrd,
                c.no_fixed_line_hrd,
                c.skema_pembiayaan,
                c.status_pekerjaan as status_pekerjaan,
                f.status_pekerjaan as status_pekerjaan_pasangan,
                c.sumber_pengembalian_dana,
                c.alamat_perusahaan as c_alamat_perusahaan,
                c.provinsi as c_provinsi,
                c.kelurahan as c_kelurahan,
                c.kecamatan as c_kecamatan,
                c.kab_kota as c_kab_kota,
                c.kode_pos as c_kode_pos,
                c.jabatan as jabatan,
                c.usia_perusahaan,
                c.usia_tempat_usaha,
                f.masa_kerja_tahun as masa_kerja_tahun_pasangan,
                f.masa_kerja_bulan as masa_kerja_bulan_pasangan,
                f.jabatan as jabatan_pasangan,
                c.rt,
                c.rw,
                d.nama,
                d.bidang_pekerjaan,
                d.tgl_lahir,
                f.tgl_lahir as tgl_lahir_pasangan,
                a.*,
                f.nama as nama_pasangan,
                brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan,
                date_format(DATE_SUB(date_format(NOW(),"%Y%m%d"), INTERVAL ifnull(c.masa_kerja_tahun*12+c.masa_kerja_bulan,0) Month),"%Y") as lama_bekerja,
                date_format(DATE_SUB(date_format(NOW(),"%Y%m%d"), INTERVAL ifnull(f.masa_kerja_tahun*12+f.masa_kerja_bulan,0) Month),"%Y") as lama_bekerja_pasangan,
                g.cr_maks,h.*, e.nama_risk, e.created_at as tanggal_deviasi, m_tujuan_pembiayaan.tujuan_pembiayaan as nama_produk'
            )
            ->where('a.pengajuan_id', $pengajuan_id)
            ->first();
          
        $detail_biaya = BorrowerDetailBiaya::where('pengajuan_id', $pengajuan_id)->orderBy('id', 'asc')->get();
        $detail_deviasi = \DB::table('brw_dtl_deviasi')->where('pengajuan_id', $pengajuan_id)->get();
        $pengikatan_pendanaan = \DB::table('m_pengikatan_pendanaan')->pluck('jenis_pengikatan', 'id');
        $pengikatan_jaminan = \DB::table('m_pengikatan_jaminan')->pluck('jenis_pengikatan', 'id');
        $hasilAnalisa = \DB::table('brw_dtl_lembar_hasil_analisa')->where('id_pengajuan', $pengajuan_id)->first();
               
        $margin = 0;
        $total_kewajiban_penerima = 0;
        $margin = $pengajuan->persentase_margin * $hasilAnalisa->plafond_rekomendasi;
        $total_kewajiban_penerima = $margin + $hasilAnalisa->plafond_rekomendasi;

        $naup = BorrowerDetailNaup::where('pengajuan_id', $pengajuan_id)->first();
        if(!$naup) $naup = new BorrowerDetailNaup();

        $agunan = \DB::table('brw_agunan')->where('id_pengajuan', $pengajuan_id)->first();
        $agunan->alamat_sertifikat = $pengajuan->lokasi_proyek . ', No. ' . $agunan->blok_nomor . ' RT. ' .
            $agunan->RT . '/ RW.' . $agunan->RW . ', Ds./Kel.' . $pengajuan->kelurahan . ', Kec. ' . $pengajuan->kecamatan . ', Kab/Kota. ' . $pengajuan->kota;

        $tenor = ($pengajuan->pendanaan_tipe == '2') ? $pengajuan->durasi_proyek/12 : $pengajuan->durasi_proyek;
        $pengajuan->alamat_sertifikat = $pengajuan->lokasi_proyek . ' ' . $pengajuan->RT . ' ' . $pengajuan->RW;

        if($pengajuan->pendanaan_tipe == '2'){
            $data_kalkulator_kpr = LegalitasPendanaan::kalkulatorKpr($pengajuan->pendanaan_dana_dibutuhkan,  $pengajuan->persentase_margin , $pengajuan->durasi_proyek);
            $result_simulasi =  LegalitasPendanaan::newSimulasiAngsuran($data_kalkulator_kpr);
          
            $simulasi_angsuran = $result_simulasi['total_angsuran']; 
            $simulasi_margin = $result_simulasi['angsuran_margin'];
            $simulasi_total_margin = $result_simulasi['total_margin'];
        }

        $status_risk = BorrowerAnalisaPendanaan::where('pengajuan_id', $pengajuan->pengajuan_id)->value('status_risk');

        switch($pengajuan->jenis_objek_pendanaan) {
            case '1': 
                $pengajuan->jenis_objek_pendanaan = 'Tanah Kosong';
                break;
            case '2': 
                $pengajuan->jenis_objek_pendanaan = 'Tanah Dan Bangunan';
                break;    
            default:
                $pengajuan->jenis_objek_pendanaan = '-';
               break;    
        }
      
        $persentase_plafon_pembiayaan = ($naup  && $hasilAnalisa->plafond_rekomendasi > 0) ?  ($naup->nilai_pengikatan_agunan/$hasilAnalisa->plafond_rekomendasi) * 100 : 0;
        return view('pages.admin.adrRisk_deviasi', compact('pengajuan', 'agunan', 'status_risk', 'naup', 'hasilAnalisa', 'simulasi_angsuran', 'simulasi_total_margin', 'simulasi_margin', 'margin', 'total_kewajiban_penerima', 'detail_biaya', 'detail_deviasi', 'pengikatan_pendanaan', 'pengikatan_jaminan', 'persentase_plafon_pembiayaan'));
    }
}
