<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Proyek;
use App\GambarProyek;

use App\DetilImbalUser;
use App\ListImbalUser;
use App\Log_Imbal_User;
use App\deskripsi_proyek;
use App\legalitas_proyek;
use App\pemilik_proyek;
use App\simulasi_proyek;
use App\Investor;
use App\Marketer;
use App\HariLibur;
use App\AuditTrail;
use DateTime;
use DatePeriod;
use DateInterval;

use App\IhPendanaanAktif;
use App\IhListImbalUser;


use Excel;
use App\Exports\MutasiInvestorProyek;
use App\Exports\DetilByProyek;
use App\Exports\DetilProyekExport;
use App\Exports\DetilByDate;
use App\Exports\PayoutExport;
use App\Exports\Remitproexport;
use App\Exports\IhListImbalUserExport;
// use Maatwebsite\Excel\Facades\Excel;

use App\PemilikPaket;
use App\RekeningInvestor;
use App\Http\Middleware\UserCheck;
use App\ProgressProyek;
use App\ManageCarousel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use function GuzzleHttp\json_encode;
use Cart;
use Auth;
use App;
use DB;
use App\LogPengembalianDana;
use App\MutasiInvestor;
use App\Events\MutasiInvestorEvent;
use App\Jobs\KembaliDana;
use App\eListimbalhasil;
use App\News;
use App\Exports\PayoutDate;
use App\Exports\ProyekAllData;
use App\Exports\ListImbalUserExport;
use App\Http\Middleware\NotifikasiProyek;
use App\Http\Middleware\StatusProyek;
use App\TmpSelectedProyek;
use App\TermCondition;
use App\TestimoniPendana;
use Response;
use ZipArchive;
use File;


class ImbalHasilAdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->only(['dashboard_generate', 'cetak_payout']);
    }

    public function dashboard_generate()
    {
        return view('pages.admin.imbalhasil_dashboard_generate');
    }

    public function imbalhasil_DaftarProyekReady()
    {
        $proyeks = DB::SELECT("select * from `proyek` left join (SELECT proyek_id, count(*) AS total FROM pendanaan_aktif WHERE status = 1 GROUP BY proyek_id) as total_proyek on `proyek_id` = `id` where `proyek`.`status` in (2, 3, 4) and `proyek`.`tgl_selesai` >= DATE_ADD(DATE(NOW()), INTERVAL -7 DAY) order by `proyek`.`id` DESC");

        $response = ['data' => $proyeks];
        return response()->json($response);
    }

    public function generateImbalHasil($id)
    {
        DB::select("CALL proc_imbalhasil($id)");
        $getdata = 1;
        $response = ['data' => $getdata];
        return response()->json($response);
    }

    // public function admin_detil_payout($id){
    public function detil_daftarpayout($id)
    {
        $imbalhasil = DB::SELECT("SELECT a.id,a.tanggal_payout, a.ket_libur, a.status_payout,a.keterangan_payout
								  FROM 	ih_list_imbal_user a
										LEFT JOIN pendanaan_aktif b ON a.pendanaan_id = b.id
								  WHERE b.proyek_id = $id AND keterangan_payout IN (1,2,3) AND a.imbal_payout != 0 AND b.status = 1
								  GROUP BY a.keterangan_payout, a.tanggal_payout
								  ORDER BY a.id");

        $getbulan1 = DB::SELECT("SELECT a.tanggal_payout FROM ih_list_imbal_user a LEFT JOIN ih_detil_imbal_user b ON a.detilimbaluser_id = b.id WHERE b.proyek_id = $id AND keterangan_payout = 1 GROUP BY a.tanggal_payout ORDER BY a.id limit 1");

        $bulan1 = $getbulan1[0]->tanggal_payout;

        $response = ['data_payout' => $imbalhasil, 'bulan1' => $bulan1];
        return response()->json($response);
    }

    // public function detil_month_payout(Request $request)
    public function list_payout_pendana(Request $request)
    {
        $data = IhListImbalUser::where('ih_detil_imbal_user.proyek_id', $request->data_id)
            ->where('ih_list_imbal_user.tanggal_payout', $request->date_id)
            ->where('ih_list_imbal_user.keterangan_payout', $request->flag_id)
            ->where('pendanaan_aktif.status', 1)
            ->leftJoin('ih_detil_imbal_user', 'ih_detil_imbal_user.id', '=', 'ih_list_imbal_user.detilimbaluser_id')
            ->leftJoin('pendanaan_aktif', 'pendanaan_aktif.id', '=', 'ih_detil_imbal_user.pendanaan_id')
            ->leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'pendanaan_aktif.investor_id')
            ->orderby('ih_detil_imbal_user.id', 'ASC')
            ->select([
                'ih_list_imbal_user.id',
                'ih_detil_imbal_user.pendanaan_id',
                'detil_investor.nama_investor',
                'ih_list_imbal_user.tanggal_payout',
                'ih_list_imbal_user.imbal_payout',
                'ih_detil_imbal_user.proposional',
                'ih_detil_imbal_user.sisa_imbal',
                'pendanaan_aktif.total_dana',
                'ih_list_imbal_user.keterangan',
                'ih_list_imbal_user.status_payout',
                'ih_list_imbal_user.nominal_pajak',
            ])->get();

        $jmlpendana = count($data);
        //    dd($data);
        //    die();
        $response = ['data' => $data, 'jmlpendana' => $jmlpendana];
        return response()->json($response);
    }

    public function kirim_imbal_hasil(Request $request)
    {
        if ($request->jmlpendana == count($request->status_id)) {
            $status = implode("|", $request->status_id);
            // echo "CALL proc_kirimimbal(".'$status'.",".$request->data_id.",".'$request->date_id'.",".$request->flag_id.")";
            DB::select("CALL proc_kirimimbal('$status',$request->data_id,'$request->date_id',$request->flag_id)");
            $status = 1;
        } else {
            $status = 0; //gagal
        }

        $data = ['sukses' => $status];
        return response()->json($data);
    }

    public function update_imbal_hasil(Request $request)
    {
        $jmlstatliu = count($request->statliu);
        if ($request->jmlpendana == $jmlstatliu) {
            $status = implode("|", $request->statliu);
            //echo "CALL proc_updateimbal(".$status.",".$request->data_id.",".$request->date_id.",".$request->flag_id.",".$jmlstatliu.")";
            DB::select("CALL proc_updateimbalhasil('$status',$request->data_id,'$request->date_id',$request->flag_id,$jmlstatliu)");
            $status = 1;
        } else {
            $status = 0; //gagal
        }
        $data = ['sukses' => $status];
        return response()->json($data);
    }

    public function payout7harikedepan($id)
    {
        $today = date("Y-m-d");
        switch ($id) {
            case "0":
                $date = $today;
                break;
            case "1":
                $date = (new DateTime('+1 day'))->format('Y-m-d');
                break;
            case "2":
                $date = (new DateTime('+2 day'))->format('Y-m-d');
                break;
            case "3":
                $date = (new DateTime('+3 day'))->format('Y-m-d');
                break;
            case "4":
                $date = (new DateTime('+4 day'))->format('Y-m-d');
                break;
            case "5":
                $date = (new DateTime('+5 day'))->format('Y-m-d');
                break;
            case "6":
                $date = (new DateTime('+6 day'))->format('Y-m-d');
                break;
            case "7":
                $date = (new DateTime('+7 day'))->format('Y-m-d');
                break;
            default:
                echo "gagal Men";
        }
        // echo $date;die();
        // $payoutseven = DB::select("SELECT proyek.nama, ih_list_imbal_user.tanggal_payout, datediff(ih_list_imbal_user.tanggal_payout,'$today') AS sisa_tanggal FROM proyek INNER JOIN ih_list_imbal_user ON proyek.id = ih_list_imbal_user.proyek_id WHERE tanggal_payout = '$date' GROUP BY proyek.nama ORDER BY ih_list_imbal_user.tanggal_payout ASC");
        $payoutseven = DB::select("SELECT proyek.nama, ih_list_imbal_user.tanggal_payout, datediff(ih_list_imbal_user.tanggal_payout,'$today') AS sisa_tanggal FROM proyek INNER JOIN ih_detil_imbal_user ON proyek.id = ih_detil_imbal_user.proyek_id INNER JOIN ih_list_imbal_user ON ih_detil_imbal_user.id = ih_list_imbal_user.detilimbaluser_id WHERE ih_list_imbal_user.tanggal_payout = '$date' AND ih_list_imbal_user.keterangan_payout IN (1,2) GROUP BY proyek.nama ORDER BY ih_list_imbal_user.tanggal_payout ASC");
        $response = ['payoutseven' => $payoutseven];
        return response()->json($response);
    }

    public function cetak_payout_mingguan(Request $request)
    {
        $today = date("Y-m-d");
        switch ($request->id) {
            case "0":
                $date = $today;
                break;
            case "1":
                $date = (new DateTime('+1 day'))->format('Y-m-d');
                break;
            case "2":
                $date = (new DateTime('+2 day'))->format('Y-m-d');
                break;
            case "3":
                $date = (new DateTime('+3 day'))->format('Y-m-d');
                break;
            case "4":
                $date = (new DateTime('+4 day'))->format('Y-m-d');
                break;
            case "5":
                $date = (new DateTime('+5 day'))->format('Y-m-d');
                break;
            case "6":
                $date = (new DateTime('+6 day'))->format('Y-m-d');
                break;
            case "7":
                $date = (new DateTime('+7 day'))->format('Y-m-d');
                break;
            default:
                echo "gagal Men";
        }
        return Excel::download(new PayoutExport($date), 'payoutexport.xlsx');
    }

    public function cetak_payout_mingguan_Remitpro(Request $request)
    {
        $today = date("Y-m-d");
        switch ($request->id) {
            case "0":
                $date = $today;
                break;
            case "1":
                $date = (new DateTime('+1 day'))->format('Y-m-d');
                break;
            case "2":
                $date = (new DateTime('+2 day'))->format('Y-m-d');
                break;
            case "3":
                $date = (new DateTime('+3 day'))->format('Y-m-d');
                break;
            case "4":
                $date = (new DateTime('+4 day'))->format('Y-m-d');
                break;
            case "5":
                $date = (new DateTime('+5 day'))->format('Y-m-d');
                break;
            case "6":
                $date = (new DateTime('+6 day'))->format('Y-m-d');
                break;
            case "7":
                $date = (new DateTime('+7 day'))->format('Y-m-d');
                break;
            default:
                echo "gagal Men";
        }
        return Excel::download(new Remitproexport($date), 'payoutexport_Remitpro-'.$date.'.xlsx');
    }

    public function cetak_data_payout(Request $request)
    {
        $LIU_id = $request->id;
        $getid_proyek = DB::select("SELECT a.proyek_id, c.nama FROM ih_detil_imbal_user a JOIN ih_list_imbal_user b ON a.id = b.detilimbaluser_id JOIN proyek c ON a.proyek_id = c.id WHERE b.id = $LIU_id");
        $id_proyek = $getid_proyek[0]->proyek_id;
        $nama = $getid_proyek[0]->nama;
        $getnonama = explode("/", $nama);
        $nonama = trim($getnonama[0], ".,-");
        // echo $id_proyek;die();

        return Excel::download(new IhListImbalUserExport($id_proyek), 'ImbalHasilUser-' . $nonama . '.xlsx');
    }

    public function cetak_payout(Request $request)
    {
        $proyekid = $request->id;
        $proyek = DB::select("SELECT nama from proyek where id=$proyekid ");
        $id_proyek = $request->id;
        $nama = $proyek[0]->nama;
        $getnonama = explode("/", $nama);
        $nonama = trim($getnonama[0], ".,-");

        return Excel::download(new IhListImbalUserExport($proyekid), 'ImbalHasilUser-' . $nonama . '.xlsx');
    }

    public function hari_libur()
    {

        $delete = HariLibur::truncate();


        $API_CEK = 'https://raw.githubusercontent.com/guangrei/Json-Indonesia-holidays/master/calendar.json';

        $client = new Client();
        $res = $client->request('GET', $API_CEK);

        $hari_libur = json_decode($res->getBody());
        $array = json_decode(json_encode($hari_libur), true);

        $data = array();
        foreach ($array as $index => $datas) {
            $data[] = [
                "tgl" => $index,
                "detail" => $datas
            ];
        }
        array_pop($data);

        //DIWALI-DEEPAVALI DAN MALAM TAHUN BARU TIDAK MASUK LIBUR NASIONAL
        foreach ($data as $key => $datas) {

            if ($datas['detail']['deskripsi'] == 'Diwali / Deepavali' or $datas['detail']['deskripsi'] == 'Malam Tahun Baru') {
                unset($data[$key]);
                $izero = array_values($data);
            }
        }

        for ($i = 0; $i < sizeof($izero); $i++) {

            $hari_libur_save = new HariLibur();
            $thn = substr($izero[$i]['tgl'], 0, 4);
            $bln = substr($izero[$i]['tgl'], 4, 2);
            $tgl = substr($izero[$i]['tgl'], 6, 2);
            $hari_libur_save->tgl_harilibur = $thn . '-' . $bln . '-' . $tgl;
            $hari_libur_save->deskripsi = $izero[$i]['detail']['deskripsi'];
            $hari_libur_save->save();
        }

        $year = date("Y");
        DB::select("CALL generate_sabtuminggu('$year')");
        $data = ['status' => 1];
        return response()->json($data);
    }

    public function cetak_payout_mingguan_BSINet(Request $request)
    {
        $today = date("Y-m-d");
        switch ($request->id) {
            case "0":
                $date = $today;
                break;
            case "1":
                $date = (new DateTime('+1 day'))->format('Y-m-d');
                break;
            case "2":
                $date = (new DateTime('+2 day'))->format('Y-m-d');
                break;
            case "3":
                $date = (new DateTime('+3 day'))->format('Y-m-d');
                break;
            case "4":
                $date = (new DateTime('+4 day'))->format('Y-m-d');
                break;
            case "5":
                $date = (new DateTime('+5 day'))->format('Y-m-d');
                break;
            case "6":
                $date = (new DateTime('+6 day'))->format('Y-m-d');
                break;
            case "7":
                $date = (new DateTime('+7 day'))->format('Y-m-d');
                break;
            default:
                echo "Failed date type";
        }


        // $fileText = "This is some text\nThis test belongs to my file download\nBooyah";


        $result = DB::table('ih_list_imbal_user as b')->select(
            DB::raw('CASE WHEN SUBSTR(proyek.tgl_mulai,6,2) = SUBSTR(b.tanggal_payout,6,2)-1 THEN ih_detil_imbal_user.proposional+b.imbal_payout ELSE b.imbal_payout END as total_payout'),
            DB::raw('CONCAT("",detil_investor.rekening) as rekening'),
            'm_bank.kode_bank',
            'm_bank.nama_bank',
            'm_bank.kode_bank_SKN_BSI',
            'detil_investor.warganegara',
            'detil_investor.nama_pemilik_rek',
            'investor.email',
            'ih_detil_imbal_user.proyek_id',
            'proyek.id_pemilik',
            'b.id AS idlistIH',
            DB::raw('CASE WHEN b.keterangan_payout = 1 THEN CONCAT("IH ",proyek.nama) WHEN b.keterangan_payout = 2 THEN CONCAT("SH ",proyek.nama) END AS keterangan')
        )
            ->rightJoin('ih_detil_imbal_user', 'b.detilimbaluser_id', '=', 'ih_detil_imbal_user.id')
            ->rightJoin('pendanaan_aktif', 'ih_detil_imbal_user.pendanaan_id', '=', 'pendanaan_aktif.id')
            ->rightJoin('proyek', 'ih_detil_imbal_user.proyek_id', '=', 'proyek.id')
            ->leftJoin('detil_investor', 'pendanaan_aktif.investor_id', '=', 'detil_investor.investor_id')
            ->leftJoin('m_bank', 'detil_investor.bank_investor', '=', 'm_bank.kode_bank')
            ->leftJoin('investor', 'pendanaan_aktif.investor_id', '=', 'investor.id')
            ->where('b.tanggal_payout', $date)
            ->where('pendanaan_aktif.status', 1)
            ->wherein('b.keterangan_payout', [1, 2])
            ->orderBy('ih_detil_imbal_user.proyek_id', 'ASC')
            ->orderBy('ih_detil_imbal_user.id', 'ASC')->get();

        $fileText = "";
        // $public_dir = public_path();
        $public_dir = storage_path('app/private/BSInet');
        if (!File::isDirectory($public_dir)) {
            File::makeDirectory($public_dir, 0777, true, true);
        }
        $hour = date('H:s');
        $MAX_ROW = 40;

        // Zip File Name
        // $zipFileName = "BSINet_payoutexport_" . $date . "_" . date('H:i') . ".zip";
        $zipFileName = "BSINet_payoutexport_" . $date . "_" . date('H-i') . ".zip";
        // Create ZipArchive Obj
        $zip = new ZipArchive;
        if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {


            $cnt = 0;
            $idx = 0;
            foreach ($result as $r) {
                $rekening = $r->rekening;
                $sender = "PT. DANA SYARIAH INDONESIA";
                $bene_name = $r->nama_pemilik_rek;
                $berita_transfer = $r->keterangan;
                $amount_transfer = $r->total_payout;
                $bene_bank_code = $r->kode_bank;
                $bene_bank_code_SKN = $r->kode_bank_SKN_BSI;
                $bene_bank_name = $r->nama_bank;
                $type_bank_penerima = 1;
                if ($r->warganegara == 0)
                    $bene_citizenship = 1;
                else
                    $bene_citizenship = 0;
                $statWn = $r->warganegara;
                // $ref = substr($r->keterangan,0,2)."-".$r->proyek_id."-".$r->id_pemilik;
                $ref = substr($r->keterangan, 0, 2) . $r->idlistIH;
                if ($bene_bank_code == "451")
                    $is_SKN = "N";
                else
                    $is_SKN = "Y";

                $fileText .= str_pad($rekening, 30, " ", STR_PAD_RIGHT) . "";
                $fileText .= str_pad($sender, 30, " ", STR_PAD_RIGHT) . "";
                $fileText .= str_pad(substr($bene_name, 0, 30), 30, " ", STR_PAD_RIGHT) . "";
                $fileText .= str_pad(substr($berita_transfer, 0, 30), 30, " ", STR_PAD_RIGHT) . "";
                $fileText .= str_pad(round($amount_transfer), 12, "0", STR_PAD_LEFT);
                $fileText .= str_pad($bene_bank_code, 3, "0", STR_PAD_RIGHT);
                $fileText .= str_pad($bene_bank_code_SKN, 8, "0", STR_PAD_RIGHT) . "";
                $fileText .= str_pad(substr($bene_bank_name, 0, 30), 30, " ", STR_PAD_RIGHT) . "";
                $fileText .= str_pad($type_bank_penerima, 1, "0", STR_PAD_RIGHT);
                $fileText .= str_pad($bene_citizenship, 1, "0", STR_PAD_RIGHT);
                $fileText .= str_pad($statWn, 1, "0", STR_PAD_RIGHT);
                $fileText .= str_pad($ref, 20, " ", STR_PAD_RIGHT);
                $fileText .= str_pad($is_SKN, 1);

                $cnt++;
                if ($cnt == $MAX_ROW) {



















                    // Add File in ZipArchive
                    $idx++;
                    $myName = "BSINet_payoutexport_" . $date . "_" . sprintf("%03d", $idx) . ".txt";
                    $zip->addFromString($myName, $fileText);

                    // reset counter
                    $cnt = 0;
                    $fileText = "";
                } else {
                    $fileText .= "\r\n";
                }
            }

            // check left row
            if ($cnt > 0) {
                // Add File in ZipArchive
                $idx++;
                $myName = "BSINet_payoutexport_" . $date . "_" . sprintf("%03d", $idx) . ".txt";
                $zip->addFromString($myName, $fileText);
            }







            $zip->close();



            // $headers = ['Content-type'=>'text/plain',   'Content-Disposition'=>sprintf('attachment; filename="%s"', $myName),
            //    'Content-Length'=>strlen($fileText)];
            // return Response::make($fileText, 200, $headers);


            return Response::download($public_dir . '/' . $zipFileName, $zipFileName, array('Content-Type: application/zip', 'Content-Length: ' . filesize($public_dir . '/' . $zipFileName)));
        } else {
            //  return Response:make(400);
        }

        // $myName = "BSINet_payoutexport_".$date.".txt";
        // $headers = ['Content-type'=>'text/plain',   'Content-Disposition'=>sprintf('attachment; filename="%s"', $myName),
        //    'Content-Length'=>strlen($fileText)];
        // return Response::make($fileText, 200, $headers);
    }

    public function cetak_payout_mingguan_cimb(Request $request)
    {
        $today = date("Y-m-d");
        switch ($request->id) {
            case "0":
                $date = $today;
                break;
            case "1":
                $date = (new DateTime('+1 day'))->format('Y-m-d');
                break;
            case "2":
                $date = (new DateTime('+2 day'))->format('Y-m-d');
                break;
            case "3":
                $date = (new DateTime('+3 day'))->format('Y-m-d');
                break;
            case "4":
                $date = (new DateTime('+4 day'))->format('Y-m-d');
                break;
            case "5":
                $date = (new DateTime('+5 day'))->format('Y-m-d');
                break;
            case "6":
                $date = (new DateTime('+6 day'))->format('Y-m-d');
                break;
            case "7":
                $date = (new DateTime('+7 day'))->format('Y-m-d');
                break;
            default:
                echo "Failed date type";
        }

        $result = DB::table('ih_list_imbal_user as b')->select(
            DB::raw('CASE WHEN SUBSTR(proyek.tgl_mulai,6,2) = SUBSTR(b.tanggal_payout,6,2)-1 THEN REPLACE(FLOOR(ih_detil_imbal_user.proposional+b.imbal_payout),",","") ELSE REPLACE(FLOOR(b.imbal_payout),",","") END as total_payout'),
            DB::raw('CONCAT("",detil_investor.rekening) as rekening'),
            'm_bank.kode_bank',
            'm_bank.nama_bank',
            'm_bank.kode_bank_SKN_BSI',
            'detil_investor.warganegara',
            'detil_investor.nama_pemilik_rek',
            'investor.email',
            'ih_detil_imbal_user.proyek_id',
            'proyek.id_pemilik',
            'b.id AS idlistIH',
            'detil_investor.tipe_pengguna',
            'detil_investor.bank_investor',
            DB::raw('CASE WHEN b.keterangan_payout = 1 THEN CONCAT("IH ",proyek.nama) WHEN b.keterangan_payout = 2 THEN CONCAT("SH ",proyek.nama) END AS keterangan')
        )
            ->rightJoin('ih_detil_imbal_user', 'b.detilimbaluser_id', '=', 'ih_detil_imbal_user.id')
            ->rightJoin('pendanaan_aktif', 'ih_detil_imbal_user.pendanaan_id', '=', 'pendanaan_aktif.id')
            ->rightJoin('proyek', 'ih_detil_imbal_user.proyek_id', '=', 'proyek.id')
            ->leftJoin('detil_investor', 'pendanaan_aktif.investor_id', '=', 'detil_investor.investor_id')
            ->leftJoin('m_bank', 'detil_investor.bank_investor', '=', 'm_bank.kode_bank')
            ->leftJoin('investor', 'pendanaan_aktif.investor_id', '=', 'investor.id')
            ->where('b.tanggal_payout', $date)
            ->where('pendanaan_aktif.status', 1)
            ->wherein('b.keterangan_payout', [1, 2])
            ->orderBy('ih_detil_imbal_user.proyek_id', 'ASC')
            ->orderBy('ih_detil_imbal_user.id', 'ASC')
            ->get();
        $totalpayout = DB::SELECT("SELECT func_allPayout(0,'" . $date . "') AS totalpembayaran");

        $allPayout =  ($totalpayout[0]->totalpembayaran == null) ? 0 : $totalpayout[0]->totalpembayaran;
        $fileName = 'SKN_CIMB_' . $date . '.csv';
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        //parameter email
        $dsiEmail = config('app.dsiEmailPayout');
        $dsiCIMB = config('app.dsiEmailPayoutCIMB');

        $columns = array('860009611100', 'Dana Syariah Indonesia', 'IDR', $allPayout, 'imbalhasil' . $date, $result->count(), str_replace('-', '', $date), $dsiEmail);
        $callback = function () use ($result, $columns, $fileName, $dsiCIMB) {

            // cekdirektory
            $path = storage_path('app/private/cimb/file');

            if (!File::isDirectory($path)) {
                File::makeDirectory($path, 0777, true, true);
            } else {
                File::deleteDirectory($path);
                File::makeDirectory($path, 0777, true, true);
            }

            $file = fopen(storage_path() . '/app/private/cimb/file/' . $fileName, 'w');
            fputcsv($file, $columns);

            foreach ($result as $task) {
                if ($task->bank_investor != '022') {
                    $row['rekening']  = ($task->rekening != null) ? str_replace(array('-', ',', "'"), "", $task->rekening) : 0;
                    $row['nama_pemilik_rek'] = ($task->nama_pemilik_rek == null) ? '-' : str_replace(array('-', ',', "'"), "", $task->nama_pemilik_rek);
                    $row['IDR']  = 'IDR';
                    $row['total_payout']  = $task->total_payout;
                    $row['keterangan']  = $task->keterangan;
                    $row['nama_bank']  = ($task->nama_bank == null) ? '-' : $task->nama_bank;
                    if ($task->warganegara == 0) {
                        $row['bene_citizenship'] = 'Y';
                        $row['statWn'] = 'Y';
                    } else {
                        $row['bene_citizenship'] = 'N';
                        $row['statWn'] = 'N';
                    }
                    $row['email']  = $task->email . ';' . $dsiCIMB;
                    if ($task->tipe_pengguna == 'personal') {
                        $row['tipe_pengguna']  = 1;
                    } elseif ($task->tipe_pengguna == 'company') {
                        $row['tipe_pengguna']  = 2;
                    } elseif ($task->tipe_pengguna == 'goverment') {
                        $row['tipe_pengguna']  = 3;
                    } else {
                        $row['tipe_pengguna'] = '-';
                    }

                    fputcsv($file, array($row['rekening'], $row['nama_pemilik_rek'], $row['IDR'], $row['total_payout'], $row['keterangan'], $row['nama_bank'], $row['bene_citizenship'], $row['statWn'], $row['email'], $row['tipe_pengguna']));
                }
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function cetak_payout_mingguan_cimbIN(Request $request)
    {
        $today = date("Y-m-d");
        switch ($request->id) {
            case "0":
                $date = $today;
                break;
            case "1":
                $date = (new DateTime('+1 day'))->format('Y-m-d');
                break;
            case "2":
                $date = (new DateTime('+2 day'))->format('Y-m-d');
                break;
            case "3":
                $date = (new DateTime('+3 day'))->format('Y-m-d');
                break;
            case "4":
                $date = (new DateTime('+4 day'))->format('Y-m-d');
                break;
            case "5":
                $date = (new DateTime('+5 day'))->format('Y-m-d');
                break;
            case "6":
                $date = (new DateTime('+6 day'))->format('Y-m-d');
                break;
            case "7":
                $date = (new DateTime('+7 day'))->format('Y-m-d');
                break;
            default:
                echo "Failed date type";
        }

        $resultCIMB = DB::table('ih_list_imbal_user as b')->select(
            DB::raw('CASE WHEN SUBSTR(proyek.tgl_mulai,6,2) = SUBSTR(b.tanggal_payout,6,2)-1 THEN REPLACE(FLOOR(ih_detil_imbal_user.proposional+b.imbal_payout),",","") ELSE REPLACE(FLOOR(b.imbal_payout),",","") END as total_payout'),
            DB::raw('detil_investor.rekening as rekening'),
            'm_bank.kode_bank',
            'm_bank.nama_bank',
            'm_bank.kode_bank_SKN_BSI',
            'detil_investor.warganegara',
            'detil_investor.nama_pemilik_rek',
            'investor.email',
            'ih_detil_imbal_user.proyek_id',
            'proyek.id_pemilik',
            'b.id AS idlistIH',
            'detil_investor.tipe_pengguna',
            DB::raw('CASE WHEN b.keterangan_payout = 1 THEN CONCAT("IH ",proyek.nama) WHEN b.keterangan_payout = 2 THEN CONCAT("SH ",proyek.nama) END AS keterangan')
        )
            ->rightJoin('ih_detil_imbal_user', 'b.detilimbaluser_id', '=', 'ih_detil_imbal_user.id')
            ->rightJoin('pendanaan_aktif', 'ih_detil_imbal_user.pendanaan_id', '=', 'pendanaan_aktif.id')
            ->rightJoin('proyek', 'ih_detil_imbal_user.proyek_id', '=', 'proyek.id')
            ->leftJoin('detil_investor', 'pendanaan_aktif.investor_id', '=', 'detil_investor.investor_id')
            ->leftJoin('m_bank', 'detil_investor.bank_investor', '=', 'm_bank.kode_bank')
            ->leftJoin('investor', 'pendanaan_aktif.investor_id', '=', 'investor.id')
            ->where('detil_investor.bank_investor', '022')
            ->where('b.tanggal_payout', $date)
            ->where('pendanaan_aktif.status', 1)
            ->wherein('b.keterangan_payout', [1, 2])
            ->orderBy('ih_detil_imbal_user.proyek_id', 'ASC')
            ->orderBy('ih_detil_imbal_user.id', 'ASC')
            ->get();
        $totalpayout = DB::SELECT("SELECT func_allPayout(1,'" . $date . "') AS totalpembayaran");

        $allPayout =  ($totalpayout[0]->totalpembayaran == null) ? 0 : $totalpayout[0]->totalpembayaran;
        $fileName = 'IN_CIMB_' . $date . '.csv';
        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        //parameter email
        $dsiEmail = config('app.dsiEmailPayout');
        $dsiCIMB = config('app.dsiEmailPayoutCIMB');

        $columns = array('860009611100', 'Dana Syariah Indonesia', 'IDR', $allPayout, 'imbalhasil' . $date, $resultCIMB->count(), str_replace('-', '', $date), $dsiEmail);
        $callback = function () use ($resultCIMB, $columns, $fileName, $dsiCIMB) {

            $file = fopen(storage_path() . '/app/private/cimb/file/' . $fileName, 'w');

            fputcsv($file, $columns);

            foreach ($resultCIMB as $task) {
                $row['rekening']  = ($task->rekening != null) ? str_replace(array('-', ',', "'"), "", $task->rekening) : 0;
                $row['nama_pemilik_rek'] = ($task->nama_pemilik_rek == null) ? '-' : str_replace(array('-', ',', "'"), "", $task->nama_pemilik_rek);
                $row['IDR']  = 'IDR';
                $row['total_payout']  = $task->total_payout;
                $row['keterangan']  = $task->keterangan;
                $row['email']  = $task->email . ';' . $dsiCIMB;

                fputcsv($file, array($row['rekening'], $row['nama_pemilik_rek'], $row['IDR'], $row['total_payout'], $row['keterangan'], $row['email']));
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function generateCimbZip(Request $request)
    {
        $today = date("Y-m-d");
        switch ($request->id) {
            case "0":
                $date = $today;
                break;
            case "1":
                $date = (new DateTime('+1 day'))->format('Y-m-d');
                break;
            case "2":
                $date = (new DateTime('+2 day'))->format('Y-m-d');
                break;
            case "3":
                $date = (new DateTime('+3 day'))->format('Y-m-d');
                break;
            case "4":
                $date = (new DateTime('+4 day'))->format('Y-m-d');
                break;
            case "5":
                $date = (new DateTime('+5 day'))->format('Y-m-d');
                break;
            case "6":
                $date = (new DateTime('+6 day'))->format('Y-m-d');
                break;
            case "7":
                $date = (new DateTime('+7 day'))->format('Y-m-d');
                break;
            default:
                echo "Failed date type";
        }

        $zipFileName = "CIMB_payoutexport_" . $date . "_" . date('H-i') . ".zip";
        $zip = new ZipArchive;

        if ($zip->open(storage_path('app/private/cimb/' . $zipFileName), ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
            $files = File::files(storage_path('app/private/cimb/file'));

            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }

        return Response::download(storage_path('app/private/cimb/' . $zipFileName), $zipFileName, array('Content-Type: application/zip', 'Content-Length: ' . filesize(storage_path('app/private/cimb/' . $zipFileName))));
    }
}
