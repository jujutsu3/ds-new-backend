<?php

namespace App\Http\Controllers\Admin;

use App\BorrowerAnalisaPendanaan;
use App\Http\Controllers\Controller;
use App\Mail\HasilAnalisaKelengkapanEmail;
use Illuminate\Http\Request;
use App\Mail\HasilVerifikasiKElayakanKprEmail;
use App\Services\VerifikatorService;
use App\BorrowerPengurus;
use Illuminate\Support\Facades\Input;
use Exception;
use Mail;
use DB;
use Excel;

use App\Helpers\Helper;
use App\BorrowerMaterialRequisitionHeader;
use App\BorrowerPemegangSaham;
use App\Borrower;
use App\Http\Controllers\MaterialOrdersController;
use App\Constants\BorrowerUserType;
use Yajra\DataTables\DataTables;
use App\Exports\ListPengajuanKelayakan;

use Illuminate\Support\Facades\Storage;

class AnalisaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->only(['listKelayakan', 'listKelengkapan', 'detailPengajuanKelengkapan', 'detailPengajuanKelayakan']);
    }

    public function getQueryListPengajuan(){

        //Pengajuan 
        $query = \DB::table('brw_pengajuan');
        $query->join('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
        $query->leftJoin('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
        $query->join('brw_user_detail', 'brw_pengajuan.brw_id', 'brw_user_detail.brw_id');
        $query->leftjoin('brw_verifikator_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_verifikator_pengajuan.pengajuan_id');
        $query->leftJoin('m_pekerjaan', 'm_pekerjaan.id_pekerjaan', 'brw_user_detail.pekerjaan');
        $query->selectRaw('brw_pengajuan.brw_id, m_pekerjaan.pekerjaan, brw_pengajuan.pendanaan_tipe,  
                    IF(brw_user_detail.brw_type = 1, UPPER(brw_user_detail.nama), UPPER(brw_user_detail.nm_bdn_hukum)) as nama_cust, 
                    brw_pengajuan.pengajuan_id,  brw_pengajuan.created_at AS tgl_pengajuan, 
                    brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan, m_tujuan_pembiayaan.tujuan_pembiayaan,  brw_pengajuan.pendanaan_dana_dibutuhkan AS nilai_pengajuan, 
                    brw_pengajuan.status as stts');            

        return $query;             

    }

    public function ListPengajuan($tipe_analisa)
    {

        $input = Input::all();

        //Pengajuan 
        $query = $this->getQueryListPengajuan();

        if ($tipe_analisa == 'kelengkapan') {
            $query->whereIn('brw_pengajuan.status', [1, 3, 11, 12]);
            // $query->where('brw_pengajuan.verified_dokumen', 1);
            $query->where('brw_verifikator_pengajuan.status_aktifitas', 3);
        } else {
            $query->whereNotIn('brw_pengajuan.status', [11, 3, 12, 99, 999]);
        }

        $query->whereIn('brw_pengajuan.pendanaan_tipe', [1, 2, 3]);


        if (isset($input['search_filter'])) {

            $filter_data = json_decode($input['search_filter']);

            if (isset($filter_data->filter_nama_penerima) && !empty($filter_data->filter_nama_penerima)) {
                $query->where('brw_user_detail.nama', 'like', '%' . $filter_data->filter_nama_penerima . '%');
            }

            if (!empty($filter_data->filter_tipe_id)) {
                $query->where('brw_pengajuan.pendanaan_tipe', $filter_data->filter_tipe_id);
            }

            if (isset($filter_data->filter_status) && $filter_data->filter_status <> '') {
                $query->where('brw_pengajuan.status', $filter_data->filter_status);
            }


            if (!empty($filter_data->filter_start_date) && !empty($filter_data->filter_end_date)) {
                $query->whereBetween('brw_pengajuan.created_at', [$filter_data->filter_start_date . " 00:00:00", $filter_data->filter_end_date . " 23:59:59"]);
            }
        }

        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->get();
        $response = ['data' => $pengajuan];
        return response()->json($response);
    }

    public function exportDataKelayakan()
    {
        
        $input = Input::all();
        $query = $this->getQueryListPengajuan();

        $query->whereNotIn('brw_pengajuan.status', [11, 3, 12, 99, 999]);
        $query->whereIn('brw_pengajuan.pendanaan_tipe', [1, 2, 3]);


        if (isset($input['search_filter'])) {

            $filter_data = json_decode($input['search_filter']);

            if (isset($filter_data->filter_nama_penerima) && !empty($filter_data->filter_nama_penerima)) {
                $query->where('brw_user_detail.nama', 'like', '%' . $filter_data->filter_nama_penerima . '%');
            }

            if (!empty($filter_data->filter_tipe_id)) {
                $query->where('brw_pengajuan.pendanaan_tipe', $filter_data->filter_tipe_id);
            }

            if (isset($filter_data->filter_status) && $filter_data->filter_status <> '') {
                $query->where('brw_pengajuan.status', $filter_data->filter_status);
            }


            if (!empty($filter_data->filter_start_date) && !empty($filter_data->filter_end_date)) {
                $query->whereBetween('brw_pengajuan.created_at', [$filter_data->filter_start_date . " 00:00:00", $filter_data->filter_end_date . " 23:59:59"]);
            }
        }

        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->get();

        $filename = 'ListPengajuan-' . date('Ymdhis') . '.xlsx';

        $files =   Storage::disk('public')->allFiles('export/analisa_kelayakan');
        if (count($files) > 100) {
            Storage::disk('public')->delete($files);
        }

        Excel::store(new ListPengajuanKelayakan($pengajuan), '/export/analisa_kelayakan/'.$filename, 'public');

        $path = url('storage/export/analisa_kelayakan/' . $filename);
        return response()->json(['download_file' => $path]);
    
    }

    public function data($tipe_analisa)
    {

        $input = Input::all();

        $query= $this->getQueryListPengajuan();

        if ($tipe_analisa == 'kelengkapan') {
            $query->whereIn('brw_pengajuan.status', [1, 3, 11, 12]);
            // $query->where('brw_pengajuan.verified_dokumen', 1);
            $query->where('brw_verifikator_pengajuan.status_aktifitas', 3);
        } else {
            $query->whereNotIn('brw_pengajuan.status', [11, 3, 12, 99, 999]);
        }

        $query->whereIn('brw_pengajuan.pendanaan_tipe', [1, 2, 3]);


        if (isset($input['search_filter'])) {

            $filter_data = json_decode($input['search_filter']);

            if (isset($filter_data->filter_nama_penerima) && !empty($filter_data->filter_nama_penerima)) {
                $query->where('brw_user_detail.nama', 'like', '%' . $filter_data->filter_nama_penerima . '%');
            }

            if (!empty($filter_data->filter_tipe_id)) {
                $query->where('brw_pengajuan.pendanaan_tipe', $filter_data->filter_tipe_id);
            }

            if (isset($filter_data->filter_status) && $filter_data->filter_status <> '') {
                $query->where('brw_pengajuan.status', $filter_data->filter_status);
            }


            if (!empty($filter_data->filter_start_date) && !empty($filter_data->filter_end_date)) {
                $query->whereBetween('brw_pengajuan.created_at', [$filter_data->filter_start_date . " 00:00:00", $filter_data->filter_end_date . " 23:59:59"]);
            }
        }

        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->get();

        return DataTables::of($pengajuan)
            ->addIndexColumn()
            ->make(true);
    }


    public function listKelengkapan()
    {
        $tipe_pendanaan = \DB::table('brw_tipe_pendanaan')->pluck('pendanaan_nama', 'tipe_id');
        return view('pages.admin.analisa.list_kelengkapan', compact('tipe_pendanaan'));
    }


    public function listKelayakan()
    {

        $tipe_pendanaan = \DB::table('brw_tipe_pendanaan')->pluck('pendanaan_nama', 'tipe_id');
        return view('pages.admin.analisa.list_kelayakan', compact('tipe_pendanaan'));
    }


    public function kirimKelayakan(Request $request)
    {

        $brw_id = $request->brw_id;
        $pengajuanId = $request->pengajuanId;
        $nilai_pefindo = ($request->nilai_pefindo != null) ? $request->nilai_pefindo : 0;
        $grade_pefindo = $request->grade_pefindo;
        $complete_id = $request->complete_id;
        $ocr = $request->ocr;
        $npwpa = $request->npwpa;
        $npwpc = $request->npwpc;
        $npwpd = $request->npwpd;
        $npwp_verify = $request->npwp_verify;
        $workplace_verification_f = $request->workplace_verification_f;
        $negative_list_verification_j = $request->ngetive_list_verification_j;
        $verify_property_k = $request->verify_property_k;
        $skor_personal = $request->skor_personal;
        $skor_pendanaan = $request->skor_pendanaan;
        $ftv = $request->ftv;
        $catatan_verifikasi = $request->catatan_verifikasi;
        $keputusan = $request->keputusan;

        $getEmail = DB::SELECT("SELECT email FROM brw_user WHERE brw_id = " . $brw_id . "")[0];
        $cek = DB::SELECT("SELECT * FROM brw_verifikasi_awal WHERE pengajuan_id = " . $pengajuanId . " AND brw_id = " . $brw_id . "");

        DB::beginTransaction();

        try {

            if ($cek == NULL) {
                $upsert = DB::table('brw_verifikasi_awal')->insert(
                    [
                        'brw_id' => $brw_id,
                        'pengajuan_id' => $pengajuanId,
                        'skor_pefindo' => $nilai_pefindo,
                        'grade_pefindo' => $grade_pefindo,
                        'complete_id' => $complete_id,
                        'ocr' => $ocr,
                        'npwp_a' => $npwpa,
                        'npwp_c' => $npwpc,
                        'npwp_d' => $npwpd,
                        'npwp_verify' => $npwp_verify,
                        'workplace_verification_f' => $workplace_verification_f,
                        'negative_list_verification_j' => $negative_list_verification_j,
                        'verify_property_k' => $verify_property_k,
                        'skor_personal_credolab' => $skor_personal,
                        'skor_pendanaan_credolab' => $skor_pendanaan,
                        'financing_to_value' => $ftv,
                        'catatan' => $catatan_verifikasi,
                        'status' => $keputusan
                    ]
                );
            } else {
                $upsert = DB::table('brw_verifikasi_awal')
                    ->where('pengajuan_id', $pengajuanId)
                    ->where('brw_id', $brw_id)
                    ->update([
                        'skor_pefindo' => $nilai_pefindo,
                        'grade_pefindo' => $grade_pefindo,
                        'complete_id' => $complete_id,
                        'ocr' => $ocr,
                        'npwp_a' => $npwpa,
                        'npwp_c' => $npwpc,
                        'npwp_d' => $npwpd,
                        'npwp_verify' => $npwp_verify,
                        'workplace_verification_f' => $workplace_verification_f,
                        'negative_list_verification_j' => $negative_list_verification_j,
                        'verify_property_k' => $verify_property_k,
                        'skor_personal_credolab' => $skor_personal,
                        'skor_pendanaan_credolab' => $skor_pendanaan,
                        'financing_to_value' => $ftv,
                        'catatan' => $catatan_verifikasi,
                        'status' => $keputusan
                    ]);
            }

            if ($upsert) {

                $setuju_verifikator_pengajuan = ($keputusan == '1') ? 1 : 0;
                $setuju_verifikator_dokumen = ($keputusan == '1') ? 1 : 0;

                DB::table('brw_pengajuan')
                    ->where('pengajuan_id', $pengajuanId)
                    ->update([
                        'setuju_verifikator_pengajuan' => $setuju_verifikator_pengajuan,
                        'setuju_verifikator_dokumen' => $setuju_verifikator_dokumen,
                        'status' => $keputusan
                    ]);


                $penerimaPendanaan = DB::SELECT("SELECT a.nama as nama_pengguna, a.brw_type, b.id_field_verifikator_vendor, b.pendanaan_tipe, b.pendanaan_tujuan,  c.pendanaan_nama as nama_pendanaan , b.pendanaan_dana_dibutuhkan as dana_dibutuhkan, DATE_FORMAT(b.created_at,'%d %M %Y') as tanggal_pengajuan, " . $keputusan . " as status
                FROM brw_user_detail_pengajuan a, brw_pengajuan b, brw_tipe_pendanaan c 
                WHERE a.pengajuan_id = b.pengajuan_id
                AND b.pendanaan_tipe = c.tipe_id
                AND b.pengajuan_id = " . $pengajuanId . "")[0];

                $email = new HasilVerifikasiKElayakanKprEmail($penerimaPendanaan);
                Mail::to($getEmail->email)->send($email);

                if ($keputusan == "1") {
                    $verifikatorService = new VerifikatorService();
                    $email_verifikator = DB::table('m_field_verificator_vendor')->where('id', $penerimaPendanaan->id_field_verifikator_vendor)->value('email');
                    $verifikatorService->makeAnalisaPendanaan($pengajuanId, $brw_id, $email_verifikator, 0);

                    VerifikatorService::createVerifikatorPengajuan($pengajuanId);

                    if ($penerimaPendanaan->brw_type === BorrowerUserType::COMPANY) {
                        Borrower::where('brw_id', $brw_id)->where('status', 'notpreapproved')->update(['status' => 'active']);
                    }
                }

                $response = ['data' => 1];
            } else {
                $response = ['data' => 0];
            }

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['error' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }



        return response()->json($response);
    }


    public function kirimKelengkapan(Request $request)
    {

        $pengajuanId = $request->pengajuanId;
        $brw_id = $request->brw_id;
        $keputusan = ($request->keputusan == "1") ? 3 : $request->keputusan;

        $result = DB::table('brw_pengajuan')->where('pengajuan_id', $pengajuanId)->update(['status' => $keputusan, 'id_field_verifikator_vendor' => $request->id_field_verifikator_vendor, 'updated_at' => date('Y-m-d H:i:s')]);

        if ($result) {

            $getEmail = DB::SELECT("SELECT email FROM brw_user WHERE brw_id = " . $brw_id . "")[0];
            $penerimaPendanaan = DB::SELECT("SELECT a.nama as nama_pengguna, b.id_field_verifikator_vendor,  b.pendanaan_tipe, b.pendanaan_tujuan,  c.pendanaan_nama as nama_pendanaan , b.pendanaan_dana_dibutuhkan as dana_dibutuhkan, DATE_FORMAT(b.created_at,'%d %M %Y') as tanggal_pengajuan, " . $keputusan . " as status
            FROM brw_user_detail_pengajuan a, brw_pengajuan b, brw_tipe_pendanaan c 
            WHERE a.pengajuan_id = b.pengajuan_id
            AND b.pendanaan_tipe = c.tipe_id
            AND b.pengajuan_id = " . $pengajuanId . "")[0];

            try {
                $email = new HasilAnalisaKelengkapanEmail($penerimaPendanaan);
                Mail::to($getEmail->email)->send($email);

                if ($keputusan === 3) {
                    $verifikatorService = new VerifikatorService();
                    $email_verifikator = DB::table('m_field_verificator_vendor')->where('id', $penerimaPendanaan->id_field_verifikator_vendor)->value('email');
                    $verifikatorService->makeAnalisaPendanaan($pengajuanId, $brw_id, $email_verifikator, 1);
                }
            } catch (\Exception $e) {
            }

            $response = ['data' => 1];
        } else {
            $response = ['data' => 0];
        }


        return response()->json($response);
    }

    public function detailPengajuanKelengkapan(Request $request)
    {
        $pengajuanId = $request->pengajuanId;
        $brw_id = $request->brw_id;

        $verifikasi = DB::SELECT("SELECT * FROM brw_verifikasi_awal WHERE pengajuan_id = " . $pengajuanId . " AND brw_id = " . $brw_id . "");
        if ($verifikasi == NULL) {
            $isiVerif = 0;
        } else {
            $isiVerif = 1;
        }

        // dd($verifikasi[0]);
        $nilai_pefindo = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_pefindo;
        $grade_pefindo = ($isiVerif == 0) ? 0 : $verifikasi[0]->grade_pefindo;
        $skor_personal = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_personal_credolab;
        $skor_pendanaan = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_pendanaan_credolab;
        $complete_id = ($isiVerif == 0) ? 0 : $verifikasi[0]->complete_id;
        $ocr = ($isiVerif == 0) ? 0 : $verifikasi[0]->ocr;
        $npwpa = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_a;
        $npwpc = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_c;
        $npwpd = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_d;
        $npwp_verify = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_verify;
        $workplace_verification_f = ($isiVerif == 0) ? 0 : $verifikasi[0]->workplace_verification_f;
        $negative_list_verification_j = ($isiVerif == 0) ? 0 : $verifikasi[0]->negative_list_verification_j;
        $verify_property_k = ($isiVerif == 0) ? 0 : $verifikasi[0]->verify_property_k;
        $ftv = ($isiVerif == 0) ? 0 : $verifikasi[0]->financing_to_value;
        $catatan_verifikasi = ($isiVerif == 0) ? 0 : $verifikasi[0]->catatan;
        $keputusan = ($isiVerif == 0) ? 0 : $verifikasi[0]->status;


        $pengajuan = DB::SELECT("SELECT a.*, 
        DATE_FORMAT(a.created_at,'%d/%m/%Y') AS tgl_pengajuan,UPPER(b.nama) as nama_cust, 
        d.pendanaan_nama AS tipe_pendanaan, 
        c.tujuan_pembiayaan as ctujuan_pembiayaan,
        CONCAT('Rp.',FORMAT(a.pendanaan_dana_dibutuhkan,0)) AS nilai_pengajuan, 
        a.status AS stts, d.tipe_id
        FROM brw_pengajuan a, brw_user_detail_pengajuan b, m_tujuan_pembiayaan c, brw_tipe_pendanaan d 
        WHERE a.pengajuan_id=b.pengajuan_id 
        AND a.pendanaan_tujuan = c.id 
        AND a.pendanaan_tipe = d.tipe_id
        AND a.pengajuan_id = " . $pengajuanId . "")[0];


        $getprofil = DB::SELECT("SELECT a.*,b.*,c.brw_norek,c.brw_nm_pemilik,c.brw_kd_bank,h.nama_bank, c.kantor_cabang_pembuka, d.agama as dagama , e.pendidikan as ependidikan, f.jenis_kawin as fkawin, b.status_kawin as id_kawin
        FROM brw_user_detail_pengajuan b
        LEFT JOIN brw_pengajuan a ON  a.pengajuan_id = b.pengajuan_id 
        LEFT JOIN brw_rekening c ON  a.brw_id = c.brw_id
        LEFT JOIN m_agama d ON  b.agama = d.id_agama
        LEFT JOIN m_pendidikan e ON  b.pendidikan_terakhir = e.id_pendidikan
        LEFT JOIN m_kawin f ON  b.status_kawin = f.id_kawin
        LEFT JOIN m_pekerjaan g ON  g.id_pekerjaan = b.pekerjaan
        LEFT JOIN m_bank h ON  c.brw_kd_bank = h.kode_bank
        WHERE a.pengajuan_id = " . $pengajuanId . "");

        if ($getprofil == NULL) {
            $profil = 0;
        } else {
            $profil = $getprofil[0];
        }


        $getpekerjaan = DB::select("SELECT a.pekerjaan as pekerjaan_id, c.pekerjaan,d.tipe_online as bidang_online, 
        case when b.bentuk_badan_usaha = 0 then 0 ELSE 
        (SELECT bentuk_badan_usaha FROM m_bentuk_badan_usaha WHERE id = b.bentuk_badan_usaha) END AS bbu ,b.*, e.bidang_pekerjaan
        FROM brw_user_detail_pengajuan a
        LEFT JOIN brw_user_detail_penghasilan_pengajuan b ON  b.pengajuan_id = a.pengajuan_id 
        LEFT JOIN m_pekerjaan c ON  a.pekerjaan = c.id_pekerjaan
        LEFT JOIN m_online d ON  a.bidang_online = d.id_online
        LEFT JOIN m_bidang_pekerjaan e ON e.id_bidang_pekerjaan = a.bidang_pekerjaan
        WHERE a.brw_id = ? and a.pengajuan_id = ? ", [$brw_id, $pengajuanId]);


        if ($getpekerjaan == NULL) {
            $pekerjaan = 0;
        } else {
            $pekerjaan = $getpekerjaan[0];
        }

        $noPasangan = 1;
        $v_pekerjaanPasangan = 1;
        if ($profil->id_kawin == 1) {
            $getPasangan = DB::SELECT("SELECT a.*,b.agama as bagama, c.pendidikan as cpendidikan,
            case when a.bentuk_badan_usaha = 0 then 0 ELSE 
            (SELECT bentuk_badan_usaha FROM m_bentuk_badan_usaha WHERE id = a.bentuk_badan_usaha) END AS dbbu
            FROM brw_pasangan a, m_agama b , m_pendidikan c
            WHERE a.agama = b.id_agama 
            AND a.pendidikan  = c.id_pendidikan
            AND a.pasangan_id = " . $brw_id . "");
            if ($getPasangan == NULL) {
                $noPasangan = 0;
            } else {
                $pasangan = $getPasangan[0];
            }
            if ($pekerjaan->skema_pembiayaan == 2) {

                $getPekerjaanPasangan = DB::select("SELECT b.pekerjaan as pekerjaan_pasangan, d.bidang_pekerjaan as bidang_pekerjaan_pasangan , c.tipe_online as bidang_online_pasangan 
                FROM brw_user_detail_pengajuan a , m_pekerjaan b , m_online c, m_bidang_pekerjaan d
                WHERE a.pekerjaan_pasangan = b.id_pekerjaan 
                AND a.bd_pekerjaan_pasangan = d.id_bidang_pekerjaan
                AND a.bd_pekerjaanO_pasangan = c.id_online
                AND a.brw_id = ? and a.pengajuan_id = ?", [$brw_id, $pengajuanId]);

                if ($getPekerjaanPasangan == NULL) {
                    $v_pekerjaanPasangan = 0;
                } else {
                    $pekerjaanPasangan = $getPekerjaanPasangan[0];
                }
            } else {
                $v_pekerjaanPasangan = 0;
            }
        } else {
            $noPasangan = 0;
            $v_pekerjaanPasangan = 0;
        }

        $jenis_properti = '-';
        if ($pengajuan->jenis_properti == "1") {
            $jenis_properti = 'Baru';
        } elseif ($pengajuan->jenis_properti == "2") {
            $jenis_properti = 'Lama';
        }

        $verifikator_vendor = \DB::table('m_field_verificator_vendor')->pluck('nama', 'id');
        $status_keputusan  = "";
        if ($pengajuan->stts == "2") {
            $status_keputusan = "2";
        } elseif ($pengajuan->stts == "3") {
            $status_keputusan = "1";
        }


        //Dokumen pendukung
        $dokumen_pendukung = \DB::table('brw_pengajuan')
            ->leftjoin('brw_agunan', 'brw_agunan.id_pengajuan', 'brw_pengajuan.pengajuan_id')
            ->leftjoin('brw_dokumen_objek_pendanaan', 'brw_dokumen_objek_pendanaan.id_pengajuan', 'brw_pengajuan.pengajuan_id')
            ->leftjoin('brw_dokumen_legalitas_pribadi', 'brw_dokumen_legalitas_pribadi.brw_id', 'brw_pengajuan.brw_id')
            ->join('brw_user_detail_pengajuan', 'brw_user_detail_pengajuan.pengajuan_id', 'brw_pengajuan.pengajuan_id')
            ->where('brw_pengajuan.pengajuan_id', $pengajuanId)
            ->where('brw_pengajuan.brw_id', $brw_id)
            ->first();

        $borrower_penghasilan =  \DB::table('brw_user_detail_penghasilan_pengajuan')->where('brw_id2', $brw_id)->where('pengajuan_id', $pengajuanId)->select(['sumber_pengembalian_dana', 'skema_pembiayaan'])->first();
        $count_dana_non_rumah_lain = \DB::table('brw_pendanaan_non_rumah_lain')->where('pengajuan_id', $pengajuanId)->count();

        $kantor_bpn = \DB::table('m_kantor_bpn')->pluck('nama_kantor', 'id');
        $rumah_ke = \DB::table('brw_pendanaan_rumah_lain')->where('pengajuan_id', $pengajuanId)->count('rumah_ke');
        $data_rumah_lain = \DB::table('brw_pendanaan_rumah_lain')->where('pengajuan_id', $pengajuanId)->orderBy('rumah_ke', 'asc')->get();
        $data_non_rumah_lain = \DB::table('brw_pendanaan_non_rumah_lain')->where('pengajuan_id', $pengajuanId)->get();

        $existsAnalisaPendanaan = BorrowerAnalisaPendanaan::where('pengajuan_id', $pengajuanId)->get();
        // $optionKeputusan = [''=>'Pilih', '1'=> 'Lanjut', '12'=> 'Tolak'];
        // if(count($existsAnalisaPendanaan) > 0) array_pop($optionKeputusan);

        $resultDocumentPersyaratan =  Helper::getDokumenList($dokumen_pendukung);

        $data_dokumen_persyaratan = [];
        foreach ($resultDocumentPersyaratan as $val) {
            $data_dokumen_persyaratan[$val->page_title][$val->category_title][] = $val;
        }

        $brw_verifikator_pengajuan = \DB::table('brw_verifikator_pengajuan')->where('pengajuan_id', $pengajuanId)->first();


        $queryMaterialItems = MaterialOrdersController::queryMaterialItemList();
        $requisitionHeader = BorrowerMaterialRequisitionHeader::where('pengajuan_id', $pengajuanId)->first();
        $requisitionDetail  = $queryMaterialItems->orderBy('brw_material_requisition_dtl.task_id', 'asc')->orderBy('creation_date', 'asc')->where('brw_material_requisition_hdr.pengajuan_id', $pengajuanId)->get();

        $data_pengurus = [];
        $data_pemegang_saham = [];
        if ($profil->brw_type === BorrowerUserType::COMPANY) {
            $data_pengurus = BorrowerPengurus::where('brw_id', $brw_id)->get();
            $data_pemegang_saham = BorrowerPemegangSaham::where('brw_id', $brw_id)->get();
        }

        return view('pages.admin.analisa.analisa_kelengkapan')->with([
            // data dokumen persyaratan 
            'data_dokumen_persyaratan' => $data_dokumen_persyaratan,
            'pengurus' => $data_pengurus,
            'pemegang_saham' => $data_pemegang_saham,
            // informasi pribadi
            'brw_id' => $brw_id,
            'pengajuan' => $pengajuan,
            'pengajuanId' => $pengajuanId,
            'pengajuan_id' => $pengajuanId,
            'nama' => $profil->nama,
            'jns_kelamin' => $profil->jns_kelamin,
            'ktp' => $profil->ktp,
            'kk' => $profil->nomor_kk_pribadi,
            'tempat_lahir' => $profil->tempat_lahir,
            'tgl_lahir' => $profil->tgl_lahir,
            'telepon' => $profil->no_tlp,
            'agama' => $profil->dagama,
            'pendidikan_terakhir' => $profil->ependidikan,
            'id_kawin' => $profil->id_kawin,
            'status_kawin' => $profil->fkawin,
            'nm_ibu' => $profil->nm_ibu,
            'npwp' => $profil->npwp,
            'profil' => $profil,

            // informasi alamat
            'alamat' => $profil->alamat,
            'provinsi' => $profil->provinsi,
            'kota' => $profil->kota,
            'kecamatan' => $profil->kecamatan,
            'kelurahan' => $profil->kelurahan,
            'kode_pos' => $profil->kode_pos,
            'status_rumah' => $profil->status_rumah,

            // informasi domisili alamat 
            'domisili_alamat' => $profil->domisili_alamat,
            'domisili_provinsi' => $profil->domisili_provinsi,
            'domisili_kota' => $profil->domisili_kota,
            'domisili_kecamatan' => $profil->domisili_kecamatan,
            'domisili_kelurahan' => $profil->domisili_kelurahan,
            'domisili_kode_pos' => $profil->domisili_kd_pos,
            'domisili_status_rumah' => $profil->domisili_status_rumah,

            // informasi rekening
            'brw_norek' => $profil->brw_norek,
            'brw_nm_pemilik' => $profil->brw_nm_pemilik,
            'brw_kd_bank' => $profil->nama_bank,
            'kantorcabangpembuka' => $profil->kantor_cabang_pembuka,

            // informasi pasangan
            'pasangan_nama' => ($noPasangan == 0) ? '0' : $pasangan->nama,
            'pasangan_jenis_kelamin' => ($noPasangan == 0) ? '0' : $pasangan->jenis_kelamin,
            'pasangan_ktp' => ($noPasangan == 0) ? '0' : $pasangan->ktp,
            'pasangan_tempat_lahir' => ($noPasangan == 0) ? '0' : $pasangan->tempat_lahir,
            'pasangan_tanggal_lahir' => ($noPasangan == 0) ? '0' : $pasangan->tgl_lahir,
            'pasangan_telepon' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'pasangan_agama' => ($noPasangan == 0) ? '0' : $pasangan->bagama,
            'pasangan_pendidikan_terakhir' => ($noPasangan == 0) ? '0' : $pasangan->cpendidikan,
            'pasangan_npwp' => ($noPasangan == 0) ? '0' : $pasangan->npwp,
            'pasangan_alamat' => ($noPasangan == 0) ? '0' : $pasangan->alamat,
            'pasangan_provinsi' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'pasangan_kota' => ($noPasangan == 0) ? '0' : $pasangan->kota,
            'pasangan_kecamatan' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'pasangan_kelurahan' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'pasangan_kode_pos' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,

            // fixincome
            'nama_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nama_perusahaan,
            'sumberpengembaliandana_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->sumber_pengembalian_dana,
            'bentuk_badan_usaha_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->dbbu,
            'status_kepegawaian_pasangan' => ($noPasangan == 0) ? '0' : (($pasangan->status_pekerjaan == 1) ? 'Kontrak' : 'Tetap'),
            'usia_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->usia_perusahaan,
            'departemen_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->departemen,
            'jabatan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->jabatan,
            'tahun_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->masa_kerja_tahun,
            'bulan_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->masa_kerja_bulan,
            'nip_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nip_nrp_nik,
            'nama_hrd_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nama_hrd,
            'no_fixed_line_hrd_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->no_fixed_line_hrd,
            'no_telpon_usaha_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->no_telp_pekerjaan_pasangan,
            'alamat_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->alamat_perusahaan,
            'rt_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->rt_pekerjaan_pasangan,
            'rw_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->rw_pekerjaan_pasangan,
            'provinsi_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'kabupaten_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kab_kota_pekerjaan_pasangan,
            'kecamatan_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'kelurahan_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'kodepos_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,
            'tahun_pengalaman_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->pengalaman_kerja_tahun,
            'bulan_pengalaman_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->pengalaman_kerja_bulan,
            'penghasilan_pasangan' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_pasangan' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->biaya_hidup, 2, ",", "."),

            // nonfixincome
            'nama_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->nama_perusahaan,
            'lama_usaha_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->usia_perusahaan,
            'lama_tempat_usaha_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->usia_tempat_usaha,
            'no_telpon_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'no_hp_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'alamat_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->alamat_perusahaan,
            'rt_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->rt_pekerjaan_pasangan,
            'rw_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->rw_pekerjaan_pasangan,
            'provinsi_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'kabupaten_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kab_kota_pekerjaan_pasangan,
            'kecamatan_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'kelurahan_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'kodepos_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,
            'surat_ijin_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->surat_ijin,
            'nomor_ijin_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_surat_ijin,
            'penghasilan_pasangan_non' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_pasangan_non' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->biaya_hidup, 2, ",", "."),

            // informasi pekerjaan pasangan 
            'pekerjaan_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->pekerjaan_pasangan,
            'bidang_pekerjaan_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->bidang_pekerjaan_pasangan,
            'bidang_online_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->bidang_online_pasangan,

            // informasi pekerjaan
            // fixincomE
            'jenis_pekerjaan' => $pekerjaan->pekerjaan,
            'sumberpengembaliandana' => $pekerjaan->sumber_pengembalian_dana,
            'skemapembiayaan' => $pekerjaan->skema_pembiayaan,
            'nama_perusahaan' => $pekerjaan->nama_perusahaan,
            'bidang_online' => $pekerjaan->bidang_online,
            'bentuk_badan_usaha' => $pekerjaan->bbu,
            'status_kepegawaian' => ($pekerjaan->status_pekerjaan == 1) ? 'Kontrak' : 'Tetap',
            'usia_perusahaan' => $pekerjaan->usia_perusahaan,
            'departemen' => $pekerjaan->departemen,
            'jabatan' => $pekerjaan->jabatan,
            'tahun_bekerja' => $pekerjaan->masa_kerja_tahun,
            'bulan_bekerja' => $pekerjaan->masa_kerja_bulan,
            'nip' => $pekerjaan->nip_nrp_nik,
            'nama_hrd' => $pekerjaan->nama_hrd,
            'no_fixed_line_hrd' => $pekerjaan->no_fixed_line_hrd,
            'alamat_perusahaan' => $pekerjaan->alamat_perusahaan,
            'rt_perusahaan' => $pekerjaan->rt,
            'rw_perusahaan' => $pekerjaan->rw,
            'provinsi_perusahaan' => $pekerjaan->provinsi,
            'kabupaten_perusahaan' => $pekerjaan->kab_kota,
            'kecamatan_perusahaan' => $pekerjaan->kecamatan,
            'kelurahan_perusahaan' => $pekerjaan->kelurahan,
            'kodepos_perusahaan' => $pekerjaan->kode_pos,
            'no_telpon_usaha' => $pekerjaan->no_telp,
            'tahun_pengalaman_bekerja' => $pekerjaan->pengalaman_kerja_tahun,
            'bulan_pengalaman_bekerja' => $pekerjaan->pengalaman_kerja_bulan,
            'penghasilan' => "Rp." . number_format($pekerjaan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup' => "Rp." . number_format($pekerjaan->biaya_hidup, 2, ",", "."),
            'detail_penghasilan_lain_lain' => $pekerjaan->detail_penghasilan_lain_lain,
            'total_penghasilan_lain_lain' => "Rp." . number_format($pekerjaan->total_penghasilan_lain_lain, 2, ",", "."),
            'nilai_spt' => "Rp." . number_format($pekerjaan->nilai_spt, 2, ",", "."),

            // non fixincome
            'nama_perusahaan_non' => $pekerjaan->nama_perusahaan,
            'bidang_online_non' => $pekerjaan->bidang_online,
            'lama_usaha_non' => $pekerjaan->usia_perusahaan,
            'lama_tempat_usaha_non' => $pekerjaan->usia_tempat_usaha,
            'no_telpon_non' => $pekerjaan->no_telp,
            'no_hp_non' => $pekerjaan->no_hp,
            'alamat_perusahaan_non' => $pekerjaan->alamat_perusahaan,
            'rt_perusahaan_non' => $pekerjaan->rt,
            'rw_perusahaan_non' => $pekerjaan->rw,
            'provinsi_perusahaan_non' => $pekerjaan->provinsi,
            'kabupaten_perusahaan_non' => $pekerjaan->kab_kota,
            'kecamatan_perusahaan_non' => $pekerjaan->kecamatan,
            'kelurahan_perusahaan_non' => $pekerjaan->kelurahan,
            'kodepos_perusahaan_non' => $pekerjaan->kode_pos,
            'surat_ijin_non' => $pekerjaan->surat_ijin,
            'nomor_ijin_non' => $pekerjaan->no_surat_ijin,
            'penghasilan_non' => "Rp." . number_format($pekerjaan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_non' => "Rp." . number_format($pekerjaan->biaya_hidup, 2, ",", "."),

            // pengajuan
            'type_pendanaan' => $pengajuan->tipe_pendanaan,
            'tujuan_pendanaan' => $pengajuan->ctujuan_pembiayaan,
            'detail_pendanaan' =>  $pengajuan->detail_pendanaan,
            'alamat_objek_pendanaan' => $pengajuan->lokasi_proyek,
            'provinsi_objek_pendanaan' => $pengajuan->provinsi,
            'kota_objek_pendanaan' => $pengajuan->kota,
            'kecamatan_objek_pendanaan' => $pengajuan->kecamatan,
            'kelurahan_objek_pendanaan' => $pengajuan->kelurahan,
            'kodepos_objek_pendanaan' => $pengajuan->kode_pos,
            'harga_objek_pendanaan' => "Rp." . number_format($pengajuan->harga_objek_pendanaan, 2, ",", "."),
            'uang_muka' => "Rp." . number_format($pengajuan->uang_muka, 2, ",", "."),
            'uang_muka_percentage' => (($pengajuan->uang_muka / $pengajuan->harga_objek_pendanaan) * 100) . "%",
            'nilai_pengajuan' => "Rp." . number_format($pengajuan->pendanaan_dana_dibutuhkan, 2, ",", "."),
            'jangka_waktu' => $pengajuan->durasi_proyek,
            'nama_pemilik' => $pengajuan->nm_pemilik,
            'tlp_pemilik' => $pengajuan->no_tlp_pemilik,
            'alamat_pemilik' => $pengajuan->alamat_pemilik,
            'provinsi_pemilik' => $pengajuan->provinsi_pemilik,
            'kota_pemilik' => $pengajuan->kota_pemilik,
            'kecamatan_pemilik' => $pengajuan->kecamatan_pemilik,
            'kelurahan_pemilik' => $pengajuan->kelurahan_pemilik,
            'kodepos_pemilik' => $pengajuan->kd_pos_pemilik,
            'brw_pic' => $profil->brw_pic,
            'brw_pic_ktp' => $profil->brw_pic_ktp,
            'brw_pic_user_ktp' => $profil->brw_pic_user_ktp,
            'brw_pic_npwp' => $profil->brw_pic_npwp,
            'brw_type' => $profil->brw_type,
            'pekerjaan' => 0,
            'bidang_perusahaan' => 0,
            'bidang_pekerjaan' => $pekerjaan->bidang_pekerjaan,
            'pengalaman_pekerjaan' => 0,
            'pendapatan' => 0,
            'total_aset' => 0,
            'kewarganegaraan' => 0,
            'brw_online' => 0,
            'jenis_properti' => $jenis_properti,

            // data Verifikasi
            'nilai_pefindo' => $nilai_pefindo,
            'grade_pefindo' => $grade_pefindo,
            'skor_personal' => $skor_personal,
            'skor_pendanaan' => $skor_pendanaan,
            'complete_id' => $complete_id,
            'ocr' => $ocr,
            'npwpa' => $npwpa,
            'npwpc' => $npwpc,
            'npwpd' => $npwpd,
            'npwp_verify' => $npwp_verify,
            'workplace_verification_f' => $workplace_verification_f,
            'negative_list_verification_j' => $negative_list_verification_j,
            'verify_property_k' => $verify_property_k,
            'ftv' => $ftv,
            'catatan_verifikasi' => $catatan_verifikasi,
            'keputusan' => $keputusan,
            'isiVerif' => $isiVerif,
            'status_keputusan' => $status_keputusan,
            'status_pengajuan' => $pengajuan->stts,
            'verifikator_vendor' => $verifikator_vendor,
            'id_field_verifikator_vendor' => $pengajuan->id_field_verifikator_vendor,
            'dokumen_pendukung' => $dokumen_pendukung,
            'sumber_pengembalian_dana' => $borrower_penghasilan->sumber_pengembalian_dana,
            'skema_pembiayaan' => $borrower_penghasilan->skema_pembiayaan,
            // 'brw_user_detail_pic'=> $brw_user_detail_pic,
            'status_bank_non_rumah_lain' => ($count_dana_non_rumah_lain > 0) ? '1' : '2',
            'kantor_bpn' => $kantor_bpn,
            'rumah_ke' => ($rumah_ke + 1),
            'data_rumah_lain' => $data_rumah_lain,
            'data_non_rumah_lain' => $data_non_rumah_lain,
            //'optionKeputusan'=> $optionKeputusan
            'brw_verifikator_pengajuan' => $brw_verifikator_pengajuan,
            'requisitionHeader' => $requisitionHeader,
            'requisitionDetail' => $requisitionDetail
        ]);
    }

    public function detailPengajuanKelayakan(Request $request)
    {

        $pengajuanId = $request->pengajuanId;
        $brw_id = $request->brw_id;
        $jenis_properti = '-';


        $verifikasi = DB::SELECT("SELECT * FROM brw_verifikasi_awal WHERE pengajuan_id = " . $pengajuanId . " AND brw_id = " . $brw_id . "");
        if ($verifikasi == NULL) {
            $isiVerif = 0;
        } else {
            $isiVerif = 1;
        }

        // dd($verifikasi[0]);
        $nilai_pefindo = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_pefindo;
        $grade_pefindo = ($isiVerif == 0) ? 0 : $verifikasi[0]->grade_pefindo;
        $skor_personal = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_personal_credolab;
        $skor_pendanaan = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_pendanaan_credolab;
        $complete_id = ($isiVerif == 0) ? 0 : $verifikasi[0]->complete_id;
        $ocr = ($isiVerif == 0) ? 0 : $verifikasi[0]->ocr;
        $npwpa = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_a;
        $npwpc = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_c;
        $npwpd = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_d;
        $npwp_verify = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_verify;
        $workplace_verification_f = ($isiVerif == 0) ? 0 : $verifikasi[0]->workplace_verification_f;
        $negative_list_verification_j = ($isiVerif == 0) ? 0 : $verifikasi[0]->negative_list_verification_j;
        $verify_property_k = ($isiVerif == 0) ? 0 : $verifikasi[0]->verify_property_k;
        $ftv = ($isiVerif == 0) ? 0 : $verifikasi[0]->financing_to_value;
        $catatan_verifikasi = ($isiVerif == 0) ? 0 : $verifikasi[0]->catatan;
        $keputusan = ($isiVerif == 0) ? 0 : $verifikasi[0]->status;


        $pengajuan = DB::SELECT("SELECT a.*, 
        DATE_FORMAT(a.created_at,'%d/%m/%Y') AS tgl_pengajuan, 
        UPPER(b.nama) as nama_cust, 
        d.pendanaan_nama AS tipe_pendanaan, 
        c.tujuan_pembiayaan as ctujuan_pembiayaan,
        CONCAT('Rp.',FORMAT(a.pendanaan_dana_dibutuhkan,0)) AS nilai_pengajuan, 
        a.status AS stts, d.tipe_id
        FROM brw_pengajuan a, brw_user_detail_pengajuan b, m_tujuan_pembiayaan c, brw_tipe_pendanaan d
        WHERE a.pengajuan_id=b.pengajuan_id 
        AND a.pendanaan_tujuan = c.id 
        AND a.pendanaan_tipe = d.tipe_id
        AND a.pengajuan_id = " . $pengajuanId . "")[0];

        $getprofil = DB::SELECT("SELECT a.*,b.*,c.brw_norek,c.brw_nm_pemilik,c.brw_kd_bank,h.nama_bank, c.kantor_cabang_pembuka, d.agama as dagama , e.pendidikan as ependidikan, f.jenis_kawin as fkawin, b.status_kawin as id_kawin
        FROM brw_user_detail_pengajuan b
          LEFT JOIN brw_pengajuan a ON  a.pengajuan_id = b.pengajuan_id 
          LEFT JOIN brw_rekening c ON  a.brw_id = c.brw_id
          LEFT JOIN m_agama d ON  b.agama = d.id_agama
          LEFT JOIN m_pendidikan e ON  b.pendidikan_terakhir = e.id_pendidikan
          LEFT JOIN m_kawin f ON  b.status_kawin = f.id_kawin
          LEFT JOIN m_pekerjaan g ON  g.id_pekerjaan = b.pekerjaan
          LEFT JOIN m_bank h ON  c.brw_kd_bank = h.kode_bank
        WHERE a.pengajuan_id = " . $pengajuanId . "");

        if ($getprofil == NULL) {
            $profil = 0;
        } else {
            $profil = $getprofil[0];
        }

        $getpekerjaan = DB::select("SELECT a.pekerjaan as pekerjaan_id, c.pekerjaan,d.tipe_online as bidang_online, 
            case when b.bentuk_badan_usaha = 0 then 0 ELSE 
            (SELECT bentuk_badan_usaha FROM m_bentuk_badan_usaha WHERE id = b.bentuk_badan_usaha) END AS bbu ,b.*, e.bidang_pekerjaan
            FROM brw_user_detail_pengajuan a
            LEFT JOIN brw_user_detail_penghasilan_pengajuan b ON  b.pengajuan_id = a.pengajuan_id 
            LEFT JOIN m_pekerjaan c ON  a.pekerjaan = c.id_pekerjaan
            LEFT JOIN m_online d ON  a.bidang_online = d.id_online
            LEFT JOIN m_bidang_pekerjaan e ON e.id_bidang_pekerjaan = a.bidang_pekerjaan
            WHERE a.brw_id = ? and a.pengajuan_id = ? ", [$brw_id, $pengajuanId]);

        if ($getpekerjaan == NULL) {
            $pekerjaan = 0;
        } else {
            $pekerjaan = $getpekerjaan[0];
        }


        $noPasangan = 1;
        $v_pekerjaanPasangan = 1;
        if ($profil->id_kawin == 1) {
            $getPasangan = DB::SELECT("SELECT a.*,b.agama as bagama, c.pendidikan as cpendidikan,
            case when a.bentuk_badan_usaha = 0 then 0 ELSE 
            (SELECT bentuk_badan_usaha FROM m_bentuk_badan_usaha WHERE id = a.bentuk_badan_usaha) END AS dbbu
            FROM brw_pasangan a, m_agama b , m_pendidikan c
            WHERE a.agama = b.id_agama 
            AND a.pendidikan  = c.id_pendidikan
            AND a.pasangan_id = " . $brw_id . "");
            if ($getPasangan == NULL) {
                $noPasangan = 0;
            } else {
                $pasangan = $getPasangan[0];
            }
            if ($pekerjaan->skema_pembiayaan == 2) {

                $getPekerjaanPasangan = DB::select("SELECT b.pekerjaan as pekerjaan_pasangan, d.bidang_pekerjaan as bidang_pekerjaan_pasangan , c.tipe_online as bidang_online_pasangan 
                FROM brw_user_detail_pengajuan a , m_pekerjaan b , m_online c, m_bidang_pekerjaan d
                WHERE a.pekerjaan_pasangan = b.id_pekerjaan 
                AND a.bd_pekerjaan_pasangan = d.id_bidang_pekerjaan
                AND a.bd_pekerjaanO_pasangan = c.id_online
                AND a.brw_id = ? and a.pengajuan_id = ?", [$brw_id, $pengajuanId]);

                if ($getPekerjaanPasangan == NULL) {
                    $v_pekerjaanPasangan = 0;
                } else {
                    $pekerjaanPasangan = $getPekerjaanPasangan[0];
                }
            } else {
                $v_pekerjaanPasangan = 0;
            }
            // dd($pekerjaanPasangan);
        } else {
            $noPasangan = 0;
            $v_pekerjaanPasangan = 0;
        }


        if ($pengajuan->jenis_properti == "1") {
            $jenis_properti = 'Baru';
        } elseif ($pengajuan->jenis_properti == "2") {
            $jenis_properti = 'Lama';
        }



        $queryMaterialItems = MaterialOrdersController::queryMaterialItemList();
        $requisitionHeader = BorrowerMaterialRequisitionHeader::where('pengajuan_id', $pengajuanId)->first();
        $requisitionDetail  = $queryMaterialItems->orderBy('brw_material_requisition_dtl.task_id', 'asc')->orderBy('creation_date', 'asc')->where('brw_material_requisition_hdr.pengajuan_id', $pengajuanId)->get();

        $uangMukaPercentage = $pengajuan->uang_muka ? (($pengajuan->uang_muka / $pengajuan->harga_objek_pendanaan) * 100) . "%" : '';
        $verifikator_vendor = \DB::table('m_field_verificator_vendor')->pluck('nama', 'id');


        $data_pengurus = [];
        $data_pemegang_saham = [];
        if ($profil->brw_type === BorrowerUserType::COMPANY) {
            $data_pengurus = BorrowerPengurus::where('brw_id', $brw_id)->get();
            $data_pemegang_saham = BorrowerPemegangSaham::where('brw_id', $brw_id)->get();
        }

        return view('pages.admin.analisa.analisa_kelayakan')->with([
            // informasi pribadi
            'brw_id' => $brw_id,
            'pengurus' => $data_pengurus,
            'pemegang_saham' => $data_pemegang_saham,
            'pengajuanId' => $pengajuanId,
            'nama' => ($profil->brw_type == '1')  ? $profil->nama : $profil->nm_bdn_hukum,
            'jns_kelamin' => $profil->jns_kelamin,
            'ktp' => $profil->ktp,
            'kk' => $profil->nomor_kk_pribadi,
            'tempat_lahir' => $profil->tempat_lahir,
            'tgl_lahir' => $profil->tgl_lahir,
            'telepon' => $profil->no_tlp,
            'agama' => $profil->dagama,
            'pendidikan_terakhir' => $profil->ependidikan,
            'id_kawin' => $profil->id_kawin,
            'status_kawin' => $profil->fkawin,
            'nm_ibu' => $profil->nm_ibu,
            'npwp' => $profil->npwp,
            'jenis_properti' => $jenis_properti,
            'profil' => $profil,
            // informasi alamat
            'alamat' => $profil->alamat,
            'provinsi' => $profil->provinsi,
            'kota' => $profil->kota,
            'kecamatan' => $profil->kecamatan,
            'kelurahan' => $profil->kelurahan,
            'kode_pos' => $profil->kode_pos,
            'status_rumah' => $profil->status_rumah,

            // informasi domisili alamat 
            'domisili_alamat' => $profil->domisili_alamat,
            'domisili_provinsi' => $profil->domisili_provinsi,
            'domisili_kota' => $profil->domisili_kota,
            'domisili_kecamatan' => $profil->domisili_kecamatan,
            'domisili_kelurahan' => $profil->domisili_kelurahan,
            'domisili_kode_pos' => $profil->domisili_kd_pos,
            'domisili_status_rumah' => $profil->domisili_status_rumah,

            // informasi rekening
            'brw_norek' => $profil->brw_norek,
            'brw_nm_pemilik' => $profil->brw_nm_pemilik,
            'brw_kd_bank' => $profil->nama_bank,
            'kantorcabangpembuka' => $profil->kantor_cabang_pembuka,

            // informasi pasangan
            'pasangan_nama' => ($noPasangan == 0) ? '0' : $pasangan->nama,
            'pasangan_jenis_kelamin' => ($noPasangan == 0) ? '0' : $pasangan->jenis_kelamin,
            'pasangan_ktp' => ($noPasangan == 0) ? '0' : $pasangan->ktp,
            'pasangan_tempat_lahir' => ($noPasangan == 0) ? '0' : $pasangan->tempat_lahir,
            'pasangan_tanggal_lahir' => ($noPasangan == 0) ? '0' : $pasangan->tgl_lahir,
            'pasangan_telepon' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'pasangan_agama' => ($noPasangan == 0) ? '0' : $pasangan->bagama,
            'pasangan_pendidikan_terakhir' => ($noPasangan == 0) ? '0' : $pasangan->cpendidikan,
            'pasangan_npwp' => ($noPasangan == 0) ? '0' : $pasangan->npwp,
            'pasangan_alamat' => ($noPasangan == 0) ? '0' : $pasangan->alamat,
            'pasangan_provinsi' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'pasangan_kota' => ($noPasangan == 0) ? '0' : $pasangan->kota,
            'pasangan_kecamatan' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'pasangan_kelurahan' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'pasangan_kode_pos' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,

            // fixincome
            'nama_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nama_perusahaan,
            'sumberpengembaliandana_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->sumber_pengembalian_dana,
            'bentuk_badan_usaha_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->dbbu,
            'status_kepegawaian_pasangan' => ($noPasangan == 0) ? '0' : (($pasangan->status_pekerjaan == 1) ? 'Kontrak' : 'Tetap'),
            'usia_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->usia_perusahaan,
            'departemen_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->departemen,
            'jabatan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->jabatan,
            'tahun_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->masa_kerja_tahun,
            'bulan_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->masa_kerja_bulan,
            'nip_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nip_nrp_nik,
            'nama_hrd_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nama_hrd,
            'no_fixed_line_hrd_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->no_fixed_line_hrd,
            'no_telpon_usaha_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->no_telp_pekerjaan_pasangan,
            'alamat_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->alamat_perusahaan,
            'rt_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->rt_pekerjaan_pasangan,
            'rw_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->rw_pekerjaan_pasangan,
            'provinsi_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'kabupaten_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kab_kota_pekerjaan_pasangan,
            'kecamatan_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'kelurahan_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'kodepos_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,
            'tahun_pengalaman_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->pengalaman_kerja_tahun,
            'bulan_pengalaman_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->pengalaman_kerja_bulan,
            'penghasilan_pasangan' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_pasangan' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->biaya_hidup, 2, ",", "."),

            // nonfixincome
            'nama_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->nama_perusahaan,
            'lama_usaha_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->usia_perusahaan,
            'lama_tempat_usaha_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->usia_tempat_usaha,
            'no_telpon_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'no_hp_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'alamat_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->alamat_perusahaan,
            'rt_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->rt_pekerjaan_pasangan,
            'rw_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->rw_pekerjaan_pasangan,
            'provinsi_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'kabupaten_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kab_kota_pekerjaan_pasangan,
            'kecamatan_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'kelurahan_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'kodepos_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,
            'surat_ijin_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->surat_ijin,
            'nomor_ijin_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_surat_ijin,
            'penghasilan_pasangan_non' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_pasangan_non' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->biaya_hidup, 2, ",", "."),

            // informasi pekerjaan pasangan 
            'pekerjaan_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->pekerjaan_pasangan,
            'bidang_pekerjaan_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->bidang_pekerjaan_pasangan,
            'bidang_online_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->bidang_online_pasangan,

            // informasi pekerjaan
            // fixincome
            'pekerjaan_id' => $pekerjaan->pekerjaan_id,
            'jenis_pekerjaan' => $pekerjaan->pekerjaan,
            'sumberpengembaliandana' => $pekerjaan->sumber_pengembalian_dana,
            'skemapembiayaan' => $pekerjaan->skema_pembiayaan,
            'nama_perusahaan' => $pekerjaan->nama_perusahaan,
            'bidang_online' => $pekerjaan->bidang_online,
            'bentuk_badan_usaha' => $pekerjaan->bbu,
            'status_kepegawaian' => ($pekerjaan->status_pekerjaan == 1) ? 'Kontrak' : 'Tetap',
            'usia_perusahaan' => $pekerjaan->usia_perusahaan,
            'departemen' => $pekerjaan->departemen,
            'jabatan' => $pekerjaan->jabatan,
            'tahun_bekerja' => $pekerjaan->masa_kerja_tahun,
            'bulan_bekerja' => $pekerjaan->masa_kerja_bulan,
            'nip' => $pekerjaan->nip_nrp_nik,
            'nama_hrd' => $pekerjaan->nama_hrd,
            'no_fixed_line_hrd' => $pekerjaan->no_fixed_line_hrd,
            'alamat_perusahaan' => $pekerjaan->alamat_perusahaan,
            'rt_perusahaan' => $pekerjaan->rt,
            'rw_perusahaan' => $pekerjaan->rw,
            'provinsi_perusahaan' => $pekerjaan->provinsi,
            'kabupaten_perusahaan' => $pekerjaan->kab_kota,
            'kecamatan_perusahaan' => $pekerjaan->kecamatan,
            'kelurahan_perusahaan' => $pekerjaan->kelurahan,
            'kodepos_perusahaan' => $pekerjaan->kode_pos,
            'no_telpon_usaha' => $pekerjaan->no_telp,
            'tahun_pengalaman_bekerja' => $pekerjaan->pengalaman_kerja_tahun,
            'bulan_pengalaman_bekerja' => $pekerjaan->pengalaman_kerja_bulan,
            'penghasilan' => "Rp." . number_format($pekerjaan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup' => "Rp." . number_format($pekerjaan->biaya_hidup, 2, ",", "."),
            'detail_penghasilan_lain_lain' => $pekerjaan->detail_penghasilan_lain_lain,
            'total_penghasilan_lain_lain' => "Rp." . number_format($pekerjaan->total_penghasilan_lain_lain, 2, ",", "."),
            'nilai_spt' => "Rp." . number_format($pekerjaan->nilai_spt, 2, ",", "."),

            // non fixincome
            'nama_perusahaan_non' => $pekerjaan->nama_perusahaan,
            'bidang_online_non' => $pekerjaan->bidang_online,
            'lama_usaha_non' => $pekerjaan->usia_perusahaan,
            'lama_tempat_usaha_non' => $pekerjaan->usia_tempat_usaha,
            'no_telpon_non' => $pekerjaan->no_telp,
            'no_hp_non' => $pekerjaan->no_hp,
            'alamat_perusahaan_non' => $pekerjaan->alamat_perusahaan,
            'rt_perusahaan_non' => $pekerjaan->rt,
            'rw_perusahaan_non' => $pekerjaan->rw,
            'provinsi_perusahaan_non' => $pekerjaan->provinsi,
            'kabupaten_perusahaan_non' => $pekerjaan->kab_kota,
            'kecamatan_perusahaan_non' => $pekerjaan->kecamatan,
            'kelurahan_perusahaan_non' => $pekerjaan->kelurahan,
            'kodepos_perusahaan_non' => $pekerjaan->kode_pos,
            'surat_ijin_non' => $pekerjaan->surat_ijin,
            'nomor_ijin_non' => $pekerjaan->no_surat_ijin,
            'penghasilan_non' => "Rp." . number_format($pekerjaan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_non' => "Rp." . number_format($pekerjaan->biaya_hidup, 2, ",", "."),

            // pengajuan
            'type_pendanaan' => $pengajuan->tipe_pendanaan,
            'tujuan_pendanaan' => $pengajuan->ctujuan_pembiayaan,
            'detail_pendanaan' =>  $pengajuan->detail_pendanaan,
            'alamat_objek_pendanaan' => $pengajuan->lokasi_proyek,
            'provinsi_objek_pendanaan' => $pengajuan->provinsi,
            'kota_objek_pendanaan' => $pengajuan->kota,
            'kecamatan_objek_pendanaan' => $pengajuan->kecamatan,
            'kelurahan_objek_pendanaan' => $pengajuan->kelurahan,
            'kodepos_objek_pendanaan' => $pengajuan->kode_pos,
            'harga_objek_pendanaan' => "Rp." . number_format($pengajuan->harga_objek_pendanaan, 2, ",", "."),
            'uang_muka' => "Rp." . number_format($pengajuan->uang_muka, 2, ",", "."),
            'uang_muka_percentage' => $uangMukaPercentage,
            'nilai_pengajuan' => "Rp." . number_format($pengajuan->pendanaan_dana_dibutuhkan, 2, ",", "."),
            'jangka_waktu' => $pengajuan->durasi_proyek,
            'nama_pemilik' => $pengajuan->nm_pemilik,
            'tlp_pemilik' => $pengajuan->no_tlp_pemilik,
            'alamat_pemilik' => $pengajuan->alamat_pemilik,
            'provinsi_pemilik' => $pengajuan->provinsi_pemilik,
            'kota_pemilik' => $pengajuan->kota_pemilik,
            'kecamatan_pemilik' => $pengajuan->kecamatan_pemilik,
            'kelurahan_pemilik' => $pengajuan->kelurahan_pemilik,
            'kodepos_pemilik' => $pengajuan->kd_pos_pemilik,
            'brw_pic' => $profil->brw_pic,
            'brw_pic_ktp' => $profil->brw_pic_ktp,
            'brw_pic_user_ktp' => $profil->brw_pic_user_ktp,
            'brw_pic_npwp' => $profil->brw_pic_npwp,
            'brw_type' => $profil->brw_type,
            'pekerjaan' => 0,
            'bidang_perusahaan' => 0,
            'bidang_pekerjaan' => $pekerjaan->bidang_pekerjaan,
            'pengalaman_pekerjaan' => 0,
            'pendapatan' => 0,
            'total_aset' => 0,
            'kewarganegaraan' => 0,
            'brw_online' => 0,

            // data Verifikasi
            'nilai_pefindo' => $nilai_pefindo,
            'grade_pefindo' => $grade_pefindo,
            'skor_personal' => $skor_personal,
            'skor_pendanaan' => $skor_pendanaan,
            'complete_id' => $complete_id,
            'ocr' => $ocr,
            'npwpa' => $npwpa,
            'npwpc' => $npwpc,
            'npwpd' => $npwpd,
            'npwp_verify' => $npwp_verify,
            'workplace_verification_f' => $workplace_verification_f,
            'negative_list_verification_j' => $negative_list_verification_j,
            'verify_property_k' => $verify_property_k,
            'ftv' => $ftv,
            'catatan_verifikasi' => $catatan_verifikasi,
            'keputusan' => $keputusan,
            'isiVerif' => $isiVerif,
            'pengajuan' => $pengajuan,
            'requisitionDetail' => $requisitionDetail,
            'requisitionHeader' => $requisitionHeader,
            'verifikator_vendor' => $verifikator_vendor,
            'id_field_verifikator_vendor' => $pengajuan->id_field_verifikator_vendor
        ]);
    }
}
