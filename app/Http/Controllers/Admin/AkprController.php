<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DateTime;
use DatePeriod;
use DateInterval;

use Excel;

use App\Http\Middleware\UserCheck;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use function GuzzleHttp\json_encode;
use Cart;
use Auth;
use App;
use DB;

use App\Http\Middleware\NotifikasiProyek;
use App\Http\Middleware\StatusProyek;

use Response;
use ZipArchive;
use Mail;
use App\Mail\HasilVerifikasiKElayakanKprEmail;
use App\Services\VerifikatorService;

class AkprController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin')->only(['dashboard', 'detailPengajuan']);
    }

    public function dashboard()
    {
        return view('pages.admin.kpr_dashboard');
    }

    public function ListPengajuan()
    {
        $pengajuans = DB::SELECT("SELECT a.brw_id,a.pengajuan_id, DATE_FORMAT(a.created_at,'%d/%m/%Y') AS tgl_pengajuan,UPPER(b.nama) as nama_cust, 'Dana Rumah' AS tipe_pendanaan, c.tujuan_pembiayaan , CONCAT('Rp.',FORMAT(a.pendanaan_dana_dibutuhkan,0)) AS nilai_pengajuan, a.status AS stts 
        FROM brw_pengajuan a, brw_user_detail b, m_tujuan_pembiayaan c 
        WHERE a.brw_id=b.brw_id 
        AND a.pendanaan_tujuan = c.id 
        AND a.pendanaan_tipe = 2 
        ORDER BY pengajuan_id DESC");

        $response = ['data' => $pengajuans];
        return response()->json($response);
    }

    public function detailPengajuan(Request $request)
    {
        $pengajuanId = $request->pengajuanId;
        $brw_id = $request->brw_id;

        $verifikasi = DB::SELECT("SELECT * FROM brw_verifikasi_awal WHERE pengajuan_id = " . $pengajuanId . " AND brw_id = " . $brw_id . "");
        if ($verifikasi == NULL) {
            $isiVerif = 0;
        } else {
            $isiVerif = 1;
        }

        // dd($verifikasi[0]);

        $nilai_pefindo = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_pefindo;
        $grade_pefindo = ($isiVerif == 0) ? 0 : $verifikasi[0]->grade_pefindo;
        $skor_personal = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_personal_credolab;
        $skor_pendanaan = ($isiVerif == 0) ? 0 : $verifikasi[0]->skor_pendanaan_credolab;
        $complete_id = ($isiVerif == 0) ? 0 : $verifikasi[0]->complete_id;
        $ocr = ($isiVerif == 0) ? 0 : $verifikasi[0]->ocr;
        $npwpa = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_a;
        $npwpc = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_c;
        $npwpd = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_d;
        $npwp_verify = ($isiVerif == 0) ? 0 : $verifikasi[0]->npwp_verify;
        $workplace_verification_f = ($isiVerif == 0) ? 0 : $verifikasi[0]->workplace_verification_f;
        $negative_list_verification_j = ($isiVerif == 0) ? 0 : $verifikasi[0]->negative_list_verification_j;
        $verify_property_k = ($isiVerif == 0) ? 0 : $verifikasi[0]->verify_property_k;
        $ftv = ($isiVerif == 0) ? 0 : $verifikasi[0]->financing_to_value;
        $catatan_verifikasi = ($isiVerif == 0) ? 0 : $verifikasi[0]->catatan;
        $keputusan = ($isiVerif == 0) ? 0 : $verifikasi[0]->status;


        $pengajuan = DB::SELECT("SELECT a.*, 
        DATE_FORMAT(a.created_at,'%d/%m/%Y') AS tgl_pengajuan,UPPER(b.nama) as nama_cust, 
        'Dana Rumah' AS tipe_pendanaan, 
        c.tujuan_pembiayaan as ctujuan_pembiayaan,
        CONCAT('Rp.',FORMAT(a.pendanaan_dana_dibutuhkan,0)) AS nilai_pengajuan, 
        a.status AS stts 
        FROM brw_pengajuan a, brw_user_detail b, m_tujuan_pembiayaan c 
        WHERE a.brw_id=b.brw_id 
        AND a.pendanaan_tujuan = c.id 
        AND a.pengajuan_id = " . $pengajuanId . "")[0];



        $getprofil = DB::SELECT("SELECT a.*,b.*,c.brw_norek,c.brw_nm_pemilik,c.brw_kd_bank,h.nama_bank, c.kantor_cabang_pembuka, d.agama as dagama , e.pendidikan as ependidikan, f.jenis_kawin as fkawin, b.status_kawin as id_kawin
        FROM brw_user_detail b
        LEFT JOIN brw_pengajuan a ON  a.brw_id = b.brw_id 
        LEFT JOIN brw_rekening c ON  a.brw_id = c.brw_id
        LEFT JOIN m_agama d ON  b.agama = d.id_agama
        LEFT JOIN m_pendidikan e ON  b.pendidikan_terakhir = e.id_pendidikan
        LEFT JOIN m_kawin f ON  b.status_kawin = f.id_kawin
        LEFT JOIN m_pekerjaan g ON  g.id_pekerjaan = b.pekerjaan
        LEFT JOIN m_bank h ON  c.brw_kd_bank = h.kode_bank
        WHERE a.pengajuan_id = " . $pengajuanId . "");

        if ($getprofil == NULL) {
            $profil = 0;
        } else {
            $profil = $getprofil[0];
        }
        // dd($profil);


        $getpekerjaan = DB::SELECT("SELECT c.pekerjaan,d.tipe_online as bidang_online, 
        case when b.bentuk_badan_usaha = 0 then 0 ELSE 
        (SELECT bentuk_badan_usaha FROM m_bentuk_badan_usaha WHERE id = b.bentuk_badan_usaha) END AS bbu ,b.*
        FROM brw_user_detail a
        LEFT JOIN brw_user_detail_penghasilan b ON  b.brw_id2 = a.brw_id 
        LEFT JOIN m_pekerjaan c ON  a.pekerjaan = c.id_pekerjaan
        LEFT JOIN m_online d ON  a.bidang_online = d.id_online
        WHERE a.brw_id = " . $brw_id . "");

        if ($getpekerjaan == NULL) {
            $pekerjaan = 0;
        } else {
            $pekerjaan = $getpekerjaan[0];
        }
        // dd($pekerjaan);

        $noPasangan = 1;
        $v_pekerjaanPasangan = 1;
        if ($profil->id_kawin == 1) {
            $getPasangan = DB::SELECT("SELECT a.*,b.agama as bagama, c.pendidikan as cpendidikan,
            case when a.bentuk_badan_usaha = 0 then 0 ELSE 
            (SELECT bentuk_badan_usaha FROM m_bentuk_badan_usaha WHERE id = a.bentuk_badan_usaha) END AS dbbu
            FROM brw_pasangan a, m_agama b , m_pendidikan c
            WHERE a.agama = b.id_agama 
            AND a.pendidikan  = c.id_pendidikan
            AND a.pasangan_id = " . $brw_id . "");
            if ($getPasangan == NULL) {
                $noPasangan = 0;
            } else {
                $pasangan = $getPasangan[0];
            }
            if ($pekerjaan->skema_pembiayaan == 2) {
                $getPekerjaanPasangan = DB::SELECT("SELECT b.pekerjaan as pekerjaan_pasangan, d.bidang_pekerjaan as bidang_pekerjaan_pasangan , c.tipe_online as bidang_online_pasangan 
                FROM brw_user_detail a , m_pekerjaan b , m_online c, m_bidang_pekerjaan d
                WHERE a.pekerjaan_pasangan = b.id_pekerjaan 
                AND a.bd_pekerjaan_pasangan = d.id_bidang_pekerjaan
                AND a.bd_pekerjaanO_pasangan = c.id_online
                AND a.brw_id = " . $brw_id . "");
                if ($getPekerjaanPasangan == NULL) {
                    $v_pekerjaanPasangan = 0;
                } else {
                    $pekerjaanPasangan = $getPekerjaanPasangan[0];
                }
            } else {
                $v_pekerjaanPasangan = 0;
            }
            // dd($pekerjaanPasangan);
        } else {
            $noPasangan = 0;
            $v_pekerjaanPasangan = 0;
        }





        // dd([
        //     $profil->alamat


        // ]);
        return view('pages.borrower.verifikasi_kpr_profil')->with([
            // informasi pribadi
            'brw_id' => $brw_id,
            'pengajuanId' => $pengajuanId,
            'nama' => $profil->nama,
            'jns_kelamin' => $profil->jns_kelamin,
            'ktp' => $profil->ktp,
            'kk' => $profil->nomor_kk_pribadi,
            'tempat_lahir' => $profil->tempat_lahir,
            'tgl_lahir' => $profil->tgl_lahir,
            'telepon' => $profil->no_tlp,
            'agama' => $profil->dagama,
            'pendidikan_terakhir' => $profil->ependidikan,
            'id_kawin' => $profil->id_kawin,
            'status_kawin' => $profil->fkawin,
            'nm_ibu' => $profil->nm_ibu,
            'npwp' => $profil->npwp,

            // informasi alamat
            'alamat' => $profil->alamat,
            'provinsi' => $profil->provinsi,
            'kota' => $profil->kota,
            'kecamatan' => $profil->kecamatan,
            'kelurahan' => $profil->kelurahan,
            'kode_pos' => $profil->kode_pos,
            'status_rumah' => $profil->status_rumah,

            // informasi domisili alamat 
            'domisili_alamat' => $profil->domisili_alamat,
            'domisili_provinsi' => $profil->domisili_provinsi,
            'domisili_kota' => $profil->domisili_kota,
            'domisili_kecamatan' => $profil->domisili_kecamatan,
            'domisili_kelurahan' => $profil->domisili_kelurahan,
            'domisili_kode_pos' => $profil->domisili_kd_pos,
            'domisili_status_rumah' => $profil->domisili_status_rumah,

            // informasi rekening
            'brw_norek' => $profil->brw_norek,
            'brw_nm_pemilik' => $profil->brw_nm_pemilik,
            'brw_kd_bank' => $profil->nama_bank,
            'kantorcabangpembuka' => $profil->kantor_cabang_pembuka,

            // informasi pasangan
            'pasangan_nama' => ($noPasangan == 0) ? '0' : $pasangan->nama,
            'pasangan_jenis_kelamin' => ($noPasangan == 0) ? '0' : $pasangan->jenis_kelamin,
            'pasangan_ktp' => ($noPasangan == 0) ? '0' : $pasangan->ktp,
            'pasangan_tempat_lahir' => ($noPasangan == 0) ? '0' : $pasangan->tempat_lahir,
            'pasangan_tanggal_lahir' => ($noPasangan == 0) ? '0' : $pasangan->tgl_lahir,
            'pasangan_telepon' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'pasangan_agama' => ($noPasangan == 0) ? '0' : $pasangan->bagama,
            'pasangan_pendidikan_terakhir' => ($noPasangan == 0) ? '0' : $pasangan->cpendidikan,
            'pasangan_npwp' => ($noPasangan == 0) ? '0' : $pasangan->npwp,
            'pasangan_alamat' => ($noPasangan == 0) ? '0' : $pasangan->alamat,
            'pasangan_provinsi' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'pasangan_kota' => ($noPasangan == 0) ? '0' : $pasangan->kota,
            'pasangan_kecamatan' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'pasangan_kelurahan' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'pasangan_kode_pos' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,

            // fixincome
            'nama_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nama_perusahaan,
            'sumberpengembaliandana_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->sumber_pengembalian_dana,
            'bentuk_badan_usaha_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->dbbu,
            'status_kepegawaian_pasangan' => ($noPasangan == 0) ? '0' : (($pasangan->status_pekerjaan == 1) ? 'Kontrak' : 'Tetap'),
            'usia_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->usia_perusahaan,
            'departemen_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->departemen,
            'jabatan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->jabatan,
            'tahun_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->masa_kerja_tahun,
            'bulan_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->masa_kerja_bulan,
            'nip_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nip_nrp_nik,
            'nama_hrd_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->nama_hrd,
            'no_fixed_line_hrd_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->no_fixed_line_hrd,
            'no_telpon_usaha_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->no_telp_pekerjaan_pasangan,
            'alamat_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->alamat_perusahaan,
            'rt_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->rt_pekerjaan_pasangan,
            'rw_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->rw_pekerjaan_pasangan,
            'provinsi_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'kabupaten_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kab_kota_pekerjaan_pasangan,
            'kecamatan_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'kelurahan_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'kodepos_perusahaan_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,
            'tahun_pengalaman_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->pengalaman_kerja_tahun,
            'bulan_pengalaman_bekerja_pasangan' => ($noPasangan == 0) ? '0' : $pasangan->pengalaman_kerja_bulan,
            'penghasilan_pasangan' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_pasangan' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->biaya_hidup, 2, ",", "."),

            // nonfixincome
            'nama_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->nama_perusahaan,
            'lama_usaha_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->usia_perusahaan,
            'lama_tempat_usaha_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->usia_tempat_usaha,
            'no_telpon_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'no_hp_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_hp,
            'alamat_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->alamat_perusahaan,
            'rt_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->rt_pekerjaan_pasangan,
            'rw_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->rw_pekerjaan_pasangan,
            'provinsi_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->provinsi,
            'kabupaten_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kab_kota_pekerjaan_pasangan,
            'kecamatan_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kecamatan,
            'kelurahan_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kelurahan,
            'kodepos_perusahaan_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->kode_pos,
            'surat_ijin_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->surat_ijin,
            'nomor_ijin_pasangan_non' => ($noPasangan == 0) ? '0' : $pasangan->no_surat_ijin,
            'penghasilan_pasangan_non' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_pasangan_non' => ($noPasangan == 0) ? '0' : "Rp." . number_format($pasangan->biaya_hidup, 2, ",", "."),

            // informasi pekerjaan pasangan 
            'pekerjaan_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->pekerjaan_pasangan,
            'bidang_pekerjaan_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->bidang_pekerjaan_pasangan,
            'bidang_online_pasangan' => ($noPasangan == 0 || $v_pekerjaanPasangan == 0) ? '0' : $pekerjaanPasangan->bidang_online_pasangan,

            // informasi pekerjaan
            // fixincome
            'jenis_pekerjaan' => $pekerjaan->pekerjaan,
            'sumberpengembaliandana' => $pekerjaan->sumber_pengembalian_dana,
            'skemapembiayaan' => $pekerjaan->skema_pembiayaan,
            'nama_perusahaan' => $pekerjaan->nama_perusahaan,
            'bidang_online' => $pekerjaan->bidang_online,
            'bentuk_badan_usaha' => $pekerjaan->bbu,
            'status_kepegawaian' => ($pekerjaan->status_pekerjaan == 1) ? 'Kontrak' : 'Tetap',
            'usia_perusahaan' => $pekerjaan->usia_perusahaan,
            'departemen' => $pekerjaan->departemen,
            'jabatan' => $pekerjaan->jabatan,
            'tahun_bekerja' => $pekerjaan->masa_kerja_tahun,
            'bulan_bekerja' => $pekerjaan->masa_kerja_bulan,
            'nip' => $pekerjaan->nip_nrp_nik,
            'nama_hrd' => $pekerjaan->nama_hrd,
            'no_fixed_line_hrd' => $pekerjaan->no_fixed_line_hrd,
            'alamat_perusahaan' => $pekerjaan->alamat_perusahaan,
            'rt_perusahaan' => $pekerjaan->rt,
            'rw_perusahaan' => $pekerjaan->rw,
            'provinsi_perusahaan' => $pekerjaan->provinsi,
            'kabupaten_perusahaan' => $pekerjaan->kab_kota,
            'kecamatan_perusahaan' => $pekerjaan->kecamatan,
            'kelurahan_perusahaan' => $pekerjaan->kelurahan,
            'kodepos_perusahaan' => $pekerjaan->kode_pos,
            'no_telpon_usaha' => $pekerjaan->no_telp,
            'tahun_pengalaman_bekerja' => $pekerjaan->pengalaman_kerja_tahun,
            'bulan_pengalaman_bekerja' => $pekerjaan->pengalaman_kerja_bulan,
            'penghasilan' => "Rp." . number_format($pekerjaan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup' => "Rp." . number_format($pekerjaan->biaya_hidup, 2, ",", "."),
            'detail_penghasilan_lain_lain' => $pekerjaan->detail_penghasilan_lain_lain,
            'total_penghasilan_lain_lain' => "Rp." . number_format($pekerjaan->total_penghasilan_lain_lain, 2, ",", "."),
            'nilai_spt' => "Rp." . number_format($pekerjaan->nilai_spt, 2, ",", "."),

            // non fixincome
            'nama_perusahaan_non' => $pekerjaan->nama_perusahaan,
            'bidang_online_non' => $pekerjaan->bidang_online,
            'lama_usaha_non' => $pekerjaan->usia_perusahaan,
            'lama_tempat_usaha_non' => $pekerjaan->usia_tempat_usaha,
            'no_telpon_non' => $pekerjaan->no_telp,
            'no_hp_non' => $pekerjaan->no_hp,
            'alamat_perusahaan_non' => $pekerjaan->alamat_perusahaan,
            'rt_perusahaan_non' => $pekerjaan->rt,
            'rw_perusahaan_non' => $pekerjaan->rw,
            'provinsi_perusahaan_non' => $pekerjaan->provinsi,
            'kabupaten_perusahaan_non' => $pekerjaan->kab_kota,
            'kecamatan_perusahaan_non' => $pekerjaan->kecamatan,
            'kelurahan_perusahaan_non' => $pekerjaan->kelurahan,
            'kodepos_perusahaan_non' => $pekerjaan->kode_pos,
            'surat_ijin_non' => $pekerjaan->surat_ijin,
            'nomor_ijin_non' => $pekerjaan->no_surat_ijin,
            'penghasilan_non' => "Rp." . number_format($pekerjaan->pendapatan_borrower, 2, ",", "."),
            'biaya_hidup_non' => "Rp." . number_format($pekerjaan->biaya_hidup, 2, ",", "."),

            // pengajuan
            'type_pendanaan' => $pengajuan->tipe_pendanaan,
            'tujuan_pendanaan' => $pengajuan->ctujuan_pembiayaan,
            'detail_pendanaan' =>  $pengajuan->detail_pendanaan,
            'alamat_objek_pendanaan' => $pengajuan->lokasi_proyek,
            'provinsi_objek_pendanaan' => $pengajuan->provinsi,
            'kota_objek_pendanaan' => $pengajuan->kota,
            'kecamatan_objek_pendanaan' => $pengajuan->kecamatan,
            'kelurahan_objek_pendanaan' => $pengajuan->kelurahan,
            'kodepos_objek_pendanaan' => $pengajuan->kode_pos,
            'harga_objek_pendanaan' => "Rp." . number_format($pengajuan->harga_objek_pendanaan, 2, ",", "."),
            'uang_muka' => "Rp." . number_format($pengajuan->uang_muka, 2, ",", "."),
            'nilai_pengajuan' => "Rp." . number_format($pengajuan->pendanaan_dana_dibutuhkan, 2, ",", "."),
            'jangka_waktu' => $pengajuan->durasi_proyek,
            'nama_pemilik' => $pengajuan->nm_pemilik,
            'tlp_pemilik' => $pengajuan->no_tlp_pemilik,
            'alamat_pemilik' => $pengajuan->alamat_pemilik,
            'provinsi_pemilik' => $pengajuan->provinsi_pemilik,
            'kota_pemilik' => $pengajuan->kota_pemilik,
            'kecamatan_pemilik' => $pengajuan->kecamatan_pemilik,
            'kelurahan_pemilik' => $pengajuan->kelurahan_pemilik,
            'kodepos_pemilik' => $pengajuan->kd_pos_pemilik,
            'brw_pic' => $profil->brw_pic,
            'brw_pic_ktp' => $profil->brw_pic_ktp,
            'brw_pic_user_ktp' => $profil->brw_pic_user_ktp,
            'brw_pic_npwp' => $profil->brw_pic_npwp,
            'brw_type' => $profil->brw_type,
            'pekerjaan' => 0,
            'bidang_perusahaan' => 0,
            'bidang_pekerjaan' => 0,
            'pengalaman_pekerjaan' => 0,
            'pendapatan' => 0,
            'total_aset' => 0,
            'kewarganegaraan' => 0,
            'brw_online' => 0,

            // data Verifikasi
            'nilai_pefindo' => $nilai_pefindo,
            'grade_pefindo' => $grade_pefindo,
            'skor_personal' => $skor_personal,
            'skor_pendanaan' => $skor_pendanaan,
            'complete_id' => $complete_id,
            'ocr' => $ocr,
            'npwpa' => $npwpa,
            'npwpc' => $npwpc,
            'npwpd' => $npwpd,
            'npwp_verify' => $npwp_verify,
            'workplace_verification_f' => $workplace_verification_f,
            'negative_list_verification_j' => $negative_list_verification_j,
            'verify_property_k' => $verify_property_k,
            'ftv' => $ftv,
            'catatan_verifikasi' => $catatan_verifikasi,
            'keputusan' => $keputusan,
            'isiVerif' => $isiVerif
        ]);
    }

    public function simpanVerifikasi(Request $request)
    {
        $brw_id = $request->brw_id;
        $pengajuanId = $request->pengajuanId;
        $nilai_pefindo = $request->nilai_pefindo;
        $grade_pefindo = $request->grade_pefindo;
        $complete_id = $request->complete_id;
        $ocr = $request->ocr;
        $npwpa = $request->npwpa;
        $npwpc = $request->npwpc;
        $npwpd = $request->npwpd;
        $npwp_verify = $request->npwp_verify;
        $workplace_verification_f = $request->workplace_verification_f;
        $negative_list_verification_j = $request->ngetive_list_verification_j;
        $verify_property_k = $request->verify_property_k;
        $skor_personal = $request->skor_personal;
        $skor_pendanaan = $request->skor_pendanaan;
        $ftv = $request->ftv;
        $catatan_verifikasi = $request->catatan_verifikasi;
        $keputusan = $request->keputusan;

        $getEmail = DB::SELECT("SELECT email FROM brw_user WHERE brw_id = " . $brw_id . "")[0];

        $cek = DB::SELECT("SELECT * FROM brw_verifikasi_awal WHERE pengajuan_id = " . $pengajuanId . " AND brw_id = " . $brw_id . "");
        if ($cek == NULL) {
            $upsert = DB::table('brw_verifikasi_awal')->insert(
                [
                    'brw_id' => $brw_id,
                    'pengajuan_id' => $pengajuanId,
                    'skor_pefindo' => $nilai_pefindo,
                    'grade_pefindo' => $grade_pefindo,
                    'complete_id' => $complete_id,
                    'ocr' => $ocr,
                    'npwp_a' => $npwpa,
                    'npwp_c' => $npwpc,
                    'npwp_d' => $npwpd,
                    'npwp_verify' => $npwp_verify,
                    'workplace_verification_f' => $workplace_verification_f,
                    'negative_list_verification_j' => $negative_list_verification_j,
                    'verify_property_k' => $verify_property_k,
                    'skor_personal_credolab' => $skor_personal,
                    'skor_pendanaan_credolab' => $skor_pendanaan,
                    'financing_to_value' => $ftv,
                    'catatan' => $catatan_verifikasi,
                    'status' => $keputusan
                ]
            );
        } else {
            $upsert = DB::table('brw_verifikasi_awal')
                ->where('pengajuan_id', $pengajuanId)
                ->where('brw_id', $brw_id)
                ->update([
                    'skor_pefindo' => $nilai_pefindo,
                    'grade_pefindo' => $grade_pefindo,
                    'complete_id' => $complete_id,
                    'ocr' => $ocr,
                    'npwp_a' => $npwpa,
                    'npwp_c' => $npwpc,
                    'npwp_d' => $npwpd,
                    'npwp_verify' => $npwp_verify,
                    'workplace_verification_f' => $workplace_verification_f,
                    'negative_list_verification_j' => $negative_list_verification_j,
                    'verify_property_k' => $verify_property_k,
                    'skor_personal_credolab' => $skor_personal,
                    'skor_pendanaan_credolab' => $skor_pendanaan,
                    'financing_to_value' => $ftv,
                    'catatan' => $catatan_verifikasi,
                    'status' => $keputusan
                ]);
        }

        if ($upsert) {
          
            DB::table('brw_pengajuan')
                ->where('pengajuan_id', $pengajuanId)
                ->update([
                    'status' => $keputusan
                ]);
            $penerimaPendanaan = DB::SELECT("SELECT a.nama as nama_pengguna, b.pendanaan_tipe, b.pendanaan_tujuan,  c.pendanaan_nama as nama_pendanaan , b.pendanaan_dana_dibutuhkan as dana_dibutuhkan, DATE_FORMAT(b.created_at,'%d %M %Y') as tanggal_pengajuan, " . $keputusan . " as status
            FROM brw_user_detail a, brw_pengajuan b, brw_tipe_pendanaan c 
            WHERE a.brw_id = b.brw_id
            AND b.pendanaan_tipe = c.tipe_id
            AND b.pengajuan_id = " . $pengajuanId . "")[0];

            $email = new HasilVerifikasiKElayakanKprEmail($penerimaPendanaan);
            Mail::to($getEmail->email)->send($email);
          
            //If status diterima
            // if ($keputusan == "1") {
            //     $verifikatorService = new VerifikatorService();
            //     $resultVerifikator = $verifikatorService->makeAnalisaPendanaan($pengajuanId, $brw_id);
            // }
         
            $response = ['data' => 1];

        } else {
            $response = ['data' => 0];
        }

        return response()->json($response);
    }
}
