<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DataTables;
// use GuzzleHttp\Client;
// use DateTime;
// use DatePeriod;
// use DateInterval;

// use Excel;

use App\BorrowerAnalisaPendanaan;
use App\BorrowerPengajuan;
use App\BorrowerHdAnalisa;
use App\BorrowerDetailCeklistAnalisa;
use App\BorrowerDetailLembarFasilitasExternal;
use App\BorrowerDetailLembarPendapatan;
use App\BorrowerAgunan;
use App\BorrowerDetailLembarAnalisa;
use App\BorrowerDetailDeviasi;
use App\BorrowerDetailResumeAkad;

// use App\Http\Middleware\UserCheck;
use Auth;
use Carbon\Carbon;
use DB;
// use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Facades\Hash;
// use function GuzzleHttp\json_encode;
// use Cart;
// use Auth;
// use App;

// use App\Http\Middleware\NotifikasiProyek;
// use App\Http\Middleware\StatusProyek;

// use Response;
// use ZipArchive;
// use Mail;
// use App\Mail\HasilVerifikasiKElayakanKprEmail;


class AnalisController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function listPendanaan(BorrowerAnalisaPendanaan $listBrwAnalisPendanaan) {
        return view('pages.admin.analis.list_pendanaan');
    }

    public function getJsonListPendanaan(BorrowerAnalisaPendanaan $listBrwAnalisPendanaan) {

        $data = $listBrwAnalisPendanaan::from('brw_analisa_pendanaan as a')
        ->leftjoin('brw_tipe_pendanaan as b', 'b.tipe_id', 'a.jenis_pendanaan')
        ->leftjoin('m_tujuan_pembiayaan as c', 'c.id', 'a.tujuan_pendanaan')
        ->select('a.*', 'b.pendanaan_nama as jenis_pendanaan', 'c.tujuan_pembiayaan as tujuan_pendanaan')
        ->where('a.status_verifikator', '3')
        // ->where('a.status_analis', '4')
        ->orderBy('a.tanggal_pengajuan', 'DESC')
        ->get();

        return \DataTables::of($data)
            ->addColumn('status', function($data) {
                $status_analis = $data->status_analis;
                $pengajuan_id = $data->pengajuan_id;
                $detail_route = route('analis.detail-pendanaan', ['pengajuan_id' => $pengajuan_id, 'status_analis' => $status_analis]);
                $btn_status = '';
                switch ($status_analis) {
                    case '5':
                        $btn_status = '<a id="btn_status" class="btn btn-primary text-white btn-sm" type="button" data-id="'.$pengajuan_id.'" href="'.$detail_route.'">Proses</a>';
                        break;
                    
                    case '6':
                        $btn_status = '<a id="btn_status" class="btn btn-success text-white btn-sm" type="button" data-id="'.$pengajuan_id.'" href="'.$detail_route.'">Selesai</a>';
                        break;
                        
                    default :
                        $btn_status = '<a id="btn_status" class="btn btn-secondary text-white btn-sm" type="button" data-id="'.$pengajuan_id.'" href="'.$detail_route.'">Baru</a>';
                        break;
                }
                return $btn_status;
            })
            ->rawColumns(['status'])
            ->make(true);
    }

    public function detailPendanaan(Request $request, BorrowerPengajuan $brwPengajuan) {

        $status_analis = $request->status_analis;

        $data =  $brwPengajuan::from('brw_pengajuan as a')
        ->leftjoin('brw_tipe_pendanaan as b', 'b.tipe_id', 'a.pendanaan_tipe')
        ->leftjoin('m_tujuan_pembiayaan as c', 'c.id', 'a.pendanaan_tujuan')
        ->where('a.pengajuan_id', $request->pengajuan_id)
        ->select('a.*', 'b.pendanaan_nama', 'c.tujuan_pembiayaan')
        ->groupBy('a.pengajuan_id')
        ->first();
        
        return view('pages.admin.analis.detail_pendanaan', ['data' => $data, 'status_analis' => $status_analis]);
    }

    public function listPekerjaan(
        Request $request, 
        BorrowerHdAnalisa $brw_hd_analis
    ) {

        $data = $brw_hd_analis::from('brw_hd_analisa as a')
        ->leftjoin('brw_pengajuan as b', 'b.pengajuan_id', 'a.pengajuan_id')
        ->leftjoin('m_akad as c', 'c.id', 'b.pendanaan_akad')
        ->leftjoin('brw_analisa_pendanaan as d', 'd.pengajuan_id', 'a.pengajuan_id')
        ->leftjoin('brw_user_detail as e', 'e.brw_id', 'b.brw_id')
        ->select('a.*', 'c.jenis_akad', 'd.status_analis', 'e.nama', 'e.ktp', 'e.jns_kelamin', '.e.brw_pic')
        ->where('a.pengajuan_id', $request->pengajuan_id)
        ->first();

        return view('pages.admin.analis.list_pekerjaan', ['data' => $data, 'pengajuan_id' => $request->pengajuan_id]);
    }

    public function detailPekerjaan(
        Request $request, 
        BorrowerHdAnalisa $brw_hd_analis, 
        BorrowerPengajuan $brw_pengajuan,
        BorrowerDetailCeklistAnalisa $brw_dtl_ceklist_analisa,
        BorrowerDetailDeviasi $brw_dtl_deviasi,
        BorrowerDetailResumeAkad $brw_dtl_resume_akad
    ) {
        // 1 = RAC, 2 = Lembar Perhitungan, 3 = Resume Akad
        $keterangan = $request->keterangan;

        // a = brw_hd_analisa, b = brw_analisa_pendanaan
        $data_hd_analis = $brw_hd_analis::from('brw_hd_analisa as a')
        ->leftjoin('brw_analisa_pendanaan as b', 'b.pengajuan_id', 'a.pengajuan_id')
        ->select([
            'a.nama_analis_dsi as nama_kredit_analis',
            'a.created_at_rac_dsi as tanggal_analisa_rac' , 
            'a.created_at_lembar_dsi as tanggal_analisa_lembar' , 
            'a.created_at_resume_dsi as tanggal_analisa_resume' , 
            'b.status_analis'
        ])
        ->where('a.pengajuan_id', $request->pengajuan_id)
        ->first();

        // a = brw_pengajuan, b = brw_user_detail, 
        // c = brw_user_detail_penghasilan, d = brw_tipe_pendanaan, 
        // e = brw_dtl_naup, f = m_tujuan_pembiayaan
        $data_brw = $brw_pengajuan::from('brw_pengajuan AS a')
        ->leftjoin('brw_user_detail_pengajuan AS b', 'b.pengajuan_id', 'a.pengajuan_id')
        ->leftjoin('brw_user_detail_penghasilan_pengajuan AS c', 'c.pengajuan_id', 'a.pengajuan_id')
        ->leftjoin('brw_tipe_pendanaan AS d', 'd.tipe_id', 'a.pendanaan_tipe')
        ->leftjoin('brw_dtl_naup AS e', 'e.pengajuan_id', 'a.pengajuan_id')
        ->leftjoin('m_tujuan_pembiayaan AS f', 'f.id', 'a.pendanaan_tujuan')
        // ->leftjoin('m_jabatan as g', 'g.id', 'c.jabatan')
        ->leftjoin('m_akad as h', 'h.id', 'a.pendanaan_akad')
        ->selectRaw(
            'a.pengajuan_id,
            a.pendanaan_dana_dibutuhkan AS plafon_pengajuan,
            a.durasi_proyek AS jangka_waktu_pendanaan,
            a.harga_objek_pendanaan,
            a.persentase_margin AS margin,
            a.lokasi_proyek,
            a.provinsi,
            a.kota,
            a.kecamatan,
            a.kelurahan,
            a.kode_pos,
            a.jenis_properti,
            a.uang_muka,
            b.brw_id,
            b.status_kawin,
            b.tgl_lahir,
            b.nama AS nama_penerima_pendana,
            c.status_pekerjaan As status_karyawan,
            c.nama_perusahaan As tempat_bekerja,
		    c.masa_kerja_tahun,
		    c.masa_kerja_bulan,
		    c.usia_perusahaan,
            c.sumber_pengembalian_dana AS kategori_penerima_pendanaan,
            c.skema_pembiayaan,
            c.jabatan,
            d.pendanaan_nama,
            e.no_naup,
            f.tujuan_pembiayaan AS produk_pembiayaan,
            h.jenis_akad AS akad_pendanaan,
            date_format(DATE_SUB(date_format(NOW(),"%Y%m%d"), INTERVAL ifnull(c.masa_kerja_tahun*12 + c.masa_kerja_bulan, 0) Month), "%Y") as `lama_bekerja`,
            date_format(DATE_SUB(date_format(NOW(),"%Y%m%d"), INTERVAL ifnull(c.usia_perusahaan*12, 0) Month), "%Y") as lama_perusahaan,
            date_format(DATE_SUB(date_format(NOW(),"%Y%m%d"), INTERVAL ifnull(c.usia_perusahaan*12, 0) Month), "%Y") as lama_usaha,
            date_format(DATE_SUB(date_format(NOW(),"%Y%m%d"), INTERVAL ifnull(c.usia_tempat_usaha*12, 0) Month), "%Y") as lama_tempat_usaha'
        )
        ->where('a.pengajuan_id', $request->pengajuan_id)
        ->first();

        switch ($keterangan) {
            case '1': // RAC

                $data_chk_analisa = $brw_dtl_ceklist_analisa::where('id_pengajuan', $request->pengajuan_id)->first();
                $data_verifikasi = DB::SELECT("SELECT * FROM brw_verifikasi_awal WHERE pengajuan_id = " . $request->pengajuan_id . " AND brw_id = " . $data_brw->brw_id . "");
 
                return view('pages.admin.analis.detail_pekerjaan', [
                    'data_hd_analis' => $data_hd_analis, 
                    'data_brw' => $data_brw, 
                    'data_chk_analisa' => $data_chk_analisa,
                    'data_verifikasi' => ($data_verifikasi == NULL) ? 0 : $data_verifikasi
                ]);
                break;
            case '2': // Lembar Perhitungan

                $master_akad = DB::table('m_akad')->get();
                $default_margin = DB::table('m_param_kalkulator')->where('tenor', 999)->value('margin_efektif');

                $data_brw_pasangan = DB::table('brw_pasangan')->where('pasangan_id', $data_brw->brw_id)->select(['nama','tgl_lahir'])->get();
                $brw_dtl_lembar_fasilitas_external = DB::table('brw_dtl_lembar_fasilitas_external')->where('id_pengajuan', $request->pengajuan_id)->get();
                    
                $brw_pendapatan = DB::table('brw_dtl_lembar_pendapatan')->where('id_pengajuan', $request->pengajuan_id)->get();
                
                $data_objek_pendanaan = DB::table('brw_agunan')->where('id_pengajuan', $request->pengajuan_id)->get();
                $data_dtl_lembar_analisa = DB::table('brw_dtl_lembar_hasil_analisa')->where('id_pengajuan', $request->pengajuan_id)->get();

                return view('pages.admin.analis.detail_pekerjaan', [
                    'data_hd_analis' => $data_hd_analis, 
                    'data_brw' => $data_brw,
                    'default_margin'=> $default_margin,
                    'master_akad' => $master_akad,
                    'data_brw_pasangan' =>  !$data_brw_pasangan->isEmpty() ? $data_brw_pasangan[0] : '',
                    'brw_lembar_fasilitas_external' => !$brw_dtl_lembar_fasilitas_external->isEmpty() ? $brw_dtl_lembar_fasilitas_external : '',
                    'brw_pendapatan' => !$brw_pendapatan->isEmpty() ? $brw_pendapatan[0] : '',
                    'data_objek_pendanaan' => !$data_objek_pendanaan->isEmpty() ? $data_objek_pendanaan[0] : '',
                    'data_lembar_analisa' => !$data_dtl_lembar_analisa->isEmpty() ? $data_dtl_lembar_analisa[0] : ''
                ]);
                break;
            case '3': // Resume Akad

                $data_brw_pasangan = DB::table('brw_pasangan as a')->where('a.pasangan_id', $data_brw->brw_id)
                // ->leftjoin('m_jabatan as b', 'b.id', 'a.jabatan')
                ->select([
                    'a.nama',
                    'a.tgl_lahir',
                    'a.status_pekerjaan',
                    'a.nama_perusahaan',
                    // 'b.jabatan as jabatan'
                    'a.jabatan'
                ])->get();

                $brw_pendapatan = DB::table('brw_dtl_lembar_pendapatan')
                ->where('id_pengajuan', $request->pengajuan_id)->get();

                 $data_dtl_lembar_analisa = DB::table('brw_dtl_lembar_hasil_analisa')
                ->where('id_pengajuan', $request->pengajuan_id)->get();

                $data_objek_pendanaan = DB::table('brw_agunan')
                ->where('id_pengajuan', $request->pengajuan_id)->get();

                $data_resume_akad = $brw_dtl_resume_akad::where('pengajuan_id', $request->pengajuan_id)->first();

                $data_deviasi = $brw_dtl_deviasi::where('pengajuan_id', $request->pengajuan_id)->get();

                return view('pages.admin.analis.detail_pekerjaan', [
                    'data_hd_analis' => $data_hd_analis, 
                    'data_brw' => $data_brw,
                    'data_brw_pasangan' =>  !$data_brw_pasangan->isEmpty() ? $data_brw_pasangan[0] : '',
                    'brw_pendapatan' => !$brw_pendapatan->isEmpty() ? $brw_pendapatan[0] : '',
                    'data_lembar_analisa' => !$data_dtl_lembar_analisa->isEmpty() ? $data_dtl_lembar_analisa[0] : '',
                    'data_objek_pendanaan' => !$data_objek_pendanaan->isEmpty() ? $data_objek_pendanaan[0] : '',
                    'data_resume_akad' => $data_resume_akad,
                    'data_deviasi' => $data_deviasi,

                ]);
                break;
        }       
    }

    public function detailPekerjaanUpdate(
        Request $request, 
        BorrowerAnalisaPendanaan $brw_analisa_pendanaan, 
        BorrowerDetailCeklistAnalisa $brw_dtl_ceklist_analisa,
        BorrowerHdAnalisa $brw_hd_analis,
        BorrowerDetailLembarFasilitasExternal $brw_dtl_lembar_fasilitas_external,
        BorrowerDetailLembarPendapatan $brw_dtl_lembar_pendapatan,
        BorrowerAgunan $brw_agunan,
        BorrowerDetailLembarAnalisa $brw_dtl_lembar_analisa,
        BorrowerDetailDeviasi $brw_dtl_deviasi,
        BorrowerDetailResumeAkad $brw_dtl_resume_akad
    ){

        $user_login     = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
        $pengajuan_id   = $request->pengajuan_id;
        $response       = '';

        // 1 = RAC, 2 = Lembar Perhitungan, 3 = Resume Akad
        $keterangan     = $request->keterangan;

        switch ($keterangan) {
            case '1':

                // START: Detail ceklist analisa
                $update_brw_dtl_ceklist_analisa = $brw_dtl_ceklist_analisa::updateOrCreate([
                    'id_pengajuan' => $pengajuan_id
                ], [
                    // Doc Legalitas
                    'dok_legal_1_dsi'  => $request->dok_legal_1_dsi ? $request->dok_legal_1_dsi : 0,
                    'dok_legal_2_dsi'  => $request->dok_legal_2_dsi ? $request->dok_legal_2_dsi : 0,
                    'dok_legal_3_dsi'  => $request->dok_legal_3_dsi ? $request->dok_legal_3_dsi : 0,
                    'dok_legal_4_dsi'  => $request->dok_legal_4_dsi ? $request->dok_legal_4_dsi : 0,
                    'dok_legal_5_dsi'  => $request->dok_legal_5_dsi ? $request->dok_legal_5_dsi : 0,
                    'dok_legal_6_dsi'  => $request->dok_legal_6_dsi ? $request->dok_legal_6_dsi : 0,
                    'dok_legal_7_dsi'  => $request->dok_legal_7_dsi ? $request->dok_legal_7_dsi : 0,
                    'dok_legal_8_dsi'  => $request->dok_legal_8_dsi ? $request->dok_legal_8_dsi : 0,
                    'dok_legal_9_dsi'  => $request->dok_legal_9_dsi ? $request->dok_legal_9_dsi : 0,
                    'dok_legal_10_dsi' => $request->dok_legal_10_dsi ? $request->dok_legal_10_dsi : 0,
                    'dok_legal_11_dsi' => $request->dok_legal_11_dsi ? $request->dok_legal_11_dsi : 0,
                    
                    'dok_legal_1_dsi_hasil'  => $request->dok_legal_1_dsi ? $request->dok_legal_1_dsi_hasil : '',
                    'dok_legal_2_dsi_hasil'  => $request->dok_legal_2_dsi ? $request->dok_legal_2_dsi_hasil : '',
                    'dok_legal_3_dsi_hasil'  => $request->dok_legal_3_dsi ? $request->dok_legal_3_dsi_hasil : '',
                    'dok_legal_4_dsi_hasil'  => $request->dok_legal_4_dsi ? $request->dok_legal_4_dsi_hasil : '',
                    'dok_legal_5_dsi_hasil'  => $request->dok_legal_5_dsi ? $request->dok_legal_5_dsi_hasil : '',
                    'dok_legal_6_dsi_hasil'  => $request->dok_legal_6_dsi ? $request->dok_legal_6_dsi_hasil : '',
                    'dok_legal_7_dsi_hasil'  => $request->dok_legal_7_dsi ? $request->dok_legal_7_dsi_hasil : '',
                    'dok_legal_8_dsi_hasil'  => $request->dok_legal_8_dsi ? $request->dok_legal_8_dsi_hasil : '',
                    'dok_legal_9_dsi_hasil'  => $request->dok_legal_9_dsi ? $request->dok_legal_9_dsi_hasil : '',
                    'dok_legal_10_dsi_hasil' => $request->dok_legal_10_dsi ? $request->dok_legal_10_dsi_hasil : '',
                    'dok_legal_11_dsi_hasil' => $request->dok_legal_11_dsi ? $request->dok_legal_11_dsi_hasil : '',

                    // Usia
                    'usia_1_dsi' => $request->usia_1_dsi ? $request->usia_1_dsi : 0,
                    'usia_1_dsi_hasil' => $request->usia_1_dsi ? $request->usia_1_dsi_hasil : '',

                    // Pekerjaan
                    'pekerjaan_1_dsi' => $request->pekerjaan_1_dsi ? $request->pekerjaan_1_dsi : 0,
                    'pekerjaan_2_dsi' => $request->pekerjaan_2_dsi ? $request->pekerjaan_2_dsi : 0,
                    'pekerjaan_3_dsi' => $request->pekerjaan_3_dsi ? $request->pekerjaan_3_dsi : 0,
                    'pekerjaan_4_dsi' => $request->pekerjaan_4_dsi ? $request->pekerjaan_4_dsi : 0,
                    'pekerjaan_5_dsi' => $request->pekerjaan_5_dsi ? $request->pekerjaan_5_dsi : 0,
                    'pekerjaan_6_dsi' => $request->pekerjaan_6_dsi ? $request->pekerjaan_6_dsi : 0,

                    'pekerjaan_1_dsi_hasil' => $request->pekerjaan_1_dsi ? $request->pekerjaan_1_dsi_hasil : '',
                    'pekerjaan_2_dsi_hasil' => $request->pekerjaan_2_dsi ? $request->pekerjaan_2_dsi_hasil : '',
                    'pekerjaan_3_dsi_hasil' => $request->pekerjaan_3_dsi ? $request->pekerjaan_3_dsi_hasil : '',
                    'pekerjaan_4_dsi_hasil' => $request->pekerjaan_4_dsi ? $request->pekerjaan_4_dsi_hasil : '',
                    'pekerjaan_5_dsi_hasil' => $request->pekerjaan_5_dsi ? $request->pekerjaan_5_dsi_hasil : '',
                    'pekerjaan_6_dsi_hasil' => $request->pekerjaan_6_dsi ? $request->pekerjaan_6_dsi_hasil : '',

                    // Verifikasi
                    'verifikasi_1_dsi' => $request->verifikasi_1_dsi ? $request->verifikasi_1_dsi : 0,
                    'verifikasi_2_dsi' => $request->verifikasi_2_dsi ? $request->verifikasi_2_dsi : 0,
                    'verifikasi_3_dsi' => $request->verifikasi_3_dsi ? $request->verifikasi_3_dsi : 0,
                    'verifikasi_4_dsi' => $request->verifikasi_4_dsi ? $request->verifikasi_4_dsi : 0,
                    'verifikasi_5_dsi' => $request->verifikasi_5_dsi ? $request->verifikasi_5_dsi : 0,
                    'verifikasi_6_dsi' => $request->verifikasi_6_dsi ? $request->verifikasi_6_dsi : 0,

                    'verifikasi_1_dsi_hasil' => $request->verifikasi_1_dsi ? $request->verifikasi_1_dsi_hasil : '',
                    'verifikasi_2_dsi_hasil' => $request->verifikasi_2_dsi ? $request->verifikasi_2_dsi_hasil : '',
                    'verifikasi_3_dsi_hasil' => $request->verifikasi_3_dsi ? $request->verifikasi_3_dsi_hasil : '',
                    'verifikasi_4_dsi_hasil' => $request->verifikasi_4_dsi ? $request->verifikasi_4_dsi_hasil : '',
                    'verifikasi_5_dsi_hasil' => $request->verifikasi_5_dsi ? $request->verifikasi_5_dsi_hasil : '',
                    'verifikasi_6_dsi_hasil' => $request->verifikasi_6_dsi ? $request->verifikasi_6_dsi_hasil : '',

                    // Pendapatan
                    'pendapatan_1_dsi' => $request->pendapatan_1_dsi ? $request->pendapatan_1_dsi : 0,
                    'pendapatan_2_dsi' => $request->pendapatan_2_dsi ? $request->pendapatan_2_dsi : 0,
                    'pendapatan_3_dsi' => $request->pendapatan_3_dsi ? $request->pendapatan_3_dsi : 0,
                    'pendapatan_4_dsi' => $request->pendapatan_4_dsi ? $request->pendapatan_4_dsi : 0,
                    'pendapatan_5_dsi' => $request->pendapatan_5_dsi ? $request->pendapatan_5_dsi : 0,
                    'pendapatan_6_dsi' => $request->pendapatan_6_dsi ? $request->pendapatan_6_dsi : 0,
                    'pendapatan_7_dsi' => $request->pendapatan_7_dsi ? $request->pendapatan_7_dsi : 0,

                    'pendapatan_1_dsi_hasil' => $request->pendapatan_1_dsi ? $request->pendapatan_1_dsi_hasil : '',
                    'pendapatan_2_dsi_hasil' => $request->pendapatan_2_dsi ? $request->pendapatan_2_dsi_hasil : '',
                    'pendapatan_3_dsi_hasil' => $request->pendapatan_3_dsi ? $request->pendapatan_3_dsi_hasil : '',
                    'pendapatan_4_dsi_hasil' => $request->pendapatan_4_dsi ? $request->pendapatan_4_dsi_hasil : '',
                    'pendapatan_5_dsi_hasil' => $request->pendapatan_5_dsi ? $request->pendapatan_5_dsi_hasil : '',
                    'pendapatan_6_dsi_hasil' => $request->pendapatan_6_dsi ? $request->pendapatan_6_dsi_hasil : '',
                    'pendapatan_7_dsi_hasil' => $request->pendapatan_7_dsi ? $request->pendapatan_7_dsi_hasil : '',

                    // Jaminan
                    'jaminan_1_dsi' => $request->jaminan_1_dsi ? $request->jaminan_1_dsi : 0,
                    'jaminan_2_dsi' => $request->jaminan_2_dsi ? $request->jaminan_2_dsi : 0,
                    'jaminan_3_dsi' => $request->jaminan_3_dsi ? $request->jaminan_3_dsi : 0,
                    'jaminan_4_dsi' => $request->jaminan_4_dsi ? $request->jaminan_4_dsi : 0,
                    'jaminan_5_dsi' => $request->jaminan_5_dsi ? $request->jaminan_5_dsi : 0,
                    'jaminan_6_dsi' => $request->jaminan_6_dsi ? $request->jaminan_6_dsi : 0,
                    'jaminan_7_dsi' => $request->jaminan_7_dsi ? $request->jaminan_7_dsi : 0,
                    'jaminan_8_dsi' => $request->jaminan_8_dsi ? $request->jaminan_8_dsi : 0,
                    'jaminan_9_dsi' => $request->jaminan_9_dsi ? $request->jaminan_9_dsi : 0,

                    'jaminan_1_dsi_hasil' => $request->jaminan_1_dsi ? $request->jaminan_1_dsi_hasil : '',
                    'jaminan_2_dsi_hasil' => $request->jaminan_2_dsi ? $request->jaminan_2_dsi_hasil : '',
                    'jaminan_3_dsi_hasil' => $request->jaminan_3_dsi ? $request->jaminan_3_dsi_hasil : '',
                    'jaminan_4_dsi_hasil' => $request->jaminan_4_dsi ? $request->jaminan_4_dsi_hasil : '',
                    'jaminan_5_dsi_hasil' => $request->jaminan_5_dsi ? $request->jaminan_5_dsi_hasil : '',
                    'jaminan_6_dsi_hasil' => $request->jaminan_6_dsi ? $request->jaminan_6_dsi_hasil : '',
                    'jaminan_7_dsi_hasil' => $request->jaminan_7_dsi ? $request->jaminan_7_dsi_hasil : '',
                    'jaminan_8_dsi_hasil' => $request->jaminan_8_dsi ? $request->jaminan_8_dsi_hasil : '',
                    'jaminan_9_dsi_hasil' => $request->jaminan_9_dsi ? $request->jaminan_9_dsi_hasil : '',

                    // Cash Rasio
                    'cash_ratio_1_dsi' => $request->cash_ratio_1_dsi ? $request->cash_ratio_1_dsi : 0,
                    'cash_ratio_2_dsi' => $request->cash_ratio_2_dsi ? $request->cash_ratio_2_dsi : 0,
                    'cash_ratio_3_dsi' => $request->cash_ratio_3_dsi ? $request->cash_ratio_3_dsi : 0,
                    'cash_ratio_4_dsi' => $request->cash_ratio_4_dsi ? $request->cash_ratio_4_dsi : 0,

                    'cash_ratio_1_dsi_hasil' => $request->cash_ratio_1_dsi ? $request->cash_ratio_1_dsi_hasil : '',
                    'cash_ratio_2_dsi_hasil' => $request->cash_ratio_2_dsi ? $request->cash_ratio_2_dsi_hasil : '',
                    'cash_ratio_3_dsi_hasil' => $request->cash_ratio_3_dsi ? $request->cash_ratio_3_dsi_hasil : '',
                    'cash_ratio_4_dsi_hasil' => $request->cash_ratio_4_dsi ? $request->cash_ratio_4_dsi_hasil : '',

                    // FTV
                    'ftv_1_dsi'     => $request->ftv_1_dsi ? $request->ftv_1_dsi : 0,
                    'ftv_2_dsi'     => $request->ftv_2_dsi ? $request->ftv_2_dsi : 0,
                    'ftv_3_dsi'     => $request->ftv_3_dsi ? $request->ftv_3_dsi : 0,
                    'ftv_4_dsi'     => $request->ftv_4_dsi ? $request->ftv_4_dsi : 0,
                    'ftv_5_dsi'     => $request->ftv_5_dsi ? $request->ftv_5_dsi : 0,
                    'ftv_6_dsi'     => $request->ftv_6_dsi ? $request->ftv_6_dsi : 0,
                    'ftv_7_dsi'     => $request->ftv_7_dsi ? $request->ftv_7_dsi : 0,
                    'ftv_8_dsi'     => $request->ftv_8_dsi ? $request->ftv_8_dsi : 0,
                    'ftv_9_dsi'     => $request->ftv_9_dsi ? $request->ftv_9_dsi : 0,
                    'ftv_10_dsi'    => $request->ftv_10_dsi ? $request->ftv_10_dsi : 0,
                    'ftv_11_dsi'    => $request->ftv_11_dsi ? $request->ftv_11_dsi : 0,
                    'ftv_12_dsi'    => $request->ftv_12_dsi ? $request->ftv_12_dsi : 0,
                    'ftv_13_dsi'    => $request->ftv_13_dsi ? $request->ftv_13_dsi : 0,
                    'ftv_14_dsi'    => $request->ftv_14_dsi ? $request->ftv_14_dsi : 0,

                    'ftv_1_dsi_hasil'   => $request->ftv_1_dsi ? $request->ftv_1_dsi_hasil : '',
                    'ftv_2_dsi_hasil'   => $request->ftv_2_dsi ? $request->ftv_2_dsi_hasil : '',
                    'ftv_3_dsi_hasil'   => $request->ftv_3_dsi ? $request->ftv_3_dsi_hasil : '',
                    'ftv_4_dsi_hasil'   => $request->ftv_4_dsi ? $request->ftv_4_dsi_hasil : '',
                    'ftv_5_dsi_hasil'   => $request->ftv_5_dsi ? $request->ftv_5_dsi_hasil : '',
                    'ftv_6_dsi_hasil'   => $request->ftv_6_dsi ? $request->ftv_6_dsi_hasil : '',
                    'ftv_7_dsi_hasil'   => $request->ftv_7_dsi ? $request->ftv_7_dsi_hasil : '',
                    'ftv_8_dsi_hasil'   => $request->ftv_8_dsi ? $request->ftv_8_dsi_hasil : '',
                    'ftv_9_dsi_hasil'   => $request->ftv_9_dsi ? $request->ftv_9_dsi_hasil : '',
                    'ftv_10_dsi_hasil'  => $request->ftv_10_dsi ? $request->ftv_10_dsi_hasil : '',
                    'ftv_11_dsi_hasil'  => $request->ftv_11_dsi ? $request->ftv_11_dsi_hasil : '',
                    'ftv_12_dsi_hasil'  => $request->ftv_12_dsi ? $request->ftv_12_dsi_hasil : '',
                    'ftv_13_dsi_hasil'  => $request->ftv_13_dsi ? $request->ftv_13_dsi_hasil : '',
                    'ftv_14_dsi_hasil'  => $request->ftv_14_dsi ? $request->ftv_14_dsi_hasil : '',
                ]);

                $rac_dsi_dsi = $brw_dtl_ceklist_analisa::where('id_pengajuan', $request->pengajuan_id)->first();
                if (empty($rac_dsi_dsi->created_at_dsi) || $rac_dsi_dsi->created_at_dsi == 0) {
                    $brw_dtl_ceklist_analisa::where('id_pengajuan', $pengajuan_id)->update([
                        'created_at_dsi' => Carbon::now()
                    ]);
                } else {
                    $brw_dtl_ceklist_analisa::where('id_pengajuan', $pengajuan_id)->update([
                        'updated_at_dsi' => Carbon::now()
                    ]);
                }
                // END: START: Detail ceklist analisa

                // START: Update flag RAC
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        11, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AnalisController.php', /* file varchar(100) */
                        433 /* line int */
                    )"
                );
                // END: Update flag RAC

                $result_update_status = $update_status[0]->sout;
                if ($result_update_status == 1) {
                    $response = [
                        'status'    => 'success',
                        'msg'       => 'Data berhasil disimpan',
                    ];
                } else {
                    $response = [
                        'status'    => 'failed',
                        'msg'       => 'Data gagal disimpan',
                    ];
                }


                return $response;

                break;

            case '2':
                // Update tanggal lahir brw
                // $brw_tgl_lahir = $request->text_tgl_lahir;
                // $brw_usia = $request->text_usia_saat_pengajuan;
                // $current_year = Carbon::now()->year;
                // $brw_thn_lahir = $current_year - $brw_usia;
                // $brw_tgl_lahir_new = substr_replace($brw_tgl_lahir, $brw_thn_lahir, 0, 4);

                // Update tanggal lahir brw pasangan
                // $brw_tgl_lahir_pasangan = $request->text_tgl_lahir_pasangan;
                // $brw_usia_pasangan = $request->text_usia_pasangan;
                // $brw_thn_lahir_pasangan = $current_year - $brw_usia_pasangan;
                // $brw_tgl_lahir_new_pasangan = substr_replace($brw_tgl_lahir_pasangan, $brw_thn_lahir_pasangan, 0, 4);

                $update_brw_dtl_naup = DB::table('brw_dtl_naup')->where('pengajuan_id', $request->pengajuan_id)
                ->limit(1)->update( [ 
                    'no_naup' => $request->text_nomor_naup
                 ]);

                // $update_brw_user_detail = DB::table('brw_user_detail')->where('brw_id', $request->brw_id)
                // ->limit(1)->update( [ 
                //     'nama' => $request->text_nama_penerima_pendanaan,
                //     'tgl_lahir' => $brw_tgl_lahir_new
                //  ]);

                $update_brw_pengajuan = DB::table('brw_pengajuan')->where('pengajuan_id', $request->pengajuan_id)
                ->limit(1)->update( [ 
                    'pendanaan_dana_dibutuhkan' => preg_replace('/[^0-9]/', '', $request->text_plafon_pengajuan),
                    'persentase_margin'      => $request->text_margin_pendanaan,
                    'durasi_proyek'             => $request->text_jangka_waktu,
                    'pendanaan_akad'            => $request->text_akad_pendanaan,
                 ]);

                // $brw_user_detail_penghasilan = DB::table('brw_user_detail_penghasilan')->where('brw_id2', $request->brw_id)
                // ->limit(1)->update( [ 
                //     'skema_pembiayaan' => $request->text_skema_pengajuan,
                //  ]);

                // $brw_pasangan = DB::table('brw_pasangan')->where('pasangan_id', $request->brw_id)
                // ->limit(1)->update( [ 
                //     'nama' => $request->text_nama_pasangan,
                //     'tgl_lahir' => $brw_tgl_lahir_new_pasangan,
                // ]);

                // Hubungan Dengan Lembaga Keuangan
                $total_fasilitas_external = count($request->in_num ? $request->in_num : []);
                for ($i = 0; $i < $total_fasilitas_external; $i++) {

                     $update_brw_fasilitas_external = $brw_dtl_lembar_fasilitas_external::updateOrCreate([
                        'id' =>  $request->in_num[$i]
                    ], [
                         'id_pengajuan'             => $request->pengajuan_id,
                         'jenis_fasilitas'          => $request->in_jenis_fasilitas[$i],
                         'fasilitas_milik'          => $request->in_fasilitas_milik[$i],
                         'tenor'                    => $request->in_tenor[$i],
                         'margin'                   => $request->in_margin[$i],
                         'bank_lembaga_keuangan'    => $request->in_bank[$i],
                         'plafond_awal'             => $request->in_plafond_awal[$i],
                         'baki_debet'               => $request->in_baki_debet[$i],
                         'angsuran'                 => $request->in_angsuran[$i],
                         'history_payment'          => $request->in_history_payment[$i],
                    ]);
                }

                $total_fasilitas_external_deleted = count($request->deleted_data_hub ? $request->deleted_data_hub : []);
                for ($i=0; $i < $total_fasilitas_external_deleted; $i++) { 
                    $brw_dtl_lembar_fasilitas_external::where('id', $request->deleted_data_hub[$i])->delete();
                }

                // Data Pendapatan
                $total_pendapatan = preg_replace('/[^0-9]/', '', $request->text_total_pendapatan);
                $maks_angsuran_sebelum = preg_replace('/[^0-9]/','', $request->text_angsuran_sebelum);
                $maks_angsuran_setelah = preg_replace('/[^0-9]/','', $request->text_angsuran_setelah);

                $update_brw_dtl_lembar_pendapatan = $brw_dtl_lembar_pendapatan::updateOrCreate([
                    'id_pengajuan'                  =>  $request->pengajuan_id
                ], [
                    'gaji_pokok_1'                  => preg_replace('/[^0-9]/', '', $request->gaji_pokok_1),
                    'gaji_pokok_2'                  => preg_replace('/[^0-9]/', '', $request->gaji_pokok_2),
                    'gaji_pokok_3'                  => preg_replace('/[^0-9]/', '', $request->gaji_pokok_3),
                    'gaji_pokok_rata2'              => preg_replace('/[^0-9]/', '', $request->gaji_pokok_rata2),
                    'tunjangan_1'                   => preg_replace('/[^0-9]/', '', $request->tunjangan_1),
                    'tunjangan_2'                   => preg_replace('/[^0-9]/', '', $request->tunjangan_2),
                    'tunjangan_3'                   => preg_replace('/[^0-9]/', '', $request->tunjangan_3),
                    'tunjangan_rata2'               => preg_replace('/[^0-9]/', '', $request->tunjangan_rata2),
                    'thp_1'                         => preg_replace('/[^0-9]/', '', $request->thp_1),
                    'thp_2'                         => preg_replace('/[^0-9]/', '', $request->thp_2),
                    'thp_3'                         => preg_replace('/[^0-9]/', '', $request->thp_3),
                    'thp_rata2'                     => preg_replace('/[^0-9]/', '', $request->thp_rata2),

                    'gaji_pokok_1_pasangan'         => preg_replace('/[^0-9]/', '', $request->gaji_pokok_1_pasangan),
                    'gaji_pokok_2_pasangan'         => preg_replace('/[^0-9]/', '', $request->gaji_pokok_2_pasangan),
                    'gaji_pokok_3_pasangan'         => preg_replace('/[^0-9]/', '', $request->gaji_pokok_3_pasangan),
                    'gaji_pokok_rata2_pasangan'     => preg_replace('/[^0-9]/', '', $request->gaji_pokok_rata2_pasangan),
                    'tunjangan_1_pasangan'          => preg_replace('/[^0-9]/', '', $request->tunjangan_1_pasangan),
                    'tunjangan_2_pasangan'          => preg_replace('/[^0-9]/', '', $request->tunjangan_2_pasangan),
                    'tunjangan_3_pasangan'          => preg_replace('/[^0-9]/', '', $request->tunjangan_3_pasangan),
                    'tunjangan_rata2_pasangan'      => preg_replace('/[^0-9]/', '', $request->tunjangan_rata2_pasangan),
                    'thp_1_pasangan'                => preg_replace('/[^0-9]/', '', $request->thp_1_pasangan),
                    'thp_2_pasangan'                => preg_replace('/[^0-9]/', '', $request->thp_2_pasangan),
                    'thp_3_pasangan'                => preg_replace('/[^0-9]/', '', $request->thp_3_pasangan),
                    'thp_rata2_pasangan'            => preg_replace('/[^0-9]/', '', $request->thp_rata2_pasangan),

                    'total_pendapatan'              => $total_pendapatan,
                    'cr_maks'                       => $request->text_cash_ratio,
                    'maks_angsuran_sebelum'         => $maks_angsuran_sebelum,
                    'maks_angsuran_setelah'         => $maks_angsuran_setelah

                ]);
                    
                // Data Objek Pendanaan
                $update_brw_agunan = $brw_agunan::updateOrCreate([
                    'id_pengajuan'                  =>  $request->pengajuan_id
                ], [
                    'tipe_agunan'                   => $request->text_tipe_agunan,
                    'jenis_objek_pendanaan'         => $request->text_jenis_objek_pendanaan,
                    'nilai_pasar_wajar'             => preg_replace('/[^0-9]/', '', $request->text_nilai_pasar_wajar),
                    'nilai_likuidasi'               => preg_replace('/[^0-9]/', '', $request->text_nilai_likuidasi),
                    'maks_ftv_persentase'           => $request->text_maks_ftv_percent,
                    'maks_ftv_nominal'              => preg_replace('/[^0-9]/', '', $request->text_maks_ftv_nominal),
                    'fasilitas_ke'                  => $request->text_untuk_fasilitas,
                    'luas_tanah'                    => $request->text_luas_tanah,
                    'luas_bangunan'                 => $request->text_luas_bangunan,
                    'jenis_agunan'                  => $request->text_bukti_kepemilikan_angunan,
                    'nomor_agunan'                  => $request->text_nomor_sertifikat,
                    'tanggal_jatuh_tempo'           => $request->text_tanggal_jatuh_tempo,
                    'tanggal_terbit'                => $request->text_tanggal_terbit_sertifikat,
                ]);
                
                // Data Perhitungan Analis
                $update_brw_dtl_lembar_analisa = $brw_dtl_lembar_analisa::updateOrCreate([
                    'id_pengajuan'                  =>  $request->pengajuan_id
                ], [
                    'maks_plafond_by_ftv'           => preg_replace('/[^0-9]/', '', $request->text_maks_ftv_nominal),
                    'maks_plafond_by_cr'            => $request->text_maks_plafon_cr,
                    'plafond_rekomendasi'           => preg_replace('/[^0-9]/','', $request->input_plafon_rekomendasi),
                    'angsuran'                      => $request->text_angsuran_2,
                ]);

                // START: Update flag lembar
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        12, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AnalisController.php', /* file varchar(100) */
                        604 /* line int */
                    )"
                );
                // END: Update flag lembar

                $result_update_status = $update_status[0]->sout;
                if ($result_update_status == 1) {
                    $response = [
                        'status' => 'success',
                        'msg' => 'Data berhasil disimpan',
                    ];
                } else {
                    $response = [
                        'status' => 'failed',
                        'msg' => 'Data gagal disimpan',
                    ];
                }

                return $response;
                break;
            
            default:
                // return $request->all();
                
                $update_brw_dtl_resume_akad = $brw_dtl_resume_akad::updateOrCreate([
                    'pengajuan_id' => $request->pengajuan_id
                ],[
                    'porsi_syirkah_dsi_persentase'              => preg_replace('/[^0-9]/','', $request->mmq_parsi_syirkah_dsi_percent),

                    'porsi_syirkah_penerima_dana'               => preg_replace('/[^0-9]/','', $request->mmq_parsi_syirkah_penerima_dana),
                    'porsi_syirkah_penerima_dana_persentase'    => preg_replace('/[^0-9]/','', $request->mmq_parsi_syirkah_penerima_dana_percent),

                    'porsi_nisbah_dsi'                          => preg_replace('/[^0-9]/','', $request->mmq_parsi_nisbah_dsi),
                    'porsi_nisbah_dsi_persentase'               => preg_replace('/[^0-9]/','', $request->mmq_parsi_nisbah_dsi_percent),

                    'porsi_nisbah_penerima_dana'                => preg_replace('/[^0-9]/','', $request->mmq_parsi_nisbah_penerima_dana),
                    'porsi_nisbah_penerima_dana_persentase'     => preg_replace('/[^0-9]/','', $request->mmq_parsi_nisbah_penerima_dana_percent),
                    'kesimpulan'                                => $request->text_kesimpulan,
                ]);

                $deviasi = $request->deviasi;

                // 1 = Ada deviasi
                if ($deviasi == 1) {
                    $total_deviasi = $request->in_jenis_deviasi ? count($request->in_jenis_deviasi) : 0;
                    for ($i = 0; $i < $total_deviasi; $i++) {
                        if ($request->in_jenis_deviasi[$i]) {
                            $data_deviasi = $brw_dtl_deviasi::updateOrCreate([
                                'id' =>  $request->in_id[$i]
                            ], [
                                'pengajuan_id'              => $request->pengajuan_id,
                                'jenis_deviasi'             => $request->in_jenis_deviasi[$i],
                            ]);
                        } else {
                            $brw_dtl_deviasi::where('id', $request->in_id[$i])->delete();
                        }
                    }
                } else {
                    $brw_dtl_deviasi::where('pengajuan_id', $request->pengajuan_id)->delete();
                }

                $total_deleted_deviasi = count($request->in_deleted_deviasi ? $request->in_deleted_deviasi : []);
                for ($i=0; $i < $total_deleted_deviasi; $i++) { 
                    $brw_dtl_deviasi::where('id', $request->in_deleted_deviasi[$i])->delete();
                }

                // START: Update flag resume akad
                $update_status = DB::select(
                    "CALL proc_click_button_borrower(
                        13, /* form_id int */
                        '$user_login', /* user_login varchar(35) */
                        '$pengajuan_id', /* pengajuan_id int */
                        'simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                        'AnalisController.php', /* file varchar(100) */
                        670 /* line int */
                    )"
                );
                // END: Update flag resume akad

                $result_update_status = $update_status[0]->sout;
                if ($result_update_status == 1) {
                    $response = [
                        'status' => 'success',
                        'msg' => 'Data berhasil disimpan',
                    ];
                } else {
                    $response = [
                        'status' => 'failed',
                        'msg' => 'Data gagal disimpan',
                    ];
                }

                return $response;
                break;
        }

    }

    public function pekerjaanAnalisUpdate(
        Request $request
    ) {

        $user_login     = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
        $pengajuan_id   = $request->pengajuan_id;
        $response       = '';

        // START: Update flag resume akad
        $update_status = DB::select(
            "CALL proc_click_button_borrower(
                10, /* form_id int */
                '$user_login', /* user_login varchar(35) */
                '$pengajuan_id', /* pengajuan_id int */
                'kirim', /* method varchar(10) ['simpan' , 'kirim'] */
                'AnalisController.php', /* file varchar(100) */
                710 /* line int */
            )"
        );
        // END: Update flag resume akad

        $result_update_status = $update_status[0]->sout;
        if ($result_update_status == 1) {
            $response = [
                'status' => 'success',
                'msg' => 'Data berhasil disimpan',
            ];
        } else {
            $response = [
                'status' => 'failed',
                'msg' => 'Data gagal disimpan',
            ];
        }

        return $response;
    }
}
