<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\BorrowerAnalisaPendanaan;
use App\BorrowerPengajuan;
use App\BorrowerTipePendanaan;
use App\Admins;
use App\ManageRole;
use App\AuditTrail;
use App\BorrowerPengurus;
use App\Deskripsi_Proyek;
use App\Pemilik_Proyek;
use App\Legalitas_Proyek;
use App\Simulasi_Proyek;
class KomitePendanaan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin')->only(['listproyek', 'komite_detilpendanaan']);
    }    
    
    public function ListPengajuanCommittee()
    {
        $query = \DB::table('brw_analisa_pendanaan');
        $query->leftJoin('m_tujuan_pembiayaan','m_tujuan_pembiayaan.id','brw_analisa_pendanaan.tujuan_pendanaan');
        $query->leftJoin('brw_tipe_pendanaan','brw_tipe_pendanaan.tipe_id','brw_analisa_pendanaan.jenis_pendanaan');
        $query->leftJoin('m_status_analis','m_status_analis.id_status_analis','brw_analisa_pendanaan.status_komite');
		$query->where('m_status_analis.id_vendor', 14);
        $query->OrderBy('brw_analisa_pendanaan.status_komite','asc');
        $query->OrderBy('brw_analisa_pendanaan.tanggal_verifikasi', 'asc');
        $query->Select([
            'pengajuan_id',
            DB::raw('DATE_FORMAT(tanggal_pengajuan,"%d-%m-%Y") as tanggal_pengajuan'),
            DB::raw('DATE_FORMAT(tanggal_verifikasi,"%d-%m-%Y") as tanggal_verifikasi'),
            'penerima_pendanaan',
            'jenis_pendanaan',
            'tujuan_pembiayaan',
            'nilai_pengajuan_pendanaan',
            'status_compliance',
            'keterangan_status'
        ]);
        $pengajuan = $query->get();

        $response = ['data' => $pengajuan];
        return response()->json($response);
    }

    public function listproyek()
    {
        $admins = Admins::all();
        $role = ManageRole::all();
        $cekUser = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')->where('admins.id', \Auth::id())->first(['roles.name', 'admins.id']);

        $qstatus = \DB::table('m_status_analis');
		$qstatus->where('m_status_analis.id_vendor', 11);
        $status_analis = $qstatus->get();
        return view('pages.admin.adrKomite_listproyek')->with(['cekUser' => $cekUser,'status_analis' => $status_analis]);
    }

    public function komite_detilpendanaan($pengajuanId)
    {   
        $admins = Admins::all();
        $role = ManageRole::all();
        $cekUser = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')->where('admins.id', \Auth::id())->first(['roles.name', 'admins.id']);

        $data['namakomite'] = Auth::user()->firstname . ' ' . Auth::user()->lastname;
        $data['tanggalkeputusan'] = date('d F Y');
        $pengajuan = BorrowerAnalisaPendanaan::join('brw_pengajuan', 'brw_pengajuan.pengajuan_id', '=', 'brw_analisa_pendanaan.pengajuan_id')
            ->leftjoin('brw_tipe_pendanaan', 'brw_tipe_pendanaan.tipe_id', '=', 'brw_analisa_pendanaan.jenis_pendanaan')
            ->leftjoin('brw_user_detail_penghasilan_pengajuan', 'brw_user_detail_penghasilan_pengajuan.brw_id2', '=', 'brw_pengajuan.brw_id')
            ->leftjoin('brw_dtl_naup', 'brw_dtl_naup.pengajuan_id', '=', 'brw_analisa_pendanaan.pengajuan_id')
            ->leftjoin('brw_agunan', 'brw_agunan.id_pengajuan', '=', 'brw_analisa_pendanaan.pengajuan_id')
            ->leftjoin('m_tujuan_pembiayaan', 'm_tujuan_pembiayaan.id', '=', 'brw_analisa_pendanaan.tujuan_pendanaan')
            ->leftjoin('brw_dtl_lembar_hasil_analisa', 'brw_dtl_lembar_hasil_analisa.id_pengajuan', '=', 'brw_analisa_pendanaan.pengajuan_id')
            ->leftjoin('m_pengikatan_jaminan', 'm_pengikatan_jaminan.id', '=', 'brw_dtl_naup.jenis_pengikatan_agunan')
            ->where('brw_analisa_pendanaan.pengajuan_id', $pengajuanId)
            ->select([
                'brw_analisa_pendanaan.pengajuan_id as pengajuan_id',
                'brw_id', 'penerima_pendanaan',
                'brw_pengajuan.pendanaan_dana_dibutuhkan as plafon',
                'brw_tipe_pendanaan.pendanaan_nama as jenisPendanaan',
                'brw_pengajuan.durasi_proyek',
                'brw_user_detail_penghasilan_pengajuan.sumber_pengembalian_dana',
                'brw_dtl_naup.no_naup',
                'brw_user_detail_penghasilan_pengajuan.skema_pembiayaan',
                'brw_agunan.maks_ftv_persentase',
                'brw_agunan.maks_ftv_nominal',
                'm_tujuan_pembiayaan.tujuan_pembiayaan as tujuanPendanaan',
                'brw_pengajuan.nm_pemilik',
                // fasilitas pendanaan 
                'brw_dtl_naup.wakalah',
                'brw_dtl_naup.qardh',
                'brw_dtl_naup.murabahah',
                'brw_dtl_naup.imbt',
                'brw_dtl_naup.mmq',
                'brw_dtl_naup.ijarah',
                // wakalaj
                'brw_dtl_naup.tujuan_penggunaan_wakalah',
                'brw_dtl_naup.jangka_waktu_wakalah',
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_pendanaan WHERE id = brw_dtl_naup.pengikatan_pendanaan_wakalah) AS pengikatanPendanaanWakalah"),
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_jaminan WHERE id = brw_dtl_naup.pengikatan_jaminan_wakalah) AS pengikatanJaminanWakalah"),
                'brw_dtl_naup.jumlah_wakalah',
                // qardh
                'brw_dtl_naup.tujuan_penggunaan_qardh',
                'brw_dtl_naup.take_over_dari_qardh',
                'brw_dtl_naup.jumlah_qardh',
                'brw_dtl_naup.jangka_waktu_qardh',
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_pendanaan WHERE id = brw_dtl_naup.pengikatan_pendanaan_qardh) AS pengikatanPendanaanQardh"),
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_jaminan WHERE id = brw_dtl_naup.pengikatan_jaminan_qardh) AS pengikatanJaminanQardh"),
                // murabahah
                'brw_dtl_naup.tujuan_penggunaan_murabahah',
                'brw_pengajuan.harga_objek_pendanaan',
                'brw_dtl_naup.uang_muka_murabahah',
                'brw_dtl_lembar_hasil_analisa.plafond_rekomendasi',
                'brw_pengajuan.persentase_margin as estimasi_imbal_hasil',
                'brw_dtl_naup.denda_murabahah',
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_pendanaan WHERE id = brw_dtl_naup.pengikatan_pendanaan_murabahah) AS pengikatanPendanaanMurabahah"),
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_jaminan WHERE id = brw_dtl_naup.pengikatan_jaminan_murabahah) AS pengikatanJaminanMurabahah"),
                // imbt
                'brw_dtl_naup.tujuan_penggunaan_imbt',
                'brw_dtl_naup.penetapan_nilai_ujroh_selanjutnya',
                'brw_dtl_naup.uang_muka_imbt',
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_pendanaan WHERE id = brw_dtl_naup.pengikatan_pendanaan_imbt) AS pengikatanPendanaanimbt"),
                DB::raw("(SELECT jenis_pengikatan FROM m_pengikatan_jaminan WHERE id = brw_dtl_naup.pengikatan_jaminan_imbt) AS pengikatanJaminanimbt"),
                'brw_dtl_naup.denda_imbt',
                // mmq
                'brw_dtl_naup.tujuan_penggunaan_mmq',
                'brw_dtl_naup.uang_muka_mmq',
                'brw_dtl_naup.hishshah_per_unit',
                'brw_dtl_naup.jangka_waktu_penyerahan_objek_mmq',
                'brw_dtl_naup.objek_bagi_hasil_mmq',
                'brw_dtl_naup.nisbah_penyelenggara',
                'brw_dtl_naup.nisbah_penerima_dana',
                'brw_dtl_naup.denda_mmq',
                // ijarah
                'brw_dtl_naup.objek_sewa',
                'brw_dtl_naup.jangka_waktu_penyerahan_ijarah',
                'brw_dtl_naup.penyesuaian_nilai_sewa',
                // objek pendanaan
                DB::raw("(SELECT jenis_objek_pendanaan FROM m_jenis_objek_pendanaan WHERE id = brw_agunan.jenis_objek_pendanaan) AS jenis_objek_pendanaan"),
                'brw_agunan.jenis_agunan',
                'brw_agunan.nomor_agunan',
                'brw_agunan.tanggal_jatuh_tempo',
                'brw_agunan.atas_nama_agunan',
                DB::raw("(SELECT CONCAT(brw_pengajuan.lokasi_proyek,' RT.',brw_agunan.rt,' RW.',brw_agunan.rw,' kel.',kelurahan,' kec.',kecamatan,' ',Jenis,Kota,' ',Provinsi,' ',kode_pos) FROM m_kode_pos WHERE id_kode_pos = brw_pengajuan.kode_pos) AS alamat_sertifikat"),
                'brw_agunan.no_imb',
                'brw_dtl_naup.imbt_atas_nama',
                'brw_agunan.luas_tanah',
                'brw_agunan.luas_bangunan',
                'brw_dtl_naup.tanggal_penilaian_agunan',
                'brw_dtl_naup.appraisal',
                'brw_agunan.nilai_pasar_wajar',
                'brw_agunan.nilai_likuidasi',
                'brw_dtl_naup.rekomendasi_appraisal',
                'brw_dtl_naup.jenis_pengikatan_agunan',
                'brw_dtl_naup.nilai_pengikatan_agunan',
                'm_pengikatan_jaminan.jenis_pengikatan',

            ])->first();

        // dd($pengajuan);
        $data['pengajuanId'] = $pengajuan->pengajuan_id;
        $data['status_pendanaan'] =$pengajuan->status_komite?$pengajuan->status_komite: 0;
        $data['brwId'] = $pengajuan->brw_id;
        $data['penerima_pendanaan'] = $pengajuan->penerima_pendanaan;
        $data['plafon'] = "Rp" . number_format($pengajuan->plafon, 2, ",", ".");
        $data['jenisPendanaan'] = $pengajuan->jenisPendanaan;
        $data['durasi_proyek'] = ($pengajuan->durasi_proyek <= 12 ? $pengajuan->durasi_proyek . ' Bulan' : ($pengajuan->durasi_proyek % 12 == 0 ? $pengajuan->durasi_proyek / 12 . ' Bulan' : floor($pengajuan->durasi_proyek / 12 . ' Tahun ' . $pengajuan->durasi_proyek % 12 . ' Bulan')));
        $data['kategoriPenerimaPendanaan'] = ($pengajuan->sumber_pengembalian_dana == 1 ? 'Fixed Income (Penghasilan Tetap)' : ($pengajuan->sumber_pengembalian_dana == 2 ? 'Non Fixed Income (Penghasilan Tidak Tetap)' : '-'));
        $data['no_naup'] = $pengajuan->no_naup;
        $data['skema_pembiayaan'] = ($pengajuan->skema_pembiayaan == 1 ? 'Single Income (Penghasilan Sendiri)' : ($pengajuan->skema_pembiayaan == 2 ? 'Joint Income (Penghasilan Bersama)' : '-'));
        $data['maks_ftv_persentase'] = $pengajuan->maks_ftv_persentase;
        $data['maks_ftv_nominal'] = number_format($pengajuan->maks_ftv_nominal, 2, ",", ".");
        $data['pendanaan_tujuan'] = $pengajuan->tujuanPendanaan;
        $data['pembelian_dari'] = $pengajuan->nm_pemilik;
        // fasilitas pendanaan
        $data['wakalah'] = !empty($pengajuan->wakalah) ? $pengajuan->wakalah : '0';
        $data['qardh'] = !empty($pengajuan->qardh) ? $pengajuan->qardh : '0';
        $data['murabahah'] = !empty($pengajuan->murabahah) ? $pengajuan->murabahah : '0';
        $data['imbt'] = !empty($pengajuan->imbt) ? $pengajuan->imbt : '0';
        $data['mmq'] = !empty($pengajuan->mmq) ? $pengajuan->mmq : '0';
        $data['ijarah'] = !empty($pengajuan->ijarah) ? $pengajuan->ijarah : '0';
        // wakalah
        $data['tujuan_penggunaan_wakalah'] = $pengajuan->tujuan_penggunaan_wakalah;
        $data['jangka_waktu_wakalah'] = !empty($pengajuan->jangka_waktu_wakalah) ? $pengajuan->jangka_waktu_wakalah . ' hari' : '';
        $data['pengikatanPendanaanWakalah'] = $pengajuan->pengikatanPendanaanWakalah;
        $data['pengikatanJaminanWakalah'] = $pengajuan->pengikatanJaminanWakalah;
        $data['jumlah_wakalah'] = !empty($pengajuan->jumlah_wakalah) ? 'Rp.' . number_format($pengajuan->jumlah_wakalah, 2, ",", ".") : '';
        // qard 
        $data['tujuan_penggunaan_qardh'] = $pengajuan->tujuan_penggunaan_qardh;
        $data['take_over_dari_qardh'] = $pengajuan->take_over_dari_qardh;
        $data['jumlah_qardh'] = !empty($pengajuan->jumlah_qardh) ? 'Rp.' . number_format($pengajuan->jumlah_qardh, 2, ",", ".") : '';
        $data['jangka_waktu_qardh'] = !empty($pengajuan->jangka_waktu_qardh) ? $pengajuan->jangka_waktu_qardh . ' hari' : '';
        $data['pengikatanPendanaanQardh'] = $pengajuan->pengikatanPendanaanQardh;
        $data['pengikatanJaminanQardh'] = $pengajuan->pengikatanJaminanQardh;
        // murabahah
        $data['tujuan_penggunaan_murabahah'] = $pengajuan->tujuan_penggunaan_murabahah;
        $data['harga_beli_barang_murabahah'] = !empty($pengajuan->harga_objek_pendanaan) ? 'Rp.' . number_format($pengajuan->harga_objek_pendanaan, 2, ",", ".") : '';
        $data['uang_muka_murabahah'] = !empty($pengajuan->uang_muka_murabahah) ? 'Rp.' . number_format($pengajuan->uang_muka_murabahah, 2, ",", ".") : '';
        $harga = !empty($pengajuan->harga_objek_pendanaan) ? $pengajuan->harga_objek_pendanaan : 0;
        $uangmuka = !empty($pengajuan->uang_muka_murabahah) ? $pengajuan->uang_muka_murabahah : 0;
        $data['harga_jual_barang_murabahah'] = 'Rp.' . number_format(($harga - $uangmuka), 2, ",", ".");
        $data['plafond_rekomendasi'] = !empty($pengajuan->plafond_rekomendasi) ? 'Rp.' . number_format($pengajuan->plafond_rekomendasi, 2, ",", ".") : '';
        $plafond = !empty($pengajuan->plafond_rekomendasi) ? $pengajuan->plafond_rekomendasi : 0;
        $estimasi_imbal_hasil = !empty($pengajuan->estimasi_imbal_hasil) ? $pengajuan->estimasi_imbal_hasil : 0;
        $data['margin'] = 'Rp' . number_format($estimasi_imbal_hasil * $plafond, 2, ",", ".");
        $data['totalKPP_murabahah'] = 'Rp' . number_format($plafond + ($estimasi_imbal_hasil * $plafond), 2, ",", ".");
        $data['pricing'] = $estimasi_imbal_hasil;
        $data['jangka_waktu_murabahah'] = $data['durasi_proyek'];
        $data['denda_murabahah'] = !empty($pengajuan->denda_murabahah) ? 'Rp' . number_format($pengajuan->denda_murabahah, 2, ",", ".") : 0;
        $data['pengikatanPendanaanMurabahah'] = $pengajuan->pengikatanPendanaanMurabahah;
        $data['pengikatanJaminanMurabahah'] = $pengajuan->pengikatanJaminanMurabahah;
        // imtb
        $data['tujuan_penggunaan_imbt'] = $pengajuan->tujuan_penggunaan_imbt;
        $data['nilai_ujroh'] = (($data['pricing'] * $plafond) == 0 ? '0' : 'Rp.' . number_format(($data['pricing'] * $plafond), 2, ",", "."));
        $data['penetapan_nilai_ujroh_selanjutnya'] = !empty($pengajuan->penetapan_nilai_ujroh_selanjutnya) ? $pengajuan->penetapan_nilai_ujroh_selanjutnya . " bulan" : '0';
        $data['security_deposit_imbt'] = !empty($pengajuan->uang_muka_imbt) ? 'Rp.' . number_format($pengajuan->uang_muka_imbt, 2, ",", ".") : '0';
        $data['pengikatanPendanaanimbt'] = $pengajuan->pengikatanPendanaanimbt;
        $data['pengikatanJaminanimbt'] = $pengajuan->pengikatanJaminanimbt;
        $data['denda_imbt'] = !empty($pengajuan->denda_imbt) ? 'Rp.' . number_format($pengajuan->denda_imbt, 2, ",", ".") : '0';
        // mmq
        $data['tujuan_penggunaan_mmq'] = $pengajuan->tujuan_penggunaan_mmq;
        $data['harga_beli_asset_mmq'] = ($harga != '0' ? 'Rp.' . number_format($harga, 2, ",", ".") : $harga);
        $uang_muka_mmq = !empty($pengajuan->uang_muka_mmq) ? $pengajuan->uang_muka_mmq : '0';
        $data['uang_muka_mmq'] = ($uang_muka_mmq != '0' ? 'Rp.' . number_format($uang_muka_mmq, 2, ",", ".") : '0');
        $data['modal_syirkah'] = !empty($plafond + $pengajuan->uang_muka_mmq) ? 'Rp' . number_format($plafond + $pengajuan->uang_muka_mmq, 2, ",", ".") : '0';
        $hishshah_per_unit = !empty($pengajuan->hishshah_per_unit) ? $pengajuan->hishshah_per_unit : '0';
        $data['hishshah_per_unit'] = ($hishshah_per_unit != '0' ? 'Rp.' . number_format($hishshah_per_unit, 2, ",", ".") : '0');
//        $data['hishshah_penyelenggara'] = !empty($plafond / $hishshah_per_unit) ? 'Rp' . number_format($plafond / $hishshah_per_unit, 2, ",", ".") : '0';
        $data['hishshah_penyelenggara'] = self::calc_rep_ratio($plafond, $hishshah_per_unit) != 0 ? 'Rp.' . number_format($plafond / $hishshah_per_unit, 2, ",", ".") : '0';
//        $data['hishshah_penerima_pendanaan'] = !empty($uang_muka_mmq / $hishshah_per_unit) ? 'Rp' . number_format($uang_muka_mmq / $hishshah_per_unit, 2, ",", ".") : '0';
        $data['hishshah_penerima_pendanaan'] = self::calc_rep_ratio($uang_muka_mmq, $hishshah_per_unit) != 0 ? 'Rp.' . number_format($uang_muka_mmq / $hishshah_per_unit, 2, ",", ".") : '0';
//        $total_hishshah = ($plafond / $hishshah_per_unit) + ($uang_muka_mmq / $hishshah_per_unit);
        $total_hishshah = (self::calc_rep_ratio($plafond, $hishshah_per_unit)) + (self::calc_rep_ratio($uang_muka_mmq, $hishshah_per_unit));
        $data['total_hishshah'] = ($data['hishshah_penyelenggara'] == '0' && $data['hishshah_penerima_pendanaan'] == '0') ? '0' : 'Rp.' . number_format($total_hishshah, 2, ",", ".");
        $data['jangka_waktu_pendanaan_mmq'] = !empty($pengajuan->durasi_proyek) ? $pengajuan->durasi_proyek : '0';
        $data['jangka_waktu_penyerahan_objek_mmq'] = !empty($pengajuan->jangka_waktu_penyerahan_objek_mmq) ? $pengajuan->jangka_waktu_penyerahan_objek_mmq : '0';
        $data['objek_bagi_hasil_mmq'] = (empty($pengajuan->objek_bagi_hasil_mmq) ? '' : ($pengajuan->objek_bagi_hasil_mmq == 1 ? "Pendapatan Sewa Dari Pembayaran Angsuran Sewa Per Bulan Oleh Penyewa (Musta'Jir)" : 'Lainnya'));
        $data['ekspektasi_nilai_sewa_mmq'] = $estimasi_imbal_hasil;
        $data['nisbah_penyelenggara_mmq'] = !empty($pengajuan->nisbah_penyelenggara) ? 'Rp.' . number_format($pengajuan->nisbah_penyelenggara, 2, ",", ".") : '0';
        $data['nisbah_penerima_mmq'] = !empty($pengajuan->nisbah_penerima_dana) ? 'Rp.' . number_format($pengajuan->nisbah_penerima_dana, 2, ",", ".") : '0';
        $data['denda_mmq'] = !empty($pengajuan->denda_mmq) ? 'Rp.' . number_format($pengajuan->denda_mmq, 2, ",", ".") : '0';
        $data['objek_sewa'] = $pengajuan->objek_sewa;
        $data['jangka_waktu_sewa'] = $pengajuan->durasi_proyek . ' bulan';
        $data['jangka_waktu_penyerahan_ijarah'] = $pengajuan->jangka_waktu_penyerahan_ijarah . ' bulan';
        $data['penyesuaian_nilai_sewa'] = ($pengajuan->penyesuaian_nilai_sewa == 1 ? "Review Ujrah Dilakukan Pada Setiap Tanggal 01 - 05, Periode November Dan Mei Pada Tahun Berjalan" : ($pengajuan->penyesuaian_nilai_sewa == 2 ? "Penyesuaian Ujrah Pada Sistem Dilakukan Pada Tanggal 01 - 05 Periode Desember Dan Juni Tahun Berjalan" : ($pengajuan->penyesuaian_nilai_sewa == 3 ? "Penyesuaian Ujrah Berlaku Efektif Pada Setiap Tanggal 01 Periode Januari Dan Juli Tahun Berjalan" : '')));
        $data['jenis_objek_pendanaan'] = $pengajuan->jenis_objek_pendanaan;
        $data['status_jenis_sertifikat'] = ($pengajuan->jenis_agunan == 1 ? "SHGB" : "SHM");
        $data['nomor_agunan'] = $pengajuan->nomor_agunan;
        $data['tanggal_jatuh_tempo'] = date("j F Y", strtotime($pengajuan->tanggal_jatuh_tempo));
        $data['atas_nama_agunan'] = $pengajuan->atas_nama_agunan;
        $data['alamat_sertifikat'] = $pengajuan->alamat_sertifikat;
        $data['no_izin_mendirikan_bangunan'] = $pengajuan->no_imb;
        $data['imbt_atas_nama'] = $pengajuan->imbt_atas_nama;
        $data['luas_tanah'] = $pengajuan->luas_tanah . ' m2';
        $data['luas_bangunan'] = $pengajuan->luas_bangunan . ' m2';
        $data['tanggal_penilaian_agunan'] = date("j F Y", strtotime($pengajuan->tanggal_penilaian_agunan));
        $data['appraisal'] = ($pengajuan->appraisal == '1' ? 'Internal' : ($pengajuan->appraisal == '2' ? 'External (KJPP)' : ''));
        $data['nilai_pasar_wajar'] = !empty($pengajuan->nilai_pasar_wajar) ? 'Rp.' . number_format($pengajuan->nilai_pasar_wajar, 2, ",", ".") : '0';
        $data['nilai_likuidasi'] = !empty($pengajuan->nilai_likuidasi) ? 'Rp.' . number_format($pengajuan->nilai_likuidasi, 2, ",", ".") : '0';
        $data['rekomendasi_appraisal'] = ($pengajuan->rekomendasi_appraisal == 1 ? 'Direkomendasikan' : ($pengajuan->rekomendasi_appraisal == 2 ? 'Tidak Direkomendasikan' : ''));
        $data['jenis_pengikatan_agunan'] = !empty($pengajuan->jenis_pengikatan)? $pengajuan->jenis_pengikatan : '';
        $data['nilai_pengikatan_agunan'] = !empty($pengajuan->nilai_pengikatan_agunan) ? 'Rp.' . number_format($pengajuan->nilai_pengikatan_agunan, 2, ",", ".") : '0';
        $data['percent_pengikat_agunan'] = self::calc_rep_ratio($pengajuan->nilai_pengikatan_agunan,$pengajuan->plafon) !=0? number_format($pengajuan->nilai_pengikatan_agunan/$pengajuan->plafon, 2, ",", ".") : '0';

        $Qbiaya = \DB::table('brw_dtl_biaya')
        ->where('brw_dtl_biaya.pengajuan_id', $pengajuanId);
        $biaya = $Qbiaya->get();

        $Qresume = \DB::table('brw_resume_pendanaan')
        ->where('brw_resume_pendanaan.pengajuan_id', $pengajuanId);
        $resume = $Qresume->get();

        $Qdeviasi = \DB::table('brw_dtl_deviasi')
        ->where('brw_dtl_deviasi.pengajuan_id', $pengajuanId);
        $deviasi = $Qdeviasi->get();

        $Qputusan = \DB::table('brw_komite_putusan')
        ->where('brw_komite_putusan.pengajuan_id', $pengajuanId);
        $putusan = $Qputusan->get();

        $Qkeputusan = \DB::table('brw_komite_putusan')
        ->where('brw_komite_putusan.pengajuan_id', $pengajuanId)
        ->first();
        
        if ($cekUser->name == 'Direktur'){
            $data['ROD']        = '';
            $data['disabledD']  = '';
            $data['RODU']       = 'readonly';
            $data['disabledDU'] = 'disabled';
        }elseif ($cekUser->name == 'Direktur Utama'){
            $data['ROD']        = 'readonly';
            $data['disabledD']  = 'disabled';
            $data['RODU']       = '';
            $data['disabledDU'] = '';
        }else{
            $data['ROD']        = 'readonly';
            $data['disabledD']  = 'disabled';
            $data['RODU']       = 'readonly';
            $data['disabledDU'] = 'disabled';
        }
        if(isset($Qkeputusan)){
            if ($cekUser->name == 'Direktur' ){
                $data['nama_direktur']          = auth::user()->firstname.' '.auth::user()->lastname;
                $data['nama_direktur_utama']    = $Qkeputusan->nama_direktur_utama;
            }elseif ($cekUser->name == 'Direktur Utama'){
                $data['nama_direktur']          = $Qkeputusan->nama_direktur;
                $data['nama_direktur_utama']    = auth::user()->firstname.' '.auth::user()->lastname;
            }else{
                $data['nama_direktur']          = $Qkeputusan->nama_direktur;
                $data['nama_direktur_utama']    = $Qkeputusan->nama_direktur_utama;
            }
            $data['catatan_direktur']                   = $Qkeputusan->catatan_direktur;
            $data['rekomendasi_direktur']               = $Qkeputusan->rekomendasi_direktur;
            $data['penggalangan_dana_direktur']         = $Qkeputusan->penggalangan_dana_direktur;
            $data['tanggal_keputusan_direktur']         = date('Y-m-d',strtotime(now()));
            $data['created_at_direktur']                = $Qkeputusan->created_at_direktur = null ? date('Y-m-d',strtotime($Qkeputusan->created_at_direktur)) :date('Y-m-d',strtotime(now()));
            
            $data['catatan_direktur_utama']             = $Qkeputusan->catatan_direktur_utama;
            $data['rekomendasi_direktur_utama']         = $Qkeputusan->rekomendasi_direktur_utama;
            $data['penggalangan_dana_direktur_utama']   = $Qkeputusan->penggalangan_dana_direktur_utama;
            $data['tanggal_keputusan_direktur_utama']   = date('Y-m-d',strtotime(now()));
            $data['created_at_direktur_direktur_utama'] = $Qkeputusan->created_at_direktur_direktur_utama = null ? date('Y-m-d',strtotime($Qkeputusan->created_at_direktur_direktur_utama)) : date('Y-m-d',strtotime(now()));
        }else{
            if ($cekUser->name == 'Direktur'){
                $data['nama_direktur']                      = auth::user()->firstname.' '.auth::user()->lastname;
                $data['nama_direktur_utama']                = '';
                $data['tanggal_keputusan_direktur']         = date('Y-m-d',strtotime(now()));
                $data['tanggal_keputusan_direktur_utama']   = '';
            }elseif ($cekUser->name == 'Direktur Utama'){
                $data['nama_direktur']                      = '';
                $data['nama_direktur_utama']                = auth::user()->firstname.' '.auth::user()->lastname;
                $data['tanggal_keputusan_direktur']         = '';
                $data['tanggal_keputusan_direktur_utama']   = date('Y-m-d',strtotime(now()));
            }else{
                $data['nama_direktur']                      = '';
                $data['nama_direktur_utama']                ='';
                $data['tanggal_keputusan_direktur']         = '';
                $data['tanggal_keputusan_direktur_utama']   = '';
            }
            $data['catatan_direktur']                   = '';
            $data['rekomendasi_direktur']               = 0;
            $data['penggalangan_dana_direktur']         = 0;
                
            $data['catatan_direktur_utama']             = '';
            $data['rekomendasi_direktur_utama']         = 0;
            $data['penggalangan_dana_direktur_utama']   = 0;
        }
        //dd($data);
        
        return view('pages.admin.adrKomite_detilpendanaan')->with(['cekUser' => $cekUser,'data'=> $data, 'biaya'=> $biaya, 'resume' => $resume, 'deviasi' => $deviasi, 'putusan' => $putusan]);
    }

    public function calc_rep_ratio($self, $other)
    {
        if ($self != 0) {
            return $other / $self;
        } else {
            return 0;
        }
    }

    public function insertUpdateResume(Request $request)
    {
        // return $request->all();
        $resume = DB::table('brw_komite_putusan')->where('pengajuan_id', $request->pengajuan_id);
        $role = $request->role;
        if($resume->first() == null){
            $resume->insert([
                'pengajuan_id' => $request->pengajuan_id,
                'nama'.$role => $request->nama_user,
                'rekomendasi'.$role => $request->rekomendasi,
                'catatan'.$role => $request->catatan,
                'penggalangan_dana'.$role => $request->penggalangan_dana,
                'tanggal_keputusan'.$role => $request->tanggal_keputusan,
                'created_at'.$role => $request->tanggal_buat,
                'updated_at'.$role => date("Y-m-d H:i:s")
            ]);
/*            DB::table('brw_analisa_pendanaan')->where('pengajuan_id', $request->pengajuan_id)->update([
                'status_komite' => 17 
            ]);*/
        } else {
            $resume->update([
                'nama'.$role => $request->nama_user,
                'rekomendasi'.$role => $request->rekomendasi,
                'catatan'.$role => $request->catatan,
                'penggalangan_dana'.$role => $request->penggalangan_dana,
                'tanggal_keputusan'.$role => $request->tanggal_keputusan,
                'created_at'.$role => $request->tanggal_buat,
                'updated_at'.$role => date("Y-m-d H:i:s")
            ]);
        };

        $Simpan = '';
        if ($role == '_direktur'){
            $Simpan = 'SIMPAN_DIREKTUR';
        }else if ($role == '_direktur_utama'){
            $Simpan = 'SIMPAN_DIRUT';
        }

        $update_status = DB::select(
            "CALL proc_click_button_borrower(
                30, /* form_id int */
                '$request->nama_user',
                $request->pengajuan_id, /* pengajuan_id int */
                '$Simpan', /* method varchar(10) ['simpan' , 'kirim'] */
                'KomitePendanaan.php', /* file varchar(100) */
                380 /* line int */
           )"
        );
        // dd($update_status);

        $audit = new AuditTrail;
        $audit->fullname = $request->nama_user;
        $audit->menu = "Komite Pendanaan";
        $audit->description = $request->nama_user." on pengajuan ".$request->pengajuan_id ;
        $audit->ip_address =  \Request::ip();
        $audit->save();

        return 1;
    }

    public function kirimKomite(Request $request)
    {
        // return $request->all();
        $idPengajuan            = $request->idPengajuan;
        $user_login             = $request->user_login;       

        $checking = DB::table('brw_komite_putusan')->where('pengajuan_id', $request->idPengajuan);
        $checking ->whereNotNull('nama_direktur');
        $checking ->whereNotNull('nama_direktur_utama');
        if(!$checking->first() == null){
            $update_status = DB::select(
                "CALL proc_click_button_borrower(
                    30, /* form_id int */
                    '$user_login',
                    $idPengajuan, /* pengajuan_id int */
                    'KIRIM', /* method varchar(10) ['simpan' , 'kirim'] */
                    'KomitePendanaan.php', /* file varchar(100) */
                    410 /* line int */
                )"
            );


            $pengajuan = BorrowerPengajuan::where('pengajuan_id', $idPengajuan)->first();


            if($pengajuan->pendanaan_tipe ===  BorrowerTipePendanaan::DANA_KONSTRUKSI) {
                
                $deskripsi_proyek = new  Deskripsi_Proyek;
                $deskripsi_proyek->deskripsi = $request->deskripsi;
                $deskripsi_proyek->save();
        
                $legalitas_proyek = new Legalitas_Proyek;
                $legalitas_proyek->deskripsi_legalitas = $request->legalitas;
                $legalitas_proyek->save();
        
                $pemilik_deskrip = new Pemilik_Proyek;
                $pemilik_deskrip->deskripsi_pemilik = $request->pemilik_projec;
                $pemilik_deskrip->save();
        
                $simulasi_proyek = new Simulasi_Proyek;
                $simulasi_proyek->deskripsi_simulasi = $request->simulasi;
                $simulasi_proyek->save();
                
                $proyek = new Proyek;

                $proyek->nama = $pengajuan->pendanaan_nama;
                $proyek->alamat = $pengajuan->kota;
                $proyek->akad = 1;
                $proyek->interval = 1;
                $proyek->harga_paket = 1000000;
                $proyek->profit_margin = $pengajuan->estimasi_imbal_hasil;
                $proyek->total_need = $pengajuan->pendanaan_dana_dibutuhkan;
                $proyek->status = 99;

                $proyek->save();


                $audit = new AuditTrail;
                $username = Auth::guard('admin')->user()->firstname;
                $audit->fullname = $username;
                $audit->menu = "Kelola Proyek";
                $audit->description = "Tambah proyek baru";
                $audit->ip_address =  \Request::ip();
                $audit->save();


                //Update proyek id to pengajuan 
                BorrowerPengajuan::where('pengajuan_id', $idPengajuan)->update(['id_proyek'=> $proyek->id]);

            }

           
             
            return 1;
        }else{
            // dd(2);
            return 2;
        }
    }

}
