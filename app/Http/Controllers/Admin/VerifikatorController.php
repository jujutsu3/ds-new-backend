<?php

namespace App\Http\Controllers\Admin;

use DB;

use App\Helpers\Helper;
use App\BorrowerPengurus;
use Illuminate\Http\Request;
use App\BorrowerAnalisaPendanaan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Services\VerifikatorService;
use App\BorrowerVerifikatorPengajuan;
use App\BorrowerMaterialRequisitionHeader;
use App\Http\Controllers\MaterialOrdersController;

class VerifikatorController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:admin')->only(['taskList', 'listLengkapiPengajuan', 'form_lengkapi_pengajuan', 'profilPendanaan']);
    }


    public function listLengkapiPengajuan()
    {
        $tipe_pendanaan = \DB::table('brw_tipe_pendanaan')->pluck('pendanaan_nama', 'tipe_id');
        return view('pages.admin.verifikator.list_lengkapi_pengajuan', compact('tipe_pendanaan'));
    }


    public function populateData($pengajuan_id, $brw_id)
    {
        $pengajuan = \DB::table('brw_pengajuan')
            ->leftjoin('brw_agunan', 'brw_agunan.id_pengajuan', 'brw_pengajuan.pengajuan_id')
            ->leftjoin('brw_dokumen_objek_pendanaan', 'brw_dokumen_objek_pendanaan.id_pengajuan', 'brw_pengajuan.pengajuan_id')
            ->leftjoin('brw_dokumen_legalitas_pribadi', 'brw_dokumen_legalitas_pribadi.brw_id', 'brw_pengajuan.brw_id')
            ->leftjoin('brw_dokumen_legalitas_badan_hukum', 'brw_dokumen_legalitas_badan_hukum.brw_id', 'brw_pengajuan.brw_id')
            ->join('brw_user_detail_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_user_detail_pengajuan.pengajuan_id')
            ->where('brw_pengajuan.pengajuan_id', $pengajuan_id)
            ->where('brw_pengajuan.brw_id', $brw_id)
            ->first();

            $getprofil = DB::SELECT("SELECT a.*,b.*,c.brw_norek,c.brw_nm_pemilik,c.brw_kd_bank,h.nama_bank, c.kantor_cabang_pembuka, d.agama as dagama , e.pendidikan as ependidikan, f.jenis_kawin as fkawin, b.status_kawin as id_kawin
        FROM brw_user_detail_pengajuan b
        LEFT JOIN brw_pengajuan a ON  a.pengajuan_id = b.pengajuan_id 
        LEFT JOIN brw_rekening c ON  a.brw_id = c.brw_id
        LEFT JOIN m_agama d ON  b.agama = d.id_agama
        LEFT JOIN m_pendidikan e ON  b.pendidikan_terakhir = e.id_pendidikan
        LEFT JOIN m_kawin f ON  b.status_kawin = f.id_kawin
        LEFT JOIN m_pekerjaan g ON  g.id_pekerjaan = b.pekerjaan
        LEFT JOIN m_bank h ON  c.brw_kd_bank = h.kode_bank
        WHERE a.pengajuan_id = ?", [$pengajuan_id]);

            if ($getprofil == NULL) {
                $profil = 0;
            } else {
                $profil = $getprofil[0];
            }

            $getpekerjaan = DB::select("SELECT a.pekerjaan as pekerjaan_id, c.pekerjaan,d.tipe_online as bidang_online, 
        case when b.bentuk_badan_usaha = 0 then 0 ELSE 
        (SELECT bentuk_badan_usaha FROM m_bentuk_badan_usaha WHERE id = b.bentuk_badan_usaha) END AS bbu ,b.*, e.bidang_pekerjaan
        FROM brw_user_detail_pengajuan a
        LEFT JOIN brw_user_detail_penghasilan_pengajuan b ON  b.pengajuan_id = a.pengajuan_id 
        LEFT JOIN m_pekerjaan c ON  a.pekerjaan = c.id_pekerjaan
        LEFT JOIN m_online d ON  a.bidang_online = d.id_online
        LEFT JOIN m_bidang_pekerjaan e ON e.id_bidang_pekerjaan = a.bidang_pekerjaan
        WHERE a.brw_id = ? and a.pengajuan_id = ? ", [$brw_id, $pengajuan_id]);


            if ($getpekerjaan == NULL) {
                $pekerjaan = 0;
            } else {
                $pekerjaan = $getpekerjaan[0];
            }



        $borrower_penghasilan =  \DB::table('brw_user_detail_penghasilan')->where('brw_id2', $brw_id)->select(['sumber_pengembalian_dana', 'skema_pembiayaan'])->first();
        $status_kawin = \DB::table('brw_user_detail')->where('brw_id', $brw_id)->value('status_kawin');
        $rumah_ke = \DB::table('brw_pendanaan_rumah_lain')->where('pengajuan_id', $pengajuan_id)->count('rumah_ke');

        $pendanaan_nama = \DB::table('brw_tipe_pendanaan')->where('tipe_id', $pengajuan->pendanaan_tipe)->value('pendanaan_nama');
        $tujuan_pendanaan = \DB::table('m_tujuan_pembiayaan')->where('id', $pengajuan->pendanaan_tujuan)->value('tujuan_pembiayaan');
        $count_dana_non_rumah_lain = \DB::table('brw_pendanaan_non_rumah_lain')->where('pengajuan_id', $pengajuan_id)->count();

        $brw_user_detail_pic = \DB::table('brw_user_detail_pengajuan')->where('pengajuan_id', $pengajuan_id)->where('brw_id', $brw_id)->select(['brw_pic', 'brw_pic_user_ktp', 'brw_pic_ktp', 'brw_pic_npwp', 'brw_type'])->first();

        $kantor_bpn = \DB::table('m_kantor_bpn')->pluck('nama_kantor', 'id');

        $borrower_pasangan = \DB::table('brw_pasangan')->where('pasangan_id', $brw_id)->first();
        $status_verifikator = BorrowerAnalisaPendanaan::where('pengajuan_id', $pengajuan_id)->value('status_verifikator');

        $resultDocumentPersyaratan =  Helper::getDokumenList($pengajuan);

        $data_dokumen_persyaratan = [];
        foreach ($resultDocumentPersyaratan as $val) {
            $data_dokumen_persyaratan[$val->page_title][$val->category_title][] = $val;
        }

        $enable_edit = true;
        $brw_verifikator_pengajuan = BorrowerVerifikatorPengajuan::where('pengajuan_id', $pengajuan_id)->first();
        if ($brw_verifikator_pengajuan) {
            $enable_edit = ($brw_verifikator_pengajuan->status_aktifitas === 3) ? false : true;
        }


        $data_pengurus = [];
        if ($profil->brw_type === 2) {
            $data_pengurus = BorrowerPengurus::where('brw_id', $brw_id)->get();
        }

        $jenis_properti = '-';
        if ($pengajuan->jenis_properti == "1") {
            $jenis_properti = 'Baru';
        } elseif ($pengajuan->jenis_properti == "2") {
            $jenis_properti = 'Lama';
        }

        $queryMaterialItems = MaterialOrdersController::queryMaterialItemList();
        $requisitionHeader = BorrowerMaterialRequisitionHeader::where('pengajuan_id', $pengajuan_id)->first();
        $requisitionDetail  = $queryMaterialItems->orderBy('brw_material_requisition_dtl.task_id', 'asc')->orderBy('creation_date', 'asc')->where('brw_material_requisition_hdr.pengajuan_id', $pengajuan_id)->get();
        
        return ['title' => $pendanaan_nama . ' - ' . $tujuan_pendanaan,
        'rumah_ke' => ($rumah_ke + 1),
        'pengajuan' => $pengajuan,
        'sumber_pengembalian_dana' => $borrower_penghasilan->sumber_pengembalian_dana,
        'skema_pembiayaan' => $borrower_penghasilan->skema_pembiayaan,
        'status_bank_non_rumah_lain' => ($count_dana_non_rumah_lain > 0) ? '1' : '2',
        'status_kawin' => $status_kawin,
        'pendanaanTipe' => $pendanaan_nama,
        'tujuanPendanaan' => $tujuan_pendanaan,
        'count_dana_non_rumah_lain' => $count_dana_non_rumah_lain,
        'kantor_bpn' => $kantor_bpn,
        'brw_id' => $brw_id,
        'pengajuan_id' => $pengajuan_id,
        'status_pengajuan' => '',
        'brw_user_detail_pic' => $brw_user_detail_pic,
        'borrower_pasangan' => $borrower_pasangan,
        'status_verifikator' => $status_verifikator,
        'enable_edit' => $enable_edit,
        'data_dokumen_persyaratan' => $data_dokumen_persyaratan,
        'brw_type' => $brw_user_detail_pic->brw_type,
        'profil' => $profil,
        'alamat' => $profil->alamat,
        'pengurus' => $data_pengurus,
        'pendanaan_tipe' => $pendanaan_nama,
        'tujuan_pendanaan' => $tujuan_pendanaan,
        'jenis_pekerjaan' => $pekerjaan->pekerjaan,
        'bidang_pekerjaan' => $pekerjaan->bidang_pekerjaan,
        'agama' => $profil->dagama,
        'status_kawin' => $profil->fkawin,
        'pendidikan_terakhir' => $profil->ependidikan,
        'telepon' => $profil->no_tlp,
        'tempat_lahir' => $profil->tempat_lahir,
        'tgl_lahir' => $profil->tgl_lahir,
        'provinsi' => $profil->provinsi,
        'kota' => $profil->kota,
        'kecamatan' => $profil->kecamatan,
        'kelurahan' => $profil->kelurahan,
        'kode_pos' => $profil->kode_pos,
        'status_rumah' => $profil->status_rumah,
        'pekerjaan_id' => $pekerjaan->pekerjaan_id,
        'jenis_pekerjaan' => $pekerjaan->pekerjaan,
        'sumberpengembaliandana' => $pekerjaan->sumber_pengembalian_dana,
        'skemapembiayaan' => $pekerjaan->skema_pembiayaan,
        'nama_perusahaan' => $pekerjaan->nama_perusahaan,
        'bidang_online' => $pekerjaan->bidang_online,
        'bentuk_badan_usaha' => $pekerjaan->bbu,
        'status_kepegawaian' => ($pekerjaan->status_pekerjaan == 1) ? 'Kontrak' : 'Tetap',
        'usia_perusahaan' => $pekerjaan->usia_perusahaan,
        'departemen' => $pekerjaan->departemen,
        'jabatan' => $pekerjaan->jabatan,
        'tahun_bekerja' => $pekerjaan->masa_kerja_tahun,
        'bulan_bekerja' => $pekerjaan->masa_kerja_bulan,
        'bidang_pekerjaan' => $pekerjaan->bidang_pekerjaan,
        'no_telpon_usaha' => $pekerjaan->no_telp,
        'nama_perusahaan' => $pekerjaan->nama_perusahaan,
        'penghasilan' => "Rp." . number_format($pekerjaan->pendapatan_borrower, 0, ",", "."),
        'harga_objek_pendanaan' => "Rp." . number_format($pengajuan->harga_objek_pendanaan, 2, ",", "."),
        'uang_muka' => "Rp." . number_format($pengajuan->uang_muka, 2, ",", "."),
        'uang_muka_percentage' => ($pengajuan->uang_muka) ? (($pengajuan->uang_muka / $pengajuan->harga_objek_pendanaan) * 100) . "%" : '',
        'nilai_pengajuan' => "Rp." . number_format($pengajuan->pendanaan_dana_dibutuhkan, 2, ",", "."),
        'jenis_properti' => $jenis_properti,
        'requisitionHeader' => $requisitionHeader,
        'requisitionDetail' => $requisitionDetail];
   
    }

    public function profilPendanaan($pengajuan_id, $brw_id)
    {
        $data = $this->populateData($pengajuan_id, $brw_id);
        return view('pages.admin.verifikator.profil_penerima_pendanaan')->with($data);
    }


    public function form_lengkapi_pengajuan($pengajuan_id, $brw_id)
    {

        $data = $this->populateData($pengajuan_id, $brw_id);
        return view('pages.admin.verifikator.detail_lengkapi_pengajuan')->with($data);
    }


    public function lengkapiPengajuanData()
    {
        $input = Input::all();

        $query = DB::table('brw_pengajuan');
        $query->join('brw_tipe_pendanaan', 'brw_pengajuan.pendanaan_tipe', 'brw_tipe_pendanaan.tipe_id');
        $query->join('m_tujuan_pembiayaan', 'brw_pengajuan.pendanaan_tujuan', 'm_tujuan_pembiayaan.id');
        $query->join('brw_user_detail_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_user_detail_pengajuan.pengajuan_id');
        $query->join('brw_verifikator_pengajuan', 'brw_pengajuan.pengajuan_id', 'brw_verifikator_pengajuan.pengajuan_id');
        $query->leftjoin('brw_hd_verifikator', 'brw_hd_verifikator.pengajuan_id', 'brw_pengajuan.pengajuan_id');
        $query->join('brw_user', 'brw_user_detail_pengajuan.brw_id' , 'brw_user.brw_id');
        $query->selectRaw('brw_pengajuan.brw_id, IF(brw_user_detail_pengajuan.brw_type = 1, UPPER(brw_user_detail_pengajuan.nama), UPPER(brw_user_detail_pengajuan.nm_bdn_hukum)) as nama_cust, brw_pengajuan.pengajuan_id, brw_pengajuan.created_at AS tgl_pengajuan, 
                    brw_tipe_pendanaan.tipe_id, brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan, m_tujuan_pembiayaan.tujuan_pembiayaan,  brw_pengajuan.pendanaan_dana_dibutuhkan AS nilai_pengajuan, 
                    brw_pengajuan.status as stts, setuju_verifikator_pengajuan, setuju_verifikator_dokumen, brw_hd_verifikator.spk, brw_verifikator_pengajuan.status_aktifitas, 
                    brw_verifikator_pengajuan.rekomendasi_file, brw_verifikator_pengajuan.updated_by as updated_verifikator, brw_verifikator_pengajuan.updated_at as tgl_updated_verifikator,
                    brw_user.email, IF(brw_user_detail_pengajuan.brw_type = 1, brw_user_detail_pengajuan.no_tlp, brw_user_detail_pengajuan.telpon_perusahaan) as no_telp ');

        

        // $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->get();

        // $response = ['data' => $pengajuan];
        // return response()->json($response);
        $query->where(function ($query) {
            $query->where('brw_pengajuan.setuju_verifikator_pengajuan', '=', 1)
                  ->orWhere('brw_pengajuan.setuju_verifikator_dokumen', '=', 1);
        });

        if(isset($input['search_filter'])){

            $filter_data = json_decode($input['search_filter']);
           
            if (isset($filter_data->filter_nama_penerima) && !empty($filter_data->filter_nama_penerima)) {
                $query->where('brw_user_detail_pengajuan.nama', 'like', '%'. $filter_data->filter_nama_penerima. '%');
            }

            if (!empty($filter_data->filter_tipe_id)) {
                $query->where('brw_pengajuan.pendanaan_tipe', $filter_data->filter_tipe_id);
            }

            if (isset($filter_data->filter_status) && $filter_data->filter_status <> '') {
                $query->where('brw_verifikator_pengajuan.status_aktifitas', $filter_data->filter_status);
            }

            if (!empty($filter_data->filter_start_date) && !empty($filter_data->filter_end_date)) {
                $query->whereBetween('brw_pengajuan.created_at', [$filter_data->filter_start_date." 00:00:00", $filter_data->filter_end_date." 23:59:59"]);
            }     
        }

        
        $pengajuan = $query->orderBy('brw_pengajuan.created_at', 'desc')->get();
        $response = ['data' => $pengajuan];
        return response()->json($response);
    }

    public function taskList()
    {
        return view('pages.admin.borrower.main_verifikasi_occasio_view');
    }

    public function new_tahap_verifikasi($pengajuan_id)
    {
        return view('pages.admin.borrower.verifikator.new_tahap_verifikasi', ['pengajuan_id' => $pengajuan_id]);
    }

    public function view_detil_verif_occasio($pengajuan_id)
    {
        return view('pages.admin.borrower.detil_verif_occasio_view', ['pengajuan_id' => $pengajuan_id]);
    }

    public function kirimVerifikator(Request $request)
    {
        $verifikator = new VerifikatorService();
        $result = $verifikator->kirimVerifikasiData($request);
        return ($result) ? '1' : '0';
    }

    public function jumlahTaskSelesai($pengajuan_id)
    {
        $verifikator = new VerifikatorService();
        return $verifikator->checkAllStatusPekerjaan($pengajuan_id);
    }


    public function setuju_verifikator_pengajuan(Request $request)
    {
        $verifikator = new VerifikatorService();
        return $verifikator->setujuVerifikatorPengajuan($request->pengajuan_id);
    }

    public function setuju_verifikator_dokumen(Request $request)
    {
        $verifikator = new VerifikatorService();
        return $verifikator->setujuVerifikatorDokumen($request->pengajuan_id);
    }

    public function kirim_pengajuan(Request $request)
    {
        return VerifikatorService::kirimPengajuan($request);
    }

    public function getVerifikatorPengajuan($pengajuan_id)
    {
        return VerifikatorService::getVerifikatorPengajuan($pengajuan_id);
    }
}
