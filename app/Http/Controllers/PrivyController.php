<?php
// privy
namespace App\Http\Controllers;

use App\InvestorAdministrator;
use App\InvestorCorporation;
use App\InvestorPengurus;
use App\Services\InvestorService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\CheckUserSign;
use App\Investor;
use App\DetilInvestor;
use App\MasterProvinsi;
use App\MasterAgama;
use App\MasterAsset;
use App\MasterBadanHukum;
use App\MasterBank;
use App\MasterBidangPekerjaan;
use App\MasterJenisKelamin;
use App\MasterJenisPengguna;
use App\MasterKawin;
use App\MasterKepemilikanRumah;
use App\MasterNegara;
use App\MasterOnline;
use App\MasterPekerjaan;
use App\MasterPendapatan;
use App\MasterPendidikan;
use App\MasterPengalamanKerja;
use App\RekeningInvestor;
use App\PendanaanAktif;
use App\Proyek;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\LogAkadDigiSignInvestor;
use App\MasterNoAkadInvestor;
use Illuminate\Support\Facades\Storage;
use App\MasterNoAkadBorrower;
use App\BorrowerJaminan;
use App\MasterJenisJaminan;
use App\Borrower;
use App\BorrowerDetails;
use App\BorrowerPendanaan;
use App\BorrowerTipePendanaan;
use App\BorrowerRekening;
use App\LogAkadDigiSignBorrower;
use App\AhliWarisInvestor;
use App\MasterNoSP3;
use App\LogSP3Borrower;
use App\LogRekening;
use App\LogDigiSignResponse;
use PhpOffice\PhpWord\Element\TextRun;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\Word2007\Element\Section;
use Terbilang;
use DB;


class PrivyController extends Controller
{

    public function __construct()
    {

        //$this->middleware('auth:api-providers',['only' => ['callbackResponse']]);

    }

    //API Redirect
    private function decryptDigiSign($data, $key)
    {
        $output = false;
        $output = openssl_decrypt(base64_decode($data), 'aes-128-ecb', $key, OPENSSL_PKCS1_PADDING);
        return $output;
    }

    public function base64_usename_password()
    {

        return base64_encode(config('app.privy_username') . ':' . config('app.privy_password'));
    }


    public function testAPI()
    {

        $testakad = $this->logAkadDigiSignBorrower('257', '', '843', 1, '100000000', 'kirim', 'borrowerKontrak_20200913_843_19', 'uwow', 'URL', 'none');

        return $testakad;
    }

    // ####################################################### CEK POTO EXIST INVESTOR & BORROWER  ####################################################### //

    // cek poto exist
    public function cekFotoDiriExist($userId, $user_type)
    {

        if ($user_type == 2) {

            $selectPathFoto = BorrowerDetails::leftJoin('brw_user', 'brw_user.brw_id', '=', 'brw_user_detail.brw_id')
                ->where('brw_user.brw_id', $user_id)->first();


            $fotoDiriExist = "";
            if (is_file(storage_path('app/private/' . $selectPathFoto->brw_pic))) {
                $fotoDiriExist = fopen(storage_path('app/private/' . $selectPathFoto->brw_pic), 'r');
            } else {
                $fotoKtpExist = NULL;
            }

            return $fotoDiriExist;
        }


        $selectPathFoto = DB::table("detil_investor")->where("investor_id", $userId)->first();
        if($selectPathFoto->tipe_pengguna == 2){
        	$selectPathFotoPengurus = DB::table("investor_pengurus")->where("investor_id", $userId)->first();
        	$fotodiri = $selectPathFotoPengurus->foto_diri;
        }elseif($selectPathFoto->tipe_pengguna == 1){
        	$fotodiri = $selectPathFoto->pic_investor;
        }
        $fotoDiriExist = "";
        if (is_file(storage_path('app/private/' . $fotodiri))) {
            $fotoDiriExist = fopen(storage_path('app/private/' . $fotodiri), 'r');
        } else {
            $fotoKtpExist = NULL;
        }

        return $fotoDiriExist;
    }

    // cek poto exist
    public function cekFotoKtpExist($userId, $user_type)
    {

        if ($user_type == 2) {

            $selectPathFoto = BorrowerDetails::leftJoin('brw_user', 'brw_user.brw_id', '=', 'brw_user_detail.brw_id')
                ->where('brw_user.brw_id', $user_id)->first();


            $fotoDiriExist = "";
            if (is_file(storage_path('app/private/' . $selectPathFoto->brw_pic_ktp))) {
                $fotoDiriExist = fopen(storage_path('app/private/' . $selectPathFoto->brw_pic_ktp), 'r');
            } else {
                $fotoKtpExist = NULL;
            }

            return $fotoDiriExist;
        }
        $selectPathFoto = DB::table("detil_investor")->where("investor_id", $userId)->first();
        if($selectPathFoto->tipe_pengguna == 2){
        	$selectPathFotoPengurus = DB::table("investor_pengurus")->where("investor_id", $userId)->first();
        	$fotodiri = $selectPathFotoPengurus->foto_ktp;
        }elseif($selectPathFoto->tipe_pengguna == 1){
        	$fotodiri = $selectPathFoto->pic_ktp_investor;
        }
        $fotoDiriExist = "";
        if (is_file(storage_path('app/private/' . $fotodiri))) {
            $fotoDiriExist = fopen(storage_path('app/private/' . $fotodiri), 'r');
        } else {
            $fotoKtpExist = NULL;
        }

        return $fotoDiriExist;
    }



    // ####################################################### END CEK POTO EXIST INVESTOR & BORROWER  ####################################################### //


    // ####################################################### GENERATE AKAD  ####################################################### //

    // generate No Akad Investor
    public function generateNoAkadInvestor($Id)
    {

        $sekarang = explode("-", date('d-n-Y'));
        $sekarang_format = explode("-", date('d-m-Y'));
        $tgl = $sekarang[0];
        $bln = $sekarang[1];
        $thn = (string)$sekarang[2];
        $randID     = rand(0, 9879);

        switch ($bln) {
            case 1:
                $blnAkad = 'I';
                break;
            case 2:
                $blnAkad = 'II';
                break;
            case 3:
                $blnAkad = 'III';
                break;
            case 4:
                $blnAkad = 'IV';
                break;
            case 5:
                $blnAkad = 'V';
                break;
            case 6:
                $blnAkad = 'VI';
                break;
            case 7:
                $blnAkad = 'VII';
                break;
            case 8:
                $blnAkad = 'VIII';
                break;
            case 9:
                $blnAkad = 'IX';
                break;
            case 10:
                $blnAkad = 'X';
                break;
            case 11:
                $blnAkad = 'XI';
                break;
            case 12:
                $blnAkad = 'XII';
                break;
            default:
                $blnAkad = 0;
                break;
        }

        $riwayatMutasi   = DB::table("mutasi_investor")->where("investor_id", $Id)->whereIn("perihal", ["Transfer Rekening", "Penarikan dana selesai"])->orderBy("id", "DESC")->first();
        $getDataNoAkad = MasterNoAkadInvestor::where('investor_id', $Id)
            ->where('investor_id', $Id)
            ->where('mutasi_id', !empty($riwayatMutasi->id) ? $riwayatMutasi->id : "")
            ->where('bln_akad_inv', $blnAkad)
            ->where('thn_akad_inv', $thn)->orderBy("id_no_akad_inv", "DESC")->first();



        if (empty($getDataNoAkad)) { // jika data kosong, maka akan generate nomor akad

            $masterNoAkad = new MasterNoAkadInvestor;
            $masterNoAkad->investor_id = $Id;
            $masterNoAkad->mutasi_id = $riwayatMutasi->id;
            $masterNoAkad->no_akad_inv = $randID;
            $masterNoAkad->bln_akad_inv = $blnAkad;
            $masterNoAkad->thn_akad_inv = $thn;
            $masterNoAkad->save();

            return $masterNoAkad->no_akad_inv . $riwayatMutasi->id . '/DSI/AWBL/' . $blnAkad . '/' . $thn;
        } else { // jika data ada, maka akan menampilkan nomor akad

            return $getDataNoAkad->no_akad_inv . $riwayatMutasi->id . '/DSI/AWBL/' . $blnAkad . '/' . $thn;
        }
    }

    // generate no akad borrower
    public function generateNoAkadBorrower($id_Proyek)
    {

        $guardName = "";
        if (Auth::guard('web')->check()) {
            //die('hellow web');
            $guardName = Auth::guard('web')->user()->id;
        } elseif (Auth::guard('borrower')->check()) {
            $guardName = Auth::guard('borrower')->user()->brw_id;
        }
        $sekarang = explode("-", date('d-n-Y'));
        $tgl = $sekarang[0];
        $bln = $sekarang[1];
        $thn = (string)$sekarang[2];

        switch ($bln) {
            case 1:
                $blnAkad = 'I';
                break;
            case 2:
                $blnAkad = 'II';
                break;
            case 3:
                $blnAkad = 'III';
                break;
            case 4:
                $blnAkad = 'IV';
                break;
            case 5:
                $blnAkad = 'V';
                break;
            case 6:
                $blnAkad = 'VI';
                break;
            case 7:
                $blnAkad = 'VII';
                break;
            case 8:
                $blnAkad = 'VIII';
                break;
            case 9:
                $blnAkad = 'IX';
                break;
            case 10:
                $blnAkad = 'X';
                break;
            case 11:
                $blnAkad = 'XI';
                break;
            case 12:
                $blnAkad = 'XII';
                break;
            default:
                $blnAkad = 0;
                break;
        }

        $getDataNoAkad = MasterNoAkadBorrower::where('bln_akad_bor', $blnAkad)->where('thn_akad_bor', $thn)->where('proyek_id', $id_Proyek)->orderBy('no_akad_bor', 'desc')->first();


        if (empty($getDataNoAkad)) {


            $getCountNoAkad = DB::select("SELECT count(*) as jumlahNo FROM m_no_akad_borrower");
            if ($getCountNoAkad[0]->jumlahNo == 0) {

                $masterNoAkad = new MasterNoAkadBorrower;
                $masterNoAkad->no_akad_bor = 1;
                $masterNoAkad->proyek_id = $id_Proyek;
                $masterNoAkad->bln_akad_bor = $blnAkad;
                $masterNoAkad->thn_akad_bor = $thn;

                $masterNoAkad->save();

                $nextNoAkad = '001';
                $nextBlnAkad = $blnAkad;
                $nextThnAkad = $thn;
                return $nextNoAkad . '/DSI/AMRB/' . $nextBlnAkad . '/' . $nextThnAkad;
            } else {

                // dd($getDataNoAkad);
                $masterNoAkad = new MasterNoAkadBorrower;
                // $masterNoAkad->no_akad_bor = 0;

                $getDataNoAkad = DB::select("SELECT * FROM m_no_akad_borrower ORDER BY no_akad_bor DESC limit 1");

                $noAkad = $getDataNoAkad[0]->no_akad_bor + 1;

                $nextBlnAkad = $getDataNoAkad[0]->bln_akad_bor;
                $nextThnAkad = $getDataNoAkad[0]->thn_akad_bor;
                if (strlen($noAkad) == 1) {
                    $nextNoAkad = '00' . $noAkad;
                } elseif (strlen($noAkad) == 2) {
                    $nextNoAkad = '0' . $noAkad;
                } else {
                    $nextNoAkad = $noAkad;
                }

                $masterNoAkad->proyek_id = $id_Proyek;
                $masterNoAkad->no_akad_bor = $nextNoAkad;
                $masterNoAkad->bln_akad_bor = $nextBlnAkad;
                $masterNoAkad->thn_akad_bor = $nextThnAkad;

                $masterNoAkad->save();


                return $nextNoAkad . '/DSI/AMRB/' . $nextBlnAkad . '/' . $nextThnAkad;
            }

            // old 

        } else {

            $masterNoAkad = new MasterNoAkadBorrower;
            // $masterNoAkad->no_akad_bor = 0;

            $getDataNoAkad = DB::select("SELECT * FROM m_no_akad_borrower ORDER BY no_akad_bor DESC limit 1");

            $noAkad = $getDataNoAkad[0]->no_akad_bor;

            $nextBlnAkad = $getDataNoAkad[0]->bln_akad_bor;
            $nextThnAkad = $getDataNoAkad[0]->thn_akad_bor;
            if (strlen($noAkad) == 1) {
                $nextNoAkad = '00' . $noAkad;
            } elseif (strlen($noAkad) == 2) {
                $nextNoAkad = '0' . $noAkad;
            } else {
                $nextNoAkad = $noAkad;
            }

            return $nextNoAkad . '/DSI/AMRB/' . $nextBlnAkad . '/' . $nextThnAkad;
        }
    }

    // generate No SP3
    private function generateNoSP3($id_Proyek)
    {
        $sekarang = explode("-", date('d-n-Y'));
        $tgl = $sekarang[0];
        $bln = $sekarang[1];
        $thn = (string)$sekarang[2];

        switch ($bln) {
            case 1:
                $blnSP3 = 'I';
                break;
            case 2:
                $blnSP3 = 'II';
                break;
            case 3:
                $blnSP3 = 'III';
                break;
            case 4:
                $blnSP3 = 'IV';
                break;
            case 5:
                $blnSP3 = 'V';
                break;
            case 6:
                $blnSP3 = 'VI';
                break;
            case 7:
                $blnSP3 = 'VII';
                break;
            case 8:
                $blnSP3 = 'VIII';
                break;
            case 9:
                $blnSP3 = 'IX';
                break;
            case 10:
                $blnSP3 = 'X';
                break;
            case 11:
                $blnSP3 = 'XI';
                break;
            case 12:
                $blnSP3 = 'XII';
                break;
            default:
                $blnSP3 = 0;
                break;
        }

        $getDataNoSP3 = MasterNoSP3::where('bln_sp3', $blnSP3)->where('thn_sp3', $thn)->first();

        if (!empty($getDataNoSP3)) {
            $noSP3 = $getDataNoSP3->no_sp3;
            $noSP3 += 1;
            $nextBlnSP3 = $getDataNoSP3->bln_sp3;
            $nextThnSP3 = $getDataNoSP3->thn_sp3;
            if (strlen($noSP3) == 1) {
                $nextNoSP3 = '00' . $noSP3;
            } elseif (strlen($noSP3) == 2) {
                $nextNoSP3 = '0' . $noSP3;
            } else {
                $nextNoSP3 = $noSP3;
            }

            $getDataNoSP3->no_sp3 = $noSP3;

            $getDataNoSP3->save();
        } else {

            $masterNoSP3 = new MasterNoSP3;
            $masterNoSP3->no_sp3 = 0;
            $masterNoSP3->proyek_id = $id_Proyek;
            $masterNoSP3->bln_sp3 = $blnSP3;
            $masterNoSP3->thn_sp3 = $thn;

            $masterNoSP3->save();

            $nextNoSP3 = '000';
            $nextBlnSP3 = $blnSP3;
            $nextThnSP3 = $thn;
        }

        return $nextNoSP3 . '/DSI/SP3/' . $nextBlnSP3 . '/' . $nextThnSP3;
    }

    // generate No Form RDL
    private function generateNoFormRDL()
    {
        return '001';
    }

    // ####################################################### END GENERATE AKAD  ####################################################### //


    // ####################################################### AKSES LOGIN PORTAL PRIVY ID  ####################################################### //
    public function oauth_privy()
    {

        $client = new Client();
        $request = $client->post(config('app.privy_auth') . '/oauth/token', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'multipart/form-data' . config('app.boundary'), //multipart/form-data //application/x-www-form-urlencoded
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key')
            ],

            'form_params' => [
                'client_id' => config('app.privy_client_id'),
                'client_secret' => config('app.privy_secret_key'),
                'grant_type' => 'authorization_code',
                'code' => 'rl8GFuEfHDuR4VnpUktM',
                'redirect_uri' => 'staging.danasyariah.id'
            ]

        ]);

        $response = $request->getBody()->getContents();
        // die($response);
        $response_decode = json_decode($response);
    }


    public function generateTokenOauthPrivy()
    {
        $client = new Client();
        $request = $client->post(config('app.privy_oauth') . '/registration/status', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'multipart/form-data', //multipart/form-data //application/x-www-form-urlencoded
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key')
            ],
            'form_params' => [
                'token' => $token
            ]

        ]);

        $response = $request->getBody()->getContents();
        $response_decode = json_decode($response);
    }

    // ####################################################### END AKSES LOGIN PORTAL PRIVY ID  ####################################################### //

    // ####################################################### REGISTRASI  ####################################################### //

    // check registasi
    public function checkRegistrasi($user_id, $user_type)
    {

        if ($user_type == 1) {
            $column_user = "investor_id";
        } else {
            $column_user = "brw_id";
        }

        $checkTokenTable = CheckUserSign::where($column_user, $user_id)->first();

        $response = "";

        if ($checkTokenTable) {

            if ($checkTokenTable->userToken == "" || $checkTokenTable->userToken == null) {

                return response()->json([
                    'status' => 'belum_terdaftar'
                ]);

                //$response = $this->RegisterPrivyID($user_id, $user_type);

            } else {
                // jika user ada token atau sudah pernah registrasi namun menunggu verifikasi

                $response = $this->checkRegistrasiPrivy($checkTokenTable->userToken);
            }
        } else {

            return response()->json([
                'status' => 'belum_terdaftar'
            ]);
        }

        // jika user baru
        // else{

        //     $response = $this->RegisterPrivyID($user_id, $user_type);

        // }

        return $response;
    }

    // check registrasi user yg sudah berhasil registrasi dan mendapatkan token
    public function checkRegistrasiPrivy($token)
    {

        $client = new Client();
        // multipart body
        $multipart_form = [
            [
                'name'     => 'token',
                'contents' => $token
            ]
        ];

        $request = $client->post(config('app.privy_url') . '/registration/status', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'multipart/form-data', //multipart/form-data //application/x-www-form-urlencoded
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key')
            ],
            'form_params' => [
                'token' => $token
            ]

        ]);

        $response = $request->getBody()->getContents();
        $response_decode = json_decode($response);
        //dd($response_decode);

        // jika status user verified & registered maka update column status, privyID, userToken, tgl_aktifasi (Jika user terverifikasi)
        if ($response_decode->{'code'} == 201 && $response_decode->{'data'}->{'status'} == "verified" or $response_decode->{'data'}->{'status'} == "registered") {

            $date = \Carbon\Carbon::parse($response_decode->{'data'}->{'processedAt'});

            $updateStatusReg = CheckUserSign::where('userToken', $token)->update(
                [
                    'status' => $response_decode->{'data'}->{'status'},
                    'privyID' => $response_decode->{'data'}->{'privyId'},
                    'userToken' => $response_decode->{'data'}->{'userToken'},
                    'tgl_aktifasi' => $date->format('Y-m-d')
                ]
            );

            return $response;
        }

        // hanya update column status & user token
        else if ($response_decode->{'code'} == 201 && $response_decode->{'data'}->{'status'} == "rejected" or $response_decode->{'data'}->{'status'} == "invalid") {

            //$date = \Carbon\Carbon::parse($response_decode->{'data'}->{'processedAt'});

            $updateStatusReg = CheckUserSign::where('userToken', $token)->update(
                [
                    'status' => $response_decode->{'data'}->{'status'},

                ]
            );

            return $response;
        } else {
            return $response;
        }
    }

    // proses registrasi 
    public function RegisterPrivyID($user_id, $user_type)
    {

        $DataUsers = "";

        // get data investor & borrower

        if ($user_type == 1) { // user type 1 = investor & 2 = borrower

            $DataUsers = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
                ->where('investor.id', $user_id)
                ->first();
        } else {

            $DataUsers = BorrowerDetails::leftJoin('brw_user', 'brw_user.brw_id', '=', 'brw_user_detail.brw_id')
                ->where('brw_user.brw_id', $user_id)
                ->first();
        }

        // dd($DataUsers);

        $fotoDiri = $this->cekFotoDiriExist($user_id, $user_type);


        $fotoKtp = $this->cekFotoKtpExist($user_id, $user_type);

        $namaBorrower = "";
        $tglLahir = "";
        // if($user_type == 2 ){

        //     if($DataUsers->brw_type == 1){
        //         $namaBorrower = $DataUsers->nama;
        //     }else{
        //         $namaBorrower = $DataUsers->nm_bdn_hukum;
        //         $tglLahir = "";
        //     }

        // }

        // param identity investor & borrower

        if ($user_type == 1) {

            if ($DataUsers->warganegara != 0) {

                $identity = [

                    "nomorPassport" => !empty($DataUsers->no_passpor_investor) ? $DataUsers->no_passpor_investor : '',
                    "nama" => !empty($DataUsers->nama_investor) ? $DataUsers->nama_investor : '',


                ];


                // multipart body 
                $multipart_form =   [
                    [
                        'name' => 'email',
                        'contents' => $DataUsers->email
                    ],
                    [
                        'name' => 'phone',
                        'contents' => !empty($DataUsers->phone_investor) ? $DataUsers->phone_investor : $DataUsers->no_tlp
                    ],
                    [
                        'name' => 'selfie',
                        'contents' => $fotoDiri
                    ],
                    [
                        'name' => 'passport',
                        'contents' => $fotoKtp
                    ],
                    [
                        'name' => 'identity',
                        'contents' => json_encode($identity)
                    ],
                    [
                        'name' => 'citizenship',
                        'contents' => 'wna'
                    ]


                ];
            } else {
            	// badan hukum
            	if($DataUsers->tipe_pengguna == 2){
            		$DataUsers_pengurus = DB::SELECT("SELECT identitas_pengurus,nm_pengurus,tgl_lahir FROM investor_pengurus WHERE investor_id = " . $user_id . " AND jabatan = 2");

            		$no_ktp_investor = "";
					$nama_investor = "";
					$tgl_lahir_investor = "";
            		if(count($DataUsers_pengurus) > 0){
            			$no_ktp_investor = $DataUsers_pengurus[0]->identitas_pengurus;
            			$nama_investor = $DataUsers_pengurus[0]->nm_pengurus;
            			$tgl_lahir_investor = $DataUsers_pengurus[0]->tgl_lahir;
            		}

            		$DataUsers_kontak = DB::SELECT("SELECT no_tlp FROM investor_corp_contact WHERE investor_id = " . $user_id . " order by contact_id ASC");

            		$no_tlp="";
            		if(count($DataUsers_kontak) > 0){
            			$no_tlp = $DataUsers_kontak[0]->no_tlp;
            		}

            		$identity = [

		                "nik" => !empty($no_ktp_investor) ? $no_ktp_investor : "",
		                "nama" => !empty($nama_investor) ? $nama_investor : "",
		                "tanggalLahir" => !empty($tgl_lahir_investor) ? $tgl_lahir_investor : ""

		            ];


		            // multipart body 
		            $multipart_form =   [
		                [
		                    'name' => 'email',
		                    'contents' => $DataUsers->email
		                ],
		                [
		                    'name' => 'phone',
		                    'contents' => !empty($no_tlp) ? $no_tlp : ""
		                ],
		                [
		                    'name' => 'selfie',
		                    'contents' => $fotoDiri
		                ],
		                [
		                    'name' => 'ktp',
		                    'contents' => $fotoKtp
		                ],
		                [
		                    'name' => 'identity',
		                    'contents' => json_encode($identity)
		                ]


		            ];
            	}elseif($DataUsers->tipe_pengguna == 1){
            		// individu
            		$identity = [

	                    "nik" => !empty($DataUsers->no_ktp_investor) ? $DataUsers->no_ktp_investor : $DataUsers->ktp,
	                    "nama" => !empty($DataUsers->nama_investor) ? $DataUsers->nama_investor : $DataUsers->nama,
	                    "tanggalLahir" => !empty($DataUsers->tgl_lahir_investor) ? $DataUsers->tgl_lahir_investor : $DataUsers->tgl_lahir

	                ];


	                // multipart body 
	                $multipart_form =   [
	                    [
	                        'name' => 'email',
	                        'contents' => $DataUsers->email
	                    ],
	                    [
	                        'name' => 'phone',
	                        'contents' => !empty($DataUsers->phone_investor) ? $DataUsers->phone_investor : $DataUsers->no_tlp
	                    ],
	                    [
	                        'name' => 'selfie',
	                        'contents' => $fotoDiri
	                    ],
	                    [
	                        'name' => 'ktp',
	                        'contents' => $fotoKtp
	                    ],
	                    [
	                        'name' => 'identity',
	                        'contents' => json_encode($identity)
	                    ]


	                ];
            	}
                
            }
        } else {

            $identity = [

                "nik" => !empty($DataUsers->no_ktp_investor) ? $DataUsers->no_ktp_investor : $DataUsers->ktp,
                "nama" => !empty($DataUsers->nama_investor) ? $DataUsers->nama_investor : $DataUsers->nama,
                "tanggalLahir" => !empty($DataUsers->tgl_lahir_investor) ? $DataUsers->tgl_lahir_investor : $DataUsers->tgl_lahir

            ];


            // multipart body 
            $multipart_form =   [
                [
                    'name' => 'email',
                    'contents' => $DataUsers->email
                ],
                [
                    'name' => 'phone',
                    'contents' => !empty($DataUsers->no_tlp) ? $DataUsers->no_tlp : ''
                ],
                [
                    'name' => 'selfie',
                    'contents' => $fotoDiri
                ],
                [
                    'name' => 'ktp',
                    'contents' => $fotoKtp
                ],
                [
                    'name' => 'identity',
                    'contents' => json_encode($identity)
                ]


            ];
        }

        $client = new Client();

        $boundary = '----WebKitFormBoundary7MA4YWxkTrZu0gW';
        $request = $client->post(config('app.privy_url') . '/registration', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'multipart/form-data; ' . config('app.boundary'),
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key'),

            ],

            'body' => new \GuzzleHttp\Psr7\MultipartStream($multipart_form, $boundary),
        ]);



        $response = $request->getBody()->getContents();
        $response_decode = json_decode($response);


        if ($response_decode->{'code'} == "201") { // jika response sukses 

            $insertRegistrasi = new CheckUserSign;


            if ($user_type == 1) { // split column table cek_user_sign

                $insertRegistrasi->investor_id = $user_id;
            } else {

                $insertRegistrasi->brw_id = $user_id;
            }

            $insertRegistrasi->provider_id = 1;
            $insertRegistrasi->userToken = !empty($response_decode->{'data'}->{'userToken'}) ? $response_decode->{'data'}->{'userToken'} : 'null';
            $insertRegistrasi->privyId = !empty($response_decode->{'data'}->{'privyId'}) ? $response_decode->{'data'}->{'privyId'} : 'null';
            $insertRegistrasi->status = !empty($response_decode->{'data'}->{'status'}) ? $response_decode->{'data'}->{'status'} : 'null';
            $insertRegistrasi->link_aktifasi = '';
            $insertRegistrasi->tgl_register = date('Y-m-d');
            $insertRegistrasi->tgl_aktifasi = '';

            $insertRegistrasi->save();

            return $response;
        } else {

            return $response;
        }
    }


    // ####################################################### END REGISTRASI  ####################################################### //


    // ####################################################### GENERATE DOC TERM & CONDITION RDL ####################################################### //

    public function generateFormRDL($user_id)
    {

        if ($user_id) {
            $getDataInvestor = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('investor_id', $user_id)
                ->first();

            $getDataRekening = RekeningInvestor::where('investor_id', $user_id)->first();
            $getDataBank = MasterBank::where('kode_bank', $getDataInvestor->bank)->first();
            $getKota = MasterProvinsi::where('kode_kota', $getDataInvestor->kota_investor)->first();

            $Nomor_Form = $this->generateNoFormRDL();
            $Nama_Investor = !empty($getDataInvestor->nama_investor) ? $getDataInvestor->nama_investor : '-';
            $Nama_Jabatan = !empty($getDataInvestor->pekerjaan_investor) ? $getDataInvestor->pekerjaan_investor : '-';
            $Nama_Kota = !empty($getKota->nama_kota) ? $getKota->nama_kota : '-';
            $Nama_Alamat = !empty($getDataInvestor->alamat_investor) ? $getDataInvestor->alamat_investor : '-';
            $No_Tlp = !empty($getDataInvestor->phone_investor) ? $getDataInvestor->phone_investor : '0';
            $Email = !empty($getDataInvestor->email) ? $getDataInvestor->email : '-';
            $No_Rekening = !empty($getDataInvestor->rekening) ? $getDataInvestor->rekening : '0';
            $bank = !empty($getDataBank->nama_bank) ? $getDataBank->nama_bank : '';
            $Nama_Rekening = !empty($getDataInvestor->nama_pemilik_rek) ? $getDataInvestor->nama_pemilik_rek : '-';
            $Tgl_Sekarang = date("d-n-Y");
            $Tgl_Expired = date("d-n-Y");

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/public/akad_template/Form_Surat_Kuasa_Pernyataan_BNI_API_Corp_OGP.docx'));

            $templateProcessor->setValue(
                [
                    'Nomor_Form',
                    'Nama_Investor',
                    'Nama_Jabatan',
                    'Nama_Kota',
                    'Nama_Alamat',
                    'No_Rekening',
                    'Nama_Rekening',
                    'No_Tlp',
                    'Email',
                    'Tgl_Sekarang',
                    'Tgl_Expired'
                ],
                [
                    $Nomor_Form,
                    $Nama_Investor,
                    $Nama_Jabatan,
                    $Nama_Kota,
                    $Nama_Alamat,
                    $No_Rekening,
                    $Nama_Rekening,
                    $No_Tlp,
                    $Email,
                    $Tgl_Sekarang,
                    $Tgl_Expired
                ]
            );

            Storage::disk('public')->makeDirectory('akad_investor/' . $user_id);
            $templateProcessor->saveAs(storage_path('app/public/akad_investor/' . $user_id . '/Form_Surat_Kuasa_Pernyataan_BNI_API_Corp_OGP.docx'));

            shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_investor/' . $user_id . '/Form_Surat_Kuasa_Pernyataan_BNI_API_Corp_OGP.docx') . ' ' . base_path('storage/app/public/akad_investor/' . $user_id));

            return response()->json(['status' => 'Berhasil']);
        } else {
            return response()->json(['status' => 'Gagal']);
        }
    }

    // ####################################################### END GENERATE DOC TERM & CONDITION RDL ####################################################### //



    // ####################################################### WAKALAH BIL UJROH  ####################################################### //

    public function checkDocumentStatusWakalah($userID)
    {

        $dataLogAkad = LogAkadDigiSignInvestor::where(\DB::raw('substr(document_id, 1, 15)'), '=', 'investorKontrak')
            ->where('investor_id', $userID)
            ->whereIn('status', ['kirim', 'In Progress', 'Completed'])
            ->orderBy('id_log_akad_investor', 'desc')
            ->first();

        $getPrivy = CheckUserSign::where('investor_id', $userID)->first();
        if ($dataLogAkad) {
            if ($dataLogAkad->status == "kirim") {
                return response()->json(['status_doc' => 'ttd', 'privyID' => $getPrivy->privyID, 'doc_token' => $dataLogAkad->docToken, 'url_doc' => $dataLogAkad->urlDocument]);
            } else if ($dataLogAkad->status == "In Progress") {
                return response()->json(['status_doc' => 'ttd', 'privyID' => $getPrivy->privyID, 'doc_token' => $dataLogAkad->docToken, 'url_doc' => $dataLogAkad->urlDocument]);
            } else if ($dataLogAkad->status == "Completed") {
                return response()->json(['status_doc' => 'sukses', 'privyID' => $getPrivy->privyID, 'doc_token' => $dataLogAkad->docToken, 'url_doc' => $dataLogAkad->urlDocument]);
            }
        } else {

            return response()->json(['status_doc' => 'sukses', 'privyID' => $getPrivy->privyID, 'doc_token' => $dataLogAkad->docToken, 'url_doc' => $dataLogAkad->urlDocument]);
        }
    }

    public function checkFileAkad($user_id)
    {

        // if (!storage_path('app/public/akad_investor/'.$user_id.'/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAHad.pdf')) {
        if (Storage::disk('private')->exists("/akad_investor/" . $user_id . "/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.pdf")) {

            return response()->json(["status" => "file_ada"]);
        } else {

            return response()->json(["status" => "file_tidak_ada"]);
        }
    }
    public function createDocInvestorBorrower($userID, $idProyek)
    {

        if ($userID) {
            $getDataBorrower    =   BorrowerPendanaan::leftJoin('brw_user', 'brw_user.brw_id', '=', 'brw_pendanaan.brw_id')
                ->leftJoin('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_pendanaan.brw_id')
                ->where('brw_pendanaan.id_proyek', $idProyek)
                ->first();
            $id_borrower = $getDataBorrower !== null ? $getDataBorrower->brw_id : null;

            $getDataInvestor    = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('investor.id', $userID)
                ->first();
            $getRekening        = $id_borrower !== null ? BorrowerRekening::leftJoin("m_bank", "m_bank.kode_bank", "=", "brw_rekening.brw_kd_bank")->where('brw_rekening.brw_id', $id_borrower)->first() : null;

            $getPekerjaan = $getDataBorrower !== null ? MasterPekerjaan::where('id_pekerjaan', $getDataBorrower->pekerjaan)->first() : null;

            $getDataProyek = $idProyek !== null ? Proyek::where('id', $idProyek)->first() : null;
            $getDataPendanaanAktif = $getDataProyek !== null ? PendanaanAktif::where('proyek_id', $getDataProyek->id)->sum('total_dana') : null;
            $getDataBank = $getDataBorrower !== null ? MasterBank::where('kode_bank', $getRekening->brw_kd_bank)->first() : null;

            $getJaminan = $id_borrower !== null ? BorrowerJaminan::leftJoin('brw_pendanaan', 'brw_jaminan.pengajuan_id', '=', 'brw_pendanaan.pendanaan_id')
                ->leftJoin('m_jenis_jaminan', 'm_jenis_jaminan.id_jenis_jaminan', '=', 'brw_jaminan.jaminan_jenis')
                ->where('brw_pendanaan.brw_id', $id_borrower)
                ->first() : null;

            $getProyekInvestor = $idProyek !== null ? PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->leftJoin('m_no_akad_investor', 'm_no_akad_investor.investor_id', '=', 'pendanaan_aktif.investor_id')
                ->leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'pendanaan_aktif.investor_id')
                ->where('pendanaan_aktif.proyek_id', $idProyek)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana group by proyek_id')])
                ->get() : null;

            $getJumlahProyekInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $userID)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $userID . ' group by proyek_id')])
                ->count('pendanaan_aktif.id');

            $nama_investor = $getDataInvestor->nama_investor;
            $no_ktp_investor = $getDataInvestor->no_ktp_investor;
            $alamat_investor = $getDataInvestor->alamat_investor;
            $no_hp_investor = $getDataInvestor->phone_investor;

            $marginProyek = !empty($getDataProyek) ? number_format($getDataProyek->profit_margin, 0, '', '') : 0;
            $totalMargin = $marginProyek + 5;
            $perhitunganMargin = $totalMargin / 100 * $getDataPendanaanAktif;
            $totalHarga = $getDataPendanaanAktif + $perhitunganMargin;


            $nama = $getDataBorrower !== null ? $getDataBorrower->nama : '-';
            $nama_badan = $getDataBorrower !== null ? $getDataBorrower->nm_bdn_hukum : '-';
            $jabatan = $getDataBorrower !== null ? $getDataBorrower->jabatan : '-';
            $alamat = $getDataBorrower !== null ? $getDataBorrower->alamat : '-';

            $no_akta = '-';
            $tgl_akta = '-';
            $nama_notaris = '-';

            $Nomor_SP3 = '-';
            $Tanggal_SP3 = '-';
            $Nomor_Waad = '-';
            $Tanggal_Waad = '-';

            $Jenis_Obyek_Pembiayaan = $getDataProyek !== null ? $getDataProyek->nama : '';
            $Alamat_Proyek =  $getDataProyek !== null ? $getDataProyek->alamat : '-';
            $Jumlah_Plafond = $getRekening !== null ? $getRekening->total_plafon : 0;
            $Harga_Pokok = $getDataPendanaanAktif !== null ? $getDataPendanaanAktif : 0;
            $Jumlah_Uang_Muka = 0;
            $Jumlah_Margin_Pembiayaan = $perhitunganMargin;
            $Harga_Jual = $totalHarga;
            $Biaya_Administrasi = 0;
            $Jangka_Waktu_Pembiayaan = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;
            $Tanggal_Akad = Carbon::now()->format('d-m-Y');
            $Tanggal_Jatuh_Tempo = $getDataProyek !== null ? Carbon::parse($getDataProyek->tgl_selesai)->format('d-m-Y') : 0;
            $Jangka_Waktu = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;

            $Nominal_Angsuran = 0;

            $Jenis_Jaminan = $getJaminan !== null ? $getJaminan->jenis_jaminan : '-';
            $Alamat_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_detail : '-';
            $Pemilik_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nama : '-';
            $Nilai_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nilai : 0;

            $Nomor_Rekening = $getRekening !== null ? $getRekening->brw_norek : 0;

            $Bank_Rekening = $getRekening !== null ? $getRekening->nama_bank : '-';
            $Nama_Pemilik_Rekening = $getRekening !== null ? $getRekening->brw_nm_pemilik : '-';
            $Terbilang_Jangka_Waktu =  ucwords(Terbilang::make($Jangka_Waktu_Pembiayaan, ''));

            $totalProyek = !empty($getJumlahProyekInvestor) ? $getJumlahProyekInvestor : 0;

            $tgl_sekarang = date("d-n-Y");
            $data_tgl = !empty($tgl_sekarang) ? explode("-", $tgl_sekarang) : null;
            // tgl
            $cek_tgl = 0;
            if ($data_tgl !== null && $data_tgl !== '') {
                if ($data_tgl[0] !== null && $data_tgl[0] !== '') {
                    if (strlen($data_tgl[0])  == 2) {
                        if ($data_tgl[0][0] == 0) {
                            $cek_tgl = $data_tgl[0][1];
                        } else {
                            $cek_tgl = $data_tgl[0];
                        }
                    } else {
                        $cek_tgl = $data_tgl[0];
                    }
                } else {
                    $cek_tgl = 0;
                }
            } else {
                $cek_tgl = 0;
            }
            // end tgl
            // bulan
            $data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
            for ($x = 1; $x <= 12; $x++) {
                if ($x == $data_tgl[1]) {
                    $cek_bln = $data_bulan[$x - 1];
                }
            }
            // end bulan
            $tgl_bln_thn = $cek_tgl . ' ' . $cek_bln . ' ' . $data_tgl[2];
            $no_hari = date("N");

            switch ($no_hari) {
                case 1:
                    $hari_transaksi = 'Senin';
                    break;
                case 2:
                    $hari_transaksi = 'Selasa';
                    break;
                case 3:
                    $hari_transaksi = 'Rabu';
                    break;
                case 4:
                    $hari_transaksi = 'Kamis';
                    break;
                case 5:
                    $hari_transaksi = 'Jumat';
                    break;
                case 6:
                    $hari_transaksi = 'Sabtu';
                    break;
                case 7:
                    $hari_transaksi = 'Minggu';
                    break;
                default:
                    $hari_transaksi = 'Libur';
                    break;
            };

            $noAkad = $this->generateNoAkadBorrower($idProyek);

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/public/akad_template/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx'));

            $templateProcessor->setValue(
                [
                    'Nama_Investor',
                    'Nomor_KTP',
                    'Alamat_Investor',
                    'Nomor_HP_Investor',
                    'Nomor_Perjanjian',
                    'Hari_Sekarang',
                    'Tanggal_Sekang',
                    'Nama_Borrowers',
                    'Alamat_Borrowers',
                    'Nama_Direktur',
                    'Jabatan_Direktur',
                    'Nomor_Akta_Pendirian',
                    'Tanggal_Pendirian',
                    'Nama_Notaris',
                    'Nomor_SP3',
                    'Tanggal_SP3',
                    'Jenis_Obyek_Pembiayaan/Proyek',
                    'Harga_Pokok',
                    'Jumlah_Margin_Pembiayaan',
                    'Harga_Pengembalian',
                    'Jangka_Waktu_Pembiayaan',
                    'Tanggal_Hari_Ini',
                    'Tanggal_Jatuh_Tempo',
                    'Jumlah_Angsuran',
                    'Tanggal_Jatuh_Tempo_Angsuran',
                    'Jenis_Jaminan',
                    'Legalitas_Jaminan',
                    'Nama_Pemilik_Jaminan',
                    'Nilai_Jaminan',
                    'Nomor_Rekening',
                    'Bank_Transfer',
                    'Nama_Rekening',
                    'Jangka_Waktu',
                    'Terbilang_Jangka_Waktu',
                    'Nama_PT_Borrower'
                ],
                [
                    $nama_investor,
                    $no_ktp_investor,
                    $alamat_investor,
                    $no_hp_investor,
                    $noAkad,
                    $hari_transaksi,
                    $Tanggal_Akad,
                    $nama_badan,
                    $alamat,
                    $nama,
                    $jabatan,
                    $no_akta,
                    $tgl_akta,
                    $nama_notaris,
                    $Nomor_SP3,
                    $Tanggal_SP3,
                    $Jenis_Obyek_Pembiayaan,
                    $Harga_Pokok,
                    $Jumlah_Margin_Pembiayaan,
                    $Harga_Jual,
                    $Jangka_Waktu_Pembiayaan,
                    $Tanggal_Akad,
                    $Tanggal_Jatuh_Tempo,
                    $Jangka_Waktu_Pembiayaan,
                    '-',
                    $Jenis_Jaminan,
                    '-',
                    $Pemilik_Jaminan,
                    $Nilai_Jaminan,
                    $Nomor_Rekening,
                    $Bank_Rekening,
                    $Nama_Pemilik_Rekening,
                    $Jangka_Waktu_Pembiayaan,
                    $Terbilang_Jangka_Waktu,
                    $nama_badan
                ]
            );
            // $templateProcessor->setComplexBlock('Data_Tabel', $table);


            if ($id_borrower == null) {
                Storage::disk('public')->makeDirectory('akad_investor/' . $userID);

                $templateProcessor->saveAs(storage_path('app/public/akad_investor/' . $userID . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx'));
                shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_investor/' . $userID . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx') . ' ' . base_path('storage/app/public/akad_investor/' . $userID));
            } else {
                Storage::disk('public')->makeDirectory('akad_borrower/' . $id_borrower);

                $templateProcessor->saveAs(storage_path('app/public/akad_borrower/' . $id_borrower . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx'));
                shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_borrower/' . $id_borrower . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx') . ' ' . base_path('storage/app/public/akad_borrower/' . $id_borrower));
            }


            return ['status' => 'Berhasil'];
        } else {
            return ['status' => 'Gagal'];
        }
    }

    // create doc wakalah bil ujroh (INVESTOR & DSI)
    public function CreateDocAkadWakalahBilujrohInvestor($user_id)
    {
        if ($user_id) {

            $getDataInvestor = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('investor_id', $user_id)
                ->first();

            $callFuncMutasi  = DB::select("SELECT func_generate_serial_number('" . $user_id . "','proc:generate_serial_number','1434') as response");
            $riwayatMutasi   = DB::table("mutasi_investor")->where("investor_id", $user_id)->whereIn("perihal", ["Transfer Rekening", "Penarikan dana selesai"])->orderBy("id", "DESC")->first();

            // $mutasi_id       = $riwayatMutasi->id;
            // $responseMutasi  = $callFuncMutasi[0]->{'respopnse'};
            // $parsingFuncMutasiId = substr($responseMutasi, 0, 8);
            // $explodeMutasiId = explode(0, $parsingFuncMutasiId);

            $getDataRekening = RekeningInvestor::where('investor_id', $user_id)->first();
            $getDataBank = MasterBank::where('kode_bank', $getDataInvestor->bank_investor)->first();

            $getDataAhliWaris = AhliWarisInvestor::where('id_investor', $user_id)->first();
            $GetHubAw = DB::table("m_hub_ahli_waris")->where("id_hub_ahli_waris", !empty($getDataAhliWaris) ? $getDataAhliWaris->hubungan_keluarga_ahli_waris : '')->first(); // hubungan ahli waris
            $getProyekInvestor = PendanaanAktif::select('nama', 'tgl_mulai', 'tanggal_invest', 'tgl_selesai', 'total_dana', 'profit_margin')
                ->leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $user_id)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $user_id . ' group by proyek_id')])
                ->get();
            // dd($getProyekInvestor);

            $getNominalInvestasiInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $user_id)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $user_id . ' group by proyek_id')])
                ->sum('pendanaan_aktif.total_dana');

            $getJumlahProyekInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $user_id)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $user_id . ' group by proyek_id')])
                ->count('pendanaan_aktif.id');

            $nama_investor = $getDataInvestor->nama_investor;
            if ($getDataInvestor->jenis_identitas == 2)
                $no_ktp = $getDataInvestor->no_passpor_investor;
            else
                $no_ktp = $getDataInvestor->no_ktp_investor;
            $alamat = $getDataInvestor->alamat_investor;
            $username = $getDataInvestor->username;
            $no_hp = $getDataInvestor->phone_investor;
            $email = $getDataInvestor->email;

            // $tgl_invest = !empty($getDataRekening) ? Carbon::parse(explode(" ", $getDataRekening->updated_at)[0])->format('d F Y') : 0;
            $gettgl_invest = !empty($riwayatMutasi->created_at) ? Carbon::parse(explode(" ", $riwayatMutasi->created_at)[0])->format('d m Y') : 0;
            $total_aset = !empty($getDataRekening) ? $getDataRekening->total_dana : 0;
            $nominal_investasi = !empty($getDataRekening) ? number_format($getDataRekening->total_dana, 0, '', '.') : 0;
            $va = !empty($getDataRekening) ? $getDataRekening->va_number : 0;

            $rekening = $getDataInvestor->rekening;
            $bank = !empty($getDataBank->nama_bank) ? $getDataBank->nama_bank : '';
            $pemilik_rekening = $getDataInvestor->nama_pemilik_rek;
            $sebagai_kontak_lainnya = 'Ahli Waris';
            $nama_waris = !empty($getDataAhliWaris) ? $getDataAhliWaris->nama_ahli_waris : '-';
            $hub_keluarga_waris = !empty($GetHubAw) ? $GetHubAw->jenis_hubungan : '-';
            $no_ktp_waris = !empty($getDataAhliWaris) ? $getDataAhliWaris->nik_ahli_waris : '-';
            $no_hp_waris = !empty($getDataAhliWaris) ? $getDataAhliWaris->no_hp_ahli_waris : '-';
            $alamat_waris = !empty($getDataAhliWaris) ? $getDataAhliWaris->alamat_ahli_waris : '-';
            # tgl dari terakhir top up dana
            // $tgl_sekarang = Carbon::parse($riwayatMutasi->created_at)->format('d-m-Y');

            $tgl_sekarang = date("d-m-Y");
            $data_tgl = !empty($tgl_sekarang) ? explode("-", $tgl_sekarang) : null;

            // tgl
            $cek_tgl = 0;
            if ($data_tgl !== null && $data_tgl !== '') {
                if ($data_tgl[0] !== null && $data_tgl[0] !== '') {
                    if (strlen($data_tgl[0])  == 2) {
                        if ($data_tgl[0][0] == 0) {
                            $cek_tgl = $data_tgl[0][1];
                        } else {
                            $cek_tgl = $data_tgl[0];
                        }
                    } else {
                        $cek_tgl = $data_tgl[0];
                    }
                } else {
                    $cek_tgl = 0;
                }
            } else {
                $cek_tgl = 0;
            }

            // end tgl

            // bulan
            $getbulantgl_invest = explode(" ", $gettgl_invest);
            $data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
            for ($x = 1; $x <= 12; $x++) {
                if ($x == $data_tgl[1]) {
                    $cek_bln = $data_bulan[$x - 1];
                }
                if ($x == $getbulantgl_invest[1]) {
                    $bulantgl_invest = $data_bulan[$x - 1];
                }
            }
            // end bulan
            $tgl_bln_thn = $cek_tgl . ' ' . $cek_bln . ' ' . $data_tgl[2];
            $no_hari = date('N', strtotime($tgl_sekarang));
            $tgl_invest = $getbulantgl_invest[0] . ' ' . $bulantgl_invest . ' ' . $getbulantgl_invest[2];

            switch ($no_hari) {
                case 1:
                    $hari_transaksi = 'Senin';
                    break;
                case 2:
                    $hari_transaksi = 'Selasa';
                    break;
                case 3:
                    $hari_transaksi = 'Rabu';
                    break;
                case 4:
                    $hari_transaksi = 'Kamis';
                    break;
                case 5:
                    $hari_transaksi = 'Jumat';
                    break;
                case 6:
                    $hari_transaksi = 'Sabtu';
                    break;
                case 7:
                    $hari_transaksi = 'Minggu';
                    break;
                default:
                    $hari_transaksi = 'Libur';
                    break;
            };

            $noAkadInvestor = $this->generateNoAkadInvestor($user_id);
            $noKuasaInvestor = str_replace("AWBL", "Kuasa", $noAkadInvestor);

            $totalInvestasi = !empty($getNominalInvestasiInvestor) ? number_format($getNominalInvestasiInvestor, 0, '', '.') : 0;
            $totalProyek = !empty($getJumlahProyekInvestor) ? $getJumlahProyekInvestor : 0;

            $getvaBNI  = DB::select("SELECT va_number FROM rekening_investor WHERE investor_id = " . $user_id . " LIMIT 1");
            $getvaBSI  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $user_id . " AND kode_bank = '451' LIMIT 1");
            $getvaCIMB  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $user_id . " AND kode_bank = '022' LIMIT 1");

            $vaBNI = !empty($getvaBNI[0]->va_number) ? $getvaBNI[0]->va_number : '-';
            $vaBSI = !empty($getvaBSI[0]->va_number) ? '900' . $getvaBSI[0]->va_number : '-';
            $vaCIMB = !empty($getvaCIMB[0]->va_number) ? $getvaCIMB[0]->va_number : '-';

            if ((int) $getDataInvestor->tipe_pengguna === DetilInvestor::TYPE_CORPORATION) {
                $corp = InvestorService::getInvestorCorporation($user_id);
                $nama_investor = $corp->getDataSingle(InvestorCorporation::NAMA);
                $alamat_investor = $corp->getDataSingle(InvestorCorporation::ALAMAT);
            }

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/private/akad_template/WAKALAH_BIL_UJRAH_TEMPLATE-1.docx'));
            $templateProcessor->setValue(
                [
                    'nomor_surat_kuasa',
                    'nama_lender',
                    'nomor_identitas',
                    'alamat_lender',
                    'user_name',
                    'nomor_telepon',
                    'alamat_email',
                    'tanggal_perjanjian',
                    'hari_transaksi',
                    'tanggal_transaksi',
                    'nomor_akad',
                    'nominal_investasi',
                    'tanggal_transfer',
                    'rekening_imbal_hasil',
                    'nama_bank',
                    'nama_pemilik_rekening',
                    'sebagai_kontak_lainnya',
                    'nama_kontak_lainnya',
                    'hubungan_kontak_lainnya',
                    'nomor_ktp_kontak_lainnya',
                    'nomor_handphone_kontak_lainnya',
                    'alamat_kontak_lainnya',
                    'nomor_va_bni',
                    'nomor_va_bsi',
                    'nomor_va_cimb'
                ],
                [
                    $noKuasaInvestor,
                    $nama_investor,
                    $no_ktp,
                    $alamat,
                    $username,
                    $no_hp,
                    $email,
                    $tgl_bln_thn,
                    $hari_transaksi,
                    $tgl_bln_thn,
                    $noAkadInvestor,
                    'Rp' . $nominal_investasi,
                    $tgl_invest,
                    $rekening,
                    $bank,
                    $pemilik_rekening,
                    $sebagai_kontak_lainnya,
                    $nama_waris,
                    $hub_keluarga_waris,
                    $no_ktp_waris,
                    $no_hp_waris,
                    $alamat_waris,
                    $vaBNI,
                    $vaBSI,
                    $vaCIMB
                ]
            );

            $path = $getDataInvestor->pic_ktp_investor;
            $templateProcessor->setImageValue('logo_dsi', ['path' => public_path('img/logodsi.png'), 'width' => 200, 'height' => 80]);
            if ((int) $getDataInvestor->tipe_pengguna === DetilInvestor::TYPE_CORPORATION) {
                /** @var InvestorPengurus $pengurusPertama */
                $pengurusPertama = InvestorPengurus::query()
                    ->where('investor_id', $user_id)
                    ->orderBy('created_at')
                    ->first();

                $path = $pengurusPertama->getData([InvestorAdministrator::FOTO_KTP])[InvestorAdministrator::FOTO_KTP];
            }
            $fullPathKtp = storage_path('app/private/' . $path);
            $templateProcessor->setImageValue('foto_ktp_lender', ['path' => $fullPathKtp, 'width' => 700, 'height' => 400]);

            $values = [];
            for ($u = 0; $u < count($getProyekInvestor); $u++) {
                $list = array(
                    'no' => $u + 1,
                    'nama_proyek' => $getProyekInvestor[$u]['nama'],
                    'tanggal_mulai_proyek' => date('d-m-Y', strtotime($getProyekInvestor[$u]['tgl_mulai'])),
                    'tanggal_pendanaan' => date('d-m-Y', strtotime($getProyekInvestor[$u]['tanggal_invest'])),
                    'tanggal_selesai' => date('d-m-Y', strtotime($getProyekInvestor[$u]['tgl_selesai'])),
                    'total_dana' => "Rp. " . number_format($getProyekInvestor[$u]['total_dana']),
                    'imbal_hasil' => $getProyekInvestor[$u]['profit_margin'] . ' %'
                );
                array_push($values, $list);
            }
            $templateProcessor->cloneRowAndSetValues('no', $values);

            Storage::disk('private')->makeDirectory('akad_investor/' . $user_id);
            $templateProcessor->saveAs(storage_path('app/private/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.docx'));

            //Load temp file
            // $phpWord = \PhpOffice\PhpWord\IOFactory::load(storage_path('app/public/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.docx')); 

            // //Save it
            // $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord , 'PDF');
            // $xmlWriter->save(storage_path('app/public/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.pdf'));  

            shell_exec('unoconv -f pdf ' . base_path('storage/app/private/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.docx') . ' ' . base_path('storage/app/private/akad_investor/' . $user_id));
            return response()->json(['status' => 'Berhasil']);
        } else {
            return response()->json(['status' => 'Gagal']);
        }
    }

    //  kirim document akad wakalah bil ujroh (INVESTOR & DSI)

    public function sendDocAkadWakalahBilUjrohInvestor($user_id, $access_user)
    {


        $getDataInvestor = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
            ->where('investor.id', $user_id)
            ->first();
        // $getDataTaufiqSign = DetilInvestor::leftJoin('investor','investor.id','=','detil_investor.investor_id')
        // ->where('investor.email',config('app.email_pak_taufiq'))
        // ->first();

        $getDataRegistration = CheckUserSign::where('investor_id', $user_id)
            ->first();
        $getTotalAset = RekeningInvestor::where('investor_id', $user_id)->first();
        $riwayatMutasi   = DB::table("mutasi_investor")->where("investor_id", $user_id)->whereIn("perihal", ["Transfer Rekening", "Penarikan dana selesai"])->orderBy("id", "DESC")->first();
        $date = Carbon::now()->format('Ymd');
        $idInvestor = $getDataInvestor->id;
        $totalAset = !empty($getTotalAset) ? $getTotalAset->total_dana : 0;
        $getJumlahDoc = LogAkadDigiSignInvestor::where('investor_id', $idInvestor)
            ->count();
        $jumlahDocNext = !empty($getTotalAset) ? $getJumlahDoc + 1 : 1;
        $document_id = 'investorKontrak_' . $date . '_' . $idInvestor . '_' . $jumlahDocNext;



        $client = new Client();

        // buat document akad wakalah bil ujroh 
        if($getDataInvestor->tipe_pengguna == 1){
        	$this->createDocAkadWakalahBilUjrohInvestor($user_id);
        	$templateId = 'dsi001';
        	$signPage1 = 1;
        	$posX_signPage1 = 129;
        	$posY_signPage1 = 890;
        	$signPage2 = 10;
        	$posX_signPage2 = 129;
        	$posY_signPage2 = 125;
        }else{
        	$this->createDocAkadWakalahBilUjrohInvestor_badanhukum($user_id);
        	$templateId = 'dsi002';
        	$signPage1 = 1;
        	$posX_signPage1 = 129;
        	$posY_signPage1 = 900;
        	$signPage2 = 9;
        	$posX_signPage2 = 128;
        	$posY_signPage2 = 800;
        }

        $docPDF = fopen('../storage/app/private/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.pdf', 'r');

        $owner = [

            "privyId" => config('app.idPrivy'),
            "enterpriseToken" => config('app.privy_enterprise_token')

        ];

        // $recipients = [
        //     [
        //         // "email"=> $getDataTaufiqSign->email,
        //         "email" => config('app.email_pak_taufiq'),
        //         "type" => "Signer",
        //         "enterpriseToken" => config('app.privy_enterprise_token')
        //     ],
        //     [
        //         "email" => $getDataInvestor->email,
        //         "type" => "Signer",
        //         "enterpriseToken" => null
        //     ]
        // ];

        $recipients = [
            [
                "privyId" => config('app.idPrivy'),
                "type" => "Signer",
                "enterpriseToken" => config('app.privy_enterprise_token')
            ],
            [
                "privyId" => $getDataRegistration->privyID,
                "type" => 'Signer',
                "enterpriseToken" => '',
                "signature" => 2,
                "signMultiple" => 'true',
                "draggable" => 'true',
                "signCoordinates" => [
                    [
                        "signPage" => $signPage1,
                        "posX" => $posX_signPage1,
                        "posY" => $posY_signPage1,
                    ], [
                        "signPage" => $signPage2,
                        "posX" => $posX_signPage2,
                        "posY" => $posY_signPage2,
                    ]
                ]
            ]
        ];

        $multipart_form =   [
            [
                'name' => 'documentTitle',
                'contents' => 'PERJANJIAN PEMBIAYAAN WAKALAH BIL UJRAH'
            ],
            [
                'name' => 'docType',
                // 'contents' => 'Serial'
                'contents' => 'Parallel'
            ],
            [
                'name' => 'owner',
                'contents' => json_encode($owner)
            ],
            [
                'name' => 'document',
                'contents' => $docPDF
            ],
            [
                'name' => 'recipients',
                'contents' => json_encode($recipients)
            ],
            [
                'name' => 'templateId',
                'contents' => $templateId
            ]


        ];

        $boundary = '----WebKitFormBoundary7MA4YWxkTrZu0gW';
        $request = $client->post(config('app.privy_url') . '/document/upload', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'multipart/form-data; ' . config('app.boundary'), //multipart/form-data //application/x-www-form-urlencoded
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key'),

            ],
            'body' => new \GuzzleHttp\Psr7\MultipartStream($multipart_form, $boundary)
        ]);

        $response = $request->getBody()->getContents();
        DB::table("log_db_app")->insert(array(
            "file_name" => "PrivyController.php",
            "line" => "1497",
            "description" => "response = .$response.",
            "created_at" => date("Y-m-d H:i:s")
        ));
        $reponse_decode = json_decode($response);
        // var_dump([json_encode($recipients), $reponse_decode]);
        // die();

        $docToken = $reponse_decode->{'data'}->{'docToken'} == null ? '' : $reponse_decode->{'data'}->{'docToken'};
        $urlDocument = $reponse_decode->{'data'}->{'urlDocument'} == null ? '' : $reponse_decode->{'data'}->{'urlDocument'};

        $updateStatusLogDana = LogRekening::whereRaw("id = (select max(`id`) from log_rekening where investor_id = '$user_id')")->update(
            [
                'status_dana' => 1
            ]
        );

        // $insertLogAkadInvestor = DB::table("log_akad_wakalah_investor")->insert(array(
        // "investor_id" => $user_id,
        // "docToken" => $reponse_decode->{'data'}->{'docToken'},
        // "status" => 1, // 1 proses , 2 completed
        // "created_at" => date("Y-m-d H:i:s"), 
        // "updated_at" => date("Y-m-d H:i:s")

        // ));

        $this->logAkadDigiSignInvestor($user_id, 0, $riwayatMutasi->id, $access_user, $totalAset, 'kirim', $document_id, $docToken, $urlDocument, $downloadURL = "none");
        return $response;
    }


    // // untuk manggil url document utk tanda tangan
    // public function signDocAkadWakalahBilUjrohInvestor($userID, $token){

    //     $logAkadInvestor = LogAkadDigiSignInvestor::where('investor_id', $userID)->where('docToken', $token)->first();
    //     if(isset($logAkadInvestor)){
    //         return response()->json(["status_doc"=>"sukses_upload", "url_doc"=>$logAkadInvestor->urlDocument]);
    //     }else{
    //         return response()->json(["status_doc"=>"gagal_upload"]);
    //     }

    // }

    // ####################################################### END WAKALAH BIL UJROH  ####################################################### //

    // ####################################################### MUROBAHAH  ####################################################### //
    // check status document jika belum ttd
    public function checkDocumentStatus($userID)
    {

        //$cekDocument = LogAkadDigiSignInvestor::where('investor_id', $userID)->where('status', 'kirim')->first();
        $dataLogAkad = LogAkadDigiSignInvestor::where(\DB::raw('substr(document_id, 1, 15)'), '=', 'kontrakAll')
            ->where('investor_id', $userID)
            ->where('status', 'kirim')
            ->orderBy('id_log_akad_investor', 'desc')
            ->first();

        if (isset($cekDocument)) {

            $proyek = Proyek::where('id', $cekDocument->proyek_id)->first();
            return response()->json(['status_doc' => 'belum_ttd', 'nama_pendanaan' => $proyek->nama, 'doc_token' => $cekDocument->docToken, 'url_doc' => $cekDocument->urlDocument]);
        } else {

            return response()->json(['status_doc' => 'lanjut']);
        }
    }

    // Create Document Murobahah Investor Borrower
    public function createDocMurobahahInvestorBorrower($user_id, $id_proyek)
    {

        if ($user_id) {
            $getDataBorrower    =   BorrowerPendanaan::leftJoin('brw_user', 'brw_user.brw_id', '=', 'brw_pendanaan.brw_id')
                ->leftJoin('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_pendanaan.brw_id')
                ->where('brw_pendanaan.id_proyek', $id_proyek)
                ->first();
            $id_borrower = $getDataBorrower !== null ? $getDataBorrower->brw_id : null;

            $getDataInvestor    = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('investor.id', $user_id)
                ->first();
            $getRekening        = $id_borrower !== null ? BorrowerRekening::leftJoin("m_bank", "m_bank.kode_bank", "=", "brw_rekening.brw_kd_bank")->where('brw_rekening.brw_id', $id_borrower)->first() : null;

            $getPekerjaan = $getDataBorrower !== null ? MasterPekerjaan::where('id_pekerjaan', $getDataBorrower->pekerjaan)->first() : null;

            $getDataProyek = $id_proyek !== null ? Proyek::where('id', $id_proyek)->first() : null;
            $getDataPendanaanAktif = $getDataProyek !== null ? PendanaanAktif::where('proyek_id', $getDataProyek->id)->sum('total_dana') : null;
            $getDataBank = $getDataBorrower !== null ? MasterBank::where('kode_bank', $getRekening->brw_kd_bank)->first() : null;

            $getJaminan = $id_borrower !== null ? BorrowerJaminan::leftJoin('brw_pendanaan', 'brw_jaminan.pengajuan_id', '=', 'brw_pendanaan.pendanaan_id')
                ->leftJoin('m_jenis_jaminan', 'm_jenis_jaminan.id_jenis_jaminan', '=', 'brw_jaminan.jaminan_jenis')
                ->where('brw_pendanaan.brw_id', $id_borrower)
                ->first() : null;

            $getProyekInvestor = $id_proyek !== null ? PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->leftJoin('m_no_akad_investor', 'm_no_akad_investor.investor_id', '=', 'pendanaan_aktif.investor_id')
                ->leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'pendanaan_aktif.investor_id')
                ->where('pendanaan_aktif.proyek_id', $id_proyek)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana group by proyek_id')])
                ->get() : null;

            $getJumlahProyekInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $user_id)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $user_id . ' group by proyek_id')])
                ->count('pendanaan_aktif.id');

            $nama_investor = $getDataInvestor->nama_investor;
            $no_ktp_investor = $getDataInvestor->no_ktp_investor;
            $alamat_investor = $getDataInvestor->alamat_investor;
            $no_hp_investor = $getDataInvestor->phone_investor;

            $marginProyek = !empty($getDataProyek) ? number_format($getDataProyek->profit_margin, 0, '', '') : 0;
            $totalMargin = $marginProyek + 5;
            $perhitunganMargin = $totalMargin / 100 * $getDataPendanaanAktif;
            $totalHarga = $getDataPendanaanAktif + $perhitunganMargin;


            $nama = $getDataBorrower !== null ? $getDataBorrower->nama : '-';
            $nama_badan = $getDataBorrower !== null ? $getDataBorrower->nm_bdn_hukum : '-';
            $jabatan = $getDataBorrower !== null ? $getDataBorrower->jabatan : '-';
            $alamat = $getDataBorrower !== null ? $getDataBorrower->alamat : '-';

            $no_akta = '-';
            $tgl_akta = '-';
            $nama_notaris = '-';

            $Nomor_SP3 = '-';
            $Tanggal_SP3 = '-';
            $Nomor_Waad = '-';
            $Tanggal_Waad = '-';

            $Jenis_Obyek_Pembiayaan = $getDataProyek !== null ? $getDataProyek->nama : '';
            $Alamat_Proyek =  $getDataProyek !== null ? $getDataProyek->alamat : '-';
            $Jumlah_Plafond = $getRekening !== null ? $getRekening->total_plafon : 0;
            $Harga_Pokok = $getDataPendanaanAktif !== null ? $getDataPendanaanAktif : 0;
            $Jumlah_Uang_Muka = 0;
            $Jumlah_Margin_Pembiayaan = $perhitunganMargin;
            $Harga_Jual = $totalHarga;
            $Biaya_Administrasi = 0;
            $Jangka_Waktu_Pembiayaan = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;
            $Tanggal_Akad = Carbon::now()->format('d-m-Y');
            $Tanggal_Jatuh_Tempo = $getDataProyek !== null ? Carbon::parse($getDataProyek->tgl_selesai)->format('d-m-Y') : 0;
            $Jangka_Waktu = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;

            $Nominal_Angsuran = 0;

            $Jenis_Jaminan = $getJaminan !== null ? $getJaminan->jenis_jaminan : '-';
            $Alamat_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_detail : '-';
            $Pemilik_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nama : '-';
            $Nilai_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nilai : 0;

            $Nomor_Rekening = $getRekening !== null ? $getRekening->brw_norek : 0;

            $Bank_Rekening = $getRekening !== null ? $getRekening->nama_bank : '-';
            $Nama_Pemilik_Rekening = $getRekening !== null ? $getRekening->brw_nm_pemilik : '-';
            $Terbilang_Jangka_Waktu =  ucwords(Terbilang::make($Jangka_Waktu_Pembiayaan, ''));

            $totalProyek = !empty($getJumlahProyekInvestor) ? $getJumlahProyekInvestor : 0;

            $tgl_sekarang = date("d-n-Y");
            $data_tgl = !empty($tgl_sekarang) ? explode("-", $tgl_sekarang) : null;
            // tgl
            $cek_tgl = 0;
            if ($data_tgl !== null && $data_tgl !== '') {
                if ($data_tgl[0] !== null && $data_tgl[0] !== '') {
                    if (strlen($data_tgl[0])  == 2) {
                        if ($data_tgl[0][0] == 0) {
                            $cek_tgl = $data_tgl[0][1];
                        } else {
                            $cek_tgl = $data_tgl[0];
                        }
                    } else {
                        $cek_tgl = $data_tgl[0];
                    }
                } else {
                    $cek_tgl = 0;
                }
            } else {
                $cek_tgl = 0;
            }
            // end tgl
            // bulan
            $data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
            for ($x = 1; $x <= 12; $x++) {
                if ($x == $data_tgl[1]) {
                    $cek_bln = $data_bulan[$x - 1];
                }
            }
            // end bulan
            $tgl_bln_thn = $cek_tgl . ' ' . $cek_bln . ' ' . $data_tgl[2];
            $no_hari = date("N");

            switch ($no_hari) {
                case 1:
                    $hari_transaksi = 'Senin';
                    break;
                case 2:
                    $hari_transaksi = 'Selasa';
                    break;
                case 3:
                    $hari_transaksi = 'Rabu';
                    break;
                case 4:
                    $hari_transaksi = 'Kamis';
                    break;
                case 5:
                    $hari_transaksi = 'Jumat';
                    break;
                case 6:
                    $hari_transaksi = 'Sabtu';
                    break;
                case 7:
                    $hari_transaksi = 'Minggu';
                    break;
                default:
                    $hari_transaksi = 'Libur';
                    break;
            };

            $noAkad = $this->generateNoAkadBorrower($id_proyek);


            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/public/akad_template/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx'));

            $templateProcessor->setValue(
                [
                    'Nama_Investor',
                    'Nomor_KTP',
                    'Alamat_Investor',
                    'Nomor_HP_Investor',
                    'Nomor_Perjanjian',
                    'Hari_Sekarang',
                    'Tanggal_Sekang',
                    'Nama_Borrowers',
                    'Alamat_Borrowers',
                    'Nama_Direktur',
                    'Jabatan_Direktur',
                    'Nomor_Akta_Pendirian',
                    'Tanggal_Pendirian',
                    'Nama_Notaris',
                    'Nomor_SP3',
                    'Tanggal_SP3',
                    'Jenis_Obyek_Pembiayaan/Proyek',
                    'Harga_Pokok',
                    'Jumlah_Margin_Pembiayaan',
                    'Harga_Pengembalian',
                    'Jangka_Waktu_Pembiayaan',
                    'Tanggal_Hari_Ini',
                    'Tanggal_Jatuh_Tempo',
                    'Jumlah_Angsuran',
                    'Tanggal_Jatuh_Tempo_Angsuran',
                    'Jenis_Jaminan',
                    'Legalitas_Jaminan',
                    'Nama_Pemilik_Jaminan',
                    'Nilai_Jaminan',
                    'Nomor_Rekening',
                    'Bank_Transfer',
                    'Nama_Rekening',
                    'Jangka_Waktu',
                    'Terbilang_Jangka_Waktu',
                    'Nama_PT_Borrower'
                ],
                [
                    $nama_investor,
                    $no_ktp_investor,
                    $alamat_investor,
                    $no_hp_investor,
                    $noAkad,
                    $hari_transaksi,
                    $Tanggal_Akad,
                    $nama_badan,
                    $alamat,
                    $nama,
                    $jabatan,
                    $no_akta,
                    $tgl_akta,
                    $nama_notaris,
                    $Nomor_SP3,
                    $Tanggal_SP3,
                    $Jenis_Obyek_Pembiayaan,
                    $Harga_Pokok,
                    $Jumlah_Margin_Pembiayaan,
                    $Harga_Jual,
                    $Jangka_Waktu_Pembiayaan,
                    $Tanggal_Akad,
                    $Tanggal_Jatuh_Tempo,
                    $Jangka_Waktu_Pembiayaan,
                    '-',
                    $Jenis_Jaminan,
                    '-',
                    $Pemilik_Jaminan,
                    $Nilai_Jaminan,
                    $Nomor_Rekening,
                    $Bank_Rekening,
                    $Nama_Pemilik_Rekening,
                    $Jangka_Waktu_Pembiayaan,
                    $Terbilang_Jangka_Waktu,
                    $nama_badan
                ]
            );


            if ($id_borrower == null) {
                Storage::disk('public')->makeDirectory('akad_investor/' . $user_id);

                $templateProcessor->saveAs(storage_path('app/public/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx'));
                //exec('unoconv -f pdf '.base_path('storage/app/public/akad_investor/'.$user_id.'/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx').' '.base_path('storage/app/public/akad_investor/'.$user_id));
                shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx') . ' ' . base_path('storage/app/public/akad_investor/' . $user_id));
            } else {
                Storage::disk('public')->makeDirectory('akad_borrower/' . $id_borrower);

                $templateProcessor->saveAs(storage_path('app/public/akad_borrower/' . $id_borrower . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx'));
                //exec('unoconv -f pdf '.base_path('storage/app/public/akad_borrower/'.$id_borrower.'/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx').' '.base_path('storage/app/public/akad_borrower/'.$id_borrower));
                shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_borrower/' . $id_borrower . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.docx') . ' ' . base_path('storage/app/public/akad_borrower/' . $id_borrower));
            }


            return ['status' => 'Berhasil'];
        } else {
            return ['status' => 'Gagal'];
        }
    }


    // send document akad murobahah
    public function sendDocMurobahahInvestorBorrower($idProyek, $userID)
    {


        $getDataBorrower = DB::table('brw_pendanaan AS a')
            ->join('brw_user AS b', 'b.brw_id', '=', 'a.brw_id')
            ->select('b.brw_id AS idBorrower', 'b.email')
            ->where('a.id_proyek', $idProyek)
            ->first();

        $getDataInvestor = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
            ->where('investor.id', $userID)
            ->first();

        $getTotalPendanaan = Proyek::where('id', $idProyek)->first();

        $pendanaan = $getTotalPendanaan->total_need;
        $getTotalAset = RekeningInvestor::where('investor_id', $userID)->first();


        $date = Carbon::now()->format('Ymd');
        $idInvestor = $getDataInvestor->id;
        $idBorrower = !empty($getDataBorrower->idBorrower) ? $getDataBorrower->idBorrower : null;

        $totalAset = !empty($getTotalAset) ? $getTotalAset->total_dana : 0;
        $getJumlahDoc = LogAkadDigiSignBorrower::where('investor_id', $userID)
            ->count();
        $jumlahDocNext = $getJumlahDoc + 1;
        $document_id = 'kontrakAll_' . $date . '_' . $idProyek . '_' . $jumlahDocNext;



        $this->createDocMurobahahInvestorBorrower($userID, $idProyek);

        // handle jika user OJK memilih proyek. proyek yang dibuat oleh admin
        if ($idBorrower == null) {
            $docPDF = fopen('../storage/app/public/akad_investor/' . $idInvestor . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.pdf', 'r');
        } else {
            $docPDF = fopen('../storage/app/public/akad_borrower/' . $idBorrower . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_revisi.pdf', 'r');
        }


        // document owner
        $owner = [

            "privyId" => 'DEVRA4440',
            "enterpriseToken" => config('app.privy_enterprise_token')

        ];

        // jika borrower kosong
        if ($idBorrower == null || $idBorrower = "") {

            $recipients = [
                [
                    "email" => $getDataInvestor->email,
                    "type" => "Signer",
                    "enterpriseToken" => null
                ],
            ];
        } else {

            $recipients = [
                [
                    "email" => $getDataInvestor->email,
                    "type" => "Signer",
                    "enterpriseToken" => null
                ],
                [
                    "email" => $getDataBorrower->email,
                    "type" => "Signer",
                    "enterpriseToken" => null
                ]
            ];
        }


        $multipart_form =   [
            [
                'name' => 'documentTitle',
                'contents' => 'PERJANJIAN PEMBIAYAAN MURABAHAH'
            ],
            [
                'name' => 'docType',
                'contents' => 'Parallel'
            ],
            [
                'name' => 'owner',
                'contents' => json_encode($owner)
            ],
            [
                'name' => 'document',
                'contents' => $docPDF
            ],
            [
                'name' => 'recipients',
                'contents' => json_encode($recipients)
            ]


        ];

        $client = new Client();
        $boundary = '----WebKitFormBoundary7MA4YWxkTrZu0gW';
        $request = $client->post(config('app.privy_url') . '/document/upload', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'multipart/form-data; ' . config('app.boundary'), //multipart/form-data //application/x-www-form-urlencoded
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key'),

            ],
            'body' => new \GuzzleHttp\Psr7\MultipartStream($multipart_form, $boundary)
        ]);

        $response = $request->getBody()->getContents();
        $response_decode = json_decode($response);


        $docToken = "";
        $urlDocument = "";
        $downloadURL = "";
        $status = "";

        if ($response_decode->{'code'} == 201) {

            $docToken = $response_decode->{'data'}->{'docToken'};
            $urlDocument = $response_decode->{'data'}->{'urlDocument'};
            $downloadURL = '-';
            $status = "kirim";
        } else {
            $docToken = "";
            $urlDocument = "";
            $downloadURL = "";
            $status = "gagal";
        }



        $this->logAkadDigiSignInvestor($userID, $idProyek, 1, $totalAset, $status, $document_id, $docToken, $urlDocument, $downloadURL);

        // jika proyek berasal dari borrower

        if ($getDataBorrower->idBorrower == "" || $getDataBorrower->idBorrower == null) {
        } else {
            $this->logAkadDigiSignBorrower($getDataBorrower->idBorrower, $userID, $idProyek, 1, $pendanaan, $status, $document_id, $docToken, $urlDocument, $downloadURL);
        }


        return $response;
    }

    // create document murabahah DSI & Borrower Individu
    public function createDocMurobahahBorrowerIndividu($userID, $idProyek)
    {

        if ($userID) {
            $getDataBorrower    = Borrower::Join('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                ->where('brw_user.brw_id', $userID)
                ->first();
            // $getDataPendanaan   = BorrowerPendanaan::leftJoin('brw_tipe_pendanaan','brw_tipe_pendanaan.tipe_id','=','brw_pendanaan.pendanaan_tipe')
            //                                         ->where('brw_pendanaan.brw_id',$userID)
            //                                         ->first(['brw_tipe_pendanaan.pendanaan_nama','brw_pendanaan.*']);
            $getRekening        = BorrowerRekening::leftJoin("m_bank", "m_bank.kode_bank", "=", "brw_rekening.brw_kd_bank")->where('brw_rekening.brw_id', $userID)->first();

            //dd($getDataBorrower);
            $getPekerjaan = MasterPekerjaan::where('id_pekerjaan', $getDataBorrower->pekerjaan)->first();

            $getDataProyek = $idProyek !== null ? Proyek::where('id', $idProyek)->first() : null;
            $getDataPendanaanAktif = $getDataProyek !== null ? PendanaanAktif::where('proyek_id', $getDataProyek->id)->sum('total_dana') : null;
            $getDataBank = MasterBank::where('kode_bank', $getRekening->brw_kd_bank)->first();

            $getJaminan = BorrowerJaminan::leftJoin('brw_pendanaan', 'brw_jaminan.pengajuan_id', '=', 'brw_pendanaan.pendanaan_id')
                ->leftJoin('m_jenis_jaminan', 'm_jenis_jaminan.id_jenis_jaminan', '=', 'brw_jaminan.jaminan_jenis')
                ->where('brw_pendanaan.brw_id', $userID)
                ->first();

            $marginProyek = !empty($getDataProyek) ? number_format($getDataProyek->profit_margin, 0, '', '') : 0;
            $totalMargin = $marginProyek + 5;
            $perhitunganMargin = $totalMargin / 100 * $getDataPendanaanAktif;
            $totalHarga = $getDataPendanaanAktif + $perhitunganMargin;
            //echo json_encode($getJenis_Bukti_Kepemilikan_Jaminan);die;
            //$getJenis_Bukti_Kepemilikan_Jaminan= MasterJenisJaminan::where('jaminan_jenis',$getDataPendanaan->jaminan_id)->first();



            $nama = !empty($getDataBorrower->nama) ? $getDataBorrower->nama : '-';
            $no_ktp = !empty($getDataBorrower->ktp) ? $getDataBorrower->ktp : '-';
            $alamat = !empty($getDataBorrower->alamat) ? $getDataBorrower->alamat : '-';
            $pekerjaan = !empty($getTotalAset) ? $getPekerjaan->pekerjaan : '';

            $Nomor_SP3 = '-';
            $Tanggal_SP3 = '-';
            $Nomor_Waad = '-';
            $Tanggal_Waad = '-';

            $Jenis_Obyek_Pembiayaan = !empty($getDataProyek) ? $getDataProyek->nama : '-';
            $Alamat_Proyek =  $getDataProyek !== null ? $getDataProyek->alamat : '-';
            $Jumlah_Plafond = $getRekening !== null ? $getRekening->total_plafon : 0;
            $Harga_Pokok = $getDataPendanaanAktif !== null ? $getDataPendanaanAktif : 0;
            $Jumlah_Uang_Muka = 0;
            $Jumlah_Margin_Pembiayaan = $perhitunganMargin;
            $Harga_Jual = $totalHarga;
            $Biaya_Administrasi = 0;
            $Jangka_Waktu_Pembiayaan =  $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;
            $Tanggal_Akad = Carbon::now()->format('d-m-Y');
            $Tanggal_Jatuh_Tempo = $getDataProyek !== null ? Carbon::parse($getDataProyek->tgl_selesai)->format('d-m-Y') : 0;
            $Jangka_Waktu = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;

            $Nominal_Angsuran = 0;

            // $Tanggal_Saja_Jatuh_Tempo = substr($Tanggal_Jatuh_Tempo,0,2);
            $Jenis_Jaminan = $getJaminan !== null ? $getJaminan->jenis_jaminan : '-';
            $Alamat_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_detail : '-';
            $Pemilik_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nama : '-';
            $Nilai_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nilai : 0;

            $Nomor_Rekening = $getRekening->brw_norek;

            $Bank_Rekening = $getRekening->nama_bank;
            $Nama_Pemilik_Rekening = $getRekening->brw_nm_pemilik;
            //$terbilang =  ucwords(Terbilang::make($Jangka_Waktu_Pembiayaan,''));

            $tgl_sekarang = date("d-n-Y");
            $data_tgl = !empty($tgl_sekarang) ? explode("-", $tgl_sekarang) : null;
            // tgl
            $cek_tgl = 0;
            if ($data_tgl !== null && $data_tgl !== '') {
                if ($data_tgl[0] !== null && $data_tgl[0] !== '') {
                    if (strlen($data_tgl[0])  == 2) {
                        if ($data_tgl[0][0] == 0) {
                            $cek_tgl = $data_tgl[0][1];
                        } else {
                            $cek_tgl = $data_tgl[0];
                        }
                    } else {
                        $cek_tgl = $data_tgl[0];
                    }
                } else {
                    $cek_tgl = 0;
                }
            } else {
                $cek_tgl = 0;
            }
            // end tgl
            // bulan
            $data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
            for ($x = 1; $x <= 12; $x++) {
                if ($x == $data_tgl[1]) {
                    $cek_bln = $data_bulan[$x - 1];
                }
            }
            // end bulan
            $tgl_bln_thn = $cek_tgl . ' ' . $cek_bln . ' ' . $data_tgl[2];

            $noAkad = $this->generateNoAkadBorrower($idProyek);

            $phpWordObj = new PhpWord();
            $section = $phpWordObj->addSection();

            $fontStyle = array('bold' => true, 'align' => 'center');
            $table = $section->addTable();
            for ($r = 1; $r <= 8; $r++) {
                if ($r == 1) {
                    $table->addRow();
                    $table->addCell(1750)->addText("No", $fontStyle);
                    $table->addCell(1750)->addText("Nama", $fontStyle);
                    $table->addCell(1750)->addText("Alamat", $fontStyle);
                    $table->addCell(1750)->addText("No Telp", $fontStyle);
                    $table->addCell(1750)->addText("Keterangan", $fontStyle);
                } else {
                    $table->addRow();
                    for ($c = 1; $c <= 5; $c++) {
                        $table->addCell(1750)->addText("tes");
                    }
                }
            }

            // Create writer to convert document to xml
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWordObj, 'Word2007');

            // Get all document xml code
            $fullxml = $objWriter->getWriterPart('Document')->write();

            // Get only table xml code
            $tablexml = preg_replace('/^[\s\S]*(<w:tbl\b.*<\/w:tbl>).*/', '$1', $fullxml);

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/public/akad_template/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERORANGAN.docx'));

            $templateProcessor->setValue(
                [
                    'Nama_Borrowers',
                    'Nomor_Perjanjian',
                    'Nomor_KTP_Borrowers',
                    'Alamat_Borrowers',
                    'Pekerjaan_Borrowers',
                    'Nomor_SP3',
                    'Tanggal_SP3',
                    'Nomor_Waad',
                    'Tanggal_Waad',
                    'Jenis_Obyek_Pembiayaan/Proyek',
                    'Alamat_Proyek',
                    'Jumlah_Plafond',
                    'Harga_Pokok',
                    'Jumlah_Uang_Muka',
                    'Jumlah_Margin_Pembiayaan',
                    'Harga_Jual',
                    'Biaya_Administrasi',
                    'Jangka_Waktu_Pembiayaan',
                    'Jangka_Waktu',
                    'Tanggal_Akad',
                    'Tanggal_Jatuh_Tempo',
                    'Jangka_Waktu_Pembiayaan',
                    'Nominal_Angsuran',
                    'Jenis_Jaminan',
                    'Alamat_Jaminan',
                    'Pemilik_Jaminan',
                    'Nilai_Jaminan',
                    'Nomor_Rekening',
                    'Bank_Rekening',
                    'Nama_Pemilik_Rekening',
                    'terbilang',
                    'Tanggal_Bulan_Tahun'
                ],
                [
                    $nama,
                    $noAkad,
                    $no_ktp,
                    $alamat,
                    $pekerjaan,
                    $Nomor_SP3,
                    $Tanggal_SP3,
                    $Nomor_Waad,
                    $Tanggal_Waad,
                    $Jenis_Obyek_Pembiayaan,
                    $Alamat_Proyek,
                    $Jumlah_Plafond,
                    $Harga_Pokok,
                    $Jumlah_Uang_Muka,
                    $Jumlah_Margin_Pembiayaan,
                    $Harga_Jual,
                    $Biaya_Administrasi,
                    $Jangka_Waktu_Pembiayaan,
                    $Jangka_Waktu,
                    $Tanggal_Akad,
                    $Tanggal_Jatuh_Tempo,
                    $Jangka_Waktu_Pembiayaan,
                    $Nominal_Angsuran,
                    $Jenis_Jaminan,
                    $Alamat_Jaminan,
                    $Pemilik_Jaminan,
                    $Nilai_Jaminan,
                    $Nomor_Rekening,
                    $Bank_Rekening,
                    $Nama_Pemilik_Rekening,
                    '0',
                    $tgl_bln_thn
                ]
            );

            Storage::disk('public')->makeDirectory('akad_borrower/' . $userID);

            $templateProcessor->saveAs(storage_path('app/public/akad_borrower/' . $userID . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERORANGAN.docx'));
            shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_borrower/' . $userID . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERORANGAN.docx') . ' ' . base_path('storage/app/public/akad_borrower/' . $userID));

            return ['status' => 'Berhasil'];
        } else {
            return ['status' => 'Gagal'];
        }
    }

    // createDoc Murabahah DSI & BORROWER PERUSAHAAN
    public function createDocMurobahahBorrowerPerusahaan($userID, $idProyek)
    {
        if ($userID) {
            $getDataBorrower    = Borrower::leftJoin('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                ->where('brw_user.brw_id', $userID)
                ->first();
            // $getDataPendanaan   = BorrowerPendanaan::leftJoin('brw_tipe_pendanaan','brw_tipe_pendanaan.tipe_id','=','brw_pendanaan.pendanaan_tipe')
            //                                         ->where('brw_pendanaan.brw_id',$userID)
            //                                         ->first(['brw_tipe_pendanaan.pendanaan_nama','brw_pendanaan.*']);
            $getRekening        = BorrowerRekening::leftJoin("m_bank", "m_bank.kode_bank", "=", "brw_rekening.brw_kd_bank")->where('brw_rekening.brw_id', $userID)->first();

            $getPekerjaan = MasterPekerjaan::where('id_pekerjaan', $getDataBorrower->pekerjaan)->first();

            $getDataProyek = $idProyek !== null ? Proyek::where('id', $idProyek)->first() : null;
            $getDataPendanaanAktif = $getDataProyek !== null ? PendanaanAktif::where('proyek_id', $getDataProyek->id)->sum('total_dana') : null;
            $getDataBank = MasterBank::where('kode_bank', $getRekening->brw_kd_bank)->first();

            $getJaminan = BorrowerJaminan::leftJoin('brw_pendanaan', 'brw_jaminan.pengajuan_id', '=', 'brw_pendanaan.pendanaan_id')
                ->leftJoin('m_jenis_jaminan', 'm_jenis_jaminan.id_jenis_jaminan', '=', 'brw_jaminan.jaminan_jenis')
                ->where('brw_pendanaan.brw_id', $userID)
                ->first();

            $getProyekInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->leftJoin('m_no_akad_investor', 'm_no_akad_investor.investor_id', '=', 'pendanaan_aktif.investor_id')
                ->leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'pendanaan_aktif.investor_id')
                ->where('pendanaan_aktif.proyek_id', $idProyek)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana group by proyek_id')])
                ->get();
            $getJumlahProyekInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $userID)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $userID . ' group by proyek_id')])
                ->count('pendanaan_aktif.id');

            $marginProyek = !empty($getDataProyek) ? number_format($getDataProyek->profit_margin, 0, '', '') : 0;
            $totalMargin = $marginProyek + 5;
            $perhitunganMargin = $totalMargin / 100 * $getDataPendanaanAktif;
            $totalHarga = $getDataPendanaanAktif + $perhitunganMargin;
            //echo json_encode($getJenis_Bukti_Kepemilikan_Jaminan);die;
            //$getJenis_Bukti_Kepemilikan_Jaminan= MasterJenisJaminan::where('jaminan_jenis',$getDataPendanaan->jaminan_id)->first();


            $nama = $getDataBorrower->nama;
            $nama_badan = $getDataBorrower->nm_bdn_hukum;
            $jabatan = $getDataBorrower->jabatan;
            $alamat = $getDataBorrower->alamat;

            $no_akta = '-';
            $tgl_akta = '-';
            $nama_notaris = '-';

            $Nomor_SP3 = '-';
            $Tanggal_SP3 = '-';
            $Nomor_Waad = '-';
            $Tanggal_Waad = '-';

            $Jenis_Obyek_Pembiayaan = $getDataProyek !== null ? $getDataProyek->nama : '';
            $Alamat_Proyek =  $getDataProyek !== null ? $getDataProyek->alamat : '-';
            $Jumlah_Plafond = $getRekening !== null ? $getRekening->total_plafon : 0;
            $Harga_Pokok = $getDataPendanaanAktif !== null ? $getDataPendanaanAktif : 0;
            $Jumlah_Uang_Muka = 0;
            $Jumlah_Margin_Pembiayaan = $perhitunganMargin;
            $Harga_Jual = $totalHarga;
            $Biaya_Administrasi = 0;
            $Jangka_Waktu_Pembiayaan = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;
            $Tanggal_Akad = Carbon::now()->format('d-m-Y');
            $Tanggal_Jatuh_Tempo = $getDataProyek !== null ? Carbon::parse($getDataProyek->tgl_selesai)->format('d-m-Y') : 0;
            $Jangka_Waktu = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;

            $Nominal_Angsuran = 0;

            // $Tanggal_Saja_Jatuh_Tempo = substr($Tanggal_Jatuh_Tempo,0,2);
            $Jenis_Jaminan = $getJaminan !== null ? $getJaminan->jenis_jaminan : '-';
            $Alamat_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_detail : '-';
            $Pemilik_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nama : '-';
            $Nilai_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nilai : 0;

            $Nomor_Rekening = $getRekening->brw_norek;

            $Bank_Rekening = $getRekening->nama_bank;
            $Nama_Pemilik_Rekening = $getRekening->brw_nm_pemilik;
            $Terbilang_Jangka_Waktu =  ucwords(Terbilang::make($Jangka_Waktu_Pembiayaan, ''));

            $totalProyek = !empty($getJumlahProyekInvestor) ? $getJumlahProyekInvestor : 0;

            $tgl_sekarang = date("d-n-Y");
            $data_tgl = !empty($tgl_sekarang) ? explode("-", $tgl_sekarang) : null;
            // tgl
            $cek_tgl = 0;
            if ($data_tgl !== null && $data_tgl !== '') {
                if ($data_tgl[0] !== null && $data_tgl[0] !== '') {
                    if (strlen($data_tgl[0])  == 2) {
                        if ($data_tgl[0][0] == 0) {
                            $cek_tgl = $data_tgl[0][1];
                        } else {
                            $cek_tgl = $data_tgl[0];
                        }
                    } else {
                        $cek_tgl = $data_tgl[0];
                    }
                } else {
                    $cek_tgl = 0;
                }
            } else {
                $cek_tgl = 0;
            }
            // end tgl
            // bulan
            $data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
            for ($x = 1; $x <= 12; $x++) {
                if ($x == $data_tgl[1]) {
                    $cek_bln = $data_bulan[$x - 1];
                }
            }
            // end bulan
            $tgl_bln_thn = $cek_tgl . ' ' . $cek_bln . ' ' . $data_tgl[2];
            $no_hari = date("N");

            switch ($no_hari) {
                case 1:
                    $hari_transaksi = 'Senin';
                    break;
                case 2:
                    $hari_transaksi = 'Selasa';
                    break;
                case 3:
                    $hari_transaksi = 'Rabu';
                    break;
                case 4:
                    $hari_transaksi = 'Kamis';
                    break;
                case 5:
                    $hari_transaksi = 'Jumat';
                    break;
                case 6:
                    $hari_transaksi = 'Sabtu';
                    break;
                case 7:
                    $hari_transaksi = 'Minggu';
                    break;
                default:
                    $hari_transaksi = 'Libur';
                    break;
            };

            $noAkad = $this->generateNoAkadBorrower($idProyek);

            $phpWordObj = new PhpWord();
            $section = $phpWordObj->addSection();

            $fontStyle = [
                'bold' => true,
                'align' => 'center'
            ];

            $borderStyle = [
                'borderTopColor' => 'ff0000',
                'borderTopSize' => 6,
                'borderRightColor' => 'ff0000',
                'borderRightSize' => 6,
                'borderBottomColor' => 'ff0000',
                'borderBottomSize' => 6,
                'borderLeftColor' => 'ff0000',
                'borderLeftSize' => 6,
            ];

            $table = $section->addTable();
            $table->addRow();
            $table->addCell(700, $borderStyle)->addText("No", $fontStyle);
            $table->addCell(1100, $borderStyle)->addText("Nama Pemberi Pembiayaan", $fontStyle);
            $table->addCell(1750, $borderStyle)->addText("Jumlah Dana yang Dibiayai", $fontStyle);
            $table->addCell(1600, $borderStyle)->addText("Margin Keuntungan", $fontStyle);
            $table->addCell(1600, $borderStyle)->addText("No Proyek yang Dibiayai", $fontStyle);
            $table->addCell(1300, $borderStyle)->addText("No Surat Kuasa", $fontStyle);
            for ($r = 0; $r < $totalProyek; $r++) {

                $table->addRow();
                $table->addCell(700, $borderStyle)->addText($r + 1);
                $table->addCell(1100, $borderStyle)->addText($getProyekInvestor[$r]['nama_investor']);
                $table->addCell(1750, $borderStyle)->addText(number_format($getProyekInvestor[$r]['total_dana'], 0, '', '.'));
                $table->addCell(1600, $borderStyle)->addText(number_format($getProyekInvestor[$r]['profit_magin'], 0, '', '') . '%');
                $table->addCell(1600, $borderStyle)->addText(explode(' ', $getProyekInvestor[$r]['nama'])[1]);
                $table->addCell(1300, $borderStyle)->addText($getProyekInvestor[$r]['no_akad_inv'] . '/DSI/AWBL/' . $getProyekInvestor[$r]['bln_akad_inv'] . '/' . $getProyekInvestor[$r]['thn_akad_inv']);
            }

            // Create writer to convert document to xml
            // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWordObj, 'Word2007');

            // Get all document xml code
            // $fullxml = $objWriter->getWriterPart('Document')->write();

            // Get only table xml code
            // $tablexml = preg_replace('/^[\s\S]*(<w:tbl\b.*<\/w:tbl>).*/', '$1', $fullxml);

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/public/akad_template/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERUSAHAAN.docx'));

            $templateProcessor->setValue(
                [
                    'Nama_Borrowers',
                    'Nomor_Perjanjian',
                    'Hari_Sekarang',
                    'Tanggal_Sekarang',
                    'Alamat_Borrowers',
                    'Nama_Direktur',
                    'Jabatan_Direktur',
                    'Nomor_Akta_Pendirian',
                    'Tanggal_Pendirian',
                    'Nama_Notaris',
                    'Nomor_SP3',
                    'Tanggal_SP3',
                    // 'Nomor_Waad',
                    // 'Tanggal_Waad',
                    'Jenis_Obyek_Pembiayaan/Proyek',
                    // 'Alamat_Proyek',
                    // 'Jumlah_Plafond',
                    'Harga_Pokok',
                    // 'Jumlah_Uang_Muka',
                    'Jumlah_Margin_Pembiayaan',
                    'Harga_Pengembalian',
                    // 'Biaya_Administrasi',
                    'Jangka_Waktu_Pembiayaan',
                    // 'Jangka_Waktu',
                    'Tanggal_Hari_Ini',
                    'Tanggal_Jatuh_Tempo',
                    // 'Jangka_Waktu_Pembiayaan',
                    'Jumlah_Angsuran',
                    // 'Nominal_Angsuran',
                    'Tanggal_Jatuh_Tempo_Angsuran',
                    'Jenis_Jaminan',
                    'Legalitas_Jaminan',
                    // 'Alamat_Jaminan',
                    'Nama_Pemilik_Jaminan',
                    'Nilai_Jaminan',
                    'Nomor_Rekening',
                    'Bank_Transfer',
                    'Nama_Rekening',
                    'Jangka_Waktu',
                    'Terbilang_Jangka_Waktu',
                    'Nama_PT_Borrower'
                    // 'Tanggal_Bulan_Tahun'
                ],
                [
                    $nama_badan,
                    $noAkad,
                    $hari_transaksi,
                    $Tanggal_Akad,
                    $alamat,
                    $nama,
                    $jabatan,
                    $no_akta,
                    $tgl_akta,
                    $nama_notaris,
                    $Nomor_SP3,
                    $Tanggal_SP3,
                    // $Nomor_Waad,
                    // $Tanggal_Waad,
                    $Jenis_Obyek_Pembiayaan,
                    // $Alamat_Proyek,
                    // $Jumlah_Plafond,
                    $Harga_Pokok,
                    // $Jumlah_Uang_Muka,
                    $Jumlah_Margin_Pembiayaan,
                    $Harga_Jual,
                    // $Biaya_Administrasi,
                    $Jangka_Waktu_Pembiayaan,
                    // $Jangka_Waktu,
                    $Tanggal_Akad,
                    $Tanggal_Jatuh_Tempo,
                    // $Jangka_Waktu_Pembiayaan,
                    $Jangka_Waktu_Pembiayaan,
                    // $Nominal_Angsuran,
                    '-',
                    $Jenis_Jaminan,
                    '-',
                    // $Alamat_Jaminan,
                    $Pemilik_Jaminan,
                    $Nilai_Jaminan,
                    $Nomor_Rekening,
                    $Bank_Rekening,
                    $Nama_Pemilik_Rekening,
                    $Jangka_Waktu_Pembiayaan,
                    $Terbilang_Jangka_Waktu,
                    $nama_badan
                    // $tgl_bln_thn
                ]
            );
            $templateProcessor->setComplexBlock('Data_Tabel', $table);

            Storage::disk('public')->makeDirectory('akad_borrower/' . $userID);

            $templateProcessor->saveAs(storage_path('app/public/akad_borrower/' . $userID . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERUSAHAAN.docx'));
            shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_borrower/' . $userID . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERUSAHAAN.docx') . ' ' . base_path('storage/app/public/akad_borrower/' . $userID));


            return ['status' => 'Berhasil'];
        } else {
            return ['status' => 'Gagal'];
        }
    }


    // SEND DOC MURABAHAH  DSI & BORROWER
    public function sendDocMurabahahBorrower($brw_id, $id_proyek)
    {

        $getDataBorrower = BorrowerDetails::leftJoin('brw_user', 'brw_user.brw_id', '=', 'brw_user_detail.brw_id')
            ->where('brw_user.brw_id', $brw_id)
            ->first();

        $getDataTaufiqSign = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
            ->where('investor.email', config('app.email_pak_taufiq'))
            ->first();
        $getDataProyek = Proyek::where('id', $id_proyek)->first();
        $pendanaan = $getDataProyek->total_need;

        $date = Carbon::now()->format('Ymd');
        $idBorrower = $getDataBorrower->brw_id;
        $document_id = 'borrowerKontrak_' . $date . '_' . $idBorrower . '_' . $id_proyek;
        $type_borrower = $getDataBorrower->brw_type;


        $templateID = "";
        // cek brw type
        if ($type_borrower == 1 or $type_borrower == 3) {
            $this->createDocMurobahahBorrowerIndividu($brw_id, $id_proyek);
            $docPDF = fopen('../storage/app/public/akad_borrower/' . $brw_id . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERORANGAN.pdf', 'r');
            $templateID = "dsi003";
        } else {
            $this->createDocMurobahahBorrowerPerusahaan($brw_id, $id_proyek);

            $docPDF = fopen('../storage/app/public/akad_borrower/' . $brw_id . '/PERJANJIAN_PEMBIAYAAN_MURABAHAH_DSI_Penerima_Pembiayaan_PERUSAHAAN.pdf', 'r');
            $templateID = "dsi002";
        }


        $owner = [

            "privyId" => 'DEVRA4440',
            "enterpriseToken" => config('app.privy_enterprise_token')

        ];

        $recipients = [
            [
                // "email"=> $getDataTaufiqSign->email,
                "email" => config('app.email_pak_taufiq'),
                "type" => "Signer",
                "enterpriseToken" => config('app.privy_enterprise_token')
            ],
            [
                "email" => $getDataBorrower->email,
                "type" => "Signer",
                "enterpriseToken" => null
            ]
        ];

        $multipart_form =   [
            [
                'name' => 'documentTitle',
                'contents' => 'PERJANJIAN PEMBIAYAAN MURABAHAH'
            ],
            [
                'name' => 'docType',
                'contents' => 'Serial'
            ],
            [
                'name' => 'owner',
                'contents' => json_encode($owner)
            ],
            [
                'name' => 'document',
                'contents' => $docPDF
            ],
            [
                'name' => 'recipients',
                'contents' => json_encode($recipients)
            ],
            [
                'name' => 'templateId',
                'contents' => $templateID
            ]


        ];

        $client = new Client();
        $boundary = '----WebKitFormBoundary7MA4YWxkTrZu0gW';
        $request = $client->post(config('app.privy_url') . '/document/upload', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'multipart/form-data; ' . config('app.boundary'), //multipart/form-data //application/x-www-form-urlencoded
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key'),

            ],
            'body' => new \GuzzleHttp\Psr7\MultipartStream($multipart_form, $boundary)
        ]);

        $response = $request->getBody()->getContents();
        $reponse_decode = json_decode($response);
        $docToken = $reponse_decode->{'data'}->{'docToken'} == null ? '' : $reponse_decode->{'data'}->{'docToken'};
        $urlDocument = $reponse_decode->{'data'}->{'urlDocument'} == null ? '' : $reponse_decode->{'data'}->{'urlDocument'};

        // $updateStatusLogDana = LogRekening::whereRaw('id = (select max(`id`) from log_rekening)')->where('investor_id', $user_id)->update(
        //     [
        //         'status_dana' => 1            
        //     ]
        // );

        $logAkadDigiSign = new LogAkadDigiSignBorrower;
        $logAkadDigiSign->brw_id = $brw_id;
        $logAkadDigiSign->investor_id = '';
        $logAkadDigiSign->id_proyek = $id_proyek;
        $logAkadDigiSign->provider_id = 1;
        $logAkadDigiSign->total_pendanaan = '';
        $logAkadDigiSign->document_id = $document_id;
        $logAkadDigiSign->docToken = $docToken;
        $logAkadDigiSign->urlDocument = $urlDocument;
        $logAkadDigiSign->downloadURL = '';
        $logAkadDigiSign->status = 'kirim';
        //$logAkadDigiSign->tgl_sign = date("Y-m-d");

        $logAkadDigiSign->save();

        //$this->logAkadDigiSignBorrower($brw_id,'',$id_proyek,'1','','kirim',$document_id, $docToken, $urlDocument, '');
        return $response;
    }


    // ####################################################### END MUROBAHAH  ####################################################### //

    // ####
    ###################################################### BORROWER ######################################################
    // ####




    // ####################################################### SP3  ####################################################### //

    public function createDocSP3($userID, $idProyek, $idPendanaan)
    {
        if ($userID) {
            $getDataBorrower    = Borrower::leftJoin('brw_user_detail', 'brw_user_detail.brw_id', '=', 'brw_user.brw_id')
                ->where('brw_user.brw_id', $userID)
                ->first();
            $getDataPendanaan   = BorrowerPendanaan::leftJoin('brw_tipe_pendanaan', 'brw_tipe_pendanaan.tipe_id', '=', 'brw_pendanaan.pendanaan_tipe')
                ->where('brw_pendanaan.brw_id', $userID)
                ->where('brw_pendanaan.pendanaan_id', $idPendanaan)
                ->first(['brw_tipe_pendanaan.pendanaan_nama AS tipe_pendanaan', 'brw_pendanaan.*']);

            $getDataProyek = $idProyek !== null ? Proyek::where('id', $idProyek)->first() : null;
            $getDataPendanaanAktif = $getDataProyek !== null ? PendanaanAktif::where('proyek_id', $getDataProyek->id)->sum('total_dana') : null;


            // if(count($jaminan) < 6){
            //     $ulang = 6-count($jaminan);
            //     for($i=1;$i<=$ulang;$i++){
            //         array_push($jaminan," ");
            //     }    
            // }
            // dd(count($jaminan));

            $nilai_proyek = $getDataPendanaan->pendanaan_dana_dibutuhkan;
            $marginProyek = $getDataPendanaan->estimasi_imbal_hasil;
            $totalMargin = $marginProyek;
            $perhitunganMargin = $totalMargin / 100 * $nilai_proyek;
            $totalHarga = $nilai_proyek + $perhitunganMargin;
            // $getno_proyek = explode(" ",$getDataPendanaan->pendanaan_nama);
            $getno_proyek = explode(" ", $getDataPendanaan->pendanaan_nama);
            // $no_proyek = $getno_proyek[1];
            $no_proyek = $getDataPendanaan->pendanaan_id;


            // $no_proyek = $getDataProyek !== null ? explode(' ',$getDataProyek->nama)[1] : '';
            // $mulai_penggalangan = $getDataProyek !== null ? Carbon::parse($getDataProyek->tgl_mulai_penggalangan) : 0;
            // $selesai_penggalangan = $getDataProyek !== null ? Carbon::parse($getDataProyek->tgl_selesai_penggalangan) : 0;
            // $jumlah_hari_penggalangan = $mulai_penggalangan->diffInDays($selesai_penggalangan)+1;
            $mulai_penggalangan = date("Y-n-d");
            $selesai_penggalangan = date('Y-n-d', strtotime("+14 day"));
            $jumlah_hari_penggalangan = 14;

            $nama = $getDataBorrower->nama;
            $nama_badan_hukum = !empty($getDataBorrower->nm_bdn_hukum) ? $getDataBorrower->nm_bdn_hukum : " ";
            $nama_badan = !empty($getDataBorrower->nm_bdn_hukum) ? $getDataBorrower->nm_bdn_hukum : $getDataBorrower->nama;
            $jabatan = $getDataBorrower->jabatan;
            $alamat = $getDataBorrower->alamat;
            $get_tgl_permohonan = Carbon::parse(explode(' ', $getDataPendanaan->created_at)[0])->format('d-n-Y');
            $kelurahan = $getDataBorrower->kelurahan;
            $kecamatan = $getDataBorrower->kecamatan;
            $kota = $getDataBorrower->kota;

            // $Jenis_Obyek_Pembiayaan = $getDataProyek !== null ? $getDataProyek->nama : '';
            $Jenis_Obyek_Pembiayaan = ucwords($getDataPendanaan->pendanaan_nama);
            // $Jangka_Waktu_Pembiayaan = $getDataProyek !== null ? $getDataProyek->tenor_waktu : 0;
            $Jangka_Waktu_Pembiayaan = $getDataPendanaan->durasi_proyek;
            // $Tanggal_Jatuh_Tempo = $getDataProyek !== null ? Carbon::parse($getDataProyek->tgl_selesai)->format('d-m-Y') : 0;
            $getTglselesaiproyek = date('Y-m-d', strtotime(date('Y-m-d') . "+16 day"));
            $selisihBulan = $getTglselesaiproyek . "+" . $getDataPendanaan->durasi_proyek . " month";
            $selesai_proyek = date('d-m-Y', strtotime($selisihBulan));
            // tipe pendanaan
            $tipe_pendanaan = $getDataPendanaan->tipe_pendanaan;
            $imbal_hasil = $getDataPendanaan->estimasi_imbal_hasil;
            $imbal_hasil_min5 = ($imbal_hasil - 5) . ' %';
            $dana_dibutuhkan = number_format($getDataPendanaan->pendanaan_dana_dibutuhkan);
            $tgl_waktu_disetujui = date("d-m-Y");
            if ($getDataBorrower->brw_type == 1) {
                $getPenerimaPendanaan = $getDataBorrower->nama;
                $qAhliWaris = DB::select("SELECT nama_ahli_waris FROM brw_ahli_waris WHERE brw_id = '" . $getDataBorrower->brw_id . "'");
                $getAhliWaris = $qAhliWaris[0]->{'nama_ahli_waris'};
                $jabatan1 = 'Penerima Pendanaan';
                $jabatan2 = 'Ahli Waris';
            } else {
                $getPengurus = DB::SELECT("SELECT a.nm_pengurus,b.jabatan FROM brw_pengurus a, m_jabatan b WHERE a.jabatan = b.id AND a.brw_id = '" . $getDataBorrower->brw_id . "' ORDER BY a.jabatan ASC");
                $getPenerimaPendanaan = $getPengurus[0]->{'nm_pengurus'};
                $jabatan1 = $getPengurus[0]->{'jabatan'};
                if (count($getPengurus) > 1) {
                    $getAhliWaris = $getPengurus[1]->{'nm_pengurus'};
                    $jabatan2 = $getPengurus[1]->{'jabatan'};
                } else {
                    $getAhliWaris = " ";
                    $jabatan2 = " ";
                }
            }
            $penerima_pendanaan =  $getPenerimaPendanaan;
            $ahli_waris = $getAhliWaris;

            // penilaian pefindo
            $getNilai = DB::SELECT("SELECT nilai FROM brw_scorring_personal WHERE brw_id ='" . $getDataBorrower->brw_id . "'");
            $scoring_pefindo = $getNilai[0]->{'nilai'};


            // $Jenis_Jaminan = $getJaminan !== null ? $getJaminan->jenis_jaminan : '-';
            // $No_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nomor : '-';
            // $Nama_Jaminan = $getJaminan !== null ? $getJaminan->jaminan_nama : '-';
            // ${Jaminan}
            // ${Jenis_Jaminan}
            // ${Nomor_Jaminan}
            // ${Pemilik_Jaminan}

            $Terbilang_Jangka_Waktu =  ucwords(Terbilang::make($Jangka_Waktu_Pembiayaan, ''));

            $tgl_sekarang = date("d-n-Y");
            $data_tgl = !empty($tgl_sekarang) ? explode("-", $tgl_sekarang) : null;
            // tgl
            $cek_tgl = 0;
            if ($data_tgl !== null && $data_tgl !== '') {
                if ($data_tgl[0] !== null && $data_tgl[0] !== '') {
                    if (strlen($data_tgl[0])  == 2) {
                        if ($data_tgl[0][0] == 0) {
                            $cek_tgl = $data_tgl[0][1];
                        } else {
                            $cek_tgl = $data_tgl[0];
                        }
                    } else {
                        $cek_tgl = $data_tgl[0];
                    }
                } else {
                    $cek_tgl = 0;
                }
            } else {
                $cek_tgl = 0;
            }
            // end tgl
            // bulan
            $data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
            for ($x = 1; $x <= 12; $x++) {
                if ($x == $data_tgl[1]) {
                    $cek_bln = $data_bulan[$x - 1];
                }
            }
            // end bulan
            $tgl_bln_thn = $cek_tgl . ' ' . $cek_bln . ' ' . $data_tgl[2];



            $data_tgl_permohonan = !empty($get_tgl_permohonan) ? explode("-", $get_tgl_permohonan) : null;
            // tgl
            $cek_tgl_permohonan = 0;
            if ($data_tgl_permohonan !== null && $data_tgl_permohonan !== '') {
                if ($data_tgl_permohonan[0] !== null && $data_tgl_permohonan[0] !== '') {
                    if (strlen($data_tgl_permohonan[0])  == 2) {
                        if ($data_tgl_permohonan[0][0] == 0) {
                            $cek_tgl_permohonan = $data_tgl_permohonan[0][1];
                        } else {
                            $cek_tgl_permohonan = $data_tgl_permohonan[0];
                        }
                    } else {
                        $cek_tgl_permohonan = $data_tgl_permohonan[0];
                    }
                } else {
                    $cek_tgl_permohonan = 0;
                }
            } else {
                $cek_tgl_permohonan = 0;
            }
            // end tgl
            // bulan
            $data_bulan_permohonan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
            for ($x = 1; $x <= 12; $x++) {
                if ($x == $data_tgl_permohonan[1]) {
                    $cek_bln_permohonan = $data_bulan_permohonan[$x - 1];
                }
            }
            // end bulan
            $tgl_bln_thn_permohonan = $cek_tgl_permohonan . ' ' . $cek_bln_permohonan . ' ' . $data_tgl_permohonan[2];

            $noSP3 = $this->generateNoSP3($idProyek);

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/public/akad_template/SP3.docx'));

            $templateProcessor->setValue(
                [
                    'Nomor_SP3',
                    'Tanggal_Sekarang',
                    'Nama_Borrower',
                    'Alamat_Borrower',
                    'Kelurahan',
                    'Kecamatan',
                    'Kota',
                    'Nama_Direktur',
                    'Tanggal_Permohonan',
                    'Nama_Proyek',
                    'Jangka_Waktu_Proyek',
                    'Nilai_Proyek',
                    'Nilai_Imbal_Hasil',
                    'Nilai_Pengembalian',
                    'Sekaligus_Atau_Bertahap',
                    'selesai_proyek',
                    'Jumlah_Angsuran',
                    'Nilai_Angsuran',
                    'Tanggal_Pembayaran_Angsuran',
                    'Jangka_Waktu_Pembiayaan',
                    // 'Jaminan1',
                    // 'Jaminan2',
                    // 'Jaminan3',
                    // 'Jaminan4',
                    // 'Jaminan5',
                    // 'Jaminan6',
                    'nomor_proyek',
                    'Nilai_Proyek',
                    'Jumlah_Penggalangan_Dana',
                    'Kategori_Scoring',
                    'imbal_hasil',
                    'tipe_pendanaan',
                    'imbal_hasil_min5',
                    'nama_badan_hukum',
                    'tanggal_waktu_disetujui',
                    'penerima_pendanaan',
                    'ahli_waris',
                    'jabatan1',
                    'jabatan2',
                    'dana_dibutuhkan',
                    'scoring_pefindo'
                ],
                [
                    $noSP3,
                    $tgl_bln_thn,
                    $nama_badan,
                    $alamat,
                    $kelurahan,
                    $kecamatan,
                    $kota,
                    $nama,
                    $tgl_bln_thn_permohonan,
                    $Jenis_Obyek_Pembiayaan,
                    $Jangka_Waktu_Pembiayaan,
                    number_format($nilai_proyek),
                    number_format($perhitunganMargin),
                    number_format($totalHarga),
                    '-',
                    $selesai_proyek,
                    $Jangka_Waktu_Pembiayaan,
                    0,
                    '-',
                    $Jangka_Waktu_Pembiayaan,
                    // $jaminan[0],
                    // $jaminan[1],
                    // $jaminan[2],
                    // $jaminan[3],
                    // $jaminan[4],
                    // $jaminan[5],
                    $no_proyek,
                    $nilai_proyek,
                    $jumlah_hari_penggalangan,
                    '-',
                    $marginProyek . " %",
                    $tipe_pendanaan,
                    $imbal_hasil_min5,
                    $nama_badan_hukum,
                    $tgl_waktu_disetujui,
                    $penerima_pendanaan,
                    $ahli_waris,
                    $jabatan1,
                    $jabatan2,
                    $dana_dibutuhkan,
                    $scoring_pefindo
                ]
            );

            // tabel angsuran
            $tenor = $Jangka_Waktu_Pembiayaan;
            $getCicilan = $nilai_proyek * 0.02;
            $getCicilanAkhir = $totalHarga - (($tenor - 1) * $getCicilan);

            $values = [];
            $totalBayar = 0;
            for ($u = 1; $u <= $tenor; $u++) {
                $bulanKe = $u;
                $selisihBulanTempo = $getTglselesaiproyek . "+" . $u . " month";
                $tglJatuhTempo = date('Y-m-d', strtotime($selisihBulanTempo));
                if ($u == $tenor) {
                    $nominalTagihanPerbulan = $getCicilanAkhir;
                } else {
                    $nominalTagihanPerbulan = $getCicilan;
                }
                $totalBayar = $totalBayar + $nominalTagihanPerbulan;
                $getSisaKewajiban = $totalHarga - $totalBayar;

                $list = array(
                    'bulan' => $bulanKe,
                    'jatuhtempo' => date('d-m-Y', strtotime($tglJatuhTempo)),
                    'nominaltagihan' => "Rp. " . number_format($nominalTagihanPerbulan),
                    'sisakewajiban' => "Rp. " . number_format($getSisaKewajiban)
                );
                array_push($values, $list);
            }

            $templateProcessor->cloneRowAndSetValues('bulan', $values);

            // jaminan
            $getJaminan = BorrowerJaminan::leftJoin('brw_pendanaan', 'brw_jaminan.pengajuan_id', '=', 'brw_pendanaan.pengajuan_id')
                ->leftJoin('m_jenis_jaminan', 'm_jenis_jaminan.id_jenis_jaminan', '=', 'brw_jaminan.jaminan_jenis')
                ->where('brw_pendanaan.pengajuan_id', $getDataPendanaan->pengajuan_id)
                ->where('brw_pendanaan.pendanaan_id', $getDataPendanaan->pendanaan_id)
                ->get();

            $jaminan = [];
            $noo = 1;
            foreach ($getJaminan as $row) {
                $namaJaminan = $row->jaminan_nama;
                $jenisJaminan = $row->jenis_jaminan;
                $nomorJaminan = $row->jaminan_nomor;

                $jaminanGet = array(
                    'jenisJaminan' => $row->jenis_jaminan,
                    'nomorJaminan' => $row->jaminan_nomor,
                    'detailJaminan' => $row->jaminan_detail,
                    'NOP' => $row->NOP,
                    'pemilik' => $row->jaminan_nama
                );
                // $jaminanGet = "- ".$namaJaminan.'/no jaminan: '.$nomorJaminan.'/'.$jenisJaminan;
                array_push($jaminan, $jaminanGet);
            }
            // dd(count($jaminan));
            $templateProcessor->cloneRowAndSetValues('jenisJaminan', $jaminan);

            Storage::disk('public')->makeDirectory('akad_borrower/' . $userID . '/' . $idPendanaan);

            $templateProcessor->saveAs(storage_path('app/public/akad_borrower/' . $userID . '/' . $idPendanaan . '/SP3.docx'));
            shell_exec('unoconv -f pdf ' . base_path('storage/app/public/akad_borrower/' . $userID . '/' . $idPendanaan . '/SP3.docx') . ' ' . base_path('storage/app/public/akad_borrower/' . $userID . '/' . $idPendanaan));

            // $this->logSP3Borrower($userID,$noSP3,$idProyek,$nilai_proyek,1);

            $response = ['status' => 'Berhasil', 'nosp3' => $noSP3];
        } else {
            $response = ['status' => 'Gagal'];
        }

        return response()->json($response);
    }


    // ####################################################### END SP3  ####################################################### //

    // ####################################################### CHECK STATUS DOCUMENT ####################################################### //

    // function untuk show document murobahah yg sudah didanai (halaman dashboard investor)
    public function kirimTokenDoc($id)
    {

        $getToken = LogAkadDigiSignInvestor::where('id_log_akad_investor', $id)->first();

        if ($getToken) { // jika document ada di table
            $callFuncDoc = $this->checkStatusDocumentPrivy($getToken->docToken);
            return $callFuncDoc;
        } else { // jika tidak ada di table

            return response()->json([
                'status' => 'document_kosong'
            ]);
        }
    }

    // function untuk download Document yang sudah completed
    public function getDocTokenWakalahInvestor($userID)
    {

        // $getWakalahToken = LogAkadDigiSignInvestor::where(\DB::raw('substr(document_id, 1, 15)'), '=' , 'investorKontrak')
        // ->where('investor_id',$userID)
        // ->where('status', 'Completed')
        // ->orderBy('id_log_akad_investor','desc')
        // ->first();
        // if($getWakalahToken){ // jika document ada di table

        // $callFuncDoc = $this->checkStatusDocumentPrivy($getWakalahToken->docToken);
        // return $callFuncDoc;

        // }else{

        // return response()->json([
        // 'status' => 'document_kosong'
        // ]);

        // }

        $cekDocWakalahBorrower = LogAkadDigiSignInvestor::where(\DB::raw('substr(document_id, 1, 15)'), '=', 'investorKontrak')
            ->where('investor_id', $userID)
            ->where('status', 'Completed')
            ->orderBy('id_log_akad_investor', 'desc')
            ->first();

        $callFuncDoc = $this->checkStatusDocumentPrivy($cekDocWakalahBorrower->docToken);
        return $callFuncDoc;
    }

    public function checkStatusDocumentPrivy($docToken)
    {

        $client = new Client();
        $request = $client->get(config('app.privy_url') . '/document/status/' . $docToken, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'charset' => 'utf-8',
                'Authorization' => 'Basic ' . $this->base64_usename_password(),
                'Merchant-Key' => config('app.privy_merchent_key')
            ]

        ]);

        $response = (string)$request->getBody();
        $response_decode = json_decode($response);
        return $response;

        // jika response code 200 = berhasil
        if ($response_decode->{'code'} == 200) {

            // jika status document sudah completed
            if ($response_decode->{'data'}->{'documentStatus'} == "Completed") {


                return response()->json([
                    'status' => $response_decode->{'data'}->{'documentStatus'},
                    'docToken' => $response_decode->{'data'}->{'docToken'},
                    'urlDocument' => $response_decode->{'data'}->{'urlDocument'},
                    'downloadURL' => $response_decode->{'data'}->{'download'}->{'url'}
                ]);
            }

            // jika status document in progress
            else {

                // cek document wakalah investor
                $cekDocWakalah = LogAkadDigiSignInvestor::where('docToken', $docToken)
                    ->where(\DB::raw('substr(document_id, 1, 15)'), '=', 'investorKontrak')
                    ->orderBy('id_log_akad_investor', 'desc')
                    ->first();

                // jika document token ada di table log akad investor
                if ($cekDocWakalah) {

                    return response()->json([

                        'status_wakalah_investor' => $response_decode->{'data'}->{'recipients'}[1]->{'signatoryStatus'},
                        'docToken' => $response_decode->{'data'}->{'docToken'},
                        'urlDocument' => $response_decode->{'data'}->{'urlDocument'}

                    ]);
                } else {

                    // cek document murabahah investor & borrower
                    $cekDocMurabahahInvestor = LogAkadDigiSignBorrower::where('docToken', $docToken)
                        ->where(\DB::raw('substr(document_id, 1, 15)'), '=', 'borrowerKontrak')
                        ->orderBy('id_log_akad_borrower', 'desc')
                        ->first();

                    // jika document token ada di table log akad investor
                    if ($cekDocMurabahahInvestor) {

                        return response()->json([
                            'status_murabahah_borrower' => $response_decode->{'data'}->{'recipients'}[1]->{'signatoryStatus'},
                            'status_document' => $response_decode->{'data'}->{'documentStatus'},
                            'docToken' => $response_decode->{'data'}->{'docToken'},
                            'urlDocument' => $response_decode->{'data'}->{'urlDocument'}
                        ]);
                    }
                }
            }
        }

        // jika response code ! == 200 = gagal
        else {

            return $response;
        }
    }

    public function downloadWakalahBorrower($id_brw, $proyek_id)
    {

        // cek document murabahah investor & borrower
        $cekDocWakalahBorrower = LogAkadDigiSignBorrower::where('brw_id', $id_brw)
            ->where('id_proyek', $proyek_id)
            ->orderBy('id_log_akad_borrower', 'desc')
            ->first();

        $callFuncDoc = $this->checkStatusDocumentPrivy($cekDocWakalahBorrower->docToken);
        return $callFuncDoc;
    }

    public function downloadMurabahahBorrower($proyek_id, $investor_id)
    {

        // cek document murabahah investor & borrower
        $cekDocMurobahahBorrower = LogAkadDigiSignBorrower::where('id_proyek', $proyek_id)
            ->where('investor_id', $investor_id)
            ->orderBy('id_log_akad_borrower', 'desc')
            ->first();

        $callFuncDoc = $this->checkStatusDocumentPrivy($cekDocMurobahahBorrower->docToken);
        return $callFuncDoc;
    }

    // ####################################################### END CHECK STATUS DOCUMENT ####################################################### //



    // ####################################################### VIEW DOC ####################################################### //

    public function viewTTD_wakalah($privyID, $x, $y, $page, $token)
    {

        return view('pages.user.ttd_view_wakalah')->with('privyID', $privyID)->with('x', $x)->with('y', $y)->with('page', $page)->with('token', $token);
    }

    public function viewTTD_murabahah($privyID, $x, $y, $page, $token)
    {

        return view('pages.user.ttd_view_murabahah')->with('privyID', $privyID)->with('x', $x)->with('y', $y)->with('page', $page)->with('token', $token);
    }
    public function viewTTD_wakalah_borrower($privyID, $x, $y, $page, $token)
    {

        return view('pages.user.ttd_view_wakalah_borrower')->with('privyID', $privyID)->with('x', $x)->with('y', $y)->with('page', $page)->with('token', $token);
    }
    public function viewTTD_murabahah_borrower($privyID, $x, $y, $page, $token)
    {

        return view('pages.user.ttd_view_murabahah_borrower')->with('privyID', $privyID)->with('x', $x)->with('y', $y)->with('page', $page)->with('token', $token);
    }

    // ####################################################### END VIEW DOC ####################################################### //




    // ####################################################### LOG AKAD PRIVY ####################################################### //
    private function logAkadDigiSignInvestor($user_id, $proyek_id, $mutasi_id, $provider_id, $total_aset, $status, $doc_id, $docToken, $urlDocument, $downloadURL)

    {

        if ($user_id) {

            $cekLog = LogAkadDigiSignInvestor::where('document_id', $doc_id)->count();

            if ($cekLog != 0) {
                $dataLog = LogAkadDigiSignInvestor::where('document_id', $doc_id)->first();

                $dataLog->docToken = $docToken;
                $dataLog->urlDocument = $urlDocument;
                $dataLog->downloadURL = $downloadURL;
                $dataLog->status = $status;
                //$dataLog->tgl_sign = date("Y-m-d");

                $dataLog->save();
            } else {
                $logAkadDigiSign = new LogAkadDigiSignInvestor;
                $logAkadDigiSign->investor_id = $user_id;
                $logAkadDigiSign->mutasi_id = $mutasi_id;
                $logAkadDigiSign->proyek_id = $proyek_id;
                $logAkadDigiSign->provider_id = $provider_id;
                $logAkadDigiSign->total_aset = $total_aset;
                $logAkadDigiSign->document_id = $doc_id;
                $logAkadDigiSign->docToken = $docToken;
                $logAkadDigiSign->urlDocument = $urlDocument;
                $logAkadDigiSign->downloadURL = $downloadURL;
                $logAkadDigiSign->status = $status;
                $logAkadDigiSign->tgl_sign = date("Y-m-d");

                $logAkadDigiSign->save();
            }

            $response = ['status' => 'Data Berhasil di Update'];
        } else {
            $response = ['status' => 'Data Gagal di Update'];
        }
        // return $response;
    }

    // log akad borrower
    private function logAkadDigiSignBorrower($id_borrower, $investor_id, $proyek_id, $provider_id, $total_pendanaan, $status, $doc_id, $docToken, $urlDocument, $downloadURL)
    {

        if ($id_borrower) {
            $cekLog = LogAkadDigiSignBorrower::where('document_id', $doc_id)->count();
            if ($cekLog != 0) {
                $logAkadDigiSign = LogAkadDigiSignBorrower::where('document_id', $doc_id)->first();

                $logAkadDigiSign->docToken = $docToken;
                $logAkadDigiSign->urlDocument = $urlDocument;
                $logAkadDigiSign->downloadURL = $downloadURL;
                $logAkadDigiSign->status = $status;
                $logAkadDigiSign->save();
            } else {
                $logAkadDigiSign = new LogAkadDigiSignBorrower;
                $logAkadDigiSign->brw_id = $id_borrower;
                $logAkadDigiSign->investor_id = $investor_id;
                $logAkadDigiSign->id_proyek = $proyek_id;
                $logAkadDigiSign->provider_id = $provider_id;
                $logAkadDigiSign->total_pendanaan = $total_pendanaan;
                $logAkadDigiSign->document_id = $doc_id;
                $logAkadDigiSign->docToken = $docToken;
                $logAkadDigiSign->urlDocument = $urlDocument;
                $logAkadDigiSign->downloadURL = $downloadURL;
                $logAkadDigiSign->status = $status;
                $logAkadDigiSign->tgl_sign = date("Y-m-d");

                $logAkadDigiSign->save();
            }

            $response = ['status' => 'Data Berhasil di Update'];
        } else {

            $response = ['status' => 'Data Gagal di Update'];
        }
        return $response;
    }

    private function logAkadDigiSignInvestorNonDigital($user_id, $proyek_id, $provider_id, $total_aset, $status, $doc_id, $docToken, $urlDocument, $downloadURL)
    {
        if ($user_id) {

            $logAkadDigiSign = new LogAkadDigiSignInvestor;
            $logAkadDigiSign->investor_id = $user_id;
            $logAkadDigiSign->proyek_id = $proyek_id;
            $logAkadDigiSign->provider_id = $provider_id;
            $logAkadDigiSign->total_aset = $total_aset;
            $logAkadDigiSign->document_id = $doc_id;
            $logAkadDigiSign->docToken = $docToken;
            $logAkadDigiSign->urlDocument = $urlDocument;
            $logAkadDigiSign->downloadURL = $downloadURL;
            $logAkadDigiSign->status = $status;
            $logAkadDigiSign->tgl_sign = date("Y-m-d");

            $logAkadDigiSign->save();

            $response = ['status' => 'Data Berhasil di Update'];
        } else {

            $response = ['status' => 'Data Gagal di Update'];
        }
        return $response;
    }

    #SP3
    private function logSP3Borrower($user_id, $no_sp3, $proyek_id, $total_pendanaan, $status)
    {
        if ($user_id) {
            $cekLog = LogSP3Borrower::where('brw_id', $user_id)
                ->where('id_proyek', $proyek_id)
                ->count();
            if ($cekLog != 0) {
                $dataLog = LogSP3Borrower::where('brw_id', $user_id)
                    ->where('id_proyek', $proyek_id)
                    ->first();

                $dataLog->status = $status;
                $dataLog->sp3_dir = "";

                $dataLog->save();
            } else {
                $logSP3 = new LogSP3Borrower;
                $logSP3->brw_id = $user_id;
                $logSP3->no_sp3 = $no_sp3;
                $logSP3->id_proyek = $proyek_id;
                $logSP3->total_pendanaan = $total_pendanaan;
                $logSP3->status = $status;
                $logSP3->sp3_dir = "";

                $logSP3->save();
            }

            $response = ['status' => 'Data Berhasil di Update'];
        } else {

            $response = ['status' => 'Data Gagal di Update'];
        }
        return $response;
    }

    public function uStatusDownload($user_id)
    {
        if ($user_id) {
            LogAkadDigiSignInvestor::whereRaw("id_log_akad_investor = (select max(id_log_akad_investor) from log_akad_digisign_investor where investor_id = $user_id)")->where('investor_id', $user_id)->update(
                [
                    'status_download' => '1',
                    'updated_at' => NOW()
                ]
            );
            return $response = ['status' => 'Dokumen Berhasil Di Unduh'];
        }
    }

    public function dl_wakalah($user_id)
    {
        if ($user_id) {
            $file = "../storage/app/private/akad_investor/" . $user_id . "/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.pdf";
            $headers = array(
                'Content-Type: application/pdf',
            );
            return response()->download($file, 'PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.pdf', $headers);
        }
    }

    // ####################################################### END LOG AKAD PRIVY ####################################################### //


    public function createDocAkadWakalahBilUjrohInvestor_badanhukum($user_id)
    {
        if ($user_id) {
            $getDataInvestor = Investor::leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'investor.id')
                ->where('investor_id', $user_id)
                ->select(
                    'detil_investor.*',
                    'investor.*',
                    DB::raw('(SELECT nama_kota FROM m_provinsi_kota WHERE kode_kota = detil_investor.kota_perusahaan) as kota'),
                    DB::raw('(SELECT nama_provinsi FROM m_provinsi_kota WHERE kode_kota = detil_investor.kota_perusahaan) as provinsi'),
                    DB::raw('(SELECT nama_kota FROM m_provinsi_kota WHERE kode_kota = detil_investor.kedudukan_notaris_pendirian) as kedudukan_notaris_pendirian'),
                    DB::raw('(SELECT nama_kota FROM m_provinsi_kota WHERE kode_kota = detil_investor.kedudukan_perusahaan) AS kedudukan_perusahaan'),
                    DB::raw('(SELECT nama_kota FROM m_provinsi_kota WHERE kode_kota = detil_investor.kedudukan_notaris_perubahan) AS kedudukan_notaris_perubahan')
                )
                ->first();

            $getDirektur = DB::SELECT("SELECT investor_pengurus.*,m_jabatan.jabatan AS nama_jabatan,m_provinsi_kota.nama_kota,m_provinsi_kota.nama_provinsi FROM investor_pengurus 
            	LEFT JOIN m_provinsi_kota ON m_provinsi_kota.kode_kota = investor_pengurus.kota 
            	LEFT JOIN m_jabatan ON m_jabatan.id = investor_pengurus.jabatan 
            	WHERE investor_pengurus.investor_id = " . $user_id . " ORDER BY investor_pengurus.pengurus_id ASC LIMIT 2 ");
            // AND investor_pengurus.jabatan = ?",[$user_id,'2']);
            if (count($getDirektur) > 0) {
                for ($i = 0; $i < count($getDirektur); $i++) {
                    if ($getDirektur[$i]->jabatan == 2) {
                        $nm_pengurus = ucfirst($getDirektur[$i]->nm_pengurus);
                        $nik_pengurus = $getDirektur[$i]->identitas_pengurus;
                        $alamat_pengurus = $getDirektur[$i]->alamat . " Kelurahan " . $getDirektur[$i]->kelurahan . " Kecamatan " . $getDirektur[$i]->kecamatan . " " . $getDirektur[$i]->nama_kota . ", " . $getDirektur[$i]->nama_provinsi;
                        $no_tlp = $getDirektur[$i]->no_tlp;
                        $path = $getDirektur[$i]->foto_ktp;
                    } else {
                        $sebagai_kontak_lainnya =  ucfirst($getDirektur[$i]->nama_jabatan);
                        $nama_kontak_lainnya =  ucfirst($getDirektur[$i]->nm_pengurus);
                        $nomor_ktp_kontak_lainnya =  $getDirektur[$i]->identitas_pengurus;
                        $nomor_handphone_kontak_lainnya =  $getDirektur[$i]->no_tlp;
                        $alamat_kontak_lainnya =  $getDirektur[$i]->alamat . " Kelurahan " . $getDirektur[$i]->kelurahan . " Kecamatan " . $getDirektur[$i]->kecamatan . " " . $getDirektur[$i]->nama_kota . ", " . $getDirektur[$i]->nama_provinsi;
                        $foto_ktp_kontak_lainnya = $getDirektur[$i]->foto_ktp;
                    }
                }
            } else {
                $nm_pengurus = "";
                $nik_pengurus = "";
                $alamat_pengurus = "";
                $no_tlp = "";
                $path = "";
                $sebagai_kontak_lainnya = "";
                $nama_kontak_lainnya = "";
                $nomor_ktp_kontak_lainnya = "";
                $nomor_handphone_kontak_lainnya = "";
                $alamat_kontak_lainnya = "";
                $foto_ktp_kontak_lainnya = "";
            }

            $getBO = DB::SELECT("SELECT investor_corp_pemegang_saham.*,m_jabatan.jabatan AS nama_jabatan,m_provinsi_kota.nama_kota,m_provinsi_kota.nama_provinsi FROM investor_corp_pemegang_saham 
            	LEFT JOIN m_provinsi_kota ON m_provinsi_kota.kode_kota = investor_corp_pemegang_saham.kota 
            	LEFT JOIN m_jabatan ON m_jabatan.id = investor_corp_pemegang_saham.jabatan 
            	WHERE investor_corp_pemegang_saham.investor_id = " . $user_id . " ORDER BY investor_corp_pemegang_saham.pemegang_saham_id ASC LIMIT 1 ");
            if (count($getBO) > 0) {
            	$jabatan_BO = ucfirst($getBO[0]->nama_jabatan);
            	$nama_BO = ucfirst($getBO[0]->nm_pemegang_saham);
				$identitas_BO = $getBO[0]->identitas_pemegang_saham;
				$npwp_BO = $getBO[0]->npwp;
				$telepon_BO = $getBO[0]->no_tlp;
				$alamat_BO = $getBO[0]->alamat . " Kelurahan " . $getBO[0]->kelurahan . " Kecamatan " . $getBO[0]->kecamatan . " " . $getBO[0]->nama_kota . ", " . $getBO[0]->nama_provinsi;
				$foto_ktp_beneficial_owner = ($getBO[0]->jenis_pemegang_saham == 2) ? $getBO[0]->foto_npwp : $getBO[0]->foto_ktp;
            }else{
            	$jabatan_BO = "";
				$nama_BO = "";
				$identitas_BO = "";
				$npwp_BO = "";
				$telepon_BO = "";
				$alamat_BO = "";
				$foto_ktp_beneficial_owner = "";
            }

            $callFuncMutasi  = DB::select("SELECT func_generate_serial_number('" . $user_id . "','proc:generate_serial_number','1434') as response");
            $riwayatMutasi   = DB::table("mutasi_investor")->where("investor_id", $user_id)->whereIn("perihal", ["Transfer Rekening", "Penarikan dana selesai"])->orderBy("id", "DESC")->first();

            $getDataRekening = RekeningInvestor::where('investor_id', $user_id)->first();
            $getDataBank = MasterBank::where('kode_bank', $getDataInvestor->bank_investor)->first();

            $getProyekInvestor = PendanaanAktif::select('nama', 'tgl_mulai', 'tanggal_invest', 'tgl_selesai', 'total_dana', 'profit_margin')
                ->leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $user_id)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $user_id . ' group by proyek_id')])
                ->get();
            // dd($getProyekInvestor);

            $getNominalInvestasiInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $user_id)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $user_id . ' group by proyek_id')])
                ->sum('pendanaan_aktif.total_dana');


            $getJumlahProyekInvestor = PendanaanAktif::leftJoin('proyek', 'proyek.id', '=', 'pendanaan_aktif.proyek_id')
                ->where('pendanaan_aktif.investor_id', $user_id)
                ->where('pendanaan_aktif.status', 1)
                ->whereNotIn('proyek.id', [\DB::raw('select proyek_id from log_pengembalian_dana where investor_id = ' . $user_id . ' group by proyek_id')])
                ->count('pendanaan_aktif.id');

            $totalInvestasi = !empty($getNominalInvestasiInvestor) ? number_format($getNominalInvestasiInvestor, 0, '', '.') : 0;
            $totalProyek = !empty($getJumlahProyekInvestor) ? $getJumlahProyekInvestor : 0;

            $gettgl_invest = !empty($riwayatMutasi->created_at) ? Carbon::parse(explode(" ", $riwayatMutasi->created_at)[0])->format('Y-m-d') : 0;
            $total_aset = !empty($getDataRekening) ? $getDataRekening->total_dana : 0;
            $nominal_investasi = !empty($getDataRekening) ? number_format($getDataRekening->total_dana, 0, '', '.') : 0;
            $va = !empty($getDataRekening) ? $getDataRekening->va_number : 0;

            $rekening = $getDataInvestor->rekening;
            $bank = !empty($getDataBank->nama_bank) ? $getDataBank->nama_bank : '';
            $pemilik_rekening = ($getDataInvestor->is_valid_rekening == 1) ? $getDataInvestor->pemilik_rekening_asli : $getDataInvestor->nama_pemilik_rek;

            # tgl dari terakhir top up dana
            // $tgl_sekarang = Carbon::parse($riwayatMutasi->created_at)->format('d-m-Y');

            $tgl_sekarang = date("Y-m-d");
            $no_hari = date('N', strtotime($tgl_sekarang));

            switch ($no_hari) {
                case 1:
                    $hari_transaksi = 'Senin';
                    break;
                case 2:
                    $hari_transaksi = 'Selasa';
                    break;
                case 3:
                    $hari_transaksi = 'Rabu';
                    break;
                case 4:
                    $hari_transaksi = 'Kamis';
                    break;
                case 5:
                    $hari_transaksi = 'Jumat';
                    break;
                case 6:
                    $hari_transaksi = 'Sabtu';
                    break;
                case 7:
                    $hari_transaksi = 'Minggu';
                    break;
                default:
                    $hari_transaksi = 'Libur';
                    break;
            };

            $getvaBNI  = DB::select("SELECT va_number FROM rekening_investor WHERE investor_id = " . $user_id . " LIMIT 1");
            $getvaBSI  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $user_id . " AND kode_bank = '451' LIMIT 1");
            $getvaCIMB  = DB::select("SELECT va_number FROM detil_rekening_investor WHERE investor_id = " . $user_id . " AND kode_bank = '022' LIMIT 1");

            $vaBNI = !empty($getvaBNI[0]->va_number) ? $getvaBNI[0]->va_number : '-';
            $vaBSI = !empty($getvaBSI[0]->va_number) ? '900' . $getvaBSI[0]->va_number : '-';
            $vaCIMB = !empty($getvaCIMB[0]->va_number) ? $getvaCIMB[0]->va_number : '-';

            $noAkadInvestor = $this->generateNoAkadInvestor($user_id);
            $noKuasaInvestor = str_replace("AWBL", "Kuasa", $noAkadInvestor);

            $nama_perusahaan = ucfirst($getDataInvestor->nama_perusahaan);
            $kedudukan_badan_hukum = ucfirst($getDataInvestor->kedudukan_perusahaan);
            $alamat_badan_hukum = $getDataInvestor->alamat_perusahaan . " Kelurahan " . $getDataInvestor->kelurahan_perusahaan . " Kecamatan " . $getDataInvestor->kecamatan_perusahaan . " " . $getDataInvestor->kota . ", " . $getDataInvestor->provinsi;
            $alamat_saja = $getDataInvestor->alamat_perusahaan;
            $rukun_tetangga = '07';
            $rukun_warga = '07';
            $kabupaten = $getDataInvestor->kota;
            $provinsi = $getDataInvestor->provinsi;
            $kelurahan = $getDataInvestor->kelurahan_perusahaan;
            $kecamatan = $getDataInvestor->kecamatan_perusahaan;

            $nama_direktur = $nm_pengurus;
            $jabatan_direktur = 'Direktur Utama';

            $nama_akta =  "Akta Pendirian " . " ";
            $nomor_akta = $getDataInvestor->no_akta_pendirian;
            $tanggal_akta = $this->tgl_indo($getDataInvestor->tgl_berdiri);
            $notaris_dan_kedudukan_akta = ucfirst($getDataInvestor->nama_notaris_akta_pendirian).', '.ucfirst($getDataInvestor->kedudukan_notaris_pendirian);
            $nomor_sk_kemenkumham = $getDataInvestor->no_sk_kemenkumham_pendirian;
            $tanggal_sk_kemenkumham = $this->tgl_indo($getDataInvestor->tgl_sk_kemenkumham_pendirian);

            if ($getDataInvestor->nomor_akta_perubahan == NULL || $getDataInvestor->nomor_akta_perubahan == '') {
                $textAkta = ".";
                $nomor_akta_perubahan = " -";
                $tanggal_akta_perubahan = " -";
                $notaris_akta_dan_kedudukan_perubahan = " -";
                $nomor_sk_kemenkumham_perubahan = " -";
                $tanggal_sk_kemenkumham_perubahan = " -";
            } else {
                $textAkta = ", dan ada beberapa perubahan yaitu akta perubahan dituangkan dalam Akta Perubahan Nomor " . $getDataInvestor->nomor_akta_perubahan . ", tanggal " . $this->tgl_indo($getDataInvestor->tanggal_akta_perubahan) . ", yang dibuat di hadapan Notaris " . ucfirst($getDataInvestor->nama_notaris_akta_perubahan) . " Notaris dan PPAT berkedudukan di " . ucfirst($getDataInvestor->kedudukan_notaris_perubahan) . ", dan telah didaftarkan Kementerian Hukum dan Hak Asasi Manusia Republik Indonesia, sebagaimana ternyata dari suratnya Nomor " . $getDataInvestor->no_sk_kemenkumham_perubahan . ", tertanggal " . $this->tgl_indo($getDataInvestor->tgl_sk_kemenkumham_perubahan) . ".";
                $nomor_akta_perubahan = $getDataInvestor->nomor_akta_perubahan;
                $tanggal_akta_perubahan = $this->tgl_indo($getDataInvestor->tanggal_akta_perubahan);
                $notaris_akta_dan_kedudukan_perubahan = ucfirst($getDataInvestor->nama_notaris_akta_perubahan).', '.ucfirst($getDataInvestor->kedudukan_notaris_perubahan);
                $nomor_sk_kemenkumham_perubahan = $getDataInvestor->no_sk_kemenkumham_perubahan;
                $tanggal_sk_kemenkumham_perubahan = $this->tgl_indo($getDataInvestor->tgl_sk_kemenkumham_perubahan);
            }
            $akta_perubahan = $textAkta;

            $tanggal_perjanjian = $this->tgl_indo($tgl_sekarang);
            $nomor_akad = $noAkadInvestor;
            $nominal_investasi = 'Rp' . $nominal_investasi;
            $tanggal_transfer = $this->tgl_indo($gettgl_invest);
            $rekening_imbal_hasil = $rekening;
            $nama_bank = $bank;
            $nama_pemilik_rekening = $pemilik_rekening;

            $nomor_identitas = $nik_pengurus;
            $alamat = $alamat_pengurus;
            $username = $getDataInvestor->username;
            $nomor_telepon = $no_tlp;
            $alamat_email = $getDataInvestor->email;


            $nomor_va_bni = $vaBNI;
            $nomor_va_bsi = $vaBSI;
            $nomor_va_cimb = $vaCIMB;



            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/private/akad_template/WAKALAH_BIL_UJRAH_TEMPLATE_BADAN_HUKUM.docx'));
            $templateProcessor->setValue(
                [
                    'nomor_surat_kuasa',
                    'nama_lender_badan_hukum',
                    'kedudukan_badan_hukum',
                    'alamat_badan_hukum',
                    'alamat_saja',
                    'rukun_tetangga',
                    'rukun_warga',
                    'kelurahan',
                    'kecamatan',
                    'kabupaten',
                    'provinsi',
                    'nama_direktur',
                    'jabatan_direktur',
                    'nama_akta',
                    'nomor_akta',
                    'tanggal_akta',
                    'notaris_dan_kedudukan_akta',
                    'nomor_sk_kemenkumham',
                    'tanggal_sk_kemenkumham',
                    'akta_perubahan',
                    'tanggal_perjanjian',
                    'nomor_akad',
                    'nominal_investasi',
                    'tanggal_transfer',
                    'rekening_imbal_hasil',
                    'nama_bank',
                    'nama_pemilik_rekening',
                    'sebagai_kontak_lainnya',
                    'nama_kontak_lainnya',
                    'nomor_ktp_kontak_lainnya',
                    'nomor_handphone_kontak_lainnya',
                    'alamat_kontak_lainnya',
                    'nomor_identitas',
                    'alamat',
                    'username',
                    'nomor_telepon',
                    'alamat_email',
                    'nomor_va_bni',
                    'nomor_va_bsi',
                    'nomor_va_cimb',
                    'hari_transaksi',
                    'jabatan_BO',
					'nama_BO',
					'identitas_BO',
					'npwp_BO',
					'telepon_BO',
					'alamat_BO',
                    'nomor_akta_perubahan',
                    'tanggal_akta_perubahan',
                    'notaris_akta_dan_kedudukan_perubahan',
                    'nomor_sk_kemenkumham_perubahan',
                    'tanggal_sk_kemenkumham_perubahan'
                ],
                [
                    $noKuasaInvestor,
                    $nama_perusahaan,
                    $kedudukan_badan_hukum,
                    $alamat_badan_hukum,
                    $alamat_saja,
                    $rukun_tetangga,
                    $rukun_warga,
                    $kelurahan,
                    $kecamatan,
                    $kabupaten,
                    $provinsi,
                    $nama_direktur,
                    $jabatan_direktur,
                    $nama_akta,
                    $nomor_akta,
                    $tanggal_akta,
                    $notaris_dan_kedudukan_akta,
                    $nomor_sk_kemenkumham,
                    $tanggal_sk_kemenkumham,
                    $akta_perubahan,
                    $tanggal_perjanjian,
                    $nomor_akad,
                    $nominal_investasi,
                    $tanggal_transfer,
                    $rekening_imbal_hasil,
                    $nama_bank,
                    $nama_pemilik_rekening,
                    $sebagai_kontak_lainnya,
                    $nama_kontak_lainnya,
                    $nomor_ktp_kontak_lainnya,
                    $nomor_handphone_kontak_lainnya,
                    $alamat_kontak_lainnya,
                    $nomor_identitas,
                    $alamat,
                    $username,
                    $nomor_telepon,
                    $alamat_email,
                    $nomor_va_bni,
                    $nomor_va_bsi,
                    $nomor_va_cimb,
                    $hari_transaksi,
                    $jabatan_BO,
					$nama_BO,
					$identitas_BO,
					$npwp_BO,
					$telepon_BO,
					$alamat_BO,
                    $nomor_akta_perubahan,
                    $tanggal_akta_perubahan,
                    $notaris_akta_dan_kedudukan_perubahan,
                    $nomor_sk_kemenkumham_perubahan,
                    $tanggal_sk_kemenkumham_perubahan
                ]
            );

            $templateProcessor->setImageValue('logo_dsi', ['path' => public_path('img/logodsi.png'), 'width' => 200, 'height' => 80]);
            $templateProcessor->setImageValue('foto_ktp_direktur', ['path' => storage_path('app/private/' . $path), 'width' => 555, 'height' => 380]);
            $templateProcessor->setImageValue('foto_ktp_beneficial_owner', ['path' => storage_path('app/private/' . $foto_ktp_beneficial_owner), 'width' => 555, 'height' => 380]);
            $templateProcessor->setImageValue('foto_ktp_kontak_lainnya', ['path' => storage_path('app/private/' . $foto_ktp_kontak_lainnya), 'width' => 555, 'height' => 380]);
            $templateProcessor->setImageValue('foto_npwp_perusahaan', ['path' => storage_path('app/private/' . $getDataInvestor->foto_npwp_perusahaan), 'width' => 555, 'height' => 380]);

            $values = [];
            for ($u = 0; $u < count($getProyekInvestor); $u++) {
                $list = array(
                    'no' => $u + 1,
                    'nama_proyek' => $getProyekInvestor[$u]['nama'],
                    'tanggal_mulai_proyek' => date('d-m-Y', strtotime($getProyekInvestor[$u]['tgl_mulai'])),
                    'tanggal_pendanaan' => date('d-m-Y', strtotime($getProyekInvestor[$u]['tanggal_invest'])),
                    'tanggal_selesai' => date('d-m-Y', strtotime($getProyekInvestor[$u]['tgl_selesai'])),
                    'total_dana' => "Rp. " . number_format($getProyekInvestor[$u]['total_dana']),
                    'imbal_hasil' => $getProyekInvestor[$u]['profit_margin'] . ' %'
                );
                array_push($values, $list);
            }
            $templateProcessor->cloneRowAndSetValues('no', $values);

            Storage::disk('private')->makeDirectory('akad_investor/' . $user_id);
            $templateProcessor->saveAs(storage_path('app/private/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.docx'));
            shell_exec('unoconv -f pdf ' . base_path('storage/app/private/akad_investor/' . $user_id . '/PERJANJIAN_PEMBIAYAAN_WAKALAH_BIL_UJRAH.docx') . ' ' . base_path('storage/app/private/akad_investor/' . $user_id));
            return response()->json(['status' => 'Berhasil']);
        } else {
            return response()->json(['status' => 'Gagal']);
        }
    }

    public function tgl_indo($tanggal)
    {

        if (isset($tanggal) && !empty($tanggal)) {
            $bulan = array(
                1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
            $pecahkan = explode('-', $tanggal);

            // variabel pecahkan 0 = tanggal
            // variabel pecahkan 1 = bulan
            // variabel pecahkan 2 = tahun

            return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
        } else {
            return '';
        }
    }
}
