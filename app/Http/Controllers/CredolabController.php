<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class CredolabController extends Controller
{
    public function login()
    {
        $client = new Client();
        $url = config('app.credo_url') . '/v5.0/account/login';
        $username = config('app.credo_username');
        $password = config('app.credo_password');

        $response = $client->request('POST', $url, [
            'json' => [
                "userEmail" => $username,
                "password" => $password
            ]
        ])->getBody()->getContents();

        return json_decode($response);
    }

    public function refresh_token($refresh_token)
    {
        $client = new Client();
        $url = config('app.credo_url') . '/v5.0/account/login/refreshToken';

        $response = $client->request('POST', $url, [
            'json' => [
                "refreshToken" => $refresh_token,
            ]
        ])->getBody()->getContents();

        return json_decode($response);
    }

    public function get_dataset_insight($access_token, $reference_number)
    {
        $client = new Client();
        $url = config('app.credo_url') . '/v5.0/datasets/' . $reference_number . '/datasetinsight';

        $response = $client->request('GET', $url, [
            'headers' => [
                'accept' => "application/json",
                'Authorization' => "Bearer {$access_token}"
            ],
        ])->getBody()->getContents();

        return json_decode($response);
    }

    public function get_credolab_score_status($access_token, $reference_number)
    {
        $client = new client();
        $url = config('app.credo_url') . '/v5.0/datasets/' . $reference_number . '/datasetinsight';

        try {
            $response = $client->request('GET', $url, [
                'headers' => [
                    'accept' => "application/json",
                    'Authorization' => "Bearer {$access_token}"
                ],
            ]);

            $status_code = $response->getStatusCode();
            if ($status_code == 200) {
                $response = json_decode($response->getBody()->getContents());
                $credolab_score = $response->scores[0]->value;
                $response = [
                    'error' => 'false',
                    'status_code' => '200',
                    'message' => 'credolab score found',
                    'credolab_score' => $credolab_score,
                ];
            }
        } catch (\Exception $th) {
            $response = [
                'error' => 'true',
                'status_code' => '404',
                'message' => 'credolab score not found',
            ];
        }

        return response()->json($response);
    }

    public function get_credolab_score($reference_number)
    {
        $response_login = $this->login();
        $access_token = $response_login->access_token;
        $credolab_score = $this->get_credolab_score_status($access_token, $reference_number);
        return $credolab_score;
    }
}
