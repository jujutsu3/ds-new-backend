<?php

namespace App\Http\Controllers\Pub;

use App\Http\Controllers\Controller;
use App\MasterKodePos;
use Illuminate\Http\Request;

class BorrowerMasterControllerV1 extends Controller
{
    public function getKelurahanByKodePos(Request $request)
    {
        $data = MasterKodePos::query()
            ->where('kode_pos', $request->get('KodePos'))
            ->get([
                'kode_pos',
                'kelurahan'
            ])
            ->pluck('kelurahan')
            ->unique();

        return PublicResponse::success($data);
    }
}
