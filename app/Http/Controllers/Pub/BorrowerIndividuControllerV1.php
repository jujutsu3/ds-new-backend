<?php

namespace App\Http\Controllers\Pub;

use App\Borrower;
use App\BorrowerCorporation;
use App\BorrowerIndividu;
use App\BorrowerKYCStep;
use App\Http\Controllers\Controller;
use App\MasterKodePos;
use App\PublicBorrowerIndividu;
use App\Services\BorrowerService;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class BorrowerIndividuControllerV1 extends Controller
{
    public static function getSystemToRequestMap()
    {
        return [
            BorrowerIndividu::USERNAME => 'username',
            BorrowerIndividu::PASSWORD => 'password',
            BorrowerIndividu::EMAIL => 'email',
            BorrowerIndividu::NAMA => 'nama',
            BorrowerIndividu::JENIS_KELAMIN => 'JenisKelamin',
            BorrowerIndividu::NIK => 'Identitas',
            BorrowerIndividu::TEMPAT_LAHIR => 'TempatLahir',
            BorrowerIndividu::TANGGAL_LAHIR => 'TanggalLahir',
            BorrowerIndividu::NO_TELEPON => 'NomorTelepon',
            BorrowerIndividu::NAMA_IBU => 'NamaIbuKandung',
            BorrowerIndividu::AGAMA => 'Agama',
            BorrowerIndividu::PENDIDIKAN => 'PendidikanTerakhir',
            BorrowerIndividu::STATUS_PERNIKAHAN => 'StatusPernikahan',
            BorrowerIndividu::STATUS_RUMAH => 'StatusRumah',
            BorrowerIndividu::ALAMAT => 'Alamat',
            BorrowerIndividu::KELURAHAN => 'Kelurahan',
            BorrowerIndividu::KODE_POS => 'KodePos',
            BorrowerIndividu::ALAMAT_DOMISILI => 'AlamatDomisili',
            BorrowerIndividu::KELURAHAN_DOMISILI => 'KelurahanDomisili',
            BorrowerIndividu::KODE_POS_DOMISILI => 'KodePosDomisili',
            BorrowerIndividu::STATUS_RUMAH_DOMISILI => 'StatusRumahDomisili',
            BorrowerIndividu::BIDANG_PEKERJAAN => 'BidangPekerjaan',
            BorrowerIndividu::BIDANG_ONLINE => 'BidangPekerjaanOnline',
            BorrowerIndividu::LAMA_USAHA => 'PengalamanKerja',
            BorrowerIndividu::PENGHASILAN_PERBULAN => 'Pendapatan',
            BorrowerIndividu::NPWP => 'NPWP',
            BorrowerIndividu::FOTO_KTP => 'Photo_Identitas',
            BorrowerIndividu::FOTO_DIRI => 'Photo_Selfie',
            BorrowerIndividu::FOTO_DIRI_KTP => 'Photo_Diri_Identitas',
            BorrowerIndividu::FOTO_NPWP => 'NPWP_Image',
            BorrowerIndividu::PEKERJAAN => 'JenisPekerjaan'
        ];
    }

    /**
     * @param string $base64
     * @return \Illuminate\Http\UploadedFile
     */
    public static function base64ToTmpFile(string $base64)
    {
        $base64File = $base64;

        // decode the base64 file
        $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64File));

        // save it to temporary dir first.
        $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
        file_put_contents($tmpFilePath, $fileData);

        // this just to help us get file info.
        $tmpFile = new File($tmpFilePath);

        return new UploadedFile(
            $tmpFile->getPathname(),
            $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            0,
            true // Mark it as test, since the file isn't from real HTTP POST.
        );
    }

    public function getData(Request $request, $borrower)
    {
        $borrower = BorrowerService::getBorrowerIndividu($borrower);

        $borrowerIdOriginalOwner = $borrower->brw_id;
        $check = self::picCheck($borrowerIdOriginalOwner);

        $data = $borrower->getData([
            BorrowerIndividu::USERNAME,
            BorrowerIndividu::PASSWORD,
            BorrowerIndividu::EMAIL,
            BorrowerIndividu::NAMA,
            BorrowerIndividu::JENIS_KELAMIN,
            BorrowerIndividu::NIK,
            BorrowerIndividu::TEMPAT_LAHIR,
            BorrowerIndividu::TANGGAL_LAHIR,
            BorrowerIndividu::NO_TELEPON,
            BorrowerIndividu::NAMA_IBU,
            BorrowerIndividu::AGAMA,
            BorrowerIndividu::PENDIDIKAN,
            BorrowerIndividu::STATUS_PERNIKAHAN,
            BorrowerIndividu::STATUS_RUMAH,
            BorrowerIndividu::ALAMAT,
            BorrowerIndividu::KELURAHAN,
            BorrowerIndividu::KODE_POS,
            BorrowerIndividu::ALAMAT_DOMISILI,
            BorrowerIndividu::KELURAHAN_DOMISILI,
            BorrowerIndividu::KODE_POS_DOMISILI,
            BorrowerIndividu::STATUS_RUMAH_DOMISILI,
            BorrowerIndividu::BIDANG_PEKERJAAN,
            BorrowerIndividu::BIDANG_ONLINE,
            BorrowerIndividu::LAMA_USAHA,
            BorrowerIndividu::PENGHASILAN_PERBULAN,
            BorrowerIndividu::NPWP,
            BorrowerIndividu::FOTO_KTP,
            BorrowerIndividu::FOTO_DIRI,
            BorrowerIndividu::FOTO_DIRI_KTP,
            BorrowerIndividu::FOTO_NPWP,
            BorrowerIndividu::PEKERJAAN
        ]);

        $map = self::getSystemToRequestMap();
        $data = $data->mapWithKeys(function ($value, $key) use ($map) {
            return [$map[$key] => $value];
        });

        BorrowerIndividu::fieldFileAccess($data, $map[BorrowerIndividu::FOTO_NPWP], $check);
        BorrowerIndividu::fieldFileAccess($data, $map[BorrowerIndividu::FOTO_KTP], $check);
        BorrowerIndividu::fieldFileAccess($data, $map[BorrowerIndividu::FOTO_DIRI], $check);
        BorrowerIndividu::fieldFileAccess($data, $map[BorrowerIndividu::FOTO_DIRI_KTP], $check);
        BorrowerIndividu::fieldDateAccess($data, $map[BorrowerIndividu::TANGGAL_LAHIR]);

        return response()->json($data);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\PublicBorrowerIndividu $borrower
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException|\Exception
     */
    public function register(Request $request, PublicBorrowerIndividu $borrower)
    {
        DB::beginTransaction();

        $systemToRequestMap = collect(self::getSystemToRequestMap());
        $requestToSystemMap = $systemToRequestMap->filter()->flip()->toArray();

        $data = collect($request)
            ->intersectByKeys($requestToSystemMap)
            /**
             * Re-key to default map rather than request
             */
            ->mapWithKeys(function ($item, $key) use ($borrower, $requestToSystemMap) {
                $newKey = $requestToSystemMap[$key];
                if (empty($item)) {
                    return [$newKey => $item];
                }

                $keys = [
                    $borrower::FOTO_DIRI,
                    $borrower::FOTO_NPWP,
                    $borrower::FOTO_KTP,
                    $borrower::FOTO_DIRI_KTP,
                ];
                if (in_array($newKey, $keys, true)) {
                    $item = self::base64ToTmpFile($item);
                }

                return [$newKey => $item];
            });

        try {
            /**
             * Get All Rules and add required
             */
            $rules = collect(
                BorrowerIndividu::getRules($borrower->brw_id, array_keys($systemToRequestMap->toArray()))
            )
                ->put(BorrowerIndividu::KELURAHAN, ['kelurahan_by_kode_pos'])
                ->map(function (array $item) {
                    $item[] = 'required';

                    return $item;
                })
                ->toArray();

            $this->extendValidator($requestToSystemMap, $systemToRequestMap);

            Validator::make(
                $data->toArray(),
                $rules,
                [
                    "{$requestToSystemMap[BorrowerIndividu::USERNAME]}.unique" => 'Username Sudah Digunakan',
                    "{$requestToSystemMap[BorrowerIndividu::EMAIL]}.unique" => 'Email Sudah Digunakan',
                ],
                BorrowerIndividu::getAttributeName()
            )
                ->validate();

            /**
             * Fill borrower dengan data dari map
             */
            $borrowerDbMap = BorrowerIndividu::getDbMap();
            $borrower
                ->forceFill(BorrowerService::mapToDatabase($data->toArray(), $borrowerDbMap))
                ->forceFill([
                    // Ref number sebagai pihak ketiga yang mendaftarkan user tersebut
                    'ref_number' => Auth::id(),
                ])
                ->save();

            /**
             * Isi OTP jika baru dibuat / bukan di update
             */
            if ($borrower->wasRecentlyCreated) {
                $borrower->forceFill([
                    'status' => $borrower->status ?: Borrower::STATUS_ACTIVE,
                    'otp' => random_int(100000, 999999),
                    'kyc_step' => BorrowerKYCStep::STEP_COR_ACTIVE,
                    $borrowerDbMap[BorrowerIndividu::PASSWORD] => Hash::make($data[BorrowerIndividu::PASSWORD]),
                ])
                    ->save();
            }

            $borrowerDetailDbMap = BorrowerIndividu::getDetailDbMap();

            $provinsi = MasterKodePos::query()
                ->where($borrowerDetailDbMap[BorrowerIndividu::KELURAHAN], $data[BorrowerIndividu::KELURAHAN])
                ->where($borrowerDetailDbMap[BorrowerIndividu::KODE_POS], $data[BorrowerIndividu::KODE_POS])
                ->first();

            $detilBorrower = BorrowerService::getDetilBorrower($borrower);
            $detilBorrower
                ->forceFill(BorrowerService::mapToDatabase($data->toArray(), $borrowerDetailDbMap))
                ->forceFill([
                    $borrowerDetailDbMap[BorrowerIndividu::TANGGAL_LAHIR] => BorrowerIndividu::fieldDateMutate(
                        $data[BorrowerIndividu::TANGGAL_LAHIR],
                        $detilBorrower->{BorrowerIndividu::TANGGAL_LAHIR}
                    ),
                    $borrowerDetailDbMap[BorrowerIndividu::PROVINSI] => $provinsi->Provinsi,
                    $borrowerDetailDbMap[BorrowerIndividu::KABUPATEN] => $provinsi->Kota,
                    $borrowerDetailDbMap[BorrowerIndividu::KECAMATAN] => $provinsi->kecamatan,
                ])
                ->save();

            $detilBorrower->forceFill([
                  'brw_type' => 1
            ])->save();

            $keys = [
                BorrowerIndividu::FOTO_DIRI,
                BorrowerIndividu::FOTO_NPWP,
                BorrowerIndividu::FOTO_KTP,
                BorrowerIndividu::FOTO_DIRI_KTP,
            ];

            foreach ($keys as $key) {
                if ($data->has($key)) {
                    BorrowerIndividu::upload(
                        $borrowerDetailDbMap[$key],
                        $data->get($key),
                        $detilBorrower,
                        "brw_pic_" . $key
                    );
                }
            }

            $penghasilanBorrowerDbMap = BorrowerIndividu::getPenghasilanDbMap();
            $penghasilanBorrower = BorrowerService::getDetilBorrowerPenghasilan($borrower);
            $penghasilanBorrower
                ->forceFill(BorrowerService::mapToDatabase($data->toArray(), $penghasilanBorrowerDbMap))
                ->save();

            DB::commit();

            return PublicResponse::success([
                'Borrower_ID' => $borrower->brw_id,
            ]);
        } catch (ValidationException $exception) {
            $message = collect($exception->validator->getMessageBag()->getMessages())
                ->mapWithKeys(function ($item, $key) use ($systemToRequestMap) {
                    return [$systemToRequestMap[$key] => $item];
                });

            return PublicResponse::validationError($message);
        } catch (Exception $exception) {
            DB::rollBack();

            if (config('app.debug') === true) {
                throw $exception;
            }

            Log::critical($exception);

            return PublicResponse::generic('99', 'Unhandled Exception');
        }
    }

    /**
     * @param $map
     * @param \Illuminate\Support\Collection $systemToRequestMap
     * @return void
     */
    public function extendValidator($map, Collection $systemToRequestMap): void
    {
        Validator::extend(
            'kelurahan_by_kode_pos',
            function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) use (
                $map,
                $systemToRequestMap
            ) {
                $data = Arr::get($validator->getData(), BorrowerCorporation::KODE_POS);

                return MasterKodePos::query()
                    ->where('kode_pos', $data)
                    ->where('kelurahan', $value)
                    ->exists();
            },
            __('validation.exists')
        );
    }

    private static function picCheck($borrowerIdOriginalOwner)
    {
        return static function ($borrowerId) use ($borrowerIdOriginalOwner) {
            return true;
            return $borrowerId === $borrowerIdOriginalOwner;
        };
    }
}
