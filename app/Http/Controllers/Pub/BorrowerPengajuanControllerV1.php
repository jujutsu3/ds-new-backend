<?php

namespace App\Http\Controllers\Pub;

use App\BorrowerCorporation;
use App\BorrowerPengajuan;
use App\BorrowerTipePendanaan;
use App\Constants\BorrowerPengajuanStatus;
use App\Http\Controllers\Controller;
use App\MasterTujuanPembiayaan;
use App\PublicBorrowerCorporation;
use App\Services\BorrowerService;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class BorrowerPengajuanControllerV1 extends Controller
{
    public static function getSystemToRequestMap()
    {
        return [
            BorrowerPengajuan::TUJUAN => 'pembiayaan_tujuan',
            BorrowerPengajuan::KEBUTUHAN_DANA => 'kebutuhan_dana',
            BorrowerPengajuan::ESTIMASI_MULAI => 'estimasi_mulai',
            BorrowerPengajuan::JANGKA_WAKTU => 'jangka_waktu',
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\PublicBorrowerCorporation|BorrowerCorporation $borrower
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function submit(Request $request, PublicBorrowerCorporation $borrower)
    {
        DB::beginTransaction();

        $requestToSystemMap = collect(self::getSystemToRequestMap());
        $map = $requestToSystemMap->filter()->flip()->toArray();

        $data = collect($request)
            ->intersectByKeys($map)
            ->mapWithKeys(function ($item, $key) use ($map) {
                return [$map[$key] => $item];
            });
        
        $rules = BorrowerPengajuan::getRules($borrower->brw_id, [BorrowerPengajuan::TUJUAN]);
        $rules[BorrowerPengajuan::TUJUAN] = Rule::exists('m_tujuan_pembiayaan', 'id')
            //->whereIn('tipe_id', [BorrowerTipePendanaan::DANA_KONSTRUKSI]);
		;

        Validator::make([
            BorrowerPengajuan::TUJUAN => $request->get($requestToSystemMap[BorrowerPengajuan::TUJUAN]),
        ],
            $rules,
            BorrowerPengajuan::getAttributeName()
        )
            ->validate();

        try {
            $tujuan = MasterTujuanPembiayaan::query()->find(
                $request->get($requestToSystemMap[BorrowerPengajuan::TUJUAN])
            );

            $rules = collect(
                BorrowerPengajuan::getRules($borrower->brw_id, array_keys($requestToSystemMap->toArray()), $tujuan)
            )->map(function (array $item) {
                $item[] = 'required';

                return $item;
            })
                ->toArray();

            Validator::make(
                $data->toArray(),
                $rules,
                [],
                BorrowerPengajuan::getAttributeName()
            )
                ->validate();

            $pengajuanMap = BorrowerPengajuan::getBorrowerDbMap();

            $borrowerPengajuan = BorrowerPengajuan::query()->make();
            $borrowerPengajuan
                ->forceFill(BorrowerService::mapToDatabase($data->toArray(), $pengajuanMap))
                ->forceFill([
                    'brw_id' => $borrower->brw_id,
                    $pengajuanMap[BorrowerPengajuan::TIPE] => $tujuan->tipe_id,
                    $pengajuanMap[BorrowerPengajuan::STATUS] => BorrowerPengajuanStatus::STATUS_BARU,
                    $pengajuanMap[BorrowerPengajuan::ESTIMASI_MULAI] => Carbon::createFromFormat('d-m-Y', $request->get($requestToSystemMap[BorrowerPengajuan::ESTIMASI_MULAI])),
                ])
                ->save();


            $responseProfilePengajuan = DB::select('select update_user_profile_pengajuan(?, ?) as response', [$borrowerPengajuan->pengajuan_id, 'create']);

            DB::commit();

            return PublicResponse::success([
                'Pengajuan_ID' => $borrowerPengajuan->pengajuan_id,
            ]);
        } catch (ValidationException $exception) {
            return PublicResponse::validationFromException($exception);
        } catch (Exception $exception) {
            DB::rollBack();

            if (config('app.debug') === true) {
                throw $exception;
            }

            Log::critical($exception);

            return PublicResponse::generic('99', 'Unhandled Exception');
        }
    }
}
