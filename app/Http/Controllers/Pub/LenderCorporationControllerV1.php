<?php

namespace App\Http\Controllers\Pub;

use App\BorrowerPendanaan;
use App\DetilInvestor;
use App\Http\Controllers\Controller;
use App\PendanaanAktif;
use App\Proyek;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LenderCorporationControllerV1 extends Controller
{
    public function getAvailableProyek(Request $request)
    {
        $pagination = Proyek::query()
            ->where([
                ['lender_class', DetilInvestor::CLASS_PRIVATE],
                ['investor_id', Auth::user()->id],
            ])
            ->paginate();

        $tipeProyek = BorrowerPendanaan::query()
            ->whereIn('id_proyek', collect($pagination->items())->pluck('id'))
            ->pluck('pendanaan_tipe', 'id_proyek');

        $pendanaanAktif = PendanaanAktif::query()
            ->whereIn('proyek_id', collect($pagination->items())->pluck('id'))
            ->get()
            ->groupBy('proyek_id');

        $items = collect($pagination->items())->map(function ($proyek) use ($pendanaanAktif, $tipeProyek) {
            $statusMap = [
                3 => 'Full',
                2 => 'Closed',
            ];
            $dayLeft = $statusMap[$proyek->status] ?? null;
            if ($dayLeft === null) {
                $dayLeft = sprintf("%d hari",
                    Carbon::today()->diffInDays(Carbon::parse($proyek->tgl_selesai_penggalangan)) + 1
                );
            }

            $akadMap = [
                1 => 'Murabahah',
                2 => 'Mudharabah',
                3 => 'MMQ',
            ];

            $all_dana = isset($pendanaanAktif[$proyek->id]) ? $pendanaanAktif->sum('nominal_awal') : 0;

            $profit_explode = explode('.', $proyek->profit_margin);

            return [
                'id' => $proyek->id,
                'nama' => $proyek->nama,
                'imbal_hasil' => $profit_explode[1] === '00'
                    ? $profit_explode[0]
                    : $proyek->profit_margin,
                'harga_paket' => number_format($proyek->harga_paket, 0, ',', '.'),
                'interval' => $proyek->interval,
                'status' => $proyek->status,
                'tipe' => $tipeProyek[$proyek->id] ?? null,
                'dayleft' => $dayLeft,
                'terkumpul' => $proyek->status === 3 || $proyek->status === 2
                    ? 100
                    : number_format(($proyek->terkumpul + $all_dana) / $proyek->total_need * 100, 2),
                'image_url' => '/storage/' . $proyek->gambar_utama,
                // 'tenor'=>$item->tgl_mulai->diffInMonths($item->tgl_selesai)];
                'tenor' => $proyek->tenor_waktu,
                'akad' => $akadMap[$proyek->akad] ?? 'IMBT',
                'alamat' => $proyek->alamat,
                'butuh' => number_format($proyek->total_need, 0, ',', '.'),
                'terkumpul_value' => $proyek->status === 3 || $proyek->status === 2
                    ? $proyek->total_need
                    : number_format($proyek->terkumpul + $all_dana),
                'lender_class' => $proyek->lender_class,
            ];
        });

        return PublicResponse::success($items, null, [
            'PerPage' => $pagination->perPage(),
            'CurrentPage' => $pagination->currentPage(),
            'LastPage' => $pagination->lastPage(),
            'TotalItem' => $pagination->total(),
        ]);
    }
}
