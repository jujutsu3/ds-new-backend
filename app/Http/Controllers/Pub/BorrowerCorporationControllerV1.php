<?php

namespace App\Http\Controllers\Pub;

use App\Borrower;
use App\BorrowerAdministrator;
use App\BorrowerAdministratorInterface;
use App\BorrowerContact;
use App\BorrowerCorporation;
use App\BorrowerKYCStep;
use App\BorrowerPemegangSaham;
use App\BorrowerPengurus;
use App\Http\Controllers\Controller;
use App\MasterKodePos;
use App\PublicBorrowerCorporation;
use App\Services\BorrowerService;
use Exception;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class BorrowerCorporationControllerV1 extends Controller
{
    public static function getSystemToRequestMap()
    {
        return [
            BorrowerCorporation::USERNAME => 'username',
            BorrowerCorporation::EMAIL => 'email',
            BorrowerCorporation::PASSWORD => 'password',
            BorrowerCorporation::NAMA => 'nama_badan_hukum',
            BorrowerCorporation::NO_SURAT_IZIN => 'NIB',
            BorrowerCorporation::NPWP => 'NPWP',
            BorrowerCorporation::FOTO_NPWP => 'NPWP_Image',
            BorrowerCorporation::NO_AKTA_PENDIRIAN => 'NoAktaPendirian',
            BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN => 'TanggalPendirian',
            BorrowerCorporation::NO_AKTA_PERUBAHAN => 'NoAktaPerubahanTerakhir',
            BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN => 'TanggalPerubahanTerakhir',
            BorrowerCorporation::TELEPON => 'TeleponPerusahaan',
            BorrowerCorporation::ALAMAT => 'Alamat',
            // BorrowerCorporation::PROVINSI => 'Provinsi',
            // BorrowerCorporation::KABUPATEN => 'Kota',
            // BorrowerCorporation::KECAMATAN => 'Kecamatan',
            BorrowerCorporation::KELURAHAN => 'Kelurahan',
            BorrowerCorporation::KODE_POS => 'KodePos',
            // BorrowerCorporation::REKENING => '',
            // BorrowerCorporation::BANK => '',
            // BorrowerCorporation::NAMA_PEMILIK_REKENING => '',
            // BorrowerCorporation::BIDANG_PEKERJAAN => '',
            BorrowerCorporation::OMSET_TAHUN_TERAKHIR => 'OmsetTahunTerakhir',
            BorrowerCorporation::TOTAL_ASET_TAHUN_TERAKHIR => 'TotalAsetTahunTerakhir',
            // BorrowerCorporation::LAPORAN_KEUANGAN => '',
        ];
    }

    public static function getSystemToAdministratorRequestMap()
    {
        return [
            BorrowerAdministrator::NAMA => 'Nama',
            BorrowerAdministrator::JENIS_KELAMIN => 'JenisKelamin',
            BorrowerAdministrator::JENIS_IDENTITAS => 'JenisIdentitas',
            BorrowerAdministrator::IDENTITAS => 'Identitas',
            BorrowerAdministrator::TEMPAT_LAHIR => 'TempatLahir',
            BorrowerAdministrator::TANGGAL_LAHIR => 'TanggalLahir',
            BorrowerAdministrator::NO_TELEPON => 'NomorTelepon',
            BorrowerAdministrator::AGAMA => 'Agama',
            BorrowerAdministrator::PENDIDIKAN => 'PendidikanTerakhir',
            BorrowerAdministrator::NPWP => 'NPWP',
            BorrowerAdministrator::JABATAN => 'Jabatan',
            BorrowerAdministrator::ALAMAT => 'Alamat',
            // BorrowerAdministrator::PROVINSI => 'Provinsi',
            // BorrowerAdministrator::KABUPATEN => 'Kota',
            // BorrowerAdministrator::KECAMATAN => 'Kecamatan',
            BorrowerAdministrator::KELURAHAN => 'Kelurahan',
            BorrowerAdministrator::KODE_POS => 'KodePos',
            BorrowerAdministrator::FOTO_KTP => 'Photo_Identitas',
            BorrowerAdministrator::FOTO_PROFILE_KTP => 'Photo_Diri_Identitas',
            BorrowerAdministrator::FOTO_PROFILE => 'Photo_Selfie',
            BorrowerAdministrator::FOTO_NPWP => 'NPWP_Image',
        ];
    }

    /**
     * @param string $base64
     * @return \Illuminate\Http\UploadedFile
     */
    public static function base64ToTmpFile(string $base64)
    {
        $base64File = $base64;

        // decode the base64 file
        $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64File));

        // save it to temporary dir first.
        $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
        file_put_contents($tmpFilePath, $fileData);

        // this just to help us get file info.
        $tmpFile = new File($tmpFilePath);

        return new UploadedFile(
            $tmpFile->getPathname(),
            $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            0,
            true // Mark it as test, since the file isn't from real HTTP POST.
        );
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\PublicBorrowerCorporation|BorrowerCorporation $borrower
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request, PublicBorrowerCorporation $borrower)
    {
        DB::beginTransaction();

        $systemToRequestMap = collect(self::getSystemToRequestMap());
        $requestToSystemMap = $systemToRequestMap->filter()->flip()->toArray();

        $data = collect($request)
            ->intersectByKeys($requestToSystemMap)
            ->mapWithKeys(function ($item, $key) use ($borrower, $requestToSystemMap) {
                $newKey = $requestToSystemMap[$key];
                if (empty($item)) {
                    return [$newKey => $item];
                }

                if ($newKey === $borrower::FOTO_NPWP) {
                    $item = self::base64ToTmpFile($item);
                }

                return [$newKey => $item];
            });

        try {
            $rules = collect(
                BorrowerCorporation::getRules($borrower->brw_id, array_keys($systemToRequestMap->toArray()))
            )
                ->put(BorrowerCorporation::KELURAHAN, ['kelurahan_by_kode_pos'])
                ->map(function (array $item) {
                    $item[] = 'required';

                    return $item;
                })
                ->toArray();

            $this->extendValidator($requestToSystemMap, $systemToRequestMap);

            Validator::make(
                $data->toArray(),
                $rules,
                [
                    "{$requestToSystemMap[BorrowerCorporation::USERNAME]}.unique" => 'Username Sudah Digunakan',
                    "{$requestToSystemMap[BorrowerCorporation::EMAIL]}.unique" => 'Email Sudah Digunakan',
                ],
                BorrowerCorporation::getAttributeName()
            )
                ->validate();

            $borrowerDbMap = BorrowerCorporation::getBorrowerDbMap();
            $borrower
                ->forceFill(BorrowerService::mapToDatabase($data->toArray(), $borrowerDbMap))
                ->forceFill([
                    'ref_number' => Auth::id(),
                    /** @note Password dan status tidak boleh diubah ketika sudah di set pertama kali */
                ])
                ->save();

            if ($borrower->wasRecentlyCreated) {
                $borrower->forceFill([
                    'status' => $borrower->status ?: Borrower::STATUS_ACTIVE,
                    'otp' => random_int(100000, 999999),
                    'kyc_step' => BorrowerKYCStep::STEP_COR_ACTIVE,
                    $borrowerDbMap[BorrowerCorporation::PASSWORD] => Hash::make($data[BorrowerCorporation::PASSWORD]),
                ])
                    ->save();
            }

            $borrowerDetailDbMap = BorrowerCorporation::getBorrowerDetailDbMap();

            $provinsi = MasterKodePos::query()
                ->where($borrowerDetailDbMap[BorrowerCorporation::KELURAHAN], $data[BorrowerCorporation::KELURAHAN])
                ->where($borrowerDetailDbMap[BorrowerCorporation::KODE_POS], $data[BorrowerCorporation::KODE_POS])
                ->first();

            $detilBorrower = BorrowerService::getDetilBorrower($borrower);
            $detilBorrower
                ->forceFill(BorrowerService::mapToDatabase($data->toArray(), $borrowerDetailDbMap))
                ->forceFill([
                    $borrowerDetailDbMap[BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN] => BorrowerCorporation::fieldDateMutate(
                        $data[BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN],
                        $detilBorrower->{BorrowerCorporation::TANGGAL_AKTA_PERUBAHAN}
                    ),
                    $borrowerDetailDbMap[BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN] => BorrowerCorporation::fieldDateMutate(
                        $data[BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN],
                        $detilBorrower->{BorrowerCorporation::TANGGAL_AKTA_PENDIRIAN}
                    ),
                    $borrowerDetailDbMap[BorrowerCorporation::PROVINSI] => $provinsi->Provinsi,
                    $borrowerDetailDbMap[BorrowerCorporation::KABUPATEN] => $provinsi->Kota,
                    $borrowerDetailDbMap[BorrowerCorporation::KECAMATAN] => $provinsi->kecamatan,
                ])
                ->save();

            if ($data->has($key = BorrowerCorporation::FOTO_NPWP)) {
                BorrowerCorporation::upload(
                    $key,
                    $data->get($key),
                    $detilBorrower, "pic_corp_" . $key
                );
            }

            $this->updateAdministrator($request, $borrower);
            $this->updateShareholders($request, $borrower);
            $this->updateContacts($request, $borrower);

            DB::commit();

            return PublicResponse::success([
                'Borrower_ID' => $borrower->brw_id,
            ]);
        } catch (ValidationException $exception) {
            if ($exception->errorBag === 'custom') {
                $message = collect($exception->validator->getMessageBag()->getMessages());

                return PublicResponse::validationError($message);
            }

            $message = collect($exception->validator->getMessageBag()->getMessages())
                ->mapWithKeys(function ($item, $key) use ($systemToRequestMap) {
                    return [$systemToRequestMap[$key] => $item];
                });

            return PublicResponse::validationError($message);
        } catch (Exception $exception) {
            DB::rollBack();

            if (config('app.debug') === true) {
                throw $exception;
            }

            Log::critical($exception);

            return PublicResponse::generic('99', 'Unhandled Exception');
        }
    }

    private function updateGenericPerson(Request $request, Borrower $borrower, BorrowerAdministratorInterface $administrator)
    {
        $requestKey = $administrator::getRequestIdentifier();
        $relationKey = $administrator::getRelationIdentifier();
        $administratorDbMap = $administrator::getAdministratorDbMap();
        $systemToRequestMap = $this->getSystemAdministratorToRequestMap($administrator);
        $requestToSystemMap = $systemToRequestMap->filter()->flip()->toArray();
        try {
            /**
             * @note Tidak dilakukan perubahan format data, untuk memudahkan
             *       proses validasi
             * @see self::getSystemToAdministratorRequestMap()
             * @see BorrowerAdministrator::getAdministratorDbMap()
             */
            $allPengurus = collect($request->get($requestKey))
                ->map(function ($item) use ($requestToSystemMap) {
                    /**
                     * @note Lakukan intersect untuk cleaning data
                     *        dengan format yang sudah disetujui
                     */
                    $data = collect(array_intersect_key($item, $requestToSystemMap))
                        ->map(function ($item, $key) use ($requestToSystemMap) {
                            if (! empty($item)
                                /** @note Ubah base64 ke UploadedFile */
                                && in_array($requestToSystemMap[$key] ?? null, [
                                    BorrowerAdministrator::FOTO_KTP,
                                    BorrowerAdministrator::FOTO_NPWP,
                                    BorrowerAdministrator::FOTO_PROFILE,
                                    BorrowerAdministrator::FOTO_PROFILE_KTP,
                                ], true)) {
                                $item = self::base64ToTmpFile($item);
                            }

                            return $item;
                        });

                    return $data->toArray();
                })
                ->toArray();

            $this->extendAdministratorValidator($administrator, $requestToSystemMap, $systemToRequestMap);

            $rules = collect($administrator::getRules(
                [],
                $borrower->brw_id,
                $borrower->$relationKey()->pluck($administrator->getKeyName())->toArray()
            ))
                ->intersectByKeys($systemToRequestMap->toArray())
                ->put(BorrowerCorporation::KELURAHAN, ['kelurahan_by_kode_pos_admin'])
                ->mapWithKeys(function ($rules, $key) use ($systemToRequestMap, $requestKey) {
                    $ignoredIfNIB = [
                        BorrowerAdministrator::AGAMA,
                        BorrowerAdministrator::JENIS_KELAMIN,
                        BorrowerAdministrator::TEMPAT_LAHIR,
                        BorrowerAdministrator::TANGGAL_LAHIR,
                        BorrowerAdministrator::PENDIDIKAN,
                        BorrowerAdministrator::JABATAN,
                    ];

                    if (in_array($key, $ignoredIfNIB, true)) {
                        $rules[] = sprintf("required_if:%s,%s",
                            "$requestKey.*.{$systemToRequestMap[BorrowerAdministrator::JENIS_IDENTITAS]}",
                            implode(',', [
                                BorrowerAdministrator::JENIS_IDENTITAS_NIK,
                                BorrowerAdministrator::JENIS_IDENTITAS_PASPORT,
                            ])
                        );
                    } else {
                        $rules[] = 'required';
                    }

                    return ["{$requestKey}.*.{$systemToRequestMap[$key]}" => $rules];
                })
                ->merge([
                    $requestKey => ['required', 'array', 'min:1'],
                ])
                ->toArray();

            Validator::make(
                [$requestKey => $allPengurus],
                $rules,
                [
                    "$requestKey.*.{$administratorDbMap[$administrator::IDENTITAS]}.unique" => 'NIK Sudah Digunakan',
                ],
                collect($administrator::getAttributeName())
                    ->intersectByKeys($systemToRequestMap->toArray())
                    ->mapWithKeys(function ($item, $key) use ($requestKey, $systemToRequestMap) {
                        return ["$requestKey.*.$systemToRequestMap[$key]" => $item];
                    })
                    ->toArray()
            )
                ->validate();

            $borrower->$relationKey()->delete();
            foreach ($allPengurus as $key => $data) {
                $data = collect($data)->mapWithKeys(function ($item, $key) use ($requestToSystemMap) {
                    return [$requestToSystemMap[$key] => $item];
                })->toArray();

                $provinsi = MasterKodePos::query()
                    ->where($administratorDbMap[BorrowerCorporation::KELURAHAN], $data[BorrowerCorporation::KELURAHAN])
                    ->where($administratorDbMap[BorrowerCorporation::KODE_POS], $data[BorrowerCorporation::KODE_POS])
                    ->first();

                $admin = $borrower->$relationKey()->make()
                    ->forceFill(BorrowerService::mapToDatabase($data, $administratorDbMap))
                    ->forceFill([
                        'brw_id' => $borrower->brw_id,
                        $administratorDbMap[$administrator::TANGGAL_LAHIR] => $administrator::fieldDateMutate(
                            $data[$administrator::TANGGAL_LAHIR] ?? null,
                            $administrator->tanggal_lahir
                        ),
                        $administratorDbMap[BorrowerCorporation::PROVINSI] => $provinsi->Provinsi,
                        $administratorDbMap[BorrowerCorporation::KABUPATEN] => $provinsi->Kota,
                        $administratorDbMap[BorrowerCorporation::KECAMATAN] => $provinsi->kecamatan,
                    ]);

                $mapRequestToColumnPhoto = array_intersect_key($administratorDbMap, array_flip([
                    $administrator::FOTO_KTP,
                    $administrator::FOTO_PROFILE_KTP,
                    $administrator::FOTO_PROFILE,
                    $administrator::FOTO_NPWP,
                ]));

                foreach ($mapRequestToColumnPhoto as $requestField => $columnKey) {
                    if ($data[$requestField] instanceof UploadedFile) {
                        BorrowerCorporation::upload($columnKey, $data[$requestField], $admin, "pic_corp_{$requestKey}_{$key}_$columnKey");
                    }
                }

                $admin->save();
            }
        } catch (ValidationException $exception) {
            $exception->errorBag = 'custom';

            throw $exception;
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\BorrowerCorporation $borrower
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateAdministrator(Request $request, BorrowerCorporation $borrower)
    {
        $this->updateGenericPerson($request, $borrower, new class extends BorrowerPengurus {
            public static function getRequestIdentifier()
            {
                return 'Pengurus';
            }
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\BorrowerCorporation $borrower
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateShareholders(Request $request, BorrowerCorporation $borrower)
    {
        $this->updateGenericPerson($request, $borrower, new class extends BorrowerPemegangSaham {
            public static function getRequestIdentifier()
            {
                return 'PemegangSaham';
            }
        });
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\BorrowerCorporation $borrower
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateContacts(Request $request, BorrowerCorporation $borrower)
    {
        $this->updateGenericPerson($request, $borrower, new class extends BorrowerContact {
            public static function getRequestIdentifier()
            {
                return 'Kontak';
            }
        });
    }

    /**
     * @param \App\BorrowerAdministrator $administrator
     * @param array $map
     * @param \Illuminate\Support\Collection $systemToRequestMap
     * @return void
     */
    public function extendAdministratorValidator(BorrowerAdministrator $administrator, array $map, Collection $systemToRequestMap): void
    {
        Validator::extend('kelurahan_by_kode_pos_admin', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) use ($administrator, $map, $systemToRequestMap) {
            $prefix = explode('.', $attribute);
            array_pop($prefix);
            $prefix = implode('.', $prefix);
            $kodePosColumn = $systemToRequestMap[$administrator::KODE_POS];

            $data = Arr::get($validator->getData(), "$prefix.$kodePosColumn");

            return MasterKodePos::query()
                ->where('kode_pos', $data)
                ->where('kelurahan', $value)
                ->exists();
        }, __('validation.exists'));
    }

    /**
     * @param $map
     * @param \Illuminate\Support\Collection $systemToRequestMap
     * @return void
     */
    public function extendValidator($map, Collection $systemToRequestMap): void
    {
        Validator::extend('kelurahan_by_kode_pos', function ($attribute, $value, $parameters, \Illuminate\Validation\Validator $validator) use ($map, $systemToRequestMap) {
            $data = Arr::get($validator->getData(), BorrowerCorporation::KODE_POS);

            return MasterKodePos::query()
                ->where('kode_pos', $data)
                ->where('kelurahan', $value)
                ->exists();
        }, __('validation.exists'));
    }

    private function getSystemAdministratorToRequestMap(BorrowerAdministratorInterface $borrowerAdministrator)
    {
        $map = self::getSystemToAdministratorRequestMap();
        
        if ($borrowerAdministrator instanceof BorrowerPemegangSaham) {
            $map[BorrowerPemegangSaham::JENIS_PEMEGANG_SAHAM] = "JenisPemegangSaham";
            $map[BorrowerPemegangSaham::NILAI_SAHAM] = "NilaiSaham";
            $map[BorrowerPemegangSaham::LEMBAR_SAHAM] = "LembarSaham";
        }
        
        return collect($map); 
    }
}
