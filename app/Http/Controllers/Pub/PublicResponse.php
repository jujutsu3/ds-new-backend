<?php

namespace App\Http\Controllers\Pub;

use Illuminate\Validation\ValidationException;

class PublicResponse
{
    public const SUCCESS = '00';
    public const VALIDATION_ERROR = '02';
    public const NOTFOUND = '04';
    public const FORBIDDEN = '03';
    public const ERROR = '99';

    public static function generic($code, $desc, $data = [], $metadata = [])
    {
        return response()->json([
            'StatusCode' => $code,
            'StatusDescription' => $desc,
            'Metadata' => $metadata,
            'Data' => $data,
            'Requests' => request()->all(),
        ]);
    }

    public static function fail($data = [], $message = null)
    {
        return self::generic(self::ERROR, $message ?: 'Gagal', $data);
    }

    public static function success($data = [], $message = null, $metadata = [])
    {
        return self::generic(self::SUCCESS, $message ?: 'Sukses', $data, $metadata);
    }

    public static function validationFromException(ValidationException $exception, $requestMap = [])
    {
        $message = collect($exception->validator->getMessageBag()->getMessages());

        if (! empty($requestMap)) {
            $message = $message
                ->mapWithKeys(function ($item, $key) use ($requestMap) {
                    return [$requestMap[$key] ?? '' => $item];
                });
        }

        return self::validationError($message);
    }

    public static function validationError($data = [], $message = null)
    {
        return self::generic(self::VALIDATION_ERROR, $message ?: 'Error Validasi', $data);
    }

    public static function forbidden($data = [], $message = null)
    {
        return self::generic(self::FORBIDDEN, $message ?: 'Tidak ada akses pada resource tersebut', $data);
    }

    public static function notFound($data = [], $message = null)
    {
        return self::generic(self::NOTFOUND, $message ?: 'Data Tidak Ditemukan', $data);
    }
}
