<?php

    namespace App;
    use Illuminate\Support\Carbon;
    use Illuminate\Database\Eloquent\Model;

    class VerifyShareHolder extends Model
    {
        protected $table = 'verify_shareholder';
        protected $fillable = ['verify_id', 'npwp_company', 'valid_npwp_company', 'company_name', 'valid_company_name', 'nik_share_holder', 'valid_nik_share_holder', 'name_share_holder', 'valid_name_share_holder', 'updated_at'];
    
        public function getCreatedAtAttribute()
        {
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
        }
    
        public function getUpdatedAtAttribute()
        {
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y H:i:s');
        }
    }
