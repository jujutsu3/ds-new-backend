<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class VerifyLog extends Model
{
    protected $table = 'verify_log';
    protected $fillable = ['verify_id', 'trx_id', 'request_data', 'response_data', 'status', 'errors', 'created_by'];

    public function getCreatedAtAttribute(){
        return Carbon::createfromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
    }
}
