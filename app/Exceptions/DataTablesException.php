<?php

namespace App\Exceptions;

use Exception;

class DataTablesException extends Exception
{
    public function __construct($message = "")
    {
        parent::__construct($message);
    }

    public function render()
    {
        return response([
            "error loading data_tables, please refresh page",
        ]);
    }
}
