<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
class VerifyTempatKerja extends Model
{
    protected $table = 'verify_tempat_kerja';
    protected $fillable = ['verify_id', 'nik', 'valid_nik', 'name', 'valid_name', 'valid_company', 'company_name', 'valid_company_name', 'company_phone', 'valid_company_phone', 'updated_at'];
   
    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y H:i:s');
    }
    
}