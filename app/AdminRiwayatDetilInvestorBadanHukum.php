<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatDetilInvestorBadanHukum extends Model
{
    protected $primaryKey = 'hist_investor_id';
    protected $table = 'admin_riwayat_detil_investor_badan_hukum';
    protected $guarded = [];
    public $timestamps = false;
}
