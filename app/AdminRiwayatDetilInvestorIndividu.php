<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatDetilInvestorIndividu extends Model
{
    protected $primaryKey = 'hist_investor_id';
    protected $table = 'admin_riwayat_detil_investor_individu';
    protected $guarded = [];
    public $timestamps = false;
}
