<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerFormKunjunganDomisili extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_form_kunjungan_domisili';
    protected $guarded = [];
}
