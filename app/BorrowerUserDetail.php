<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BorrowerUserDetail extends Model
{
    public const TABLE = 'brw_user_detail';
    protected $primaryKey = 'brw_id';
    protected $table = 'brw_user_detail';
    protected $guard = 'borrower';

    protected $guarded = [];

    public function getAge() {
        return Carbon::parse($this->attributes['tgl_lahir'])->age;
    }
}
