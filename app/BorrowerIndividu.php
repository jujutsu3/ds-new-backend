<?php

namespace App;

use App\Constants\JenisKelamin;
use App\Services\BorrowerFileService;
use App\Services\BorrowerService;
use App\Services\FileService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class BorrowerIndividu extends Borrower implements FieldFileInterface, PublicBorrowerIndividu
{
    use FieldFileHandler;
    use FieldDateHandler;

    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const PASSWORD = 'password';
    public const NAMA = 'nama';
    public const NIK = 'nik';
    public const JENIS_KELAMIN = 'jenis_kelamin';
    public const TEMPAT_LAHIR = 'tempat_lahir';
    public const TANGGAL_LAHIR = 'tanggal_lahir';
    public const NO_TELEPON = 'no_telepon';
    public const NAMA_IBU = 'nama_ibu_kandung';
    public const AGAMA = 'agama';
    public const STATUS_PERNIKAHAN = 'status_pernikahan';
    public const PROVINSI = 'provinsi';
    public const KABUPATEN = 'kabupaten';
    public const KECAMATAN = 'kecamatan';
    public const KELURAHAN = 'kelurahan';
    public const KODE_POS = 'kode_pos';
    public const ALAMAT = 'alamat';
    public const ALAMAT_DOMISILI = 'domisili_alamat';
    public const KELURAHAN_DOMISILI = 'domisili_kelurahan';
    public const KODE_POS_DOMISILI = 'domisili_kode_pos';
    public const STATUS_RUMAH_DOMISILI = 'domisili_status_rumah';
    public const STATUS_RUMAH = 'status_rumah';
    public const PENDIDIKAN = 'pendidikan';
    public const PEKERJAAN = 'pekerjaan';
    public const BIDANG_PEKERJAAN = 'bidang_pekerjaan';
    public const BIDANG_ONLINE = 'bidang_online';
    public const PENGHASILAN_PERBULAN = 'penghasilan_perbulan';
    public const NPWP = 'npwp';
    public const LAMA_USAHA = 'lama_usaha';
    public const MASA_KERJA_TAHUN = 'masa_kerja_tahun';
    public const MASA_KERJA_BULAN = 'masa_kerja_bulan';
    public const NAMA_PERUSAHAAN = 'nama_perusahaan';
    public const NO_TLP_USAHA = 'no_tlp_usaha';
    public const FOTO_KTP = 'foto_ktp';
    public const FOTO_NPWP = 'foto_npwp';
    public const FOTO_DIRI_KTP = 'foto_profile_ktp';
    public const FOTO_DIRI = 'foto_profile';

    public function getData($neededData)
    {
        $detailmap = self::getDetailDbMap();
        $penghasilanMap = self::getPenghasilanDbMap();

        $getter = static function ($model, $map, $columns) {
            $data = [];
            if ($model === null) {
                return $data;
            }

            foreach ($columns as $column) {
                $datum = $map[$column];
                $data[$column] = $model->$datum;
            }

            return $data;
        };

        $responseData = collect();
        if ($neededBorrowerDetail = array_intersect(array_keys($detailmap), $neededData)) {
            $detail = BorrowerService::getDetilBorrower($this);
            $responseData = $responseData->merge(
                $getter($detail, $detailmap, $neededBorrowerDetail)
            );
        }

        if ($neededBorrowerPenghasilan = array_intersect(array_keys($penghasilanMap), $neededData)) {
            $penghasilan = BorrowerService::getDetilBorrowerPenghasilan($this);
            $responseData = $responseData->merge(
                $getter($penghasilan, $penghasilanMap, $neededBorrowerPenghasilan)
            );
        }

        return $responseData;
    }

    /**
     * For brw_user
     *
     * @return string[]
     */
    public static function getDbMap(): array
    {
        return [
            self::USERNAME => 'username',
            self::EMAIL => 'email',
            self::PASSWORD => 'password',
        ];
    }

    /**
     * For brw_user_detail
     *
     * @return string[]
     */
    public static function getDetailDbMap(): array
    {
        return [
            self::NAMA => 'nama',
            self::NIK => 'ktp',
            self::JENIS_KELAMIN => 'jns_kelamin',
            self::TEMPAT_LAHIR => 'tempat_lahir',
            self::TANGGAL_LAHIR => 'tgl_lahir',
            self::NO_TELEPON => 'no_tlp',
            self::NAMA_IBU => 'nm_ibu',
            self::AGAMA => 'agama',
            self::STATUS_PERNIKAHAN => 'status_kawin',
            self::PROVINSI => 'provinsi',
            self::KABUPATEN => 'kota',
            self::KECAMATAN => 'kecamatan',
            self::KELURAHAN => 'kelurahan',
            self::KODE_POS => 'kode_pos',
            self::ALAMAT => 'alamat',
            self::ALAMAT_DOMISILI => 'domisili_alamat',
            self::KELURAHAN_DOMISILI => 'domisili_kelurahan',
            self::KODE_POS_DOMISILI => 'domisili_kd_pos',
            self::STATUS_RUMAH_DOMISILI => 'domisili_status_rumah',
            self::STATUS_RUMAH => 'status_rumah',
            self::PENDIDIKAN => 'pendidikan_terakhir',
            self::PEKERJAAN => 'pekerjaan',
            self::BIDANG_PEKERJAAN => 'bidang_pekerjaan',
            self::BIDANG_ONLINE => 'bidang_online',
            self::NPWP => 'npwp',
            self::FOTO_KTP => 'brw_pic_ktp',
            self::FOTO_NPWP => 'brw_pic_npwp',
            self::FOTO_DIRI_KTP => 'brw_pic_user_ktp',
            self::FOTO_DIRI => 'brw_pic',
        ];
    }

    /**
     * For brw_rekening
     *
     * @return string[]
     */
    public static function getPenghasilanDbMap(): array
    {
        return [
            self::PENGHASILAN_PERBULAN => 'pendapatan_borrower',
            self::LAMA_USAHA => 'usia_perusahaan',
            self::MASA_KERJA_TAHUN => 'masa_kerja_tahun',
            self::MASA_KERJA_BULAN => 'masa_kerja_bulan',
            self::NAMA_PERUSAHAAN => 'nama_perusahaan',
            self::NO_TLP_USAHA => 'no_telp',
        ];
    }

    /**
     * @return string[]
     */
    public static function getAttributeName()
    {
        if (App::isLocale('en')) {
            return [
                self::NAMA => 'Name',
                self::NIK => 'Identity',
                self::JENIS_KELAMIN => 'Gender',
                self::TEMPAT_LAHIR => 'Place of Birth',
                self::TANGGAL_LAHIR => 'Date of Birth',
                self::NO_TELEPON => 'Phone',
                self::NAMA_IBU => 'Mother Maiden Name',
                self::AGAMA => 'Religion',
                self::STATUS_PERNIKAHAN => 'Marital Status',
                self::PROVINSI => 'Province',
                self::KABUPATEN => 'Regency',
                self::KECAMATAN => 'District',
                self::KELURAHAN => 'Sub District',
                self::KODE_POS => 'Postal Code',
                self::ALAMAT => 'Address',
                self::ALAMAT_DOMISILI => 'Residence Address',
                self::KELURAHAN_DOMISILI => 'Residence Sub District',
                self::KODE_POS_DOMISILI => 'Residence Postal Code',
                self::STATUS_RUMAH_DOMISILI => 'Residence Home Onwership Status',
                self::STATUS_RUMAH => 'Home Onwership Status',
                self::PENDIDIKAN => 'Education',
                self::PEKERJAAN => 'Occupation',
                self::BIDANG_PEKERJAAN => 'Field of Work',
                self::BIDANG_ONLINE => 'Online Field of Work',
                self::PENGHASILAN_PERBULAN => 'Income',
                self::NPWP => 'NPWP',
                self::LAMA_USAHA => 'Work Experience / Business Experience',
                self::MASA_KERJA_TAHUN => 'Working Time (Year)',
                self::MASA_KERJA_BULAN => 'Working Time (Month)',
                self::NAMA_PERUSAHAAN => 'Company Name',
                self::NO_TLP_USAHA => 'Business Phone Number',
                static::FOTO_KTP => 'ID Card Photo',
                static::FOTO_DIRI_KTP => 'Selfie ID Card Photo',
                static::FOTO_DIRI => 'Selfie Photo',
                static::FOTO_NPWP => 'NPWP Photo',
            ];
        }

        return [
            self::NAMA => 'Nama',
            self::NIK => 'NIK',
            self::JENIS_KELAMIN => 'Jenis Kelamin',
            self::TEMPAT_LAHIR => 'Tempat Lahir',
            self::TANGGAL_LAHIR => 'Tanggal Lahir',
            self::NO_TELEPON => 'No Telepon',
            self::NAMA_IBU => 'Nama Ibu Kandung',
            self::AGAMA => 'Agama',
            self::STATUS_PERNIKAHAN => 'Status Pernikahan',
            self::PROVINSI => 'Provinsi',
            self::KABUPATEN => 'Kabupaten',
            self::KECAMATAN => 'Kecamatan',
            self::KELURAHAN => 'Kelurahan',
            self::KODE_POS => 'Kode Pos',
            self::ALAMAT => 'Alamat',
            self::ALAMAT_DOMISILI => 'Alamat Domisili',
            self::KELURAHAN_DOMISILI => 'Kelurahan Domisili',
            self::KODE_POS_DOMISILI => 'Kode Pos Domisili',
            self::STATUS_RUMAH_DOMISILI => 'Status Rumah Domisili',
            self::STATUS_RUMAH => 'Status Rumah',
            self::PENDIDIKAN => 'Pendidikan',
            self::PEKERJAAN => 'Pekerjaan',
            self::BIDANG_PEKERJAAN => 'Bidang Pekerjaan',
            self::BIDANG_ONLINE => 'Bidang Pekerjaan Online',
            self::PENGHASILAN_PERBULAN => 'Pendapatan',
            self::NPWP => 'NPWP',
            self::LAMA_USAHA => 'Pengalaman Kerja / Lama Usaha',
            self::MASA_KERJA_TAHUN => 'Masa Kerja Tahun',
            self::MASA_KERJA_BULAN => 'Masa Kerja Bulan',
            self::NAMA_PERUSAHAAN => 'Nama Perusahaan',
            self::NO_TLP_USAHA => 'No Telepon Usaha',
            self::FOTO_KTP => 'Foto Identitas',
            self::FOTO_NPWP => 'Foto NPWP',
            self::FOTO_DIRI_KTP => 'Foto Diri Identitas',
            self::FOTO_DIRI => 'Foto Diri',
        ];
    }

    public static function getRules($borrowerId, array $neededRules = [])
    {
        $rules = [
            self::NAMA => ['max:50'],
            self::EMAIL => ['email', Rule::unique('brw_user', 'email')->ignore($borrowerId, 'brw_id'), 'max:50'],
            self::USERNAME => [
                'alpha_num',
                Rule::unique('brw_user', 'username')->ignore($borrowerId, 'brw_id'),
                'max:50',
            ],
            self::NIK => ['digits:16', Rule::unique('brw_user_detail', 'ktp')->ignore($borrowerId, 'brw_id')],
            self::JENIS_KELAMIN => [Rule::in([JenisKelamin::LAKILAKI, JenisKelamin::PEREMPUAN])],
            self::TEMPAT_LAHIR => ['max:100'],
            self::TANGGAL_LAHIR => ['date_format:d-m-Y'],
            self::AGAMA => [Rule::exists('m_agama', 'id_agama')],
            self::STATUS_PERNIKAHAN => [Rule::exists('m_kawin', 'id_kawin')],
            self::PROVINSI => [Rule::exists('m_kode_pos', 'Provinsi')],
            self::KABUPATEN => [
                function ($attribute, $value, $fail) {
                    if (! MasterKodePos::query()->where([
                        ['Jenis', explode(' ', trim($value))[0] ?? null],
                        ['Kota', substr(strstr($value, " "), 1),],
                    ])->exists()) {
                        $fail("Kabupaten tidak valid");
                    }
                },
            ],
            self::KECAMATAN => [Rule::exists('m_kode_pos', 'kecamatan')],
            self::KELURAHAN => [Rule::exists('m_kode_pos', 'kelurahan')],
            self::KODE_POS => [Rule::exists('m_kode_pos', 'kode_pos')],
            self::ALAMAT => ['max:100'],
            self::STATUS_RUMAH => [Rule::exists('m_kepemilikan_rumah', 'id_kepemilikan_rumah')],
            self::PENDIDIKAN => [Rule::exists('m_pendidikan', 'id_pendidikan')],
            self::PEKERJAAN => [Rule::exists('m_pekerjaan', 'id_pekerjaan')],
            self::BIDANG_PEKERJAAN => [Rule::exists('m_bidang_pekerjaan', 'id_bidang_pekerjaan')],
            self::BIDANG_ONLINE => [Rule::exists('m_online', 'id_online')],
            self::PENGHASILAN_PERBULAN => ['numeric'],
            self::NPWP => [
                'numeric',
                'digits_between:15,16',
                Rule::unique('brw_user_detail', 'npwp')->ignore($borrowerId, 'brw_id'),
            ],
            self::LAMA_USAHA => ['numeric', 'digits_between:0,5'],
            self::MASA_KERJA_TAHUN => ['numeric', 'digits_between:0,5'],
            self::MASA_KERJA_BULAN => ['numeric', 'digits_between:0,5', 'between:0,11'],
            self::NAMA_PERUSAHAAN => ['max:50'],
            self::NO_TLP_USAHA => ['max:15'],
            self::FOTO_KTP => ['mimes:jpeg,jpg,bmp,png'],
            self::FOTO_NPWP => ['mimes:jpeg,jpg,bmp,png'],
            self::FOTO_DIRI_KTP => ['mimes:jpeg,jpg,bmp,png'],
            self::FOTO_DIRI => ['mimes:jpeg,jpg,bmp,png'],
        ];

        if (! empty($neededRules)) {
            return array_intersect_key($rules, array_flip($neededRules));
        }

        return $rules;
    }

    public static function getFileServiceClass(): FileService
    {
        return new BorrowerFileService();
    }

    /**
     * @param string $column
     * @param \Illuminate\Http\UploadedFile $file
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param null $fileName
     * @return bool
     */
    public static function upload(string $column, UploadedFile $file, Model $model, $fileName = null)
    {
        $fileName = $fileName ?: $column;

        try {
            $model->forceFill([
                $column => BorrowerFileService::uploadFile($file, $fileName, $model->brw_id),
            ])
                ->save();
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }
}
