<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterParameter extends Model
{
    public const INVESTOR_KYC_MODE = 'investor_kyc_mode';
    public const BORROWER_KYC_MODE = 'borrower_kyc_mode';

    protected $table = 'm_param';

    public static function getParam($paramName)
    {
        $row = self::query()->where('deskripsi', $paramName)->first();

        if ($row === null) {
            return null;
        }

        return $row->value;
    }
}
