<?php

namespace App;

class InvestorPengurus extends InvestorAdministrator implements InvestorAdministratorInterface, FieldFileInterface
{
    use FieldDateHandler;
    use FieldFileHandler;
    use FieldPhoneHandler;

    public const READABLE_KEY = "pengurus";
    protected $table = 'investor_pengurus';
    protected $primaryKey = 'pengurus_id';
    protected $guarded = ['pengurus_id'];

    public static function getAdministratorDbMap(): array
    {
        $map = parent::getAdministratorDbMap();
        $map[self::NAMA] = 'nm_pengurus';
        $map[self::IDENTITAS] = 'identitas_pengurus';

        return $map;
    }

    public static function getRules(array $neededRules = [], $investorId = null, $adminIds = [])
    {
        $rules = parent::getRules($neededRules, $investorId);
        $map = static::getAdministratorDbMap();
        $rules[static::IDENTITAS] = ['numeric'];

        return $rules;
    }

    public static function getRequestIdentifier()
    {
        return 'admins';
    }

    public static function getRequestIdentifierName()
    {
        return 'Pengurus';
    }
}
