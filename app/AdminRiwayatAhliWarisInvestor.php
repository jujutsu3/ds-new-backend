<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatAhliWarisInvestor extends Model
{
    protected $primaryKey = 'hist_investor_id';
    protected $table = 'admin_riwayat_ahli_waris_investor';
    protected $guarded = [];
    public $timestamps = false;
}
