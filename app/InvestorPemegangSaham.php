<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class InvestorPemegangSaham extends InvestorAdministrator implements InvestorAdministratorInterface, FieldFileInterface
{
    use FieldDateHandler;
    use FieldFileHandler;
    use FieldPhoneHandler;

    public const PMG_SHM_INDIVIDU = 1;
    public const PMG_SHM_BDN_HKM = 2;

    public const TABLE = 'investor_corp_pemegang_saham';

    public const JENIS_PEMEGANG_SAHAM = 'jenis_pemegang_saham';
    public const NILAI_SAHAM = 'nilai_saham';
    public const LEMBAR_SAHAM = 'lembar_saham';
    public const READABLE_KEY = "pemegang saham";
    protected $table = 'investor_corp_pemegang_saham';
    protected $primaryKey = 'pemegang_saham_id';
    protected $guarded = ['pemengang_saham_id'];

    public static function getAdministratorDbMap(): array
    {
        $map = parent::getAdministratorDbMap();
        $map[self::NAMA] = 'nm_pemegang_saham';
        $map[self::IDENTITAS] = 'identitas_pemegang_saham';
        $map[self::JENIS_PEMEGANG_SAHAM] = 'jenis_pemegang_saham';
        $map[self::NILAI_SAHAM] = 'nilai_saham';
        $map[self::LEMBAR_SAHAM] = 'lembar_saham';

        return $map;
    }

    public static function getRules(array $neededRules = [], $investorId = null, $adminIds = [])
    {
        $rules = parent::getRules($neededRules, $investorId);
        $map = static::getAdministratorDbMap();
        $rules[static::JENIS_IDENTITAS] = [
            Rule::in(
                [self::JENIS_IDENTITAS_NIK, self::JENIS_IDENTITAS_PASPORT, self::JENIS_IDENTITAS_NIB]
            ),
        ];
        $rules[self::JENIS_PEMEGANG_SAHAM] = [Rule::in([self::PMG_SHM_BDN_HKM, self::PMG_SHM_INDIVIDU])];
        $rules[self::NILAI_SAHAM] = ['numeric', 'digits_between:0,17'];
        $rules[self::LEMBAR_SAHAM] = ['numeric', 'min:1'];

        return $rules;
    }

    public static function getAttributeName()
    {
        $attributes = parent::getAttributeName();

        if (App::isLocale('en')) {
            $attributes[self::JENIS_PEMEGANG_SAHAM] = 'Shareholders Type';
            $attributes[self::NILAI_SAHAM] = 'Stock Value';
            $attributes[self::LEMBAR_SAHAM] = 'Shares';

            return $attributes;
        }

        $attributes[self::JENIS_PEMEGANG_SAHAM] = 'Jenis Pemegang Saham';
        $attributes[self::NILAI_SAHAM] = 'Nilai Saham';
        $attributes[self::LEMBAR_SAHAM] = 'Lembar Saham';

        return $attributes;
    }

    public static function getRequestIdentifier()
    {
        return 'shareholders';
    }

    public static function getRequestIdentifierName()
    {
        return 'Pemegang Saham';
    }
}
