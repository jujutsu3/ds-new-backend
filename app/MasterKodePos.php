<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MasterKodePos extends Model
{
    protected $table = 'm_kode_pos';
    protected $primaryKey = 'id_kode_pos';
}
