<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use App\BniEnc;
use App\RekeningInvestor;
use App\Investor;
use DB;

class GenerateVABank_BSI implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $no_va, $userid, $invid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($no_va, $userid, $invid)
    {
        $this->no_va = $no_va;
        $this->userid = $userid;
        $this->invid = $invid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $date = \Carbon\Carbon::now()->addYear(4);

        $data_user = Investor::where('username', $this->userid)->first();


        $data = [
            'type' => 'createbilling',
            'client_id' => config('app.bsi_id'),
            'trx_id' => date("YmdHis") . "-" . $data_user->id,
            'trx_amount' => '0',
            'customer_name' => $data_user->detilInvestor->nama_investor,
            'customer_email' => $data_user->email,
            'customer_phone' => $data_user->detilInvestor->phone_investor,
            'virtual_account' =>  $data_user->detilInvestor->getVa(),
            'datetime_expired' => $date->format('Y-m-d') . 'T' . $date->format('H:i:sP'),
            'billing_type' => 'o',
            'description' => 'VA LENDER ' . $data_user->detilInvestor->getVa()
        ];
        $encrypted = BniEnc::encrypt($data, config('app.bsi_id'), config('app.bsi_key'));

        // insert log db app
        $insertLog = DB::table("log_db_app")->insert(array(
            "file_name" => __FILE__,
            "line" => __LINE__,
            "description" => "Try to send " . json_encode($data),
            "created_at" => date("Y-m-d H:i:s")
        ));

        $client = new Client(); //GuzzleHttp\Client
        $result = $client->post(config('app.bsi_url') . "bni/register", [
            'json' => [
                'client_id' => config('app.bsi_id'),
                'data' => $encrypted,
            ]
        ]);

        $result = json_decode($result->getBody()->getContents());
        $insertLog = DB::table("log_db_app")->insert(array(
            "file_name" => __FILE__,
            "line" => __LINE__,
            "description" => "respon BSI " . json_encode($result),
            "created_at" => date("Y-m-d H:i:s")
        ));

        if ($result->status !== '000') {
            // $kdbank = config('app.bsi_code_bank');
            // DB::table("detil_rekening_investor")->insert(array(
            //     'investor_id' => $this->invid,
            //     'va_number' => "",
            //     'last_amount' => 0,
            //     'kode_bank' => $kdbank,
            //     'type' => 'V',
            //     'status' => 'A',
            //     'created_at' => date("Y-m-d H:i:s")
            // ));

            // insert log db app
            DB::table("log_db_app")->insert(array(
                "file_name" => __FILE__,
                "line" => __LINE__,
                "description" => "Gagal Generate VA BSI " . config('app.bsi_code_bank') . " " . $this->invid . " " . $data_user->detilInvestor->getVa() . " RESP=" . json_encode($result) . " DETAIL=" . BniEnc::decrypt($result->data, config('app.bsi_id'), config('app.bsi_key')),
                "created_at" => date("Y-m-d H:i:s")
            ));
            return $result->message;
        } else {
            $decrypted = BniEnc::decrypt($result->data, config('app.bsi_id'), config('app.bsi_key'));
            try {
                DB::table("detil_rekening_investor")->insert(array(
                    'investor_id' => $this->invid,
                    'va_number' => config('app.bsi_id') . $decrypted['virtual_account'],
                    'last_amount' => 0,
                    'kode_bank' => config('app.bsi_code_bank'),
                    'type' => 'V',
                    'status' => 'A',
                    'created_at' => date("Y-m-d H:i:s")
                ));
                // insert log db app
                DB::table("log_db_app")->insert(array(
                    "file_name" => __FILE__,
                    "line" => __LINE__,
                    "description" => "Sukses Generate VA BSI " . $this->invid . " " . $decrypted['virtual_account'] . " " . $this->userid,
                    "created_at" => date("Y-m-d H:i:s")
                ));

                return 'VA Generate Success!';
            } catch (\Illuminate\Database\QueryException $e) {
                DB::table("log_db_app")->insert(array(
                    "file_name" => __FILE__,
                    "line" => __LINE__,
                    "description" => "Generate VA BSI Already Exist on detil_rekening_investor " . $this->invid . " " . $decrypted['virtual_account'] . " " . $this->userid,
                    "created_at" => date("Y-m-d H:i:s")
                ));

                return 'VA Generate Gagal!';
            }
        }
    }
}
