<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use DB;

class Reminder_Borrower_SMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $no_va, $userid, $invid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($no_va, $userid, $invid)
    {
        $this->no_va = $no_va;
        $this->userid = $userid;
        $this->invid = $invid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $from               = "DANASYARIAH";
        $username           = config("app.SMS1_BROADCAST_USER");
        $password           = config("app.SMS1_BROADCAST_PASSWORD");  
        $postUrl            = config("app.SMS1_BROADCAST_URL");
        
        $text               = config("app.SMS1_BROADCAST_REMINDER1_TMPLT");
        
        $pecah              = explode(",", $to);
        $jumlah             = count($pecah);


        try {

            for ($i = 0; $i < $jumlah; $i++) {
         
		            if (substr($pecah[$i], 0, 2) == "62" || substr($pecah[$i], 0, 3) == "+62") {
		                $pecah = $pecah;
		            } elseif (substr($pecah[$i], 0, 1) == "0") {
		                $pecah[$i][0] = "X";
		                $pecah = str_replace("X", "62", $pecah);
		            } else {
		                echo "Invalid mobile number format";
		            }
		            $destination = array("to" => $pecah[$i]);
		            $message     = array(
		                "from" => $from,
		                "destinations" => $destination,
		                "text" => $text,
		                "smsCount" => 20
		            );
		            $postData           = array("messages" => array($message));
		            $postDataJson       = json_encode($postData);
		            $ch                 = curl_init();
		            $header             = array("Content-Type:application/json", "Accept:application/json");

		            curl_setopt($ch, CURLOPT_URL, $postUrl);
		            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
		            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
		            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		            curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
		            curl_setopt($ch, CURLOPT_POST, 1);
		            curl_setopt($ch, CURLOPT_POSTFIELDS, $postDataJson);
		            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		            $response = curl_exec($ch);
		            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		            $responseBody = json_decode($response);
		            curl_close($ch);
		        }

            DB::table("log_db_app")->insert(array(
                "file_name" => __FILE__,
                "line" => __LINE__,
                "description" => "Sukses SMSB " . $this->invid . " " . $this->no_va . " " . $this->userid,
                "created_at" => date("Y-m-d H:i:s")
            ));
        } catch (\Illuminate\Database\QueryException $e) {

            DB::table("log_db_app")->insert(array(
                "file_name" => __FILE__,
                "line" => __LINE__,
                "description" => "Gagal SMSB " . $this->invid . " " . $this->no_va . " " . $this->userid,
                "created_at" => date("Y-m-d H:i:s")
            ));
        }



        $i++;
    }
}
