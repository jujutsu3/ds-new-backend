<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use App\DetilInvestor;
use DB;
use DateTime;

class SyncContactCenterLender_Create_Single implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $investor_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        //
        $this->investor_id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $date = new DateTime;
        $date->modify('-5 minutes');
        $formatted_date = $date->format('Y-m-d H:i:s');

        $result = DetilInvestor::leftJoin('investor', 'investor.id', '=', 'detil_investor.investor_id')
            ->leftJoin('m_provinsi_kota', 'kode_kota', '=', 'detil_investor.kota_investor')
            ->leftJoin('m_negara', 'id_negara', '=', 'detil_investor.domisili_negara')
            ->where('investor.id', '=', $this->investor_id)->get();

        $data = array();
        foreach ($result as $row) {
            # code...

            $cust_id = $row['investor_id'];
            $cust_fname = $row['nama_investor'];
            $cust_lname = '';
            $phone_number = $row['phone_investor'];
            $cust_email_id = $row['email'];
            $Customer_ID_DSI = $row['id'];
            $cust_address = $row['alamat_investor'] . " " . $row['kelurahan'] . " " . $row['kecamatan'] . " " .
                $row['nama_kota'] . " " . $row['nama_provinsi'] . " " . $row['negara'];

            if (strlen($cust_address) > 300)
                $cust_address = substr($cust_address, 0, 300);


            $element_data = array(
                [
                    'cust_fname'     => $cust_fname,
                    'cust_lname'     => $cust_lname,
                    'phone_number'   => $phone_number,
                    'cust_address'   => $cust_address,
                    'cust_email_id'  => $cust_email_id,
                    'agent_id'       => '',
                    'cust_priority'   => 'LOW',
                    'Customer_ID_DSI' => $Customer_ID_DSI
                ]
            );
        } // for each

        $insertLog = DB::table("log_db_app")->insert(array(
            "file_name" => __FILE__,
            "line" => __LINE__,
            "description" => "Try to send: " . json_encode($element_data),
            "created_at" => date("Y-m-d H:i:s")
        ));

        $client = new Client();
        $arr = [
            'token' => config('app.CC_TOKEN_ID'),
            'tenant_id' => config('app.CC_TENANT_ID'),
            'customer_group_id' => config('app.CC_DEFAULT_CUSTGROUP'),
            'data' => json_encode($element_data)
        ];

        $request = $client->post(config('app.CC_BASE_URL') . '/Tenant/Create_CustomersAsync', [
            'headers' => [

                'Content-Type' => 'application/json',
                'api_key' => config('app.CC_API_KEY'),
                'api_secret' => config('app.CC_API_SECRET')

            ],

            'json' => $arr

        ]);

        $response = $request->getBody()->getContents();

        $insertLog = DB::table("log_db_app")->insert(array(
            "file_name" => __FILE__,
            "line" => __LINE__,
            "description" => "REQ:" . $json_encode($arr) . " RESULT:" . $response,
            "created_at" => date("Y-m-d H:i:s")
        ));

        $response_decode = json_decode($response);
        if ($response_decode->{'result'} == 'true') {


            if ($response_decode->{'payload'}->{'primetelco'}->{'status'} == 'OK') {


                return $response;
            }
        }
    }
}
