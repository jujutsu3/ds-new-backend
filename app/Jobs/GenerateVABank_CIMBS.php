<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Client;
use App\RekeningInvestor;
use DB;

class GenerateVABank_CIMBS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $no_va, $userid, $invid;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($no_va, $userid, $invid)
    {
        $this->no_va = $no_va;
        $this->userid = $userid;
        $this->invid = $invid;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $codebankArray = [config('app.cimbs_id')];
        $corp_codeArray = [config('app.cimbs_id_comp_code')];



        $i = 0;


        $bank = $codebankArray[$i];
        $corp_code = $corp_codeArray[$i];

        try {

            DB::table("detil_rekening_investor")->insert(array(
                'investor_id' => $this->invid,
                'va_number' => $corp_code . $this->no_va,
                'last_amount' => 0,
                'kode_bank' => $bank,
                'type' => 'V',
                'status' => 'A',
                'created_at' => date("Y-m-d H:i:s")
            ));

            DB::table("log_db_app")->insert(array(
                "file_name" => __FILE__,
                "line" => __LINE__,
                "description" => "Sukses Generate VA CIMBS " . $this->invid . " " . $this->no_va . " " . $this->userid,
                "created_at" => date("Y-m-d H:i:s")
            ));
        } catch (\Illuminate\Database\QueryException $e) {

            DB::table("log_db_app")->insert(array(
                "file_name" => __FILE__,
                "line" => __LINE__,
                "description" => "VA Already Created CIMBS " . $this->invid . " " . $this->no_va . " " . $this->userid,
                "created_at" => date("Y-m-d H:i:s")
            ));
        }



        $i++;
    }
}
