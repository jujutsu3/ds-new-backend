<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerFormInterviewHrd extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_form_interview_hrd';
    protected $guarded = [];
}
