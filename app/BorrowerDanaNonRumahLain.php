<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDanaNonRumahLain extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'brw_pendanaan_non_rumah_lain';
    protected $guard = 'borrower';

    protected $fillable = [
        'pengajuan_id', 'bank', 'plafond', 'jangka_waktu', 'outstanding', 'angsuran', 'updated_at'
    ];
}
