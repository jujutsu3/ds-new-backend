<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDokumenLegalitas extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_dokumen_ceklis';
    protected $guarded = [];
}
