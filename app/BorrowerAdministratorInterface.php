<?php

namespace App;

interface BorrowerAdministratorInterface
{
    public function getData($neededData);

    public static function getRequestIdentifier();

    public static function getRelationIdentifier();

    public static function getAdministratorDbMap(): array;

    public static function getRules(array $neededRules = [], $borrowerId = null, $adminIds = []);

    public static function getAttributeName();
}
