<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerPendanaanRumahLain extends Model
{
    public const STATUS_BELUM_LUNAS = 1;
    public const STATUS_LUNAS = 2;
    protected $primaryKey = 'id';
    protected $table = 'brw_pendanaan_rumah_lain';
    protected $guarded = [];
}
