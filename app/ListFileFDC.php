<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListFileFDC extends Model
{
    protected $table = 'list_file_fdc';
    protected $fillable = ['filename', 'version', 'status_pengiriman', 'status_file', 'parent', 'tanggal_file','tanggal_upload', 'bulan','tahun'];

}
