<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerTipePendanaan extends Model
{
    public const TABLE = 'brw_tipe_pendanaan';
    public const DANA_KONSTRUKSI = 1;
    public const DANA_RUMAH = 2;
    public const DANA_MATERIAL=3;

    public static $enabledPembiayaan = [
        BorrowerDetails::TYPE_INDIVIDU => [
            self::DANA_RUMAH => 'Dana Rumah',
            self::DANA_KONSTRUKSI => 'Dana Konstruksi',
            self::DANA_MATERIAL=> 'Dana Material'
        ],
        BorrowerDetails::TYPE_CORPORATION => [
            self::DANA_KONSTRUKSI => 'Dana Konstruksi',
            self::DANA_MATERIAL=> 'Dana Material'
        ],
    ];

    protected $table = 'brw_tipe_pendanaan';
    protected $primaryKey = 'tipe_id';

    public function tujuanPembiayaan()
    {
        return $this->hasMany(MasterTujuanPembiayaan::class, 'tipe_id', 'tipe_id');
    }
}
