<?php

namespace App;

// use Illuminate\Auth\Passwords\CanResetPassword;

use App\Services\FileService;
use App\Services\LenderFileService;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\File\File;

class InvestorCorporation extends Investor implements FieldFileInterface, PublicInvestorCorporation
{
    use FieldFileHandler;
    use FieldDateHandler;
    use FieldPhoneHandler;

    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const NAMA = 'nama';
    public const NO_SURAT_IZIN = 'no_surat_izin';
    public const NPWP = 'npwp';
    public const NO_AKTA_PENDIRIAN = 'no_akta_pendirian';
    public const TANGGAL_AKTA_PENDIRIAN = 'tanggal_akta_pendirian';
    public const NO_AKTA_PERUBAHAN = 'no_akta_perubahan';
    public const TANGGAL_AKTA_PERUBAHAN = 'tanggal_akta_perubahan';
    public const TELEPON = 'telepon';
    public const ALAMAT = 'alamat';
    public const PROVINSI = 'provinsi';
    public const KABUPATEN = 'kabupaten';
    public const KECAMATAN = 'kecamatan';
    public const KELURAHAN = 'kelurahan';
    public const KODE_POS = 'kode_pos';
    public const REKENING = 'rekening';
    public const BANK = 'bank';
    public const NAMA_PEMILIK_REKENING = 'nama_pemilik';
    public const BIDANG_PEKERJAAN = 'bidang_pekerjaan';
    public const OMSET_TAHUN_TERAKHIR = 'omset_tahun_terakhir';
    public const TOTAL_ASET_TAHUN_TERAKHIR = 'total_aset_tahun_terakhir';
    public const FOTO_NPWP = 'foto_npwp';
    public const LAPORAN_KEUANGAN = 'laporan_keuangan';
    public const LONG = 'long';
    public const LAT = 'lat';
    public const ALT = 'alt';
    public const BERKEDUDUKAN = 'berkedudukan';
    public const NAMA_NOTARIS_AKTA_PENDIRIAN = 'nama_notaris_akta_pendirian';
    public const KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN = 'kedudukan_notaris_akta_pendirian';
    public const NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN = 'no_sk_kemenkumham_akta_pendirian';
    public const TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN = 'tanggal_sk_kemenkumham_akta_pendirian';
    public const NAMA_NOTARIS_AKTA_PERUBAHAN = 'nama_notaris_akta_perubahan';
    public const KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN = 'kedudukan_notaris_akta_perubahan';
    public const NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN = 'no_sk_kemenkumham_akta_perubahan';
    public const TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN = 'tanggal_sk_kemenkumham_akta_perubahan';

    protected $primaryKey = 'id';

    public function rekeningInvestor()
    {
        return $this->hasOne(RekeningInvestor::class, 'investor_id', 'id');
    }

    public function detilInvestor()
    {
        return $this->hasOne(DetilInvestor::class, 'investor_id', 'id');
    }

    public function getDataSingle(string $neededData)
    {
        return $this->getData([$neededData])[$neededData] ?? null;
    }

    public function getData($neededData)
    {
        $detilInvestorMap = self::getDetilInvestorMap();
        $detilInvestorLokasiMap = self::getLokasiMap();
        $investorMap = self::getInvestorMap();

        $getter = static function ($model, $map, $columns) {
            $data = [];
            if ($model === null) {
                return $data;
            }

            foreach ($columns as $column) {
                $datum = $map[$column];
                $data[$column] = $model->$datum;
            }

            return $data;
        };

        $responseData = collect();
        if ($neededDetilInvestor = array_intersect(array_keys($investorMap), $neededData)) {
            $responseData = $responseData->merge($getter($this, $investorMap, $neededDetilInvestor));
        }

        if ($neededDetilInvestor = array_intersect(array_keys($detilInvestorMap), $neededData)) {
            $this->load('detilInvestor');
            $responseData = $responseData->merge(
                $getter($this->detilInvestor, $detilInvestorMap, $neededDetilInvestor)
            );
        }

        if ($neededLokasi = array_intersect(array_keys($detilInvestorLokasiMap), $neededData)) {
            $this->load('location');
            $responseData = $responseData->merge($getter($this->location, $detilInvestorLokasiMap, $neededLokasi));
        }

        return $responseData;
    }

    public static function validation($param)
    {

    }

    /**
     * @return string[]
     */
    public static function getDetilInvestorMap(): array
    {
        return [
            self::NAMA => 'nama_perusahaan',
            self::NO_SURAT_IZIN => 'nib',
            self::NPWP => 'npwp_perusahaan',
            self::NO_AKTA_PENDIRIAN => 'no_akta_pendirian',
            self::TANGGAL_AKTA_PENDIRIAN => 'tgl_berdiri',
            self::NO_AKTA_PERUBAHAN => 'nomor_akta_perubahan',
            self::TANGGAL_AKTA_PERUBAHAN => 'tanggal_akta_perubahan',
            self::TELEPON => 'telpon_perusahaan',

            self::ALAMAT => 'alamat_perusahaan',
            self::PROVINSI => 'provinsi_perusahaan',
            self::KABUPATEN => 'kota_perusahaan',
            self::KECAMATAN => 'kecamatan_perusahaan',
            self::KELURAHAN => 'kelurahan_perusahaan',
            self::KODE_POS => 'kode_pos_perusahaan',

            self::REKENING => 'rekening',
            self::BANK => 'bank_investor',
            self::NAMA_PEMILIK_REKENING => 'nama_pemilik_rek',

            self::BIDANG_PEKERJAAN => 'bidang_pekerjaan',
            self::OMSET_TAHUN_TERAKHIR => 'omset_tahun_terakhir',
            self::TOTAL_ASET_TAHUN_TERAKHIR => 'tot_aset_tahun_terakhr',

            self::FOTO_NPWP => 'foto_npwp_perusahaan',
            self::LAPORAN_KEUANGAN => 'laporan_keuangan',

            self::BERKEDUDUKAN => 'kedudukan_perusahaan',
            self::NAMA_NOTARIS_AKTA_PENDIRIAN => 'nama_notaris_akta_pendirian',
            self::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN => 'kedudukan_notaris_pendirian',
            self::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'no_sk_kemenkumham_pendirian',
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'tgl_sk_kemenkumham_pendirian',
            self::NAMA_NOTARIS_AKTA_PERUBAHAN => 'nama_notaris_akta_perubahan',
            self::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN => 'kedudukan_notaris_perubahan',
            self::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'no_sk_kemenkumham_perubahan',
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'tgl_sk_kemenkumham_perubahan',
        ];
    }

    /**
     * @return string[]
     */
    public static function getInvestorMap(): array
    {
        return [
            self::USERNAME => 'username',
            self::EMAIL => 'email',
        ];
    }

    /**
     * @return string[]
     */
    public static function getLokasiMap(): array
    {
        return [
            self::LONG => 'longitude',
            self::LAT => 'latitude',
            self::ALT => 'altitude',
        ];
    }

    public function getVANumber()
    {
        return substr(str_pad($this->getDataSingle(self::NO_SURAT_IZIN), 12, '0', STR_PAD_RIGHT), -12);
    }

    /**
     * @return string[]
     */
    public static function getAttributeName()
    {
        if (App::isLocale('en')) {
            return [
                self::USERNAME => 'Username',
                self::EMAIL => 'Email',
                self::NAMA => 'Name',
                self::NO_SURAT_IZIN => 'License No',
                self::NPWP => 'NPWP',
                self::NO_AKTA_PENDIRIAN => 'Deed of Establishment No',
                self::TANGGAL_AKTA_PENDIRIAN => 'Deed of Establishment Date',
                self::TELEPON => 'Phone',
                self::ALAMAT => 'Address',
                self::PROVINSI => 'Provinsi',
                self::KABUPATEN => 'Regency',
                self::KECAMATAN => 'District',
                self::KELURAHAN => 'Sub District',
                self::KODE_POS => 'Postal Code',
                self::REKENING => 'Bank Account',
                self::NAMA_PEMILIK_REKENING => 'Bank Account Owner Name',
                self::OMSET_TAHUN_TERAKHIR => 'Last Year Turnover',
                self::TOTAL_ASET_TAHUN_TERAKHIR => 'Total Assets Last Year',
                self::FOTO_NPWP => 'NPWP Photo',
                self::NO_AKTA_PERUBAHAN => 'Deed of Change No',
                self::TANGGAL_AKTA_PERUBAHAN => 'Deed of Change Date',
                self::BANK => 'Bank',
                self::BIDANG_PEKERJAAN => 'Field of Work',
                self::LONG => 'Long',
                self::LAT => 'Lat',
                self::ALT => 'Alt',
                self::LAPORAN_KEUANGAN => 'Financial Statements',
                self::BERKEDUDUKAN => 'Domicile',
                self::NAMA_NOTARIS_AKTA_PENDIRIAN => "Deed of Establishment Notary's Name",
                self::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN => 'Deed of Establishment Notary\'s Domicile',
                self::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'Deed of Establishment Kemenkumham Certificate No',
                self::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'Deed of Establishment Kemenkumham Certificate Date',
                self::NAMA_NOTARIS_AKTA_PERUBAHAN => 'Deed of Change Notary\'s Name',
                self::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN => 'Deed of Change Notary\'s Domicile',
                self::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'Deed of Change Kemenkumham Certificate No',
                self::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'Deed of Change Kemenkumham Certificate Date',
            ];
        }
        
        return [
            self::NAMA => 'Nama',
            self::NO_SURAT_IZIN => 'No Surat Izin',
            self::NPWP => 'NPWP',
            self::NO_AKTA_PENDIRIAN => 'No Akta Pendirian',
            self::TANGGAL_AKTA_PENDIRIAN => 'Tanggal Akta Pendirian',
            self::NO_AKTA_PERUBAHAN => 'No Akta Perubahan',
            self::TANGGAL_AKTA_PERUBAHAN => 'Tanggal Akta Perubahan',
            self::TELEPON => 'Telepon',
            self::ALAMAT => 'Alamat',
            self::PROVINSI => 'Provinsi',
            self::KABUPATEN => 'Kabupaten',
            self::KECAMATAN => 'Kecamatan',
            self::KELURAHAN => 'Kelurahan',
            self::KODE_POS => 'Kode Pos',
            self::REKENING => 'Rekening',
            self::BANK => 'Bank',
            self::NAMA_PEMILIK_REKENING => 'Nama Pemilik',
            self::BIDANG_PEKERJAAN => 'Bidang Pekerjaan',
            self::OMSET_TAHUN_TERAKHIR => 'Omset Tahun Terakhir',
            self::TOTAL_ASET_TAHUN_TERAKHIR => 'Total Aset Tahun Terakhir',
            self::FOTO_NPWP => 'Foto NPWP',
            self::LONG => 'Long',
            self::LAT => 'Lat',
            self::ALT => 'Alt',
            self::LAPORAN_KEUANGAN => 'Laporan Keuangan',
            self::BERKEDUDUKAN => 'Berkedudukan',
            self::NAMA_NOTARIS_AKTA_PENDIRIAN => 'Nama Notaris Akta Pendirian',
            self::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN => 'Kedudukan Notaris Akta Pendirian',
            self::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'No SK Kemenkumham Akta Pendirian',
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'Tanggal SK Kemenkumham Akta Pendirian',
            self::NAMA_NOTARIS_AKTA_PERUBAHAN => 'Nama Notaris Akta Perubahan',
            self::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN => 'Kedudukan Notaris Akta Perubahan',
            self::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'No SK Kemenkumham Akta Perubahan',
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'Tanggal SK Kemenkumham Akta Perubahan',
        ];
    }

    public static function getRules($investorId, array $neededRules = [])
    {
        Validator::extend(
            'mimes_ext',
            function ($attribute, File $file, $parameters, \Illuminate\Validation\Validator $validator) {
                $isValid = $validator->validateMimes($attribute, $file, $parameters);

                if ($isValid === false && $file instanceof UploadedFile) {
                    $isValid = in_array($file->getClientOriginalExtension(), Arr::wrap($parameters));
                }

                return $isValid;
            }
        );

        Validator::replacer(
            'mimes_ext',
            function ($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
                return $validator->makeReplacements(__('validation.mimes'), $attribute, 'mimes', $parameters);
            }
        );

        Validator::extend(
            'after_ext',
            function ($attribute, $date, $parameters, \Illuminate\Validation\Validator $validator) {
                $data = $validator->getData();
                if (! ($after = $data[$parameters[0]] ?? null)) {
                    return true;
                }

                return $validator->validateAfter(
                    $attribute,
                    Carbon::createFromFormat('d-m-Y', $date)->toDateString(),
                    [Carbon::createFromFormat('d-m-Y', $after)->toDateString()]
                );
            }
        );

        Validator::replacer(
            'after_ext',
            function ($message, $attribute, $rule, $parameters, \Illuminate\Validation\Validator $validator) {
                return $validator->makeReplacements(__('validation.after'), $attribute, 'after', $parameters);
            }
        );

        $rules = [
            'nama' => ['max:191'],
            'no_surat_izin' => ['numeric', 'digits_between:0,13'],
            'npwp' => [
                'digits_between:15,16',
                Rule::unique('detil_investor', 'npwp_perusahaan')->ignore($investorId, 'investor_id'),
            ],
            self::BERKEDUDUKAN => [Rule::exists('m_provinsi_kota', 'kode_kota')],
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN => ['date_format:d-m-Y'],
            self::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN => [Rule::exists('m_provinsi_kota', 'kode_kota')],
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN => [
                'date_format:d-m-Y',
                'after_ext:' . self::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN,
            ],
            self::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN => [Rule::exists('m_provinsi_kota', 'kode_kota')],
            'no_akta_pendirian' => ['numeric', 'digits_between:0,50'],
            'tanggal_akta_pendirian' => ['date_format:d-m-Y'],
            'no_akta_perubahan' => ['numeric', 'digits_between:0,25'],
            'tanggal_akta_perubahan' => ['date_format:d-m-Y', 'after_ext:tanggal_akta_pendirian'],
            'telepon' => ['numeric'],
            'alamat' => ['max:191'],
            'provinsi' => [Rule::exists('m_provinsi_kota', 'kode_provinsi')],
            'kabupaten' => [Rule::exists('m_provinsi_kota', 'kode_kota')],
            'kecamatan' => [],
            'kelurahan' => [],
            'kode_pos' => ['nullable', 'numeric', 'digits:5', Rule::exists('m_kode_pos', 'kode_pos')],
            'rekening' => ['numeric', 'digits_between:0,191'],
            'bank' => [Rule::exists('m_bank', 'kode_bank')],
            'nama_pemilik' => ['max:191'],
            'bidang_pekerjaan' => [Rule::exists('m_bidang_pekerjaan', 'kode_bidang_pekerjaan')],
            'omset_tahun_terakhir' => ['numeric'],
            'total_aset_tahun_terakhir' => ['numeric'],
            'foto_npwp' => ['mimes:jpeg,jpg,bmp,png'],
            'laporan_keuangan' => ['mimes_ext:pdf,xlsx,xls,doc,docx'],
            'long' => [],
            'lat' => [],
            'alt' => [],
        ];

        if (! empty($neededRules)) {
            return array_intersect_key($rules, array_flip($neededRules));
        }

        return $rules;
    }

    public static function getFileServiceClass(): FileService
    {
        return new LenderFileService();
    }

    /**
     * @param string $column
     * @param \Illuminate\Http\UploadedFile $file
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param null $fileName
     * @return bool
     */
    public static function upload(string $column, UploadedFile $file, Model $model, $fileName = null)
    {
        $fileName = $fileName ?: $column;

        try {
            $model->forceFill([
                $column => LenderFileService::uploadFile($file, $fileName, $model->investor_id),
            ])
                ->save();
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param string $column
     * @param \Illuminate\Http\UploadedFile $file
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param null $fileName
     * @return bool
     */
    public static function uploadLapKeuangan(string $column, UploadedFile $file, Model $model, $fileName = null)
    {
        $fileName = $fileName ?: $column;

        try {
            $model->forceFill([
                $column => LenderFileService::uploadFile($file, $fileName, $model->investor_id),
            ])
                ->save();
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @param \App\InvestorAdministratorInterface $administrator
     * @param array $neededRules
     * @param \App\Investor $investor
     * @return void
     */
    public static function validator(
        array $data,
        InvestorAdministratorInterface $administrator,
        array $neededRules,
        Investor $investor
    ): void {
        $minItem = 1;
        if ($administrator instanceof InvestorPengurus) {
            $minItem = 2;
        }

        $requestKey = $administrator::getRequestIdentifier();

        Validator::make(
            $data,
            collect(
                $administrator::getRules(
                    $neededRules,
                    $investor->id,
                    $investor->$requestKey()->pluck($administrator->getKeyName())->toArray()
                )
            )
                ->mapWithKeys(function ($rule, $key) use ($administrator, $requestKey) {
                    if ($key === $administrator::JENIS_IDENTITAS) {
                        $rule[] = 'required_with:' . "$requestKey.*." . $administrator::IDENTITAS;
                    }

                    return ["{$requestKey}.*.$key" => $rule,];
                })
                ->merge([
                    $administrator::getRequestIdentifier() => ['array', 'min:' . $minItem],
                ])
                ->toArray(),
            [
                "$requestKey.*." . $administrator::NPWP . ".digits_between" => 'Harap masukkan NPWP setidaknya :min digit',
            ],
            /**
             * Map flat admin attributes to array on format tipe.*.key
             * ex pengurus.*.nik
             */
            collect($administrator::getAttributeName())
                ->mapWithKeys(function ($attr, $key) use ($requestKey) {
                    return ["{$requestKey}.*.$key" => $attr,];
                })
                ->merge([
                    $requestKey => $administrator::getRequestIdentifierName(),
                ])
                ->toArray()
        )
            ->validate();
    }

    public static function additionalValidator(Request $request, InvestorCorporation $investorCorporation): void
    {
        $additionalRules = [];
        $additionalAttributes = self::getAttributeName();
        $additionalMessages = [];

        if ($tanggalPendirian = $request->get(self::TANGGAL_AKTA_PENDIRIAN) ?: null) {
            // Validate akta perubahan wajib diisi jika tanggal akta pendirian > 5 tahun
            if (Carbon::createFromFormat('d-m-Y', $tanggalPendirian)->diffInYears(
                    Carbon::parse(Carbon::now()->toDateString())
                ) > 5) {
                $additionalRules[self::NO_AKTA_PERUBAHAN] = ['required'];
                $additionalRules[self::TANGGAL_AKTA_PERUBAHAN] = ['required'];
                $additionalRules[self::NAMA_NOTARIS_AKTA_PERUBAHAN] = ['required'];
                $additionalRules[self::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN] = ['required'];
                $additionalRules[self::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN] = ['required'];
                $additionalRules[self::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN] = ['required'];

                $message = __('validation.required') . ' Jika tanggal akta pendirian > 5 tahun.';
                $additionalMessages[self::NO_AKTA_PERUBAHAN . '.required'] = $message;
                $additionalMessages[self::TANGGAL_AKTA_PERUBAHAN . '.required'] = $message;
                $additionalMessages[self::NAMA_NOTARIS_AKTA_PERUBAHAN] = $message;
                $additionalMessages[self::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN] = $message;
                $additionalMessages[self::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN] = $message;
                $additionalMessages[self::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN] = $message;
            }
        }

        $validator = Validator::make(
            $request->all(),
            $additionalRules,
            $additionalMessages,
            $additionalAttributes
        );

        $validator->validate();
    }
}
