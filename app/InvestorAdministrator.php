<?php

namespace App;

use App\Constants\JenisKelamin;
use App\Services\FileService;
use App\Services\LenderFileService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

abstract class InvestorAdministrator extends Model
{
    public const NAMA = 'nama';
    public const JENIS_KELAMIN = 'jenis_kelamin';
    public const JENIS_IDENTITAS = 'jenis_identitas';
    public const IDENTITAS = 'identitas';
    public const TEMPAT_LAHIR = 'tempat_lahir';
    public const TANGGAL_LAHIR = 'tanggal_lahir';
    public const NO_TELEPON = 'no_telepon';
    public const AGAMA = 'agama';
    public const PENDIDIKAN = 'pendidikan';
    public const NPWP = 'npwp';
    public const JABATAN = 'jabatan';
    public const ALAMAT = 'alamat';
    public const PROVINSI = 'provinsi';
    public const KABUPATEN = 'kabupaten';
    public const KECAMATAN = 'kecamatan';
    public const KELURAHAN = 'kelurahan';
    public const KODE_POS = 'kode_pos';
    public const FOTO_KTP = 'foto_ktp';
    public const FOTO_PROFILE_KTP = 'foto_profile_ktp';
    public const FOTO_PROFILE = 'foto_profile';
    public const FOTO_NPWP = 'foto_npwp';
    public const KODE_OPERATOR = 'kode_operator';

    public const JENIS_IDENTITAS_NIK = 1;
    public const JENIS_IDENTITAS_PASPORT = 2;
    public const JENIS_IDENTITAS_NIB = 3;

    public static function getAdministratorDbMap(): array
    {
        return [
            self::NAMA => 'nama',
            self::JENIS_KELAMIN => 'jenis_kelamin',
            self::JENIS_IDENTITAS => 'jenis_identitas',
            // self::IDENTITAS => 'identitas',
            self::TEMPAT_LAHIR => 'tempat_lahir',
            self::TANGGAL_LAHIR => 'tgl_lahir',
            self::KODE_OPERATOR => 'kode_operator',
            self::NO_TELEPON => 'no_tlp',
            self::AGAMA => 'agama',
            self::PENDIDIKAN => 'pendidikan_terakhir',
            self::NPWP => 'npwp',
            self::JABATAN => 'jabatan',
            self::ALAMAT => 'alamat',
            self::PROVINSI => 'provinsi',
            self::KABUPATEN => 'kota',
            self::KECAMATAN => 'kecamatan',
            self::KELURAHAN => 'kelurahan',
            self::KODE_POS => 'kode_pos',
            self::FOTO_KTP => 'foto_ktp',
            self::FOTO_PROFILE_KTP => 'foto_diri_ktp',
            self::FOTO_PROFILE => 'foto_diri',
            self::FOTO_NPWP => 'foto_npwp',
        ];
    }

    public static function getRules(array $neededRules = [], $investorId = null, $adminIds = [])
    {
        return [
            static::NAMA => ['max:191'],
            static::JENIS_KELAMIN => [Rule::in([JenisKelamin::LAKILAKI, JenisKelamin::PEREMPUAN])],
            static::JENIS_IDENTITAS => [Rule::in([self::JENIS_IDENTITAS_NIK, self::JENIS_IDENTITAS_PASPORT])],
            static::TEMPAT_LAHIR => ['max:191'],
            static::TANGGAL_LAHIR => [
                'date_format:d-m-Y',
                'bail',
                function ($attribute, $value, $fail) {
                    if (Carbon::createFromFormat('d-m-Y', $value)->diffInYears() < 17) {
                        $fail('Minimal Harus berusia 17 tahun');
                    }
                },
            ],
            static::KODE_OPERATOR => [],
            static::NPWP => ['numeric', 'digits_between:15,16'],
            static::NO_TELEPON => [
                'numeric',
                'digits_between:9,15',
                'bail',
                'distinct',
                function ($attribute, $value, $fail) {
                    if (Str::startsWith($value, '8')) {
                        return true;
                    }

                    // Placeholder untuk yang non WNI
                    if (false) {
                        $fail('Harap masukan no telepon yang valid');
                    }

                    $fail('Harap masukan no telepon yang valid');
                },
            ],
            static::AGAMA => [Rule::exists('m_agama', 'id_agama')],
            static::PENDIDIKAN => [Rule::exists('m_pendidikan', 'id_pendidikan')],
            static::JABATAN => [Rule::exists('m_jabatan', 'id')],
            static::ALAMAT => ['max:191'],
            static::PROVINSI => [Rule::exists('m_provinsi_kota', 'kode_provinsi')],
            static::KABUPATEN => [Rule::exists('m_provinsi_kota', 'kode_kota')],
            static::KECAMATAN => [],
            static::KELURAHAN => [],
            static::KODE_POS => ['nullable', 'numeric', 'digits:5', Rule::exists('m_kode_pos', 'kode_pos')],
            static::FOTO_KTP => ['mimes:jpeg,jpg,bmp,png'],
            static::FOTO_PROFILE_KTP => ['mimes:jpeg,jpg,bmp,png'],
            static::FOTO_PROFILE => ['mimes:jpeg,jpg,bmp,png'],
            static::FOTO_NPWP => ['mimes:jpeg,jpg,bmp,png'],
        ];
    }

    public static function getAttributeName()
    {
        if (App::isLocale('en')) {
            return [
                static::NAMA => 'Name',
                static::JENIS_KELAMIN => 'Gender',
                static::IDENTITAS => 'Identity',
                static::JENIS_IDENTITAS => 'Identity Type',
                static::TEMPAT_LAHIR => 'Place of Birth',
                static::TANGGAL_LAHIR => 'Date of Birth',
                static::NO_TELEPON => 'Phone',
                static::AGAMA => 'Religion',
                static::PENDIDIKAN => 'Education',
                static::NPWP => 'NPWP',
                static::JABATAN => 'Position',
                static::ALAMAT => 'Address',
                static::PROVINSI => 'Province',
                static::KABUPATEN => 'Regency',
                static::KECAMATAN => 'District',
                static::KELURAHAN => 'Sub District',
                static::KODE_POS => 'Postal Code',
                static::FOTO_KTP => 'ID Card Photo',
                static::FOTO_PROFILE_KTP => 'Selfie ID Card Photo',
                static::FOTO_PROFILE => 'Selfie Photo',
                static::FOTO_NPWP => 'NPWP Photo',
            ];
        }

        return [
            static::NAMA => 'Nama',
            static::JENIS_KELAMIN => 'Jenis Kelamin',
            static::IDENTITAS => 'Identitas',
            static::JENIS_IDENTITAS => 'Jenis Identitas',
            static::TEMPAT_LAHIR => 'Tempat Lahir',
            static::TANGGAL_LAHIR => 'Tanggal Lahir',
            static::NO_TELEPON => 'No Telepon',
            static::AGAMA => 'Agama',
            static::PENDIDIKAN => 'Pendidikan',
            static::NPWP => 'NPWP',
            static::JABATAN => 'Jabatan',
            static::ALAMAT => 'Alamat',
            static::PROVINSI => 'Provinsi',
            static::KABUPATEN => 'Kabupaten',
            static::KECAMATAN => 'Kecamatan',
            static::KELURAHAN => 'Kelurahan',
            static::KODE_POS => 'Kode Pos',
            static::FOTO_KTP => 'Foto KTP',
            static::FOTO_PROFILE_KTP => 'Foto Profile KTP',
            static::FOTO_PROFILE => 'Foto Profile',
            static::FOTO_NPWP => 'Foto NPWP',
        ];
    }

    public function getData($neededData)
    {
        $administratorMap = static::getAdministratorDbMap();

        $getter = static function ($model, $map, $columns) {
            $data = [];
            foreach ($columns as $column) {
                $datum = $map[$column];
                $data[$column] = $model->$datum;
            }

            return $data;
        };

        $responseData = collect();
        if ($neededAdministrator = array_intersect(array_keys($administratorMap), $neededData)) {
            $responseData = $responseData->merge($getter($this, $administratorMap, $neededAdministrator));
        }

        return $responseData;
    }

    public static function getFileServiceClass(): FileService
    {
        return new LenderFileService();
    }

    /**
     * @param $adminRequest
     * @param \App\InvestorAdministratorInterface $administrator
     * @param $requestKey
     * @param int $key
     * @return void
     */
    public static function additionalValidator(
        $adminRequest,
        InvestorAdministratorInterface $administrator,
        $requestKey,
        int $key
    ): void {
        $additionalRules = [];
        $additionalAttributes = [];
        $additionalMessages = [];
        if ($jenisIdentitas = $adminRequest[self::JENIS_IDENTITAS] ?? null) {
            $rules = [
                self::JENIS_IDENTITAS_NIK => ['digits_between:16,16'],
                self::JENIS_IDENTITAS_PASPORT => ['alpha_num', 'max:9'],
                self::JENIS_IDENTITAS_NIB => ['digits_between:13,13'],
            ];

            $additionalRules["$requestKey.*." . self::IDENTITAS] = $rules[$jenisIdentitas];
            $additionalMessages["$requestKey.*." . self::IDENTITAS . ".digits_between"] = 'Harap masukkan Identitas setidaknya :min digit';
            $additionalMessages["$requestKey.*." . self::IDENTITAS . ".max"] = 'Identitas maksimal berisi :max karakter';
        }

        $validator = Validator::make(
            [$requestKey => [$key => $adminRequest]],
            $additionalRules,
            $additionalMessages,
            $additionalAttributes
        );

        $validator->validate();
    }
}
