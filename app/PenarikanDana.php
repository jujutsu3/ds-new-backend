<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PenarikanDana extends Model
{
    protected $table = 'penarikan_dana';

    protected $fillable = [
        'investor_id','jumlah','no_rekening','bank','accepted','perihal','alasan_penarikan','note_alasan_penarikan','alasan_penolakan'
    ];

    public function investor() {
        return $this->belongsTo('App\Investor');
    }
}
