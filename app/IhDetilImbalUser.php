<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IhDetilImbalUser extends Model
{
    protected $table = 'ih_detil_imbal_user';

    public function pendanaan_aktif() {
        return $this->hasMany('App\PendanaanAktif');
    }
}
