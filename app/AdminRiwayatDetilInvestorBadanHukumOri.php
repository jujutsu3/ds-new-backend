<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatDetilInvestorBadanHukumOri extends Model
{
    protected $primaryKey = 'hist_investor_id';
    protected $table = 'admin_riwayat_detil_investor_badan_hukum_ori';
    protected $guarded = [];
    public $timestamps = false;
}
