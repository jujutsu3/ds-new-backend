<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class InvestorContacts extends InvestorAdministrator implements InvestorAdministratorInterface, FieldFileInterface
{
    use FieldFileHandler;
    use FieldDateHandler;
    use FieldPhoneHandler;

    public const TABLE = 'investor_corp_contact';
    public const READABLE_KEY = "kontak";
    protected $table = 'investor_corp_contact';
    protected $primaryKey = 'contact_id';
    protected $guarded = ['contact_id'];

    public static function getAdministratorDbMap(): array
    {
        $map = parent::getAdministratorDbMap();
        $map[self::NAMA] = 'nm_contact';
        $map[self::IDENTITAS] = 'identitas_contact';

        return $map;
    }

    public static function getRules(array $neededRules = [], $investorId = null, $adminIds = [])
    {
        $rules = parent::getRules($neededRules, $investorId);
        $map = static::getAdministratorDbMap();
        $rules[static::IDENTITAS] = ['numeric'];

        return $rules;
    }

    public static function getRequestIdentifier()
    {
        return 'contacts';
    }

    public static function getRequestIdentifierName()
    {
        return 'Kontak';
    }

    public static function additionalValidator(
        $adminRequest,
        InvestorAdministratorInterface $administrator,
        $requestKey,
        int $key
    ): void {
        parent::additionalValidator($adminRequest, $administrator, $requestKey, $key);

        $map = static::getAdministratorDbMap();
        Validator::make([
            static::NO_TELEPON => ($adminRequest[static::KODE_OPERATOR] ?? null) . ($adminRequest[static::NO_TELEPON] ?? null),
        ], [
            static::NO_TELEPON => [
                Rule::unique(self::TABLE, $map[static::NO_TELEPON]),
                Rule::unique('detil_investor', 'phone_investor'),
            ],
        ], [], static::getAttributeName())
            ->validate();
    }
}
