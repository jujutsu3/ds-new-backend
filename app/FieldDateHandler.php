<?php

namespace App;

use Carbon\Carbon;

trait FieldDateHandler
{
    public static function fieldDateAccess(&$data, string $column, string $columnName = null)
    {
        $columnName = $columnName ?: $column;
        $data[$columnName] = optional($data[$column] ?? null, function ($value) {
            return Carbon::parse($value)->format('d-m-Y');
        });
    }

    public static function fieldDateMutate($data, $default = null, $toFormat = 'Y-m-d', $fromFormat = 'd-m-Y')
    {
        return optional($data, function ($item) use ($fromFormat, $toFormat) {
            return Carbon::createFromFormat($fromFormat, $item)->format($toFormat);
        }) ?: $default;
    }
}
