<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailLembarPendapatan extends Model
{
    protected $primaryKey = 'id_pengajuan';
    protected $table = 'brw_dtl_lembar_pendapatan';
    protected $guarded = [];
    // public $timestamps = false;
}
