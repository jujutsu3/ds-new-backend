<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerificationVendor extends Model
{
    protected $table = 'm_verification_vendor';
    protected $fillable = ['nama', 'updated_at'];
}
