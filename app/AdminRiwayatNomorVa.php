<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatNomorVa extends Model
{
    protected $primaryKey = 'hist_id';
    protected $table = 'admin_riwayat_nomor_va';
    protected $guarded = [];
    public $timestamps = false;
}
