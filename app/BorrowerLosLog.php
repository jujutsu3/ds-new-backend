<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerLosLog extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_los_log';
    protected $guarded = [];

}
