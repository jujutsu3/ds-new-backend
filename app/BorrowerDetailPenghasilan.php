<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailPenghasilan extends Model
{
    public const SKEMA_FIXED = 1;
    public const SKEMA_JOINT = 2;
    public const STATUS_PEGAWAI_KONTRAK = 1;
    public const STATUS_PEGAWAI_TETAP = 2;
    protected $table = 'brw_user_detail_penghasilan';
    protected $guarded = [];

    public static $statusPegawaiNames = [
        self::STATUS_PEGAWAI_KONTRAK => 'kontrak',
        self::STATUS_PEGAWAI_TETAP => 'tetap',
    ];
}
