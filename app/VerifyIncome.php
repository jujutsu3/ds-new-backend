<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class VerifyIncome extends Model
{
    protected $table = 'verify_income';
    protected $fillable = ['verify_id', 'npwp', 'valid_npwp', 'nik', 'valid_nik', 'name',  'valid_name', 'birthdate', 'valid_birthdate', 'birthplace', 'valid_birthplace', 'income', 'valid_income', 'match_result', 'updated_at'];

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y H:i:s');
    }
}
