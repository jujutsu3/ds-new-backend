<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailLembarFasilitasExternal extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'brw_dtl_lembar_fasilitas_external';
    protected $guarded = [];
    // public $timestamps = false;
}
