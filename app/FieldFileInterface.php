<?php

namespace App;

use App\Services\FileService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

interface FieldFileInterface
{
    public static function getFileServiceClass(): FileService;
}
