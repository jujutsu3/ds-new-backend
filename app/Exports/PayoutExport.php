<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use App\Proyek;
use App\MasterAgama;
use App\ListImbalUser;
use App\IhListImbalUser;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PayoutExport implements FromQuery, ShouldAutoSize, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    // public function collection()
    // {
    //     return MasterAgama::all();
    // }

    public function __construct(string $name)
    {
        $this->name = $name;
    }
    public function query()
    {
        return DB::table('ih_list_imbal_user as b')->select(
            'proyek.nama',
            'detil_investor.nama_investor',
            'b.tanggal_payout',
            DB::raw('CASE WHEN b.keterangan IS NULL THEN b.imbal_payout ELSE b.imbal_payout+b.keterangan END AS payout'),
            DB::raw('CASE WHEN DATEDIFF(b.tanggal_payout,proyek.tgl_mulai) < 39 THEN ih_detil_imbal_user.proposional ELSE 0 END as total_payout'),
            DB::raw('CASE WHEN b.keterangan IS NULL THEN 0 ELSE b.keterangan END AS potongan_privy'),
            DB::raw('CONCAT(b.tarif,"%")'),
            'b.nominal_pajak',
            DB::raw('CASE WHEN detil_investor.no_npwp_investor = "0" THEN "-" ELSE CONCAT("`",detil_investor.no_npwp_investor) END as npwp'),
            DB::raw('CASE WHEN detil_investor.is_valid_npwp = "1" THEN "Ya" ELSE "Tidak" END AS isvalidnpwp'),
            DB::raw('CONCAT("`",detil_investor.rekening) as rekening'),
            'm_bank.nama_bank',
            'detil_investor.nama_pemilik_rek',
            DB::raw('CASE WHEN detil_investor.pemilik_rekening_asli IS NULL THEN "-" ELSE detil_investor.pemilik_rekening_asli END as pemilik_rekening_asli'),
            'investor.email',
            DB::raw('CASE WHEN b.keterangan_payout = 1 THEN "Imbal Hasil" WHEN b.keterangan_payout = 2 THEN "Sisa Imbal Hasil" END AS keterangan')
        )
            ->rightJoin('ih_detil_imbal_user', 'b.detilimbaluser_id', '=', 'ih_detil_imbal_user.id')
            ->rightJoin('pendanaan_aktif', 'ih_detil_imbal_user.pendanaan_id', '=', 'pendanaan_aktif.id')
            ->rightJoin('proyek', 'ih_detil_imbal_user.proyek_id', '=', 'proyek.id')
            ->leftJoin('detil_investor', 'pendanaan_aktif.investor_id', '=', 'detil_investor.investor_id')
            ->leftJoin('m_bank', 'detil_investor.bank_investor', '=', 'm_bank.kode_bank')
            ->leftJoin('investor', 'pendanaan_aktif.investor_id', '=', 'investor.id')
            ->where('b.tanggal_payout', $this->name)
            ->where('pendanaan_aktif.status', 1)
            ->wherein('b.keterangan_payout', [1, 2])
            ->orderBy('ih_detil_imbal_user.proyek_id', 'ASC')
            ->orderBy('ih_detil_imbal_user.id', 'ASC')
            ->limit(10000);
        // return DB::table('ih_list_imbal_user as b')->select('proyek.nama','detil_investor.nama_investor','b.tanggal_payout','b.imbal_payout',DB::raw('CASE WHEN SUBSTR(proyek.tgl_mulai,6,2) = SUBSTR(b.tanggal_payout,6,2)-1 THEN ih_detil_imbal_user.proposional ELSE 0 END as total_payout'),DB::raw('CONCAT("`",detil_investor.rekening) as rekening'),'m_bank.nama_bank','detil_investor.nama_pemilik_rek','investor.email',DB::raw('CASE WHEN b.keterangan_payout = 1 THEN "Imbal Hasil" WHEN b.keterangan_payout = 2 THEN "Sisa Imbal Hasil" END AS keterangan'))
        // ->rightJoin('ih_detil_imbal_user','b.detilimbaluser_id','=','ih_detil_imbal_user.id')
        // ->rightJoin('pendanaan_aktif','ih_detil_imbal_user.pendanaan_id','=','pendanaan_aktif.id')
        // ->rightJoin('proyek','ih_detil_imbal_user.proyek_id','=','proyek.id')
        // ->leftJoin('detil_investor','pendanaan_aktif.investor_id','=','detil_investor.investor_id')
        // ->leftJoin('m_bank','detil_investor.bank_investor','=','m_bank.kode_bank')
        // ->leftJoin('investor','pendanaan_aktif.investor_id','=','investor.id')
        // ->where('b.tanggal_payout', $this->name)
        // ->where('pendanaan_aktif.status',1)
        // ->wherein('b.keterangan_payout',[1,2])
        // ->orderBy('ih_detil_imbal_user.proyek_id', 'ASC')
        // ->orderBy('ih_detil_imbal_user.id', 'ASC')
        // ->limit(10000);                   
    }

    public function headings(): array
    {
        return [
            'Proyek',
            'Nama Lender',
            'Tanggal Imbal Hasil',
            'Nilai Imbal Hasil',
            'Proposional',
            'Potongan Privy',
            'pph23',
            'Nominal Pajak',
            'NPWP',
            'NPWP valid',
            'No Rek',
            'Bank',
            'Nama Account',
            'Nama Pemilik Rekening Asli',
            'Email',
            'Keterangan'
        ];
    }
}
