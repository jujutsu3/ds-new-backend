<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\PenarikanDana;
use App\Investor;
use App\DetilInvestor;
use Carbon\Carbon;
use DB;

class PenarikanExport implements FromArray, ShouldAutoSize, WithHeadings, WithStyles
{
    
    /**
    * @return \Illuminate\Support\Collection
    */

    private $data_arr;
    public function __construct(array $data){
        $this->data_arr = array(
            "tgl_m"=>$data['tgl_m'],
            "tgl_s"=>$data['tgl_s'],
        );
        
    }

    public function array(): array
    {
        $tgl_m = $this->data_arr['tgl_m'];
        $tgl_s = $this->data_arr['tgl_s'];

        if($tgl_m != null && $tgl_s != null)
        {

                $data = PenarikanDana::leftJoin('investor', 'investor.id', '=', 'penarikan_dana.investor_id')
                ->leftJoin('detil_investor', 'detil_investor.investor_id', '=', 'penarikan_dana.investor_id')
                ->leftJoin('rekening_investor', 'rekening_investor.investor_id', '=', 'penarikan_dana.investor_id')
                ->leftJoin('m_alasan_penarikan', 'm_alasan_penarikan.id', '=', 'penarikan_dana.alasan_penarikan')
                ->leftJoin(DB::raw("(select * from e_coll_bni group by no_va) data_e_coll"), 'data_e_coll.no_va', '=', 'rekening_investor.va_number')
                ->where('penarikan_dana.created_at', '>=',$tgl_m.' 00:00:00')
                ->where('penarikan_dana.created_at', '<=',$tgl_s.' 23:59:59')
                ->orderBy('penarikan_dana.id', 'desc')
                ->get([
                    'penarikan_dana.jumlah',
                    'penarikan_dana.no_rekening',
                    'penarikan_dana.bank',
                    'investor.username',
                    'investor.email',
                    'detil_investor.nama_investor',
                    'detil_investor.nama_pemilik_rek',
                    'detil_investor.pemilik_rekening_asli',
                    'detil_investor.phone_investor',
                    'rekening_investor.va_number',
                    'data_e_coll.nama',
                    'penarikan_dana.created_at',
                    'penarikan_dana.accepted',
                    'penarikan_dana.alasan_penarikan',
                    'penarikan_dana.note_alasan_penarikan',
                    'penarikan_dana.alasan_penolakan',
                    'm_alasan_penarikan.alasan_penarikan as penarikan_alasan',
                ]);
                $dataExport = array();
                
                foreach($data as $row)
                {
                    if($row['accepted'] == 0){
                        $status='Pengajuan';
                    }else if($row['accepted'] === 1){
                        $status='Diterima';
                    }else{
                        $status='Ditolak - '.$row['alasan_penolakan'];
                    }

                    if($row['alasan_penarikan'] == null){
                        $alasan = '-';
                    }elseif($row['alasan_penarikan'] == 9){
                        $alasan = $row['note_alasan_penarikan'];
                    }elseif($row['alasan_penarikan'] < 9){
                        $alasan = $row['penarikan_alasan'];
                    } 

                    if($row['pemilik_rekening_asli'] == null){
                        $nama_rekening = $row['nama_pemilik_rek'];
                    }else{
                        $nama_rekening = $row['pemilik_rekening_asli'];
                    }

                    $dataExport[] = [
                                        'username' => $row['username'],
                                        'nama_investor' => $row['nama_investor'],
                                        'email' => $row['email'],
                                        'va_number' => "'".$row['va_number'],
                                        'phone_investor' => "'".$row['phone_investor'],
                                        'nama_pemilik_rek' => $nama_rekening,
                                        'no_rekening' => "'".$row['no_rekening'],
                                        'nama_ecol' => $row['nama'],
                                        'nama_bank' => $row['bank'],
                                        'jumlah_penarikan' => $row['jumlah'],
                                        'tanggal_invest' => Carbon::parse($row['created_at'])->format('Y-m-d'),
                                        'alasan_penarikan' => $alasan,
                                        'status_penarikan' => $status,
                                    ];
                }
                // var_dump($dataExport);die;
                return $dataExport;
        }
    }
    public function headings(): array
    {
        return [
                'Username',
                'Atas Nama',
                'Email',
                'Nomor Virtual Account',
                'Nomor Telepon',
                'Nama Pemilik Rekening',
                'Nomor Rekening',
                'Nama (E-Collection BNI)',
                'Nama Bank',
                'Jumlah Penarikan',
                'Tanggal Penarikan',
                'Alasan Penarikan',
                'Status Penarikan',
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true, 'size' => 13]]
        ];
    }
}
