<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use Carbon\Carbon;

class ListPengajuanKelayakan implements FromArray, ShouldAutoSize, WithEvents, WithHeadings, WithColumnFormatting, 
   WithColumnWidths, WithTitle
{

    public $pengajuan;

    use Exportable;

    public function __construct($pengajuan)
    {
        $this->pengajuan = $pengajuan;
    }

    public function title(): string
    {
        return 'Daftar Pengajuan';
    }

    public function array(): array
    {

        $dataExport = array();
        foreach ($this->pengajuan as $row) {

            $status = '-';
            if ($row->stts == 0 || $row->stts == 3) {
                $status = 'Baru';
            } else if ($row->stts == 2) {
                $status = 'Ditolak';
            } else if ($row->stts == 1) {
                 $status = 'Diterima';
            } 
            $dataExport[] = [
                'tgl_pengajuan' => Carbon::parse($row->tgl_pengajuan)->format('Y-m-d'),
                'pengajuan_id' => $row->pengajuan_id,
                'nama_cust' => $row->nama_cust,
                'tipe_pendanaan' => $row->tipe_pendanaan,
                'tujuan_pembiayaan' => $row->tujuan_pembiayaan,
                'nilai_pengajuan' => $row->nilai_pengajuan,
                'pekerjaan' => $row->pekerjaan,
                'status' => $status
            ];
        }

    
        return $dataExport;

    }

    public function headings(): array
    {
        return [
            'Tanggal Pengajuan',
            'Pengajuan Id',
            'Penerima Pendanaan',
            'Jenis Pendanaan',
            'Tujuan Pendanaan ',
            'Nilai Pengajuan Pendanaan (Rp)',
            'Pekerjaan',
            'Status'
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 10,
            'C' => 22,
            'D'=> 20,
            'E'=> 20,
            'F'=> 25,
            'G'=> 20,
            'H'=> 20

        ];
    }

    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED2
        ];
    }

    public function registerEvents(): array
    {

         return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $cellRange = 'A1:H1';
                $sheet->getStyle($cellRange)->getFont()->setSize(12)->getColor()->setARGB('ffffff');
                $sheet->getStyle($cellRange)->getFill()
                      ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                      ->getStartColor()->setARGB('000000');
        
                $sheet->freezePane('A2');
            }
         ];

    }

}
