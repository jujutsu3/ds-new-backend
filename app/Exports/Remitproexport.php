<?php

namespace App\Exports;

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use DB;

class Remitproexport implements FromView, ShouldAutoSize, WithStyles, WithColumnWidths, WithColumnFormatting
{
    
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;
    public function __construct(string $date)
    {
        $this->date = $date;
    }

    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('A1:T3')->applyFromArray([
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_DOTTED,
                    'color' => ['argb' => '000000'],
                ],
            ]
        ]);
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_NUMBER,
            'O' => NumberFormat::FORMAT_NUMBER,
            'S' => NumberFormat::FORMAT_NUMBER
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 11,
            'B' => 12,            
            'C' => 22,            
            'D' => 45,            
            'E' => 21,            
            'F' => 26,            
            'G' => 14,            
            'H' => 10,            
            'I' => 11,            
            'J' => 21,            
            'K' => 25,            
            'L' => 14,            
            'M' => 31,            
            'N' => 36,            
            'O' => 20,            
            'P' => 25,            
            'Q' => 30,            
            'R' => 20,            
            'S' => 16,            
            'T' => 15,            
        ];
    }

    public function view():view
    {   
        // $id = $this->id;
        $dataE = DB::SELECT("SELECT a.id AS adv, 
                GROUP_CONCAT(d.nama) AS proyek,
                e.nama_investor AS nama_lender,
                c.tanggal_payout AS tanggal_imbal_hasil, 
                SUM(c.imbal_payout) AS nilai_imbal_hasil,
                f.nama_kota AS kota_lender,
                e.rekening AS rekening,
                g.kode_remitpro AS account_code, 
                CONCAT(g.kode_bank,'-',g.nama_bank) AS rep_account_code,e.nama_pemilik_rek AS account_name,
                e.pemilik_rekening_asli AS account_name_asli,
                e.is_valid_rekening AS valid_rekening,
                e.phone_investor AS mobile_number_recipient,
                a.email AS email,
                sum(CASE WHEN c.keterangan IS NULL THEN 0 ELSE c.keterangan END) AS biaya_privy,
                sum(CASE WHEN DATEDIFF(c.tanggal_payout,d.tgl_mulai) < 39 THEN h.proposional ELSE 0 END) as proposional,
                c.tarif AS tarif
                FROM investor a
                LEFT JOIN pendanaan_aktif b ON a.id = b.investor_id
                LEFT JOIN ih_list_imbal_user c ON b.id = c.pendanaan_id
                LEFT JOIN proyek d ON b.proyek_id = d.id
                LEFT JOIN detil_investor e ON e.investor_id = a.id
                LEFT JOIN m_provinsi_kota f ON e.kota_investor = f.kode_kota
                LEFT JOIN m_bank g ON e.bank_investor = g.kode_bank
                LEFT JOIN ih_detil_imbal_user h ON c.detilimbaluser_id = h.id
                WHERE c.tanggal_payout = '".$this->date."'
                and c.keterangan_payout IN (1,2)
                and b.status = 1
                and c.imbal_payout > 0
                GROUP BY a.id
                ORDER BY e.nama_investor,d.id ASC");

                $data = array();
                $no = 1;
                foreach($dataE as $row)
                {
                    if($row->valid_rekening == 1){
                        $account_name = $row->account_name_asli;
                    }else{
                        $account_name = $row->account_name;
                    }

                    $s = '';
                    $getProyek = explode(",",$row->proyek);
                    for($i=0;$i<count($getProyek);$i++){
                        $r = substr(preg_replace('/[^,0-9]/', '', $getProyek[$i]),0,-2);
                        $s.=$r.',';
                    }
                    $proyek = substr($s,0,-1);

                    $total_imbal = ($row->nilai_imbal_hasil+$row->proposional+$row->biaya_privy)*((100-$row->tarif)/100);

                    $data[] = [
                                        'external_id' => $no++,
                                        'amount' => $total_imbal-$row->biaya_privy,
                                        'purpose_code' => 'SHARES_INVESTMENT',
                                        'description' => "Pendanaan ".$proyek,
                                        'bussiness_individual_sender' => 'Business',
                                        'name_sender' => 'PT Dana Syariah Indonesia',
                                        'country_code_sender' => 'ID',
                                        'city_sender' => 'Jakarta',
                                        'dob_sender' => '19-Sep-17',
                                        'bussiness_individual_recipient' => 'Individual',
                                        'name_recipient' => $row->nama_lender,
                                        'country_code_recipient' => 'ID',
                                        'city_recipient' => $row->kota_lender,
                                        'account_code_recipient' => empty($row->account_code) ? $row->rep_account_code : $row->account_code,
                                        'account_number_recipient' => $row->rekening,
                                        'account_holder_name_recipient' => $account_name,
                                        'email_recipient' => $row->email,
                                        'cc_recipient' => 'dsicimb@gmail.com',
                                        'mobile_number_recipient' => $row->mobile_number_recipient,
                                        'phone_number_recipient' => '',
                                    ];
                }
        $data['dataExport'] = $data;
        $view = view('export_report.Remitproexport',$data);
        return $view;
    }

    
    
}
