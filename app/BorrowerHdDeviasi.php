<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerHdDeviasi extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_hd_deviasi';
    protected $guarded = [];
}
