<?php

namespace App\Helpers;

use App\Borrower;
use App\BorrowerDetails;
use App\BorrowerPengurus;
use Illuminate\Support\Facades\Auth;

class BorrowerFile
{
    public const S_KYC_FOTO = 'kyc_foto';
    public const S_KYC_COMPANY_PENGURUS_FOTO = 'borrower_kyc_company_pengurus_foto';
    public const S_KYC_COMPANY_FOTO = 'borrower_kyc_company_foto';

    public static $scopes = [
        self::S_KYC_FOTO,
        self::S_KYC_COMPANY_PENGURUS_FOTO,
        self::S_KYC_COMPANY_FOTO,
    ];

    public static function makeUrl($path, $scope, $column)
    {
        return route('v1.borrower.file', [
            'path' => $path,
            'scope' => $scope,
            'column' => $column,
        ]);
    }

    /**
     * @param $column
     * @param $path
     * @return \Closure[]
     */
    public static function verifyScope($column, $path): array
    {
        return [
            self::S_KYC_FOTO => function () use ($column, $path) {
                return self::KycFotoHandler($column, $path);
            },
            self::S_KYC_COMPANY_PENGURUS_FOTO => function () use ($column, $path) {
                return self::KycCompanyPengurusFotoHandler($column, $path);
            },
            self::S_KYC_COMPANY_FOTO => function () use ($column, $path) {
                return self::KycCompanyFotoHandler($column, $path);
            },
        ];
    }

    /**
     * @param $column
     * @param $path
     * @return bool
     */
    public static function KycFotoHandler($column, $path): bool
    {
        $map = [
            'ktp' => 'brw_pic_ktp',
            'profile_ktp' => 'brw_pic_user_ktp',
        ];
        if (! array_key_exists($column, $map)) {
            return false;
        }
        $pengurus = BorrowerDetails::query()->where('brw_id', Auth::id())->first();

        if ($pengurus === null) {
            return false;
        }

        return $pengurus->{$map[$column]} === $path;
    }

    /**
     * @param $column
     * @param $path
     * @return bool
     */
    public static function KycCompanyPengurusFotoHandler($column, $path): bool
    {
        $map = [
            'ktp' => 'foto_ktp',
            'profile_ktp' => 'foto_diri_ktp',
            'npwp' => 'foto_npwp',
        ];
        if (! array_key_exists($column, $map)) {
            return false;
        }
        $pengurus = BorrowerPengurus::query()->where('brw_id', Auth::id())->first();

        if ($pengurus === null) {
            return false;
        }

        return $pengurus->{$map[$column]} === $path;
    }

    /**
     * @param $column
     * @param $path
     * @return bool
     */
    public static function KycCompanyFotoHandler($column, $path): bool
    {
        $map = [
            'npwp' => 'foto_npwp_perusahaan',
        ];
        if (! array_key_exists($column, $map)) {
            return false;
        }
        $borrower = BorrowerDetails::query()->where('brw_id', Auth::id())->first();

        if ($borrower === null) {
            return false;
        }

        return $borrower->{$map[$column]} === $path;
    }
}
