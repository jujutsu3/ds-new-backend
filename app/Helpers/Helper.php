<?php


namespace App\Helpers;

use DB;

use Auth;
use App\Services\BorrowerService;
use App\BorrowerPersyaratanPendanaan;
use Illuminate\Support\Facades\Storage;

class Helper
{

    public static function generate_number($length)
    {
        $character = '1234567890';
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $pos = rand(0, strlen($character) - 1);
            $string .= $character[$pos];
        }
        return $string;
    }

    public static function date_indonesia($date)
    {
        $month = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $split = explode('-', $date);

        return $split[2] . ' ' . $month[(int)$split[1]] . ' ' . $split[0];
    }


    public static function validResultVerijelas($value)
    {
        return ($value == 1) ? '<span class="badge badge-success" style="font-size: 1em;">Valid</span>' : '<span class="badge badge-danger" style="font-size: 1em;">Tidak Valid</span>';
    }

    public static function scorePhotoVerijelas($value)
    {
        if ($value > 70) {
            return '<span class="badge badge-success" style="font-size: 1em;">' . $value . '</span>';
        } elseif ($value > 50 && $value <= 69) {
            return '<span class="badge badge-warning" style="font-size: 1em;">' . $value . '</span>';
        } else {
            return '<span class="badge badge-danger" style="font-size: 1em;">' . $value . '</span>';
        }
    }

    public static function scoreIncomeVerijelas($value, $viewInCheck = false)
    {

        switch ($value) {
            case 'ABOVE':
                return '<span class="badge badge-success" style="font-size: 1em;">' . $value . '</span>';
                break;
            case 'AMIDST':
                return '<span class="badge badge-warning" style="font-size: 1em;">' . $value . '</span>';
                break;
            case 'BELOW':
                return '<span class="badge badge-danger" style="font-size: 1em;">' . $value . '</span>';
                break;
            default:
            return ($viewInCheck === true) ? '<input type="text" readonly class="form-control" value="' . __('verify.no_valid') . '" style="background-color: #dc3545; color: white;font-size: 10px;width: 16% !important">' :'<span class="badge badge-danger" style="font-size: 1em;">'. __('verify.no_valid').'</span>';
            break;
        }
    }

    public static function logDbApp($filename, $line, $description){
        DB::table('log_db_app')->insert(['file_name' => $filename,'line' => $line, 'description' => $description, 'created_at'=> date('Y-m-d H:i:s')]);
    }

    public static function logAktifitasPengajuan($pengajuan_id, $data, $tablename){

         if (Auth::guard('borrower')->check()) {
            $created_by = Auth::guard('borrower')->user()->email;
        }

        if (Auth::guard('admin')->check()) {
            $created_by = Auth::guard('admin')->user()->email;
        }

        DB::table('log_aktifitas_pengajuan')->insert(['pengajuan_id'=> $pengajuan_id, 'data' => json_encode($data), 'tablename'=> $tablename, 'created_by' => $created_by]);
    }

    public static function isJSON($string){
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
     }


    public static function uploadFile($new_path, $file, $file_name) {

        $err_msg = '';
        $path    = '';

        try {
            $path = $file->storeAs($new_path, $file_name, 'private');
        } catch (\Exception  $ex) {
            $err_msg = $ex;
        }

        if (!$path) {
            DB::table('log_db_app')->insert([
                'file_name' => 'Helper.php',
                'line' => 86,
                'description' => 'Proses unggah file ' .$file_name. 'gagal karena ' .$err_msg,
            ]);
            return false;
        } else {
            return true;
        }
    } 

    public static function deleteFile($path) {
        if (Storage::disk('private')->exists($path)) {
            Storage::disk('private')->delete($path);
        }
    }

    public static function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }


    /*
        Get Dokumen Persyaratan By Pengajuan
    */
    public static function getDokumenList($pengajuan)
    {
        $borrower = BorrowerService::getBorrower($pengajuan->brw_id);
        $borrowerDetail = BorrowerService::getDetilBorrower($borrower);
        $borrowerPenghasilan = BorrowerService::getDetilBorrowerPenghasilan($borrower);
        $borrowerPasangan = BorrowerService::getDetilBorrowerPasangan($borrower);

        return BorrowerPersyaratanPendanaan::query()->where([
            'tipe_id' => $pengajuan->pendanaan_tipe,
            'user_type' => $borrowerDetail->brw_type,
        ])
            ->where('category', '<>', '')
            ->orderBy('category')
            ->orderBy('sort')
            ->get()
            ->filter(function ($doc) use ($borrowerPasangan, $borrowerPenghasilan, $pengajuan, $borrowerDetail) {
                if ($doc->status_kawin !== null
                    && $doc->status_kawin !== $borrowerDetail->status_kawin) {
                    return false;
                }
                if ($doc->skema_pembiayaan !== null
                    && $doc->skema_pembiayaan !== $borrowerPenghasilan->skema_pembiayaan) {
                    return false;
                }
                if ($doc->sumber_pengembalian_dana !== null
                    && $doc->sumber_pengembalian_dana !== $borrowerPenghasilan->sumber_pengembalian_dana) {
                    return false;
                }
                if ($doc->sumber_pengembalian_dana_pasangan !== null
                    && $doc->sumber_pengembalian_dana_pasangan !== $borrowerPasangan->sumber_pengembalian_dana) {
                    return false;
                }

                return true;
            });
    }

    public static function checkLimitPengajuan($brw_id, $channel = 'mobile')
    {

        $limitDays = DB::table('m_param')->where('deskripsi', 'limit_days')->value('value');
        $checkLimit = DB::select('select func_check_limit_pengajuan(?) as response', [$brw_id]);

        $responseMessage = [
            'message' => 'The given data was invalid.',
            'errors' => [
                'pengajuan' => [
                    0 => 'Maksimal Pengajuan Kembali adalah ' . $limitDays . ' hari',
                ],
            ],
        ];

        return ['hasil' => $checkLimit[0]->response, 'responseMessage' => ($checkLimit[0]->response == 1) ? null : $responseMessage, 'limitDays' => $limitDays];
    }

    public static function validateDate($date, $format = 'Y-m-d'){
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    public static function getProfileUser($type = 'borrower'){

        if($type === 'borrower'){
            $query = \DB::table("brw_user");
            $query->leftjoin("brw_user_detail", "brw_user.brw_id", "brw_user_detail.brw_id");
            $query->selectRaw(" brw_user.email, IF(brw_user_detail.brw_type = 1, UPPER(brw_user_detail.nama), UPPER(brw_user_detail.nm_bdn_hukum)) as full_name, 
            IF(brw_user_detail.brw_type = 1, UPPER(brw_user_detail.no_tlp), UPPER(brw_user_detail.telpon_perusahaan)) as contact_number,
            CASE WHEN brw_user_detail.jns_kelamin = 1 THEN 'Male'
                 WHEN brw_user_detail.jns_kelamin = 2 THEN 'Female'
                 ELSE '-' END AS gender, 
            CASE WHEN brw_user_detail.brw_type = 1 THEN 'Individu'
                 WHEN brw_user_detail.brw_type = 2 THEN 'Badan Hukum'
                 ELSE '-' END AS tipe_pengguna, brw_user_detail.tgl_lahir");
            return $query;

        }else{
            $query = \DB::table("investor");
            $query->leftjoin("detil_investor", "investor.id", "detil_investor.investor_id");
            $query->selectRaw("investor.email, IF(detil_investor.tipe_pengguna = 1, UPPER(detil_investor.nama_investor), UPPER(detil_investor.nama_perusahaan)) as full_name, 
                 IF(detil_investor.tipe_pengguna = 1, UPPER(detil_investor.phone_investor), UPPER(detil_investor.telpon_perusahaan)) as contact_number,
                 CASE WHEN detil_investor.jenis_kelamin_investor = 1 THEN 'Male'
                      WHEN detil_investor.jenis_kelamin_investor = 2 THEN 'Female'
                      ELSE '-' END AS gender, 
                 CASE WHEN detil_investor.tipe_pengguna = 1 THEN 'Individu'
                      WHEN detil_investor.tipe_pengguna = 2 THEN 'Badan Hukum'
                      ELSE '-' END AS tipe_pengguna, tgl_lahir_investor");
    
            return $query;
        }

        return $query;
    }

    public static function mappingExtension(){
        return ['png'=> 'data:image/png;base64,', 
                'jpeg'=> 'data:image/jpeg;base64,', 
                'pdf'=>'data:application/pdf;base64,', 
                'jpg'=>'data:image/jpg;base64,',
                'xls'=>'data:application/vnd.ms-excel;base64,',
                'xlsx'=> 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,',
                'docx'=> 'data:application/vnd.openxmlformats-officedocument.wordprocessingml.document;base64,',
                'doc' => 'data:application/msword;base64,'];
     }
    
}
