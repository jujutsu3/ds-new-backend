<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterJabatan extends Model
{
    protected $table = 'm_jabatan';
    protected $primaryKey = 'id';
}
