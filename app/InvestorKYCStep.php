<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestorKYCStep extends Model
{
    public const TYPE_INDIVIDU = 'ind';
    public const TYPE_CORPORATION = 'cor';

    public const STEP_IND_AWAL = 'awal';
    public const STEP_IND_DATA = 'data_diri';
    public const STEP_IND_OTP = 'otp';
    public const STEP_IND_VERIFICATION = 'verification';
    public const STEP_IND_ACTIVE = 'active';

    public const STEP_COR_AWAL = 'awal';
    public const STEP_COR_DATA = 'data_perusahaan';
    public const STEP_COR_OTP = 'otp';
    public const STEP_COR_ADMIN = 'verification';
    public const STEP_COR_ACTIVE = 'active';

    protected $table = 'm_investor_kyc_step';

    public static function corporation()
    {
        return self::query()->where('type', self::TYPE_CORPORATION);
    }

    public static function individu()
    {
        return self::query()->where('type', self::TYPE_INDIVIDU);
    }
}
