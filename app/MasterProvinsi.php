<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterProvinsi extends Model
{
    public const KOTA_LAINLAIN = 'e999';
    public const PROVINSI_LAINLAIN = 99;
    protected $table = 'm_provinsi_kota';
    protected $primaryKey = 'id_provinsi';
}
