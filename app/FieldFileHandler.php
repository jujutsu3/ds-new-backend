<?php

namespace App;

/**
 * @mixin \App\FieldFileInterface
 */
trait FieldFileHandler
{
    public static function fieldFileAccess(&$data, string $column, \Closure $check, string $columnName = null)
    {
        if (! isset($data[$column])) {
            return;
        }

        $columnName = $columnName ?: "{$column}_full";
        $serviceClass = static::getFileServiceClass();
        $data[$columnName] = $serviceClass::makeUrl($data[$column], $check);
    }
}
