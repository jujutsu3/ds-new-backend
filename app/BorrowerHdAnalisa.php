<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerHdAnalisa extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_hd_analisa';
    protected $guarded = [];
    public $timestamps = false;

    public function getData()
    {
        return static::orderBy('id_pengajuan','asc');
    }
}
