<?php

namespace App;

interface InvestorAdministratorInterface
{
    public function getData($neededData);

    public static function getRequestIdentifier();
    
    public static function getRequestIdentifierName();

    public static function getAdministratorDbMap(): array;

    public static function getRules(array $neededRules = [], $investorId = null, $adminIds = []);

    public static function getAttributeName();
}
