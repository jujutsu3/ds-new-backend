<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerMaterialRequisitionDetail extends Model
{
    public const CREATED_AT = 'creation_date';
    public const UPDATED_AT = 'last_update_date';
    protected $primaryKey = 'requisition_line_id';
    protected $table = 'brw_material_requisition_dtl';

    protected $fillable = ['requisition_hdr_id', 'line_number', 'item_category_id', 'item_description', 'material_item_id', 'material_item_name', 'unit_price', 'quantity', 'task_id', 'created_by'];


    public function task()
    {
        return $this->belongsTo(BorrowerMaterialTask::class, 'task_id', 'task_id');
    }
}
