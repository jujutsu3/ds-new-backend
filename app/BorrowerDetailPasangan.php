<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailPasangan extends Model
{
    public const SKEMA_FIXED = 1;
    public const SKEMA_JOINT = 2;
    protected $primaryKey = 'pasangan_id';
    protected $table = 'brw_pasangan';
    protected $guarded = [];
}
