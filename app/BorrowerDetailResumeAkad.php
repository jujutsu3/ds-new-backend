<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailResumeAkad extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_dtl_resume_akad';
    protected $guarded = [];
    // public $timestamps = false;
}
