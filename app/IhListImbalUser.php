<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IhListImbalUser extends Model
{
    protected $table = 'ih_list_imbal_user';

    public function pendanaan_aktif() {
        return $this->hasMany('App\PendanaanAktif');
    }

    public function detilImbalUser()
    {
        return $this->belongsTo(IhDetilImbalUser::class, 'detilimbaluser_id');
    }
}
