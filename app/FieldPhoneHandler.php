<?php

namespace App;

trait FieldPhoneHandler
{
    public static function fieldPhoneAccess(&$data, string $phoneColumn, string $kodeOperatorColumn = null)
    {
        if (! isset($data[$phoneColumn])) {
            return;
        }

        $trimmed = $data[$kodeOperatorColumn] ?? "62";

        $data[$phoneColumn] = optional($data[$phoneColumn], function () use ($trimmed, $phoneColumn, $data) {
            return ltrim($data[$phoneColumn], $trimmed);
        });
    }

    public static function fieldPhoneMutate($data, $kodeOperator)
    {
        if (empty($data) || empty($kodeOperator)) {
            return $data;
        }

        return $kodeOperator.$data;
        // return ltrim('+', $kodeOperator) . $data;
    }
}
