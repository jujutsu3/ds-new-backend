<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerHdLegalitas extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_hd_legalitas';
    protected $guarded = [];
    public $timestamps = false;
}
