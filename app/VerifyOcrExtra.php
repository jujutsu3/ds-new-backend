<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class VerifyOcrExtra extends Model
{
    protected $table = 'verify_ocr_extra';
    protected $fillable = ['verify_id', 'nik', 'nama', 'jenis_kelamin', 'tgl_lahir', 'tempat_lahir', 'alamat', 'rt_rw', 'kelurahan', 'kecamatan', 'provinsi', 'gol_darah', 'kota', 'kewarganegaraan', 'agama', 'status_perkawinan', 'pekerjaan', 'updated_at'];


    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y H:i:s');
    }
}
