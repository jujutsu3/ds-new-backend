<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
class VerifyNegativeList extends Model
{
    protected $table = 'verify_negative_list';
    protected $fillable = ['verify_id', 'nik', 'valid_nik', 'name', 'valid_name', 'dob', 'valid_dob', 'pob', 'valid_pob', 'detail_url', 'updated_at'];


    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y H:i:s');
    }
}