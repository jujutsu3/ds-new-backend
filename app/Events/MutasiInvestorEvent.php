<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MutasiInvestorEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    #old
    // public function __construct($user_id,$type,$nominal,$perihal = null, $data = null)
    // {
    //     //
    //     $this->user_id = $user_id;
    //     $this->type = $type;
    //     $this->nominal = $nominal;
    //     $this->perihal = $perihal;
    //     $this->data = $data;
    // }
    #endold
    public function __construct()
    {
        $get_arguments       = func_get_args();
        $number_of_arguments = func_num_args();

        if (method_exists($this, $method_name = '__construct' . $number_of_arguments)) {
            call_user_func_array(array($this, $method_name), $get_arguments);
        }
    }

    public function __construct4($user_id, $type, $nominal, $perihal = null)
    {
        //
        $this->user_id = $user_id;
        $this->type = $type;
        $this->nominal = $nominal;
        $this->perihal = $perihal;
    }

    public function __construct9($user_id, $type, $nominal, $perihal = null, $data = null, $kode_bank, $type_account, $no_account, $no_ref_bank)
    {
        //
        $this->user_id = $user_id;
        $this->type = $type;
        $this->nominal = $nominal;
        $this->perihal = $perihal;
        $this->data = $data;
        /* ARL */
        $this->kode_bank = $kode_bank;
        $this->type_account = $type_account;
        $this->no_account = $no_account;
        $this->no_ref_bank = $no_ref_bank;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
