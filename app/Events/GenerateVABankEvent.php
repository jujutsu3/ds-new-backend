<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use app\Investor;

class GenerateVABankEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $va, $userid, $id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Investor $data_user)
    {
        //
        $this->va = $data_user->detilInvestor->getVa();
        $this->id = $data_user->id;
        $this->userid = $data_user->username;
    }

    public function getVA()
    {
        return $this->va;
    }

    public function getuserid()
    {
        return $this->userid;
    }

    public function getid()
    {
        return $this->id;
    }
}
