<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatInvestorOri extends Model
{
    protected $primaryKey = 'hist_investor_id';
    protected $table = 'admin_riwayat_investor_ori';
    protected $guarded = [];
    public $timestamps = false;
}
