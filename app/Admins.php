<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admins extends Authenticatable
{
    protected $guard = 'admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',  'lastname', 'email', 'address', 'password','last_login_at','last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeGetRole(){

        $user_id = '';
        if (\Auth::guard('admin')->check()) {
            $user_id = \Auth::guard('admin')->user()->id;
        }

        $role = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')->where('admins.id', $user_id)->first(['roles.name', 'admins.id']);
        
        return $role ? $role->name : '';
    }

}