<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailLembarAnalisa extends Model
{
    protected $primaryKey = 'id_pengajuan';
    protected $table = 'brw_dtl_lembar_hasil_analisa';
    protected $guarded = [];
    // public $timestamps = false;
}
