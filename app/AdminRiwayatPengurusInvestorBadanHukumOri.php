<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatPengurusInvestorBadanHukumOri extends Model
{
    protected $primaryKey = 'pengurus_id';
    protected $table = 'admin_riwayat_pengurus_investor_badan_hukum_ori';
    protected $guarded = [];
    public $timestamps = false;
}
