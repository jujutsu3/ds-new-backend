<?php

namespace App\Models;

use App\Interfaces\Investable;
use App\Models\DetailInvestor;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\CanResetPassword;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Auth\Passwords\CanResetPassword;

class Investor extends Authenticatable implements JWTSubject
{
    public const STATUS_ACTIVE = 'active';
    public const STATUS_SUSPENDED = 'suspend';
    public const STATUS_EXPIRED = 'expired';
    
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'investor';

    protected $fillable = [
        'username', 'email', 'password', 'email_verif','ref_number', 'status', 'provider', 'provider_id','keterangan','suspended_by'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rekeningInvestor() {
        return $this->hasOne('App\RekeningInvestor');
    }
    public function detailInvestor() {
        return $this->hasOne(DetailInvestor::class, 'investor_id', 'id');
    }
    public function mutasiInvestor() {
        return $this->hasMany('App\MutasiInvestor');
    }
    public function perusahaanInvestor(){
        return $this->hasOne('App\PerusahaanInvestor');
    }
    public function pendanaanAktif() {
        return $this->hasMany('App\PendanaanAktif');
    }
    public function penarikanDana() {
        return $this->hasMany('App\PenarikanDana');
    }
    // public function activeCart(){
    //     return $this->hasMany('App\Cart');
    // }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new Notifications\ResetPasswordNotification($token));
    }

    public function subscribe(){
        return $this->hasMany('App\Subscribe');
    }

        public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
