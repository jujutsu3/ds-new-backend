<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvestorLocation extends Model
{
    protected $table = 'investor_location';
    protected $guarded = [];
}
