<?php

namespace App\Models;

use DB;
use App\Models\Investor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DetailInvestor extends Model
{
    use HasFactory;
    
    public const TYPE_INDIVIDU = 1;
    public const TYPE_COMPANY = 2;
    public const JENIS_NIK = 1;
    public const JENIS_PASPORT = 2;
    protected $table = 'detil_investor';
    // protected $fillable = ['agama_investor', 'pic_investor', 'pic_ktp_investor', 'pic_user_ktp_investor'];
    protected $guarded  = ['id'];

    public function investor()
    {
        // return $this->belongsTo('App\Models\Investor');
        return $this->belongsTo(Investor::class, 'investor_id', 'id');
    }

    public function getVA()
    {
        $number = $this->attributes['phone_investor'];
        if (strlen($number) < 12) {
            $number = str_pad($number, 12, '0', STR_PAD_RIGHT);
        } elseif (strlen($number) > 12) {
            $number = substr($number, -12);
        }

        $number_cut = substr($number, 2, 12);
        $number_va = str_pad($number_cut, 12, '1', STR_PAD_LEFT);
        return $number_va;
    }

    public function getVA_konv()
    {
        $number = $this->attributes['phone_investor'];
        if (strlen($number) < 12) {
            $number = str_pad($number, 12, '0', STR_PAD_RIGHT);
        } elseif (strlen($number) > 12) {
            $number = substr($number, -12);
        }

        $number_cut = substr($number, 6, 12);
        $number_va = str_pad($number_cut, 8, '01', STR_PAD_LEFT);
        return $number_va;
    }

    public $timestamps = false;

    public function updateIsValidNull($inv_id){
        DB::table('detil_investor')->where('investor_id', $inv_id)
        ->update( [ 'is_valid_npwp' => null, 'tgl_validasi_npwp' => date('Y-m-d H:i:s')]); 
        return 1;
    }

    public function updateIsValidrekeningNull($inv_id){
        DB::table('detil_investor')->where('investor_id', $inv_id)
        ->update( [ 'is_valid_rekening' => null, 'tgl_validasi_rekening' => null,'pemilik_rekening_asli' => null]); 
        return 1;
    }
}
