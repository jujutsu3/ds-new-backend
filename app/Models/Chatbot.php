<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chatbot extends Model
{
    protected $table = 'chatbots'; 
    protected $fillable = [
        'code',
        'greetings',
        'category_id',
        'question_id',
        'question',
        'answer_id',
        'answer',
    ];
}