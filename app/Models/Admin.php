<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Admin extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'admins';
    protected $guard = 'admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',  'lastname', 'email', 'address', 'password','last_login_at','last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function scopeGetRole(){

        $user_id = '';
        if (\Auth::guard('admin')->check()) {
            $user_id = \Auth::guard('admin')->user()->id;
        }

        $role = Admins::leftJoin('roles', 'roles.id', '=', 'admins.role')->where('admins.id', $user_id)->first(['roles.name', 'admins.id']);
        
        return $role ? $role->name : '';
    }
}
