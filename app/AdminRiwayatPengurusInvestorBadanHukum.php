<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatPengurusInvestorBadanHukum extends Model
{
    protected $primaryKey = 'pengurus_id';
    protected $table = 'admin_riwayat_pengurus_investor_badan_hukum';
    protected $guarded = [];
    public $timestamps = false;
}
