<?php

namespace App;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class VerifyProperti extends Model
{   
    protected $table = 'verify_properti';
    protected $fillable = ['verify_id', 'nop', 'nik', 'property_name', 'valid_property_name', 'property_building_area', 'valid_property_building_area', 'property_surface_area', 'valid_property_surface_area', 'property_estimation', 'valid_property_estimation', 'certificate_id', 'valid_certificate_id', 'certificate_name', 'valid_certificate_name', 'certificate_type', 'valid_certificate_type', 'certificate_date', 'valid_certificate_date', 'certificate_address', 'property_address', 'updated_at'];

    public function getCreatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y H:i:s');
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y H:i:s');
    }
}