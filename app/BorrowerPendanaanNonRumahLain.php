<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerPendanaanNonRumahLain extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'brw_pendanaan_non_rumah_lain';
    protected $guarded = [];
}
