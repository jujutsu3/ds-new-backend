<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailNaup extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_dtl_naup';
    protected $guarded = [];
}
