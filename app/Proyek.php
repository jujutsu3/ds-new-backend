<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Interfaces\Investable;

class Proyek extends Model implements Investable
{
    protected $table='proyek';
    protected $guarded = [];




    protected $dates=['tgl_mulai', 'tgl_selesai'];
    public function pendanaanAktif() {
        return $this->hasMany('App\PendanaanAktif');
    }
    public function gambarProyek() {
        return $this->hasMany('App\GambarProyek');
    }
    public function pemilikPaket() {
        return $this->hasOne('App\PemilikPaket');
    }
    public function progressProyek() {
        return $this->hasMany('App\ProgressProyek');
    }
    public function log_payout_user() {
        return $this->hasOne('App\log_payout_user');
    }

    public function mutationProyek(){
        return $this->hasMany('App\ProjectMutation');
    }

    public function getInvestableIdentifier()
    {
        return $this->id;
    }

    public function getInvestableDescription()
    {
        return $this->nama;
    }

    public function getInvestablePrice()
    {
        return $this->harga_paket;
    }

    public $timestamps = true;

    public function pemilikProyekRel()
    {
        return $this->belongsTo(Pemilik_Proyek::class, 'id_pemilik', 'id');
    }

    public function deskripsiProyekRel()
    {
        return $this->belongsTo(Deskripsi_Proyek::class, 'id_deskripsi', 'id');
    }

    public function legalitasProyekRel()
    {
        return $this->belongsTo(Legalitas_Proyek::class, 'id_legalitas', 'id');
    }
}
