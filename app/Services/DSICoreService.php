<?php

namespace App\Services;

use GuzzleHttp\Client;

use App\Helpers\Helper;
use JWTAuth;
use Auth;
class DSICoreService
{

    public static function getPengajuan()
    {
        try {

            $client = new Client();
            $apiToken = JWTAuth::fromUser(Auth::guard('borrower-api-mobile')->user());

            $requestApi = $client->get(config('app.dsi_api_core').'/pengajuan', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'charset' => 'utf-8',
                    'Authorization' => 'Bearer ' . $apiToken
                ],
            ]);

            $responseApi = $requestApi->getBody()->getContents();
            return json_decode($responseApi);
        } catch (\GuzzleHttp\Exception\RequestException $e) {

            if($e->getResponse()){
              $requestResponse = json_decode($e->getResponse()->getBody()->getContents());
              Helper::logDbApp('Error Get Pengajuan', '43', 'User Token : ' . $apiToken . ' Response : ' . json_encode($requestResponse));
              return response()->json($requestResponse, 422);
            }else{
                return response()->json(['error' => config('app.env') !== 'production' ? $e->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
            }
           
        } 
    }


    public static function updatePengajuan($pengajuan_id): void
    {

        try {

            $client = new Client();

            $apiToken = JWTAuth::fromUser(Auth::guard('borrower-api-mobile')->user());

            $requestApi = $client->put(config('app.dsi_api_core').'/pengajuan/' . $pengajuan_id, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'charset' => 'utf-8',
                    'Authorization' => 'Bearer ' . $apiToken
                ]
            ]);

            $responseApi = $requestApi->getBody()->getContents();
            $data = json_decode($responseApi);
            //return response()->json(['success' => $data->success, 'pengajuan_id' => $data->pengajuan_id]);

        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $requestResponse = json_decode($e->getResponse()->getBody()->getContents());
            Helper::logDbApp('Error Update Pengajuan', '43', 'User Token : ' . $apiToken . ' Response : ' . json_encode($requestResponse));
            //return response()->json($requestResponse, 422);
        }
    }

    public static function sendDocument($documents, $pengajuan_id): void
    {

        try {

            foreach ($documents as $document_id) {

                $client = new Client();
                $body = ['persyaratan_id' => $document_id];

                $apiToken = JWTAuth::fromUser(Auth::guard('borrower-api-mobile')->user());

                $client->put(config('app.dsi_api_core').'/pengajuan/' . $pengajuan_id . '/documents', [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                        'charset' => 'utf-8',
                        'Authorization' => 'Bearer ' . $apiToken
                    ],
                    'body' => json_encode($body)
                ]);

                 Helper::logDbApp('[LOS] Send Document ', '97', 'Document id : ' . $document_id);

            }
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $requestResponse = json_decode($e->getResponse()->getBody()->getContents());
            Helper::logDbApp('Error Send Document', '94', 'User Token : ' . $apiToken . ' Response : ' . json_encode($requestResponse));
        }
    }

    public static function entryPengajuan($request)
    {
        $client = new Client();
        $body = [
            'tujuan_id' => $request->tujuan,
            'harga_objek' => $request->harga_objek,
            'uang_muka' => $request->uang_muka,
            'jenis_properti' => (int) $request->jenis_properti,
            'jangka_waktu' => (int) $request->jangka_waktu
        ];
        $apiToken = JWTAuth::fromUser(Auth::guard('borrower-api-mobile')->user());

        $requestApi = $client->request('POST', config('app.dsi_api_core').'/pengajuan', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'charset' => 'utf-8',
                'Authorization' => 'Bearer ' . $apiToken
            ],
            'body' => json_encode($body),
            'http_errors'=> false
        ]);

        $responseApi = $requestApi->getBody()->getContents();
        $data = json_decode($responseApi);
        return response()->json($data, $requestApi->getStatusCode());
    }


    public static function retryPengajuan($pengajuan_id, $brw_id){
         try {

            $client = new Client();
            $body = [
                'pengajuan_id' => $pengajuan_id,
                'brw_id' => $brw_id
            ];
    
            $dsiCoreAuth = config('app.dsi_auth_core');
            $requestApi = $client->post(config('app.dsi_api_core').'/pengajuan/retry', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'charset' => 'utf-8',
                    'Authorization' => $dsiCoreAuth
                ],
                'body' => json_encode($body)
            ]);

            $responseApi = $requestApi->getBody()->getContents();
            $data = json_decode($responseApi);
            return response()->json($data);
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $requestResponse = json_decode($e->getResponse()->getBody()->getContents());
            Helper::logDbApp('Error Retry Pengajuan', '167', 'Response ' . json_encode($requestResponse) .' Request : ' . json_encode($body));
            return response()->json(['success' => null, 'pengajuan_id' => null]);
        }
    }
}
