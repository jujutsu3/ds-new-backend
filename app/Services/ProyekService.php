<?php

namespace App\Services;

use App\Proyek;
use Illuminate\Contracts\Auth\Authenticatable;

class ProyekService
{
    public static function getByLenderClass(Authenticatable $user = null, $status = 1)
    {
        $detilInvestor = $user ? InvestorService::getDetilInvestor(InvestorService::getInvestor($user->id)) : null;
        $lenderClass = optional($detilInvestor, function ($item) {
            return $item->lender_class;
        }) ?: 0;
        
        return Proyek::query()
            ->where('status', $status)
            ->orderBy('proyek.profit_margin', 'desc')
            ->where('lender_class', $lenderClass)
            ->where(function ($query) use ($detilInvestor, $lenderClass) {
                if (isset($detilInvestor) && $lenderClass === 1) {
                    $query->where('investor_id', $detilInvestor->investor_id);
                }
            });
    }
}
