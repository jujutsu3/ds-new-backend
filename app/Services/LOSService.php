<?php

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\Client;
use App\BorrowerPengajuan;
use App\BorrowerLosLog;
use App\BorrowerDetails;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use JWTAuth;
use JWTFactory;
use Auth;
use Exception;


class LOSService
{

    public static function sendDataEntryLOS(BorrowerPengajuan $pengajuan)
    {

        try {

            $client = new Client();
            $mappingExtension = Helper::mappingExtension();
            $borrowerDetail = BorrowerDetails::where('brw_id', $pengajuan->brw_id)->first();
            $documents = self::getDocuments($borrowerDetail, $mappingExtension);
            
            $body = ['pengajuan_id'=> $pengajuan->pengajuan_id, 'docs' => $documents];
            $apiToken = JWTAuth::fromUser(Auth::guard('borrower-api-mobile')->user());

            $requestApi = $client->post('http://172.31.82.77:8889/v1/los/entry-datas', [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                    'charset' => 'utf-8',
                    'Authorization' => 'Bearer ' . $apiToken
                ],
                'body' => json_encode($body)
            ]);

            $response = json_decode($requestApi->getBody()->getContents());
        } catch (Exception $e) {
             Helper::logDbApp('Error Catch Data entry LOS', '58', 'User Token : '. $userToken. ' Message : '. $e->getMessage());
        }

        return 'OK';
    }


    public static function sendDataEntry($pengajuan): void
    {

        $client = new Client();
        $mappingExtension = Helper::mappingExtension();

        $appNo  = null;

        $query = DB::table("brw_user");
        $query->leftjoin("brw_user_detail", "brw_user.brw_id", "brw_user_detail.brw_id");
        $query->leftjoin("brw_user_detail_penghasilan", "brw_user_detail.brw_id", "brw_user_detail_penghasilan.brw_id2");
        $query->leftjoin("brw_pasangan", "brw_user_detail.brw_id", "brw_pasangan.brw_id2");
        $query->selectRaw(" brw_user.email, IF(brw_user_detail.brw_type = 1, UPPER(brw_user_detail.nama), UPPER(brw_user_detail.nm_bdn_hukum)) as full_name, 
        IF(brw_user_detail.brw_type = 1, UPPER(brw_user_detail.no_tlp), UPPER(brw_user_detail.telpon_perusahaan)) as contact_number,
        CASE WHEN brw_user_detail.jns_kelamin = 1 THEN 'Male'
             WHEN brw_user_detail.jns_kelamin = 2 THEN 'Female'
             ELSE '-' END AS gender, 
        CASE WHEN brw_user_detail.brw_type = 1 THEN 'P'
             WHEN brw_user_detail.brw_type = 2 THEN 'C'
             ELSE '-' END AS tipe_pengguna, brw_user_detail.tgl_lahir,
             brw_user_detail.pekerjaan AS jenis_pekerjaan,  brw_user_detail.tempat_lahir, brw_user_detail.nm_ibu, brw_user_detail.agama, brw_user_detail.ktp, brw_user_detail.npwp, 
             brw_user_detail.alamat, brw_user_detail.kelurahan, brw_user_detail.kecamatan, brw_user_detail.provinsi, brw_user_detail.kota, brw_user_detail.kode_pos,
             brw_user_detail.domisili_alamat, brw_user_detail.domisili_provinsi, brw_user_detail.domisili_kota,
             brw_user_detail.domisili_kelurahan, brw_user_detail.domisili_kecamatan, brw_user_detail.domisili_kd_pos,
             brw_user_detail.domisili_status_rumah, brw_user_detail.tlprumah, brw_user_detail.no_tlp, brw_user_detail_penghasilan.no_telp as no_telp_office,
             brw_user_detail.status_kawin, brw_user_detail_penghasilan.skema_pembiayaan, brw_user_detail.pendidikan_terakhir, brw_user_detail_penghasilan.pendapatan_borrower,
             brw_user_detail.status_rumah, brw_pasangan.nama as nama_pasangan, brw_pasangan.jenis_kelamin as pasangan_jenis_kelamin , brw_pasangan.tempat_lahir as pasangan_tempat_lahir, brw_pasangan.tgl_lahir as pasangan_tgl_lahir,
             brw_pasangan.agama as pasangan_agama, brw_pasangan.ktp as pasangan_ktp, brw_pasangan.npwp as pasangan_npwp, brw_pasangan.no_hp as pasangan_no_hp,
             brw_pasangan.alamat as pasangan_alamat, brw_pasangan.provinsi as pasangan_provinsi, brw_pasangan.kota as pasangan_kota, brw_pasangan.kecamatan as pasangan_kecamatan, 
             brw_pasangan.kelurahan as pasangan_kelurahan, brw_pasangan.kode_pos as pasangan_kode_pos, brw_pasangan.pendidikan as pasangan_pendidikan,
             brw_pasangan.pendapatan_borrower as pasangan_income, brw_user_detail_penghasilan.nama_perusahaan as office_name, 
             brw_user_detail_penghasilan.alamat_perusahaan as office_address, brw_user_detail_penghasilan.kelurahan as office_kelurahan,  
             brw_user_detail_penghasilan.kecamatan as office_kecamatan, brw_user_detail_penghasilan.kab_kota as office_kota, brw_user_detail_penghasilan.provinsi as office_provinsi,  brw_user_detail_penghasilan.kode_pos as office_kode_pos,
             brw_user_detail_penghasilan.departemen, brw_user_detail_penghasilan.jabatan, brw_user_detail_penghasilan.status_pekerjaan,
             date_format(DATE_SUB(date_format(NOW(),'%Y%m%d'), INTERVAL ifnull(brw_user_detail_penghasilan.masa_kerja_tahun*12 + brw_user_detail_penghasilan.masa_kerja_bulan, 0) MONTH), '%d-%m-%Y') as bekerja_sejak,
             brw_pasangan.nama_perusahaan as office_pasangan, brw_pasangan.total_penghasilan_lain_lain as pasangan_income_lain, brw_pasangan.alamat_perusahaan as office_alamat_pasangan,
             brw_pasangan.kelurahan_pekerjaan_pasangan, brw_pasangan.kecamatan_pekerjaan_pasangan, brw_pasangan.kab_kota_pekerjaan_pasangan, brw_pasangan.provinsi_pekerjaan_pasangan, brw_pasangan.kode_pos_pekerjaan_pasangan,
             brw_pasangan.usia_perusahaan as pasangan_lama_usaha, brw_user_detail.pekerjaan_pasangan as pasangan_pekerjaan, brw_pasangan.departemen as pasangan_departemen, brw_pasangan.jabatan as pasangan_jabatan,
             date_format(DATE_SUB(date_format(NOW(),'%Y%m%d'), INTERVAL ifnull(brw_pasangan.masa_kerja_tahun*12 + brw_pasangan.masa_kerja_bulan, 0) MONTH), '%d-%m-%Y') as bekerja_sejak_pasangan,
             brw_pasangan.status_pekerjaan as status_pekerjaan_pasangan, brw_pic, brw_pic_ktp, brw_pic_user_ktp, brw_pic_npwp
             ");

        $borrowerDetail = $query->where('brw_user.brw_id', $pengajuan->brw_id)->first();


        $dataDebitur = array(
            'Gender' => $borrowerDetail->gender,
            'IDName' => $borrowerDetail->full_name,
            'FullName' => $borrowerDetail->full_name,
            'POB' =>  $borrowerDetail->tempat_lahir,
            'DOB' => Carbon::parse($borrowerDetail->tgl_lahir)->format('d-m-Y'),
            'MotherMaidenName' => $borrowerDetail->nm_ibu,
            'ReligionId' => $borrowerDetail->agama,
            'isPG' => false,
            "IDType" => "KTP",
            "IDNumber" => $borrowerDetail->ktp,
            "NPWP" => $borrowerDetail->npwp,
            "LegalAddress" => $borrowerDetail->alamat,
            "LegalKelurahan" => $borrowerDetail->kelurahan,
            "LegalKecamatan" => $borrowerDetail->kecamatan,
            "LegalProvince" => $borrowerDetail->provinsi,
            "LegalCity" => $borrowerDetail->kota,
            "LegalZipCode" => $borrowerDetail->kode_pos,
            "ResidenceAddress" => $borrowerDetail->domisili_alamat,
            "ResidenceCity" => $borrowerDetail->domisili_kota,
            "ResidenceKecamatan" => $borrowerDetail->domisili_kecamatan,
            "ResidenceKelurahan" => $borrowerDetail->domisili_kelurahan,
            "ResidenceProvince" => $borrowerDetail->domisili_provinsi,
            "ResidenceZipCode" => $borrowerDetail->domisili_kd_pos,
            "HomePhone" => $borrowerDetail->tlprumah,
            "OfficePhone" => $borrowerDetail->no_telp_office, //user detail penghasilan
            "MobilePhone" => $borrowerDetail->no_tlp,
            "MaritalId" =>  $borrowerDetail->status_kawin,
            "isJointIncome" => (isset($borrowerDetail->skema_pembiayaan) && $borrowerDetail->skema_pembiayaan == 2) ?  true : false,
            "Education" => $borrowerDetail->pendidikan_terakhir,
            "Nationality" => "WNI",
            "PersonalEmail" => $borrowerDetail->email,
            "MailingAddress" => "0",
            "WNACountryId" => "",
            "OfficeEmail" => "",
            "StatusKepemilikanId" => $borrowerDetail->status_rumah,
            "StatusTempatTinggalId" => $borrowerDetail->domisili_status_rumah,
            "Income" => floatval($borrowerDetail->pendapatan_borrower)
        );


        $dataJobDebitur = array(
            "CompanyName" => $borrowerDetail->office_name,
            "JumlahKaryawan" => "",
            "Income" => floatval($borrowerDetail->pendapatan_borrower),
            "OtherIncome" => 0,
            "CopyOfficeAddress" => false,
            "OfficeAddress" => $borrowerDetail->office_address,
            "OfficeKelurahan" => $borrowerDetail->office_kelurahan,
            "OfficeKecamatan" => $borrowerDetail->office_kecamatan,
            "OfficeCity" => $borrowerDetail->office_kota,
            "OfficeProvince" => $borrowerDetail->office_provinsi,
            "OfficeZipCode" => $borrowerDetail->office_kode_pos,
            "StatusTempatUsahaId" => "",
            "LamaUsaha" => "",
            "JumlahSupplier" => 0,
            "StatusTempatTinggalId" => $borrowerDetail->domisili_status_rumah,
            "JenisPekerjaan" => $borrowerDetail->jenis_pekerjaan,
            "BadanUsaha" => "",
            "SektorEkonomi" => "",
            "DivisiKerja" => $borrowerDetail->departemen,
            "Jabatan" => $borrowerDetail->jabatan,
            "BekerjaSejak" => $borrowerDetail->bekerja_sejak,
            "StatusKaryawan" => $borrowerDetail->status_pekerjaan,
            "OfficePhone" => "",
            "OfficeFacPhone" => "",
            "NomorIjinUsaha" => "",
            "ExpIjin" => "",
            "JenisDokumenUsaha" => "",
            "DetailBidangUsaha" => "",
            "JenisDokumenUsahaUrus" => "",
            "JenisUsaha" => "",
            "LokasiUsaha" => "",
            "TanggalBerdiri" => "",
            "TempatPendirian" => "",
            "NPWPPerusahaan" => "",
            "RadiusUsaha" => "",
            "OmzetPerBulan" => "",
            "PunyaCabangUsaha" => "",
            "PasanganIkutTerlibat" => "",
            "Profesi" => "",
            "JenisDokumenProfesional" => "",
            "NomorIjinDokumenProfesional" => "",
            "NPWPProfessional" => ""
        );


        $dataFacility = array(
            "FacSeq" => "1",
            "ProductId" => $pengajuan->pendanaan_tipe,
            "ProgramId" => $pengajuan->pendanaan_tujuan,
            "Plafon" => floatval($pengajuan->harga_objek_pendanaan),
            "Tenor" => $pengajuan->durasi_proyek,
            "TotalPlafon" => floatval($pengajuan->pendanaan_dana_dibutuhkan),
            "DownPayment" => floatval($pengajuan->uang_muka)
        );

        $dataPasangan = array();
        if (!empty($borrowerDetail->nama_pasangan)) {
            $dataPasangan = array(
                "Gender" => $borrowerDetail->pasangan_jenis_kelamin,
                "IDName" => $borrowerDetail->nama_pasangan,
                "FullName" => $borrowerDetail->nama_pasangan,
                "POB" => $borrowerDetail->pasangan_tempat_lahir,
                "DOB" =>  Carbon::parse($borrowerDetail->pasangan_tgl_lahir)->format('d-m-Y'),
                "MotherMaidenName" => "-",
                "ReligionId" => $borrowerDetail->pasangan_agama,
                "IDType" => "KTP",
                "IDNumber" => $borrowerDetail->pasangan_ktp,
                "NPWP" => $borrowerDetail->pasangan_npwp,
                "LegalAddress" => $borrowerDetail->pasangan_alamat,
                "LegalKelurahan" => $borrowerDetail->pasangan_kelurahan,
                "LegalKecamatan" => $borrowerDetail->pasangan_kecamatan,
                "LegalProvince" => $borrowerDetail->pasangan_provinsi,
                "LegalCity" => $borrowerDetail->pasangan_kota,
                "LegalZipCode" => $borrowerDetail->pasangan_kode_pos,
                "ResidenceAddress" => $borrowerDetail->pasangan_alamat,
                "ResidenceCity" => $borrowerDetail->pasangan_kota,
                "ResidenceKecamatan" => $borrowerDetail->pasangan_kecamatan,
                "ResidenceKelurahan" => $borrowerDetail->pasangan_kelurahan,
                "ResidenceProvince" => $borrowerDetail->pasangan_provinsi,
                "ResidenceZipCode" => $borrowerDetail->pasangan_kode_pos,
                "HomePhone" => "",
                "OfficePhone" => "",
                "MobilePhone" => $borrowerDetail->pasangan_no_hp,
                "Education" => $borrowerDetail->pasangan_pendidikan,
                "Nationality" => "WNI",
                "PersonalEmail" => "",
                "MailingAddress" => "0",
                "WNACountryId" => "",
                "OfficeEmail" => "",
                "StatusKepemilikanId" => $borrowerDetail->status_rumah,
                "StatusTempatTinggalId" => $borrowerDetail->domisili_status_rumah,
                "CopySpouse" => false,
                "Income" => floatval($borrowerDetail->pasangan_income)
            );
        }


        $dataJobPasangan = array(

            "CompanyName" => $borrowerDetail->office_pasangan,
            "JumlahKaryawan" => "",
            "Income" => floatval($borrowerDetail->pasangan_income),
            "OtherIncome" => floatval($borrowerDetail->pasangan_income_lain),
            "CopyOfficeAddress" => false,
            "OfficeAddress" => $borrowerDetail->office_alamat_pasangan,
            "OfficeKelurahan" => $borrowerDetail->kelurahan_pekerjaan_pasangan,
            "OfficeKecamatan" => $borrowerDetail->kecamatan_pekerjaan_pasangan,
            "OfficeCity" => $borrowerDetail->kab_kota_pekerjaan_pasangan,
            "OfficeProvince" => $borrowerDetail->provinsi_pekerjaan_pasangan,
            "OfficeZipCode" => $borrowerDetail->kode_pos_pekerjaan_pasangan,
            "StatusTempatUsahaId" => "",
            "LamaUsaha" => $borrowerDetail->pasangan_lama_usaha,
            "JumlahSupplier" =>  0,
            "StatusTempatTinggalId" => "1",
            "JenisPekerjaan" => $borrowerDetail->pasangan_pekerjaan,
            "BadanUsaha" => "",
            "SektorEkonomi" => 0,
            "DivisiKerja" => $borrowerDetail->pasangan_departemen,
            "Jabatan" => $borrowerDetail->pasangan_jabatan,
            "BekerjaSejak" => $borrowerDetail->bekerja_sejak_pasangan,
            "StatusKaryawan" => $borrowerDetail->status_pekerjaan_pasangan,
            "OfficePhone" => "",
            "OfficeFacPhone" => "",
            "NomorIjinUsaha" => "",
            "ExpIjin" => "",
            "JenisDokumenUsaha" => "10004",
            "DetailBidangUsaha" => "",
            "JenisDokumenUsahaUrus" => "",
            "JenisUsaha" => "",
            "LokasiUsaha" => "",
            "TanggalBerdiri" => "",
            "TempatPendirian" => "",
            "NPWPPerusahaan" => "",
            "RadiusUsaha" => "",
            "OmzetPerBulan" => "",
            "PunyaCabangUsaha" => "",
            "PasanganIkutTerlibat" => "",
            "Profesi" => "",
            "JenisDokumenProfesional" => "",
            "NomorIjinDokumenProfesional" => "",
            "NPWPProfessional" => ""
        );


        $dataGuarantor = array(
            "Gender" => "",
            "IDName" => "",
            "FullName" => "",
            "POB" => "",
            "DOB" => "",
            "MotherMaidenName" => "",
            "ReligionId" => "",
            "IDType" => "",
            "IDNumber" => "",
            "NPWP" => "",
            "LegalAddress" => "",
            "LegalKelurahan" => "",
            "LegalKecamatan" => "",
            "LegalProvince" => "",
            "LegalCity" => "",
            "LegalZipCode" => "",
            "ResidenceAddress" => "",
            "ResidenceCity" => "",
            "ResidenceKecamatan" => "",
            "ResidenceKelurahan" => "",
            "ResidenceProvince" => "",
            "ResidenceZipCode" => "",
            "HomePhone" => "",
            "OfficePhone" => "",
            "MobilePhone" => "",
            "MaritalId" => "",
            "isJointIncome" => "",
            "Education" => "",
            "Nationality" => "",
            "PersonalEmail" => "",
            "MailingAddress" => "",
            "WNACountryId" => "",
            "OfficeEmail" => "",
            "StatusKepemilikanId" => "",
            "StatusTempatTinggalId" => "",
            "Income" => ""
        );


        $dataJobGuarantor = array(
            "CompanyName" => "",
            "JumlahKaryawan" => "",
            "Income" => "",
            "OtherIncome" => "",
            "CopyOfficeAddress" => false,
            "OfficeAddress" => "",
            "OfficeKelurahan" => "",
            "OfficeKecamatan" => "",
            "OfficeCity" => "",
            "OfficeProvince" => "",
            "OfficeZipCode" => "",
            "StatusTempatUsahaId" => "",
            "LamaUsaha" => "",
            "JumlahSupplier" => "",
            "StatusTempatTinggalId" => "",
            "JenisPekerjaan" => "",
            "BadanUsaha" => "",
            "SektorEkonomi" => "",
            "DivisiKerja" => "",
            "Jabatan" => "",
            "BekerjaSejak" => "",
            "StatusKaryawan" => "",
            "OfficePhone" => "",
            "OfficeFacPhone" => "",
            "NomorIjinUsaha" => "",
            "ExpIjin" => "",
            "JenisDokumenUsaha" => "",
            "DetailBidangUsaha" => "",
            "JenisDokumenUsahaUrus" => "",
            "JenisUsaha" => "",
            "LokasiUsaha" => "",
            "TanggalBerdiri" => "",
            "TempatPendirian" => "",
            "NPWPPerusahaan" => "",
            "RadiusUsaha" => "",
            "OmzetPerBulan" => "",
            "PunyaCabangUsaha" => "",
            "PasanganIkutTerlibat" => "",
            "Profesi" => "",
            "JenisDokumenProfesional" => "",
            "NomorIjinDokumenProfesional" => "",
            "NPWPProfessional" => ""
        );



        $dataDocuments = self::getDocuments($borrowerDetail, $mappingExtension);


        $body = [
            'RequestId' => $pengajuan->pengajuan_id,
            'BranchId' => '20001',
            'CustExists' => '0',
            'CustTypeId' => $borrowerDetail->tipe_pengguna,
            'RequestDate' => Carbon::parse($pengajuan->created_at)->format('d-m-Y'),
            'DataDebitur' => $dataDebitur,
            'DataJobDebitur' => $dataJobDebitur,
            'DataSpouse' => $dataPasangan,
            'DataJobSpouse' => $dataJobPasangan,
            'DataGuarantor' => $dataGuarantor,
            'DataJobGuarantor' => $dataJobGuarantor,
            'DataCollateral' => null,
            'DataFacility' => $dataFacility,
            'Document' => $dataDocuments
        ];

        $requestApi = $client->post(config('app.los_url') . '/DANASYARIAH_EVO/PipeLine/SendDataEntry', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'charset' => 'utf-8',
            ],
            'body' => json_encode($body)
        ]);

        $response = json_decode($requestApi->getBody()->getContents());

        if ($response->Flag === 'Y') {
            BorrowerPengajuan::where('pengajuan_id', $pengajuan->pengajuan_id)->update(['app_no_LOS' => $response->Data->AppNo, 'created_at_LOS' =>  Carbon::now()]);
        }

        BorrowerLosLog::create([
            'pengajuan_id' => $pengajuan->pengajuan_id,
            'app_no' => (isset($response->Data->AppNo) && !empty($response->Data->AppNo)) ?  $response->Data->AppNo : null,
            'request_data' => json_encode($body),
            'response_data' => json_encode($response),
            'method' => 'sendDataEntry',
            'status' => $response->Flag,
            'message' => $response->Message,

        ]);
    }


    public static function getDocuments($borrowerDetail, $mappingExtension)
    {
        $dataDocuments = [];

        $destinationPath = storage_path('app/private/');
        $fileBrwPic = $destinationPath . $borrowerDetail->brw_pic;
        $fileBrwPicKtp = $destinationPath . $borrowerDetail->brw_pic_ktp;
        $fileBrwPicUserKtp = $destinationPath . $borrowerDetail->brw_pic_user_ktp;
        $fileBrwPicNpwp = $destinationPath . $borrowerDetail->brw_pic_npwp;

        //Doc brw_pic_ktp
        if (file_exists($fileBrwPicKtp)) {
            $extension = pathinfo(parse_url($fileBrwPicKtp, PHP_URL_PATH), PATHINFO_EXTENSION);
            $pic = file_get_contents($fileBrwPicKtp);
            $file = $mappingExtension[strtolower($extension)] . base64_encode($pic);

            $dataDocuments[] = [
                "doc_code" => "291",
                "file" => $file
                // "DocCode" => "291",
                // "DocNo" => "",
                // "DocSeq" => "1",
                // "AltType" => "1",
                // "AltId" => "",
                // "Owner" => "Borrower",
                // "ExpiredDate" => "02-01-2031",
                // "Remarks" => "Debitur Pic Ktp",
                // "File" => $file
            ];
        }

        //Doc brw_pic
        if (file_exists($fileBrwPic)) {
            $extension = pathinfo(parse_url($fileBrwPic, PHP_URL_PATH), PATHINFO_EXTENSION);
            $pic = file_get_contents($fileBrwPic);
            $file = $mappingExtension[strtolower($extension)] . base64_encode($pic);

            $dataDocuments[] = [
                "doc_code" => "405",
                "file" => $file
                // "DocCode" => "405",
                // "DocNo" => "",
                // "DocSeq" => "1",
                // "AltType" => "1",
                // "AltId" => "",
                // "Owner" => "Borrower",
                // "ExpiredDate" => "02-01-2031",
                // "Remarks" => "Debitur Pic Foto Selfie",
                // "File" => $file
            ];
        }

        //Doc brw_pic_user_ktp
        if (file_exists($fileBrwPicUserKtp)) {
            $extension = pathinfo(parse_url($fileBrwPicUserKtp, PHP_URL_PATH), PATHINFO_EXTENSION);
            $pic = file_get_contents($fileBrwPicUserKtp);
            $file = $mappingExtension[strtolower($extension)] . base64_encode($pic);

            $dataDocuments[] = [
                "doc_code" => "406",
                "file" => $file
                // "DocCode" => "406",
                // "DocNo" => "",
                // "DocSeq" => "1",
                // "AltType" => "1",
                // "AltId" => "",
                // "Owner" => "Borrower",
                // "ExpiredDate" => "02-01-2031",
                // "Remarks" => "Debitur Pic Foto Selfie & KTP",
                // "File" => $file
            ];
        }

        //Doc brw_pic_npwp
        if (file_exists($fileBrwPicNpwp)) {
            $extension = pathinfo(parse_url($fileBrwPicNpwp, PHP_URL_PATH), PATHINFO_EXTENSION);
            $pic = file_get_contents($fileBrwPicNpwp);
            $file = $mappingExtension[strtolower($extension)] . base64_encode($pic);

            $dataDocuments[] = [
                "doc_code" => "292",
                "file" => $file
                // "DocCode" => "292",
                // "DocNo" => "",
                // "DocSeq" => "1",
                // "AltType" => "1",
                // "AltId" => "",
                // "Owner" => "Borrower",
                // "ExpiredDate" => "02-01-2031",
                // "Remarks" => "Debitur Pic Foto NPWP",
                // "File" => $file
            ];
        }

        return $dataDocuments;
    }

    public static function sendDocument($docCode, $data = [])
    {

        $clientDoc = new Client();


        $body = [
            "AppNo" => $data['app_no'],
            "DocCode" => $docCode,
            "DocNo" => "92",
            "DocSeq" => "1",
            "AltType" => "1",
            "AltId" => "",
            "Owner" => "Borrower",
            "ExpiredDate" => "02-01-2031",
            "Remarks" => $data['remarks'],
            "File" => $data['file']
        ];


        $requestApiDoc = $clientDoc->post(config('app.los_url') . '/DANASYARIAH_EVO/PipeLine/SendDokumen', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'charset' => 'utf-8',
            ],
            'body' => json_encode($body)
        ]);


        $responseDoc = json_decode($requestApiDoc->getBody()->getContents());

        BorrowerLosLog::create([
            'pengajuan_id' => $data['pengajuan_id'],
            'app_no' => (isset($responseDoc->Data->AppNo) && !empty($responseDoc->Data->AppNo)) ?  $responseDoc->Data->AppNo : null,
            'request_data' => json_encode($body),
            'response_data' => json_encode($responseDoc),
            'method' => 'sendDocument',
            'status' => $responseDoc->Flag,
            'message' => $responseDoc->Message

        ]);
    }
}
