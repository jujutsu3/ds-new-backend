<?php

namespace App\Services;

class LenderLaporanKeuanganFileService extends FileService
{
    /**
     * @param $investorId
     * @return string
     */
    public static function getDiskPath($investorId)
    {
        return "lap_keuangan/{$investorId}";
    }

    protected static function getRouteName()
    {
        return 'v1.lender.file';
    }
}
