<?php

namespace App\Services;

use App\Borrower;
use App\BorrowerAnalisaPendanaan;
use App\BorrowerCorporation;
use App\BorrowerDetailPasangan;
use App\BorrowerDetailPenghasilan;
use App\BorrowerDetails;
use App\BorrowerIndividu;
use App\BorrowerPengajuan;
use App\BorrowerRekening;
use App\Helpers\Helper;
use Illuminate\Support\Facades\Auth;

class BorrowerService
{

    public static function getLoggedInBorrowerId()
    {
        return Auth::id();
    }

    /**
     * @param $borrowerId
     * @return \App\Borrower|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public static function getBorrower($borrowerId = null)
    {
        if ($borrowerId === null) {
            $borrowerId = self::getLoggedInBorrowerId();
        }

        /** @var Borrower $borrower */
        return Borrower::query()->where('brw_id', $borrowerId)->firstOrFail();
    }

    public static function getBorrowerCorporation($borrowerId = null)
    {
        if ($borrowerId === null) {
            $borrowerId = self::getLoggedInBorrowerId();
        }

        /** @var \App\BorrowerCorporation $borrower */
        $borrower = BorrowerCorporation::query()->where('brw_id', $borrowerId)->firstOrFail();

        return $borrower;
    }

    public static function getBorrowerIndividu($borrowerId = null)
    {
        if ($borrowerId === null) {
            $borrowerId = self::getLoggedInBorrowerId();
        }

        /** @var \App\BorrowerCorporation $borrower */
        $borrower = BorrowerIndividu::query()->where('brw_id', $borrowerId)->firstOrFail();

        return $borrower;
    }

    /**
     * @param \App\Borrower $borrower
     * @return \App\BorrowerDetails|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public static function getDetilBorrower(Borrower $borrower)
    {
        $brwId = $borrower->brw_id;

        return BorrowerDetails::query()
            ->where('brw_id', $brwId)
            ->firstOrCreate([
                'brw_id' => $brwId,
            ], [
                'brw_type' => null,
                'jns_kelamin' => null,
                'agama' => null,
                'status_kawin' => null,
                'status_rumah' => null,
                'pendidikan_terakhir' => null,
                'pekerjaan' => null,
                'bidang_pekerjaan' => null,
                'bidang_online' => null,
                'pengalaman_pekerjaan' => null,
                'pendapatan' => null,
                'brw_online' => null,
            ]);
    }

    /**
     * @param \App\Borrower $borrower
     * @return \App\BorrowerDetails|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public static function getBorrowerRekening(Borrower $borrower)
    {
        $brwId = $borrower->brw_id;

        return BorrowerRekening::query()
            ->where('brw_id', $brwId)
            ->firstOrCreate([
                'brw_id' => $brwId,
            ], [
                'va_number' => null,
                'brw_norek' => null,
                'brw_nm_pemilik' => null,
                'brw_kd_bank' => null,
                'total_plafon' => null,
                'total_terpakai' => null,
                'total_sisa' => null,
                'status_isi_data_rekening' => null,
                'kantor_cabang_pembuka' => null,
            ]);
    }

    public static function getDetilBorrowerPenghasilan(Borrower $borrower)
    {
        $brwId = $borrower->brw_id;

        return BorrowerDetailPenghasilan::query()
            ->where('brw_id2', $brwId)
            ->firstOrCreate([
                'brw_id2' => $brwId,
            ], [
                'sumber_pengembalian_dana' => null,
                'skema_pembiayaan' => null,
                'nama_perusahaan' => null,
                'alamat_perusahaan' => null,
                'rt' => null,
                'rw' => null,
                'provinsi' => null,
                'kab_kota' => null,
                'kecamatan' => null,
                'kelurahan' => null,
                'kode_pos' => null,
                'no_telp' => null,
                'no_hp' => null,
                'surat_ijin' => null,
                'no_surat_ijin' => null,
                'bentuk_badan_usaha' => null,
                'status_pekerjaan' => null,
                'usia_perusahaan' => null,
                'usia_tempat_usaha' => null,
                'departemen' => null,
                'jabatan' => null,
                'masa_kerja_tahun' => null,
                'masa_kerja_bulan' => null,
                'nip_nrp_nik' => null,
                'nama_hrd' => null,
                'no_fixed_line_hrd' => null,
                'pengalaman_kerja_tahun' => null,
                'pengalaman_kerja_bulan' => null,
                'pendapatan_borrower' => null,
                'biaya_hidup' => null,
                'detail_penghasilan_lain_lain' => null,
                'total_penghasilan_lain_lain' => null,
                'nilai_spt' => null,
            ]);
    }

    public static function getDetilBorrowerPasangan(Borrower $borrower)
    {
        $brwId = $borrower->brw_id;

        return BorrowerDetailPasangan::query()
            ->where('brw_id2', $brwId)
            ->firstOrCreate([
                'pasangan_id' => $brwId,
                'brw_id2' => $brwId,
            ], [
                'nama' => null,
                'jenis_kelamin' => null,
                'ktp' => null,
                'tempat_lahir' => null,
                'tgl_lahir' => null,
                'no_hp' => null,
                'agama' => null,
                'pendidikan' => null,
                'npwp' => null,
                'alamat' => null,
                'provinsi' => null,
                'kota' => null,
                'kecamatan' => null,
                'kelurahan' => null,
                'kode_pos' => null,
                'no_kk' => null,
                'sumber_pengembalian_dana' => null,
                'skema_pembiayaan' => null,
                'nama_perusahaan' => null,
                'alamat_perusahaan' => null,
                'rt_pekerjaan_pasangan' => null,
                'rw_pekerjaan_pasangan' => null,
                'provinsi_pekerjaan_pasangan' => null,
                'kab_kota_pekerjaan_pasangan' => null,
                'kecamatan_pekerjaan_pasangan' => null,
                'kelurahan_pekerjaan_pasangan' => null,
                'kode_pos_pekerjaan_pasangan' => null,
                'no_telp_pekerjaan_pasangan' => null,
                'no_hp_pekerjaan_pasangan' => null,
                'surat_ijin' => null,
                'no_surat_ijin' => null,
                'bentuk_badan_usaha' => null,
                'status_pekerjaan' => null,
                'usia_perusahaan' => null,
                'usia_tempat_usaha' => null,
                'departemen' => null,
                'jabatan' => null,
                'masa_kerja_tahun' => null,
                'masa_kerja_bulan' => null,
                'nip_nrp_nik' => null,
                'nama_hrd' => null,
                'no_fixed_line_hrd' => null,
                'pengalaman_kerja_tahun' => null,
                'pengalaman_kerja_bulan' => null,
                'pendapatan_borrower' => null,
                'biaya_hidup' => null,
                'detail_penghasilan_lain_lain' => null,
                'total_penghasilan_lain_lain' => null,
                'nilai_spt' => null,
            ]);
    }

    /**
     * @param array $data
     * @param array $map of [request_key => database_column]
     * @return array
     */
    public static function mapToDatabase(array $data, array $map): array
    {
        return collect($data)
            ->only(array_keys($map))
            ->mapWithKeys(function ($value, $key) use ($map) {
                return [$map[$key] => $value];
            })
            ->toArray();
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @param bool $withLocked
     * @return void
     */
    public static function validatePengajuanOwnershipAndLock(BorrowerPengajuan $pengajuan, $withLocked = true): void
    {
        if ($pengajuan->brw_id !== self::getLoggedInBorrowerId()) {
            abort(403);
        }

        if ($withLocked && self::isPengajuanLocked($pengajuan)) {
            abort(403, 'Pengajuan sudah terkunci');
        }
    }

    /**
     * @param \App\BorrowerPengajuan $pengajuan
     * @return bool
     */
    private static function isPengajuanLocked(BorrowerPengajuan $pengajuan)
    {
        $analisanPendanaan = BorrowerAnalisaPendanaan::query()
            ->where('pengajuan_id', $pengajuan->pengajuan_id)
            ->first();

        return ($analisanPendanaan !== null && $analisanPendanaan->status_verifikator >= 3);
    }

    public static function canSubmitPengajuan(Borrower $borrower)
    {
        return Helper::checkLimitPengajuan($borrower->brw_id);
    }

}
