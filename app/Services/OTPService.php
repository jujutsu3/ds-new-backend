<?php

namespace App\Services;

use App\Borrower;
use App\Investor;
use GuzzleHttp;
use Illuminate\Support\Facades\Validator;

class OTPService
{
    /** @var string SMS Maskin Name */
    private static $from = "DANASYARIAH";

    /** @var string Username for auth */
    private static $username = "danasyariahpremium";

    /** @var string Password for auth */
    private static $password = "Dsi701@2019";

    /** @var string Where to post to */
    private static $url = "http://107.20.199.106/restapi/sms/1/text/advanced";

    public static function trim($phone)
    {
        if (substr($phone, 0, 2) !== '62') {
            return $phone;
        }

        $phone = "62" . ltrim($phone, '62+62');
        $phone = "62" . ltrim($phone, '6262');
        $phone = "62" . ltrim($phone, '620');
        $phone = "62" . ltrim($phone, '62');

        return $phone;
    }

    private static function send($originalTo, $otp)
    {
        $text = "<#> DANASYARIAH-JANGAN MEMBERITAHU KODE INI KE SIAPAPUN termasuk pihak DANASYARIAH. Kode OTP : $otp Silahkan masukan kode ini untuk melanjutkan proses pendaftaran anda.\nX96Ckidjiws";

        $to = self::trim($originalTo);

        $validator = Validator::make([
            'phone' => $to,
        ], [
            'phone' => ['numeric', 'digits_between:11,15'],
        ], [
            'phone.numeric' => 'No telpon ' . $originalTo . ' tidak valid',
            'phone.digits_between' => 'No telpon ' . $originalTo . ' tidak valid',
        ]);

        if ($validator->fails()) {
            return [false, $validator->getMessageBag()->get('phone')[0] ?? "No telpon tidak valid"];
        }

        $postData = [
            "messages" => [[
                "from" => self::$from,
                "destinations" => [
                    "to" => $to,
                ],
                "text" => $text,
                "smsCount" => 20,
            ]],
        ];

        $client = new GuzzleHttp\Client();
        $response = $client->request('POST', self::$url, [
            GuzzleHttp\RequestOptions::JSON => $postData,
            GuzzleHttp\RequestOptions::HEADERS => [
                "Content-Type" => "application/json",
                "Accept" => "application/json",
            ],
            GuzzleHttp\RequestOptions::AUTH => [self::$username, self::$password],
            GuzzleHttp\RequestOptions::CONNECT_TIMEOUT => 2,
            GuzzleHttp\RequestOptions::VERIFY => false,
        ]);

        $httpCode = $response->getStatusCode();

        if ($httpCode === 200) {
            return [true, "Berhasil mengirim OTP"];
        }

        return [false, "Gagal mengirim OTP"];
    }

    /**
     * @param \App\Investor $investor
     * @param $phone
     * @return array [bool $status, string $message]
     * @throws \Exception
     */
    public static function investorSend(Investor $investor, $phone)
    {
        $otp = random_int(100000, 999999);
        $investor->forceFill(['otp' => $otp])->save();

        [$status, $message] = self::send($phone, $otp);

        return [$status, $message];
    }

    /**
     * @param \App\Borrower $borrower
     * @param $phone
     * @return array [bool $status, string $message]
     * @throws \Exception
     */
    public static function borrowerSend(Borrower $borrower, $phone)
    {
        $otp = random_int(100000, 999999);
        $borrower->forceFill(['otp' => $otp])->save();

        [$status, $message] = self::send($phone, $otp);

        return [$status, $message];
    }
}
