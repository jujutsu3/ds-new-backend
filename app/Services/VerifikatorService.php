<?php

namespace App\Services;

use DB;
use Mail;
use PDF;

use Illuminate\Support\Facades\Storage;

use App\Helpers\Helper;
use App\Mail\EmailSPKVerifikator;
use App\BorrowerAnalisaPendanaan;
use App\BorrowerVerifikatorPengajuan;
use App\BorrowerPengajuan;
use Carbon\Carbon;
use Exception;

use Auth;

class VerifikatorService
{

    public function generate_no_spk()
    {
        $nomor_spk = DB::table('brw_hd_verifikator')->max('nomor_spk');
        $nomor_spk += 1;

        return $nomor_spk;
    }

    public function get_nomor_surat()
    {
        return sprintf("%04s", $this->generate_no_spk()) . '/DSI/SPK/' . date('m') . '/' . date('Y');
    }

    public function makeAnalisaPendanaan($pengajuan_id, $borrower_id, $email_verifikator, $status_verifikator)
    {

        try {

            $tanggal_verifikasi = DB::table('brw_verifikasi_awal')->where('pengajuan_id', $pengajuan_id)->where('brw_id', $borrower_id)->value('created_at');
            $pengajuan = DB::table('brw_pengajuan')->where('pengajuan_id', $pengajuan_id)->where('brw_id', $borrower_id)->first();
            $borrower_detail = DB::table('brw_user_detail_pengajuan')->where('pengajuan_id', $pengajuan_id)->first();

            $brw_hrd_verfikator = DB::table('brw_hd_verifikator')->where('pengajuan_id', $pengajuan_id)->first();

            $result = BorrowerAnalisaPendanaan::updateOrCreate(
                ['pengajuan_id' => $pengajuan_id],
                [
                    'tanggal_pengajuan' => date('Y-m-d', strtotime($pengajuan->created_at)),
                    'tanggal_verifikasi' => date('Y-m-d', strtotime($tanggal_verifikasi)),
                    'penerima_pendanaan' => $borrower_detail->nama,
                    'jenis_pendanaan' => $pengajuan->pendanaan_tipe,
                    'tujuan_pendanaan' => $pengajuan->pendanaan_tujuan,
                    'nilai_pengajuan_pendanaan' => $pengajuan->pendanaan_dana_dibutuhkan,
                    'status_verifikator' => $status_verifikator
                ]
            );

            if (!$brw_hrd_verfikator) {
                $nomor_surat =  $this->insertSPKVerifikator($pengajuan, $borrower_detail);
                $this->sendMailSPK($nomor_surat, $pengajuan, $borrower_detail, $email_verifikator);
            }

            return $result;
        } catch (\Exception $e) {
            dd("Error Analisa Pendanaan : " . $e->getMessage());
        }
    }


    public function insertSPKVerifikator($pengajuan,  $borrower_detail)
    {

        try {

            $nomor_surat = $this->get_nomor_surat();
            $domisili_alamat = $borrower_detail->domisili_alamat . ' ' .  $borrower_detail->domisili_provinsi . ' ' .   $borrower_detail->domisili_kota . ' ' . $borrower_detail->domisili_kecamatan . ' ' . $borrower_detail->kelurahan . ' ' . $borrower_detail->domisili_kd_pos;
            $pendanaan_nama = DB::table('brw_tipe_pendanaan')->where('tipe_id', $pengajuan->pendanaan_tipe)->value('pendanaan_nama');
            $data = ['pengajuan_id' => $pengajuan->pengajuan_id, 'borrower' => $borrower_detail, 'domisili_alamat' => $domisili_alamat, 'nomor_surat' => $nomor_surat, 'tanggal_surat' => Helper::date_indonesia(date('Y-m-d')), 'pendanaan_nama' => $pendanaan_nama];

            $pdf = PDF::loadView('pages.borrower.pdf_spk_verifikator',  ['data' => $data]);
            $filePath = 'spk/' . $pengajuan->pengajuan_id . '/' . $pengajuan->pengajuan_id . '-' . date('dmY') . '-SK-Occasio.pdf';

            Storage::put('private/' . $filePath, $pdf->output());

            $data = [
                'pengajuan_id' => $pengajuan->pengajuan_id,
                'spk' => $filePath,
                'nomor_spk' => $this->generate_no_spk(),
                'flag_spk' => 1,
                'flag_dokumen_ceklis' => 0,
                'flag_rac' => 0,
                'flag_interview_hrd' => 0,
                'flag_kunjungan_domisili' => 0,
                'flag_kunjungan_objek_pendanaan' => 0,
                'flag_informasi_pendanaan_berjalan' => 0,
                'flag_laporan_appraisal' => 0,
                'created_at_spk' => date('Y-m-d H:i:s'),
                'updated_at_spk' => date('Y-m-d H:i:s'),
            ];

            $result =  DB::table('brw_hd_verifikator')->insert($data);
            if ($result) {
                return $nomor_surat;
            }
        } catch (\Exception $e) {
            dd("Insert Verifikator error : " . $e->getMessage());
        }
    }

    public function sendMailSPK($nomor_surat, $pengajuan, $borrower_detail, $email)
    {

        $domisili_alamat = $borrower_detail->domisili_alamat . ' ' .  $borrower_detail->domisili_provinsi . ' ' .   $borrower_detail->domisili_kota . ' ' . $borrower_detail->domisili_kecamatan . ' ' . $borrower_detail->kelurahan . ' ' . $borrower_detail->domisili_kd_pos;
        $pendanaan_nama = DB::table('brw_tipe_pendanaan')->where('tipe_id', $pengajuan->pendanaan_tipe)->value('pendanaan_nama');

        $data = ['pengajuan_id' => $pengajuan->pengajuan_id, 'borrower' => $borrower_detail, 'domisili_alamat' => $domisili_alamat, 'nomor_surat' => $nomor_surat, 'tanggal_surat' => Helper::date_indonesia(date('Y-m-d')), 'pendanaan_nama' => $pendanaan_nama];

        try {

            $email_verifikator = $email;

            if ($email_verifikator) {
                $email_content = new EmailSPKVerifikator($data);
                Mail::to($email)->send($email_content);
            }
        } catch (\Exception $e) {
            dd('Email SPK error :' . $e->getMessage());
        }
    }

    public function kirimVerifikasiData($data)
    {
        try {
            // START: Update form pendanaan berjalan
            $user_login = Auth::guard('admin')->user()->firstname . ' ' . Auth::guard('admin')->user()->lastname;
            $update_status = DB::select(
                "CALL proc_click_button_borrower(
                    17, /* form_id int */
                    '$user_login', /* user_login varchar(35) */
                    '$data->pengajuan_id', /* pengajuan_id int */
                    'kirim', /* method varchar(10) ['simpan' , 'kirim'] */
                    'VerifikatorService.php', /* file varchar(100) */
                    136 /* line int */
                )"
            );

            $result_update_status = $update_status[0]->sout;
            return $result_update_status;
            // END: Update form pendanaan berjalan
            // return DB::table('brw_analisa_pendanaan')->where('pengajuan_id', $data->pengajuan_id)->update(['status_verifikator'=> 3, 'status_analis'=> 4]);
        } catch (\Exception $e) {
            dd('Kirim Verifikasi Data Error : ' . $e->getMessage());
        }
    }

    public function statusVerifikator($pengajuan_id)
    {
        return DB::table('brw_analisa_pendanaan')->where('pengajuan_id', $pengajuan_id)->value('status_verifikator');
    }

    public function checkAllStatusPekerjaan($pengajuan_id)
    {
        $jumlah_selesai_task = DB::table('brw_hd_verifikator')->where('pengajuan_id', $pengajuan_id)
            ->selectRaw('(flag_spk+flag_rac+flag_dokumen_ceklis+flag_interview_hrd+flag_kunjungan_domisili+flag_kunjungan_objek_pendanaan+flag_informasi_pendanaan_berjalan+flag_laporan_appraisal) as total_task')
            ->value('total_task');
        $status_verifikator = $this->statusVerifikator($pengajuan_id);

        $parsingJson = array('data' => ['jumlah_selesai' => $jumlah_selesai_task, 'status_verifikator' => $status_verifikator]);
        echo json_encode($parsingJson);
    }

    public function setujuVerifikatorPengajuan($pengajuan_id)
    {
        $result =  DB::table('brw_pengajuan')->where('pengajuan_id', $pengajuan_id)->update(['setuju_verifikator_pengajuan' => 1]);
        return ($result) ? "1" : "0";
    }

    public function setujuVerifikatorDokumen($pengajuan_id)
    {
        $result =  DB::table('brw_pengajuan')->where('pengajuan_id', $pengajuan_id)->update(['setuju_verifikator_dokumen' => 1]);
        return ($result) ? "1" : "0";
    }

    public function insertSPKOccasioZZZ($pengajuan_id, $borrower_id, $borrower_detail): void
    {

        $domPdfPath = base_path('vendor/dompdf/dompdf');

        \PhpOffice\PhpWord\Settings::setPdfRendererPath($domPdfPath);
        \PhpOffice\PhpWord\Settings::setPdfRendererName('DomPDF');

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/private/template_spk/TemplateSPKOccasio.docx'));
        $date_indo = Helper::date_indonesia(date('Y-m-d'));
        $bulan_tahun = date('m') . '/' . date('Y');
        $nomor_spk = $this->generate_no_spk();

        $domisili_alamat = $borrower_detail->domisili_alamat . ' ' .  $borrower_detail->domisili_provinsi . ' ' .   $borrower_detail->domisili_kota . ' ' . $borrower_detail->domisili_kecamatan . ' ' . $borrower_detail->kelurahan . ' ' . $borrower_detail->domisili_kd_pos;
        $templateProcessor->setValue(
            [
                'brw_id',
                'pengajuan_id',
                'nama',
                'tanggal',
                'nomor_spk',
                'bulan_tahun',
                'domisili_alamat',
                'no_tlp'
            ],
            [
                $borrower_id,
                $pengajuan_id,
                $borrower_detail->nama,
                $date_indo,
                sprintf("%04s", $nomor_spk),
                $bulan_tahun,
                $domisili_alamat,
                $borrower_detail->no_tlp

            ]
        );

        Storage::disk('private')->makeDirectory('spk/' . $pengajuan_id);

        $templateProcessor->saveAs(storage_path('app/private/spk/' . $pengajuan_id . '/TemplateSPKOccasio.docx'));
        $file_doc = base_path('storage/app/private/spk/' . $pengajuan_id . '/TemplateSPKOccasio.docx');
        // shell_exec('unoconv -f pdf '.base_path('storage/app/public/spk/'.$pengajuan_id.'/TemplateSPKOccasio.docx').' '.base_path('storage/app/public/spk/'.$pengajuan_id));

        $phpWord = \PhpOffice\PhpWord\IOFactory::load($file_doc);


        $pdfWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'PDF');
        $filePath = 'spk/' . $pengajuan_id . '/' . $pengajuan_id . '-' . date('dmY') . '-SK-Occasio.pdf';
        $pdfWriter->save(storage_path('app/private/' . $filePath), TRUE);

        //generate spk pdf file, save to storage
        $data = [
            'pengajuan_id' => $pengajuan_id,
            'spk' => $filePath,
            'nomor_spk' => $nomor_spk,
            'flag_spk' => 1,
            'flag_dokumen_ceklis' => 0,
            'flag_rac' => 0,
            'flag_interview_hrd' => 0,
            'flag_kunjungan_domisili' => 0,
            'flag_kunjungan_objek_pendanaan' => 0,
            'flag_informasi_pendanaan_berjalan' => 0,
            'created_at_spk' => date('Y-m-d H:i:s'),
            'updated_at_spk' => date('Y-m-d H:i:s'),
        ];

        $result =  DB::table('brw_hd_occasio')->insert($data);
    }

    public static function createVerifikatorPengajuan($pengajuan_id): void
    {

        BorrowerVerifikatorPengajuan::updateOrCreate(
            ['pengajuan_id' => $pengajuan_id],
            [
                'status_aktifitas' => 1, //status baru
                'created_by' => Auth::guard('admin')->user()->email
            ]
        );
    }

    public static function updateVerifikatorPengajuan($pengajuan_id): void
    {
        $updated = BorrowerVerifikatorPengajuan::where('pengajuan_id', $pengajuan_id)->update(
            [
                'status_aktifitas' => 2, //status proses
                'updated_at' => Carbon::now(),
                'updated_by' => Auth::guard('admin')->user()->email
            ]
        );
    }

    public static function getVerifikatorPengajuan($pengajuan_id)
    {
        $dataVerifikatorPengajuan = BorrowerVerifikatorPengajuan::where('pengajuan_id', $pengajuan_id)->first();
        return response()->json(['data' => $dataVerifikatorPengajuan]);
    }

    public static function kirimPengajuan($request)
    {


        DB::beginTransaction();
        try {

            if ($request->file('file_rekomendasi')) {

                $file_temp = $request->file('file_rekomendasi');
                $extension  = $file_temp->getClientOriginalExtension();
                $size = $file_temp->getSize();
                $maxsize = 3000000; // 3MB

                $destinationPath = storage_path('app/private/borrower/' . $request->brw_id . '/' . $request->pengajuan_id . '/');

                if ($size > $maxsize)  return response()->json(['status' => 'failed', 'message' => 'Maksimum Unggah File Size 3 MB']);
                $safeName =  'file_rekomendasi_' . date('YmdHis') . '.' . $extension;

                if ($file_temp->move($destinationPath, $safeName)) {

                    $fileToSave = 'borrower/' . $request->brw_id . '/' . $request->pengajuan_id . '/' . $safeName;
                    BorrowerVerifikatorPengajuan::where('pengajuan_id', $request->pengajuan_id)->update(
                        [
                            'status_aktifitas' => 3, //status selesai 
                            'updated_at' => Carbon::now(),
                            'tgl_kirim' => Carbon::now(),
                            'catatan_rekomendasi' => $request->catatan_rekomendasi,
                            'updated_by' => Auth::guard('admin')->user()->email,
                            'rekomendasi_file' => $fileToSave
                        ]
                    );
                }
            } else {
                return response()->json(['status' => 'failed', 'message' => 'File Rekomendasi Belum di upload']);
            }

            DB::commit();
            return response()->json(['status' => 'success', 'message' => 'Data berhasil dikirim']);
        } catch (Exception $exception) {
            DB::rollBack();
            return response()->json(['status' => 'failed', 'message' => config('app.env') !== 'production' ? $exception->getMessage() : 'Terjadi kesalahan, silahkan coba lagi']);
        }
    }
}
