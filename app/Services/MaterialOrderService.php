<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Services\AiqqonService;
use Illuminate\Support\Facades\Cache;
use App\BorrowerCartMaterialOrder;

class MaterialOrderService
{

    public static function productListing(Request $request)
    {


        $params = ['offset' => 0, 'limit' => 100, 'category_id' => $request->category_id, 'name' => $request->name];
        $cacheName = "cacheProductList_" . $request->category_id . "_" . $request->name;

        //cache in 60 minutes
        $listProducts = Cache::remember($cacheName, 60, function () use ($params) {
            return AiqqonService::call("v3/list-product", $params);
        });

        $returnHTML = '';
        if (isset($listProducts['data']) && count($listProducts['data']) > 0) {
            foreach ($listProducts['data'] as $key => $val) {
                $returnHTML .= "
                    <div class='row justify-content-center mb-3'>
                        <div class='col-md-12 col-xl-10'>
                            <div class='card shadow-0 border rounded-3'>
                                <div class='card-body'>
                                    <div class='row'>
                                        <div class='col-md-12 col-lg-3 col-xl-3 mb-4 mb-lg-0'>
                                            <div class='bg-image  ripple rounded ripple-surface'>
                                                <img src='" . $val['image'] . "'
                                                    class='w-100' />
                                            </div>
                                        </div>
                                        <div class='col-md-6 col-lg-6 col-xl-6'>
                                            <h6>" . $val['nama'] . "</h6>
                                        
                                            <p class='mb-4 mb-md-0 small'>
                                                " . htmlentities($val['deskripsi']) . "
                                            </p>

                                            <div class='mt-1 mb-0 text-bold'>
                                                Harga : Rp. " . number_format($val['harga'], 0, ",", ".") . "

                                            </div>
                                        </div>
                                        <div class='col-md-6 col-lg-3 col-xl-3 border-sm-start-none border-start'>
                                        
                                            <div class='d-flex flex-column'>
                                                <button class='btn btn-success btn-sm mt-2' type='button' data-price='" . $val['harga'] . "' data-image='" . $val['image'] . "' data-category='" . $val['category_id'] . "' data-desc='" . $val['deskripsi'] . "' data-title='" . $val['nama'] . "' data-id='" . $val['id'] . "' data-toggle='modal' data-target='#modalAddKeranjang'>
                                                    <i class='fa fa-plus-circle'></i> Tambah
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>";
            }

            return $returnHTML;
        } else {
            return 'Produk Tidak Tersedia';
        }
    }

    public static function productCategories()
    {
        //cache in 120 minutes
        $listCategory = Cache::remember("cacheProductCateories", 120, function ()  {
            return AiqqonService::call("v2/list-category-product");
        });

        return $listCategory;
    }

    public static function addToCart(Request $request){

        BorrowerCartMaterialOrder::updateOrCreate(
            ['pengajuan_id' => $request->pengajuan_id, 'material_item_id'=> $request->material_item_id, 'task_id'=> $request->task_id],
            [
                'qty' => 1,
                'price' => $request->price,
                'material_item_name'=> $request->material_item_name,
                'material_item_url'=> $request->material_item_url,
                'material_item_desc'=> $request->material_item_desc,
                'item_category_id'=> $request->item_category_id
            ]
        );

        $countItems = BorrowerCartMaterialOrder::where('pengajuan_id', $request->pengajuan_id)->count();
        return response()->json(['status' => 'success', 'message' => 'Berhasil di simpan', 'countItemCart'=> '('.$countItems.')']);
    }
}
