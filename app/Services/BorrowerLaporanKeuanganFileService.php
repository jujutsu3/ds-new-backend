<?php

namespace App\Services;

class BorrowerLaporanKeuanganFileService extends FileService
{
    /**
     * @param $borrowerId
     * @return string
     */
    public static function getDiskPath($borrowerId)
    {
        return "lap_keuangan/{$borrowerId}";
    }

    protected static function getRouteName()
    {
        return 'v1.borrower.file';
    }
}
