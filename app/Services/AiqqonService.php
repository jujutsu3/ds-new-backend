<?php

namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class AiqqonService
{
    public static function getVendor()
    {
        $getVendorInfo = DB::select("SELECT  param1
                                            ,param2
                                            ,param3
                                            ,api_key
                                            ,merchant_id
                                            ,endpoint_url
                                       FROM vendor_endpoint
                                      WHERE shortname = 'MEDIFFA'");

        $phone = $getVendorInfo[0]->param1;
        $pin = $getVendorInfo[0]->param2;
        $p2p_name = $getVendorInfo[0]->param3;
        $merchant_id = $getVendorInfo[0]->merchant_id;
        $client_key = $getVendorInfo[0]->api_key;
        $endpoint_url = $getVendorInfo[0]->endpoint_url;

        $client = new Client();
        $request = $client->post($endpoint_url . '/v2/login/p2p', [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Client-Key' => $client_key,
            ],
            'body' => '{
                "phone"   : "' . $phone . '",
                "pin"     : "' . $pin . '",
                "p2p_name": "' . $p2p_name . '"
            }',
        ]);

        $response = $request->getBody()->getContents();
        // die($response);
        $response_decode = json_decode($response);

        $token = $response_decode->{'data'}->{'token'};

        #return $token;
        #$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
        $response = [
            'token' => $token,
            'merchant_id' => $merchant_id,
            'client_key' => $client_key,
            'endpoint_url' => $endpoint_url,
        ];

        return json_encode($response);
    }

    /**
     * @param string $path
     * @return array
     */
    public static function call(string $path, array $params = []): array
    {
        $vendor = json_decode(self::getVendor(), false);
        $client = new Client();

        $query = http_build_query(array_merge([
            "merchant_id" => $vendor->merchant_id,
        ], $params));

        $response = $client->get("{$vendor->endpoint_url}/$path?$query", [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Client-Key' => $vendor->client_key,
                'Authorization' => 'Bearer ' . $vendor->token],
        ]);

        return json_decode($response->getBody()->getContents(), true) ?? [];
    }
}
