<?php

namespace App\Services;

use App\AhliWarisInvestor;
use App\DetilInvestor;
use App\Investor;
use App\InvestorContacts;
use App\InvestorCorporation;
use App\InvestorLocation;
use App\InvestorPemegangSaham;
use App\InvestorPengurus;

class InvestorService
{
    public static function getLoggedInInvestorId()
    {
        return request()->user()->id;
    }

    /**
     * @param $investorId
     * @return \App\Investor
     */
    public static function getInvestor($investorId = null)
    {
        if ($investorId === null) {
            $investorId = self::getLoggedInInvestorId();
        }

        /** @var Investor $investor */
        $investor = Investor::query()->where('id', $investorId)->firstOrFail();

        return $investor;
    }

    /**
     * @param $investorId
     * @return \App\InvestorCorporation
     */
    public static function getInvestorCorporation($investorId = null)
    {
        if ($investorId === null) {
            $investorId = self::getLoggedInInvestorId();
        }

        /** @var InvestorCorporation $investor */
        $investor = InvestorCorporation::query()->where('id', $investorId)->firstOrFail();

        return $investor;
    }


    /**
     * @param \App\Investor $investor
     * @return \App\DetilInvestor
     */
    public static function getDetilInvestor(Investor $investor)
    {
        $investorId = $investor->id;
        /** @var DetilInvestor $detilInvestor */
        $detilInvestor = DetilInvestor::query()
            ->where('investor_id', $investorId)
            ->firstOrCreate([
                'investor_id' => $investorId,
            ]);
        
        $investor->load('detilInvestor');

        return $detilInvestor;
    }

    public static function getAdmins(Investor $investor)
    {
        $investorId = $investor->id;
        /** @var DetilInvestor $detilInvestor */
        $detilInvestor = InvestorPengurus::query()
            ->where('investor_id', $investorId)
            ->get();

        return $detilInvestor;
    }

    public static function getContacts(Investor $investor)
    {
        $investorId = $investor->id;
        /** @var DetilInvestor $detilInvestor */
        $detilInvestor = InvestorContacts::query()
            ->where('investor_id', $investorId)
            ->get();

        return $detilInvestor;
    }

    public static function getShareholders(Investor $investor)
    {
        $investorId = $investor->id;
        /** @var DetilInvestor $detilInvestor */
        $detilInvestor = InvestorPemegangSaham::query()
            ->where('investor_id', $investorId)
            ->get();

        return $detilInvestor;
    }

    public static function getDetilInvestorLokasi(Investor $investor)
    {
        $investorId = $investor->id;
        /** @var \App\InvestorLocation $detilInvestorLokasi */
        $detilInvestorLokasi = InvestorLocation::query()
            ->where('investor_id', $investorId)
            ->firstOrCreate([
                'investor_id' => $investorId,
            ]);
        
        $investor->load('location');

        return $detilInvestorLokasi;
    }

    /**
     * @return \App\AhliWarisInvestor
     */
    public static function getAhliWaris()
    {
        $investorId = self::getLoggedInInvestorId();
        /** @var AhliWarisInvestor $ahliWaris */
        $ahliWaris = AhliWarisInvestor::query()
            ->where('id_investor', $investorId)
            ->firstOrCreate([
                'id_investor' => $investorId,
            ]);

        return $ahliWaris;
    }

    /**
     * @param array $data
     * @param array $map
     * @return array
     */
    public static function mapToDatabase(array $data, array $map): array
    {
        return collect($data)
            ->only(array_keys($map))
            ->mapWithKeys(function ($value, $key) use ($map) {
                return [$map[$key] => $value];
            })
            ->toArray();
    }
}
