<?php

namespace App\Services;

class LenderFileService extends FileService
{
    /**
     * @param $investorId
     * @return string
     */
    public static function getDiskPath($investorId)
    {
        return "user/{$investorId}";
    }

    protected static function getRouteName()
    {
        return 'v1.lender.file';
    }
}
