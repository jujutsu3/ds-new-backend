<?php

namespace App\Services;

class BorrowerFileService extends FileService
{
    /**
     * @param $investorId
     * @return string
     */
    public static function getDiskPath($investorId)
    {
        return "borrower/{$investorId}";
    }

    protected static function getRouteName()
    {
        return 'v1.borrower.file';
    }
}
