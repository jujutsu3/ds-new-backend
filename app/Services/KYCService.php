<?php

namespace App\Services;

use App\Constants\JenisKelamin;
use App\Constants\KYCStatus;
use Carbon\Carbon;
use Dotenv\Exception\ValidationException;

class KYCService
{
    /**
     * @param array $columns
     * @param $data
     * @param array $incompleteFields
     * @return string
     */
    public static function baseCheck(array $columns, $data, array &$incompleteFields = []): string
    {
        if (count($columns) === 0) {
            return KYCStatus::COMPLETE_TRUE;
        }

        $emptyCount = 0;
        foreach ($columns as $column) {
            if (is_array($column)) {
                $subColumns = collect($column);
                $subColumns = $subColumns->filter(function ($column) use ($data) {
                    return $data[$column] !== null && $data[$column] !== '';
                });

                if ($subColumns->count() === 0) {
                    $emptyCount++;
                }

                continue;
            }
            $value = $data[$column] ?? null;
            if ($value === null || $value === '') {
                $incompleteFields[] = $column;
                $emptyCount++;
            }
        }

        if ($emptyCount === count($columns)) {
            return KYCStatus::COMPLETE_FALSE;
        }

        if ($emptyCount > 0) {
            return KYCStatus::COMPLETE_PARTIAL;
        }

        return KYCStatus::COMPLETE_TRUE;
    }

    public static function multipleCheck(array $values)
    {
        $items = collect($values)->unique();
        if ($items->isEmpty()) {
            return KYCStatus::COMPLETE_FALSE;
        }

        if ($items->count() === 1 && $items->contains(KYCStatus::COMPLETE_FALSE)) {
            return KYCStatus::COMPLETE_FALSE;
        }

        if ($items->count() === 1 && $items->contains(KYCStatus::COMPLETE_TRUE)) {
            return KYCStatus::COMPLETE_TRUE;
        }

        return KYCStatus::COMPLETE_PARTIAL;
    }

    public static function parseNik($nik)
    {
        $dobArr = [
            'date' => (int) substr($nik, 6, 2),
            'month' => (int) substr($nik, 8, 2),
            'year' => (int) substr($nik, 10, 2),
        ];

        $gender = $dobArr['date'] <= 31
            ? JenisKelamin::LAKILAKI
            : JenisKelamin::PEREMPUAN;

        $dob = sprintf("%d%s-%s-%s",
            ($dobArr['year'] > 40 && $dobArr['year'] <= 99 ? '19' : '20'),
            str_pad($dobArr['year'], 2, '0', STR_PAD_LEFT),
            str_pad($dobArr['month'], 2, '0', STR_PAD_LEFT),
            str_pad($dobArr['date'] > 40 ? $dobArr['date'] - 40 : $dobArr['date'], 2, '0', STR_PAD_LEFT)
        );

        try {
            Carbon::parse($dob);
        } catch (\Exception $exception) {
            throw new ValidationException('NIK tidak valid');
        }

        return [
            'dob' => $dob,
            'gender' => $gender,
        ];
    }
}
