<?php

namespace App;

use Illuminate\Validation\Rule;

class BorrowerPengurus extends BorrowerAdministrator implements BorrowerAdministratorInterface, FieldFileInterface
{
    use FieldDateHandler;
    use FieldFileHandler;
    use FieldPhoneHandler;

    protected $table = 'brw_pengurus';
    protected $primaryKey = 'pengurus_id';
    protected $guarded = ['pengurus_id'];

    public static function getAdministratorDbMap(): array
    {
        $map = parent::getAdministratorDbMap();
        $map[self::NAMA] = 'nm_pengurus';
        $map[self::IDENTITAS] = 'identitas_pengurus';

        return $map;
    }

    public static function getRules(array $neededRules = [], $ignoredBorrowerId = null, $ignoredAdminIds = [])
    {
        $rules = parent::getRules($neededRules, $ignoredBorrowerId);
        $map = static::getAdministratorDbMap();

        return $rules;
    }

    public static function getRequestIdentifier()
    {
        return 'admins';
    }

    public static function getRelationIdentifier()
    {
        return 'admins';
    }
}
