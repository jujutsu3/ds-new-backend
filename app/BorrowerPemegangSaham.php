<?php

namespace App;

use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class BorrowerPemegangSaham extends BorrowerAdministrator implements BorrowerAdministratorInterface, FieldFileInterface
{
    use FieldDateHandler;
    use FieldFileHandler;
    use FieldPhoneHandler;

    public const PMG_SHM_INDIVIDU = 1;
    public const PMG_SHM_BDN_HKM = 2;

    public const JENIS_PEMEGANG_SAHAM = 'jenis_pemegang_saham';
    public const NILAI_SAHAM = 'nilai_saham';
    public const LEMBAR_SAHAM = 'lembar_saham';
    public const JENIS_IDENTITAS_NIB = 3;

    protected $table = 'brw_pemegang_saham';
    protected $primaryKey = 'pemegang_saham_id';
    protected $guarded = ['pemegang_saham_id'];

    public static function getAdministratorDbMap(): array
    {
        $map = parent::getAdministratorDbMap();
        $map[self::NAMA] = 'nm_pemegang_saham';
        $map[self::IDENTITAS] = 'identitas_pemegang_saham';
        $map[self::JENIS_PEMEGANG_SAHAM] = 'jenis_pemegang_saham';
        $map[self::NILAI_SAHAM] = 'nilai_saham';
        $map[self::LEMBAR_SAHAM] = 'lembar_saham';

        return $map;
    }

    public static function getRules(array $neededRules = [], $ignoredBorrowerId = null, $ignoredAdminIds = [])
    {
        $rules = parent::getRules($neededRules, $ignoredBorrowerId);
        $map = static::getAdministratorDbMap();
        $rules[self::JENIS_PEMEGANG_SAHAM] = [Rule::in([self::PMG_SHM_BDN_HKM, self::PMG_SHM_INDIVIDU])];
        $rules[self::NILAI_SAHAM] = ['numeric'];
        $rules[self::LEMBAR_SAHAM] = ['numeric'];

        return $rules;
    }

    public static function getAttributeName()
    {
        $attributes = parent::getAttributeName();

        if (App::isLocale('en')) {
            $attributes[self::JENIS_PEMEGANG_SAHAM] = 'Shareholders Type';
            $attributes[self::NILAI_SAHAM] = 'Stock Value';
            $attributes[self::LEMBAR_SAHAM] = 'Shares';

            return $attributes;
        }

        $attributes[self::JENIS_PEMEGANG_SAHAM] = 'Jenis Pemegang Saham';
        $attributes[self::NILAI_SAHAM] = 'Nilai Saham';
        $attributes[self::LEMBAR_SAHAM] = 'Lembar Saham';

        return $attributes;
    }

    public static function getRequestIdentifier()
    {
        return 'shareholders';
    }

    public static function getRelationIdentifier()
    {
        return 'shareholders';
    }
}
