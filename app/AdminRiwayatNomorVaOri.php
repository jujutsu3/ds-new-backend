<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatNomorVaOri extends Model
{
    protected $primaryKey = 'hist_id';
    protected $table = 'admin_riwayat_nomor_va_ori';
    protected $guarded = [];
    public $timestamps = false;
}
