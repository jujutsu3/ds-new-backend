<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatInvestor extends Model
{
    protected $primaryKey = 'hist_investor_id';
    protected $table = 'admin_riwayat_investor';
    protected $guarded = [];
    public $timestamps = false;
}
