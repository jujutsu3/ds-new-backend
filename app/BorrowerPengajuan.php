<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\Rule;

// use Illuminate\Notifications\Notifiable;
// use Illuminate\Contracts\Auth\CanResetPassword;
// use Illuminate\Foundation\Auth\User as Authenticatable;

class BorrowerPengajuan extends Model
{
    use FieldDateHandler;

    public const TUJUAN = 'tujuan';
    public const KEBUTUHAN_DANA = 'kebutuhan_dana';
    public const ESTIMASI_MULAI = 'estimasi_mulai';
    public const JANGKA_WAKTU = 'jangka_waktu';
    public const TIPE = 'tipe';
    public const STATUS = 'status';

    public const JENIS_BARU = '1';
    public const JENIS_LAMA = '2';

    // use Notifiable;
    public const TABLE = 'brw_pengajuan';
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_pengajuan';
    protected $guard = 'borrower';
    protected $guarded = [];

    public static $jenisNames = [
        self::JENIS_BARU => 'baru',
        self::JENIS_LAMA => 'lama',
    ];

    // protected $fillable = [
    //     'pengajuan_id', 'id_proyek', 'brw_id',
    //     'pendanaan_nama', 'pendanaan_tipe',
    //     'pendanaan_akad', 'pendanaan_dana_dibutuhkan',
    //     'estimasi_mulai', 'estimasi_imbal_hasil', 'mode_pembayaran', 'status_tampil',
    //     'durasi_proyek', 'detail_pendanaan','dana_dicairkan', 'status', 'status_dana', 'va_number', 'keterangan'
    // ];

    /**
     * @return string[]
     */
    public static function getBorrowerDbMap(): array
    {
        return [
            self::TUJUAN => 'pendanaan_tujuan',
            self::TIPE => 'pendanaan_tipe',
            self::KEBUTUHAN_DANA => 'pendanaan_dana_dibutuhkan',
            self::ESTIMASI_MULAI => 'estimasi_mulai',
            self::JANGKA_WAKTU => 'durasi_proyek',
            self::STATUS => 'status',
        ];
    }

    /**
     * @return string[]
     */
    public static function getAttributeName()
    {
        return [
            self::TUJUAN => 'Tujuan Pendanaan',
            self::TIPE => 'Tipe Pendanaan',
            self::KEBUTUHAN_DANA => 'Kebutuhan Dana',
            self::ESTIMASI_MULAI => 'Estimasi Mulai',
            self::JANGKA_WAKTU => 'Jangka Waktu',
            self::STATUS => 'Status',
        ];
    }

    public static function getRules($borrowerId, array $neededRules = [], MasterTujuanPembiayaan $tujuan = null)
    {
        $rules = [
            self::TUJUAN => [Rule::exists('m_tujuan_pembiayaan', 'id')],
            self::TIPE => [Rule::exists('brw_tipe_pendanaan', 'tipe_id')],
            self::KEBUTUHAN_DANA => ['numeric', function ($attribute, $value, $fail) use ($tujuan) {
                if ($value < $tujuan->min_pembiayaan) {
                    $fail('Minimal Kebutuhan Dana adalah ' . number_format($tujuan->min_pembiayaan, 2));
                }
            }],
            self::ESTIMASI_MULAI => ['date_format:d-m-Y'],
            self::JANGKA_WAKTU => ['numeric', function ($attribute, $value, $fail) use ($tujuan) {
                if ($tujuan->max_tenor !== null && (int) $value > $tujuan->max_tenor) {
                    $fail('Jangka waktu tidak valid');
                }
            }],
            self::STATUS => [],
        ];

        if (! empty($neededRules)) {
            return array_intersect_key($rules, array_flip($neededRules));
        }

        return $rules;
    }

    public function getLokasiLengkapAttribute()
    {
        return "$this->lokasi_proyek, $this->kelurahan, $this->kecamatan, $this->provinsi, $this->kode_pos";
    }

    public function proyek()
    {
        return $this->belongsTo(Proyek::class, 'id_proyek');
    }

    public function materialRequisition()
    {
        return $this->hasOne(BorrowerMaterialRequisitionHeader::class, 'pengajuan_id', 'pengajuan_id');
    }
}
