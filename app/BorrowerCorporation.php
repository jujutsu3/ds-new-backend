<?php

namespace App;

use App\Services\BorrowerFileService;
use App\Services\BorrowerService;
use App\Services\FileService;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class BorrowerCorporation extends Borrower implements FieldFileInterface, PublicBorrowerCorporation
{
    use FieldFileHandler;
    use FieldDateHandler;

    public const USERNAME = 'username';
    public const EMAIL = 'email';
    public const PASSWORD = 'password';
    public const NAMA = 'nama';
    public const NO_SURAT_IZIN = 'no_surat_izin';
    public const NPWP = 'npwp';
    public const NO_AKTA_PENDIRIAN = 'no_akta_pendirian';
    public const TANGGAL_AKTA_PENDIRIAN = 'tanggal_akta_pendirian';
    public const NO_AKTA_PERUBAHAN = 'no_akta_perubahan';
    public const TANGGAL_AKTA_PERUBAHAN = 'tanggal_akta_perubahan';
    public const TELEPON = 'telepon';
    public const ALAMAT = 'alamat';
    public const PROVINSI = 'provinsi';
    public const KABUPATEN = 'kabupaten';
    public const KECAMATAN = 'kecamatan';
    public const KELURAHAN = 'kelurahan';
    public const KODE_POS = 'kode_pos';
    public const REKENING = 'rekening';
    public const KODE_BANK = 'bank';
    public const NAMA_PEMILIK_REKENING = 'nama_pemilik';
    public const BIDANG_USAHA = 'bidang_pekerjaan';
    public const OMSET_TAHUN_TERAKHIR = 'omset_tahun_terakhir';
    public const TOTAL_ASET_TAHUN_TERAKHIR = 'total_aset_tahun_terakhir';
    public const FOTO_NPWP = 'foto_npwp';
    public const LAPORAN_KEUANGAN = 'laporan_keuangan';

    public const NAMA_NOTARIS_AKTA_PENDIRIAN = 'nama_notaris_akta_pendirian';
    public const KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN = 'kedudukan_notaris_akta_pendirian';
    public const NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN = 'no_sk_kemenkumham_akta_pendirian';
    public const TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN = 'tanggal_sk_kemenkumham_akta_pendirian';
    public const NAMA_NOTARIS_AKTA_PERUBAHAN = 'nama_notaris_akta_perubahan';
    public const KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN = 'kedudukan_notaris_akta_perubahan';
    public const NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN = 'no_sk_kemenkumham_akta_perubahan';
    public const TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN = 'tanggal_sk_kemenkumham_akta_perubahan';

    public function getData($neededData)
    {
        $borrowerDetailMap = self::getBorrowerDetailDbMap();
        $borrowerRekeningMap = self::getBorrowerRekeningDbMap();

        $getter = static function ($model, $map, $columns) {
            $data = [];
            if ($model === null) {
                return $data;
            }

            foreach ($columns as $column) {
                $datum = $map[$column];
                $data[$column] = $model->$datum;
            }

            return $data;
        };

        $responseData = collect();
        /**
         * @todo May need to refactor biar gk linear
         */
        if ($neededBorrowerDetail = array_intersect(array_keys($borrowerDetailMap), $neededData)) {
            $detail = BorrowerService::getDetilBorrower($this);
            $responseData = $responseData->merge(
                $getter($detail, $borrowerDetailMap, $neededBorrowerDetail)
            );
        }

        if ($neededBorrowerDetail = array_intersect(array_keys($borrowerRekeningMap), $neededData)) {
            $rekening = BorrowerService::getBorrowerRekening($this);
            $responseData = $responseData->merge(
                $getter($rekening, $borrowerRekeningMap, $neededBorrowerDetail)
            );
        }

        return $responseData;
    }

    /**
     * For brw_user
     *
     * @return string[]
     */
    public static function getBorrowerDbMap(): array
    {
        return [
            self::USERNAME => 'username',
            self::EMAIL => 'email',
            self::PASSWORD => 'password',
        ];
    }

    /**
     * For brw_user_detail
     *
     * @return string[]
     */
    public static function getBorrowerDetailDbMap(): array
    {
        return [
            self::NAMA => 'nm_bdn_hukum',
            self::NO_SURAT_IZIN => 'nib',
            self::NPWP => 'npwp_perusahaan',
            self::NO_AKTA_PENDIRIAN => 'no_akta_pendirian',
            self::TANGGAL_AKTA_PENDIRIAN => 'tgl_berdiri',
            self::NO_AKTA_PERUBAHAN => 'no_akta_perubahan',
            self::TANGGAL_AKTA_PERUBAHAN => 'tgl_akta_perubahan',
            self::TELEPON => 'telpon_perusahaan',
            self::ALAMAT => 'alamat',
            self::PROVINSI => 'provinsi',
            self::KABUPATEN => 'kota',
            self::KECAMATAN => 'kecamatan',
            self::KELURAHAN => 'kelurahan',
            self::KODE_POS => 'kode_pos',

            self::BIDANG_USAHA => 'bidang_usaha',
            self::OMSET_TAHUN_TERAKHIR => 'omset_tahun_terakhir',
            self::TOTAL_ASET_TAHUN_TERAKHIR => 'tot_aset_tahun_terakhr',

            self::FOTO_NPWP => 'foto_npwp_perusahaan',
            self::LAPORAN_KEUANGAN => 'pic_lapkeuangan',

            self::NAMA_NOTARIS_AKTA_PENDIRIAN => 'nama_notaris_akta_pendirian',
            self::KEDUDUKAN_NOTARIS_AKTA_PENDIRIAN => 'kedudukan_notaris_pendirian',
            self::NO_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'no_sk_kemenkumham_pendirian',
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PENDIRIAN => 'tgl_sk_kemenkumham_pendirian',
            self::NAMA_NOTARIS_AKTA_PERUBAHAN => 'nama_notaris_akta_perubahan',
            self::KEDUDUKAN_NOTARIS_AKTA_PERUBAHAN => 'kedudukan_notaris_perubahan',
            self::NO_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'no_sk_kemenkumham_perubahan',
            self::TANGGAL_SK_KEMENKUMHAM_AKTA_PERUBAHAN => 'tgl_sk_kemenkumham_perubahan',
        ];
    }

    /**
     * For brw_rekening
     *
     * @return string[]
     */
    public static function getBorrowerRekeningDbMap(): array
    {
        return [
            self::REKENING => 'brw_norek',
            self::KODE_BANK => 'brw_kd_bank',
            self::NAMA_PEMILIK_REKENING => 'brw_nm_pemilik',
        ];
    }

    /**
     * @return string[]
     */
    public static function getAttributeName()
    {
        if (App::isLocale('en')) {
            return [
                self::USERNAME => 'Username',
                self::EMAIL => 'Email',
                self::PASSWORD => 'Password',
                self::NAMA => 'Name',
                self::NO_SURAT_IZIN => 'License No',
                self::NPWP => 'NPWP',
                self::NO_AKTA_PENDIRIAN => 'Deed of Establishment No',
                self::TANGGAL_AKTA_PENDIRIAN => 'Deed of Establishment Date',
                self::TELEPON => 'Phone',
                self::ALAMAT => 'Address',
                self::PROVINSI => 'Provinsi',
                self::KABUPATEN => 'Regency',
                self::KECAMATAN => 'District',
                self::KELURAHAN => 'Sub District',
                self::KODE_POS => 'Postal Code',
                self::REKENING => 'Bank Account',
                self::KODE_BANK => 'Bank',
                self::NAMA_PEMILIK_REKENING => 'Bank Account Owner Name',
                self::BIDANG_USAHA => 'Field of Business',
                self::OMSET_TAHUN_TERAKHIR => 'Last Year Turnover',
                self::TOTAL_ASET_TAHUN_TERAKHIR => 'Total Assets Last Year',
                self::FOTO_NPWP => 'NPWP Photo',
            ];
        }

        return [
            self::USERNAME => 'Username',
            self::EMAIL => 'Email',
            self::PASSWORD => 'Password',
            self::NAMA => 'Nama',
            self::NO_SURAT_IZIN => 'No Surat Izin',
            self::NPWP => 'NPWP',
            self::NO_AKTA_PENDIRIAN => 'No Akta Pendirian',
            self::TANGGAL_AKTA_PENDIRIAN => 'Tanggal Akta Pendirian',
            self::TELEPON => 'Telepon',
            self::ALAMAT => 'Alamat',
            self::PROVINSI => 'Provinsi',
            self::KABUPATEN => 'Kabupaten',
            self::KECAMATAN => 'Kecamatan',
            self::KELURAHAN => 'Kelurahan',
            self::KODE_POS => 'Kode Pos',
            self::REKENING => 'Rekening',
            self::KODE_BANK => 'Bank',
            self::NAMA_PEMILIK_REKENING => 'Nama Pemilik',
            self::BIDANG_USAHA => 'Bidang Usaha',
            self::OMSET_TAHUN_TERAKHIR => 'Omset Tahun Terakhir',
            self::TOTAL_ASET_TAHUN_TERAKHIR => 'Total Aset Tahun Terakhir',
            self::FOTO_NPWP => 'Foto NPWP',
        ];
    }

    public static function getRules($borrowerId, array $neededRules = [])
    {
        $rules = [
            self::EMAIL => ['email', Rule::unique('brw_user', 'email')->ignore($borrowerId, 'brw_id'), 'max:50'],
            self::USERNAME => [
                'alpha_num',
                Rule::unique('brw_user', 'username')->ignore($borrowerId, 'brw_id'),
                'max:50',
            ],
            self::PASSWORD => ['max:191'],
            self::NAMA => ['max:191'],
            self::NO_SURAT_IZIN => ['max:50'],
            self::NPWP => [
                'digits_between:15,16',
                Rule::unique('brw_user_detail', 'npwp_perusahaan')->ignore($borrowerId, 'brw_id'),
            ],
            self::NO_AKTA_PENDIRIAN => ['numeric', 'digits_between:0,50'],
            self::TANGGAL_AKTA_PENDIRIAN => ['date_format:d-m-Y'],
            self::NO_AKTA_PERUBAHAN => ['numeric', 'digits_between:0,25'],
            self::TANGGAL_AKTA_PERUBAHAN => ['date_format:d-m-Y'],
            self::TELEPON => ['numeric', 'digits_between:9,15'],
            self::ALAMAT => ['max:191'],
            self::PROVINSI => [Rule::exists('m_kode_pos', 'Provinsi')],
            self::KABUPATEN => [
                function ($attribute, $value, $fail) {
                    if (! MasterKodePos::query()->where([
                        ['Jenis', explode(' ', trim($value))[0] ?? null],
                        [
                            'Kota',
                            substr(strstr($value, " "), 1),
                        ],
                    ])->exists()) {
                        $fail("Kabupaten tidak valid");
                    }
                },
            ],
            self::KECAMATAN => [],
            self::KODE_POS => ['nullable', 'numeric', 'digits:5', Rule::exists('m_kode_pos', 'kode_pos')],
            self::REKENING => ['numeric', 'digits_between:0,191'],
            self::KODE_BANK => [Rule::exists('m_bank', 'kode_bank')],
            self::NAMA_PEMILIK_REKENING => ['max:191'],
            self::BIDANG_USAHA => [Rule::exists('m_bidang_pekerjaan', 'kode_bidang_pekerjaan')],
            self::OMSET_TAHUN_TERAKHIR => ['numeric'],
            self::TOTAL_ASET_TAHUN_TERAKHIR => ['numeric'],
            self::FOTO_NPWP => ['mimes:jpeg,jpg,bmp,png'],
            self::LAPORAN_KEUANGAN => ['mimes:pdf,xlsx,xls,doc,docx'],
        ];

        if (! empty($neededRules)) {
            return array_intersect_key($rules, array_flip($neededRules));
        }

        return $rules;
    }

    public static function getFileServiceClass(): FileService
    {
        return new BorrowerFileService();
    }

    /**
     * @param string $column
     * @param \Illuminate\Http\UploadedFile $file
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param null $fileName
     * @return bool
     */
    public static function upload(string $column, UploadedFile $file, Model $model, $fileName = null)
    {
        $fileName = $fileName ?: $column;

        try {
            $model->forceFill([
                $column =>
                    BorrowerFileService::uploadFile($file, $fileName, $model->brw_id),
            ])
                ->save();
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }

    /**
     * @param string $column
     * @param \Illuminate\Http\UploadedFile $file
     * @param \App\BorrowerDetails $model
     * @param null $fileName
     * @return bool
     */
    public static function uploadLapKeuangan(
        string $column,
        UploadedFile $file,
        BorrowerDetails $model,
        $fileName = null
    ) {
        $fileName = $fileName ?: $column;

        try {
            $model->forceFill([
                $column =>
                    BorrowerFileService::uploadFile($file, $fileName, $model->brw_id),
            ])
                ->save();
        } catch (Exception $exception) {
            return false;
        }

        return true;
    }
}
