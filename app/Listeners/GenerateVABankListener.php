<?php

namespace App\Listeners;

use App\Events\GenerateVABankEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MutasiInvestor;
use App\RekeningInvestor;
use App\Investor;

use App\Jobs\GenerateVABank_CIMBS;
use App\Jobs\GenerateVABank_BSI;

// use App\Jobs\SyncContactCenterLender_Create_Single;

class GenerateVABankListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MutasiInvestorEvent  $event
     * @return void
     */
    public function handle(GenerateVABankEvent $event)
    {

        if (strlen(config('app.cimbs_id_comp_code')) != 0) {
            GenerateVABank_CIMBS::dispatch($event->getVA(), $event->getuserid(), $event->getid())->onQueue('queue_VA_Bank_CIMBS');
        }

        if (strlen(config('app.bsi_url')) != 0) {
            GenerateVABank_BSI::dispatch($event->getVA(), $event->getuserid(), $event->getid())->onQueue('queue_VA_Bank_BSI');
        }

        // SyncContactCenterLender_Create_Single::dispatch($event->getid())->onQueue('queue_CC');
    }
}
