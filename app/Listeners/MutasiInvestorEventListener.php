<?php

namespace App\Listeners;

use App\Events\MutasiInvestorEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MutasiInvestor;
use App\RekeningInvestor;
use App\Investor;
use Mail;
//use App\Jobs\DepositEmail;
use App\Mail\DepositEmail;
use App\Mail\PayoutEmail;


class MutasiInvestorEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MutasiInvestorEvent  $event
     * @return void
     */
    public function handle(MutasiInvestorEvent $event)
    {

        //Create new Mutasi on investor account
        $mutasi = new MutasiInvestor();
        $mutasi->investor_id = $event->user_id;
        $mutasi->nominal = $event->nominal;
        $mutasi->perihal = $event->perihal;
        $mutasi->tipe = $event->type;

        $mutasi->kode_bank = !empty($event->kode_bank) ? $event->kode_bank : '';
        $mutasi->type_account = !empty($event->type_account) ? $event->type_account : '';
        $mutasi->no_account = !empty($event->no_account) ? $event->no_account : '';
        $mutasi->no_ref_bank = !empty($event->no_ref_bank) ? $event->no_ref_bank : '';
        $mutasi->save();

        $user = Investor::find($event->user_id);



        if ($event->perihal == 'Transfer Rekening') {

            try {
                $email = new DepositEmail($event->nominal, $user);
                Mail::to($user->email)->send($email);
            } catch (\Swift_RfcComplianceException $e) {
                // echo $e->getMessage();
            } catch (\Swift_TransportException $e) {
                // echo $e->getMessage();
            }
        }
        //if ($event->perihal == 'Monthly payout') {
        //dispatch(new DepositEmail($event->nominal, 'PAYOUT_MONTHLY', $user));
        //}
    }
}
