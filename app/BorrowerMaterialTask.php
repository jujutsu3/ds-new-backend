<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerMaterialTask extends Model
{
    protected $primaryKey = 'task_id';
    protected $table = 'brw_material_task';
    protected $guarded = [];
}
