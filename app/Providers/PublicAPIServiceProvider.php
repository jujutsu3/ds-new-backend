<?php

namespace App\Providers;

use App\BorrowerCorporation;
use App\BorrowerIndividu;
use App\Http\Controllers\Pub\PublicResponse;
use App\PublicBorrowerCorporation;
use App\PublicBorrowerIndividu;
use App\PublicInvestorCorporation;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class PublicAPIServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(PublicBorrowerCorporation::class, function ($app) {
            $request = $app['request'];
            $response = null;
            if (! $request->has('Borrower_ID')) {
                return BorrowerCorporation::query()->make();
            }

            $borrower = BorrowerCorporation::query()->where('brw_id', $request->get('Borrower_ID'))->first();

            if ($borrower === null) {
                $response = PublicResponse::notFound();
            } elseif ($borrower->ref_number !== Auth::id()) {
                $response = PublicResponse::forbidden();
            }

            if ($response !== null) {
                abort(
                    new Response(json_encode($response->getData()), 200, [
                        'Content-Type' => 'application/json',
                    ])
                );
            }

            return $borrower;
        });

        $this->app->singleton(PublicBorrowerIndividu::class, function ($app) {
            $request = $app['request'];
            $response = null;
            if (! $request->has('Borrower_ID')) {
                return BorrowerIndividu::query()->make();
            }

            $borrower = BorrowerIndividu::query()->where('brw_id', $request->get('Borrower_ID'))->first();

            if ($borrower === null) {
                $response = PublicResponse::notFound();
            } elseif ($borrower->ref_number !== Auth::id()) {
                $response = PublicResponse::forbidden();
            }

            if ($response !== null) {
                abort(
                    new Response(json_encode($response->getData()), 200, [
                        'Content-Type' => 'application/json',
                    ])
                );
            }

            return $borrower;
        });

        $this->app->singleton(PublicInvestorCorporation::class, function ($app) {
            $request = $app['request'];
            $response = null;
            if (! $request->has('Borrower_ID')) {
                return BorrowerCorporation::query()->make();
            }

            $borrower = BorrowerCorporation::query()->where('brw_id', $request->get('Borrower_ID'))->first();

            if ($borrower === null) {
                $response = PublicResponse::notFound();
            } elseif ($borrower->ref_number !== Auth::id()) {
                $response = PublicResponse::forbidden();
            }

            if ($response !== null) {
                abort(
                    new Response(json_encode($response->getData()), 200, [
                        'Content-Type' => 'application/json',
                    ])
                );
            }

            return $borrower;
        });
    }
}
