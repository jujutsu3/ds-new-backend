<?php

namespace App\Providers;

use App\Investor;
use App\Marketer;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Lcobucci\JWT\Parser;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::extend('wso2-borrower', function ($app, $name, array $config) {
            return new class (
                Auth::createUserProvider($config['provider']),
                $app['request']
            ) implements Guard {
                public function __construct(UserProvider $provider, Request $request)
                {
                    $this->provider = $provider;
                    $this->request = $request;
                }

                public function getRef()
                {
                    $token = $this->request->header('X-JWT-ASSERTION');

                    $claims = (new Parser())
                        ->parse($token);

                    if (
                        ($ref = $claims->getClaim('sub')) === null
                        || ($marketer = Marketer::query()->where('ref_code', $ref)->first()) === null
                    ) {
                        return false;
                    }

                    return $marketer;
                }

                public function check()
                {
                    return $this->getRef() !== false;
                }

                public function guest()
                {
                    // TODO: Implement guest() method.
                }

                public function user()
                {
                    // TODO: Implement user() method.
                }

                public function id()
                {
                    return $this->getRef()->ref_code;
                }

                public function validate(array $credentials = [])
                {
                    // TODO: Implement validate() method.
                }

                public function setUser(Authenticatable $user)
                {
                    // TODO: Implement setUser() method.
                }
            };
        });
        
        Auth::extend('wso2-lender', function ($app, $name, array $config) {
            return new class (
                Auth::createUserProvider($config['provider']),
                $app['request']
            ) implements Guard {
                public function __construct(UserProvider $provider, Request $request)
                {
                    $this->provider = $provider;
                    $this->request = $request;
                }

                public function getInvestor()
                {
                    $token = $this->request->header('X-JWT-ASSERTION');

                    $claims = (new Parser())
                        ->parse($token);

                    if (
                        ($ref = $claims->getClaim('sub')) === null
                        || ($investor = Investor::query()->where('username', $ref)->first()) === null
                    ) {
                        return false;
                    }

                    return $investor;
                }

                public function check()
                {
                    return $this->getInvestor() !== false;
                }

                public function guest()
                {
                    // TODO: Implement guest() method.
                }

                public function user()
                {
                    return $this->getInvestor();
                }

                public function id()
                {
                    return $this->getInvestor()->username;
                }

                public function validate(array $credentials = [])
                {
                    // TODO: Implement validate() method.
                }

                public function setUser(Authenticatable $user)
                {
                    // TODO: Implement setUser() method.
                }
            };
        });
    }
}
