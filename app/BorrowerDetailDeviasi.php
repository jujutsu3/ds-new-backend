<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailDeviasi extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'brw_dtl_deviasi';
    protected $guarded = [];
    // public $timestamps = false;
}
