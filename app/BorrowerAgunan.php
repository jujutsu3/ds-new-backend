<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerAgunan extends Model
{
    public const JENIS_SHM = 1;
    public const JENIS_HGB = 2;

    public const STATUS_IMB_ADA = 1;
    public const STATUS_IMB_TIDAK = 2;

    protected $primaryKey = 'id_pengajuan';
    protected $table = 'brw_agunan';
    protected $guarded = [];

    public static $jenisNames =[
        self::JENIS_SHM => 'Sertifikat Hak Milik',
        self::JENIS_HGB => 'Hak Guna Bangunan'
    ];

    public static $statusIMBNames =[
        self::STATUS_IMB_ADA => 'SUDAH ADA',
        self::STATUS_IMB_TIDAK => 'BELUM ADA'
    ];
}
