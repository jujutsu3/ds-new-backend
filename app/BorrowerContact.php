<?php

namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class BorrowerContact extends BorrowerAdministrator implements BorrowerAdministratorInterface, FieldFileInterface
{
    use FieldFileHandler;
    use FieldDateHandler;
    use FieldPhoneHandler;

    public const TABLE = 'brw_corp_contact';
    protected $table = 'brw_corp_contact';
    protected $primaryKey = 'contact_id';
    protected $guarded = ['contact_id'];

    public static function getAdministratorDbMap(): array
    {
        $map = parent::getAdministratorDbMap();
        $map[self::NAMA] = 'nm_contact';
        $map[self::IDENTITAS] = 'identitas_contact';

        return $map;
    }

    public static function getRules(array $neededRules = [], $ignoredBorrowerId = null, $ignoredAdminIds = [])
    {
        $rules = parent::getRules($neededRules, $ignoredBorrowerId);
        $map = static::getAdministratorDbMap();

        return $rules;
    }

    public static function getRequestIdentifier()
    {
        return 'contacts';
    }

    public static function getRelationIdentifier()
    {
        return 'contacts';
    }

    public static function additionalValidator(
        $adminRequest,
        BorrowerAdministratorInterface $administrator,
        $requestKey,
        int $key
    ): void {
        parent::additionalValidator($adminRequest, $administrator, $requestKey, $key);

        $map = static::getAdministratorDbMap();
        Validator::make([
            static::NO_TELEPON => ($adminRequest[static::KODE_OPERATOR] ?? null) . ($adminRequest[static::NO_TELEPON] ?? null),
        ], [
            static::NO_TELEPON => [
                Rule::unique(self::TABLE, $map[static::NO_TELEPON]),
                Rule::unique('detil_investor', 'phone_investor'),
            ],
        ], [], static::getAttributeName())
            ->validate();
    }
}
