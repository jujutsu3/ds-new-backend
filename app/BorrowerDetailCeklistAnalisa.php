<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailCeklistAnalisa extends Model
{
    protected $primaryKey = 'id_pengajuan';
    protected $table = 'brw_dtl_ceklist_analisa';
    protected $guarded = [];
    public $timestamps = false;

    public function getData()
    {
        return static::orderBy('id_pengajuan','desc');
    }

}
