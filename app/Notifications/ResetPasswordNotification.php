<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Http\Request;

class ResetPasswordNotification extends Notification
{
    use Queueable;
    protected $token;
    protected $user_type;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $user_type = 'users')
    {
        $this->token = $token;
        $this->user_type = $user_type;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        // ->line('Klik link dibawah untuk menuju halaman atur ulang kata sandi')
        // ->action('Atur Ulang Kata Sandi', url('password/reset', $this->token))
        // ->line('Abaikan jika anda tidak meminta');
        return (new MailMessage)
            // ->from('noreply@danasyariah.id')
            ->from(config("app.email_no_reply"))
            ->line('Klik Tombol Link Dibawah untuk Menuju Halaman Atur Ulang Kata Sandi')
            ->action('Atur Ulang Kata Sandi', route('users.showResetForm', ['token' =>  $this->token, 'email' => $notifiable->getEmailForPasswordReset(), 'user_type' => $this->user_type]) )
            ->line('Abaikan Email Ini, Apabila Anda Tidak Merasa Mengubahnya');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
