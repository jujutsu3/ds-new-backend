<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDokumenLegalitasPribadi extends Model
{
    protected $primaryKey = 'brw_id';
    protected $table = 'brw_dokumen_legalitas_pribadi';
    protected $guarded = [];
}
