<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminRiwayatAhliWarisInvestorOri extends Model
{
    protected $primaryKey = 'hist_investor_id';
    protected $table = 'admin_riwayat_ahli_waris_investor_ori';
    protected $guarded = [];
    public $timestamps = false;
}
