<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerMaterialRequisitionHeader extends Model
{
    public const STATUS_CREATED = 0;
    public const CREATED_AT = 'creation_date';
    public const UPDATED_AT = 'last_update_date';
    protected $primaryKey = 'requisition_hdr_id';
    protected $table = 'brw_material_requisition_hdr';

    protected $fillable = ['requisition_number', 'requisition_date', 'description', 'brw_id', 'pengajuan_id', 'requisition_status', 'approved_flag',
      'approved_date', 'approved_by', 'need_by_date', 'reject_reason', 'cancel_reason', 'delivery_to_address','shipping_fee', 'created_by', 'last_updated_by'];
     

    public function items()
    {
        return $this->hasMany(BorrowerMaterialRequisitionDetail::class, 'requisition_hdr_id', 'requisition_hdr_id');
    }
}
