<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDetailBiaya extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'brw_dtl_biaya';
    protected $guarded = [];
}
