<?php

namespace App\Console\Commands;

use Illuminate\Http\Request;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\DetilImbalUser;
use App\ListImbalUser;
use App\Log_Imbal_User;
use Mail;
use App\Mail\AutoDebetReg;


class cutOffImbalHasil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'console:cutOffImbalHasil';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'proses cutOffimbalhasil';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dateCutOff = date("Y-m-d", strtotime("+1 day"));
        DB::select("CALL proc_kirimimbal_cutoff('$dateCutOff')");
        // cek untuk kirim email
        $reg = DB::SELECT("SELECT id_user_cek_reg,investor_id,DATE_FORMAT(tgl_register,'%d %M %Y') AS tglReg,DATE_FORMAT(NOW(),'%d %M %Y') AS tglautodebet FROM cek_user_reg_sign WHERE link_aktifasi = 1000");
        if ($reg != NULL) {
            foreach ($reg as $data) {
                $invReg = $data->investor_id;
                $emailReg = DB::SELECT("SELECT a.email,b.nama_investor FROM investor a, detil_investor b WHERE a.id = b.investor_id AND a.id = '$invReg'")[0];
                $dataReg = array(
                    'nama_investor' => $emailReg->nama_investor,
                    'tglReg' => $data->tglReg,
                    'nominal' => "Rp30.000,00.",
                    'tgl_autoDebet' => $data->tglautodebet,
                    'flag' => 0
                );
                // dd($dataReg);
                try {
                    $email = new autoDebetReg($dataReg);
                    Mail::to($emailReg->email)->send($email);
                } catch (\Swift_RfcComplianceException $e) {
                    // echo $e->getMessage();
                } catch (\Swift_TransportException $e) {
                    // echo $e->getMessage();
                }

                DB::table('cek_user_reg_sign')
                    ->where('id_user_cek_reg', $data->id_user_cek_reg)
                    ->update([
                        'link_aktifasi' => 1
                    ]);
            }
        }
        $ttd = DB::SELECT("SELECT id_log_privy,investor_id,nominal,DATE_FORMAT(created_at,'%d %M %Y') AS tglTtd,DATE_FORMAT(NOW(),'%d %M %Y') AS tglautodebet FROM log_privy_investor WHERE status = 1000");
        if ($ttd != NULL) {
            foreach ($ttd as $data) {
                $invTtd = $data->investor_id;
                $emailTtd = DB::SELECT("SELECT a.email,b.nama_investor FROM investor a, detil_investor b WHERE a.id = b.investor_id AND a.id = '$invTtd'")[0];
                $dataReg = array(
                    'nama_investor' => $emailTtd->nama_investor,
                    'tglReg' => $data->tglTtd,
                    'nominal' => "Rp." . $data->nominal,
                    'tgl_autoDebet' => $data->tglautodebet,
                    'flag' => 1
                );
                // dd($dataReg);
                try {
                    $email = new autoDebetReg($dataReg);
                    Mail::to($emailTtd->email)->send($email);
                } catch (\Swift_RfcComplianceException $e) {
                    // echo $e->getMessage();
                } catch (\Swift_TransportException $e) {
                    // echo $e->getMessage();
                }

                DB::table('log_privy_investor')
                    ->where('id_log_privy', $data->id_log_privy)
                    ->update([
                        'status' => 1
                    ]);
            }
        }
    }
}
