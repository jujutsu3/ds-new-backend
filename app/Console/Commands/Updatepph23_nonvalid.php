<?php

namespace App\Console\Commands;

use Illuminate\Http\Request;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\DetilImbalUser;
use App\ListImbalUser;
use App\Log_Imbal_User;
use Mail;
use App\Mail\AutoDebetReg;


class Updatepph23_nonvalid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'console:Updatepph23_nonvalid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'proses Updatepph23_nonvalid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        DB::select("CALL migration_all_lender_pph23_07052022(12,'valid npwp generate to nominal pajak',12,0)");    
    }
}
