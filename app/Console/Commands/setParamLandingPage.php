<?php

namespace App\Console\Commands;
use Illuminate\Http\Request;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Tindikator;


class setParamLandingPage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'console:setParamLandingPage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'proses setParamLandingPage';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::select("CALL proc_setParamLandingPage()");
        echo "dengan nama Allah";
    }
}
