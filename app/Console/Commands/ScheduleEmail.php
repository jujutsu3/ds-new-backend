<?php

namespace App\Console\Commands;
use Illuminate\Http\Request;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Proyek;
use App\Admins;
use Mail;
use App\Mail\PenggalanganSelesaiEmail;
use App\Mail\BrwProyekSelesaiEmail;
use App\Mail\TagihanJatuhTempoEmail;



class ScheduleEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'console:ScheduleEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'proses ScheduleEmail jatuhTempo-proyekselesai-penggalangandanaselesai';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $dateNow = date("Y-m-d");
       $email_analis = Admins::select('email')->where('role', '=', 8)->get();
       $jatuhTempo = DB::select("SELECT a.*,b.nama,c.pendanaan_nama,d.email FROM brw_invoice a JOIN brw_user_detail b ON a.brw_id = b.brw_id JOIN brw_pendanaan c ON a.pendanaan_id = c.pendanaan_id JOIN brw_user d ON a.brw_id = d.brw_id where date_format(a.tgl_jatuh_tempo,'%Y%m%d') = date_format(now(),'%Y%m%d') AND a.status IN (3,4)");
       if($jatuhTempo != null){
                foreach($jatuhTempo as $jt){
                  $invoiceId = $jt->invoice_id;
                  $no_invoice = $jt->no_invoice;
                  $brwId = $jt->brw_id;
                  $tgl_jatuh_tempo = date("d F Y", strtotime($jt->tgl_jatuh_tempo));
                  $nama_pengguna = $jt->nama;
                  $nama_pendanaan = $jt->pendanaan_nama;
                  $gettotal_tagihan = $jt->nominal_tagihan_perbulan - $jt->nominal_transfer_tagihan;
                  $total_tagihan = "Rp. ".number_format($gettotal_tagihan);
                  $email_to = $jt->email;


                  $data_email = array(
                    'nama_pengguna' => $nama_pengguna,
                    'tanggal' => $tgl_jatuh_tempo,
                    'no_invoice' => $no_invoice,
                    'nama_pendanaan' => $nama_pendanaan,
                    'tgl_jatuh_tempo' => $tgl_jatuh_tempo,
                    'total_tagihan' => $total_tagihan
                  );

                    // $array_email_to = [];
                    // array_push($array_email_to, $email_to);
                    // $temp_email = 'anindita.harimurti@danasyariah.id';
                    // array_push($array_email_to, $temp_email);

                    $email = new TagihanJatuhTempoEmail($data_email);
                    Mail::to($email_to)->send($email);
                }
       }
       $proyekSelesai = DB::select("Select * from proyek a JOIN brw_pendanaan b ON a.id = b.id_proyek where a.tgl_selesai = '".$dateNow."' AND a.status = 4");
        if($proyekSelesai != null){
            foreach($proyekSelesai as $ps){
                $id_proyek = $ps->id;
                $data = DB::select("SELECT brw_user.email,brw_user_detail.nama,brw_pendanaan.pendanaan_nama,brw_pendanaan.pendanaan_dana_dibutuhkan,brw_pencairan.nominal_pencairan FROM brw_pendanaan JOIN brw_user ON brw_pendanaan.brw_id = brw_user.brw_id JOIN brw_user_detail ON brw_pendanaan.brw_id = brw_user_detail.brw_id JOIN brw_pencairan ON brw_pendanaan.pendanaan_id = brw_pencairan.pendanaan_id WHERE id_proyek = '".$id_proyek."'");
                $dataProyek = DB::select("SELECT tgl_mulai,tgl_selesai FROM proyek where id = '".$id_proyek."'");

                $email_to = $data[0]->{'email'};
                $nama_penerima_pendanaan = $data[0]->{'nama'};
                $nama_pendanaan = $data[0]->{'pendanaan_nama'};
                $total_pendanaan = "Rp. ".number_format($data[0]->{'nominal_pencairan'});
                $tanggal_mulai = date("d F Y", strtotime($dataProyek[0]->{'tgl_mulai'}));
                $tanggal_selesai = date("d F Y", strtotime($dataProyek[0]->{'tgl_selesai'}));

                $data_email = array(
                    'nama_penerima_pendanaan' => $nama_penerima_pendanaan,
                    'nama_pendanaan' => $nama_pendanaan,
                    'total_pendanaan' => $total_pendanaan,
                    'tanggal_mulai' => $tanggal_mulai,
                    'tanggal_selesai' => $tanggal_selesai
                );

                    $array_email_to = [];
                    array_push($array_email_to, $email_to);
                    foreach($email_analis as $data){
                        array_push($array_email_to, $data->email);
                    }

                $email = new BrwProyekSelesaiEmail($data_email);
                Mail::to($array_email_to)->send($email);
            }
        }
       $penggalanganDanaSelesai = DB::select("Select * from proyek a JOIN brw_pendanaan b ON a.id = b.id_proyek where a.tgl_selesai_penggalangan = '".$dateNow."'"); 
        if($penggalanganDanaSelesai != null){
            foreach($penggalanganDanaSelesai as $pds){
                $id_proyek = $pds->id;
                $data = DB::select("SELECT brw_user.email,brw_user_detail.nama,brw_pendanaan.pendanaan_nama,brw_pendanaan.pendanaan_dana_dibutuhkan FROM brw_pendanaan JOIN brw_user ON brw_pendanaan.brw_id = brw_user.brw_id JOIN brw_user_detail ON brw_pendanaan.brw_id = brw_user_detail.brw_id WHERE id_proyek = '".$id_proyek."'");
                
                $email_to = $data[0]->{'email'};
                $get_dana_terkumpul = DB::select("SELECT sum(total_dana) as dana_terkumpul FROM pendanaan_aktif WHERE proyek_id = '".$id_proyek."'");
                $dana_terkumpul = $get_dana_terkumpul[0]->{'dana_terkumpul'};
                
                $get_investor = DB::select("SELECT nama_investor,total_dana FROM pendanaan_aktif JOIN detil_investor ON pendanaan_aktif.investor_id = detil_investor.investor_id where pendanaan_aktif.proyek_id = '".$id_proyek."' AND pendanaan_aktif.status = 1");

                $data_email = array(
                    'nama_pengguna' => $data[0]->{'nama'},
                    'nama_pendanaan' => $data[0]->{'pendanaan_nama'},
                    'dana_dibutuhkan' => "Rp. ".number_format($data[0]->{'pendanaan_dana_dibutuhkan'}),
                    'dana_terkumpul' => "Rp. ".number_format($dana_terkumpul),
                    'data_pendana' => $get_investor
                );
              
                
                $email = new PenggalanganSelesaiEmail($data_email);
                Mail::to($email_to)->send($email);
            }
        }
    }
}
