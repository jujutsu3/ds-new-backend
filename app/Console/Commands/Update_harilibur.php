<?php

namespace App\Console\Commands;

use Illuminate\Http\Request;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\DetilImbalUser;
use App\ListImbalUser;
use App\Log_Imbal_User;
use Mail;
use App\Mail\AutoDebetReg;
use App\Http\Controllers\Admin\ImbalHasilAdminController;


class Update_harilibur extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'console:Update_harilibur';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'proses Update_harilibur';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $run = new ImbalHasilAdminController();
        $result = $run->hari_libur();  
    }
}
