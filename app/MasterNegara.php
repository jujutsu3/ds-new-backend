<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterNegara extends Model
{
    public const INDONESIA = 0;
    protected $table = 'm_negara';
    protected $primaryKey = 'id_negara';
}
