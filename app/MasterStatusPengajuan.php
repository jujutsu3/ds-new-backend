<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterStatusPengajuan extends Model
{
    public const TABLE = 'm_status_pengajuan';
    // Values Constant
    public const V_BARU = 0;
    public const V_PENGGALANGAN = 8;

    protected $primaryKey = 'id_status_pengajuan';
    protected $table = 'm_status_pengajuan';
    protected $guard = 'borrower';
    protected $guarded = [];
}
