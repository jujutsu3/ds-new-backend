<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerDokumenCeklis extends Model
{
    protected $primaryKey = 'pengajuan_id';
    protected $table = 'brw_dokumen_ceklis';
    protected $guarded = [];
}
