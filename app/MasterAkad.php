<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class MasterAkad extends Model
{
    public const TABLE = 'm_akad';
    protected $table = 'm_akad';
    protected $primaryKey = 'id_agama';
}
