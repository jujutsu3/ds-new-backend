<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// API notif midtrans
// Route::post('notif', 'UserController@notificationHandler');
// end

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('digiSign/redirectAktifasi', 'DigiSignController@redirectDigisignAktifasi');

Route::get('digiSign/redirectSign', 'DigiSignController@redirectDigisignSign');

Route::post('/bni/callback', 'RekeningController@bankResponse');
Route::post('/bni_konven/callback', 'RekeningController@bankResponseKonven');
Route::post('/bsi/payment451', 'RekeningController@bankResponse451_payment');

Route::get('/cimbs/inquiry022', 'RekeningController@bankResponse022_inquiry');
Route::post('/cimbs/payment022', 'RekeningController@bankResponse022_payment');

// ########################################### PRIVY ID #################################################################### //

Route::post('/register', 'ProviderController@register');
Route::post('/privy/login', 'ProviderController@login');
Route::post('/privy/CallBack', 'ProviderController@CallBack');
// Route::post('/privy/CallBack','PrivyCallBackController@callbackResponse')->middleware('jwt.verify');
// Route::post('/privy/userStatus','PrivyController@callbackResponseUser')->middleware('jwt.verify');
// Route::get('/privy/callbackOauth','PrivyController@callbackURI');

Route::prefix('privyID')->group(function () {
    Route::post('/checkRegistrasi', 'Mobile\PrivyIDController@checkRegistrasi');
    Route::post('/checkDocumentStatus', 'Mobile\PrivyIDController@checkDocumentStatus');
    Route::post('/checkStatusDocumentPrivy', 'Mobile\PrivyIDController@checkStatusDocumentPrivy');
    Route::post('/sendDocMurobahahInvestorBorrower', 'Mobile\PrivyIDController@sendDocMurobahahInvestorBorrower');
    Route::post('/createDocMurobahahInvestorBorrower', 'Mobile\PrivyIDController@createDocMurobahahInvestorBorrower');
    Route::post('/sendDocAkadWakalahBilUjrohInvestor', 'Mobile\PrivyIDController@sendDocAkadWakalahBilUjrohInvestor');
    // Route::post('/viewTTD', 'Mobile\PrivyIDController@viewTTD');
    Route::post('/checkDocumentStatusWakalah', 'Mobile\PrivyIDController@checkDocumentStatusWakalah');
    Route::post('/RegisterPrivyID', 'Mobile\PrivyIDController@RegisterPrivyID');
    Route::post('/generateFormRDL', 'Mobile\PrivyIDController@generateFormRDL');
    Route::post('/getDocTokenWakalahInvestor', 'Mobile\PrivyIDController@getDocTokenWakalahInvestor');
});
// Route::post('/bni/enkripsi','RekeningController@enkripsi');
// Route::get('/msgVerification','RekeningController@msgVerification');
// route::get('/testing', 'RekeningController@testing');
Route::get('/test', function () {
    return 'API SUCCESS';
});

// Route::prefix('auth')->group(function() {
//     Route::post('/login', 'Mobile\Auth\ApiAuthController@login');
//     Route::post('/logout', 'Mobile\Auth\ApiAuthController@logout');
//     Route::post('/refresh', 'Mobile\Auth\ApiAuthController@refresh');
//     Route::post('/check', 'Mobile\Auth\ApiAuthController@checkToken');
//     Route::post('/register', 'Mobile\Auth\ApiAuthController@register');
//     Route::post('/datafill', 'Mobile\Auth\ApiAuthController@datafill');
// });

Route::prefix('newAuth')->group(function () {
    Route::post('/login', 'Mobile\Auth\NewApiAuthController@login');
    Route::post('/logout', 'Mobile\Auth\NewApiAuthController@logout');
    Route::post('/refresh', 'Mobile\Auth\NewApiAuthController@refresh');
    Route::post('/check', 'Mobile\Auth\NewApiAuthController@checkToken');
    Route::post('/newCheck', 'Mobile\Auth\NewApiAuthController@newCheckToken');
    Route::post('/newnewCheck', 'Mobile\Auth\NewApiAuthController@newnewCheckToken');
    Route::post('/register', 'Mobile\Auth\NewApiAuthController@register');
    Route::post('/register_new', 'Mobile\Auth\NewApiAuthController@register_new');
    Route::post('/register_new_new', 'Mobile\Auth\NewApiAuthController@register_new_new');
    Route::post('/datafill', 'Mobile\Auth\NewApiAuthController@datafill');
    Route::post('/datafillNew', 'Mobile\Auth\NewApiAuthController@datafillNew');
    Route::post('/datafillNewNew', 'Mobile\Auth\NewApiAuthController@datafillNewNew');
    Route::post('/datafillNewNewNew', 'Mobile\Auth\NewApiAuthController@datafillNewNewNew');
    Route::post('/datafillNewNewNewNew', 'Mobile\Auth\NewApiAuthController@datafillNewNewNewNew');
    Route::post('/datafillNewNewNewNewNew', 'Mobile\Auth\NewApiAuthController@datafillNewNewNewNewNew');
    Route::post('/datafillNewNewNewNewNewNew', 'Mobile\Auth\NewApiAuthController@datafillNewNewNewNewNewNew');
    Route::post('/datafillbaru', 'Mobile\Auth\NewApiAuthController@datafillbaru');
    Route::post('/datafillbarubaru', 'Mobile\Auth\NewApiAuthController@datafillbarubaru');
    Route::post('/datafillbaruVAkonv', 'Mobile\Auth\NewApiAuthController@datafillbaru_VA_konv');
    Route::post('/upload1', 'Mobile\Auth\NewApiAuthController@actionUpload1');
    Route::post('/upload2', 'Mobile\Auth\NewApiAuthController@actionUpload2');
    Route::post('/upload3', 'Mobile\Auth\NewApiAuthController@actionUpload3');
    Route::post('/upload1new', 'Mobile\Auth\NewApiAuthController@actionUpload1new');
    Route::post('/upload2new', 'Mobile\Auth\NewApiAuthController@actionUpload2new');
    Route::post('/upload3new', 'Mobile\Auth\NewApiAuthController@actionUpload3new');
    Route::post('/upload1newnew', 'Mobile\Auth\NewApiAuthController@actionUpload1newnew');
    Route::post('/upload2newnew', 'Mobile\Auth\NewApiAuthController@actionUpload2newnew');
    Route::post('/upload3newnew', 'Mobile\Auth\NewApiAuthController@actionUpload3newnew');

    Route::post('/upload1newnewnew', 'Mobile\Auth\NewApiAuthController@actionUpload1newnewnew');
    Route::post('/upload2newnewnew', 'Mobile\Auth\NewApiAuthController@actionUpload2newnewnew');
    Route::post('/upload3newnewnew', 'Mobile\Auth\NewApiAuthController@actionUpload3newnewnew');

    Route::post('/verificationCode', 'Mobile\Auth\NewApiAuthController@verificationCode');
    Route::get('/checkVersion', 'Mobile\Auth\NewApiAuthController@checkVersion');
    Route::post('/checkPhoneNumber', 'Mobile\Auth\NewApiAuthController@checkPhoneNumber');
    Route::post('/check_KTP', 'Mobile\Auth\NewApiAuthController@check_KTP');
    Route::post('/verificationOtp', 'Mobile\Auth\NewApiAuthController@verificationOtp');
    Route::post('/validateOTP', 'Mobile\Auth\NewApiAuthController@validateOTP');
    Route::post('/resendEmail', 'Mobile\Auth\NewApiAuthController@resendEmail');
    Route::post('/termCondition', 'Mobile\Auth\NewApiAuthController@getTermCondition');
    Route::post('/datafillOJK', 'Mobile\Auth\NewApiAuthController@datafill_OJK');
    Route::post('/register_baru', 'Mobile\Auth\NewApiAuthController@register_baru');
    Route::post('/cek_status_reg', 'Mobile\Auth\NewApiAuthController@cek_status_reg');

    Route::post('/verifyPassword', 'Mobile\Auth\NewApiAuthController@verifyPassword');
});

// Route::prefix('proyek')->group(function(){
//     Route::post('/list', 'Mobile\ProyekController@proyek');
//     Route::post('/detil', 'Mobile\ProyekController@detil_proyek');
//     Route::post('/checkout', 'Mobile\ProyekController@checkout');
//     Route::post('/cart', 'Mobile\ProyekController@cart');
// });

Route::prefix('newProyek')->group(function () {
    Route::post('/list', 'Mobile\NewProyekController@proyek');
    Route::post('/listAll', 'Mobile\NewProyekController@proyekAll');
    Route::post('/v2/listAll', 'Mobile\NewProyekController@listAllV2');
    Route::post('/detil', 'Mobile\NewProyekController@detil_proyek');
    Route::post('/checkout', 'Mobile\NewProyekController@checkout');
    Route::post('/checkout_new', 'Mobile\NewProyekController@checkout_new');
    Route::post('/checkout_new_new', 'Mobile\NewProyekController@checkout_new_new');
    Route::post('/cart', 'Mobile\NewProyekController@cart');
    Route::post('/total-penarikan', 'Mobile\NewProyekController@totalPenarikan');
    Route::post('/selectedProject', 'Mobile\NewProyekController@selectedProject');
    Route::post('/showSelectedProject', 'Mobile\NewProyekController@showSelectedProject');
    Route::post('/deleteSelectedProject', 'Mobile\NewProyekController@deleteSelectedProject');

    Route::post('/updatePaket', 'Mobile\NewProyekController@updatePaket');
});

Route::prefix('newSimulation')->group(function () {
    Route::post('/listAll', 'Mobile\NewProyekController@simulationAll');
});

Route::prefix('pendanaan')->group(function () {
    Route::post('/list', 'Mobile\PendanaanController@showPendanaan');
    Route::post('/tambah-pendanaan', 'Mobile\PendanaanController@tambahPendanaan');
    Route::post('/ambil-pendanaan', 'Mobile\PendanaanController@ambilPendanaan');
    // Route::post('/subscribe', 'Mobile\PendanaanController@subscribe');
    // Route::post('/unsubscribe', 'Mobile\PendanaanController@unsubscribe');
    Route::post('/progress', 'Mobile\PendanaanController@detilProgress');
});

Route::prefix('newPendanaan')->group(function () {
    Route::post('/list', 'Mobile\NewPendanaanController@showPendanaan');
    Route::post('/listKelolaInvestasi', 'Mobile\NewPendanaanController@showPendanaanKelolaInvestasi');
    Route::post('/listKelolaInvestasiNew', 'Mobile\NewPendanaanController@showPendanaanKelolaInvestasiNew');
    Route::post('/listKelolaInvestasiNewNew', 'Mobile\NewPendanaanController@showPendanaanKelolaInvestasiNewNew');
    Route::post('/checkValidation', 'Mobile\NewPendanaanController@checkValidation');
    Route::post('/tambah-pendanaan', 'Mobile\NewPendanaanController@tambahPendanaan');
    Route::post('/tambah-pendanaan-new', 'Mobile\NewPendanaanController@tambahPendanaanNew');
    Route::post('/ambil-pendanaan', 'Mobile\NewPendanaanController@ambilPendanaan');
    Route::post('/ambil-pendanaan-new', 'Mobile\NewPendanaanController@ambilPendanaanNew');
    // Route::post('/subscribe', 'Mobile\PendanaanController@subscribe');
    // Route::post('/unsubscribe', 'Mobile\PendanaanController@unsubscribe');
    Route::post('/progress', 'Mobile\NewPendanaanController@detilProgress');
    //Route::post('/cekAkadMurobahah','Mobile\NewPendanaanController@cek_akad_murobahah');
    Route::post('/cekRegDigisign', 'Mobile\NewPendanaanController@cek_reg_digisign');
    Route::post('/cekAkad', 'Mobile\NewPendanaanController@cekAkad');
});

// Route::prefix('profile')->group(function(){
//     Route::post('/show', 'Mobile\ProfileController@showProfile');
//     Route::post('/update', 'Mobile\ProfileController@updateProfile');
//     Route::post('/change-pass', 'Mobile\ProfileController@changePassword');
//     Route::post('/home', 'Mobile\ProfileController@home');
//     Route::post('/main', 'Mobile\ProfileController@mainProfile');
//     Route::post('/showva', 'Mobile\ProfileController@showVa');
// });

Route::prefix('v2/newProfile')->group(function () {
    Route::post('/allMasterAgama', 'Mobile\NewProfileController@allMasterAgama');
    Route::post('/allMasterJenisKelamin', 'Mobile\NewProfileController@allMasterJenisKelamin');
    Route::post('/allMasterBank', 'Mobile\NewProfileController@allMasterBank');
    Route::post('/allMasterProvinsi', 'Mobile\NewProfileController@allMasterProvinsi');
    Route::post('/allMasterKotaKab', 'Mobile\NewProfileController@allMasterKotaKab');
    Route::post('/allMasterKawin', 'Mobile\NewProfileController@allMasterKawin');
    Route::post('/allMasterPendapatan', 'Mobile\NewProfileController@allMasterPendapatan');
    Route::post('/allMasterPendidikan', 'Mobile\NewProfileController@allMasterPendidikan');
    Route::post('/allMasterPekerjaan', 'Mobile\NewProfileController@allMasterPekerjaan');
    Route::post('/allMasterJenisPengguna', 'Mobile\NewProfileController@allMasterJenisPengguna');
    Route::post('/allMasterNegara', 'Mobile\NewProfileController@allMasterNegara');
    Route::post('/allMasterBidangPekerjaan', 'Mobile\NewProfileController@allMasterBidangPekerjaan');
    Route::post('/allMasterPengalamanKerja', 'Mobile\NewProfileController@allMasterPengalamanKerja');
    Route::post('/allMasterJabatan', 'Mobile\NewProfileController@allMasterJabatan');
    Route::post('/allMasterOnline', 'Mobile\NewProfileController@allMasterOnline');
    Route::post('/allMasterHubunganWaris', 'Mobile\NewProfileController@allMasterHubunganWaris');
    Route::post('/allMasterKodeOperatorNegara', 'Mobile\NewProfileController@allMasterKodeOperatorNegara');
    
    Route::post('/allMasterKodeAlasanPenarikan', 'Mobile\NewProfileController@allMasterKodeAlasanPenarikan');
});

Route::prefix('newProfile')->group(function () {
    Route::post('/show', 'Mobile\NewProfileController@showProfile');
    Route::post('/showBaru', 'Mobile\NewProfileController@showProfileBaru');
    Route::post('/showBaruBaru', 'Mobile\NewProfileController@showProfileBaruBaru');

    Route::post('/update', 'Mobile\NewProfileController@updateProfile');
    Route::post('/updateNew', 'Mobile\NewProfileController@updateProfileNew');
    Route::post('/updateProfileBaru', 'Mobile\NewProfileController@updateProfileBaru');
    Route::post('/updateProfileBaruBaru', 'Mobile\NewProfileController@updateProfileBaruBaru');

    Route::post('/change-pass', 'Mobile\NewProfileController@changePassword');
    Route::post('/change-pass-expired', 'Mobile\NewProfileController@changePasswordExpired');
    Route::post('/home', 'Mobile\NewProfileController@home');
    Route::post('/homeLogin', 'Mobile\NewProfileController@homeLogin');
    Route::post('/showva', 'Mobile\NewProfileController@showVa');
    Route::post('/showVaNewNew', 'Mobile\NewProfileController@showVaNewNew');
    Route::post('/showStepTopUp', 'Mobile\NewProfileController@showStepTopUp');
    Route::post('/allMaster', 'Mobile\NewProfileController@allMaster');
    Route::post('/allMasterBaru', 'Mobile\NewProfileController@allMasterBaru');

    Route::post('/allProfile', 'Mobile\NewProfileController@allProfile');
    Route::post('/allProfileSertifikat', 'Mobile\NewProfileController@allProfileSertifikat');
    Route::post('/newallProfileSertifikat', 'Mobile\NewProfileController@newallProfileSertifikat');
    Route::post('/newnewallProfileSertifikat', 'Mobile\NewProfileController@newnewallProfileSertifikat');
    Route::post('/upload1', 'Mobile\NewProfileController@actionUpload1');
    Route::post('/upload2', 'Mobile\NewProfileController@actionUpload2');
    Route::post('/upload3', 'Mobile\NewProfileController@actionUpload3');
    Route::post('/upload1new', 'Mobile\NewProfileController@actionUpload1new');
    Route::post('/upload2new', 'Mobile\NewProfileController@actionUpload2new');
    Route::post('/upload3new', 'Mobile\NewProfileController@actionUpload3new');

    Route::post('/upload1newnew', 'Mobile\NewProfileController@actionUpload1newnew');
    Route::post('/upload2newnew', 'Mobile\NewProfileController@actionUpload2newnew');
    Route::post('/upload3newnew', 'Mobile\NewProfileController@actionUpload3newnew');

    Route::post('/get_aktif_dana', 'Mobile\NewProfileController@get_aktif_dana');
    Route::post('/get_aktif_dana_new', 'Mobile\NewProfileController@get_aktif_dana_new');
    Route::post('/get_aktif_dana_new_new', 'Mobile\NewProfileController@get_aktif_dana_new_new');
    Route::post('/ListProyekFunded', 'Mobile\NewProfileController@list_proyek_funded');
    Route::post('/all_imbal', 'Mobile\NewProfileController@all_imbal');
    Route::post('/cekDanaTeralokasi', 'Mobile\NewProfileController@cek_dana_teralokasi');
    Route::post('/registerRDLInvestor', 'Mobile\NewProfileController@registerRDLInvestor');
    Route::post('/generarteVAkonv', 'Mobile\NewProfileController@generateVA_BNI_konv');

    Route::post('/registerAccountNumberInvestor', 'Mobile\NewProfileController@registerAccountNumberInvestor');
    Route::post('/newnewallProfileSertifikatBaru', 'Mobile\NewProfileController@newnewallProfileSertifikatBaru');
    Route::post('/newnewnewallProfileSertifikatBaru', 'Mobile\NewProfileController@newnewnewallProfileSertifikatBaru');
    Route::post('/showVaNew', 'Mobile\NewProfileController@showVaNew');

    //DIGISIGN MOBILE

    //REGISTRASI DAN AKTIVIASI AKUN DIGISIGN
    Route::post('/statusRegDigisign', 'Mobile\NewProyekController@statusRegDigisign');
    Route::post('/statusRegPrivy', 'Mobile\NewProyekController@statusRegPrivy');
    Route::post('/registerAkad', 'Mobile\NewProfileController@register_akad');
    Route::post('/callbackDigiSignInvestor', 'Mobile\NewProfileController@callbackRegisterInvestor');
    Route::post('/actDigiSign', 'Mobile\NewProfileController@actDigiSign');
    Route::post('/getIdLog', 'Mobile\NewProfileController@get_id_log');

    //TTD DAN DOWNLOAD AKAD INVESTOR - DSI
    Route::post('/signDigiSign', 'Mobile\NewProfileController@signDigiSign');
    Route::post('/signDigiSignInvestorBorrower', 'Mobile\NewProfileController@signDigiSignInvestorBorrower');
    Route::post('/sendDocDigiSignInvestorBorrower', 'Mobile\NewProfileController@sendDocDigiSignInvestorBorrower');
    Route::post('/createDocDigiSignInvestorBorrower', 'Mobile\NewProfileController@createDocDigiSignInvestorBorrower');
    Route::post('/sendDigiSignawal', 'Mobile\NewProfileController@sendDigiSignawal');
    Route::post('/downloadDigiSign', 'Mobile\NewProfileController@downloadDigiSignInvestor');
    Route::post('/convertBase64', 'Mobile\NewProfileController@convertBase64');
    Route::post('/convertBase64Murobahah', 'Mobile\NewProfileController@convertBase64Murobahah');
    Route::post('/signDigiSignMurobahah', 'Mobile\NewProfileController@signDigiSignMurobahah');
    Route::post('/downloadBase64DigiSignMurobahah', 'Mobile\NewProfileController@downloadBase64DigiSignMurobahah');
    Route::post('/logAkad', 'Mobile\NewProfileController@logAkad');
    Route::post('/downloadAkad', 'Mobile\NewProfileController@downloadAkad');
    Route::post('/newdownloadAkad', 'Mobile\NewProfileController@newdownloadAkad');
});

Route::prefix('khazanah')->group(function () {
    Route::post('/get', 'Mobile\NewProfileController@getKhazanah');
    Route::post('/detil', 'Mobile\NewProfileController@getKhazanahDetil');
});

// Route::prefix('rekening')->group(function(){
//     Route::post('/mutasi', 'Mobile\RekeningController@listMutasi');
//     Route::post('/showPenarikan', 'Mobile\RekeningController@showPenarikan');
//     Route::post('/requestPenarikan', 'Mobile\RekeningController@requestPenarikan');
// });

Route::prefix('newRekening')->group(function () {
    Route::post('/mutasi', 'Mobile\NewRekeningController@listMutasi');
    Route::post('/showPenarikan', 'Mobile\NewRekeningController@showPenarikan');
    Route::post('/requestPenarikan', 'Mobile\NewRekeningController@requestPenarikan');
    Route::post('/verificationCode', 'Mobile\NewRekeningController@verificationCode');
    Route::post('/sendVerifikasi', 'Mobile\NewRekeningController@sendVerifikasi');
    Route::post('/checkUploadFoto', 'Mobile\NewRekeningController@checkUploadFoto');
    Route::post('/historyPenarikanDana', 'Mobile\NewRekeningController@historyPenarikanDana');

    Route::post('/downloadSertifikat', 'Mobile\NewRekeningController@sertifikat');
    Route::post('/viewSertifikat', 'Mobile\NewRekeningController@viewSertifikat');
    Route::post('/cekSertifikat', 'Mobile\NewRekeningController@cekSertifikat');
    Route::post('/checkStatusUserInvest', 'Mobile\NewRekeningController@checkStatusUserInvest');
    Route::post('/checkStatusRDL', 'Mobile\NewRekeningController@checkStatusRDL');
    Route::post('/createRDL', 'Mobile\NewRekeningController@createRDL');
});

Route::prefix('news')->group(function () {
    Route::post('/get', 'Mobile\NewsController@getNews');
    Route::post('/detil', 'Mobile\NewsController@getDetil');
});

Route::prefix('kprAuth')->group(function () {
    Route::post('/login', 'Mobile\Kpr\KprAuthController@login');
    Route::post('/logout', 'Mobile\Kpr\KprAuthController@logout');
    Route::post('/newnewCheck', 'Mobile\Kpr\KprAuthController@newnewCheckToken');
    Route::post('/register', 'Mobile\Kpr\KprAuthController@register_new_new_new');

    //Route::post('/hasTooManyLoginAttempts', 'Mobile\Kpr\KprAuthController@hasTooManyLoginAttempts');
    Route::post('/sendLockoutResponse', 'Mobile\Kpr\KprAuthController@sendLockoutResponse');

    Route::post('/resendEmail', 'Mobile\Kpr\KprAuthController@resendEmail');

    Route::post('/getDataBerandaKpr', 'Mobile\Kpr\KprGetDataController@getDataBerandaKpr');
    Route::post('/getDataPendanaanKpr', 'Mobile\Kpr\KprGetDataController@getDataPendanaanKpr');
    Route::post('/getDataProfileKpr', 'Mobile\Kpr\KprGetDataController@getDataProfileKpr');
    Route::post('/allMasterBaru', 'Mobile\Kpr\KprGetDataController@allMasterBaru');
    Route::post('/allMasterFormRpc', 'Mobile\Kpr\KprGetDataController@allMasterFormRpc');
    Route::post('/allMasterPengajuan', 'Mobile\Kpr\KprGetDataController@allMasterPengajuan');
    Route::post('/masterSimulasiPengajuan', 'Mobile\Kpr\KprGetDataController@masterSimulasiPengajuan');
    Route::post('/getResultSimulationKepemilikan', 'Mobile\Kpr\KprGetDataController@getResultSimulationKepemilikan');
    Route::post('/getResultSimulationPraKepemilikan', 'Mobile\Kpr\KprGetDataController@getResultSimulationPraKepemilikan');
    Route::post('/listMenuKpr', 'Mobile\Kpr\KprGetDataController@listMenuKpr');
    Route::post('/listMenuKprPengajuan', 'Mobile\Kpr\KprGetDataController@listMenuKprPengajuan');
    Route::post('/getDataAllRpc', 'Mobile\Kpr\KprGetDataController@getDataAllRpc');
    Route::post('/getDataFotoRpc', 'Mobile\Kpr\KprGetDataController@getDataFotoRpc');
    Route::post('/getDataFotoDokumenObjekPendanaan', 'Mobile\Kpr\KprGetDataController@getDataFotoDokumenObjekPendanaan');
    Route::post('/getDataFotoDokumenLegalitasPribadi', 'Mobile\Kpr\KprGetDataController@getDataFotoDokumenLegalitasPribadi');
    Route::post('/getDataAllRpcAndPengajuan', 'Mobile\Kpr\KprGetDataController@getDataAllRpcAndPengajuan');
    Route::post('/getDataBiodataPribadi', 'Mobile\Kpr\KprGetDataController@getDataBiodataPribadi');
    Route::post('/getDataBiodataAlamat', 'Mobile\Kpr\KprGetDataController@getDataBiodataAlamat');
    Route::post('/getDataRekening', 'Mobile\Kpr\KprGetDataController@getDataRekening');
    Route::post('/getDataPekerjaan', 'Mobile\Kpr\KprGetDataController@getDataPekerjaan');
    Route::post('/getDataAjukanPendanaanRpc', 'Mobile\Kpr\KprGetDataController@getDataAjukanPendanaanRpc');
    Route::get('/getProvinsi', 'Mobile\Kpr\KprGetDataController@getProvinsi');
    Route::get('/getKota/{id}', 'Mobile\Kpr\KprGetDataController@getKota');
    Route::get('/getKecamatan/{id}', 'Mobile\Kpr\KprGetDataController@getKecamatan');
    Route::get('/getKelurahan/{id}', 'Mobile\Kpr\KprGetDataController@getKelurahan');
    Route::get('/getKodePos/{kelurahan}/{kecamatan}', 'Mobile\Kpr\KprGetDataController@getKodePos');
    Route::get('/getProvinsiPendanaan', 'Mobile\Kpr\KprGetDataController@getProvinsiPendanaan');
    Route::get('/getKotaPendanaan/{id}', 'Mobile\Kpr\KprGetDataController@getKotaPendanaan');
    Route::get('/getKecamatanPendanaan/{id}', 'Mobile\Kpr\KprGetDataController@getKecamatanPendanaan');
    Route::get('/getKelurahanPendanaan/{id}', 'Mobile\Kpr\KprGetDataController@getKelurahanPendanaan');
    Route::get('/getKodePosPendanaan/{kelurahan}/{kecamatan}', 'Mobile\Kpr\KprGetDataController@getKodePos');
    Route::post('/getKantorPenerbit', 'Mobile\Kpr\KprGetDataController@getKantorPenerbit');
    Route::post('/getDataPengajuan2', 'Mobile\Kpr\KprGetDataController@getDataPengajuan2');
    Route::post('/getDataPengajuan2Page3', 'Mobile\Kpr\KprGetDataController@getDataPengajuan2Page3');
    Route::post('/getDataExistPengajuan2Page1', 'Mobile\Kpr\KprGetDataController@getDataExistPengajuan2Page1');
    Route::post('/getDataExistPengajuan2Page2', 'Mobile\Kpr\KprGetDataController@getDataExistPengajuan2Page2');
    Route::post('/getDataExistPengajuan2Page3', 'Mobile\Kpr\KprGetDataController@getDataExistPengajuan2Page3');

    Route::post('/validateOTP', 'Mobile\Kpr\KprValidasiController@validateOTP');
    Route::post('/sendOtp', 'Mobile\Kpr\KprValidasiController@sendOtp');
    Route::post('/check_KTP', 'Mobile\Kpr\KprValidasiController@check_KTP');
    Route::post('/checkNpwp', 'Mobile\Kpr\KprValidasiController@checkNpwp');
    Route::post('/checkPhoneNumber', 'Mobile\Kpr\KprValidasiController@checkPhoneNumber');
    Route::post('/cekBiodataPribadi', 'Mobile\Kpr\KprValidasiController@cekBiodataPribadi');

    Route::post('/addPendanaanKprRpc', 'Mobile\Kpr\KprProcessDataController@addPendanaanKprRpc');
    Route::post('/saveKprAll', 'Mobile\Kpr\KprProcessDataController@saveKprAll');
    Route::post('/saveKprDataPribadi', 'Mobile\Kpr\KprProcessDataController@saveKprDataPribadi');
    Route::post('/saveKprDataAlamat', 'Mobile\Kpr\KprProcessDataController@saveKprDataAlamat');
    Route::post('/saveKprDataRekening', 'Mobile\Kpr\KprProcessDataController@saveKprDataRekening');
    Route::post('/addPendanaanKpr', 'Mobile\Kpr\KprProcessDataController@addPendanaanKpr');
    Route::post('/pengajuanPendanaanRumah', 'Mobile\Kpr\KprProcessDataController@pengajuanPendanaanRumah');
    Route::post('/pengajuanPendanaanRumahDibantu', 'Mobile\Kpr\KprProcessDataController@pengajuanPendanaanRumahDibantu');
    Route::post('/pengajuanPendanaanRumahDibantuDokumen', 'Mobile\Kpr\KprProcessDataController@pengajuanPendanaanRumahDibantuDokumen');
    Route::post('/uploadFoto', 'Mobile\Kpr\KprProcessDataController@uploadFoto');
    Route::post('/uploadDokumen', 'Mobile\Kpr\KprProcessDataController@uploadDokumen');
});

Route::post('v1/borrower/auth/forgot_password', 'Mobile\BorrowerForgotPasswordControllerV1@sendResetLinkEmail');
Route::post('v1/lender/auth/forgot_password', 'Mobile\LenderForgotPasswordControllerV1@sendResetLinkEmail');

Route::get('v1/borrower_info', 'Mobile\InfoControllerV1@borrower')->middleware('auth:borrower-api-mobile');
Route::get('v1/lender_info', 'Mobile\InfoControllerV1@lender')->middleware('auth:api');

Route::prefix('v1/lender/kyc')->group(function () {
    Route::get('all_status', 'Mobile\LenderKYCControllerV1@allStatus');
    Route::get('status', 'Mobile\LenderKYCControllerV1@currentStatus');
    Route::get('data_diri', 'Mobile\LenderKYCControllerV1@profile');
    Route::post('data_diri', 'Mobile\LenderKYCControllerV1@updateProfile');
    Route::get('data_diri/foto', 'Mobile\LenderKYCControllerV1@profilePic');
    Route::post('data_diri/foto', 'Mobile\LenderKYCControllerV1@updateProfilePic');
    Route::get('pendidikan', 'Mobile\LenderKYCControllerV1@education');
    Route::post('pendidikan', 'Mobile\LenderKYCControllerV1@updateEducation');
    Route::get('ahli_waris', 'Mobile\LenderKYCControllerV1@inheritance');
    Route::post('ahli_waris', 'Mobile\LenderKYCControllerV1@updateInheritance');
    Route::post('send_otp', 'Mobile\LenderKYCControllerV1@sendOtp');
    Route::post('verify_otp', 'Mobile\LenderKYCControllerV1@updateOtp');
});

Route::prefix('v1/lender_corporation/kyc')->group(function () {
    Route::get('all_status', 'Mobile\LenderCorporationKYCControllerV1@allStatus');
    Route::get('status', 'Mobile\LenderCorporationKYCControllerV1@currentStatus');
    Route::get('data_perusahaan', 'Mobile\LenderCorporationKYCControllerV1@getCorporation');
    Route::post('data_perusahaan', 'Mobile\LenderCorporationKYCControllerV1@updateCorporation');
    Route::get('pengurus', 'Mobile\LenderCorporationKYCControllerV1@getAdministrator');
    Route::post('pengurus', 'Mobile\LenderCorporationKYCControllerV1@updateAdministrator');
    Route::get('pemegang_saham', 'Mobile\LenderCorporationKYCControllerV1@getShareholder');
    Route::post('pemegang_saham', 'Mobile\LenderCorporationKYCControllerV1@updateShareholder');
    Route::get('contacts', 'Mobile\LenderCorporationKYCControllerV1@getContacts');
    Route::post('contacts', 'Mobile\LenderCorporationKYCControllerV1@updateContacts');
    Route::post('send_otp', 'Mobile\LenderCorporationKYCControllerV1@sendOtp');
    Route::post('verify_otp', 'Mobile\LenderCorporationKYCControllerV1@updateOtp');
});

Route::prefix('v1/lender_corporation/profile')->group(function () {
    Route::get('data_perusahaan', 'Mobile\LenderCorporationProfileControllerV1@getCorporation');
    Route::post('data_perusahaan', 'Mobile\LenderCorporationProfileControllerV1@updateCorporation');
    Route::get('pengurus', 'Mobile\LenderCorporationProfileControllerV1@getAdministrator');
    Route::post('pengurus', 'Mobile\LenderCorporationProfileControllerV1@updateAdministrator');
    Route::get('pemegang_saham', 'Mobile\LenderCorporationProfileControllerV1@getShareholder');
    Route::post('pemegang_saham', 'Mobile\LenderCorporationProfileControllerV1@updateShareholder');
    Route::get('contacts', 'Mobile\LenderCorporationProfileControllerV1@getContacts');
    Route::post('contacts', 'Mobile\LenderCorporationProfileControllerV1@updateContacts');
    Route::post('verify_data', 'Mobile\LenderCorporationProfileControllerV1@verifyData');
});

Route::post('v1/lender/email_check', function (Request $request) {
    $isExists = \App\Investor::query()->where('email', $request->get('email'))->exists();
    $isDomainValid = checkdnsrr(explode("@", $request->get('email'))[1] ?? null, 'ANY');

    return [
        'valid' => ! $isExists && $isDomainValid,
    ];
})->name('v1.lender.email_check');
Route::post('v1/borrower/email_check', function (Request $request) {
    $isExists = \App\Borrower::query()->where('email', $request->get('email'))->exists();
    $isDomainValid = checkdnsrr(explode("@", $request->get('email'))[1] ?? null, 'ANY');

    return [
        'valid' => ! $isExists && $isDomainValid,
    ];
})->name('v1.borrower.email_check');
Route::get('v1/borrower/email_check', 'Mobile\FileControllerV1@getFile')->middleware('auth:borrower-api-mobile')->name('v1.borrower.file');

Route::get('v1/lender/file', 'Mobile\FileControllerV1@getFile')->middleware('auth:api')->name('v1.lender.file');
Route::get('v1/borrower/file', 'Mobile\FileControllerV1@getFile')->middleware('auth:borrower-api-mobile')->name('v1.borrower.file');

Route::prefix('v1/borrower/kyc')->group(function () {
    Route::get('all_status', 'Mobile\BorrowerKYCControllerV1@allStatus');
    Route::get('status', 'Mobile\BorrowerKYCControllerV1@currentStatus');
    Route::get('data_diri', 'Mobile\BorrowerKYCControllerV1@profile');
    Route::post('data_diri', 'Mobile\BorrowerKYCControllerV1@updateProfile');
    Route::get('data_diri/foto', 'Mobile\BorrowerKYCControllerV1@profilePic');
    Route::post('data_diri/foto', 'Mobile\BorrowerKYCControllerV1@updateProfilePic');
    Route::get('pendidikan', 'Mobile\BorrowerKYCControllerV1@education');
    Route::post('pendidikan', 'Mobile\BorrowerKYCControllerV1@updateEducation');
    Route::get('pendidikan/foto', 'Mobile\BorrowerKYCControllerV1@getEducationPic');
    Route::post('pendidikan/foto', 'Mobile\BorrowerKYCControllerV1@updateEducationPic');
    Route::post('send_otp', 'Mobile\BorrowerKYCControllerV1@sendOtp');
    Route::post('verify_otp', 'Mobile\BorrowerKYCControllerV1@updateOtp');
});

Route::prefix('v1/borrower_corporation/kyc')->group(function () {
    Route::get('all_status', 'Mobile\BorrowerCorporationKYCControllerV1@allStatus');
    Route::get('status', 'Mobile\BorrowerCorporationKYCControllerV1@currentStatus');
    Route::get('data_perusahaan', 'Mobile\BorrowerCorporationKYCControllerV1@getCorporation');
    Route::post('data_perusahaan', 'Mobile\BorrowerCorporationKYCControllerV1@updateCorporation');
    Route::get('pengurus', 'Mobile\BorrowerCorporationKYCControllerV1@getAdministrator');
    Route::post('pengurus', 'Mobile\BorrowerCorporationKYCControllerV1@updateAdministrator');
    Route::get('pemegang_saham', 'Mobile\BorrowerCorporationKYCControllerV1@getShareholder');
    Route::post('pemegang_saham', 'Mobile\BorrowerCorporationKYCControllerV1@updateShareholder');
    Route::get('contacts', 'Mobile\BorrowerCorporationKYCControllerV1@getContacts');
    Route::post('contacts', 'Mobile\BorrowerCorporationKYCControllerV1@updateContacts');
    Route::post('send_otp', 'Mobile\BorrowerCorporationKYCControllerV1@sendOtp');
    Route::post('verify_otp', 'Mobile\BorrowerCorporationKYCControllerV1@updateOtp');
});

Route::prefix('v1/borrower_corporation/profile')->group(function () {
    Route::get('data_perusahaan', 'Mobile\BorrowerCorporationProfileControllerV1@getCorporation');
    Route::post('data_perusahaan', 'Mobile\BorrowerCorporationProfileControllerV1@updateCorporation');
    Route::get('pengurus', 'Mobile\BorrowerCorporationProfileControllerV1@getAdministrator');
    Route::post('pengurus', 'Mobile\BorrowerCorporationProfileControllerV1@updateAdministrator');
    Route::get('pemegang_saham', 'Mobile\BorrowerCorporationProfileControllerV1@getShareholder');
    Route::post('pemegang_saham', 'Mobile\BorrowerCorporationProfileControllerV1@updateShareholder');
    Route::get('contacts', 'Mobile\BorrowerCorporationProfileControllerV1@getContacts');
    Route::post('contacts', 'Mobile\BorrowerCorporationProfileControllerV1@updateContacts');
    Route::post('verify_data', 'Mobile\BorrowerCorporationProfileControllerV1@verifyData');
});

Route::prefix('v2/lender/profile')->group(function () {
    Route::get('data_diri', 'Mobile\LenderProfileControllerV2@profile');
    Route::post('data_diri', 'Mobile\LenderProfileControllerV2@updateProfile');
    Route::get('ahli_waris', 'Mobile\LenderProfileControllerV2@inheritance');
    Route::post('ahli_waris', 'Mobile\LenderProfileControllerV2@updateInheritance');
    Route::get('pendidikan', 'Mobile\LenderProfileControllerV2@education');
    Route::post('pendidikan', 'Mobile\LenderProfileControllerV2@updateEducation');
    Route::post('verify_data', 'Mobile\LenderProfileControllerV2@verifyData');
});


Route::post('v1/lender/imbal_hasil_pendanaan', 'Mobile\LenderPendanaanControllerV1@getYield');

Route::prefix('v2/borrower/profile')->group(function () {
    Route::get('data_diri', 'Mobile\BorrowerProfileControllerV2@getProfile');
    Route::post('data_diri', 'Mobile\BorrowerProfileControllerV2@updateProfile');
    Route::get('pendidikan', 'Mobile\BorrowerProfileControllerV2@getEducation');
    Route::post('pendidikan', 'Mobile\BorrowerProfileControllerV2@updateEducation');
    Route::post('/verify_password', 'Mobile\BorrowerProfileControllerV2@verifyPassword');
    Route::post('/change_password', 'Mobile\BorrowerProfileControllerV2@changePassword');
    Route::post('/change_pass_expired', 'Mobile\BorrowerProfileControllerV2@changePasswordExpired');
    Route::post('verify_data', 'Mobile\BorrowerProfileControllerV2@verifyData');
});

Route::prefix('v1/borrower/pendanaan')->group(function () {
    Route::get('pengajuan', 'Mobile\BorrowerPendanaanControllerV1@getFundingNew');
    Route::get('daftar_tipe', 'Mobile\BorrowerPendanaanControllerV1@getType');
    Route::get('pengajuan/{pengajuan}', 'Mobile\BorrowerPendanaanControllerV1@show');
    Route::get('pengajuan/{pengajuan}/items', 'Mobile\BorrowerPendanaanControllerV1@getItems');
    Route::post('pengajuan', 'Mobile\BorrowerPendanaanControllerV1@submitApplicationNew');

    Route::prefix('verifikasi_1/{pengajuan}')->group(function () {
        Route::get('status', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@currentStatus');
        Route::post('submit', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@submit');
        Route::get('data_diri', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@getProfile');
        Route::post('data_diri', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@updateProfile');
        Route::get('pekerjaan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@getProfession');
        Route::post('pekerjaan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@updateProfession');
        Route::get('objek_pendanaan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@objekPendanaan');
        Route::post('objek_pendanaan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@updateObjekPendanaan');
        Route::get('pemilik_pendanaan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@pemilikPendanaan');
        Route::post('pemilik_pendanaan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@updatePemilikPendanaan');
        Route::get('pendanaan_berjalan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@pendanaanBerjalan');
        Route::post('pendanaan_berjalan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@updatePendanaanBerjalan');
        Route::get('agunan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@agunan');
        Route::post('agunan', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@updateAgunan');
        Route::get('rekening', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@rekening');
        Route::post('rekening', 'Mobile\BorrowerPendanaanVerifikasi1ControllerV1@updateRekening');
        Route::get('pasangan_status', 'Mobile\BorrowerPendanaanVerifikasi1PasanganControllerV1@currentStatus');
        Route::get('pasangan_data_diri', 'Mobile\BorrowerPendanaanVerifikasi1PasanganControllerV1@getProfile');
        Route::post('pasangan_data_diri', 'Mobile\BorrowerPendanaanVerifikasi1PasanganControllerV1@updateProfile');
        Route::get('pasangan_alamat', 'Mobile\BorrowerPendanaanVerifikasi1PasanganControllerV1@getAddress');
        Route::post('pasangan_alamat', 'Mobile\BorrowerPendanaanVerifikasi1PasanganControllerV1@updateAddress');
        Route::get('pasangan_pekerjaan', 'Mobile\BorrowerPendanaanVerifikasi1PasanganControllerV1@getProfession');
        Route::post('pasangan_pekerjaan', 'Mobile\BorrowerPendanaanVerifikasi1PasanganControllerV1@updateProfession');
    });

    Route::prefix('verifikasi_2/{pengajuan}')->group(function () {
        Route::get('status', 'Mobile\BorrowerPendanaanVerifikasi2ControllerV1@currentStatus');
        Route::get('status', 'Mobile\BorrowerPendanaanVerifikasi2ControllerV1@currentStatus');
        Route::get('form_dokumen', 'Mobile\BorrowerPendanaanVerifikasi2ControllerV1@getDocumentForm');
        Route::get('dokumen', 'Mobile\BorrowerPendanaanVerifikasi2ControllerV1@getDocument');
        Route::post('dokumen', 'Mobile\BorrowerPendanaanVerifikasi2ControllerV1@updateDocument');
    });
});

# add by ichal_sl, March 14, 2022
Route::prefix('material_orders')->group(function () {
    Route::get('/list_construction_task', 'MaterialOrdersController@listConstructionTask');
    Route::get('/list_kategori_barang', 'MaterialOrdersController@listKategoriBarang');
    Route::get('/list_produk_per_kategori', 'MaterialOrdersController@listProdukPerCategory');
    Route::get('/list_produk', 'MaterialOrdersController@listProduk');
    Route::post('/purchase_requisition', 'MaterialOrdersController@purchaseRequisition');
    #Route::post('/history_transactions/{start_date}/{end_date}/{limit}/{offset}', 'MaterialOrdersController@historyTransactions');
    Route::post('/history_transactions', 'MaterialOrdersController@historyTransactions');
});

# add by ichal_sl, March 14, 2022
Route::prefix('faq')->group(function () {
    Route::get('/list_kategori', 'FaqController@listKategoriFaq');
    Route::get('/list_kategori_group', 'FaqController@listKategoriGroupFaq');
    Route::get('/list_group', 'FaqController@listGroupFaq');
    Route::get('/list_content/{category_id}/{group_id}', 'FaqController@listContentFaq');
    Route::post('proyekAvailable', 'ProyekController@availableProyek');
    Route::post('proyekDetail/{id_proyek}', 'ProyekController@detailProyek');    
});

# add by ichal_sl, April 14, 2022
Route::prefix('bridge')->group(function () {
    #mitraPajakku
    Route::get('/list_proyek', 'BridgeController@listProyek');
    Route::get('/lender_with_fund/{limit}', 'BridgeController@lenderWithFund');
    Route::post('/konfirmasi_mpajak', 'BridgeController@konfirmasiMPajakResponse');
    Route::get('/pajak_data_info', 'BridgeController@pajakDataInfo');
    Route::get('/valid_npwp', 'BridgeController@validNPWP');
    Route::get('/lender_with_fund_single/{investor_id}', 'BridgeController@lenderWithFundSingle');
    Route::get('/validasi_npwp/{investor_id}/{tipe_user}', 'BridgeController@validasiNpwp');
    Route::get('/lender_with_fund_all', 'BridgeController@lenderWithFundAll');
    Route::get('/count_lender_with_fund_all', 'BridgeController@countLenderWithFundAll');

    #mitraPajakku Borrower
    Route::get('/borrower_data_limit/{limit}', 'BridgeController@borrowerDataLimit');
    Route::get('/borrower_data_single/{brw_id}', 'BridgeController@borrowerDataSingle');
    Route::post('/konfirmasi_mpajak_b', 'BridgeController@konfirmasiMPajakBResponse');

    #mitraPajakku Imbar User
    Route::get('/list_imbal_user_limit/{masa}/{tahun}/{isvalid}/{limit}', 'BridgeController@listImbalUserLimit');
    Route::get('/list_imbal_user_all/{masa}/{tahun}', 'BridgeController@listImbalUserAll');
    Route::get('/list_imbal_user_single/{masa}/{tahun}/{investor_id}', 'BridgeController@listImbalUserSingle');
    Route::post('/imbal_is_taken', 'BridgeController@imbalIsTakenResponse');
    Route::post('/info_bukti_potong', 'BridgeController@infoBuktiPotongResponse');
    Route::get('/count_imbal_user_all/{masa}/{tahun}', 'BridgeController@countImbalUserAll');
    Route::post('/imbal_is_taken_all', 'BridgeController@imbalIsTakenAllResponse');

    #rekRekening
    Route::get('/cek_rekening_single/{investor_id}', 'BridgeController@cekRekeningSingle');
    Route::get('/cek_rekening_limit/{limit}', 'BridgeController@cekRekeningLimit');
    Route::post('/validasi_rekening', 'BridgeController@cekRekeningResponse');
    Route::get('/get_validasi_rekening/{investor_id}/{tipe_user}', 'BridgeController@validasiRekening');
	
	#inquiryRekening Lender
	Route::get('/inq_rekening_single_l/{investor_id}', 'BridgeController@inqRekeningSingleL');
	Route::get('/inq_rekening_limit_l/{limit}', 'BridgeController@inqRekeningLimitL');
	Route::post('/inq_rekening_l', 'BridgeController@inqRekeningResponseL');
	Route::get('/get_inq_rekening_l/{investor_id}/{tipe_user}', 'BridgeController@inqRekeningL');
	
	#inquiryRekening Borrower
	Route::get('/inq_rekening_single_b/{brw_id}', 'BridgeController@inqRekeningSingleB');
	Route::get('/inq_rekening_limit_b/{limit}', 'BridgeController@inqRekeningLimitB');
	Route::post('/inq_rekening_b', 'BridgeController@inqRekeningResponseB');
	Route::get('/get_inq_rekening_b/{brw_id}/{tipe_user}', 'BridgeController@inqRekeningB');
});


Route::prefix('v1/los')->group(function(){
    Route::post('/document', 'Mobile\BorrowerLosController@updateDocument');
    Route::delete('/document', 'Mobile\BorrowerLosController@deleteDocument');
    Route::post('/status', 'Mobile\BorrowerLosController@updateStatus');

});

Route::prefix('public')->group(function () {
    Route::get('/proyekAvailable', 'ProyekController@availableProyek');
    Route::get('/proyekDetail/{id_proyek}', 'ProyekController@detailProyek');    
});