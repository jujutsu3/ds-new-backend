<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'auth:borrower-api-public'
], function () {
    Route::get('borrower/master/kelurahan_by_kodepos', 'BorrowerMasterControllerV1@getKelurahanByKodePos');
    Route::post('borrower/register', 'BorrowerCorporationControllerV1@register');
    Route::post('borrower/pengajuan', 'BorrowerPengajuanControllerV1@submit');
    Route::get('borrower_individu/{borrower}', 'BorrowerIndividuControllerV1@getData');
    Route::post('borrower_individu/register', 'BorrowerIndividuControllerV1@register');
    // Route::post('borrower_corporation/register', 'BorrowerCorporationControllerV1@register');
    // Route::post('borrower_corporation/pendanaan', 'BorrowerCorporationControllerV1@register');
});

Route::group([
    'middleware' => [
        'auth:lender-api-public',
        'lender_class:' . \App\DetilInvestor::CLASS_PRIVATE,
    ]
], function () {
    Route::get('lender/proyek/list_available', 'LenderCorporationControllerV1@getAvailableProyek');
    // Route::post('borrower_corporation/register', 'BorrowerCorporationControllerV1@register');
    // Route::post('borrower_corporation/pendanaan', 'BorrowerCorporationControllerV1@register');
});
// Route::post('borrower/register', function (Request $request) {
//     try {
//         \Illuminate\Support\Facades\Auth::login(\App\Investor::query()->where('id', $request->header('X-USER-ID'))->first());
//     } catch (\Exception $exception) {
//         echo "No user found";
//     }
//     echo "<pre>";
//     print_r($request->header());
//     print_r($request->method());
//     print_r($request->all());
//     print_r(auth()->user()->toArray());
//     echo "</pre>";
// });
//
//
