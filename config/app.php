<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    |
    */

    'name' => env('APP_NAME', 'Laravel'),

    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */

    // 'env' => env('APP_ENV','production'),
    'env' => env('APP_ENV'),

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    // 'url' => env('APP_URL','https://www.danasyariah.id'),
    'url' => env('APP_URL'),

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Asia/Jakarta',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    // 'locale' => 'en',
    'locale' => 'id',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    // 'fallback_locale' => 'en',
    'fallback_locale' => 'id',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        Tymon\JWTAuth\Providers\LaravelServiceProvider::class,

        /*
         * Package Service Providers...
         */
        Maatwebsite\Excel\ExcelServiceProvider::class,
        Barryvdh\DomPDF\ServiceProvider::class,
        Riskihajar\Terbilang\TerbilangServiceProvider::class,
        //Harimayco\Menu\MenuServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
        Laravel\Socialite\SocialiteServiceProvider::class,
        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        // App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
        App\Providers\PublicAPIServiceProvider::class,



    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App' => Illuminate\Support\Facades\App::class,
        'Artisan' => Illuminate\Support\Facades\Artisan::class,
        'Auth' => Illuminate\Support\Facades\Auth::class,
        'Blade' => Illuminate\Support\Facades\Blade::class,
        'Broadcast' => Illuminate\Support\Facades\Broadcast::class,
        'Bus' => Illuminate\Support\Facades\Bus::class,
        'Cart' => App\Facades\Cart::class,
        'Cache' => Illuminate\Support\Facades\Cache::class,
        'Config' => Illuminate\Support\Facades\Config::class,
        'Cookie' => Illuminate\Support\Facades\Cookie::class,
        'Crypt' => Illuminate\Support\Facades\Crypt::class,
        'DB' => Illuminate\Support\Facades\DB::class,
        'Eloquent' => Illuminate\Database\Eloquent\Model::class,
        'Event' => Illuminate\Support\Facades\Event::class,
        'File' => Illuminate\Support\Facades\File::class,
        'Gate' => Illuminate\Support\Facades\Gate::class,
        'Hash' => Illuminate\Support\Facades\Hash::class,
        'Helper'=> App\Helpers\Helper::class,
        'Lang' => Illuminate\Support\Facades\Lang::class,
        'Log' => Illuminate\Support\Facades\Log::class,
        'Mail' => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password' => Illuminate\Support\Facades\Password::class,
        'Queue' => Illuminate\Support\Facades\Queue::class,
        'Redirect' => Illuminate\Support\Facades\Redirect::class,
        'Redis' => Illuminate\Support\Facades\Redis::class,
        'Request' => Illuminate\Support\Facades\Request::class,
        'Response' => Illuminate\Support\Facades\Response::class,
        'Route' => Illuminate\Support\Facades\Route::class,
        'Schema' => Illuminate\Support\Facades\Schema::class,
        'Session' => Illuminate\Support\Facades\Session::class,
        'Storage' => Illuminate\Support\Facades\Storage::class,
        'URL' => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View' => Illuminate\Support\Facades\View::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'PDF' => Barryvdh\DomPDF\Facade::class,
        'Terbilang' => Riskihajar\Terbilang\Facades\Terbilang::class,
        'Menu' => Harimayco\Menu\Facades\Menu::class,
        'Image' => Intervention\Image\Facades\Image::class,
        'Socialite' => Laravel\Socialite\Facades\Socialite::class,
        'JWTAuth' => Tymon\JWTAuth\Facades\JWTAuth::class,
        'JWTFactory' => Tymon\JWTAuth\Facades\JWTFactory::class,

    ],

    'userid' => env('USERID'),
    'api_digisign' => env('API_DIGISIGN'),
    // 'content_type' => env('CONTENT_TYPE'),
    'boundary' => env('BOUNDARY'),
    'authorization' => env('AUTHORIZATION'),
    'token' => env('TOKEN'),
    'apilink' => env('APILINK'),
    'clientlink' => env('CLIENTLINK'),
    'link_digisign' => env('LINK_DIGISIGN'),

    'email_pak_taufiq' => env('EMAIL_PAK_TAUFIQ'),
    'email_no_reply' => env('MAIL_FROM_ADDRESS2'),

    'pefindo_user' => env('PEFINDO_USER'),
    'pefindo_password' => env('PEFINDO_PASSWORD'),
    'pefindo_wsdl' => env('PEFINDO_WSDL'),
    'pefindo_location' => env('PEFINDO_LOCATION'),
    'pefindo_uri' => env('PEFINDO_URI'),

    'bni_id' => env('BNIS_VA_CLIENT'),
    'bni_key' => env('BNIS_VA_KEY'),
    'bni_expire_minutes' => env('BNIS_VA_EXPIRY_MINUTES'),
    'bni_url' => env('BNIS_VA_API_URL'),

    'DMS_URL_SVC' => env('DMS_URL_SVC'),
    'DMS_USER' => env('DMS_USER'),
    'DMS_PASSWORD' => env('DMS_PASSWORD'),
    'DMS_PATH_BORROWER_CORPORATE_LOCAL' => env('DMS_PATH_BORROWER_CORPORATE_LOCAL'),
    'DMS_PATH_BORROWER_CORPORATE_FOREIGN' => env('DMS_PATH_BORROWER_CORPORATE_FOREIGN'),
    'DMS_PATH_BORROWER_PERSONAL_WNI' => env('DMS_PATH_BORROWER_PERSONAL_WNI', "/okm:root/DEV_AREA/BORROWER/PERSONAL/WNI/"),
    'DMS_PATH_BORROWER_PERSONAL_WNA' => env('DMS_PATH_BORROWER_PERSONAL_WNA'),
    'DMS_PATH_LENDER_CORPORATE_LOCAL' => env('DMS_PATH_LENDER_CORPORATE_LOCAL'),
    'DMS_PATH_LENDER_CORPORATE_FOREIGN' => env('DMS_PATH_LENDER_CORPORATE_FOREIGN'),
    'DMS_PATH_LENDER_PERSONAL_WNI' => env('DMS_PATH_LENDER_PERSONAL_WNI'),
    'DMS_PATH_LENDER_PERSONAL_WNA' => env('DMS_PATH_LENDER_PERSONAL_WNA'),

    'bnik_id' => env('BNIK_VA_CLIENT'),
    'bnik_key' => env('BNIK_VA_KEY'),
    'bnik_expire_minutes' => env('BNIK_VA_EXPIRY_MINUTES'),
    'bnik_url' => env('BNIK_VA_API_URL'),
    'bnik_main_account' => env('BNIK_MAIN_ACCOUNT'),

    'idPrivy' => env('IDPRIVY'),
    'isDev' => env('ISDEVPRIVY'),
    'privy_url' => env('PRIVY_URL'),
    'privy_auth' => env('PRIVY_AUTH'),
    'privy_client_id' => env('PRIVY_CLIENT_ID'),
    'privy_secret_key' => env('PRIVY_SECRET_KEY'),
    'privy_prod' => env('PRIVY_PROD'),
    'privy_merchent_key' => env('PRIVY_MERCHENT_KEY'),
    'privy_username' => env('PRIVY_USERNAME'),
    'privy_password' => env('PRIVY_PASSWORD'),
    'privy_enterprise_token' => env('PRIVY_ENTERPRISE_TOKEN'),

    // RDL
    'url_rdl' => env('URL_RDL'),
    'port_rdl' => env('PORT_RDL'),
    'company_id_rdl' => env('COMPANY_ID_RDL'),
    'username_rdl' => env('USERNAME_RDL'),
    'password_rdl' => env('PASSWORD_RDL'),
    'api_key_id_rdl' => env('API_KEY_ID_RDL'),
    'api_key_secret_rdl' => env('API_KEY_SECRET_RDL'),
    'api_key_outh_id_rdl' => env('API_KEY_OUTH_ID_RDL'),
    'api_key_outh_secret_rdl' => env('API_KEY_OUTH_SECRET_RDL'),

    // SFTP

    'sftp_host' => env('SFTP_HOST'),
    'sftp_driver' => env('SFTP_DRIVER'),
    'sftp_username' => env('SFTP_USERNAME'),
    'sftp_password' => env('SFTP_PASSWORD'),
    'sftp_account_username' => env('SFTP_ACCOUNT_USERNAME'),
    'sftp_account_password' => env('SFTP_ACCOUNT_PASSWORD'),
    'sftp_port' => env('SFTP_PORT'),
    'sftp_id' => env('SFTP_ID'),

    // Email Checker

    'api_email' => env('MAIL_CHECKER_KEY'),
    'jwt_ttl_dynamic' => env('JWT_TTL_DYNAMIC'),

    // payout CIMB
    'dsiEmailPayout' => env('DSIEMAILPAYOUT'),
    'dsiEmailPayoutCIMB' => env('DSIEMAILPAYOUT_CIMB'),

    // CREDO
    'credo_url' => env('CREDO_URL'),
    'credo_username' => env('CREDO_USERNAME'),
    'credo_password' => env('CREDO_PASSWORD'),
    'credo_auth_key' => env('CREDO_AUTH_KEY'),

    // PUSDAFIL
    'pusdafil_username' => env('PUSDAFIL_USERNAME'),
    'pusdafil_password' => env('PUSDAFIL_PASSWORD'),
    'pusdafil_key' => env('PUSDAFIL_KEY'),

    // CIMB
    'cimbs_id' => env('CIMBS_CODE_BANK'),
    'cimbs_id_comp_code' => env('CIMBS_ID_COMP_CODE'),
    'cimbs_lender_multiplication_amount' => env('CIMBS_LENDER_MULTIPLICATION_AMOUNT'),
    'cimbs_lender_min_amount' => env('CIMBS_LENDER_MIN_AMOUNT'),

    // BSI
    'bsi_id' => env('BSI_VA_CLIENT'),
    'bsi_key' => env('BSI_VA_KEY'),
    'bsi_url' => env('BSI_VA_API_URL'),
    'bsi_code_bank' => env('BSI_CODE_BANK'),

    // BOA
    'boa_api_clientid_1' => env('BOA_API_CLIENTID_1'),
    'boa_api_key_1' => env('BOA_API_KEY_1'),
    'boa_api_clientid_2' => env('BOA_API_CLIENTID_2'),
    'boa_api_key_2' => env('BOA_API_KEY_2'),

    // Contact Center
    'CC_API_KEY' => env('CC_API_KEY'),
    'CC_API_SECRET' => env('CC_API_SECRET'),
    'CC_BASE_URL' => env('CC_BASE_URL'),
    'CC_TOKEN_ID' => env('CC_TOKEN_ID'),
    'CC_TENANT_ID' => env('CC_TENANT_ID'),
    'CC_DEFAULT_CUSTGROUP' => env('CC_DEFAULT_CUSTGROUP'),

    'verijelas_url' => env('VERIJELAS_URL'),
    'verijelas_token' => env('VERIJELAS_TOKEN'),
    'verijelas_timeout'=> env('VERIJELAS_TIMEOUT'),

    //Email Verifikator (Occasio)
    'email_verifikator' => env('VERIFIKATOR_EMAIL'),

    //Dsi api core
    'dsi_api_core' => env('DSI_API_CORE'),
    'dsi_auth_core'=> env('DSI_AUTH_CORE'),

    //Qontak Webchat Code & ID
    'qontak_id' => env('QONTAK_ID'),
    'qontak_code'=> env('QONTAK_CODE')

];
