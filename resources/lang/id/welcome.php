<?php

return [

    /*
    |--------------------------------------------------------------------------
    | English Language Pages
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'welcome_01' => '#AyoHijrahFinansial',
    'welcome_02' => 'Bersama',
    'welcome_03' => 'DANA SYARIAH',
    'welcome_04' => 'Imbal Hasil Setara<br>Hingga <span style="font-style: italic; color: #27792F; font-weight: 700;">20%</span> Per Tahun',
    'welcome_05' => 'Terdaftar & Diawasi <span style="color: #7F2529; font-weight:600;">Otoritas Jasa Keuangan</span> <br> dan Didampingi oleh Dewan Pengawas Syariah (MUI) <br>',
    'welcome_06' => 'Unduh di',
    'TKB'        => 'Tingkat Keberhasilan <br> ( TKB ',
    'Total_investor'    => 'Total Pendana<br>Dana Syariah',
    'Total_pinjaman_a'  => 'Total Pendanaan<br>( Sejak Berdiri )',
    'Total_pinjaman_b'  => 'Total Pendanaan<br>( Tahun Berjalan )',
    'Total_outstanding' => 'Total Outstanding<br>Pendanaan',
    'Jumlah_borrower'   => 'Jumlah Penerima<br>(Institusi & Individu)',
    'Jumlah_borrower_a' => 'Jumlah Penerima Aktif<br>(Institusi & Individu)',

    'video_lihat'       => 'Lihat Video',
    'lebih_mendalam'    => 'Lebih Dalam Tentang Dana Syariah',

    'reg_title'=> 'Bersama Danasyariah.id. Mudah, Berkah dan Aman',
    'reg_desc'=> 'Nikmati kemudahan dalam layanan pendanaan secara Syariah. <br> Produktifkan asset mu dengan menjadi Pendana atau ajukan pembiayaan properti',
    'reg_inv'   => 'Berikan Pendanaan',
    'reg_borrower'   => 'Ajukan Pembiayaan',
    'desc_inv'  => 'Yuk menjadi <b>#PendanaHalal</b> untuk dapat menikmati <br> imbal hasil menarik dengan Mudah, Aman dan Halal. <br> <b>#AyoHijrahFinansial</b>',
    'desc_borrower'  => '<span>Nikmati akses pembiayaan properti sebagai berikut: <br></span><span class="ml-3" style="list-style:disc outside none;display:list-item;">Pembiayaan Konstruksi bagi Pengembang Properti.</span><span class="ml-3" style="list-style:disc outside none;display:list-item;">Pembiayaan Kepemilikan Hunian bagi Individu.</span><br>',
    'reg_donatur'   => 'Daftar Sebagai Donatur',
    'desc_donatur'   => 'Alhamdulillah, saat ini Danasyariah.id memiliki fitur donasi <b>#AyoCariBerkah | Presented by Danasyariah.id</b><br><br>Kemudahan berdonasi online diujung jari #AyoCariBerkah',

    'keunggulan_a'=> '8 Keunggulan,',
    'keunggulan_b'=> 'Pendanaan di Dana Syariah',
    'keunggulan_c'=> 'Manfaatkan Berbagai Keunggulan Menjadi Pendanaan di Dana Syariah',
    'keunggulan_1'=> 'Imbal Hasil Tinggi',
    'keunggulan_1_det'=> 'Setara hingga 15 - 20% setara per tahun',
    'keunggulan_2'=> 'Online & Mudah',
    'keunggulan_2_det'=> 'Semudah <br> belanja Online',
    'keunggulan_3'=> 'Tarik Dana&nbsp;',
    'keunggulan_3_det'=> 'Tanpa denda & penalti.<br>(Kami carikan Pendana pengganti)',
    'keunggulan_4'=> 'Legal & <br> Aman',
    'keunggulan_4_det'=> 'Berizin di OJK & ada Agunan Properti',
    'keunggulan_5'=> 'Pendanaan Syariah<br>',
    'keunggulan_5_det'=> 'Sesuai dengan prinsip syariah',
    'keunggulan_6'=> 'Imbal Hasil <br> Pasti & tetap',
    'keunggulan_6_det'=> 'Imbal hasil Jual-beli Properti',
    'keunggulan_7'=> 'Pendanaan<br>Terjangkau',
    'keunggulan_7_det'=> 'Mulai Rp 1 Juta dan Kelipatannya',
    'keunggulan_8'=> 'Terima Hasil<br>Tiap Bulan',
    'keunggulan_8_det'=> 'Otomatis tiap bulan di transfer ke rekening',

    'pendanaan_aktif'       => 'Pendanaan Aktif',
    'pendanaan_aktif_det'   => 'Berikan Pendanaanmu, dan Nikmati Imbal Hasil Terbaik',

    'varian'        => 'Pendanaan yang Tersedia',
    'varian_det'    => 'Kami menyediakan beberapa jenis pendanaan untuk didanai',

    'testimoni'     => 'Testimoni',
    'testimoni_det' => 'Beberapa testimoni pengguna Dana Syariah',
    'testimoni_1' => 'Ternyata sangat bagus, ini diluar perkiraan saya. Ini familiar, karena berbasis Islam atau Syariah. Selain menghasilkan yang produktif sambil beribadah',
    'testimoni_2' => 'Alhamdulillah saya senang, Semakin di kembangkan lagi informasinya, supaya umat mengetahui',
    'testimoni_3' => 'Proyeknya bisa dilihat langsung ke lokasi, Laporannya selalu ada, Saya bisa pantau sewaktu-waktu. Syari dan berizin di OJK',

    'news'      => 'Berita Terbaru',
    'news_desc' => 'Informasi terbaru dari Dana Syariah Indonesia',

];
