<?php

return [
    'tujuan_pendanaan' => 'Tujuan Pembiayaan',
    'harga_objek_pendanaan'=> 'Harga Objek Properti',
    'uang_muka' => 'Uang Muka',
    'nilai_pengajuan_dana'=> 'Nilai Pengajuan Pembiayaan',
    'jangka_waktu'=> 'Jangka Waktu',
    'title'=> '<b>Dana Rumah</b>',
    'subtitle_1'=>'<b>Dana Rumah</b> merupakan layanan Dana Syariah Indonesia, untuk alternatif pembiayaan kepemilikan rumah dengan skema syariah, yang mudah.',
    'subtitle_2'=>'<b>Pembiayaan Dana Rumah</b> bertujuan membantu masyarakat untuk mudah memiliki rumah sendiri, dengan skema Pembiayaan Kepemilikan Rumah  <b>Angsuran Ringan, Berjenjang</b> dan <b>Transparan</b> sesuai <b>Kaidah Syariah</b>, dengan tenor maksimal 15 Tahun.',
    'subtitle_3'=> '<b>Pembiayaan Pra Dana Rumah</b> : Adalah pembiayaan dana rumah khusus untuk konsumen yang belum mempunyai dana yang cukup untuk membayar uang muka (DP) Penuh, Sehingga Layanan ini memungkinkan untuk membantu pembayaran  uang muka (DP) diangsur maksimal 1 Tahun. ',
    'subtitle_4'=> 'Nikmati kesempatan hijrah dengan promo Margin mulai dari  <b>9,9%</b> ( Setara 1 Tahun ).',
    'subtitle_5'=> '<b>Angsuran Tetap</b> tiap bulan dengan komposisi <b><i>Angsuran Margin</i></b> lebih kecil dari pada <b><i>Angsuran Pokok</i></b>  dari Awal Angsuran sampai Angsuran Terakhir.',
    'subtitle_6'=> '<b>Ayo daftar</b> melalui apps Dana Syariah yang dapat didownload via <a href="https://play.google.com/store/apps/details?id=com.danasyariah.mobiledanasyariah">Android playstore</a> dan <a href="https://itunes.apple.com/US/app/id1461445952?mt=8">Apple App Store</a> dan juga melalui website <a href="https://www.danasyariah.id">danasyariah.id</a>.',
    'subtitle_7'=>'<b>Syarat Bergabung :</b><br>
                                1.&nbsp Warga negara Indonesia.<br>
                                2.&nbsp Usia minimal 21 tahun, maksimal 55 tahun pada waktu akhir pelunasan.<br>
                                3.&nbsp Untuk rumah baru (indent/ready stock), rumah bekas (second), take over.<br>',
    'subtitle_8'=>'<b>4 Alasan mudah punya rumah, dengan Dana Rumah</b><br>
                                1.&nbsp Skema akad syariah.<br>
                                2.&nbsp DP bisa 0% dan angsuran lebih ringan.<br>
                                3.&nbsp Terbuka untuk Wiraswasta dan Karyawan.<br>
                                4.&nbsp Daftar online, proses mudah dan cepat.<br>',
    'subtitle_9'=>'<b>Tertarik mengajukan pembiayaan kepemilikan rumah?</b><br>',
    'subtitle_10'=>'Ajukan segera melalui  mobile app  Dana Syariah Indonesia, yang dapat didownload melalui <a href="https://play.google.com/store/apps/details?id=com.danasyariah.mobiledanasyariah">Android playstore</a> dan <a href="https://itunes.apple.com/US/app/id1461445952?mt=8">Apple App Store</a> dan juga website <a href="https://www.danasyariah.id">danasyariah.id</a>.<br>',
    'kepemilikanrumah' => 'Kepemilikan Rumah adalah pinjaman pemilikan rumah dengan syarat telah/mampu melunasi uang muka',
    'pra_kepemilikanrumah'=> 'Pra Kepemilikan Rumah adalah pinjaman uang muka untuk pemilikan rumah yang dapat dicicil maksimal selama 12 bulan',
    'loading_hitung_kpr'=> 'Mohon tunggu ...',
    'btn_hitung_kpr' => 'Hitung',
    'persentase_margin' => 'Persentase Margin'
];