<?php

return [
    'tujuan_pendanaan' => 'Tujuan Pendanaan',
    'harga_objek_pendanaan'=> 'Harga Objek Pendanaan',
    'uang_muka' => 'Uang Muka',
    'nilai_pengajuan_dana'=> 'Nilai Pengajuan Pendanaan',
    'jangka_waktu'=> 'Jangka Waktu',
    'title'=> 'Simulasi Angsuran Dana Rumah',
    'subtitle_1'=>'<b>Dana Rumah</b> adalah produk layanan PT Dana Syariah Indonesia, dimana tujuan untuk membantu masyarakat memenuhi kebutuhan rumah.',
    'subtitle_2'=>'<b>Pembiayaan Dana Rumah</b> bertujuan membantu masyarakat untuk mudah memiliki rumah sendiri, dengan skema Pembiayaan Kepemilikan Rumah  <b>Angsuran Ringan, Berjenjang</b> dan <b>Transparan</b> sesuai <b>Kaidah Syariah</b>, dengan tenor maksimal 15 Tahun.',
    'subtitle_3'=> '<b>Pembiayaan Pra Dana Rumah</b> : Adalah pembiayaan dana rumah khusus untuk konsumen yang belum mempunyai dana yang cukup untuk membayar uang muka (DP) Penuh, Sehingga Layanan ini memungkinkan untuk membantu pembayaran  uang muka (DP) diangsur maksimal 1 Tahun. ',
    'subtitle_4'=> 'Nikmati kesempatan hijrah dengan promo Margin mulai dari  <b>9,9%</b> ( Setara 1 Tahun ).',
    'subtitle_5'=> '<b>Angsuran Tetap</b> tiap bulan dengan komposisi <b><i>Angsuran Margin</i></b> lebih kecil dari pada <b><i>Angsuran Pokok</i></b>  dari Awal Angsuran sampai Angsuran Terakhir.',
    'kepemilikanrumah' => 'Kepemilikan Rumah adalah pinjaman pemilikan rumah dengan syarat telah/mampu melunasi uang muka',
    'pra_kepemilikanrumah'=> 'Pra Kepemilikan Rumah adalah pinjaman uang muka untuk pemilikan rumah yang dapat dicicil maksimal selama 12 bulan',
    'loading_hitung_kpr'=> 'Mohon tunggu ...',
    'btn_hitung_kpr' => 'Hitung',
    'persentase_margin' => 'Persentase Margin'

];