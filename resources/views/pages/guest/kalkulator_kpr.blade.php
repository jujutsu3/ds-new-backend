@extends('layouts.guest.master')

@section('navbar')
@include('includes.navbar')
@endsection
@section('style')
<style>
    #tenor_pra_wrapper>a>span.selectBox-label {
        width: 100px !important;
    }

    .selectBox-dropdown .selectBox-arrow {
        background: url('../img/selectBox-arrow.gif') no-repeat .3rem;
    }

    .tujuanPendanaanSelect,
    .tenorSelect {
        width: 100% !important;
    }

    .scroll {
        overflow-y: scroll;
        height: 500px;
        display: block;
    }
</style>
@endsection
@section('body')
<div class="title-section" style="padding-top: 100px;">
    <div class="container">
        <div class="row wow fadeInUp delay-04s">
            <div class="col-12">
                <div class="row">
                    <div class="detail col-12">
                        <h2 class="text-left heading">
                            @lang('kalkulator_kpr.title')
                        </h2>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8 text-left">
                        <p>@lang('kalkulator_kpr.subtitle_1')</p>
                        <br>
                        <p>@lang('kalkulator_kpr.subtitle_8')</p>
                        <br>
                        <p>@lang('kalkulator_kpr.subtitle_7')</p>
                    </div>
                    <div class="col-4 text-right" >
                        <img alt="dana syariah indonesia" class="start-png-home parallax d-none d-xl-block d-lg-block pl-10"  src="/img/kalkulator_image.png">
                    </div>
                    <div class="col-8 text-left">
                        <br>
                        <p>@lang('kalkulator_kpr.subtitle_9')</p>
                        <p>@lang('kalkulator_kpr.subtitle_10')</p>
                    </div>
                    <div class="col-4 text-right">
                        <br>
                        <br>
                            <a href="https://play.google.com/store/apps/details?id=com.danasyariah.mobiledanasyariah" class="parallax  mb-3 ml-auto lozad" data-speed-x="20" data-speed-y="4"><img class="img-fluid mt-2" src="{{ asset('img/ic-google-play-png-logo.png') }}" width="138" alt=""></a>
                            <span width="50"></span>
                            <a href="https://itunes.apple.com/US/app/id1461445952?mt=8"
                                class="mb-3  mr-4 parallax wowc fadeInUp delay-04s ml-auto" data-speed-x="10" data-speed-y="4">
                                <img class="img-fluid mt-2 lozad" src="{{ asset('img/ic-app-store-png-logo.png') }}" width="150" height="30" alt="download mobile app store">
                            </a>
                    </div>

                </div>
            </div>


        </div>
    </div>
</div>
<div class="row"><p><br></p></div>
<div class="kalkulator-kpr bg-dsi-gradient-flip" style="padding-top: 10px !important;font-size: 14px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12 s-brd-1 wow fadeInUp delay-04s">
                <div class="services-info-5 text-left" style="background: #dfe6e9 !important;border-radius: 0px !important;">
                    <div class="col">
                        {{-- <h6><i style="color: #4e85b2; font-size:1rem;" class="fas fa-calculator pt-2"> </i> Hitung KPR
                            </h6> --}}

                        <div class="col-sm-12 mb-3">
                            <div class="form-group">
                                <label for="file" id="step-manual-4"> @lang('kalkulator_kpr.tujuan_pendanaan') <span style="color: red;">* </span></label>
                                <select name="tujuan_pendanaan" class="form-control custom-select tujuanPendanaanSelect mb-2" id="tujuan_pendanaan" disabled>
                                    <!--<option value="">Pilih</option>-->
                                    <option value="1">Kepemilikan Rumah</option>

                                </select>
                                <span id="kepemilikanRumah" class="d-none" style="font-size: 10px;">@lang('kalkulator_kpr.kepemilikanrumah')</span>
                                <span id="praKepemilikanRumah" class="d-none" style="font-size: 10px;">@lang('kalkulator_kpr.pra_kepemilikanrumah')</span>
                            </div>

                        </div>


                        <div class="col-sm-12 mb-2">
                            <div class="form-group">
                                <label for="file" id="step-manual-6"> @lang('kalkulator_kpr.harga_objek_pendanaan')
                                    <span style="color: red;">* </span>
                                </label>
                                <input type="text" name="harga_objek_pendanaan" id="harga_objek_pendanaan" class="form-control  numberOnly no-zero form-control-sm" />
                            </div>
                        </div>


                        <div class="col-sm-12 mb-2" id="wrapperUangMuka">
                            <div class="form-group">
                                <label for="uang_muka">@lang('kalkulator_kpr.uang_muka') <span style="color: red;">*
                                    </span></label>
                                <input type="text" name="uang_muka" id="uang_muka" class="form-control numberOnly form-control-sm" />
                            </div>
                        </div>

                        <div class="col-sm-12 mb-2">
                            <div class="form-group">
                                <label for="nilai_pengajuan">@lang('kalkulator_kpr.nilai_pengajuan_dana') <span style="color: red;">* </span></label>
                                <input type="text" name="nilai_pengajuan" id="nilai_pengajuan" class="form-control numberOnly no-zero  form-control-sm" readonly />
                            </div>
                        </div>


                        <div class="col-sm-12 mb-4">
                            <div class="form-group">
                                <label for="tenor">@lang('kalkulator_kpr.jangka_waktu') <span style="color: red;">*
                                    </span></label>
                                <div id="tenor_wrapper">
                                    <select name="tenor" class="form-control custom-select tenorSelect" id="tenor">
                                        <option value="">Pilih</option>
                                        <option value="1">1 Tahun</option>
                                        <option value="2">2 Tahun</option>
                                        <option value="3">3 Tahun</option>
                                        <option value="4">4 Tahun</option>
                                        <option value="5">5 Tahun</option>
                                        <option value="6">6 Tahun</option>
                                        <option value="7">7 Tahun</option>
                                        <option value="8">8 Tahun</option>
                                        <option value="9">9 Tahun</option>
                                        <option value="10">10 Tahun</option>
                                        <option value="11">11 Tahun</option>
                                        <option value="12">12 Tahun</option>
                                        <option value="13">13 Tahun</option>
                                        <option value="14">14 Tahun</option>
                                        <option value="15">15 Tahun</option>
                                    </select>
                                </div>

                                <div id="tenor_pra_wrapper" class="d-none">
                                    <select name="tenor_pra" class="form-control  custom-select" id="tenor_pra">
                                        <option value="">Pilih</option>
                                        <option value="1">1 Bulan</option>
                                        <option value="2">2 Bulan</option>
                                        <option value="3">3 Bulan</option>
                                        <option value="4">4 Bulan</option>
                                        <option value="5">5 Bulan</option>
                                        <option value="6">6 Bulan</option>
                                        <option value="7">7 Bulan</option>
                                        <option value="8">8 Bulan</option>
                                        <option value="9">9 Bulan</option>
                                        <option value="10">10 Bulan</option>
                                        <option value="11">11 Bulan</option>
                                        <option value="12">12 Bulan</option>
                                    </select>

                                </div>


                            </div>
                        </div>

                        <div class="col-sm-12 mb-4">
                            <div class="form-group">
                                <label for="tenor">@lang('kalkulator_kpr.persentase_margin') <span style="color: red;">*
                                    </span></label>
                                <div id="margin_wrapper">
                                    <select name="margin" class="form-control custom-select tenorSelect" id="margin">
                                        <option value="">Pilih Margin</option>
                                        <option value="14">14%</option>
                                        <option value="16">16%</option>
        
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="alert_wrapper"></div>
                        <div class="col-sm-12">
                            <button type="button" class="btn text-white btn-block" id="btnHitungSimulasiKpr" style="background-color: #057758;">
                                @lang('kalkulator_kpr.btn_hitung_kpr')
                            </button>

                            <button type="button" class="btn text-white btn-block d-none" id="btnLoadingHitung" style="background-color: #057758;" disabled>
                                @lang('kalkulator_kpr.loading_hitung_kpr')
                            </button>

                        </div>

                    </div>
                </div>

            </div>

            <div class="col-lg-8 col-md-6 col-sm-12 s-brd-1 wow fadeInUp delay-04s">
                <div class="services-info-5 text-left">
                    <div class="col">
                        <h6><i style="color: #4e85b2; font-size:1rem;" class="fas fa-calculator pt-2"> </i> Detail
                            Angsuran
                        </h6>
                        <table class="table table-striped table-bordered" id="dataResultSimulasi">

                        </table>
                        <span id="disclamer" class="text-justify d-none" style="font-size: 11px;">
                        Catatan :<br>
                        <ol style="margin-left: 10px">
                            <li>Angka-angka terlampir hanya simulasi, sesuai dengan asumsi margin terlampir, nilai sebenarnya akan tercantum dalam kontrak setelah disetujui.</li>
                            <li>Secara umum angka-angka terlampir belum termasuk biaya-biaya transaksi seperti pajak, biaya notaris, dll. Jika diinginkan biaya-biaya dalam transaksi tersebut, dapat juga disertakan ke dalam perhitungan angsuran.</li>
                            <li>Nilai margin terkini yang berlaku, dapat dilihat pada kalkulator simulasi di website www.danasyariah.id atau melalui Apps Dana Syariah Indonesia.</li>
                        <ol>
                        <!-- Perhitungan diatas merupakan simulasi awal dalam pengajuan permohonan Dana Rumah anda, dan bukan menjadi acuan dalam persetujuan pendanaan yang akan anda terima. -->
                        </span>
                    </div>
                </div>


            </div>


        </div>


    </div>

</div>


@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function() {


        var alert_wrapper = document.getElementById("alert_wrapper");
        const formatRupiah = (angka, prefix) => {

            if (angka.charAt(0) == '0') {
                return '0';
            }

            let number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }

        function show_alert(message, alert) {
            alert_wrapper.innerHTML = `
                    <div id="alert" class="alert alert-${alert} alert-dismissible fade show mt-2" role="alert">
                        <span>${message}</span>
                        <button type="button" class="close" style="float:right; !imporant;" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`;
        }

        function set_nilai_pengajuan() {
            var harga_pendanaan = $("#harga_objek_pendanaan").val();
            var uang_muka = $("#uang_muka").val();

            let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
            let int_uang_muka = parseInt(uang_muka.replaceAll('.', ''))

            let total_pendanaan = int_harga_pendanaan - int_uang_muka
            $("#nilai_pengajuan").val(formatRupiah(total_pendanaan.toString()));

            if (int_uang_muka > int_harga_pendanaan || !int_harga_pendanaan) {
                swal.fire({
                    title: "Info",
                    text: "Uang muka tidak boleh lebih dari harga objek pendanaan",
                    type: "info",
                }).then(() => {
                    $("#uang_muka").val('')
                    $('#nilai_pengajuan').val('')

                })
            }

        }


        $('#tujuan_pendanaan').on('change', function() {

            if ($(this).val() == '1') {
                $("#kepemilikanRumah").removeClass("d-none");
                $("#praKepemilikanRumah").addClass("d-none");

                $("#tenor_wrapper").removeClass("d-none");
                $("#tenor_pra_wrapper").addClass("d-none");

                $("#uang_muka").attr("disabled", false);
                $("#nilai_pengajuan").val("").attr({
                    "readonly": true,
                    required: false
                });

                $("#harga_objek_pendanaan").val("");
                $("#uang_muka").val("");
                $("#wrapperUangMuka").removeClass("d-none");
                $("#dataResultSimulasi").html("");

                $("#alert").hide();
                $('#kepemilikanRumah').addClass("d-none");


            } else {
                $("#praKepemilikanRumah").removeClass("d-none");
                $("#kepemilikanRumah").addClass("d-none");

                $("#tenor_wrapper").addClass("d-none");
                $("#tenor_pra_wrapper").removeClass("d-none");

                $("#uang_muka").attr("disabled", true).val('');
                $("#wrapperUangMuka").addClass("d-none");

                $("#nilai_pengajuan").val("").attr({
                    "readonly": false,
                    required: true
                });

                $("#harga_objek_pendanaan").val("");
                $("#uang_muka").val("");
                $("#dataResultSimulasi").html("");
                $("#alert").hide();
            }
        });

        $('#harga_objek_pendanaan, #nilai_pengajuan').keyup(function() {
            $(this).val(formatRupiah($(this).val()));
        });


        $("#harga_objek_pendanaan").blur(function() {

            if ($(this).val() != '') {

                var harga_pendanaan_rumah = $(this).val().replaceAll('.', '');
                if (harga_pendanaan_rumah < 100000000) {
                    swal.fire({
                        title: "Info",
                        text: "Harga Objek Pendanaan Minimal Rp 100.000.000",
                        type: "info",
                    }).then(() => {
                        $('#harga_objek_pendanaan').val('')
                    })
                }
                set_nilai_pengajuan();

            }



        })

        $("#nilai_pengajuan").keyup(function() {

            if ($(this).val() != '' && $("#harga_objek_pendanaan").val() != '') {

                var harga_pendanaan = $("#harga_objek_pendanaan").val();
                let nilai_pengajuan = $(this).val()

                let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
                let int_nilai_pengajuan = parseInt(nilai_pengajuan.replaceAll('.', ''))

                if (int_nilai_pengajuan > int_harga_pendanaan || !int_harga_pendanaan) {
                    swal.fire({
                        title: "Info",
                        text: "Nilai Pengajuan tidak boleh lebih dari harga objek pendanaan",
                        type: "info",
                    }).then(() => {
                        $(this).val('0')
                        $('#nilai_pengajuan').val('')
                    })
                }

            }

        })

        $('#uang_muka').keyup(function() {
            var tujuan = $("#tujuan_pendanaan option:selected").val();
            $(this).val(formatRupiah($(this).val()));
            if (tujuan != 2) {

                if ($(this).val() == '0') {
                    $('#kepemilikanRumah').addClass("d-none");
                } else {
                    $('#kepemilikanRumah').removeClass("d-none");
                }
                set_nilai_pengajuan();

            }

        });


        $(".numberOnly").on("input", function() {
            this.value = this.value.replace(/[^0-9]/g, "");
        });

        $(".no-zero").on("input", function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });


        function hitungSimulasi() {

            let nilai_pengajuan = $("#nilai_pengajuan").val().replaceAll('.', '');
            let tipe_pendanaan = $("#tujuan_pendanaan option:selected").val();
            $.ajax({
                url: "{{ route('simulasi.post.kpr') }}",
                type: "post",
                dataType: "html",
                data: {
                    _token: "{{ csrf_token() }}",
                    nilai_pengajuan: nilai_pengajuan,
                    tipe_pendanaan: tipe_pendanaan,
                    tenor: ($("#tujuan_pendanaan option:selected").val() == '1') ? $("#tenor").val() : $("#tenor_pra").val(),
                    margin: $("#margin").val()
                },
                error: function(xhr, status, error) {

                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    $("#btnHitungSimulasiKpr").removeClass("d-none");

                    // Show the loading button
                    $("#btnLoadingHitung").addClass("d-none");

                    $("#disclamer").addClass("d-none");
                    $(".table").removeClass("scroll");

                    if (xhr.status === 419) {
                        show_alert("Page expired. please refresh the page", "danger");
                    }


                },
                success: function(result) {


                    $("#dataResultSimulasi").html(result);
                    // Hide the hitung button
                    $("#btnHitungSimulasiKpr").removeClass("d-none");

                    // Show the loading button
                    $("#btnLoadingHitung").addClass("d-none");

                    $("#disclamer").removeClass("d-none");
                    $(".table").addClass("scroll");

                    show_alert("Perhitungan selesai. Lihat Hasil Pada Tabel Detail Angsuran ",
                        "success");
                }
            });


        }

        $("#btnHitungSimulasiKpr").on('click', function(event) {


            let tujuan_pendanaan = $("#tujuan_pendanaan").val();
            let harga_objek_pendanaan = $("#harga_objek_pendanaan").val();
            let uang_muka = $("#uang_muka").val();
            let nilai_pengajuan = $("#nilai_pengajuan").val();
            let tenor = (tujuan_pendanaan == '1') ? $("#tenor").val() : $("#tenor_pra").val();
            let margin = $("#margin").val();

            if (!tujuan_pendanaan) {
                show_alert("Tujuan Pendanaan harus pilih", "warning");
                return;
            }

            if (!harga_objek_pendanaan) {
                show_alert("Harga Objek Pendanaan harus diisi", "warning");
                return;
            }


            if (tujuan_pendanaan == '1') {
                if (!uang_muka) {
                    show_alert("Uang Muka harus diisi", "warning");
                    return;
                }

            }

            if (!harga_objek_pendanaan) {
                show_alert("Harga objek pendanaan harus diisi", "warning");
                return;
            }


            if (!nilai_pengajuan) {
                show_alert("Nilai Pengajuan Pendanaan harus diisi", "warning");
                return;
            }


            if (!tenor) {
                show_alert("Jangka Waktu harus dipilih", "warning");
                return;
            }

            if (!margin) {
                show_alert("Persentase Margin Belum dipilih", "warning");
                return;
            }


            //Kepemilikan Rumah
            if (tujuan_pendanaan == '1') {

                let cek_int_harga_pendanaan = $("#harga_objek_pendanaan").val().replaceAll('.', '');
                let cek_int_uang_muka = $("#uang_muka").val().replaceAll('.', '');

                let cek_total_pendanaan = cek_int_harga_pendanaan - cek_int_uang_muka;


                if (cek_total_pendanaan < 100000000) {

                    swal.fire({
                        title: "Info",
                        text: "Nilai Pengajuan Pendanaan Minimal Rp 100.000.000",
                        type: "info",
                    }).then(() => {
                        $('#nilai_pengajuan').val('')
                        $('#uang_muka').val('')

                    })

                    $("#dataResultSimulasi").html("");
                    $("#disclamer").addClass("d-none");
                    $(".table").removeClass("scroll");

                    return;
                }



                if (cek_total_pendanaan > 2000000000) {
                    swal.fire({
                        title: "Info",
                        text: "Nilai Pengajuan Pendanaan Maksimal Rp 2.000.000.000",
                        type: "info",
                    }).then(() => {
                        $('#nilai_pengajuan').val('')
                        $('#uang_muka').val('')
                    })

                    $("#dataResultSimulasi").html("");

                    return;
                }

            }


            if (tujuan_pendanaan == '2') {
                let cek_int_nilai_pengajuan = $("#nilai_pengajuan").val().replaceAll('.', '');
                if (cek_int_nilai_pengajuan < 20000000) {
                    swal.fire({
                        title: "Info",
                        text: "Nilai Pengajuan Pendanaan Minimal Rp 20.000.000",
                        type: "info",
                    }).then(() => {
                        $('#nilai_pengajuan').val('')
                    })

                    $("#dataResultSimulasi").html("");
                    $("#disclamer").addClass("d-none");
                    $(".table").removeClass("scroll");

                    return;
                }

                if (cek_int_nilai_pengajuan > 2000000000) {
                    swal.fire({
                        title: "Info",
                        text: "Nilai Pengajuan Pendanaan Maksimal Rp 2.000.000.000",
                        type: "info",
                    }).then(() => {
                        $('#nilai_pengajuan').val('')
                    })

                    $("#dataResultSimulasi").html("");
                    $("#disclamer").addClass("d-none");
                    $(".table").removeClass("scroll");

                    return;
                }
            }


            // Hide the hitung button
            $("#btnHitungSimulasiKpr").addClass("d-none");

            // Show the loading button
            $("#btnLoadingHitung").removeClass("d-none");
            hitungSimulasi();


        });

    });
</script>
@endsection