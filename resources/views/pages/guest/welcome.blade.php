@extends('layouts.guest.masterhome')
@section('style')
<style>
    /*
    @font-face {
        font-family: 'Lingua_TRIAL';
        src: url('{{asset("admin/assets/fonts/Lingua_TRIAL.ttf")}}');
        font-display: fallback;
    }
    @font-face {
        font-family: 'Montserrat-Black';
        src: url('{{asset("admin/assets/fonts/Montserrat-Black.ttf")}}');
        font-display: fallback;
    }
    @font-face {
        font-family: 'Montserrat-Bold';
        src: url('{{asset("admin/assets/fonts/Montserrat-Bold.ttf")}}');
        font-display: fallback;
    }*/
    body {overflow-x: hidden;}
    /*
    .font-ligua-trial {font-family: 'Lingua_TRIAL', sans-serif !important;}
    .font-montserrat {font-family: 'Montserrat-Black', sans-serif !important;}
    .font-montserrat-bold {font-family: 'Montserrat-Bold', sans-serif !important;}
    */

    .table td {padding: .8em;padding-bottom: .6em;}
    .lead {font-size: 1rem;font-weight: bold;}
    .notify-badge {z-index: 900;width: 20% !important;height: 140px;width: 100px;}
    .flaticon {color: black;}
    .bg-css {color: white;}
    .hover-card {background-color: white;border-radius: 10px;box-shadow: 0 0 5px rgba(0, 0, 0, .1);}
    .hover-card:hover {box-shadow: 0 0 8px 0 rgba(0, 0, 0, .3);transition: all 0.4s;}
    .delapan-image {width:35%!important; height:60%; display: inline-block!important; opacity:1;}
    .feedback-slider-item {display: flex;min-height:500px;align-items:center;}
    .feedback-slider-item .image-testimoni{position: absolute; bottom: 25px;text-align:center}
    .main-thread {font-size: .8em; line-height: 1.5em; font-weight: 400;}

    /******************  News Slider Demo-7 *******************/
/*
    .post-slide {
        padding-right: 20px;
        display: inline-block;
    }

    .post-slide img {
        width: 100%;
        height: auto;
    }

    .post-slide .post-review {}

    .post-slide .post-date {
        float: left;
        margin-right: 10px;
        padding: 8px 15px;
        text-align: center;
        background: #444;
        font-size: 26px;
        color: #fff;
        font-weight: 700;
        transition: background 0.20s linear 0s;
    }

    .post-slide:hover .post-date {
        background: #078071;
    }

    .post-slide .post-date small {
        display: block;
        margin-bottom: 10px;
        font-size: 13px;
        text-transform: capitalize;
    }

    .post-slide .post-date small:before {
        content: "";
        display: block;
        margin-bottom: 5px;
        border-top: 1px solid #fff;
    }

    .post-slide .post-title {
        margin: 0;
        padding-top: 15px;
    }

    .post-slide .post-title a {
        font-size: 15px;
        color: #444;
        text-transform: uppercase;
        margin-bottom: 6px;
        display: block;
        line-height: 20px;
        font-weight: bold;
    }

    .post-slide:hover .post-title a {
        color: #078071;
        text-decoration: none;
    }

    .post-comment {
        margin: 0;
        list-style: none;
    }

    .post-comment li a {
        color: #a9a9a9;
        text-transform: capitalize;
    }

    .post-comment li a:before {
        content: "|";
        margin: 0 5px 0 5px;
        color: #d3d3d3;
    }
*/
    @media only screen and (max-width: 480px) {
        .post-slide {
            padding: 0;
        }

        .modal-dialog {
            width: 100% !important;
        }
    }

    @media only screen and (device-width: 768px) {

        /* For general iPad layouts */
        .col-md-4 {
            max-width: 304px;
        }

        .modal-dialog {
            width: 100% !important;
        }
    }

    .modal-dialog {
        max-width: 800px;
        margin: 30px auto;
    }

    .modal-body {
        position: relative;
        padding: 0px;
    }

    .close {position: absolute;right: -30px;top: 0;z-index: 999;font-size: 2rem;font-weight: normal;color: #fff;opacity: 1;}
    .bold {font-weight: bolder;}
    .title-with-line {margin-left: -5px;background-image: -webkit-linear-gradient(bottom, #ffc100 50%, transparent 50%);}

</style>

@endsection
<!--{{-- @if (Carbon\Carbon::now()->toDateString() >= Carbon\Carbon::parse(29-12-2019))
@else
  @include('includes.newversion')    
@endif --}}
-->
@section('navbar')
@include('includes.navbar')
@endsection
@section('body')
{{-- =====================================23 JUNI 2020 ( untuk pengumuman nasabah )================================================ --}}
<!-- hero end -->
<!-- hero us start -->

    <div class="about-us content-area-2 banner-style-one lozad" data-speed-x="30" data-speed-y="40">
        <!-- <img  alt="dana syariah indonesia" class="lozad" data-src="img/wave-static-02.svg" class="w-100 position-absolute ts-bottom__0"> -->
        <div>
            <div id="parallax-container" class="container  pb-5 banner-style-two pt-5-onMobile">
                <div class="row pt-5  pt-5-onMobile" data-speed-x="10" data-speed-y="10">
                    <div class="col-lg-8 col-xs-12 align-self-center pt-5 pt-5-onMobile order-1">
                        <div class="about-text pt-2">
                            <h2 class="heading-slide pt-1">
                                <span class="text-pagar" style="line-height: .2em;">
                                    <span style="font-size: .4em; color:#02775B; font-style: italic;">
                                        @lang('welcome.welcome_01')</span>
                                    <span class="text-pagar text-dark font-weight-bold" style="font-size: .5em; line-height: 1em; margin-left: -5px; font-weight: 400;">
                                        Cara Mudah dan Cepat</span>
                                    <span style="font-size: .5em; line-height: 1.5em;">
                                        <span class="text-dark font-weight-bold" style="margin-left: -5px;">
                                            <span class="title-with-line px-1 d-inline">
                                                Mendanai atau Mengajukan</span>
                                                <br> Pembiayaan Properti
                                        </span>
                                    </span>
                                    <br>
                                </span>
                                <br>
                            </h2>
                            <div class="row align-items-end">
                                <div class="col-12">
                                    <p class="main-thread pb-3">
                                        <div class="row mt-4">
                                            <div class="col text-left">
                                                <span class="font-weight-bold px-1 title-with-line d-inline">
                                                    Berikan Pendanaan</span>
                                                <br>
                                                <span>Nikmati <b>#PendanaanHalal</b> <br> dengan imbal hasil menarik. <b><br>Pendanaan mulai dari Rp
                                                    1 Juta</b></span>
                                            </div>
                                            <div class="col text-left">
                                                <span class="font-weight-bold">Pembiayaan Konstruksi 
                                                    <br>
                                                    <span class="title-with-line px-1 d-inline">
                                                        atau Kepemilikan Rumah</span>
                                                </span>
                                                <br>
                                                <span>Solusi akses pembiayaan konstruksi untuk pengembang properti dan kepemilikan hunian bagi individu</span>
                                            </div>
                                        </div>
                                    </p>
                                </div>
                            </div>
                            @if (Auth::guard('borrower')->check())
                            @elseif (Auth::check('user'))
                            @else
                            <div class="row">
                                <div class="col-md-6">
                                    <button id="cta-reg" type="button" class="btn btn-success px-4">
                                        <span class="h4 text-white my-0 mx-4">Selengkapnya</span>
                                    </button>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 pt-5 mt-2 order-sm-3 mx-auto">
                        <div id="container">
                            <figure>
                            <picture id="imageHeader">
                                <source scrset="/img/16092021---Hero-Header-DSI.png" type="image/png" media="(min-width:992px)">
                                <source scrset="/img/16092021---Hero-Header-DSI.webp" type="image/webp" media="(min-width:1200px)">
                                <img alt="dana syariah indonesia"
                                class="start-png-home parallax d-none d-xl-block d-lg-block pl-10 lozad"
                                src="/img/16092021---Hero-Header-DSI-min.png" style="width:105%; height:81%" data-speed-x="60" data-speed-y="44">
                            </picture>
                            </figure>
                        </div>
                    </div>
                </div>
                <div style="padding-top: 5rem;">
                    <h2>&nbsp;</h2>
                    <h3>&nbsp;</h3>
                    <div class="container mb-3 wow fadeInDown delay-01s lozad">
                        <div class="delapan-keunggulan pt-2 slick-slider">
                            <div class="slide-wrap slick-slider">
                                <div class="card image parallax mx-auto">
                                    <div class="counter-box featured-center">
                                        <picture>
                                            <source scrset="{{ url('/img/investorRocket.png') }}" type="image/png" media="(min-width:992px)">
                                            <img class="lozad delapan-image pt-5" src="{{ url('/img/investorRocket.png') }}" alt="Danasyariah">
                                        </picture>
                                        <h4 class="pt-4">{{$tkb}} %</h4>
                                        @php
                                        $dt= Carbon\Carbon::now()->format('m');
                                        if ($dt == 1) {
                                        $Bulan = "Januari";
                                        } elseif ($dt == 2) {
                                        $Bulan ="Februari";
                                        } elseif ($dt == 3) {
                                        $Bulan ="Maret";
                                        } elseif ($dt == 4) {
                                        $Bulan ="April";
                                        } elseif ($dt == 5) {
                                        $Bulan ="Mei";
                                        } elseif ($dt == 6) {
                                        $Bulan ="Juni";
                                        } elseif ($dt == 7) {
                                        $Bulan ="Juli";
                                        } elseif ($dt == 8) {
                                        $Bulan ="Agustus";
                                        } elseif ($dt == 9) {
                                        $Bulan ="September";
                                        } elseif ($dt == 10) {
                                        $Bulan ="Oktober";
                                        } elseif ($dt == 11) {
                                        $Bulan ="November";
                                        } elseif ($dt == 12) {
                                        $Bulan ="Desember";
                                        } else {$Bulan = "";}
                                        @endphp
                                        <h5>@lang('welcome.TKB') {{ $Bulan.' '.\Carbon\Carbon::now()->format('Y') }} )
                                        </h5><!-- format indo -->
                                    </div>
                                </div>
                            </div>
                            <div class="slide-wrap slick-slider">
                                <div class="card image parallax mx-auto">
                                    <div class="counter-box featured-center">
                                        <picture>
                                            <source scrset="{{ url('/img/agenMitra.png') }}" type="image/png" media="(min-width:992px)">
                                            <img class="lozad delapan-image pt-5"
                                            src="{{ url('/img/agenMitra.png') }}"
                                            alt="Danasyariah">
                                        </picture>
                                        <h4 class="counter pt-4">{{ $jumlah_investor }}</h4>
                                        <h5>@lang('welcome.Total_investor')</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-wrap slick-slider">
                                <div class="card image parallax mx-auto">
                                    <div class="counter-box featured-center">
                                        <picture>
                                            <source scrset="{{ url('/img/pendanaanSelesai.png') }}" type="image/png" media="(min-width:992px)">
                                            <img class="lozad delapan-image pt-5"
                                            data-src="{{ url('/img/pendanaanSelesai.png') }}"
                                            alt="Danasyariah">
                                        </picture>
                                        <h4 class="pt-4">
                                            Rp.
                                            @php
                                            $number = $dana_pinjaman;
                                            if ($number < 1000000) { $format=number_format($number / 1000, 2,',','.')
                                                . ' Ribu' ; } else if ($number < 1000000000) {
                                                $format=number_format($number / 1000000, 2,',','.') . ' JT' ; } else if
                                                ($number < 1000000000000) { $format=number_format($number / 1000000000,
                                                2,',','.') . ' M' ; } else { $format=number_format($number /
                                                1000000000000, 2,',','.') . ' T' ; } echo $format @endphp </h4>
                                        <h5>@lang('welcome.Total_pinjaman_a')</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-wrap slick-slider">
                                <div class="card image parallax mx-auto">
                                    <div class="counter-box featured-center">
                                        <picture>
                                            <source scrset="{{ url('/img/pendanaanAktif.png') }}" type="image/png" media="(min-width:992px)">
                                            <img class="lozad delapan-image pt-5" data-src="{{ url('/img/pendanaanAktif.png') }}" alt="Danasyariah">
                                        </picture>
                                        <h4 class="pt-4">
                                            Rp.
                                            @php
                                            $number = $dana_pinjaman_2;
                                            if ($number < 1000000) { $format=number_format($number / 1000, 2,',','.')
                                                . ' Ribu' ; } else if ($number < 1000000000) {
                                                $format=number_format($number / 1000000, 2,',','.') . ' JT' ; } else if
                                                ($number < 1000000000000) { $format=number_format($number / 1000000000,
                                                2,',','.') . ' M' ; } else { $format=number_format($number /
                                                1000000000000, 2,',','.') . ' T' ; } echo $format @endphp </h4> <h5>
                                                @lang('welcome.Total_pinjaman_b')</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-wrap slick-slider">
                                <div class="card image parallax mx-auto">
                                    <div class="counter-box  featured-center">
                                        <picture>
                                            <source scrset="{{ url('/img/borrowerAll.png') }}" type="image/png" media="(min-width:992px)">
                                            <img class="lozad delapan-image pt-5"
                                            data-src="{{ url('/img/borrowerAll.png') }}"
                                            alt="Danasyariah">
                                        </picture>
                                        <h4 class="pt-4">{{ $borrower_all }}</h4>
                                        <h5>@lang('welcome.Jumlah_borrower')</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="slide-wrap slick-slider">
                                <div class="card image parallax mx-auto">
                                    <div class="counter-box fetured-center">
                                        <picture>
                                            <source scrset="{{ url('/img/borrowerAktiff.png') }}" type="image/png" media="(min-width:992px)">
                                            <img class="lozad delapan-image pt-5" src="{{ url('/img/borrowerAktiff.png') }}" alt="Danasyariah">
                                        </picture>
                                        <h4 class="pt-4">{{ $borrower_aktif }} </h4>
                                        <h5>@lang('welcome.Jumlah_borrower_a')</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (Auth::guard('borrower')->check())
    @elseif (Auth::check('user'))
    @else
    <div id="sec-register" class="mb-3"></div>
    <section class="video-box-style-one gray-bg pt-5">
        <div class="thm-container container">
            {{-- START: Title --}}
            <div class="row mb-4">
                <div class="col-md-12 mb-2">
                    <h1 style="font-weight: 600;">@lang('welcome.reg_title')</h1>
                    <p style="font-size: 18px">@lang('welcome.reg_desc')</p>
                </div>
            </div>
            {{-- END: Title --}}
            {{-- START: Content --}}
            <div class="row mt-4">
                <div class="col-md-12 col-xs-12 col-sm-12 mb-10">
                    <div class="row ">
                        <div class="col-md-6 col-sm-12 mb-5">
                            <div class="card-m card-m-round" style="background-color: #FFFCF0;">
                                <div class="block p-5">
                                    <p class="card-title bold h3">@lang('welcome.reg_inv')</p>
                                    <p class="card-text pb-5">@lang('welcome.desc_inv') </p>
                                    {{-- ============================23 Juni 2020 ( Untuk Pengumuman Nasabah ) =================================== --}}
                                    <a href="#" data-toggle="modal" data-target="#modal_register_investor"
                                        class="card-link btn btn-success pl-3 pr-3">Berikan Pendanaan</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 mb-5">
                            <div class="card-m card-m-round" style="background-color: #FFFCF0;">
                                <div class="block p-5">
                                    <p class="card-title bold h3">@lang('welcome.reg_borrower')</p>
                                    <p class="card-text pb-4">@lang('welcome.desc_borrower') </p>
                                    {{-- ============================23 Juni 2020 ( Untuk Pengumuman Nasabah ) =================================== --}}
                                    <a href="#" data-toggle="modal" data-target="#modal_register_borrower"
                                        class="card-link btn btn-success pl-3 pr-3">Ajukan Pembiayaan</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- END: Content --}}
    </section>
    @endif
    <section id="proyek" class="pb-0 ts-block ts-overflow__hidden ts-shape-mask__down pt-5" data-bg-color="#fff"
        data-bg-image="/img/" data-bg-repeat="no-repeat" data-bg-position="bottom" data-bg-size="inherit"
        style="background-size: inherit; background-repeat: no-repeat; background-position: left top; margin: 0 10% 0 10%;">
        <div class="featured-properties content-area-2 bg-transparent dsi-block ">
            <div class="container-fluid">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 align-self-center">
                                <div class="about-text mt-2">
                                    <h1 style="font-weight: 600;">@lang('welcome.pendanaan_aktif')</h1>
                                    <p style="font-size: 18px">@lang('welcome.pendanaan_aktif_det')</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid ">
                        <div class="col-lg-12 ml-auto justify-content-end">
                            <div class="row slick-normal">
                                @foreach ($proyek as $proyek)

                                <div class="slick-slide-item mt-5 mb-3">
                                    <div class="slide-wrap property-box project-card single-service-one" style="border-radius: 20px!important;">
                                        <div class="property-thumbnail cover-image">
                                            <a href="/proyek/{{$proyek->id}}" class="property-img">
                                                    @if($proyek->status == 2)
                                                    <!-- <div class="tag button alt featured bg-secondary"></div> -->
                                                    <div class="ribbon-wrapper-green" style="font-weight:bold;margin:0;float:right;">
                                                        <div class="ribbon-green">Closed</div>
                                                    </div>
                                                    @elseif($proyek->status == 3)
                                                    <div class="ribbon-wrapper-green" style="font-weight:bold;margin:0;float:right;">
                                                        <div class="ribbon-gold">Full</div>
                                                    </div>
                                                    @else
                                                    <div class="tag button alt featured"
                                                        style="font-size: 12px; background-color: #1F5865;">
                                                        <i class="fas fa-stopwatch"></i> Sisa {!!
                                                        date_diff(date_create(Carbon\Carbon::now()->format('Y-m-d')),date_create(Carbon\Carbon::parse($proyek->tgl_selesai_penggalangan)->format('Y-m-d')))->format('%d')
                                                        + 1 !!} Hari Lagi</div>
                                                    @endif
                                                <!-- 
                                <div class="tag button alt featured-right two_chars" style="position: absolute; background-color: white; color: ">{{substr($proyek->profit_margin, 0, 2)}}%
                                </div> -->
                                                <img alt="dana syariah indonesia"
                                                    src="/storage/{{$proyek->gambar_utama}}" width="350px" height="233px"
                                                    style="max-height:150px; margin-top: 0px; border-radius: 5px 5px 5px 5px; object-fit: cover;"
                                                    alt="gbr {{$proyek->id}}" class="img-fluid">
                                            </a>
                                        </div>
                                        <a href="/proyek/{{$proyek->id}}" alt="link {{$proyek->id}}"  class="property-img">
                                            <div style="min-height:50px;">
                                                <label class="ratings bold text-secondary" style="padding-left: 10px; font-size: 1rem; ">{{$proyek->nama}}</label>
                                            </div>
                                            <div class="detail" style="cursor:default;">
                                                <div class="location row no-gutters">
                                                    <div class="col-6 text-left ratings text-dark proyek-bold">
                                                        Dana Terkumpul
                                                    </div>
                                                    <h1 class="col-6 text-right title" style=" color: #00775b; font-weight: 600; ">
                                                        @if($proyek->status == 2)
                                                                {{$proyek->total_need != 0 ? number_format(($proyek->terkumpul+$proyek->pendanaanAktif->where('proyek_id',$proyek->id)->where('status', 1)->sum('nominal_awal'))/$proyek->total_need*100, 2, '.', '') : '0'}} %
                                                        @elseif($proyek->status == 3)
                                                                100%
                                                        @else
                                                            @if($proyek->total_need > 0)
                                                                {{$proyek->total_need !=0 ? number_format(($proyek->terkumpul+$proyek->pendanaanAktif->where('proyek_id',$proyek->id)->where('status', 1)->sum('nominal_awal'))/$proyek->total_need*100, 2, '.', '') : '0'}} %
                                                            @else
                                                                0.00%
                                                            @endif
                                                        @endif

                                                    </h1>
                                                </div>
                                                <!-- progressbar -->
                                                            <!--  //close -->
                                                @if($proyek->status == 2)

                                                <div class="progress" style="background-color: #e2e3e4; height: 10px;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-css"
                                                        role="progressbar" aria-label ="{{$proyek->id}}"
                                                        style="font-weight: bold; box-shadow:-1px 10px 10px rgba(116, 195, 116,0.7) !important; font-size: 15px; width:{{$proyek->total_need != 0 ? number_format(($proyek->terkumpul+$proyek->pendanaanAktif->where('proyek_id',$proyek->id)->where('status', 1)->sum('nominal_awal'))/$proyek->total_need*100, 2, '.', '') : '0'}}%"
                                                        aria-valuenow="{{$proyek->total_need != 0 ? number_format(($proyek->terkumpul+$proyek->pendanaanAktif->where('proyek_id',$proyek->id)->where('status', 1)->sum('nominal_awal'))/$proyek->total_need*100, 2, '.', '') : '0'}}"
                                                        aria-valuemin="0" aria-valuemax="100">
                                                    </div>
                                                </div>
                                                            <!-- //full -->
                                                @elseif($proyek->status == 3)

                                                <div class="progress" style="background-color: #e2e3e4; height: 10px;">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped active"
                                                        role="progressbar"
                                                        style="font-weight: bold; box-shadow:-1px 10px 10px rgba(116, 195, 116,0.7) !important; font-size: 15px; width:100%"
                                                        aria-valuenow="100" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                    </div>
                                                </div>
                                                @else
                                                    @if($proyek->total_need > 0)

                                                <div class="progress" style="background-color: #e2e3e4; height: 10px;">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped active"
                                                        role="progressbar"
                                                        style="font-weight: bold; box-shadow:-1px 10px 10px rgba(116, 195, 116,0.7) !important; font-size: 15px; width:{{$proyek->total_need != 0 ? number_format(($proyek->terkumpul+$proyek->pendanaanAktif->where('proyek_id',$proyek->id)->where('status', 1)->sum('nominal_awal'))/$proyek->total_need*100, 2, '.', '') : '0'}}%"
                                                        aria-valuenow="{{$proyek->total_need != 0 ? number_format(($proyek->terkumpul+$proyek->pendanaanAktif->where('proyek_id',$proyek->id)->where('status', 1)->sum('nominal_awal'))/$proyek->total_need*100, 2, '.', '') : '0'}}"
                                                        aria-valuemin="0" aria-valuemax="100">
                                                    </div>
                                                </div>
                                                    @else

                                                <div class="progress" style="background-color: #e2e3e4; height: 10px;">
                                                    <div class="progress-bar progress-bar-success progress-bar-striped active"
                                                        style="width:0%; box-shadow:-1px 10px 10px rgba(116, 195, 116,0.7) !important;"
                                                        aria-valuenow="0" aria-valuemin="0"
                                                        aria-valuemax="100">
                                                    </div>
                                                </div>
                                                    @endif
                                                @endif
                                                
                                                <div class="location row no-gutters pt-3">
                                                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12  text-left pr-2 ratings text-secondary">
                                                        <span class="p-text-regular">Dana Dibutuhkan</span> 
                                                        <br>
                                                        <span class="p-text-bold">Rp {{number_format($proyek->total_need,  0, '', '.')}}</span>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12  text-left pt-onMobile ratings text-secondary">
                                                        <span class="p-text-regular">Durasi Proyek</span> 
                                                        <br>
                                                        <span class="p-text-bold">{{$proyek->tenor_waktu}} Bulan</span>
                                                    </div>
                                                </div>
                                                <div class="location row no-gutters">
                                                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12  text-left pr-2 ratings text-secondary">
                                                        <span class="p-text-regular">Imbal Hasil/Tahun</span> 
                                                        <br>
                                                        <span class="p-text-bold">Rp {{number_format((($proyek->total_need*($proyek->profit_margin/100))),0, '', '.')}} ({{$proyek->profit_margin}}%)</span>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12  text-left pt-onMobile ratings text-secondary">
                                                        <span class="p-text-regular">Minimum Pendanaan</span> 
                                                        <br>
                                                        <span class="p-text-bold">Rp {{number_format($proyek->harga_paket,  0, '', '.')}}</span>
                                                    </div>
                                                </div>
                                                <div class=" row no-gutters">
                                                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 text-left ratings text-secondary">
                                                        <span class="p-text-regular">Jenis Akad</span> 
                                                        <br>
                                                        <span class="p-text-bold">
                                                            @if($proyek->akad == 1)
                                                                Murabahah
                                                            @elseif($proyek->akad == 2)
                                                                Mudharabah
                                                            @elseif($proyek->akad == 3)
                                                                MMQ
                                                            @elseif($proyek->akad == 4)
                                                                IMBT
                                                            @endif

                                                        </span>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 text-left pt-onMobile ratings text-secondary">
                                                        <span class="p-text-regular">Terima Imbal Hasil</span>
                                                        <br>
                                                        <span class="p-text-bold">
                                                            @if($proyek->waktu_bagi == 1)
                                                            Tiap Bulan
                                                            @else
                                                            Akhir Proyek
                                                            @endif

                                                        </span>
                                                    </div>
                                                </div>
                                                <div class=" wow fadeInUp delay-10s">
                                                    <a class="line-button" href="/proyek/{{$proyek->id}}"
                                                        style="font-size: 0.6em; padding-top: 5em; color: #175D43; text-decoration: underline;"><i>
                                                            @lang('language.selengkapnya') →</i></a>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                    <!-- end for -->
                    <div class="container">
                        <div class="row">
                            <div class="col text-center">
                                <div class="main-title pb-2 pt-0 mr-4 wow fadeInUp delay-10s">
                                    <a href="/penggalangan-berlangsung"
                                        class="btn btn-md banner-btn-right-ico ">@lang('proyek.lihat') <i
                                            class="fas fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- Testimonial Slider DSI -->`
    <div class="testimonial  bg-dsi-gradient-flip ">
        <div class="customer-feedback  pt-5">
            <div class="container text-center ">
                <div class="row">
                    <div class="col-12 ">
                        <div>
                            <h1 class="section-title" style="font-weight: 600;">@lang('welcome.testimoni')</h1>
                            <p> @lang('welcome.testimoni_det')</p>
                        </div>
                    </div><!-- /End col -->
                </div><!-- /End row -->

                <div class="row">
                    <div class="col-12">
                        <div class="owl-carousel feedback-slider p-10 lozad">

                            @foreach ($testimoni as $testimoni)
                            <div class="feedback-slider-item">
                                <p>" {!!$testimoni->content!!} "</p>
                                <div class="image-testimoni">
                                <img alt="dana syariah indonesia" src="{{asset('/storage')}}/{{$testimoni->gambar}}" align="left"
                                    class="mx-auto rounded-circle" alt="Customer Feedback">
                                <label class="customer-name"><p>&nbsp;</p><p>&nbsp;{{$testimoni->nama}}</p></label>
                                </div>
                            </div>
                            @endforeach

                        </div><!-- /End feedback-slider -->
                    </div><!-- /End col -->
                </div><!-- /End row -->
            </div><!-- /End container -->
        </div><!-- /End customer-feedback -->
    </div>
    <!-- end testimonial -->

    <!-- Blog start -->
    <div class="blog content-area-2">
        <div class="container pb-5 pt-5">
            <div class="main-title pb-5">
                <h1>@lang('welcome.news')</h1><h2>&nbsp;</h2>
                <h3>@lang('welcome.news_desc')</h3><h4>&nbsp;</h4>
            </div>
            <div class="row">
                @foreach($news as $dataNews)
                @php
                $data_url = str_slug($dataNews->title,'-');
                @endphp
                <div class="col-lg-4 col-md-6">
                    <div class="blog-1">
                        <a href="/news_detil/{{ $dataNews->id }}/{{ $data_url }}">
                            <img alt="dana syariah indonesia {{$dataNews->title}}"
                                data-src="{{asset('/storage')}}/{{$dataNews->image}}" alt="blog"
                                class="img-fluid lozad">
                        </a>
                        <div class="detail mb-5" style="overflow-wrap: break-word;">
                            <div class="date-box">
                                <p style="color:white;">{{ Carbon\Carbon::parse($dataNews->created_at)->format('d') }}</p>
                                <p style="color:white;">{{ Carbon\Carbon::parse($dataNews->created_at)->format('M') }}</p>
                            </div>
                            <h3 class="pt-3">
                                <a href="/news_detil/{{ $dataNews->id }}/{{ $data_url }}"
                                    class="text-dark">{{$dataNews->title}}</a>
                            </h3>
                            <div class="post-meta">
                                <label style="font-size: .8rem"><i class="fa fa-user"></i> <span class="pl-2">
                                        {{$dataNews->writer}} </span> </label>
                            </div>
                            <p>{!! substr(strip_tags($dataNews->deskripsi),0,250) !!}..</p>
                            <a href="/news_detil/{{ $dataNews->id }}/{{ $data_url }}"
                                class="text-success ">@lang('language.selengkapnya') →</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="container">
                <div class="row">
                    <div class="col text-center pt-5">
                        <div class="main-title pb-2 pt-0 mr-4">
                            <a href="news" class="btn btn-md banner-btn-right-ico ">@lang('language.selengkapnya') →</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal_notif" class="modal fade in modal_scroll" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered ">
            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-body">
                    <button type="button" class="close ml-5 mr-5 pr-2 mt-3 text-dark" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true"><i class="lni-close size-sm"></i> </span>
                    </button>
                    <div>
			             <img src="{{url($banner)}}" class="img-fluid w-100" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){		
            $("#cta-reg").click(function () {
                $('html, body').animate({
                    scrollTop: $('#sec-register').offset().top
                }, 'slow');
            });

        	@if($tampilbanner == 'A')
                    $("#modal_notif").modal("show");
            @endif

        });
    </script>
    <!-- Blog end -->
    <!-- partner end -->
    @endsection