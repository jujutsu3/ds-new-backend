@push('add-style')
<style>
    .line {
        display: flex;
        flex-direction: row;
    }

    .line:after {
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #000;
        margin: auto;
    }

    .bg-green-dsi {
        background-color: #18783A
    }

    @media screen and (max-width: 600px) {
        .step-title {
            visibility: hidden;
            clear: both;
            float: left;
            margin: 10px auto 5px 20px;
            width: 28%;
            display: none;
        }
    }

    .input-hidden {
        height: 0;
        width: 0;
        visibility: hidden;
        padding: 0;
        margin: 0;
        float: right;
    }
</style>
@endpush

<div class="row">
    <div class="col-sm-12 col-lg-12 ">
        <form id="example-advanced-form">
            @csrf
            <div id="demo">
                <div class="step-app">

                    <ul class="step-steps">
                        <li><a href="#tab1"><span class="number">1</span> Data Diri</a></li>
                        <li><a href="#tab2"><span class="number">2</span> Data Rekening</a></li>
                    </ul>

                    <div class="step-content">
                        <div class="step-tab-panel" id="tab1">
                            {{-- <h3>Data Diri</h3> --}}
                            <div class="form-row">
                                <div class="form-group col-sm-6">
                                    <input type="hidden" name="tipe_pengguna" id="tipe_pengguna" value="1">
                                    <label for="nama" class="font-weight-bold">Nama *</label>
                                    <input type="text" maxlength="30" name="nama"
                                        class="form-control text_only required" placeholder="Nama"
                                        value="{{ old('nama') }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <label for="warga negara" class="font-weight-bold">Warga Negara *</label>
                                    <select name="warga_negara" class="form-control required" required>
                                        <option value="">--Pilih--</option>
                                        @foreach ($master_negara as $b)
                                        <option value="{{$b->id_negara}}"
                                            {{ old('id_negara') == $b->negara ? 'selected' : '' }}>
                                            {{$b->negara}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="tempat_lahir" class="font-weight-bold">Tempat Lahir *</label>
                                    <input type="text" maxlength="35" name="tempat_lahir"
                                        class="form-control text_only required" placeholder="Tempat Lahir"
                                        value="{{ old('tempat_lahir') }}">
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="tanggal_lahir" class="font-weight-bold">Tanggal Lahir *</label>
                                    @php
                                    // bulan
                                    $data_bulan =
                                    ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember'];
                                    // end bulan
                                    @endphp
                                    <div class="form-row">
                                        <div class="col-sm-3">
                                            <select name="tgl_lahir" class="form-control required">
                                                <option value="">--Pilih--</option>
                                                @for($i=1;$i<=31;$i++) <option value={{ strlen($i) == 1 ? "0".$i : $i }}
                                                    {{ old('tgl_lahir') == $i ? 'selected' : '' }}>
                                                    {{ strlen($i) == 1 ? "0".$i : $i }}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                        <div class="col-sm-5">
                                            <select name="bln_lahir" class="form-control required">
                                                <option value="01">Januari</option>
                                                <option value="02">Februari</option>
                                                <option value="03">Maret</option>
                                                <option value="04">April</option>
                                                <option value="05">Mei</option>
                                                <option value="06">Juni</option>
                                                <option value="07">Juli</option>
                                                <option value="08">Agustus</option>
                                                <option value="09">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">Nopember</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select name="thn_lahir" class="form-control required" id="thn_lahir">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="form-group col-sm-2">
                        <label for="jenis_kelamin" class="font-weight-bold">Jenis Kelamin *</label>
                        <select name="jenis_kelamin" class="form-control required">
                            <option value="">--Pilih--</option>
                            @foreach (session('master_jenis_kelamin') as $b)
                                <option value="{{$b->id_jenis_kelamin}}"
                                {{ old('jenis_kelamin') == $b->id_jenis_kelamin ? 'selected' : '' }}>{{$b->jenis_kelamin}}
                                </option>
                                @endforeach
                                </select>
                            </div> --}}
                        </div>
                        {{-- </div> --}}
                        <div class="form-row">
                            <div class="form-group col-sm-3">
                                <label for="jenis_kelamin" class="font-weight-bold">Jenis Identitas *</label>
                                <br />
                                <label class="css-control css-control-primary css-radio mr-10 text-dark">
                                    <input type="radio" class="css-control-input" id="jenis_ktp" name="jenis_identitas"
                                        value="1" checked required>
                                    <span class="css-control-indicator"></span> KTP
                                </label>
                                <label class="css-control css-control-primary css-radio mr-10 text-dark">
                                    <input type="radio" class="css-control-input" id="jenis_ktp" name="jenis_identitas"
                                        value="2">
                                    <span class="css-control-indicator"></span> Paspor
                                </label>
                            </div>
                            <div class="form-group col-sm-3 div_ktp div_jenis_ktp" id="data_perorangan_1">
                                <label for="no_ktp" class="font-weight-bold">No KTP</label>
                                <input type="text" minlength="16" maxlength="16" name="no_ktp" id="no_ktp"
                                    onkeyup="check_ktp()" class="form-control checkNOKTP" placeholder="No KTP"
                                    value="{{ old('no_ktp') }}">
                            </div>
                            <div class="form-group col-sm-3" id="div_jenis_passpor" style="display:none;">
                                <label for="no_ktp" class="font-weight-bold">Paspor</label>
                                <input type="text" name="no_passpor" minlength="5" maxlength="11" id="no_passpor"
                                    class="form-control char_only required" placeholder="Masukan Nomor Paspor Anda">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-3">
                                <label for="jenis_kelamin" class="font-weight-bold">Jenis Kelamin*</label>
                                <select name="jenis_kelamin" class="form-control required">
                                    <option value="">--Pilih--</option>
                                    @foreach ($master_jenis_kelamin as $b)
                                    <option value="{{$b->id_jenis_kelamin}}"
                                        {{ old('jenis_kelamin') == $b->id_jenis_kelamin ? 'selected' : '' }}>
                                        {{$b->jenis_kelamin}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="status_kawin" class="font-weight-bold">Status Perkawinan *</label>
                                <select name="status_kawin" class="form-control required">
                                    <option value="">--Pilih--</option>
                                    @foreach ($master_kawin as $b)
                                    <option value="{{$b->id_kawin}}"
                                        {{ old('jenis_kawin') == $b->id_kawin ? 'selected' : '' }}>
                                        {{$b->jenis_kawin}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-sm-2">
                                <label for="jenis_kelamin" class="font-weight-bold">Agama</label>
                                <select name="txt_agama" class="form-control required" required>
                                    <option value="">--Pilih--</option>
                                    @foreach ($master_agama as $b)
                                    <option value="{{$b->id_agama}}"
                                        {{ old('id_agama') == $b->agama ? 'selected' : '' }}>{{$b->agama}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>

                            {{-- <div class="form-group col-sm-3">
                      <label for="no_npwp" class="font-weight-bold">No NPWP</label>
                      <input type="text" name="no_npwp" class="form-control" placeholder="No NPWP" value="{{ old('no_npwp') }}">
                        </div> --}}
                        <div class="form-group col-sm-1">
                            <label for="txt_kode_negara" class="font-weight-bold">Operator</label>
                            <select id="txt_kode_operator" name="txt_kode_operator" class="form-control" required>
                                <option value="62" selected>(62) Indonesia</option>

                                @foreach ($master_kode_operator as $b)
                                <option value="{{$b->kode_operator}}"
                                    {{ old('id_operator') == $b->kode_operator ? 'selected' : '' }}>
                                    ({{$b->kode_operator}}) {{$b->negara}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3 div_hp">
                            <label for="no_telp" id="lbl_no_hp" class="font-weight-bold">No HP *</label>
                            <input type="text" minlength="9" maxlength="13" name="no_telp" id="no_telp"
                                onkeyup="check_hp()" class="form-control checkNOHP required"
                                placeholder="Contoh:8xxxxxxxxxx" value="{{ old('no_telp') }}">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="no_telp" class="font-weight-bold">Sumber Dana *</label>
                            <input type="text" maxlength="50" name="txt_sumber_dana" id="txt_sumber_dana"
                                class="form-control char_only required" placeholder="Masukan Sumber Dana Anda" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-3">
                            <label for="nama_ibu_kandung" class="font-weight-bold">Nama Ibu Kandung *</label>
                            <input type="text" maxlength="30" name="nama_ibu_kandung" id="nama_ibu_kandung"
                                class="form-control char_only required" placeholder="Nama Ibu Kandung"
                                value="{{ old('nama_ibu_kandung') }}">
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="pendidikan" class="font-weight-bold">Pendidikan *</label>
                            <select id="pendidikan" name="pendidikan" class="form-control required">
                                <option value="">--Pilih--</option>
                                @foreach ($master_pendidikan as $b)
                                <option value="{{$b->id_pendidikan}}"
                                    {{ old('pendidikan') == $b->id_pendidikan ? 'selected' : '' }}>
                                    {{$b->pendidikan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3" id="data_perorangan_1">
                            <label for="Pekerjaan" class="font-weight-bold">Pekerjaan *</label>
                            <select name="pekerjaan" class="form-control required">
                                <option value="">--Pilih--</option>
                                @foreach ($master_pekerjaan as $b)
                                <option value="{{$b->id_pekerjaan}}"
                                    {{ old('pekerjaan') == $b->id_pekerjaan ? 'selected' : '' }}>
                                    {{$b->pekerjaan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="Pekerjaan" class="font-weight-bold">Bidang Pekerjaan *</label>
                            <select id="bidang_pekerjaan" name="bidang_pekerjaan" class="form-control required"
                                required>
                                <option value="">--Pilih--</option>
                                @foreach ($master_bidang_pekerjaan as $b)
                                <option value="{{$b->kode_bidang_pekerjaan}}"
                                    {{ old('kode_bidang_pekerjaan') == $b->kode_bidang_pekerjaan ? 'selected' : '' }}>
                                    {{$b->bidang_pekerjaan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label class="font-weight-bold">Pendapatan *</label>
                            <select name="pendapatan" class="form-control required" required>
                                <option value="">--Pilih--</option>
                                @foreach ($master_pendapatan as $b)
                                <option value="{{$b->id_pendapatan}}"
                                    {{ old('pendapatan') == $b->id_pendapatan ? 'selected' : '' }}>
                                    {{$b->pendapatan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label class="font-weight-bold">Pengalaman Kerja *</label>
                            <select id="pengalaman_kerja" name="pengalaman_kerja" class="form-control required"
                                required>
                                <option value="">--Pilih--</option>
                                @foreach ($master_pengalaman_kerja as $b)
                                <option value="{{$b->id_pengalaman_kerja}}"
                                    {{ old('id_pengalaman_kerja') == $b->id_pengalaman_kerja ? 'selected' : '' }}>
                                    {{$b->pengalaman_kerja}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label class="font-weight-bold">Bidang Online *</label>
                            <select id="bidang_online" name="bidang_online" class="form-control required" required>
                                <option value="">--Pilih--</option>
                                @foreach ($master_online as $b)
                                <option value="{{$b->id_online}}"
                                    {{ old('id_online') == $b->id_online ? 'selected' : '' }}>
                                    {{$b->tipe_online}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat" class="font-weight-bold">Alamat *</label>
                        <textarea name="alamat" maxlength="90" class="form-control col-sm-10 required" rows="3"
                            id="alamat" placeholder="Alamat Lengkap">{{ old('alamat') }}</textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-sm-3">
                            <label for="domisili negara" class="font-weight-bold">Domisili Negara *</label>
                            <select id="domisili_negara" name="domisili_negara" class="form-control required" required>
                                <option value="">--Pilih--</option>
                                @foreach ($master_negara as $b)
                                <option value="{{$b->id_negara}}"
                                    {{ old('id_negara') == $b->negara ? 'selected' : '' }}>{{$b->negara}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="provinsi" class="font-weight-bold">Provinsi *</label>
                            <select id="provinsi" name="provinsi" class="form-control required" id="provinsi">
                                <option value="">--Pilih--</option>
                                @foreach ($master_provinsi as $data)
                                <option value={{$data->kode_provinsi}}
                                    {{ old('provinsi') == $data->kode_provinsi ? 'selected' : ''}}>
                                    {{$data->nama_provinsi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-2">
                            <label for="kota" class="font-weight-bold">Kota *</label>
                            <select name="kota" class="form-control" id="kota">
                            </select>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="kecamatan" class="font-weight-bold">Kecamatan *</label>
                            <input type="text" maxlength="30" name="kecamatan" class="form-control char_only required"
                                id="kecamatan" placeholder="Kecamatan" value="{{ old('kecamatan') }}">
                        </div>
                        <div class="form-group col-sm-2">
                            <label for="kelurahan" class="font-weight-bold">Kelurahan *</label>
                            <input type="text" maxlength="30" name="kelurahan" class="form-control char_only required"
                                id="kelurahan" placeholder="Kelurahan" value="{{ old('kelurahan') }}">
                        </div>
                        <div class="form-group col-sm-2">
                            <label for="kode_pos" class="font-weight-bold">Kode Pos *</label>
                            <input type="text" minlength="5" maxlength="5" name="kode_pos" class="form-control required"
                                id="kode_pos" placeholder="Kode Pos" value="{{ old('kode_pos') }}">
                        </div>
                    </div>
                    {{-- <div class="form-group col-sm-4 ml-2">
                  <input class="form-check-input" type="checkbox" id="id_check_poto"> &nbsp;
                  <label class="font-weight-bold" for="defaultCheck1"> Unggah Poto (Optional)</label>
                </div> --}}
                    <div class="form-row ml-3" id="divHidePoto">
                        <div class="col-sm-2 imgUp">
                            <div id="upload_diri">
                                <label class="font-weight-bold">Foto Diri *</label>
                                <img class="imagePreview" id="preview-1">
                                <!--label class="btn btn-primary">Upload<input type="file" name="pic_investor" id="pic_investor" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;" accept=".jpg, .jpeg, .png,.bmp">
                      </label-->
                                <div id="camera_diri">
                                    <label class="btn btn-primary">Kamera</label>
                                </div><br />
                                <div id="tampil_error_diri">
                                </div>
                            </div>
                        </div>
                        <div id="take_camera_diri" style="display: none;">
                            <div class="col-sm-4 imgUp">
                                <label class="font-weight-bold">Foto Diri *</label>
                                <div class="col-md-6">
                                    <img id="user-guide" src="{{URL::to('assets/img/user-guide.png')}}" alt="guide"
                                        style="position: absolute; z-index: 9999;  width: 200px; height: 185px; top: 22px;">
                                    <div id="my_camera">
                                    </div>
                                    <input class="btn btn-primary" type="button" value="Ambil Foto"
                                        onClick="take_snapshot()">
                                    <input type="hidden" id="user_val" value="0">
                                    <input type="hidden" name="image_foto" id="user" class="image-tag"><br />
                                    <!--div id="base_upload_diri">
                          <label class="btn btn-primary">Upload</label>
                        </div-->
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold">Hasil</label>
                                    <div id="results"></div>
                                </div><br />
                            </div>
                        </div>

                        <div class="col-sm-2 imgUp">
                            <div id="upload_ktp">
                                <label class="font-weight-bold">Foto KTP *</label>
                                <img class="imagePreview" id="preview-2">
                                <!--label class="btn btn-primary">Upload<input type="file" name="pic_ktp_investor" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;" accept=".jpg, .jpeg, .png,.bmp" id="pic_ktp_investor">
                      </label-->
                                <div id="camera_ktp">
                                    <label class="btn btn-primary">Kamera</label>
                                </div><br />
                                <div id="tampil_error_ktp">
                                </div>
                            </div>
                        </div>
                        <div id="take_camera_ktp" style="display: none;">
                            <div class="col-sm-4 imgUp">
                                <label class="font-weight-bold">Foto KTP *</label>
                                <div class="col-md-6">
                                    <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                                        style="position: absolute; z-index: 9999;  width: 200px; height: 155px; top: 22px;">
                                    <div id="my_camera2"></div>
                                    <input class="btn btn-primary" type="button" value="Ambil Foto"
                                        onClick="take_snapshot2()">
                                    <input type="hidden" id="ktp_val" value="0">
                                    <input type="hidden" name="image_ktp" id="ktp" class="image-tag"><br />
                                    <!--div id="base_upload_ktp">
                          <label class="btn btn-primary">Upload</label>
                        </div-->
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold">Hasil</label>
                                    <div id="results2"></div>
                                </div><br />
                            </div>
                        </div>

                        <div class="col-sm-2 imgUp">
                            <div id="upload_ktp_diri">
                                <label class="font-weight-bold" style="width: 204px;">Foto Diri dengan KTP *</label>
                                <img class="imagePreview" id="preview-3">
                                <!--label class="btn btn-primary">Upload<input type="file" name="pic_user_ktp_investor" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;" accept=".jpg, .jpeg, .png,.bmp" id="pic_user_ktp_investor">
                      </label-->
                                <div id="camera_ktp_diri">
                                    <label class="btn btn-primary">Kamera</label>
                                </div><br />
                                <div id="tampil_error_ktp_diri">
                                </div>
                            </div>
                        </div>
                        <!--  </div> -->
                        <div id="take_camera_ktp_diri" style="display: none;">
                            <div class="col-sm-9 imgUp">
                                <label class="font-weight-bold">Foto Diri dengan KTP *</label>
                                <div class="col-md-6">
                                    <img id="user-guide" src="{{URL::to('assets/img/guide-diridanktp.png')}}"
                                        alt="guide"
                                        style="position: absolute; z-index: 9999;  width: 200px; height: 170px; top: 22px;">
                                    <div id="my_camera3"></div>
                                    <input class="btn btn-primary" type="button" value="Ambil Foto"
                                        onClick="take_snapshot3()">
                                    <input type="hidden" id="user_ktp_val" value="0">
                                    <input type="hidden" name="image_user_ktp" id="user_ktp" class="image-tag"><br />
                                    <!--div id="base_upload_ktp_diri">
                          <label class="btn btn-primary">Upload</label>
                        </div-->
                                </div>
                                <div class="col-md-6">
                                    <label class="font-weight-bold">Hasil</label>
                                    <div id="results3"></div>
                                </div><br>
                            </div>
                        </div>
                    </div>
                    <p>Format file .jpg, .jpeg, .gif, dan .png</p>
                    <div class="form-group col-sm-4 ml-2">
                        <input class="form-check-input" type="checkbox" name="npwp_check" id="npwp_check">
                        <label class="font-weight-bold" for="defaultCheck1">Saya Punya NPWP</label>
                    </div>
                    <div class="form-group div_npwp" id="show_npwp" style="display: none;">
                        <input type="text" minlength="15" maxlength="15" id="no_npwp" name="no_npwp"
                            class="form-control checkNONPWP" placeholder="No NPWP" value="{{ old('no_npwp') }}">
                    </div>
                    <!-- Ahli Waris -->
                    <hr>
                    </hr>
                    <div class="form-row">
                        <div class="form-group col-sm-3">
                            <label for="tempat_lahir" class="font-weight-bold">Nama Ahli Waris *</label>
                            <input type="text" maxlength="30" name="nm_ahli_waris"
                                class="form-control char_only required" placeholder="Nama Ahli Waris" value=""
                                aria-required="true">
                        </div>

                        <div class="form-group col-sm-3">
                            <label for="tanggal_lahir" class="font-weight-bold">Hubungan Ahli Waris *</label>
                            <select id="hubungan_ahli_aris" name="hubungan_ahli_aris" class="form-control required"
                                aria-required="true">
                                <option value="">--Pilih--</option>
                                @foreach ($master_hubungan_ahli_waris as $b)
                                <option value="{{$b->id_hub_ahli_waris}}"
                                    {{ old('id_hub_ahli_waris') == $b->jenis_hubungan ? 'selected' : '' }}>
                                    {{$b->jenis_hubungan}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-sm-3 div_nik_aw">
                            <label for="tempat_lahir" class="font-weight-bold">No KTP Ahli Waris *</label>
                            <input type="text" minlength="16" maxlength="16" id="nik_ahli_waris" name="nik_ahli_waris"
                                class="form-control checkNIKAW required" placeholder="NIK Ahli Waris" value=""
                                aria-required="true">
                            {{-- <input type="text" minlength="16" maxlength="16"  id="nik_ahli_waris" name="nik_ahli_waris" class="form-control checkNIKAW required" onkeyup="chek_nik_aw()" placeholder="NIK Ahli Waris" value="" aria-required="true"> --}}
                        </div>

                        <div class="form-group col-sm-1">
                            <label for="txt_kode_operator_aw" class="font-weight-bold">Operator</label>
                            <select id="txt_kode_operator_aw" name="txt_kode_operator_aw" class="form-control" required>
                                <option value="62" selected>(62) Indonesia</option>

                                @foreach ($master_kode_operator as $b)
                                <option value="{{$b->kode_operator}}"
                                    {{ old('id_operator') == $b->kode_operator ? 'selected' : '' }}>
                                    ({{$b->kode_operator}}) {{$b->negara}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-2 div_hp_aw">
                            <label class="font-weight-bold">No HP Ahli Waris *</label>
                            <input type="text" minlength="9" maxlength="13" id="no_hp_ahli_waris"
                                name="no_hp_ahli_waris" class="form-control checkHPAW required" onkeyup="check_hp_aw()"
                                placeholder="No HP Ahli Waris" value="" aria-required="true">
                            {{-- <input type="text" minlength="13" maxlength="13" id="no_hp_ahli_waris" name="no_hp_ahli_waris" class="form-control checkHPAW required" onkeyup="check_hp_aw()" placeholder="No HP Ahli Waris" value="" aria-required="true"> --}}
                        </div>

                        <div class="form-group col-sm-3">
                            <label class="font-weight-bold">Alamat Ahli Waris *</label>
                            <input type="text" maxlength="90" name="alamat_ahli_waris" class="form-control required"
                                placeholder="Alamat Ahli Waris" value="" aria-required="true">
                        </div>


                    </div>
                    <p><b>*) Wajib Diisi</b></p>
                </div>
                <div class="step-tab-panel" id="tab2">
                    {{-- <h3>Data Rekening</h3> --}}
                    <div class="form-row">
                        <div class="form-group col-sm-4">
                            <label for="rekening" class="font-weight-bold">No Rekening *</label>
                            <input type="text" minlength="10" maxlength="16" name="rekening" id="rekening"
                                class="form-control required" placeholder="No Rekening" value="{{ old('rekening') }}">
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="rekening" class="font-weight-bold">Nama Pemilik Rekening *</label>
                            <input type="text" maxlength="30" name="nama_pemilik_rek" id="nama_pemilik_rek"
                                class="form-control text_only required" placeholder="Nama Pemilik Rekening"
                                value="{{ old('nama_pemilik_rek') }}">
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="bank" class="font-weight-bold">Bank *</label>
                            <select name="bank" class="form-control required" id="bank">
                                <option value="">--Pilih--</option>
                                @foreach($master_bank as $b)
                                <option value="{{ $b->kode_bank }}"
                                    {{ old('bank') == $b->kode_bank ? 'selected' : '' }}>
                                    {{ $b->nama_bank }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <p><b>*) Wajib Diisi</b></p>
                </div>

            </div>

            <div class="step-footer float-right">
                <button data-direction="prev" class="step-btn">Sebelumnya</button>
                <button data-direction="next" class="step-btn" id="btn_lanjut" disabled>Lanjut</button>
                <button data-direction="finish" class="step-btn" id="selesai">Selesai</button>
            </div>
        </form>
    </div>
</div>

@push('add-script')
<script type="text/javascript">
    $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    
      $(document).on("change","#pic_investor", function()
        {
          var uploadFile = $(this);
          var files =!!this.files?this.files:[];
          if(!files.length||!window.FileReader)return;
    
          if (/^image/.test( files[0].type)){ // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
    
              reader.onloadend = function(){ // set image data as background of div
                  //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                uploadFile.closest(".imgUp").find('.imagePreview').attr("src", this.result);
              }
    
          var fileUpload = $(this).get(0);
          var filess = fileUpload.files[0];
          var form_data = new FormData();
          form_data.append('file', filess);
          console.log(filess);
    
          $.ajax({
                  url : "/user/new_upload1",
                  method : "post",
                  dataType: 'JSON',
                  data: form_data,
                  contentType: false,
                  processData: false,
                  success:function(data)
                  {
                      console.log(data)
                      if(data.success){
                        alert(data.success);
                      }else{
                        alert(data.failed);
                      }
                  }
              });
            }
        });
    
        $(document).on("change","#pic_ktp_investor", function()
        {
          var uploadFile = $(this);
          var files =!!this.files?this.files:[];
          if(!files.length||!window.FileReader)return;
    
          if (/^image/.test( files[0].type)){ // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
    
              reader.onloadend = function(){ // set image data as background of div
                  //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                uploadFile.closest(".imgUp").find('.imagePreview').attr("src", this.result);
              }
    
          var fileUpload = $(this).get(0);
          var filess = fileUpload.files[0];
          var form_data = new FormData();
          form_data.append('file', filess);
          console.log(filess);
    
          $.ajax({
                  url : "/user/new_upload2",
                  method : "post",
                  dataType: 'JSON',
                  data: form_data,
                  contentType: false,
                  processData: false,
                  success:function(data)
                  {
                      console.log(data)
                      if(data.success){
                        alert(data.success);
                      }else{
                        alert(data.failed);
                      }
                  }
              });
            }
        });
    
        $(document).on("change","#pic_user_ktp_investor", function()
        {
          var uploadFile = $(this);
          var files =!!this.files?this.files:[];
          if(!files.length||!window.FileReader)return;
    
          if (/^image/.test( files[0].type)){ // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
    
              reader.onloadend = function(){ // set image data as background of div
                  //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                uploadFile.closest(".imgUp").find('.imagePreview').attr("src", this.result);
              }
    
          var fileUpload = $(this).get(0);
          var filess = fileUpload.files[0];
          var form_data = new FormData();
          form_data.append('file', filess);
          console.log(filess);
    
          $.ajax({
                  url : "/user/new_upload3",
                  method : "post",
                  dataType: 'JSON',
                  data: form_data,
                  contentType: false,
                  processData: false,
                  success:function(data)
                  {
                      console.log(data)
                      if(data.success){
                        alert(data.success);
                      }else{
                        alert(data.failed);
                      }
                  }
              });
            }
        });
    
        (function($) {
        $.fn.inputFilter = function(inputFilter) {
          return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
              this.oldValue = this.value;
              this.oldSelectionStart = this.selectionStart;
              this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
              this.value = this.oldValue;
              this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
          });
        };
      }(jQuery));
    
      $("#no_telp").inputFilter(function(value) {
        return /^\d*$/.test(value); });
    
      // $("#no_ktp").inputFilter(function(value) {
      // return /^\d*$/.test(value); });
    
      $("#rekening").inputFilter(function(value) {
      return /^\d*$/.test(value); });
    
      
    
      $('#provinsi').on('change',function(e){
          e.preventDefault();
          var kode_provinsi = this.value;
          console.log(kode_provinsi)
          $('#kota').empty();
          $.ajax({
              url : "/getKota/"+kode_provinsi,
              method : "get",
              dataType: 'JSON',
              success:function(data)
              {
                  $.each(data.kota,function(index,value){
                      $('#kota').append(
                          '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
                      );
                  })
              }
          });
      });
</script>

<script>
    $(document).ready(function(){
      $("#camera_diri").click(function(){
        Webcam.attach( '#my_camera' );
        $("#take_camera_diri").fadeIn();
        $("#take_camera_diri").css( { "margin-left" : "-290px"} );
        $("#upload_diri").fadeOut();;
      });
    });
    $(document).ready(function(){
      $("#base_upload_diri").click(function(){
        $("#take_camera_diri").fadeOut();
        $("#upload_diri").fadeIn();
      });
    });

    $(document).ready(function(){
      $("#camera_ktp").click(function(){
        Webcam.attach( '#my_camera2' );
        $("#take_camera_ktp").fadeIn();
        $("#take_camera_ktp").css( { "margin-left" : "-290px"} );
        $("#upload_ktp").fadeOut();;
      });
    });
    $(document).ready(function(){
      $("#base_upload_ktp").click(function(){
        $("#take_camera_ktp").fadeOut();
        $("#upload_ktp").fadeIn();
      });
    });

    $(document).ready(function(){
      $("#camera_ktp_diri").click(function(){
        Webcam.attach( '#my_camera3' );
        $("#take_camera_ktp_diri").fadeIn();
        $("#take_camera_ktp_diri").css( { "margin-left" : "-290px"} );
        $("#upload_ktp_diri").fadeOut();
      });
    });
    $(document).ready(function(){
      $("#base_upload_ktp_diri").click(function(){
        $("#take_camera_ktp_diri").fadeOut();
        $("#upload_ktp_diri").fadeIn();
      });
    });
</script>

<script language="JavaScript">
    // Webcam.set({
    //     width: 200,
    //     height: 200,
    //     image_format: 'jpg',
    //     jpeg_quality: 90
    // });
  
    function take_snapshot()
    {
     
        Webcam.snap( function(data_uri) {
		        document.getElementById('user').value = data_uri;
            document.getElementById('user_val').value = '1';
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'" style="width:200px;height:160px;"/>';
        } );

    }
</script>

<script language="JavaScript">
    // Webcam.set({
    //     width: 200,
    //     height: 200,
    //     image_format: 'jpg',
    //     jpeg_quality: 90
    // });

    function take_snapshot2()
    {
      
      Webcam.snap( function(data_uri) {
          document.getElementById('ktp').value = data_uri;
          document.getElementById('ktp_val').value = '1';
          document.getElementById('results2').innerHTML = '<img src="'+data_uri+'" style="width:200px;height:160px;"/>';
      } );

    }

</script>

<script language="JavaScript">
    // Webcam.set({
    //     width: 200,
    //     height: 200,
    //     image_format: 'jpg',
    //     jpeg_quality: 90
    // });

    function take_snapshot3()
    {
        
      Webcam.snap( function(data_uri) {
          document.getElementById('user_ktp').value = data_uri;
          document.getElementById('user_ktp_val').value = '1';
          document.getElementById('results3').innerHTML = '<img src="'+data_uri+'" style="width:200px;height:160px;"/>';
      } );

    }
</script>

<script type="text/javascript">
    var frmInfo = $('#example-advanced-form');
    var frmInfoValidator = frmInfo.validate();
    
    $(document).on("click", "#jenis_ktp", function(){
      if(this.value == 1 ){
        $(".div_jenis_ktp").fadeIn("slow");
        $("#div_jenis_passpor").fadeOut("slow");
      }else{
        $(".div_jenis_ktp").fadeOut("slow");
        $("#div_jenis_passpor").fadeIn("slow");
      }
    });
    $(document).on("input", "#no_ktp", function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });
    $(document).on("input", "#no_npwp", function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });

    $(document).on("input", "#kode_pos", function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });

    $(document).on("input", "#kode_otp", function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });
    $(document).on("input", "#nik_ahli_waris", function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });

    $(document).on("input", "#no_hp_ahli_waris", function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });
    $('.char_only').on('input', function (event) { 
      this.value = this.value.replace(/[^a-zA-Z 0-9 _.]/g, '');
    });

    $('.text_only').on('input', function (event) { 
      this.value = this.value.replace(/[^a-zA-Z ]/g, '');
    });
    var checkNOHP = false; 
    function check_hp() {
			var notlp = $(".checkNOHP").val() ;
			
      if(notlp.indexOf('0') == 0){
				
				$('.checkNOHP').val('');
			}
      if($("#txt_kode_operator option:selected").val() == "62"){
		  var subStr = notlp.substring(0,1);
		  if(subStr != 8){

			console.log("tidak sama dengan 8");
			$('.checkNOHP').val('');			  
			// alert("Format Nomor Hp Tidak Valid");
			$("#btn_lanjut").prop("disabled", true);
			

		  }
		  else{

			
			console.log("sama dengan 8");
			$("#btn_lanjut").prop("disabled", false);

		  }
		}

      var regex = new RegExp (/^[1-9]*$/);
      $(".div_hp").removeClass("has-error has-success is-invalid");
      $(".div_hp label").remove();
      $(".div_hp").prepend('<label class="control-label" for="txt_notlp_pribadi"><i class="fa fa-spin fa-spinner"></i></label>');

      if (checkNOHP) {
          clearTimeout(checkNOHP);
      }

      checkNOHP = setTimeout(function () {

          $.ajax({
              url :  "/user/checkPhone/"+notlp,
              method : "get",
              success:function(data)
              {
                $(".div_hp label").remove();
                if (data.status == "ada") {
                  $(".div_hp").addClass("has-error is-invalid");
                  $(".div_hp").prepend('<label style="color:red;" for="txt_notlp_pribadi"><i class="fa fa-times-circle-o"></i> No HP Sudah Terdaftar</label>');
                  // $("#btn_lanjut").prop("disabled", true);
                } else {
                  $(".div_hp").removeClass("has-error is-invalid");
                  $(".div_hp").addClass("has-success is-valid");
                  $(".div_hp").prepend('<label for="txt_notlp_pribadi"><i class="fa fa-check"></i> No HP Belum Terdaftar</label>');
                  // $("#btn_lanjut").prop("disabled", false);
                }
                
              }
          });
        
     }, 50);
    }

    var checkNOKTP = false; 
    function check_ktp() {
        var noKTP = $(".checkNOKTP").val() ;
        // var notlp = $(".checkNOHP").val() ;
			
        if(noKTP.indexOf('0') == 0){
            
            $('.checkNOKTP').val('');
  
          }
  
          var subStr = noKTP.substring(0,1);
          if(subStr == 0 || subStr == 4){
            $('.checkNOKTP').val('');
          }
        var regex = new RegExp (/^[1-9]*$/);
        $(".div_ktp").removeClass("has-error has-success is-invalid");
        $(".div_ktp label").remove();
        $(".div_ktp").prepend('<label class="control-label"><i class="fa fa-spin fa-spinner"></i></label>');

        if (checkNOKTP) {
            clearTimeout(checkNOKTP);
        }
        checkNOKTP = setTimeout(function () {
          $.ajax({
            url :  "/user/checkKTP/"+noKTP,
            method : "get",
            success:function(data)
            {
              console.log(data);
              $(".div_ktp label").remove();
              if (data.status == "ada") {
                $(".div_ktp").addClass("has-error is-invalid");
                $(".div_ktp").prepend('<label style="color:red;" ><i class="fa fa-times-circle-o"></i> No KTP Sudah Terdaftar</label>');
                // $("#btn_lanjut").prop("disabled", true);
              } else {
                $(".div_ktp").addClass("has-success is-valid");
                $(".div_ktp").removeClass("has-error is-invalid");
                $(".div_ktp").prepend('<label ><i class="fa fa-check"></i> NO KTP Belum Terdaftar</label>');
                // $("#btn_lanjut").prop("disabled", false);
              }
              
            }
        });
        }, 50);

      }

      var checkNIKAW = false; 
      function chek_nik_aw() {
        var noKTP = $(".checkNIKAW").val() ;
        
        var regex = new RegExp (/^[1-9]*$/);
        $(".div_nik_aw").removeClass("has-error has-success is-invalid");
        $(".div_nik_aw label").remove();
        $(".div_nik_aw").prepend('<label class="control-label"><i class="fa fa-spin fa-spinner"></i></label>');

        if (checkNIKAW) {
            clearTimeout(checkNIKAW);
        }
        checkNIKAW = setTimeout(function () {
          $.ajax({
            url :  "/user/checkKTP/"+noKTP,
            method : "get",
            success:function(data)
            {
              console.log(data);
              $(".div_nik_aw label").remove();
              if (data.status == "ada") {
                $(".div_nik_aw").addClass("has-error is-invalid");
                $(".div_nik_aw").prepend('<label style="color:red;" ><i class="fa fa-times-circle-o"></i> No KTP Sudah Terdaftar</label>');
                $("#btn_lanjut").prop("disabled", true);
              } else {
                $(".div_nik_aw").addClass("has-success is-valid");
                $(".div_nik_aw").prepend('<label ><i class="fa fa-check"></i> NO KTP Belum Terdaftar</label>');
                $("#btn_lanjut").prop("disabled", false);
              }
              
            }
        });
        }, 50);

      }

      var checkHPAW = false; 
      function check_hp_aw() {
        var noHP = $(".checkHPAW").val() ;
        
        if(noHP.indexOf('0') == 0){
            
          $('.checkHPAW').val('');

        }
        
        
        if($("#txt_kode_operator_aw option:selected").val() == "62"){
          var subStr = noHP.substring(0,1);
          if(subStr != 8){

          console.log("tidak sama dengan 8");
          $('.checkHPAW').val('');			  
          // alert("Format Nomor Hp Tidak Valid");
          $("#btn_lanjut").prop("disabled", true);
          

          }
          else{

          
          console.log("sama dengan 8");
          $("#btn_lanjut").prop("disabled", false);

          }
        }
		
	    }
      $(document).on("change", "#domisili_negara" , function(){
		
		
		if($(this).val() == 0) {
			$("#provinsi").val("").prop("disabled", false);
			$("#kota").val("").prop("disabled", false);
			$("#kecamatan").val("").prop("disabled", false);
			$("#kelurahan").val("").prop("disabled", false);
			$("#kode_pos").val("").prop("disabled", false);
		}else{
			$("#provinsi").val(99).prop("selected", true).prop("disabled", true);
			$("#kota").prepend("<option value='99' selected> Lain-lain </option>").prop("disabled", true);
			$("#kecamatan").val("Lain-lain").prop("disabled", true);
			$("#kelurahan").val("Lain-lain").prop("disabled", true);
			$("#kode_pos").val("Lain-lain").prop("disabled", true);
		}
	});
    
    $('#demo').steps({
      onChange: function (currentIndex, newIndex, stepDirection) {
        function cekValueHiddenUser(){
            var el = $('#user_val').val();
            console.log(el)
            if (el == '0')
            {
               return 'kosong';
            }
            else
            {
                return el;
            }
        }

        function cekValueHiddenKtp(){
            var el = $('#ktp_val').val();
            console.log(el)
            if (el == '0')
            {
               return 'kosong';
            }
            else
            {
                return el;
            }
        }

        function cekValueHiddenUserKtp(){
            var el = $('#user_ktp_val').val();
            console.log(el)
            if (el == '0')
            {
               return 'kosong';
            }
            else
            {
                return el;
            }
        }
        function checkLengthHP(){
            var noHP = $('#no_telp').val();
            if (noHP.length < 10)
            {
               return 'error';
            }
            else
            {
                return noHP;
            }
        }
        var valueHiddenUser = cekValueHiddenUser();
        var valueHiddenKtp = cekValueHiddenKtp();
        var valueHiddenUserKtp = cekValueHiddenUserKtp();
        var checkLengthHP = checkLengthHP();
        console.log('onChange', currentIndex, newIndex, stepDirection);
        // tab1
        // if (currentIndex === 3) {
          // if (valueHiddenUser !== 'kosong')
          // {
          //     return true;
          // }
          
          if (stepDirection === 'forward') {
              var valid = frmInfo.valid();
              // console.log(valid)
              if (valueHiddenUser == 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp == 'kosong' && valid == true)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser != 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp == 'kosong' && valid == true)
              {
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser != 'kosong' && valueHiddenKtp != 'kosong' && valueHiddenUserKtp == 'kosong' && valid == true)
              {
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser == 'kosong' && valueHiddenKtp != 'kosong' && valueHiddenUserKtp == 'kosong' && valid == true)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser == 'kosong' && valueHiddenKtp != 'kosong' && valueHiddenUserKtp != 'kosong' && valid == true)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser == 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp != 'kosong' && valid == true)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser != 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp != 'kosong' && valid == true)
              {
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser == 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp == 'kosong' && valid == false)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser == 'kosong' && valueHiddenKtp != 'kosong' && valueHiddenUserKtp == 'kosong' && valid == false)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser == 'kosong' && valueHiddenKtp != 'kosong' && valueHiddenUserKtp != 'kosong' && valid == false)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser == 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp != 'kosong' && valid == false)
              {
                  $('#tampil_error_diri').empty().append('<label id="error_label_user" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser != 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp == 'kosong' && valid == false)
              {
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser != 'kosong' && valueHiddenKtp != 'kosong' && valueHiddenUserKtp == 'kosong' && valid == false)
              {
                  $('#tampil_error_ktp_diri').empty().append('<label id="error_label_ktp_diri" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              if (valueHiddenUser != 'kosong' && valueHiddenKtp != 'kosong' && valueHiddenUserKtp != 'kosong' && valid == false)
              {
                  return false;
              }
              if (valueHiddenUser != 'kosong' && valueHiddenKtp == 'kosong' && valueHiddenUserKtp != 'kosong' && valid == false)
              {
                  $('#tampil_error_ktp').empty().append('<label id="error_label_ktp" style="color:red;">Harus Diisi</label>')
                  return false;
              }
              else
              {
                  return true;
              }
          }
          if (stepDirection === 'backward') {
            frmInfoValidator.resetForm();
          }
        // }

        // tab2
        // if (currentIndex === 1) {
        //   if (stepDirection === 'forward') {
        //     var valid = frmInfo.valid();
        //     return valid;
        //   }
        //   if (stepDirection === 'backward') {
        //     frmInfoValidator.resetForm();
        //   }
        // }

        // tab3
        // if (currentIndex === 4) {
        //   if (stepDirection === 'forward') {
        //     var valid = frmMobile.valid();
        //     return valid;
        //   }
        //   if (stepDirection === 'backward') {
        //     frmMobileValidator.resetForm();
        //   }
        // }

        return true;

      },
      onFinish: function () {
        // alert('Wizard Completed');
        //frmInfo.submit();
        // var rek = $('#rekening').val();
        // var pemilik_rek = $('#nama_pemilik_rek').val();
        // var bank = $('#bank option:selected').val();
        // if (rek != '' && pemilik_rek != '' && bank != '')
        // {
        //     $('#selesai').attr('data-toggle','modal').attr('data-target','#otp');
        //     return true;
        // }
      }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // generate tahun
      var select = document.getElementById('thn_lahir'),
          year = new Date().getFullYear(),
          html = '<option value="">--Pilih--</option>',
          data_old = {{ old('thn_lahir') !== null ? old('thn_lahir') : 0 }};
      for(i = year; i >= year-100; i--) {
        html += '<option value="' + i + '" '+(data_old !== 0 && data_old == i ? "selected" : "")+'>' + i + '</option>';
      }
      select.innerHTML = html;
    // end generate tahun

    // var provinsi = ($('#provinsi option:selected').val() !== '' ? $('#provinsi option:selected').val() : 0),
    //     url = ($('#provinsi option:selected').val() !== '' ? '/getKota/'+provinsi : ''),
    //     kota = "{{ old('kota') !== null ? old('kota') : '' }}";
    // console.log(provinsi+' '+url)
    // $.ajax({
    //     url : url,
    //     method : "get",
    //     success:function(data)
    //     {
    //         $.each(data.kota,function(index,value){
    //             if (value.kode_kota == kota)
    //             {
    //                 var select = 'selected=selected';
    //             }
    //             else
    //             {
    //                 var select = '';
    //             }
    //             $('#kota').append(
    //                 '<option value="'+value.kode_kota+'"'+ select+'>'+value.nama_kota+'</option>'
    //             );
    //         })
    //     }
    // });

    $(document).ready(function() {
		$('#id_check_poto').on('click',function(){
          if($(this).is(':checked'))
          {
              $('#divHidePoto').fadeIn();
              // alert('checked')
          }
          else
          {
              $('#divHidePoto').fadeOut();
          }
      })
      $('#npwp_check').on('click',function(){
          if($(this).is(':checked'))
          {
              $('#show_npwp').fadeIn();
              // alert('checked')
          }
          else
          {
              $('#show_npwp').fadeOut();
          }
      })
    })

    $('#preview-1').on('click',function(){
        $('#file-1').trigger('click');
    })

    $('#preview-2').on('click',function(){
        $('#file-2').trigger('click');
    })

    $('#preview-3').on('click',function(){
        $('#file-3').trigger('click');
    })

    $('#selesai').on('click',function(){
        var valid = frmInfo.valid();
		if(valid == false){
			return false;
		}else{
			var rek = $('#rekening').val();
			var pemilik_rek = $('#nama_pemilik_rek').val();
			var bank = $('#bank option:selected').val();
			if (rek == '' && pemilik_rek == '' && bank == '')
			{
			  
			  $('#rekening').focus();
			  $('#nama_pemilik_rek').focus();
			  $('#bank option:selected').focus();
			  return false;

			}
			else if($("#no_telp").val().length < 9 ){

			  swal("Gagal", "Pastikan Jumlah Digit Nomor HP Anda", "error");
			  $("#no_telp").focus();
			  return false;

			}
			else if($("#no_hp_ahli_waris").val().length < 9 ){

			  swal("Gagal", "Pastikan Jumlah Digit Nomor HP Ahli Waris", "error");
			  $("#no_hp_ahli_waris").focus();
			  return false;

			}

			// else if($("#no_ktp").val().length < 16 ){
			else if($("input[name=jenis_identitas] :checked").val() == 1 ){

			  if($("#no_ktp").val().length < 16 ){

          swal("Gagal", "Pastikan Jumlah Digit KTP Anda", "error");
				$("#no_ktp").focus();
				return false;

			  }

			}
      else if($(".div_ktp").hasClass("is-invalid")){
            swal("Gagal", "No KTP Sudah Terdaftar", "error");
				    $(".div_ktp").focus();
				    return false;
        }
        else if($(".div_hp").hasClass("is-invalid")){
            swal("Gagal", "No HP Sudah Terdaftar", "error");
				    $(".div_hp").focus();
				    return false;
        }
			
			else{
				
				$('#selesai').attr('data-toggle','modal').attr('data-target','#otp');
				$('#kirim_lagi').prop('disabled',true);
				$('#no_hp').val($("#txt_kode_operator option:selected").val()+$('#no_telp').val());
				  noHP = $('#no_hp').val();
				  console.log(noHP)
				  $.ajax({
					  url : "/user/kirimOTP/",
					  method : "post",
					  dataType: 'JSON',
					  data: {hp:noHP},
					  success:function(data)
					  {
						console.log(data.status)
					  }
				  });
				  timerDisableButton();
				return true;
		   }
		}   
    })

    // $('#kirim_lagi').on('click',function(){
    //     $('#kirim_lagi').prop('disabled',true);
    //     // $('#no_hp').val($("#txt_kode_operator option:selected").val()+$('#no_telp').val());
    //     noHP = $("#txt_kode_operator option:selected").val()+$('#no_telp').val();
    //     console.log(noHP)
    //     $.ajax({
    //         url : "/user/kirimOTP/",
    //         method : "post",
    //         dataType: 'JSON',
    //         data: {hp:noHP},
    //         success:function(data)
    //         {
    //             console.log(data.status)
    //         }
    //     });
    //     timerDisableButton();
    // });

    // $('#kode_otp').on('keyup',function(){
    //     console.log($('#kode_otp').val().length)
    //     if ($('#kode_otp').val().length == 6)
    //     {
    //         $('#kirim_data').removeAttr('disabled');

    //     }
    //     else
    //     {
    //         $('#kirim_data').prop('disabled',true);
    //     }
    // })
    
    // $('#kirim_data').on('click',function(){
    //    $('#kirim_data').prop("disabled", true);
    //     $.ajax({
    //         url : "/user/cekOTP/",
    //         method : "post",
    //         dataType: 'JSON',
    //         data: {otp:$('#kode_otp').val()},
    //         beforeSend: function() {
    //           $("#overlay").css('display','block');
    //         },
    //         success:function(data)
    //         {
    //           if (data.status == '00')
    //             {
    //                 console.log("sukses" + data.status);
    //                 $('#example-advanced-form').attr('action','{{route('firstUpdateProfile')}}').attr('method','POST').attr('enctype','multipart/form-data');
    //                 $('#example-advanced-form').submit();
    //                 $('#overlay').css('display','none');
    //                 $('#kirim_data').prop("disabled", true);
    //             }
    //             else
    //             {
    //                 $('#kirim_data').prop("disabled", false);
    //                 // $('#otp').modal('hide');
    //                 $('#overlay').css('display','none');
    //                 $('#alertError').css('display','block').text("OTP tidak sama")
    //             }
    //             $('#overlay').css('display','none');
    //         }
    //     });
         
    // })
    
</script>
@endpush