@extends('layouts.user.sidebar')

@section('title', 'Data Identitas')

@section('style')
{{-- <link rel="stylesheet" id="css-main" href="{{url('assetsBorrower/css/codebase.min.css')}}">
<link rel="stylesheet" id="css-theme" href="{{url('assetsBorrower/css/themes/flat.css')}}"> --}}
@endsection

@section('content')
<div class="row">

  @if ($detil->tipe_pengguna == '2')
  {{-- START: Badan Hukum --}}
  <div class="col-12">
    <div id="layout-badan-hukum">
      @include('pages.user.manage_profile_badan_hukum')
    </div>
  </div>
  {{-- END: Badan Hukum --}}

  @else
  {{-- START: Individu --}}
  <div id="layout-individu">
    @include('pages.user.manage_profile_individu')
  </div>
  @endif
  {{-- END: Individu --}}
</div>

@endsection

@section('script')
@stack('add-style')

<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/jquery_step/jquery.steps.js') }}"></script>
<script src="{{ asset('assetsBorrower/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
{{-- <script src="{{ asset('js/jquery_step/jquery.validate.min.js') }}"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

<script>
  Webcam.set({
    width:180,
    height:200,
    image_format: 'jpg',
    jpeg_quality: 90,
    flip_horiz: false
  });
</script>
@stack('add-script')
@endsection