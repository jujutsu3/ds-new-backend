<html>

<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Document Wakalah Bil Ujroh</title>
</head>

<body>
    <div class='privy-document'></div>
    <p id="response_ttd"></p>
    <script src='https://unpkg.com/privy-sdk'></script>
    <script src='https://code.jquery.com/jquery-3.5.1.min.js'></script>

    <script>
        var token = "@php echo $token; @endphp";
        var privyID = "@php echo $privyID; @endphp";
        var page = "@php echo $page; @endphp";
        var x = "@php echo $x; @endphp";
        var y = "@php echo $y; @endphp";
        var isDev = "@php echo config('app.isDev'); @endphp"

        Privy.openDoc(token, {
                dev: isDev,
                container: '.privy-document',
                privyId: privyID,
                signature: {
                    page: '',
                    x: '',
                    y: '',
                    fixed: true
                }
            })
            .on('after-action', (data) => {
                // 
            })
            .on('after-sign', (data) => {
                //

            })
            .on('after-review', (data) => {
                console.log('ttd after-review');
            })
    </script>
</body>

</html>");