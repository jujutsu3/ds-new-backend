@extends('layouts.user.sidebar')

@section('title', 'User Verification')

@section('style')
<link rel="stylesheet" href="{{ asset('css/jquery_step/new/jquery-steps.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery_step/new/style.css') }}">
@endsection

@section('content')

<div id="overlay">
  <div class="cv-spinner">
    <span class="spinner"></span>
  </div>
</div>
<div class="row">
  @if (session('error'))
  <div class="alert alert-danger col-sm-12">
    {{ session('error') }}
  </div>
  @endif
  @if (session('success'))
  <div class="alert alert-success col-sm-12">
    {{ session('success') }}
  </div>
  @endif
  @if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
</div>
@if($confirmationemail)
<div class="row">
  <div class="col-sm-12 col-lg-8 mx-auto">
    <?php
            $virtual_account = false
          ?>
    <div class="card bg-dark text-light">
      <div class="card-body text-center">

        <h2 style="color:white">Silahkan Konfirmasi Email pada email yang anda daftarkan </h2>
        <p style="color:white">Silahkan cek folder spam jika email tidak ditemukan</p>

      </div>
    </div>
  </div>
</div>
@elseif($suspend)
<div class="card bg-dark text-light">
  <div class="card-body text-center">
    <h2 style="color:white">Akun anda telah Kami suspend !</h2>
    <p style="color:white">Silahkan hubungi pihak kami untuk aktivasi kembali akun</p>
  </div>
</div>
@elseif($dataverification)
<div class="row mb-4">
  <div class="col-sm-12 col-lg-8 mx-auto">
    <div class="card bg-dark text-light">
      <div class="card-body text-center">
        <h2 style="color:white">Silahkan isikan data diri anda dengan data yang benar dan asli :</h2>

      </div>
    </div>
  </div>
</div>

{{-- START: Modal OTP --}}
<div class="modal fade" id="otp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
  data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header mb-3">
        <h5 class="modal-title" id="scrollmodalLabel">Verifikasi Nomor Handphone</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetTimeout()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="alert alert-success col-sm-12" id="alertError" style="display: none;">
      </div>

      <div class="modal-body">
        <form id="form_otp">
          @csrf
          <div class="form-group row ml-1">
            <label class="col-sm-3 col-form-label">No HP</label>
            <div class="col-sm-9">
              <input type="text" class="form-control col-sm-10" value="" name="no_hp" id="no_hp" disabled>
            </div>
          </div>
          <div class="form-group row ml-1">
            <label class="col-sm-3 col-form-label">Kode OTP</label>
            <div class="col-sm-9">
              <input type="text" class="form-control col-sm-6" value="" name="kode_otp" id="kode_otp">
            </div>
          </div>
          <label class="col-12 col-form-label"><p>Silahkan masukan kode OTP yang dikirimkan melalui SMS ke nomor handphone Anda untuk proses verifikasi</p></label>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-info" id="kirim_lagi">Kirim Lagi <span id="count"></span></button>
        <button type="button" class="btn btn-sm btn-success" id="kirim_data" disabled>Kirim</button>
      </div>
    </div>
  </div>
</div>
{{-- END: Modal OTP --}}

<input type="hidden" name="investor_type" id="investor_type" value="">
{{-- START: Badan Hukum --}}
<div id="layout-badan-hukum" class="d-none mb-60">
  @include('pages.user.verif_user_badan_hukum')
</div>
{{-- END: Badan Hukum --}}

{{-- START: Individu --}}
<div id="layout-individu" class="d-none">
  @include('pages.user.verif_user_individu')
</div>
{{-- END: Individu --}}
@endif

@endsection

@section('script')
@stack('add-style')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
{{-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> --}}
{{-- <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script> --}}
<script src="{{ asset('js/jquery_step/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/jquery_step/new/jquery-steps.js') }}"></script>
<script src="{{ asset('assetsBorrower/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

{{-- START: Init --}}
<script>
  let user_status = "{{ $dataverification }}"
  let tipe_penguna = "{{ $detil_investor ? $detil_investor->tipe_pengguna : ''}}"
  if (user_status && tipe_penguna != '2') {
    swal.fire({
      title: "Lengkapi Profile",
      text: "Mendaftar sebagai ?",
      type : "question",
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Individu",
      cancelButtonText: "Perusahaan",
      confirmButtonColor: "#70B29C",
      cancelButtonColor: "#FFCA28",
    }).then( function (response) {
      if (response.value){
        $('#investor_type').val("1");
        $('#layout-individu').removeClass('d-none');
        $('#layout-badan-hukum').remove();
      } else {
        $('#investor_type').val("2");
        $('#layout-badan-hukum').removeClass('d-none');
        $('#layout-individu').remove();
      }
    });
  } else {
    $('#investor_type').val("2");
    $('#layout-badan-hukum').removeClass('d-none');
    $('#layout-individu').remove();
  }

  Webcam.set({
    width:180,
    height:200,
    image_format: 'jpg',
    jpeg_quality: 90,
    flip_horiz: false
  });
</script>
{{-- END: Init --}}

{{-- START: Function --}}
<script>
  var timer = null;
  function timerDisableButton(){
    var spn = document.getElementById("count");
    var btn = document.getElementById("kirim_lagi");

    var count = 30;     // Set count
    timer = null;  // For referencing the timer
    (function countDown(){
      // Display counter and start counting down
      spn.textContent = count;
      
      // Run the function again every second if the count is not zero
      if(count !== 0){
        timer = setTimeout(countDown, 1000);
        count--; // decrease the timer
      } else {
        // Enable the button
        btn.removeAttribute("disabled");
        spn.removeText
      }
    }());
  }

  const resetTimeout = () => {
    clearTimeout(timer);
  }
</script>
{{-- END: Function --}}

@stack('add-script')

@endsection