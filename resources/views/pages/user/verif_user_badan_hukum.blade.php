{{-- START: Super Lender --}}
{{-- START: Style --}}
@push('add-style')
<style>
  .line {
    display: flex;
    flex-direction: row;
  }

  .line:after {
    content: "";
    flex: 1 1;
    border-bottom: 1px solid #000;
    margin: auto;
  }

  .bg-green-dsi {
    background-color: #18783A
  }

  @media screen and (max-width: 600px) {
    .step-title {
      visibility: hidden;
      clear: both;
      float: left;
      margin: 10px auto 5px 20px;
      width: 28%;
      display: none;
    }
  }

  .input-hidden2 {
    opacity: 0;
    height: 0;
    width: 0;
    padding: 0;
    margin: 0;
    float: right;
  }

  #no_tlp_badan_hukum-error {
    position: absolute;
    top: 100%;
  }
</style>
@endpush
{{-- END: Style --}}

{{-- START: Form Super Lender --}}
<div class="col-sm-12 col-lg-12">
  <form action="{{route('firstUpdateProfile')}}" method="POST" id="form-super-lender" name="form-super-lender"
    enctype="multipart/form-data">
    @csrf
    <div id="step-super-lender">

      <div class="step-app">

        <ul class="step-steps">
          <li>
            <a class="py-3 text-center h-100" href="#step-super-lender1"><i class="fas fa-hotel mr-2"></i>
              <span class="step-title">Informasi Badan Hukum</span>
            </a>
          </li>
          <li>
            <a class="py-3 text-center h-100" href="#step-pengurus"><i class="fas fa-user mr-2"></i>
              <span class="step-title">Informasi Pengurus</span>
            </a>
          </li>
        </ul>

        <div class="step-content pt-4">
          <div class="step-tab-panel" id="step-super-lender1">

            <!-- START: Baris 1 -->
            <div class="form-row">
              <input type="hidden" name="tipe_pengguna" id="tipe_pengguna" value="2">
              <input type="hidden" name="tab" id="tab">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="nama_badan_hukum">Nama Badan Hukum <i class="text-danger">*</i></label>
                  <input type="text" name="nama_badan_hukum" class="form-control checkKarakterAneh"
                    placeholder="Masukkan nama badan hukum" minlength="3" maxlength="50"
                    value="{{ $detil_investor ? $detil_investor->nama_perusahaan : ''}}" required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="nib_badan_hukum">Nomor Surat Izin <i class="text-danger">*</i></label>
                  <input class="form-control" type="text" id="nib_badan_hukum" name="nib_badan_hukum" minlength="3"
                    maxlength="50" placeholder="SIUP/TDP/Nomor surat izin lainnya"
                    value="{{ $detil_investor ? $detil_investor->nib : ''}}" required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="npwp_bdn_hukum">NPWP Perusahaan<i class="text-danger">*</i></label>
                  <input class="form-control" type="text" pattern=".{15,15}"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()" minlength="15"
                    maxlength="15" id="npwp_bdn_hukum" name="npwp_bdn_hukum" placeholder="Masukkan nomor NPWP"
                    value="{{ $detil_investor ? $detil_investor->npwp_perusahaan : ''}}" required>
                </div>
              </div>
            </div>
            <!-- END: Baris 1 -->

            <!-- START: Baris 2 -->
            <div class="form-row">
              <div class="col-md-4">
                <div class="form-group">
                  <label id="label_no_akta_pendirian_bdn_hukum" for="no_akta_pendirian_bdn_hukum">No Akta Pendirian <i
                      class="text-danger">*</i></label>
                  <input class="form-control" type="text" id="no_akta_pendirian_bdn_hukum"
                    name="no_akta_pendirian_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Pendirian"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                    value="{{ $detil_investor ? $detil_investor->no_akta_pendirian : ''}}" required>
                </div>
              </div>
              <div class=" col-md-4">
                <div class="form-group">
                  <label for="tgl_berdiri_badan_hukum">Tanggal Akta Pendirian <i class="text-danger">*</i></label>
                  <input class="form-control" type="date" id="tgl_berdiri_badan_hukum" name="tgl_berdiri_badan_hukum"
                    max="{{ date("Y-m-d")}}" placeholder="Masukkan Tanggal Berdiri" onchange="$(this).valid(); $('#tgl_akta_perubahan_bdn_hukum').attr('min', $(this).val())"
                    value="{{ $detil_investor ? $detil_investor->tgl_berdiri : ''}}" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label id="label_no_akta_perubahan_bdn_hukum" for="no_akta_perubahan_bdn_hukum">No Akta Perubahan
                    Terakhir</label>
                  <input class="form-control" type="text" id="no_akta_perubahan_bdn_hukum"
                    name="no_akta_perubahan_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Perubahan"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                    value="{{ $detil_investor ? $detil_investor->nomor_akta_perubahan : ''}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="tgl_akta_perubahanbdn_hukum">Tanggal Akta Perubahan Terakhir</label>
                  <input class="form-control" type="date" id="tgl_akta_perubahan_bdn_hukum"
                    name="tgl_akta_perubahan_bdn_hukum" max="{{ date("Y-m-d")}}"
                    placeholder="Masukkan Tanggal Akta Perubahan Terakhir" onchange="$(this).valid()"
                    value="{{ $detil_investor ? $detil_investor->tanggal_akta_perubahan : ''}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label id="label_no_tlp_badan_hukum">Nomor Telepon Perusahaan <i class="text-danger">*</i></label>
                  <div class="input-group">
                    <div class="input-group-append">
                      <span class="input-group-text bg-green-dsi text-white"> +62 </span>
                    </div>
                    <input class="form-control no-zero" type="text"
                      onkeyup="this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" id="no_tlp_badan_hukum"
                      name="no_tlp_badan_hukum" placeholder="Masukkan nomor perusahaan"
                      value="{{ $detil_investor ? substr($detil_investor->telpon_perusahaan, 2) : ''}}" required>
                  </div>
                </div>
              </div>
            </div>
            <!-- END: Baris 2 -->

            <!-- START: Baris 3 -->
            <div class="row">
              <div class="col-12">
                <label>Foto NPWP Perusahaan <i class="text-danger">*</i></label>
              </div>
              <div class="col-4">
                <div id="preview_camera_npwp_badan_hukum" class="pt-3">
                  <img class="imagePreview" id="preview-1"
                    src="{{ $detil_investor ? route('getUserFile', ['filename' => str_replace('/',':', $detil_investor->foto_npwp_perusahaan)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                    style="{{ $detil_investor ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                  <div id="btn_camera_npwp_badan_hukum">
                    <label class="btn btn-primary mt-4">Kamera</label>
                  </div>
                </div>

                <div id="take_camera_npwp_badan_hukum" class="d-none">
                  <div class="col p-0">
                    <img id="user-guide img-fluid" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                      style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                    <div id="camera_npwp_badan_hukum"></div>
                    <input class="btn btn-primary" type="button" value="Ambil Foto" onClick="takeSnapshot()">
                    <input type="hidden" name="user_camera_npwp_badan_hukum" id="user_camera_npwp_badan_hukum"
                      class="image-tag">
                  </div>
                </div>
                <input class="input-hidden2" type="text" id="user_camera_npwp_badan_hukum_val"
                  value="{{ $detil_investor ? $detil_investor->foto_npwp_perusahaan : '' }}" required>
                <div id="result_camera_npwp" class="d-none">
                  <label class="my-3">Hasil</label>
                  <div id="result_npwp_perusahaan"></div>
                </div>
              </div>
            </div>
            <!-- END: Baris 3 -->

            <!-- START: Baris 4 -->
            <div class="form-row">
              <div class="col-12">
                <h6 class="line my-4 font-weight-bold">Alamat Sesuai AKTA &nbsp</h6>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-alamat">Alamat Lengkap <i class="text-danger">*</i></label>
                  <textarea class="form-control form-control-lg" maxlength="100" id="alamat_bdn_hukum"
                    name="alamat_bdn_hukum" rows="6" placeholder="Masukkan alamat lengkap Anda.."
                    required>{{ $detil_investor ? $detil_investor->alamat_perusahaan : ''}}</textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-namapengguna">Provinsi <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="provinsi_bdn_hukum" name="provinsi_bdn_hukum"
                    onchange="provinsiChange(this.value, this.id, 'kota_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                    @foreach ($master_provinsi as $data)
                    <option value={{$data->kode_provinsi}}
                      {{ $detil_investor ? $data->kode_provinsi == $detil_investor->provinsi_perusahaan ? 'selected' : '' : ''}}>
                      {{$data->nama_provinsi}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-kota">Kota/Kabupaten <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="kota_bdn_hukum" name="kota_bdn_hukum"
                    onchange="kotaChange(this.value, this.id, 'kecamatan_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                  </select>
                </div>
              </div>
            </div>
            <!-- END: Baris 4 -->

            <!-- START: Baris 5 -->
            <div class="form-row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-kecamatan">Kecamatan <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="kecamatan_bdn_hukum" name="kecamatan_bdn_hukum"
                    onchange="kecamatanChange(this.value, this.id, 'kelurahan_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="kelurahan_bdn_hukum">Kelurahan <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="kelurahan_bdn_hukum" name="kelurahan_bdn_hukum"
                    onchange="kelurahanChange(this.value, this.id, 'kode_pos_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="kode_pos_bdn_hukum">Kode Pos <i class="text-danger">*</i></label>
                  <input class="form-control" type="text" maxlength="30" id="kode_pos_bdn_hukum"
                    name="kode_pos_bdn_hukum" placeholder="--"
                    value="{{ $detil_investor ? $detil_investor->kode_pos_perusahaan : ''}}" readonly>
                </div>
              </div>
            </div>
            <!-- END: Baris 5 -->

            {{-- START: Baris 6 --}}
            <div class="form-row">
              <div class="col-12">
                <h6 class="line my-4 font-weight-bold">Informasi Rekening &nbsp</h6>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="rekening_bdn_hukum">No Rekening <i class="text-danger">*</i></label>
                  <input type="text" minlength="10" maxlength="16" name="rekening_bdn_hukum" id="rekening_bdn_hukum"
                    pattern=".{10,16}" onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                    class="form-control" placeholder="No Rekening"
                    value="{{ $detil_investor ? $detil_investor->rekening : ''}}" required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="rekening_bdn_hukum checkKarakterAneh">Nama Pemilik Rekening <i
                      class="text-danger">*</i></label>
                  <input type="text" maxlength="35" name="nama_pemilik_rek_bdn_hukum" id="nama_pemilik_rek_bdn_hukum"
                    class="form-control" placeholder="Nama Pemilik Rekening"
                    value="{{ $detil_investor ? $detil_investor->nama_pemilik_rek : ''}}" required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="bank_bdn_hukum">Bank <i class="text-danger">*</i></label>
                  <select name="bank_bdn_hukum" class="form-control custom-select" id="bank_bdn_hukum" required>
                    <option value="">--Pilih--</option>
                    @foreach($master_bank as $b)
                    <option value="{{ $b->kode_bank }}"
                      {{ $detil_investor ? $b->kode_bank == $detil_investor->bank_investor ? 'selected' : '' : ''}}>
                      {{ $b->nama_bank }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            {{-- END: Baris 6 --}}

            <!-- START: Informasi Lain Lain Title -->
            <div class="row">
              <div class="col-12">
                <h6 class="line my-4 font-weight-bold">Informasi Lain Lain &nbsp</h6>
              </div>
            </div>
            <!-- END: Informasi Lain Lain -->

            <!-- START: Baris 7 -->
            <div class="form-row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="bidang_pekerjaan_bdn_hukum">Bidang Usaha <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="bidang_pekerjaan_bdn_hukum"
                    name="bidang_pekerjaan_bdn_hukum" required>
                    <option value="">Pilih Bidang Usaha</option>
                    @foreach ($master_bidang_pekerjaan as $b)
                    @if ($b->kode_bidang_pekerjaan != 'e72' && $b->kode_bidang_pekerjaan != 'e00' && $b->kode_bidang_pekerjaan != 'e01')
                      <option value="{{$b->kode_bidang_pekerjaan}}"
                        {{ $detil_investor ? $b->kode_bidang_pekerjaan == $detil_investor->bidang_pekerjaan ? 'selected' : '' : ''}}>
                        {{$b->bidang_pekerjaan}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="omset_tahun_terakhir">Omset Tahun Terakhir<i class="text-danger">*</i></label>
                  <input type="text" maxlength="30" name="omset_tahun_terakhir" id="omset_tahun_terakhir"
                    class="form-control no-zero" placeholder="Masukkan omset tahun terakhir"
                    onkeyup="$(this).val(formatRupiah($(this).val()))" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="tot_aset_tahun_terakhr">Total Aset Tahun Terakhir <i class="text-danger">*</i></label>
                  <input type="text" maxlength="30" name="tot_aset_tahun_terakhr" id="tot_aset_tahun_terakhr"
                    class="form-control no-zero" placeholder="Masukkan total aset tahun terakhir"
                    onkeyup="$(this).val(formatRupiah($(this).val()))" required>
                </div>
              </div>
            </div>
            <!-- END: Baris 7 -->

            {{-- START: Baris 8 --}}
            <div class="form-row">
              <div class="col-md-4">
                <label id="label_laporan_keuangan_bdn_hukum" for="laporan_keuangan_bdn_hukum">Laporan Keuangan <i
                    class="text-danger">*</i></label>
                <div class="custom-file">
                  <input id="laporan_keuangan_bdn_hukum" name="laporan_keuangan_bdn_hukum" type="file"
                    accept=".doc, .docx, .xls, .xlsx, .pdf" class="custom-file-input" required>
                  <label for="laporan_keuangan_bdn_hukum" class="custom-file-label text-truncate">Pilih File</label>
                </div>
              </div>

              <div class="col-md-12">
                <p><i class="text-danger h6">* &nbsp</i> <b> Format file .doc, docx, .xls, .xlsx, .pdf </b></p>
              </div>
            </div>
            {{-- END: Baris 8 --}}

            <p class="text-danger"><b>(*) Wajib Diisi</b></p>

          </div>

          <div class="step-tab-panel" id="step-pengurus">
            {{-- START: Pengurus --}}
            <div id="pengurus">
              <!-- START: Baris 1 -->
              <div class="form-row">
                <div class="col-12">
                  <h6 class="line my-4 font-weight-bold">Informasi Pengurus &nbsp</h6>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="nama_pengurus">Nama Pengurus <i class="text-danger">*</i></label>
                    <input class="form-control" type="text" id="nama_pengurus" minlength="3" maxlength="30"
                      name="nama_pengurus[]" placeholder="Masukkan Nama Pengurus..." required>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="jns_kelamin_pengurus">Jenis Kelamin <i class="text-danger">*</i></label>
                    <select id="jns_kelamin_pengurus" name="jns_kelamin_pengurus[]" class="form-control custom-select"
                      required>
                      <option value="">-- Pilih Satu --</option>
                      @foreach ($master_jenis_kelamin as $b)
                      <option value="{{$b->id_jenis_kelamin}}">
                        {{$b->jenis_kelamin}}
                      </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label id="label_ktp_pengurus" for="ktp_pengurus">Nomor KTP <i class="text-danger">*</i></label>
                    <input class="form-control" type="text" id="ktp_pengurus" name="ktp_pengurus[]"
                      placeholder="Masukkan nomor KTP" minlength="16" maxlength="16" pattern=".{16,16}"
                      onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()" required>
                  </div>
                </div>
              </div>
              <!-- END: Baris 1 -->

              {{-- START: Baris 2 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="tempat_lahir_pengurus">Tempat Lahir <i class="text-danger">*</i></label>
                    <input class="form-control" type="text" maxlength="35" id="tempat_lahir_pengurus"
                      name="tempat_lahir_pengurus[]" placeholder="Masukkan tempat lahir" required>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="tanggal_lahir_pengurus">Tanggal Lahir <i class="text-danger">*</i></label>
                    <input class="form-control" type="date" id="tanggal_lahir_pengurus" name="tanggal_lahir_pengurus[]"
                      onchange="$(this).valid()" max="<?= date("Y-m-d"); ?>" placeholder="Masukkan tanggal lahir"
                      required>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label for="txt_kode_operator_pengurus">Kode Negara <i class="text-danger">*</i></label>
                        <select class="form-control custom-select" id="txt_kode_operator_pengurus"
                          name="txt_kode_operator_pengurus[]" required>

                          @foreach ($master_kode_operator as $b)
                          <option value="{{$b->kode_operator}}" {{ $b->kode_operator == "62" ? 'selected' : '' }}>
                            ({{$b->kode_operator}}) {{$b->negara}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label id="label_no_telp_pengurus" for="no_telp_pengurus">Nomor Telepon <i
                            class="text-danger">*</i>
                        </label>
                        <input type="text" minlength="9" maxlength="13" name="no_telp_pengurus[]" id="no_telp_pengurus"
                          class="form-control no-zero eight"
                          onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                          placeholder="Contoh:8xxxxxxxxxx" required>
                      </div>
                    </div>
                  </div>

                </div>

              </div>
              {{-- END: Baris 2 --}}

              {{-- START: Baris 3 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group row">
                    <label for="agama_pengurus" class="col-12">Agama <i class="text-danger">*</i></label>
                    <div class="col-12">
                      <select class="form-control custom-select" id="agama_pengurus" name="agama_pengurus[]" required>
                        <option value="">-- Pilih Satu --</option>
                        @foreach ($master_agama as $b)
                        <option value="{{$b->id_agama}}" {{ old('agama_pengurus[]') == $b->agama ? 'selected' : '' }}>
                          {{$b->agama}}
                        </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="pendidikan_terakhir_pengurus">Pendidikan Terakhir <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="pendidikan_terakhir_pengurus"
                      name="pendidikan_terakhir_pengurus[]" required>
                      <option value="">-- Pilih Satu --</option>
                      @foreach ($master_pendidikan as $b)
                      <option value="{{$b->id_pendidikan}}"
                        {{ old('pendidikan_terakhir_pengurus[]') == $b->id_pendidikan ? 'selected' : '' }}>
                        {{$b->pendidikan}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="npwp_pengurus">Nomor NPWP <i class="text-danger">*</i></label>
                    <input class="form-control " type="text" maxlength="15" minlength="15" id="npwp_pengurus"
                      name="npwp_pengurus[]" pattern=".{15,15}"
                      onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                      placeholder="Masukkan nomor NPWP" required>
                  </div>
                </div>
              </div>
              {{-- END: Baris 3 --}}

              {{-- START: Baris 4 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="jabatan_pengurus">Jabatan <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="jabatan_pengurus" name="jabatan_pengurus[]" required>
                      <option value="">-- Pilih Satu --</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
              </div>
              {{-- END: Baris 4 --}}

              {{-- START: Baris 5 --}}
              <div class="form-row">
                <div class="col-12">
                  <h6 class="line my-4 font-weight-bold">Alamat Domisili Pengurus &nbsp</h6>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="alamat_pengurus">Alamat<i class="text-danger">*</i></label>
                    <textarea class="form-control form-control-lg" maxlength="90" id="alamat_pengurus"
                      name="alamat_pengurus[]" rows="6" placeholder="Masukkan alamat lengkap Anda.."
                      required></textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="provinsi_pengurus">Provinsi <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="provinsi_pengurus" name="provinsi_pengurus[]"
                      onchange="provinsiChange(this.value, this.id, 'kota_pengurus')" required>
                      <option value="">-- Pilih Satu --</option>
                      @foreach ($master_provinsi as $data)
                      <option value={{$data->kode_provinsi}}>
                        {{$data->nama_provinsi}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kota_pengurus">Kota/Kabupaten <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="kota_pengurus" name="kota_pengurus[]"
                      onchange="kotaChange(this.value, this.id, 'kecamatan_pengurus')" required>
                      <option value="">-- Pilih Satu --</option>
                    </select>
                  </div>
                </div>
              </div>
              {{-- END: Baris 5 --}}

              {{-- START: Baris 6 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kecamatan_pengurus">Kecamatan <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="kecamatan_pengurus" name="kecamatan_pengurus[]"
                      onchange="kecamatanChange(this.value, this.id, 'kelurahan_pengurus')" required>
                      <option value="">-- Pilih Satu --</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kelurahan_pengurus">Kelurahan <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="kelurahan_pengurus" name="kelurahan_pengurus[]"
                      onchange="kelurahanChange(this.value, this.id, 'kode_pos_pengurus')" required>
                      <option value="">-- Pilih Satu --</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kode_pos_pengurus">Kode Pos <i class="text-danger">*</i></label>
                    <input class="form-control" type="text" id="kode_pos_pengurus" name="kode_pos_pengurus[]"
                      placeholder="--" readonly>
                  </div>
                </div>
              </div>
              {{-- END: Baris 6 --}}

              {{-- START: Baris 7 --}}
              {{-- START: Foto Pengurus 1 --}}
              <div class="row">
                <div class="col-12">
                  <h6 class="line my-4 font-weight-bold">Foto Pengurus &nbsp</h6>
                </div>

                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto Diri <i class="text-danger">*</i></label>
                  <div id="preview_camera_diri_pengurus" name="preview_camera_diri_pengurus[]" class="pt-3">
                    <img class="imagePreview" id="preview-1">
                    <div>
                      <button class="btn btn-primary" type="button" id="btn_camera_diri_pengurus"
                        name="btn_camera_diri_pengurus[]">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_diri_pengurus" name="take_camera_diri_pengurus[]" class="d-none">
                    <div class="col p-0">
                      <img id="user-guide1" src="{{URL::to('assets/img/user-guide.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_diri_pengurus" name="camera_diri_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_diri_pengurus" name="take_snapshot_diri_pengurus[]">
                      <input type="hidden" id="user_camera_diri_pengurus" name="user_camera_diri_pengurus[]"
                        class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_diri_pengurus" name="result_camera_diri_pengurus[]" class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_diri_pengurus" name="result_diri_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_diri_pengurus_val"
                    name="user_camera_diri_pengurus_val[]" required>
                </div>

                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto KTP <i class="text-danger">*</i></label>
                  <div id="preview_camera_ktp_pengurus" name="preview_camera_ktp_pengurus[]" class="pt-3">
                    <img class="imagePreview" id="preview-1">
                    <div>
                      <button class="btn btn-primary" type="button" id="btn_camera_ktp_pengurus"
                        name="btn_camera_ktp_pengurus[]">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_ktp_pengurus" name="take_camera_ktp_pengurus[]" class="d-none">
                    <div class="col p-0">
                      <img id="user-guide2" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_ktp_pengurus" name="camera_ktp_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_ktp_pengurus" name="take_snapshot_ktp_pengurus[]">
                      <input type="hidden" id="user_camera_ktp_pengurus" name="user_camera_ktp_pengurus[]"
                        class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_ktp_pengurus" name="result_camera_ktp_pengurus[]" class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_ktp_pengurus" name="result_ktp_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_ktp_pengurus_val"
                    name="user_camera_ktp_pengurus_val[]" required>
                </div>

                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto Diri & KTP <i class="text-danger">*</i></label>
                  <div id="preview_camera_diri_dan_ktp_pengurus" name="preview_camera_diri_dan_ktp_pengurus[]"
                    class="pt-3">
                    <img class="imagePreview" id="preview-1">
                    <div>
                      <button class="btn btn-primary" id="btn_camera_diri_dan_ktp_pengurus"
                        name="btn_camera_diri_dan_ktp_pengurus[]" type="button">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_diri_dan_ktp_pengurus" name="take_camera_diri_dan_ktp_pengurus[]" class="d-none">
                    <div class="col p-0">
                      <img id="user-guide3" src="{{URL::to('assets/img/guide-diridanktp.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_diri_dan_ktp_pengurus" name="camera_diri_dan_ktp_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_diri_dan_ktp_pengurus" name="take_snapshot_diri_dan_ktp_pengurus[]">
                      <input type="hidden" id="user_camera_diri_dan_ktp_pengurus"
                        name="user_camera_diri_dan_ktp_pengurus[]" class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_diri_dan_ktp_pengurus" name="result_camera_diri_dan_ktp_pengurus[]"
                    class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_diri_dan_ktp_pengurus" name="result_diri_dan_ktp_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_diri_dan_ktp_pengurus_val"
                    name="user_camera_diri_dan_ktp_pengurus_val[]" required>
                </div>

                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto NPWP <i class="text-danger">*</i></label>
                  <div id="preview_camera_npwp_pengurus" name="preview_camera_npwp_pengurus[]" class="pt-3">
                    <img class="imagePreview" id="preview-1">
                    <div>
                      <button class="btn btn-primary" id="btn_camera_npwp_pengurus" name="btn_camera_npwp_pengurus[]"
                        type="button">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_npwp_pengurus" name="take_camera_npwp_pengurus[]" class="d-none">
                    <div class="col p-0">
                      <img id="user-guide4" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_npwp_pengurus" name="camera_npwp_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_npwp_pengurus" name="take_snapshot_npwp_pengurus[]">
                      <input type="hidden" id="user_camera_npwp_pengurus" name="user_camera_npwp_pengurus[]"
                        class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_npwp_pengurus" name="result_camera_npwp_pengurus[]" class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_npwp_pengurus" name="result_npwp_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_npwp_pengurus_val"
                    name="user_camera_npwp_pengurus_val[]" required>
                </div>

              </div>
              {{-- END: Foto Pengurus 1 --}}
              {{-- END: Baris 7 --}}
            </div>
            {{-- END: Pengurus --}}

            {{-- START: Pengurus N --}}
            <div id="add-new-form"></div>
            {{-- END: Pengurus N --}}

            <div class="row">
              <div class="col-md-6">
                <p class="text-danger float-left"><b>(*) Wajib Diisi</b></p>
              </div>
              <div class="col-md-6">
                <button id="add-more-pengurus" class="btn btn-success float-right" type="button"><i
                    class="fas fa-plus mr-2"></i>Tambah Data
                  Pengurus</button>
              </div>
            </div>
          </div>
        </div>

        <div class="step-footer">
          {{-- <div class="float-left"> --}}
          <button id="btn-refresh" type="button" class="step-btn float-left">Atur Ulang</button>
          {{-- </div> --}}
          {{-- <div class="float-right"> --}}
          <button data-direction="finish" class="step-btn float-right" id="finish">Simpan dan Kirim</button>
          <button data-direction="prev" id="prev" class="step-btn float-right mr-2">Sebelumnya</button>
          <button data-direction="next" id="next" class="step-btn float-right">Simpan dan Lanjutkan</button>
          {{-- </div> --}}
        </div>
      </div>
    </div>
  </form>
</div>
{{-- END: Form Super Lender --}}

{{-- START: Script --}}
@push('add-script')

{{-- START: Init Var --}}
<script>
  let n = 0;
  let start_from_tab = "{{ $detil_investor ? 1 : 0 }}"
  if (start_from_tab == '1') {
    $('#tab').val('2')
  } else {
    $('#tab').val('1')
  }
  const formatRupiah = (angka) => {
    let number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    
    if(ribuan){
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return rupiah;
  }
</script>
{{-- END: Init Var --}}

{{-- START: Camera Click Action --}}
<script>
  $(document).ready(function(){
    $("#btn_camera_npwp_badan_hukum").click(function(){
      Webcam.attach( '#camera_npwp_badan_hukum' );
      $("#preview_camera_npwp_badan_hukum").addClass('d-none');
      $("#take_camera_npwp_badan_hukum").removeClass('d-none')
    });
  });

  const cameraDiriPengurusClick = (x) => {
    let camera_diri_pengurus = $('div[name="camera_diri_pengurus[]"]')
    let preview_camera_diri_pengurus = $('div[name="preview_camera_diri_pengurus[]"]')
    let take_camera_diri_pengurus = $('div[name="take_camera_diri_pengurus[]"]')
    let btn_camera_diri_pengurus = $('button[name="btn_camera_diri_pengurus[]"]')

    $(btn_camera_diri_pengurus[x]).click(function() {
      if ($(camera_diri_pengurus[x]).length) {
          Webcam.attach(camera_diri_pengurus[x]);
          $(preview_camera_diri_pengurus[x]).addClass('d-none');
          $(take_camera_diri_pengurus[x]).removeClass('d-none');
      }
    });
  }

  const cameraKtpPengurusClick = (x) => {
    let camera_ktp_pengurus = $('div[name="camera_ktp_pengurus[]"]')
    let preview_camera_ktp_pengurus = $('div[name="preview_camera_ktp_pengurus[]"]')
    let take_camera_ktp_pengurus = $('div[name="take_camera_ktp_pengurus[]"]')
    let btn_camera_ktp_pengurus = $('button[name="btn_camera_ktp_pengurus[]"]')

    $(btn_camera_ktp_pengurus[x]).click(function() {
      if ($(camera_ktp_pengurus[x]).length) {
        Webcam.attach(camera_ktp_pengurus[x]);
        $(preview_camera_ktp_pengurus[x]).addClass('d-none');
        $(take_camera_ktp_pengurus[x]).removeClass('d-none');
      }
    });
  }

  const cameraDiriDanKtpPengurusClick = (x) => {
    let camera_diri_dan_ktp_pengurus = $('div[name="camera_diri_dan_ktp_pengurus[]"]')
    let preview_camera_diri_dan_ktp_pengurus = $('div[name="preview_camera_diri_dan_ktp_pengurus[]"]')
    let take_camera_diri_dan_ktp_pengurus = $('div[name="take_camera_diri_dan_ktp_pengurus[]"]')
    let btn_camera_diri_dan_ktp_pengurus = $('button[name="btn_camera_diri_dan_ktp_pengurus[]"]')

    $(btn_camera_diri_dan_ktp_pengurus[x]).click(function() {
      if ($(camera_diri_dan_ktp_pengurus[x]).length) {
        Webcam.attach(camera_diri_dan_ktp_pengurus[x]);
        $(preview_camera_diri_dan_ktp_pengurus[x]).addClass('d-none');
        $(take_camera_diri_dan_ktp_pengurus[x]).removeClass('d-none');
      }
    });
  }

  const cameraNpwpPengurusClick = (x) => {
    let camera_npwp_pengurus = $('div[name="camera_npwp_pengurus[]"]')
    let preview_camera_npwp_pengurus = $('div[name="preview_camera_npwp_pengurus[]"]')
    let take_camera_npwp_pengurus = $('div[name="take_camera_npwp_pengurus[]"]')
    let btn_camera_npwp_pengurus = $('button[name="btn_camera_npwp_pengurus[]"]')

    $(btn_camera_npwp_pengurus[x]).click(function() {
      if ($(camera_npwp_pengurus[x]).length) {
        Webcam.attach(camera_npwp_pengurus[x]);
        $(preview_camera_npwp_pengurus[x]).addClass('d-none');
        $(take_camera_npwp_pengurus[x]).removeClass('d-none');
      }
    });
  }

  cameraDiriPengurusClick(n);
  cameraKtpPengurusClick(n);
  cameraDiriDanKtpPengurusClick(n);
  cameraNpwpPengurusClick(n);
</script>
{{-- END: Camera Click Action --}}

{{-- START: Take Snapshot Camera --}}
<script>
  // START: Camera NPWP Badan Hukum  
  const takeSnapshot = () => {
    $("#result_camera_npwp").removeClass('d-none');
    Webcam.snap( function(data_uri) {
      $('#user_camera_npwp_badan_hukum').val(data_uri)
      $('#user_camera_npwp_badan_hukum_val').val('1').valid()
      // document.getElementById('result_npwp_perusahaan').innerHTML = `<img src="${data_uri}" style="width:200px;height:160px;" />`;
      $('#result_npwp_perusahaan').empty().html(`<img src="${data_uri}" style="width:200px;height:160px;" />`);
    });
  }
  // END: Camera NPWP Badan Hukum

  const takeSnapshotDiriPengurus = (x) => {
    let take_snapshot_diri_pengurus = $("input[name='take_snapshot_diri_pengurus[]']")
    let result_camera_diri_pengurus = $("div[name='result_camera_diri_pengurus[]']")
    let user_camera_diri_pengurus = $('input[name="user_camera_diri_pengurus[]"]')
    let user_camera_diri_pengurus_val = $('input[name="user_camera_diri_pengurus_val[]"]')
    let result_diri_pengurus = $('div[name="result_diri_pengurus[]"]')

    $(take_snapshot_diri_pengurus[x]).click(function() {
      $(result_camera_diri_pengurus[x]).removeClass('d-none');
        Webcam.snap( function(data_uri) {
          $(user_camera_diri_pengurus[x]).val(data_uri);
          $(user_camera_diri_pengurus_val[x]).val('1').valid();
          $(result_diri_pengurus[x]).html(`
            <img src="${data_uri}" style="width:200px;height:160px;" />
          `);
      });
    });
  }

  const takeSnapshotKtpPengurus = (x) => {
    let take_snapshot_ktp_pengurus = $("input[name='take_snapshot_ktp_pengurus[]']")
    let result_camera_ktp_pengurus = $("div[name='result_camera_ktp_pengurus[]']")
    let user_camera_ktp_pengurus = $('input[name="user_camera_ktp_pengurus[]"]')
    let user_camera_ktp_pengurus_val = $('input[name="user_camera_ktp_pengurus_val[]"]')
    let result_ktp_pengurus = $('div[name="result_ktp_pengurus[]"]')

    $(take_snapshot_ktp_pengurus[x]).click(function() {
      $(result_camera_ktp_pengurus[x]).removeClass('d-none');
      Webcam.snap( function(data_uri) {
        $(user_camera_ktp_pengurus[x]).val(data_uri);
        $(user_camera_ktp_pengurus_val[x]).val('1').valid();
        $(result_ktp_pengurus[x]).html(` 
          <img src="${data_uri}" style="width:200px;height:160px;" />
        `);
      });
    });
  }

  const takeSnapshotDiriDanKtpPengurus = (x) => {
    let take_snapshot_diri_dan_ktp_pengurus = $("input[name='take_snapshot_diri_dan_ktp_pengurus[]']")
    let result_camera_diri_dan_ktp_pengurus = $("div[name='result_camera_diri_dan_ktp_pengurus[]']")
    let user_camera_diri_dan_ktp_pengurus = $('input[name="user_camera_diri_dan_ktp_pengurus[]"]')
    let user_camera_diri_dan_ktp_pengurus_val = $('input[name="user_camera_diri_dan_ktp_pengurus_val[]"]')
    let result_diri_dan_ktp_pengurus = $('div[name="result_diri_dan_ktp_pengurus[]"]')

    $(take_snapshot_diri_dan_ktp_pengurus[x]).click(function() {
      $(result_camera_diri_dan_ktp_pengurus[x]).removeClass('d-none');
      Webcam.snap( function(data_uri) {
        $(user_camera_diri_dan_ktp_pengurus[x]).val(data_uri);
        $(user_camera_diri_dan_ktp_pengurus_val[x]).val('1').valid();
        $(result_diri_dan_ktp_pengurus[x]).html(`
          <img src="${data_uri}" style="width:200px;height:160px;" />
        `);
      });
    });
  }

  const takeSnapshotNpwpPengurus = (x) => {
    let take_snapshot_npwp_pengurus = $("input[name='take_snapshot_npwp_pengurus[]']")
    let result_camera_npwp_pengurus = $("div[name='result_camera_npwp_pengurus[]']")
    let user_camera_npwp_pengurus = $('input[name="user_camera_npwp_pengurus[]"]')
    let user_camera_npwp_pengurus_val = $('input[name="user_camera_npwp_pengurus_val[]"]')
    let result_npwp_pengurus = $('div[name="result_npwp_pengurus[]"]')
    $(take_snapshot_npwp_pengurus[x]).click(function() {
      $(result_camera_npwp_pengurus[x]).removeClass('d-none');
      Webcam.snap( function(data_uri) {
        $(user_camera_npwp_pengurus[x]).val(data_uri);
        $(user_camera_npwp_pengurus_val[x]).val('1').valid();
        $(result_npwp_pengurus[x]).html(`
          <img src="${data_uri}" style="width:200px;height:160px;" />
        `);
      });
    });
  }

  takeSnapshotDiriPengurus(n);
  takeSnapshotKtpPengurus(n);
  takeSnapshotDiriDanKtpPengurus(n);
  takeSnapshotNpwpPengurus(n);
</script>
{{-- END: Take Snapshot Camera --}}

{{-- START: Get Json Data --}}
<script>
  // Jabatan
  const getJabatan = (x) => {
    let jabatan = $('select[name="jabatan_pengurus[]"]')
    $.getJSON("/borrower/data_jabatan", function(data_jabatan){
      for(var i = 0; i<data_jabatan.length; i++){
        $(jabatan[x]).each(function() {
          $(this).append($('<option>', { 
            value: data_jabatan[i].id,
            text : data_jabatan[i].text
          }));
        });
      }
    })
  }

  let detil_investor = "{{ $detil_investor ? $detil_investor : '' }}"
  if (detil_investor) {
    
    // Tanggal Akte Perubahan
    let tgl_berdiri_bdn_hukum = "{{ $detil_investor ? $detil_investor->tgl_berdiri : ''}}"
    if (tgl_berdiri_bdn_hukum) {
      $('#tgl_akta_perubahan_bdn_hukum').attr('min', tgl_berdiri_bdn_hukum)
    }

    // Laporan Keuangan
    let laporan_keuangan = "{{ $detil_investor ? $detil_investor->laporan_keuangan : ''}}"
    if (laporan_keuangan) {
      $('#laporan_keuangan_bdn_hukum').removeAttr('required')
      var str = laporan_keuangan.lastIndexOf('/');
      var result = laporan_keuangan.substring(str + 1);
      
      $('.custom-file-label').text(result)
    }

    // START: Omset & Aset Bdn Hukum
    let omset_tahun_terakhir = "{{ $detil_investor ? (int)$detil_investor->omset_tahun_terakhir : ''}}"
    let tot_aset_tahun_terakhr = "{{ $detil_investor ? (int)$detil_investor->tot_aset_tahun_terakhr : ''}}"

    $('#omset_tahun_terakhir').val(formatRupiah( omset_tahun_terakhir))
    $('#tot_aset_tahun_terakhr').val(formatRupiah( tot_aset_tahun_terakhr))
    // END: Omset & Aset Bdn Hukum

    // START: History Alamat
    let provinsi_bdn_hukum = "{{ $detil_investor ? $detil_investor->provinsi_perusahaan : ''}}"
    let kota_bdn_hukum = "{{ $detil_investor ? $detil_investor->kota_perusahaan : ''}}"
    let kecamatan_bdn_hukum = "{{ $detil_investor ? $detil_investor->kecamatan_perusahaan : ''}}"
    let kelurahan_bdn_hukum = "{{ $detil_investor ? $detil_investor->kelurahan_perusahaan : ''}}"

    if (provinsi_bdn_hukum) {
      $.getJSON("/getKota/"+provinsi_bdn_hukum, function(data_kota){
        $.each(data_kota.kota,function(index,value){
            $('#kota_bdn_hukum').append(
                '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
            );
        })

        $(`#kota_bdn_hukum option[value='${kota_bdn_hukum}']`).prop('selected', true);
      });
    }

    if (kota_bdn_hukum) {
      $.getJSON("/getKecamatan/"+kota_bdn_hukum, function(data_kecamatan){
        for(let i = 0; i<data_kecamatan.length; i++){
          $('#kecamatan_bdn_hukum').append($('<option>', { 
              value: data_kecamatan[i].text,
              text : data_kecamatan[i].text
          }));

          $(`#kecamatan_bdn_hukum option[value='${kecamatan_bdn_hukum}']`).prop('selected', true);
        }
      }); 
    }

    if (kecamatan_bdn_hukum) {
      $.getJSON("/borrower/data_kelurahan/"+kecamatan_bdn_hukum, function(data_kelurahan){
        for(let i = 0; i<data_kelurahan.length; i++){
          $('#kelurahan_bdn_hukum').append($('<option>', { 
              value: data_kelurahan[i].text,
              text : data_kelurahan[i].text
          }));

          $(`#kelurahan_bdn_hukum option[value='${kelurahan_bdn_hukum}']`).prop('selected', true);
        }
      });
    }
    // END: History Alamat
  }

  // Jabatan
  getJabatan(n)
</script>
{{-- END: Get Json Data --}}

{{-- START: DOM Manipulation --}}
<script>
  // START: Alamat
  const provinsiChange = (thisValue, thisId, nextId) => {
    $('#'+nextId).empty(); // set null
    $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
    $.getJSON("/getKota/"+thisValue, function(data_kota){
      $.each(data_kota.kota,function(index,value){
          $('#'+nextId).append(
              '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
          );
      })
    });
  }

  const kotaChange = (thisValue, thisId, nextId) => {
    $('#'+nextId).empty(); // set null
    $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
    $.getJSON("/getKecamatan/"+thisValue, function(data_kecamatan){
      for(let i = 0; i<data_kecamatan.length; i++){
        $('#'+nextId).append($('<option>', { 
            value: data_kecamatan[i].text,
            text : data_kecamatan[i].text
        }));
      }
    });
  }

  const kecamatanChange = (thisValue, thisId, nextId) => {
    $('#'+nextId).empty(); // set null
    $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
    $.getJSON("/borrower/data_kelurahan/"+thisValue+"/", function(data){
        for(let i = 0; i<data.length; i++){ 
            $('#'+nextId).append($('<option>', {
                value: data[i].text,
                text : data[i].text
            }));
        }
    });
  }

  const kelurahanChange = (thisValue, thisId, nextId) => {
    $.getJSON("/borrower/data_kode_pos/"+thisValue+"/", function(data){
        $('#'+nextId).val(data[0].text)
    });
  }
  // END: Alamat

  $(document).ready(function(){
    $('#ktp_pengurus').on('keyup',function(){
      let thisVal = $(this).val()
      if (thisVal.length == 16) {
        isNikUnique(thisVal)
      } else {
        $('#label_ktp_pengurus').text('').append('Nomor KTP <i class="text-danger">*</i>')
      }
    })

    $('#no_telp_pengurus').on('keyup',function(){
      let thisVal = $(this).val()
      if (thisVal.length > 9) {
        isPhoneUnique(thisVal)
      } else {
        $('#label_no_telp_pengurus').text('').append('Nomor Telepon<i class="text-danger">*</i>')
      }
    })

    $('#btn-refresh').click(function () {
      swal.fire({
        title: "Yakin ingin merefresh data ?",
        type : "warning",
        showCancelButton: true,
        cancelButtonText: 'Batal',
        confirmButtonClass: "btn-success",
      }).then(function(response) {
        if (response.value) {
          location.href="/user/userverification" 
        }
      })
    })

    // Kirim OTP
    $('#kirim_lagi').on('click',function(){
      $('#kirim_lagi').prop('disabled',true);
      let tipe_pengguna = $('#tipe_pengguna').val()
      let noHP = ""
      if (tipe_pengguna == '2') {
        noHP = $("#txt_kode_operator_pengurus option:selected").val() + $('#no_telp_pengurus').val();
      } else {
        noHP = $("#txt_kode_operator option:selected").val()+$('#no_telp').val();
      }
      $.ajax({
          url : "/user/kirimOTP/",
          method : "post",
          dataType: 'JSON',
          data: {hp:noHP},
          success:function(data)
          {
              console.log(data.status)
          }
      });
      timerDisableButton();
    });

    $('#kode_otp').on('keyup',function(){
      if ($('#kode_otp').val().length == 6)
      {
          $('#kirim_data').removeAttr('disabled');

      }
      else
      {
          $('#kirim_data').prop('disabled',true);
      }
    })

    $('#kirim_data').on('click',function(){
      $('#kirim_data').prop("disabled", true);
      $.ajax({
          url : "/user/cekOTP/",
          method : "post",
          dataType: 'JSON',
          data: {otp:$('#kode_otp').val()},
          beforeSend: function() {
            $('#otp').modal('hide')
            Swal.fire({
              html: '<h5>Mengecek OTP...</h5>',
              onBeforeOpen: () => {
                Swal.showLoading();
              },
              allowOutsideClick: () => !Swal.isLoading()
            })
          },
          success:function(data) {
            if (data.status == '00') {
                  let tipe_pengguna = $('#tipe_pengguna').val()
                  if (tipe_pengguna == '2') {
                    $('#form-super-lender').trigger('submit')
                  } else {
                    swal.close()
                    $('#example-advanced-form').attr('action','{{route('firstUpdateProfile')}}').attr('method','POST').attr('enctype','multipart/form-data');
                    $('#example-advanced-form').submit();
                  }
                  $('#kirim_data').prop("disabled", true);
              }
              else {
                $('#kirim_data').prop("disabled", false);
                swal.fire({
                  title: "Kode OTP tidak sama",
                  type: "error",
                  showCancelButton: false,
                  confirmButtonClass: "btn-danger",
                }).then(function() {
                  $('#otp').modal('show')
                })
              }
          },
          error:function () {
            swal.fire({
              title: "Error",
              text: "internal error",
              type: "error",
              showCancelButton: false,
              confirmButtonClass: "btn-danger",
            })
          }
      });
          
    })

    // Kirim Data
    $("#form-super-lender").submit(function(e) {
      e.preventDefault()

      let form = $("#form-super-lender");
      let formData = new FormData(this);
      let tab = $('#tab').val()
      $.ajax({
        url:$(this).attr("action"),
        data: formData,
        // cache:false,
        contentType: false,
        processData: false,
        type:$(this).attr("method"),
        beforeSend: function() {
          if (tab == '1') {
            console.log('tab 1')
          } else {
            Swal.fire({
              html: '<h5>Menyimpan Data...</h5>',
              onBeforeOpen: () => {
                Swal.showLoading();
              },
              allowOutsideClick: () => !Swal.isLoading()
            })
          }
        },
        success:function(response) {
          if (response.error == true) {
            console.log(response)
            swal.close()
          } else {
            if (!response.tab) {
              swal.fire({
                title: "Berhasil",
                type : "success",
                text: response.msg,
                showCancelButton: false,
                confirmButtonClass: "btn-success",
              }).then(function() {
                location.href="/user/dashboard" ;
              })
            }
          }
        },
        error:function(response) {
          
          if (response.msg) {
            swal.fire({
              title: "Error",
              type: "error",
              text: response.msg,
              showCancelButton: false,
              confirmButtonClass: "btn-danger",
            })
          } else {
            swal.fire({
              title: "Informasi",
              type: "warning",
              text: "file yang diunggah melebihi batas penyimpanan",
              showCancelButton: false,
              confirmButtonClass: "btn-danger",
            })
          }
        
          console.log(response)
        }
      });
    })
  })
</script>
{{-- END: DOM Manipulation --}}

{{-- START: Validation --}}
<script>
  $.extend( $.validator.messages, {
    required: "Harus Diisi",
    minlength: $.validator.format( "Harap masukkan setidaknya {0} karakter." ),
    min: $.validator.format("Harap masukkan nilai yang lebih besar dari atau sama dengan {0}."),
    max: $.validator.format("Harap masukkan nilai yang kurang dari atau sama dengan {0}."),
  } );
  $("#form-super-lender").validate({
    // lang: 'id',
    onfocusout: false,
    messages: {
      tgl_akta_perubahan_bdn_hukum : {
        max: "Harap Masukan Tanggal Lebih Kecil atau Sama Dengan Tanggal Akta Pendirian",
        min: "Harap Masukan Tanggal Lebih Besar atau Sama Dengan Tanggal Akta Pendirian",
      }
    },
    invalidHandler: function(e, validator){
      if(validator.errorList.length){
        validator.errorList[0].element.focus()
      }
    }
  });

  const isNikUnique = (nik) => {
    $.ajax({
      url :  "/user/checkKTP/"+nik,
      method : "get",
      beforeSend: function() {
        $("#label_ktp_pengurus").addClass("has-error");
        $('#label_ktp_pengurus').text('').append('<i class="fa fa-spin fa-spinner"></i>')
      },
      success:function(data){
        if (data.status == "ada") {
          $("#label_ktp_pengurus").addClass("has-error");
          $('#label_ktp_pengurus').text('').append('<i class="text-danger">Nomor NIK sudah terdaftar</i>')
        } else {
          $("#label_ktp_pengurus").removeClass("has-error");
          $('#label_ktp_pengurus').text('').append('Nomor KTP <i class="text-danger">*</i>')
        }
      }
    })
  }

  const isPhoneUnique = (phone_number) => {
    $.ajax({
        url :  "/user/checkPhone/"+phone_number,
        method : "get",
        beforeSend: function() {
          $("#label_no_telp_pengurus").addClass("has-error");
          $('#label_no_telp_pengurus').text('').append('<i class="fa fa-spin fa-spinner"></i>')
        },
        success:function(data){
          if (data.status == "ada") {
            $("#label_no_telp_pengurus").addClass("has-error");
            $('#label_no_telp_pengurus').text('').append('<i class="text-danger">Nomor Telepon sudah terdaftar</i>')
          } else {
            $("#label_no_telp_pengurus").removeClass("has-error");
            $('#label_no_telp_pengurus').text('').append('Nomor Telepon<i class="text-danger">*</i>')
          }
          
        }
    });
  }

  $('#laporan_keuangan_bdn_hukum').on('change', function() {
    let validExtensions = ["doc", "docx", "xls", "xlsx", "pdf"]
    let file = $(this).val().split('.').pop();
    if (validExtensions.indexOf(file) == -1) {
      $('#label_laporan_keuangan_bdn_hukum').addClass("has-error")
      $('#label_laporan_keuangan_bdn_hukum').text('').append('<i class="text-danger">Format laporan keuangan tidak didukung</i>')
    } else {
      $("#label_laporan_keuangan_bdn_hukum").removeClass("has-error");
      $('#label_laporan_keuangan_bdn_hukum').text('').append('Laporan keuangan<i class="text-danger">*</i>')
    }

    let fileName = $(this).val().split('\\').pop();
    $('.custom-file-label').addClass("selected").text(fileName);
    // $(this).next('.custom-file-label').addClass("selected").html(fileName);
  });

  $(".no-zero").on("input", function(){
      let thisValue = $(this).val()
      if (thisValue.charAt(0) == '0') {
          $(this).val(thisValue.substring(1))
      }
  });

  $(".no-four").on("input", function(){
      let thisValue = $(this).val()
      if (thisValue.charAt(0) == '4') {
          $(this).val(thisValue.substring(1))
      }
  });

  $(".eight").on("input", function(){
      let thisValue = $(this).val()
      if (thisValue.charAt(0) != '8') {
          $(this).val(thisValue.substring(1))
      }
  });

  $('.checkKarakterAneh').on('input', function (event) { 
      this.value = this.value.replace(/[^a-zA-Z 0-9 _.]/g, '');
  });

  $('.number-only').on('input', function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
  });
</script>
{{-- END: Validation --}}

{{-- START: Jquery Steps --}}
<script>
  $(document).keypress(
    function(event){
      if (event.which == '13') {
        event.preventDefault();
      }
  });
  $('#step-super-lender').steps({
    startAt: start_from_tab,
    autoFocus: true,
    enableKeyNavigation: "false",
    // onInit: function () {
      
    // },
    onChange: function (currentIndex, newIndex, stepDirection) {
      if (currentIndex == 0 ) {
        $('#tab').val('1')
        // return true
        if($("#form-super-lender").valid()) {
          if ($('#label_laporan_keuangan_bdn_hukum').hasClass('has-error')) {
            $('#laporan_keuangan_bdn_hukum').focus()
          } else {
            $('#form-super-lender').trigger('submit')
            $('#tab').val('2')
            return true
          }
        } else {
          return false;
        }
      } else {
        $('#nama_pengurus').focus()
        return true
      }
    },
    onFinish: function () {
      // Pengurus Validation;
      if($("#form-super-lender").valid()) {
        if($('#label_ktp_pengurus').hasClass('has-error')) {
          $('#ktp_pengurus').focus()
          return false
        } else {
          if($('#label_no_telp_pengurus').hasClass('has-error')) {
            $('#no_telp_pengurus').focus()
            return false
          } else {
              sendOtp()
          }
        }
      } else {
        return false;
      }
    }
  });
</script>
{{-- END: Jquery Steps --}}

{{-- START: Other --}}
<script>
  $(document).ready(function() {
    // let steps = $('#step-super-lender').steps();
    // let steps_api = steps.data('plugin_Steps');

    // START: Pengurus
    $("#add-more-pengurus").click(function(){
      n++;
      let html = (`
      {{-- START: Pengurus --}}
      <div id="pengurus">
        <!-- START: Baris 1 -->
        <div class="form-row">
          <div class="col-12">
            <h6 class="line my-4 font-weight-bold">Informasi Pengurus &nbsp</h6>
          </div>
      
          <div class="col-md-4">
            <div class="form-group">
              <label for="nama_pengurus_${n}">Nama Pengurus <i class="text-danger">*</i></label>
              <input class="form-control" type="text" id="nama_pengurus_${n}" minlength="3" maxlength="30" name="nama_pengurus[]"
                placeholder="Masukkan Nama Pengurus..." required>
            </div>
          </div>
      
          <div class="col-md-4">
            <div class="form-group">
              <label for="jns_kelamin_pengurus_${n}">Jenis Kelamin <i class="text-danger">*</i></label>
              <select id="jns_kelamin_pengurus_${n}" name="jns_kelamin_pengurus[]" class="form-control custom-select" required>
                <option value="">-- Pilih Satu --</option>
                @foreach ($master_jenis_kelamin as $b)
                <option value="{{$b->id_jenis_kelamin}}">
                  {{$b->jenis_kelamin}}
                </option>
                @endforeach
              </select>
            </div>
          </div>
      
          <div class="col-md-4">
            <div class="form-group">
              <label for="ktp_pengurus">Nomor KTP <i class="text-danger">*</i></label>
              <input class="form-control" type="text" id="ktp_pengurus_${n}" name="ktp_pengurus[]" placeholder="Masukkan nomor KTP"
                minlength="16" maxlength="16" pattern=".{16,16}" oninput="this.setCustomValidity('')"
                oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')"
                onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()" required>
            </div>
          </div>
        </div>
        <!-- END: Baris 1 -->
      
        {{-- START: Baris 2 --}}
        <div class="form-row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="tempat_lahir_pengurus_${n}">Tempat Lahir <i class="text-danger">*</i></label>
              <input class="form-control" type="text" maxlength="35" id="tempat_lahir_pengurus_${n}" name="tempat_lahir_pengurus[]"
                placeholder="Masukkan tempat lahir" required>
            </div>
          </div>
      
          <div class="col-md-4">
            <div class="form-group">
              <label for="tanggal_lahir_pengurus_${n}">Tanggal Lahir <i class="text-danger">*</i></label>
              <input class="form-control" type="date" id="tanggal_lahir_pengurus_${n}" name="tanggal_lahir_pengurus[]"
                max="<?= date("Y-m-d"); ?>" placeholder="Masukkan tanggal lahir" required>
            </div>
          </div>
      
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="form-group">
                  <label for="txt_kode_operator_pengurus_${n}">Kode Negara <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="txt_kode_operator_pengurus_${n}"
                    name="txt_kode_operator_pengurus[]" required>
      
                    @foreach ($master_kode_operator as $b)
                    <option value="{{$b->kode_operator}}" {{ $b->kode_operator == "62" ? 'selected' : '' }}>
                      ({{$b->kode_operator}}) {{$b->negara}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
      
              <div class="col-md-6 col-sm-12">
                <div class="form-group div_hp">
                  <label for="no_telp_pengurus_${n}">Nomor Telepon <i class="text-danger">*</i>
                  </label>
                  <input type="text" minlength="9" maxlength="13" name="no_telp_pengurus[]" id="no_telp_pengurus_${n}" onkeyup="if ($(this).val().charAt(0) == '0') {$(this).val($(this).val().substring(1))} else if ($(this).val().charAt(0) != '8') { $(this).val($(this).val().substring(1))}; this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                    class="form-control no-zero eight" placeholder="Contoh:8xxxxxxxxxx" required>
                </div>
              </div>
            </div>
      
          </div>
      
        </div>
        {{-- END: Baris 2 --}}
      
        {{-- START: Baris 3 --}}
        <div class="form-row">
          <div class="col-md-4">
            <div class="form-group row">
              <label for="agama_pengurus_${n}" class="col-12">Agama <i class="text-danger">*</i></label>
              <div class="col-12">
                <select class="form-control custom-select" id="agama_pengurus_${n}" name="agama_pengurus[]" required>
                  <option value="">-- Pilih Satu --</option>
                  @foreach ($master_agama as $b)
                  <option value="{{$b->id_agama}}" {{ old('agama_pengurus[]') == $b->agama ? 'selected' : '' }}>
                    {{$b->agama}}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
      
          <div class="col-md-4">
            <div class="form-group">
              <label for="pendidikan_terakhir_pengurus_${n}">Pendidikan Terakhir <i class="text-danger">*</i></label>
              <select class="form-control custom-select" id="pendidikan_terakhir_pengurus_${n}"
                name="pendidikan_terakhir_pengurus[]" required>
                <option value="">-- Pilih Satu --</option>
                @foreach ($master_pendidikan as $b)
                <option value="{{$b->id_pendidikan}}"
                  {{ old('pendidikan_terakhir_pengurus[]') == $b->id_pendidikan ? 'selected' : '' }}>
                  {{$b->pendidikan}}</option>
                @endforeach
              </select>
            </div>
          </div>
      
          <div class="col-md-4">
            <div class="form-group">
              <label for="npwp_pengurus_${n}">Nomor NPWP <i class="text-danger">*</i></label>
              <input class="form-control " type="text" maxlength="15" minlength="15" id="npwp_pengurus_${n}" name="npwp_pengurus[]"
                pattern=".{15,15}" oninput="this.setCustomValidity('')"
                oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')"
                onkeyup="this.value = this.value.replace(/[^0-9]/g, '');$(this).valid()"
                placeholder="Masukkan nomor NPWP" required>
            </div>
          </div>
        </div>
        {{-- END: Baris 3 --}}
      
        {{-- START: Baris 4 --}}
        <div class="form-row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="jabatan_pengurus_${n}">Jabatan <i class="text-danger">*</i></label>
              <select class="form-control custom-select" id="jabatan_pengurus_${n}" name="jabatan_pengurus[]" required>
                <option value="">-- Pilih Satu --</option>
              </select>
            </div>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4"></div>
        </div>
        {{-- END: Baris 4 --}}
      
        {{-- START: Baris 5 --}}
        <div class="form-row">
          <div class="col-12">
            <h6 class="line my-4 font-weight-bold">Alamat Domisili Pengurus &nbsp</h6>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="alamat_pengurus_${n}">Alamat<i class="text-danger">*</i></label>
              <textarea class="form-control form-control-lg" maxlength="90" id="alamat_pengurus_${n}" name="alamat_pengurus[]"
                rows="6" placeholder="Masukkan alamat lengkap Anda.." required></textarea>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="provinsi_pengurus_${n}">Provinsi <i class="text-danger">*</i></label>
              <select class="form-control custom-select" id="provinsi_pengurus_${n}" name="provinsi_pengurus[]"
                onchange="provinsiChange(this.value, this.id, 'kota_pengurus_${n}')" required>
                <option value="">-- Pilih Satu --</option>
                @foreach ($master_provinsi as $data)
                <option value={{$data->kode_provinsi}}>
                  {{$data->nama_provinsi}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="kota_pengurus_${n}">Kota/Kabupaten <i class="text-danger">*</i></label>
              <select class="form-control custom-select" id="kota_pengurus_${n}" name="kota_pengurus[]"
                onchange="kotaChange(this.value, this.id, 'kecamatan_pengurus_${n}')" required>
                <option value="">-- Pilih Satu --</option>
              </select>
            </div>
          </div>
        </div>
        {{-- END: Baris 5 --}}
      
        {{-- START: Baris 6 --}}
        <div class="form-row">
          <div class="col-md-4">
            <div class="form-group">
              <label for="kecamatan_pengurus_${n}">Kecamatan <i class="text-danger">*</i></label>
              <select class="form-control custom-select" id="kecamatan_pengurus_${n}" name="kecamatan_pengurus[]" 
                onchange="kecamatanChange(this.value, this.id, 'kelurahan_pengurus_${n}')" required>
                <option value="">-- Pilih Satu --</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="kelurahan_pengurus_${n}">Kelurahan <i class="text-danger">*</i></label>
              <select class="form-control custom-select" id="kelurahan_pengurus_${n}" name="kelurahan_pengurus[]" 
                onchange="kelurahanChange(this.value, this.id, 'kode_pos_pengurus_${n}')" required>
                <option value="">-- Pilih Satu --</option>
              </select>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label for="kode_pos_pengurus_${n}">Kode Pos <i class="text-danger">*</i></label>
              <input class="form-control" type="text" id="kode_pos_pengurus_${n}" name="kode_pos_pengurus[]" placeholder="--" readonly>
            </div>
          </div>
        </div>
        {{-- END: Baris 6 --}}
      
        {{-- START: Baris 7 --}}
        {{-- START: Foto Pengurus 1 --}}
        <div class="row">
          <div class="col-12">
            <h6 class="line my-4 font-weight-bold">Foto Pengurus &nbsp</h6>
          </div>
      
          <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
            <label>Foto KTP <i class="text-danger">*</i></label>
            <div id="preview_camera_ktp_pengurus_${n}" name="preview_camera_ktp_pengurus[]" class="pt-3">
              <img class="imagePreview" id="preview-${n}">
              <div>
                <button class="btn btn-primary" type="button" id="btn_camera_ktp_pengurus_${n}" name="btn_camera_ktp_pengurus[]">
                  Kamera
                </button>
              </div>
            </div>
      
            <div id="take_camera_ktp_pengurus_${n}" name="take_camera_ktp_pengurus[]" class="d-none">
              <div class="col p-0">
                <img id="user-guide5" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                  style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                <div id="camera_ktp_pengurus_${n}" name="camera_ktp_pengurus[]"></div>
                <input class="btn btn-primary mt-4" type="button" value="Ambil Foto" id="take_snapshot_ktp_pengurus_${n}"
                  name="take_snapshot_ktp_pengurus[]">
                <input type="hidden" id="user_camera_ktp_pengurus_${n}" name="user_camera_ktp_pengurus[]"
                  class="image-tag"><br />
              </div>
            </div>
      
            <div id="result_camera_ktp_pengurus_${n}" name="result_camera_ktp_pengurus[]" class="d-none">
              <label class="my-3">Hasil</label>
              <div id="result_ktp_pengurus_${n}" name="result_ktp_pengurus[]"></div>
            </div>
            <input type="text" class="input-hidden2" id="user_camera_ktp_pengurus_val_${n}" name="user_camera_ktp_pengurus_val[]"
              required>
          </div>

          <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
            <label>Foto NPWP <i class="text-danger">*</i></label>
            <div id="preview_camera_npwp_pengurus_${n}" name="preview_camera_npwp_pengurus[]" class="pt-3">
              <img class="imagePreview" id="preview-${n}">
              <div>
                <button class="btn btn-primary" id="btn_camera_npwp_pengurus_${n}" name="btn_camera_npwp_pengurus[]"
                  type="button">
                  Kamera
                </button>
              </div>
            </div>
      
            <div id="take_camera_npwp_pengurus_${n}" name="take_camera_npwp_pengurus[]" class="d-none">
              <div class="col p-0">
                <img id="user-guide6" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                  style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                <div id="camera_npwp_pengurus_${n}" name="camera_npwp_pengurus[]"></div>
                <input class="btn btn-primary mt-4" type="button" value="Ambil Foto" id="take_snapshot_npwp_pengurus_${n}"
                  name="take_snapshot_npwp_pengurus[]">
                <input type="hidden" id="user_camera_npwp_pengurus_${n}" name="user_camera_npwp_pengurus[]"
                  class="image-tag"><br />
              </div>
            </div>
      
            <div id="result_camera_npwp_pengurus_${n}" name="result_camera_npwp_pengurus[]" class="d-none">
              <label class="my-3">Hasil</label>
              <div id="result_npwp_pengurus_${n}" name="result_npwp_pengurus[]"></div>
            </div>
            <input type="text" class="input-hidden2" id="user_camera_npwp_pengurus_val_${n}"
              name="user_camera_npwp_pengurus_val[]" required>
          </div>
      
          <div class="col-md-12 mb-3">
            <button class="btn btn-danger float-right remove" type="button"><i class="glyphicon glyphicon-remove"></i> Hapus Data Pengurus</button>
          </div>
        </div>
        {{-- END: Foto Pengurus 1 --}}
        {{-- END: Baris 7 --}}
      </div>
      {{-- END: Pengurus --}}
      `);

      $("#add-new-form").append(html);
      getJabatan(n)

      // Camera
      takeSnapshotDiriPengurus(n);
      takeSnapshotKtpPengurus(n);
      takeSnapshotDiriDanKtpPengurus(n);
      takeSnapshotNpwpPengurus(n);

      cameraDiriPengurusClick(n);
      cameraKtpPengurusClick(n);
      cameraDiriDanKtpPengurusClick(n);
      cameraNpwpPengurusClick(n);

    });

    $("body").on("click",".remove",function(){
      $(this).parents("#pengurus").remove();
      n--;
    });
    
    // $("#next").click(function () {
    //   console.log(steps_api.getStepIndex());
    // });
  });

  const sendOtp = () => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let noHP = $("#txt_kode_operator_pengurus option:selected").val() + $('#no_telp_pengurus').val();

    $('#otp').modal('show')
    $('#kirim_lagi').prop('disabled',true);
    $('#no_hp').val(noHP);
    $.ajax({
      url : "/user/kirimOTP/",
      method : "post",
      dataType: 'JSON',
      data: {hp:noHP},
      success:function(data)
      {
      console.log(data.status)
      }
    });
    timerDisableButton();
  }
</script>
{{-- END: Other --}}
@endpush
{{-- END: Script --}}
{{-- END: Super Lender --}}