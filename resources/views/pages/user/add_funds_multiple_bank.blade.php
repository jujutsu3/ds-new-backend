@extends('layouts.user.sidebar')

@section('title', 'Tambah Dana')

@section('content')

<style>
    #parent {
        /* margin: 0; */
        /* width: 100%; */
        width: 100%;
        text-align: center;
    }

    .left {
        float: left;
        width: 20%;
        /* background-color: blue; */
    }

    .center {
        display: inline-block;
        margin-top: 5px;
        width: 70%;
        /* background-color: yellow; */
    }

    .right {
        float: right;
        margin-top: 5px;
        width: 10%;
        /* background-color: red; */
    }

    .btns {
        position: absolute;
        width: 0.1px;
        height: 0.1px;
        z-index: -99999999999;
    }
</style>
<div class="row">
    <div class="col-sm-12 col-lg-8 mx-auto">
        <?php
        $virtual_account = false;
        $va = [];
        ?>

        @if(!empty($valain))
        @foreach ($valain as $item)
        @if($item->kode_bank == '009')
        @if(!empty($item->va_number))
        <div class="card bg-light">
            <div class="card-body">
                <div id="parent">
                    <div class="left">
                        <img src="/img/logobank009.png" class="text-left" width="128px">
                    </div>
                    <div class="center">
                        <h2 style="color:black" class="text-right"> Virtual Account {{!empty($rekening->va_number) ? $rekening->va_number : ''}}</h2>
                    </div>
                    <div class="right text-right">
                        <?php $va[$item->kode_bank] = !empty($rekening->va_number) ? $rekening->va_number : ''; ?>
                        <button class="btn copy-button" id="bnis" value="{{$va[$item->kode_bank]}}" title="click to copy va number" onclick="CopyText('#bnis')"><img src="/img/copyicon1.png" height="40px"></button>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @elseif($item->kode_bank == '451')
        @if(!empty($item->va_number))
        <div class="card bg-light">
            <div class="card-body">
                <div id="parent">
                    <div class="left">
                        <img src="/img/logobank451.png" class="text-left">
                    </div>
                    <div class="center">
                        <h2 style="color:dark" class="text-right"> Virtual Account {{!empty($item->va_number) ? "900".$item->va_number : ''}}</h2>
                    </div>
                    <div class="right text-right">
                        <?php $va[$item->kode_bank]  = !empty($item->va_number) ? $item->va_number : ''; ?>
                        <button class="btn copy-button" id="bsi" value="{{'900'.$va[$item->kode_bank]}}" title="click to copy va number" onclick="CopyText('#bsi')"><img src="/img/copyicon1.png" height="40px"></button>

                    </div>
                </div>
            </div>
        </div>
        @endif
        @elseif($item->kode_bank == '022')
        @if(!empty($item->va_number))
        <div class="card bg-light">
            <div id="parent">
                <div class="card-body">
                    <div class="left">
                        <img src="/img/logobank022.png" height="62px" width="128px">
                    </div>
                    <div class="center">
                        <h2 style="color:dark" class="text-right">
                            Virtual Account {{!empty($item->va_number) ? $item->va_number : ''}}
                        </h2>
                    </div>
                    <div class="right text-right">
                        <?php $va[$item->kode_bank] = !empty($item->va_number) ? $item->va_number : ''; ?>
                        <button class="btn copy-button" id="cimbs" value="{{$va[$item->kode_bank]}}" title="click to copy va number" onclick="CopyText('#cimbs')"><img src="/img/copyicon1.png" height="40px"></button>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endif
        @endforeach
        @endif
    </div>
</div>
<br>

<div class="row">
    <div class="col-sm-12">
        <h2>Tata Cara Top Up : </h2>
        <div class="accordion" id="bank_accordion">
            <?php $no = 1; ?>
            @foreach($bank_name as $item)
            <div class="card">
                <!-- header -->
                <div class="card-header collapsed bg-info" id="heading{{ $item->kode_bank }}" data-toggle="collapse" data-target="#collapse{{ $item->kode_bank }}" aria-expanded="false" aria-controls="collapse{{ $item->kode_bank }}">
                    <h5 class="mb-0" style="float:left">
                        {{ $item->nama_bank }}
                    </h5><img src="/img/panahkebawah.svg" height="20px" style="float:right">
                </div>
                <!-- #isi collapse-->
                <?php $r = $no++; ?>
                <div id="collapse{{ $item->kode_bank }}" class="collapse" aria-labelledby="heading{{ $item->kode_bank }}" data-parent="#bank_accordion">
                    <div class="accordion" id="detail_accordion<?php echo $r ?>">
                        @foreach($m_bank_partner as $key)
                        @if($item->kode_bank == $key->kode_bank)
                        <div class="card">
                            <!-- header1 -->
                            <div class="card-header collapsed bg-light" id="heading{{ str_replace([' ','/'],'',$key->merchant_description).$key->kode_bank }}" data-toggle="collapse" data-target="#collapse{{ str_replace([' ','/'],'',$key->merchant_description).$key->kode_bank }}" aria-expanded="false" aria-controls="collapse{{ str_replace([' ','/'],'',$key->merchant_description).$key->kode_bank }}">
                                <h5 class="mb-0">
                                    {{ $key->merchant_description }}
                                </h5>
                            </div>
                            <!-- isi1 -->
                            <div id="collapse{{ str_replace([' ','/'],'',$key->merchant_description).$key->kode_bank }}" class="collapse" aria-labelledby="heading{{ str_replace([' ','/'],'',$key->merchant_description).$key->kode_bank }}" data-parent="#detail_accordion<?php echo $r ?>">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <?php echo sprintf($key->description, $va[$key->kode_bank], $va[$key->kode_bank], $va[$key->kode_bank], $va[$key->kode_bank]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>


@endsection