{{-- START: Super Lender --}}
{{-- START: Style --}}
@push('add-style')
<style>
  .line {
    display: flex;
    flex-direction: row;
  }

  .line:after {
    content: "";
    flex: 1 1;
    border-bottom: 1px solid #000;
    margin: auto;
  }

  .bg-green-dsi {
    background-color: #18783A
  }

  @media screen and (max-width: 600px) {
    .step-title {
      visibility: hidden;
      clear: both;
      float: left;
      margin: 10px auto 5px 20px;
      width: 28%;
      display: none;
    }
  }

  .input-hidden2 {
    opacity: 0;
    height: 0;
    width: 0;
    padding: 0;
    margin: 0;
    float: right;
  }

  #no_tlp_badan_hukum-error {
    position: absolute;
    top: 100%;
  }

  .card-header[aria-expanded=false] .fa-angle-up {
    display: none;
  }

  .card-header[aria-expanded=true] .fa-angle-down {
    display: none;
  }

  .error {
    color: red;
  }
</style>
@endpush
{{-- END: Style --}}

{{-- START: Form Super Lender --}}

<!-- START: Form Super Lender -->
<form action="{{route('updateUser')}}" method="POST" id="form-super-lender" name="form-super-lender"
  enctype="multipart/form-data">
  @csrf

  <!-- START: Informasi Akun -->
  <div id="layout-akun">
    <div class="card">
      <div class="card-header" id="card-akun" data-toggle="collapse" data-target="#collapseZero"
        aria-expanded="false" aria-controls="collapseZero">
        <p class="h5 font-weight-bold">
          Informasi Akun <i class="fa fa-angle-up float-right"></i><i class="fa fa-angle-down float-right"></i>
        </p>
        </h5>
      </div>
      <div id="collapseZero" class="collapse coll" aria-labelledby="card-akun" data-parent="#layout-akun">
        <div class="card-body">

          <div id="content-akun">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="username">Akun <i class="text-danger">*</i></label>
                  <input type="text" id="username" name="username" class="form-control" value="{{!empty($detil->username) ? $detil->username : ''}}" disabled>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">E-mail <i class="text-danger">*</i></label>
                  <input type="text" id="email" name="email" class="form-control" value="{{!empty($detil->email) ? $detil->email : ''}}" disabled>
                </div>
              </div>
            </div>

        </div>
      </div>
    </div>
  </div>
  <!-- START: Informasi Akun -->

  <!-- START: Informasi Badan Hukum -->
  <div id="layout-badan-hukum" class="mt-4">
    <div class="card">
      <div class="card-header" id="card-badan-hukum" data-toggle="collapse" data-target="#collapseOne"
        aria-expanded="false" aria-controls="collapseOne">
        <p class="h5 font-weight-bold">
          Informasi Badan Hukum <i class="fa fa-angle-up float-right"></i><i class="fa fa-angle-down float-right"></i>
        </p>
        </h5>
      </div>

      <div id="collapseOne" class="collapse coll" aria-labelledby="card-badan-hukum" data-parent="#layout-badan-hukum">
        <div class="card-body">

          <div id="content-badan-hukum">

            <!-- START: Baris 1 -->
            <div class="form-row">
              <input type="hidden" name="tipe_pengguna" id="tipe_pengguna" value="2">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="nama_badan_hukum">Nama Badan Hukum <i class="text-danger">*</i></label>
                  <input type="text" name="nama_badan_hukum" class="form-control checkKarakterAneh"
                    placeholder="Masukkan nama badan hukum" minlength="3" maxlength="50"
                    value="{{ $detil ? $detil->nama_perusahaan : ''}}" readonly required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="nib_badan_hukum">Nomor Surat Izin <i class="text-danger">*</i></label>
                  <input class="form-control" type="text" id="nib_badan_hukum" name="nib_badan_hukum" minlength="3"
                    maxlength="50" placeholder="SIUP/TDP/Nomor surat izin lainnya"
                    value="{{ $detil ? $detil->nib : ''}}" required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="npwp_bdn_hukum">NPWP Perusahaan<i class="text-danger">*</i></label>
                  <input class="form-control" type="text" pattern=".{15,15}"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()" minlength="15"
                    maxlength="15" id="npwp_bdn_hukum" name="npwp_bdn_hukum" placeholder="Masukkan nomor NPWP"
                    value="{{ $detil ? $detil->npwp_perusahaan : ''}}" disabled required>
                </div>
              </div>
            </div>
            <!-- END: Baris 1 -->

            <!-- START: Baris 2 -->
            <div class="form-row">
              <div class="col-md-4">
                <div class="form-group">
                  <label id="label_no_akta_pendirian_bdn_hukum" for="no_akta_pendirian_bdn_hukum">No Akta Pendirian <i
                      class="text-danger">*</i></label>
                  <input class="form-control" type="text" id="no_akta_pendirian_bdn_hukum"
                    name="no_akta_pendirian_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Pendirian"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                    value="{{ $detil ? $detil->no_akta_pendirian : ''}}" disabled required>
                </div>
              </div>
              <div class=" col-md-4">
                <div class="form-group">
                  <label for="tgl_berdiri_badan_hukum">Tanggal Akta Pendirian <i class="text-danger">*</i></label>
                  <input class="form-control" type="date" id="tgl_berdiri_badan_hukum" name="tgl_berdiri_badan_hukum"
                    max="{{ date("Y-m-d")}}" placeholder="Masukkan Tanggal Berdiri"
                    onchange="$(this).valid(); $('#tgl_akta_perubahan_bdn_hukum').attr('min', $(this).val())"
                    value="{{ $detil ? $detil->tgl_berdiri : ''}}" disabled required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label id="label_no_akta_perubahan_bdn_hukum" for="no_akta_perubahan_bdn_hukum">No Akta Perubahan
                    Terakhir</label>
                  <input class="form-control" type="text" id="no_akta_perubahan_bdn_hukum"
                    name="no_akta_perubahan_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Perubahan"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                    value="{{ $detil ? $detil->nomor_akta_perubahan : ''}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="tgl_akta_perubahanbdn_hukum">Tanggal Akta Perubahan Terakhir</label>
                  <input class="form-control" type="date" id="tgl_akta_perubahan_bdn_hukum"
                    name="tgl_akta_perubahan_bdn_hukum" max="{{ date("Y-m-d")}}"
                    placeholder="Masukkan Tanggal Akta Perubahan Terakhir" onchange="$(this).valid()"
                    value="{{ $detil ? $detil->tanggal_akta_perubahan : ''}}">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label id="label_no_tlp_badan_hukum">Nomor Telepon Perusahaan <i class="text-danger">*</i></label>
                  <div class="input-group">
                    <div class="input-group-append">
                      <span class="input-group-text bg-green-dsi text-white"> +62 </span>
                    </div>
                    <input class="form-control no-zero" type="text"
                      onkeyup="this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" id="no_tlp_badan_hukum"
                      name="no_tlp_badan_hukum" placeholder="Masukkan nomor perusahaan"
                      value="{{ $detil ? substr($detil->telpon_perusahaan, 2) : ''}}" required>
                  </div>
                </div>
              </div>
            </div>
            <!-- END: Baris 2 -->

            <!-- START: Baris 3 -->
            <div class="row">
              <div class="col-12">
                <label>Foto NPWP Perusahaan <i class="text-danger">*</i></label>
              </div>
              <div class="col-4">
                <div id="preview_camera_npwp_badan_hukum" class="pt-3">
                  <img class="imagePreview" id="preview-1"
                    src="{{ $detil ? route('getUserFile', ['filename' => str_replace('/',':', $detil->foto_npwp_perusahaan)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                    style="{{ $detil ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                  <div id="btn_camera_npwp_badan_hukum">
                    <label class="btn btn-primary mt-4">Kamera</label>
                  </div>
                </div>

                <div id="take_camera_npwp_badan_hukum" class="d-none">
                  <div class="col p-0">
                    <img id="user-guide img-fluid" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                      style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                    <div id="camera_npwp_badan_hukum"></div>
                    <input class="btn btn-primary" type="button" value="Ambil Foto" onClick="takeSnapshot()">
                    <input type="hidden" name="user_camera_npwp_badan_hukum" id="user_camera_npwp_badan_hukum"
                      class="image-tag">
                  </div>
                </div>
                <input class="input-hidden2" type="text" id="user_camera_npwp_badan_hukum_val"
                  value="{{ $detil ? $detil->foto_npwp_perusahaan : '' }}" required>
                <div id="result_camera_npwp" class="d-none">
                  <label class="my-3">Hasil</label>
                  <div id="result_npwp_perusahaan"></div>
                </div>
              </div>
            </div>
            <!-- END: Baris 3 -->

            <!-- START: Baris 4 -->
            <div class="form-row">
              <div class="col-12">
                <h6 class="line my-4 font-weight-bold">Alamat Sesuai AKTA &nbsp</h6>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-alamat">Alamat Lengkap <i class="text-danger">*</i></label>
                  <textarea class="form-control form-control-lg" maxlength="100" id="alamat_bdn_hukum"
                    name="alamat_bdn_hukum" rows="6" placeholder="Masukkan alamat lengkap Anda.."
                    required>{{ $detil ? $detil->alamat_perusahaan : ''}}</textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-namapengguna">Provinsi <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="provinsi_bdn_hukum" name="provinsi_bdn_hukum"
                    onchange="provinsiChange(this.value, this.id, 'kota_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                    @foreach ($master_provinsi as $data)
                    <option value={{$data->kode_provinsi}}
                      {{ $detil ? $data->kode_provinsi == $detil->provinsi_perusahaan ? 'selected' : '' : ''}}>
                      {{$data->nama_provinsi}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-kota">Kota/Kabupaten <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="kota_bdn_hukum" name="kota_bdn_hukum"
                    onchange="kotaChange(this.value, this.id, 'kecamatan_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                  </select>
                </div>
              </div>
            </div>
            <!-- END: Baris 4 -->

            <!-- START: Baris 5 -->
            <div class="form-row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="wizard-progress2-kecamatan">Kecamatan <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="kecamatan_bdn_hukum" name="kecamatan_bdn_hukum"
                    onchange="kecamatanChange(this.value, this.id, 'kelurahan_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="kelurahan_bdn_hukum">Kelurahan <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="kelurahan_bdn_hukum" name="kelurahan_bdn_hukum"
                    onchange="kelurahanChange(this.value, this.id, 'kode_pos_bdn_hukum')" required>
                    <option value="">-- Pilih Satu --</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="kode_pos_bdn_hukum">Kode Pos <i class="text-danger">*</i></label>
                  <input class="form-control" type="text" maxlength="30" id="kode_pos_bdn_hukum"
                    name="kode_pos_bdn_hukum" placeholder="--" value="{{ $detil ? $detil->kode_pos_perusahaan : ''}}"
                    readonly>
                </div>
              </div>
            </div>
            <!-- END: Baris 5 -->

            {{-- START: Baris 6 --}}
            <div class="form-row">
              <div class="col-12">
                <h6 class="line my-4 font-weight-bold">Informasi Rekening &nbsp</h6>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="rekening_bdn_hukum">No Rekening <i class="text-danger">*</i></label>
                  <input type="text" minlength="10" maxlength="16" name="rekening_bdn_hukum" id="rekening_bdn_hukum"
                    pattern=".{10,16}" onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                    class="form-control" placeholder="No Rekening" value="{{ $detil ? $detil->rekening : ''}}" disabled
                    required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="rekening_bdn_hukum checkKarakterAneh">Nama Pemilik Rekening <i
                      class="text-danger">*</i></label>
                  <input type="text" maxlength="35" name="nama_pemilik_rek_bdn_hukum" id="nama_pemilik_rek_bdn_hukum"
                    class="form-control" placeholder="Nama Pemilik Rekening"
                    value="{{ $detil ? $detil->nama_pemilik_rek : ''}}" disabled required>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label for="bank_bdn_hukum">Bank <i class="text-danger">*</i></label>
                  <select name="bank_bdn_hukum" class="form-control custom-select" id="bank_bdn_hukum" disabled
                    required>
                    <option value="">--Pilih--</option>
                    @foreach($master_bank as $b)
                    <option value="{{ $b->kode_bank }}"
                      {{ $detil ? $b->kode_bank == $detil->bank_investor ? 'selected' : '' : ''}}>
                      {{ $b->nama_bank }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            {{-- END: Baris 6 --}}

            <!-- START: Informasi Lain Lain Title -->
            <div class="row">
              <div class="col-12">
                <h6 class="line my-4 font-weight-bold">Informasi Lain Lain &nbsp</h6>
              </div>
            </div>
            <!-- END: Informasi Lain Lain -->

            <!-- START: Baris 7 -->
            <div class="form-row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="bidang_pekerjaan_bdn_hukum">Bidang Usaha <i class="text-danger">*</i></label>
                  <select class="form-control custom-select" id="bidang_pekerjaan_bdn_hukum"
                    name="bidang_pekerjaan_bdn_hukum" required>
                    <option value="">Pilih Bidang Usaha</option>
                    @foreach ($master_bidang_pekerjaan as $b)
                    @if ($b->kode_bidang_pekerjaan != 'e72' && $b->kode_bidang_pekerjaan != 'e00' &&
                    $b->kode_bidang_pekerjaan !=
                    'e01')
                    <option value="{{$b->kode_bidang_pekerjaan}}"
                      {{ $detil ? $b->kode_bidang_pekerjaan == $detil->bidang_pekerjaan ? 'selected' : '' : ''}}>
                      {{$b->bidang_pekerjaan}}</option>
                    @endif
                    @endforeach
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="omset_tahun_terakhir">Omset Tahun Terakhir<i class="text-danger">*</i></label>
                  <input type="text" maxlength="30" name="omset_tahun_terakhir" id="omset_tahun_terakhir"
                    class="form-control no-zero" placeholder="Masukkan omset tahun terakhir"
                    onkeyup="$(this).val(formatRupiah($(this).val()))" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="tot_aset_tahun_terakhr">Total Aset Tahun Terakhir <i class="text-danger">*</i></label>
                  <input type="text" maxlength="30" name="tot_aset_tahun_terakhr" id="tot_aset_tahun_terakhr"
                    class="form-control no-zero" placeholder="Masukkan total aset tahun terakhir"
                    onkeyup="$(this).val(formatRupiah($(this).val()))" required>
                </div>
              </div>
            </div>
            <!-- END: Baris 7 -->

            {{-- START: Baris 8 --}}
            <div class="form-row">
              <div class="col-md-4">
                <label id="label_laporan_keuangan_bdn_hukum" for="laporan_keuangan_bdn_hukum">Laporan Keuangan <i
                    class="text-danger">*</i></label>
                <div class="custom-file">
                  <input id="laporan_keuangan_bdn_hukum" name="laporan_keuangan_bdn_hukum" type="file"
                    accept=".doc, .docx, .xls, .xlsx, .pdf" class="custom-file-input" required>
                  <label for="laporan_keuangan_bdn_hukum" class="custom-file-label text-truncate">Pilih File</label>
                </div>
              </div>

              <div class="col-md-12">
                <p><i class="text-danger h6">* &nbsp</i> <b> Format file .doc, docx, .xls, .xlsx, .pdf </b></p>
              </div>
            </div>
            {{-- END: Baris 8 --}}

            <p class="text-danger"><b>(*) Wajib Diisi</b></p>

          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- END: Informasi Badan Hukum -->

  {{-- START: Pengurus --}}
  @foreach ($pengurus as $index => $item)
  <div class="mt-4 layout-pengurus" id="layout-pengurus-{{ $index + 1 }}">
    <div class="card">
      <div class="card-header" id="card-pengurus-{{ $index }}" data-toggle="collapse"
        data-target="#collapse-{{ $index }}" aria-expanded="false" aria-controls="collapse-{{ $index }}">
        @if ($index == 0)
          <p class="h5 font-weight-bold">
            Pengurus 1 (Mewakili Sebagai Perusahaan) <i class="fa fa-angle-up float-right"></i><i
              class="fa fa-angle-down float-right"></i>
          </p>
        @else
          <p class="h5 font-weight-bold">
            Informasi Pengurus <span name="card-title[]"></span> <i class="fa fa-angle-up float-right"></i><i
              class="fa fa-angle-down float-right"></i>
          </p>
        @endif
      </div>

      <div id="collapse-{{ $index }}" class="collapse coll" aria-labelledby="card-pengurus-{{ $index }}"
        data-parent="#layout-pengurus-{{ $index + 1 }}">
        <div class="card-body">

          <div id="content-pengurus">

            <div id="pengurus">
              <!-- START: Baris 1 -->
              <div class="form-row">

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="nama_pengurus_{{ $index }}">Nama Pengurus <i class="text-danger">*</i></label>
                    <input type="hidden" id="pengurus_id_{{ $index }}" name="pengurus_id[]"
                      value="{{ $item->pengurus_id }}">
                    <input class="form-control" type="text" id="nama_pengurus_{{ $index }}" minlength="3" maxlength="30"
                      name="nama_pengurus[]" placeholder="Masukkan Nama Pengurus..." value="{{ $item->nm_pengurus }}"
                      required>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="jns_kelamin_pengurus_{{ $index }}">Jenis Kelamin <i class="text-danger">*</i></label>
                    <select id="jns_kelamin_pengurus_{{ $index }}" name="jns_kelamin_pengurus[]"
                      class="form-control custom-select" required>
                      <option value="">-- Pilih Satu --</option>
                      @foreach ($master_jenis_kelamin as $b)
                      <option value="{{$b->id_jenis_kelamin}}"
                        {{ $b->id_jenis_kelamin == $item->jenis_kelamin ? 'selected' : '' }}>
                        {{$b->jenis_kelamin}}
                      </option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="ktp_pengurus_{{ $index }}">Nomor KTP <i class="text-danger">*</i></label>
                    <input class="form-control" type="text" id="ktp_pengurus_{{ $index }}" name="ktp_pengurus[]"
                      placeholder="Masukkan nomor KTP" minlength="16" maxlength="16" pattern=".{16,16}"
                      onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                      value="{{ $item->nik_pengurus }}" {{ $index == 0 ? 'readonly' : '' }} required>
                  </div>
                </div>
              </div>
              <!-- END: Baris 1 -->

              {{-- START: Baris 2 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="tempat_lahir_pengurus_{{ $index }}">Tempat Lahir <i class="text-danger">*</i></label>
                    <input class="form-control" type="text" maxlength="35" id="tempat_lahir_pengurus_{{ $index }}"
                      name="tempat_lahir_pengurus[]" placeholder="Masukkan tempat lahir"
                      value="{{ $item->tempat_lahir }}" required>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="tanggal_lahir_pengurus_{{ $index }}">Tanggal Lahir <i class="text-danger">*</i></label>
                    <input class="form-control" type="date" id="tanggal_lahir_pengurus_{{ $index }}"
                      name="tanggal_lahir_pengurus[]" max="<?= date("Y-m-d"); ?>" placeholder="Masukkan tanggal lahir"
                      value="{{ $item->tgl_lahir }}" onchange="$(this).valid()" required>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="row">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label for="txt_kode_operator_pengurus_{{ $index }}">Kode Negara <i
                            class="text-danger">*</i></label>
                        <select class="form-control custom-select" id="txt_kode_operator_pengurus_{{ $index }}"
                          name="txt_kode_operator_pengurus[]" {{ $index == 0 ? 'readonly' : '' }} required>

                          @foreach ($master_kode_operator as $b)
                          <option value="{{$b->kode_operator}}" {{ $b->kode_operator == $item->kode_operator ? 'selected' : '' }}>
                            ({{$b->kode_operator}}) {{$b->negara}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                      <div class="form-group div_hp">
                        <label for="no_telp_pengurus_{{ $index }}">Nomor Telepon <i class="text-danger">*</i>
                        </label>
                        <input type="text" minlength="9" maxlength="13" name="no_telp_pengurus[]"
                          id="no_telp_pengurus_{{ $index }}" class="form-control no-zero eight"
                          onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                          placeholder="Contoh:8xxxxxxxxxx" value="{{ substr($item->no_tlp, strlen($item->kode_operator)) }}"
                          {{ $index == 0 ? 'readonly' : '' }} required>
                      </div>
                    </div>
                  </div>

                </div>

              </div>
              {{-- END: Baris 2 --}}

              {{-- START: Baris 3 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group row">
                    <label for="agama_pengurus_{{ $index }}" class="col-12">Agama <i class="text-danger">*</i></label>
                    <div class="col-12">
                      <select class="form-control custom-select" id="agama_pengurus_{{ $index }}"
                        name="agama_pengurus[]" required>
                        <option value="">-- Pilih Satu --</option>
                        @foreach ($master_agama as $b)
                        <option value="{{$b->id_agama}}" {{ $b->id_agama == $item->agama ? 'selected' : '' }}>
                          {{$b->agama}}
                        </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="pendidikan_terakhir_pengurus_{{ $index }}">Pendidikan Terakhir <i
                        class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="pendidikan_terakhir_pengurus_{{ $index }}"
                      name="pendidikan_terakhir_pengurus[]" required>
                      <option value="">-- Pilih Satu --</option>
                      @foreach ($master_pendidikan as $b)
                      <option value="{{$b->id_pendidikan}}"
                        {{ $b->id_pendidikan == $item->pendidikan_terakhir ? 'selected' : '' }}>
                        {{$b->pendidikan}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="npwp_pengurus_{{ $index }}">Nomor NPWP <i class="text-danger">*</i></label>
                    <input class="form-control " type="text" maxlength="15" minlength="15"
                      id="npwp_pengurus_{{ $index }}" name="npwp_pengurus[]" pattern=".{15,15}"
                      onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                      placeholder="Masukkan nomor NPWP" value="{{ $item->npwp }}" {{ $index == 0 ? 'readonly' : '' }}
                      required>
                  </div>
                </div>
              </div>
              {{-- END: Baris 3 --}}

              {{-- START: Baris 4 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="jabatan_pengurus_{{ $index }}">Jabatan <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="jabatan_pengurus_{{ $index }}"
                      name="jabatan_pengurus[]" required>
                      <option value="">-- Pilih Satu --</option>
                      @foreach ($master_jabatan as $jabs)
                      <option value="{{$jabs->id}}" {{ $item->jabatan == $jabs->id ? 'selected' : '' }}>
                        {{$jabs->jabatan}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
              </div>
              {{-- END: Baris 4 --}}

              {{-- START: Baris 5 --}}
              <div class="form-row">
                <div class="col-12">
                  <h6 class="line my-4 font-weight-bold">Alamat Domisili Pengurus &nbsp</h6>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="alamat_pengurus_{{ $index }}">Alamat<i class="text-danger">*</i></label>
                    <textarea class="form-control form-control-lg" maxlength="90" id="alamat_pengurus_{{ $index }}"
                      name="alamat_pengurus[]" rows="6" placeholder="Masukkan alamat lengkap Anda.."
                      required>{{ $item->alamat }}</textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="provinsi_pengurus_{{ $index }}">Provinsi <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="provinsi_pengurus_{{ $index }}"
                      name="provinsi_pengurus[]" onchange="provinsiChange(this.value, this.id, 'kota_pengurus_{{ $index }}')"
                      required>
                      <option value="">-- Pilih Satu --</option>
                      @foreach ($master_provinsi as $data)
                      <option value={{$data->kode_provinsi}}
                        {{ $item->provinsi == $data->kode_provinsi ? 'selected' : '' }}>
                        {{$data->nama_provinsi}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kota_pengurus_{{ $index }}">Kota/Kabupaten <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="kota_pengurus_{{ $index }}" name="kota_pengurus[]"
                      onchange="kotaChange(this.value, this.id, 'kecamatan_pengurus_{{ $index }}')" required>
                      <option value="">-- Pilih Satu --</option>
                    </select>
                  </div>
                </div>
              </div>
              {{-- END: Baris 5 --}}

              {{-- START: Baris 6 --}}
              <div class="form-row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kecamatan_pengurus_{{ $index }}">Kecamatan <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="kecamatan_pengurus_{{ $index }}"
                      name="kecamatan_pengurus[]" onchange="kecamatanChange(this.value, this.id, 'kelurahan_pengurus_{{ $index }}')"
                      required>
                      <option value="">-- Pilih Satu --</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kelurahan_pengurus_{{ $index }}">Kelurahan <i class="text-danger">*</i></label>
                    <select class="form-control custom-select" id="kelurahan_pengurus_{{ $index }}"
                      name="kelurahan_pengurus[]" onchange="kelurahanChange(this.value, this.id, 'kode_pos_pengurus_{{ $index }}')"
                      required>
                      <option value="">-- Pilih Satu --</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="kode_pos_pengurus_{{ $index }}">Kode Pos <i class="text-danger">*</i></label>
                    <input class="form-control" type="text" id="kode_pos_pengurus_{{ $index }}"
                      name="kode_pos_pengurus[]" placeholder="--" value="{{ $item->kode_pos }}" readonly>
                  </div>
                </div>
              </div>
              {{-- END: Baris 6 --}}

              {{-- START: Baris 7 --}}
              {{-- START: Foto Pengurus  --}}
              <div class="row">
                <div class="col-12">
                  <h6 class="line my-4 font-weight-bold">Foto Pengurus &nbsp</h6>
                </div>

                @if ($index == 0)
                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto Diri <i class="text-danger">*</i></label>
                  <div id="preview_camera_diri_pengurus_{{ $index }}" name="preview_camera_diri_pengurus[]"
                    class="pt-3">
                    <img class="imagePreview" id="preview-1"
                      src="{{ route('getUserFile', ['filename' => str_replace('/',':', $item->foto_diri)]) .'?t='.date("Y-m-d h:i:sa") }}"
                      style="width: 180px; height: 138px; top: 32px;">
                    <div>
                      <button class="btn btn-primary mt-4" type="button" id="btn_camera_diri_pengurus_{{ $index }}"
                        name="btn_camera_diri_pengurus[]" onclick="cameraDiriPengurusClick({{ $index }})">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_diri_pengurus_{{ $index }}" name="take_camera_diri_pengurus[]" class="d-none">
                    <div class="col p-0">
                      <img src="{{URL::to('assets/img/user-guide.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_diri_pengurus_{{ $index }}" name="camera_diri_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_diri_pengurus_{{ $index }}" name="take_snapshot_diri_pengurus[]"
                        onclick="takeSnapshotDiriPengurus({{ $index }})">
                      <input type="hidden" id="user_camera_diri_pengurus_{{ $index }}"
                        name="user_camera_diri_pengurus[]" class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_diri_pengurus_{{ $index }}" name="result_camera_diri_pengurus[]"
                    class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_diri_pengurus_{{ $index }}" name="result_diri_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_diri_pengurus_val_{{ $index }}"
                    name="user_camera_diri_pengurus_val[]" value="{{ $item ? $item->foto_diri : '' }}" required>
                </div>
                @endif

                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto KTP <i class="text-danger">*</i></label>
                  <div id="preview_camera_ktp_pengurus_{{ $index }}" name="preview_camera_ktp_pengurus[]" class="pt-3">
                    <img class="imagePreview" id="preview-1"
                      src="{{ route('getUserFile', ['filename' => str_replace('/',':', $item->foto_ktp)]) .'?t='.date("Y-m-d h:i:sa") }}"
                      style="width: 180px; height: 138px; top: 32px;">
                    <div>
                      <button class="btn btn-primary mt-4" type="button" id="btn_camera_ktp_pengurus_{{ $index }}"
                        name="btn_camera_ktp_pengurus[]" onclick="cameraKtpPengurusClick({{ $index }})">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_ktp_pengurus_{{ $index }}" name="take_camera_ktp_pengurus[]" class="d-none">
                    <div class="col p-0">
                      <img src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_ktp_pengurus_{{ $index }}" name="camera_ktp_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_ktp_pengurus_{{ $index }}" name="take_snapshot_ktp_pengurus[]"
                        onclick="takeSnapshotKtpPengurus({{ $index }})">
                      <input type="hidden" id="user_camera_ktp_pengurus_{{ $index }}" name="user_camera_ktp_pengurus[]"
                        class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_ktp_pengurus_{{ $index }}" name="result_camera_ktp_pengurus[]" class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_ktp_pengurus_{{ $index }}" name="result_ktp_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_ktp_pengurus_val_{{ $index }}"
                    name="user_camera_ktp_pengurus_val[]" value="{{ $item ? $item->foto_ktp : '' }}" required>
                </div>

                @if ($index == 0)
                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto Diri & KTP <i class="text-danger">*</i></label>
                  <div id="preview_camera_diri_dan_ktp_pengurus_{{ $index }}"
                    name="preview_camera_diri_dan_ktp_pengurus[]" class="pt-3">
                    <img class="imagePreview" id="preview-{{ $index }}"
                      src="{{ route('getUserFile', ['filename' => str_replace('/',':', $item->foto_diri_ktp)]) .'?t='.date("Y-m-d h:i:sa") }}"
                      style="width: 180px; height: 138px; top: 32px;">
                    <div>
                      <button class="btn btn-primary mt-4" type="button"
                        id="btn_camera_diri_dan_ktp_pengurus_{{ $index }}" name="btn_camera_diri_dan_ktp_pengurus[]"
                        onclick="cameraDiriDanKtpPengurusClick({{ $index }})">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_diri_dan_ktp_pengurus_{{ $index }}" name="take_camera_diri_dan_ktp_pengurus[]"
                    class="d-none">
                    <div class="col p-0">
                      <img src="{{URL::to('assets/img/guide-diridanktp.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_diri_dan_ktp_pengurus_{{ $index }}" name="camera_diri_dan_ktp_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_diri_dan_ktp_pengurus_{{ $index }}"
                        name="take_snapshot_diri_dan_ktp_pengurus[]"
                        onclick="takeSnapshotDiriDanKtpPengurus({{ $index }})">
                      <input type="hidden" id="user_camera_diri_dan_ktp_pengurus_{{ $index }}"
                        name="user_camera_diri_dan_ktp_pengurus[]" class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_diri_dan_ktp_pengurus_{{ $index }}"
                    name="result_camera_diri_dan_ktp_pengurus[]" class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_diri_dan_ktp_pengurus_{{ $index }}" name="result_diri_dan_ktp_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_diri_dan_ktp_pengurus_val_{{ $index }}"
                    name="user_camera_diri_dan_ktp_pengurus_val[]" value="{{ $item ? $item->foto_diri_ktp : '' }}" required>
                </div>
                @endif

                <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                  <label>Foto NPWP <i class="text-danger">*</i></label>
                  <div id="preview_camera_npwp_pengurus_{{ $index }}" name="preview_camera_npwp_pengurus[]"
                    class="pt-3">
                    <img class="imagePreview" id="preview-{{ $index }}"
                      src="{{ route('getUserFile', ['filename' => str_replace('/',':', $item->foto_npwp)]) .'?t='.date("Y-m-d h:i:sa") }}"
                      style="width: 180px; height: 138px; top: 32px;">
                    <div>
                      <button class="btn btn-primary mt-4" type="button" id="btn_camera_npwp_pengurus_{{ $index }}"
                        name="btn_camera_npwp_pengurus[]" onclick="cameraNpwpPengurusClick({{ $index }})">
                        Kamera
                      </button>
                    </div>
                  </div>

                  <div id="take_camera_npwp_pengurus_{{ $index }}" name="take_camera_npwp_pengurus[]" class="d-none">
                    <div class="col p-0">
                      <img src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                        style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                      <div id="camera_npwp_pengurus_{{ $index }}" name="camera_npwp_pengurus[]"></div>
                      <input class="btn btn-primary mt-4" type="button" value="Ambil Foto"
                        id="take_snapshot_npwp_pengurus_{{ $index }}" name="take_snapshot_npwp_pengurus[]"
                        onclick="takeSnapshotNpwpPengurus({{ $index }})">
                      <input type="hidden" id="user_camera_npwp_pengurus_{{ $index }}"
                        name="user_camera_npwp_pengurus[]" class="image-tag"><br />
                    </div>
                  </div>

                  <div id="result_camera_npwp_pengurus_{{ $index }}" name="result_camera_npwp_pengurus[]"
                    class="d-none">
                    <label class="my-3">Hasil</label>
                    <div id="result_npwp_pengurus_{{ $index }}" name="result_npwp_pengurus[]"></div>
                  </div>
                  <input type="text" class="input-hidden2" id="user_camera_npwp_pengurus_val_{{ $index }}"
                    name="user_camera_npwp_pengurus_val[]" value="{{ $item ? $item->foto_npwp : '' }}" required>
                </div>

                @if ($index != 0)
                <div class="col-md-12 mb-3">
                  <button class="btn btn-danger float-right remove" type="button"
                    onclick="removePengurus({{ $index + 1}}, {{ $item->pengurus_id }})"> Hapus
                    Data
                    Pengurus</button>
                </div>
                @endif

              </div>
              {{-- END: Foto Pengurus --}}
              {{-- END: Baris 7 --}}
            </div>

            <div class="row">
              <div class="col-md-6">
                <p class="text-danger float-left"><b>(*) Wajib Diisi</b></p>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
  @endforeach
  {{-- END: Pengurus --}}

  {{-- START: Pengurus N --}}
  <div id="add-new-form"></div>
  {{-- END: Pengurus N --}}

  <div class="row mt-4">

    <div class="col-md-12">

      <button id="add-more-pengurus" class="btn btn-success float-left mt-2" type="button"><i
          class="fas fa-plus mr-2"></i>Tambah
        Data
        Pengurus</button>

      <button id="btn-edit-profile" type="button" class="btn btn-success float-right mt-2">
        <i class="fa fa-check mr-2"></i> Simpan</button>

    </div>
  </div>
</form>
{{--END: Form Super Lender --}}

{{-- START: Script --}}
@push('add-script')

{{-- START: Init Var --}}
<script>
  let total_pengurus = "{{ $total_pengurus }}"

  const formatRupiah = (angka) => {
    let number_string = angka.replace(/[^,\d]/g, '').toString(),
    split = number_string.split(','),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    
    if(ribuan){
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
    
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return rupiah;
  }
  let n = parseInt(total_pengurus);
  const dynamicNumber = (nx) => {
    let card_title = $('span[name="card-title[]"]')
    for (let i = 0; i < nx; i++) { 
      $(card_title[i]).text(i + 2)
    } 
  }
  dynamicNumber(total_pengurus)

  const removePengurus = (layout_id, pengurus_id) => {
    swal.fire({
      title: "Konfirmasi",
      text: "Yakin ingin menghapus data ? data yang dihapus tidak bisa dikembalikan lagi.",
      type : "warning",
      showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonText: "Batal",
      cancelButtonText: "Hapus",
      confirmButtonColor: "#70B29C",
      cancelButtonColor: "#FFCA28",
    }).then( function (response) {
      if (response.dismiss){
        if (pengurus_id) {
          let delete_url = "{{ route('deletePengurus', ['id' => ':id']) }}";
          $.ajax({
            url: delete_url.replace(':id', pengurus_id),
            method : "get",
            dataType: 'JSON',
            beforeSend: function() {
              Swal.fire({
                html: '<h5>Menghapus Data Pengurus...</h5>',
                onBeforeOpen: () => {
                  Swal.showLoading();
                },
                allowOutsideClick: () => !Swal.isLoading()
              })
            },
            success:function(response){
              if (!response.error) {
                swal.fire({
                  title: "Berhasil",
                  type : "success",
                  text: response.msg,
                  showCancelButton: false,
                  confirmButtonClass: "btn-success",
                }).then(function() {
                  $('#layout-pengurus-'+layout_id).remove();
                  // n--;
                  dynamicNumber(n)
                })
              }
            },
            error:function(response) {
              console.log(response)
            }
          });
        } else {
          $('#layout-pengurus-'+layout_id).remove();
          // n--;
          dynamicNumber(n)
        }
      }
    });
  }
</script>
{{-- END: Init Var --}}

{{-- START: Camera Click Action --}}
<script>
  $(document).ready(function(){
    $("#btn_camera_npwp_badan_hukum").click(function(){
      Webcam.attach( '#camera_npwp_badan_hukum' );
      $("#preview_camera_npwp_badan_hukum").addClass('d-none');
      $("#take_camera_npwp_badan_hukum").removeClass('d-none')
    });
  });

  const cameraDiriPengurusClick = (x) => {
    let camera_diri_pengurus = $(`#camera_diri_pengurus_${x}`)
    let preview_camera_diri_pengurus = $(`#preview_camera_diri_pengurus_${x}`)
    let take_camera_diri_pengurus = $(`#take_camera_diri_pengurus_${x}`)
    let btn_camera_diri_pengurus = $(`#btn_camera_diri_pengurus_${x}`)

    if (camera_diri_pengurus.length) {
        Webcam.attach(`#camera_diri_pengurus_${x}`);
        preview_camera_diri_pengurus.addClass('d-none');
        take_camera_diri_pengurus.removeClass('d-none');
    }
  }

  const cameraKtpPengurusClick = (x) => {
    let camera_ktp_pengurus = $(`#camera_ktp_pengurus_${x}`)
    let preview_camera_ktp_pengurus = $(`#preview_camera_ktp_pengurus_${x}`)
    let take_camera_ktp_pengurus = $(`#take_camera_ktp_pengurus_${x}`)
    let btn_camera_ktp_pengurus = $(`#btn_camera_ktp_pengurus_${x}`)

    if (camera_ktp_pengurus.length) {
      Webcam.attach(`#camera_ktp_pengurus_${x}`);
      preview_camera_ktp_pengurus.addClass('d-none');
      take_camera_ktp_pengurus.removeClass('d-none');
    }
  }

  const cameraDiriDanKtpPengurusClick = (x) => {
    let camera_diri_dan_ktp_pengurus = $(`#camera_diri_dan_ktp_pengurus_${x}`)
    let preview_camera_diri_dan_ktp_pengurus = $(`#preview_camera_diri_dan_ktp_pengurus_${x}`)
    let take_camera_diri_dan_ktp_pengurus = $(`#take_camera_diri_dan_ktp_pengurus_${x}`)
    let btn_camera_diri_dan_ktp_pengurus = $(`#btn_camera_diri_dan_ktp_pengurus_${x}`)

    if (camera_diri_dan_ktp_pengurus.length) {
      Webcam.attach(`#camera_diri_dan_ktp_pengurus_${x}`);
      preview_camera_diri_dan_ktp_pengurus.addClass('d-none');
      take_camera_diri_dan_ktp_pengurus.removeClass('d-none');
    }
  }

  const cameraNpwpPengurusClick = (x) => {
    let camera_npwp_pengurus = $(`#camera_npwp_pengurus_${x}`)
    let preview_camera_npwp_pengurus = $(`#preview_camera_npwp_pengurus_${x}`)
    let take_camera_npwp_pengurus = $(`#take_camera_npwp_pengurus_${x}`)
    let btn_camera_npwp_pengurus = $(`#btn_camera_npwp_pengurus_${x}`)

    if (camera_npwp_pengurus.length) {
      Webcam.attach(`#camera_npwp_pengurus_${x}`);
      preview_camera_npwp_pengurus.addClass('d-none');
      take_camera_npwp_pengurus.removeClass('d-none');
    }
  }
</script>
{{-- END: Camera Click Action --}}

{{-- START: Take Snapshot Camera --}}
<script>
  // START: Camera NPWP Badan Hukum  
  const takeSnapshot = () => {
    $("#result_camera_npwp").removeClass('d-none');
    Webcam.snap( function(data_uri) {
      $('#user_camera_npwp_badan_hukum').val(data_uri)
      $('#user_camera_npwp_badan_hukum_val').val('1').valid()
      $('#result_npwp_perusahaan').empty().html(`<img src="${data_uri}" style="width:200px;height:160px;" />`);
    });
  }
  // END: Camera NPWP Badan Hukum

  const takeSnapshotDiriPengurus = (x) => {
    let take_snapshot_diri_pengurus = $(`#take_snapshot_diri_pengurus_${x}`)
    let result_camera_diri_pengurus = $(`#result_camera_diri_pengurus_${x}`)
    let user_camera_diri_pengurus = $(`#user_camera_diri_pengurus_${x}`)
    let user_camera_diri_pengurus_val = $(`#user_camera_diri_pengurus_val_${x}`)
    let result_diri_pengurus = $(`#result_diri_pengurus_${x}`)

    result_camera_diri_pengurus.removeClass('d-none');
      Webcam.snap( function(data_uri) {
        user_camera_diri_pengurus.val(data_uri);
        user_camera_diri_pengurus_val.val('1').valid();
        result_diri_pengurus.html(`
          <img src="${data_uri}" style="width:200px;height:160px;" />
        `);
    });
  }

  const takeSnapshotKtpPengurus = (x) => {
    let take_snapshot_ktp_pengurus = $(`#take_snapshot_ktp_pengurus_${x}`)
    let result_camera_ktp_pengurus = $(`#result_camera_ktp_pengurus_${x}`)
    let user_camera_ktp_pengurus = $(`#user_camera_ktp_pengurus_${x}`)
    let user_camera_ktp_pengurus_val = $(`#user_camera_ktp_pengurus_val_${x}`)
    let result_ktp_pengurus = $(`#result_ktp_pengurus_${x}`)

    result_camera_ktp_pengurus.removeClass('d-none');
    Webcam.snap( function(data_uri) {
      user_camera_ktp_pengurus.val(data_uri);
      user_camera_ktp_pengurus_val.val('1').valid();
      result_ktp_pengurus.html(` 
        <img src="${data_uri}" style="width:200px;height:160px;" />
      `);
    });
  }

  const takeSnapshotDiriDanKtpPengurus = (x) => {
    let take_snapshot_diri_dan_ktp_pengurus = $(`#take_snapshot_diri_dan_ktp_pengurus_${x}`)
    let result_camera_diri_dan_ktp_pengurus = $(`#result_camera_diri_dan_ktp_pengurus_${x}`)
    let user_camera_diri_dan_ktp_pengurus = $(`#user_camera_diri_dan_ktp_pengurus_${x}`)
    let user_camera_diri_dan_ktp_pengurus_val = $(`#user_camera_diri_dan_ktp_pengurus_val_${x}`)
    let result_diri_dan_ktp_pengurus = $(`#result_diri_dan_ktp_pengurus_${x}`)

    result_camera_diri_dan_ktp_pengurus.removeClass('d-none');
    Webcam.snap( function(data_uri) {
      user_camera_diri_dan_ktp_pengurus.val(data_uri);
      user_camera_diri_dan_ktp_pengurus_val.val('1').valid();
      result_diri_dan_ktp_pengurus.html(`
        <img src="${data_uri}" style="width:200px;height:160px;" />
      `);
    });
  }

  const takeSnapshotNpwpPengurus = (x) => {
    let take_snapshot_npwp_pengurus = $(`#take_snapshot_npwp_pengurus_${x}`)
    let result_camera_npwp_pengurus = $(`#result_camera_npwp_pengurus_${x}`)
    let user_camera_npwp_pengurus = $(`#user_camera_npwp_pengurus_${x}`)
    let user_camera_npwp_pengurus_val = $(`#user_camera_npwp_pengurus_val_${x}`)
    let result_npwp_pengurus = $(`#result_npwp_pengurus_${x}`)

    result_camera_npwp_pengurus.removeClass('d-none');
    Webcam.snap( function(data_uri) {
      user_camera_npwp_pengurus.val(data_uri);
      user_camera_npwp_pengurus_val.val('1').valid();
      result_npwp_pengurus.html(`
        <img src="${data_uri}" style="width:200px;height:160px;" />
      `);
    });
  }
</script>
{{-- END: Take Snapshot Camera --}}

{{-- START: Get Json Data --}}
<script>
  let detil = "{{ $detil ? '1' : '' }}"
  if (detil) {
    
    // Tanggal Akte Perubahan
    let tgl_berdiri_bdn_hukum = "{{ $detil ? $detil->tgl_berdiri : ''}}"
    if (tgl_berdiri_bdn_hukum) {
      $('#tgl_akta_perubahan_bdn_hukum').attr('min', tgl_berdiri_bdn_hukum)
    }

    // Laporan Keuangan
    let laporan_keuangan = "{{ $detil ? $detil->laporan_keuangan : ''}}"
    if (laporan_keuangan) {
      $('#laporan_keuangan_bdn_hukum').removeAttr('required')
      var str = laporan_keuangan.lastIndexOf('/');
      var result = laporan_keuangan.substring(str + 1);
      
      $('.custom-file-label').text(result)
    }

    // START: Omset & Aset Bdn Hukum
    let omset_tahun_terakhir = "{{ $detil ? (int)$detil->omset_tahun_terakhir : ''}}"
    let tot_aset_tahun_terakhr = "{{ $detil ? (int)$detil->tot_aset_tahun_terakhr : ''}}"

    $('#omset_tahun_terakhir').val(formatRupiah( omset_tahun_terakhir))
    $('#tot_aset_tahun_terakhr').val(formatRupiah( tot_aset_tahun_terakhr))
    // END: Omset & Aset Bdn Hukum

    // START: History Alamat
    let provinsi_bdn_hukum = "{{ $detil ? $detil->provinsi_perusahaan : ''}}"
    let kota_bdn_hukum = "{{ $detil ? $detil->kota_perusahaan : ''}}"
    let kecamatan_bdn_hukum = "{{ $detil ? $detil->kecamatan_perusahaan : ''}}"
    let kelurahan_bdn_hukum = "{{ $detil ? $detil->kelurahan_perusahaan : ''}}"

    if (provinsi_bdn_hukum) {
      $.getJSON("/getKota/"+provinsi_bdn_hukum, function(data_kota){
        $.each(data_kota.kota,function(index,value){
            $('#kota_bdn_hukum').append(
                '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
            );
        })

        $(`#kota_bdn_hukum option[value='${kota_bdn_hukum}']`).prop('selected', true);
      });
    }

    if (kota_bdn_hukum) {
      $.getJSON("/getKecamatan/"+kota_bdn_hukum, function(data_kecamatan){
        for(let i = 0; i<data_kecamatan.length; i++){
          $('#kecamatan_bdn_hukum').append($('<option>', { 
              value: data_kecamatan[i].text,
              text : data_kecamatan[i].text
          }));

          $(`#kecamatan_bdn_hukum option[value='${kecamatan_bdn_hukum}']`).prop('selected', true);
        }
      }); 
    }

    if (kecamatan_bdn_hukum) {
      $.getJSON("/borrower/data_kelurahan/"+kecamatan_bdn_hukum, function(data_kelurahan){
        for(let i = 0; i<data_kelurahan.length; i++){
          $('#kelurahan_bdn_hukum').append($('<option>', { 
              value: data_kelurahan[i].text,
              text : data_kelurahan[i].text
          }));

          $(`#kelurahan_bdn_hukum option[value='${kelurahan_bdn_hukum}']`).prop('selected', true);
        }
      });
    }
    // END: History Alamat
  }

  // START: History Alamat Pengurus
  let pengurus = {!! $pengurus !!}
  if (pengurus) {
    pengurus.forEach((data, index_pengurus) => {
      let provinsi_pengurus = data.provinsi
      let kota_pengurus = data.kota
      let kecamatan_pengurus = data.kecamatan
      let kelurahan_pengurus = data.kelurahan

      if (provinsi_pengurus) {
        $.getJSON("/getKota/"+provinsi_pengurus, function(data_kota){
          $.each(data_kota.kota,function(index,value){
            $('#kota_pengurus_'+index_pengurus).append(
                '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
            );
          })
          
          $(`#kota_pengurus_${index_pengurus} option[value='${kota_pengurus}']`).prop('selected', true);
        });
      }

      if (kota_pengurus) {
        $.getJSON("/getKecamatan/"+kota_pengurus, function(data_kecamatan){
          for(let i = 0; i<data_kecamatan.length; i++){
            $('#kecamatan_pengurus_'+index_pengurus).append($('<option>', { 
                value: data_kecamatan[i].text,
                text : data_kecamatan[i].text
            }));

            $(`#kecamatan_pengurus_${index_pengurus} option[value='${kecamatan_pengurus}']`).prop('selected', true);
          }
        }); 
      }

      if (kecamatan_pengurus) {
        $.getJSON("/borrower/data_kelurahan/"+kecamatan_pengurus, function(data_kelurahan){
          for(let i = 0; i<data_kelurahan.length; i++){
            $('#kelurahan_pengurus_'+index_pengurus).append($('<option>', { 
                value: data_kelurahan[i].text,
                text : data_kelurahan[i].text
            }));

            $(`#kelurahan_pengurus_${index_pengurus} option[value='${kelurahan_pengurus}']`).prop('selected', true);
          }
        });
      }
    });
  }
  // END: History Alamat Pengurus
</script>
{{-- END: Get Json Data --}}

{{-- START: DOM Manipulation --}}
<script>
  // START: Alamat
  const provinsiChange = (thisValue, thisId, nextId) => {
    $('#'+nextId).empty(); // set null
    $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
    $.getJSON("/getKota/"+thisValue, function(data_kota){
      $.each(data_kota.kota,function(index,value){
          $('#'+nextId).append(
              '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
          );
      })
    });
  }

  const kotaChange = (thisValue, thisId, nextId) => {
    $('#'+nextId).empty(); // set null
    $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
    $.getJSON("/getKecamatan/"+thisValue, function(data_kecamatan){
      for(let i = 0; i<data_kecamatan.length; i++){
        $('#'+nextId).append($('<option>', { 
            value: data_kecamatan[i].text,
            text : data_kecamatan[i].text
        }));
      }
    });
  }

  const kecamatanChange = (thisValue, thisId, nextId) => {
    $('#'+nextId).empty(); // set null
    $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
    $.getJSON("/borrower/data_kelurahan/"+thisValue+"/", function(data){
        for(let i = 0; i<data.length; i++){ 
            $('#'+nextId).append($('<option>', {
                value: data[i].text,
                text : data[i].text
            }));
        }
    });
  }

  const kelurahanChange = (thisValue, thisId, nextId) => {
    $.getJSON("/borrower/data_kode_pos/"+thisValue+"/", function(data){
        $('#'+nextId).val(data[0].text)
    });
  }
  // END: Alamat

  $(document).ready(function(){
    // Kirim Data
    $('#btn-edit-profile').click(function() {
      if($("#form-super-lender").valid()) {
        if ($('#label_laporan_keuangan_bdn_hukum').hasClass('has-error')) {
          $('#laporan_keuangan_bdn_hukum').focus()
        } else {
          $('#form-super-lender').trigger('submit')
        }
      } else {
        return false;
      }
    })

    $("#form-super-lender").submit(function(e) {
      e.preventDefault()

      let form = $("#form-super-lender");
      let formData = new FormData(this);
      $.ajax({
        url:$(this).attr("action"),
        data: formData,
        // cache:false,
        contentType: false,
        processData: false,
        type:$(this).attr("method"),
        beforeSend: function() {
          Swal.fire({
            html: '<h5>Menyimpan Data...</h5>',
            onBeforeOpen: () => {
              Swal.showLoading();
            },
            allowOutsideClick: () => !Swal.isLoading()
          })
        },
        success:function(response) {
          console.log(response)
          if (response.error == true) {
            swal.fire({
              title: "Gagal",
              type : "error",
              text: response.msg,
              showCancelButton: false,
              confirmButtonClass: "btn-success",
            })
          } else {
            swal.fire({
              title: "Berhasil",
              type : "success",
              text: response.msg,
              showCancelButton: false,
              confirmButtonClass: "btn-success",
            }).then(function() {
              location.href="/user/dashboard" ;
            })
          }
        },
        error:function(response) {
          
          if (response.msg) {
            swal.fire({
              title: "Error",
              type: "error",
              text: response.msg,
              showCancelButton: false,
              confirmButtonClass: "btn-danger",
            })
          } else {
            swal.fire({
              title: "Informasi",
              type: "warning",
              text: "file yang diunggah melebihi batas penyimpanan",
              showCancelButton: false,
              confirmButtonClass: "btn-danger",
            })
          }
        
          console.log(response)
        }
      });
    })
  })
</script>
{{-- END: DOM Manipulation --}}

{{-- START: Validation --}}
<script>
  $.extend( $.validator.messages, {
    required: "Harus Diisi",
    minlength: $.validator.format( "Harap masukkan setidaknya {0} karakter." ),
    min: $.validator.format("Harap masukkan nilai yang lebih besar dari atau sama dengan {0}."),
    max: $.validator.format("Harap masukkan nilai yang kurang dari atau sama dengan {0}."),
  } );
  $("#form-super-lender").validate({
    // lang: 'id',
    onfocusout: false,
    ignore: "hidden",
    messages: {
      tgl_akta_perubahan_bdn_hukum : {
        max: "Harap Masukan Tanggal Lebih Kecil atau Sama Dengan Tanggal Akta Pendirian",
        min: "Harap Masukan Tanggal Lebih Besar atau Sama Dengan Tanggal Akta Pendirian",
      }
    },
    invalidHandler: function(e, validator){
      if(validator.errorList.length){
        $('.coll').addClass('show')
        validator.errorList[0].element.focus()
      }
    }
  });

  $('#laporan_keuangan_bdn_hukum').on('change', function() {
    let validExtensions = ["doc", "docx", "xls", "xlsx", "pdf"]
    let file = $(this).val().split('.').pop();
    if (validExtensions.indexOf(file) == -1) {
      $('#label_laporan_keuangan_bdn_hukum').addClass("has-error")
      $('#label_laporan_keuangan_bdn_hukum').text('').append('<i class="text-danger">Format laporan keuangan tidak didukung</i>')
    } else {
      $("#label_laporan_keuangan_bdn_hukum").removeClass("has-error");
      $('#label_laporan_keuangan_bdn_hukum').text('').append('Laporan keuangan<i class="text-danger">*</i>')
    }

    let fileName = $(this).val().split('\\').pop();
    $('.custom-file-label').addClass("selected").text(fileName);
    // $(this).next('.custom-file-label').addClass("selected").html(fileName);
  });

  $(".no-zero").on("input", function(){
      let thisValue = $(this).val()
      if (thisValue.charAt(0) == '0') {
          $(this).val(thisValue.substring(1))
      }
  });

  $(".no-four").on("input", function(){
      let thisValue = $(this).val()
      if (thisValue.charAt(0) == '4') {
          $(this).val(thisValue.substring(1))
      }
  });

  $(".eight").on("input", function(){
      let thisValue = $(this).val()
      if (thisValue.charAt(0) != '8') {
          $(this).val(thisValue.substring(1))
      }
  });

  $('.checkKarakterAneh').on('input', function (event) { 
      this.value = this.value.replace(/[^a-zA-Z 0-9 _.]/g, '');
  });

  $('.number-only').on('input', function (event) {
    this.value = this.value.replace(/[^0-9]/g, '');
  });
</script>
{{-- END: Validation --}}

{{-- START: Other --}}
<script>
  $(document).ready(function() {

    // START: Pengurus
    $("#add-more-pengurus").click(function(){
      n++;
      let html = (`
      <div class="mt-4 layout-pengurus" id="layout-pengurus-${n}">
        <div class="card">
          <div class="card-header" id="card-pengurus-${n}" data-toggle="collapse" data-target="#collapse-${n}"
            aria-expanded="true" aria-controls="collapse-${n}">
            <p class="h5 font-weight-bold">
              Informasi Pengurus <span name="card-title[]"></span> <i class="fa fa-angle-up float-right"></i><i
                class="fa fa-angle-down float-right"></i>
            </p>
            </h5>
          </div>
      
          <div id="collapse-${n}" class="collapse coll show" aria-labelledby="card-pengurus-${n}"
            data-parent="#layout-pengurus-${n}">
            <div class="card-body">
      
              <div id="pengurus">
                <!-- START: Baris 1 -->
                <div class="form-row">
                  
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="nama_pengurus_${n}">Nama Pengurus <i class="text-danger">*</i></label>
                      <input type="hidden" id="pengurus_id_${n}" name="pengurus_id[]">
                      <input class="form-control" type="text" id="nama_pengurus_${n}" minlength="3" maxlength="30"
                        name="nama_pengurus[]" placeholder="Masukkan Nama Pengurus..." required>
                    </div>
                  </div>
              
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="jns_kelamin_pengurus_${n}">Jenis Kelamin <i class="text-danger">*</i></label>
                      <select id="jns_kelamin_pengurus_${n}" name="jns_kelamin_pengurus[]" class="form-control custom-select"
                        required>
                        <option value="">-- Pilih Satu --</option>
                        @foreach ($master_jenis_kelamin as $b)
                        <option value="{{$b->id_jenis_kelamin}}">
                          {{$b->jenis_kelamin}}
                        </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
              
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="ktp_pengurus">Nomor KTP <i class="text-danger">*</i></label>
                      <input class="form-control" type="text" id="ktp_pengurus_${n}" name="ktp_pengurus[]"
                        placeholder="Masukkan nomor KTP" minlength="16" maxlength="16" pattern=".{16,16}"
                        oninput="this.setCustomValidity('')"
                        oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')"
                        onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()" required>
                    </div>
                  </div>
                </div>
                <!-- END: Baris 1 -->
              
                {{-- START: Baris 2 --}}
                <div class="form-row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="tempat_lahir_pengurus_${n}">Tempat Lahir <i class="text-danger">*</i></label>
                      <input class="form-control" type="text" maxlength="35" id="tempat_lahir_pengurus_${n}"
                        name="tempat_lahir_pengurus[]" placeholder="Masukkan tempat lahir" required>
                    </div>
                  </div>
              
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="tanggal_lahir_pengurus_${n}">Tanggal Lahir <i class="text-danger">*</i></label>
                      <input class="form-control" type="date" id="tanggal_lahir_pengurus_${n}" name="tanggal_lahir_pengurus[]"
                        max="<?= date("Y-m-d"); ?>" placeholder="Masukkan tanggal lahir" onchange="$(this).valid()" required>
                    </div>
                  </div>
              
                  <div class="col-md-4">
                    <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                          <label for="txt_kode_operator_pengurus_${n}">Kode Negara <i class="text-danger">*</i></label>
                          <select class="form-control custom-select" id="txt_kode_operator_pengurus_${n}"
                            name="txt_kode_operator_pengurus[]" required>
              
                            @foreach ($master_kode_operator as $b)
                            <option value="{{$b->kode_operator}}" {{ $b->kode_operator == "62" ? 'selected' : '' }}>
                              ({{$b->kode_operator}}) {{$b->negara}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
              
                      <div class="col-md-6 col-sm-12">
                        <div class="form-group div_hp">
                          <label for="no_telp_pengurus_${n}">Nomor Telepon <i class="text-danger">*</i>
                          </label>
                          <input type="text" minlength="9" maxlength="13" name="no_telp_pengurus[]" id="no_telp_pengurus_${n}"
                            onkeyup="if ($(this).val().charAt(0) == '0') {$(this).val($(this).val().substring(1))} else if ($(this).val().charAt(0) != '8') { $(this).val($(this).val().substring(1))}; this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                            class="form-control no-zero eight" placeholder="Contoh:8xxxxxxxxxx" required>
                        </div>
                      </div>
                    </div>
              
                  </div>
              
                </div>
                {{-- END: Baris 2 --}}
              
                {{-- START: Baris 3 --}}
                <div class="form-row">
                  <div class="col-md-4">
                    <div class="form-group row">
                      <label for="agama_pengurus_${n}" class="col-12">Agama <i class="text-danger">*</i></label>
                      <div class="col-12">
                        <select class="form-control custom-select" id="agama_pengurus_${n}" name="agama_pengurus[]" required>
                          <option value="">-- Pilih Satu --</option>
                          @foreach ($master_agama as $b)
                          <option value="{{$b->id_agama}}" {{ old('agama_pengurus[]') == $b->agama ? 'selected' : '' }}>
                            {{$b->agama}}
                          </option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
              
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="pendidikan_terakhir_pengurus_${n}">Pendidikan Terakhir <i class="text-danger">*</i></label>
                      <select class="form-control custom-select" id="pendidikan_terakhir_pengurus_${n}"
                        name="pendidikan_terakhir_pengurus[]" required>
                        <option value="">-- Pilih Satu --</option>
                        @foreach ($master_pendidikan as $b)
                        <option value="{{$b->id_pendidikan}}"
                          {{ old('pendidikan_terakhir_pengurus[]') == $b->id_pendidikan ? 'selected' : '' }}>
                          {{$b->pendidikan}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
              
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="npwp_pengurus_${n}">Nomor NPWP <i class="text-danger">*</i></label>
                      <input class="form-control " type="text" maxlength="15" minlength="15" id="npwp_pengurus_${n}"
                        name="npwp_pengurus[]" pattern=".{15,15}" oninput="this.setCustomValidity('')"
                        oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')"
                        onkeyup="this.value = this.value.replace(/[^0-9]/g, '');$(this).valid()" placeholder="Masukkan nomor NPWP"
                        required>
                    </div>
                  </div>
                </div>
                {{-- END: Baris 3 --}}
              
                {{-- START: Baris 4 --}}
                <div class="form-row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="jabatan_pengurus_${n}">Jabatan <i class="text-danger">*</i></label>
                      <select class="form-control custom-select" id="jabatan_pengurus_${n}" name="jabatan_pengurus[]" required>
                        <option value="">-- Pilih Satu --</option>
                        @foreach ($master_jabatan as $jabs)
                        <option value="{{$jabs->id}}">
                          {{$jabs->jabatan}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4"></div>
                  <div class="col-md-4"></div>
                </div>
                {{-- END: Baris 4 --}}
              
                {{-- START: Baris 5 --}}
                <div class="form-row">
                  <div class="col-12">
                    <h6 class="line my-4 font-weight-bold">Alamat Domisili Pengurus &nbsp</h6>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="alamat_pengurus_${n}">Alamat<i class="text-danger">*</i></label>
                      <textarea class="form-control form-control-lg" maxlength="90" id="alamat_pengurus_${n}" name="alamat_pengurus[]"
                        rows="6" placeholder="Masukkan alamat lengkap Anda.." required></textarea>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="provinsi_pengurus_${n}">Provinsi <i class="text-danger">*</i></label>
                      <select class="form-control custom-select" id="provinsi_pengurus_${n}" name="provinsi_pengurus[]"
                        onchange="provinsiChange(this.value, this.id, 'kota_pengurus_${n}')" required>
                        <option value="">-- Pilih Satu --</option>
                        @foreach ($master_provinsi as $data)
                        <option value={{$data->kode_provinsi}}>
                          {{$data->nama_provinsi}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="kota_pengurus_${n}">Kota/Kabupaten <i class="text-danger">*</i></label>
                      <select class="form-control custom-select" id="kota_pengurus_${n}" name="kota_pengurus[]"
                        onchange="kotaChange(this.value, this.id, 'kecamatan_pengurus_${n}')" required>
                        <option value="">-- Pilih Satu --</option>
                      </select>
                    </div>
                  </div>
                </div>
                {{-- END: Baris 5 --}}
              
                {{-- START: Baris 6 --}}
                <div class="form-row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="kecamatan_pengurus_${n}">Kecamatan <i class="text-danger">*</i></label>
                      <select class="form-control custom-select" id="kecamatan_pengurus_${n}" name="kecamatan_pengurus[]"
                        onchange="kecamatanChange(this.value, this.id, 'kelurahan_pengurus_${n}')" required>
                        <option value="">-- Pilih Satu --</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="kelurahan_pengurus_${n}">Kelurahan <i class="text-danger">*</i></label>
                      <select class="form-control custom-select" id="kelurahan_pengurus_${n}" name="kelurahan_pengurus[]"
                        onchange="kelurahanChange(this.value, this.id, 'kode_pos_pengurus_${n}')" required>
                        <option value="">-- Pilih Satu --</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="kode_pos_pengurus_${n}">Kode Pos <i class="text-danger">*</i></label>
                      <input class="form-control" type="text" id="kode_pos_pengurus_${n}" name="kode_pos_pengurus[]" placeholder="--"
                        readonly>
                    </div>
                  </div>
                </div>
                {{-- END: Baris 6 --}}
              
                {{-- START: Baris 7 --}}
                {{-- START: Foto Pengurus 1 --}}
                <div class="row">
                  <div class="col-12">
                    <h6 class="line my-4 font-weight-bold">Foto Pengurus &nbsp</h6>
                  </div>
              
                  <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                    <label>Foto KTP <i class="text-danger">*</i></label>
                    <div id="preview_camera_ktp_pengurus_${n}" name="preview_camera_ktp_pengurus[]" class="pt-3">
                      <img class="imagePreview" id="preview-${n}">
                      <div>
                        <button class="btn btn-primary" type="button" id="btn_camera_ktp_pengurus_${n}"
                          name="btn_camera_ktp_pengurus[]" onclick="cameraKtpPengurusClick(${n})">
                          Kamera
                        </button>
                      </div>
                    </div>
              
                    <div id="take_camera_ktp_pengurus_${n}" name="take_camera_ktp_pengurus[]" class="d-none">
                      <div class="col p-0">
                        <img id="user-guide5" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                          style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                        <div id="camera_ktp_pengurus_${n}" name="camera_ktp_pengurus[]"></div>
                        <input class="btn btn-primary mt-4" type="button" value="Ambil Foto" id="take_snapshot_ktp_pengurus_${n}"
                          name="take_snapshot_ktp_pengurus[]" onclick="takeSnapshotKtpPengurus(${n})">
                        <input type="hidden" id="user_camera_ktp_pengurus_${n}" name="user_camera_ktp_pengurus[]"
                          class="image-tag"><br />
                      </div>
                    </div>
              
                    <div id="result_camera_ktp_pengurus_${n}" name="result_camera_ktp_pengurus[]" class="d-none">
                      <label class="my-3">Hasil</label>
                      <div id="result_ktp_pengurus_${n}" name="result_ktp_pengurus[]"></div>
                    </div>
                    <input type="text" class="input-hidden2" id="user_camera_ktp_pengurus_val_${n}"
                      name="user_camera_ktp_pengurus_val[]" required>
                  </div>
              
                  <div class="col-xl-3 col-md-4 col-sm-6 mb-4">
                    <label>Foto NPWP <i class="text-danger">*</i></label>
                    <div id="preview_camera_npwp_pengurus_${n}" name="preview_camera_npwp_pengurus[]" class="pt-3">
                      <img class="imagePreview" id="preview-${n}">
                      <div>
                        <button class="btn btn-primary" type="button" id="btn_camera_npwp_pengurus_${n}" name="btn_camera_npwp_pengurus[]"
                          onclick="cameraNpwpPengurusClick(${n})">
                          Kamera
                        </button>
                      </div>
                    </div>
              
                    <div id="take_camera_npwp_pengurus_${n}" name="take_camera_npwp_pengurus[]" class="d-none">
                      <div class="col p-0">
                        <img id="user-guide6" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide"
                          style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                        <div id="camera_npwp_pengurus_${n}" name="camera_npwp_pengurus[]"></div>
                        <input class="btn btn-primary mt-4" type="button" value="Ambil Foto" id="take_snapshot_npwp_pengurus_${n}"
                          name="take_snapshot_npwp_pengurus[]" onclick="takeSnapshotNpwpPengurus(${n})">
                        <input type="hidden" id="user_camera_npwp_pengurus_${n}" name="user_camera_npwp_pengurus[]"
                          class="image-tag"><br />
                      </div>
                    </div>
              
                    <div id="result_camera_npwp_pengurus_${n}" name="result_camera_npwp_pengurus[]" class="d-none">
                      <label class="my-3">Hasil</label>
                      <div id="result_npwp_pengurus_${n}" name="result_npwp_pengurus[]"></div>
                    </div>
                    <input type="text" class="input-hidden2" id="user_camera_npwp_pengurus_val_${n}"
                      name="user_camera_npwp_pengurus_val[]" required>
                  </div>
              
                  <div class="col-md-12 mb-3">
                    <button class="btn btn-danger float-right remove" type="button" onclick="removePengurus(${n}, '')"> Hapus
                      Data
                      Pengurus</button>
                  </div>
                </div>
                {{-- END: Foto Pengurus 1 --}}
                {{-- END: Baris 7 --}}
              </div>

            </div>

          </div>
        </div>
      </div>
      `);

      $("#add-new-form").append(html);
      dynamicNumber(n)
    });
    // END: Pengurus
    
  });
</script>
{{-- END: Other --}}
@endpush
{{-- END: Script --}}
{{-- END: Super Lender --}}