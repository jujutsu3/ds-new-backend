@extends('layouts.admin.master')

@section('title', 'Panel Admin Compliance')
{{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> --}}

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Resume Pendanaan</h1>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Daftar Pendanaan</strong>
                </div>
                <div class="card-body">
                    <!-- table select all admin -->
                    <table id="pengajuanKPR_data" class="table table-striped table-bordered table-responsive-sm">
                        <thead>
                            <tr>
                                <th style="display: none;">Id</th>
                                <th>No</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Tanggal Verifikasi</th>
                                <th>Penerima Pendanaan</th>
                                <th>Jenis Pendanaan</th>
                                <th>Tujuan Pendanaan</th>
                                <th>Nilai Pengajuan Pendanaan</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!-- end of table select all -->
                </div>
            </div>
        </div>
    </div>
</div><!-- .content -->

<style>
    .modal-xl {
        max-width: 95% !important;
    }
</style>


<!-- 2. GOOGLE JQUERY JS v3.2.1  JS !-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>

<script type="text/javascript">
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });
</script>
<script type="text/javascript">
    $(document).ready(function() {

             var pengajuKPR_table = $('#pengajuanKPR_data').DataTable({
                 searching: true,
                 processing: true,
                 "order": [
                     [1, "asc"]
                 ],
                 ajax: {
                     url: '/admin/adr/ListPengajuanCompliance',
                     dataSrc: 'data'
                 },
                 paging: true,
                 info: true,
                 lengthChange: false,
                 pageLength: 10,
                 columns: [{
                         data: 'pengajuan_id'
                     },
                     {
                         data: null,
                         render: function(data, type, row, meta) {
                             return meta.row + 1;
                        }
                     },
                     {
                         data: 'tanggal_pengajuan'
                     },
                     {
                        data: 'tanggal_verifikasi'
                     },
                     {
                         data: 'penerima_pendanaan'
                     },
                     {
                         data: 'jenis_pendanaan',
                         render: function(data, type, row, meta) {
                            //return row.jenis_pendanaan;
                            
                            if(row.jenis_pendanaan === '1'){
                                return 'DANA KONSTRUKSI';
                            }else{
                                return 'DANA RUMAH';
                            }
                            
                         }
                     },
                     {
                         data: 'tujuan_pembiayaan'
                     },
                     {
                         data: 'nilai_pengajuan_pendanaan',
                         render: $.fn.dataTable.render.number( '.', ',', 2, 'Rp' ) 
                         /*render: function(data, type, row, meta) {
                             return '<p class=text-right>' + row.nilai_pengajuan_pendanaan + '</p>';
                         }*/
                     },
                     {
                         data: null,
                         render: function(data, type, row, meta) {
                             if (row.status_compliance == 13) {
                                var disable = '';
                                var button_type = 'secondary';
                             } else if (row.status_compliance == 14) {
                                var disable = '';
                                var button_type = 'primary';
                             } else if (row.status_compliance == 15) {
                                /*
                                if ('<?php // echo $cekUser->name ;?>' == 'Compliant'){
                                    var disable = '';
                                }else{
                                    var disable = 'disabled';
                                }
                                */
                                /*var disable = 'disabled';*/ 
                                var button_type = 'success';
                             } else {
                                var button_type = 'info'; 
                             }
                             if (row.status_compliance == 15) {
                                return '<button id="tombol" class="btn btn-' + button_type + ' btn-block" ' + disable +'> Selesai </button>';
                             }else{
                                return '<button id="tombol" class="btn btn-' + button_type + ' btn-block" ' + disable +'>' + row.keterangan_status + '</button>';
                             } 
                         }
                     },
                 ],
                 columnDefs: [{
                     targets: [0],
                     visible: false
                    },
                    {
                     targets: [7],
                     className: 'text-right'
                    }
                 ]
             })
             var id;

             $('#pengajuanKPR_data tbody').on('click', '#tombol', function() {
                 var data = pengajuKPR_table.row($(this).parents('tr')).data();
                 idPengajuan = data.pengajuan_id;
                 brw_id = data.brw_id;
                 window.location.href = "../adr/detilresumependanaan/" + idPengajuan;
             })

    });
</script>

@endsection