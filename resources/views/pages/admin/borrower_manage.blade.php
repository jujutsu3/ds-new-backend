@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Kelola Tautan Verifikasi Pendaftaran Penerima Pembiayaan</h1>
            </div>
        </div>
    </div>
</div>


<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if (session('error'))
            <div class="alert alert-danger col-sm-12">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif (session('success'))
            <div class="alert alert-success col-sm-12">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="alert alert-success col-sm-12" id="error_search" style="display: none;">
                Data Tidak Ditemukan !
            </div>
            <div class="card" id="view_card_search">
                <div class="card-header">
                    <strong class="card-title">Pencarian Penerima Pembiayaan</strong>
                </div>
                <div class="card-body">
                    <form class="form" id="form_search">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">Akun</label>
                            <div class="col-lg-10">
                                <input type="text" name="username" class="form-control" id="username" autocomplete="off"
                                    placeholder="Masukkan username Penerima Pembiayaan"
                                    onkeyup="this.value = this.value.replace(/\s/g, '');">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">Email</label>
                            <div class="col-lg-10">
                                <input type="email" name="searchEmail" class="form-control" id="searchEmail"
                                    autocomplete="off" placeholder="Masukkan email Penerima Pembiayaan" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">No Telepon</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-md-4 pr-md-0 pt-md-0 pt-2">
                                        <select name="in_kode_operator" id="in_kode_operator"
                                            class="form-control custom-select"
                                            onchange="$('#in_no_hp').val('')">
                                            @foreach ($master_kode_operator as $item)
                                            <option value="{{$item->kode_operator}}" {{ $item->kode_operator == '62' ?
                                                'selected' : ''}}>
                                                ({{$item->kode_operator}}) {{$item->negara}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pt-md-0 pt-2">
                                        <input type="text" class="form-control ff-style" id="in_no_hp" maxlength="15"
                                            name="in_no_hp"
                                            placeholder="Masukkan no handphone Penerima Pembiayaan, contoh: 831xxxxxx"
                                            onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary rounded float-right mx-2" type="submit" id="search"> <i
                                class="fa fa-search"></i> Cari</button>
                    </form>
                </div>
            </div>

            <div class="card" id="view_card_table" style="display: none;">
                <div class="card-header">
                    <strong class="card-title">Data Penerima Pembiayaan</strong>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_penerima_pendanaan" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="display: none;">Id</th>
                                    <th width="3%">No</th>
                                    <th width="18%">Akun </th>
                                    <th width="25%">Email</th>
                                    <th width="12%">Status</th>
                                    <th style="display: none;">email_verif</th>
                                    <th style="display: none;">otp</th>
                                    <th width="45%">Informasi Tautan Verifikasi / OTP Penerima Pembiayaan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div><!-- .content -->

<link rel="stylesheet" href="/css/jquery_step/jquery.steps.css">
<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/jquery_step/jquery.steps.js"></script>
<script src="/js/jquery_step/jquery.validate.min.js"></script>

<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        $('#search').on('click', function(e) {
            e.preventDefault();
            var username = $('#username').val();
            var search_email = $('#searchEmail').val();
            let kode_operator = $('#in_kode_operator').val();
            let phone_number = $('#in_no_hp').val();

            let dataSearch = {
                username: username ? username : null,
                search_email: search_email ? search_email : null,
                kode_operator: kode_operator ? kode_operator : null,
                phone_number: phone_number ? phone_number : null
            }

            if (username == '' && search_email == '' && phone_number == '') {
                Swal.fire({
                    title: "Peringatan",
                    text: "Salah satu field harus terisi",
                    type: "warning",
                })
                return false
            }

            console.log(dataSearch)
            $.ajax({
                url: '/admin/borrower/data',
                method: 'post',
                dataType: 'json',
                data: dataSearch,
                beforeSend: function() {
                    Swal.fire({
                        html: '<h5>Mencari Data ...</h5>',
                        onBeforeOpen: () => {
                            Swal.showLoading();
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                },
                success: function(data) {
                    Swal.close()
                    if (data.status == 'Ada') {
                        $('#view_card_search').attr('style', 'display: none');
                        $('#view_card_table').attr('style', 'display: block');
                        penerimaPendanaanTable = $('#table_penerima_pendanaan').DataTable({
                            searching: true,
                            processing: true,
                            // serverSide: true,
                            ajax: {
                                url: '/admin/borrower/get_borrower_datatables',
                                type: 'post',
                                data: dataSearch,
                                dataSrc: 'data'
                            },
                            paging: true,
                            info: true,
                            lengthChange: false,
                            order: [1, 'asc'],
                            pageLength: 10,
                            columns: [{
                                    data: 'idBrw'
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        //I want to get row index here somehow
                                        return meta.row + 1;
                                    }
                                },
                                {
                                    data: 'username'
                                },
                                {
                                    data: 'email'
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        if (row.status == 'notfilled') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>notfilled</button>';
                                        } else if (row.status == 'Not Active') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>not active</button>';
                                        } else if (row.status == 'pending') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>Pending</button>';
                                        } else if (row.status == 'reject') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>Reject</button>';
                                        } else if (row.status == 'active') {
                                            return '<button class="btn btn-outline-success btn-sm active" disabled>active</button>';
                                        } else if (row.status == 'suspend') {
                                            return '<button class="btn btn-outline-secondary btn-sm active" disabled>suspend</button>';
                                        } else if (row.status == 'expired') {
                                            return '<button class="btn btn-outline-secondary btn-sm active" disabled>expired password</button>';
                                        }else if (row.status == 'notpreapproved') {
                                            return '<button class="btn btn-outline-secondary btn-sm active" disabled>Not Preapproved</button>';
                                        }
                                    }
                                },
                                {
                                    data: 'email_verif',
                                    visible: false
                                },
                                {
                                    data: 'otp',
                                    visible: false
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        if (row.status == 'Not Active') {
                                            var url = "{{ config('app.url') }}"+"/borrower/confirm-email";
                                            var link = url+'/'+row.email_verif;
                                            return '<button class="btn btn-outline-success btn-sm btn-block" id="btn_email_verif" value="'+link+'" title="click to copy Link Verification">'+link+'&nbsp <i class="menu-icon fa fa-copy"></i></button>';
                                        } else if (row.status == 'notfilled' || row.status == 'notpreapproved'){
                                            if(row.otp == null || row.otp == ''){
                                                return "<i style='color:red'>otp belum tersedia</i>";
                                            }
                                            return '<button class="btn btn-outline-success btn-sm active" disabled>'+row.otp+'</button>'+
                                            '<button class="btn btn-outline-success copy-button btn-sm" id="otp" value="'+row.otp+'" title="click to copy OTP" ><i class="menu-icon fa fa-copy"></i></button>';
                                        }
                                    }
                                }
                            ],
                            columnDefs: [{
                                targets: [0],
                                visible: false
                            }]
                        });
                    } else {
                        $('#error_search').attr('style', 'display: block');
                    }
                },
                error: function(error) {
                    Swal.close()
                    alert('Koneksi Gagal, Periksa Koneksi Internet Anda !');
                    console.log(error)
                }
            })
        });

        $('#table_penerima_pendanaan tbody').on( 'click', '#btn_email_verif', function () {
            var data = penerimaPendanaanTable.row( $(this).parents('tr') ).data();
            email_verif = data.email_verif;
            url = "{{ config('app.url') }}"+"/borrower/confirm-email";
            link = url+'/'+email_verif;
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(link).select();
            document.execCommand("copy");
            $temp.remove();
            swal.fire('Link Tersalin');

        });

        $('#table_penerima_pendanaan tbody').on( 'click', '#otp', function () {
            var data = penerimaPendanaanTable.row( $(this).parents('tr') ).data();
            otp = data.otp;
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val(data.otp).select();
            document.execCommand("copy");
            $temp.remove();
            swal.fire('OTP Tersalin');
        });
    });

    $('#in_no_hp').on("input", function(){
        let in_kode_operator = $('#in_kode_operator').val()
        let thisValue = $(this).val()

        if (in_kode_operator == '62' ) {
            if (thisValue.charAt(0) == '6' && thisValue.charAt(1) == '2') {
                $(this).val(thisValue.substring(2))
            } else {
                if (thisValue.charAt(0) != '8') {
                    $(this).val(thisValue.substring(1))
                }
            }
        } 
    })
</script>
@endsection