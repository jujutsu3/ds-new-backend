@extends('layouts.admin.master')

@section('title', 'Cart Material Order ')
<link href="{{ asset('assetsBorrower/js/plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet"
    type="text/css" />
<link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }} " rel="stylesheet" />
<link href="{{ url('assetsBorrower/js/plugins/select2/css/select2-bootstrap.min.css') }} " rel="stylesheet" />
<style>
    .scroll {
       overflow-y: scroll;
       height: 600px;
       display: block;
   }
</style>

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-shopping-cart"></i> Material Order - Keranjang</h1>
                </div>
            </div>
        </div>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item" aria-current="page"><a href="{{ route('material_order.list') }}"
                    style="text-decoration: underline">List</a></li>
            <li class="breadcrumb-item" aria-current="page"><a
                    href="{{ route('material_order.rab.detail', $pengajuan->pengajuan_id) }}"
                    style="text-decoration: underline">Detail</a></li>
            <li class="breadcrumb-item" aria-current="page"  style="text-decoration: underline"><a
                href="{{ route('material_order.add.products', $pengajuan->pengajuan_id) }}"
                style="text-decoration: underline">Pilih Barang</a></li>
            <li class="breadcrumb-item active" aria-current="page">Keranjang</li>
        </ol>
    </nav>


    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white" style="background-color: #e67e22">
                        <i class="fa fa-shopping-bag"></i> Keranjang
                    </div>
                    <div class="card-body">
                        <input type="hidden" id="pengajuanId" name="pengajuanId" value="{{ $pengajuan->pengajuan_id }}"/>
                        <div class="col-md-12 mb-4">
                            <section style="background-color: #eaf2d7;">
                                <div class="container py-5 scroll" id="productCart">
                                </div>
                            </section>
                        </div>

                        <div class="col-12 mb-4">
                            <a href="{{ route('material_order.detail.checkout', $pengajuan->pengajuan_id) }}" class="col-12 btn btn-warning text-lg-center btnCheckout" style="background-color: #e67e22;color: white"><i class="fa fa-check"></i> Checkout <span id="countItems"></span> </a>
                        </div>

                    
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

            const productCartItems = () => {

            $.ajax({
                url: "/admin/material_orders/ajax/cart",
                type: "get",
                dataType: "json",
                data: {
                    _token: "{{ csrf_token() }}",
                    pengajuan_id: $("#pengajuanId").val(),
                },

                beforeSend: () => {
                    swal.fire({
                        html: '<h5>Loading Keranjang ... </h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                error: function(xhr, status, error) {

                    var errorMessage = xhr.status + ': ' + xhr.statusText

                    if (xhr.status === 419) {
                        errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                    }

                    swal.fire({
                            title: "Proses Gagal",
                            text: errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        });
                },
                success: function(result) {
                  
                    $('#productCart').html(result.message);
 
                    if(result.count == 0){
                        $(".btnCheckout").addClass('d-none');
                    }else{
                        $(".btnCheckout").removeClass('d-none');
                    }
                    swal.close();
                }
            });

            }

         const updateCart = (cart_id, qty, action) => {
            $.ajax({
                url: "/admin/material_orders/ajax/cart/update",
                type: "post",
                dataType: "json",
                data: {
                    _token: "{{ csrf_token() }}",
                    cart_id: cart_id,
                    qty: qty,
                    action: action
                },
                success: function(result) {

                    if(action == 'delete'){
                        productCartItems();
                    }
                }
            });
        }

        $(document).ready(function() {
             productCartItems();
        });
    </script>


@endsection
