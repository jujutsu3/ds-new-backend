@extends('layouts.admin.master')

@section('title', 'Detail Material Order ')
<link href="{{ asset('assetsBorrower/js/plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet"
    type="text/css" />
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1><i class="fa fa-shopping-cart"></i> Material Order - Detail</h1>
                </div>
            </div>
        </div>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item" aria-current="page"><a href="{{ route('material_order.list') }}" style="text-decoration: underline">List</a></li>
          <li class="breadcrumb-item active" aria-current="page">Detail</li>
        </ol>
      </nav>


    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white" style="background-color: #18783A">
                        <i class="fa fa-info"></i>  Informasi Pengajuan
                    </div>
                    <div class="card-body">

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna" class="control-label">Nama Penerima  Pendanaan</label>
                                <input class="form-control" id="nama_penerima_pendanaan" name="nama_penerima_pendanaan" value="{{ ($pengajuan) ? $pengajuan->nama_cust : '' }}" readonly>
                            </div>
                        </div>

                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna" class="control-label">No. Telepon</label>
                                <input class="form-control" id="telepon" name="telepon" value="{{ ($pengajuan) ? $pengajuan->no_telp : '' }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_estimasi">Tanggal Pengajuan</label>
                                <input class="form-control" style="width: 30%" type="text" id="tgl_pengajuan" name="tgl_pengajuan" value="{{ ($pengajuan) ? \Carbon\Carbon::parse($pengajuan->tgl_pengajuan)->format('d/m/Y') : '' }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna" class="control-label">Tujuan Pendanaan</label>
                                <input class="form-control tujuan_pendanaan" id="tujuan_pendanaan" name="tujuan_pendanaan" value="{{ ($pengajuan) ? $pengajuan->tujuan_pembiayaan : '' }}" readonly>
                            </div>
                        </div>
                        
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Tipe Pendanaan</label>
                                <input class="form-control type_pendanaan" id="type_pendanaan" name="type_pendanaan" value="{{ ($pengajuan) ? $pengajuan->tipe_pendanaan : '' }}" readonly>
                            </div>
                        </div>
                        
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Kebutuhan Dana </label>
                                <input type="text" style="width: 50%" class="form-control" id="pendanaan_dana_dibutuhkan" name="pendanaan_dana_dibutuhkan" value="Rp. {{ ($pengajuan) ? number_format($pengajuan->pendanaan_dana_dibutuhkan,2) : '' }}" readonly>
                            </div>
                        </div>
                        
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_estimasi">Perkiraan estimasi proyek akan mulai dikerjakan</label>
                                <input class="form-control" style="width: 30%" type="text" id="date_estimasi" name="date_estimasi" value="{{ ($pengajuan && $pengajuan->estimasi_mulai !== NULL) ? \Carbon\Carbon::parse($pengajuan->estimasi_mulai)->format('d/m/Y') : '' }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="content mt-3">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white" style="background-color: #18783A">
                        <i class="fa fa-shopping-cart"></i>  Rencana Anggaran Belanja Barang
                    </div>
                    <div class="card-body">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 mb-4">
                                    <div class="col-md-3 control-label">
                                        <label> Tanggal Permintaan </label>
                                     </div> 
                                    <div class="col-md-9">
                                        <input type="text" name="requitionDate" class="form-control" value="{{ ($pengajuan && $pengajuan->requisition_date !== NULL) ? \Carbon\Carbon::parse($pengajuan->requisition_date)->format('d/m/Y') : "-" }}"  readonly/>
                                    </div>
                
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-12 mb-4">
                                    <div class="col-md-3 control-label">
                                        <label> Alamat Pengiriman </label>
                                     </div> 
                                    <div class="col-md-9">
                                       <textarea name="deliveryAddress" class="form-control" readonly cols="100" rows="8">{!! ($pengajuan) ? $pengajuan->delivery_to_address : '-' !!}  </textarea>
                                    </div>
                
                                </div>    
                            </div>
                
                        </div>

                        <div class="col-md-6 text-left"><a href="{{ route('material_order.add.products', $pengajuan->pengajuan_id) }}" class="btn btn-secondary text-lg-center"><i class="fa fa-search"></i> Pilih Barang</a></div>
                        <div class="col-md-6 text-right"> <a href="{{ route('material_order.detail.cart', $pengajuan->pengajuan_id) }}" class="btn btn-danger text-lg-center"><i class="fa fa-shopping-cart"></i> Keranjang ({!! $countItems !!})</a></div>

                        <div class="col-md-12 mt-4">
    
                            <table class="table table-striped table-bordered" id="itemRabTable" width="100%">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>No.</th>
                                        <th>Tahap</th>
                                        <th>Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Unit Price</th>
                                        <th>Created</th>
                                        <th>Total Harga Barang</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php 
                                        $total = 0;
                                    @endphp
                                    @if(isset($requisitionHeader) && count($requisitionHeader) > 0)
                                        @foreach ($requisitionHeader as $row)
                                                <tr>
                                                    <td>{!! $loop->index+1 !!}</td>
                                                    <td>{!! $row->task_description !!}</td>
                                                    <td>{!! $row->material_item_name !!}</td>
                                                    <td>{!! $row->quantity !!}</td>
                                                    <td>{!! number_format($row->unit_price, 0, ',', '.') !!}</td>
                                                    <td>{!! \Carbon\Carbon::parse($row->creation_date)->format('d/m/Y') !!} <br/> {!! $row->created_by !!}</td>
                                                    <td>{!! number_format(($row->quantity * $row->unit_price) , 0, ',','.') !!}</td>
                                                </tr>
                                                @php
                                                $total += ($row->quantity * $row->unit_price);
                                                @endphp
                                        @endforeach 
                                    @endif
                                </tbody>
                                <tfoot>
                                    <td colspan="6" class="text-right">Total </td>
                                    <td>{!! number_format($total, 0, ',', '.') !!} </td>
                                </tfoot>
                
                            </table>
                
                        </div>  

                       
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ url('assetsBorrower/js/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ url('assetsBorrower/js/plugins/daterangepicker/js/daterangepicker.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
           


        });
    </script>


@endsection
