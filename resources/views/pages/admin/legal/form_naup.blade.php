<div class="card">

    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Nota Analisa Usulan Pendanaan
                        &nbsp</label>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Nomor NAUP <label class="mandatory_label">*</label> <span
                    class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0 col-6">
                <div class="col-md-6 px-0"> <input class="form-control form-control-sm" type="text" id="nomor_naup"
                        name="nomor_naup" required
                        value="(ketik disini)/DR/NAUP-DSI/{{ date('m') }}/{{ date('Y') }}" maxlength="25" />

                </div>

            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Tanggal Permohonan Penerima Pendanaan <span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="tanggal_permohonan_penerima_pendanaan"
                    name="tanggal_permohonan_penerima_pendanaan" readonly>
            </div>
        </div>



        <div class="row mt-2">
            <div class="col-md-5 col-12">Fasilitas Property Ke- <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="fasilitas_property_ke" name="fasilitas_property_ke"
                    readonly>

            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Maksimum FTV <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="maksimum_FTV" name="maksimum_FTV" readonly>
            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Jumlah Permohonan Penerima Pendanaan<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                    </div>

                    <input class="form-control" type="text" id="jumlah_permohonan_penerima_pendanaan"
                        name="jumlah_permohonan_penerima_pendanaan" readonly>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Jumlah Plafond Diusulkan <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                    </div>
                    <input class="form-control" type="text" id="jumlah_plafond_diusulkan"
                        name="jumlah_plafond_diusulkan" readonly>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Uang Muka / Security Deposit<span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                    </div>
                    <input class="form-control" type="text" id="uang_muka_deposit" name="uang_muka_deposit" readonly>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Presentase Uang Muka / Security Deposit<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-1 pl-md-0">
                <div class="input-group mb-3">
                    <input class="form-control" type="text" id="presentase_uang_muka" name="presentase_uang_muka"
                        readonly>
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">%</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Angsuran Pendanaan / Sewa Sesuai Usulan<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <div class="input-group mb-3">
                    <table class="table table-striped table-bordered" id="angsuran_pendanaan">
                    </table>
                </div>
            </div>
        </div>

    </div>

</div>

<!-- Rekomendasi Struktur Pendanaan  -->
<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2 label-struktur-pendanaan">Rekomendasi Struktur Pendanaan
                        &nbsp</label>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Struktur Pendanaan <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0 col-12">

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <input type="checkbox" class="pilih_struktur_pendanaan" id="wakalah" name="wakalah" value="1"> Wakalah
                        </div>
                        <div class="col-md-6">
                            <input type="checkbox"  class="pilih_struktur_pendanaan" id="imbt" name="imbt" value="1"> IMBT
                        </div>
                        <div class="col-md-6">
                            <input type="checkbox"  class="pilih_struktur_pendanaan" id="qard" name="qard" value="1"> Qardh
                        </div>
                        <div class="col-md-6">
                            <input type="checkbox"  class="pilih_struktur_pendanaan" id="mmq" name="mmq" value="1"> MMQ
                        </div>
                        <div class="col-md-6">
                            <input type="checkbox"  class="pilih_struktur_pendanaan" id="murabahah" name="murabahah" value="1"> Murabahah
                        </div>
                        <div class="col-md-6">
                            <input type="checkbox"  class="pilih_struktur_pendanaan" id="ijarah" name="ijarah" value="1"> Ijarah
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="row mt-2 div_wakalah d-none">
            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Wakalah</u></label>
                </div>
            </div>


            <div class="col-md-12 div_wakalah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nama_produk">Nama Produk</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="nama_produk" name="nama_produk" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="jangka_waktu_wakalah">Jangka Waktu</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" id="jangka_waktu_wakalah" name="jangka_waktu_wakalah"
                            placeholder="ketik disini" maxlength="30">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Hari</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_wakalah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-tujuan_penggunaan">Tujuan Penggunaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="tujuan_penggunaan_wakalah"
                            name="tujuan_penggunaan_wakalah" placeholder="ketik disini" maxlength="100">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-sumber_pelunasan">Sumber Pelunasan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="sumber_pelunasan" name="sumber_pelunasan"
                            readonly>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_wakalah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pembelian_dari">Pembelian dari</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="pembelian_dari" name="pembelian_dari" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pengikatan_pendanaan">Pengikatan Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" id="pengikatan_pendanaan_wakalah"
                            name="pengikatan_pendanaan_wakalah">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_wakalah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-jumlah_wakalah">Jumlah Wakalah</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="jumlah_wakalah" name="jumlah_wakalah"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pengikatan_jaminan">Pengikatan Jaminan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" id="pengikatan_jaminan_wakalah"
                            name="pengikatan_jaminan_wakalah">

                        </select>
                    </div>
                </div>
            </div>

        </div>

        <!-- start qardh -->
        <div class="row mt-2 div_qard d-none">
            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Qardh</u></label>
                </div>
            </div>

            <div class="col-md-12 div_qard d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nama_produk_qardh">Nama Produk</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="nama_produk_qardh" name="nama_produk_qardh"
                            readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="jangka_waktu_qardh">Jangka Waktu</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="number" id="jangka_waktu_qardh" name="jangka_waktu_qardh"
                            placeholder="ketik disini" maxlength="30">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Hari</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_qard d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-tujuan_penggunaan_qardh">Tujuan Penggunaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="tujuan_penggunaan_qardh"
                            name="tujuan_penggunaan_qardh" placeholder="ketik disini" maxlength="100">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-sumber_pelunasan_qardh">Sumber Pelunasan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="sumber_pelunasan_qardh"
                            name="sumber_pelunasan_qardh" readonly>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_qard d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pembelian_dari_qardh">Take Over Pendanaan dari</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="take_over_dari_qardh" name="take_over_dari_qardh"
                            placeholder="ketik disini">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pengikatan_pendanaan_qardh">Pengikatan Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" type="text" id="pengikatan_pendanaan_qardh"
                            name="pengikatan_pendanaan_qardh">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_qard d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-jumlah_wakalah_qardh">Jumlah Qardh</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="jumlah_qardh" name="jumlah_qardh"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                    </div>

                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="pengikatan_jaminan_qardh">Pengikatan Jaminan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" type="text" id="pengikatan_jaminan_qardh"
                            name="pengikatan_jaminan_qardh">
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <!-- end qardh -->

        <!-- start murabahah -->
        <div class="row mt-2 div_murabahah d-none">
            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Murabahah</u></label>
                </div>
            </div>


            <div class="col-md-12 div_murabahah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-skema_pendanaan_murabahah">Skema Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="skema_pendanaan_murabahah"
                            name="skema_pendanaan_murabahah" value="Murabahah" readonly>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="total_kewajiban_penerima">Total Kewajiban Penerima Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="total_kewajiban_penerima"
                            name="total_kewajiban_penerima" readonly>
                    </div>
                </div>


            </div>

            <div class="col-md-12 div_murabahah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nama_produk_murabahah">Nama Produk</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="nama_produk_murabahah"
                            name="nama_produk_murabahah" readonly>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pricing_pendanaan_murabahah">Pricing Pendanaan (Margin
                            Rate)</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-8 pl-md-0">
                        <div class="form-group">
                            <input class="form-control" type="text" id="pricing_pendanaan_murabahah"
                                name="pricing_pendanaan_murabahah" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        % effective p.a
                    </div>
                </div>

            </div>

            <div class="col-md-12 div_murabahah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-tujuan_penggunaan_murabahah">Tujuan Penggunaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <textarea class="form-control" cols="60" rows="5" id="tujuan_penggunaan_murabahah"
                            name="tujuan_penggunaan_murabahah"></textarea>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-angsuran_pendanaan_murabahah">Angsuran Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <table class="table table-striped table-bordered" id="angsuran_pendanaan_murabahah">
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_murabahah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-harga_beli_barang_murabahah">Harga Beli Barang</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="harga_beli_barang_murabahah"
                            name="harga_beli_barang_murabahah" readonly>
                    </div>

                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="jangka_waktu_murabahah">Jangka Waktu</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" id="jangka_waktu_murabahah"
                            name="jangka_waktu_murabahah" readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_murabahah d-none">

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-uang_muka_penerima_pendanaan_murabahah">Uang Muka Penerima
                            Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="uang_muka_murabahah" name="uang_muka_murabahah"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-denda_murabahah">Denda</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="denda_murabahah" name="denda_murabahah"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">

                    </div>
                </div>
            </div>
            <div class="col-md-12 div_murabahah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-harga_jual_barang_murabahah">Harga Jual Barang</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="harga_jual_barang_murabahah"
                            name="harga_jual_barang_murabahah" readonly>
                    </div>

                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pengikatan_pendanaan_murabahah">Pengikatan Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" type="text" id="pengikatan_pendanaan_murabahah"
                            name="pengikatan_pendanaan_murabahah">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_murabahah d-none">


                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-margin_murabahah">Porsi/Plafon Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="plafon_pendanaan_murabahah"
                            name="plafon_pendanaan_murabahah" readonly>
                    </div>

                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="pengikatan_jaminan_murabahah">Pengikatan Jaminan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" type="text" id="pengikatan_jaminan_murabahah"
                            name="pengikatan_jaminan_murabahah">
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_murabahah d-none">

                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-margin_murabahah">Margin</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="margin_murabahah" name="margin_murabahah"
                            readonly>

                    </div>
                </div>

            </div>
        </div>

        <!-- end murabahah -->

        <!-- start IMBT -->
        <div class="row mt-2 div_imbt d-none">
            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Al Ijarah Al Muntahiyah bit
                            Al-Tamlik (IMBT)</u></label>
                </div>
            </div>

            <div class="col-md-12 div_imbt d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-skema_pendanaan_imtb">Skema Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="skema_pendanaan_imtb" name="skema_pendanaan_imtb"
                            readonly value="IMBT">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="nilai_sewa_imbt">Nilai Sewa/Ujroh Perbulan (Rp)</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="table-wrapper">    
                        <table class="table table-striped table-bordered" id="nilai_sewa_imbt">
                        </table>
                   </div>
                </div>
            </div>
            <div class="col-md-12 div_imbt d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nama_produk_imtb">Nama Produk</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="nama_produk_imbt" name="nama_produk_imbt"
                            readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="uang_muka_imbt">Security Deposit Penerima Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="uang_muka_imbt" name="uang_muka_imbt"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">

                    </div>
                </div>
            </div>
            <div class="col-md-12 div_imbt d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-tujuan_penggunaan_imtb">Tujuan Penggunaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="tujuan_penggunaan_imbt"
                            name="tujuan_penggunaan_imbt" placeholder="ketik disini">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-porsi_pendanaan_imtb">Porsi Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="porsi_pendanaan_imbt" name="porsi_pendanaan_imbt"
                            readonly>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_imbt d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="jangka_waktu_imbt">Jangka Waktu Sewa</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" id="jangka_waktu_imbt" name="jangka_waktu_imbt"
                            readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pengikatan_pendanaan_imtb">Pengikatan Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" id="pengikatan_pendanaan_imbt" name="pengikatan_pendanaan_imbt">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_imbt d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="harga_perolehan_aktiva_imbt">Harga Perolehan Aktiva</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="harga_perolehan_aktiva_imbt"
                            name="harga_perolehan_aktiva_imbt" readonly>

                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-pengikatan_jaminan_imtb">Pengikatan Jaminan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" id="pengikatan_jaminan_imbt" name="pengikatan_jaminan_imbt">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_imbt d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="ekpektasi_nilai_sewa_imbt">Ekspektasi Nilai Sewa/Ujroh (%) Asumsi 6 (Enam) Bulan
                            Pertama</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" id="ekpektasi_nilai_sewa_imbt"
                            name="ekpektasi_nilai_sewa_imbt" readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">% P.A</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-angsuran_pendanaan_imtb">Angsuran Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                        <table class="table table-striped table-bordered" id="angsuran_pendanaan_imbt">
                        </table>
                </div>
            </div>
            <div class="col-md-12 div_imbt d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="nilai_ujroh_imbt">Nilai Ujroh</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="nilai_ujroh_imbt" name="nilai_ujroh_imbt"
                            readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-denda_imtb">Denda</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="denda_imbt" name="denda_imbt"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                    </div>
                </div>
            </div>
            <div class="col-md-12 div_imbt  d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="penetapan_nilai_ujroh_selanjutnya">Penetapan Nilai Ujroh Selanjutanya
                            dilakukan setiap </label>
                    </div>
                </div>
                <div class="col-md-4">

                    <div class="input-group mb-3">
                        <select class="form-control" id="penetapan_nilai_ujroh_selanjutnya"
                            name="penetapan_nilai_ujroh_selanjutnya">
                            <option value="">Pilih</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- end imbt -->
        <!-- start MMQ -->
        <div class="row mt-2 div_mmq d-none">
            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Musyarakah Mutanaqishah
                            (MMQ)</u></label>
                </div>
            </div>


            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-skema_pendanaan_mmq">Skema Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="skema_pendanaan_mmq" value="MMQ"
                            name="skema_pendanaan_mmq" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-jangka_waktu_pendanaan_mmq">Jangka Waktu Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" id="jangka_waktu_pendanaan_mmq"
                            name="jangka_waktu_pendanaan_mmq" readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nama_produk_mmq">Nama Produk</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" id="nama_produk_mmq" name="nama_produk_mmq" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="jangka_waktu_penyerahan_objek_mmq">Jangka Waktu Penyerahan Objek
                            MMQ</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group mb-3">
                            <input class="form-control" type="text" id="jangka_waktu_penyerahan_objek_mmq"
                                name="jangka_waktu_penyerahan_objek_mmq" placeholder="ketik disini">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Hari/Bulan</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-tujuan_penggunaan_mmq">Tujuan Penggunaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <textarea class="form-control" name="tujuan_penggunaan_mmq" id="tujuan_penggunaan_mmq"
                            cols="60" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-objek_bagi_hasil_mmq">Objek Bagi Hasil</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" id="objek_bagi_hasil_mmq" name="objek_bagi_hasil_mmq">
                            <option value="">Pilih</option>
                            <option value="1">Pendapatan Sewa Dari Pembayaran Angsuran Sewa Per Bulan Oleh Penyewa
                                (Musta'Jir)</option>
                            <option value="2">Lainnya</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none mt-1">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-tujuan_penggunaan_mmq">&nbsp;</label>
                    </div>
                </div>
                <div class="col-md-6 pl-md-4">
                    <ol style="font-size: 12px;">
                        Contoh :
                        <li>Pembelian Aset Berupa Rumah Tinggal Tipe 70/150 Dengan Alamat Jl. …, Blok …, No. …, Rt.
                            <br /> …/Rw. …, Kec. …, Ds/Kel. …, Kab/Kota …, Prov. …, Kode Pos …
                        </li>
                        <li>Take Over KPR Dari Bank ... <br />dan/atau Penambahan Plafon Pendanaan (Top Up) Untuk
                            Renovasi Rumah Tinggal di ... </li>
                        <li> Refinancing Aset Berupa Rumah Tinggal Tipe .../... Dalam Rangka ...</li>
                    </ol>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="harga_beli_aset_mmq">Harga Beli Aset</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="harga_beli_aset_mmq" name="harga_beli_aset_mmq"
                            readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="ekspektasi_nilai_sewa_mmq">Ekspektasi Nilai Sewa/Ujroh (%)</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-8 pl-md-0">
                        <div class="form-group">
                            <input class="form-control" type="text" id="ekspektasi_nilai_sewa_mmq"
                                name="ekspektasi_nilai_sewa_mmq" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        % effective p.a
                    </div>

                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-plafon_pendanaan_mmq">Plafon Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="plafond_rekomendasi_mmq"
                            name="plafond_rekomendasi_mmq" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-proyeksi_total_pendanaan_mmq">Proyeksi Total Pendapatan
                            Sewa/Ujroh</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <table class="table table-striped table-bordered" id="proyeksi_total_pendanaan_mmq">
                    </table>
                </div>

            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        &nbsp;
                    </div>
                </div>
                <div class="col-md-4">
                    &nbsp;
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-proyeksi_total_pendanaan_mmq">Proyeksi Pendapatan
                            Sewa/Ujroh</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="table-wrapper">
                    <table class="table table-striped table-bordered" id="proyeksi_pendanaan_mmq">
                    </table>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-uang_muka_penerima_dana_mmq">Uang Muka/Modal Penerima Dana</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Rp.</span>
                            </div>
                            <input class="form-control" type="text" id="uang_muka_mmq" name="uang_muka_mmq"
                                onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                        </div>


                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nisbah_mmq"><b>Nisbah :</b></label>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="hishshah_per_unit">Modal Syirkah</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="modal_syirkah" name="modal_syirkah" readonly>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nisbah_penyelenggara_mmq">1. Nisbah Penyelenggara</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="number" placeholder="ketik disini"
                            id="nisbah_penyelenggara" name="nisbah_penyelenggara">
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="hishshah_per_unit">Hishshah Per Unit</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="hishshah_per_unit" name="hishshah_per_unit"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nisbah_penerima_mmq">2. Nisbah Penerima Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="number" placeholder="ketik disini"
                            id="nisbah_penerima_dana" name="nisbah_penerima_dana">
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-hishshah_mmq"><b>Hishshah :</b></label>
                    </div>
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-denda_perhari_mmq">Denda (Per Hari Keterlambatan)</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="denda_mmq" name="denda_mmq"
                            onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-hishshah_penyelenggara_mmq">1. Hishshah Penyelenggara</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="hishshah_penyelenggara_mmq"
                            name="hishshah_penyelenggara_mmq" readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Unit</span>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-hishshah_penerima_pendanaan_mmq">2. Hishshah Penerima
                            Pendanaan</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                        </div>
                        <input class="form-control" type="text" id="hishshah_penerima_pendanaan_mmq"
                            name="hishshah_penerima_pendanaan_mmq" readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Unit</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_mmq d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-total_hishshah_mmq">Total Hishshah</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" id="total_hishshah_mmq" name="total_hishshah_mmq"
                            readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Unit</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end MMQ-->

        <!-- start Ijarah -->
        <div class="row mt-2 div_ijarah d-none">
            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Ijarah</u></label>
                </div>
            </div>

            <div class="col-md-12 div_ijarah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-objek_sewa_ijarah">Objek Sewa</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="ketik disini" id="objek_sewa"
                            name="objek_sewa">
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_ijarah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="jangka_waktu_penyerahan_ijarah">Jangka Waktu Sewa</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" id="jangka_waktu_sewa_ijarah"
                            name="jangka_waktu_sewa_ijarah" readonly>
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_ijarah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-jangka_waktu_penyerahan_sewa_ijarah">Jangka Waktu Penyerahan Objek
                            Sewa</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" placeholder="ketik disini"
                            id="jangka_waktu_penyerahan_ijarah" name="jangka_waktu_penyerahan_ijarah">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">Hari / Bulan</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 div_ijarah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-nilai_sewa_ijarah">Nilai Sewa/Ujroh</label>
                    </div>
                </div>
                <div class="col-md-4">
                  <div class="table-wrapper">
                    <table class="table table-striped table-bordered" id="nilai_sewa_ijarah">
                    </table>
                </div>
                </div>
            </div>

            <div class="col-md-12 div_ijarah d-none">
                <div class="col-md-2">
                    <div class="form-group">
                        <label for="wizard-progress2-penyesuaian_nilai_sewa_ijarah">Penyesuaian Nilai Sewa/Ujroh</label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <select class="form-control" id="penyesuaian_nilai_sewa" name="penyesuaian_nilai_sewa">
                            <option value="">Pilih</option>
                            <option value="1">Review Ujrah Dilakukan Pada Setiap Tanggal 01 - 05, Periode November Dan
                                Mei
                                Pada Tahun Berjalan</option>
                            <option value="2">Penyesuaian Ujrah Pada Sistem Dilakukan Pada Tanggal 01 - 05 Periode
                                Desember
                                Dan Juni Tahun Berjalan</option>
                            <option value="3">Penyesuaian Ujrah Berlaku Efektif Pada Setiap Tanggal 01 Periode Januari
                                Dan
                                Juli Tahun Berjalan</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <!-- end ijarah -->

    </div>

</div>


<!-- Data Objek Pendanaan (Agunan/Jaminan)   -->
<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Data Objek Pendanaan
                        (Agunan/Jaminan) &nbsp</label>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Jenis Objek Pendanaan <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="jenis_objek_pendanaan" name="jenis_objek_pendanaan"
                    readonly>

            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Status/Jenis Sertifikat <span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="jenis_agunan" name="jenis_agunan" readonly>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">No Sertifikat <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="nomor_agunan" name="nomor_agunan" readonly>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Tanggal Berakhir Hak <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="tanggal_jatuh_tempo" name="tanggal_jatuh_tempo" readonly>
            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Sertifikat Atas Nama<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="atas_nama_agunan" name="atas_nama_agunan" readonly>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Alamat Sertifikat <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <textarea name="alamat_sertifikat" class="form-control" id="alamat_sertifikat" readonly cols="60"
                    rows="5"></textarea>
                {{-- <input class="form-control" type="text" id="alamat_sertifikat" name="alamat_sertifikat" readonly> --}}
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">No Izin Mendirikan Bangunan (IMB)<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="nomor_imb" name="nomor_imb" readonly>

            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">IMB Atas Nama<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="imbt_atas_nama" name="imbt_atas_nama" placeholder="ketik disini">
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Luas Tanah m2<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="luas_tanah" name="luas_tanah" readonly>

            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Luas Bangunan m2<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="luas_bangunan" name="luas_bangunan" readonly>

            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Tanggal Penilaian Agunan (sesuai Laporan Hasil Appraisal)<span
                    class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="date" id="tanggal_penilaian_agunan" name="tanggal_penilaian_agunan">
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Appraisal<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <select class="form-control" id="appraisal" name="appraisal" required>
                    <option value="">Pilih</option>
                    <option value="1">Internal</option>
                    <option value="2">External (KJPP)</option>
                </select>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Nilai Pasar Wajar<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                    </div>
                    <input class="form-control" type="text" id="nilai_pasar_wajar" name="nilai_pasar_wajar" readonly>

                </div>

            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Nilai Likuidasi<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                    </div>
                    <input class="form-control" type="text" id="nilai_likuidasi" name="nilai_likuidasi" readonly>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Rekomendasi Appraisal<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <select class="form-control" id="rekomendasi_appraisal" name="rekomendasi_appraisal">
                    <option value="">Pilih</option>
                    <option value="1">Direkomendasikan</option>
                    <option value="2">Tidak Direkomendasikan</option>
                </select>
            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Jenis Pengikat Agunan<span class="float-right ml-1">:</span>
            </div>
            <div class="col-md-6 pl-md-0  col-12">
                <select class="form-control" id="jenis_pengikatan_agunan" name="jenis_pengikatan_agunan">
                </select>
            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Nilai Pengikat Agunan<span class="float-right ml-1">:</span>
            </div>

            <div class="col-md-3 pl-md-0">

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                    </div>

                    <input class="form-control" required type="text" id="nilai_pengikatan_agunan" name="nilai_pengikatan_agunan" onkeyup="this.value = formatRupiah(this.value);" placeholder="ketik disini">
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-md-4">
                    <input class="form-control" type="number" id="plafon_pembiayaan" name="plafon_pembiayaan" placeholder="ketik disini" required max="100">
                </div>
                <div class="col-md-8">
                    % Plafon Pembiayaan
                </div>
            </div>
        </div>

    </div>
</div>


<!-- Hasil Scoring -->
<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Hasil Scoring
                        &nbsp</label>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Biro Kredit <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <input class="form-control" type="text" id="biro_kredit" name="biro_kredit" readonly>

            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Credolab <span class="float-right ml-1">:</span></div>
            <div class="col-md-6 pl-md-0  col-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Personal</label>
                        <input class="form-control" type="text" placeholder="score_personal" id="score_personal"
                            name="score_personal" readonly>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pendanaan</label>
                        <input class="form-control" type="text" placeholder="score_pendanaan" id="score_pendanaan"
                            name="score_pendanaan" readonly>
                    </div>
                </div>

            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-5 col-12">Very Jelas <span class="float-right ml-1">:</span></div>
            <div class="col-md-2 pl-md-0  col-2">
                <button type="button" data-toggle="modal" data-target="#verijelas_modal"
                    class="btn btn btn-block btn-success btn-sm" id="resultVeriJelas"> <i class="fa fa-folder"
                        aria-hidden="true"></i> Hasil Verijelas</button>

            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-5 col-12">Hasil Scoring Akhir <span class="float-right ml-1">:</span></div>
            <div class="col-md-3 pl-md-0  col-3">
                <div class="input-group mb-3">
                    <select class="form-control" id="hasil_scoring_akhir" name="hasil_scoring_akhir">
                        <option value="">Pilih</option>
                        <option value="1">Low</option>
                        <option value="2">Medium</option>
                        <option value="3">High</option>
                    </select>
                    &nbsp;
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Risk</span>
                    </div>
                </div>



            </div>
        </div>


        <!-- end hasil scorring -->

    </div>
</div>

<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Usulan/Catatan Analis Kredit
                        &nbsp
                        &nbsp</label>
                </div>
            </div>
        </div>


        <div class="row mt-2">
            <div class="col-md-12 pl-md-0  col-12">
                <textarea class="form-control" name="catatan_analis_kredit" id="catatan_analis_kredit" cols="30"
                    rows="5" readonly></textarea>


            </div>
        </div>
    </div>
</div>

@include('layouts.modals.modal-verijelas')

@push('naup-scripts')
    <script type="text/javascript" src="{{ url('assetsBorrower/js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var pengajuan_id = "{{ request()->get('pengajuan_id') }}";
        var brw_id;
        var brw_nama;

        $.ajax({
            url: '/admin/adr/detailnaup/' + pengajuan_id,
            method: "get",
            success: function(data, value) {

                let persentase_uang_muka = (data.pengajuan.uang_muka / data.pengajuan.pendanaan_dana_dibutuhkan) * 100;
                var jenis_objek_pendanaan = '-';

                var nilai_ujroh_imbt = parseInt(data.pengajuan.estimasi_imbal_hasil) * parseInt(data.pengajuan.plafond_rekomendasi) / 100;
                let margin = (parseInt(data.pengajuan.estimasi_imbal_hasil) * parseInt(data.pengajuan.plafond_rekomendasi)) / 100;
                let total_kewajiban_penerima = (parseInt(margin) + parseInt(data.pengajuan.plafond_rekomendasi));
 
                let valueTujuanPenggunaanWakalah = 'Pembelian Rumah dari ' + data.pengajuan.nm_pemilik + ' di (ketik disini)'
                let valueTujuanPenggunaanQardh ='Take Over Pendanaan yang sebelumnya digunakan untuk pembelian rumah dari ' + data.pengajuan.nm_pemilik
                let valueTujuanPenggunaanMurabahah ='Pembelian aset berupa … (…) unit rumah tinggal/tanah kavling*) tipe … / … m2 dari ' + data.pengajuan.nm_pemilik + ' di ...'
                let valueTujuanMMQ = 'Pembelian Aset Berupa Rumah Tinggal Tipe 70/150 Dengan Alamat ' + data.pengajuan.alamat_sertifikat

                brw_id = data.brw_id;
                brw_nama = data.brw_nama;

                $("#angsuran_pendanaan_murabahah, #angsuran_pendanaan, #angsuran_pendanaan_imbt").html(data.simulasi_angsuran);
                $("#nilai_sewa_imbt, #nilai_sewa_ijarah, #proyeksi_pendanaan_mmq").html(data.simulasi_margin);
                $("#proyeksi_total_pendanaan_mmq").html(data.simulasi_total_margin);


                if (data.pengajuan.no_naup) $('#nomor_naup').val(data.pengajuan.no_naup);
                if (data.pengajuan.tujuan_penggunaan_wakalah) {
                    $('#tujuan_penggunaan_wakalah').val(data.pengajuan.tujuan_penggunaan_wakalah);
                } else {
                    $('#tujuan_penggunaan_wakalah').val(valueTujuanPenggunaanWakalah);
                }

                if (data.pengajuan.tujuan_penggunaan_qardh) {
                    $('#tujuan_penggunaan_qardh').val(data.pengajuan.tujuan_penggunaan_qardh);
                } else {
                    $('#tujuan_penggunaan_qardh').val(valueTujuanPenggunaanQardh);
                }

                if (data.pengajuan.tujuan_penggunaan_murabahah) {
                    $('#tujuan_penggunaan_murabahah').val(data.pengajuan.tujuan_penggunaan_murabahah);
                } else {
                    $('#tujuan_penggunaan_murabahah').val(valueTujuanPenggunaanMurabahah);
                }


                if (data.pengajuan.tujuan_penggunaan_mmq) {
                    $('#tujuan_penggunaan_mmq').val(data.pengajuan.tujuan_penggunaan_mmq);
                } else {
                    $('#tujuan_penggunaan_mmq').val(valueTujuanMMQ);
                }

                $('#tanggal_permohonan_penerima_pendanaan').val(moment(data.pengajuan.created_at).format('DD/MM/YYYY'));
                $("#fasilitas_property_ke").val(data.pengajuan.fasilitas_ke);
                $("#maksimum_FTV").val(data.pengajuan.maks_ftv_persentase);
                $("#jumlah_permohonan_penerima_pendanaan").val(formatRupiah(parseFloat(data.pengajuan.pendanaan_dana_dibutuhkan).toString()));
                $("#jumlah_plafond_diusulkan, #plafond_rekomendasi_mmq").val(formatRupiah(parseFloat(data.pengajuan.plafond_rekomendasi).toString()));
                $("#uang_muka_deposit").val(formatRupiah(parseFloat(data.pengajuan.uang_muka).toString()));
                $("#presentase_uang_muka").val(parseInt(persentase_uang_muka));

                $('#nama_produk, #nama_produk_qardh, #nama_produk_murabahah, #nama_produk_imbt, #nama_produk_mmq').val(data.pengajuan.pendanaan_nama);
                $('#pembelian_dari').val(data.pengajuan.nm_pemilik);
                $('#sumber_pelunasan, #sumber_pelunasan_qardh').val('Pencairan Fasilitas Pendanaan ' + data.pengajuan.pendanaan_nama);

                getPengikatPendanaan(data.pengajuan.pengikatan_pendanaan_wakalah, 'pengikatan_pendanaan_wakalah');
                getPengikatJaminan(data.pengajuan.pengikatan_jaminan_wakalah, 'pengikatan_jaminan_wakalah');

                getPengikatPendanaan(data.pengajuan.pengikatan_pendanaan_qardh, 'pengikatan_pendanaan_qardh');
                getPengikatJaminan(data.pengajuan.pengikatan_jaminan_qardh, 'pengikatan_jaminan_qardh');

                getPengikatJaminan(data.pengajuan.pengikatan_jaminan_murabahah, 'pengikatan_jaminan_murabahah');
                getPengikatPendanaan(data.pengajuan.pengikatan_pendanaan_murabahah, 'pengikatan_pendanaan_murabahah');

                getPengikatJaminan(data.pengajuan.pengikatan_jaminan_imbt, 'pengikatan_jaminan_imbt');
                getPengikatPendanaan(data.pengajuan.pengikatan_pendanaan_imbt, 'pengikatan_pendanaan_imbt');

                getPengikatJaminan(data.pengajuan.jenis_pengikatan_agunan, 'jenis_pengikatan_agunan');

                $("#jangka_waktu_imbt, #jangka_waktu_murabahah, #jangka_waktu_pendanaan_mmq, #jangka_waktu_sewa_ijarah").val(data.pengajuan.durasi_proyek);
                $("#nilai_pasar_wajar").val(formatRupiah(parseFloat(data.pengajuan.nilai_pasar_wajar).toString()));
                $("#nilai_likuidasi").val(formatRupiah(parseFloat(data.pengajuan.nilai_likuidasi).toString()));
                $("#score_personal").val(data.pengajuan.skor_personal_credolab);
                $("#score_pendanaan").val(data.pengajuan.skor_pendanaan_credolab);
                $("#biro_kredit").val(data.pengajuan.skor_pefindo);
                $("#hasil_scoring_akhir").val(data.pengajuan.hasil_scoring_akhir);

                if (data.pengajuan.jenis_objek_pendanaan == '1') {
                    jenis_objek_pendanaan = 'Tanah Kosong';
                } else if (data.pengajuan.jenis_objek_pendanaan == '2') {
                    jenis_objek_pendanaan = 'Tanah Dan Bangunan';
                }

                $("#jenis_objek_pendanaan").val(jenis_objek_pendanaan);
                $("#nilai_pengikatan_agunan").val(formatRupiah(parseFloat(data.pengajuan.nilai_pengikatan_agunan)
                    .toString()));
                $("#harga_beli_barang_murabahah").val(formatRupiah(parseFloat(data.pengajuan.harga_objek_pendanaan).toString()));


                // let persentase_nilai_pengikatan_agunan = (data.naup.nilai_pengikatan_agunan / data.hasil_analisa.plafond_rekomendasi) * 100;
                // $("#plafon_pembiayaan").val(parseFloat(persentase_nilai_pengikatan_agunan).toFixed(2));

                $("#plafon_pembiayaan").val(parseFloat(data.pengajuan.plafon_pembiayaan));

                $("#catatan_analis_kredit").val(data.usulan_analis_kredit);

                if (data.pengajuan.wakalah == '1') {
                    $(".div_wakalah").removeClass('d-none');
                    $("input[name=wakalah][value='1']").attr('checked', 'checked');
                    $('#jangka_waktu_wakalah').val(data.pengajuan.jangka_waktu_wakalah);
                    $("#jumlah_wakalah").val(formatRupiah(parseFloat(data.pengajuan.jumlah_wakalah).toString()));
                    $('#jangka_waktu_wakalah').val(data.pengajuan.jangka_waktu_wakalah);
                }

                if (data.pengajuan.qardh == '1') {
                    $(".div_qard").removeClass('d-none');
                    $("input[name=qard][value='1']").attr('checked', 'checked');
                    $('#take_over_dari_qardh').val(data.pengajuan.take_over_dari_qardh);
                    $('#jangka_waktu_qardh').val(data.pengajuan.jangka_waktu_qardh);
                    $("#jumlah_qardh").val(formatRupiah(parseFloat(data.pengajuan.jumlah_qardh).toString()));
                }


                $("#plafon_pendanaan_murabahah").val(formatRupiah(parseFloat(data.pengajuan.plafond_rekomendasi).toString()));
                $("#margin_murabahah").val(formatRupiah(parseFloat(margin).toString()));

                $("#total_kewajiban_penerima").val(formatRupiah(parseFloat(total_kewajiban_penerima).toString()));
                $("#pricing_pendanaan_murabahah").val(data.pengajuan.persentase_margin);

                if (data.pengajuan.murabahah == '1') {

                    $(".div_murabahah").removeClass('d-none');
                    $("input[name=murabahah][value='1']").attr('checked', 'checked');
                    $("#uang_muka_murabahah").val(formatRupiah(parseFloat(data.pengajuan.uang_muka_murabahah)
                        .toString()));
                    $("#denda_murabahah").val(formatRupiah(parseFloat(data.pengajuan.denda_murabahah).toString()));

                    let uang_muka_murabahah = parseInt($('#uang_muka_murabahah').val().replaceAll('.', ''));
                    let harga_beli_barang_murabahah = parseInt($('#harga_beli_barang_murabahah').val().replaceAll('.', ''));
                    let harga_jual_barang = parseInt(harga_beli_barang_murabahah - uang_muka_murabahah);

                    $("#harga_jual_barang_murabahah").val(formatRupiah(parseFloat(harga_jual_barang)
                    .toString()));
                }

                $("#porsi_pendanaan_imbt").val(formatRupiah(parseFloat(data.pengajuan.plafond_rekomendasi)
                    .toString()));
                $("#ekpektasi_nilai_sewa_imbt").val(data.pengajuan.persentase_margin);
                $("#harga_perolehan_aktiva_imbt").val(formatRupiah(parseFloat(data.pengajuan.plafond_rekomendasi).toString()));
                $("#nilai_ujroh_imbt").val(formatRupiah(parseInt(nilai_ujroh_imbt).toString()));
                $("#uang_muka_imbt").val(formatRupiah(parseInt(data.pengajuan.uang_muka_imbt).toString()));

                if (data.pengajuan.imbt == '1') {
                    $(".div_imbt").removeClass('d-none');
                    $("input[name=imbt][value='1']").attr('checked', 'checked');
                    $('#tujuan_penggunaan_imbt').val(data.pengajuan.tujuan_penggunaan_imbt);
                    $("#denda_imbt").val(formatRupiah(parseFloat(data.pengajuan.denda_imbt).toString()));
                    $("#penetapan_nilai_ujroh_selanjutnya").val(data.pengajuan.penetapan_nilai_ujroh_selanjutnya);
                }

                $("#harga_beli_aset_mmq").val(formatRupiah(parseFloat(data.pengajuan.harga_objek_pendanaan).toString()));
                $("#ekspektasi_nilai_sewa_mmq").val(data.pengajuan.persentase_margin);
                if (data.pengajuan.mmq == '1') {
                    $(".div_mmq").removeClass('d-none');
                    $("input[name=mmq][value='1']").attr('checked', 'checked');
                    // $('#tujuan_penggunaan_mmq').val(data.naup.tujuan_penggunaan_mmq);
                    $("#jangka_waktu_penyerahan_objek_mmq").val(data.pengajuan.jangka_waktu_penyerahan_objek_mmq);
                    $("#objek_bagi_hasil_mmq").val(data.pengajuan.objek_bagi_hasil_mmq);
                    $("#uang_muka_mmq").val(formatRupiah(parseFloat(data.pengajuan.uang_muka_mmq).toString()));

                    $("#nisbah_penerima_dana").val(data.pengajuan.nisbah_penerima_dana);
                    $("#nisbah_penyelenggara").val(data.pengajuan.nisbah_penyelenggara);
                    $("#denda_mmq").val(formatRupiah(parseFloat(data.pengajuan.denda_mmq).toString()));
                    $("#hishshah_per_unit").val(formatRupiah(parseFloat(data.pengajuan.hishshah_per_unit).toString()));

                    let uang_muka_mmq = parseInt($('#uang_muka_mmq').val().replaceAll('.', ''));
                    let plafond_rekomendasi = parseInt($('#plafond_rekomendasi_mmq').val().replaceAll('.', ''));
                    let total_modal_syirkah = parseInt(plafond_rekomendasi + uang_muka_mmq);
                    let hasil_hishah_penyelenggara = parseInt(plafond_rekomendasi / data.pengajuan.hishshah_per_unit);
                    let hasil_hishah_penerima = parseInt(uang_muka_mmq / data.pengajuan.hishshah_per_unit);

                    $("#hishshah_penyelenggara_mmq").val(hasil_hishah_penyelenggara);
                    $("#hishshah_penerima_pendanaan_mmq").val(hasil_hishah_penerima);
                    $('#total_hishshah_mmq').val(hasil_hishah_penyelenggara + hasil_hishah_penerima);
                    $("#modal_syirkah").val(formatRupiah(parseFloat(total_modal_syirkah).toString()));
                }

                if (data.pengajuan.ijarah == '1') {
                    $(".div_ijarah").removeClass('d-none');
                    $("input[name=ijarah][value='1']").attr('checked', 'checked');
                    $("#objek_sewa").val(data.pengajuan.objek_sewa);
                    $("#jangka_waktu_penyerahan_ijarah").val(data.pengajuan.jangka_waktu_penyerahan_ijarah);
                    $("#penyesuaian_nilai_sewa").val(data.pengajuan.penyesuaian_nilai_sewa);
                }


                $("#alamat_sertifikat").val(data.pengajuan.alamat_sertifikat);
                $("#jenis_agunan").val(data.pengajuan.jenis_agunan);
                $("#nomor_agunan").val(data.pengajuan.nomor_agunan);

                if(data.pengajuan.tanggal_jatuh_tempo && data.pengajuan.tanggal_jatuh_tempo != '0000-00-00') $("#tanggal_jatuh_tempo").val(moment(data.pengajuan.tanggal_jatuh_tempo).format('MM/DD/YYYY'));
              
                $("#atas_nama_agunan").val(data.pengajuan.atas_nama_agunan);
                $("#nomor_imb").val(data.pengajuan.no_imb);
                $("#luas_tanah").val(data.pengajuan.luas_tanah);
                $("#luas_bangunan").val(data.pengajuan.luas_bangunan);
                $("#imbt_atas_nama").val(data.atas_nama_imb);
                $("#appraisal").val(data.pengajuan.appraisal);
                $("#rekomendasi_appraisal").val(data.pengajuan.rekomendasi_appraisal);
                $("#tanggal_penilaian_agunan").val(data.pengajuan.tanggal_penilaian_agunan);
            }
        });

        const getPengikatJaminan = (data_pengikat_jaminan, element_id) => {
            $('#' + element_id).empty(); // set null
            $('#' + element_id).append($('<option>', {
                value: '',
                text: '-- Pilih Satu --'
            }));
            $.getJSON("/borrower/data_pengikat_jaminan", function(data) {
                if (data) {

                    $.each(data, function(key, value) {
                        var selected = (data_pengikat_jaminan == key) ? 'selected' : '';
                        $('#' + element_id).append('<option value="' + key + '" ' + selected + '>' +
                            value + '</option>');
                    });
                }
            })
        }


        const getPengikatPendanaan = (data_pengikat_pendanaan, element_id) => {
            $('#' + element_id).empty(); // set null
            $('#' + element_id).append($('<option>', {
                value: '',
                text: '-- Pilih Satu --'
            }));
            $.getJSON("/borrower/data_pengikat_pendanaan", function(data) {
                if (data) {

                    $.each(data, function(key, value) {
                        var selected = (data_pengikat_pendanaan == key) ? 'selected' : '';
                        $('#' + element_id).append('<option value="' + key + '" ' + selected + '>' +
                            value + '</option>');
                    });
                }
            })
        }



        $("document").ready(function() {


            $(document).on("keyup", "#uang_muka_murabahah", function() {
                let uang_muka_murabahah = parseInt($(this).val().replaceAll('.', ''));
                let harga_beli_barang_murabahah = parseInt($('#harga_beli_barang_murabahah').val()
                    .replaceAll('.', ''));
                let harga_jual_barang = parseInt(harga_beli_barang_murabahah - uang_muka_murabahah);

                $("#harga_jual_barang_murabahah").val(formatRupiah(parseFloat(harga_jual_barang)
                    .toString()));

            });

            $(document).on("keyup", "#uang_muka_mmq", function() {
                let uang_muka_mmq = parseInt($(this).val().replaceAll('.', ''));
                let hishshah_per_unit = parseInt($('#hishshah_per_unit').val().replaceAll('.', ''));
                let plafond_rekomendasi = parseInt($('#plafond_rekomendasi_mmq').val().replaceAll('.', ''));
                let total_modal_syirkah = parseInt(plafond_rekomendasi + uang_muka_mmq);

                if (uang_muka_mmq > 0 && hishshah_per_unit > 0) {
                    $("#hishshah_penerima_pendanaan_mmq").val(parseInt(uang_muka_mmq / hishshah_per_unit));
                }

                $("#modal_syirkah").val(formatRupiah(parseFloat(total_modal_syirkah).toString()));
                $('#total_hishshah_mmq').val(parseInt($("#hishshah_penyelenggara_mmq").val()) + parseInt($(
                    "#hishshah_penerima_pendanaan_mmq").val()));

            });

            $(document).on("keyup", "#plafon_pembiayaan", function() {
                //set nilai pengikat agunan
                //value plafon pembiayaan x plafon diusulkan
                if ($(this).val() > 0) {
                    let persen_plafon_pembiayaan = parseFloat($(this).val() / 100);
                    let jumlah_plafond_diusulkan = parseFloat($('#jumlah_plafond_diusulkan').val().replaceAll('.', ''));
                    if (jumlah_plafond_diusulkan > 0) $('#nilai_pengikatan_agunan').val(formatRupiah(parseFloat(persen_plafon_pembiayaan * jumlah_plafond_diusulkan).toString()));
                }else{
                    $('#nilai_pengikatan_agunan').val('');
                }
            });

            $(document).on("keyup", "#nilai_pengikatan_agunan", function() { 
                let nilai_pengikatan_agunan = parseFloat($(this).val().replaceAll('.', ''));
                let jumlah_plafond_diusulkan = parseFloat($('#jumlah_plafond_diusulkan').val().replaceAll('.', ''));
                if (jumlah_plafond_diusulkan > 0){
                    // nilai_pengikatan_agunan
                    $('#plafon_pembiayaan').val(parseFloat((nilai_pengikatan_agunan / jumlah_plafond_diusulkan) * 100).toFixed(1));
                }

            });

            $(document).on("keyup", "#hishshah_per_unit", function() {
                let plafond_rekomendasi_mmq = parseInt($('#plafond_rekomendasi_mmq').val().replaceAll('.',
                    ''));
                let hishshah_per_unit = parseInt($(this).val().replaceAll('.', ''));
                let uang_muka_mmq = parseInt($("#uang_muka_mmq").val().replaceAll('.', ''));
                let total_hishshah = 0;

                if (plafond_rekomendasi_mmq > 0 && hishshah_per_unit > 0) {
                    let hasil_hishah_penyelenggara = parseInt(plafond_rekomendasi_mmq / hishshah_per_unit);
                    $("#hishshah_penyelenggara_mmq").val(hasil_hishah_penyelenggara);
                    total_hishshah += hasil_hishah_penyelenggara;
                }

                if (uang_muka_mmq > 0 && hishshah_per_unit > 0) {
                    let hasil_hishah_penerima = parseInt(uang_muka_mmq / hishshah_per_unit)
                    $("#hishshah_penerima_pendanaan_mmq").val(hasil_hishah_penerima);
                    total_hishshah += hasil_hishah_penerima;
                }

                $('#total_hishshah_mmq').val(total_hishshah);
            });


            $(document).on("click", "#wakalah", function() {
                if ($('input[name="wakalah"]').is(':checked')) {
                    $('.div_wakalah').removeClass('d-none');
                } else {
                    $('.div_wakalah').addClass('d-none');
                }

            });

            $(document).on("click", "#qard", function() {
                if ($('input[name="qard"]').is(':checked')) {
                    $('.div_qard').removeClass('d-none');
                } else {
                    $('.div_qard').addClass('d-none');
                }

            });

            $(document).on("click", "#murabahah", function() {
                if ($('input[name="murabahah"]').is(':checked')) {
                    $('.div_murabahah').removeClass('d-none');
                } else {
                    $('.div_murabahah').addClass('d-none');
                }

            });

            $(document).on("click", "#imbt", function() {

                if ($('input[name="imbt"]').is(':checked')) {
                    $('.div_imbt').removeClass('d-none');
                } else {
                    $('.div_imbt').addClass('d-none');
                }

            });

            $(document).on("click", "#mmq", function() {

                if ($('input[name="mmq"]').is(':checked')) {
                    $('.div_mmq').removeClass('d-none');
                } else {
                    $('.div_mmq').addClass('d-none');
                }

            });

            $(document).on("click", "#ijarah", function() {

                if ($('input[name="ijarah"]').is(':checked')) {
                    $('.div_ijarah').removeClass('d-none');
                } else {
                    $('.div_ijarah').addClass('d-none');
                }

            });

            function veriJelasData() {

                $.ajax({
                    url: "{{ route('verify.data') }}",
                    type: "post",
                    dataType: "html",
                    data: {
                        _token: "{{ csrf_token() }}",
                        borrower_id: brw_id,
                        brw_type: 1,
                        vendor: 'verijelas'
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        if (xhr.status == '419') {
                            errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                        }

                        $('#loading_title').html('Data Failed');
                        $('#loading_content_data').html(errorMessage);
                        $('#loading_footer_data').removeClass('d-none');
                    },
                    success: function(result) {
                        $('#loading_title').html('Preparing Result ...');
                        $('#loading_content_data').html(result);
                        $('#loading_title').html('');
                        $('#loading_footer_data').removeClass('d-none');
                    }
                });


            }

            $('#verijelas_modal').on('show.bs.modal', function(event) {
                $('.modal-title').html('{{ trans('verify.hasil.title') }}' + ' - ' + brw_nama);
                veriJelasData();
            });

        });
    </script>

@endpush
