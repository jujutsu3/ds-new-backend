<div class="card">
    <div class="card-body">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h3" for="form_biaya_biaya">Biaya - biaya &nbsp</label>
            </div>
            <hr>
        </div>
        <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="wizard-progress2-nomor_naup">Nomor NAUP</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input class="form-control" type="text" id="nomor_naup" name="nomor_naup" readonly value="{{ $data_naup['no_naup'] }}">
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="wizard-progress2-skema_pengajuan">Skema Pengajuan</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input class="form-control" type="text" id="skema_pengajuan" name="skema_pengajuan" value="{{ ($data->skema_pembiayaan == '1') ? 'Single Income': 'Joint Income' }}" readonly>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-2">
                <div class="form-group">
                    <label for="wizard-progress2-nama_penerima_pendanaan">Nama Penerima Pendanaan</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input class="form-control" type="text" id="nama_penerima_pendanaan"
                        name="nama_penerima_pendanaan" value="{{ $data->nama }}" readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="wizard-progress2-nama_pasangan">Nama Pasangan</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input class="form-control" type="text" id="nama_pasangan" name="nama_pasangan" value="{{ $data->nama_pasangan }}" readonly>
                </div>
            </div>
        </div>
        <div class="col-12">
            <br>
            <div class="col-md-8">
                <div class="form-group">
                    <label for="wizard-progress2-judul_biaya">Biaya - biaya yang timbul untuk Pengajuan Pendanaan ini
                        adalah sebagai berikut : &nbsp</label>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="col-md-12">
                <table id="biayaTableData" class="table table-striped table-bordered table-responsive-sm">
                    <thead>
                        <tr>
                            <th class="col-1">No</th>
                            <th class="col-3">Rincian Biaya</th>
                            <th class="col-3">Keterangan</th>
                            <th class="col-2">Jumlah</th>
                            <th class="col-3">Nama Rekanan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_biaya as $val)
                            <tr id="row_{{ $loop->iteration }}" class="biaya-row"
                                data-biaya-index="{{ $loop->iteration }}">
                                <td id="in_number_row">{{ $val['no'] }}</td>
                                <td>{{ $val['rincian_biaya'] }}</td>
                                <td>
                                    <input type="hidden" name="biaya[{{ $val['no'] }}][rincian_biaya]"
                                        value="{{ $val['rincian_biaya'] }}" />
                                    <input type="hidden" name="biaya[{{ $val['no'] }}][value_id]" id="value_id" value="{{ $val['value_id'] }}" />
                                    <input class="form-control" type="text" id="keterangan" name="biaya[{{ $val['no'] }}][keterangan]" value="{{ $val['value_keterangan'] }}" placeholder="ketik disini">
                                </td>
                                <td>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control txt" placeholder="ketik disini" type="text" id="jumlah" name="biaya[{{ $val['no'] }}][jumlah]" value="{{ $val['value_jumlah'] }}" onkeyup="this.value = formatRupiah(this.value);">

                                    </div>

                                </td>
                                @if ($val['show_nama_rekanan'] == '1')
                                    <td>
                                        <select class="form-control select_rekanan" id="list_rekanan_{{ $val['no'] }}"
                                            name="biaya[{{ $val['no'] }}][nama_rekanan]">
                                          <option value="">-- Pilih --</option>
                                          @foreach($val['list_rekanan'] as $key => $val2)
                                                <option value="{{$key}}"  {{ ($val['value_rekanan'] == $key  ? 'selected': '')  }}>{{ $val2 }}</option>
                                           @endforeach
                                        </select>
                                    </td>
                                @else

                                    <td><input type="hidden" name="biaya[{{ $val['no'] }}][nama_rekanan]"
                                            id="nama_rekanan" value="" />
                                        @if ($val['no'] == '8')
                                            <button type="button" id="btn-add-non-rumah-lain" class="btn btn-primary"
                                                onclick="addSubBiaya()"> <i class="fa fa-plus"></i></button>
                                        @endif

                                    </td>
                                @endif
                            </tr>
                        @endforeach

                        @foreach ($data_sub_biaya as $val)
                            <tr id="row_{{ $val['no'] }}" class="biaya-row"
                                data-biaya-index="{{ $val['no'] }}">
                                <td id="in_number_row">{{ $val['no'] }}</td>
                                <td><input class="form-control" type="text" id="rincian_biaya"
                                        name="biaya[{{ $val['no'] }}][rincian_biaya]"
                                        value="{{ $val['rincian_biaya'] }}"></td>
                                <td><input class="form-control" type="text" id="keterangan"
                                        name="biaya[{{ $val['no'] }}][keterangan]"
                                        value="{{ $val['value_keterangan'] }}"></td>
                                <td><input class="form-control txt" type="text" id="jumlah"
                                        name="biaya[{{ $val['no'] }}][jumlah]"
                                        value="{{ $val['value_jumlah'] }}" onkeyup="this.value = formatRupiah(this.value);"></td>
                                <td><input type="hidden" name="biaya[{{ $val['no'] }}][nama_rekanan]"
                                        id="nama_rekanan" value="" /><input type="hidden"
                                        name="biaya[{{ $val['no'] }}][value_id]" id="value_id"
                                        value="{{ $val['value_id'] }}" /> <button type="button"
                                        id="{{ $val['no'] }}" class="btn btn-danger btn_remove_biaya"> <i
                                            class="fa fa-trash"></i></button></td>
                            </tr>

                        @endforeach


                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                                <h6 style="margin-top:10px;">Total Biaya-biaya</h6>
                            </td>
                            <td>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                                    </div>
                                    <input class="form-control" type="text" id="total_biaya" name="total_biaya" readonly>


                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="col-md-12">
                <br>
                <div align="justify">
                    Biaya-biaya yang timbul dalam pengajuan Pendanaan ini, harap disetorkan/ditransfer ke Rekening DSI,
                    apabila Pendanaan tersebut tidak di terima oleh keputusan komite DSI, maka biaya-biaya tersebut akan
                    di kembalikan seluruhnya kecuali biaya appraisal. Dan apabila Penerima Pendanaan yang mengundurkan
                    diri setelah persetujuan Pendanaan, maka biaya tersebut tidak dapat dikembalikan. Syarat dan
                    Ketentuan Berlaku.
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-8">
        </div>

    </div>


</div>


@push('biaya-scripts')

<script src="{{ url('assetsBorrower/js/plugins/select2/js/select2.full.min.js') }}"></script> 
    <script type="text/javascript">
        var count_number_sub_biaya = "{{ $row_number_sub_biaya }}";
        let row_number = (count_number_sub_biaya > 0) ? parseInt(count_number_sub_biaya - 1) : 8;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var pengajuan_id = "{{ request()->get('pengajuan_id') }}";

        $("#list_rekanan_2, #list_rekanan_3").select2({
            placeholder: "-- Pilih --",
            allowClear: true,
            width: '100%'
        });

        $("document").ready(function() {
            calculateSum();
            $(document).on("keyup", ".txt", function() {
                calculateSum();

            });
            
        });

        const addSubBiaya = () => {
            row_number += 1
            $('#biayaTableData').find('tbody').append(`
                 <tr id="row_${row_number}">
                        <td name="text_number[]">${row_number}</td>
                        <td><input class="form-control" type="text" id="rincian_biaya" name="biaya[${row_number}][rincian_biaya]" placeholder="ketik disini"></td>
                        <td>
                            <input class="form-control" type="text" id="keterangan" name="biaya[${row_number}][keterangan]" placeholder="ketik disini">
                        </td>
                        <td>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                                    </div>
                            <input class="form-control txt" type="text" id="jumlah" placeholder="ketik disini" name="biaya[${row_number}][jumlah]" onkeyup="this.value = formatRupiah(this.value);">

                            </div>
                        </td>
                        <td><input type="hidden" name="biaya[${row_number}][nama_rekanan]" id="nama_rekanan" value="" /> <button type="button" id="${row_number}" class="btn btn-danger btn_remove_biaya"> <i class="fa fa-trash"></i></button></td>
                  </tr>

        `)


        }

        $(document).on('click', '.btn_remove_biaya', function() {

            $(this).closest('tr').remove();
            $('tbody tr').each(function(i) {
                i = i + 1;
                $(this).find('td').eq(0).text(i);
                $(this).attr("data-biaya-index", i);
                $(this).attr("id", "budget-row-" + i);
            });

            row_number--;

            calculateSum();
        });


        const calculateSum = () => {
            var sum = 0;
            $(".txt").each(function() {
 
                if (!isNaN(this.value) && this.value.length != 0) {
                    sum += parseFloat(this.value.replace(/[^0-9]/g, ''));
                }else{

                    if(this.value != ''){
                        var a = parseFloat(this.value.replace(/[^0-9]/g, ''));
                        sum += parseFloat(a);
                    }
                  
                } 

            });

            var sumFormat = formatRupiah(sum.toString());
            $("#total_biaya").val(sumFormat);
        }


        function showDetailRekanan(title, rekanan_id, table_rekanan){
            $.ajax({
                    url: "{{ route('rekanan.detail') }}",
                    type: "post",
                    dataType: "html",
                    data: {
                        id: rekanan_id,
                        table_rekanan: table_rekanan
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        if (xhr.status == '419') {
                            errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                        }

    
                    },
                    success: function(result) {
                        Swal.fire({
                            title: title,
                            icon: 'info',
                            html: result,
                            width: '1200px',
                            showCloseButton: true,
                            focusConfirm: false,
                            confirmButtonText:
                                '<i class="fa fa-info"></i> Ok!',
                        
                            })
                    }
                });
           
        }

         
        $('#list_rekanan_2').change(function() {
            if($(this).val() !== null){
                showDetailRekanan('Informasi Detail KJPP', $(this).val(), 'm_kjpp');
            }
           
        });

        $('#list_rekanan_3').change(function() {
            if($(this).val() !== null){
               showDetailRekanan('Informasi Detail Notaris PPAT', $(this).val(), 'm_notarisppat');
            }
        });
        
     
    </script>
@endpush
