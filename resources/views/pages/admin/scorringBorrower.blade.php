@extends('layouts.admin.master')

@section('title', 'Dashboard Borrower')

@section('content')
<br>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
			<div class="page-title">
				<h1>Penilaian Pengajuan Pendanaan</h1>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
<div class="row">
  <div class="col-md-12">
    @if (session('error'))
        <div class="alert alert-danger col-sm-12">
            {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @elseif (session('success'))
        <div class="alert alert-success col-sm-12">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @elseif(session('updated'))
        <div class="alert alert-success col-sm-12">
            {{ session('updated') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

      <div class="card">
          <div class="card-header">
              <strong class="card-title">Daftar Pengajuan Pendanaan</strong>
              <button class="btn btn-success float-right" id="draft_penilaian" data-toggle="modal" data-target="#modalDokumen" style="margin-left: 15px;">Dokumen Penilaian</button>
              {{-- <button type="button" class="btn btn-success float-right" data-toggle="modal" id="addNewJenis" data-target=".modelAddJenisPendanaan">Tambah Jenis Pendanaan</button> --}}
          </div>
          <div class="card-body">
              <table id="tablePendanaan" name="uhuy" class="table table-striped table-bordered table-responsive-sm">
                  <thead>
                  <tr>
                      <th>No</th>
                      <th>brw_id</th>
                      <th>idPengajuan</th>
                      <th>Pendanaan</th>
                      <th>Penerima Pendanaan</th>
                      <th>Tgl Pengajuan</th>
                      <th>KTP</th>
                      <th>Skor Pefindo</th>
                      <th>Skor Internal DSI</th>
                      <th>Skor Total</th>
                      <th>Keterangan Persetujuan</th>
                      <th>Status</th>
                      <th>Aksi</th>
                      <th>Tipe Pendanaan</th>
                      <th>ID Proyek</th>
                  </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- Large modal -->
<div class="modal fade modelAddJenisPendanaan" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4>Tambah Jenis Pendanaan</h4>
      </div>
        <div class="modal-body">
            <form action="/admin/borrower/prosess/postNewJenis" method="POST">
              @csrf
              <div class="form-group">
                <label>Jenis Pendanaan</label>
                <input type="text" name="pendanaanJenis" class="form-control" id="addJenisNama" aria-describedby="" placeholder="Jenis Pendanaan">
              </div>
              <div class="form-group">
                <textarea name="pendanaanKeterangan" id="textJenisPendana"></textarea>
              </div>
        </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary float-right  ">Kirim</button>
        </form>        
      </div>
    </div>
  </div>
</div>

{{-- Modal dokumen --}}
  <div class="modal fade" id="modalDokumen" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
            <h5>Dokumen Penilaian</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
        </div>
        <div class="modal-body" id="modalBodyDokumen">
            <embed src="{{ url('kategori_resiko_borrower') }}#toolbar=0" style="width:100%; height:500px;"frameborder="0">
        </div>
        {{-- <div class="modal-footer">
          
        </div> --}}
      </div>
    </div>
  </div>
 {{-- Modal dokumen End --}}

<!-- start of modal detil -->

<div class="modal fade" id="ubah_data" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="scrollmodalLabel">Detail Pendanaan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
          <form id="form_setuju_pendanaan" action="{{route('admin.borrower.client.update')}}" method="POST"  enctype="multipart/form-data">
              @csrf
              <input type="hidden" id="brw_id_edit" name="brw_id_edit" >
              <input type="hidden" id="pengajuan_id_edit" name="pengajuan_id_edit" >
              <input type="hidden" id="total_sisa_pinjaman" name="total_sisa_pinjaman" >
              <input type="hidden" id="brw_type_edit" name="brw_type_edit" >
              <input type="hidden" id="status_setuju" name="status_setuju" >
              <input type="hidden" id="id_image_detil1" name="id_image_detil1" >
              <input type="hidden" id="id_image_detil2" name="id_image_detil2" >
              
              <div class="col-lg-12 mt-2">
                <div class="row">
                    <div class="col-sm-9">
                      <label style="font-size: larger"><b>Data Pendanaan</b></label>
                    </div>
                    <div class="col-sm-3">
                      <div class="row float-right">
                        <div class="mx-2"><a target="_blank" id="file_sp3" style="color: #ffffff"><button type="button" class="btn btn-success">SP3</button></a></div>
                        <div class="mx-2"><a target="_blank" id="file_akad" style="color: #ffffff"><button type="button" class="btn btn-success">Akad Murabahah</button></a></div>
                      </div>
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                    <div class="input-group">
                          <div class="input-group-prepend">
                              <div class="input-group-text">Nama Pendanaan</i></div>
                          </div>
                        <input type="text" name="pendanaan_nama_edit" id="pendanaan_nama_edit" placeholder="Nama Pendanaan" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                    <div class="input-group">
                          <div class="input-group-prepend">
                              <div class="input-group-text">Alamat Proyek</i></div>
                          </div>
                        <input type="text" name="alamat_edit" id="alamat_edit" placeholder="Alamat Proyek" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                    <div class="input-group">
                          <div class="input-group-prepend">
                              <div class="input-group-text">Geocode Proyek</i></div>
                          </div>
                        <input type="text" name="geocode_edit" id="geocode_edit" placeholder="Geocode Proyek" class="form-control" required>
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">Tipe Pendanaan</i></div>
                        </div>
                            <select id='tipe_pendanaan_edit' name='tipe_pendanaan_edit' class="form-control" required>
                              <option value="" class="form-control">-- Tipe Pendanaan --</option>
                          </select>
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                  <div class="input-group">
                      <div class="input-group-prepend">
                          <div class="input-group-text">Akad</i></div>
                      </div>
                          <select name="akad" class="form-control" id="akad_edit" required>
                              <option value="" class="form-control"> -- Akad -- </option>
                              <option value="1" class="form-control">Murabahah</option>
                              <option value="2" class="form-control">Mudharabah</option>
                              <option value="3" class="form-control">MMQ</option>
                              <option value="4" class="form-control">IMBT</option>
                          </select>
                  </div>
                </div>
                <div class="col-lg-6 p-2">
                      <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Dana Dibutuhkan RP</i></div>
                            </div>
                        <input type="number" name="pendanaan_dana_dibutuhkan_edit" id="pendanaan_dana_dibutuhkan_edit" onkeyup="createSimulasi()" onchange="validasi_dana_dibutuhkan(this)"  placeholder="Dana dibutuhkan" class="form-control" min="0" max="2000000000" oninput="this.value = !!this.value && Math.abs(this.value) >= 0 ? Math.abs(this.value) : null" required>
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                    <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Estimasi Imbal Hasil %</i></div>
                            </div>
                        <input type="number" name="estimasi_imbal_hasil_edit" id="estimasi_imbal_hasil_edit" onkeyup="createSimulasi()" onchange="validasi_imbal_hasil(this)" placeholder="Imbal Hasil" min="0" oninput="this.value = !!this.value && Math.abs(this.value) >= 0 ? Math.abs(this.value) : null" required class="form-control">
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                    <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">Durasi Proyek(Bulan)</i></div>
                            </div>
                        <input type="number" name="durasi_proyek_edit" id="durasi_proyek_edit" onkeyup="createSimulasi()" onchange="validasi_durasi_proyek(this)"  class="form-control" placeholder="Durasi Proyek(Bulan)" min="0" oninput="this.value = !!this.value && Math.abs(this.value) >= 0 ? Math.abs(this.value) : null" required>
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                    <div class="form-group p-1 m-0 col-lg-12 float-left">
                        <label for="tgl_mulai_penggalangan" class=" form-control-label">Estimasi Tanggal Mulai Proyek</label>
                        <input type="date" name="estimasi_mulai_edit" id="estimasi_mulai_edit" min="{{ now()->toDateString('d-m-Y') }}" onchange="createSimulasi()" class="form-control" >
                    </div>
                </div>
                <div class="col-lg-6 p-2">
                  <div class="form-group p-1 m-0 col-lg-12 float-left">
                      <label class=" form-control-label">Deskripsi Proyek</label>
                      <textarea id='textarea_deskripsi' name="textarea_deskripsi"></textarea>
                  </div>
                </div>
              </div>
              
              <hr style="height:2px;border-width:0;color:black;background-color:black;width:100%; display:inline-block">
              <div class="col-lg-12 mt-2" id="Divsimulasi">
                  <div class="row">
                      <div class="col-sm-6">
                        <label style="font-size: larger"><b>Simulasi Cicilan</b></label>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <table class="table compact table-striped table-bordered table-hover table-sm" id="list_simulasi">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Tgl Jatuh Tempo</th>
                                      <th>Periode</th>
                                      <th>Tagihan Pokok + Imbal Hasil</th>
                                      <th>Sisa Tagihan</th>
                                  </tr>
                              </thead>
                          </table>
                      </div>
                  </div>
                  <div class="col-lg-12 mt-2">
                    <div class="float-right">
                        <label class=" form-control-label">Total Bayar : </label>
                        <label class=" form-control-label" id="simulasi_total_bayar"></label>
                    </div>
                  </div>
              </div>
              <br>
              <hr style="height:2px;border-width:0;color:black;background-color:black;width:100%; display:inline-block">
              <div class="col-lg-12 mt-2">
                  <div class="row">
                      <div class="col-sm-6">
                        <label style="font-size: larger"><b>Gambar Proyek</b></label>
                      </div>
                  </div>
                  <br>
                  <div class="row">
                      <div class="col-sm-4">
                        <img id='id_image' height="300px" width="400px" >
                        <label style="margin-top: 5px" for="gambar">Gambar Utama(.png/.jpeg/.jpg)</label>
                        <input type="file" class="form-control" accept="image/*" name="file" id="file" onchange="readURL(this, 'id_image');" placeholder="Edit Foto Proyek">
                      </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-sm-4">
                      <img id='image_detil1' height="300px" width="400px" >
                      <label style="margin-top: 5px" for="gambar">Gambar Detail(.png/.jpeg/.jpg)</label>
                      <input type="file" class="form-control" accept="image/*" name="file_detil1" id="file_detil1" onchange="readURL(this, 'image_detil1');" placeholder="Edit Foto Proyek">
                    </div>
                    <div class="col-sm-4">
                      <img id='image_detil2' height="300px" width="400px" >
                      <label style="margin-top: 5px" for="gambar">Gambar Detail(.png/.jpeg/.jpg)</label>
                      <input type="file" class="form-control" accept="image/*" name="file_detil2" id="file_detil2" onchange="readURL(this, 'image_detil2');" placeholder="Edit Foto Proyek">
                    </div>
                  </div>
              </div>

              <hr style="height:2px;border-width:0;color:black;background-color:black;width:100%; display:inline-block">
              <div class="col-lg-12 mt-2" id="jaminan">
                  <div class="row">
                      <div class="col-sm-6">
                        <label style="font-size: larger"><b>Jaminan Pendanaan</b></label>
                      </div>
                      <div class="col-sm-6">
                        <button class="btn btn-success float-right" type="button"  data-toggle="modal" data-target="#modal_jaminan" id="btnTambahJaminan" onclick="clearTambahJaminan()">Tambah Jaminan</button>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-12">
                          <br>
                          <table class="table compact table-striped table-bordered table-hover" id="table_list_jaminan">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Jaminan ID</th>
                                      <th>Pengajuan ID</th>
                                      <th>Nama Pemilik Jaminan</th>
                                      <th>Nomor Sertifikat</th>
                                      <th>Jenis Jaminan</th>
                                      <th>Nilai Objek Pajak</th>
                                      <th>Detail Jaminan</th>
                                      <th>Jaminan Jenis</th>
                                      <th>Kantor Penerbit</th>
                                      <th>NOP/Nomor Surat Ukur</th>
                                      <th>Sertifikat</th>
                                      <th>Aksi</th>
                                  </tr>
                              </thead>
                          </table>

                      </div>
                  </div>
              </div>

              <hr style="height:2px;border-width:0;color:black;background-color:black;width:100%; display:inline-block">

              <div class="col-lg-12 mt-2" id="dokumen_pendukung">
                  <div class="row">
                      <div class="col-sm-6">
                        <label style="font-size: larger"><b>Dokumen Pendukung</b></label>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row" id="checklist_persyaratan">
                          
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <label style= "font-size:12px">#Catatan : <span style="color:red">*</span> Wajib Punya </label>
                  </div>
              </div>

              <hr style="height:2px;border-width:0;color:black;background-color:black;width:100%; display:inline-block">

              <div class="col-lg-12 mt-2" id="penilaian">
                <div class="row">
                    <div class="col-sm-6">
                      <label style="font-size: larger"><b>Penilaian</b></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label>Penilaian Pefindo</label>
                        <div class="row-md-12">
                          <input type="number" class="col-md-8 form-control" id="penilaian_personal" name="penilaian_personal" placeholder="Nilai Personal" value='0'  required>
                          <br>
                          <button class="btn btn-success" id="btnScore" disabled>Nilai Pefindo</button>&nbsp;&nbsp;
                        </div>
                    </div>
                    <div class="col-md-4">
                      <label>Penilaian Dana Syariah</label>
                      <input type="number" class="col-sm-8 form-control" id="penilaian_pendanaan" name="penilaian_pendanaan" placeholder="Nilai Pendanaan" value='0' required>
                    </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-6">
                    <label>Detail Penilaian</label>
                      <textarea class="form-control" rows="3" id="keterangan_penilaian" name="keterangan_penilaian"></textarea>
                  </div>
                </div>
            </div>

              <style>
              .nav-pills .nav-link.active, .nav-pills .show > .nav-link{
                  background-color: green;
                  color: white;
                  border-radius:5px;
              }
              table, th, td { 
                border: 1px solid black; 
                border-collapse: collapse; 
                text-align:center; 
              } 
              /* setting the text-align property to center*/ 
              td { 
                padding: 5px; 
                text-align:center; 
              } 
              </style>
              
            </div>
                  <div class="modal-footer">
                      <button id="button_setuju_save" type="button" class="btn btn-success">Kirim</button>
                      <button type="submit" id="save_persetujuan_data" class="invisible"></button>
                  </div>
              </div>
          </form>
  </div>
</div>
<!-- end of modal detil -->

<!-- start modal ubah jaminan-->
<div class="modal fade" id="modal_jaminan" name="modal_jaminan" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="scrollmodalLabel">Detail Jaminan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          
          <form method="POST" id="form_jaminan" enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
              <div class="col-lg-12">
                  <div class="card-body card-block">
                      <div class="form-row">
                        <div class="col-sm-9 col-lg-10 ml-3 ml-sm-0">
                          <input type="hidden" id="id_jaminan" name="id_jaminan">
                          <input type="hidden" id="id_pengajuan" name="id_pengajuan">
                          <input type="hidden" id="status_form_jaminan" name="status_form_jaminan">
                          <div class="container">
                            <div class="row">
                              <div class="col-lg-4">
                                <label class="font-weight-bold">Nama Pemilik Jaminan</label>
                                <input type="text" class="col-sm-12 form-control" id="nama_jaminan" name="nama_jaminan" placeholder="Nama Pemilik Jaminan" required>
                              </div>
                              <div class="col-lg-4">
                                <label class="font-weight-bold">Jenis Jaminan</label>
                                <select class="form-control" id="jenis_jaminan_edit" name="jenis_jaminan_edit" required>
                                    <option value="" class="form-control"> -- Jenis Jaminan -- </option>
                                </select>
                              </div>
                              <div class="col-lg-4">
                                <label class="font-weight-bold">Nomor Sertifikat</label>
                                <input type="text" class="col-sm-12 form-control" id="nomor_jaminan" name="nomor_jaminan" placeholder="Nomor Sertifikat" required>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                              <div class="col-lg-4">
                                <label class="font-weight-bold">Kantor Penerbit</label>
                                  <select class="form-control" id="kantor_penerbit_jaminan" name="kantor_penerbit_jaminan" required>
                                      <option value="" class="form-control"> -- Kantor Penerbit -- </option>
                                  </select>
                              </div>
                              <div class="col-lg-4">
                                <label class="font-weight-bold">NOP/Nomor Surat Ukur</label>
                                <input type="number" class="col-sm-12 form-control" id="nop_jaminan" name="nop_jaminan" placeholder="NOP/Nomor Surat Ukur" required>
                              </div>
                              <div class="col-lg-4">
                                <label class="font-weight-bold">Nilai Objek Pajak</label>
                                <input type="number" class="col-sm-12 form-control" id="nilai_jaminan" name="nilai_jaminan" placeholder="Nilai Objek Pajak" min="0" oninput="this.value = !!this.value && Math.abs(this.value) >= 0 ? Math.abs(this.value) : null" required>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                              <div class="col">
                                <label class="font-weight-bold">Detail Jaminan</label>
                                <textarea class="form-control" rows="3" id="detil_jaminan" name="detil_jaminan" required></textarea>
                              </div>
                            </div>
                            <br>
                            <div class="row">
                              <div class="col">
                                <div class="my-2"><a target="_blank" id="file_exist_jaminan" style="color: #ffffff"><button type="button" class="btn btn-success">Lihat Sertifikat</button></a></div>
                                <label class="font-weight-bold">Sertifikat Objek Pajak(3 halaman pertama dengan format pdf)</label>
                                <input class="form-control" type="file" accept="application/pdf" id="file_jaminan" name="file_jaminan" required>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
          <div class="modal-footer">
              <button type="button" id="tutup_jaminan" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
              <button type="button" id="simpan_jaminan" class="btn btn-primary">Simpan</button>
          </div>
          </form>
      </div>
  </div>
</div>
<!-- end modal ubah jaminan-->

<!-- start modal upload dokumen scoring personal-->
<div class="modal fade" id="myUploadScoringPersonal" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="scrollmodalLabel">Unggah Dokumen Penilaian Personal</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          
          <form action="{{route('admin.uploadDokumenScoringPersonal')}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
              <div class="col-lg-12">
                  <div class="card-body card-block">
                      <div class="form-row">
                        <div class="col-sm-9 col-lg-10 ml-3 ml-sm-0">
                          <input type="hidden" id="brw_id2" name="brw_id">
                          <input type="hidden" id="brw_type2" name="brw_type">
                          <input type="hidden" id="pendanaan_id2" name="pendanaan_id">
                          <input type="hidden" id="identitas2" name="ktp">
                          <div class="row" id="tambahUpload">
                            <label class="font-weight-bold">Nama Dokumen</label>
                            <input type="text" class="col-sm-12 form-control" name="nama_dokumen" placeholder="Nama Dokumen" required="required"><br/>

                            <label class="font-weight-bold">Unggah File</label>
                            <input type="file" class="form-control" name="file" id="file" placeholder="Type Here"><br/>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>
          </form>
      </div>
  </div>
</div>
<!-- end of modal upload dokumen scoring personal-->

 
<!-- start modal upload dokumen scoring pendanaan-->
<div class="modal fade" id="myUploadScoring" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="scrollmodalLabel">Unggah Dokumen Penilaian Pendanaan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          
          <form action="{{route('admin.uploadDokumenScoringPendanaan')}}" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="modal-body">
              <div class="col-lg-12">
                  <div class="card-body card-block">
                      <div class="form-row">
                        <div class="col-sm-9 col-lg-10 ml-3 ml-sm-0">
                          <input type="hidden" id="brw_id" name="brw_id">
                          <input type="hidden" id="brw_type" name="brw_type">
                          <input type="hidden" id="pendanaan_id" name="pendanaan_id">
                          <input type="hidden" id="identitas" name="ktp">
                          <div class="row" id="tambahUpload">
                            <label class="font-weight-bold">Nama Dokumen</label>
                            <input type="text" class="col-sm-12 form-control" name="nama_dokumen" placeholder="Nama Dokumen" required="required"/><br/><br/>

                            <label class="font-weight-bold">Unggah File</label>
                            <input type="file" class="form-control" name="file" id="file" placeholder="Type Here"><br/>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
            </div>
          <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          </div>
          </form>
      </div>
  </div>
</div>
<!-- end of modal upload dokumen scoring pendanaan-->

    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="/tinymce/js/tinymce/tinymce.min.js"></script>
    <style>
      .btn-cancel {
          background-color: #C0392B;
          color: #FFFF;
      }
      .custom-datatable{
          width: 250px;
          white-space: nowrap !important;
          overflow: hidden !important;
          text-overflow: ellipsis !important;
      }
      .modal-xl {
          max-width: 80% !important;
      }
      .swal-button--confirm {
          background: rgb(25, 150, 25);
      }
      .modal-lg {
          max-width: 60% !important;
      }
    </style>

    <script type="text/javascript">
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });
    
          $(document).on("input", ".allowCharacter", function(){
            this.value = this.value.replace(/[^0-9]/g, '');
          });
    
          $(document).ready(function () {

            $(document).on('hidden.bs.modal', function (event) {
              if ($('.modal:visible').length) {
                $('body').addClass('modal-open');
              }
            });

            $(document).on('show.bs.modal', '.modal', function (event) {
              var zIndex = 1040 + (10 * $('.modal:visible').length);
              $(this).css('z-index', zIndex);
              setTimeout(function() {
                  $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
              }, 0);
            });
          });

          validasi_imbal_hasil=(selectObject)=>{
            let imbal_hasil = selectObject.value;
            if(imbal_hasil < 18 ){
              swal({
                title: "Perhatian",
                text: "Imbal Hasil tidak boleh kurang dari 18%",
                icon: "warning",
              }).then(results=>{
                $('#estimasi_imbal_hasil_edit').val('');
              });
            }
          };

          validasi_durasi_proyek=(selectObject)=>{
            let bulan = selectObject.value;
            if(bulan < 3 ){
              swal({
                title: "Perhatian",
                text: "Durasi Proyek tidak boleh kurang dari 3 bulan",
                icon: "warning",
              }).then(results=>{
                $('#durasi_proyek_edit').val('');
              });
            }
          };

          validasi_dana_dibutuhkan=(selectObject)=>{
            let input = selectObject.value;
            let dana = $('#total_sisa_pinjaman').val();
            let dana_text = (new Intl.NumberFormat(['ban', 'id']).format(dana));
            
            console.log({input, dana});
            if(input > Number(dana) ){
              swal({
                title: "Perhatian",
                text: "Dana Dibutuhkan melebihi Sisa Pinjaman yang bisa dipinjamkan yaitu sebesar Rp. "+dana_text,
                icon: "warning",
              }).then(results=>{
                $('#pendanaan_dana_dibutuhkan_edit').val('');
              });
            }
          }

          var side = '/admin/borrower';
          var table = $('#tablePendanaan').DataTable({
            processing: true,
            // serverSide: true,
            ajax : {
              url : side+'/client/tableGetBorrower',
              type : 'get',
            },
            "columns" : [
              {"data" : "no"},
              {"data" : "id"},
              {"data" : "idPengajuan"},
              {"data" : "pendanaanBorrower"},
              {"data" : "namaBorrower"},
              {"data" : "tgl_pengajuan"}, 
              {"data" : "ktp"},
              {"data" : "nilaiBorrower"},
              {"data" : "nilaiPendanaan"},
              {"data" : "brw_type"},
              {"data" : "keterangan_approval"},
              {"data" : "status_proyek"},
              {"data" : "status"},
              {"data" : "pendanaan_tipe"},
              {"data" : "id_proyek"},
            ],
            "columnDefs" :[
              {
                "targets": 0,
                class : 'text-left',
                // "visible" : false,
              },
              {
                "targets": 1,
                class : 'text-left',
                "visible" : false,
              },
              {
                "targets": 2,
                class : 'text-left',
                "visible" : false,
              },
              {
                "targets": 3,
                class : 'text-left',
                // "visible" : false,
              },
              {
                "targets": 4,
                class : 'text-left',
                // "visible" : false,
              },
              {
                "targets": 5,
                class : 'text-left',
                // "visible" : false,
              },
              {
                "targets": 6,
                class : 'text-left',
                "visible" : false,
              },
              {
                "targets": 7,
                class : 'text-left',
                style : 'width:150px;',
                //"visible" : false
                "render" : function(data, type, value, meta){
                  let scorebor = "";
                  if(value["nilaiBorrower"] != ""){
                    if(value["status"]==0 && value["status_proyek"]=='Pengajuan Baru'){
                      scorebor = 0;
                    }else{
                      scorebor = value["nilaiBorrower"];
                    }
                  }else{
                    scorebor = 0;
                  }
                  return scorebor;
                }
              },
              {
                "targets": 8,
                class : 'text-left',
                style : 'width:150px;',
                //"visible" : false
                "render" : function(data, type, value, meta){
                  let score_pendanaan = '';
                  if(value["nilaiPendanaan"] != ""){
                    score_pendanaan = value["nilaiPendanaan"];
                  }else{
                    score_pendanaan = 0;
                  }
                  return score_pendanaan;
                }
              },
              {
                "targets": 9,
                class : 'text-left',
                "visible" : false,
                "render" : function(data, type, value, meta){
                  return '<input type="text" readonly class="form-control allowCharacter" id="totalPendanaan" aria-describedby="" placeholder="Scorre Total">';
                }
              },
              {
                "targets": 10,
                class : 'text-left',
                // "visible" : false,
              },
              {
                "targets": 11,
                class : 'text-left',
                // "visible" : false,
              },
              {
                "targets": 12,
                class : 'text-left',
                //"visible" : false
                "render" : function(data, type, value, meta){
                  if(value["status"]==0 && value["status_proyek"]=='Pengajuan Baru'){
                    return '<button class="btn btn-success " data-toggle="modal" data-target="#ubah_data"  id="btnUbahData">Penilaian</button>';
                  }else if(value["status"]==2 && value["status_proyek"]=='Pengajuan Ditolak'){
                    return '<button class="btn btn-danger " data-toggle="modal" data-target="#ubah_data" id="btnUbahData">Lihat</button>';
                  }else if(value["status"]==3 && value["status_proyek"]=='Menunggu Persetujuan'){
                    return '<button class="btn btn-info " data-toggle="modal" data-target="#ubah_data"  id="btnUbahData">Ubah</button>';
                  }else{
                    return '<button class="btn btn-primary " data-toggle="modal" data-target="#ubah_data"  id="btnUbahData">Lihat</button>';
                  }
                }
              },
              {
                "targets": 13,
                class : 'text-left',
                "visible" : false,
              },
              {
                "targets": 14,
                class : 'text-left',
                "visible" : false,
              }
            ]
          });
      
      

          // $('#tablePendanaan tbody').on('focusout','#scorePendanaan , #scorrePersonal', function(){
          //   var data = table.row( $(this).parents('tr') ).data();

          //    if($('#scorrePersonal').val() == '' || $('#scorePendanaan').val() == '')
          //    {
          //       $('#totalPendanaan').val('');
          //    }
          //    else
          //    {
          //     nilai1 = $('#scorrePersonal').val();
          //     nilai2 = $('#scorePendanaan').val();

          //     hasil = parseInt(nilai1) + parseInt(nilai2);
          //     if(isNaN(hasil)){
          //       $('#totalPendanaan').html('0');
          //     }
          //     else{
          //       $('#totalPendanaan').val(hasil);
          //     }
          //    }
          // })
          
        $('#button_setuju_save').click(function(e){
          let estimasi = new Date($('#estimasi_mulai_edit').val());
          estimasi.setHours(0,0,0,0)

          let hari_ini = new Date();
          hari_ini.setHours(0,0,0,0)

          if(hari_ini > estimasi){
            swal({
              title: "Notifikasi",   
              text: "Tanggal Mulai Proyek harus lebih dari hari ini",   
              icon: "warning",   
            }).then(results=>{
              $('#estimasi_mulai_edit').focus();
            });
            return false;
          }else if( $('#penilaian_personal').val() == 0 ){
            swal({
              title: "Notifikasi",   
              text: "Penilaian Pefindo tidak boleh 0",
              icon: "warning",   
            }).then(results=>{
              $('#penilaian_personal').focus();
            });
            return false;
          }else if($('#penilaian_pendanaan').val() < 100 ){
            swal({
              title: "Notifikasi",   
              text: "Penilaian Dana Syariah tidak boleh kurang dari 100",   
              icon: "warning",   
            }).then(results=>{
              $('#penilaian_pendanaan').focus();
            });
            return false;
          }else{
            swal({
              title: "Notifikasi",   
              text: "Penilaian Dilakukan dengan sebaik-baiknya, berpedoman pada dokumen panduan penilaian dan sudah sesuai dengan aturan yang berlaku. Apakah anda yakin?",   
              icon: "success",   
              buttons: true,
              showCancelButton: true,
            }).then(results=>{
              if(results){
                $("input[type=checkbox]").removeAttr("disabled");
                $('#save_persetujuan_data').trigger('click');
              }else{
                return false;
              }
            });
          }
        });

        $('#tipe_pendanaan_edit').change(function(){
            let tipe_pendanaan = $('#tipe_pendanaan_edit option:selected').val();
            let brw_id = $('#brw_id_edit').val();
            let pengajuan_id = $('#pengajuan_id_edit').val();
            let brw_type = $('#brw_type_edit').val();

            console.log({tipe_pendanaan, brw_id, pengajuan_id});

            show_dokumen_pendukung(pengajuan_id, brw_id, tipe_pendanaan, brw_type);
        });

          $('#tablePendanaan tbody').on('click','#btnUbahData', function(){
            var data = table.row( $(this).parents('tr') ).data();

            var idbrw = data.id;
            var idPengajuan = data.idPengajuan;
            var brw_type = data.brw_type;
            var nilai_borrower = data.status == 0 && data.status_proyek == 'Pengajuan Baru' ? 0 : data.nilaiBorrower;
            var nilai_pendanaan = data.nilaiPendanaan !== '' ? data.nilaiPendanaan : 0;
            var status_approve = data.status;
            var status_proyek = data.status_proyek;
            let id_proyek = data.id_proyek;

            console.log(status_proyek);
            // tinymce.get('#textarea_deskripsi').getBody().setAttribute('contenteditable', !this.checked);

            $("#brw_type_edit").val(brw_type);
            $('#penilaian_personal').val(nilai_borrower);
            $('#penilaian_pendanaan').val(nilai_pendanaan);
            $("#simpan_jaminan").show();
            $("#tutup_jaminan").show();

            $("#form_setuju_pendanaan :input").prop("readonly", false);
            $("#modal_jaminan :input").prop("readonly", false);
            $('#btnScore').show();
            $('#btnTambahJaminan').show();
            $("#button_setuju_save").show();
            $("#file_sp3").hide();
            $("#file_akad").hide();
            $("input[type=file]").prop('disabled', false);
            $("#tipe_pendanaan_edit").prop('disabled', false);
            $("#jenis_jaminan_edit").prop('disabled', false);
            $("#kantor_penerbit_jaminan").prop('disabled', false);
            $("#akad_edit").prop('disabled', false);
            $("textarea").prop('readonly', false);
            tinymce.get('textarea_deskripsi').setMode('code');

            $('#image_detil1').attr('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');
            $('#image_detil2').attr('src', 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==');

            if(status_proyek=='Pengajuan Ditolak' || status_proyek=='Tidak Disetujui oleh Pak Atis' || status_proyek=='Pendanaan Aktif'
            || status_proyek=='Penggalangan Dana' || status_proyek == 'Menunggu TTD Akad' || status_proyek=='Proyek Berjalan'|| status_proyek=='Proyek Selesai'){
                $("#form_setuju_pendanaan :input").prop("readonly", true);
                $("#modal_jaminan :input").prop("readonly", true);
                $('#btnScore').hide();
                $('#btnTambahJaminan').hide();
                $("#button_setuju_save").hide();
                $("#simpan_jaminan").hide();
                $("#tutup_jaminan").hide();
                $("input[type=file]").prop('disabled', true);
                $("#tipe_pendanaan_edit").prop('disabled', true);
                $("#jenis_jaminan_edit").prop('disabled', true);
                $("#kantor_penerbit_jaminan").prop('disabled', true);
                $("#akad_edit").prop('disabled', true);
                $("textarea").prop('readonly', true);
                tinymce.get('textarea_deskripsi').setMode('readonly');
            }

            if(status_proyek=='Pendanaan Aktif' || status_proyek=='Penggalangan Dana'|| status_proyek=='Proyek Berjalan' || status_proyek == 'Menunggu TTD Akad' || status_proyek=='Proyek Selesai'){
                $("#file_sp3").show();
                $("#file_sp3").attr("href", '{{asset("/storage/akad_borrower")}}/'+idbrw+'/SP3.pdf');
            }else{
                $("#file_sp3").hide();
                $("#file_sp3").removeAttr('href');
            }

            if(status_proyek=='Proyek Berjalan'|| status_proyek=='Proyek Selesai'){
                lihat_dokumen_akad(idbrw, id_proyek);
            }else{
                $("#file_akad").hide();
            }

            console.log({
              'idbrw' : idbrw,
              'idPengajuan' : idPengajuan,
              'side'  : side,
              'brw_type':brw_type,
              'nilai_borrower':nilai_borrower,
              'nilai_pendanaan':nilai_pendanaan
            })

            $.ajax({
                url : side+'/client/GetUbahVerifBorrower/'+idPengajuan+'/'+idbrw,
                method: "get",
                success:function(data,value)
                {
                  console.log(data);
                  let brw_id = data.data_edit.brw_id;
                  let durasi_proyek = data.data_edit.durasi_proyek;
						      let pengajuan_id = data.data_edit.pengajuan_id;
                  let estimasi_imbal_hasil = data.data_edit.estimasi_imbal_hasil;
                  let estimasi_mulai = data.data_edit.estimasi_mulai;
                  let status_approval = data.data_edit.status;
                  let pendanaan_dana_dibutuhkan = data.data_edit.pendanaan_dana_dibutuhkan;
                  let pendanaan_nama = data.data_edit.pendanaan_nama;
                  let keterangan = data.data_edit.keterangan;
                  var pendanaan_tipe = data.data_edit.pendanaan_tipe;
                  let pendanaan_akad = data.data_edit.pendanaan_akad;
                  let lokasi_proyek = data.data_edit.lokasi_proyek;
                  let geocode = data.data_edit.geocode;
                  let detail_pendanaan = data.data_edit.detail_pendanaan;
                  let total_sisa_pinjaman = data.data_rekening.total_sisa

                  let master_tipe_pendanaan = data.data_master;
                  let master_jenis_jaminan = data.data_jenis_jaminan;
                  let master_kantor_penerbit = data.data_kantor_penerbit;

                  let image_url = '{{asset("/storage")}}/'+data.data_edit.gambar_utama+'?'+performance.now();
                  $('#id_image').attr('src', image_url);

                  if(data.gambar_proyek_detil.length == 2){
                    let image_url_detil1 = '{{asset("/storage")}}/'+data.gambar_proyek_detil[0].gambar+'?'+performance.now();
                    let image_url_detil2 = '{{asset("/storage")}}/'+data.gambar_proyek_detil[1].gambar+'?'+performance.now();
                    let id_image_detil1 = data.gambar_proyek_detil[0].id;
                    let id_image_detil2 = data.gambar_proyek_detil[1].id;
                    $('#image_detil1').attr('src', image_url_detil1);
                    $('#image_detil2').attr('src', image_url_detil2);
                    $('#id_image_detil1').val(id_image_detil1);
                    $('#id_image_detil2').val(id_image_detil2);
                  }else if(data.gambar_proyek_detil.length > 0){
                    let image_url_detil1 = '{{asset("/storage")}}/'+data.gambar_proyek_detil[0].gambar+'?'+performance.now();
                    let id_image_detil1 = data.gambar_proyek_detil[0].id;
                    $('#image_detil1').attr('src', image_url_detil1);
                    $('#id_image_detil1').val(id_image_detil1);
                  }

                  // Empty the dropdown
                  $('#tipe_pendanaan_edit').find('option').not(':first').remove();
                  $('#jenis_jaminan_edit').find('option').not(':first').remove();
                  $('#kantor_penerbit_jaminan').find('option').not(':first').remove();

                  $('#brw_id_edit').val(brw_id);
                  $("#pendanaan_nama_edit").val(pendanaan_nama);
                  $('#alamat_edit').val(lokasi_proyek);
                  $('#geocode_edit').val(geocode);
                  $('#id_pengajuan').val(pengajuan_id);
                  if(pendanaan_akad == 1){
                    $('#akad_edit').val(1);
                  }else{
                    $('#akad_edit').val(2);
                  }
                  for(let i=0; i<master_tipe_pendanaan.length; i++){
                    let id = master_tipe_pendanaan[i].tipe_id;
                    let name = master_tipe_pendanaan[i].pendanaan_nama;
                    let option = "<option value='"+id+"'>"+name+"</option>"; 
                    $("#tipe_pendanaan_edit").append(option); 
                  }
                  for(let i=0; i<master_jenis_jaminan.length; i++){
                    let id = master_jenis_jaminan[i].id_jenis_jaminan;
                    let name = master_jenis_jaminan[i].jenis_jaminan;
                    let option = "<option value='"+id+"'>"+name+"</option>"; 
                    $("#jenis_jaminan_edit").append(option); 
                  }
                  for(let i=0; i<master_kantor_penerbit.length; i++){
                    let name = 'BPN '+master_kantor_penerbit[i].kota;
                    let option = "<option value='"+name+"'>"+name+"</option>"; 
                    $("#kantor_penerbit_jaminan").append(option); 
                  }

                  $('#tipe_pendanaan_edit').val(pendanaan_tipe);
                  $('#durasi_proyek_edit').val(durasi_proyek);
                  $("#pengajuan_id_edit").val(pengajuan_id);
                  $("#estimasi_imbal_hasil_edit").val(estimasi_imbal_hasil);
                  $("#estimasi_mulai_edit").val(estimasi_mulai);
                  $("#keterangan_penilaian").val(keterangan);
                  $("#pendanaan_dana_dibutuhkan_edit").val(pendanaan_dana_dibutuhkan);
                  $('#total_sisa_pinjaman').val(total_sisa_pinjaman);
                  if (detail_pendanaan != null){
                      tinymce.get('textarea_deskripsi').setContent(detail_pendanaan);
                  }
                  else{
                      tinymce.get('textarea_deskripsi').setContent("");
                  }

                  createSimulasi();
                  createJaminan(idPengajuan, status_approval);
                  show_dokumen_pendukung(idPengajuan, brw_id, pendanaan_tipe, brw_type, status_approval);
                  
                }
            });
          });

          lihat_dokumen_akad=(idbrw, id_proyek)=>{
            $.ajax({
              url : '/borrower/downloadWakalahBorrower/'+idbrw+'/'+id_proyek,
              method: "get",
              success: function (response) {
                let data = JSON.parse(response);
                console.log(data);
                if(data.code == 200){
                  $("#file_akad").show();
                  $("#file_akad").attr("href", data.data.download.url);
                }else{
                  $("#file_akad").removeAttr("href");
                }
              },
              error: function(XMLHttpRequest, textStatus, errorThrown) {
                $("#file_akad").removeAttr("href");
              }
            });
          }

          show_dokumen_pendukung=(idPengajuan, idbrw, pendanaan_tipe, brw_type, status_approval)=>{
            $.ajax({
                url : side+'/client/GetLihatDokumen/'+idPengajuan+'/'+idbrw+'/'+pendanaan_tipe+'/'+brw_type,
                method: "get",
                success:function(data)
                {
                  console.log(data);
                  let html ="";
                  $('#checklist_persyaratan').html('');
                    for (let i =0; i<data.length; i++) {
                        let mandatory = "";
                        let checked   = "";
                        let disabled   = "";
                        if(status_approval==1 || status_approval==2 || status_approval==5){
                          disabled   = "disabled";
                        }else{
                          disabled   = "";
                        }
                        if(data[i].checked == 1){
                            checked   = 'checked';
                        }
                        if(data[i].persyaratan_mandatory == 1){
                            mandatory = '<span class="text-danger">*</span>';
                            checked   = 'checked';
                            disabled  = "disabled";
                        }
                        
                        html += '<div class="col-6">'
                                    +'<label class="css-control css-control-primary css-radio mr-10 text-dark">'
                                        +'<input type="checkbox" '+disabled+' '+checked+' class="css-control-input" id='+data[i].persyaratan_id+' name="txt_persyaratan_pendanaan[]" value='+data[i].persyaratan_id+'>'
                                        +'<span class="css-control-indicator"></span> '+data[i].persyaratan_nama+' '
                                        +mandatory
                                    +'</label>'
                                +'</div>';
                  }
                  
                  $('#checklist_persyaratan').append(html);

                }
            });
          }

          $('#tablePendanaan tbody').on('click','#btnScore', function(){
          // function btn_score_pefindo(id1){
            
            // console.log(table);
              var id1 = $(this).data('idbrw');
              var idp = $(this).data('idp');
            
              console.log(side+'/searchPefindo/'+id1);
              $.ajax({
                  url: side+'/searchPefindo/'+id1,
                  method : 'get',
                  success:function(data1)
                  {
                    console.log(data1);
                    var data = JSON.parse(data1);
                    if(data.status_code == '00')
                    {
                      if(data.num_record>1){
                        
                        var i ;
                        var nilai = [];
                        var ID = [];
                        var pilihan = "<select class='form-control' id='pilihlah' name='pilihlah'>";
                        for (i = 0; i < data.num_record; i++) {
                          nilai[i] = data["detail"][i]["s:Envelope"]["s:Body"]["GetCustomReportResponse"]["GetCustomReportResult"]["a:CIP"]["b:RecordList"]["b:Record"][0]["b:Score"];
                          ID[i] = data.data[i].PefindoId;
                          pilihan += "<option value="+nilai[i]+">PefindoID : "+ID[i]+" - Nilai : "+nilai[i]+"</option>";
                        }
                        pilihan += "</select>";

                        var nilai_score = "";

                        Swal.fire({
                          title:"<b>Pilih Pefindo ID</b>",
                          html: pilihan,
                          type: "info",
                          showCancelButton: true,
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Terapkan",
                          cancelButtonText: "Batal",
                          closeOnConfirm: true,
                          closeOnCancel: true
                        }).then((result) => {
                          var nl = $('#pilihlah option:selected').val();
                          var nli = parseInt(nl);
                          $("#scorrePersonal_"+idp).val(nli);
                        });

                      }else if(data.num_record==1){
                        var b = data["detail"]["s:Envelope"]["s:Body"]["GetCustomReportResponse"]["GetCustomReportResult"]["a:CIP"];
                        var d=b["b:RecordList"];
                        var c = d["b:Record"];
                        var nilaiP = data["detail"]["s:Envelope"]["s:Body"]["GetCustomReportResponse"]["GetCustomReportResult"]["a:CIP"]["b:RecordList"]["b:Record"][0]["b:Score"];
                      
                        // notif
                        Swal.fire({
                          title:"<b>Pefindo Biro Kredit ID:"+data.data.PefindoId+"</b>",
                          html: "<strong>Nilai Personal: <u>"+nilaiP+"</u></strong>",
                          type: "info",
                          showCancelButton: true,
                          confirmButtonClass: "btn-danger",
                          confirmButtonText: "Terapkan",
                          cancelButtonText: "Batal",
                          closeOnConfirm: true,
                          closeOnCancel: true
                        }).then((result) => {
                          $("#scorrePersonal_"+idp).val(nilaiP);
                        });
                      }
                      
                    } else {
                      swal("Gagal", "Tidak Ada Pefindo Biro Kredit ID", "error");
                    }
                  },
                    error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    console.log('Status: ' + textStatus); console.log('Error: ' + errorThrown); 
                }    
              })
          })

          $('#tablePendanaan tbody').on('click','#btnScoreDld', function(){
          // function btn_score_pefindo(id1){
            
            // console.log(table);
              var id1 = $(this).data('idbrw');
              var idp = $(this).data('idp');
            
              console.log(side+'/searchPefindo/'+id1);
              // $.ajax({
              //     url: side+'/searchPefindo/'+id1,
              //     method : 'get',
              //     success:function(data1)
              //     {
              //       console.log(data1);
              //       var data = JSON.parse(data1);
              //       if(data.status_code == '00')
              //       {
              //         if(data.num_record>1){
                        
              //           var i ;
              //           var nilai = [];
              //           var ID = [];
              //           var pilihan = "<select class='form-control' id='pilihlah' name='pilihlah'>";
              //           for (i = 0; i < data.num_record; i++) {
              //             nilai[i] = data["detail"][i]["s:Envelope"]["s:Body"]["GetCustomReportResponse"]["GetCustomReportResult"]["a:CIP"]["b:RecordList"]["b:Record"][0]["b:Score"];
              //             ID[i] = data.data[i].PefindoId;
              //             pilihan += "<option value="+nilai[i]+">PefindoID : "+ID[i]+" - Nilai : "+nilai[i]+"</option>";
              //           }
              //           pilihan += "</select>";

              //           var nilai_score = "";

              //           Swal.fire({
              //             title:"<b>Pilih Pefindo ID</b>",
              //             html: pilihan,
              //             type: "info",
              //             showCancelButton: true,
              //             confirmButtonClass: "btn-danger",
              //             confirmButtonText: "Terapkan",
              //             cancelButtonText: "Batal",
              //             closeOnConfirm: true,
              //             closeOnCancel: true
              //           }).then((result) => {
              //             var nl = $('#pilihlah option:selected').val();
              //             var nli = parseInt(nl);
              //             $("#scorrePersonal_"+idp).val(nli);
              //           });

              //         }else if(data.num_record==1){
              //           var b = data["detail"]["s:Envelope"]["s:Body"]["GetCustomReportResponse"]["GetCustomReportResult"]["a:CIP"];
              //           var d=b["b:RecordList"];
              //           var c = d["b:Record"];
              //           var nilaiP = data["detail"]["s:Envelope"]["s:Body"]["GetCustomReportResponse"]["GetCustomReportResult"]["a:CIP"]["b:RecordList"]["b:Record"][0]["b:Score"];
                      
              //           // notif
              //           Swal.fire({
              //             title:"<b>Pefindo Biro Kredit ID:"+data.data.PefindoId+"</b>",
              //             html: "<strong>Nilai Personal: <u>"+nilaiP+"</u></strong>",
              //             type: "info",
              //             showCancelButton: true,
              //             confirmButtonClass: "btn-danger",
              //             confirmButtonText: "Terapkan",
              //             cancelButtonText: "Batal",
              //             closeOnConfirm: true,
              //             closeOnCancel: true
              //           }).then((result) => {
              //             $("#scorrePersonal_"+idp).val(nilaiP);
              //           });
              //         }
                      
              //       } else {
              //         swal("Gagal", "Tidak Ada Pefindo Biro Kredit ID", "error");
              //       }
              //     },
              //       error: function(XMLHttpRequest, textStatus, errorThrown) { 
              //       console.log('Status: ' + textStatus); console.log('Error: ' + errorThrown); 
              //   }    
              // })
          })

          
        //})
        
        function getDataBorrower(brw_id,pendanaan_id,ktp,brw_type)
        {
          
          var id_brw = brw_id;
          var brw_type = brw_type;
          var id_pendanaan = pendanaan_id;
          var identitas = ktp;

          //alert(brw_type);
          $('#brw_id').val(id_brw);
          $('#brw_type').val(brw_type);
          $('#pendanaan_id').val(id_pendanaan);
          $('#identitas').val(ktp);

          $('#brw_id2').val(id_brw);
          $('#brw_type2').val(brw_type);
          $('#pendanaan_id2').val(id_pendanaan);
          $('#identitas2').val(ktp)
         
        }

        function getDocScoring(pendanaan_id)
        {
          
          var id_pendanaan = pendanaan_id;

          location.href  = "/admin/getDokumenScoring/"+id_pendanaan;

        }

        function getDocScoringPersonal(pendanaan_id)
        {
          
          var id_pendanaan = pendanaan_id;

          location.href  = "/admin/getDokumenScoringPersonal/"+id_pendanaan;

        }

        function readURL(input, id_image) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                  
                    $('#'+id_image)
                        .attr('src', e.target.result)
                        .width('400px')
                        .height('300px');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function createSimulasi(){

            var EstimasiWaktu   = $('#estimasi_mulai_edit').val();
            var DanaDibutuhkan  = $('#pendanaan_dana_dibutuhkan_edit').val();
            var Durasi          = $('#durasi_proyek_edit').val();
            var ImbalHasil      = $('#estimasi_imbal_hasil_edit').val();
            var cicilanBulanan  = 2;
            var imbalPercent    = DanaDibutuhkan*((ImbalHasil/100)*(Durasi/12));
            var totalPinjaman   = parseInt(DanaDibutuhkan) + parseInt(imbalPercent); 
            var percentCicilan  = (DanaDibutuhkan*cicilanBulanan)/100; 
            var nTenor          = Durasi-1;
            var endCicilan      = totalPinjaman - (nTenor * percentCicilan);
            var tagihanPokokAkhir = totalPinjaman-(percentCicilan*nTenor);

            console.log({
              'EstimasiWaktu':EstimasiWaktu,
              'DanaDibutuhkan':DanaDibutuhkan,
              'Durasi':Durasi,
              'ImbalHasil':ImbalHasil,
              'endCicilan':endCicilan
            })

            $("#Divsimulasi").html();
            $( "#Divsimulasi" ).fadeIn( 1000, function() {
            $("#list_simulasi").DataTable().clear().draw();
                if(Durasi > 12){
                //
                }else{
                    let total_bayar = 0;
                    let sisa_tagihan = totalPinjaman;
                    let table_simulasi = $("#list_simulasi").DataTable({
                      destroy: true,
                      "searchable": false,								   
                      "paging":   false,					 
                      bFilter: false,
                      "bPaginate": false,
                      "bInfo": false,
                    });  
                    for(var i=1; i<=Durasi; i++){
                      let tgl_mulai_proyek = new Date(EstimasiWaktu);
                      let newDate = new Date(tgl_mulai_proyek.setMonth(tgl_mulai_proyek.getMonth()+i));

                      let month = newDate.getUTCMonth() + 1; //months from 1-12
                      let day = newDate.getUTCDate();
                      let year = newDate.getUTCFullYear();

                      let newdate = day + "/" + month + "/" + year;
                      if(i == Durasi){
                        total_bayar += endCicilan;
                        sisa_tagihan -= endCicilan;
                        table_simulasi.row.add([
                          i,
                          newdate,
                          'Bulan ke '+i,
                          parseInt(tagihanPokokAkhir).toLocaleString(),
                          '0',
                        ]).draw( );
                      }else{
                        total_bayar += percentCicilan; 
                        sisa_tagihan -= percentCicilan;
                        table_simulasi.row.add([
                            i,
                            newdate,
                            'Bulan ke '+i,
                            parseInt(percentCicilan).toLocaleString(),
                            parseInt(sisa_tagihan).toLocaleString(),
                        ]).draw( );
                      }
                    };
                    let text_total_bayar = 0;
                    text_total_bayar = 'Rp.'+parseInt(total_bayar).toLocaleString()+',-';
                    $("#simulasi_total_bayar").html(text_total_bayar);
                } 
            });
        }

        clearTambahJaminan=()=>{
          $('#file_exist_jaminan').hide();
          $('#status_form_jaminan').val('add');
          $('#nama_jaminan').val("");
          $('#nomor_jaminan').val("");
          $('#nilai_jaminan').val("");
          $('#detil_jaminan').val("");
          $('#jenis_jaminan_edit').val("");
          $('#id_jaminan').val("");
          $('#kantor_penerbit_jaminan').val("");
          $('#nop_jaminan').val("");
          $('#file_jaminan').val("");
        }

        createJaminan=(idPengajuan, status_approval)=>{
          console.log({idPengajuan, status_approval});
            var table_jaminan = $('#table_list_jaminan').DataTable({
              ajax : {
                url : side+'/client/tableGetJaminan/'+idPengajuan,
                type : 'get',
              },
              processing: true,
              destroy: true,
              "searchable": false,								   
              "paging":   false,					 
              bFilter: false,
              "bPaginate": false,
              "bInfo": false,
              autoWidth: false,
              "columns" : [
                {"data" : "no"},
                {"data" : "pengajuan_id"},
                {"data" : "jaminan_id"},
                {"data" : "jaminan_nama"},
                {"data" : "jaminan_nomor"},
                {"data" : "jenis_jaminan"}, 
                {"data" : "jaminan_nilai"},
                {"data" : "jaminan_detail"},
                {"data" : "jaminan_jenis"},
                {"data" : "kantor_penerbit"},
                {"data" : "nomor_objek_pajak"},
                {"data" : "sertifikat"},
              ],
              "columnDefs" :[
                {
                  "targets": 0,
                  class : 'text-left',
                  // "visible" : false,
                },
                {
                  "targets": 1,
                  class : 'text-left',
                  "visible" : false,
                },
                {
                  "targets": 2,
                  class : 'text-left',
                  "visible" : false,
                },
                {
                  "targets": 3,
                  class : 'text-left',
                  // "visible" : false,
                },
                {
                  "targets": 4,
                  class : 'text-left',
                  // "visible" : false,
                },
                {
                  "targets": 5,
                  class : 'text-left',
                  "visible" : false,
                },
                {
                  "targets": 6,
                  class : 'text-left',
                  render : $.fn.dataTable.render.number(',', '.', 2, ''),
                  // "visible" : false,
                },
                {
                  "targets": 7,
                  class : 'text-left',
                  "visible" : false,
                },
                {
                  "targets": 8,
                  class : 'text-left',
                  "visible" : false
                },
                {
                  "targets": 9,
                  class : 'text-left',
                  // "visible" : false
                },
                {
                  "targets": 10,
                  class : 'text-left',
                  // "visible" : false
                },
                {
                  "targets": 11,
                  class : 'text-left',
                  "visible" : false
                },
                {
                  "targets": 12,
                  class : 'text-left',
                  "render" : function(data, type, value, meta){
                    console.log(status_approval);
                    if(status_approval==1 || status_approval==2 || status_approval==5){
                      return '<button class="btn btn-info" type="button" data-toggle="modal" data-target="#modal_jaminan" id="btnUbahJaminan" > Lihat </button>';
                    }else{
                      return '<div>'+
                        '<button class="btn btn-danger" type="button" id="btnHapusJaminan" data-jaminan_id ='+value["jaminan_id"]+'> Hapus </button>'+
                        '<button class="btn btn-success" style="margin-left:2px"  type="button" data-toggle="modal" data-target="#modal_jaminan" id="btnUbahJaminan" > Ubah </button>'+
                        '</div>';
                    }
                  }
                  // "visible" : false
                },
              ]
            });

            $('#table_list_jaminan tbody').on('click','#btnHapusJaminan', function(){
              let jaminan_id = $(this).data('jaminan_id');
              console.log(jaminan_id);
              swal({
                title: "Informasi",   
                text: "Yakin Akan Menghapus Jaminan Ini ?",   
                icon: "warning",   
                buttons: true,
                dangerMode: true,
                showCancelButton: true,
              }).then(results=>{
                if(results){
                  $.ajax({
                    url: side+'/prosess/deleteJaminanPengajuan',
                    method : 'post',
                    data : {jaminan_id:jaminan_id},
                    success:function(data)
                    {
                      console.log(data);
                      if(data.data == 'ok')
                      {
                        swal({
                          title: "Proses Berhasil",
                          //text: "Your will not be able to recover this imaginary file!",
                          type: "success",
                          showCancelButton: false,
                          confirmButtonClass: "btn-success",
                          closeOnConfirm: false
                        }).then(results=>{
                          table_jaminan.ajax.reload();
                        });
                      }else{
                        swal("Gagal", "Proses gagal, Silahkan dicoba lagi", "info");
                      }
                  
                    }
                  })
                }
              });
            })


            $('#table_list_jaminan tbody').on('click','#btnUbahJaminan', function(e){
              e.preventDefault();
              $('#file_exist_jaminan').show();

              $('#nama_jaminan').val('');
              $('#nomor_jaminan').val('');
              $('#nilai_jaminan').val('');
              $('#detil_jaminan').val('');
              $('#jenis_jaminan_edit').val('');
              $('#status_form_jaminan').val('');
              $('#id_jaminan').val('');
              $('#id_pengajuan').val('');
              $('#file_jaminan').val('');


              var data = table_jaminan.row( $(this).parents('tr') ).data();
              console.log(data);
              setTimeout(()=>{
                $('#nama_jaminan').val(data.jaminan_nama);
                $('#nomor_jaminan').val(data.jaminan_nomor);
                $('#nilai_jaminan').val(data.jaminan_nilai);
                $('#detil_jaminan').val(data.jaminan_detail);
                $('#jenis_jaminan_edit').val(data.jaminan_jenis);
                $('#kantor_penerbit_jaminan').val(data.kantor_penerbit);
                $('#nop_jaminan').val(data.nomor_objek_pajak);
                $('#status_form_jaminan').val('edit');
                $('#id_jaminan').val(data.jaminan_id);
                $('#id_pengajuan').val(data.pengajuan_id);

                $("#file_exist_jaminan").attr("href", '{{asset("/storage")}}/'+data.sertifikat+'?'+performance.now());
              },250);
            });

            $('#file_jaminan').change(function () {
                var ext = this.value.split('.').pop();

                switch (ext) {
                    case 'pdf':
                        break;
                    default:
                      this.value = '';
                      swal("Gagal", "Format file yang diupload bukan pdf", "error");
                }
            });


            $('#simpan_jaminan').on('click', function(e){
              console.log('simpan jaminan');
              e.preventDefault();

              let nama_jaminan = $('#nama_jaminan').val();
              let jenis_jaminan_edit = $('#jenis_jaminan_edit').val();
              let nomor_jaminan = $('#nomor_jaminan').val();
              let kantor_penerbit_jaminan = $('#kantor_penerbit_jaminan').val();
              let nop_jaminan = $('#nop_jaminan').val();
              let nilai_jaminan = $('#nilai_jaminan').val();
              let detil_jaminan = $('#detil_jaminan').val();
              let file_jaminan = $('#file_jaminan').val();
              let id_jaminan = $('#id_jaminan').val();
              let id_pengajuan = $('#id_pengajuan').val();
              let status_form_jaminan = $('#status_form_jaminan').val();

              if(nama_jaminan == ''){
                swal({
                  title: "Perhatian",
                  text: "Nama Jaminan harus diisi",
                  icon: "warning",
                }).then(results=>{
                  $('#nama_jaminan').focus();
                });
              }else if(jenis_jaminan_edit == ''){
                swal({
                  title: "Perhatian",
                  text: "Jenis Jaminan harus diisi",
                  icon: "warning",
                }).then(results=>{
                  $('#jenis_jaminan_edit').focus();
                });
              }else if(nomor_jaminan == ''){
                swal({
                  title: "Perhatian",
                  text: "Nomor Jaminan harus diisi",
                  icon: "warning",
                }).then(results=>{
                  $('#nomor_jaminan').focus();
                });
              }else if(kantor_penerbit_jaminan == ''){
                swal({
                  title: "Perhatian",
                  text: "Kantor Penerbit harus diisi",
                  icon: "warning",
                }).then(results=>{
                  $('#kantor_penerbit_jaminan').focus();
                });
              }else if(nop_jaminan == ''){
                swal({
                  title: "Perhatian",
                  text: "NOP/Nomor Surat Ukur harus diisi",
                  icon: "warning",
                }).then(results=>{
                  $('#nop_jaminan').focus();
                });
              }else if(nilai_jaminan == ''){
                swal({
                  title: "Perhatian",
                  text: "Nilai Jaminan harus diisi",
                  icon: "warning",
                }).then(results=>{
                  $('#nilai_jaminan').focus();
                });
              }
              else if( status_form_jaminan=='add' && file_jaminan == ''){
                swal({
                  title: "Perhatian",
                  text: "Sertifikat Objek Pajak harus diisi",
                  icon: "warning",
                }).then(results=>{
                  $('#file_jaminan').focus();
                });
              }
              else{

                let myform = document.getElementById("form_jaminan");
                let form_data = new FormData(myform);

                let jaminan_kata = status_form_jaminan == 'add' ? 'menambah' : 'merubah';

                // for (var pair of form_data.entries()) {
                //     console.log(pair[0]+ ', ' + pair[1]); 
                // }

                swal({
                    title: "Informasi",   
                    text:  `Apakah anda yakin ingin ${jaminan_kata} jaminan ini ?`,   
                    icon: "success",   
                    buttons: true,
                    dangerMode: false,
                    showCancelButton: true,
                  }).then(results=>{
                    if(results){
                      $.ajax({
                          url: side+'/prosess/editJaminanPengajuan',
                          method : 'post',
                          processData:false,
                          contentType:false,
                          data:form_data,
                          success:function(data)
                          {
                            console.log(data);
                            if(data.data == 'ok')
                            {
                              table_jaminan.ajax.reload();
                              if(data.file_jaminan!==''){
                                $("#file_exist_jaminan").attr("href", '{{asset("/storage")}}/'+data.file_jaminan+'?'+performance.now());
                              }
                              swal("Sukses", `Berhasil ${jaminan_kata} data`, "success");
                              document.getElementById("tutup_jaminan").click();
                            }else{
                              swal("Gagal", "Proses gagal, Silahkan dicoba lagi", "info");
                            }
                          }
                      });
                    }
                  });

                

              };

            });
        }

        getDocScoringPersonal

    </script>

    <script>
      tinymce.init({
          selector: '#textarea_deskripsi',
          height: 200,
          theme: 'modern',
          skin:'lightgray',
          plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
          toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
          image_advtab: true,
          file_picker_callback: function(callback, value, meta) {
          if (meta.filetype == 'image') {
              $('#upload').trigger('click');
              $('#upload').on('change', function() {
              var file = this.files[0];
              var reader = new FileReader();
              reader.onload = function(e) {
                  callback(e.target.result, {
                  alt: ''
                  });
              };
              reader.readAsDataURL(file);
              });
          }
          },
          imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
          content_css: [
              '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
              '//www.tinymce.com/css/codepen.min.css'
          ]
      });
    </script>

@endsection
