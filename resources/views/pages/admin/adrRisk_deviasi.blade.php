@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@push('add-more-style')
    <style>
        input[type=checkbox] {
            top: 1rem;
            width: 1rem;
            height: 1rem;
        }

        .btn-danaSyariah {
            background-color: #16793E;
        }

        .box-title {
            margin-top: -33px;
            background-color: #FFFFFF;
            width: -moz-fit-content;
            width: fit-content;
        }

        body {
            background-color: #FFFFFF;
        }

        .card {
            border-color: #000000;
            margin-bottom: 60px
        }

        .select2-selection {
            height: 35px !important;
        }

        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            background-color: #fff;
        }

        ul.breadcrumb li {
            display: inline;
            font-size: 18px;
        }

        ul.breadcrumb li+li:before {
            padding: 8px;
            color: black;
            content: "/\00a0";
        }

        ul.breadcrumb li a {
            color: #0275d8;
            text-decoration: none;
        }

        ul.breadcrumb li a:hover {
            color: #01447e;
            text-decoration: underline;
        }

        .form-control,
        .input-group-text,
        .table {
            font-size: 13px !important;
        }

        .table-wrapper {
            max-height: 250px;
            overflow: auto;
            display:inline-block;
        }

    </style>
@endpush

@section('content')

    <!-- Main Container -->
    <ul class="breadcrumb">
        <li><a href="{{ route('risk.listproyek') }}">Daftar Deviasi</a></li>
        <li>Detail</li>
    </ul>

    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('progressadd'))
                    <div class="alert alert-danger">
                        {{ session()->get('progressadd') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('updatedone'))
                    <div class="alert alert-success">
                        {{ session()->get('updatedone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('createdone'))
                    <div class="alert alert-info">
                        {{ session()->get('createdone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="box-title">
                                <label class="form-check-label text-black h3" for="form_informasi_legalitas">Informasi
                                    Deviasi &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_risk">Nama Risk Management</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ isset($pengajuan) && !empty($pengajuan->nama_risk) ? $pengajuan->nama_risk : Auth::user()->firstname . ' ' . Auth::user()->lastname }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-tanggal_risk">Tanggal Risk</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ $pengajuan->tanggal_deviasi }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-ipdipp">Id Penerima Dana | Id Pengajuan Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ !empty($pengajuan) ? $pengajuan->brw_id . ' | ' . $pengajuan->pengajuan_id : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_penerima_dana">Nama Penerima Dana</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ !empty($pengajuan) ? $pengajuan->nama : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-plafon_pengajuan_pendanaan">Plafon Pengajuan
                                        Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ !empty($pengajuan) ? 'Rp' . str_replace(',', '.', number_format($pengajuan->pendanaan_dana_dibutuhkan)) : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-fasilitas_pendanaan">Fasilitas Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ !empty($pengajuan) ? $pengajuan->tipe_pendanaan : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_pendanaan">Jangka Waktu Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {{ !empty($pengajuan) ? $pengajuan->durasi_proyek : '' }}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-kategori_penerima_pendanaan">Kategori Penerima
                                        Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if ($pengajuan->sumber_pengembalian_dana == '1')
                                        Penghasilan Tetap
                                    @else
                                        Penghasilan Tidak Tetap
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- end informasi compliance -->
                        <!-- data penerima Pendanaan -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h3" for="form_resume_fasilitas_pendanaan">Resume
                                    Fasilitas Pendanaan &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-9">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-nomor_naup">Nomor NAUP</label>
                                </div>
                            </div>
                            <div class="col-md-1">:</div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="no_naup" name="no_naup"
                                        value=" {{ !empty($pengajuan) ? $naup->no_naup : '' }}" readonly>

                                </div>
                            </div>
                        </div>
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_resume_fasilitas_pendanaan">I. DATA
                                    PENERIMA PENDANAAN &nbsp</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-skema_pengajuan">Skema Pengajuan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="skema_pembiayaan" name="skema_pembiayaan"
                                        value="{{ $pengajuan->skema_pembiayaan == '1' ? 'Single Income' : 'Joint Income' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_penerima_pendanaan">Nama Penerima Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nama_penerima_pendanaan"
                                        name="nama_penerima_pendanaan"
                                        value="{{ !empty($pengajuan) ? $pengajuan->nama : '' }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2 div_pasangan">
                                <div class="form-group">
                                    <label for="wizard-progress2-plafon_pasangan">Nama Pasangan</label>
                                </div>
                            </div>
                            <div class="col-md-4 div_pasangan">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nama_pasangan" name="nama_pasangan"
                                        value="{{ !empty($pengajuan) ? $pengajuan->nama_pasangan : '' }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-usia_saat_pengajuan">Usia Saat Pengajuan (Tahun)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="usia_saat_pengajuan"
                                        name="usia_saat_pengajuan"
                                        value="{{ !empty($pengajuan) ? \Carbon\Carbon::parse($pengajuan->tgl_lahir)->age : '' }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2 div_pasangan">
                                <div class="form-group">
                                    <label for="wizard-progress2-usia_pasangan">Usia Pasangan (Tahun)</label>
                                </div>
                            </div>
                            <div class="col-md-4 div_pasangan">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="usia_saat_pengajuan"
                                        name="usia_saat_pengajuan"
                                        value="{{ !empty($pengajuan) ? \Carbon\Carbon::parse($pengajuan->tgl_lahir_pasangan)->age : '' }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-status_karyawan">Status Karyawan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="status_karyawan" name="status_karyawan"
                                        value="{{ $pengajuan->status_pekerjaan == '1' ? 'Kontrak' : 'Tetap' }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2 div_pasangan">
                                <div class="form-group">
                                    <label for="wizard-progress2-status_karyawan_pasangan">Status Karyawan Pasangan</label>
                                </div>
                            </div>
                            <div class="col-md-4 div_pasangan">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="status_karyawan_pasangan"
                                        name="status_karyawan_pasangan"
                                        value="{{ $pengajuan->status_pekerjaan_pasangan == '1' ? 'Kontrak' : 'Tetap' }}"
                                        readonly>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jabatan">Jabatan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jabatan" name="jabatan"
                                        value=" {{ !empty($pengajuan) ? $pengajuan->jabatan : '' }}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2 div_pasangan">
                                <div class="form-group">
                                    <label for="wizard-progress2-jabatan_pasangan">Jabatan Pasangan</label>
                                </div>
                            </div>
                            <div class="col-md-4 div_pasangan">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jabatan_pasangan" name="jabatan_pasangan"
                                        value=" {{ !empty($pengajuan) ? $pengajuan->jabatan_pasangan : '' }}" readonly>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tempat_bekerja">Tempat Bekerja</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tempat_bekerja" name="tempat_bekerja"
                                        value=" {{ !empty($pengajuan) ? $pengajuan->nama_perusahaan : '' }}" readonly>
                                </div>
                            </div>

                            <div class="col-md-2 div_pasangan">
                                <div class="form-group">
                                    <label for="wizard-progress2-tempat_bekerja_pasangan">Tempat Bekerja Pasangan</label>
                                </div>
                            </div>
                            <div class="col-md-4 div_pasangan">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tempat_bekerja_pasangan"
                                        name="tempat_bekerja_pasangan"
                                        value=" {{ !empty($pengajuan) ? $pengajuan->nama_perusahaan_pasangan : '' }}"
                                        readonly>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-lama_bekerja">Lama Bekerja (Sejak Tahun)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="masa_kerja_tahun" name="masa_kerja_tahun"
                                        value="{{ !empty($pengajuan->lama_bekerja) ? $pengajuan->lama_bekerja : 0 }} "
                                        readonly>

                                </div>
                            </div>

                            <div class="col-md-2 div_pasangan">
                                <div class="form-group">
                                    <label for="wizard-progress2-lama_bekerja">Lama Bekerja Pasangan (Sejak Tahun)</label>
                                </div>
                            </div>
                            <div class="col-md-4 div_pasangan">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="masa_kerja_tahun_pasangan"
                                        name="masa_kerja_tahun_pasangan"
                                        value="{{ !empty($pengajuan->lama_bekerja_pasangan) ? $pengajuan->lama_bekerja_pasangan : 0 }}  "
                                        readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-lama_perusahaan_berdiri">Lama Perusahaan Berdiri
                                        (Sejak Tahun)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    @if ($pengajuan->sumber_pengembalian_dana == '1')
                                        <input class="form-control" type="text" id="lama_usaha" name="lama_usaha"
                                            value="{{ !empty($pengajuan->usia_perusahaan) ? $pengajuan->usia_perusahaan : '' }}"
                                            readonly>

                                    @else
                                        <input class="form-control" type="text" id="lama_usaha" name="lama_usaha"
                                            value="{{ !empty($pengajuan->usia_tempat_usaha) ? $pengajuan->usia_tempat_usaha : '' }}"
                                            readonly>

                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-cash_ratio">Cash Ratio</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="lama_usaha" name="lama_usaha"
                                        value=" {{ !empty($pengajuan) ? $pengajuan->cr_maks : '' }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-maks_ftv_persen">Maks FTV (%)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="lama_usaha" name="lama_usaha"
                                        value=" {{ !empty($pengajuan) ? $pengajuan->maks_ftv_persentase : '' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-maks_ftv_rupiah">Maks FTV (Rp)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="lama_usaha" name="lama_usaha"
                                        value=" {{ !empty($pengajuan) ? $pengajuan->maks_ftv_nominal : '' }}" readonly>
                                </div>
                            </div>
                        </div>
                        <!-- end data penerima pendanaan -->
                        <!-- fasilitas pendanaan -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_fasilitas_pendanaan">II. FASILITAS
                                    PENDANAAN &nbsp</label>
                            </div>
                        </div>
                        <!-- start wakalah -->
                        @if ($naup->wakalah == '1')
                            <div class="col-12 mb-4">
                                <br>
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Wakalah</u></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nama_produk">Nama Produk</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_produk" name="nama_produk"
                                            value=" {{ !empty($pengajuan) ? $pengajuan->nama_produk : '' }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-jangka_waktu">Jangka Waktu</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="jangka_waktu_wakalah"
                                            name="jangka_waktu_wakalah"
                                            value=" {{ !empty($naup) ? $naup->jangka_waktu_wakalah : '' }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-tujuan_penggunaan">Tujuan Penggunaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="tujuan_penggunaan_wakalah"
                                            name="tujuan_penggunaan_wakalah"
                                            value=" {{ !empty($naup) ? $naup->tujuan_penggunaan_wakalah : '' }}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-sumber_pelunasan">Sumber Pelunasan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="sumber_pelunasan"
                                            name="sumber_pelunasan"
                                            value=" {{ !empty($pengajuan) ? $pengajuan->nama_produk : '' }}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pembelian_dari">Pembelian dari</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="pembelian_dari" name="pembelian_dari"
                                            value=" {{ !empty($pengajuan) ? $pengajuan->nm_pemilik : '' }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pengikatan_pendanaan_wakalah">Pengikatan
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" type="text" id="pengikatan_pendanaan_wakalah"
                                            name="pengikatan_pendanaan_wakalah" disabled>
                                            @foreach ($pengikatan_pendanaan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_pendanaan_wakalah ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-jumlah_wakalah">Jumlah Wakalah</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="jumlah_wakalah" name="jumlah_wakalah"
                                            value="Rp {{ number_format($naup->jumlah_wakalah, 0, '.', '.') }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pengikatan_jaminan">Pengikatan Jaminan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" type="text" id="pengikatan_jaminan_wakalah"
                                            name="pengikatan_jaminan_wakalah" disabled>
                                            @foreach ($pengikatan_jaminan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_jaminan_wakalah ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- end wakalah -->
                        @endif

                        <!-- start qardh -->
                        @if ($naup->qardh == '1')
                            <div class="col-12 mb-4">
                                <br>
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6" for="form_wakalah"><u>Qardh</u></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nama_produk_qardh">Nama Produk</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_produk_qardh"
                                            name="nama_produk_qardh"
                                            value=" {{ !empty($pengajuan) ? $pengajuan->nama_produk : '' }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="jangka_waktu_qardh">Jangka Waktu</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="jangka_waktu_qardh"
                                            name="jangka_waktu_qardh"
                                            value=" {{ !empty($naup) ? $naup->jangka_waktu_qardh : '' }}" readonly>

                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Hari</span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-tujuan_penggunaan_qardh">Tujuan Penggunaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="tujuan_penggunaan_qardh"
                                            value=" {{ !empty($naup) ? $naup->tujuan_penggunaan_qardh : '' }}"
                                            name="tujuan_penggunaan_qardh" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-sumber_pelunasan_qardh">Sumber Pelunasan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="sumber_pelunasan"
                                            name="sumber_pelunasan"
                                            value=" {{ !empty($pengajuan) ? 'Pencairan Fasilitas Pendanaan ' . $pengajuan->nama_produk : '' }}"
                                            readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pembelian_dari_qardh">Take Over Pendanaan dari</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="take_over_dari_qardh"
                                            name="take_over_dari_qardh"
                                            value=" {{ !empty($naup) ? $naup->take_over_dari_qardh : '' }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pengikatan_pendanaan_qardh">Pengikatan
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" type="text" id="pengikatan_pendanaan_qardh"
                                            name="pengikatan_pendanaan_qardh" disabled>
                                            @foreach ($pengikatan_pendanaan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_pendanaan_qardh ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-jumlah_wakalah_qardh">Jumlah Qardh</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="jumlah_qardh" name="jumlah_qardh"
                                            value="{{ number_format($naup->jumlah_qardh, 0, '.', '.') }}" readonly>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="pengikatan_jaminan_qardh">Pengikatan Jaminan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" type="text" id="pengikatan_jaminan_qardh"
                                            name="pengikatan_jaminan_qardh" disabled>
                                            @foreach ($pengikatan_jaminan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_jaminan_qardh ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                        @endif

                        <!-- start murabahah -->
                        @if ($naup->murabahah == '1')
                            <div class="col-12 mb-4">
                                <br>
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6"
                                        for="form_wakalah"><u>Murabahah</u></label>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-skema_pendanaan_murabahah">Skema Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="skema_pendanaan_murabahah"
                                            name="skema_pendanaan_murabahah" value="Murabahah" readonly>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="total_kewajiban_penerima">Total Kewajiban Penerima Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="total_kewajiban_penerima"
                                            name="total_kewajiban_penerima"
                                            value=" {{ !empty($total_kewajiban_penerima) ? number_format($total_kewajiban_penerima, 0, '.', '.') : 0 }}"
                                            readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nama_produk_murabahah">Nama Produk</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_produk_murabahah"
                                            name="nama_produk_murabahah"
                                            value=" {{ !empty($pengajuan) ? $pengajuan->nama_produk : '' }}" readonly>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pricing_pendanaan_murabahah">Pricing Pendanaan (Margin
                                            Rate)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input class="form-control" type="text" id="pricing_pendanaan_murabahah"
                                                name="pricing_pendanaan_murabahah"
                                                value="{{ !empty($pengajuan) ? $pengajuan->estimasi_imbal_hasil : '' }}"
                                                readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        % effective p.a
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-tujuan_penggunaan_murabahah">Tujuan Penggunaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <textarea class="form-control" cols="60" rows="5"
                                            id="tujuan_penggunaan_murabahah" name="tujuan_penggunaan_murabahah"
                                            readonly>{{ !empty($naup) ? $naup->tujuan_penggunaan_murabahah : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-angsuran_pendanaan_murabahah">Angsuran
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <table class="table table-striped table-bordered"
                                            id="angsuran_pendanaan_murabahah">
                                            {!! $simulasi_angsuran !!}
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-harga_beli_barang_murabahah">Harga Beli Barang</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="harga_beli_barang_murabahah"
                                            name="harga_beli_barang_murabahah"
                                            value="{{ !empty($pengajuan) ? number_format($pengajuan->harga_objek_pendanaan, 0, '.', '.') : '' }}"
                                            readonly>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="jangka_waktu_murabahah">Jangka Waktu</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="jangka_waktu_murabahah"
                                            name="jangka_waktu_murabahah"
                                            value="{{ !empty($pengajuan) ? $pengajuan->durasi_proyek : '' }}" readonly>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-uang_muka_penerima_pendanaan_murabahah">Uang Muka
                                            Penerima
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="uang_muka_murabahah"
                                            name="uang_muka_murabahah"
                                            value="{{ !empty($naup) ? number_format($naup->uang_muka_murabahah, 0, '.', '.') : '' }}"
                                            readonly>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-denda_murabahah">Denda</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="denda_murabahah"
                                            name="denda_murabahah"
                                            value="{{ !empty($naup) ? number_format($naup->denda_murabahah, 0, '.', '.') : '' }}"
                                            readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-harga_jual_barang_murabahah">Harga Jual Barang</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>

                                        @php
                                            $harga_beli_barang_murabahah = $pengajuan->harga_objek_pendanaan;
                                            $harga_jual_barang = (int) ($harga_beli_barang_murabahah - $naup->uang_muka_murabahah);
                                        @endphp

                                        <input class="form-control" type="text" id="harga_jual_barang_murabahah"
                                            name="harga_jual_barang_murabahah"
                                            value="{{ number_format($harga_jual_barang, 0, '.', '.') }}" readonly>
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pengikatan_pendanaan_murabahah">Pengikatan
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" type="text" id="pengikatan_pendanaan_murabahah"
                                            name="pengikatan_pendanaan_murabahah" disabled>
                                            @foreach ($pengikatan_pendanaan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_pendanaan_murabahah ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>


                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-margin_murabahah">Porsi/Plafon Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="plafon_pendanaan_murabahah"
                                            name="plafon_pendanaan_murabahah"
                                            value="{{ !empty($hasilAnalisa) ? number_format($hasilAnalisa->plafond_rekomendasi, 0, '.', '.') : '' }}"
                                            readonly>
                                    </div>

                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="pengikatan_jaminan_murabahah">Pengikatan Jaminan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" type="text" id="pengikatan_jaminan_murabahah"
                                            name="pengikatan_jaminan_murabahah" disabled>

                                            @foreach ($pengikatan_jaminan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_jaminan_murabahah ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-margin_murabahah">Margin</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="margin_murabahah"
                                            name="margin_murabahah" value="{{ number_format($margin, 0, '.', '.') }}"
                                            readonly>

                                    </div>
                                </div>

                            </div>


                        @endif

                        <!-- start imbt -->
                        @if ($naup->imbt == '1')
                            <div class="col-12 mb-4">
                                <br>
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6" for="form_wakalah"><u>IMBT</u></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-skema_pendanaan_imtb">Skema Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="skema_pendanaan_imtb"
                                            name="skema_pendanaan_imtb" readonly value="IMBT">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="nilai_sewa_imbt">Nilai Sewa/Ujroh Perbulan (Rp)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="table-wrapper">
                                        <table class="table table-striped table-bordered" id="nilai_sewa_imbt">
                                            {!! $simulasi_margin !!}  
                                        </table>
                                   </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nama_produk_imtb">Nama Produk</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_produk_imbt"
                                            name="nama_produk_imbt"
                                            value=" {{ !empty($pengajuan) ? $pengajuan->nama_produk : '' }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="uang_muka_imbt">Security Deposit Penerima Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="uang_muka_imbt" name="uang_muka_imbt"
                                            value=" {{ !empty($naup) ? number_format($naup->uang_muka_imbt, 0, '.', '.') : '' }}"
                                            readonly>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-tujuan_penggunaan_imtb">Tujuan Penggunaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="tujuan_penggunaan_imbt"
                                            name="tujuan_penggunaan_imbt"
                                            value="{{ !empty($naup) ? $naup->tujuan_penggunaan_imbt : '' }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-porsi_pendanaan_imtb">Porsi Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="porsi_pendanaan_imbt"
                                            name="porsi_pendanaan_imbt"
                                            value=" {{ !empty($hasilAnalisa) ? number_format($hasilAnalisa->plafond_rekomendasi, 0, '.', '.') : '' }}"
                                            readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="jangka_waktu_imbt">Jangka Waktu Sewa</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="jangka_waktu_imbt"
                                            name="jangka_waktu_imbt"
                                            value="{{ !empty($pengajuan) ? $pengajuan->durasi_proyek : '' }}" readonly>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pengikatan_pendanaan_imtb">Pengikatan Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="pengikatan_pendanaan_imbt"
                                            name="pengikatan_pendanaan_imbt" disabled>
                                            @foreach ($pengikatan_pendanaan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_pendanaan_imbt ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="harga_perolehan_aktiva_imbt">Harga Perolehan Aktiva</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>


                                        <input class="form-control" type="text" id="harga_perolehan_aktiva_imbt"
                                            name="harga_perolehan_aktiva_imbt"
                                            value="{{ !empty($hasilAnalisa) ? number_format($hasilAnalisa->plafond_rekomendasi, 0, '.', '.') : '' }}"
                                            readonly>

                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pengikatan_jaminan_imtb">Pengikatan Jaminan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="pengikatan_jaminan_imbt"
                                            name="pengikatan_jaminan_imbt" disabled>
                                            @foreach ($pengikatan_jaminan as $key => $val)
                                                <option value="{{ $key }}"
                                                    {{ $key == $naup->pengikatan_jaminan_imbt ? 'selected' : '' }}>
                                                    {{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="ekpektasi_nilai_sewa_imbt">Ekspektasi Nilai Sewa/Ujroh (%) Asumsi 6
                                            (Enam) Bulan Pertama</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="ekpektasi_nilai_sewa_imbt"
                                            name="ekpektasi_nilai_sewa_imbt"
                                            value="{{ !empty($pengajuan) ? number_format($pengajuan->estimasi_imbal_hasil, 0, '.', '.') : '' }}"
                                            readonly>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-angsuran_pendanaan_imtb">Angsuran Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <table class="table table-striped table-bordered" id="angsuran_pendanaan_imbt">
                                        {!! $simulasi_angsuran !!}
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="nilai_ujroh_imbt">Nilai Ujroh</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        @php
                                            $nilai_ujroh_imbt = $pengajuan->estimasi_imbal_hasil * $hasilAnalisa->plafond_rekomendasi;
                                        @endphp
                                        <input class="form-control" type="text" id="nilai_ujroh_imbt"
                                            name="nilai_ujroh_imbt"
                                            value="{{ number_format($nilai_ujroh_imbt, 0, '.', '.') }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-denda_imtb">Denda</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="denda_imbt" name="denda_imbt"
                                            value="{{ !empty($naup) ? number_format($naup->denda_imbt, 0, '.', '.') : 0 }}"
                                            readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="penetapan_nilai_ujroh_selanjutnya">Penetapan Nilai Ujroh Selanjutanya
                                            dilakukan setiap </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <select class="form-control" id="penetapan_nilai_ujroh_selanjutnya"
                                            name="penetapan_nilai_ujroh_selanjutnya" disabled>
                                            <option value="">Pilih</option>
                                            <option value="1"
                                                {{ $naup->penetapan_nilai_ujroh_selanjutnya == '1' ? 'selected' : '' }}>
                                                1</option>
                                            <option value="2"
                                                {{ $naup->penetapan_nilai_ujroh_selanjutnya == '2' ? 'selected' : '' }}>
                                                2</option>
                                            <option value="3"
                                                {{ $naup->penetapan_nilai_ujroh_selanjutnya == '3' ? 'selected' : '' }}>
                                                3</option>
                                            <option value="4"
                                                {{ $naup->penetapan_nilai_ujroh_selanjutnya == '4' ? 'selected' : '' }}>
                                                4</option>
                                        </select>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        @endif
                        <!-- end of IMBT-->

                        <!-- start MMQ -->
                        @if ($naup->mmq == '1')
                            <div class="col-md-12">
                                <div class="col-12 mb-4">
                                    <br>
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6" for="form_wakalah"><u>Musyarakah
                                                Mutanaqishah
                                                (MMQ)</u></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-skema_pendanaan_mmq">Skema Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="skema_pendanaan_mmq" value="MMQ"
                                            name="skema_pendanaan_mmq" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-jangka_waktu_pendanaan_mmq">Jangka Waktu
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="jangka_waktu_pendanaan_mmq"
                                            name="jangka_waktu_pendanaan_mmq"
                                            value="{{ !empty($pengajuan) ? $pengajuan->durasi_proyek : '' }}" readonly>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nama_produk_mmq">Nama Produk</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nama_produk_mmq"
                                            name="nama_produk_mmq"
                                            value="{{ !empty($pengajuan) ? $pengajuan->nama_produk : '' }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="jangka_waktu_penyerahan_objek_mmq">Jangka Waktu Penyerahan Objek
                                            MMQ</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="jangka_waktu_penyerahan_objek_mmq"
                                            name="jangka_waktu_penyerahan_objek_mmq"
                                            value="{{ !empty($naup) ? $naup->jangka_waktu_penyerahan_objek_mmq : '' }}"
                                            readonly>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Hari/Bulan</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-tujuan_penggunaan_mmq">Tujuan Penggunaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <textarea class="form-control" name="tujuan_penggunaan_mmq"
                                            id="tujuan_penggunaan_mmq" cols="60" rows="5"
                                            readonly>{{ !empty($naup) ? $naup->tujuan_penggunaan_mmq : '' }}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-objek_bagi_hasil_mmq">Objek Bagi Hasil</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="objek_bagi_hasil_mmq"
                                            name="objek_bagi_hasil_mmq" disabled>
                                            <option value="">Pilih</option>
                                            <option value="1" {{ $naup->objek_bagi_hasil_mmq == '1' ? 'selected' : '' }}>
                                                Pendapatan
                                                Sewa Dari Pembayaran Angsuran Sewa Per Bulan Oleh
                                                Penyewa (Musta'Jir)</option>
                                            <option value="2" {{ $naup->objek_bagi_hasil_mmq == '2' ? 'selected' : '' }}>
                                                Lainnya
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="harga_beli_aset_mmq">Harga Beli Aset</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="harga_beli_aset_mmq"
                                            name="harga_beli_aset_mmq"
                                            value="{{ !empty($pengajuan) ? number_format($pengajuan->harga_objek_pendanaan, 0, '.', '.') : 0 }}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="ekspektasi_nilai_sewa_mmq">Ekspektasi Nilai Sewa/Ujroh (%)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="col-md-8 pl-md-0">
                                        <div class="form-group">
                                            <input class="form-control" type="text" id="ekspektasi_nilai_sewa_mmq"
                                                name="ekspektasi_nilai_sewa_mmq"
                                                value="{{ !empty($pengajuan) ? number_format($pengajuan->estimasi_imbal_hasil, 0, '.', '.') : 0 }}"
                                                readonly>
                                        </div>

                                    </div>
                                    <div class="col-md-4">
                                        % effective p.a
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-plafon_pendanaan_mmq">Plafon Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="plafond_rekomendasi_mmq"
                                            name="plafond_rekomendasi_mmq"
                                            value="{{ !empty($hasilAnalisa) ? number_format($hasilAnalisa->plafond_rekomendasi, 0, '.', '.') : 0 }}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-proyeksi_total_pendanaan_mmq">Proyeksi Total Pendapatan
                                            Sewa/Ujroh</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <table class="table table-striped table-bordered" id="proyeksi_total_pendanaan_mmq">
                                        {!! $simulasi_total_margin !!}
                                    </table>
                                </div>

                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    &nbsp;
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-proyeksi_total_pendanaan_mmq">Proyeksi Pendapatan
                                            Sewa/Ujroh</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="table-wrapper">
                                    <table class="table table-striped table-bordered" id="proyeksi_pendanaan_mmq">
                                        {!! $simulasi_margin !!}
                                    </table>
                                    </div>
                                </div>


                            </div>


                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-uang_muka_penerima_dana_mmq">Uang Muka/Modal Penerima
                                            Dana</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <div class="input-group mb-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">Rp.</span>
                                            </div>
                                            <input class="form-control" type="text" id="uang_muka_mmq"
                                                name="uang_muka_mmq"
                                                value="{{ !empty($naup) ? number_format($naup->uang_muka_mmq, 0, '.', '.') : 0 }}"
                                                readonly>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nisbah_mmq"><b>Nisbah :</b></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="hishshah_per_unit">Modal Syirkah</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>

                                        @php
                                            $uang_muka_mmq = $naup->uang_muka_mmq;
                                            $total_modal_syirkah = $hasilAnalisa->plafond_rekomendasi + $uang_muka_mmq;
                                            $hasil_hishah_penyelenggara = intval($hasilAnalisa->plafond_rekomendasi / $naup->hishshah_per_unit);
                                            $hasil_hishah_penerima = intval($uang_muka_mmq / $naup->hishshah_per_unit);
                                            $total_hishah = $hasil_hishah_penyelenggara + $hasil_hishah_penyelenggara;
                                        @endphp

                                        <input class="form-control" type="text" id="modal_syirkah" name="modal_syirkah"
                                            value="{{ number_format($total_modal_syirkah, 0, '.', '.') }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nisbah_penyelenggara_mmq">1. Nisbah
                                            Penyelenggara</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nisbah_penyelenggara"
                                            name="nisbah_penyelenggara"
                                            value="{{ !empty($naup) ? $naup->nisbah_penyelenggara : 0 }}" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="hishshah_per_unit">Hishshah Per Unit</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="hishshah_per_unit"
                                            name="hishshah_per_unit"
                                            value="{{ !empty($naup) ? number_format($naup->hishshah_per_unit, 0, '.', '.') : 0 }}"
                                            readonly>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nisbah_penerima_mmq">2. Nisbah Penerima
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="nisbah_penerima_dana"
                                            name="nisbah_penerima_dana"
                                            value="{{ !empty($naup) ? $naup->nisbah_penerima_dana : 0 }}" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-hishshah_mmq"><b>Hishshah :</b></label>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-denda_perhari_mmq">Denda (Per Hari
                                            Keterlambatan)</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Rp.</span>
                                        </div>
                                        <input class="form-control" type="text" id="denda_mmq" name="denda_mmq"
                                            value="{{ !empty($naup) ? number_format($naup->denda_mmq, 0, '.', '.') : 0 }}"
                                            readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-hishshah_penyelenggara_mmq">1. Hishshah
                                            Penyelenggara</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="hishshah_penyelenggara_mmq"
                                            name="hishshah_penyelenggara_mmq" readonly
                                            value="{{ $hasil_hishah_penyelenggara }}">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Unit</span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-hishshah_penerima_pendanaan_mmq">2. Hishshah Penerima
                                            Pendanaan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="hishshah_penerima_pendanaan_mmq"
                                            name="hishshah_penerima_pendanaan_mmq" readonly
                                            value="{{ $hasil_hishah_penerima }}">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Unit</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-total_hishshah_mmq">Total Hishshah</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group mb-3">
                                        <input class="form-control" type="text" id="total_hishshah_mmq"
                                            name="total_hishshah_mmq" readonly value="{{ $total_hishah }}">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Unit</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                        <!-- end MMQ-->

                        <!-- start Ijaroh -->
                        @if ($naup->ijarah == '1')

                            <div class="col-md-12">
                                <div class="col-12 mb-4">
                                    <br>
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6"
                                            for="form_wakalah"><u>Ijarah</u></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-objek_sewa_ijarah">Objek Sewa</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="objek_sewa" name="objek_sewa"
                                            value="{{ !empty($naup) ? $naup->objek_sewa : '' }}" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="jangka_waktu_penyerahan_ijarah">Jangka Waktu Sewa</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="jangka_waktu_sewa_ijarah"
                                            name="jangka_waktu_sewa_ijarah"
                                            value="{{ !empty($pengajuan) ? $pengajuan->durasi_proyek : '' }}" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-jangka_waktu_penyerahan_sewa_ijarah">Jangka Waktu
                                            Penyerahan Objek Sewa</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input class="form-control" type="text" id="jangka_waktu_penyerahan_ijarah"
                                            name="jangka_waktu_penyerahan_ijarah"
                                            value="{{ !empty($naup) ? $naup->jangka_waktu_penyerahan_ijarah : '' }}"
                                            readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-nilai_sewa_ijarah">Nilai Sewa/Ujroh</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                   <div class="table-wrapper">
                                    <table class="table table-striped table-bordered" id="nilai_sewa_ijarah">
                                        {!! $simulasi_margin !!}
                                    </table>
                                </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="wizard-progress2-penyesuaian_nilai_sewa_ijarah">Penyesuaian Nilai
                                            Sewa/Ujroh</label>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <select class="form-control" id="penyesuaian_nilai_sewa"
                                            name="penyesuaian_nilai_sewa" disabled>
                                            <option value="">Pilih</option>
                                            <option value="1"
                                                {{ $naup->penyesuaian_nilai_sewa == '1' ? 'selected' : '' }}>Review
                                                Ujrah Dilakukan Pada Setiap Tanggal 01 - 05, Periode November Dan Mei
                                                Pada Tahun Berjalan</option>
                                            <option value="2"
                                                {{ $naup->penyesuaian_nilai_sewa == '2' ? 'selected' : '' }}>
                                                Penyesuaian Ujrah Pada Sistem Dilakukan Pada Tanggal 01 - 05 Periode
                                                Desember
                                                Dan Juni Tahun Berjalan</option>
                                            <option value="3"
                                                {{ $naup->penyesuaian_nilai_sewa == '3' ? 'selected' : '' }}>
                                                Penyesuaian Ujrah Berlaku Efektif Pada Setiap Tanggal 01 Periode Januari Dan
                                                Juli Tahun Berjalan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        @endif
                        <!-- end Ijaroh-->

                        <!-- end fasilitas pendanaan -->
                        <!-- objek pendanaan -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_objek_pendanaan">III. OBJEK
                                    PENDANAAN &nbsp</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-no_sertifikat">Jenis Objek Pendanaan</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jenis_objek_pendanaan"
                                        name="jenis_objek_pendanaan" value="{{ $pengajuan->jenis_objek_pendanaan }}"
                                        readonly>

                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-no_sertifikat">Status/Jenis Sertifikat</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="status_jenis_sertifikat"
                                        name="status_jenis_sertifikat"
                                        value="{{ $pengajuan->jenis_agunan == '1' ? 'SHM' : 'HGB' }}" readonly>

                                </div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-no_sertifikat">No Sertifikat</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="no_sertifikat" name="no_sertifikat"
                                        value="{{ $pengajuan->nomor_agunan }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tanggal_berakhir_hak">Tanggal Berakhir Hak</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tanggal_berakhir_hak"
                                        name="tanggal_berakhir_hak" value="{{ $pengajuan->tanggal_jatuh_tempo }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-sertifikat_atas_nama">Sertifikat Atas Nama</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="sertifikat_atas_nama"
                                        name="sertifikat_atas_nama" value="{{ $pengajuan->atas_nama_agunan }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-alamat_sertifikat">Alamat Sertifikat</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <textarea class="form-control" cols="30" rows="5"
                                        readonly>{{ $agunan->alamat_sertifikat }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-no_izin_mendirikan_bangunan">No Izin Mendirikan Bangunan
                                        (IMB)</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="no_izin_mendirikan_bangunan"
                                        name="no_izin_mendirikan_bangunan" readonly value="{{ $pengajuan->no_imb }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-imb_atas_nama">IMB Atas Nama</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="imb_atas_nama" name="imb_atas_nama"
                                        value="{{ $naup->imbt_atas_nama }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-luas_tanah">Luas Tanah m2</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="luas_tanah" name="luas_tanah"
                                        value="{{ $agunan->luas_tanah }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-luas_bangunan">Luas Bangunan m2</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="luas_bangunan" name="luas_bangunan"
                                        value="{{ $agunan->luas_bangunan }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tanggal_penilaian_agunan">Tanggal Penilaian Agunan</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <input class="form-control" type="date" id="tanggal_penilaian_agunan"
                                        name="tanggal_penilaian_agunan" readonly
                                        value="{{ $naup->tanggal_penilaian_agunan }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-appraisal">Appraisal</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" id="appraisal" name="appraisal" disabled>
                                        <option value="">Pilih</option>
                                        <option value="1" {{ $naup->appraisal == '1' ? 'selected' : '' }}>Internal
                                        </option>
                                        <option value="2" {{ $naup->appraisal == '2' ? 'selected' : '' }}>External
                                            (KJPP)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nilai_pasar_wajar">Nilai Pasar Wajar</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                                    </div>
                                    <input class="form-control" type="text" id="nilai_pasar_wajar"
                                        name="nilai_pasar_wajar"
                                        value="{{ number_format($pengajuan->nilai_pasar_wajar, 0, '.', '.') }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nilai_likuidasi">Nilai Likuidasi</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                                    </div>
                                    <input class="form-control" type="text" id="nilai_likuidasi"
                                        value="{{ number_format($pengajuan->nilai_likuidasi, 0, '.', '.') }}"
                                        name="nilai_likuidasi" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-rekomendasi_appraisal">Rekomendasi Appraisal</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" id="rekomendasi_appraisal" name="rekomendasi_appraisal"
                                        disabled>
                                        <option value="">Pilih</option>
                                        <option value="1" {{ $naup->rekomendasi_appraisal == '1' ? 'selected' : '' }}>
                                            Direkomendasikan</option>
                                        <option value="2" {{ $naup->rekomendasi_appraisal == '2' ? 'selected' : '' }}>
                                            Tidak Direkomendasikan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jenis_pengikat_agunan">Jenis Pengikat Agunan</label>
                                </div>
                            </div>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <select class="form-control" id="jenis_pengikat_agunan" name="jenis_pengikat_agunan"
                                        disabled>
                                        @foreach ($pengikatan_jaminan as $key => $val)
                                            <option value="{{ $key }}"
                                                {{ $key == $naup->jenis_pengikatan_agunan ? 'selected' : '' }}>
                                                {{ $val }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nilai_pengikat_agunan">Nilai Pengikat Agunan</label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp.</span>
                                    </div>
                                    <input class="form-control" type="text" id="nilai_pengikat_agunan"
                                        name="nilai_pengikat_agunan"
                                        value="{{ number_format($naup->nilai_pengikatan_agunan, 0, '.', '.') }}"
                                        readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-4">
                                    <input class="form-control" type="text" id="plafon_pembiayaan"
                                        name="plafon_pembiayaan" value="{{ $persentase_plafon_pembiayaan }}" readonly>
                                </div>
                                <div class="col-md-8">
                                    % Plafon Pembiayaan
                                </div>
                            </div>
                        </div>
                        <!-- end objek pendanaan -->
                        <!-- biaya -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_biaya">IV. BIAYA - BIAYA
                                    &nbsp</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-md-12">
                                <table id="pengajuKPR_data" class="table table-striped table-bordered table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th class="col-1">No</th>
                                            <th class="col-8">Rincian Biaya</th>
                                            <th class="col-3">Jumlah</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($detail_biaya as $key => $val)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $val->rincian_biaya }}</td>
                                                <td>
                                                    <input class="form-control text-right" type="text" id="jumlah"
                                                        name="jumlah"
                                                        value="Rp {{ number_format($val->jumlah, 0, '.', '.') }}"
                                                        readonly>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end biaya -->

                        <!-- start covenant persetujuan pembiayaan -->
                        <form method="POST" id="form_deviasi" name="form_deviasi"
                            action="{{ route('detail-pekerjaan-risk-update') }}">
                            @csrf
                            <input type="hidden" name="pengajuan_id" value="{{ $pengajuan->pengajuan_id }}" />
                            <div class="col-12 mb-4">
                                <br>
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6" for="form_biaya">V. DEVIASI PENDANAAN
                                        &nbsp</label>
                                </div>
                            </div>
                            <div class="col-md-12">

                                <div class="col-md-12">



                                    <table id="jenisDeviasiData"
                                        class="table table-striped table-bordered table-responsive-sm">
                                        <thead>
                                            <tr>
                                                <th class="col-1">No</th>
                                                <th class="col-5">Jenis Deviasi</th>
                                                <th class="col-3">Resiko</th>
                                                <th class="col-3">Mitigasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($detail_deviasi as $key => $val)
                                                <tr>
                                                    <td class="col-1">{{ $loop->iteration }}</td>
                                                    <td class="col-5">{{ $val->jenis_deviasi }}</td>
                                                    <td class="col-3">
                                                        <input class="form-control" type="text" id="resiko"
                                                            name="deviasi[{{ $loop->iteration }}][resiko]"
                                                            value="{{ $val->resiko }}" placeholder="ketik disini">
                                                        <input class="form-control" type="hidden" id="id"
                                                            name="deviasi[{{ $loop->iteration }}][id]"
                                                            value="{{ $val->id }}">
                                                    </td>
                                                    <td class="col-3">
                                                        <input class="form-control" type="text" id="mitigasi"
                                                            name="deviasi[{{ $loop->iteration }}][mitigasi]"
                                                            value="{{ $val->mitigasi }}" placeholder="ketik disini">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>

                            <!-- end covenant persetujuan pembiayaan -->
                            <!-- button kembali / simpan -->
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                @php $count_deviasi = count($detail_deviasi) @endphp
                                @if ($count_deviasi > 0)
                                    <div class="col-md-4 btn_kembali text-right">
                                        <a href="{{ route('risk.listproyek') }}" class="btn btn-block btn-secondary"
                                            id="kembali"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                                    </div>

                                    <div class="col-md-4 btn_kirim">
                                        <button type="button" class="btn btn-block btn-primary" id="btnKonfirmasiKirim"><i
                                                class="fa fa-paper-plane" aria-hidden="true"></i> Kirim</button>
                                        <button type="submit" id="btnKirimFinal" class="invisible"></button>
                                    </div>

                                    <div class="col-md-4 btn_simpan">
                                        <button type="button" class="btn btn-block btn-success" id="btnSimpan"><i
                                                class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                                    </div>
                                @else
                                    <div class="col-md-6 btn_kembali text-right">
                                        <a href="{{ route('risk.listproyek') }}" class="btn btn-block btn-secondary"
                                            id="kembali"> <i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                                    </div>

                                    <div class="col-md-6 btn_kirim">
                                        <button type="button" class="btn btn-block btn-primary" id="btnKonfirmasiKirim"><i
                                                class="fa fa-paper-plane" aria-hidden="true"></i> Kirim</button>
                                        <button type="submit" id="btnKirimFinal" class="invisible"></button>
                                    </div>

                                @endif


                            </div>


                            <!-- end button -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div><!-- .content -->
@endsection

@section('js')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {

            let status_risk = "{{ $status_risk }}";
            let count_deviasi = "{{ $count_deviasi }}";
            let status_kirim = ["10", "12"];
            const skema_pembiayaan  =  "{{ $pengajuan->skema_pembiayaan }}";

            if(count_deviasi > 0) {
                if (status_kirim.includes(status_risk)) {
                    $('#btnKonfirmasiKirim').attr('disabled', 'disabled');
                } else {
                    if (status_risk == "12") {
                        $('#btnSimpan').attr('disabled', 'disabled');
                        $('#btnKonfirmasiKirim').attr('disabled', 'disabled');
                    }
                }
            }

        
            if(skema_pembiayaan == '1') {
                $('.div_pasangan').addClass('d-none');
            }else{
                $('.div_pasangan').removeClass('d-none');
            }
          

            $('#btnSimpan').click(() => {
                submitData('#form_deviasi')
            });


            $('#btnKonfirmasiKirim').click(function(e) {
                Swal.fire({
                    title: "Notifikasi",
                    text: "Apakah anda yakin ingin mengirim data ini ?",
                    type: "warning",
                    buttons: true,
                    showCancelButton: true,
                }).then(result => {
                    if (result.value == true) {
                        $('#btnKirimFinal').trigger('click');
                    } else {
                        return false;
                    }
                });
            });



            $('#btnKirimFinal').click(function(e) {
                e.preventDefault();

                $.get("/admin/adr/deviasi/cek_naup/"+"{{ $pengajuan->pengajuan_id }}", function( data ) {
                    if (data == "both_exits") {

                        $.ajax({
                                url: "{{ route('adr.kirim.deviasi') }}",
                                method: "post",
                                data: {
                                    pengajuan_id: "{{ $pengajuan->pengajuan_id }}"
                                },
                                beforeSend: () => {
                                    Swal.fire({
                                        html: '<h5>Mengirim Data ...</h5>',
                                        showConfirmButton: false,
                                        allowOutsideClick: () => false,
                                        onBeforeOpen: () => {
                                            swal.showLoading();
                                        }
                                    });
                                },
                                error: function(xhr, status, error) {
                                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                                    if (xhr.status === 419) {
                                        window.alert('Halaman Session Expired. Mohon login ulang');
                                    }

                                },
                                success: function(result) {
                                    if (result == '1') {
                                        location.href = "/admin/adr/deviasi"
                                    } else {
                                        return false;
                                    }

                                }

                        });
                    } else {

                        var msgBox = '';
                        if(data == 'naup_not_exists'){
                            msgBox = 'Maaf data tidak dapat dikirim. Belum ada data NAUP pada dokumen legal';
                        }else if(data == 'biaya_not_exists') {
                            msgBox = 'Maaf data tidak dapat dikirim. Belum ada data Biaya-biaya pada dokumen legal';
                        }else{
                            msgBox = 'Maaf data tidak dapat dikirim. Belum ada data NAUP dan Biaya-biaya pada dokumen legal';
                        }

                       
                        Swal.fire({
                            title: "Notifikasi",
                            text: msgBox,
                            type: "error",
                            buttons: true,
                            showCancelButton: false,
                        }).then(result => {
                            return false;
                        });
                    
                    }
                });



            });



        });
        const submitData = (element_id) => {
            let form = $(element_id);
            let formData = new FormData($(element_id)[0]);
            $.ajax({
                url: $(element_id).attr('action'),
                type: $(element_id).attr("method"),
                dataType: 'JSON',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    swal.fire({
                        html: '<h5>Menyimpan Data...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(response) {

                    if (response.status == 'success') {
                        swal.fire({
                            title: 'Berhasil',
                            type: 'success',
                            text: response.msg,
                            allowOutsideClick: () => false,
                        }).then(function(result) {
                            window.location = "{{ route('risk.listproyek') }}"
                        })
                    } else {
                        swal.fire({
                            title: 'Oops',
                            type: 'error',
                            text: "Internal Error"
                        })
                    }
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    if (xhr.status === 419) {
                        errorMessage = 'Halaman Session Expired. Mohon login ulang';
                    }
                    swal.fire({
                        title: 'Error',
                        type: 'error',
                        text: errorMessage
                    })
                    console.log(response)
                }
            })
        }
    </script>

@endsection
