@extends('layouts.admin.master')

@section('title', 'Panel Admin Compliance')

@section('content')

<style>
    .largerCheckbox {
        width: 15px;
        height: 15px;
    }

    .modal-xl {
        max-width: 95% !important;
    }
</style>

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Resume Pendanaan  ({{$cekUser->name}})</h1>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            
            <div class="card">
                <div class="card-body">
                    <!-- START: informasi compliance -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_informasi_legalitas">Informasi Resume &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_compliance">Nama  {{$cekUser->name}}</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="hidden" id="nama_compliance" name="nama_compliance" value="{{auth::user()->firstname}} {{auth::user()->lastname}}" readonly>
                                {{auth::user()->firstname}} {{auth::user()->lastname}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_keputusan">Tanggal Keputusan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="hidden" id="tanggal_keputusan" name="tanggal_keputusan" value="{{date_format(now(),'Y-m-d')}}" readonly>{{Carbon\Carbon::now()->formatLocalized('%d %B %Y')}}
                            </div>
                        </div>
                    </div>
                    @foreach($pengajuan as $pengajuan)
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-ipdipp">Id Penerima Dana |<br> Id Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                            {{$pengajuan->brw_id}} &nbsp;|&nbsp; {{$pengajuan_id = $pengajuan->pengajuan_id}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_penerima_dana">Nama Penerima Dana</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{$pengajuan->nama}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-plafon_pengajuan_pendanaan">Plafon Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                @php
                                    $plafon_pengajuan = $pengajuan->pendanaan_dana_dibutuhkan;
                                @endphp
                                Rp{{ number_format($plafon_pengajuan,2,',','.')}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-fasilitas_pendanaan">Fasilitas Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{$pendanaan_tujuan=$pengajuan->tujuan_pembiayaan}}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_pendanaan">Jangka Waktu Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{$pengajuan->durasi_proyek}}&nbsp;Bulan
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kategori_penerima_pendanaan">Kategori Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                {{$pengajuan->sumber_pengembalian_dana = 1? 'Fixed Income (Penghasilan Tetap)':'Non-Fixed Income (Penghasilan Tidak Tetap)'}}
                            </div>
                        </div>
                    </div>
                    <!-- end informasi compliance -->
                    <!-- data penerima Pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_resume_fasilitas_pendanaan">Resume Fasilitas Pendanaan &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    @endforeach
                    @if ($naup)
                        @foreach($naup as $naup)
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="wizard-progress2-nomor_naup">Nomor NAUP</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-5">
                            <div class="form-group">
                                @if (isset($naup->no_naup))
                                    {{$naup->no_naup}}
                                @else
                                    <input class="form-control" type="text" id="nomor_naup" name="nomor_naup" maxlength="4" style="width: 75px;float:left">
                                    <span class="form-control"  style="width: 150px;float:left">/DR/NAUP-DSI/</span>
                                    <input class="form-control" type="text" id="bulan_naup" name="bulan_naup" maxlength="2" style="width: 50px;float:left">
                                    <span class="form-control"  style="width: 50px;float:left">/20</span>
                                    <input class="form-control" type="text" id="tahun_naup" name="tahun_naup" maxlength="2" style="width: 50px;float:left">
                                @endif
                            </div>
                        </div>
                    </div>
                        @endforeach
                    @else
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="wizard-progress2-nomor_naup">Data NAUP Belum tersedia</label>
                            </div>
                        </div>
                    </div>
                    @endif
                    @foreach ($pengajuan1 as $pengajuan)
                    @php
                    if (is_null($pengajuan1)){
                        $pendanaan_tujuan = $pengajuan->tujuan_pembiayaan;
                        $nama_pemilik = $pengajuan->nm_pemilik;
                    }else{
                        $pendanaan_tujuan ='N/A';
                        $nama_pemilik = '';
                        $pendanaan_tujuan = $pengajuan->tujuan_pembiayaan;
                        $nama_pemilik = $pengajuan->nm_pemilik;
                    }
                    @endphp
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_resume_fasilitas_pendanaan">I. DATA PENERIMA PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-skema_pengajuan">Skema Pengajuan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="skema_pengajuan" name="skema_pengajuan" value="{{$pengajuan->skema_pembiayaan == '1'? 'Single Income (Penghasilan Sendiri)' : 'Joint Income (Penghasilan Bersama)'}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_penerima_pendanaan">Nama Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_penerima_pendanaan" name="nama_penerima_pendanaan" value="{{$pengajuan->nama}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-plafon_pasangan">Nama Pasangan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_pasangan" name="nama_pasangan" value="{{$pengajuan->nama_pasangan}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-usia_saat_pengajuan">Usia Saat Pengajuan (Tahun)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                @php
                                $now = date('Y-m-d H:i:s');
                                $date1=date_create($pengajuan->tgl_lahir);
                                $date2=date_create($now);
                                $diffU = (array) date_diff($date1, $date2);
                                $diffU['y'];
                                @endphp
                                <input class="form-control" type="text" id="usia_saat_pengajuan" name="usia_saat_pengajuan" value="{{$diffU['y']}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-usia_pasangan">Usia Pasangan (Tahun)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                @php
                                $now = date('Y-m-d H:i:s');
                                $date1=date_create($pengajuan->tgl_lahir_pasangan);
                                $date2=date_create($now);
                                $diffP = (array) date_diff($date1, $date2);
                                $diffP['y'];
                                @endphp
                                <input class="form-control" type="text" id="usia_pasangan" name="usia_pasangan" value="{{$diffP['y']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-status_karyawan">Status Karyawan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                @php
                                if ($pengajuan->status_pekerjaan==1){
                                $st = 'Karyawan Kontrak';
                                }elseif ($pengajuan->status_pekerjaan==2){
                                $st = 'Karyawan Tetap';
                                }else{
                                $st = 'No data';
                                }
                                @endphp
                                <input class="form-control" type="text" id="status_karyawan" name="status_karyawan" value="{{$st}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-status_karyawan_pasangan">Status Karyawan Pasangan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                @php
                                if ($pengajuan->status_pekerjaan_pasangan==1){
                                $stp = 'Karyawan kontrak';
                                }elseif ($pengajuan->status_pekerjaan_pasangan==2){
                                $stp = 'Karyawan Tetap';
                                }else{
                                $stp = 'No data';
                                }
                                @endphp
                                <input class="form-control" type="text" id="status_karyawan_pasangan" name="status_karyawan_pasangan" value="{{$stp}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jabatan">Jabatan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jabatan" name="jabatan" value="{{$pengajuan->jabatan}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jabatan_pasangan">Jabatan Pasangan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jabatan_pasangan" name="jabatan_pasangan" value="{{$pengajuan->jabatan_pasangan}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-lama_bekerja">Lama Bekerja (Tahun)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                @php
                                    $TahunKerja=date("Y", strtotime("-".(($pengajuan->masa_kerja_tahun*12)+($pengajuan->masa_kerja_bulan))." months"));
                                @endphp
                                
                                <input class="form-control" type="text" id="lama_bekerja" name="lama_bekerja" value="{{$TahunKerja}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tempat_bekerja_pasangan">Tempat Bekerja Pasangan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tempat_bekerja_pasangan" name="tempat_bekerja_pasangan" value="{{$pengajuan->nama_perusahaan}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-lama_perusahaan_berdiri">Lama Perusahaan Berdiri (Tahun)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                @if ($pengajuan->usia_perusahaan > 0)
                                <input class="form-control" type="text" id="lama_perusahaan_berdiri" name="lama_perusahaan_berdiri" value="{{$pengajuan->usia_perusahaan}}" readonly>
                                @else
                                <input class="form-control" type="text" id="lama_perusahaan_berdiri" name="lama_perusahaan_berdiri" value="{{$pengajuan->usia_tempat_usaha}}" readonly>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-cash_ratio">Cash Ratio</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="cash_ratio" name="cash_ratio" value="{{$pengajuan->cr_maks}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-maks_ftv_persen">Maks FTV (%)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="maks_ftv_persen" name="maks_ftv_persen" value="{{ Number_format($pengajuan->maks_ftv_persentase,2,',','.')}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-maks_ftv_rupiah">Maks FTV (Rp)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="maks_ftv_rupiah" name="maks_ftv_rupiah" value="Rp{{ Number_format($pengajuan->maks_ftv_nominal,2,',','.')}}" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end data penerima pendanaan -->
                    @endforeach
                    <!-- fasilitas pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_fasilitas_pendanaan">II. FASILITAS PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <!-- start wakalah -->
                    @if(isset($naup1) && (count($naup1)>=1))
                    @foreach ($naup1 as $naup)
                    <div class="col-12 mb-4">
                    <div class="col-md-2">
                            <div class="form-group">
                                <label for="form-check-label text-black h6">Struktur Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="form-check-label text-black h6" for="form_wakalah">Wakalah</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_produk">Nama Produk</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_produk" name="nama_produk" value="{{$pendanaan_tujuan}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu">Jangka Waktu (Hari)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu" name="jangka_waktu" value="{{$naup->jangka_waktu_wakalah}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tujuan_penggunaan">Tujuan Penggunaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tujuan_penggunaan" name="tujuan_penggunaan" value="{{$naup->tujuan_penggunaan_wakalah}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-sumber_pelunasan">Sumber Pelunasan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="sumber_pelunasan" name="sumber_pelunasan" value="Pencairan Fasilitas Pendanaan {{$pendanaan_tujuan}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pembelian_dari">Pembelian dari</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="pembelian_dari" name="pembelian_dari" value="{{$nama_pemilik}}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_pendanaan">Pengikatan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="pengikatan_pendanaan" name="pengikatan_pendanaan" value="{{$naup->pengikatan_pendanaan_qardh}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jumlah_wakalah">Jumlah Wakalah</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jumlah_wakalah" name="jumlah_wakalah" value="Rp{{ number_format($naup->jumlah_wakalah,2,',','.') }}" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_jaminan">Pengikatan Jaminan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="pengikatan_jaminan" name="pengikatan_jaminan" value="{{$naup->pengikatan_jaminan_qardh}}" readonly>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_wakalah">-</label>
                        </div>
                    </div>
                    @endif
                    <!-- end wakalah -->
                    <!-- end fasilitas pendanaan -->
                    <!-- objek pendanaan -->
                    @foreach ($agunan as $agunan)
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_objek_pendanaan">III. OBJEK PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jenis_objek_pendanaan">Jenis Objek Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jenis_objek_pendanaan" name="jenis_objek_pendanaan" value="{{$agunan->jenis_objek_pendanaan==1?'Tanah kosong':'Tanah dan Bangunan'}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-status_jenis_sertifikat">Status/Jenis Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="status_jenis_sertifikat" name="status_jenis_sertifikat" value="{{$agunan->jenis_agunan=1?'HGB':'SHM'}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-no_sertifikat">No Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="no_sertifikat" name="no_sertifikat" value="{{$agunan->nomor_agunan}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_berakhir_hak">Tanggal Berakhir Hak</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="date" id="tanggal_berakhir_hak" name="tanggal_berakhir_hak" value="{{$agunan->tanggal_jatuh_tempo}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-sertifikat_atas_nama">Sertifikat Atas Nama</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="sertifikat_atas_nama" name="sertifikat_atas_nama" value="{{$agunan->atas_nama_agunan}}"  readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-alamat_sertifikat">Alamat Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <textarea class="form-control" id="alamat_sertifikat" name="alamat_sertifikat" rows="5"  readonly>{{$agunan->lokasi_proyek}}, {{$agunan->blok_nomor}}, RT {{$agunan->RT}} / RW {{$agunan->RW}}, {{$agunan->kelurahan}}, {{$agunan->kecamatan}}, {{$agunan->kota}}, {{$agunan->provinsi}}, Kode pos {{$agunan->kode_pos}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-no_izin_mendirikan_bangunan">No Izin Mendirikan Bangunan (IMB)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="no_izin_mendirikan_bangunan" name="no_izin_mendirikan_bangunan" value="{{$agunan->no_imb}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-imb_atas_nama">IMB Atas Nama</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="imb_atas_nama" name="imb_atas_nama" value="{{$agunan->atas_nama_imb}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-luas_tanah">Luas Tanah (M<sup>2</sup>)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="luas_tanah" name="luas_tanah" value="{{$agunan->luas_tanah_imb}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-luas_bangunan">Luas Bangunan (M<sup>2</sup>)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="luas_bangunan" name="luas_bangunan" value="{{$agunan->luas_bangunan_imb}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_penilaian_agunan">Tanggal Penilaian Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="date" id="tanggal_penilaian_agunan" name="tanggal_penilaian_agunan" value="{{$agunan->tanggal_penilaian_agunan}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-appraisal">Appraisal</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" id="appraisal" name="appraisal" value="{{$agunan->appraisal == '1'? 'Internal':'External'}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_pasar_wajar">Nilai Pasar Wajar</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_pasar_wajar" name="nilai_pasar_wajar" value="Rp{{ number_format($agunan->nilai_pasar_wajar,2,',','.') }}"  readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_likuidasi">Nilai Likuidasi</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_likuidasi" name="nilai_likuidasi" value="Rp{{ number_format($agunan->nilai_likuidasi,2,',','.') }}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-rekomendasi_appraisal">Rekomendasi Appraisal</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" id="rekomendasi_appraisal" name="rekomendasi_appraisal" value="{{($agunan->rekomendasi_appraisal == '1'? 'Direkomendasikan':'Tidak Direkomendasikan')}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jenis_pengikat_agunan">Jenis Pengikat Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" id="jenis_pengikat_agunan" name="jenis_pengikat_agunan" value="{{$agunan->jenis_pengikatan}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_pengikat_agunan">Nilai Pengikat Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="nilai_pengikat_agunan" name="nilai_pengikat_agunan" value="Rp{{number_format($agunan->nilai_pengikatan_agunan,2,',','.')}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <input class="form-control" type="number" id="plafon_pembiayaan" name="plafon_pembiayaan" value="{{$agunan->plafon_pembiayaan}}" readonly>
                        </div>
                        <div class="col-md-3">
                            % Plafon Pembiayaan
                        </div>
                    </div>
                    @endforeach
                    <!-- end objek pendanaan -->
                    <!-- biaya -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_biaya">IV. BIAYA - BIAYA &nbsp</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-md-12">
                            <table id="pengajuKPR_data" class="table table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th class="col-1">No</th>
                                        <th class="col-8">Rincian Biaya</th>
                                        <th class="col-3">Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $j=0;
                                    @endphp
                                    @foreach ($biaya as $biaya)
                                    @php
                                        $j++;
                                    @endphp
                                    <tr>
                                        <td>{{$j}}</td>
                                        <td>{{$biaya->rincian_biaya}}</td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_administrasi" name="jumlah_biaya_administrasi" value="Rp{{number_format($biaya->jumlah,2,'.',',')}}" style="direction: rtl;" readonly>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end biaya -->
                    <!-- start covenant persetujuan pembiayaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_biaya">V. COVENANT PERSETUJUAN PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        @php
                            if ($cekUser->name == 'Analis' && $data['status_analis'] >= 6){$RO = '';$disabled='';$hidden = 'inline';$keputusan_kredit_analis =date_format(now(),'Y-m-d');}else{$RO = 'readonly';$disabled='disabled';$hidden = "none";$keputusan_kredit_analis = $data['tgl_keputusan_kredit_analis'];}
                            if ($data['status_pendanaan'] == 15){$RO = 'readonly';$disabled='disabled';$hidden = "none";}
                        @endphp
                        <!-- analis -->
                        <div class="card analis">
                            <div class="card-header h6">KREDIT ANALIS</div>
                            <div class="card-body">
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        Nama User :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" id="nama_kredit_analis" name="nama_kredit_analis" placeholder="Nama Kredit Analis" value="{{$data['nama_kredit_analis']}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Tanggal Keputusan :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="date" id="tanggal_keputusan_kredit_analis" name="tanggal_keputusan_kredit_analis" value="{{$keputusan_kredit_analis}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Rekomendasi :
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="rekomendasi_kredit_analis" id="rekomendasi_kredit_analis" {{$disabled}}>
                                            <option value="0" {{($data['rekomendasi_kredit_analis'] == 0 ? 'selected':'')}}>Tidak Setuju</option>
                                            <option value="1" {{($data['rekomendasi_kredit_analis'] == 1 ? 'selected':'')}}>Setuju</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="catatan_kredit_analis" id="catatan_kredit_analis" placeholder ="Catatan/Usulan Analis Kredit" cols="30" rows="5" {{$RO}}>{{$data['catatan_kredit_analis']}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">                                        
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="hidden" id="tanggal_buat_kredit_analis" name="tanggal_buat_kredit_analis" value="{{$data['created_at_kredit_analis']}}" {{$RO}}>
                                        <input class="form-control" type="hidden" id="tanggal_pembaruan_kredit_analis" name="tanggal_pembaruan_kredit_analis" value="{{now()}}" {{$RO}}>
                                    </div>
                                </div>
                                @if ($cekUser->name == 'Analis' && $data['status_pendanaan'] != 15)
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button class="btn btn-warning btn-lg" id="btn_catatan" value="_kredit_analis" type="submit">Simpan</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <!-- compliance -->
                        @php
                            if ($cekUser->name == 'Compliance' && $data['status_pendanaan'] >= 13){$RO = '';$disabled='';$keputusan_compliance =date_format(now(),'Y-m-d');}else{$RO = 'readonly';$disabled='disabled';$keputusan_compliance = $data['tgl_keputusan_compliance'];}
                            if ($data['status_pendanaan'] == 15){$RO = 'readonly';$disabled='disabled';$hidden = "none";}
                        @endphp
                        <div class="card compliance">
                            <div class="card-header h6">COMPLIANCE</div>
                            <div class="card-body">
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        Nama User :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" id="nama_compliance" name="nama_compliance" placeholder="Nama compliance" value="{{$data['nama_compliance']}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Tanggal Keputusan :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="date" id="tanggal_keputusan_compliance" name="tanggal_keputusan_compliance" value="{{$keputusan_compliance}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Rekomendasi :
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="rekomendasi_compliance" id="rekomendasi_compliance" {{$disabled}}>
                                            <option value="0" {{($data['rekomendasi_compliance'] == 0 ? 'selected':'')}}>Tidak Setuju</option>
                                            <option value="1" {{($data['rekomendasi_compliance'] == 1 ? 'selected':'')}}>Setuju</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="catatan_compliance" id="catatan_compliance" placeholder ="Catatan/Usulan compliance" cols="30" rows="5" {{$RO}}>{{$data['catatan_compliance']}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="hidden" id="tanggal_buat_compliance" name="tanggal_buat_compliance" value="{{$data['created_at_compliance']}}" {{$RO}}>
                                        <input class="form-control" type="hidden" id="tanggal_pembaruan_compliance" name="tanggal_pembaruan_compliance" value="{{now()}}" {{$RO}}>
                                    </div>
                                </div>
                                @if ($cekUser->name == 'Compliance' && $data['status_pendanaan'] != 15)
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button class="btn btn-warning btn-lg" id="btn_catatan" value="_compliance" type="submit">Simpan</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        @php
                        if ($cekUser->name == 'Risk' && $data['status_risk'] >= 12){$RO = '';$disabled='';$hidden = 'inline';$keputusan_risk_management =date_format(now(),'Y-m-d');}else{$RO = 'readonly';$disabled='disabled';$hidden = "none";$keputusan_risk_management = $data['tgl_keputusan_risk_management'];}
                            if ($data['status_pendanaan'] == 15){$RO = 'readonly';$disabled='disabled';$hidden = "none";}
                        @endphp
                        <div class="card ris_management">
                            <div class="card-header h6">RISK MANAGEMENT</div>
                            <div class="card-body">
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        Nama User :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" id="nama_risk_management" name="nama_risk_management" placeholder="Nama Risk Management" value="{{$data['nama_risk_management']}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Tanggal Keputusan :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="date" id="tanggal_keputusan_risk_management" name="tanggal_keputusan_risk_management" value="{{$keputusan_risk_management}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Rekomendasi :
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="rekomendasi_risk_management" id="rekomendasi_risk_management" {{$disabled}}>
                                            <option value="0" {{($data['rekomendasi_risk_management'] == 0 ? 'selected':'')}}>Tidak Setuju</option>
                                            <option value="1" {{($data['rekomendasi_risk_management'] == 1 ? 'selected':'')}}>Setuju</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="catatan_risk_management" id="catatan_risk_management" placeholder ="Catatan/Usulan Risk Management" cols="30" rows="5" {{$RO}}>{{$data['catatan_risk_management']}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="hidden" id="tanggal_buat_risk_management" name="tanggal_buat_risk_management" value="{{$data['created_at_risk_management']}}" {{$RO}}>
                                        <input class="form-control" type="hidden" id="tanggal_pembaruan_risk_management" name="tanggal_pembaruan_risk_management" value="{{now()}}" {{$RO}}>
                                    </div>
                                </div>
                                @if ($cekUser->name == 'Risk' && $data['status_pendanaan'] != 15)
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button class="btn btn-warning btn-lg" id="btn_catatan" value="_risk_management"  type="submit">Simpan</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        @php
                        if ($cekUser->name == 'Legal' && $data['status_legal'] >= 9){$RO = '';$disabled='';$hidden = 'inline';$keputusan_legal =date_format(now(),'Y-m-d');}else{$RO = 'readonly';$disabled='disabled';$hidden = "none";$keputusan_legal = $data['tgl_keputusan_legal'];}
                            if ($data['status_pendanaan'] == 15){$RO = 'readonly';$disabled='disabled';$hidden = "none";}
                        @endphp
                        <div class="card legal">
                            <div class="card-header h6">LEGAL</div>
                            <div class="card-body">
                                <div class="col-md-6">
                                    <div class="col-md-6">
                                        Nama User :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="text" id="nama_legal" name="nama_legal" placeholder="Nama Legal" value="{{$data['nama_legal']}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Tanggal Keputusan :
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="date" id="tanggal_keputusan_legal" name="tanggal_keputusan_legal" value="{{$keputusan_legal}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        Rekomendasi :
                                    </div>
                                    <div class="col-md-6">
                                        <select class="form-control" name="rekomendasi_legal" id="rekomendasi_legal" {{$disabled}}>
                                            <option value="0" {{$data['rekomendasi_legal'] == 0 ? 'selected':''}}>Tidak Setuju</option>
                                            <option value="1" {{$data['rekomendasi_legal'] == 1 ? 'selected':''}}>Setuju</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <textarea class="form-control" name="catatan_legal" id="catatan_legal" placeholder ="Catatan/Usulan legal" cols="30" rows="5" {{$RO}}>{{$data['catatan_legal']}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <div class="col-md-6">
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="hidden" id="tanggal_buat_legal" name="tanggal_buat_legal" value="{{$data['created_at_legal']}}" {{$RO}}>
                                        <input class="form-control" type="hidden" id="tanggal_pembaruan_legal" name="tanggal_pembaruan_legal" value="{{now()}}" {{$RO}}>
                                    </div>
                                </div>
                                @if ($cekUser->name == 'Legal' && $data['status_pendanaan'] != 15)
                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button class="btn btn-warning btn-lg" id="btn_catatan" value="_legal"   type="submit">Simpan</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- end covenant persetujuan pembiayaan -->
                    <!-- button kembali / simpan -->
                    @if ($cekUser->name == 'Compliance' && $data['status_pendanaan'] != 15 && ($data['catatan_kredit_analis'] && $data['catatan_compliance'] && $data['catatan_risk_management'] && $data['catatan_legal']))
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <a href="{{ route('compliance.listproyek') }}"><button type="button" class="btn btn-block btn-success btn-lg" id="kembali">Kembali</button></a>
                    </div>
                    <div class="col-md-2">
                        <input type="hidden" value="{{auth::user()->firstname.' '.auth::user()->lastname}}" id="user">
                        <button type="button" class="btn btn-block btn-success btn-lg" id="Kirim" value="{{$pengajuan_id}}">Kirim</button>
                    </div>
                    @else
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-10">
                    </div>
                    <div class="col-md-2">
                        <a href="{{ route('compliance.listproyek') }}"><button type="button" class="btn btn-block btn-success btn-lg" id="kembali">Kembali</button></a>
                    </div>
                    @endif
                    <!-- end button -->
                </div>
            </div>
        </div>
    </div>
</div>
</div><!-- .content -->

<!-- 2. GOOGLE JQUERY JS v3.2.1  JS !-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>

<script type="text/javascript">
    $(document).ready( function(){
        var token = "{{ csrf_token() }}";
        $('#btn_catatan').on('click',function(){
            var role = $('#btn_catatan').val();
            var nama_user = $('#nama'+role).val();
            var tanggal_keputusan = $('#tanggal_keputusan'+role).val();
            var rekomendasi = $('#rekomendasi'+role).val();
            var catatan = $('#catatan'+role).val()
            if (catatan == '') {
                Swal.fire({
                    title: "Notifikasi",
                    text: "Catatan Belum Dilengkapi",
                    type: "warning"
                })
                return false
            };
            var tanggal_buat = $('#tanggal_buat'+role).val();
            var tanggal_pembaruan = $('#tanggal_pembaruan'+role).val();
            var pengajuan_id = {{$pengajuan_id}};
            
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/adr/updateresume",
                method: "POST",
                dataType: 'JSON',
                data: {
                    'nama_user': nama_user,
                    'tanggal_keputusan': tanggal_keputusan,
                    'rekomendasi': rekomendasi,
                    'catatan': catatan,
                    'tanggal_buat': tanggal_buat,
                    'tanggal_pembaruan': tanggal_pembaruan,
                    'pengajuan_id': pengajuan_id,
                    'role': role
                },
                error: function(xhr, status, error) {
                    if (xhr.status === 419) {
                        window.alert(
                            "Page expired. please refresh the page",
                            "danger");
                    }
                },
                success: function(msg) {
                    //if (msg.data == 1) {
                    if (msg == 1) {
                        Swal.fire({
                            closeOnClickOutside: false,
                            title: "Notifikasi",
                            text: "Data Keputusan Berhasil Disimpan",
                            type: "success"
                        }).then(function (result) {
                            if (result.value) {
                                window.location = "/admin/adr/resumependanaan"
                            }
                        });
                        //window.location.href = "/admin/adr/resumependanaan";
                    }
                    // console.log(msg.data);
                    // alert(msg.data);
                }
            });
        })
    });
</script>

<script type="text/javascript">
    $(document).ready( function(){
        var token = "{{ csrf_token() }}";
        $('#Kirim').on('click',function(){
            var idPengajuan = $('#Kirim').val();
            var user = $('#user').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/adr/kirimresume",
                method: "POST",
                dataType: 'JSON',
                data: {
                    'idPengajuan': idPengajuan,
                    'user_login' : user
                },
                success: function(msg) {
                    //if (msg.data == 1) {
                    if (msg == 1) {
                        window.location.href = "/admin/adr/resumependanaan";
                    }else if(msg == 2){
                        Swal.fire({
                            title: "Notifikasi",
                            text: "Catatan Belum dilengkapi",
                            type: "warning",
                        });
                    }
                    // console.log(msg.data);
                    // alert(msg.data);
                }
            });
        })
    });
</script>

<script type="text/javascript">
    function dateDiff(date1, date2) {
        dt1 = new Date(date1);
        dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    }
</script>


@endsection