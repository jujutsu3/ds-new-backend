<div class="card-body tabObjekPendanaan">
    <div class="row">
        <!-- START: Baris 1 -->
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi Data Diri
                    &nbsp</label>
            </div>
            <hr>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Nama Lengkap Sesuai KTP <i class="text-danger">*</i></label>
                <input class="form-control" type="text" id="nama" maxlength="30" value="{{ $profil->nama }}"
                    name="wizard-progress2-namapengguna" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-jns_kelamin">Jenis Kelamin <i class="text-danger">*</i></label>
                <input class="form-control" type="text" id="jns_kelamin" name="jns_kelamin" name="jns_kelamin" value="{{ ($profil->jns_kelamin === 1) ? 'Laki-laki': 'Perempuan' }}"
                    readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-ktp">NIK<i class="text-danger">*</i></label>
                <input class="form-control" type="text" id="ktp" name="wizard-progress2-ktp" value="{{ $profil->ktp }}" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-npwp">NPWP</label>
                <input class="form-control " type="text" id="npwp" name="npwp" value="{{ $profil->npwp }}" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group row">
                <label class="col-12">Agama <i class="text-danger">*</i></label>
                <div class="col-12">
                    <input class="form-control" type="text" id="agama" name="agama" value="{{ $agama }}" readonly>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group row">
                <label class="col-12">Status Pernikahan <i class="text-danger">*</i></label>
                <div class="col-12">
                    <input class="form-control " type="text" id="status_kawin" name="status_kawin" value="{{ $status_kawin }}" readonly>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-pendidikanterakhir">Pendidikan
                    Terakhir <i class="text-danger">*</i></label>
                <input class="form-control" type="text" id="pendidikan_terakhir" name="pendidikan_terakhir" value="{{ $pendidikan_terakhir }}" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label id="lbl_no_hp">No. Telepon Selular <i class="text-danger">*</i></label>
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text input-group-text-dsi">
                            +62 </span>
                    </div>
                    <input class="form-control checkNOHP validasiString" type="text" value="{{ $telepon }}" id="telepon" name="telepon"
                        readonly>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="tempat_lahir">Tempat Lahir
                    <i class="text-danger">*</i></label>
                <input class="form-control" type="text" id="tempat_lahir" name="tempat_lahir" value="{{ $tempat_lahir }}" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="tanggal_lahir">Tanggal Lahir
                    <i class="text-danger">*</i></label>
                <input class="form-control" type="date" id="tanggal_lahir" name="tanggal_lahir" value="{{ $tgl_lahir }}" readonly>
            </div>
        </div>

        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Alamat Sesuai KTP
                    &nbsp</label>
            </div>
            <hr>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-alamat">Alamat <i class="text-danger">*</i></label>
                <textarea class="form-control"  id="alamat" name="alamat" rows="3" readonly>{{ $alamat }}</textarea>
            </div>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Provinsi <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="provinsi" name="provinsi" value="{{ $provinsi }}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kota">Kota/Kabupaten <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kota" name="kota" value="{{ $kota }}" disabled>
            </div>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kecamatan">Kecamatan <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="{{ $kecamatan }}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kelurahan">Kelurahan <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kelurahan" name="kelurahan" value="{{ $kelurahan }}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kode_pos">Kode Pos <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="{{ $kode_pos }}" disabled>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-status_kepemilikan_rumah">Status Kepemilikan
                    Rumah</label>
        
                @if($status_rumah === 1) 
                    @php $status_rumah = "Milik Pribadi" @endphp       
                @elseif($status_rumah === 2)
                    @php $status_rumah = "Sewa" @endphp    
                @else 
                    @php $status_rumah = "Milik Keluarga" @endphp 
                @endif

                <input class="form-control" type="text" id="domisili_status_rumah" value="{{ $status_rumah  }}" name="domisili_status_rumah"
                    disabled>
            </div>
        </div>



    </div>
    <!-- START: Foto Title -->
    <div class="col-12 mb-4">
        <br>
        <div class="form-check form-check-inline line">
            <label class="form-check-label text-black h5" for="foto">Foto &nbsp</label>
        </div>
        <hr>
    </div>


    <div class="col-md-12">
        <div class="col-md-3 col-sm-6 imgUp">
            <div class="col imgUp" id="foto_diri">
                <div class="d-flex flex-column">
                    <div class="mx-auto">
                        <label class="font-weight-bold ml-0 flex-row">Foto
                            Diri <i class="text-danger">*</i></label>
                    </div>
                    <div class="mx-auto">
                        <img class="imagePreview img-fluid rounded"
                            src="{{ !empty($brw_user_detail_pic) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_user_detail_pic->brw_pic)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}"
                            width="150px" height="145px">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 imgUp">
            <div class="col imgUp text-center" id="foto_ktp">
                <div class="d-flex flex-column">
                    <div class="mx-auto">
                        <label class="font-weight-bold ml-0">Foto KTP <i class="text-danger">*</i></label>
                    </div>
                    <div class="mx-auto">
                        <img class="imagePreview img-fluid rounded"
                            src="{{ !empty($brw_user_detail_pic) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_user_detail_pic->brw_pic_ktp)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}"
                            width="150px" height="145px">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 imgUp">
            <div class="col imgUp text-center" id="foto_ktp_diri">
                <div class="d-flex flex-column">
                    <div class="mx-auto">
                        <label class="font-weight-bold ml-0">Foto Diri &
                            KTP <i class="text-danger">*</i></label>
                    </div>
                    <div class="mx-auto">
                        <img class="imagePreview img-fluid rounded"
                            src="{{ !empty($brw_user_detail_pic) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_user_detail_pic->brw_pic_user_ktp)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}"
                            width="150px" height="145px">
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-3 col-sm-6 imgUp">
            <div class="col imgUp text-center" id="foto_npwp">
                <div class="d-flex flex-column">
                    <div class="mx-auto">
                        <label class="font-weight-bold ml-0">Foto NPWP
                            <i class="text-danger">*</i></label>
                    </div>
                    <div class="mx-auto">
                        <img class="imagePreview img-fluid rounded"
                            src="{{ !empty($brw_user_detail_pic) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_user_detail_pic->brw_pic_npwp)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}"
                            width="150px" height="145px">

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- START: Rekening Rekanan Title -->
    <div class="col-12 mb-4">
        <br>
        <div class="form-check form-check-inline line">
            <label class="form-check-label text-black h5" for="informasi_penghasilan">Informasi
                Pekerjaan</label>
        </div>
        <hr>
    </div>
    <!-- END: Rekening Rekanan Title -->
  
    @if($pekerjaan_id === 5) 
            <div class="blokNonFixedIncome">
                <div class="col-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-pekerjaan">Pekerjaan <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" id="jenis_pekerjaan" name="jenis_pekerjaan"
                                    value="{{ $jenis_pekerjaan }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="bentuk_badan_usaha" class="ml-0">Bidang Usaha<i
                                        class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="bidang_pekerjaan" maxlength="35"
                                    name="bidang_pekerjaan" value="{{ $bidang_pekerjaan }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status_kepegawaian" class="ml-0">Pekerjaan Online
                                    <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="bidang_online_non" maxlength="35"
                                    name="bidang_online_non" value="{{ $bidang_online }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="penghasilan" class="ml-0">Lama Usaha/Praktek (Tahun) <i
                                        class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="lama_usaha_non" name="lama_usaha_non" value="{{ $usia_perusahaan }}"
                                    value="" readonly>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="label_nama_perusahaan" for="nama_perusahaan" class="ml-0">Nama
                                    Usaha/Profesional (Dokter dll)* <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="nama_perusahaan_non" maxlength="35"
                                    name="nama_perusahaan_non" value="{{ $nama_perusahaan }}" readonly>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="no_telpon_usaha" class="ml-0">No. Telepon Usaha/Praktek
                                    <i class="text-danger">*</i></label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-dsi"> +62
                                        </span>
                                    </div>
                                    <input class="form-control" type="text" id="no_telpon_non" name="no_telpon_non" value="{{ $no_telpon_usaha }}"
                                        readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="penghasilan" class="ml-0">Penghasilan Per Bulan Anda *
                                    (Laba-Rugi)
                                    <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="penghasilan_non" name="penghasilan_non" value="{{ $penghasilan }}"
                                    value="" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @else 
    
        <div class="blokFixedIncome">
            <div class="col-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="wizard-progress2-pekerjaan">Pekerjaan <i class="text-danger">*</i></label>
                            <input type="text" class="form-control" id="jenis_pekerjaan" name="jenis_pekerjaan" value="{{ $jenis_pekerjaan }}"
                                readonly>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="bentuk_badan_usaha" class="ml-0">Bidang Pekerjaan<i
                                    class="text-danger">*</i></label>
                            <input class="form-control" type="text" id="bidang_pekerjaan" maxlength="35"
                                name="bidang_pekerjaan" value="{{ $bidang_pekerjaan }}" readonly>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="status_kepegawaian" class="ml-0">Pekerjaan Online
                                <i class="text-danger">*</i></label>
                            <input class="form-control" type="text" id="bidang_online" maxlength="35"
                                name="bidang_online" value="{{ $bidang_online }}" readonly>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="ml-0">Lama Bekerja <i class="text-danger">*</i></label>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Tahun</span>
                                        </div>
                                        <input type="number" id="tahun_bekerja" name="tahun_bekerja"
                                            class="form-control" aria-label="tahun_bekerja"
                                            aria-describedby="basic-addon1" value="{{ $tahun_bekerja }}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                        </div>
                                        <input type="number" id="bulan_bekerja" name="bulan_bekerja"
                                            class="form-control" aria-label="bulan_bekerja"
                                            aria-describedby="basic-addon1" value="{{ $bulan_bekerja }}" readonly>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label id="label_nama_perusahaan" for="nama_perusahaan" class="ml-0">Nama
                                Perusahaan <i class="text-danger">*</i></label>
                            <input class="form-control" type="text" id="nama_perusahaan" maxlength="35"
                                name="nama_perusahaan" value="{{ $nama_perusahaan }}" readonly>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="no_telpon_usaha" class="ml-0">No. Telepon Perusahaan <i
                                    class="text-danger">*</i></label>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text input-group-text-dsi"> +62
                                    </span>
                                </div>
                                <input class="form-control" type="text" id="no_telpon_usaha" name="no_telpon_usaha" value="{{ $no_telpon_usaha }}"
                                    readonly>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="penghasilan" class="ml-0">Pendapatan <i class="text-danger">*</i></label>
                            <input class="form-control" type="text" id="penghasilan" name="penghasilan" value="{{ $penghasilan }}"
                                value="" readonly>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    
    @endif

    <!-- START: info objek pendanaan  -->

    {{-- @php 
      dd($pengajuan->tipe_pendanaan)
    @endphp
     --}}
    @if($pengajuan->pendanaan_tipe  === 2)
       @include('pages.admin.verifikator._info_objek_danarumah')
    @endif 

    @if($pengajuan->pendanaan_tipe  === 1)
       @include('pages.admin.verifikator._info_objek_danakonstruksi')
    @endif 


    <!-- button lanjut -->
    <div class="col-md-12">
        <hr>
    </div>
    <div class="col-md-10">
    </div>

    <!-- end button lanjut -->
</div>
