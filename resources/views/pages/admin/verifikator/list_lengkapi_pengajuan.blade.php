@extends('layouts.admin.master')
@section('title', 'List Lengkapi Pengajuan')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>List Lengkapi Pengajuan - Verifikator</h1>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Daftar Pengajuan</strong>
                </div>
                <div class="card-body table-responsive-lg table-responsive-sm table-responsive-md">
                    <!-- table select all admin -->
                    <table id="lengkapiPengajuanTable" class="table table-striped table-bordered table-responsive-sm" width="100%">
                        <thead>
                            <tr>
                                {{-- <th style="display: none;">Id</th> --}}
                                <th class="text-center">Aksi</th>
                                <th style="width: 10px;">Pengajuan id</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Penerima Pendanaan</th>
                                <th>Pendanaan</th>
                                <th>Nilai Pengajuan</th>
                                <th>Updated Info</th>
                                <th>No Telp</th>
                                <th>Email</th>
                                <th>Status Aktifitas</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!-- end of table select all -->
                </div>
            </div>
        </div>
    </div>
</div><!-- .content -->
@include('layouts.modals.modal-serahterima')
<script type="text/javascript" src="{{url('assetsBorrower/js/plugins/moment/moment.min.js')}}"></script>
<script type="text/javascript">
   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {

        $('body').on('hidden.bs.modal', '.modal', function() {
            $(this).find('form').trigger('reset');
        });


        const formatRupiah = (angka, prefix) => {
            let number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
        }
        
        var d = new Date();
        var n = d.getTime();

        var pengajuanTable = $('#lengkapiPengajuanTable').DataTable({
            searching: true,
            processing: true,
            dom: 'Bfrtip',
            "order": [
                [1, "desc"]
            ],

            buttons: [{
                        extend: 'excelHtml5',
                        exportOptions: {
                            modifier: {
                                page: 'all',
                                search: 'none',
                                selected: false
                            },
                            stripNewlines: false,
                            columns: [1, 2, 3 , 4, 5, 6, 7, 8]
                        },
                        text: 'Export Excel',
                        className: "btn btn-primary",
                        title: 'Daftar Pengajuan',
                        filename: function() {
                            return 'list_pengajuan_verifikator_' + d.getDate() + '_' + d
                        .getTime();
                        },

                    }],
            ajax: {
                url: '/admin/borrower/verifikator/pengajuan_data',
                dataSrc: 'data'
            },
            paging: true,
            info: true,
            lengthChange: false,
            pageLength: 20,
            columns: [
                {
                    data: null,
                    render: function(data, type, row, meta) {
                        let actions = '';
                        const pengajuan_id = row.pengajuan_id;
                        const brw_id = row.brw_id;
                        let pendanaan_nama = row.tipe_pendanaan + ' ' + row.tujuan_pembiayaan;
                        let linkFileSPK = '{{ route("getUserFileForAdmin", ["filename" => ":id"]) }}';
                        
                        if(row.spk !== null){
                            let link_row_spk = row.spk.split("/").join(":");
                            linkFileSPK = linkFileSPK.replace(':id', link_row_spk);
                        }
                        
                        actions = '<div class="dropdown">';
                        actions += '<a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                        actions += '</a>';

                        actions +='<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">';
                        actions +='<a class="dropdown-item" href="profil_pendanaan/detail/'+pengajuan_id+'/'+brw_id+'"><i class="fa fa-search icon"></i> Profil Penerima Pendanaan</a>';

                        if(row.spk !== null){
                            actions +='<a class="dropdown-item" href="'+linkFileSPK+'"  target="_blank"><i class="fa fa-eye icon"></i> Lihat SPK</a>';
                        }
                       
                        
                        actions += '<a class="dropdown-item" href="lengkapi_pengajuan/detail/'+pengajuan_id+'/'+brw_id+'"><i class="fa fa-edit icon"></i> Lengkapi Pengajuan </a>';
                      
                        if(row.status_aktifitas  === 2){
                            actions += '<a class="dropdown-item" href="#" data-toggle="modal" data-brwid="'+brw_id+'" data-customer="'+row.nama_cust+'" data-amount="'+row.nilai_pengajuan+'" data-pendanaan="'+pendanaan_nama+'" data-target="#form_kirim_pengajuan" data-id="'+pengajuan_id+'" data-mode="add"><i class="fa fa-paper-plane icon"></i> Kirim Data </a>';
                        }   

                        if(row.status_aktifitas === 3){
                            actions += '<a class="dropdown-item" href="#" data-toggle="modal" data-brwid="'+brw_id+'" data-customer="'+row.nama_cust+'" data-amount="'+row.nilai_pengajuan+'" data-pendanaan="'+pendanaan_nama+'" data-target="#form_kirim_pengajuan" data-id="'+pengajuan_id+'" data-mode="view"><i class="fa fa-search icon"></i> History Kirim Data </a>';
                        }

                    
                        actions += '</div></div>';
                        return actions;

                    }
                },   
             
                {
                    data: 'pengajuan_id'
                },
        
                {
                    data: 'tgl_pengajuan',
                    'render': function(data, type) {
                         return type === 'sort' ? data : moment(data).format('D/MM/YYYY');
                    }
                },

              

                {
                    data: 'nama_cust'
                },

                {
                     data:null,
                     render: function(data, type, row, meta) {
                        return '<span>'+row.tipe_pendanaan+'</span><br/><span>'+row.tujuan_pembiayaan+'</span>';
                    }
                },
                {
                    data: 'nilai_pengajuan',
                    render: $.fn.dataTable.render.number(',', '.', 2, 'Rp. ')
                },

                {
                    data: null,
                    render: function(data, type, row, meta) {

                        if(row.updated_verifikator){
                            return '<span>'+row.updated_verifikator+'</span><br/><span>'+row.tgl_updated_verifikator+'</span>';
                        }else{
                            return '';
                        }
                    }
                },
                {
                    data :'no_telp'
           
                },

                {
                    data :'email'
           
                },
                {

                    data: null,
                    render: function(data, type, row, meta) {
                        
                        if(row.status_aktifitas == 1){
                            return '<span class="lead"><span class="badge badge-info">Baru</span></span>';
                        }else if(row.status_aktifitas == 2){
                            return '<span class="lead"><span class="badge badge-secondary">Proses</span></span>';
                        }else if(row.status_aktifitas == 3){
                            return '<span class="lead"><span class="badge badge-success">Selesai</span></span>';
                        }

                    }
                }
            ],
            columnDefs: [{
                        targets: [7,8],
                        visible: false
            }]
        });


        $('#form_kirim_pengajuan').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)
            var $recipient = button.data('id'); 
            var pendanaan = button.data('pendanaan');
            var amount = button.data('amount');
            var customer = button.data('customer');
            var brw_id = button.data('brwid');
            var mode = button.data('mode');

            $("#jsPengajuanId").text($recipient);
            $("#pengajuan_id").val($recipient);
            $("#brw_id").val(brw_id);
            $("#pendanaan_info").val(pendanaan);
            $("#pendanaan_nama").val(customer);
            $("#nilai_pengajuan").val(formatRupiah(parseFloat(amount).toString(), 'Rp. '));

            if(mode === 'add'){
                $("#catatan_rekomendasi").attr('readonly', false);
                $(".jsFileRekomendasi").removeClass("d-none");
                $("#btnKirimPengajuan").removeClass('d-none');
                $("#view_file_rekomendasi").addClass('d-none');
            }else{
               $("#catatan_rekomendasi").attr('readonly', true);
               $(".jsFileRekomendasi").addClass("d-none");
               $("#btnKirimPengajuan").addClass('d-none');
               $("#view_file_rekomendasi").removeClass('d-none');
               $("#view_file_rekomendasi").html("");
               loadVerifikatorPengajuan($recipient);

            }

         

        });

        $("form#form_kirim_pengajuan").submit(function(e) {
                e.preventDefault();

                Swal.fire({
                        title: "Notifikasi",
                        text: 'Anda yakin akan mengirim data ini ?. Data pengajuan ini akan dikirim ke tim analis Danasyariah.id',
                        type: "warning",
                        showCancelButton: true,
                    }).then(result => {
                        if (result.value == true) {
                            kirimPengajuanData();
                        } else {
                            return false;
                        }
                    });

        });

        const loadVerifikatorPengajuan = (pengajuan_id) => {

          $.getJSON("/admin/borrower/verifikator/pengajuan/get/" + pengajuan_id + "/", function(response) {
             
                if(response){
                    let linkRekomendasiFile =  response.data.rekomendasi_file.split("/").join(":");
                    let linkFile = '{{ route("getUserFileForAdmin", ["filename" => ":id"]) }}';
                    linkFile = linkFile.replace(':id', linkRekomendasiFile);

                    $("#catatan_rekomendasi").text(response.data.catatan_rekomendasi);
                    $("#view_file_rekomendasi").html("<a href='"+linkFile+"' target=_blank class='btn btn-secondary'><i class='fa fa-eye'></i> Lihat File</a>")
                }
          });

        }

        const kirimPengajuanData = () => {
       
           var data = new FormData();
           var input = document.getElementById("file_rekomendasi");

           var file = input.files[0];

           data.append("file_rekomendasi", file);
           data.append("pengajuan_id",  $("#pengajuan_id").val());
           data.append("brw_id", $("#brw_id").val());
           data.append("catatan_rekomendasi", $("#catatan_rekomendasi").val());

            $.ajax({
                    method: 'post',
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType: "json",
                    url: '/admin/borrower/verifikator/pengajuan/kirim',
                    data: data,
                    beforeSend: () => {
                        swal.fire({
                            html: '<h5>Mengirim Data ...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                            }
                        });
                    },
                    success: function(data) {

                        if (data.status == 'success') {
                            swal.fire({
                                title: "Proses Berhasil",
                                text: data.messsage,
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                            }).then((result) => {
                                 location.reload();
                            });
                        } else {
                            swal.fire({
                                title: "Proses Gagal",
                                text: data.message,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            });
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.status === 419) {
                            swal.fire({
                                title: "Proses Gagal",
                                type: "error",
                                text: "Session expired. Please re-login",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            })

                        }
                    },
                })

        }

    });
</script>


@endsection