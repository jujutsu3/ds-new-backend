<div class="card-body tabObjekPendanaan">
    <div class="row">
        <!-- START: Baris 1 -->
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi Data Perusahaan / Badan Hukum
                    &nbsp</label>
            </div>
            <hr>
        </div>
   

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Nama Perusahaan / Badan Hukum <i
                        class="text-danger">*</i></label>
                <input class="form-control" type="text" id="nm_bdn_hukum" value="{{ $profil->nm_bdn_hukum }}" maxlength="30" name="wizard-progress2-namapengguna"
                    readonly>
            </div>
        </div>


        
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-npwp">NPWP</label>
                <input class="form-control " type="text" id="npwp_perusahaan" name="npwp_perusahaan" value="{{ $profil->npwp_perusahaan }}" readonly>
            </div>
        </div>

      
        <div class="col-md-4">
            <div class="form-group">
                <label for="tanggal_berdiri">Tanggal Berdiri
                    <i class="text-danger">*</i></label>
                <input class="form-control" type="date" id="tgl_berdiri" name="tgl_berdiri" value="{{ $profil->tgl_berdiri }}" readonly>
            </div>
        </div>




    
        <div class="col-md-4">
            <div class="form-group">
                <label id="lbl_no_hp">No. Telepon Badan Hukum <i class="text-danger">*</i></label>
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text input-group-text-dsi">
                            +62 </span>
                    </div>
                    <input class="form-control" type="text" id="telpon_perusahaan" name="telpon_perusahaan" value="{{ $profil->telpon_perusahaan }}" readonly>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="tempat_lahir">No Akta Pendirian
                    <i class="text-danger">*</i></label>
                <input class="form-control" type="text" id="no_akta_pendirian" name="no_akta_pendirian" value="{{ $profil->no_akta_pendirian }}" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="tempat_lahir">No Akta Terakhir  / Perubahan
                    <i class="text-danger">*</i></label>
                <input class="form-control" type="text" id="no_akta_perubahan" name="no_akta_perubahan" value="{{ $profil->no_akta_perubahan }}" readonly>
            </div>
        </div>

        
        <div class="col-md-4">
            <div class="form-group">
                <label for="tempat_lahir">Tanggal Akta Terakhir  / Perubahan
                    <i class="text-danger">*</i></label>
                <input class="form-control" type="date" id="tgl_akta_perubahan" name="tgl_akta_perubahan" value="{{ $profil->tgl_akta_perubahan }}" readonly>
            </div>
        </div>

        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Alamat Perusahaan / Badan Hukum
                    &nbsp</label>
            </div>
            <hr>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-alamat">Alamat <i class="text-danger">*</i></label>
                <textarea class="form-control" maxlength="90" id="alamat" name="alamat" rows="3"
                    readonly>{{ $alamat }}</textarea>
            </div>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Provinsi <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="provinsi" name="provinsi" value="{{ $provinsi }}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kota">Kota/Kabupaten <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kota" name="kota" value="{{ $kota }}" disabled>
            </div>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kecamatan">Kecamatan <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kecamatan" name="kecamatan" value="{{ $kecamatan }}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kelurahan">Kelurahan <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kelurahan" name="kelurahan" value="{{ $kelurahan }}" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kode_pos">Kode Pos <i class="text-danger">*</i></label>
                <input type="text" class="form-control" id="kode_pos" name="kode_pos" value="{{ $kode_pos }}" disabled>
            </div>
        </div>

  
    </div>
    <!-- START: Foto Title -->
    <div class="col-12 mb-4">
        <br>
        <div class="form-check form-check-inline line">
            <label class="form-check-label text-black h5" for="foto">Foto NPWP Perusahaan/Badan Hukum &nbsp</label>
        </div>
        <hr>
    </div>


    <div class="col-md-12">
        <div class="col-md-3 col-sm-6 imgUp">
            <div class="col imgUp" id="foto_diri">
                <div class="d-flex flex-column">
                    <div class="mx-auto">
                        <label class="font-weight-bold ml-0 flex-row">Foto
                            NPWP <i class="text-danger">*</i></label>
                    </div>
                    <div class="mx-auto">
                        <img class="imagePreview img-fluid rounded"
                            src="{{ !empty($profil->foto_npwp_perusahaan) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $profil->foto_npwp_perusahaan)]) . '?t=' . date('Y-m-d h:i:sa'): '' }}"
                            width="150px" height="145px">
                    </div>
                </div>
            </div>
        </div>
    
    </div>


    <div class="col-12 mb-4">
        <br>
        <div class="form-check form-check-inline line">
            <label class="form-check-label text-black h5" for="informasi_penghasilan">Informasi
                Pengurus </label>
        </div>
        <hr>
    </div>


    <div class="card-body table-responsive-lg table-responsive-sm table-responsive-md">
        <table class="table table-striped table-bordered" id="productsurvey-table" width="100%">
            <thead class="thead-dark">
                <tr>
                    <th>Nama Pengurus</th>
                    <th>No KTP</th>
                    <th>NPWP</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Tempat Lahir</th>
                    <th>No Telp</th>
                </tr>

                <tbody>

                    @foreach($pengurus as $info_pengurus)
                    <tr>
                        <td>{!! $info_pengurus->nm_pengurus !!}</td>
                        <td>{!! $info_pengurus->nik_pengurus !!}</td>
                        <td>{!! $info_pengurus->npwp !!}</td>
                        <td>{!! $info_pengurus->tgl_lahir !!}</td>
                        <td>{!! $info_pengurus->gender->jenis_kelamin !!}</td>
                        <td>{!! $info_pengurus->tempat_lahir !!}</td>
                        <td>{!! $info_pengurus->no_tlp !!}</td>
                    </tr>

                    @endforeach


                </tbody>

        </table>

    </div>



    <!-- END: Baris 12 -->


    <div class="col-12 mb-4">
        <br>
        <div class="form-check form-check-inline line">
            <label class="form-check-label text-black h5" for="informasi_penghasilan">Informasi
                Objek Pendanaan</label>
        </div>
        <hr>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="wizard-progress2-namapengguna">Tujuan Pendanaan</label>
            <input class="form-control tujuan_pendanaan" id="tujuan_pendanaan" name="tujuan_pendanaan" value="{{ $tujuan_pendanaan }}" readonly>
        </div>
    </div>


    <div class="col-md-4">
        <div class="form-group">
            <label for="wizard-progress2-namapengguna">Tipe Pendanaan</label>
            <input class="form-control type_pendanaan" id="type_pendanaan" name="type_pendanaan" value="{{ $pendanaan_tipe }}" readonly>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="wizard-progress2-namapendanaan">Harga Objek Pendanaan</label>
            <input type="text" class="form-control" id="harga_objek_pendanaan" name="harga_objek_pendanaan" value="{{ number_format($pengajuan->harga_objek_pendanaan, 0, ",", ".") }}" readonly>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="wizard-progress2-namapendanaan">Uang Muka </label>
            <input type="text" class="form-control" id="uang_muka" name="uang_muka" value="0" readonly>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="wizard-progress2-namapendanaan">Nilai Pengajuan </label>
            <input type="text" class="form-control" id="nilai_pengajuan" name="nilai_pengajuan" value="{{ number_format($pengajuan->pendanaan_dana_dibutuhkan, 0, ",", ".") }}" readonly>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
         
           
            <label for="wizard-progress2-namapendanaan">Jangka Waktu</label>
            <div class="input-group mb-2">
                <input type="text" class="form-control" id="jangka_waktu" name="jangka_waktu" value="{{ $pengajuan->durasi_proyek }}" readonly>
                <div class="input-group-prepend">
                    <span class="input-group-text" id="jangka_waktu_prepend">Bulan</span>
                </div>
            </div>
        </div>
    </div>


    
    <div class="col-md-4">
        <div class="form-group">
            <label for="wizard-progress2-alamat">Lokasi Objek <i class="text-danger">*</i></label>
            <textarea class="form-control" maxlength="90" id="lokasi_proyek" name="lokasi_proyek" rows="3"
                readonly>{{ $profil->lokasi_proyek }}</textarea>
        </div>
    </div>

    <!-- button lanjut -->
    <div class="col-md-12">
        <hr>
    </div>
    <div class="col-md-10">
    </div>
  

    <!-- end button lanjut -->
</div>
