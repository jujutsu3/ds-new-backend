@extends('layouts.admin.master')

@section('title', 'Dashboard Borrower')

@push('add-more-style')
<style>
    input[type=checkbox] {
        top: 1rem;
        width: 1rem;
        height: 1rem;
    }

    .btn-danaSyariah {
        background-color: #16793E;
    }

    .box-title {
        margin-top: -0.9rem;
        margin-left: 8px;
        background-color: #FFFFFF;
        width: -moz-fit-content;
        width: fit-content;
    }

    body {
        background-color: #FFFFFF;
    }

    .card {
        border-color: #000000;
        margin-bottom: 60px
    }

    .select2-selection {
        height: 35px !important;
    }
</style>
@endpush

@section('content')

<div class="breadcrumbs mb-4">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                @if (request()->get('keterangan') == 'dokumen_verifikasi_rac')
                <h1>Verifikasi - Dokumen Verifikasi RAC</h1>
                @elseif (request()->get('keterangan') == 'form_interview_hrd')
                <h1>Verifikasi - Form Interview HRD</h1>
                @else
                <h1>Verifikasi - {{ ucwords(str_replace('_', ' ', request()->get('keterangan'))) }}</h1>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if (session('error'))
            <div class="alert alert-danger col-sm-12">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif (session('success'))
            <div class="alert alert-success col-sm-12">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session('updated'))
            <div class="alert alert-success col-sm-12">
                {{ session('updated') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="card px-3">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Informasi Verifikasi</label>
                </div>
                <div class="card-body">

                    <div class="row mt-2">
                        <div class="col-md-5 col-12">Nama Verifikator <span class="float-right ml-1">:</span></div>
                        <p id="nama_verifikator" class="col-md-6 pl-md-0 text-dark col-12">
                            {{ $data->nama_verifikator ? $data->nama_verifikator : '-' }}
                            {{-- {{ $data->nama_verifikator ? $data->nama_verifikator : Auth::user()->firstname.'
                            '.Auth::user()->lastname }} --}}
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-12">Tanggal Verifikasi <span class="float-right ml-1">:</span></div>
                        <p id="tanggal_verifikasi" class="col-md-6 pl-md-0 text-dark col-12">
                            @if (request()->get('keterangan') == 'dokumen_ceklis_verifikasi')
                            {{ !empty($data->created_at_dokumen_ceklis) ? date('d-m-Y',
                            strtotime($data->created_at_dokumen_ceklis)) : '-' }}
                            @elseif (request()->get('keterangan') == 'dokumen_verifikasi_rac')
                            {{ !empty($data->created_at_rac) ? date('d-m-Y',
                            strtotime($data->created_at_rac)) : '-' }}
                            @elseif (request()->get('keterangan') == 'form_interview_hrd')
                            {{ !empty($data->created_at_interview_hrd) ? date('d-m-Y',
                            strtotime($data->created_at_interview_hrd)) : '-' }}
                            @elseif (request()->get('keterangan') == 'form_kunjungan_domisili')
                            {{ !empty($data->created_at_kunjungan_domisili) ? date('d-m-Y',
                            strtotime($data->created_at_kunjungan_domisili)) : '-' }}
                            @elseif (request()->get('keterangan') == 'form_kunjungan_objek_pendanaan')
                            {{ !empty($data->created_at_kunjungan_objek_pendanaan) ? date('d-m-Y',
                            strtotime($data->created_at_kunjungan_objek_pendanaan)) : '-' }}
                            @elseif (request()->get('keterangan') == 'form_informasi_pendanaan_berjalan')
                            {{ !empty($data->created_at_informasi_pendanaan_berjalan) ? date('d-m-Y',
                            strtotime($data->created_at_informasi_pendanaan_berjalan)) : '-' }}
                            @endif
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-12">ID Penerima Pendanaan | ID Pengajuan Pendanaan <span
                                class="float-right ml-1">:</span></div>
                        <p id="id_penerima_pengajuan" class="col-md-6 pl-md-0 text-dark col-12">
                            {{ !empty($data) ? $data->brw_id .' | '.$data->pengajuan_id : '' }}
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-12">Nama Penerima Dana <span class="float-right ml-1">:</span></div>
                        <p id="nama_penerima" class="col-md-6 pl-md-0 text-dark col-12">
                            {{ !empty($data) ? $data->nama : '' }}
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-12">Plafon Pengajuan Pendanaan <span class="float-right ml-1">:</span>
                        </div>
                        <p id="plafon_pengajuan" class="col-md-6 pl-md-0 text-dark col-12">
                            {{ !empty($data) ? 'Rp' .str_replace(',','.',
                            number_format($data->pendanaan_dana_dibutuhkan)) : '' }}
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-12">Fasilitas Pendanaan <span class="float-right ml-1">:</span></div>
                        <p id="tipe_pendanaan" class="col-md-6 pl-md-0 text-dark col-12">
                            {{ !empty($data) ? $data->pendanaan_nama : '' }}
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-12">Jangka Waktu Pendanaan (Bulan) <span
                                class="float-right ml-1">:</span></div>
                        <p id="tenor_pendanaan" class="col-md-6 pl-md-0 text-dark col-12">
                            {{ !empty($data) ? $data->durasi_proyek : '' }}
                        </p>
                    </div>

                    <div class="row">
                        <div class="col-md-5 col-12">Kategori Penerima Pendanaan <span class="float-right ml-1">:</span>
                        </div>
                        <p id="sumber_pengembalian_dana" class="col-md-6 pl-md-0 text-dark col-12">
                            {{ !empty($data) ? $data->sumber_pengembalian_dana == '1' ? 'Fixed Income' : 'Non-Fixed
                            Income' : '-' }}
                        </p>
                    </div>
                </div>
            </div>

            @if (request()->get('keterangan') == 'dokumen_ceklis_verifikasi')
            <form method="POST"
                action="{{ route('detail-pekerjaan-verifikator-update', ['pengajuan_id' => request()->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
                id="form_dokumen_ceklis_verifikasi_occasio" name="form_dokumen_ceklis_verifikasi_occasio">
                @csrf
                @include('pages.admin.verifikator.analisa_pendanaan.form_pekerjaan.dokumen_ceklis_verifikasi_occasio_view')
            </form>
            @elseif (request()->get('keterangan') == 'dokumen_verifikasi_rac')
            <form method="POST"
                action="{{ route('detail-pekerjaan-verifikator-update', ['pengajuan_id' => request()->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
                id="form_dokumen_verifikasi_rac_occasio" name="form_dokumen_verifikasi_rac_occasio">
                @csrf
                @include('pages.admin.verifikator.analisa_pendanaan.form_pekerjaan.dokumen_verifikasi_rac_occasio_view')
            </form>
            @elseif (request()->get('keterangan') == 'form_interview_hrd')
            <form method="POST"
                action="{{ route('detail-pekerjaan-verifikator-update', ['pengajuan_id' => request()->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
                id="form_interview_hrd" name="form_interview_hrd" enctype="multipart/form-data">
                @csrf
                @include('pages.admin.verifikator.analisa_pendanaan.form_pekerjaan.form_interview_hrd_occasio_view')
            </form>
            @elseif (request()->get('keterangan') == 'form_kunjungan_domisili')
            <form method="POST"
                action="{{ route('detail-pekerjaan-verifikator-update', ['pengajuan_id' => request()->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
                id="form_kunjungan_domisili" name="form_kunjungan_domisili" enctype="multipart/form-data">
                @csrf
                @include('pages.admin.verifikator.analisa_pendanaan.form_pekerjaan.form_kunjungan_domisili_occasio_view')
            </form>
            @elseif (request()->get('keterangan') == 'form_kunjungan_objek_pendanaan')
            <form method="POST"
                action="{{ route('detail-pekerjaan-verifikator-update', ['pengajuan_id' => request()->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
                id="form_kunjungan_objek_pendanaan" name="form_kunjungan_objek_pendanaan" enctype="multipart/form-data">
                @csrf
                @include('pages.admin.verifikator.analisa_pendanaan.form_pekerjaan.form_kunjungan_objek_pendanaan_occasio_view')
            </form>
            @elseif (request()->get('keterangan') == 'form_informasi_pendanaan_berjalan')
            <form method="POST"
                action="{{ route('detail-pekerjaan-verifikator-update', ['pengajuan_id' => request()->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
                id="form_informasi_pendanaan_berjalan" name="form_informasi_pendanaan_berjalan">
                @csrf
                @include('pages.admin.verifikator.analisa_pendanaan.form_pekerjaan.form_informasi_pendanaan_berjalan_occasio_view')
            </form>
            @elseif (request()->get('keterangan') == 'form_informasi_pendanaan_berjalan')
            <form method="POST"
                action="{{ route('detail-pekerjaan-verifikator-update', ['pengajuan_id' => request()->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
                id="form_informasi_pendanaan_berjalan" name="form_informasi_pendanaan_berjalan">
                @csrf
                @include('pages.admin.verifikator.analisa_pendanaan.form_pekerjaan.form_informasi_pendanaan_berjalan_occasio_view')
            </form>
            @endif

            <div class="d-flex flex-row-reverse">
                <button type="button" id="btn_simpan" class="btn btn-danaSyariah text-white rounded mb-2 ml-2" {{ $data
                    && !empty($data->nama_verifikator) ? (Auth::user()->firstname.' '.Auth::user()->lastname) !=
                    $data->nama_verifikator ? 'disabled' : '' : ''}} {{ $data && $data->status_verifikator == '3' ?
                    'disabled' : '' }}>Simpan</button>
                <a href="{{ route('view-detil-verifikator', ['pengajuan_id' => request()->pengajuan_id]) }}"
                    class="btn btn-danaSyariah text-white rounded mb-2 ml-2">Kembali</a>
            </div>
        </div>
    </div>
</div>

{{-- START: Modal View Doc --}}
<div class="modal fade bd-example-modal-lg" id="modal-doc-pendukung" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Lihat File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="preview">
                    <p id="text-status" class="text-center">Mengambil Data..</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnUploadUlang" class="btn btn-secondary text-left" data-toggle="modal"
                    data-target="#div_upload_dokumen_pendukung" data-dismiss="modal"><i class="fa fa-upload"
                        aria-hidden="true"></i>
                    Unggah Ulang File </button>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal View Doc --}}

{{-- START: Modal Upload Doc --}}
<div class="modal fade bd-example-modal-lg" id="div_upload_dokumen_pendukung" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        {{-- START: Modal content --}}
        <form id="form_upload_dokumen_pendukung" method='post' enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Unggah File</h5>
                    <button type="button" class="close" data-dismiss="modal" id="closeDokumenPendukung"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="file" id="file" oninput="input_filename();">
                        <label id="file_input_label" class="custom-file-label text-truncate" for="image"></label>
                    </div>
                    <br>
                    <p id="required-file-type" class="text-danger">*tipe file</p>
                    <input type="hidden" name="fieldname" id="fieldname" value="" />
                    <input type="hidden" name="tablename" id="tablename" value="" />
                    <input type="hidden" name="pengajuanid" id="pengajuanId" value="" />
                    <input type="hidden" name="brw_id" id="brw_id" value="" />
                    <input type="hidden" name="filetype" id="filetype" value="" />

                    <div class="my-3">
                        <div class="form-check custom-checkbox">
                            <input type="checkbox" class="form-check-input" name="setujuUpload" id="setujuUpload">
                            <label class="form-check-label ml-2" for="setujuUpload">Dengan mengklik tombol unggah file
                                saya setuju dokumen ini akan tersimpan pada sistem dan menyetujui proses selanjutnya
                                oleh PT Dana Syariah Indonesia</label>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success text-left" id="uploadDokumen" disabled><i
                            class="fa fa-upload" aria-hidden="true"></i> Unggah File </button>
                </div>
            </div>
        </form>
        {{-- END: Modal content --}}
    </div>
</div>
{{-- END: Modal Upload Doc --}}

@endsection

@section('js')
<script type="text/javascript">
    var side = '/admin/borrower';
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const pengajuan_id = '{!! $data->pengajuan_id !!}'
    const nama_brw = '{!! $data->nama !!}'
    const brw_id = '{!! $data->brw_id !!}'
    const brw_type = '{!! $data->brw_type !!}'
    const is_single_income = "{!! $data->skema_pembiayaan !!}"
    const status_kawin = "{!! $data->status_kawin !!}"
    
    let file_input_label = document.getElementById("file_input_label");
    const inputFile = document.getElementById('file');

    $(document).ready(function(){
        let keterangan = "{!! request()->get('keterangan') !!}"
        $('#btn_simpan').click(() => {
            if (keterangan == 'dokumen_ceklis_verifikasi') {
                $('#form_dokumen_ceklis_verifikasi_occasio').submit()
            } else if (keterangan == 'dokumen_verifikasi_rac') {
                $('#form_dokumen_verifikasi_rac_occasio').submit()
            } else if (keterangan == 'form_interview_hrd') {
                $('#form_interview_hrd').submit()
            } else if (keterangan == 'form_kunjungan_domisili') {
                $('#form_kunjungan_domisili').submit()
            } else if (keterangan == 'form_kunjungan_objek_pendanaan') {
                $('#form_kunjungan_objek_pendanaan').submit()
            } else if (keterangan == 'form_informasi_pendanaan_berjalan') {
                $('#form_informasi_pendanaan_berjalan').submit()
            }
        })


        $('#form_dokumen_ceklis_verifikasi_occasio').submit(function(e) {
            e.preventDefault()
            submitData('#form_dokumen_ceklis_verifikasi_occasio')
        })

        $('#form_dokumen_verifikasi_rac_occasio').submit(function(e) {
            e.preventDefault()
            submitData('#form_dokumen_verifikasi_rac_occasio')
        })

        $('#form_interview_hrd').submit(function(e) {
            e.preventDefault()
            submitData('#form_interview_hrd')
        })

        $('#form_kunjungan_domisili').submit(function(e) {
            e.preventDefault()
            submitData('#form_kunjungan_domisili')
        })
        
        $('#form_kunjungan_objek_pendanaan').submit(function(e) {
            e.preventDefault()
            submitData('#form_kunjungan_objek_pendanaan')
        })
        $('#form_informasi_pendanaan_berjalan').submit(function(e) {
            e.preventDefault()
            submitData('#form_informasi_pendanaan_berjalan')
        })

        $('.btn-show-file').click(function() {
            let tableName = $(this).attr('data-tableName')
            let fieldName = $(this).attr('data-fieldName')
            let tableName2 = ''
            let fieldName2 = ''
            let tableNameTitle = ''
            let tableName2Title = ''
            let filetype = $(this).attr('data-fileType')

            // if there are two files
            if($(this).attr("data-tableName-2")) {
                tableNameTitle = $(this).attr('data-tableName-title')
                tableName2Title = $(this).attr('data-tableName-2-title')
                tableName2 = $(this).attr('data-tableName-2')
            }
            if($(this).attr("data-fieldName-2")) {
                fieldName2 = $(this).attr('data-fieldName-2')
            }

            getFilePath(tableName, fieldName, tableName2, fieldName2, tableNameTitle, tableName2Title)

            // Set Params to upload doc
            $('#required-file-type').text(`* ${filetype.replaceAll('|', ', ')}`)
            $('#tablename').val(tableName)
            $('#fieldname').val(fieldName)
            $('#pengajuanId').val(pengajuan_id)
            $('#brw_id').val(brw_id)
            $('#filetype').val(filetype)
        })

        $("#setujuUpload").click(function() {
            var checked_status = this.checked;
            if (checked_status == true) {
                $("#uploadDokumen").removeAttr("disabled");
            } else {
                $("#uploadDokumen").attr("disabled", "disabled");
            }
        });

        $("#uploadDokumen").click(function(e) {
            e.preventDefault();
            uploadDoc()
        });

        inputFile.addEventListener('change', (event) => {
            const target = event.target
            if (target.files && target.files[0]) {

                const maxAllowedSize = 1 * 1024 * 1024;
                if (target.files[0].size > maxAllowedSize) {

                    swal.fire({
                        title: "Proses Gagal",
                        text: "Maksimum Unggah File Size 1 MB ",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                    }).then((result) => {
                        target.value = '';
                        file_input_label.innerText = '';
                    })
                }
            }
        })
    });

    //Submit Data
    const submitData = (element_id) => {
        let form = $(element_id);
        let formData = new FormData($(element_id)[0]);
        $.ajax({
            url : $(element_id).attr('action'),
            type: $(element_id).attr("method"),
            dataType: 'JSON',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function() {
                swal.fire({
                    html: '<h5>Menyimpan Data...</h5>',
                    showConfirmButton: false,
                    allowOutsideClick: () => false,
                    onBeforeOpen: () => {
                        swal.showLoading();
                    }
                });
            },
            success: function (response) {
                console.log(response)
                if (response.status == 'success') {
                    swal.fire({
                        title: 'Berhasil',
                        type: 'success',
                        text: response.msg,
                        allowOutsideClick: () => false,
                    }).then(function (result) {
                        window.location = "{{ route('view-detil-verifikator', ['pengajuan_id' => request()->pengajuan_id]) }}"
                    })
                } else {
                    swal.fire({
                        title: 'Oops',
                        type: 'error',
                        text: "Internal Error"
                    })
                }
            },
            error: function(response) {
                swal.fire({
                    title: 'Error',
                    type: 'error',
                    text: response
                })
                console.log(response)
            }
        })
    }

    const formatRupiah = (angka, prefix) => {
        let thisValue = angka.replace(/[^,\d]/g, '')
        angka = (parseInt(thisValue, 10)).toString()

        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
    }

    const showHasilVerif = (chk_doc_id, hasil_verif_id) => {
        if ($(`#${chk_doc_id}`).is(":checked")) {
            $('#'+hasil_verif_id).removeClass('d-none')
        } else {
            $('#'+hasil_verif_id).addClass('d-none')
        }
    } 

    const getFilePath = (table_name, field_name, table_name2, field_name2, table_name_title, table_name2_title) => {
    
        let url = `{{ route('getUserFilePathForAdmin', ['tableName' => ':table_name', 'fieldName' => ':field_name', 'pengajuan_id' => ':pengajuan_id']) }}`
        url = url.replace(':table_name', table_name).replace(':field_name', field_name).replace(':pengajuan_id', pengajuan_id)

        requestFilePathAjax(url, table_name_title)

        if (table_name2 != '') {
            let url2 = `{{ route('getUserFilePathForAdmin', ['tableName' => ':table_name', 'fieldName' => ':field_name', 'pengajuan_id' => ':pengajuan_id']) }}`
            url2 = url2.replace(':table_name', table_name2).replace(':field_name', field_name2).replace(':pengajuan_id', pengajuan_id)

            requestFilePathAjax(url2, table_name2_title)
        }
    }

    const requestFilePathAjax = (url, title) => {
        let status = 0 , path = ''
        console.log(url)

        $.ajax({
            type: "get",
            url: url,
            contentType: false,
            processData: false,
            beforeSend: () => {
                $('#preview').html(`
                    <p class="text-center text-status">Mengambil Data..</p>
                `)
            },
            success: function(data) {
                status = 1 // success
                path = data
                
                $('.text-status').remove()
                showFile(status, path, title)
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;

                if (xhr.status === 419) {
                    $('.text-status').text(`Page expired. please re-login again`)
                } else {
                    $('.text-status').text(`Gagal Mengambil Data `)
                }

                status = 2 // failed
                showFile(status, path)
            },
        })
    }

    const showFile = (status, path, title) => {
        let pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");
        let file = "{{ route('getUserFileForAdmin', ['filename' => ':filename']) }}"
        path = path.replaceAll('/', ':')
        file = file.replace(':filename', path)
        
        if(status == 1) { // Success

            if (path) { 
                if (pdfSearch.test(path)) {
                    $('#preview').append($('<p>', {
                        class: 'font-weight-bold mt-2',
                        text: title
                    }))
                    $('#preview').append($('<embed>', {
                        class: 'pdf_data',
                        src: file,
                        frameborder: '0',
                        width: '100%',
                        height: '500px'
                    }))

                } else {
                    $('#preview').append($('<p>', {
                        class: 'font-weight-bold mt-2',
                        text: title
                    }))
                    $('#preview').append($('<img>', {
                        class: 'img_data img-fluid w-100',
                        src: file,
                    }))
                }
            } else {
                $('.text-status').text(`File tidak ada, silahkan unggah file`)
            }
        } else { //Failed

        }
    }

    // START: Upload File
    const input_filename = () => {
        file_input_label.innerText = inputFile.files[0].name;
    }

    const uploadDoc = () => {
        var formData = new FormData();
        var files = $('#file')[0].files;

        formData.append('_token', "{{ csrf_token() }}");
        formData.append('file', files[0]);
        formData.append('fieldName', $("#fieldname").val());
        formData.append('tableName', $("#tablename").val());
        formData.append('pengajuanId', $("#pengajuanId").val());
        formData.append('brw_id', $("#brw_id").val());
        formData.append('filetype', $("#filetype").val());

        $.ajax({
            type: "post",
            url: "{{ route('borrower.danarumah.upload_file') }}",
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: () => {
                Swal.fire({
                    html: '<h5>Unggah File ...</h5>',
                    showConfirmButton: false,
                    allowOutsideClick: () => false,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                        $('#div_upload_dokumen_pendukung').modal('hide');
                    }
                });
            },
            success: function(data) {

                if (data.status == 'success') {
                    Swal.fire({
                        title: "Proses Berhasil",
                        text: "Unggah File  Berhasil",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-success",
                    }).then((result) => {

                        file_input_label.innerText = '';
                        inputFile.value = '';
                        $('#setujuUpload').prop('checked', false);
                        $("#uploadDokumen").attr("disabled", "disabled");
                        document.getElementById('closeDokumenPendukung').click();

                    });
                } else {
                    let error_msg = data.message
                    console.log(data)
                    Swal.fire({
                        title: "Proses Gagal",
                        text: error_msg,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                    }).then(function() {
                        document.getElementById('closeDokumenPendukung').click();
                    });

                }
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;

                if (xhr.status === 419) {
                    Swal.fire({
                        title: "Upload  Gagal",
                        type: "error",
                        text: "Page expired. please re-login again",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                    }).then(function() {
                        $('.modal-backdrop').remove();
                    });

                }


            },
        })
    }
</script>
@endsection