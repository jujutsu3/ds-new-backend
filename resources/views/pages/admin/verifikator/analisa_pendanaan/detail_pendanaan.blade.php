@extends('layouts.admin.master')

@section('title', 'Lanjut Verifikasi')
@section('style')

<style>
    ul.breadcrumb {
        padding: 10px 16px;
        list-style: none;
        background-color: #fff;
    }

    ul.breadcrumb li {
        display: inline;
        font-size: 18px;
    }

    ul.breadcrumb li+li:before {
        padding: 8px;
        color: black;
        content: "/\00a0";
    }

    ul.breadcrumb li a {
        color: #0275d8;
        text-decoration: none;
    }

    ul.breadcrumb li a:hover {
        color: #01447e;
        text-decoration: underline;
    }

    .btn-danaSyariah {
        background-color: #16793E;
    }

    .box-title {
        margin-top: -0.9rem;
        margin-left: 8px;
        background-color: #F1F2F7;
        width: -moz-fit-content;
        width: fit-content;
    }

    body {
        background-color: #F1F2F7;
    }

    .card {
        border-color: #000000;
        margin-bottom: 60px;
        background-color: #F1F2F7
    }
</style>
@endsection


@section('content')

<ul class="breadcrumb">
    <li><a href="{{ route('borrower.list.verifikasi') }}">Verifikasi</a></li>
    <li>Lanjut</li>
</ul>

<div class="content mt-3">
    <div class="card">
        <div class="box-title">
            <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Informasi Objek
                Pendanaan</label>
        </div>
        <div class="card-body">
            <div class="mt-2">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="tipe_pendanaan">Tipe Pendanaan</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="tipe_pendanaan"
                                id="tipe_pendanaan" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="tujuan_pendanaan">Tujuan Pendanaan</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="tujuan_pendanaan"
                                id="tujuan_pendanaan" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="alamat_objek_pendanaan">Alamat Objek
                                Pendanaan</label>
                            <input type="text" class="form-control-plaintext text-secondary"
                                name="alamat_objek_pendanaan" id="alamat_objek_pendanaan" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="provinsi">Provinsi</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="provinsi"
                                id="provinsi" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="kota_kabupaten">Kota/Kabupaten</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kota_kabupaten"
                                id="kota_kabupaten" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="kecamatan">Kecamatan</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kecamatan"
                                id="kecamatan" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="kelurahan">Kelurahan</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kelurahan"
                                id="kelurahan" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="kode_pos">Kode Pos</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kode_pos"
                                id="kode_pos" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="harga_objek_pendanaan">Harga Objek Pendanaan</label>
                            <input type="text" class="form-control-plaintext text-secondary"
                                name="harga_objek_pendanaan" id="harga_objek_pendanaan" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="uang_muka">Uang Muka</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="uang_muka"
                                id="uang_muka" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="nilai_pengajuan_pendanaan">Nilai Pengajuan
                                Pendanaan</label>
                            <input type="text" class="form-control-plaintext text-secondary"
                                name="nilai_pengajuan_pendanaan" id="nilai_pengajuan_pendanaan" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal" for="jangka_waktu">Jangka Waktu (Bulan)</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="jangka_waktu"
                                id="jangka_waktu" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="detail_informasi_pendanaan">Detail Informasi Objek Pendanaan</label>
                            <p id="detail_informasi_pendanaan" class="text-justify">
                            </p>
                            {{-- <input type="text" class="form-control-plaintext text-secondary"
                                name="detail_informasi_pendanaan" id="detail_informasi_pendanaan" readonly> --}}
                            {{-- <textarea class="form-control" id="detail_informasi_pendanaan" rows="3"
                                readonly></textarea> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="box-title">
            <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Informasi Pemilik Objek
                Pendanaan</label>
        </div>
        <div class="card-body">
            <div class="mt-2">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">Nama</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="nama_pemilik"
                                id="nama_pemilik" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">No Telp/HP</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="no_telp_pemilik"
                                id="no_telp_pemilik" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">Alamat Pemilik Pendanaan</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="alamat_pemilik"
                                id="alamat_pemilik" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">Provinsi</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="provinsi_pemilik"
                                id="provinsi_pemilik" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">Kota/Kabupaten</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kota_pemilik"
                                id="kota_pemilik" readonly>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">Kecamatan</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kecamatan_pemilik"
                                id="kecamatan_pemilik" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">Kelurahan</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kelurahan_pemilik"
                                id="kelurahan_pemilik" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="font-weight-normal">Kode Pos</label>
                            <input type="text" class="form-control-plaintext text-secondary" name="kode_pos_pemilik"
                                id="kode_pos_pemilik" readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        {{-- <div class="col-md-12">
            <form id="form_setuju_pendanaan" action="{{route('admin.saveProcessVerifOccasio')}}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="id_brw" name="id_brw">
                <input type="hidden" id="pengajuan_id" name="pengajuan_id" value="{{ $pengajuan_id }}">
                <input type="hidden" id="proyek_id" name="proyek_id">

                <div class="col-lg-12 mt-2">

                    <div class="row">
                        <div class="col-sm-6">
                            <label style="font-size: larger"><b>Informasi Objek Pendanaan</b></label>
                        </div>
                    </div>


                </div>

                <hr
                    style="height:2px;border-width:0;color:black;background-color:black;width:100%; display:inline-block">

                <div class="col-lg-12 mt-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <label style="font-size: larger"><b>Informasi Pemilik Objek Pendanaan</b></label>
                        </div>
                    </div>


                    <br>
                </div>

        </div> --}}
        <div class="col-md-12 text-right">
            <form id="form_setuju_pendanaan" action="{{route('admin.saveProcessVerifOccasio')}}" method="POST"
                enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="id_brw" name="id_brw">
                <input type="hidden" id="pengajuan_id" name="pengajuan_id" value="{{ $pengajuan_id }}">
                <input type="hidden" id="proyek_id" name="proyek_id">

                <div class="row mb-4">
                    <div class="col-md-12">
                        {{-- <a href=""
                            class="btn btn-danaSyariah text-white float-left rounded">Kembali</a> --}}
                        <button id="button_setuju_save" type="submit"
                            class="btn btn-danaSyariah text-white float-right rounded">Lanjutkan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </form>

</div>


</div>

</div>
<script type="text/javascript">
    var side = '/admin/borrower';
 var pengajuan_id = "{{ $pengajuan_id }}";
 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let full_text = ''
    let text_summary =''

    $(document).ready(function(){
        $.ajax({
            url : side+'/getProsesVerifData/'+pengajuan_id,
            method: "get",
            beforeSend: function() {
                swal.fire({
                    html: '<h5>Mengambil Data...</h5>',
                    showConfirmButton: false,
                    allowOutsideClick: () => false,
                    onBeforeOpen: () => {
                        swal.showLoading();
                    }
                });
            },
            complete: function() {
                swal.close()
            },
            success:function(data,value)
            {


                $("#pengajuan_id").val(pengajuan_id);
              
                let tipe_pendanaan              = data.jenis_pendanaan;
                let tujuan_pendanaan            = data.tujuan_pendanaan;
                let alamat_objek_pendanaan      = data.data_pengajuan.lokasi_proyek;
                let provinsi                    = data.data_pengajuan.provinsi;
                let kota_kabupaten              = data.data_pengajuan.kota;
                let kecamatan                   = data.data_pengajuan.kecamatan;
                let kelurahan                   = data.data_pengajuan.kelurahan;
                let kode_pos                    = data.data_pengajuan.kode_pos;
                let harga_objek_pendanaan       = data.data_pengajuan.harga_objek_pendanaan;
                let uang_muka                   = data.data_pengajuan.uang_muka;
                let nilai_pengajuan_pendanaan   = data.data_pengajuan.pendanaan_dana_dibutuhkan;
                let jangka_waktu                = data.data_pengajuan.durasi_proyek;
                let detail_informasi_pendanaan  = data.data_pengajuan.detail_pendanaan ? data.data_pengajuan.detail_pendanaan : '-';

                full_text = detail_informasi_pendanaan
                if (detail_informasi_pendanaan.length > 600) {
                    text_summary = detail_informasi_pendanaan.substring(0, 600)
                    $("#detail_informasi_pendanaan").html(`
                        ${text_summary} <a href="#" id="more-text-pendanaan" class="text-primary"
                            onclick="fullText(); return false">selengkapnya...</a>
                    `);
                } else {
                    $("#detail_informasi_pendanaan").html(`${full_text}`)
                }

                $("#tipe_pendanaan").val(tipe_pendanaan);
                $("#tujuan_pendanaan").val(tujuan_pendanaan);
                $("#alamat_objek_pendanaan").val(alamat_objek_pendanaan);
                $("#provinsi").val(provinsi);
                $("#kota_kabupaten").val(kota_kabupaten);
                $("#kecamatan").val(kecamatan);
                $("#kelurahan").val(kelurahan);
                $("#kode_pos").val(kode_pos);
                $("#harga_objek_pendanaan").val(formatRupiah(parseInt(harga_objek_pendanaan).toString(), 'Rp'));
                $("#uang_muka").val(formatRupiah(parseInt(uang_muka).toString(), 'Rp'));
                $("#nilai_pengajuan_pendanaan").val(formatRupiah(parseInt(nilai_pengajuan_pendanaan).toString(), 'Rp'));
                $("#jangka_waktu").val(jangka_waktu);
                // $("#detail_informasi_pendanaan").val(detail_informasi_pendanaan);

                let nama_pemilik = data.data_pengajuan.nm_pemilik;
                let no_telp_pemilik = data.data_pengajuan.no_tlp_pemilik;
                let alamat_pemilik = data.data_pengajuan.alamat_pemilik;
                let provinsi_pemilik = data.data_pengajuan.provinsi_pemilik;
                let kota_pemilik = data.data_pengajuan.kota_pemilik;
                let kecamatan_pemilik = data.data_pengajuan.kecamatan_pemilik;
                let kelurahan_pemilik = data.data_pengajuan.kelurahan_pemilik;
                let kode_pos_pemilik = data.data_pengajuan.kd_pos_pemilik;

                $("#nama_pemilik").val(nama_pemilik);
                $("#no_telp_pemilik").val(no_telp_pemilik);
                $("#alamat_pemilik").val(alamat_pemilik);
                $("#provinsi_pemilik").val(provinsi_pemilik);
                $("#kota_pemilik").val(kota_pemilik);
                $("#kecamatan_pemilik").val(kecamatan_pemilik);
                $("#kelurahan_pemilik").val(kelurahan_pemilik);
                $("#kode_pos_pemilik").val(kode_pos_pemilik);
            }
        });


    });

     // Rupiah Currency Format
    const formatRupiah = (angka, prefix) => {
        let thisValue = angka.replace(/[^,\d]/g, '')
        angka = (parseInt(thisValue, 10)).toString()

        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
    }


    const fullText = () => {
        $('#detail_informasi_pendanaan').html(`
            ${full_text} <a href="" id="summary-text-pendanaan" class="text-primary" onclick="summaryText(); return false">lihat lebih sedikit...</a>
        `)
    }
    const summaryText = () => {
        $('#detail_informasi_pendanaan').html(`
            ${text_summary} <a href="" id="more-text-pendanaan" class="text-primary" onclick="fullText(); return false">selengkapnya...</a>
        `)
    } 

</script>
@endsection