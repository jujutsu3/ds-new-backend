<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Dokumen Legalitas</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_dokumen_legalitasi" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis Dokumen</th>
                                <th>Dokumen Pendukung</th>
                                <th>Ceklis Dokumen</th>
                                <th>Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Dokumen Pekerjaan</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_dokumen_pekerjaan" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis Dokumen</th>
                                <th>Dokumen Pendukung</th>
                                <th>Ceklis Dokumen</th>
                                <th>Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Dokumen Penghasilan</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_dokumen_penghasilan" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis Dokumen</th>
                                <th>Dokumen Pendukung</th>
                                <th>Ceklis Dokumen</th>
                                <th>Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Dokumen Jaminan</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_dokumen_jaminan" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis Dokumen</th>
                                <th>Dokumen Pendukung</th>
                                <th>Ceklis Dokumen</th>
                                <th>Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // START: Global Var
    const data = `{!! $data_dokumen_ceklist !!}`
    const data_chk = data ? JSON.parse(data) : []
    // END: Global Var
    
    /***
     * no              => urutan tampilan 
     * doc_pendukung   => 0 = tidak ada, 1 = hanya satu, 2 = terdapat lebih dari satu
     * file_type       => tipe file yang diperbolehkan
     * table_name      => nama table yang menyimpan path dokumen
     * field_name      => nama field yang menyimpan path dokumen
     * jenis_dokumen   => Text yang tampil pada tampilan
     ***/
    $(document).ready(function(){
        // START: Dokumen Legalitas
        let data_dokumen_legalitas = [
            {"no":"1", "doc_pendukung":"0", "file_type":"", "table_name":"","field_name":"","jenis_dokumen":"Asli form aplikasi permohonan Pendanaan Dana Rumah(Di isi melalui sistem)"},

            {"no":"2", "doc_pendukung":"2", "file_type":"jpeg|jpg|png|bmp", "table_name":[
                {
                    'title': 'KTP Calon Penerima Pendanaan',
                    'table_name': 'brw_user_detail_pengajuan'
                },
                {
                    'title': 'KTP Pasangan',
                    'table_name': 'brw_dokumen_legalitas_pribadi'
                }
            ],"field_name":[
                {
                    'title': 'KTP Calon Penerima Pendanaan',
                    'field_name': 'brw_pic_ktp'
                },
                {
                    'title': 'KTP Pasangan',
                    'field_name': 'ktp_pasangan'
                }
            ],"jenis_dokumen":"KTP Calon Penerima Pendanaan dan suami/istri"},

            {"no":"4", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi","field_name":"kartu_keluarga","jenis_dokumen":"Kartu Keluarga"},

            {"no":"6", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan","field_name":"surat_pemesanan_rumah","jenis_dokumen":"Surat Pemesanan Rumah (SPR) yang telah di ttd dan Bukti Bayar DP (Print Out Rekening)"},

            {"no":"7", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi","field_name":"surat_beda_nama","jenis_dokumen":"Asli surat keterangan beda nama atau tanggal lahir antara KTP / KK / Akta Nikah (jika berbeda salah satu nya) dari Kelurahan"},

            {"no":"8", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi","field_name":"perjanjian_pra_nikah","jenis_dokumen":"Perjanjian pra nikah Penerima Pendanaan dari notaris yang di daftarkan ke KUA / catatan sipil (jika menikah dengan WNA atau membuat perjanjian pra nikah atas permintaan sendiri sebelum menikah) - Jika Ada"},
        ];

        // start: skema pembiayaan
        let data_dokumen_legalitas_joint_income = [
            {"no":"3", "doc_pendukung":"2", "file_type":"jpeg|jpg|png|bmp", "table_name":[
                
                {
                    'title': 'NPWP Pribadi',
                    'table_name': 'brw_user_detail_pengajuan'
                },
                {
                    'title': 'KTP Pasangan',
                    'table_name': 'brw_dokumen_legalitas_pribadi'
                }
                
            ],"field_name":[
                
                {
                    'title': 'NPWP Pribadi',
                    'field_name': 'brw_pic_npwp'
                },
                {
                    'title': 'KTP Pasangan',
                    'field_name': 'npwp_pasangan'
                }
                
            ],"jenis_dokumen":"NPWP Pribadi(suami/istri jika joint income dilampirkan)"},
        ]

        let data_dokumen_legalitas_single_income = [
            {"no":"3", "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_user_detail_pengajuan","field_name":"brw_pic_npwp","jenis_dokumen":"NPWP Pribadi(suami/istri jika joint income dilampirkan)"},
        ]
        // end: skema pembiayaan

        // start: status menikah
        let data_dokumen_legalitas_belum_menikah = [
            {"no":"5", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_keterangan_belum_menikah", "jenis_dokumen":"Akta Nikah / Akta Perkawinan / Akta Cerai / Akta Perceraian / Surat Keterangan Belum Menikah / Surat Kematian dari RS atau Kelurahan"},
        ]
        let data_dokumen_legalitas_cerai = [
            {"no":"5", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi","field_name":"akta_cerai","jenis_dokumen":"Akta Nikah / Akta Perkawinan / Akta Cerai / Akta Perceraian / Surat Keterangan Belum Menikah / Surat Kematian dari RS atau Kelurahan"},
        ]
        let data_dokumen_legalitas_menikah = [
            {"no":"5", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi","field_name":"buku_nikah","jenis_dokumen":"Akta Nikah / Akta Perkawinan / Akta Cerai / Akta Perceraian / Surat Keterangan Belum Menikah / Surat Kematian dari RS atau Kelurahan"},
        ]
        // end: status menikah
        
        let data_dokumen_legalitas_status = is_single_income == 2 ? [...data_dokumen_legalitas, ...data_dokumen_legalitas_joint_income] : [...data_dokumen_legalitas, ...data_dokumen_legalitas_single_income]
        
        if (status_kawin == 1) { // menikah
            data_dokumen_legalitas_status.push(...data_dokumen_legalitas_menikah)
        } else if(status_kawin == 2) { // belum menikah
            data_dokumen_legalitas_status.push(...data_dokumen_legalitas_belum_menikah)
        } else { // cerai
            data_dokumen_legalitas_status.push(...data_dokumen_legalitas_cerai)
        }
        
        // Sort by no
        data_dokumen_legalitas_status.sort((a, b) => (a.no - b.no))

        let table_dokumen_legalitasi = $('#table_dokumen_legalitasi').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            data : data_dokumen_legalitas_status,
            columns:[
                {className: 'align-middle text-center', data : "no"},
                {className: 'align-middle', data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name}" data-fieldName="${data.field_name}" data-fileType="${data.file_type}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File
                                </button>
                            `
                        } else if (data.doc_pendukung == 2) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name[0].table_name}" 
                                    data-tableName-title="${data.table_name[0].title}" 
                                    data-fieldName="${data.field_name[0].field_name}" 
                                    data-fileType="${data.file_type}"
                                    data-tableName-2="${data.table_name[1].table_name}" 
                                    data-tableName-2-title="${data.table_name[1].title}"
                                    data-fieldName-2="${data.field_name[1].field_name}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File  
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_legal_verif = data_chk[`dokumen_legalitas_${row_number}`]
                        return `<input type="checkbox" class="_check" id="doc_legal_${row_number}" name="doc_legal_${row_number}" value="1" ${doc_legal_verif == 1 || row_number == 1 ? 'checked' : ''} onclick="showHasilVerif('doc_legal_${row_number}', 'doc_legal_${row_number}_hasil')">`;
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_legal_verif = data_chk[`dokumen_legalitas_${row_number}`]
                        let doc_legal_hasil = data_chk[`dokumen_legalitas_${row_number}_hasil`] ? data_chk[`dokumen_legalitas_${row_number}_hasil`] : ''
                        
                        return `<input type="text" class="_check form-control ${doc_legal_verif == 1 || row_number == 1 ? '' : 'd-none'}" id="doc_legal_${row_number}_hasil" name="doc_legal_${row_number}_hasil" value="${doc_legal_hasil ? doc_legal_hasil : ''}">`;
                    },
                    width: "20%"
                },
            ]
        });
        // END: Dokumen Legalitas

        // START: Dokumen Pekrjaan
        let data_dokumen_pekerjaan = [
            {"no":"1", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_bekerja", "jenis_dokumen":"SK Terakhir/Surat Keterangan Kerja Asli yang Menyatakan Karyawan Tetap"}
        ];
        let table_dokumen_pekerjaan = $('#table_dokumen_pekerjaan').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            data : data_dokumen_pekerjaan,
            columns:[
                {className: 'align-middle text-center', data : "no"},
                {className: 'align-middle', data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file"
                                data-toggle="modal"
                                data-tableName="${data.table_name}"
                                data-fieldName="${data.field_name}"
                                data-fileType="${data.file_type}"
                                data-target="#modal-doc-pendukung">
                                    Lihat File 
                                </button>
                            `
                        } else if (data.doc_pendukung == 2) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name[0].table_name}" 
                                    data-tableName-title="${data.table_name[0].title}" 
                                    data-fieldName="${data.field_name[0].field_name}" 
                                    data-fileType="${data.file_type}"
                                    data-tableName-2="${data.table_name[1].table_name}" 
                                    data-tableName-2-title="${data.table_name[1].title}"
                                    data-fieldName-2="${data.field_name[1].field_name}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File  
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_pekerjaan_verif = data_chk[`dokumen_pekerjaan_${row_number}`]
                        return `<input type="checkbox" class="_check" id="doc_pekerjaan_${row_number}" name="doc_pekerjaan_${row_number}" value="1" ${doc_pekerjaan_verif == 1 ? 'checked' : ''} onclick="showHasilVerif('doc_pekerjaan_${row_number}', 'doc_pekerjaan_${row_number}_hasil')">`;
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_pekerjaan_verif = data_chk[`dokumen_pekerjaan_${row_number}`]
                        let doc_pekerjaan_hasil = data_chk[`dokumen_pekerjaan_${row_number}_hasil`] ? data_chk[`dokumen_pekerjaan_${row_number}_hasil`] : ''
                        return `<input type="text" class="_check form-control ${doc_pekerjaan_verif == 1 ? '' : 'd-none'}" id="doc_pekerjaan_${row_number}_hasil" name="doc_pekerjaan_${row_number}_hasil" value="${doc_pekerjaan_hasil ? doc_pekerjaan_hasil : ''}">`;
                    },
                    width: "20%"
                },
            ]
        });
        // END: Dokumen Pekrjaan

        // START: Dokumen Penghasilan
        let data_dokumen_penghasilan = [
            {"no":"3", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"spt", "jenis_dokumen":"SPT Pajak Tahun Terakhir Penerima Pendanaan dan Pasangan(Jika Joint Income)"}
        ];

        // start: skema pembiayaan
        let data_dokumen_penghasilan_joint_income = [
            {"no":"1", "doc_pendukung":"2", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":[
                {
                    'title': 'Slip gaji penerima pendanaan',
                    'table_name': 'brw_dokumen_legalitas_pribadi'
                },
                {
                    'title': 'Slip gaji pasangan',
                    'table_name': 'brw_dokumen_legalitas_pribadi'
                }
            ], "field_name":[
                {
                    'title': 'Slip gaji penerima pendanaan',
                    'field_name': 'slip_gaji'
                },
                {
                    'title': 'Slip gaji pasangan',
                    'field_name': 'slip_gaji_pasangan'
                }
            ], "jenis_dokumen":"Asli Slip Gaji Minimal 3 Bulan Terakhir Penerima Pendanaan dan Pasangan(Jika Joint Income)"},
            {"no":"2", "doc_pendukung":"2", "file_type":"pdf", "table_name":[
                 {
                    'title': 'Mutasi rekening penerima pendanaan',
                    'table_name': 'brw_dokumen_legalitas_pribadi'
                },
                {
                    'title': 'Mutasi rekening pasangan',
                    'table_name': 'brw_dokumen_legalitas_pribadi'
                },
            ], "field_name":[
                {
                    'title': 'Mutasi rekening penerima pendanaan',
                    'field_name': 'mutasi_rekening'
                },
                {
                    'title': 'Mutasi rekening pasangan',
                    'field_name': 'mutasi_rekening_pasangan'
                }
            ], "jenis_dokumen":"Mutasi Rekening Tabungan Pribadi Penerima Pendanaan dan Pasangan Penerima Pendanaan (Jika Joint Income) Yang Mencerminkan Gaji / Pendapatan 3 Bulan Terakhir"},

        ]

        let data_dokumen_penghasilan_single_income = [
            {"no":"1", "doc_pendukung":"2", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"slip_gaji", "jenis_dokumen":"Asli Slip Gaji Minimal 3 Bulan Terakhir Penerima Pendanaan dan Pasangan(Jika Joint Income)"},
            {"no":"2", "doc_pendukung":"2", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"mutasi_rekening", "jenis_dokumen":"Mutasi Rekening Tabungan Pribadi Penerima Pendanaan dan Pasangan Penerima Pendanaan (Jika Joint Income) Yang Mencerminkan Gaji / Pendapatan 3 Bulan Terakhir"},
        ]
        // end: skema pembiayaan

        let data_dokumen_penghasilan_status = is_single_income == 2 ? [...data_dokumen_penghasilan, ...data_dokumen_penghasilan_joint_income] : [...data_dokumen_penghasilan, ...data_dokumen_penghasilan_single_income]
        // Sort by no
        data_dokumen_penghasilan_status.sort((a, b) => (a.no - b.no))

        let table_dokumen_penghasilan = $('#table_dokumen_penghasilan').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            data : data_dokumen_penghasilan_status,
            columns:[
                {className: 'align-middle text-center', data : "no"},
                {className: 'align-middle', data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file"
                                data-toggle="modal"
                                data-tableName="${data.table_name}"
                                data-fieldName="${data.field_name}"
                                data-fileType="${data.file_type}"
                                data-target="#modal-doc-pendukung">
                                    Lihat File 
                                </button>
                            `
                        } else if (data.doc_pendukung == 2) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name[0].table_name}" 
                                    data-tableName-title="${data.table_name[0].title}" 
                                    data-fieldName="${data.field_name[0].field_name}" 
                                    data-fileType="${data.file_type}"
                                    data-tableName-2="${data.table_name[1].table_name}" 
                                    data-tableName-2-title="${data.table_name[1].title}"
                                    data-fieldName-2="${data.field_name[1].field_name}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File  
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_penghasilan_verif = data_chk[`dokumen_penghasilan_${row_number}`]
                        return `<input type="checkbox" class="_check" id="doc_penghasilan_${row_number}" name="doc_penghasilan_${row_number}" value="1" ${doc_penghasilan_verif == 1 ? 'checked' : ''} onclick="showHasilVerif('doc_penghasilan_${row_number}', 'doc_penghasilan_${row_number}_hasil')">`;
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_penghasilan_verif = data_chk[`dokumen_penghasilan_${row_number}`]
                        let doc_penghasilan_hasil = data_chk[`dokumen_penghasilan_${row_number}_hasil`] ? data_chk[`dokumen_penghasilan_${row_number}_hasil`] : ''
                        return `<input type="text" class="_check form-control ${doc_penghasilan_verif == 1 ? '' : 'd-none'}" id="doc_penghasilan_${row_number}_hasil" name="doc_penghasilan_${row_number}_hasil" value="${doc_penghasilan_hasil ? doc_penghasilan_hasil : ''}">`;
                    },
                    width: "20%"
                },
            ]
        });
        // END: Dokumen Penghasilan

        // START: Dokumen Jaminan
        let data_dokumen_jaminan = [
            {"no":"1", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"sertifikat", "jenis_dokumen":"Sertifikat Objek Agunan(SHM/SHGB/Bukti Pemesanan untuk Rumah Baru)"},
            {"no":"2", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"imb", "jenis_dokumen":"IMB"},
            {"no":"3", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"pbb", "jenis_dokumen":"PBB Tahun Terakhir"},
            {"no":"4", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"ktp_pemilik", "jenis_dokumen":"Jika Penjual Perorangan / Rumah Second maka wajib dilampirkan : KTP (Suami / Istri), KK, Surat Nikah, NPWP"}
        ];
        let table_dokumen_jaminan = $('#table_dokumen_jaminan').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            "searchable": false,								   
            "paging":   false,					 
            bFilter: false,
            "bPaginate": false,
            "bInfo": false,
            autoWidth: true,
            data : data_dokumen_jaminan,
            columns:[
                {className: 'align-middle text-center', data : "no"},
                {className: 'align-middle', data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file"
                                data-toggle="modal"
                                data-tableName="${data.table_name}"
                                data-fieldName="${data.field_name}"
                                data-fileType="${data.file_type}"
                                data-target="#modal-doc-pendukung">
                                    Lihat File 
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_jaminal_verif = data_chk[`dokumen_jaminan_${row_number}`]
                        return `<input type="checkbox" class="_check" id="doc_jaminan_${row_number}" name="doc_jaminan_${row_number}" value="1" ${doc_jaminal_verif == 1 ? 'checked' : ''} onclick="showHasilVerif('doc_jaminan_${row_number}', 'doc_jaminan_${row_number}_hasil')">`;
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let row_number = data.no
                        let doc_jaminal_verif = data_chk[`dokumen_jaminan_${row_number}`]
                        let doc_jaminal_hasil = data_chk[`dokumen_jaminan_${row_number}_hasil`] ? data_chk[`dokumen_jaminan_${row_number}_hasil`] : ''
                        return `<input type="text" class="_check form-control ${doc_jaminal_verif == 1 ? '' : 'd-none'}" id="doc_jaminan_${row_number}_hasil" name="doc_jaminan_${row_number}_hasil" value="${doc_jaminal_hasil ? doc_jaminal_hasil : ''}">`;
                    },
                    width: "20%"
                },
            ]
        });
        // END: Dokumen Jaminan

        // START: DOM Manipulation
        
        // END: DOM Manipulation
    });
    // END: Upload File
</script>
@endpush