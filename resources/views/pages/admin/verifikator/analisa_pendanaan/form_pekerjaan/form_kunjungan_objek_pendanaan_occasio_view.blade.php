<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Objek Pendanaan</label>
    </div>
    <div class="card-body">
        <div class="mt-2">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nama_penerima_dana">Nama Penerima Dana</label>
                        <input type="text" class="form-control" name="nama_penerima_dana" id="nama_penerima_dana"
                            placeholder="-" value="{{ !empty($data) ? $data->nama : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pendanaan_dana_dibutuhkan">Plafond</label>
                        <input type="text" class="form-control" name="pendanaan_dana_dibutuhkan"
                            id="pendanaan_dana_dibutuhkan" placeholder="-"
                            value="{{ !empty($data) ? 'Rp' .str_replace(',', '.', number_format($data->pendanaan_dana_dibutuhkan)) : '-' }}"
                            readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tanggal_kunjungan">Tanggal Kunjungan</label>
                        <input type="date" class="form-control" name="tanggal_kunjungan" id="tanggal_kunjungan"
                            placeholder="Ketik disiini" value="{{ !empty($data) ? $data->tanggal_kunjungan : '-' }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jenis_objek_pendanaan">Jenis Jaminan Berupa</label>
                        <select name="jenis_objek_pendanaan" id="jenis_objek_pendanaan"
                            class="form-contral custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_agunan) ? $data_brw_agunan->jenis_objek_pendanaan ==
                                '1' ? 'selected' : '' : '' }}>
                                Tanah
                                Kosong</option>
                            <option value="2" {{ !empty($data_brw_agunan) ? $data_brw_agunan->jenis_objek_pendanaan ==
                                '2' ? 'selected' : '' : '' }}>
                                Tanah
                                & Bangunan</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jenis_agunan">Bukti Kepemilikan</label>
                        <select class="form-control custom-select" name="jenis_agunan" id="jenis_agunan">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_agunan) ? $data_brw_agunan->jenis_agunan == '1' ?
                                'selected' : '' : '' }}>
                                SHM
                            </option>
                            <option value="2" {{ !empty($data_brw_agunan) ? $data_brw_agunan->jenis_agunan == '2' ?
                                'selected' : '' : '' }}>
                                SHGB
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nomor_agunan">Nomor Sertifikat</label>
                        <input type="text" class="form-control" name="nomor_agunan" id="nomor_agunan"
                            placeholder="ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->nomor_agunan : '' }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="atas_nama_agunan">Atas Nama</label>
                        <input type="text" class="form-control" name="atas_nama_agunan" id="atas_nama_agunan"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->atas_nama_agunan : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="luas_tanah">Luas Tanah(<sup>2</sup>)</label>
                        <input type="text" class="form-control" name="luas_tanah" id="luas_tanah"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->luas_tanah : '' }}"
                            onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="luas_bangunan">Luas Bangunan(m<sup>2</sup>)</label>
                        <input type="text" class="form-control" name="luas_bangunan" id="luas_bangunan"
                            placeholder="ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->luas_bangunan : '' }}"
                            onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nomor_surat_ukur">Nomor Gambar Situasi/Surat Ukur</label>
                        <input type="text" class="form-control" name="nomor_surat_ukur" id="nomor_surat_ukur"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->nomor_surat_ukur : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tanggal_surat_ukur">Tanggal Gambar Situasi/Surat Ukur</label>
                        <input type="date" class="form-control" name="tanggal_surat_ukur" id="tanggal_surat_ukur"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->tanggal_surat_ukur : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="geocode">Koordinat GPS</label>
                        <input type="text" class="form-control" name="geocode" id="geocode" placeholder="Ketik disini"
                            value="{{ !empty($data) ? $data->geocode : '' }}">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <div class="form-check form-check-inline">
            <input class="form-check-input ml-2" type="checkbox" id="is_status_imb" name="is_status_imb" value="1" {{
                !empty($data_brw_agunan) ? $data_brw_agunan->no_imb ? 'checked' : '' : ''
            }}>
            <label class="form-check-label" for="is_status_imb">Memiliki IMB(Izin Mendirikan
                Bangunan)</label>
        </div>
    </div>
    <div class="card-body">
        <div id="layout-imb" class="mt-2 d-none">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="status_imb">Status IMB</label>
                        <select name="status_imb" id="status_imb" class="form-control custom-select" required>
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_agunan) ? $data_brw_agunan->status_imb == '1' ?
                                'selected' : '' : '' }}>
                                Pecahan</option>
                            <option value="2" {{ !empty($data_brw_agunan) ? $data_brw_agunan->status_imb == '2' ?
                                'selected' : '' : '' }}>
                                Induk</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="no_imb">Nomor IMB</label>
                        <input type="text" class="form-control" name="no_imb" id="no_imb" placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->no_imb : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="luas_tanah_imb">Luas Tanah Sesuai IMB(m<sup>2</sup>)</label>
                        <input type="text" class="form-control" name="luas_tanah_imb" id="luas_tanah_imb"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->luas_tanah_imb : '' }}"
                            onkeyup="this.value = this.value.replace(/[^0-9,]/g, '');">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="luas_bangunan_imb">Luas Bangunan Sesuai IMB(m<sup>2</sup>)</label>
                        <input type="text" class="form-control" name="luas_bangunan_imb" id="luas_bangunan_imb"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->luas_bangunan_imb : '' }}"
                            onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tanggal_terbit_imb">Tanggal IMB</label>
                        <input type="date" class="form-control" name="tanggal_terbit_imb" id="tanggal_terbit_imb"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->tanggal_terbit_imb : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="atas_nama_imb">Tertulis atas nama di IMB</label>
                        <input type="text" class="form-control" name="atas_nama_imb" id="atas_nama_imb"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_agunan) ? $data_brw_agunan->atas_nama_imb : '' }}">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Data Lainnya</label>
    </div>
    <div class="card-body">
        <div class="mt-2">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="telepon_rumah">Nomor Telepon Rumah</label>
                        <input type="text" class="form-control" name="telepon_rumah" id="telepon_rumah"
                            placeholder="Ketik disini" value="{{ !empty($data) ? $data->telepon_rumah : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="daya_listrik">Daya Listrik(Kwh)</label>
                        <input type="text" class="form-control" name="daya_listrik" id="daya_listrik" placeholder="Kwh"
                            value="{{ !empty($data) ? $data->daya_listrik : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="air">Air</label>
                        <select name="air" id="air" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data) ? $data->air == '1' ? 'selected' : '' : ''}}>PAM-PDAM
                            </option>
                            <option value="2" {{ !empty($data) ? $data->air == '2' ? 'selected' : '' : ''}}>JetPump
                            </option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="internet_wifi">Jaringan Internet/Wifi</label>
                        <select name="internet_wifi" id="internet_wifi" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data) ? $data->internet_wifi == '1' ? 'selected' : '' : ''}}>Ya
                            </option>
                            <option value="2" {{ !empty($data) ? $data->internet_wifi == '2' ? 'selected' : '' : ''}}>
                                Tidak</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Kondisi Rumah</label>
    </div>
    <div class="card-body">
        <div class="mt-2">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="is_bangunan_bertingkat">Apakah bentuk bangunan bertingkat?</label>
                        <select name="is_bangunan_bertingkat" id="is_bangunan_bertingkat"
                            class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1">Ya</option>
                            <option value="2">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4" id="layout-jumllah-lantai">
                    <div class="form-group">
                        <label for="jumlah_lantai">Jumlah Lantai</label>
                        <select name="jumlah_lantai" id="jumlah_lantai" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">Lebih dari 3</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jenis_properti">Jenis Rumah</label>
                        <select name="jenis_properti" id="jenis_properti" class="form-control custom-select">
                            <option value="">-- Pilih Satu</option>
                            <option value="1">Baru</option>
                            <option value="2">Bekas</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4" id="layout-rumah-baru">
                    <div class="form-group">
                        <label for="dibangun_tahun">Dibangun pada tahun?</label>
                        <input type="text" class="form-control" name="dibangun_tahun" id="dibangun_tahun"
                            placeholder="Ketik disini" value="{{ !empty($data) ? $data->dibangun_tahun : '' }}">
                    </div>
                </div>
                <div class="col-md-4 layout-rumah-bekas" id="layout-rumah-bekas">
                    <div class="form-group">
                        <label for="is_renovasi">Pernah Renovasi?</label>
                        <select name="is_renovasi" id="is_renovasi" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1">Ya</option>
                            <option value="2">Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 layout-rumah-bekas" id="layout-renovasi-tahun">
                    <div class="form-group">
                        <label for="renovasi_tahun">Renovasi pada tahun?</label>
                        <input type="text" class="form-control" name="renovasi_tahun" id="renovasi_tahun"
                            placeholder="Ketik disini" value="{{ !empty($data) ? $data->renovasi_tahun : '' }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="akses_jalan">Akses Jalan</label>
                        <select name="akses_jalan" id="akses_jalan" name="akses_jalan"
                            class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1">Aspal</option>
                            <option value="2">Batu</option>
                            <option value="3">Tanah</option>
                            <option value="4">Konblok</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="lebar_jalan">Lebar Jalan</label>
                        <input type="text" class="form-control" name="lebar_jalan" id="lebar_jalan"
                            placeholder="Ketik disini" value="{{ !empty($data) ? $data->lebar_jalan : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kode_pos">Lingkungan Sekitar Objek Pendanaan</label>
                        <select name="lingkungan_sekitar" id="lingkungan_sekitar" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1">Pemukiman</option>
                            <option value="2">Persawahan</option>
                            <option value="3">Perkebunan</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="dihuni_oleh">Dihuni Oleh</label>
                        <select name="dihuni_oleh" id="dihuni_oleh" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1">Kosong</option>
                            <option value="2">Sendiri</option>
                            <option value="3">Orang Lain</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4" id="layout-hubungan-sebagai">
                    <div class="form-group">
                        <label for="hubungan_sebagai">Hubungan Sebagai</label>
                        <select name="hubungan_sebagai" id="hubungan_sebagai" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            @foreach ($m_hub as $item)
                            <option value="{{ $item->id_hub_ahli_waris }}" {{ !empty($data->hubungan_sebagai) ?
                                $item->id_hub_ahli_waris == $data->hubungan_sebagai ? 'selected' : '' : ''}}>
                                {{ $item->jenis_hubungan }}</option>
                            @endforeach
                        </select>
                        {{-- <input type="text" class="form-control" name="hubungan_sebagai" id="hubungan_sebagai"
                            placeholder="Ketik disini" value="{{ !empty($data) ? $data->hubungan_sebagai : '' }}"> --}}
                    </div>
                </div>
                <div class="col-md-4" id="layout-dikontrak-atau-sewa">
                    <div class="form-group">
                        <label for="dikontrak_atau_sewa">Apakah dikontrakan/disewakan?</label>
                        <select name="dikontrak_atau_sewa" id="dikontrak_atau_sewa" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1">Ya</option>
                            <option value="2">Tidak</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4" id="layout-tanggal-berakhi-_kontrak">
                    <div class="form-group">
                        <label for="tanggal_berakhir_kontrak">Tanggal berakhirnya sewa/kontrak</label>
                        <input type="date" class="form-control" name="tanggal_berakhir_kontrak"
                            id="tanggal_berakhir_kontrak" placeholder="Ketik disini"
                            value="{{ !empty($data) ? $data->tanggal_berakhir_kontrak : '' }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="informasi_lain_lain">Informasi Lain</label>
                        <textarea class="form-control" id="informasi_lain_lain" name="informasi_lain_lain" rows="4"
                            placeholder="Ketik disini">{{ !empty($data) ? $data->informasi_lain_lain : '' }}</textarea>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Informasi Narasumber</label>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">

            </div>
        </div>
        <div class="row mt-2">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="narasumber_nama">Nama</label>
                    <input type="text" class="form-control" name="narasumber_nama" id="narasumber_nama"
                        placeholder="Ketik disini" value="{{ !empty($data) ? $data->narasumber_nama : '' }}"
                        onkeyup="this.value = this.value.replace(/[^0-9 a-z A-Z\.\,]/g, '')">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="narasumber_selaku">Selaku</label>
                    <input type="text" class="form-control" name="narasumber_selaku" id="narasumber_selaku"
                        placeholder="Ketik disini" value="{{ !empty($data) ? $data->narasumber_selaku : '' }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="narasumber_tlp_hp">Nomor telepon/HP</label>
                    <input type="text" class="form-control" name="narasumber_tlp_hp" minlength="9" maxlength="16" id="narasumber_tlp_hp"
                        placeholder="Ketik disini" value="{{ !empty($data) ? $data->narasumber_tlp_hp : '' }}">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Foto kondisi objek pendanaan pada
            saat ini</label>
    </div>
    <div class="card-body">
        <div class="form-group row mt-2">
            <label for="objek_depan" class="col-md-8 my-auto">Tampak Depan Objek Pendanaan</label>
            <div class="col-md-4 my-auto">
                <div class="text-center">
                    <input type="file" name="objek_depan" id="objek_depan" class="d-none" accept=".png, .jpg, .jpeg">

                    <img id="preview-image" class="img-fluid mb-3" />

                    @if (!empty($data) && $data->pic_tampak_depan_objek )
                    <button id="show_objek_depan" type="button" class="btn btn-success mx-auto" data-toggle="modal"
                        data-target="#modal-image" data-image-id="objek_depan">
                        Lihat Foto</button>
                    @else
                    <button type="button" id="btn_show_objek_depan" class="btn btn-secondary mx-auto"
                        onclick="$('#objek_depan').trigger('click');">Unggah
                        Foto</button>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="objek_dalam" class="col-md-8 my-auto">Tampak Dalam Objek Pendanaan</label>

            <div class="col-md-4 my-auto">
                <div class="text-center">
                    <input type="file" name="objek_dalam" id="objek_dalam" class="d-none" accept=".png, .jpg, .jpeg">

                    <img id="preview-image2" class="img-fluid mb-3" />

                    @if (!empty($data) && $data->pic_tampak_dalam_objek )
                    <button id="show_objek_dalam" type="button" class="btn btn-success mx-auto" data-toggle="modal"
                        data-target="#modal-image" data-image-id="objek_dalam">
                        Lihat Foto</button>
                    @else
                    <button type="button" id="btn_show_objek_dalam" class="btn btn-secondary mx-auto"
                        onclick="$('#objek_dalam').trigger('click');">Unggah
                        Foto</button>
                    @endif
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-8 my-auto">
                <label for="objek_lingkungan">Tampak Kondisi Lingkungan Objek Pendanaan</label>
            </div>
            <div class="col-md-4 my-auto">
                <div class="text-center">
                    <input type="file" name="objek_lingkungan" id="objek_lingkungan" class="d-none"
                        accept=".png, .jpg, .jpeg">

                    <img id="preview-image3" class="img-fluid mb-3" />

                    @if (!empty($data) && $data->pic_tampak_kondisi_lingkungan_objek )
                    <button id="show_objek_lingkungan" type="button" class="btn btn-success mx-auto" data-toggle="modal"
                        data-target="#modal-image" data-image-id="objek_lingkungan">
                        Lihat Foto</button>
                    @else
                    <button type="button" id="btn_show_objek_lingkungan" class="btn btn-secondary mx-auto"
                        onclick="$('#objek_lingkungan').trigger('click');">Unggah
                        Foto</button>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 my-auto">
                <label for="foto_petugas_depan_objek">Foto petugas di depan Objek Pendanaan beserta orang yang
                    ditemui</label>
            </div>
            <div class="col-md-4 my-auto">
                <div class="text-center">
                    <input type="file" name="foto_petugas_depan_objek" id="foto_petugas_depan_objek" class="d-none"
                        accept=".png, .jpg, .jpeg">

                    <img id="preview-image4" class="img-fluid mb-3" />

                    @if (!empty($data) && $data->pic_petugas )
                    <button id="show_foto_petugas_depan_objek" type="button" class="btn btn-success mx-auto"
                        data-toggle="modal" data-target="#modal-image" data-image-id="foto_petugas_depan_objek">
                        Lihat Foto</button>
                    @else
                    <button type="button" id="btn_show_foto_petugas_depan_objek" class="btn btn-secondary mx-auto"
                        onclick="$('#foto_petugas_depan_objek').trigger('click');">Unggah
                        Foto</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

{{-- START: Modal Show Photo --}}
<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-image-title"></h5>
                <button type="button" id="btn-close" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="modal-image-src" src="" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" id="btn-unggah-ulang" class="btn btn-primary">Unggah Ulang</button>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal Show Photo --}}

@push('scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){
        let status_imb = "{{ !empty($data_brw_agunan->no_imb) ?  '1' : '0'}}"
        if (status_imb == '1' ) {
            $('[name="is_status_imb"][value="' + status_imb + '"]').prop('checked', true);
            $('#layout-imb').removeClass('d-none')
        } else {
            $('#layout-imb').addClass('d-none')
        }

        $('#is_status_imb').change(function () {
            let is_checked = $(this).is(":checked")
            if (is_checked) {
                $('#layout-imb').removeClass('d-none')
            } else {
                $('#layout-imb').addClass('d-none')
            }
        })

        // START: Rumah Bertingkat
        let jumlah_lantai = "{!! $data ? $data->jumlah_lantai : '' !!}"
        if (jumlah_lantai > 0) {
            $('#is_bangunan_bertingkat option[value="1"]').prop('selected',true);
            $(`#jumlah_lantai option[value="${jumlah_lantai}"]`).prop('selected',true);
        } else {
            $('#is_bangunan_bertingkat option[value="2"]').prop('selected',true);
            $('#layout-jumllah-lantai').addClass('d-none')
        }

        $('#is_bangunan_bertingkat').change(function() { //Jika bangunan bertingkat, pilih jumlah lantai
            let this_value = $('#is_bangunan_bertingkat option:selected').val()
            if (this_value == '2') {
                $('#layout-jumllah-lantai').addClass('d-none')
            } else {
                $('#layout-jumllah-lantai').removeClass('d-none')
            }
        })
        // END: Rumah Bertingkat

        // START: Jenis Rumah
        let jenis_properti = "{{ !empty($data) ? $data->jenis_properti : '' }}"
        let renovasi_tahun = "{{ !empty($data) ? $data->renovasi_tahun : '' }}"
        $(`#jenis_properti option[value="${jenis_properti}"]`).prop('selected',true);

        if (jenis_properti == '1') { //Jika jenis properti baru

            $('#layout-rumah-baru').removeClass('d-none')
            $('.layout-rumah-bekas').addClass('d-none')
        } else { //Jika jenis properti bekas

            if (renovasi_tahun) {
                $('#is_renovasi option[value="1"]').prop('selected',true);
                $('#layout-renovasi-tahun').removeClass('d-none')
            } else {
                $('#is_renovasi option[value="2"]').prop('selected',true);
                $('#layout-renovasi-tahun').addClass('d-none')
            }
            $('#layout-rumah-baru').addClass('d-none')
        }

        $('#jenis_properti').change(function() {
            let this_value = $('#jenis_properti option:selected').val()
            if (this_value == '1') { //Jika jenis properti baru, masukkan tahun dibangun
                $('#layout-rumah-baru').removeClass('d-none')
                $('.layout-rumah-bekas').addClass('d-none')
                // $('#layout-rumah-baru').removeClass('d-none')
            } else {
                $('.layout-rumah-bekas').removeClass('d-none')
                $('#layout-rumah-baru').addClass('d-none')
                // $('#layout-rumah-baru').addClass('d-none')
            }
        })

        $('#is_renovasi').change(function () {
            let this_value = $('#is_renovasi option:selected').val()
            if (this_value == '1') { // Jika pernah renovasi, masukan tahun renovasi
                $('#layout-renovasi-tahun').removeClass('d-none')
            } else {
                $('#layout-renovasi-tahun').addClass('d-none')
            }
        })
        // END: Jenis Rumah

        // START: Akses Jalan
        let akses_jalan = "{{ !empty($data) ? $data->akses_jalan : '' }}"
        if (akses_jalan) {
            $(`#akses_jalan option[value="${akses_jalan}"]`).prop('selected',true);
        }
        // END: Akses Jalan

        // START: Lingkungan Sekitar
        let lingkungan_sekitar = "{{ !empty($data) ? $data->lingkungan_sekitar : '' }}"
        if (lingkungan_sekitar) {
            $(`#lingkungan_sekitar option[value="${lingkungan_sekitar}"]`).prop('selected',true);
        }
        // END: Lingkungan Sekitar

        // START: Dihuni Oleh
        let dihuni_oleh = "{!! $data ? $data->dihuni_oleh : '' !!}"
        $(`#dihuni_oleh option[value="${dihuni_oleh}"]`).prop('selected',true);
        
        if (dihuni_oleh == '3') {
            $('#layout-hubungan-sebagai').removeClass('d-none')
            $('#layout-dikontrak-atau-sewa').removeClass('d-none')
        } else {
            $('#layout-hubungan-sebagai').addClass('d-none')
            $('#layout-dikontrak-atau-sewa').addClass('d-none')
        }

        $('#dihuni_oleh').change(function() {
            let this_value = $('#dihuni_oleh option:selected').val()
            if (this_value == '3') { // Jika dihuni oleh orang lain, masukkan hubungan dan apakah dikontrakkan
                $('#layout-hubungan-sebagai').removeClass('d-none')
                $('#layout-dikontrak-atau-sewa').removeClass('d-none')
            } else {
                $('#layout-hubungan-sebagai').addClass('d-none')
                $('#layout-dikontrak-atau-sewa').addClass('d-none')
            }
        })
        // END: Dihuni Oleh

        // START: Apakah Dikontrakkan
        let tanggal_berakhir_kontrak = "{!! $data ? $data->tanggal_berakhir_kontrak : '' !!}"
        let dikontrak_atau_sewa      = "{!! $data ? $data->dikontrak_atau_sewa : '' !!}"
        $(`#dikontrak_atau_sewa option[value="${dikontrak_atau_sewa}"]`).prop('selected',true);

        if (Date.parse(tanggal_berakhir_kontrak)) {
            console.log(Date.parse(tanggal_berakhir_kontrak))
            $('#layout-dikontrak-atau-sewa').removeClass('d-none')
        } else {
            $('#layout-tanggal-berakhi-_kontrak').addClass('d-none')
        }

        $('#dikontrak_atau_sewa').change(function() {
            let this_value = $('#dikontrak_atau_sewa option:selected').val()
            if (this_value == '1') { // Jika dikontrakkan, masukkan tanggal berakhir sewa
                $('#layout-tanggal-berakhi-_kontrak').removeClass('d-none')
            } else {
                $('#layout-tanggal-berakhi-_kontrak').addClass('d-none')
            }
        })
        // END: Apakah Dikontrakkan

        // START: Show Image
        $('#show_objek_depan').click(function() {
            let path = "{!! !empty($data) ? $data->pic_tampak_depan_objek : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto Tampak Depan Objek Pendanaan'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        $('#show_objek_dalam').click(function() {
            let path = "{!! !empty($data) ? $data->pic_tampak_dalam_objek : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto Tampak Dalam Objek Pendanaan'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        $('#show_objek_lingkungan').click(function() {
            let path = "{!! !empty($data) ? $data->pic_tampak_kondisi_lingkungan_objek : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto Tampak Kondisi Lingkungan Objek Pendanaan'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        $('#show_foto_petugas_depan_objek').click(function() {
            let path = "{!! !empty($data) ? $data->pic_petugas : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto Foto petugas di depan Objek Pendanaan beserta orang yang ditemui'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })
        // END: Show Image

        // START: File Upload
        $('#modal-image').on('show.bs.modal', function(e) {
            let target_id = $(e.relatedTarget).data('image-id');
            $('#btn-unggah-ulang').attr('onclick', `uploadFoto("${target_id}")`)
        });

        $('#objek_depan').change(function() {
            if (checkFileExtention('#objek_depan')) {
                $('#preview-image').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_show_objek_depan').text('Unggah Ulang Foto')
            }
        })

        $('#objek_dalam').change(function() {
            if (checkFileExtention('#objek_dalam')) {
                $('#preview-image2').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_show_objek_dalam').text('Unggah Ulang Foto')
            }
        })

        $('#objek_lingkungan').change(function() {
            if (checkFileExtention('#objek_lingkungan')) {
                $('#preview-image3').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_show_objek_lingkungan').text('Unggah Ulang Foto')
            }
        })

        $('#foto_petugas_depan_objek').change(function() {
            if (checkFileExtention('#foto_petugas_depan_objek')) {
                $('#preview-image4').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_show_foto_petugas_depan_objek').text('Unggah Ulang Foto')
            }
        })
        // END: File Upload
    });
            
    const checkFileExtention = (file_id) => {
        let validExtensions = ["png", "jpg", "jpeg"]

        let file = $(file_id).val().split('.').pop();
        if (validExtensions.indexOf(file) == -1) {
            swal.fire({
                title: 'Peringatan',
                type: 'warning',
                text: 'File extention yang di upload tidak sesuai!!',
                allowOutsideClick: () => false,
            }).then(function (result) {
                $(file_id).val('')
                return false
            })
        } else {
            return true
        }
    }

    const uploadFoto = (target_id) => {
        $('#'+target_id).trigger('click')
        document.getElementById('btn-close').click()
    }
</script>
@endpush