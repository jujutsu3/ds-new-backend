<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Dokumen Legalitas</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_dokumen_legalitasi" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">No</th>
                                <th class="align-middle text-center">Jenis Kriteria Resiko</th>
                                <th class="align-middle text-center">Dokumen Pendukung</th>
                                <th class="align-middle text-center">Ceklis Dokumen</th>
                                <th class="align-middle text-center">Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Usia</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_usia" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">No</th>
                                <th class="align-middle text-center">Jenis Kriteria Resiko</th>
                                <th class="align-middle text-center">Dokumen Pendukung</th>
                                <th class="align-middle text-center">Ceklis Dokumen</th>
                                <th class="align-middle text-center">Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Pekerjaan</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_pekerjaan" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">No</th>
                                <th class="align-middle text-center">Jenis Kriteria Resiko</th>
                                <th class="align-middle text-center">Dokumen Pendukung</th>
                                <th class="align-middle text-center">Ceklis Dokumen</th>
                                <th class="align-middle text-center">Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Verifikasi</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_verifikasi" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">No</th>
                                <th class="align-middle text-center">Jenis Kriteria Resiko</th>
                                <th class="align-middle text-center">Nilai Kelayakan</th>
                                <th class="align-middle text-center">Ceklis Dokumen</th>
                                <th class="align-middle text-center">Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Pendapatan</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_pendapatan" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">No</th>
                                <th class="align-middle text-center">Jenis Kriteria Resiko</th>
                                <th class="align-middle text-center">Dokumen Pendukung</th>
                                <th class="align-middle text-center">Ceklis Dokumen</th>
                                <th class="align-middle text-center">Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Jaminan</label>
    </div>
    <div class="card-body">
        <div class="row mt-3">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="table_jaminan" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="align-middle text-center">No</th>
                                <th class="align-middle text-center">Jenis Kriteria Resiko</th>
                                <th class="align-middle text-center">Dokumen Pendukung</th>
                                <th class="align-middle text-center">Ceklis Dokumen</th>
                                <th class="align-middle text-center">Hasil Verifikasi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- START: Modal View Verifikasi --}}
<div class="modal fade bd-example-modal-lg" id="modal-doc-verifikasi" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Nilai Kelayakan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="preview-nilai">
                    <p id="text-status" class="text-center">Mengambil Data..</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-left" data-toggle="modal"
                    data-dismiss="modal">
                    Tutup </button>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal View Verifikasi --}}

@include('layouts.modals.modal-verijelas')

@push('scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){

        const data                  = `{!! $data_rac !!}`
        const data_chk              = data ? JSON.parse(data) : []

        // STAR: DOM
        $('#verijelas_modal').on('show.bs.modal', function(event) {
            $('#loading_footer_data').html(`
                <button type="button" data-dismiss="modal" onclick="javascript:$(this).parent().addClass('hidden');"
                    class="btn btn-secondary" id="btnCloseHasil">
                    @lang('verify.btn_close')
                </button>
            `)
            var buttonTrigger = $(event.relatedTarget);
            var checkVerijelas = buttonTrigger.data('check');
            $('#verijelas_modal .modal-title').html('{{ trans('verify.hasil.title') }}' + ' - ' + nama_brw);
            
            if(checkVerijelas == '1') {
                $('.btnCheckNext').removeClass('d-none');
            }else{
                $('.btnCheckNext').addClass('d-none');
            }

            veriJelasData();
        });

        // END: DOM
        
        // START: Dokumen Legalitas
        let data_dokumen_legalitas = [
            {"no":"1",  "doc_pendukung":"0", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"kartu_keluarga", "jenis_dokumen":"Form Aplikasi Permohonan Pendanaan Dana Rumah"},
            {"no":"2",  "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_user_detail_pengajuan", "field_name":"brw_pic_ktp", "jenis_dokumen":"KTP Penerima Dana"},
            {"no":"3",  "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"ktp_pasangan", "jenis_dokumen":"KTP Pasangan Penerima Dana (Suami/Istri)"},
            {"no":"4",  "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_user_detail_pengajuan", "field_name":"brw_pic_npwp", "jenis_dokumen":"NPWP Penerima Dana"},
            {"no":"6",  "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"kartu_keluarga", "jenis_dokumen":"Kartu Keluarga"},
            {"no":"8",  "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"surat_pemesanan_rumah", "jenis_dokumen":"Surat Pemesanan Rumah(SPR) yang telah di Tandatangani"},
            {"no":"9",  "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"bukti_bayar_uang_muka", "jenis_dokumen":"Bukti Bayar Uang Muka atau Down Payment(DP)/NUP(Printout Rekening Koran/Mutasi Rekening)"},
            {"no":"10", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_beda_nama", "jenis_dokumen":"Surat Keterangan Beda Nama atau Tanggal Lahir antara KTP / KK / Akta Nikah dari Kelurahan (jika berbeda salah satu nya) - Jika Ada"},
            {"no":"11", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"perjanjian_pra_nikah", "jenis_dokumen":"Perjanjian Pra-Nikah Penerima Dana dari Notaris yang di daftarkan ke KUA / catatan sipil (jika menikah dengan WNA atau membuat perjanjian pra nikah atas permintaan sendiri sebelum menikah) - Jika Ada"},
        ];
        let data_dokumen_legalitas_join_income = [
            {"no":"5", "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"npwp_pasangan", "jenis_dokumen":"NPWP Pasangan (Suami /Istri)"},
        ]

        // start: status menikah
        let data_dokumen_legalitas_belum_menikah = [
            {"no":"7",  "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_keterangan_belum_menikah", "jenis_dokumen":"Akta Nikah/Akta Perkawinan (jika Menikah) Akta Cerai/Akta Perceraian (jika Cerai Hidup) Surat Pernyataan Belum Pernah Menikah dari Kelurahan (Jika Belum Menikah) Surat Pernyataan Belum Menikah Lagi dari Kelurahan dan/atau Surat Kematian dari RS / Puskesmas / Kelurahan (Jika Cerai Mati) - Jika Ada"},
        ]
        let data_dokumen_legalitas_cerai = [
            {"no":"7",  "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"akta_cerai", "jenis_dokumen":"Akta Nikah/Akta Perkawinan (jika Menikah) Akta Cerai/Akta Perceraian (jika Cerai Hidup) Surat Pernyataan Belum Pernah Menikah dari Kelurahan (Jika Belum Menikah) Surat Pernyataan Belum Menikah Lagi dari Kelurahan dan/atau Surat Kematian dari RS / Puskesmas / Kelurahan (Jika Cerai Mati) - Jika Ada"},
        ]
        let data_dokumen_legalitas_menikah = [
            {"no":"7",  "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"buku_nikah", "jenis_dokumen":"Akta Nikah/Akta Perkawinan (jika Menikah) Akta Cerai/Akta Perceraian (jika Cerai Hidup) Surat Pernyataan Belum Pernah Menikah dari Kelurahan (Jika Belum Menikah) Surat Pernyataan Belum Menikah Lagi dari Kelurahan dan/atau Surat Kematian dari RS / Puskesmas / Kelurahan (Jika Cerai Mati) - Jika Ada"},
        ]
        // end: status menikah

        // brw_user_detail_penghasilan = 2 (join income)
        let data_dokumen_legalitas_status = is_single_income == 2 ? data_dokumen_legalitas.concat(data_dokumen_legalitas_join_income) : data_dokumen_legalitas

        if (status_kawin == 1) { // menikah
            data_dokumen_legalitas_status.push(...data_dokumen_legalitas_menikah)
        } else if(status_kawin == 2) { // belum menikah
            data_dokumen_legalitas_status.push(...data_dokumen_legalitas_belum_menikah)
        } else { // cerai
            data_dokumen_legalitas_status.push(...data_dokumen_legalitas_cerai)
        }

        // Sort by no
        data_dokumen_legalitas_status.sort((a, b) => (a.no - b.no))

        let table_dokumen_legalitasi = $('#table_dokumen_legalitasi').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            data : data_dokumen_legalitas_status,
            columnDefs: [
                { className: "align-middle", targets: "_all" },
            ],
            columns:[
                {
                    data: "no",
                    className: 'align-middle text-center',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name}" data-fieldName="${data.field_name}" data-fileType="${data.file_type}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let doc_legal_verif =  data_chk[`dok_legal_${data.no}_verif`]
                        return `<input type="checkbox" class="_check" id="dok_legal_${data.no}_verif" name="dok_legal_${data.no}_verif" value="1" ${doc_legal_verif == '1' ? 'checked' : ''}
                        onclick="showHasilVerif('dok_legal_${data.no}_verif', 'dok_legal_${data.no}_verif_hasil')">`;
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let doc_legal_verif = data_chk[`dok_legal_${data.no}_verif`]
                        let doc_legal_verif_hasil = data_chk[`dok_legal_${data.no}_verif_hasil`]
                        
                        return `<input type="text" class="_check form-control ${doc_legal_verif == 1 ? '' : 'd-none'}"
                            id="dok_legal_${data.no}_verif_hasil" name="dok_legal_${data.no}_verif_hasil"
                            value="${doc_legal_verif_hasil ? doc_legal_verif_hasil : ''}">`;
                        },
                        width: "20%"
                },
            ]
        });
        // END: Dokumen Legalitas

        // START: Usia
        let data_usia = [
            {"no":"1", "doc_pendukung":"0", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"kartu_keluarga", "jenis_dokumen":"Minimum 21 Tahun dan pada saat jatuh tempo, maksimum umur 55 tahun(untuk pegawai)"}
        ];
        let table_usia = $('#table_usia').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            data : data_usia,
            columns:[
                {className: 'align-middle text-center', data : "no"},
                {data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name}" data-fieldName="${data.field_name}" data-fileType="${data.file_type}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let usia_verif = data_chk[`usia_${data.no}_verif`]
                        return `<input type="checkbox" class="_check" id="usia_${data.no}_verif" name="usia_${data.no}_verif" value="1" ${usia_verif == '1' ? 'checked' : ''}
                        onclick="showHasilVerif('usia_${data.no}_verif', 'usia_${data.no}_verif_hasil')">`;
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let usia_verif = data_chk[`usia_${data.no}_verif`]
                        let usia_verif_hasil = data_chk[`usia_${data.no}_verif_hasil`]
                        
                        return `<input type="text" class="_check form-control ${usia_verif == 1 ? '' : 'd-none'}"
                            id="usia_${data.no}_verif_hasil" name="usia_${data.no}_verif_hasil"
                            value="${usia_verif_hasil ? usia_verif_hasil : ''}">`;
                        },
                    width: "20%"
                },
            ]
        });
        // END: Usia

        // START: Dokumen Pekerjaan
        let data_dokumen_pekerjaan = [
            {"no":"1", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_bekerja", "jenis_dokumen":"SK Terakhir/Surat Keterangan Kerja Asli"},
            {"no":"2", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Lama Usia Perusahaan minimal 2 tahun"},
            {"no":"3", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Pekerjaan dari Penerima Dana dan/atau Pasangan merupakan Pekerjaan/Profesi yang tidak berhubungan/sebagai :</b>
                <br>a. Hukum (Hakim, Pengacara, Petugas Pengadilan, Konsultan Hukum, Petugas Firma Hukum dan termasuk karyawan yang bekerja
                pada profesi tersebut);
                <br>b. Petugas Militer (Angkatan Udara, Angkatan Laut, Angkatan Darat, dan Polisi termasuk karyawan yang bekerja pada
                instansi terkait);
                <br>c. Petugas Surat Kabar (Wartawan, Editor dan Jurnalis);
                <br>d. Anggota dan/atau Petugas Legislatif (DPR, MPR, DPRD, DPD);
                <br>e. Penjual Multi Level Marketing (MLM);
                <br>f. Staff kantor kedutaan besar`
            },
        ];

        let data_dokumen_pekerjaan_single_income = [
            {"no":"4", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Jika pegawai tetap di perusahaan saat ini :</b>
            <br>1. Minimum masa kerja 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap Kecuali PNS tidak melihat masa
            kerja), atau
            <br>2. Jika di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai pegawai tetap
            di perusahaan terakhir sebelumnya.`}
        ];

        let data_dokumen_pekerjaan_joint_income  = [
            {"no":"5", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Penerima Dana dan pasangan merupakan Pegawai / Karyawan tetap di perusahaan saat ini :</b>
            <br>1. Minimum masa kerja 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap), atau
            <br>2. Jika di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai pegawai tetap
            di perusahaan terakhir sebelumnya`},
            {"no":"6", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Penerima Dana merupakan Pegawai / Karyawan tetap dan Pasangan merupakan Pegawai / Karyawan kontrak di perusahaan saat
            ini : </b>
            <br>1. Penerima Dana memiliki masa kerja minimum 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap), atau
            <br>2. Jika Penerima Dana di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai
            pegawai tetap di perusahaan terakhir sebelumnya, atau
            <br>3. Pasangan minimum memiliki pengalaman kerja 2 tahun di perusahaan saat ini, atau
            <br>4. Pasangan minimum 1 tahun diperusahaan saat ini dengan memiliki pengalaman kerja minimal 2 tahun sebagai pegawai
            kontrak / tetap di perusahaan terakhir sebelumnya, atau
            <br>5. Pasangan minimum 6 bulan diperusahaan saat ini dengan memiliki pengalaman kerja minimal 3 tahun sebagai pegawai
            kontrak / tetap di perusahaan sebelumnya`},
        ];

        // brw_user_detail_penghasilan = 2 (join income)
        let data_dokumen_pekerjaan_status = is_single_income == 2 ? data_dokumen_pekerjaan.concat(data_dokumen_pekerjaan_joint_income) : data_dokumen_pekerjaan.concat(data_dokumen_pekerjaan_single_income)
        // Sort by no
        data_dokumen_pekerjaan_status.sort((a, b) => (a.no - b.no))

        let table_pekerjaan = $('#table_pekerjaan').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            data : data_dokumen_pekerjaan_status,
            columns:[
                {
                    data: "no",
                    className: 'align-middle text-center',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name}" data-fieldName="${data.field_name}" data-fileType="${data.file_type}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let pekerjaan_verif = data_chk[`pekerjaan_${data.no}_verif`]
                        return `<input type="checkbox" class="_check" id="pekerjaan_${data.no}_verif" name="pekerjaan_${data.no}_verif" value="1" ${pekerjaan_verif == '1' ? 'checked' : ''}
                        onclick="showHasilVerif('pekerjaan_${data.no}_verif', 'pekerjaan_${data.no}_verif_hasil')">`;
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let pekerjaan_verif = data_chk[`pekerjaan_${data.no}_verif`]
                        let pekerjaan_verif_hasil = data_chk[`pekerjaan_${data.no}_verif_hasil`]
                        
                        return `<input type="text" class="_check form-control ${pekerjaan_verif == 1 ? '' : 'd-none'}"
                            id="pekerjaan_${data.no}_verif_hasil" name="pekerjaan_${data.no}_verif_hasil"
                            value="${pekerjaan_verif_hasil ? pekerjaan_verif_hasil : ''}">`;
                        },
                    width: "20%"
                },
            ]
        });
        // END: Dokumen Pekerjaan

        // START: Data Verifikasi
        let data_verifikasi = [
            {"no":"1", "nilai_kelayakan":"0", "is_title": "",  "jenis_dokumen":"Tidak Memiliki Kredit/Pendanaan Bermasalah Baik di Bank maupun Non Bank"},
            {"no":"2", "nilai_kelayakan":"0", "is_title": "1", "jenis_dokumen":"Biro Kredit"},
            {"no":"2", "nilai_kelayakan":"2", "is_title": "",  "jenis_dokumen":"Biro Kredit Fasilitas Pendanaan Penerima Dana sesuai ketentuan yang ditetapkan (Score Minimal C3)."},
            // {"no":"3", "nilai_kelayakan":"1", "is_title": "",  "jenis_dokumen":"Biro Kredit Fasilitas Pendanaan Pasangan sesuai ketentuan yang ditetapkan (Score Minimal C3)."},

            {"no":"3", "nilai_kelayakan":"0", "is_title": "1", "jenis_dokumen":"Credolab Checking"},
            {"no":"4", "nilai_kelayakan":"3", "is_title": "",  "jenis_dokumen":"Hasil Kredolab Checking Penerima Dana sesuai ketentuan (terverifikasi)"},

            {"no":"4", "nilai_kelayakan":"0",  "is_title": "1", "jenis_dokumen":"Verijelas"},
            {"no":"5", "nilai_kelayakan":"4",  "is_title": "",  "jenis_dokumen":"Hasil Verijelas Checking Penerima Dana sesuai ketentuan (terverifikasi)"},
            // {"no":"6", "nilai_kelayakan":"1",  "is_title": "",  "jenis_dokumen":"Hasil Verijelas Checking Pasangan sesuai ketentuan (terverifikasi)"},
        ];

        let table_verifikasi = $('#table_verifikasi').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            ordering: false,
            data : data_verifikasi,
            columns:[
                 {
                    // targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    render: function (data, type, row, meta) {
                        let is_title = ((data.no == '1') || data.is_title) != '1' ? '' : data.no
                        return is_title;
                    }
                },
                {data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {

                        
                        if(data.nilai_kelayakan == 2) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn_show_nilai" data-skor="${data.nilai_kelayakan}" data-toggle="modal"
                                    data-target="#modal-doc-verifikasi">
                                    Lihat
                                </button>
                            `
                        } else if(data.nilai_kelayakan == 3) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn_show_nilai" data-skor="${data.nilai_kelayakan}" data-toggle="modal"
                                    data-target="#modal-doc-verifikasi">
                                    Lihat
                                </button>
                            `
                        } else if (data.nilai_kelayakan == 4) {
                            return `
                                <button type="button" data-toggle="modal" id="btnCekVerijelas" data-target="#verijelas_modal" data-backdrop="static"
                                    data-keyboard="false" href="#" class="btn btn-primary rounded btn_show_nilai" data-skor="${data.nilai_kelayakan}" data-check="1"> 
                                    Lihat 
                                </button>
                            `
                        } else if (data.is_title == 1) {
                            return ``
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (data.is_title != '1') {
                            let verifikasi_verif = data_chk[`verifikasi_${data.no}_verif`]
                            return `<input type="checkbox" class="_check" id="verifikasi_${data.no}_verif" name="verifikasi_${data.no}_verif"
                                value="1" ${verifikasi_verif=='1' ? 'checked' : '' }
                                onclick="showHasilVerif('verifikasi_${data.no}_verif', 'verifikasi_${data.no}_verif_hasil')">`;
                        } else {
                            return '';
                        }
                        
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (data.is_title != '1') {
                            let verifikasi_verif = data_chk[`verifikasi_${data.no}_verif`]
                            let verifikasi_verif_hasil = data_chk[`verifikasi_${data.no}_verif_hasil`]
                            
                            return `<input type="text" class="_check form-control ${verifikasi_verif == 1 ? '' : 'd-none'}"
                                id="verifikasi_${data.no}_verif_hasil" name="verifikasi_${data.no}_verif_hasil"
                                value="${verifikasi_verif_hasil ? verifikasi_verif_hasil : ''}">`;
                        }else {
                            return '';
                        }
                    },
                        
                    width: "20%"
                },
            ]
        });
        
        $('.btn_show_nilai').click(function() {

            const skor_biro_kredit = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->skor_pefindo !!}`
            const grade_biro_kredit = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->grade_pefindo !!}`
            const skor_personal_credolab = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->skor_personal_credolab !!}`
            const skor_pendanaan_credolab = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->skor_pendanaan_credolab !!}`
            
            let value = $(this).attr('data-skor')
            let preview_nilai = ``

            if (value == 2) {
                    preview_nilai = `
                    <div class="row">
                        <div class="col">
                            <p class="text-left text-status text-dark">Skor Biro Kredit: ${skor_biro_kredit}</p>
                        </div>
                        <div class="col">
                            <p class="text-left text-status text-dark">Grade Biro Kredit: ${grade_biro_kredit}</p>
                        </div>
                    </div>
                    `
                } else {
                    preview_nilai = `
                    <div class="row">
                        <div class="col">
                            <p class="text-left text-status text-dark">Skor Personal Credolab: ${skor_personal_credolab}</p>
                        </div>
                        <div class="col">
                            <p class="text-left text-status text-dark">Skor Pendanaan Credolab: ${skor_pendanaan_credolab}</p>
                        </div>
                    </div>
                    `
                }
                
            $('#preview-nilai').html(preview_nilai)

        })
        // END: Data Verifikasi

        // START: Data Pendapatan
        let data_pendapatan = [
            {"no":"1", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "is_title": "", "jenis_dokumen":"Minimum THP Calon Penerima Pendanaan Adalah Sebesar UMR Sesuai Domisili Tempat Bekerja"},

            {"no":"2", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "is_title": "1", "jenis_dokumen":"<b>Dokumen Penghasilan</b>"},
            {"no":"2", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"slip_gaji", "is_title": "", "jenis_dokumen":"Asli Slip Gaji minimal 3 bulan terakhir"},
            {"no":"4", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"mutasi_rekening", "is_title": "", "jenis_dokumen":"Mutasi rekening tabungan pribadi yang mencerminkan gaji/pendapatan penerima dana 3 bulan terakhir"},
            {"no":"6", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"spt", "is_title": "", "jenis_dokumen":"SPT Pajak Tahunan Penerima Dana"},
        ];

        let data_pendapatan_join_income = [
            {"no":"3", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"slip_gaji_pasangan", "is_title": "", "jenis_dokumen":"Asli slip gaji minimal 3 bulan terakhir pasangan (jika joint income)"},
            {"no":"5", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"mutasi_rekening_pasangan", "is_title": "", "jenis_dokumen":"Mutasi rekening tabungan yang mencerminkan gaji / pendapatan pasangan Penerima Dana 3 bulan terakhir (jika joint income)"},
            {"no":"7", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"spt_pasangan", "is_title": "", "jenis_dokumen":"SPT pajak tahunan Pasangan (Suami/Istri) (jika joint income)"},
        ]

        // brw_user_detail_penghasilan = 2 (join income)
        let data_pendapatan_status = is_single_income == 2 ? data_pendapatan.concat(data_pendapatan_join_income) : data_pendapatan
        // Sort by no
        data_pendapatan_status.sort((a, b) => (a.no - b.no))

        let table_pendapatan = $('#table_pendapatan').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            ordering: false,
            data : data_pendapatan_status,
            columnDefs: [
                { className: "align-middle my-auto", targets: "_all" },
            ],
            columns:[
                {
                    data: null,
                    className: 'align-middle text-center',
                    render: function (data, type, row, meta) {
                        let is_title = ((data.no == '1') || data.is_title) != '1' ? '' : data.no
                        return is_title;
                    }
                },
                {data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name}" data-fieldName="${data.field_name}" data-fileType="${data.file_type}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File
                                </button>
                            `
                        } else if (data.is_title == 1) {
                            return ``
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (data.is_title != '1') {
                            let pendapatan_verif = data_chk[`pendapatan_${data.no}_verif`]
                            return `<input type="checkbox" class="_check" id="pendapatan_${data.no}_verif" name="pendapatan_${data.no}_verif" value="1" ${pendapatan_verif == '1' ? 'checked' : ''}
                            onclick="showHasilVerif('pendapatan_${data.no}_verif', 'pendapatan_${data.no}_verif_hasil')">`;
                        } else {
                            return '';
                        }
                    },
                    width: "5%"
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        if (data.is_title != '1') {
                        let pendapatan_verif = data_chk[`pendapatan_${data.no}_verif`]
                        let pendapatan_verif_hasil = data_chk[`pendapatan_${data.no}_verif_hasil`]
                        
                        return `<input type="text" class="_check form-control ${pendapatan_verif == 1 ? '' : 'd-none'}"
                            id="pendapatan_${data.no}_verif_hasil" name="pendapatan_${data.no}_verif_hasil"
                            value="${pendapatan_verif_hasil ? pendapatan_verif_hasil : ''}">`;
                            } else {
                            return '';
                            }
                    },
                    width: "20%"
                },
            ]
        });
        // END: Data Pendapatan

        var data_jaminan = [
            {"no":"1", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"sertifikat", "jenis_dokumen":"Sertifikat Objek Pendanaan(SHM/SHGB)"},
            {"no":"2", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"imb", "jenis_dokumen":"IMB"},
            {"no":"3", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"sptt_stts", "jenis_dokumen":"SPTT dan STTS Tahun Terakhir"},
            {"no":"4", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Nilai Taksasi Berdasarkan Appraisal Internal dan/atau KJPP (Nilai Pasar Wajar dan Nilai Likuidasi)"},
            {"no":"5", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Lebar Jalan di Depan Objek Jaminan Minimal 2,2 Meter atau Dapat Dilalui oleh 1 Mobil"},
            {"no":"6", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Jaminan tidak dekat sutet, kuburan/TPU, +- 30 Meter dan Tidak Tusuk Sate"},
            {"no":"7", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Lokasi Jaminan Tidak Terkena Banjir dalam 2 Tahun Terakhir"},
            {"no":"8", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Tidak berlokasi di jalur hijau(Green Belt), Bantaran Sungai, Bantaran Rel Kereta Api"},
            {"no":"9", "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"ktp_pemilik", "jenis_dokumen":"Jika Penjual Perorangan/Rumah Second maka wajib dilampirkan : KTP(Suami/Istri), KK, Surat Nikah, NPWP"},
        ];
        var table_jaminan = $('#table_jaminan').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,								   
            paging:   false,					 
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            autoWidth: true,
            data : data_jaminan,
            columns:[
                {className: 'align-middle text-center', data : "no"},
                {data : "jenis_dokumen"},
                {
                    className: 'align-middle text-center', 
                    data : null,
                    render: function (data, type, full, meta) {
                        if(data.doc_pendukung == 1) {
                            return `
                                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                                    data-tableName="${data.table_name}" data-fieldName="${data.field_name}" data-fileType="${data.file_type}"
                                    data-target="#modal-doc-pendukung">
                                    Lihat File
                                </button>
                            `
                        } else {
                            return `-`
                        }
                        
                    }
                },
                {
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let jaminan_verif = data_chk[`jaminan_${data.no}_verif`]
                        return `<input type="checkbox" class="_check" id="jaminan_${data.no}_verif" name="jaminan_${data.no}_verif" value="1" ${jaminan_verif == '1' ? 'checked' : ''}
                        onclick="showHasilVerif('jaminan_${data.no}_verif', 'jaminan_${data.no}_verif_hasil')">`;
                    },
                    width: "5%"
                },
                {   
                    targets: 0,
                    data: null,
                    className: 'align-middle text-center',
                    searchable: false,
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let jaminan_verif = data_chk[`jaminan_${data.no}_verif`]
                        let jaminan_verif_hasil = data_chk[`jaminan_${data.no}_verif_hasil`]
                        
                        return `<input type="text" class="_check form-control ${jaminan_verif == 1 ? '' : 'd-none'}"
                            id="jaminan_${data.no}_verif_hasil" name="jaminan_${data.no}_verif_hasil"
                            value="${jaminan_verif_hasil ? jaminan_verif_hasil : ''}">`;
                        },
                    width: "20%"
                },
            ]
        });

    });
    
    const veriJelasData = () => {

        $.ajax({
            url: "{{ route('verify.data') }}",
            type: "post",
            dataType: "html",
            data: {
                _token: "{{ csrf_token() }}",
                borrower_id: brw_id,
                brw_type: brw_type,
                vendor: 'verijelas'
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                if (xhr.status == '419') {
                    errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                }

                $('#loading_title').html('Data Failed');
                $('#loading_content_data').html(errorMessage);
                $('#loading_footer_data').removeClass('d-none');
            },
            success: function(result) {
                $('#loading_title').html('Preparing Result ...');
                $('#loading_content_data').html(result);
                $('#loading_title').html('');
                $('#loading_footer_data').removeClass('d-none');
            }
        });


    }
</script>
@endpush