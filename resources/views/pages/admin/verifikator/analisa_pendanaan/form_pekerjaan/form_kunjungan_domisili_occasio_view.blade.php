@push('add-more-style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
    type="text/css" rel="stylesheet" />
<link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }} " rel="stylesheet" />
@endpush
<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Form Kunjungan Rumah</label>
    </div>
    <div class="card-body">
        <div class="mt-2">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nama_penerima_dana">Nama Penerima Dana</label>
                        <input type="text" class="form-control" name="nama_penerima_dana" id="nama_penerima_dana"
                            placeholder="-" value="{{ !empty($data) ? $data->nama : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pendanaan_dana_dibutuhkan">Plafond</label>
                        <input type="text" class="form-control" name="pendanaan_dana_dibutuhkan"
                            id="pendanaan_dana_dibutuhkan" placeholder="-" readonly
                            value="{{ !empty($data) ? 'Rp' .str_replace(',','.', number_format($data->pendanaan_dana_dibutuhkan)) : '-' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tanggal_kunjungan">Tanggal Kunjungan</label>
                        <input type="date" class="form-control" name="tanggal_kunjungan" id="tanggal_kunjungan"
                            placeholder="ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->tanggal_kunjungan : '' }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jam_kunjungan">Jam Kunjungan</label>
                        <input type="text" class="form-control" name="jam_kunjungan" id="jam_kunjungan"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->jam_kunjungan : ''}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nama_yang_ditemui">Nama yang Ditemui</label>
                        <input type="text" class="form-control" name="nama_yang_ditemui" id="nama_yang_ditemui"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->nama_yang_ditemui : ''}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="selaku">Selaku</label>
                        <select name="selaku" id="selaku" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            @foreach ($m_hub as $item)
                            <option value="{{ $item->id_hub_ahli_waris }}" {{ !empty($data_brw_form_kunjungan_domisili->
                                selaku) ? $item->id_hub_ahli_waris == $data_brw_form_kunjungan_domisili->selaku ?
                                'selected' : '' : ''}}>
                                {{ $item->jenis_hubungan }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="alamat_rumah_saat_ini">Alamat Rumah saat ini</label>
                        <textarea name="alamat_rumah_saat_ini" id="alamat_rumah_saat_ini" placeholder="Ketik disini"
                            class="form-control"
                            rows="3">{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->alamat_rumah_saat_ini : ''}}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="provinsi">Provinsi</label>
                        <select name="provinsi" id="provinsi" class="form-control custom-select"
                            onChange="provinsiChange(this.value, this.id, 'kota_kabupaten')">
                            <option value="">-- Pilih Satu --</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kota_kabupaten">Kota/Kabupaten</label>
                        <select name="kota_kabupaten" id="kota_kabupaten" class="form-control custom-select"
                            onChange="kotaChange(this.value, this.id, 'kecamatan')">
                            <option value="">-- Pilih Satu --</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kecamatan">Kecamatan</label>
                        <select name="kecamatan" id="kecamatan" class="form-control custom-select"
                            onChange="kecamatanChange(this.value, this.id, 'kelurahan')">
                            <option value="">-- Pilih Satu --</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kelurahan">Kelurahan</label>
                        <select name="kelurahan" id="kelurahan" class="form-control custom-select"
                            onChange="kelurahanChange(this.value, this.id, 'kode_pos')">
                            <option value="">-- Pilih Satu --</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kode_pos">Kode Pos</label>
                        <input type="text" class="form-control" name="kode_pos" id="kode_pos" placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->kode_pos : ''}}"
                            readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="status_rumah" class="col-form-label col-sm-12 pl-0">Status Rumah Saat ini</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status_rumah" id="milik_pribadi"
                                value="1">
                            <label class="form-check-label" for="milik_pribadi">Milik Pribadi</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status_rumah" id="milik_keluarga"
                                value="2">
                            <label class="form-check-label" for="milik_keluarga">Milik Keluarga</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status_rumah" id="sewa" value="3">
                            <label class="form-check-label" for="sewa">Sewa</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="lama_tinggal">Lama Tinggal</label>
                        <input type="text" class="form-control" name="lama_tinggal" id="lama_tinggal"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->lama_tinggal : ''}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kondisi_lingkungan">Kondisi Lingkungan</label>
                        <input type="text" class="form-control" name="kondisi_lingkungan" id="kondisi_lingkungan"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->kondisi_lingkungan : ''}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="informasi_negatif">Terdapat Informasi Negatif dari Tetangga atau
                            RT/RW</label>
                        <textarea class="form-control" id="informasi_negatif" name="informasi_negatif"
                            rows="4">{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->informasi_negatif : ''}}</textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nama_pemberi_informasi">Nama Pemberi Informasi</label>
                        <input type="text" class="form-control" name="nama_pemberi_informasi"
                            id="nama_pemberi_informasi" placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->nama_pemberi_informasi : ''}}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tlp_hp_pemberi_informasi">No. Telp/HP pemberi informasi</label>
                        <input type="text" class="form-control" name="tlp_hp_pemberi_informasi"
                            id="tlp_hp_pemberi_informasi" placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->tlp_hp_pemberi_informasi : ''}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="penjelasan_terkait_pengajuan">Penjelasan Terkait Pengajuan</label>
                        <textarea class="form-control" id="penjelasan_terkait_pengajuan"
                            name="penjelasan_terkait_pengajuan" rows="3"
                            placeholder="Ketik disini">{{ !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->penjelasan_terkait_pengajuan : ''}}</textarea>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Foto kondisi rumah saat in</label>
    </div>
    <div class="card-body">
        <div id="layout-foto-kondisi-rumah">

            <div class="form-group row">
                <div class="col-md-8">
                    <label for="rumah_depan">Tampak Depan Rumah Tinggal</label>
                </div>
                <div class="col-md-4 my-auto">
                    <div class="text-center">

                        <input type="file" name="rumah_depan" id="rumah_depan" class="d-none"
                            accept=".png, .jpg, .jpeg">

                        <img id="preview-image" class="img-fluid mb-3" />

                        @if (!empty($data_brw_form_kunjungan_domisili) &&
                        $data_brw_form_kunjungan_domisili->foto_tampak_depan_rumah )
                        <button id="show_rumah_depan" type="button" class="btn btn-success mx-auto" data-toggle="modal"
                            data-target="#modal-image" data-image-id="rumah_depan">
                            Lihat Foto</button>
                        @else
                        <button type="button" id="btn_rumah_depan" class="btn btn-secondary mx-auto"
                            onclick="$('#rumah_depan').trigger('click');">Unggah
                            Foto</button>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-8">
                    <label for="rumah_dalam">Tampak Kondisi Dalam Rumah tinggal</label>
                </div>
                <div class="col-md-4 my-auto">
                    <div class="text-center">
                        <input type="file" name="rumah_dalam" id="rumah_dalam" class="d-none"
                            accept=".png, .jpg, .jpeg">

                        <img id="preview-image2" class="img-fluid mb-3" />

                        @if (!empty($data_brw_form_kunjungan_domisili) &&
                        $data_brw_form_kunjungan_domisili->foto_tampak_dalam_rumah )
                        <button id="show_rumah_dalam" type="button" class="btn btn-success mx-auto" data-toggle="modal"
                            data-target="#modal-image" data-image-id="rumah_dalam">
                            Lihat Foto</button>
                        @else
                        <button type="button" id="btn_rumah_dalam" class="btn btn-secondary mx-auto"
                            onclick="$('#rumah_dalam').trigger('click');">Unggah
                            Foto</button>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-8">
                    <label for="rumah_lingkungan">Tampak Kondisi Lingkungan Rumah tinggal</label>
                </div>
                <div class="col-md-4 my-auto">
                    <div class="text-center">
                        <input type="file" name="rumah_lingkungan" id="rumah_lingkungan" class="d-none"
                            accept=".png, .jpg, .jpeg">

                        <img id="preview-image3" class="img-fluid mb-3" />

                        @if (!empty($data_brw_form_kunjungan_domisili) &&
                        $data_brw_form_kunjungan_domisili->foto_lingkungan_rumah )
                        <button id="show_rumah_lingkungan" type="button" class="btn btn-success mx-auto"
                            data-toggle="modal" data-target="#modal-image" data-image-id="rumah_lingkungan">
                            Lihat Foto</button>
                        @else
                        <button type="button" id="btn_rumah_lingkungan" class="btn btn-secondary mx-auto"
                            onclick="$('#rumah_lingkungan').trigger('click');">Unggah
                            Foto</button>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="foto_petugas_depan_rumah" class="col-sm-8 my-auto">Foto petugas di depan Rumah
                    tinggal</label>
                <div class="col-md-4 my-auto">
                    <div class="text-center">
                        <input type="file" name="foto_petugas_depan_rumah" id="foto_petugas_depan_rumah" class="d-none"
                            accept=".png, .jpg, .jpeg">

                        <img id="preview-image4" class="img-fluid mb-3" />

                        @if (!empty($data_brw_form_kunjungan_domisili) &&
                        $data_brw_form_kunjungan_domisili->foto_petugas_depan_rumah )
                        <button id="show_foto_petugas_depan_rumah" type="button" class="btn btn-success mx-auto"
                            data-toggle="modal" data-target="#modal-image" data-image-id="foto_petugas_depan_rumah">
                            Lihat Foto</button>
                        @else
                        <button type="button" id="btn_foto_petugas_depan_rumah" class="btn btn-secondary mx-auto"
                            onclick="$('#foto_petugas_depan_rumah').trigger('click');">Unggah
                            Foto</button>
                        @endif
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

{{-- START: Modal Show Photo --}}
<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-image-title"></h5>
                <button type="button" id="btn-close" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="modal-image-src" src="" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" id="btn-unggah-ulang" class="btn btn-primary">Unggah Ulang</button>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal Show Photo --}}

@push('scripts')
<script src="{{url('assetsBorrower/js/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){
        $('#provinsi, #kota_kabupaten, #kecamatan, #kelurahan').prepend('<option selected></option>').select2({
            // theme: "bootstrap",
            placeholder: "Pilih Satu",
            allowClear: true,
            width: '100%',
        })

        $.getJSON("/borrower/data_provinsi/", function(data){
            for(let i = 0; i<data.length; i++){
                $('#provinsi').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        })
        
        loadData()

        // START: Show Image
        $('#show_rumah_depan').click(function() {
            let path = "{!! !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->foto_tampak_depan_rumah : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto tampak depan rumah tinggal'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        $('#show_rumah_dalam').click(function() {
            let path = "{!! !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->foto_tampak_dalam_rumah : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto tampak  kondisi dalam rumah'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        $('#show_rumah_lingkungan').click(function() {
            let path = "{!! !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->foto_lingkungan_rumah : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto tampak kondisi lingkungan rumah tinggal'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        $('#show_foto_petugas_depan_rumah').click(function() {
            let path = "{!! !empty($data_brw_form_kunjungan_domisili) ? $data_brw_form_kunjungan_domisili->foto_petugas_depan_rumah : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto petugas di depan Rumah Tinggal'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })
        // END: Show Image

        // START: File Upload
        $('#modal-image').on('show.bs.modal', function(e) {
            let target_id = $(e.relatedTarget).data('image-id');
            $('#btn-unggah-ulang').attr('onclick', `uploadFoto("${target_id}")`)
        });

        $('#rumah_depan').change(function() {
            if (checkFileExtention('#rumah_depan')) {
                $('#preview-image').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_rumah_depan').text('Unggah Ulang Foto')
            }
        })

        $('#rumah_dalam').change(function() {
            if (checkFileExtention('#rumah_dalam')) {
                $('#preview-image2').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_rumah_dalam').text('Unggah Ulang Foto')
            }
        })

        $('#rumah_lingkungan').change(function() {
            if (checkFileExtention('#rumah_lingkungan')) {
                $('#preview-image3').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_rumah_lingkungan').text('Unggah Ulang Foto')
            }
        })

        $('#foto_petugas_depan_rumah').change(function() {
            if (checkFileExtention('#foto_petugas_depan_rumah')) {
                $('#preview-image4').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_foto_petugas_depan_rumah').text('Unggah Ulang Foto')
            }
        })
        // END: File Upload
    });

    const provinsiChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kota/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kotaChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kecamatan/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kecamatanChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kelurahan/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kelurahanChange = (thisValue, thisId, nextId) => {
        $.getJSON("/borrower/data_kode_pos/"+thisValue+"/", function(data){
            $('#'+nextId).val(data[0].text)
            $('#kode_pos').val(data[0].text)
        });
    }

    // Load data from db
    const getProvinsiData = (data_provinsi, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_provinsi/", function(data){
            for(let i = 0; i<data.length; i++){
                let option = $(`<option ${data[i].text == data_provinsi ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        })
    }

    const getKotaData = (data_provinsi, this_data_kota, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kota/"+data_provinsi+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                let option = $(`<option ${data[i].text == this_data_kota ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        });
    }

    const getKecamatan = (data_kota, this_data_kecamatan, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kecamatan/"+data_kota+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                let option = $(`<option ${data[i].text == this_data_kecamatan ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        });
    }

    const getKelurahanData = (data_kecamatan, this_data_kelurahan, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kelurahan/"+data_kecamatan+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
               let option = $(`<option ${data[i].text == this_data_kelurahan ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        });
    }

    const loadData = () => {
        let status_rumah = "{{ $data_brw_form_kunjungan_domisili ? $data_brw_form_kunjungan_domisili->status_rumah_saat_ini : ''}}"
        $('input:radio[name="status_rumah"][value="' + status_rumah + '"]').prop('checked', true);

        let provinsi = "{{ !empty($data_brw_form_kunjungan_domisili->provinsi) ? $data_brw_form_kunjungan_domisili->provinsi : '' }}";
        let kota = "{{ !empty($data_brw_form_kunjungan_domisili->kota_kabupaten) ? $data_brw_form_kunjungan_domisili->kota_kabupaten : '' }}";
        let kecamatan = "{{ !empty($data_brw_form_kunjungan_domisili->kecamatan) ? $data_brw_form_kunjungan_domisili->kecamatan : '' }}";
        let kelurahan = "{{ !empty($data_brw_form_kunjungan_domisili->kelurahan) ? $data_brw_form_kunjungan_domisili->kelurahan : '' }}";
        
        getProvinsiData(provinsi, 'provinsi')
        getKotaData(provinsi, kota, 'kota_kabupaten')
        getKecamatan(kota, kecamatan, 'kecamatan')
        getKelurahanData(kecamatan, kelurahan, 'kelurahan')
    }

    const checkFileExtention = (file_id) => {
        let validExtensions = ["png", "jpg", "jpeg"]

        let file = $(file_id).val().split('.').pop();
        if (validExtensions.indexOf(file) == -1) {
            swal.fire({
                title: 'Peringatan',
                type: 'warning',
                text: 'File extention yang di upload tidak sesuai!!',
                allowOutsideClick: () => false,
            }).then(function (result) {
                $(file_id).val('')
                return false
            })
        } else {
            return true
        }
    }

    const uploadFoto = (target_id) => {
        $('#'+target_id).trigger('click')
        document.getElementById('btn-close').click()
    }
</script>
@endpush