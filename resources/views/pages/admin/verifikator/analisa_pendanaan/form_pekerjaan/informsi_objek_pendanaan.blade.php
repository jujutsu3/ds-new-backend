{{-- <div class="row">
    <div class="col-md-12"> --}}
        <ul class="breadcrumb">
            <li><a href="{{ route('borrower.list.verifikasi') }}">Verifikasi</a></li>
            <li>Lanjut</li>
        </ul>
        {{--
    </div>
</div> --}}

{{-- START: Informasi Objek Pendanaan --}}
<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Informasi Objek
                        Pendanaan</label>
                </div>
            </div>
        </div>
        <div class="mt-2">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tipe_pendanaan">Tipe Pendanaan</label>
                        <input type="text" class="form-control" name="tipe_pendanaan" id="tipe_pendanaan" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tujuan_pendanaan">Tujuan Pendanaan</label>
                        <input type="text" class="form-control" name="tujuan_pendanaan" id="tujuan_pendanaan"
                            placeholder="Tujuan Pendanaan" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="alamat_objek_pendanaan">Alamat Objek Pendanaan</label>
                        <input type="text" class="form-control" name="alamat_objek_pendanaan"
                            id="alamat_objek_pendanaan" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="provinsi">Provinsi</label>
                        <input type="text" class="form-control" name="provinsi" id="provinsi" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kota_kabupaten">Kota/Kabupaten</label>
                        <input type="text" class="form-control" name="kota_kabupaten" id="kota_kabupaten" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kecamatan">Kecamatan</label>
                        <input type="text" class="form-control" name="kecamatan" id="kecamatan" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kelurahan">Kelurahan</label>
                        <input type="text" class="form-control" name="kelurahan" id="kelurahan" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kode_pos">Kode Pos</label>
                        <input type="text" class="form-control" name="kode_pos" id="kode_pos" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="harga_objek_pendanaan">Harga Objek Pendanaan</label>
                        <input type="text" class="form-control" name="harga_objek_pendanaan" id="harga_objek_pendanaan"
                            readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="uang_muka">Uang Muka</label>
                        <input type="text" class="form-control" name="uang_muka" id="uang_muka" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nilai_pengajuan_pendanaan">Nilai Pengajuan Pendanaan</label>
                        <input type="text" class="form-control" name="nilai_pengajuan_pendanaan"
                            id="nilai_pengajuan_pendanaan" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jangka_waktu">Jangka Waktu (Bulan)</label>
                        <input type="text" class="form-control" name="jangka_waktu" id="jangka_waktu" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="detail_informasi_pendanaan">Detail Informasi Objek Pendanaan</label>
                        <textarea class="form-control" id="detail_informasi_pendanaan" rows="3" readonly></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Informasi Objek Pendanaan --}}

{{-- START: Informasi Pemilik Objek Pendanaan --}}
<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-md-12">
                <div class="box-title">
                    <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Informasi Pemilik Objek
                        Pendanaan</label>
                </div>
            </div>
        </div>

        <div class="mt-2">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama_pemilik" id="nama_pemilik" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>No Telp/HP</label>
                        <input type="text" class="form-control" name="no_telp_pemilik" id="no_telp_pemilik" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Alamat Pemilik Pendanaan</label>
                        <input type="text" class="form-control" name="alamat_pemilik" id="alamat_pemilik" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Provinsi</label>
                        <input type="text" class="form-control" name="provinsi_pemilik" id="provinsi_pemilik" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kota/Kabupaten</label>
                        <input type="text" class="form-control" name="kota_pemilik" id="kota_pemilik" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kecamatan</label>
                        <input type="text" class="form-control" name="kecamatan_pemilik" id="kecamatan_pemilik"
                            readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kelurahan</label>
                        <input type="text" class="form-control" name="kelurahan_pemilik" id="kelurahan_pemilik"
                            readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Kode Pos</label>
                        <input type="text" class="form-control" name="kode_pos_pemilik" id="kode_pos_pemilik" readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Informasi Pemilik Objek Pendanaan --}}

@push('scripts')
<script type="text/javascript">
    var pengajuan_id = "{{ $pengajuan_id }}";
 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function(){

    });

</script>
@endpush