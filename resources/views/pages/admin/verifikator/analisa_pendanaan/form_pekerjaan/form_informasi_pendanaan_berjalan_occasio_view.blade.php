@push('add-more-style')
@endpush
<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Informasi Pendanaan
            Berjalan</label>
    </div>
    <div class="card-body">
        <div class="form-group row">
            <label for="rumah_ke" class="col-sm-9 col-form-label">1. Ini merupakan status rumah ke</label>
            <div class="col-md-3">
                <select name="rumah_ke" id="rumah_ke" class="form-control custom-select">
                    <option value="">-- Pilih Satu --</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="status_kepemilikan_rumah_lain" class="col-md-9 col-form-label">2. Kepemilikan rumah lain
                sebanyak</label>
            <div class="col-md-3">
                <input type="text" name="status_kepemilikan_rumah_lain" id="status_kepemilikan_rumah_lain"
                    class="form-control" value="" readonly>
            </div>
        </div>

        <div id="layout-table-rumah-lain">
            <div class="row mt-2 mb-2">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="table_rumah_lain" class="table table-striped table-bordered dt-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center align-middle">No</th>
                                    <th class="text-center align-middle" style="width: 15%">Status</th>
                                    <th class="text-center align-middle">Bank</th>
                                    <th class="text-center align-middle">Plafon</th>
                                    <th class="text-center align-middle">Jangka Waktu</th>
                                    <th class="text-center align-middle">Outstanding</th>
                                    <th class="text-center align-middle">Angsuran</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data_brw_pendanaan_rumah_lain as $item)
                                <tr>
                                    <td class="text-center align-middle">{{ $loop->iteration }}</td>
                                    <td class="text-center align-middle">
                                        <input type="hidden" name="rumah_ke[]" value="{{ $loop->iteration }}">
                                        <select name="text_status_rumah_lain[]" id="text_status_rumah_lain_{{ $loop->iteration }}"
                                            class="form-control custom-select">
                                            <option value="">-- Pilih Satu --</option>
                                            <option value="1" {{ $item->status == '1' ? 'selected' : '' }}>Sedang
                                                Berjalan 
                                            </option>
                                            <option value="2" {{ $item->status == '2' ? 'selected' : ''}}>Lunas</option>
                                        </select>
                                    </td>
                                    <td class="text-left align-middle">
                                        <input type="text" name="text_bank_rumah_lain[]"
                                            id="text_bank_rumah_lain_{{ $loop->iteration }}" value="{{ $item->bank }}"
                                            class="form-control">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_plafond_rumah_lain[]"
                                            id="text_plafond_rumah_lain_{{ $loop->iteration }}"
                                            value="Rp{{ str_replace(',', '.', number_format($item->plafond)) }}"
                                            class="form-control" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_jangka_waktu_rumah_lain[]"
                                            id="text_jangka_waktu_rumah_lain_{{ $loop->iteration }}"
                                            value="{{ $item->jangka_waktu }}" class="form-control">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_outstanding_rumah_lain[]"
                                            id="text_outstanding_rumah_lain_{{ $loop->iteration }}"
                                            value="Rp{{ str_replace(',', '.', number_format($item->outstanding)) }}"
                                            class="form-control" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_angsuran_rumah_lain[]"
                                            id="text_angsuran_rumah_lain_{{ $loop->iteration }}"
                                            value="Rp{{ str_replace(',', '.', number_format($item->angsuran)) }}"
                                            class="form-control" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row mt-4 mb-3">
            <label for="status_kepemilikan_non_rumah_lain" class="col-md-9 col-form-label">3. Apakah anda memiliki
                fasilitas pembiayaan
                berjalan selain Kepemilikan
                Rumah</label>
            <div class="col-md-3">
                <select name="status_kepemilikan_non_rumah_lain" id="status_kepemilikan_non_rumah_lain"
                    class="form-control custom-select">
                    <option value="">-- Pilih Satu --</option>
                    <option value="1">Ya</option>
                    <option value="2">Tidak</option>
                </select>
            </div>
        </div>

        <div id="layout-table-non-rumah-lain">
            <div class="row mt-2 mb-2">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="table_non_rumah_lain" class="table table-striped table-bordered dt-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center align-middle">No</th>
                                    <th class="text-center align-middle">Bank</th>
                                    <th class="text-center align-middle">Plafon</th>
                                    <th class="text-center align-middle">Jangka Waktu</th>
                                    <th class="text-center align-middle">Outstanding</th>
                                    <th class="text-center align-middle">Angsuran</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr></tr>
                                @foreach ($data_brw_pendanaan_non_rumah_lain as $item)
                                <tr id="tr-{{ $loop->iteration }}">
                                    <td class="align-middle text-center" id="text_number_{{ $loop->iteration }}"
                                        name="text_number[]">{{ $loop->iteration }}</td>
                                    <td>
                                        <input type="hidden" name="text_id[]" id="text_id" value="{{ $item->id }}">
                                        <input type="text" name="text_bank_non_rumah_lain[]"
                                            id="text_bank_non_rumah_lain_{{ $loop->iteration }}" class="form-control"
                                            value="{{ $item->bank }}">
                                    </td>
                                    <td>
                                        <input type="text" name="text_plafond_non_rumah_lain[]"
                                            id="text_plafond_non_rumah_lain_{{ $loop->iteration }}" class="form-control"
                                            value="Rp{{ str_replace(',', '.', number_format($item->plafond)) }}"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td>
                                        <input type="text" name="text_jangka_waktu_non_rumah_lain[]"
                                            id="text_jangka_waktu_non_rumah_lain_{{ $loop->iteration }}"
                                            class="form-control" value="{{ $item->jangka_waktu }}">
                                    </td>
                                    <td>
                                        <input type="text" name="text_outstanding[]"
                                            id="text_outstanding_{{ $loop->iteration }}" class="form-control"
                                            value="Rp{{ str_replace(',', '.', number_format($item->outstanding)) }}"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td>
                                        <input type="text" name="text_angsuran_non_rumah_lain[]"
                                            id="text_angsuran_non_rumah_lain_{{ $loop->iteration }}"
                                            class="form-control"
                                            value="Rp{{ str_replace(',', '.', number_format($item->angsuran)) }}"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td class="bg-white align-middle text-center border-0">
                                        <button type="button" id="btn-remove-non-rumah-lain" class="btn btn-danger"
                                            onclick="deletePendanaanNonRumah({{ $loop->iteration }}, {{ $item->id }})">
                                            <i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                @endforeach
                                <div id="deleted-data-non-rumah-lain">

                                </div>
                            </tbody>
                            <tfoot>
                                <tr height="12px"></tr>
                                <tr>
                                    <td id="in_number_row" class="text-center align-middle">
                                        {{ count($data_brw_pendanaan_non_rumah_lain) + 1 }}</td>
                                    <td class="text-center align-middle">
                                        <input type="hidden" name="text_id[]" value="0">
                                        <input type="text" name="text_bank_non_rumah_lain[]" id="in_bank_non_rumah_lain"
                                            class="form-control" value="">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_plafond_non_rumah_lain[]"
                                            id="in_plafond_non_rumah_lain" class="form-control"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_jangka_waktu_non_rumah_lain[]"
                                            id="in_jangka_waktu_non_rumah_lain" class="form-control">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_outstanding[]" id="in_outstanding"
                                            class="form-control" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" name="text_angsuran_non_rumah_lain[]"
                                            id="in_angsuran_non_rumah_lain" class="form-control"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                    </td>
                                    <td class="text-center align-middle border-0">
                                        <button type="button" id="btn-add-non-rumah-lain" class="btn btn-primary"
                                            onclick="addPendanaanNonRumah()"> <i class="fa fa-plus"></i></button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    const data_brw_pendanaan_rumah_lain = `{!! $data_brw_pendanaan_rumah_lain !!}`
    
    const total_data_brw_pendanaan_rumah_lain = "{!! !empty($data_brw_pendanaan_rumah_lain) ? count($data_brw_pendanaan_rumah_lain) : [] !!}"
    const total_data_brw_pendanaan_non_rumah_lain = "{!! !empty($data_brw_pendanaan_non_rumah_lain) ? count($data_brw_pendanaan_non_rumah_lain) : [] !!}"
    let id_number = parseInt(total_data_brw_pendanaan_non_rumah_lain)
    let row_number = parseInt(total_data_brw_pendanaan_non_rumah_lain)

    $(document).ready(function(){
        // START: Pendanaan rumah lain
        $('#rumah_ke').change(function () {
            let this_value = $('#rumah_ke option:selected').val()
            if (this_value) {
                $('#status_kepemilikan_rumah_lain').val(this_value - 1)
            } else {
                $('#status_kepemilikan_rumah_lain').val('')
            }

            if (this_value == '1' || this_value == '') {
                $('#layout-table-rumah-lain').addClass('d-none')
            } else {
                $('#layout-table-rumah-lain').removeClass('d-none')
            }

            if (this_value == '2') {
                addPendanaanRumahLain(1)
            } else if(this_value == '3') {
                addPendanaanRumahLain(2)
            } else {
                addPendanaanRumahLain(0)
            }
        })

        if (total_data_brw_pendanaan_rumah_lain > 0) { //Jika memiliki pendanaan rumah lain
            $(`#rumah_ke option[value='${parseInt(total_data_brw_pendanaan_rumah_lain) + 1}']`).prop('selected', true)
            $('#status_kepemilikan_rumah_lain').val(total_data_brw_pendanaan_rumah_lain)
            $('#layout-table-rumah-lain').removeClass('d-none')
        } else {
            $('#layout-table-rumah-lain').addClass('d-none')
            $('#rumah_ke option[value="1"]').prop('selected', true)
            $('#status_kepemilikan_rumah_lain').val('0')
        }
        // END: Pendanaan rumah lain

        // START: Pendanaan non rumah
        $('#status_kepemilikan_non_rumah_lain').change(function() {
            let this_value = $('#status_kepemilikan_non_rumah_lain option:selected').val()
            if (this_value == '1') {
                $('#layout-table-non-rumah-lain').removeClass('d-none')
            } else {
                $('#layout-table-non-rumah-lain').addClass('d-none')
            }
        })
        console.log(total_data_brw_pendanaan_non_rumah_lain)
        if (total_data_brw_pendanaan_non_rumah_lain > 0) { //Jika memiliki pendanaan non rumah
            $(`#status_kepemilikan_non_rumah_lain option[value='1']`).prop('selected', true)
            $('#layout-table-non-rumah-lain').removeClass('d-none')

        } else {
            $(`#status_kepemilikan_non_rumah_lain option[value='2']`).prop('selected', true)
            $('#layout-table-non-rumah-lain').addClass('d-none')
        }
        // END: Pendanaan non rumah
    });  

    // Other Function
    const addPendanaanRumahLain = (lenght) => {
        if (lenght == 0) {
            $('#table_rumah_lain').find('tbody').html('')
        } else {
            $('#table_rumah_lain').find('tbody').html('')

            const data_rumah_lain = data_brw_pendanaan_rumah_lain ? JSON.parse(data_brw_pendanaan_rumah_lain) : []

            for (let i = 0; i < lenght; i++) {
                let plafon_rumah_lain       = data_rumah_lain[i] ? formatRupiah( parseInt(data_rumah_lain[i]['plafond']).toString(), 'Rp') : ''
                let outstanding_rumah_lain  = data_rumah_lain[i] ? formatRupiah( parseInt(data_rumah_lain[i]['outstanding']).toString(), 'Rp') : ''
                let angsuran_rumah_lain     = data_rumah_lain[i] ? formatRupiah(parseInt(data_rumah_lain[i]['angsuran']).toString(), 'Rp') : ''

                $('#table_rumah_lain').find('tbody').append(`
                    <tr>
                        <td class="text-center align-middle">${i + 1}</td>
                        <td class="text-center align-middle">
                            <input type="hidden" name="rumah_ke[]" value="${i + 1}">
                            <select name="text_status_rumah_lain[]" class="form-control custom-select">
                                <option value="">-- Pilih Satu --</option>
                                <option value="1" ${data_rumah_lain[i] && data_rumah_lain[i]['status'] == '1' ? 'selected' : '' }>Sedang
                                    Berjalan
                                </option>
                                <option value="2" ${data_rumah_lain[i] && data_rumah_lain[i]['status'] == '2' ? 'selected' : '' }>Lunas</option>
                            </select>
                        </td>
                        <td class="text-center align-middle">
                            <input type="hidden" name="text_id_rumah_lain[]" value="0">
                            <input type="text" name="text_bank_rumah_lain[]"
                                class="form-control" value="${data_rumah_lain[i] ? data_rumah_lain[i]['bank'] : ''}">
                        </td>
                        <td class="text-center align-middle">
                            <input type="text" name="text_plafond_rumah_lain[]"
                                class="form-control" value="${plafon_rumah_lain}" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                        </td>
                        <td class="text-center align-middle">
                            <input type="text" name="text_jangka_waktu_rumah_lain[]"
                                class="form-control" value="${data_rumah_lain[i] ? data_rumah_lain[i]['jangka_waktu'] : ''}">
                        </td>
                        <td class="text-center align-middle">
                            <input type="text" name="text_outstanding_rumah_lain[]" class="form-control"
                                value="${outstanding_rumah_lain}" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                        </td>
                        <td class="text-center align-middle">
                            <input type="text" name="text_angsuran_rumah_lain[]"
                                class="form-control" value="${angsuran_rumah_lain}" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                        </td>
                    </tr>
                `)    
            }
        }
    }
    const addPendanaanNonRumah = () => {
        id_number += 1
        row_number += 1

        let bank            = $('#in_bank_non_rumah_lain').val() 
        let plafon          = $('#in_plafond_non_rumah_lain').val()
        let jangka_waktu    = $('#in_jangka_waktu_non_rumah_lain').val()
        let outstanding     = $('#in_outstanding').val()
        let angsuran        = $('#in_angsuran_non_rumah_lain').val()

        $('#table_non_rumah_lain').find('tbody').append(`
            <tr id="tr-${id_number}">
                <td id="text_number_${id_number}" name="text_number[]" class="text-center align-middle">${row_number}</td>
                <td class="text-center align-middle">
                    <input type="hidden" name="text_id[]" id="text_id_${id_number}" value="0">
                    <input type="text" name="text_bank_non_rumah_lain[]" id="text_bank_non_rumah_lain_${id_number}" class="form-control" value="${bank}">
                </td>
                <td class="text-center align-middle">
                    <input type="text" name="text_plafond_non_rumah_lain[]" id="text_plafond_non_rumah_lain_${id_number}" class="form-control" value="${plafon}" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                </td>
                <td class="text-center align-middle">
                    <input type="text" name="text_jangka_waktu_non_rumah_lain[]" id="text_jangka_waktu_non_rumah_lain_${id_number}"
                        class="form-control" value="${jangka_waktu}">
                </td>
                <td class="text-center align-middle">
                    <input type="text" name="text_outstanding[]" id="text_outstanding_${id_number}" class="form-control" value="${outstanding}" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                </td>
                <td class="text-center align-middle">
                    <input type="text" name="text_angsuran_non_rumah_lain[]" id="text_angsuran_non_rumah_lain_${id_number}" class="form-control" value="${angsuran}" onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                </td>
                <td class="text-center align-middle border-0 bg-white">
                    <button type="button" id="btn-remove-non-rumah-lain" class="btn btn-danger" onclick="deletePendanaanNonRumah(${id_number})"> <i class="fa fa-trash"></i></button>
                </td>
            </tr>
        `)

        $('#in_number_row').text(row_number + 1)

        clearValueField()
    }

    const deletePendanaanNonRumah = (row, id) => {
        if (id) {
            $('#deleted-data-non-rumah-lain').append(`
                <input type="hidden" name="deleted_data_non_rumah_lain[]" value="${id}">
            `)   
        }
    
        $(`#tr-${row}`).remove();

        incrementRow()
    }

    const incrementRow = () => {
        row_number -= 1

        let text_number = $('td[name="text_number[]"]')
        for (let i = 1; i <= text_number.length; i++) { 
            $(text_number[i-1]).text(i)
        }
        $('#in_number_row').text(row_number + 1)
    }

    const clearValueField = () => {
        $('#in_bank_non_rumah_lain').val('')
        $('#in_plafond_non_rumah_lain').val('')
        $('#in_jangka_waktu_non_rumah_lain').val('')
        $('#in_outstanding').val('')
        $('#in_angsuran_non_rumah_lain').val('')
    }

</script>
@endpush