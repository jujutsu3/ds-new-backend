@push('add-more-style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
    type="text/css" rel="stylesheet" />
<link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }} " rel="stylesheet" />
@endpush
<div class="card mt-4">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Form Interview HRD</label>
    </div>
    <div class="card-body">
        <div class="mt-2">

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nama_penerima_dana">Nama Penerima Dana</label>
                        <input type="text" class="form-control" name="nama_penerima_dana" id="nama_penerima_dana"
                            placeholder="-" value="{{ !empty($data) ? $data->nama : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pendanaan_dana_dibutuhkan">Plafond</label>
                        <input type="text" class="form-control" name="pendanaan_dana_dibutuhkan"
                            id="pendanaan_dana_dibutuhkan" placeholder="-"
                            value="{{ !empty($data) ? 'Rp' .str_replace(',', '.',number_format($data->pendanaan_dana_dibutuhkan)) : '-' }}"
                            readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="no_fixed_line_hrd">No. Telp Fixed Line HRD</label>
                        <input type="text" class="form-control" name="no_fixed_line_hrd" id="no_fixed_line_hrd"
                            placeholder="-" value="{{ !empty($data) ? $data->no_fixed_line_hrd : '-' }}" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="tanggal_interview">Tanggal Telepon/Kunjungan</label>
                        <input type="date" class="form-control" name="tanggal_interview" id="tanggal_interview"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->tanggal_interview : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jam_interview">Jam Telpon/Kunjungan</label>
                        <input type="text" class="form-control" name="jam_interview" id="jam_interview"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->jam_interview : '' }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="verifikasi_melalui">Verifikasi Melalui</label>
                        <select name="verifikasi_melalui" id="verifikasi_melalui" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                verifikasi_melalui == '1' ? 'selected' : '' : '' }}>
                                Telpon/Email</option>
                            <option value="2" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                verifikasi_melalui == '2' ? 'selected' : '' : '' }}>
                                Kunjungan</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nama_perusahaan">Nama Perusahaan</label>
                        <input type="text" class="form-control" name="nama_perusahaan" id="nama_perusahaan"
                            placeholder="-" value="{{ !empty($data) ? $data->nama_perusahaan : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bidang_pekerjaan">Bidang Usaha</label>
                        <select name="bidang_pekerjaan" id="bidang_pekerjaan" class="form-control custom-select"
                            disabled>
                            <option value="">-- Pilih Satu --</option>
                            @foreach ($m_bidang_pekerjaan as $item)
                            <option value="{{ $item->id_bidang_pekerjaan }}" {{ $item->id_bidang_pekerjaan ==
                                $data->bidang_pekerjaan ? 'selected' : ''}}>
                                {{ $item->bidang_pekerjaan }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jumlah_total_karyawan">Jumlah Total Karyawan</label>
                        <input type="number" class="form-control" name="jumlah_total_karyawan"
                            id="jumlah_total_karyawan" placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->jumlah_total_karyawan : '' }}"
                            onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="nama_hrd">Nama HRD</label>
                        <input type="text" class="form-control" name="nama_hrd" id="nama_hrd" placeholder="-"
                            value="{{ !empty($data) ? $data->nama_hrd : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="jabatan_hrd">Jabatan HRD </label>
                        <select name="jabatan_hrd" id="jabatan_hrd" class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_form_interview_hrd) && $data_brw_form_interview_hrd->
                                jabatan_hrd == '1' ? 'selected' : '' }}>Staff</option>
                            <option value="2" {{ !empty($data_brw_form_interview_hrd) && $data_brw_form_interview_hrd->
                                jabatan_hrd == '2' ? 'selected' : '' }}>Manager</option>
                            <option value="3" {{ !empty($data_brw_form_interview_hrd) && $data_brw_form_interview_hrd->
                                jabatan_hrd == '3' ? 'selected' : '' }}>Kepala Bagian</option>
                        </select>
                        {{-- <input type="text" class="form-control" name="jabatan_hrd" id="jabatan_hrd" placeholder="-"
                            value="Kepala HRD / HRD manager" readonly> --}}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="alamat_kantor">Alamat Kantor</label>
                        <textarea name="alamat_kantor" id="alamat_kantor" rows="3" class="form-control" placeholder="-"
                            readonly>{{ !empty($data) ? $data->c_alamat_perusahaan : '-' }}</textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="provinsi_kantor">Provinsi</label>
                        <input type="text" class="form-control" name="provinsi_kantor" id="provinsi_kantor"
                            placeholder="-" value="{{ !empty($data) ? $data->c_provinsi : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kota_kantor">Kota/Kabupaten</label>
                        <input type="text" class="form-control" name="kota_kantor" id="kota_kantor" placeholder="-"
                            value="{{ !empty($data) ? $data->c_kab_kota : '-' }}" readonly>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kecamatan_kantor">Kecamatan</label>
                        <input type="text" class="form-control" name="kecamatan_kantor" id="kecamatan_kantor"
                            placeholder="-" value="{{ !empty($data) ? $data->c_kecamatan : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kelurahan_kantor">Kelurahan</label>
                        <input type="text" class="form-control" name="kelurahan_kantor" id="kelurahan_kantor"
                            placeholder="-" value="{{ !empty($data) ? $data->c_kelurahan : '-' }}" readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="kode_pos_kantor">Kode Pos</label>
                        <input type="text" class="form-control" name="kode_pos_kantor" id="kode_pos_kantor"
                            placeholder="-" value="{{ !empty($data) ? $data->c_kode_pos : '-' }}" readonly>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Pertanyaan</label>
    </div>
    <div class="card-body">
        <div class="mt-2">

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_1_jawaban">1. Apakah Benar Bapak/Ibu
                        ({{ !empty($data) ? $data->nama : '' }}) Bekerja di
                        Perusahaan ini?</label>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <select name="pertanyaan_1_jawaban" id="pertanyaan_1_jawaban"
                            class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_1_jawaban == '1' ? 'selected' : '' : ''}}>
                                Ya</option>
                            <option value="2" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_1_jawaban == '2' ? 'selected' : '' : ''}}>
                                Tidak</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-8 col-12">
                    <div class="form-group row">
                        <label for="pertanyaan_1_deskripsi" class="col-lg-2 col-12 col-form-label">Deskripsi :</label>
                        <div class="col-lg-10 col-12">
                            <textarea class="form-control" name="pertanyaan_1_deskripsi" id="pertanyaan_1_deskripsi"
                                placeholder="Ketik disini"
                                rows="3">{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_1_deskripsi : ''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_2">2. Sudah berapa lama Bapak/Ibu
                        ({{ !empty($data) ? $data->nama : '' }}) Bekerja di
                        Perusahaan ini?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="form-group">
                        <input type="text" class="form-control" name="pertanyaan_2" id="pertanyaan_2"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_2 : ''}}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_3_jawaban">3. Status bekerja Bapak/Ibu
                        ({{ !empty($data) ? $data->nama : '' }}) di Perusahaan
                        adalah?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="form-group">
                        <select name="pertanyaan_3_jawaban" id="pertanyaan_3_jawaban"
                            class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_3_jawaban == '1' ? 'selected' : '' : ''}}>
                                Karyawan Tetap</option>
                            <option value="2" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_3_jawaban == '2' ? 'selected' : '' : ''}}>
                                Karyawan Kontrak</option>
                        </select>
                    </div>
                </div>

                <div class="col-lg-8 col-12">
                    <div class="form-group row">
                        <label for="pertanyaan_3_deskripsi" class="col-lg-2 col-12 col-form-label">Deskripsi :</label>
                        <div class="col-lg-10 col-12">
                            <textarea class="form-control" name="pertanyaan_3_deskripsi" id="pertanyaan_3_deskripsi"
                                placeholder="Ketik disini"
                                rows="3">{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_3_deskripsi : ''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_4_jawaban">4. Posisi jabatan Bapak/Ibu
                        ({{ !empty($data) ? $data->nama : '' }})
                        di
                        Perusahaan sebagai apa?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="form-group">
                        <input type="text" class="form-control" name="pertanyaan_4_jawaban" id="pertanyaan_4_jawaban"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_4_jawaban : ''}}">
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="form-group row">
                        <label for="pertanyaan_4_deskripsi" class="col-sm-2 col-form-label">Deskripsi :</label>
                        <div class="col-lg-10 col-12">
                            <textarea class="form-control" name="pertanyaan_4_deskripsi" id="pertanyaan_4_deskripsi"
                                placeholder="Ketik disini"
                                rows="3">{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_4_deskripsi : ''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_5">5. Pendapatan Take Home Pay(THP) Dalam 1 Bulan dari
                        Bapak/Ibu ({{ !empty($data) ? $data->nama : '' }})?</label>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <input type="text" class="form-control" name="pertanyaan_5" id="pertanyaan_5" placeholder="Rp"
                            value="{{ !empty($data_brw_form_interview_hrd) ? 'Rp' .str_replace(',', '.', number_format($data_brw_form_interview_hrd->pertanyaan_5)) : ''}}"
                            onkeyup="this.value = formatRupiah(this.value, 'Rp')">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="tipe_pendanaan">6. Komponen Take Home Pay(THP) apa saja yang
                            didapat?</label>

                        <div class="row my-1">
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_gaji_pokok"
                                        name="pertanyaan_6_gaji_pokok" value="1" {{ !empty($data_brw_form_interview_hrd)
                                        ? $data_brw_form_interview_hrd->pertanyaan_6_gaji_pokok == '1' ? 'checked' : ''
                                    : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_gaji_pokok">Gaji Pokok</label>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_bpjs"
                                        name="pertanyaan_6_bpjs" value="1" {{ !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_bpjs == '1' ? 'checked' : '' : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_bpjs">BPJS</label>
                                </div>
                            </div>
                        </div>
                        <div class="row my-1">
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_tunjangan_jabatan"
                                        name="pertanyaan_6_tunjangan_jabatan" value="1" {{
                                        !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_tunjangan_jabatan == '1' ? 'checked'
                                    : '' : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_tunjangan_jabatan">Tunjangan
                                        Jabatan</label>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_jht"
                                        name="pertanyaan_6_jht" value="1" {{ !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_jht == '1' ? 'checked' : '' : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_jht">JHT</label>
                                </div>
                            </div>
                        </div>
                        <div class="row my-1">
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_lembur"
                                        name="pertanyaan_6_lembur" value="1" {{ !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_lembur == '1' ? 'checked' : '' :
                                    ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_lembur">Lembur</label>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_jkk"
                                        name="pertanyaan_6_jkk" value="1" {{ !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_jkk == '1' ? 'checked' : '' : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_jkk">JKK</label>
                                </div>
                            </div>
                        </div>
                        <div class="row my-1">
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_transport"
                                        name="pertanyaan_6_transport" value="1" {{ !empty($data_brw_form_interview_hrd)
                                        ? $data_brw_form_interview_hrd->pertanyaan_6_transport == '1' ? 'checked' : '' :
                                    ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_transport">Transport</label>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_jpn"
                                        name="pertanyaan_6_jpn" value="1" {{ !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_jpn == '1' ? 'checked' : '' : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_jpn">JPN</label>
                                </div>
                            </div>
                        </div>
                        <div class="row my-1">
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_uang_makan"
                                        name="pertanyaan_6_uang_makan" value="1" {{ !empty($data_brw_form_interview_hrd)
                                        ? $data_brw_form_interview_hrd->pertanyaan_6_uang_makan == '1' ? 'checked' : ''
                                    : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_uang_makan">Uang Makan</label>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_pph21"
                                        name="pertanyaan_6_pph21" value="1" {{ !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_pph21 == '1' ? 'checked' : '' : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_pph21">PPH 21</label>
                                </div>
                            </div>
                        </div>
                        <div class="row my-1">
                            <div class="col-12 col-lg-6">
                                <div class="form-check form-check-inline">
                                    <input type="checkbox" class="form-check-input" id="pertanyaan_6_bonus"
                                        name="pertanyaan_6_bonus" value="1" {{ !empty($data_brw_form_interview_hrd) ?
                                        $data_brw_form_interview_hrd->pertanyaan_6_bonus == '1' ? 'checked' : '' : ''}}>
                                    <label class="form-check-label" for="pertanyaan_6_bonus">Bonus</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_7_jawaban">7. Pembayaran gaji setiap tanggal berapa dan
                        melalui payroll bank apa?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="form-group">
                        <input type="text" class="form-control" name="pertanyaan_7_jawaban" id="pertanyaan_7_jawaban"
                            placeholder="Ketik disini" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');"
                            maxlength="2"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_7_tanggal : ''}}">
                        {{-- <input type="date" class="form-control" name="pertanyaan_7_jawaban"
                            id="pertanyaan_7_jawaban" placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_7_jawaban : ''}}">
                        --}}
                    </div>
                </div>

                <div class="col-lg-8 col-12">
                    <div class="form-group row">
                        <label for="pertanyaan_7_bank" class="col-lg-2 col-12 col-form-label">Nama Bank :</label>
                        <div class="col-lg-10 col-12">
                            <select name="pertanyaan_7_bank" id="pertanyaan_7_bank" class="form-control custom-select">
                                <option value="">-- Pilih Satu</option>
                                {{-- @foreach ($m_bank as $item)
                                <option value="{{ $item->kode_bank }}" {{ $data_brw_form_interview_hrd && $item->
                                    kode_bank == $data_brw_form_interview_hrd->pertanyaan_7_bank ?
                                    'selected' : '' }}>{{ $item->nama_bank }}</option>
                                @endforeach --}}
                            </select>
                            {{-- <input type="text" class="form-control" name="pertanyaan_7_bank" id="pertanyaan_7_bank"
                                placeholder="Ketik disini"
                                value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_7_bank : ''}}">
                            --}}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_8_jawaban">8. Berapa lama Perusahaan
                        ({{ !empty($data) ? 'PT ' .$data->nama_perusahaan : '' }})
                        berdiri ?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="form-group">
                        <input type="text" class="form-control" name="pertanyaan_8_jawaban" id="pertanyaan_8_jawaban"
                            placeholder="Ketik disini"
                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_8_jawaban : ''}}">
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="form-group row">
                        <label for="pertanyaan_8_deskripsi" class="col-lg-2 col-12 col-form-label">Deskripsi :</label>
                        <div class="col-lg-10 col-12">
                            <textarea class="form-control" id="pertanyaan_8_deskripsi" name="pertanyaan_8_deskripsi"
                                placeholder="Ketik disini"
                                rows="3">{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_8_deskripsi : ''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_9_jawaban">9. Bisa disebutkan alamat lengkap perusahaan
                        ({{ !empty($data) ? $data->nama_perusahaan : '' }})?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="form-group">
                        <textarea class="form-control" id="pertanyaan_9_jawaban" name="pertanyaan_9_jawaban"
                            placeholder="Ketik disini"
                            rows="3">{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_9_jawaban : ''}}</textarea>
                        {{-- <select name="pertanyaan_9_jawaban" id="pertanyaan_9_jawaban"
                            class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_9_jawaban == '1' ? 'selected' : '' : ''}}>
                                Ya</option>
                            <option value="2" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_9_jawaban == '2' ? 'selected' : '' : ''}}>
                                Tidak</option>
                        </select> --}}
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="form-group row">
                        <label for="pertanyaan_9_deskripsi" class="col-lg-2 col-12 col-form-label">Deskripsi :</label>
                        <div class="col-lg-10 col-12">
                            <textarea class="form-control" name="pertanyaan_9_deskripsi" id="pertanyaan_9_deskripsi"
                                placeholder="Ketik disini"
                                rows="3">{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_9_deskripsi : ''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_10_jawaban">10. Apakah ada informasi negatif dari bapak/ibu
                        ({{ !empty($data) ? $data->nama : ''}}) di Perusahaan
                        ({{ !empty($data) ? 'PT ' .$data->nama_perusahaan : '' }}) tersebut?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <div class="form-group">
                        <select name="pertanyaan_10_jawaban" id="pertanyaan_10_jawaban"
                            class="form-control custom-select">
                            <option value="">-- Pilih Satu --</option>
                            <option value="1" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_10_jawaban == '1' ? 'selected' : '' : ''}}>
                                Ya</option>
                            <option value="2" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                                pertanyaan_10_jawaban == '2' ? 'selected' : '' : ''}}>
                                Tidak</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="form-group row">
                        <label for="pertanyaan_10_deskripsi" class="col-lg-2 col-12 col-form-label">Deskripsi :</label>
                        <div class="col-lg-10 col-12">
                            <textarea class="form-control" name="pertanyaan_10_deskripsi" id="pertanyaan_10_deskripsi"
                                placeholder="Ketik disini"
                                rows="3">{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_10_deskripsi : ''}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <label for="pertanyaan_11_jawaban">11. Apakah perusahaan
                        ({{ !empty($data) ? 'PT ' .$data->nama_perusahaan : '' }})
                        memberikan fasilitas pinjaman/kredit/pendanaan terhadap
                        karyawan?</label>
                </div>
                <div class="col-lg-4 col-12">
                    <select name="pertanyaan_11_jawaban" id="pertanyaan_11_jawaban" class="form-control custom-select">
                        <option value="">-- Pilih Satu --</option>
                        <option value="1" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                            pertanyaan_11_jawaban == '1' ? 'selected' : '' : ''}}>
                            Ya</option>
                        <option value="2" {{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->
                            pertanyaan_11_jawaban == '2' ? 'selected' : '' : ''}}>
                            Tidak</option>
                    </select>
                </div>
                <div class="col-lg-8 col-12">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="pertanyaan_11_plafond"
                                        class="col-lg-2 col-12 col-form-label">Plafond</label>
                                    <div class="col-lg-10 col-12 my-auto">
                                        <input type="text" class="form-control" name="pertanyaan_11_plafond"
                                            id="pertanyaan_11_plafond" placeholder="ketik disini"
                                            value="{{ !empty($data_brw_form_interview_hrd) ? 'Rp' .str_replace(',', '.', number_format($data_brw_form_interview_hrd->pertanyaan_11_plafond)) : ''}}"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp')">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="pertanyaan_11_outstanding"
                                        class="col-lg-2 col-12 col-form-label">Outstanding</label>
                                    <div class="col-lg-10 col-12 my-auto">
                                        <input type="text" class="form-control" name="pertanyaan_11_outstanding"
                                            id="pertanyaan_11_outstanding" placeholder="Ketik disini"
                                            value="{{ !empty($data_brw_form_interview_hrd) ? 'Rp' .str_replace(',', '.', number_format($data_brw_form_interview_hrd->pertanyaan_11_outstanding)) : ''}}"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp')">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="pertanyaan_11_jangka_waktu"
                                        class="col-lg-2 col-12 col-form-label">Jangka
                                        Waktu (Bulan)</label>
                                    <div class="col-lg-10 col-12 my-auto">
                                        <input type="text" class="form-control" name="pertanyaan_11_jangka_waktu"
                                            id="pertanyaan_11_jangka_waktu" placeholder="Ketik disini"
                                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_11_jangka_waktu : ''}}"
                                            onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="pertanyaan_11_margin" class="col-lg-2 col-12 col-form-label">Margin /
                                        Bunga</label>
                                    <div class="col-lg-10 col-12 my-auto">
                                        <input type="text" class="form-control" name="pertanyaan_11_margin"
                                            id="pertanyaan_11_margin" placeholder="Ketik disini"
                                            value="{{ !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->pertanyaan_11_margin : ''}}"
                                            onkeyup="this.value = this.value.replace(/[^0-9\.]/g, '');">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group row">
                                    <label for="pertanyaan_11_angsuran" class="col-lg-2 col-12 col-form-label">Angsuran
                                        Setiap
                                        Bulan</label>
                                    <div class="col-lg-10 col-12 my-auto">
                                        <input type="text" class="form-control" name="pertanyaan_11_angsuran"
                                            id="pertanyaan_11_angsuran" placeholder="ketik disini"
                                            value="{{ !empty($data_brw_form_interview_hrd) ? 'Rp' .str_replace(',', '.', number_format($data_brw_form_interview_hrd->pertanyaan_11_angsuran)) : ''}}"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp')">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="card">
    <div class="box-title">
        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Foto verifikasi kunjungan ke
            kantor langsung jika tidak bisa dihubungi
            melalui telepon</label>
    </div>
    <div class="card-body">
        <div id="layout-foto-telepon" class="mt-2">
            <div class="form-group row">
                <label for="bukti_telpon" class="col-sm-8 my-auto">Bukti telpon/email(Tangkapan Layar)</label>
                <div class="col-sm-4 my-auto">
                    <div class="text-center">
                        <input type="file" name="bukti_telpon" id="bukti_telpon" class="d-none"
                            accept=".png, .jpg, .jpeg">

                        <img id="preview-image" class="img-fluid mb-3" />

                        @if (!empty($data_brw_form_interview_hrd) && $data_brw_form_interview_hrd->bukti_interview )
                        <button id="show_bukti_telpon" type="button" class="btn btn-success mx-auto" data-toggle="modal"
                            data-target="#modal-image" data-image-id="bukti_telpon">
                            Lihat Foto</button>
                        @else
                        <button type="button" id="btn_bukti_telpon" class="btn btn-secondary mx-auto"
                            onclick="$('#bukti_telpon').trigger('click');">Unggah
                            Foto</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div id="layout-foto-kunjungan" class="d-none">
            <div class="form-group row">
                <label for="foto_verifikator_kantor" class="col-sm-8 my-auto">Foto Verifikator di depan kantor / Loby
                    kantor
                    yang
                    memperlihatkan Nama
                    Perusahaan</label>
                <div class="col-sm-4 my-auto">
                    <div class="text-center">
                        <input type="file" name="foto_verifikator_kantor" id="foto_verifikator_kantor" class="d-none"
                            accept=".png, .jpg, .jpeg">

                        <img id="preview-image2" class="img-fluid mb-3" />

                        @if (!empty($data_brw_form_interview_hrd) &&
                        $data_brw_form_interview_hrd->foto_didepan_kantor_lobby )
                        <button id="show_foto_depan_rumah" type="button" class="btn btn-success mx-auto"
                            data-toggle="modal" data-target="#modal-image" data-image-id="foto_verifikator_kantor">
                            Lihat Foto</button>
                        @else
                        <button type="button" id="btn_foto_verifikator_kantor" class="btn btn-secondary mx-auto"
                            onclick="$('#foto_verifikator_kantor').trigger('click');">Unggah
                            Foto</button>
                        @endif
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="foto_verifikator_hrd" class="col-sm-8 my-auto">Foto Verifikator bersama HRD di
                    kantor</label>
                <div class="col-sm-4 my-auto">
                    <div class="text-center">
                        <input type="file" name="foto_verifikator_hrd" id="foto_verifikator_hrd" class="d-none"
                            accept=".png, .jpg, .jpeg">

                        <img id="preview-image3" class="img-fluid mb-3" />

                        @if (!empty($data_brw_form_interview_hrd) &&
                        $data_brw_form_interview_hrd->foto_bersama_hrd )
                        <button id="show_foto_hrd" type="button" class="btn btn-success mx-auto" data-toggle="modal"
                            data-target="#modal-image" data-image-id="foto_verifikator_hrd">
                            Lihat Foto</button>
                        @else
                        <button type="button" id="btn_foto_verifikator_hrd" class="btn btn-secondary mx-auto"
                            onclick="$('#foto_verifikator_hrd').trigger('click');">Unggah
                            Foto</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal-image" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-image-title"></h5>
                <button type="button" id="btn-close" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img id="modal-image-src" src="" alt="">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" id="btn-unggah-ulang" class="btn btn-primary">Unggah Ulang</button>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{url('assetsBorrower/js/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    var side = '/admin/borrower';

    $(document).ready(function(){
        // START: Bank
        $('#pertanyaan_7_bank').prepend('<option selected></option>').select2({
            placeholder: "Pilih Satu",
            allowClear: true,
            width: '100%',
        })

        $.getJSON("/borrower/data_bank/", function(data){
            let bank = '{!! $data_brw_form_interview_hrd ? $data_brw_form_interview_hrd->pertanyaan_7_bank : '' !!}'

            for(let i = 0; i<data.length; i++){
                $('#pertanyaan_7_bank').append($('<option>', {
                    value: data[i].id,
                    text : data[i].text
                }));
            }
            if (bank) {
                $('#pertanyaan_7_bank').val(bank);
            }
        });
        // END: Bank

        $('#verifikasi_melalui').change(function () {
            let this_value = $('select[name=verifikasi_melalui]').val();
            if (this_value == '2') {
                $('#layout-foto-telepon').addClass('d-none')
                $('#layout-foto-kunjungan').removeClass('d-none')
            } else {
                $('#layout-foto-kunjungan').addClass('d-none')
                $('#layout-foto-telepon').removeClass('d-none')
            }
        })

        let verif_melalui = "{!! $data_brw_form_interview_hrd ? $data_brw_form_interview_hrd->verifikasi_melalui : '' !!}"
        if (verif_melalui == '2') {
            $('#layout-foto-telepon').addClass('d-none')
            $('#layout-foto-kunjungan').removeClass('d-none')
        } else {
            $('#layout-foto-kunjungan').addClass('d-none')
            $('#layout-foto-telepon').removeClass('d-none')
        }

        // Bukti foto interview
        $('#show_bukti_telpon').click(function() {
            let path = "{!! !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->bukti_interview : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Bukti telpon/email(Tangkapan Layar)'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        // Foto verifikator di depan kantor
        $('#show_foto_depan_rumah').click(function() {
            let path = "{!! !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->foto_didepan_kantor_lobby : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto verifikator di depan kantor'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        // Foto verifikator bersama HRD
        $('#show_foto_hrd').click(function() {
            let path = "{!! !empty($data_brw_form_interview_hrd) ? $data_brw_form_interview_hrd->foto_bersama_hrd : '' !!}"
            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
            $('#modal-image-title').text('Foto verifikator bersama HRD'); 
            $('#modal-image-src').attr('src', url.replace(':path', path.replaceAll('/', ':')))
        })

        $('#modal-image').on('show.bs.modal', function(e) {
            let target_id = $(e.relatedTarget).data('image-id');
            $('#btn-unggah-ulang').attr('onclick', `uploadFoto("${target_id}")`)
            
        });

        // START: File Upload
        $('#bukti_telpon').change(function() {
            if (checkFileExtention('#bukti_telpon')) {
                $('#preview-image').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_bukti_telpon').text('Unggah Ulang Foto')
            }
        })

        $('#foto_verifikator_kantor').change(function() {
            if (checkFileExtention('#foto_verifikator_kantor')) {
                $('#preview-image2').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_foto_verifikator_kantor').text('Unggah Ulang Foto')
            }
        })

        $('#foto_verifikator_hrd').change(function () {
            if (checkFileExtention('#foto_verifikator_hrd')) {
                $('#preview-image3').attr('src', window.URL.createObjectURL(this.files[0])).addClass('w-100')
                $('#btn_foto_verifikator_hrd').text('Unggah Ulang Foto')
            }
        })
        // END: File Upload

       
    });

    const checkFileExtention = (file_id) => {
        let validExtensions = ["png", "jpg", "jpeg"]

        let file = $(file_id).val().split('.').pop();
        if (validExtensions.indexOf(file) == -1) {
            swal.fire({
                title: 'Peringatan',
                type: 'warning',
                text: 'File extention yang di upload tidak sesuai!!',
                allowOutsideClick: () => false,
            }).then(function (result) {
                $(file_id).val('')
                return false
            })
        } else {
            return true
        }
    }

    const uploadFoto = (target_id) => {
        $('#'+target_id).trigger('click')
        document.getElementById('btn-close').click()
    }
</script>
@endpush