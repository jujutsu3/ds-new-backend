@extends('layouts.admin.master')

@section('title', 'Verifikasi Pendanaan ')
@section('style')
<style>
    ul.breadcrumb {
        padding: 10px 16px;
        list-style: none;
        background-color: #fff;
    }

    ul.breadcrumb li {
        display: inline;
        font-size: 18px;
    }

    ul.breadcrumb li+li:before {
        padding: 8px;
        color: black;
        content: "/\00a0";
    }

    ul.breadcrumb li a {
        color: #0275d8;
        text-decoration: none;
    }

    ul.breadcrumb li a:hover {
        color: #01447e;
        text-decoration: underline;
    }

    .btn-danaSyariah {
        background-color: #16793E;
    }

    input[type=checkbox] {
        top: 1rem;
        width: 1rem;
        height: 1rem;
    }

    .box-title {
        margin-top: -0.9rem;
        margin-left: 8px;
        background-color: #ffffff;
        width: -moz-fit-content;
        width: fit-content;
    }

    body {
        background-color: #ffffff;
    }

    .card {
        border-color: #000000;
        margin-bottom: 60px;
        background-color: #ffffff
    }

    button:disabled {
        cursor: not-allowed;
        pointer-events: all !important;
    }
</style>
@endsection

@section('content')

<ul class="ml-2 breadcrumb">
    <li><a href="{{ route('borrower.list.verifikasi') }}">Verifikasi</a></li>
    <li>Detail</li>
</ul>

<div class="content mt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if (session('error'))
                <div class="alert alert-danger col-sm-12">
                    {{ session('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('success'))
                <div class="alert alert-success col-sm-12">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif(session('updated'))
                <div class="alert alert-success col-sm-12">
                    {{ session('updated') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif

            </div>
        </div>

        <div class="row mt-2">
            <div class="col-md-12">
                <button type="button" data-toggle="modal" data-target="#informasi_brw"
                    class="btn btn-info mb-2 text-white float-right rounded">Informasi Penerima Pendanaan</button>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="box-title">
                        <label class="form-check-label text-black h6 text-bold pl-2 pr-2">Daftar Pekerjaan</label>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive px-1">
                            <table id="TableVerifikasiOccasio"
                                class="table table-striped table-bordered table-responsive-sm bg-white">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Keterangan</th>
                                        <th>Terakhir Diperbaharui</th>
                                        <th style="width: 15%">Aksi</th>
                                    </tr>
                                </thead>
                            </table>

                            @if ($data_borrower_analisa_pendanaan &&
                            $data_borrower_analisa_pendanaan->status_verifikator != '3')
                            <div class="form-check form-check-inline mt-3 ml-3">
                                <input class="form-check-input" type="checkbox" id="checkListKerja" disabled>
                                <label class="form-check-label" for="checkListKerja">Hasil kerja yang dikirim adalah
                                    hasil
                                    sebenarnya dan dapat dipertanggung jawabkan</label>
                            </div>
                            @endif
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @if ($data_borrower_analisa_pendanaan && $data_borrower_analisa_pendanaan->status_verifikator != '3')
                <button type="button" class="btn btn-danaSyariah mb-2 text-white float-right rounded"
                    id="btnKonfirmasiKirim" disabled>Kirim</button>
                <button type="submit" id="btnKirimFinal" class="invisible"></button>
                @endif
            </div>
        </div>

    </div>
</div>

{{-- START: Modal View File --}}
<div id="modalViewSPK" tabindex="-1" class="modal fade in" role="dialog">
    <div class="modal-dialog modal-xl" role="document" style="max-width: 100% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Surat Perintah Kerja (SPK)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="preview">
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal View File --}}

{{-- START: Modal Informasi Borrower --}}
@component('component.admin.modal_detail_borrower')
    @slot('modal_id', 'informasi_brw')
    @slot('brw_nama', $data_borrower->nama)
    @slot('brw_nik', $data_borrower->ktp)
    @slot('brw_jns_kelamin', $data_borrower->jns_kelamin)
    @slot('brw_pic', $data_borrower->brw_pic)
@endcomponent
{{-- END: Modal Informasi Borrower --}}

<script type="text/javascript">
    var side = '/admin/borrower';
    var pengajuan_id = "{{ $pengajuan_id }}";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    table_verifikasi_occasio = ''

    $(document).ready(function() {

        loadData()
        loadChecklistKerja()

        $('#btnKirimFinal').click(function(e) {
            e.preventDefault();

            $.ajax({
                url: "{{ route('borrower.kirim.verifikator') }}",
                method: "post",
                data: { pengajuan_id: "{{ $pengajuan_id }}"},
                beforeSend: () => {
                    Swal.fire({
                        html: '<h5>Mengirim Data ...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    if (xhr.status === 419) {
                        window.alert('Halaman Session Expired. Mohon login ulang');
                    }

                },
                success: function(result) {
                    if (result == '1') {
                        location.href = "{{ route('borrower.list.verifikasi') }}"
                    }else{
                        return false;
                    }

                }
                
            });


        });

        $('#btnKonfirmasiKirim').click(function(e) {
            Swal.fire({
                title: "Notifikasi",
                text: "Apakah anda yakin ingin mengirim data ini ?",
                type: "warning",
                buttons: true,
                showCancelButton: true,
            }).then(result => {
                if (result.value == true) {
                    $('#btnKirimFinal').trigger('click');
                } else {
                    return false;
                }
            });
        });

        $('#modalViewSPK').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)
            var fileData = button.data('filedata');
            $('#preview').empty();
            $('#preview').append($('<embed>', {
                class: 'pdf_data',
                src: fileData,
                frameborder: '0',
                width: '100%',
                height: '500px'
            }))

        });

        $("#checkListKerja").click(function() {
            var checked_status = this.checked;
            if (checked_status == true) {
                $("#btnKonfirmasiKirim").removeAttr("disabled");
            } else {
                $("#btnKonfirmasiKirim").attr("disabled", "disabled");
            }
        });

    });
    const loadChecklistKerja = () => {
        $.ajax({
            url: side + '/verifikator_selesai/' + pengajuan_id,
            method: "get",
            success: function(data, value) {
                var result = $.parseJSON(data);
                console.log(result.data.jumlah_selesai)
                if (result.data.jumlah_selesai == 8 && result.data.status_verifikator != 3) {
                    $('#checkListKerja').attr('disabled', false);
                } else {
                    $('#checkListKerja').attr('disabled', true);
                }

            }
        });
    }
    const loadData = () => {
        table_verifikasi_occasio = $('#TableVerifikasiOccasio').DataTable({
            processing: false,
            serverSide: false,
            select: false,
            searchable: false,
            paging: false,
            bFilter: false,
            bPaginate: false,
            bInfo: false,
            bDestroy: true,
            autoWidth: false,
            ajax: {
                url: '/admin/borrower/listDaftarPekerjaan/' + pengajuan_id,
                type: 'get',
            },
            columnDefs: [{
                className: "align-middle", targets: "_all"
            }],
            columns: [{
                    className: "align-middle text-center",
                    data: "no"
                },
                {
                    data: "keterangan"
                },
                {
                    className: "align-middle text-center",
                    data: "terakhir_diperbaharui",
                },
                {
                    className: "align-middle text-center",
                    data: "item",
                    render: function(data, type, value, meta) {
                        let view            = (data.status == 1) ? 'Proses' : (data.status == 2) ? 'Lihat' : 'Baru';
                        let button          = (data.status == 1) ? 'btn-primary' : (data.status == 2) ? 'btn-success' : 'btn-secondary';
                        // let view            = (data.status == 1) ? 'Ubah' : (data.status == 2) ? 'Selesai' : 'Baru';
                        // let button          = (data.status == 1) ? 'btn-success' : (data.status == 2) ? 'btn-secondary' : 'btn-success';
                        let pengajuan_id    = '{!! $pengajuan_id !!}'
                        let brw_id    = '{!! $data_borrower->brw_id !!}'
                        if (data.no == 1) {
                            // let route = '{!! route("getUserFileForAdmin", ["filename" => "filename_val"]) !!}'
                            // let file_src_rep = (data.file_spk).replaceAll('/', ':')
                            // route = route.replace('filename_val', file_src_rep)

                            return `
                                <a id="btnScoreUld" class="btn btn-block btn-success text-white" href="${data.file_spk}"
                                target="_blank">Lihat</a>
                            `
                            // return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld" data-filedata="${data.file_spk}"
                            //     data-toggle="modal" data-target="#modalViewSPK">Lihat</button>
                            // `
                        } else if (data.no == 2) {
                            return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                onclick="move_page(${pengajuan_id}, ${brw_id}, 'dokumen_ceklis_verifikasi')">${view}</button>
                            `
                        } else if (data.no == 3) {
                            return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                onclick="move_page(${pengajuan_id}, ${brw_id}, 'dokumen_verifikasi_rac')">${view}</button>
                            `
                        } else if (data.no == 4) {
                            return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                onclick="move_page(${pengajuan_id}, ${brw_id}, 'form_interview_hrd')">${view}</button>
                            `
                        } else if (data.no == 5) {
                            return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                onclick="move_page(${pengajuan_id}, ${brw_id}, 'form_kunjungan_domisili')">${view}</button>
                            `
                        } else if (data.no == 6) {
                            return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                onclick="move_page(${pengajuan_id}, ${brw_id}, 'form_kunjungan_objek_pendanaan')">${view}</button>
                            `
                        } else if (data.no == 7) {
                            return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                onclick="move_page(${pengajuan_id}, ${brw_id}, 'form_informasi_pendanaan_berjalan')">${view}</button>
                            `
                        } else {

                            let view_laporan = (data.status) ? 'Unggah Ulang File' : 'Unggah File';
                            let status       = `{!! ($data_borrower_analisa_pendanaan && $data_borrower_analisa_pendanaan->status_verifikator == '3' ? '1' : '0') !!}`

                            let route = '{!! route("getUserFileForAdmin", ["filename" => "filename_val"]) !!}'
                            let file_src_rep = (data.status) ? (data.status).replaceAll('/', ':') : ''
                            route = route.replace('filename_val', file_src_rep)

                            return `
                                <form id="form_laporan_appraisal">
                                    <input type="file" name="laporan_appraisal" id="laporan_appraisal" accept=".pdf" onchange="uploadFileAppraisal()" class="d-none">
                                    <button type="button" class="btn btn-block btn-secondary ${status == 1 ? 'd-none' : ''}" id="btn_file_appraisal" onclick="$('#laporan_appraisal').click()">${view_laporan}</button>
                                    <a id="btn_show_appraisal" class="btn btn-block btn-success text-white ${data.status ? '' : 'd-none'}" href="${route}" target="_blank">Lihat</a>
                                </form>
                            `;
                        }
                    }
                }
            ]
        });
    }

    const move_page = (pengajuan_id, brw_id, page_name) => {
        let route = "{{  route('next-page-verif-occasio', ['page_name' => 'page_name_val','pengajuan_id' => 'pengajuan_id_val','brw_id' => 'brw_id_val','keterangan' => 'keterangan_val'])  }}";
        route = route.replace('page_name_val', page_name).replace('pengajuan_id_val', pengajuan_id).replace('brw_id_val', brw_id).replace('keterangan_val', page_name).replaceAll("&amp;", "&")
        window.location.href = route
    }

    const uploadFileAppraisal = () => {
        let validExtensions = ["pdf"]
        let file = $('#laporan_appraisal').val().split('.').pop();

        if (validExtensions.indexOf(file) == -1) {
            swal.fire({
                title: 'Peringatan',
                type: 'warning',
                text: 'Format file tidak sesuai',
                allowOutsideClick: () => false,
            }).then(function (result) {
                return false
            })
        } else {
            let form_data = new FormData()
            let files = $('#laporan_appraisal')[0].files
            let pengajuan_id = '{!! $pengajuan_id !!}'

            form_data.append('_token', "{{ csrf_token() }}");
            form_data.append('file_laporan_keuangan', files[0]);
            form_data.append('pengajuan_id', pengajuan_id);

            $.ajax({
                url : '{{ route("upload-laporan-appraisal") }}',
                type: 'POST',
                dataType: 'JSON',
                data: form_data,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    swal.fire({
                        html: '<h5>Mengupload File...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function (response) {

                    loadChecklistKerja()

                    if (response.status == 'success') {
                        swal.fire({
                            title: 'Berhasil',
                            type: 'success',
                            text: response.message,
                            allowOutsideClick: () => false,
                        }).then(function (result) {
                            reloadFileAppraisal(response.img_url)
                            // loadChecklistKerja()
                            loadData()
                        })
                    } else if (response.status == 'failed'){
                        swal.fire({
                            title: 'Peringatan',
                            type: 'warning',
                            text: response.message
                        })
                    }

                    $('#laporan_appraisal').val('')
                },
                error: function(response) {
                    swal.fire({
                        title: 'Error',
                        type: 'error',
                        text: response.message
                    })
                    console.log(response)
                    $('#laporan_appraisal').val('')
                }
            })
        };
    }

    const reloadFileAppraisal = (file_scr) => {
        let route = '{!! route("getUserFileForAdmin", ["filename" => "filename_val"]) !!}'
        let file_src_rep = file_scr.replaceAll('/', ':')
        route = route.replace('filename_val', file_src_rep)
        $('#btn_show_appraisal').attr('href', route)
        $('#btn_file_appraisal').text('Unggah Ulang File')
        $('#btn_show_appraisal').removeClass('d-none')
    }
</script>

@endsection