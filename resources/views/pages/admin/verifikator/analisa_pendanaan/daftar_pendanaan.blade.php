@extends('layouts.admin.master')

@section('title', 'Dashboard Borrower')

@section('style')
    <style>
        body{
            background-color: #ffffff;
        }
    </style>
@endsection

@section('content')

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Verifikasi</h1>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="container-fluid">
        
        <div class="row">
            <div class="col-md-12">
                @if (session('error'))
                <div class="alert alert-danger col-sm-12">
                    {{ session('error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif (session('success'))
                <div class="alert alert-success col-sm-12">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @elseif(session('updated'))
                <div class="alert alert-success col-sm-12">
                    {{ session('updated') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card" style="background-color: #ffffff">
                {{-- <div class="card" style="background-color: #F1F2F7"> --}}
                    <h6 class="pt-0 ml-4 pl-1 mb-n4" style="margin-top: -0.7rem; width: 137px; background-color: #ffffff">
                        Daftar Pendanaan </h6>
                    <div class="card-body">
                        <h6 class="pt-0 ml-4 pl-1 mb-n4 mt-0" style="margin-bottom: -5rem;"></h6>
                        <div class="table-responsive">
                            <table id="TableVerifikasiOccasio" class="table table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th class="text-center align-middle">No</th>
                                        <th class="text-center align-middle">ID Pengajuan</th>
                                        <th class="text-center align-middle">Tanggal Pengajuan</th>
                                        <th class="text-center align-middle">Tanggal Verifikasi</th>
                                        <th class="text-center align-middle">Penerima Pendanaan</th>
                                        <th class="text-center align-middle">Jenis Pendanaan</th>
                                        <th class="text-center align-middle">Tujuan Pendanaan</th>
                                        <th class="text-center align-middle">Nilai Pengajuan Pendanaan</th>
                                        <th class="text-center align-middle">Nama Verifikator</th>
                                        <th class="text-center align-middle">Status</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
    
                    </div>
                </div>
            </div>
        </div>
    
    </div>

    {{-- <div class="row">
        <div class="col-md-12">
            @if (session('error'))
            <div class="alert alert-danger col-sm-12">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif (session('success'))
            <div class="alert alert-success col-sm-12">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session('updated'))
            <div class="alert alert-success col-sm-12">
                {{ session('updated') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Daftar Pendanaan</strong>
                </div>
                <div class="card-body">
                    <table id="TableVerifikasiOccasio" class="table table-striped table-bordered table-responsive-sm">
                        <thead>
                            <tr>
                                <th class="text-center align-middle">No</th>
                                <th class="text-center align-middle">ID Pengajuan</th>
                                <th class="text-center align-middle">Tanggal Pengajuan</th>
                                <th class="text-center align-middle">Tanggal Verifikasi</th>
                                <th class="text-center align-middle">Penerima Pendanaan</th>
                                <th class="text-center align-middle">Jenis Pendanaan</th>
                                <th class="text-center align-middle">Tujuan Pendanaan</th>
                                <th class="text-center align-middle">Nilai Pengajuan Pendanaan</th>
                                <th class="text-center align-middle">Nama Verifikator</th>
                                <th class="text-center align-middle">Status</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div> --}}
</div>

<!-- start of modal detil -->
<div class="modal fade" id="modalProses" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Verifikasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_setuju_pendanaan" action="{{route('admin.saveProcessVerifOccasio')}}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" id="id_brw" name="id_brw">
                    <input type="hidden" id="pengajuan_id" name="pengajuan_id">
                    <input type="hidden" id="proyek_id" name="proyek_id">

                    <div class="col-lg-12 mt-2">

                        <div class="row">
                            <div class="col-sm-6">
                                <label style="font-size: larger"><b>Informasi Objek Pendanaan</b></label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tipe_pendanaan">Tipe Pendanaan</label>
                                    <input type="text" class="form-control" name="tipe_pendanaan" id="tipe_pendanaan"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="tujuan_pendanaan">Tujuan Pendanaan</label>
                                    <input type="text" class="form-control" name="tujuan_pendanaan"
                                        id="tujuan_pendanaan" placeholder="Tujuan Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="alamat_objek_pendanaan">Alamat Objek Pendanaan</label>
                                    <input type="text" class="form-control" name="alamat_objek_pendanaan"
                                        id="alamat_objek_pendanaan" placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="provinsi">Provinsi</label>
                                    <input type="text" class="form-control" name="provinsi" id="provinsi"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kota_kabupaten">Kota/Kabupaten</label>
                                    <input type="text" class="form-control" name="kota_kabupaten" id="kota_kabupaten"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kecamatan">Kecamatan</label>
                                    <input type="text" class="form-control" name="kecamatan" id="kecamatan"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kelurahan">Kelurahan</label>
                                    <input type="text" class="form-control" name="kelurahan" id="kelurahan"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kode_pos">Kode Pos</label>
                                    <input type="text" class="form-control" name="kode_pos" id="kode_pos"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="harga_objek_pendanaan">Harga Objek Pendanaan</label>
                                    <input type="text" class="form-control" name="harga_objek_pendanaan"
                                        id="harga_objek_pendanaan" placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="uang_muka">Uang Muka</label>
                                    <input type="text" class="form-control" name="uang_muka" id="uang_muka"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nilai_pengajuan_pendanaan">Nilai Pengajuan Pendanaan</label>
                                    <input type="text" class="form-control" name="nilai_pengajuan_pendanaan"
                                        id="nilai_pengajuan_pendanaan" placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="jangka_waktu">Jangka Waktu (Maks 12 Bulan)</label>
                                    <input type="text" class="form-control" name="jangka_waktu" id="jangka_waktu"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="detail_informasi_pendanaan">Detail Informasi Objek Pendanaan</label>
                                    <textarea class="form-control" id="detail_informasi_pendanaan" rows="3"
                                        readonly></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr
                        style="height:2px;border-width:0;color:black;background-color:black;width:100%; display:inline-block">

                    <div class="col-lg-12 mt-2">
                        <div class="row">
                            <div class="col-sm-6">
                                <label style="font-size: larger"><b>Informasi Pemilik Objek Pendanaan</b></label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="nama_pemilik" id="nama_pemilik"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>No Telp/HP</label>
                                    <input type="text" class="form-control" name="no_telp_pemilik" id="no_telp_pemilik"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Alamat Pemilik Pendanaan</label>
                                    <input type="text" class="form-control" name="alamat_pemilik" id="alamat_pemilik"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Provinsi</label>
                                    <input type="text" class="form-control" name="provinsi_pemilik"
                                        id="provinsi_pemilik" placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kota/Kabupaten</label>
                                    <input type="text" class="form-control" name="kota_pemilik" id="kota_pemilik"
                                        placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <input type="text" class="form-control" name="kecamatan_pemilik"
                                        id="kecamatan_pemilik" placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kelurahan</label>
                                    <input type="text" class="form-control" name="kelurahan_pemilik"
                                        id="kelurahan_pemilik" placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Kode Pos</label>
                                    <input type="text" class="form-control" name="kode_pos_pemilik"
                                        id="kode_pos_pemilik" placeholder="Tipe Pendanaan" readonly>
                                </div>
                            </div>
                        </div>

                        <br>
                    </div>

            </div>
            <div class="modal-footer">
                <button id="button_setuju_save" type="submit" class="btn btn-success">Lanjut</button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- end of modal detil -->


<script type="text/javascript">
    var side = '/admin/borrower';

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$(document).ready(function(){

		var TableVerifikasiOccasio = $('#TableVerifikasiOccasio').DataTable({
		
			processing: false,
			serverSide: false,
            dom: '<f<"mt-4 pt-4"t>ip>',
			ajax : {
				url : '/admin/borrower/listVerifikasiOccasio/',
				type : 'get',
			},
			columnDefs :[
              {
				"targets": 0,
				class : 'text-center align-middle',
				// "visible" : false,
			  },
			  {
				"targets": 1,
				class : 'text-center align-middle',
			
			  },
			  {
				"targets": 2,
				class : 'text-center align-middle',
			  },
			  {
				"targets": 3,
				class : 'text-center align-middle',
				// style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets": 4,
				class : 'text-left align-middle',
				// style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets": 5,
				class : 'text-left align-middle',
				// style : 'width:150px;',
				//"visible" : false
              },
              {
				"targets":6,
				class : 'text-left align-middle',
				// style : 'width:150px;',
				//"visible" : false
              },
              {
				"targets":7,
				class : 'text-left align-middle',
				// style : 'width:150px;',
                render : $.fn.dataTable.render.number(',', '.', 2, '')
				//"visible" : false
			  },
			  {
				"targets":8,
				class : 'text-left align-middle',
				// style : 'width:150px;',
			  },
			  {
				"targets": 9,
				class : 'text-center align-middle',
				// style : 'width:150px;',
				"render": function ( data, type, value, meta ) {
                 if(value[9]==1){
                    return '<a href="/admin/borrower/verifikator/new/'+value[1]+'" class="btn btn-block btn-secondary" id="btnNewVerifikasi">Baru</a>';
                 }else if(value[9]==2){
                    return '<a href="/admin/borrower/verifikator/new/'+value[1]+'" class="btn btn-block btn-primary">Proses</a>';
                 }else if(value[9]==3){
                    return '<a href="/admin/borrower/verifikator/new/'+value[1]+'" class="btn btn-block btn-success">Selesai</a>';
                 }else{
                    return '<a href="/admin/borrower/verifikator/new/'+value[1]+'" class="btn btn-block btn-secondary" id="btnNewVerifikasi">Baru</a>';
                 }
				}
			  },
			]
		});
		
    });

    function getDataBorrower(pengajuan_id_, proyek_id_, brw_id_){

        let pengajuan_id = pengajuan_id_;
        let proyek_id = proyek_id_;
        let id_brw = brw_id_;

        $("#id_brw").val(id_brw);
        $("#proyek_id").val(proyek_id);
        $("#pengajuan_id").val(pengajuan_id);

        $.ajax({
            url : side+'/getProsesVerifData/'+pengajuan_id,
            method: "get",
            success:function(data,value)
            {
                console.log(data);
                let tipe_pendanaan = data.jenis_pendanaan;
                let tujuan_pendanaan = data.tujuan_pendanaan;
                let alamat_objek_pendanaan = data.data_pengajuan.lokasi_proyek;
                let provinsi = data.data_pengajuan.provinsi;
                let kota_kabupaten = data.data_pengajuan.kota;
                let kecamatan = data.data_pengajuan.kecamatan;
                let kelurahan = data.data_pengajuan.kelurahan;
                let kode_pos = data.data_pengajuan.kode_pos;
                let harga_objek_pendanaan = data.data_pengajuan.harga_objek_pendanaan;
                let uang_muka = data.data_pengajuan.uang_muka;
                let nilai_pengajuan_pendanaan = data.data_pengajuan.pendanaan_dana_dibutuhkan;
                let jangka_waktu = data.data_pengajuan.durasi_proyek;
                let detail_informasi_pendanaan = data.data_pengajuan.detail_pendanaan;

                $("#tipe_pendanaan").val(tipe_pendanaan);
                $("#tujuan_pendanaan").val(tujuan_pendanaan);
                $("#alamat_objek_pendanaan").val(alamat_objek_pendanaan);
                $("#provinsi").val(provinsi);
                $("#kota_kabupaten").val(kota_kabupaten);
                $("#kecamatan").val(kecamatan);
                $("#kelurahan").val(kelurahan);
                $("#kode_pos").val(kode_pos);
                $("#harga_objek_pendanaan").val(harga_objek_pendanaan);
                $("#uang_muka").val(uang_muka);
                $("#nilai_pengajuan_pendanaan").val(nilai_pengajuan_pendanaan);
                $("#jangka_waktu").val(jangka_waktu);
                $("#detail_informasi_pendanaan").val(detail_informasi_pendanaan);

                let nama_pemilik = data.data_pengajuan.nm_pemilik;
                let no_telp_pemilik = data.data_pengajuan.no_tlp_pemilik;
                let alamat_pemilik = data.data_pengajuan.alamat_pemilik;
                let provinsi_pemilik = data.data_pengajuan.provinsi_pemilik;
                let kota_pemilik = data.data_pengajuan.kota_pemilik;
                let kecamatan_pemilik = data.data_pengajuan.kecamatan_pemilik;
                let kelurahan_pemilik = data.data_pengajuan.kelurahan_pemilik;
                let kode_pos_pemilik = data.data_pengajuan.kd_pos_pemilik;

                $("#nama_pemilik").val(nama_pemilik);
                $("#no_telp_pemilik").val(no_telp_pemilik);
                $("#alamat_pemilik").val(alamat_pemilik);
                $("#provinsi_pemilik").val(provinsi_pemilik);
                $("#kota_pemilik").val(kota_pemilik);
                $("#kecamatan_pemilik").val(kecamatan_pemilik);
                $("#kelurahan_pemilik").val(kelurahan_pemilik);
                $("#kode_pos_pemilik").val(kode_pos_pemilik);
            }
        });
    }

    function getLihatTransfer(pencairan_id_, brw_id_, proyek_id_, link)
    {

        let pencairan_id = pencairan_id_;
        let id_brw = brw_id_;
        let proyek_id = proyek_id_;
        let url = '{{asset("/storage")}}/'+link+'';

        console.log({
            'pencairan_id' : pencairan_id,
            'brw_id' : brw_id,
            'proyek_id' : proyek_id,
            'url' : url,
        })

        //alert(brw_type);
        $('#brw_id_lihat').val(id_brw);
        $('#pencairan_id_lihat').val(pencairan_id);
        $('#proyek_id_lihat').val(proyek_id);
        $('#routes_url').val(link);
        $('#id_image').attr('src', url);
	}
	
</script>

@endsection