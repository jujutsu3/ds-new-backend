<div class="card-body tabObjekPendanaan">
    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan   "> Informasi Objek Pendanaan &nbsp</label>
            </div>
            <hr>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Tipe Pendanaan</h6>
            <p class="font-w600 text-dark">{{ $pendanaanTipe }}</p>
            
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Tujuan Pendanaan</h6>
            <p class="font-w600 text-dark">{{ $tujuanPendanaan }}</p>
        </div>

        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Alamat Objek Pendanaan
            </h6>
            <p class="font-w600 text-dark" id="p_in_alamat_obj_pendanaan">
                {{ !empty($pengajuan->lokasi_proyek) ? $pengajuan->lokasi_proyek : '-'}}
            </p>
        </div>
    </div>
    <div class="row">
     
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Provinsi</h6>
            <p class="font-w600 text-dark" id="p_in_provinsi_obj_pendanaan">{{ !empty($pengajuan->provinsi) ? $pengajuan->provinsi : '-'}}</p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kota/Kabupaten</h6>
            <p class="font-w600 text-dark" id="p_in_kota_obj_pendanaan">{{ !empty($pengajuan->kota) ? $pengajuan->kota : '-' }}</p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kecamatan</h6>
            <p class="font-w600 text-dark" id="p_in_kecamatan_obj_pendanaan">{{ !empty($pengajuan->kecamatan) ? $pengajuan->kecamatan : '-' }}
            </p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kelurahan</h6>
            <p class="font-w600 text-dark" id="p_in_kelurahan_obj_pendanaan">{{ !empty($pengajuan->kelurahan) ? $pengajuan->kelurahan : '-' }}
            </p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kode Pos</h6>
            <p class="font-w600 text-dark" id="p_in_kode_pos_obj_pendanaan">{{ !empty($pengajuan->kode_pos) ? $pengajuan->kode_pos : '-' }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Harga Objek Pendanaan</h6>
                <p class="font-w600 text-dark">Rp.
                    {{ number_format($pengajuan->harga_objek_pendanaan, 2, ',', '.') }}
                </p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Uang Muka</h6>
                <p class="font-w600 text-dark">Rp.
                    {{ number_format($pengajuan->uang_muka, 2, ',', '.') }}
                </p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Nilai Pengajuan Pendanaan
            </h6>
            <p class="font-w600 text-dark">Rp.
                {{ number_format($pengajuan->pendanaan_dana_dibutuhkan, 2, ',', '.') }}
            </p>
        </div>
        <div class="col-md-4">
             <h6 class="mb-0 text-muted font-w300">Jangka Waktu (Bulan)</h6>
             <p class="font-w600 text-dark">
                 {{ $pengajuan->durasi_proyek }}</p>
        </div>
        <div class="col-md-12">
            <h6 class="mb-0 text-muted font-w300">Detail Informasi Objek
                Pendanaan</h6>
            <p class="font-w600 text-dark" id="p_in_detail_obj_pendanaan">
                {{ $pengajuan->detail_pendanaan }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="informasi_pemilik_objek_Pendanaan">Informasi Pemilik Objek Pendanaan
                    &nbsp</label>
            </div>
            <hr>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Nama</h6>
            <p class="font-w600 text-dark" id="p_in_nama_pemilik_obj">{{ !empty($pengajuan->nm_pemilik) ? $pengajuan->nm_pemilik : '-' }}
            </p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">No Telp/HP</h6>
            <p class="font-w600 text-dark" id="p_in_no_hp_pemilik_obj">
                {{ !empty($pengajuan->no_tlp_pemilik) ? $pengajuan->no_tlp_pemilik : '-'}}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Alamat Objek Pendanaan
            </h6>
            <p class="font-w600 text-dark" id="p_in_alamat_pemilik_obj">
                {{ !empty($pengajuan->alamat_pemilik) ? $pengajuan->alamat_pemilik : '-' }}</p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Provinsi</h6>
            <p class="font-w600 text-dark" id="p_in_provinsi_pemilik_obj">
                {{ !empty($pengajuan->provinsi_pemilik) ? $pengajuan->provinsi_pemilik : '-' }}</p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kota/Kabupaten</h6>
            <p class="font-w600 text-dark" id="p_in_kota_pemilik_obj">{{ !empty($pengajuan->kota_pemilik) ? $pengajuan->kota_pemilik : '-' }}
            </p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kecamatan</h6>
            <p class="font-w600 text-dark" id="p_in_kecamatan_pemilik_obj">
                {{ !empty($pengajuan->kecamatan_pemilik) ? $pengajuan->kecamatan_pemilik : '-' }}</p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kelurahan</h6>
                <p class="font-w600 text-dark" id="p_in_kelurahan_pemilik_obj">
                    {{ !empty($pengajuan->kelurahan_pemilik) ? $pengajuan->kelurahan_pemilik : '-' }}</p>
        </div>
        <div class="col-md-4">
            <h6 class="mb-0 text-muted font-w300">Kode Pos</h6>
            <p class="font-w600 text-dark" id="p_in_kode_pos_pemilik_obj">
                {{ !empty($pengajuan->kd_pos_pemilik) ? $pengajuan->kd_pos_pemilik : '-' }}</p>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
    </div>
 
  
</div>