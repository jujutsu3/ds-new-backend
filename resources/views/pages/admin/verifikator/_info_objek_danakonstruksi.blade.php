<div class="col-12 mb-4">
    <br>
    <div class="form-check form-check-inline line">
        <label class="form-check-label text-black h5" for="informasi_penghasilan">Informasi
            Objek Pendanaan</label>
    </div>
    <hr>
</div>

<div class="col-md-6">
    <div class="form-group">
        <label for="wizard-progress2-namapengguna">Tujuan Pendanaan</label>
        <input class="form-control tujuan_pendanaan" id="tujuan_pendanaan" name="tujuan_pendanaan" value="{{ $tujuanPendanaan }}" readonly>
    </div>
</div>


<div class="col-md-6">
    <div class="form-group">
        <label for="wizard-progress2-namapengguna">Tipe Pendanaan</label>
        <input class="form-control type_pendanaan" id="type_pendanaan" name="type_pendanaan" value="{{ $pendanaanTipe }}" readonly>
    </div>
</div>


<div class="col-md-6">
    <div class="form-group">
        <label for="wizard-progress2-namapendanaan">Kebutuhan Dana </label>
        <input type="text" style="width: 50%" class="form-control" id="pendanaan_dana_dibutuhkan" name="pendanaan_dana_dibutuhkan" value="Rp. {{ number_format($pengajuan->pendanaan_dana_dibutuhkan,2) }}" readonly>
    </div>
</div>


<div class="col-md-6">
    <div class="form-group">
        <label for="date_estimasi">Perkiraan estimasi proyek akan mulai dikerjakan</label>
        <input class="form-control" style="width: 30%" type="text" id="date_estimasi" name="date_estimasi" value="{{ \Carbon\Carbon::parse($pengajuan->estimasi_mulai)->format('d/m/Y') }}" readonly>
    </div>
</div>


<div class="col-md-2">
    <div class="form-group">
        <label for="wizard-progress2-namapendanaan">Jangka Waktu (Bulan)
        </label>
        <input type="text" class="form-control" id="jangka_waktu" name="jangka_waktu" readonly value="{{ $pengajuan->durasi_proyek }}">
    </div>
</div>