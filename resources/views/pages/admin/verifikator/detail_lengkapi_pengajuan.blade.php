@extends('layouts.admin.master')
@section('title', 'Detail Lengkapi Data Pengajuan')

@section('style')
    <link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }} " rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
        type="text/css" rel="stylesheet" />
    <style>
        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            background-color: #fff;
        }

        ul.breadcrumb li {
            display: inline;
            font-size: 18px;
        }

        ul.breadcrumb li+li:before {
            padding: 8px;
            color: black;
            content: "/\00a0";
        }

        ul.breadcrumb li a {
            color: #0275d8;
            text-decoration: none;
        }

        ul.breadcrumb li a:hover {
            color: #01447e;
            text-decoration: underline;
        }

        button[type=submit] {
            font-size: 14px;
        }

        .error {
            color: red;
        }

        .select2-selection {
            height: 35px !important;
        }


        .custom-file-label::after {
            content: "Pilih File" !important;
        }

    </style>
@endsection
@section('content')


    <ul class="breadcrumb">
        <li><a href="{{ route('verifikator.lengkapi_pengajuan.list') }}">Daftar Lengkapi Pengajuan</a></li>
        <li>Detail</li>
    </ul>

    <div class="content mt-3">

        <div class="row">
            <div class="col-12">
                <div class="card">

                    <div class="card-header">


                          <!-- DANA KONSTRUKSI -->
                          @if($pengajuan->pendanaan_tipe  === 1) 
                            <div class="form-group" style="width:25%; float:left;">
                                <button type="button"
                                    class="btnObjekPendanaan btn btn-default btn-lg btn-block btn-success disabled"><strong
                                        class="card-title">Informasi Objek Pendanaan</strong></button>
                            </div>

                            <div class="form-group" style="width:25%; float:left;">
                                <button type="button"
                                    class="btnRab btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Rencana Anggaran Belanja (RAB)</strong></button>
                            </div>

                            <div class="form-group" style="width:25%; float:left;">
                                <button type="button" class="btnPengajuan btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Edit Pengajuan</strong></button>
                            </div>
                            <div class="form-group" style="width:25%; float:left;">
                                <button type="button" class="btnDokumen btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Dokumen Pendukung</strong></button>
                            </div>
                        @endif 

                        <!-- DANA RUMAH -->
                        @if($pengajuan->pendanaan_tipe  === 2) 
                            <div class="form-group" style="width:33%; float:left;">
                                <button type="button"
                                    class="btnObjekPendanaan btn btn-default btn-lg btn-block btn-success disabled"><strong
                                        class="card-title">Informasi Objek Pendanaan</strong></button>
                            </div>
                            <div class="form-group" style="width:33%; float:left;">
                                <button type="button" class="btnPengajuan btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Edit Pengajuan</strong></button>
                            </div>
                            <div class="form-group" style="width:33%; float:left;">
                                <button type="button" class="btnDokumen btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Dokumen Pendukung</strong></button>
                            </div>
                        @endif     
                        
                    </div>


                    @if($brw_type === 1)
                      @include('pages.admin.verifikator._info_profile_individu')
                    @endif

                    @if($brw_type === 2)
                       @include('pages.admin.verifikator._info_profile_badanhukum')
                    @endif


                    @if($pengajuan->pendanaan_tipe === 1)
                       @include('pages.admin.verifikator.pengajuan_pendanaan_konstruksi')
                    @endif 

                    
                    @if($pengajuan->pendanaan_tipe === 2)
                        @include('pages.admin.verifikator.pengajuan_pendanaan_rumah')
                    @endif 


                      <!-- TAB RAB -->
                      @if($pengajuan->pendanaan_tipe === 1)
                          @include('pages.admin.verifikator._info_rab')
                      @endif 

                  
                    @include('pages.admin.verifikator.dokumen_pendanaan_rumah')
                 
                </div>

            </div>


        </div>

    </div>
    <script>
        $('.btnPengajuan').on('click', function() {
            $('.tabObjekPendanaan').addClass('d-none');
            $('.tabPengajuan').removeClass('d-none');
            $('.tabDokumen').addClass('d-none');
            $('.tabRab').addClass('d-none');


            $('.btnObjekPendanaan').removeClass('disabled');
            $('.btnPengajuan').addClass('disabled');
            $('.btnDokumen').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
        });


        $('.btnObjekPendanaan').on('click', function() {
            $('.tabObjekPendanaan').removeClass('d-none');
            $('.tabPengajuan').addClass('d-none');
            $('.tabDokumen').addClass('d-none');
            $('.tabRab').addClass('d-none');

            $('.btnObjekPendanaan').addClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
            $('.btnDokumen').removeClass('disabled');

        });

        $('.btnRab').on('click',function(){
            $('.tabObjekPendanaan').addClass('d-none');
            $('.tabDokumen').addClass('d-none');
            $('.tabPengajuan').addClass('d-none');
            $('.tabRab').removeClass('d-none');

            $('.btnObjekPendanaan').removeClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnRab').addClass('disabled');
            $('.btnDokumen').removeClass('disabled');
        });

        $('.btnDokumen').on('click', function() {

            $('.btnObjekPendanaan').removeClass('disabled');
            $('.btnDokumen').addClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnRab').removeClass('disabled');


            $('.tabDokumen').removeClass('d-none');
            $('.tabPengajuan').addClass('d-none');
            $('.tabObjekPendanaan').addClass('d-none');
            $('.tabRab').addClass('d-none');


          

        });
    </script>

@endsection
