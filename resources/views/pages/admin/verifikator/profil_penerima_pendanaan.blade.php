@extends('layouts.admin.master')

@section('title', 'Profil Penerima Pendanaan')

@section('style')

<style>
     .btn-danaSyariah {
            background-color: #16793E;
        }
        
    ul.breadcrumb {
      padding: 10px 16px;
      list-style: none;
      background-color: #fff;
    }
    ul.breadcrumb li {
      display: inline;
      font-size: 18px;
    }
    ul.breadcrumb li+li:before {
      padding: 8px;
      color: black;
      content: "/\00a0";
    }
    ul.breadcrumb li a {
      color: #0275d8;
      text-decoration: none;
    }
    ul.breadcrumb li a:hover {
      color: #01447e;
      text-decoration: underline;
    }
    </style>
@endsection

@section('content')


<ul class="breadcrumb">
    <li><a href="{{ route('verifikator.lengkapi_pengajuan.list') }}">Daftar Lengkapi Pengajuan</a></li>
    <li>Profil Penerima Pendanaan</li>
</ul>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
             @if($brw_type === 1)
                @include('pages.admin.verifikator._info_profile_individu')
              @endif


              @if($brw_type === 2)
                @include('pages.admin.verifikator._info_profile_badanhukum')
             @endif 

            </div>
        </div>
    </div>
</div>
</div><!-- .content -->
@endsection
