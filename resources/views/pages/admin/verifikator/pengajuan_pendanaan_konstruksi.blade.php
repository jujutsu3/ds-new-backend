<div class="card-body tabPengajuan d-none">
    <form id="form_pengajuan_pendanaan_rumah" method='post'
        action="{{ route('borrower.danarumah.pengajuan_pendanaan') }}">
        @csrf
        <input type="hidden" name="id_pengajuan" value="{{ $pengajuan->pengajuan_id }}" />
        <div class="row">
            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi Objek
                        Pendanaan Dana Konstruksi &nbsp;</label>
                </div>
                <hr>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_alamat_obj_pendanaan" class="form-label">Alamat Objek Pendanaan</label>
                    <input type="text" name="in_alamat_obj_pendanaan" id="in_alamat_obj_pendanaan"
                        value="{{ $pengajuan ? $pengajuan->lokasi_proyek : '' }}" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_provinsi_obj_pendanaan" class="form-label">Provinsi</label>
                    <select name="in_provinsi_obj_pendanaan" class="form-control custom-select"
                        id="in_provinsi_obj_pendanaan"
                        onChange="provinsiChange(this.value, this.id, 'in_kota_obj_pendanaan')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kota_obj_pendanaan" class="form-label">Kabupaten/Kota</label>
                    <select name="in_kota_obj_pendanaan" class="form-control custom-select" id="in_kota_obj_pendanaan"
                        onChange="kotaChange(this.value, this.id, 'in_kecamatan_obj_pendanaan')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>


            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kecamatan_obj_pendanaan" class="form-label">Kecamatan</label>
                    <select name="in_kecamatan_obj_pendanaan" class="form-control custom-select"
                        id="in_kecamatan_obj_pendanaan"
                        onChange="kecamatanChange(this.value, this.id, 'in_kelurahan_obj_pendanaan')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kelurahan_obj_pendanaan" class="form-label">Kelurahan</label>
                    <select name="in_kelurahan_obj_pendanaan" class="form-control custom-select"
                        id="in_kelurahan_obj_pendanaan"
                        onChange="kelurahanChange(this.value, this.id, 'in_kode_pos_obj_pendanaan')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kode_pos_obj_pendanaan" class="form-label">Kode Pos</label>
                    <input type="text" name="in_kode_pos_obj_pendanaan" id="in_kode_pos_obj_pendanaan" placeholder="--"
                        class="form-control" value="{{ $pengajuan->kode_pos }}" readonly />
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="in_dtl_obj_pendanaan" class="form-label">Detil Informasi Rumah</label>
                    <textarea class="form-control" rows="6" cols="80" id="in_dtl_obj_pendanaan"
                        name="in_dtl_obj_pendanaan">{{ $pengajuan ? $pengajuan->detail_pendanaan : '' }}</textarea>
                </div>
            </div>

            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi Pemilik
                        Objek Pendanaan &nbsp;</label>
                </div>
                <hr>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_nama_pemilik_obj" class="form-label">Nama</label>
                    <input type="text" name="in_nama_pemilik_obj" id="in_nama_pemilik_obj"
                        value="{{ $pengajuan ? $pengajuan->nm_pemilik : '' }}" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>

            <div class="col-md-4">

                <label for="in_no_hp_pemilik_obj">No.
                    Telepon/HP</label> <label class="mandatory_label">*</label>
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text input-group-text-dsi">
                            +62
                        </span>
                    </div>
                    <input class="form-control no-zero" id="in_no_hp_pemilik_obj" name="in_no_hp_pemilik_obj"
                        onkeyup="this.value = this.value.replace(/[^\d/]/g,'')" minlength="9" maxlength="13"
                        placeholder="ketik disini"
                        value="{{ $pengajuan ? substr($pengajuan->no_tlp_pemilik, 2) : '' }}">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_alamat_pemilik_obj" class="form-label">Alamat Pemilik Objek Pendanaan</label>
                    <input type="text" name="in_alamat_pemilik_obj" id="in_alamat_pemilik_obj"
                        value="{{ $pengajuan ? $pengajuan->alamat_pemilik : '' }}" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_provinsi_pemilik_obj" class="form-label">Provinsi</label>
                    <select name="in_provinsi_pemilik_obj" class="form-control custom-select"
                        id="in_provinsi_pemilik_obj"
                        onChange="provinsiChange(this.value, this.id, 'in_kota_pemilik_obj')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kota_pemilik_obj" class="form-label">Kabupaten/Kota</label>
                    <select name="in_kota_pemilik_obj" class="form-control custom-select" id="in_kota_pemilik_obj"
                        onChange="kotaChange(this.value, this.id, 'in_kecamatan_pemilik_obj')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kecamatan_pemilik_obj" class="form-label">Kecamatan</label>
                    <select name="in_kecamatan_pemilik_obj" class="form-control custom-select"
                        id="in_kecamatan_pemilik_obj"
                        onChange="kecamatanChange(this.value, this.id, 'in_kelurahan_pemilik_obj')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kelurahan_pemilik_obj" class="form-label">Kelurahan</label>
                    <select name="in_kelurahan_pemilik_obj" class="form-control custom-select"
                        id="in_kelurahan_pemilik_obj"
                        onChange="kelurahanChange(this.value, this.id, 'in_kode_pos_pemilik_obj')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="in_kode_pos_pemilik_obj" class="form-label">Kode Pos</label>
                    <input type="text" name="in_kode_pos_pemilik_obj" id="in_kode_pos_pemilik_obj" placeholder="--"
                        class="form-control" value="{{ $pengajuan->kd_pos_pemilik }}" readonly />
                </div>
            </div>

            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi
                        Pendanaan Berjalan &nbsp;</label>
                </div>
                <hr>
            </div>

        

            <div class="col-12 col-md-12">
                &nbsp;
            </div>
            <div class="col-12 col-md-12">
                <div class="table-responsive">
                    <table class="table table-sm d-none" id="table_dana_rumah_lain">
                        <thead class="thead-dark" style="font-size: 12px;">
                            <tr>
                                <th>No.</th>
                                <th>Status</th>
                                <th>Bank</th>
                                <th>Plafond (Rp)</th>
                                <th>Jangka Waktu</th>
                                <th>Outstanding (Rp)</th>
                                <th>Angsuran (Rp)</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="col-12 col-md-12">
                <div class="col-8 my-auto">
                    <label class="form-label">Apakah anda memiliki fasilitas
                        pembiayaan konstruksi lainnya ?
                    </label>
                </div>
                <div class="col-4 text-right my-auto">
                    <select name="status_bank" id="status_bank" class="form-control custom-select">
                        <option value="1" {{ $status_bank_non_rumah_lain == "1" ? 'selected' : '' }}>Ya</option>
                        <option value="2" {{ $status_bank_non_rumah_lain == "2" ? 'selected' : '' }}>Tidak</option>
                    </select>
                </div>

            </div>


            <div class="col-12 col-md-12">
                &nbsp;
            </div>

            <div class="col-12 col-md-12">
                <div class="table-responsive">
                    <table class="table table-sm d-none" id="table_dana_non_rumah_lain">
                        <thead class="thead-dark" style="font-size: 12px;">
                            <tr>
                                <th>Bank</th>
                                <th>Plafond (Rp)</th>
                                <th>Jangka Waktu</th>
                                <th>Outstanding (Rp)</th>
                                <th>Angsuran (Rp)</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="col-12 mb-4">
                <br>
                <div class="form-check form-check-inline line">
                    <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi Agunan
                        &nbsp;</label>
                </div>
                <hr>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="jenis_agunan" class="form-label">Jenis
                        Agunan</label>
                    {!! Form::select('jenis_agunan', ['1' => 'SHM', '2' => 'HGB'], $pengajuan->jenis_agunan, ['id' => 'jenis_agunan', 'class' => 'form-control custom-select']) !!}
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="nomor_agunan" class="form-label">Nomor
                        Agunan</label>
                    <input type="text" name="nomor_agunan" value="{{ $pengajuan->nomor_agunan }}" id="nomor_agunan"
                        placeholder="ketik disini" class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="nomor_agunan" class="form-label">Atas Nama
                        Agunan</label>
                    <input type="text" name="atas_nama_agunan" id="atas_nama_agunan"
                        value="{{ $pengajuan->atas_nama_agunan }}" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="luas_bangunan" class="form-label">Luas Bangunan
                        (m2)</label>
                    <input type="text" name="luas_bangunan" id="luas_bangunan"
                        value="{{ $pengajuan->luas_bangunan }}" placeholder="ketik disini"
                        class="form-control numberOnly no-zero" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="luas_tanah" class="form-label">Luas Tanah
                        (m2)</label>
                    <input type="text" name="luas_tanah" id="luas_tanah" value="{{ $pengajuan->luas_tanah }}"
                        placeholder="ketik disini" class="form-control numberOnly no-zero" />
                </div>
            </div>


            <div class="col-md-4">
                <div class="form-group">
                    <label for="nomor_surat_ukur" class="form-label">Nomor Surat
                        Ukur </label>
                    <input type="text" name="nomor_surat_ukur" id="nomor_surat_ukur"
                        value="{{ $pengajuan->nomor_surat_ukur }}" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="nomor_surat_ukur" class="form-label">Tanggal Surat
                        Ukur </label>
                    <input type="date" class="form-control bg-white" id="tanggal_surat_ukur" data-min-date="today"
                        data-date-format="d-m-Y" name="tanggal_surat_ukur"
                        value="{{ $pengajuan->tanggal_surat_ukur }}" id="tanggal_surat_ukur" placeholder="dd-mm-YYYY"
                        data-allow-input="false">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="nomor_surat_ukur" class="form-label">Tanggal Terbit
                    </label>
                    
                    <input type="date" class="form-control bg-white" id="tanggal_terbit" data-min-date="today"
                        data-date-format="dd-mm-YYYY" name="tanggal_terbit"  value="{{ $pengajuan->tanggal_terbit }}" placeholder="dd-mm-YYYY"
                        data-allow-input="false">
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="kantor_penerbit" class="form-label">Kantor
                        Penerbit</label>

                    {!! Form::select('kantor_penerbit', $kantor_bpn, $pengajuan->kantor_penerbit, ['id' => 'kantor_penerbit', 'class' => 'form-control custom-select']) !!}
                </div>
            </div>

            <div class="col-md-4 d-none" id="div_tanggal_jatuh_tempo">
                <div class="form-group">
                    <label for="nomor_surat_ukur" class="form-label">Tanggal Jatuh Tempo
                    </label>
                    <input type="date" class="form-control bg-white" id="tanggal_jatuh_tempo" data-min-date="today"
                        data-date-format="d-m-Y" name="tanggal_jatuh_tempo"
                        value="{{ $pengajuan->tanggal_jatuh_tempo }}" placeholder="dd-mm-YYYY"
                        data-allow-input="false" required>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="alamat_agunan" class="form-label">Alamat
                        Agunan</label>
                    <input type="text" name="alamat_agunan" id="alamat_agunan"
                        value="{{ $pengajuan->lokasi_proyek }}" class="form-control"  />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="blok_nomor" class="form-label">Blok/Nomor
                    </label>
                    <input type="text" id="blok_nomor" name="blok_nomor" value="{{ $pengajuan->blok_nomor }}"
                        placeholder="ketik disini" class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="RT" class="form-label">RT
                    </label>
                    <input type="text" name="RT" value="{{ $pengajuan->RT }}" id="RT" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="RW" class="form-label">RW
                    </label>
                    <input type="text" name="RW" value="{{ $pengajuan->RW }}" id="RW" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="provinsi" class="form-label">Provinsi *
                    </label>
                    <select name="provinsi" class="form-control custom-select" id="provinsi" disabled>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="kota" class="form-label">Kabupaten/Kota *
                    </label>
                    <select name="kota" class="form-control custom-select" id="kota" disabled>
                    </select>
                </div>
            </div>


            <div class="col-md-4">
                <div class="form-group">
                    <label for="kecamatan" class="form-label">Kecamatan *
                    </label>
                    <select name="kecamatan" class="form-control custom-select" id="kecamatan" disabled>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="kelurahan" class="form-label">Kelurahan *
                    </label>
                    <select name="kelurahan" class="form-control custom-select" id="kelurahan" disabled>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="kode_pos" class="form-label">Kode Pos
                    </label>
                    <input type="text" name="kode_pos" id="kode_pos" value="{{ $pengajuan->kode_pos }}" readonly
                        class="form-control" />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="cek_imb" class="form-label">IMB
                    </label>
                    <select name="cek_imb" class="form-control custom-select" id="cek_imb">
                        <option value="1" {{ !empty($pengajuan->no_imb) ? 'selected' : '' }}>Ada</option>
                        <option value="2" {{ empty($pengajuan->no_imb) ? 'selected' : '' }}>Tidak Ada</option>
                    </select>
                </div>
            </div>

            <div class="col-md-4" id="div_no_imb">
                <div class="form-group">
                    <label for="no_imb" class="form-label">Nomor IMB
                    </label>
                    <input type="text" name="no_imb" id="no_imb" value="{{ $pengajuan->no_imb }}"
                        placeholder="ketik disini" class="form-control" />
                </div>
            </div>


            <div class="col-md-4" id="div_tanggal_imb">
                <div class="form-group">
                    <label for="tanggal_terbit_imb" class="form-label">Tanggal
                        Terbit IMB
                    </label>
                    <input type="date" class="form-control bg-white" id="tanggal_terbit_imb" data-min-date="today"
                        data-date-format="d-m-Y" name="tanggal_terbit_imb"
                        value="{{ $pengajuan->tanggal_terbit_imb }}" placeholder="dd-mm-YYYY"
                        data-allow-input="false">
                </div>
            </div>

            <div class="col-md-12">
                <div class="col-12 text-right">
                   @if($enable_edit)
                        <button type="button" class="btn btn-success text-left" href="#" id="updatePengajuanRumah"><i
                            class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                  @else 
                    <button type="button" class="btn btn-success text-left" href="#" id="updatePengajuanRumah" disabled><i
                        class="fa fa-save" aria-hidden="true"></i> Data sudah tidak dapat di ubah</button>  
                   @endif
                </div>
            </div>
        </div>
    </form>
</div>

<link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
<script src="{{ url('assetsBorrower/js/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ url('assetsBorrower/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script>
    const formatRupiah = (angka, prefix) => {
        let thisValue = angka.replace(/[^,\d]/g, '')
            angka = (parseInt(thisValue, 10)).toString()
            
        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }


    $(function() {

        var count_dana_non_rumah_lain = "{{ $count_dana_non_rumah_lain }}";
        var x = (count_dana_non_rumah_lain > 0) ? count_dana_non_rumah_lain : 1;

        load_data_rumah_lain();
        load_data_non_rumah_lain();

        var status_bank_non_rumah_lain = "{{ $status_bank_non_rumah_lain }}";
        var pengajuan_id = "{{ $pengajuan->pengajuan_id }}";
        var pengajuan_provinsi = "{{ $pengajuan->provinsi }}";
        var pengajuan_kota = "{{ $pengajuan->kota }}";
        var pengajuan_kecamatan = "{{ $pengajuan->kecamatan }}";
        var pengajuan_kelurahan = "{{ $pengajuan->kelurahan }}";
        var status_pengajuan = "{{ $pengajuan->status }}";

        $("#kantor_penerbit").select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Kantor Penerbit --",
            allowClear: true,
            width: '100%'
        });

      

        $(".numberOnly").on("input", function() {
            this.value = this.value.replace(/[^0-9]/g, "");
        });

        $(".no-zero").on("input", function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });

        $("#form_pengajuan_pendanaan_rumah").validate({
            onfocusout: false,
            invalidHandler: function(e, validator) {
                if (validator.errorList.length) {
                    validator.errorList[0].element.focus()
                }
            }
        });


        if ($("#jenis_agunan option:selected").val() == '1') {
            $("#div_tanggal_jatuh_tempo").addClass('d-none');
        } else {
            $("#div_tanggal_jatuh_tempo").removeClass('d-none');
        }


        if ($("#cek_imb option:selected").val() == '1') {
            $("#div_no_imb").removeClass('d-none');
            $("#div_tanggal_imb").removeClass('d-none');
            $("#div_dokumen_imb").removeClass('d-none');

            $("#no_imb").prop('required', 'true');
            $("#tanggal_terbit_imb").prop('required', 'true');
        } else {
            $("#div_no_imb").addClass('d-none');
            $("#div_tanggal_imb").addClass('d-none');
            $("#div_dokumen_imb").addClass('d-none');

            $("#no_imb").prop('required', 'false');
            $("#tanggal_terbit_imb").prop('required', 'false');

            $("#no_imb").val('');
            $("#tanggal_terbit_imb").val('');
        }

        $("#jenis_agunan").change(function(e) {
            if ($(this).val() == '2') {
                $("#div_tanggal_jatuh_tempo").removeClass('d-none');
                $("#tanggal_jatuh_tempo").prop('required', 'true');
            } else {
                $("#div_tanggal_jatuh_tempo").addClass('d-none');
                $("#tanggal_jatuh_tempo").prop('required', 'false');
            }

        });



        $("#status_bank").change(function(e) {
            if ($(this).val() == "1") {
                $('#table_dana_non_rumah_lain').removeClass('d-none');
                (status_bank_non_rumah_lain == "2") ? $('#table_dana_non_rumah_lain > tbody').append(
                    buildRowFirstNonRumahLain()): '';
            } else {
                $('#table_dana_non_rumah_lain').addClass('d-none');

                (status_bank_non_rumah_lain == "2") ? $('#table_dana_non_rumah_lain > tbody').html(''):
                    '';
            }

        });

        $(document).on('click', '.btn_remove_non_rumah_lain', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
            x--;
        });

        $("#rumah_ke").change(function(e) {
            var length = $(this).val() - 1;
            var buildRow = "";

            $('#table_dana_rumah_lain > tbody').html("");
            if (length > 0) {

                $('#table_dana_rumah_lain').removeClass('d-none');
                $('input[name=kepemilikan_rumah_ke]').val(length);

                for (var i = 1; i <= length; i++) {
                    buildRow += '<tr><td>' + i + '</td>';
                    buildRow +=
                        '<td> <select name="status_rumah_lain[]" class="form-control custom-select" id="status_rumah_lain_' +
                        i + '">';
                    buildRow += '<option value="1">Sedang Berjalan</option>';
                    buildRow += '<option value="2">Lunas</option></select></td>';
                    buildRow +=
                        '<td><input type="text" id="bank_rumah_lain_' + i +
                        '" name="bank_rumah_lain[]"  class="form-control" maxlength="30" /></td>';
                    buildRow +=
                        '<td><input type="text" id="plafond_rumah_lain_' + i +
                        '" name="plafond_rumah_lain[]"  class="form-control"  onkeyup="this.value = formatRupiah(this.value);"/></td>';
                    buildRow +=
                        '<td><input type="text" id="jangka_waktu_rumah_lain_' + i +
                        '" name="jangka_waktu_rumah_lain[]"  class="form-control" /></td>';
                    buildRow +=
                        '<td><input type="text" id="outstanding_rumah_lain_' + i +
                        '"  name="outstanding_rumah_lain[]"  class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
                    buildRow +=
                        '<td><input type="text" id="angsuran_rumah_lain_' + i +
                        '"  name="angsuran_rumah_lain[]"  class="form-control"  onkeyup="this.value = formatRupiah(this.value);" /></td>';
                    buildRow += '</tr>';
                }
                $('#table_dana_rumah_lain > tbody').append(buildRow);
            } else {
                $('#table_dana_rumah_lain').addClass('d-none');
                $('#table_dana_rumah_lain > tbody').html("");
                $('input[name=kepemilikan_rumah_ke]').val(length);

            }



        });

        $(document).on('click', '#add_dana_non_rumahlain', function(e) {
            var buildRowNonRumah = "";
            x++;
            buildRowNonRumah += '<tr id="row' + x + '">';
            buildRowNonRumah +=
                '<td><input type="text" name="bank_non_rumah_lain[]" id="bank_non_rumah_lain_' + x +
                '"  placeholder="ketik disini" class="form-control" maxlength="30" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="plafond_non_rumah_lain[]" id="plafond_non_rumah_lain_' +
                x +
                '"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="jangkawaktu_non_rumah_lain[]" id="jangkawaktu_non_rumah_lain_' +
                x + '"  placeholder="ketik disini" class="form-control" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="outstanding_non_rumah_lain[]" id="outstanding_non_rumah_lain_' +
                x +
                '"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="angsuran_non_rumah_lain[]" id="angsuran_non_rumah_lain_' +
                x +
                '"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah += '<td> <button type="button" name="remove" id="' + x +
                '" class="btn btn-danger btn_remove_non_rumah_lain"><i class="fa fa-trash"></i></button></td></tr>';
            $('#table_dana_non_rumah_lain > tbody').append(buildRowNonRumah);
        });

        $("#cek_imb").change(function(e) {
            if ($(this).val() == '2') {
                $("#div_no_imb").addClass('d-none');
                $("#div_tanggal_imb").addClass('d-none');
                $("#div_dokumen_imb").addClass('d-none');

                $("#no_imb").val('');
                $("#tanggal_terbit_imb").val('');
            } else {
                $("#div_no_imb").removeClass('d-none');
                $("#div_tanggal_imb").removeClass('d-none');
                $("#div_dokumen_imb").removeClass('d-none');
            }

        });


        $("#updatePengajuanRumah").click(function() {
            if ($("#form_pengajuan_pendanaan_rumah").valid()) {

                Swal.fire({
                    title: "Notifikasi",
                    text: "Apakah anda yakin menyimpan data ini ?",
                    type: "warning",
                    buttons: true,
                    showCancelButton: true,
                }).then(result => {
                    if (result.value == true) {
                        $("#form_pengajuan_pendanaan_rumah").trigger("submit");
                    } else {
                        return false;
                    }
                });
            }
        });

        $('#btn_simpan_konfirmasi').click(function() {
            $("#form_pengajuan_pendanaan_rumah").trigger("submit");
        });


        function buildRowFirstNonRumahLain() {
            var buildRowNonRumah = '';
            buildRowNonRumah += '<tr id="row1">';
            buildRowNonRumah +=
                '<td><input type="text" name="bank_non_rumah_lain[]" id="bank_non_rumah_lain_1" placeholder="ketik disini" class="form-control" maxlength="30" required/></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="plafond_non_rumah_lain[]" id="plafond_non_rumah_lain_1"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="jangkawaktu_non_rumah_lain[]" id="jangkawaktu_non_rumah_lain_1" placeholder="ketik disini" class="form-control" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="outstanding_non_rumah_lain[]" id="outstanding_non_rumah_lain_1" placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="angsuran_non_rumah_lain[]" id="angsuran_non_rumah_lain_1" placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                ' <td><button type="button" class="btn btn-primary" id="add_dana_non_rumahlain"><i class="fa fa-plus"></i></button></td>';
            buildRowNonRumah += '</tr>';
            return buildRowNonRumah;
        }

        function load_data_non_rumah_lain() {
            $.ajax({
                url: "{{ route('borrower.danarumah.nonrumahlain') }}",
                type: "post",
                dataType: "html",
                data: {
                    _token: "{{ csrf_token() }}",
                    pengajuan_id: "{{ $pengajuan->pengajuan_id }}"
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText

                },
                success: function(result) {
                    if (result) {
                        $('#table_dana_non_rumah_lain').removeClass('d-none');
                        $('#table_dana_non_rumah_lain > tbody').html(result);
                    }

                }
            });

        }

        function load_data_rumah_lain() {
            $.ajax({
                url: "{{ route('borrower.danarumah.rumahlain') }}",
                type: "post",
                dataType: "html",
                data: {
                    _token: "{{ csrf_token() }}",
                    pengajuan_id: "{{ $pengajuan->pengajuan_id }}"
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText

                },
                success: function(result) {
                    if (result) {
                        $('#table_dana_rumah_lain').removeClass('d-none');
                        $('#table_dana_rumah_lain > tbody').html(result);
                    }

                }
            });

        }

        $("form#form_pengajuan_pendanaan_rumah").submit(function(e) {
            e.preventDefault();

            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: () => {
                    swal.fire({
                        html: '<h5>Menyimpan Data ...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(data) {

                    if (data.status == 'success') {
                        swal.fire({
                            title: "Proses Berhasil",
                            text: "Simpan Data Berhasil",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        }).then((result) => {
                            $('.nav-tabs a[href="#dokumen-pendanaan"]').tab('show');
                            $('#p_in_alamat_obj_pendanaan').text(data.in_obj_pendanaan.in_alamat_obj_pendanaan);
                            $('#p_in_provinsi_obj_pendanaan').text(data.in_obj_pendanaan.in_provinsi_obj_pendanaan);
                            $('#p_in_kota_obj_pendanaan').text(data.in_obj_pendanaan.in_kota_obj_pendanaan);
                            $('#p_in_kecamatan_obj_pendanaan').text(data.in_obj_pendanaan.in_kecamatan_obj_pendanaan);
                            $('#p_in_kelurahan_obj_pendanaan').text(data.in_obj_pendanaan.in_kelurahan_obj_pendanaan);
                            $('#p_in_kode_pos_obj_pendanaan').text(data.in_obj_pendanaan.in_kode_pos_obj_pendanaan);
                            $("#p_in_detail_obj_pendanaan").text(data.in_obj_pendanaan.in_dtl_obj_pendanaan);
                            $("#p_in_nama_pemilik_obj").text(data.in_obj_pendanaan.in_nama_pemilik_obj);
                            $("#p_in_no_hp_pemilik_obj").text(data.in_obj_pendanaan.in_no_hp_pemilik_obj);
                            $("#p_in_alamat_pemilik_obj").text(data.in_obj_pendanaan.in_alamat_pemilik_obj);
                            $("#p_in_provinsi_pemilik_obj").text(data.in_obj_pendanaan.in_provinsi_pemilik_obj);
                            $("#p_in_kota_pemilik_obj").text(data.in_obj_pendanaan.in_kota_pemilik_obj);
                            $("#p_in_kecamatan_pemilik_obj").text(data.in_obj_pendanaan.in_kecamatan_pemilik_obj);
                            $("#p_in_kelurahan_pemilik_obj").text(data.in_obj_pendanaan.in_kelurahan_pemilik_obj);
                            $("#p_in_kode_pos_pemilik_obj").text(data.in_obj_pendanaan.in_kode_pos_pemilik_obj);
                        });
                    } else {
                        let error_msg = data.message
                        swal.fire({
                            title: "Proses Gagal",
                            text: "Simpan Data Gagal",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        }).then((result) => {

                        })
                    }
                },
                error: function(data) {
                    // console.log(data)
                    swal.fire({
                        title: "Proses Gagal",
                        type: "error",
                        text: data,
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                    })
                },
            })
        });

        for (var i = 1; i <= 2; i++) {
            $(document).on('change', '#status_rumah_lain_' + i, {
                'index': i
            }, function(e) {
                var index = e.data.index;
                $status_rumah_lain = $('#status_rumah_lain_' + index).val();
                if ($status_rumah_lain == '2') {

                    $('#bank_rumah_lain_' + index).val('');
                    $('#bank_rumah_lain_' + index).prop('readonly', true);

                    $('#plafond_rumah_lain_' + index).val('');
                    $('#plafond_rumah_lain_' + index).prop('readonly', true);

                    $('#jangka_waktu_rumah_lain_' + index).val('');
                    $('#jangka_waktu_rumah_lain_' + index).prop('readonly', true);


                    $('#outstanding_rumah_lain_' + index).val('');
                    $('#outstanding_rumah_lain_' + index).prop('readonly', true);

                    $('#angsuran_rumah_lain_' + index).val('');
                    $('#angsuran_rumah_lain_' + index).prop('readonly', true);

                } else {

                    $('#bank_rumah_lain_' + index).prop('readonly', false);
                    $('#plafond_rumah_lain_' + index).prop('readonly', false);
                    $('#jangka_waktu_rumah_lain_' + index).prop('readonly', false);
                    $('#outstanding_rumah_lain_' + index).prop('readonly', false);
                    $('#angsuran_rumah_lain_' + index).prop('readonly', false);

                }

            });

        }


    });
</script>

<script>
    $(document).ready(function() {
        $(`#in_provinsi_obj_pendanaan, #in_provinsi_pemilik_obj, 
        #in_kota_obj_pendanaan, #in_kota_pemilik_obj, 
        #in_kecamatan_obj_pendanaan, #in_kecamatan_pemilik_obj,
        #in_kelurahan_obj_pendanaan, #in_kelurahan_pemilik_obj
        `).prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%',
            height: '100%'
        });


        
      })

    const provinsiChange = (thisValue, thisId, nextId) => {
        $('#' + nextId).empty(); // set null
        $('#' + nextId).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kota/" + thisValue + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#' + nextId).append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
            }
        });
    }

    const kotaChange = (thisValue, thisId, nextId) => {
        $('#' + nextId).empty(); // set null
        $('#' + nextId).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kecamatan/" + thisValue + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#' + nextId).append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
            }
        });
    }

    const kecamatanChange = (thisValue, thisId, nextId) => {
        $('#' + nextId).empty(); // set null
        $('#' + nextId).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kelurahan/" + thisValue + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#' + nextId).append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
            }
        });
    }

    const kelurahanChange = (thisValue, thisId, nextId) => {
        $.getJSON("/borrower/data_kode_pos/" + thisValue + "/", function(data) {
            $('#' + nextId).val(data[0].text)
            $('#kode_pos').val(data[0].text)
        });
    }

    // Load data from db
    const getProvinsiData = (data_provinsi, element_id) => {
        $('#' + element_id).empty(); // set null
        $('#' + element_id).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_provinsi", function(data) {
            for (let i = 0; i < data.length; i++) {
                let option = $(
                        `<option ${data[i].text == data_provinsi ? selected='selected' : ''}></option>`)
                    .val(data[i].text).text(data[i].text)
                $("#" + element_id).append(option);
            }
        })
    }

    const getProvinsiPemilikData = (data_provinsi, element_id) => {
        $('#' + element_id).empty(); // set null
        $('#' + element_id).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_provinsi", function(data) {
            for (let i = 0; i < data.length; i++) {
                let option = $(
                        `<option ${data[i].text == data_provinsi ? selected='selected' : ''}></option>`)
                    .val(data[i].text).text(data[i].text)
                $("#" + element_id).append(option);
            }
        })
    }

    const getKotaData = (data_provinsi, this_data_kota, element_id) => {
        $('#' + element_id).empty(); // set null
        $('#' + element_id).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kota/" + data_provinsi + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                let option = $(
                        `<option ${data[i].text == this_data_kota ? selected='selected' : ''}></option>`)
                    .val(data[i].text).text(data[i].text)
                $("#" + element_id).append(option);
            }
        });
    }

    const getKecamatan = (data_kota, this_data_kecamatan, element_id) => {
        $('#' + element_id).empty(); // set null
        $('#' + element_id).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kecamatan/" + data_kota + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                let option = $(
                    `<option ${data[i].text == this_data_kecamatan ? selected='selected' : ''}></option>`
                ).val(data[i].text).text(data[i].text)
                $("#" + element_id).append(option);
            }
        });
    }

    const getKelurahanData = (data_kecamatan, this_data_kelurahan, element_id) => {
        $('#' + element_id).empty(); // set null
        $('#' + element_id).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kelurahan/" + data_kecamatan + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                let option = $(
                    `<option ${data[i].text == this_data_kelurahan ? selected='selected' : ''}></option>`
                ).val(data[i].text).text(data[i].text)
                $("#" + element_id).append(option);
            }
        });
    }

    let provinsi_obj_pendanaan = "{{ $pengajuan->provinsi }}";
    let kota_obj_pendanaan = "{{ $pengajuan->kota }}";
    let kecamatan_obj_pendanaan = "{{ $pengajuan->kecamatan }}";
    let kelurahan_obj_pendanaan = "{{ $pengajuan->kelurahan }}";

    let provinsi_pemilik_obj_pendanaan = "{{ $pengajuan->provinsi_pemilik }}";
    let kota_pemilik_obj_pendanaan = "{{ $pengajuan->kota_pemilik }}";
    let kecamatan_pemilik_obj_pendanaan = "{{ $pengajuan->kecamatan_pemilik }}";
    let kelurahan_pemilik_obj_pendanaan = "{{ $pengajuan->kelurahan_pemilik }}";

    getProvinsiData(provinsi_obj_pendanaan, 'in_provinsi_obj_pendanaan')
    getKotaData(provinsi_obj_pendanaan, kota_obj_pendanaan, 'in_kota_obj_pendanaan')
    getKecamatan(kota_obj_pendanaan, kecamatan_obj_pendanaan, 'in_kecamatan_obj_pendanaan')
    getKelurahanData(kecamatan_obj_pendanaan, kelurahan_obj_pendanaan, 'in_kelurahan_obj_pendanaan')

    getProvinsiPemilikData(provinsi_pemilik_obj_pendanaan, 'in_provinsi_pemilik_obj')
    getKotaData(provinsi_pemilik_obj_pendanaan, kota_pemilik_obj_pendanaan, 'in_kota_pemilik_obj')
    getKecamatan(kota_pemilik_obj_pendanaan, kecamatan_pemilik_obj_pendanaan, 'in_kecamatan_pemilik_obj')
    getKelurahanData(kecamatan_pemilik_obj_pendanaan, kelurahan_pemilik_obj_pendanaan, 'in_kelurahan_pemilik_obj')

    getProvinsiData(provinsi_obj_pendanaan, 'provinsi')
    getKotaData(provinsi_obj_pendanaan, kota_obj_pendanaan, 'kota')
    getKecamatan(kota_obj_pendanaan, kecamatan_obj_pendanaan, 'kecamatan')
    getKelurahanData(kecamatan_obj_pendanaan, kelurahan_obj_pendanaan, 'kelurahan')

    function populate_data_kota(pengajuan_provinsi) {

        $.getJSON("/borrower/data_kota/" + pengajuan_provinsi + "/", function(data_kota) {
            for (var i = 0; i < data_kota.length; i++) {
                $('#kota').append($('<option>', {
                    value: data_kota[i].text,
                    text: data_kota[i].text
                }));
            }

            $("#kota option[value='" + pengajuan_kota + "']").attr('selected', 'selected');
        });

    }

    function populate_data_kecamatan(pengajuan_kota) {

        $.getJSON("/borrower/data_kecamatan/" + pengajuan_kota + "/", function(data_kecamatan) {
            for (var i = 0; i < data_kecamatan.length; i++) {
                $('#kecamatan').append($('<option>', {
                    value: data_kecamatan[i].text,
                    text: data_kecamatan[i].text
                }));
            }

            $("#kecamatan option[value='" + pengajuan_kecamatan + "']").attr('selected',
                'selected');
        });

    }

    function populate_data_kelurahan(pengajuan_kecamatan) {

        $.getJSON("/borrower/data_kelurahan/" + pengajuan_kecamatan + "/", function(data_kelurahan) {
            for (var i = 0; i < data_kelurahan.length; i++) {
                $('#kelurahan').append($('<option>', {
                    value: data_kelurahan[i].text,
                    text: data_kelurahan[i].text
                }));
            }

            $("#kelurahan option[value='" + pengajuan_kelurahan + "']").attr('selected',
                'selected');
        });

    }
</script>
