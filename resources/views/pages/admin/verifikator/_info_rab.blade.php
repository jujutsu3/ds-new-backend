<div class="card-body tabRab d-none">
    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5"
                    for="form_pengajuan_rab">Pengajuan Barang</label>
            </div>
            <hr>
        </div>
    </div>
   
   <div class="row">
        <div class="form-group">
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="col-md-3 control-label">
                        <label> Tanggal Permintaan </label>
                     </div> 
                    <div class="col-md-9">
                        <input type="text" name="requitionDate" class="form-control" value="{{ ($requisitionHeader) ? \Carbon\Carbon::parse($requisitionHeader->requisition_date)->format('d/m/Y') : "-" }}"  readonly/>
                    </div>

                </div>    
            </div>
            <div class="row">
                <div class="col-12 mb-4">
                    <div class="col-md-3 control-label">
                        <label> Alamat Pengiriman </label>
                     </div> 
                    <div class="col-md-9">
                       <textarea name="deliveryAddress" class="form-control" readonly cols="100" rows="8">{!! ($requisitionHeader) ? $requisitionHeader->delivery_to_address : '-' !!}  </textarea>
                    </div>

                </div>    
            </div>

        </div>


        <div class="col-12 mb-4">
    
            <table class="table table-striped table-bordered" id="itemTable" width="100%">
                <thead class="thead-dark">
                    <tr>
                        <th>No.</th>
                        <th>Tahap</th>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Unit Price</th>
                        <th>Created At/Created By</th>
                        <th>Total Harga Barang</th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $total = 0;
                    @endphp
                   @if(isset($requisitionDetail) && count($requisitionDetail) > 0)
                        @foreach ($requisitionDetail as $row)
                                <tr>
                                    <td>{!! $loop->index+1 !!}</td>
                                    <td>{!! $row->task_description !!}</td>
                                    <td>{!! $row->material_item_name !!}</td>
                                    <td>{!! $row->quantity !!}</td>
                                    <td>{!! number_format($row->unit_price, 0, ',', '.') !!}</td>
                                    <td>{!! \Carbon\Carbon::parse($row->creation_date)->format('d/m/Y') !!} <br/> {!! $row->created_by !!}</td>
                                    <td>{!! number_format(($row->quantity * $row->unit_price) , 0, ',','.') !!}</td>
                                </tr>
                                @php
                                $total += ($row->quantity * $row->unit_price);
                                @endphp
                        @endforeach 
                  @endif
                </tbody>
                <tfoot>
                    <td colspan="6" class="text-right">Total </td>
                    <td>{!! number_format($total, 0, ',', '.') !!} </td>
                </tfoot>

            </table>

        </div>   


    </div> 
</div>