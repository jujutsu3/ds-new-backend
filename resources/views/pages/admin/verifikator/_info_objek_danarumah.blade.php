<div class="col-12 mb-4">
    <br>
    <div class="form-check form-check-inline line">
        <label class="form-check-label text-black h5" for="informasi_penghasilan">Informasi
            Objek Pendanaan</label>
    </div>
    <hr>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="wizard-progress2-namapengguna">Tujuan Pendanaan<i class="text-danger">*</i></label>
        <input class="form-control tujuan_pendanaan" id="tujuan_pendanaan" name="tujuan_pendanaan" value="{{ $tujuanPendanaan }}" readonly>
    </div>
</div>


<div class="col-md-4">
    <div class="form-group">
        <label for="wizard-progress2-namapengguna">Tipe Pendanaan<i class="text-danger">*</i></label>
        <input class="form-control type_pendanaan" id="type_pendanaan" name="type_pendanaan" value="{{ $pendanaanTipe }}" readonly>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="wizard-progress2-namapengguna">Jenis Properti<i class="text-danger">*</i></label>
        <input class="form-control" id="jenis_properti" name="jenis_properti" value="{{ $jenis_properti }}" readonly>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="wizard-progress2-namapendanaan">Harga Objek Pendanaan <i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="harga_objek_pendanaan" name="harga_objek_pendanaan" value="{{ $harga_objek_pendanaan }}"
            readonly>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="wizard-progress2-namapendanaan">Uang Muka <i class="text-danger">*</i></label>
        <div class="input-group mb-2">
            <input type="text" class="form-control" id="uang_muka" name="uang_muka" readonly value="{{ $uang_muka }}">
            <div class="input-group-prepend">
                <span class="input-group-text" id="uang_muka_percentage">{{ $uang_muka_percentage }}</span>
            </div>
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="wizard-progress2-namapendanaan">Nilai Pengajuan <i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="nilai_pengajuan" name="nilai_pengajuan" value="{{ $nilai_pengajuan }}" readonly>
    </div>
</div>

<div class="col-md-4">
    <div class="form-group">
        <label for="wizard-progress2-namapendanaan">Jangka Waktu <i class="text-danger">*</i></label>
        <input type="text" class="form-control" id="jangka_waktu" name="jangka_waktu" value="{{ $pengajuan->durasi_proyek }}" readonly>
    </div>
</div>