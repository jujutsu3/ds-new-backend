<div class="container-fluid">
    <p class="h6 my-4 layout_informasi_akun line"><b>Informasi Akun &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_status_akun">
            <div class="col-sm-12">
                <label for="b_status_akun" class="font-weight-normal">Status Akun</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_status_akun" name="b_status_akun" class="form-control">
                <label class="text-danger font-weight-normal" id="b_label_status_akun" name="b_label_status_akun"
                    for="b_status_akun"></label>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_status_akun" name="a_status_akun" class="form-control">
                <label class="text-danger font-weight-normal" id="a_label_status_akun" name="a_label_status_akun"
                    for="a_status_akun"></label>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_username">
            <div class="col-sm-12">
                <label for="b_username" class="font-weight-normal">Akun</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_username_bdn_hukum" name="b_username" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_username_bdn_hukum" name="a_username" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_email">
            <div class="col-sm-12">
                <label for="b_email" class="font-weight-normal">Email</label>
            </div>
            <div class="col">
                <input type="email" id="b_email_bdn_hukum" name="b_email" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            <div class="col">
                <input type="email" id="a_email_bdn_hukum" name="a_email" class="form-control">
            </div>
        </div>
    </fieldset>

    {{--
    <hr class="my-4 layout_informasi_bdn_hukum"> --}}
    <p class="h6 my-4 layout_informasi_bdn_hukum line"><b>Informasi Badan Hukum &nbsp;</b></p>
    <fieldset>
        <!--START: Baris 1 -->
        <div class="row mt-2 d-none" name="f_nama_badan_hukum" id="f_nama_badan_hukum">
            <div class="col-sm-12">
                <label for="b_nama_badan_hukum">Nama Badan Hukum <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_nama_badan_hukum" id="b_nama_badan_hukum" class="form-control"
                    placeholder="Masukkan nama badan hukums" minlength="3" maxlength="50">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_nama_badan_hukum" id="a_nama_badan_hukum" class="form-control"
                    placeholder="Masukkan nama badan hukums" minlength="3" maxlength="50">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_nib_badan_hukum" id="f_nib_badan_hukum">
            <div class="col-sm-12">
                <label for="b_nib_badan_hukum">Nomor Surat Izin <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control" type="text" id="b_nib_badan_hukum" name="b_nib_badan_hukum" minlength="3"
                    maxlength="50" placeholder="SIUP/TDP/Nomor surat izin lainnya">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control" type="text" id="a_nib_badan_hukum" name="a_nib_badan_hukum" minlength="3"
                    maxlength="50" placeholder="SIUP/TDP/Nomor surat izin lainnya">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_npwp_bdn_hukum" id="f_npwp_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_npwp_bdn_hukum">NPWP Perusahaan<i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control" type="text" pattern=".{15,15}"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()" minlength="15"
                    maxlength="15" id="b_npwp_bdn_hukum" name="b_npwp_bdn_hukum" placeholder="Masukkan nomor NPWP">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control" type="text" pattern=".{15,15}"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()" minlength="15"
                    maxlength="15" id="a_npwp_bdn_hukum" name="a_npwp_bdn_hukum" placeholder="Masukkan nomor NPWP">
            </div>
        </div>
        <!-- END: Baris 1 -->

        <!-- START: Baris 2 -->
        <div class="row mt-2 d-none" name="f_no_akta_pendirian_bdn_hukum" id="f_no_akta_pendirian_bdn_hukum">
            <div class="col-sm-12">
                <label id="label_no_akta_pendirian_bdn_hukum" for="b_no_akta_pendirian_bdn_hukum">No Akta
                    Pendirian <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control" type="text" id="b_no_akta_pendirian_bdn_hukum"
                    name="b_no_akta_pendirian_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Pendirian"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control" type="text" id="a_no_akta_pendirian_bdn_hukum"
                    name="a_no_akta_pendirian_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Pendirian"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_tgl_berdiri_badan_hukum" id="f_tgl_berdiri_badan_hukum">
            <div class="col-sm-12">
                <label for="b_tgl_berdiri_badan_hukum">Tanggal Akta Pendirian <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control" type="date" id="b_tgl_berdiri_badan_hukum" name="b_tgl_berdiri_badan_hukum"
                    max="{{ date('Y-m-d')}}" placeholder="Masukkan Tanggal Berdiri"
                    onchange="$(this).valid(); $('#tgl_akta_perubahan_bdn_hukum').attr('min', $(this).val())">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control" type="date" id="a_tgl_berdiri_badan_hukum" name="a_tgl_berdiri_badan_hukum"
                    max="{{ date('Y-m-d')}}" placeholder="Masukkan Tanggal Berdiri"
                    onchange="$(this).valid(); $('#tgl_akta_perubahan_bdn_hukum').attr('min', $(this).val())">
            </div>
        </div>
        <div class="row mt-2 d-none" name="f_no_akta_perubahan_bdn_hukum" id="f_no_akta_perubahan_bdn_hukum">
            <div class="col-sm-12">
                <label id="label_no_akta_perubahan_bdn_hukum" for="b_no_akta_perubahan_bdn_hukum">No Akta
                    Perubahan
                    Terakhir</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control" type="text" id="b_no_akta_perubahan_bdn_hukum"
                    name="b_no_akta_perubahan_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Perubahan"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control" type="text" id="a_no_akta_perubahan_bdn_hukum"
                    name="a_no_akta_perubahan_bdn_hukum" minlength="1" placeholder="Masukkan No Akta Perubahan"
                    onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()">
            </div>
        </div>

        <div class="row mt-2 d-none" id="f_tgl_akta_perubahan_bdn_hukum" name="f_tgl_akta_perubahan_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_tgl_akta_perubahan_bdn_hukum">Tanggal Akta Perubahan
                    Terakhir</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control" type="date" id="b_tgl_akta_perubahan_bdn_hukum"
                    name="b_tgl_akta_perubahan_bdn_hukum" max="{{ date('Y-m-d')}}"
                    placeholder="Masukkan Tanggal Akta Perubahan Terakhir" onchange="$(this).valid()">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control" type="date" id="a_tgl_akta_perubahan_bdn_hukum"
                    name="a_tgl_akta_perubahan_bdn_hukum" max="{{ date('Y-m-d')}}"
                    placeholder="Masukkan Tanggal Akta Perubahan Terakhir" onchange="$(this).valid()">
            </div>
        </div>
        <div class="row mt-2 d-none" name="f_no_tlp_badan_hukum" id="f_no_tlp_badan_hukum">
            <div class="col-sm-12">
                <label id="label_no_tlp_badan_hukum">Nomor Telepon Perusahaan <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control no-zero" type="text" onkeyup="this.value = this.value.replace(/[^\d/]/g,'')"
                    maxlength="13" id="b_no_tlp_badan_hukum" name="b_no_tlp_badan_hukum"
                    placeholder="Masukkan nomor perusahaan">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control no-zero" type="text" onkeyup="this.value = this.value.replace(/[^\d/]/g,'')"
                    maxlength="13" id="a_no_tlp_badan_hukum" name="a_no_tlp_badan_hukum"
                    placeholder="Masukkan nomor perusahaan">
            </div>
        </div>
        <!--END: Baris 2 -->

        <!-- START: Baris 3 -->
        <div class="row mt-2 d-none" name="f_foto_npwp_badan_hukum" id="f_foto_npwp_badan_hukum">
            <div class="col-sm-12">
                <label id="label_foto_npwp_badan_hukum" for="b_foto_npwp_badan_hukum">Foto NPWP Perusahaan <i
                        class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="b_foto_npwp_badan_hukum" name="b_foto_npwp_badan_hukum">
                </div>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="a_foto_npwp_badan_hukum" name="a_foto_npwp_badan_hukum">
                </div>
            </div>
        </div>
        <!-- END: Baris 3 -->
    </fieldset>

    {{--
    <hr class="my-4 layout_informasi_bdn_hukum"> --}}
    <p class="h6 my-4 layout_informasi_alamat_sesuai_ktp_bdn_hukum line"><b>Alamat Sesuai Dengan AKTA &nbsp;</b></p>
    <fieldset>
        <!-- START: Baris 4 -->
        <div class="row d-none" name="f_alamat_bdn_hukum" id="f_alamat_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_alamat_bdn_hukum">Alamat Lengkap <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <textarea class="form-control form-control-lg" maxlength="100" id="b_alamat_bdn_hukum"
                    name="b_alamat_bdn_hukum" rows="6" placeholder="Masukkan alamat lengkap Anda.."></textarea>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <textarea class="form-control form-control-lg" maxlength="100" id="a_alamat_bdn_hukum"
                    name="a_alamat_bdn_hukum" rows="6" placeholder="Masukkan alamat lengkap Anda.."></textarea>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_provinsi_bdn_hukum" id="f_provinsi_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_provinsi_bdn_hukum">Provinsi <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <select class="form-control custom-select" id="b_provinsi_bdn_hukum" name="b_provinsi_bdn_hukum"
                    onchange="provinsiChange(this.value, this.id, 'kota_bdn_hukum')">
                    <option value="">-- Pilih Satu --</option>
                    @foreach ($master_provinsi as $data)
                    <option value={{$data->kode_provinsi}}>
                        {{$data->nama_provinsi}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select class="form-control custom-select" id="a_provinsi_bdn_hukum" name="a_provinsi_bdn_hukum"
                    onchange="provinsiChange(this.value, this.id, 'kota_bdn_hukum')">
                    <option value="">-- Pilih Satu --</option>
                    @foreach ($master_provinsi as $data)
                    <option value={{$data->kode_provinsi}}>
                        {{$data->nama_provinsi}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_kota_bdn_hukum" id="f_kota_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_kota_bdn_hukum">Kota/Kabupaten <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <select class="form-control custom-select" id="b_kota_bdn_hukum" name="b_kota_bdn_hukum">
                    <option value="">-- Pilih Satu --</option>
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select class="form-control custom-select" id="a_kota_bdn_hukum" name="a_kota_bdn_hukum">
                    <option value="">-- Pilih Satu --</option>
                </select>
            </div>
        </div>
        <!-- END: Baris 4 -->

        <!-- START: Baris 5 -->
        <div class="row mt-2 d-none" name="f_kecamatan_bdn_hukum" id="f_kecamatan_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_kecamatan_bdn_hukum">Kecamatan <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" maxlength="30" class="form-control" id="b_kecamatan_bdn_hukum"
                    name="b_kecamatan_bdn_hukum">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" maxlength="30" class="form-control" id="a_kecamatan_bdn_hukum"
                    name="a_kecamatan_bdn_hukum">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_kelurahan_bdn_hukum" id="f_kelurahan_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_kelurahan_bdn_hukum">Kelurahan <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" maxlength="30" class="form-control" id="b_kelurahan_bdn_hukum"
                    name="b_kelurahan_bdn_hukum">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" maxlength="30" class="form-control" id="a_kelurahan_bdn_hukum"
                    name="a_kelurahan_bdn_hukum">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_kode_pos_bdn_hukum" id="f_kode_pos_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_kode_pos_bdn_hukum">Kode Pos <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input class="form-control" type="text" maxlength="30" id="b_kode_pos_bdn_hukum"
                    name="b_kode_pos_bdn_hukum" placeholder="--">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input class="form-control" type="text" maxlength="30" id="a_kode_pos_bdn_hukum"
                    name="a_kode_pos_bdn_hukum" placeholder="--">
            </div>
        </div>
        <!-- END: Baris 5 -->

    </fieldset>

    {{--
    <hr class="my-4 layout_informasi_rekening_bdn_hukum"> --}}
    <p class="h6 my-4 layout_informasi_rekening_bdn_hukum line"><b>Informasi Rekening &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_va_number">
            <div class="col-sm-12">
                <label for="b_va_number" class="font-weight-normal">VA BNI</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_va_number" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_va_number" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_va_cimbs">
            <div class="col-sm-12">
                <label for="b_va_cimbs" class="font-weight-normal">VA CIMB Syariah</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_va_cimbs" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_va_cimbs" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_va_bsi">
            <div class="col-sm-12">
                <label for="b_va_bsi" class="font-weight-normal">VA BSI</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_va_bsi" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_va_bsi" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_rekening">
            <div class="col-sm-12">
                <label for="b_rekening" class="font-weight-normal">No Rekening</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_rekening" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_rekening" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_nama_pemilik_rek">
            <div class="col-sm-12">
                <label for="b_nama_pemilik_rek" class="font-weight-normal">Nama Pemilik
                    Rekening</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_nama_pemilik_rek" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_nama_pemilik_rek" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_bank">
            <div class="col-sm-12">
                <label for="b_bank" class="font-weight-normal">Bank</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select name="b_bank" class="form-control custom-select">
                    @foreach($master_bank as $b)
                    <option value="{{ $b->kode_bank }}">{{ $b->nama_bank }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select name="a_bank" class="form-control custom-select">
                    @foreach($master_bank as $b)
                    <option value="{{ $b->kode_bank }}">{{ $b->nama_bank }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </fieldset>

    {{--
    <hr class="my-4 layout_informasi_lain_lain_bdn_hukum"> --}}
    <p class="h6 my-4 layout_informasi_lain_lain_bdn_hukum line"><b>Informasi Lain Lain &nbsp;</b></p>
    <fieldset>
        <!-- START: Baris 7 -->
        <div class="row mt-2 d-none" id="f_bidang_pekerjaan_bdn_hukum" name="f_bidang_pekerjaan_bdn_hukum">
            <div class="col-sm-12">
                <label for="b_bidang_pekerjaan_bdn_hukum">Bidang Usaha <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <select class="form-control custom-select" id="b_bidang_pekerjaan_bdn_hukum"
                    name="b_bidang_pekerjaan_bdn_hukum">
                    @foreach ($master_bidang_pekerjaan as $b)
                    <option value="{{$b->kode_bidang_pekerjaan}}">
                        {{$b->bidang_pekerjaan}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select class="form-control custom-select" id="a_bidang_pekerjaan_bdn_hukum"
                    name="a_bidang_pekerjaan_bdn_hukum">
                    @foreach ($master_bidang_pekerjaan as $b)
                    <option value="{{$b->kode_bidang_pekerjaan}}">
                        {{$b->bidang_pekerjaan}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" id="f_omset_tahun_terakhir" name="f_omset_tahun_terakhir">
            <div class="col-sm-12">
                <label for="b_omset_tahun_terakhir">Omset Tahun Terakhir<i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" maxlength="30" name="b_omset_tahun_terakhir" id="b_omset_tahun_terakhir"
                    class="form-control no-zero" placeholder="Masukkan omset tahun terakhir">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" maxlength="30" name="a_omset_tahun_terakhir" id="a_omset_tahun_terakhir"
                    class="form-control no-zero" placeholder="Masukkan omset tahun terakhir">
            </div>
        </div>

        <div class="row mt-2 d-none" id="f_tot_aset_tahun_terakhr" name="f_tot_aset_tahun_terakhr">
            <div class="col-sm-12">
                <label for="b_tot_aset_tahun_terakhr">Total Aset Tahun Terakhir <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" maxlength="30" name="b_tot_aset_tahun_terakhr" id="b_tot_aset_tahun_terakhr"
                    class="form-control no-zero" placeholder="Masukkan total aset tahun terakhir">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" maxlength="30" name="a_tot_aset_tahun_terakhr" id="a_tot_aset_tahun_terakhr"
                    class="form-control no-zero" placeholder="Masukkan total aset tahun terakhir">
            </div>
        </div>
        <!-- END: Baris 7 -->
    </fieldset>

    {{-- Referal --}}
    <p class="h6 my-4 layout_informasi_referal line"><b>Referal &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_kode_referal" id="f_kode_referal">
            <div class="col-sm-12">
                <label for="b_kode_referal" class="font-weight-normal">Kode Referal</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_kode_referal" name="b_kode_referal" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_kode_referal" name="a_kode_referal" class="form-control">
            </div>
        </div>
    </fieldset>

    {{--
    <hr class="my-4 layout_informasi_lain_lain_bdn_hukum"> --}}
    <p class="h6 my-4 layout_informasi_pengurus line d-none"><b>Informasi Pengurus &nbsp;</b></p>
    <fieldset>
        <div id="layout-pengurus"></div>
    </fieldset>

</div>