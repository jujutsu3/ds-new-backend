@extends('layouts.admin.master')

@section('title', 'Panel Admin')
{{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> --}}

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Verifikasi Kelayakan Dana Rumah</h1>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">List Pengajuan Dana Rumah</strong>
                </div>
                <div class="card-body">

                    <!-- table select all admin -->
                    <table id="pengajuKPR_data" class="table table-striped table-bordered table-responsive-sm">
                        <thead>
                            <tr>
                                <th style="display: none;">Id</th>
                                <th>No</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Penerima Pendanaan</th>
                                <th>Jenis Pendanaan</th>
                                <th>Tujuan Pendanaan</th>
                                <th>Nilai Pengajuan Pendanaan</th>
                                <th class="text-center">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!-- end of table select all -->
                </div>
            </div>
        </div>
    </div>
</div><!-- .content -->



<style>
    .modal-xl {
        max-width: 95% !important;
    }
</style>


<div class="modal fade" id="view_detil" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Daftar Pendana</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-lg-2">Nama</div>
                    <div class="col-lg-2">Nama Rek</div>
                    <div class="col-lg-2">Bank Rek</div>
                    <div class="col-lg-1">Nomer Rek</div>
                    <div class="col-lg-2">Tanggal Pendanaan</div>
                    <div class="col-lg-2">Jumlah Pendanaan</div>
                    <div class="col-lg-1">Detil</div>

                </div>
                <hr>
                <div class="detil_payout p-1">
                </div>

            </div>
            <div class="modal-footer footer_payout">
            </div>
        </div>
    </div>
</div>





<!-- start modal show payout -->
<div class="modal fade" id="proses_detil" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Proses Pembayaran <button class="btn btn-info" id='cetak_payout'>Cetak Payout</button></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row ">
                    <div class="col-lg-3">Bulan Imbal Hasil</div>
                    <div class="col-lg-2">Tanggal Imbal Hasil</div>
                    <div class="col-lg-2">Status</div>
                    <!-- <div class="col-lg-2">Keterangan</div> -->
                    <div class="col-lg-1">Detil</div>
                    {{-- <div class="col-lg-1">Aksi</div>  --}}
                </div>
                <hr>
                <div class="show_payout p-1"></div>
                <div class="payout_footer p-1"></div>
            </div>
            {{-- <div class="modal-footer payout_footer"> --}}
        </div>
    </div>
</div>
</div>
<!-- end modal show payout -->

<!-- start modal payout terdekat -->
<div class="modal fade" id="modalpayout7harikedepan" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mediumModalLabel">Payout Minggu Ini <button id="action" class="btn btn-primary btn-sm">Kemarin</button> | <button id="actionBSI" class="btn btn-primary btn-sm">Kemarin</button></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-warning" disabled>Sorting Payout</button>
                    <button data-id="0" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j"); ?></button>
                    <button data-id="1" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j", strtotime('+1 days')); ?></button>
                    <button data-id="2" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j", strtotime('+2 days')); ?></button>
                    <button data-id="3" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j", strtotime('+3 days')); ?></button>
                    <button data-id="4" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j", strtotime('+4 days')); ?></button>
                    <button data-id="5" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j", strtotime('+5 days')); ?></button>
                    <button data-id="6" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j", strtotime('+6 days')); ?></button>
                    <button data-id="7" class="btn btn-secondary payout7harikedepan"><?php echo date("m-j", strtotime('+7 days')); ?></button>
                </div>
                <hr>
                <div class="row ">
                    <div class="col-lg-1">No</div>
                    <div class="col-lg-4">Nama Proyek</div>
                    <div class="col-lg-4">Tanggal Payout Terdekat</div>
                    <div class="col-lg-3">Hari Pembayaran</div>
                    {{-- <div class="col-lg-2">Detil</div> --}}
                </div>
                <hr>
                <div class="show_p7hk p-1"></div>
                <!-- <div class="payout_footer p-1"></div> -->
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<!-- end modal payout terdekat -->





<!-- 2. GOOGLE JQUERY JS v3.2.1  JS !-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>

<script src="/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/tinymce/js/tinymce/jquery.tinymce.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        var pengajuKPR_table = $('#pengajuKPR_data').DataTable({
            searching: true,
            processing: true,
            "order": [
                [1, "asc"]
            ],
            ajax: {
                url: '/admin/adr/ListPengajuan',
                dataSrc: 'data'
            },
            paging: true,
            info: true,
            lengthChange: false,
            pageLength: 10,
            columns: [{
                    data: 'pengajuan_id'
                },
                {
                    data: null,
                    render: function(data, type, row, meta) {
                        return meta.row + 1;
                    }
                },
                {
                    data: 'tgl_pengajuan'
                },
                {
                    data: 'nama_cust'
                },
                {
                    data: 'tipe_pendanaan'
                },
                {
                    data: 'tujuan_pembiayaan'
                },
                {
                    data: null,
                    render: function(data, type, row, meta) {
                        return '<p class=text-right>' + row.nilai_pengajuan + '</p>';
                    }
                },
                {
                    data: null,
                    render: function(data, type, row, meta) {
                        if (row.stts == 0 || row.stts == 3) {
                            return '<button id="tombol" class="btn btn-secondary btn-block">Menunggu Verifikasi</button>';
                        } else if (row.stts == 2 || row.stts == 5) {
                            return '<button id="tombol" class="btn btn-danger btn-block">ditolak</button>';
                        } else if (row.stts == 1) {
                            return '<button id="tombol" class="btn btn-success btn-block">diterima</button>';
                        }
                    }
                },

            ],
            columnDefs: [{
                targets: [0],
                visible: false
            }]
        })
        var id;

        $('#pengajuKPR_data tbody').on('click', '#tombol', function() {
            var data = pengajuKPR_table.row($(this).parents('tr')).data();
            idPengajuan = data.pengajuan_id;
            brw_id = data.brw_id;
            window.location.href = "../adr/detailPengajuan/" + idPengajuan + "/" + brw_id;
        })

        // view
        $('#pengajuKPR_data tbody').on('click', '#generate', function() {
            var data = pengajuKPR_table.row($(this).parents('tr')).data();
            $(this).prop('disabled', true);
            did = data.id;
            $.ajax({
                url: "manage_payout/" + did,
                method: "post",
                success: function(data) {
                    if (data.Success) {
                        swal('Berhasil', 'Kembali Dana Selesai', 'success');
                        window.location.reload();

                    }
                }
            })

        })

    });
</script>


@endsection