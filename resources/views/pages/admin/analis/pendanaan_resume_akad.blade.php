{{-- START: Data Penerima Dana --}}
<div class="row mt-3">
    <div id="layout-data-penerima-dana" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Data Penerima Dana &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">

                {{-- START: Baris 1 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_nomor_naup" class="col-sm-5 col-form-label font-weight-normal">Nomor
                                NAUP</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nomor_naup" name="text_nomor_naup"
                                    value="{{ $data_brw ? $data_brw->no_naup : '' }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 1 --}}

                {{-- START: Baris 2 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_nama_penerima_pendanaan"
                                class="col-sm-5 col-form-label font-weight-normal">Nama</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nama_penerima_pendanaan"
                                    name="text_nama_penerima_pendanaan"
                                    value="{{ $data_brw ? $data_brw->nama_penerima_pendana : '' }}" readonly>
                            </div>
                        </div>
                    </div>

                    @if ($data_brw && $data_brw->skema_pembiayaan == 2)
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_nama_pasangan" class="col-sm-5 col-form-label font-weight-normal">Nama
                                Pasangan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nama_pasangan"
                                    name="text_nama_pasangan"
                                    value="{{ $data_brw_pasangan ? $data_brw_pasangan->nama : '' }}" readonly>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                {{-- END: Baris 2 --}}

                {{-- START: Baris 3 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_usia_saat_pengajuan"
                                class="col-sm-5 col-form-label font-weight-normal">Usia Saat Pengajuan (Tahun)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_usia_saat_pengajuan"
                                    name="text_usia_saat_pengajuan"
                                    value="{{ $data_brw ? \Carbon\Carbon::parse($data_brw->tgl_lahir)->age : '' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>

                    @if ($data_brw && $data_brw->skema_pembiayaan == 2)
                    <div class="col-md-6">
                        <div class="form-group row mt-md-1">
                            <label for="text_usia_pasangan" class="col-sm-5 col-form-label font-weight-normal">Usia
                                Pasangan <br> (Tahun)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_usia_pasangan"
                                    name="text_usia_pasangan"
                                    value="{{ $data_brw_pasangan ? \Carbon\Carbon::parse($data_brw_pasangan->tgl_lahir)->age : '' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                {{-- END: Baris 3 --}}

                {{-- START: Baris 4 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_skema_pengajuan" class="col-sm-5 col-form-label font-weight-normal">Skema
                                Pengajuan</label>
                            <div class="col-sm-7 my-auto">
                                <select name="text_skema_pengajuan" id="text_skema_pengajuan"
                                    class="form-control custom-select" disabled>
                                    <option value="">-</option>
                                    <option value="1" {{ $data_brw ? $data_brw->skema_pembiayaan == 1 ? 'selected' : ''
                                        : ''}}>Single
                                        Income(Penghasilan Sendiri)</option>
                                    <option value="2" {{ $data_brw ? $data_brw->skema_pembiayaan == 2 ? 'selected' : ''
                                        : ''}}>Join
                                        Income(Penghasilan Bersama)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 4 --}}

                {{-- START: Baris 5 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_cash_ratio" class="col-sm-5 col-form-label font-weight-normal">Cash
                                Ratio (%)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_cash_ratio" name="text_cash_ratio"
                                    value="{{ $brw_pendapatan ? $brw_pendapatan->cr_maks : '' }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 5 --}}

                {{-- START: Baris 6 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_status_karyawan" class="col-sm-5 col-form-label font-weight-normal">Status
                                Karyawan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_status_karyawan"
                                    name="text_status_karyawan"
                                    value="{{ $data_brw ? ($data_brw->status_karyawan == 1 ? 'Kontrak' : ($data_brw->status_karyawan == 2 ? 'Tetap' : '-')) : '-' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>

                    @if ($data_brw && $data_brw->skema_pembiayaan == 2)
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_status_karyawan_pasangan"
                                class="col-sm-5 col-form-label font-weight-normal">Status
                                Karyawan Pasangan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_status_karyawan_pasangan"
                                    name="text_status_karyawan_pasangan"
                                    value="{{ $data_brw_pasangan ? ($data_brw_pasangan->status_pekerjaan == 1 ? 'Kontrak' : ($data_brw_pasangan->status_pekerjaan == 2 ? 'Tetap' : '-')) : '-' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                {{-- END: Baris 6 --}}

                {{-- START: Baris 7 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_tempat_bekerja" class="col-sm-5 col-form-label font-weight-normal">Tempat
                                Bekerja</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_tempat_bekerja"
                                    name="text_tempat_bekerja" value="{{ $data_brw ? $data_brw->tempat_bekerja : '' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>

                    @if ($data_brw && $data_brw->skema_pembiayaan == 2)
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_tempat_bekerja_pasangan"
                                class="col-sm-5 col-form-label font-weight-normal">Tempat
                                Bekerja Pasangan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_tempat_bekerja_pasangan"
                                    name="text_tempat_bekerja_pasangan"
                                    value="{{ $data_brw_pasangan ? $data_brw_pasangan->nama_perusahaan : '' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                {{-- END: Baris 7 --}}

                {{-- START: Baris 8 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_jabatan" class="col-sm-5 col-form-label font-weight-normal">Jabatan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_jabatan" name="text_jabatan"
                                    value="{{ $data_brw && $data_brw->jabatan ? $data_brw->jabatan : '-' }}" readonly>
                            </div>
                        </div>
                    </div>

                    @if ($data_brw && $data_brw->skema_pembiayaan == 2)
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_jabatan_pasangan"
                                class="col-sm-5 col-form-label font-weight-normal">Jabatan
                                Pasangan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_jabatan_pasangan"
                                    name="text_jabatan_pasangan"
                                    value="{{ $data_brw_pasangan && $data_brw_pasangan->jabatan ? $data_brw_pasangan->jabatan : '-' }}" readonly>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                {{-- END: Baris 8 --}}

                {{-- START: Baris 9 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_lama_bekerja" class="col-sm-5 col-form-label font-weight-normal">{{ $data_brw && $data_brw->kategori_penerima_pendanaan == 1 ? 'Lama Bekerja(sejak tahun)' : 'Lama Usaha/Praktek(sejak tahun)'}}</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_lama_bekerja" name="text_lama_bekerja"
                                    value="{{ $data_brw && $data_brw->kategori_penerima_pendanaan == 1 ? ($data_brw->lama_bekerja) : $data_brw->lama_usaha }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 9 --}}

                {{-- START: Baris 10 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_lama_perusahaan" class="col-sm-5 col-form-label font-weight-normal">{{ $data_brw && $data_brw->kategori_penerima_pendanaan == 1 ? 'Lama Perusahaan(sejak tahun)' : 'Lama Tempat Usaha/Praktek(sejak tahun)'}}</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_lama_perusahaan"
                                    name="text_lama_perusahaan"
                                    value="{{ $data_brw && $data_brw->kategori_penerima_pendanaan == 1 ? $data_brw->lama_perusahaan : $data_brw->lama_tempat_usaha }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 10 --}}
            </div>
        </div>
    </div>
</div>
{{-- END: Data Penerima Dana --}}

{{-- START: Fasilitas Pembiayaan --}}
<div class="row mt-3">
    <div id="layout-fasilitas-pembiayaan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Fasilitas Pembiayaan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="row mt-4">
                    <div class="col-md-6">
                        {{-- START: Baris 1 --}}
                        <div class="form-group row">
                            <label for="text_produk_pembiayaan"
                                class="col-sm-5 col-form-label font-weight-normal">Produk Pembiayaan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_produk_pembiayaan"
                                    name="text_produk_pembiayaan"
                                    value="{{ $data_brw ? $data_brw->produk_pembiayaan : '' }}" readonly>
                            </div>
                        </div>
                        {{-- END: Baris 1 --}}

                    </div>
                </div>

                @if ($data_brw->akad_pendanaan == 'Murabahah')
                <div class="layout-fasilitas-murabahah">
                    {{-- START: Baris 2 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_harga_transaksi"
                                    class="col-sm-5 col-form-label font-weight-normal">Harga
                                    Transaksi</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_harga_transaksi"
                                        name="text_harga_transaksi"
                                        value="Rp{{ $data_brw ? str_replace(',', '.',number_format($data_brw->plafon_pengajuan)) : '' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 2 --}}

                    {{-- START: Baris 3 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_margin"
                                    class="col-sm-5 col-form-label font-weight-normal">Margin</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_margin" name="text_margin"
                                        value="Rp{{ $data_lembar_analisa && $data_brw ? str_replace(',', '.',number_format(($data_brw->jangka_waktu_pendanaan * $data_lembar_analisa->angsuran) - $data_lembar_analisa->plafond_rekomendasi)) : ''}}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 3 --}}

                    {{-- START: Baris 4 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_total_murabahah"
                                    class="col-sm-5 col-form-label font-weight-normal">Total
                                    Murabahah</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_total_murabahah"
                                        name="text_total_murabahah"
                                        value="Rp{{ $data_lembar_analisa && $data_brw ? str_replace(',', '.',number_format(($data_lembar_analisa->plafond_rekomendasi) + (($data_brw->jangka_waktu_pendanaan * $data_lembar_analisa->angsuran) - $data_lembar_analisa->plafond_rekomendasi))) : ''}}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 4 --}}

                    {{-- START: Baris 4 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_uang_muka" class="col-sm-5 col-form-label font-weight-normal">Uang
                                    Muka</label>
                                <div class="col-sm-7 my-auto">
                                    @php
                                        $uang_muka1 = $data_lembar_analisa && $data_brw ? str_replace(',', '.',number_format($data_brw->plafon_pengajuan - $data_lembar_analisa->plafond_rekomendasi)) : 0;
                                        $is_uang_muka1_negative = $uang_muka1 < 0 ? '-' : ''
                                    @endphp
                                    <input type="text" class="form-control" id="text_uang_muka" name="text_uang_muka"
                                    value="{{ $is_uang_muka1_negative }}Rp{{ str_replace('-', '', $uang_muka1) }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 4 --}}

                    {{-- START: Baris 6 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_plafon_rekomendasi"
                                    class="col-sm-5 col-form-label font-weight-normal">Plafon
                                    Rekomendasi</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_plafon_rekomendasi"
                                        name="text_plafon_rekomendasi"
                                        value="Rp{{ $data_lembar_analisa ? str_replace(',', '.',number_format($data_lembar_analisa->plafond_rekomendasi)) : '' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 6 --}}

                    {{-- START: Baris 7 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_presentase_margin"
                                    class="col-sm-5 col-form-label font-weight-normal">Presentase Margin</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_presentase_margin"
                                        name="text_presentase_margin" value="{{ $data_brw ? $data_brw->margin : '' }} %"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 7 --}}
                </div>

                @elseif ($data_brw->akad_pendanaan == 'IMBT')

                <div class="layout-fasilitas-imbt">
                    {{-- START: Baris 2 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_harga_perolehan" class="col-sm-5 col-form-label font-weight-normal">Harga
                                    Perolehan</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_harga_perolehan" name="text_harga_perolehan"
                                        value="Rp{{ $data_brw ? str_replace(',', '.',number_format($data_brw->plafon_pengajuan)) : '' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 2 --}}

                    {{-- START: Baris 3 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_harga_sewa" class="col-sm-5 col-form-label font-weight-normal">Harga
                                    Sewa</label>
                                <div class="col-sm-7 my-auto">
                                    @php
                                        $harga_sewa = $data_lembar_analisa && $data_brw ? str_replace(',', '.',number_format((($data_brw->jangka_waktu_pendanaan * $data_lembar_analisa->angsuran) - $data_lembar_analisa->plafond_rekomendasi) + ($data_brw->plafon_pengajuan))) : 0;
                                        $is_harga_sewa_negative = $harga_sewa < 0 ? '-' : ''
                                    @endphp
                                    <input type="text" class="form-control" id="text_harga_sewa" name="text_harga_sewa"
                                    value="{{ $is_harga_sewa_negative }}Rp{{ str_replace('-', '', $harga_sewa) }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 3 --}}

                    {{-- START: Baris 4 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_uang_muka_sewa" class="col-sm-5 col-form-label font-weight-normal">Uang Muka
                                    Sewass</label>
                                <div class="col-sm-7 my-auto">
                                    @php
                                        $uang_muka_sewa = $data_lembar_analisa && $data_brw ? str_replace(',', '.',number_format($data_brw->plafon_pengajuan - $data_lembar_analisa->plafond_rekomendasi)) : 0;
                                        $is_uang_muka_sewa_negative = $uang_muka_sewa < 0 ? '-' : ''
                                    @endphp
                                    <input type="text" class="form-control" id="text_uang_muka_sewa" name="text_uang_muka_sewa"
                                    value="{{ $is_uang_muka_sewa_negative }}Rp{{ str_replace('-', '', $uang_muka_sewa) }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 4 --}}

                    {{-- START: Baris 5 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_sisa_harga_sewa" class="col-sm-5 col-form-label font-weight-normal">Sisa Harga
                                    Sewa</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_sisa_harga_sewa" name="text_sisa_harga_sewa"
                                    value="Rp{{ $data_lembar_analisa && $data_brw ? str_replace(',', '.',number_format(((($data_brw->jangka_waktu_pendanaan * $data_lembar_analisa->angsuran) - $data_lembar_analisa->plafond_rekomendasi) + ($data_brw->plafon_pengajuan)) - ($data_brw->plafon_pengajuan - $data_lembar_analisa->plafond_rekomendasi))) : ''}}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 5 --}}

                    {{-- START: Baris 6 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_harga_sewa_per_bulan" class="col-sm-5 col-form-label font-weight-normal">Harga
                                    Sewa/Bulan</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_harga_sewa_per_bulan" name="text_harga_sewa_per_bulan"
                                    value="Rp{{ $data_lembar_analisa ? str_replace(',', '.',number_format($data_lembar_analisa->angsuran)) : ''}}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 6 --}}

                    {{-- START: Baris 7 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_harga_sisa" class="col-sm-5 col-form-label font-weight-normal">Harga
                                    Sisa</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_harga_sisa" name="text_harga_sisa"
                                    value="Rp{{ $data_lembar_analisa && $data_brw ? str_replace(',', '.',number_format(((($data_brw->jangka_waktu_pendanaan * $data_lembar_analisa->angsuran) - $data_lembar_analisa->plafond_rekomendasi) + ($data_brw->plafon_pengajuan)) - ($data_brw->plafon_pengajuan - $data_lembar_analisa->plafond_rekomendasi))) : ''}}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 7 --}}

                    {{-- START: Baris 8 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_jangka_waktu_sewa" class="col-sm-5 col-form-label font-weight-normal">Jangka Waktu Sewa</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_jangka_waktu_sewa" name="text_jangka_waktu_sewa"
                                    value="{{ $data_brw ? $data_brw->jangka_waktu_pendanaan .' Bulan' : ''}}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 8 --}}

                    {{-- START: Baris 9 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="text_waktu_hibah" class="col-sm-5 col-form-label font-weight-normal">Waktu Hibah</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="text_waktu_hibah" name="text_waktu_hibah"
                                    value="{{ $data_brw ? 'Bulan Ke '.$data_brw->jangka_waktu_pendanaan : ''}}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 9 --}}
                </div>

                @else

                <div class="layout-fasilitas-mmq">
                    {{-- START: Baris 2 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="mmq_objek_musyarakah"
                                    class="col-sm-5 col-form-label font-weight-normal">Objek Musyarakah</label>
                                <div class="col-sm-7 my-auto">
                                    <textarea name="mmq_objek_musyarakah" id="mmq_objek_musyarakah" class="form-control"
                                        cols="10" rows="3"
                                        readonly>{{ $data_brw && $data_objek_pendanaan ? ($data_objek_pendanaan->jenis_objek_pendanaan == '1' ? 'Tanah Kosong' : 'Tanah dan Bangunan'). ' di ' .$data_brw->lokasi_proyek .', blok nomor. '. $data_objek_pendanaan->blok_nomor .', RT. '. $data_objek_pendanaan->RT .', RW. '. $data_objek_pendanaan->RW .', Kel. '. $data_brw->kelurahan .', Kec. '. $data_brw->kecamatan .', Kota. '. $data_brw->kota .', Prov. '. $data_brw->provinsi .', Kode Pos '. $data_brw->kode_pos : '' }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 2 --}}

                    {{-- START: Baris 3 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="mmq_harga_objek" class="col-sm-5 col-form-label font-weight-normal">Harga
                                    Objek</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_harga_objek" name="mmq_harga_objek"
                                        value="Rp{{ $data_brw ? str_replace(',', '.',number_format($data_brw->harga_objek_pendanaan)) : '' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 3 --}}

                    {{-- START: Baris 4 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="mmq_parsi_syirkah_dsi"
                                    class="col-sm-5 col-form-label font-weight-normal">Porsi Syirkah DSI</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_syirkah_dsi"
                                        name="mmq_parsi_syirkah_dsi"
                                        value="Rp{{ $data_brw && $data_lembar_analisa ? str_replace(',', '.',number_format(($data_brw->jangka_waktu_pendanaan * $data_lembar_analisa->angsuran) - $data_lembar_analisa->plafond_rekomendasi)) : '' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_syirkah_dsi_percent"
                                        name="mmq_parsi_syirkah_dsi_percent" value="{{ $data_resume_akad ? $data_resume_akad->porsi_syirkah_dsi_persentase . '%' : '' }}" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" placeholder="(%)">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 4 --}}

                    {{-- START: Baris 5 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="mmq_parsi_syirkah_penerima_dana"
                                    class="col-sm-5 col-form-label font-weight-normal">Porsi Syirkah Penerima
                                    Dana</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_syirkah_penerima_dana"
                                        name="mmq_parsi_syirkah_penerima_dana"
                                        value="Rp{{ $data_resume_akad ? str_replace(',', '.',number_format($data_resume_akad->porsi_syirkah_penerima_dana)) : '' }}"
                                        onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_syirkah_penerima_dana_percent"
                                        name="mmq_parsi_syirkah_penerima_dana_percent" value="{{ $data_resume_akad ? $data_resume_akad->porsi_syirkah_penerima_dana_persentase . '%' : '' }}" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" placeholder="(%)">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 5 --}}

                    {{-- START: Baris 6 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="mmq_parsi_nisbah_dsi"
                                    class="col-sm-5 col-form-label font-weight-normal">Porsi Nisbah DSI</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_nisbah_dsi"
                                        name="mmq_parsi_nisbah_dsi"
                                        value="Rp{{ $data_resume_akad ? str_replace(',', '.',number_format($data_resume_akad->porsi_nisbah_dsi)) : '' }}"
                                        onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_nisbah_dsi_percent"
                                        name="mmq_parsi_nisbah_dsi_percent" value="{{ $data_resume_akad ? $data_resume_akad->porsi_nisbah_dsi_persentase . '%' : '' }}" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" placeholder="(%)">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 6 --}}

                    {{-- START: Baris 7 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="mmq_parsi_nisbah_penerima_dana"
                                    class="col-sm-5 col-form-label font-weight-normal">Porsi Nisbah Penerima
                                    Dana</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_nisbah_penerima_dana"
                                        name="mmq_parsi_nisbah_penerima_dana"
                                        value="Rp{{ $data_resume_akad ? str_replace(',', '.',number_format($data_resume_akad->porsi_nisbah_penerima_dana)) : '' }}"
                                        onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_parsi_nisbah_penerima_dana_percent"
                                        name="mmq_parsi_nisbah_penerima_dana_percent" value="{{ $data_resume_akad ? $data_resume_akad->porsi_nisbah_penerima_dana_persentase . '%' : '' }}" onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');" placeholder="(%)">
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 7 --}}

                    {{-- START: Baris 8 --}}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label for="mmq_plafon_rekomendasi"
                                    class="col-sm-5 col-form-label font-weight-normal">Plafon
                                    Rekomendasi</label>
                                <div class="col-sm-7 my-auto">
                                    <input type="text" class="form-control" id="mmq_plafon_rekomendasi"
                                        name="mmq_plafon_rekomendasi"
                                        value="Rp{{ $data_lembar_analisa ? str_replace(',', '.',number_format($data_lembar_analisa->plafond_rekomendasi)) : '' }}"
                                        readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Baris 8 --}}
                </div>

                @endif

                @if ($data_brw->akad_pendanaan != 'IMBT')
                {{-- START: Baris 8 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_jangka_waktu" class="col-sm-5 col-form-label font-weight-normal">Jangka
                                Waktu</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_jangka_waktu" name="text_jangka_waktu"
                                    value="{{ $data_brw ? $data_brw->jangka_waktu_pendanaan : '' }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 8 --}}


                {{-- START: Baris 9 --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_angsuran"
                                class="col-sm-5 col-form-label font-weight-normal">Angsuran</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_angsuran" name="text_angsuran"
                                    value="Rp{{ $data_lembar_analisa ? str_replace(',', '.',number_format($data_lembar_analisa->angsuran)) : '' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 9 --}}
                @endif

            </div>
        </div>
    </div>
</div>
{{-- END: Fasilitas Pembiayaan --}}

{{-- START: Jaminan --}}
<div class="row mt-3">
    <div id="layout-jaminan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Jaminan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="row mt-4">
                    <div class="col-md-6">
                        {{-- START: Baris 1 --}}
                        <div class="form-group row">
                            <label for="text_lokasi" class="col-sm-5 col-form-label font-weight-normal">Lokasi</label>
                            <div class="col-sm-7 my-auto">
                                <textarea name="text_lokasi" id="text_lokasi" class="form-control" cols="30" rows="3"
                                    readonly>{{ $data_brw && $data_objek_pendanaan ? $data_brw->lokasi_proyek .', blok nomor. '. $data_objek_pendanaan->blok_nomor .', RT. '. $data_objek_pendanaan->RT .', RW. '. $data_objek_pendanaan->RW .', Kel. '. $data_brw->kelurahan .', Kec. '. $data_brw->kecamatan .', Kota. '. $data_brw->kota .', Prov. '. $data_brw->provinsi .', Kode Pos '. $data_brw->kode_pos : '' }}</textarea>
                            </div>
                        </div>
                        {{-- END: Baris 1 --}}

                        {{-- START: Baris 2 --}}
                        <div class="form-group row">
                            <label for="text_bukti_kepemilikan" class="col-sm-5 col-form-label font-weight-normal">Bukti
                                Kepemilikan</label>
                            <div class="col-sm-7 my-auto">
                                <select name="text_bukti_kepemilikan" id="text_bukti_kepemilikan"
                                    class="form-control custom-select" disabled>
                                    <option value="">-</option>
                                    <option value="1" {{ $data_objek_pendanaan ? $data_objek_pendanaan->jenis_agunan ==
                                        1 ? 'selected' : '' : ''}}>
                                        SHM
                                    </option>
                                    <option value="2" {{ $data_objek_pendanaan ? $data_objek_pendanaan->jenis_agunan ==
                                        2 ? 'selected' : '' : ''}}>
                                        SHGB
                                    </option>
                                </select>
                            </div>
                        </div>
                        {{-- END: Baris 2 --}}

                        {{-- START: Baris 3 --}}
                        <div class="form-group row">
                            <label for="text_nomor_sertifikat" class="col-sm-5 col-form-label font-weight-normal">Nomor
                                Sertifikat</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nomor_sertifikat"
                                    name="text_nomor_sertifikat"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->nomor_agunan : '' }}"
                                    readonly>
                            </div>
                        </div>
                        {{-- END: Baris 3 --}}

                        {{-- START: Baris 4 --}}
                        @if ($data_objek_pendanaan && $data_objek_pendanaan->jenis_agunan == 2)
                        <div class="form-group row">
                            <label for="text_tanggal_shgb" class="col-sm-5 col-form-label font-weight-normal">Tanggal
                                Jatuh Tempo SHGB</label>
                            <div class="col-sm-7 my-auto">
                                <input type="date" class="form-control" id="text_tanggal_shgb" name="text_tanggal_shgb"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->tanggal_jatuh_tempo : '' }}"
                                    readonly>
                            </div>
                        </div>
                        @endif
                        {{-- END: Baris 4 --}}

                        {{-- START: Baris 4 --}}
                        <div class="form-group row">
                            <label for="text_luas_tanah" class="col-sm-5 col-form-label font-weight-normal">Luas
                                Tanah</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_luas_tanah" name="text_luas_tanah"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->luas_tanah : '' }}"
                                    readonly>
                            </div>
                        </div>
                        {{-- END: Baris 4 --}}

                        {{-- START: Baris 6 --}}
                        <div class="form-group row">
                            <label for="text_luas_bangunan" class="col-sm-5 col-form-label font-weight-normal">Luas
                                Banguan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_luas_bangunan"
                                    name="text_luas_bangunan"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->luas_bangunan : '' }}"
                                    readonly>
                            </div>
                        </div>
                        {{-- END: Baris 6 --}}

                        {{-- START: Baris 7 --}}
                        <div class="form-group row">
                            <label for="text_nilai_pasar_wajar" class="col-sm-5 col-form-label font-weight-normal">Nilai
                                Pasar Wajar</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nilai_pasar_wajar"
                                    name="text_nilai_pasar_wajar"
                                    value="{{ $data_objek_pendanaan ? 'Rp'.str_replace(',', '.',number_format($data_objek_pendanaan->nilai_pasar_wajar)) : '' }}"
                                    readonly>
                            </div>
                        </div>
                        {{-- END: Baris 7 --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Jaminan --}}

{{-- START: kesimpulan --}}
<div class="row mt-3">
    <div id="layout-kesimpulan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp kesimpulan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="form-group">

                            <label for="text_kesimpulan">Berdasarkan Analisa Pembiayaan & Assesment, Maka Berikut Review
                                Yang Dapat Disampaikan Ke Komite sbb:</label>
                            <textarea name="text_kesimpulan" id="text_kesimpulan" class="form-control"
                                rows="10">{{ $data_resume_akad ? $data_resume_akad->kesimpulan : '' }}</textarea>
                        </div>

                        <div class="form-group row">
                            <label for="deviasi" class="col-sm-3 col-form-label">Apakah ada Deviasi ?</label>
                            <div class="col-sm-3 my-auto">
                                <select name="deviasi" id="deviasi" class="form-control custom-select">
                                    <option value="">-- Pilih Satu --</option>
                                    <option value="1" {{ count($data_deviasi)> 0 ? 'selected' : '' }}>Ya</option>
                                    <option value="2">Tidak</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="layout-table-deviasi" class="row mt-4 d-none align-items-end">
                    <div class="col-md-12">
                        <div class="table-responsive px-1">
                            <table id="table_deviasi" class="table table-bordered table-striped">
                                <thead class="text-center">
                                    <tr>
                                        <th class="align-middle">No</th>
                                        <th class="align-middle d-none">ID</th>
                                        <th class="align-middle">Jenis Deviasi</th>
                                        <th class="align-middle">Resiko</th>
                                        <th class="align-middle">Mitigasi</th>
                                    </tr>
                                </thead>
                                <tbody class="align-middle">
                                    <tr></tr>
                                    @foreach ($data_deviasi as $item)
                                    <tr id="tr-{{ $loop->iteration }}">
                                        <td class="text-center align-middle" name="in_row[]">
                                            {{ $loop->iteration }}</td>
                                        <td class="text-center align-middle">
                                            <input type="hidden" id="in_id_{{ $loop->iteration }}" name="in_id[]"
                                                value="{{ $item->id }}">
                                            <input type="text" class="form-control"
                                                id="in_jenis_deviasi_{{ $loop->iteration }}" name="in_jenis_deviasi[]"
                                                value="{{ $item->jenis_deviasi }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            {{ $item->resiko }}
                                        </td>
                                        <td class="text-center align-middle">
                                            {{ $item->mitigasi }}
                                        </td>
                                        <td class="text-center align-middle border-0 bg-white">
                                            <a type="button" id="btn-remove-deviasi" class="btn btn-danger btn-sm"
                                                onclick="removeDeviasi({{ $loop->iteration }}, {{ $item->id }})"> <i
                                                    class="fa fa-close text-white"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    <div id="deleted-deviasi">

                                    </div>
                                </tbody>
                                <tfoot class="align-middle">
                                    <tr>
                                        <td id="te_row" class="text-center align-middle">
                                            {{ !empty($data_deviasi) ? count($data_deviasi) + 1 : 1 }}
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="hidden" id="in_id_${row_number}" name="in_id[]" value="0">
                                            <input type="text" class="form-control" id="te_jenis_deviasi" name="in_jenis_deviasi[]">
                                        </td>
                                        <td class="text-center align-middle"></td>
                                        <td class="text-center align-middle"></td>
                                        <td class="text-center align-middle border-0 bg-white">
                                            <a type="button" id="btn-add-deviasi"
                                                class="btn btn-outline-secondary btn-sm" onclick="addDeviasi()"> <i
                                                    class="fa fa-plus"></i></a>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>

                        </div>
                    </div>

                    {{-- <div class="col-md-1 mr-md-0 pr-md-0">

                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: kesimpulan --}}

@push('scripts')

<script>
    $(document).ready(function() {
        let is_deviasi = '{!! count($data_deviasi) > 0 ? '1' : '0' !!}'
        if (is_deviasi == '1') {
            $('#layout-table-deviasi').removeClass('d-none')
        }

        $('#deviasi').change(function () {
            let thisValue = $(this).val()
            if (thisValue == 1) {
                $('#layout-table-deviasi').removeClass('d-none')
            } else {
                $('#layout-table-deviasi').addClass('d-none')
            }
        })
    })

    // START: Other Function
    let row_number = '{!! count($data_deviasi) !!}'
    const addDeviasi = () => {
        row_number++
        let te_row = $('#te_row').text()
        let te_jenis_deviasi = $('#te_jenis_deviasi').val()

        $('#table_deviasi').find('tbody').append(`
        <tr id="tr-${row_number}">
            <td id="in_row_${row_number}" class="text-center align-middle" name="in_row[]">${te_row}</td>
            <td class="text-center align-middle">
                <input type="hidden" id="in_id_${row_number}" name="in_id[]" value="0">
                <input type="text" class="form-control" id="in_jenis_deviasi_${row_number}" name="in_jenis_deviasi[]"
                    value="${te_jenis_deviasi}">
            </td>
            <td class="text-center align-middle"></td>
            <td class="text-center align-middle"></td>
            <td class="text-center align-middle border-0 bg-white">
                <a type="button" id="btn-remove-deviasi" class="btn btn-danger btn-sm" onclick="removeDeviasi(${row_number})"> <i
                        class="fa fa-close text-white"></i></a>
            </td>
        </tr>
        `)
        $('#te_row').text(parseInt(te_row)+1)
        $('#te_jenis_deviasi').val('').focus()
    }

    const removeDeviasi = (row, id) => {
        console.log(row, id)
        if (id) {
            $('#deleted-deviasi').append(`
                <input type="hidden" name="in_deleted_deviasi[]" value="${id}">
            `)
        }
    
        $(`#tr-${row}`).remove();
        incrementRowDeviasi()
    }

    const incrementRowDeviasi = () => {
        row_number -= 1

        let total_row = $('td[name="in_row[]"]').length
        let row = $('td[name="in_row[]"]')
        for (let i = 1; i <= total_row; i++) { 
            $(row[i-1]).text(i)
        }

        $('#te_row').text(row_number + 1)
    }
    // END: Other Function
</script>

@endpush