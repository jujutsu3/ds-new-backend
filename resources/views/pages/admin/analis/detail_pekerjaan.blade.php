@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('style')
<style>
    .btn-danaSyariah {
        background-color: #16793E;
    }

    input[type=checkbox] {
        top: 1rem;
        width: 1rem;
        height: 1rem;
    }

    body {
        background-color: white;
    }

    .card {
        border-color: #000000;
        margin-bottom: 60px
    }

    .box-title {
        margin-left: 8px;
        margin-top: -0.9rem;
        background-color: #FFFFFF;
        width: -moz-fit-content;
        width: fit-content;
    }
</style>
@endsection

@section('content')

{{-- START: Content Title --}}
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Analisa Pendanaan -
                    @if (request()->get('keterangan') == 1)
                    RAC
                    @elseif (request()->get('keterangan') == 2)
                    Lembar Perhitungan
                    @else
                    Resume Akad {{ $data_brw->akad_pendanaan ? $data_brw->akad_pendanaan : ''}}
                    @endif
                </h1>
            </div>
        </div>
    </div>
</div>
{{-- END: Content Title --}}

<div class="content mt-3 mb-4">
    <div class="container-fluid">

        <div class="row mt-5">
            <div id="layout-informasi-analis" class="col-md-12">

                <div class="card">
                    <div class="box-title">
                        <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Informasi Analisa
                            &nbsp</label>
                    </div>
                    <div class="card-body pt-4 pl-4">
                        {{-- START: Baris 1 --}}
                        <div class="row">
                            <div class="col-md-6">Nama Kredit Analis <span class="float-right ml-1">:</span></div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                {{ !empty($data_hd_analis->nama_kredit_analis) ? $data_hd_analis->nama_kredit_analis :
                                '-' }}
                                {{-- {{ !empty($data_hd_analis->nama_kredit_analis) ?
                                $data_hd_analis->nama_kredit_analis : Auth::user()->firstname.' '.Auth::user()->lastname
                                }} --}}
                            </p>
                        </div>
                        {{-- END: Baris 1 --}}

                        {{-- START: Baris 2 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">Tanggal Analisa <span class="float-right ml-1">:</span></div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                @if (request()->get('keterangan') == 1)
                                {{ $data_hd_analis->tanggal_analisa_rac ? date('d-m-Y',
                                strtotime($data_hd_analis->tanggal_analisa_rac)) : '-'}}
                                @elseif (request()->get('keterangan') == 2)
                                {{ !empty($data_hd_analis->tanggal_analisa_lembar) ? date('d-m-Y',
                                strtotime($data_hd_analis->tanggal_analisa_lembar)) : '-'}}
                                @else
                                {{ !empty($data_hd_analis->tanggal_analisa_resume) ? date('d-m-Y',
                                strtotime($data_hd_analis->tanggal_analisa_resume)) : '-' }}
                                @endif
                            </p>
                        </div>
                        {{-- END: Baris 2 --}}

                        {{-- START: Baris 3 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">ID Penerima Dana | ID Pengajuan Pendanaan <span
                                    class="float-right ml-1">:</span></div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                {{ !empty($data_brw->brw_id) ? $data_brw->brw_id.' | '.$data_brw->pengajuan_id : '-' }}
                            </p>
                        </div>
                        {{-- END: Baris 3 --}}

                        {{-- START: Baris 4 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">Nama Penerima Dana <span class="float-right ml-1">:</span></div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                {{ !empty($data_brw->nama_penerima_pendana) ? $data_brw->nama_penerima_pendana : '-' }}
                            </p>
                        </div>
                        {{-- END: Baris 4 --}}

                        {{-- START: Baris 5 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">Plafond Pengajuan Pendanaan <span class="float-right ml-1">:</span>
                            </div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                Rp{{ !empty($data_brw->plafon_pengajuan) ? str_replace(',',
                                '.',number_format($data_brw->plafon_pengajuan)) : '-' }}
                            </p>
                        </div>
                        {{-- END: Baris 5 --}}

                        {{-- START: Baris 6 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">Fasilitas Pendanaan <span class="float-right ml-1">:</span></div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                {{ !empty($data_brw->pendanaan_nama) ? $data_brw->pendanaan_nama : '-' }}</p>
                        </div>
                        {{-- END: Baris 6 --}}

                        {{-- START: Baris 7 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">Jangka Waktu Pendanaan (Bulan) <span class="float-right ml-1">:</span>
                            </div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                {{ !empty($data_brw->jangka_waktu_pendanaan) ? $data_brw->jangka_waktu_pendanaan : '-'
                                }}
                            </p>
                        </div>
                        {{-- END: Baris 7 --}}

                        {{-- START: Baris 8 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">Kategori Penerima Pendanaan <span class="float-right ml-1">:</span>
                            </div>
                            <p id="" class="col-md-6 pl-md-0 text-dark">
                                {{ !empty($data_brw->kategori_penerima_pendanaan) ?
                                $data_brw->kategori_penerima_pendanaan == '1' ? 'Fixed Income (Penghasilan Tetap)' :
                                'Non-Fixed Income (Penghasilan Tidak Tetap)' :
                                '-' }}
                            </p>
                        </div>
                        {{-- END: Baris 8 --}}

                        {{-- START: Baris 9 --}}
                        <div class="row mt-2">
                            <div class="col-md-6">Skema Pengajuan <span class="float-right ml-1">:</span></div>
                            <p class="col-md-6 pl-md-0 text-dark">
                                {{ !empty($data_brw->skema_pembiayaan) ?
                                $data_brw->skema_pembiayaan == '1' ? 'Single Income (Penghasilan Sendiri)' :
                                ($data_brw->skema_pembiayaan == '2' ? 'Join Income (Penghasilan Bersama)' : '-')
                                : '-'}}
                            </p>
                        </div>
                        {{-- END: Baris 9 --}}
                    </div>
                </div>
            </div>

        </div>

        @if (request()->get('keterangan') == '1')
        {{-- START: Layout RAC --}}
        <form method="POST"
            action="{{ route('analis.detail-pekerjaan-update', ['pengajuan_id' => $data_brw->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
            id="form-rac" name="form-rac">
            {{-- @method('put') --}}
            @csrf
            @include('pages.admin.analis.pendanaan_rac')
        </form>
        {{-- END: Layout RAC --}}
        @endif

        @if (request()->get('keterangan') == '2')
        {{-- START: Layout Lembar Perhitungan --}}
        <form method="POST"
            action="{{ route('analis.detail-pekerjaan-update', ['pengajuan_id' => $data_brw->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
            id="form-lembar-perhitungan" name="form-lembar-perhitungan">
            {{-- @method('put') --}}
            @csrf
            {{-- START: Extra ID --}}
            <input type="hidden" name="brw_id" value="{{ !empty($data_brw->brw_id) ? $data_brw->brw_id : '' }}">
            {{-- END: Extra ID --}}
            @include('pages.admin.analis.pendanaan_lembar_perhitungan')
        </form>
        {{-- END: Layout Lembar Perhitungan --}}
        @endif

        @if (request()->get('keterangan') == '3')
        {{-- START: Resume Akad --}}
        <form method="POST"
            action="{{ route('analis.detail-pekerjaan-update', ['pengajuan_id' => $data_brw->pengajuan_id, 'keterangan' => request()->get('keterangan')]) }}"
            id="form-resume-akad" name="form-resume-akad">
            @csrf
            @include('pages.admin.analis.pendanaan_resume_akad')
        </form>
        {{-- END: Resume Akad --}}
        @endif

        <div class="row mb-4">
            <div class="col-md-12">
                <div class="float-right">
                    <a href="{{ route('analis.daftar-pekerjaan', ['pengajuan_id' => $data_brw->pengajuan_id]) }}"
                        class="btn btn-danaSyariah text-white rounded">Kembali</a>
                    
                        @if (!empty($data_hd_analis->nama_kredit_analis))
                            @if ($data_hd_analis->status_analis != 6 && $data_hd_analis->nama_kredit_analis == Auth::user()->firstname.' '.Auth::user()->lastname)
                                <button type="button" id="btn_simpan" name="btn_simpan" class="btn btn-danaSyariah text-white rounded">Simpan</button>
                            @endif
                        @else
                            <button type="button" id="btn_simpan" name="btn_simpan" class="btn btn-danaSyariah text-white rounded">Simpan</button>
                        @endif
                   
                    {{-- <button type="button" id="btn_simpan" name="btn_simpan"
                        class="btn btn-danaSyariah text-white rounded" {{ empty($data_hd_analis->nama_kredit_analis) ?
                        '' : !empty($data_hd_analis->nama_kredit_analis) && $data_hd_analis->status_analis == 6 ||
                        $data_hd_analis->nama_kredit_analis != Auth::user()->firstname.' '.Auth::user()->lastname ?
                        'disabled' : ''}}>Simpan</button> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
    
    $(document).ready(function(){
        $(".no-zero").on("input", function(){
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });

        let keterangan = "{!! request()->get('keterangan') !!}"
        $('#btn_simpan').click(() => {
            if (keterangan == '1') {
                $('#form-rac').submit()
            } else if (keterangan == '2') {
                $('#form-lembar-perhitungan').submit()
            } else if (keterangan == '3') {
                $('#form-resume-akad').submit()
            }
        })

        $('#form-rac').submit(function(e) {
            e.preventDefault()
            submitData('#form-rac')
        })

        $('#form-lembar-perhitungan').submit(function(e) {
            e.preventDefault()
            submitData('#form-lembar-perhitungan')
        })

        $('#form-resume-akad').submit(function(e) {
            e.preventDefault()
            submitData('#form-resume-akad')
        })
    });

    // START: Other Function
    const formatNumber = (num) => {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    };
    // END: Other Function

     // Rupiah Currency Format
    const formatRupiah = (angka, prefix) => {
        let thisValue = angka.replace(/[^,\d]/g, '')
        angka = (parseInt(thisValue, 10)).toString()

        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
    }

    // Submit Data
    const submitData = (element_id) => {
        $.ajax({
            url : $(element_id).attr('action'),
            type: $(element_id).attr("method"),
            dataType: 'JSON',
            data: $(element_id).serialize(),
            beforeSend: function() {
                swal.fire({
                    html: '<h5>Menyimpan Data...</h5>',
                    showConfirmButton: false,
                    allowOutsideClick: () => false,
                    onBeforeOpen: () => {
                        swal.showLoading();
                    }
                });
            },
            success: function (response) {
                if (response.status == 'success') {
                    swal.fire({
                        title: 'Berhasil',
                        type: 'success',
                        text: response.msg,
                        allowOutsideClick: () => false,
                    }).then(function (result) {
                        if (result.value) {
                            window.location = "{{ route('analis.daftar-pekerjaan', ['pengajuan_id' => $data_brw->pengajuan_id]) }}"
                        }
                    })
                } else {
                    swal.fire({
                        title: 'Oops',
                        type: 'error',
                        text: response.msg
                    })
                    console.log(response)
                }
            },
            error: function(response) {
                swal.fire({
                    title: 'Error',
                    type: 'error',
                    text: response.msg
                })
                console.log(response)
            }
        })
    }
</script>
@endsection