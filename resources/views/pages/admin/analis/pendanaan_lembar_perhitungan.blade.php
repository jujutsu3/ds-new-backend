@push('add-more-style')
<style>
    tfoot tr {
        border-top: 10px solid white;
    }
</style>
@endpush
{{-- START: Informasi Pendanaan --}}
<div class="row mt-3">
    <div id="layout-informasi-pendanaan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Informasi Pendanaan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">

                {{-- START: Baris 1 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_nomor_naup" class="col-sm-5 col-form-label font-weight-normal">Nomor
                                NAUP</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nomor_naup" name="text_nomor_naup"
                                    value="{{ $data_brw ? $data_brw->no_naup : '' }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 1 --}}

                {{-- START: Baris 2 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_nama_penerima_pendanaan"
                                class="col-sm-5 col-form-label font-weight-normal">Nama Penerima Pendanaan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nama_penerima_pendanaan"
                                    name="text_nama_penerima_pendanaan"
                                    value="{{ $data_brw ? $data_brw->nama_penerima_pendana : '' }}" readonly>
                            </div>
                        </div>
                    </div>

                    @if ($data_brw && $data_brw->skema_pembiayaan == 2)
                    <div class="col-md-6">
                        {{-- START: Baris 9 --}}
                        <div class="form-group row">
                            <label for="text_nama_pasangan" class="col-sm-5 col-form-label font-weight-normal">Nama
                                Pasangan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nama_pasangan"
                                    name="text_nama_pasangan"
                                    value="{{ $data_brw_pasangan ? $data_brw_pasangan->nama : '' }}" readonly>
                            </div>
                        </div>
                        {{-- END: Baris 9 --}}
                    </div>
                    @endif
                </div>
                {{-- END: Baris 2 --}}

                {{-- START: Baris 3 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_usia_saat_pengajuan"
                                class="col-sm-5 col-form-label font-weight-normal">Usia Saat Pengajuan (Tahun)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="hidden" class="form-control" id="text_tgl_lahir" name="text_tgl_lahir"
                                    value="{{ $data_brw ? $data_brw->tgl_lahir : '' }}">
                                <input type="text" class="form-control" id="text_usia_saat_pengajuan"
                                    name="text_usia_saat_pengajuan"
                                    value="{{ $data_brw ? \Carbon\Carbon::parse($data_brw->tgl_lahir)->age : '' }}"
                                    readonly>
                            </div>
                        </div>
                    </div>
                    @if ($data_brw && $data_brw->skema_pembiayaan == 2)
                    <div class="col-md-6">
                        {{-- START: Baris 10 --}}
                        <div class="form-group row">
                            <label for="text_usia_pasangan" class="col-sm-5 col-form-label font-weight-normal">Usia
                                Pasangan (Tahun)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="hidden" class="form-control" id="text_tgl_lahir_pasangan"
                                    name="text_tgl_lahir_pasangan"
                                    value="{{ $data_brw_pasangan ? $data_brw_pasangan->tgl_lahir : '' }}">
                                <input type="text" class="form-control" id="text_usia_pasangan"
                                    name="text_usia_pasangan"
                                    value="{{ $data_brw_pasangan ? \Carbon\Carbon::parse($data_brw_pasangan->tgl_lahir)->age : '' }}"
                                    readonly>
                            </div>
                        </div>
                        {{-- END: Baris 10 --}}
                    </div>
                    @endif
                </div>
                {{-- END: Baris 3 --}}

                {{-- START: Baris 4 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_plafon_pengajuan" class="col-sm-5 col-form-label font-weight-normal">Plafon
                                Pengajuan (Rp)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_plafon_pengajuan"
                                    name="text_plafon_pengajuan"
                                    value="{{ $data_brw ? 'Rp'.str_replace(',', '.', number_format($data_brw->plafon_pengajuan)) : '' }}"
                                    onkeyup="this.value = formatRupiah(this.value, 'Rp'); $('#text_plafon_pengajuan_2').text(this.value)"
                                    readonly>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 4 --}}

                {{-- START: Baris 5 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_margin_pendanaan" class="col-sm-5 col-form-label font-weight-normal">Margin
                                (%)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_margin_pendanaan"
                                    name="text_margin_pendanaan" placeholder="(%)"
                                    onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');"
                                    value="{{ $data_brw->margin ? $data_brw->margin : $default_margin }}">
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 5 --}}

                {{-- START: Baris 6 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_jangka_waktu" class="col-sm-5 col-form-label font-weight-normal">Jangka
                                Waktu (Bulan)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_jangka_waktu" name="text_jangka_waktu"
                                    onkeyup="this.value = this.value.replace(/[^0-9]/g,'');"
                                    value="{{ $data_brw ? $data_brw->jangka_waktu_pendanaan : '' }}">
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 6 --}}

                {{-- START: Baris 7 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_skema_pengajuan" class="col-sm-5 col-form-label font-weight-normal">Skema
                                Pengajuan</label>
                            <div class="col-sm-7 my-auto">
                                <select name="text_skema_pengajuan" id="text_skema_pengajuan"
                                    class="form-control custom-select" disabled>
                                    <option value="">-- Pilih Satu --</option>
                                    <option value="1" {{ $data_brw ? $data_brw->skema_pembiayaan == 1 ? 'selected' : ''
                                        : ''}}>Single
                                        Income(Penghasilan Sendiri)</option>
                                    <option value="2" {{ $data_brw ? $data_brw->skema_pembiayaan == 2 ? 'selected' : ''
                                        : ''}}>Join
                                        Income(Penghasilan Bersama)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 7 --}}

                {{-- START: Baris 8 --}}
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="text_skema_pengajuan" class="col-sm-5 col-form-label font-weight-normal">Akad
                                Pendanaan</label>
                            <div class="col-sm-7 my-auto">
                                <select name="text_akad_pendanaan" id="text_akad_pendanaan"
                                    class="form-control custom-select">
                                    <option value="">-- Pilih Satu --</option>
                                    @foreach ($master_akad as $item)
                                    <option value="{{ $item->id }}" {{ $data_brw ? $data_brw->akad_pendanaan ==
                                        $item->jenis_akad ? 'selected' : '' : ''}}>
                                        {{ $item->jenis_akad }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Baris 8 --}}
            </div>
        </div>
    </div>
</div>
{{-- END: Informasi Pendanaan --}}

{{-- START: Hubungan dengan Lembaga Keuangan --}}
<div class="row mt-3">
    <div id="layout-informasi-hub-lembaga-keuangan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Hubungan dengan Lembaga Keuangan
                    &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="table-responsive px-1">
                            <table id="table_hub_lembaga_keuangan" class="table table-striped table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th class="align-middle">No</th>
                                        <th class="align-middle">Jenis Fasilitas</th>
                                        <th class="align-middle">Fasilitas Milik</th>
                                        <th class="align-middle">Tenor(bulan)</th>
                                        <th class="align-middle">Margin(%)</th>
                                        <th class="align-middle">Bank/Lembaga Keuangan</th>
                                        <th class="align-middle">Plafond Awal</th>
                                        <th class="align-middle">Baki Debet/OS</th>
                                        <th class="align-middle">Angsuran/Min. Payment</th>
                                        <th class="align-middle">History Payment</th>
                                        <th class="align-middle" style="width: 9%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="align-middle">
                                    <tr></tr>
                                    @if (!empty($brw_lembar_fasilitas_external))

                                    @foreach ($brw_lembar_fasilitas_external as $data)
                                    <tr class="text-center align-middle" id="tr-{{ $loop->iteration }}">
                                        <td class="text-center align-middle" scope="row">
                                            <p id="te_num_{{ $loop->iteration }}" name="te_num[]">{{ $loop->iteration }}
                                            </p>
                                            <input type="text" class="form-control d-none"
                                                id="in_num_{{ $loop->iteration }}" name="in_num[]"
                                                value="{{ $data->id }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_jenis_fasilitas_{{$loop->iteration }}">
                                                {{ $data->jenis_fasilitas }}</p>
                                            <input type="text" class="form-control d-none"
                                                id="in_jenis_fasilitas_{{$loop->iteration }}"
                                                name="in_jenis_fasilitas[]" value="{{ $data->jenis_fasilitas }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_fasilitas_milik_{{$loop->iteration }}">
                                                {{ $data->fasilitas_milik }}</p>
                                            <input type="text" class="form-control d-none"
                                                id="in_fasilitas_milik_{{$loop->iteration }}"
                                                name="in_fasilitas_milik[]" value="{{ $data->fasilitas_milik }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_tenor_{{$loop->iteration }}">{{ $data->tenor }}</p>
                                            <input type="text" class="form-control d-none"
                                                id="in_tenor_{{$loop->iteration }}" name="in_tenor[]"
                                                value="{{ $data->tenor }}"
                                                onkeyup="this.value = this.value.replace(/[^0-9]/g,'');">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_margin_{{$loop->iteration }}">{{ $data->margin }}</p>
                                            <input type="text" class="form-control d-none"
                                                id="in_margin_{{$loop->iteration }}" name="in_margin[]"
                                                value="{{ $data->margin }}"
                                                onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_bank_{{$loop->iteration }}">{{ $data->bank_lembaga_keuangan }}</p>
                                            <input type="text" class="form-control d-none"
                                                id="in_bank_{{$loop->iteration }}" name="in_bank[]"
                                                value="{{ $data->bank_lembaga_keuangan }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_plafond_awal_{{$loop->iteration }}">
                                                Rp{{ str_replace(',', '.', number_format($data->plafond_awal)) }}</p>
                                            <input type="text" class="form-control d-none"
                                                id="in_plafond_awal_{{$loop->iteration }}" name="in_plafond_awal[]"
                                                value="{{(int)$data->plafond_awal }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_baki_debet_{{$loop->iteration }}">
                                                Rp{{ str_replace(',', '.', number_format($data->baki_debet)) }}
                                            </p>
                                            <input type="text" class="form-control d-none"
                                                id="in_baki_debet_{{$loop->iteration }}" name="in_baki_debet[]"
                                                value="{{ (int)$data->baki_debet }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_angsuran_{{$loop->iteration }}">
                                                Rp{{ str_replace(',', '.', number_format($data->angsuran)) }}
                                            </p>
                                            <input type="text" class="form-control d-none"
                                                id="in_angsuran_{{$loop->iteration }}" name="in_angsuran[]"
                                                value="{{ (int)$data->angsuran }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <p id="te_history_payment_{{$loop->iteration }}">
                                                {{ $data->history_payment }}</p>
                                            <input type="text" class="form-control d-none"
                                                id="in_history_payment_{{$loop->iteration }}"
                                                name="in_history_payment[]" value="{{ $data->history_payment }}">
                                        </td>
                                        <td class="text-center align-middle">
                                            <button type="button" id="btn-edit-hub-{{$loop->iteration }}"
                                                class="btn btn-link" data-toggle="tooltip" data-placement="top"
                                                title="Edit" onclick="editHubLembagaKeuangan({{$loop->iteration }})">
                                                <i class="fa fa-lg fa-edit text-dark"></i>
                                            </button>
                                            <button type="button" id="btn-delete-hub-{{$loop->iteration }}"
                                                class="btn btn-link" data-toggle="tooltip" data-placement="top"
                                                title="Hapus"
                                                onclick="deleteHubLembagaKeuangan({{$loop->iteration }}, {{ $data->id }})">
                                                <span class="fa fa-lg fa-trash text-dark"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    <div id="deleted-data-hub">

                                    </div>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th class="text-center align-middle table-active" colspan='6'>Total</th>
                                        <th class="text-center align-middle table-active" id="total_plafond_awal">
                                        </th>
                                        <th class="text-center align-middle table-active" id="total_baki_debet">
                                        </th>
                                        <th class="text-center align-middle table-active" id="total_angsuran">
                                        </th>
                                        <th class="text-center align-middle table-active">
                                        </th>
                                        <th class="text-center align-middle border-0">
                                            <button type="button" id="btn-add-hub" class="btn btn-primary btn-block"
                                                data-toggle="modal" data-target="#modal_hub_lembaga_keuangan"
                                                data-backdrop="static" data-keyboard="false"> <i class="fa fa-plus"></i>
                                                Tambah</button>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Hubungan dengan Lembaga Keuangan --}}

{{-- START: Data Pendapatan --}}
<div class="row mt-3">
    <div id="layout-data-pendapatan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Data Pendapatan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div id="pendapatan-penerima-pendanaan">
                            <p class="h6 mb-3">Pendapatan Penerima Pendanaan</p>
                            <table id="table_pendapatan_penerima_pendanaan" class="table table-striped table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th class="align-middle"></th>
                                        <th class="align-middle">Bulan ke-1</th>
                                        <th class="align-middle">Bulan ke-2</th>
                                        <th class="align-middle">Bulan ke-3</th>
                                        <th class="align-middle">Rata-rata</th>
                                    </tr>
                                </thead>
                                <tbody class="align-middle">
                                    <tr>
                                        <td class="text-left align-middle">Gaji Pokok</td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="gaji_pokok_1"
                                                name="gaji_pokok_1"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_1)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_1', 'tunjangan_1', 'thp_1')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="gaji_pokok_2"
                                                name="gaji_pokok_2"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_2)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_2', 'tunjangan_2', 'thp_2')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="gaji_pokok_3"
                                                name="gaji_pokok_3"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_3)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_3', 'tunjangan_3', 'thp_3')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="gaji_pokok_rata2"
                                                name="gaji_pokok_rata2"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_rata2)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left align-middle">Tunjangan</td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="tunjangan_1"
                                                name="tunjangan_1"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_1)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_1', 'tunjangan_1', 'thp_1')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="tunjangan_2"
                                                name="tunjangan_2"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_2)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_2', 'tunjangan_2', 'thp_2')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="tunjangan_3"
                                                name="tunjangan_3"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_3)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_3', 'tunjangan_3', 'thp_3')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="tunjangan_rata2"
                                                name="tunjangan_rata2"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_rata2)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left align-middle">Take Home Pay</td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_1" name="thp_1"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_1)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp');" readonly>
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_2" name="thp_2"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_2)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp');" readonly>
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_3" name="thp_3"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_3)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp');" readonly>
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_rata2"
                                                name="thp_rata2"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_rata2)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        @if ($data_brw->skema_pembiayaan == 2)
                        <div id="pendapatan-pasangan-penerima-pendanaan" class="mt-4">
                            <p class="h6 mb-3">Pendapatan Pasangan Penerima Pendanaan</p>
                            <table id="table_pendapatan_pasangan_penerima_pendanaan"
                                class="table table-striped table-bordered">
                                <thead class="text-center">
                                    <tr>
                                        <th class="align-middle"></th>
                                        <th class="align-middle">Bulan ke-1</th>
                                        <th class="align-middle">Bulan ke-2</th>
                                        <th class="align-middle">Bulan ke-3</th>
                                        <th class="align-middle">Rata-rata</th>
                                    </tr>
                                </thead>
                                <tbody class="align-middle">
                                    <tr>
                                        <td class="text-left align-middle">Gaji Pokok</td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="gaji_pokok_1_pasangan"
                                                name="gaji_pokok_1_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_1_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_1_pasangan', 'tunjangan_1_pasangan', 'thp_1_pasangan')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="gaji_pokok_2_pasangan"
                                                name="gaji_pokok_2_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_2_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_2_pasangan', 'tunjangan_2_pasangan', 'thp_2_pasangan')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="gaji_pokok_3_pasangan"
                                                name="gaji_pokok_3_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_3_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_3_pasangan', 'tunjangan_3_pasangan', 'thp_3_pasangan')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero"
                                                id="gaji_pokok_rata2_pasangan" name="gaji_pokok_rata2_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->gaji_pokok_rata2_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left align-middle">Tunjangan</td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="tunjangan_1_pasangan"
                                                name="tunjangan_1_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_1_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_1_pasangan', 'tunjangan_1_pasangan', 'thp_1_pasangan')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="tunjangan_2_pasangan"
                                                name="tunjangan_2_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_2_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_2_pasangan', 'tunjangan_2_pasangan', 'thp_2_pasangan')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="tunjangan_3_pasangan"
                                                name="tunjangan_3_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_3_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp'); calThp('gaji_pokok_3_pasangan', 'tunjangan_3_pasangan', 'thp_3_pasangan')">
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero"
                                                id="tunjangan_rata2_pasangan" name="tunjangan_rata2_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->tunjangan_rata2_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left align-middle">Take Home Pay</td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_1_pasangan"
                                                name="thp_1_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_1_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_2_pasangan"
                                                name="thp_2_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_2_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_3_pasangan"
                                                name="thp_3_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_3_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                        <td class="text-center align-middle">
                                            <input type="text" class="form-control no-zero" id="thp_rata2_pasangan"
                                                name="thp_rata2_pasangan"
                                                value="{{ $brw_pendapatan ? 'Rp'.str_replace(',', '.', number_format($brw_pendapatan->thp_rata2_pasangan)) : '' }}"
                                                onkeyup="this.value = formatRupiah(this.value, 'Rp')" readonly>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="text_total_pendapatan" class="col-sm-8 col-form-label font-weight-normal">Total
                        Pendapatan</label>
                    <div class="col-sm-4 my-auto">
                        <input type="text" class="form-control" id="text_total_pendapatan" name="text_total_pendapatan"
                            value="" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="text_cash_ratio" class="col-sm-8 col-form-label font-weight-normal">% Cash Ratio(CR)
                        Maksimal Sebelum Memperhitungkan Kewajiban</label>
                    <div class="col-sm-4 my-auto">
                        <input type="text" class="form-control" id="text_cash_ratio" name="text_cash_ratio"
                            placeholder="(%)" value="{{ $brw_pendapatan ? $brw_pendapatan->cr_maks : '' }}"
                            onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="text_angsuran_sebelum" class="col-sm-8 col-form-label font-weight-normal">Maks Angsuran
                        Sebelum Memperhitungkan Kewajiban</label>
                    <div class="col-sm-4 my-auto">
                        <input type="text" class="form-control" id="text_angsuran_sebelum" name="text_angsuran_sebelum"
                            value="" readonly>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="text_angsuran_setelah" class="col-sm-8 col-form-label font-weight-normal">Maks Angsuran
                        Setelah Memperhitungkan Kewajiban</label>
                    <div class="col-sm-4 my-auto">
                        <input type="text" class="form-control" id="text_angsuran_setelah" name="text_angsuran_setelah"
                            value="" readonly>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{{-- END: Data Pendapatan --}}

{{-- START: Data Objek Pendanaan --}}
<div class="row mt-3">
    <div id="layout-data-objek-pendanaan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Data Objek Pendanaan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="row mt-4">
                    <div class="col-md-6">
                        {{-- START: Baris 1 --}}
                        <div class="form-group row">
                            <label for="text_tipe_agunan" class="col-sm-5 col-form-label font-weight-normal">Tipe
                                Agunan</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_tipe_agunan" name="text_tipe_agunan"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->tipe_agunan : '' }}"
                                    placeholder="Masukkan tipe angunan">
                            </div>
                        </div>
                        {{-- END: Baris 1 --}}

                        {{-- START: Baris 2 --}}
                        <div class="form-group row">
                            <label for="text_jenis_objek_pendanaan"
                                class="col-sm-5 col-form-label font-weight-normal">Jenis Objek Pendanaan</label>
                            <div class="col-sm-7 my-auto">
                                <select name="text_jenis_objek_pendanaan" id="text_jenis_objek_pendanaan"
                                    class="form-control custom-select">
                                    <option value="">-- Pilih Satu --</option>
                                    <option value="1" {{ $data_objek_pendanaan ? $data_objek_pendanaan->
                                        jenis_objek_pendanaan == '1' ? 'selected' : '' : '' }}>Tanah</option>
                                    <option value="2" {{ $data_objek_pendanaan ? $data_objek_pendanaan->
                                        jenis_objek_pendanaan == '2' ? 'selected' : '' : '' }}>Tanah & Bangunan</option>
                                </select>
                                {{-- <input type="text" class="form-control" id="text_jenis_objek_pendanaan"
                                    name="text_jenis_objek_pendanaan"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->jenis_objek_pendanaan : '' }}"
                                    placeholder="Masukkan jenis objek pendanaan"> --}}
                            </div>
                        </div>
                        {{-- END: Baris 2 --}}

                        {{-- START: Baris 3 --}}
                        <div class="form-group row">
                            <label for="text_nilai_pasar_wajar" class="col-sm-5 col-form-label font-weight-normal">Nilai
                                Pasar Wajar</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control no-zero" id="text_nilai_pasar_wajar"
                                    name="text_nilai_pasar_wajar"
                                    value="{{ $data_objek_pendanaan ? 'Rp'.str_replace(',', '.',number_format($data_objek_pendanaan->nilai_pasar_wajar)) : '' }}"
                                    placeholder="Masukkan nilai pasar wajar"
                                    onkeyup="this.value = formatRupiah(this.value, 'Rp'); maksFTVRp()">
                            </div>
                        </div>
                        {{-- END: Baris 3 --}}

                        {{-- START: Baris 4 --}}
                        <div class="form-group row">
                            <label for="text_nilai_likuidasi" class="col-sm-5 col-form-label font-weight-normal">Nilai
                                Likuidasi</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nilai_likuidasi"
                                    name="text_nilai_likuidasi"
                                    value="{{ $data_objek_pendanaan ? 'Rp'.str_replace(',', '.',number_format($data_objek_pendanaan->nilai_likuidasi)) : '' }}"
                                    placeholder="Masukkan nilai likuidasi"
                                    onkeyup="this.value = formatRupiah(this.value, 'Rp')">
                            </div>
                        </div>
                        {{-- END: Baris 4 --}}

                        {{-- START: Baris 5 --}}
                        <div class="form-group row">
                            <label for="text_maks_ftv_percent" class="col-sm-5 col-form-label font-weight-normal">Maks
                                FTV(%)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_maks_ftv_percent"
                                    name="text_maks_ftv_percent" placeholder="(%)"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->maks_ftv_persentase : '' }}"
                                    onkeyup="this.value = this.value.replace(/[^0-9\.]/g,''); maksFTVRp()">
                            </div>
                        </div>
                        {{-- END: Baris 5 --}}

                        {{-- START: Baris 6 --}}
                        <div class="form-group row">
                            <label for="text_maks_ftv_nominal" class="col-sm-5 col-form-label font-weight-normal">Maks
                                FTV(RP)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_maks_ftv_nominal"
                                    name="text_maks_ftv_nominal"
                                    value="{{ $data_objek_pendanaan ? 'Rp'.str_replace(',', '.',number_format($data_objek_pendanaan->maks_ftv_nominal)) : '' }}"
                                    placeholder="Masukkan maks nominal FTV" {{--
                                    onkeyup="this.value = formatRupiah($(this).val(), 'Rp'); $('#text_maks_ftv_nominal_2').text($(this).val());"
                                    --}} readonly>
                            </div>
                        </div>
                        {{-- END: Baris 6 --}}

                        {{-- START: Baris 7 --}}
                        <div class="form-group row">
                            <label for="text_untuk_fasilitas" class="col-sm-5 col-form-label font-weight-normal">Untuk
                                Fasilitas</label>
                            <div class="col-sm-7 my-auto">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Fasilitas ke-</span>
                                    </div>
                                    <input type="text" class="form-control" id="text_untuk_fasilitas"
                                        name="text_untuk_fasilitas"
                                        value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->fasilitas_ke : '' }}"
                                        onkeyup="this.value = this.value.replace(/[^0-9]/g,'');">
                                </div>

                                {{-- <input type="text" class="form-control" id="text_untuk_fasilitas"
                                    name="text_untuk_fasilitas"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->fasilitas_ke : '' }}"
                                    onkeyup="this.value = this.value.replace(/[^0-9]/g,'');"
                                    placeholder="Masukkan fasilitas"> --}}
                            </div>
                        </div>
                        {{-- END: Baris 7 --}}
                    </div>

                    <div class="col-md-6">

                        {{-- START: Baris 8 --}}
                        <div class="form-group row">
                            <label for="text_luas_tanah" class="col-sm-5 col-form-label font-weight-normal">Luas
                                Tanah(M<sup>2</sup>)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="number" class="form-control" id="text_luas_tanah" name="text_luas_tanah"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->luas_tanah : '' }}"
                                    placeholder="Masukkan luas tanan">
                            </div>
                        </div>
                        {{-- END: Baris 8 --}}

                        {{-- START: Baris 9 --}}
                        <div class="form-group row">
                            <label for="text_luas_bangunan" class="col-sm-5 col-form-label font-weight-normal">Luas
                                Bangunan(M<sup>2</sup>)</label>
                            <div class="col-sm-7 my-auto">
                                <input type="number" class="form-control" id="text_luas_bangunan"
                                    name="text_luas_bangunan"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->luas_bangunan : '' }}"
                                    placeholder="Masukkan luas bangunan">
                            </div>
                        </div>
                        {{-- END: Baris 9 --}}

                        {{-- START: Baris 10 --}}
                        <div class="form-group row">
                            <label for="text_bukti_kepemilikan_angunan"
                                class="col-sm-5 col-form-label font-weight-normal">Bukti Kepemilikan Angunan</label>
                            <div class="col-sm-7 my-auto">
                                <select name="text_bukti_kepemilikan_angunan" id="text_bukti_kepemilikan_angunan"
                                    class="form-control custom-select">
                                    <option value="">-- Pilih Satu --</option>
                                    <option value="1" {{ $data_objek_pendanaan && $data_objek_pendanaan->jenis_agunan ==
                                        1 ? 'selected' : ''}}>
                                        SHM</option>
                                    <option value="2" {{ $data_objek_pendanaan && $data_objek_pendanaan->jenis_agunan ==
                                        2 ? 'selected' : ''}}>
                                        SHGB</option>
                                </select>
                            </div>
                        </div>
                        {{-- END: Baris 10 --}}

                        {{-- START: Baris 11 --}}
                        <div class="form-group row">
                            <label for="text_nomor_sertifikat" class="col-sm-5 col-form-label font-weight-normal">Nomor
                                Sertifikat</label>
                            <div class="col-sm-7 my-auto">
                                <input type="text" class="form-control" id="text_nomor_sertifikat"
                                    name="text_nomor_sertifikat"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->nomor_agunan : '' }}"
                                    placeholder="Masukkan nomor sertifikat">
                            </div>
                        </div>
                        {{-- END: Baris 11 --}}

                        {{-- START: Baris 12 --}}
                        <div class="form-group row" id="layout_tanggal_jatuh_tempo">
                            <label for="text_tanggal_jatuh_tempo"
                                class="col-sm-5 col-form-label font-weight-normal">Tanggal Jatuh Tempo</label>
                            <div class="col-sm-7 my-auto">
                                <input type="date" class="form-control" id="text_tanggal_jatuh_tempo"
                                    name="text_tanggal_jatuh_tempo"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->tanggal_jatuh_tempo : '' }}"
                                    placeholder="Masukkan tanggal jatuh tempo">
                            </div>
                        </div>
                        {{-- END: Baris 12 --}}

                        {{-- START: Baris 13 --}}
                        <div class="form-group row">
                            <label for="text_tanggal_terbit_sertifikat"
                                class="col-sm-5 col-form-label font-weight-normal">Tanggal Terbit Sertifikat</label>
                            <div class="col-sm-7 my-auto">
                                <input type="date" class="form-control" id="text_tanggal_terbit_sertifikat"
                                    name="text_tanggal_terbit_sertifikat"
                                    value="{{ $data_objek_pendanaan ? $data_objek_pendanaan->tanggal_terbit : '' }}"
                                    placeholder="Masukkan tanggal terbit sertifikat">
                            </div>
                        </div>
                        {{-- END: Baris 13 --}}

                        {{-- START: Baris 14 --}}
                        <div class="form-group row">
                            <label for="text_lokasi_objek_pendanaan"
                                class="col-sm-5 col-form-label font-weight-normal">Lokasi Objek Pendanaan</label>
                            <div class="col-sm-7 my-auto">
                                <textarea name="text_lokasi_objek_pendanaan" id="text_lokasi_objek_pendanaan" rows="3"
                                    class="form-control"
                                    readonly>{{ $data_brw && $data_objek_pendanaan ? $data_brw->lokasi_proyek . ', blok nomor. '. $data_objek_pendanaan->blok_nomor .', RT. '. $data_objek_pendanaan->RT .', RW. '. $data_objek_pendanaan->RW .', Kel. '. $data_brw->kelurahan .', Kec. '. $data_brw->kecamatan .', Kota. '. $data_brw->kota .', Prov. '. $data_brw->provinsi .', Kode Pos '. $data_brw->kode_pos : '' }}</textarea>
                                {{-- <input type="text" class="form-control" id="text_lokasi_objek_pendanaan"
                                    name="text_lokasi_objek_pendanaan"
                                    value="{{ $data_brw ? $data_brw->lokasi_proyek . ', Kel. '. $data_brw->kelurahan .', Kec. '. $data_brw->kecamatan .', Kota. '. $data_brw->kota .', Prov. '. $data_brw->provinsi .', Kode Pos '. $data_brw->kode_pos : '' }}"
                                    placeholder="Masukkan lokasi objek pendanaan" readonly> --}}
                            </div>
                        </div>
                        {{-- END: Baris 14 --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Data Objek Pendanaan --}}

{{-- START: Data Pendapatan --}}
<div class="row mt-3">
    <div id="layout-data-pendapatan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Hasil Perhitungan Analis
                    &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="row mt-4">
                    <div class="col-md-12">
                        <table id="table_hasil_perhitungan_analis" class="table table-striped table-bordered">
                            <thead class="text-center">
                                <tr>
                                    <th class="align-middle">Plafon Pengajuan</th>
                                    <th class="align-middle">Maks Plafon by FTV</th>
                                    <th class="align-middle">Maks Plafon by CR</th>
                                    <th class="align-middle">Input Plafon Rekomendasi</th>
                                    <th class="align-middle">Angsuran</th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class="align-middle">
                                <tr>
                                    <td class="text-center align-middle">
                                        <p class="m-0 no-zero" id="text_plafon_pengajuan_2">
                                            {{ $data_brw->plafon_pengajuan ? 'Rp'.str_replace(',', '.',
                                            number_format($data_brw->plafon_pengajuan)) : '-' }}
                                        </p>
                                    </td>
                                    <td class="text-center align-middle">
                                        <p id="text_maks_ftv_nominal_2" class="m-0">
                                            {{ $data_lembar_analisa ? 'Rp'.str_replace(',',
                                            '.',number_format($data_lembar_analisa->maks_plafond_by_ftv)) : '' }}
                                        </p>
                                        <input type="hidden" name="text_maks_ftv_nominal_2"
                                            value="{{ $data_lembar_analisa ? $data_lembar_analisa->maks_plafond_by_ftv : '' }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <p id="text_maks_plafon_cr" class="m-0">
                                            {{ $data_lembar_analisa ? 'Rp'.str_replace(',',
                                            '.',number_format($data_lembar_analisa->maks_plafond_by_cr)) : '' }}
                                        </p>
                                        <input type="hidden" name="text_maks_plafon_cr"
                                            value="{{ $data_lembar_analisa ? $data_lembar_analisa->maks_plafond_by_cr : '' }}">
                                    </td>
                                    <td class="text-center align-middle">
                                        <input type="text" class="form-control no-zero" id="input_plafon_rekomendasi"
                                            name="input_plafon_rekomendasi"
                                            value="{{ $data_lembar_analisa ? 'Rp'.str_replace(',', '.', number_format($data_lembar_analisa->plafond_rekomendasi)) : '' }}"
                                            onkeyup="this.value = formatRupiah(this.value, 'Rp')">
                                    </td>
                                    <td class="text-center align-middle">
                                        <p id="text_angsuran_2" class="m-0">
                                            {{ $data_lembar_analisa ? 'Rp'.str_replace(',',
                                            '.',number_format($data_lembar_analisa->angsuran)) : '' }}
                                        </p>
                                        <input type="hidden" name="text_angsuran_2"
                                            value="{{ $data_lembar_analisa ? $data_lembar_analisa->angsuran : '' }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{{-- END: Data Pendapatan --}}

{{-- START: Modal --}}
<div class="modal fade" id="modal_hub_lembaga_keuangan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah data hubungan lembaga keuangan</h5>
                <button type="button" class="close" id="btn_close_hub" data-dismiss="modal" aria-label="Close"
                    onclick="clearValueField(); $('#btn-save-hub').val('1').text('Simpan')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{-- <form id="form-add-hub" name="form-add-hub"> --}}
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="text_jenis_fasilitas">Jenis Fasilitas</label>
                                    <input type="hidden" class="form-control" id="text_number_row"
                                        name="text_number_row">
                                    <input type="text" class="form-control" id="text_jenis_fasilitas"
                                        name="text_jenis_fasilitas" placeholder="Masukkan jenis fasilitas">
                                </div>
                                <div class="form-group">
                                    <label for="text_fasilitas_milik">Fasilitas Milik</label>
                                    <input type="text" class="form-control" id="text_fasilitas_milik"
                                        name="text_fasilitas_milik" placeholder="Masukkan fasilitas milik">
                                </div>
                                <div class="form-group">
                                    <label for="text_tenor">Tenor(bulan)</label>
                                    <input type="text" class="form-control" id="text_tenor" name="text_tenor"
                                        placeholder="Masukkan tenor"
                                        onkeyup="this.value = this.value.replace(/[^0-9]/g,'');">
                                </div>
                                <div class="form-group">
                                    <label for="text_margin">Margin(%)</label>
                                    <input type="text" class="form-control" id="text_margin" name="text_margin"
                                        placeholder="Masukkan margin"
                                        onkeyup="this.value = this.value.replace(/[^0-9\.]/g,'');">
                                </div>
                                <div class="form-group">
                                    <label for="text_bank">Bank/Lembaga Keuangan</label>
                                    <input type="text" class="form-control" id="text_bank" name="text_bank"
                                        placeholder="Masukkan bank atau lembaga keuangan">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="text_plafond_awal">Plafond Awal</label>
                                    <input type="text" class="form-control no-zero" id="text_plafond_awal"
                                        name="text_plafond_awal" placeholder="Masukkan plafond awal"
                                        onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                </div>
                                <div class="form-group">
                                    <label for="text_baki_debet">Baki Debet/Os</label>
                                    <input type="text" class="form-control no-zero" id="text_baki_debet"
                                        name="text_baki_debet" placeholder="Masukkan baki debet"
                                        onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                </div>
                                <div class="form-group">
                                    <label for="text_angsuran">Angsuran/Min. Payment</label>
                                    <input type="text" class="form-control no-zero" id="text_angsuran"
                                        name="text_angsuran" placeholder="Masukkan angsuran atau min. payment"
                                        onkeyup="this.value = formatRupiah(this.value, 'Rp');">
                                </div>
                                <div class="form-group">
                                    <label for="text_history_payment">History Payment</label>
                                    <input type="text" class="form-control" id="text_history_payment"
                                        name="text_history_payment" placeholder="Masukkan history payment">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        onclick="clearValueField(); $('#btn-save-hub').val('1').text('Simpan')">Batal</button>
                    <div id="btn-footer-hub">
                        <button type="button" class="btn btn-primary clicked" id="btn-save-hub"
                            value="1">Simpan</button>
                    </div>
                </div>
                {{--
            </form> --}}
        </div>
    </div>
</div>
{{-- END: Modal --}}

@push('scripts')

{{-- START: DOM --}}
<script>
    $(document).ready(function () {

        // Init
        let skema_pembiayaan = "{!! $data_brw ? $data_brw->skema_pembiayaan : '' !!}"
        
        $('#btn-save-hub').click(function() {
            // status 1 = add, 2 = edit
            let status = $(this).prop('value');
            let id_number = $('#text_number_row').val()
            if (status == 2) { // Edit
                addHubLembagaKeuangan(status, id_number)
            } else { // Add
                addHubLembagaKeuangan(status)
            }
        })

        // START: SHGB
        let jenis_agunan = `{!! $data_objek_pendanaan ? $data_objek_pendanaan->jenis_agunan : '' !!}`
        if (jenis_agunan == 2) {
            $('#layout_tanggal_jatuh_tempo').removeClass('d-none')
        } else {
            $('#layout_tanggal_jatuh_tempo').addClass('d-none')
        }

        $('#text_bukti_kepemilikan_angunan').change(function() {
            let this_value = $(this).val()
            if (this_value == 2) { //SHGB
                $('#layout_tanggal_jatuh_tempo').removeClass('d-none')
            } else {
                $('#layout_tanggal_jatuh_tempo').addClass('d-none')
            }
        })
        // END: SHGB

        // START: Rata - Rata Pendapatan
        $(`#gaji_pokok_1, #gaji_pokok_2, #gaji_pokok_3,
         #tunjangan_1, #tunjangan_2, #tunjangan_3,
         #thp_1, #thp_2, #thp_3`).keyup(function () {
            averageIncome()
        })

        if (skema_pembiayaan == 2) {
            $(`#gaji_pokok_1_pasangan, #gaji_pokok_2_pasangan, #gaji_pokok_3_pasangan,
            #tunjangan_1_pasangan, #tunjangan_2_pasangan, #tunjangan_3_pasangan,
            #thp_1_pasangan, #thp_2_pasangan, #thp_3_pasangan`).keyup(function () {
                averageIncomePasangan()
            })
        }
        // END: Rata - Rata Pendapatan

        // Maks Angsuran
        $('#text_cash_ratio').keyup(function() {
            maksAngsuran()
        })

        // START: Hasil Perhitungan Analis
        $('#input_plafon_rekomendasi, #text_margin_pendanaan, #text_jangka_waktu').keyup(function() {
            calculateAngsuran()
            calculatePlafonCR()
        })

        $('#text_angsuran_setelah, #text_angsuran_2').change(function() {
            calculatePlafonCR()
        })
        // END: Hasil Perhitungan Analis

        // Load Data
        sumTotalAngsuran()
        sumTotalBakiDebet()
        sumTotalPlafondAwal()
        totalPendapatan()
        maksAngsuran()
        calculateAngsuran()
        calculatePlafonCR()
    })
</script>
{{-- END: DOM --}}

{{-- START: Other Function --}}
<script>
    // Remove all char except number
    const removeAllCharExcNum = string => string.toString().replace(/[^0-9-]/g,'')

    // Calculate the average value
    // const averageNumber = data => data.reduce((a, b) => a + b) / data.lenght

    function averageNumber(data) {
        return Math.round(data.reduce((a, b) => a + b) / data.length);
    }

    const clearValueField = () => {
        //Clear Value Field
        $('#text_number_row').val('')
        $('#text_jenis_fasilitas').val('')
        $('#text_fasilitas_milik').val('')
        $('#text_tenor').val('')
        $('#text_margin').val('')
        $('#text_bank').val('')
        $('#text_plafond_awal').val('')
        $('#text_baki_debet').val('')
        $('#text_angsuran').val('')
        $('#text_history_payment').val('')
    }
    
    let row_id = '{!! $brw_lembar_fasilitas_external ? count($brw_lembar_fasilitas_external) : 0 !!}'
    let row_number = '{!! $brw_lembar_fasilitas_external ? count($brw_lembar_fasilitas_external) : 0!!}'

    const addHubLembagaKeuangan = (status, number) => {
        // status 1 = add, 2 = edit
        let text_jenis_fasilitas = $('#text_jenis_fasilitas').val()
        let text_fasilitas_milik = $('#text_fasilitas_milik').val()
        let text_tenor = $('#text_tenor').val()
        let text_margin = $('#text_margin').val()
        let text_bank = $('#text_bank').val()
        let text_plafond_awal = $('#text_plafond_awal').val()
        let text_baki_debet = $('#text_baki_debet').val()
        let text_angsuran = $('#text_angsuran').val()
        let text_history_payment = $('#text_history_payment').val()
        
        if (status == 1) {
            row_id++
            row_number++
            $('#table_hub_lembaga_keuangan').find('tbody').append(`
                <tr class="text-center align-middle" id="tr-${row_id}">
                    <td class="text-center align-middle" scope="row">
                        <p id="te_num_${row_id}" name="te_num[]">${row_number}</p>
                        <input type="text" class="form-control d-none" id="in_num_${row_id}" name="in_num[]" value="0">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_jenis_fasilitas_${row_id}">${text_jenis_fasilitas}</p>
                        <input type="text" class="form-control d-none" id="in_jenis_fasilitas_${row_id}" name="in_jenis_fasilitas[]"
                            value="${text_jenis_fasilitas}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_fasilitas_milik_${row_id}">${text_fasilitas_milik}</p>
                        <input type="text" class="form-control d-none" id="in_fasilitas_milik_${row_id}" name="in_fasilitas_milik[]"
                            value="${text_fasilitas_milik}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_tenor_${row_id}">${text_tenor}</p>
                        <input type="text" class="form-control d-none" id="in_tenor_${row_id}" name="in_tenor[]"
                            value="${text_tenor}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_margin_${row_id}">${text_margin}</p>
                        <input type="text" class="form-control d-none" id="in_margin_${row_id}" name="in_margin[]"
                            value="${text_margin}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_bank_${row_id}">${text_bank}</p>
                        <input type="text" class="form-control d-none" id="in_bank_${row_id}" name="in_bank[]" value="${text_bank}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_plafond_awal_${row_id}">${text_plafond_awal}</p>
                        <input type="text" class="form-control d-none" id="in_plafond_awal_${row_id}" name="in_plafond_awal[]"
                            value="${(formatRupiah(text_plafond_awal)).replaceAll('.', '' )}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_baki_debet_${row_id}">${text_baki_debet}</p>
                        <input type="text" class="form-control d-none" id="in_baki_debet_${row_id}" name="in_baki_debet[]"
                            value="${(formatRupiah(text_baki_debet)).replaceAll('.', '' )}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_angsuran_${row_id}">${text_angsuran}</p>
                        <input type="text" class="form-control d-none" id="in_angsuran_${row_id}" name="in_angsuran[]"
                            value="${(formatRupiah(text_angsuran)).replaceAll('.', '' )}">
                    </td>
                    <td class="text-center align-middle">
                        <p id="te_history_payment_${row_id}">${text_history_payment}</p>
                        <input type="text" class="form-control d-none" id="in_history_payment_${row_id}" name="in_history_payment[]"
                            value="${text_history_payment}">
                    </td>
                    <td class="text-center align-middle">
                        <button type="button" id="btn-edit-hub-${row_id}" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Edit"
                            onclick="editHubLembagaKeuangan(${row_id})">
                            <i class="fa fa-lg fa-edit text-dark"></i>
                        </button>
                        <button type="button" id="btn-delete-hub-${row_id}" class="btn btn-link" data-toggle="tooltip" data-placement="top"
                            title="Hapus" onclick="deleteHubLembagaKeuangan(${row_id})">
                            <span class="fa fa-lg fa-trash text-dark"></span>
                        </button>
                    </td>
                </tr>
            `);
        } else {
            $(`#in_jenis_fasilitas_${number}`).val(text_jenis_fasilitas)
            $(`#te_jenis_fasilitas_${number}`).text(text_jenis_fasilitas)
            
            $(`#in_fasilitas_milik_${number}`).val(text_fasilitas_milik)
            $(`#te_fasilitas_milik_${number}`).text(text_fasilitas_milik)
            
            $(`#in_tenor_${number}`).val(text_tenor)
            $(`#te_tenor_${number}`).text(text_tenor)
            
            $(`#in_margin_${number}`).val(text_margin)
            $(`#te_margin_${number}`).text(text_margin)
            
            $(`#in_bank_${number}`).val(text_bank)
            $(`#te_bank_${number}`).text(text_bank)
            
            $(`#in_plafond_awal_${number}`).val(formatRupiah(text_plafond_awal).replaceAll(".", ""))
            $(`#te_plafond_awal_${number}`).text(formatRupiah(text_plafond_awal, 'Rp'))
            
            $(`#in_baki_debet_${number}`).val((formatRupiah(text_baki_debet)).replaceAll(".", ""))
            $(`#te_baki_debet_${number}`).text(formatRupiah(text_baki_debet, 'Rp'))
            
            $(`#in_angsuran_${number}`).val((formatRupiah(text_angsuran)).replaceAll(".", ""))
            $(`#te_angsuran_${number}`).text(formatRupiah(text_angsuran), 'Rp')
            
            $(`#in_history_payment_${number}`).val(text_history_payment)
            $(`#te_history_payment_${number}`).text(text_history_payment)
        
        }
        
        sumTotalPlafondAwal()
        sumTotalBakiDebet()
        sumTotalAngsuran()
        
        // Change Maks Angsuran
        maksAngsuran()

        // Add Save Button Value
        $('#btn-save-hub').val('1').text('Simpan')
        
        $('#modal_hub_lembaga_keuangan').modal('hide')
        $('#btn_close_hub').trigger('click')
        
        //Clear Value Field
        clearValueField()
    }
    
    const editHubLembagaKeuangan = (number) => {
        let in_number_row       = $(`#in_num_${number}`).val()
        let in_jenis_fasilitas  = $(`#in_jenis_fasilitas_${number}`).val()
        let in_fasilitas_milik  = $(`#in_fasilitas_milik_${number}`).val()
        let in_tenor            = $(`#in_tenor_${number}`).val()
        let in_margin           = $(`#in_margin_${number}`).val()
        let in_bank             = $(`#in_bank_${number}`).val()
        let in_plafond_awal     = $(`#in_plafond_awal_${number}`).val()
        let in_baki_debet       = $(`#in_baki_debet_${number}`).val()
        let in_angsuran         = $(`#in_angsuran_${number}`).val()
        let in_history_payment  = $(`#in_history_payment_${number}`).val()

        $('#text_number_row').val(number)
        $('#text_jenis_fasilitas').val(in_jenis_fasilitas)
        $('#text_fasilitas_milik').val(in_fasilitas_milik)
        $('#text_tenor').val(in_tenor)
        $('#text_margin').val(in_margin)
        $('#text_bank').val(in_bank)
        $('#text_plafond_awal').val(formatRupiah(in_plafond_awal, 'Rp'))
        $('#text_baki_debet').val(formatRupiah(in_baki_debet, 'Rp'))
        $('#text_angsuran').val(formatRupiah(in_angsuran, 'Rp'))
        $('#text_history_payment').val(in_history_payment)
        
        // Add Edit Button Value
        $('#btn-save-hub').val('2').text('Edit')
        
        $('#btn-add-hub').click()
        // $('#modal_hub_lembaga_keuangan').modal('show')
    }

    const deleteHubLembagaKeuangan = (row, id) => {
        if (id) {
            $('#deleted-data-hub').append(`
                <input type="hidden" name="deleted_data_hub[]" value="${id}">
            `)   
        }
    
        $(`#tr-${row}`).remove();

        sumTotalPlafondAwal()
        sumTotalBakiDebet()
        sumTotalAngsuran()
        incrementRowDeviasi()

        maksAngsuran()
    }
    
    const sumTotalPlafondAwal = () => {
        let total_plafond_awal = 0
        let values = $("[name='in_plafond_awal[]']").map(function() {
            thisValue           = $(this).val() == '' ? 0 : $(this).val()
            thisValueToInt      = parseInt(removeAllCharExcNum(thisValue))
            total_plafond_awal += thisValueToInt
        }).get();
        
        $('#total_plafond_awal').text(formatRupiah(total_plafond_awal.toString(), 'Rp'))
    }
    
    const sumTotalBakiDebet = () => {
        let total_baki_debet = 0
        let values = $("[name='in_baki_debet[]']").map(function() {
            thisValue           = $(this).val() == '' ? 0 : $(this).val()
            thisValueToInt      = parseInt(removeAllCharExcNum(thisValue))
            total_baki_debet   += thisValueToInt
        }).get();
        $('#total_baki_debet').text(formatRupiah(total_baki_debet.toString(), 'Rp'))
    }
    
    const sumTotalAngsuran = () => {
        let total_angsuran_payment = 0
        let values = $("[name='in_angsuran[]']").map(function() {
            thisValue               = $(this).val() == '' ? 0 : $(this).val()
            thisValueToInt          = parseInt(removeAllCharExcNum(thisValue))
            total_angsuran_payment += thisValueToInt
        }).get();
        $('#total_angsuran').text(formatRupiah(total_angsuran_payment.toString(), 'Rp'))
    }

    const averageIncome = () => {
        let gaji_pokok_1        = parseInt(removeAllCharExcNum($('#gaji_pokok_1').val() ? $('#gaji_pokok_1').val() : 0))
        let gaji_pokok_2        = parseInt(removeAllCharExcNum($('#gaji_pokok_2').val() ? $('#gaji_pokok_2').val() : 0))
        let gaji_pokok_3        = parseInt(removeAllCharExcNum($('#gaji_pokok_3').val() ? $('#gaji_pokok_3').val() : 0))
        let tunjangan_1         = parseInt(removeAllCharExcNum($('#tunjangan_1').val() ? $('#tunjangan_1').val() : 0))
        let tunjangan_2         = parseInt(removeAllCharExcNum($('#tunjangan_2').val() ? $('#tunjangan_2').val() : 0))
        let tunjangan_3         = parseInt(removeAllCharExcNum($('#tunjangan_3').val() ? $('#tunjangan_3').val() : 0))
        let thp_1               = parseInt(removeAllCharExcNum($('#thp_1').val() ? $('#thp_1').val() : 0))
        let thp_2               = parseInt(removeAllCharExcNum($('#thp_2').val() ? $('#thp_2').val() : 0))
        let thp_3               = parseInt(removeAllCharExcNum($('#thp_3').val() ? $('#thp_3').val() : 0))

        let gaji_pokok_rata2    = Math.round(averageNumber([gaji_pokok_1, gaji_pokok_2, gaji_pokok_3]))
        let tunjangan_rata2     = Math.round(averageNumber([tunjangan_1, tunjangan_2, tunjangan_3]))
        let thp_rata2           = Math.round(averageNumber([thp_1, thp_2, thp_3]))

        $('#gaji_pokok_rata2').val(formatRupiah(gaji_pokok_rata2.toString(), 'Rp'))
        $('#tunjangan_rata2').val(formatRupiah(tunjangan_rata2.toString(), 'Rp'))

        // Total Pendapatan
        if (thp_rata2) {
            $('#thp_rata2').val(formatRupiah(thp_rata2.toString(), 'Rp'))
        } else {
            $('#thp_rata2').val('Rp0')
            $('#text_total_pendapatan').val('Rp0')
        }
        totalPendapatan()
        maksAngsuran()

    }

    const averageIncomePasangan = () => {
        let gaji_pokok_1_pasangan       = parseInt(removeAllCharExcNum($('#gaji_pokok_1_pasangan').val() ? $('#gaji_pokok_1_pasangan').val() : 0))
        let gaji_pokok_2_pasangan       = parseInt(removeAllCharExcNum($('#gaji_pokok_2_pasangan').val() ? $('#gaji_pokok_2_pasangan').val() : 0))
        let gaji_pokok_3_pasangan       = parseInt(removeAllCharExcNum($('#gaji_pokok_3_pasangan').val() ? $('#gaji_pokok_3_pasangan').val() : 0))
        let tunjangan_1_pasangan        = parseInt(removeAllCharExcNum($('#tunjangan_1_pasangan').val() ? $('#tunjangan_1_pasangan').val() : 0))
        let tunjangan_2_pasangan        = parseInt(removeAllCharExcNum($('#tunjangan_2_pasangan').val() ? $('#tunjangan_2_pasangan').val() : 0))
        let tunjangan_3_pasangan        = parseInt(removeAllCharExcNum($('#tunjangan_3_pasangan').val() ? $('#tunjangan_3_pasangan').val() : 0))
        let thp_1_pasangan              = parseInt(removeAllCharExcNum($('#thp_1_pasangan').val() ? $('#thp_1_pasangan').val() : 0))
        let thp_2_pasangan              = parseInt(removeAllCharExcNum($('#thp_2_pasangan').val() ? $('#thp_2_pasangan').val() : 0))
        let thp_3_pasangan              = parseInt(removeAllCharExcNum($('#thp_3_pasangan').val() ? $('#thp_3_pasangan').val() : 0))

        let gaji_pokok_rata2_pasangan   = Math.round(averageNumber([gaji_pokok_1_pasangan, gaji_pokok_2_pasangan, gaji_pokok_3_pasangan]))
        let tunjangan_rata2_pasangan    = Math.round(averageNumber([tunjangan_1_pasangan, tunjangan_2_pasangan, tunjangan_3_pasangan]))
        let thp_rata2_pasangan          = Math.round(averageNumber([thp_1_pasangan, thp_2_pasangan, thp_3_pasangan]))
        
        $('#gaji_pokok_rata2_pasangan').val(formatRupiah(gaji_pokok_rata2_pasangan.toString(), 'Rp'))
        $('#tunjangan_rata2_pasangan').val(formatRupiah(tunjangan_rata2_pasangan.toString(), 'Rp'))

        // Total Pendapatan
        if (thp_rata2_pasangan) {
            $('#thp_rata2_pasangan').val(formatRupiah(thp_rata2_pasangan.toString(), 'Rp'))
        } else {
            $('#thp_rata2_pasangan').val('Rp0')
            $('#text_total_pendapatan').val('Rp0')
        }
        totalPendapatan()
        maksAngsuran()
    }

    const totalPendapatan = () => {
        let skema_pembiayaan = "{!! $data_brw ? $data_brw->skema_pembiayaan : '' !!}"
        let thp_rata2 = $('#thp_rata2').val() ? removeAllCharExcNum($('#thp_rata2').val()) : 0
        let thp_rata2_pasangan = skema_pembiayaan == '2' && $('#thp_rata2_pasangan').val() ? removeAllCharExcNum($('#thp_rata2_pasangan').val()) : 0
        let total_rata2_thp = parseInt(thp_rata2) + parseInt(thp_rata2_pasangan)

        $('#text_total_pendapatan').val(formatRupiah((total_rata2_thp).toString(), 'Rp'))

    }

    const calThp = (gaji_pokok_id, tunjangan_id, thp_id) => {
        let gaji_pokok_value = $('#'+gaji_pokok_id).val() ? removeAllCharExcNum($('#'+gaji_pokok_id).val()) : '0'
        let tunjangan_value = $('#'+tunjangan_id).val() ? removeAllCharExcNum($('#'+tunjangan_id).val()) : '0'

        let gaji_pokok   = parseInt(gaji_pokok_value)
        let tunjangan    = parseInt(tunjangan_value)
        let thp          = gaji_pokok + tunjangan

        $('#'+thp_id).val(formatRupiah((thp).toString(), 'Rp'))
    }
    
    const calculateAngsuran = () => {

        // let n = 12
        // let tenor_dalam_tahun = $('#text_jangka_waktu').val() ? ($('#text_jangka_waktu').val() / 12) : 0
        // let PMT = 0
        
        // PMT = (plafon_rekomendasi * (margin/n)) / (1 - (Math.pow(1 + margin / n, -n * tenor_dalam_tahun)))
        // $('#text_angsuran_2').text(formatRupiah(Math.round(PMT).toString(), 'Rp'))
        // $("[name='text_angsuran_2']").val(Math.round(PMT)) 
        
        let plafon_rekomendasi = $('#input_plafon_rekomendasi').val() ? removeAllCharExcNum($('#input_plafon_rekomendasi').val()) : 0
        let margin = $('#text_margin_pendanaan').val() ? $('#text_margin_pendanaan').val() : 0
        let tenor = $('#text_jangka_waktu').val() ? $('#text_jangka_waktu').val() : 0

        $.ajax({
            type: "get",
            data: { "margin": margin, "tenor": tenor, "plafond_rekomendasi": plafon_rekomendasi },
            dataType: "json",
            url: '/admin/analis/total-angsuran',
            success: function (res) {
                if (res) {
                    $('#text_angsuran_2').text(formatRupiah(res.total_angsuran, 'Rp'))
                    $("[name='text_angsuran_2']").val(res.total_angsuran);  
                }else{
                    $("[name='text_angsuran_2']").val("");
                }
            }

        });

            
    }
    
    const calculatePlafonCR = () => {
        let maks_angsuran = removeAllCharExcNum($('#text_angsuran_setelah').val())
        let margin = $('#text_margin_pendanaan').val() ? ($('#text_margin_pendanaan').val() * (1/100)) : $('#text_margin_pendanaan').val()
        let n = 12
        let tenor_dalam_tahun = $('#text_jangka_waktu').val() / 12
        let angsuran = $("[name='text_angsuran_2']").val()
        let PV = 0

        // if (maks_angsuran > 0 && margin > 0 && tenor_dalam_tahun && angsuran > 0) {
        if (maks_angsuran && margin > 0 && tenor_dalam_tahun) {
            // PV = angsuran - ((1 - (Math.pow((1 + margin / n), (-n * tenor_dalam_tahun)))) / (margin / n)) * maks_angsuran
            PV = ((1 - (Math.pow((1 + margin / n), (-n * tenor_dalam_tahun)))) / (margin / n)) * maks_angsuran
            is_pv_negative = PV < 0 ? '-' : ''
            
            $('#text_maks_plafon_cr').text(is_pv_negative+formatRupiah(Math.round(PV).toString(), 'Rp'))
            $("[name='text_maks_plafon_cr']").val(Math.round(PV))
        }
    } 

    const maksAngsuran = () => {
        let total_pendapatan                    = removeAllCharExcNum($('#text_total_pendapatan').val())
        let cash_ratio                          = $('#text_cash_ratio').val()
        let total_angsuran                      = removeAllCharExcNum($('#total_angsuran').text())
        let maks_angsuran_sebelum               = parseInt(total_pendapatan * (cash_ratio / 100))
        let maks_angsuran_setelah               = parseInt(maks_angsuran_sebelum - total_angsuran)
        let is_maks_angsuran_setelah_negative   = maks_angsuran_setelah < 0 ? '-' : ''
        if (total_pendapatan && cash_ratio && total_angsuran) {
            console.log(total_pendapatan,cash_ratio,total_angsuran)
            $('#text_angsuran_sebelum').val(formatRupiah(maks_angsuran_sebelum.toString(), 'Rp'))
            $('#text_angsuran_setelah').val(is_maks_angsuran_setelah_negative + formatRupiah(maks_angsuran_setelah.toString(), 'Rp'))

            calculatePlafonCR()
        } else {
            $('#text_angsuran_sebelum').val('')
            $('#text_angsuran_setelah').val('')
        }
    } 

    const maksFTVRp = () => {
        let nilai_pasar_wajar = removeAllCharExcNum($('#text_nilai_pasar_wajar').val())
        let nilai_ftv_percent = $('#text_maks_ftv_percent').val() ? $('#text_maks_ftv_percent').val() : 0
        let nilai_ftv_rp      = 0

        nilai_ftv_rp = nilai_pasar_wajar * (nilai_ftv_percent / 100)
        console.log(nilai_ftv_rp)
        $('#text_maks_ftv_nominal').val(formatRupiah(nilai_ftv_rp.toString(), 'Rp'))
        $('#text_maks_ftv_nominal_2').text(formatRupiah(nilai_ftv_rp.toString(), 'Rp'))
    }

    const incrementRowDeviasi = () => {
        row_number -= 1

        let total_row = $('p[name="te_num[]"]').length
        let row = $('p[name="te_num[]"]')
        for (let i = 1; i <= total_row; i++) { 
            $(row[i-1]).text(i)
        }
    }
</script>
{{-- END: Other Function --}}
@endpush