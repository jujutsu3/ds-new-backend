@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('style')
<style>
    .btn-danaSyariah {
        background-color: #16793E;
    }

    input[type=checkbox] {
        top: 1rem;
        width: 1rem;
        height: 1rem;
    }
</style>
@endsection

@section('content')

{{-- START: Content Title --}}
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Analisa Pendanaan</h1>
            </div>
        </div>
    </div>
</div>
{{-- END: Content Title --}}

<div class="content mt-3">
    <div class="container-fluid">

        <div class="row mt-2">
            <div class="col-md-12">
                <button type="button" data-toggle="modal" data-target="#informasi_brw"
                    class="btn btn-info mb-2 text-white float-right rounded">Informasi Penerima Pendanaan</button>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card" style="background-color: #F1F2F7">
                    <h6 class="pt-0 ml-4 pl-1 mb-n4"
                        style="margin-top: -0.7rem; width: 180px; background-color: #F1F2F7">Daftar Pekerjaan Analis
                    </h6>
                    <div class="card-body pl-4">
                        <div class="table-responsive px-1">
                            <table id="table_list_pekerjaan" class="table table-striped table-bordered bg-white">
                                <thead class="align-middle text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>Keterangan</th>
                                        <th>Terakhir diperbarui </th>
                                        <th>aksi</th>
                                    </tr>
                                </thead>
                                <tbody class="align-middle">
                                    <tr>
                                        <th class="text-center" scope="row">1</th>
                                        <td class="text-left">RAC</td>
                                        <td class="text-center">
                                            @if ($data->updated_at_rac_dsi)
                                                {{ $data->updated_at_rac_dsi ? date('d-m-Y H:i:s', strtotime($data->updated_at_rac_dsi)) : '-' }}
                                            @else
                                                {{ $data->created_at_rac_dsi ? date('d-m-Y H:i:s', strtotime($data->created_at_rac_dsi)) : '-' }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (!empty($data->flag_rac_dsi) && $data->flag_rac_dsi == '1')
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '1']) }}"
                                                class="btn btn-primary btn-block">Ubah</a>
                                            @elseif (!empty($data->flag_rac_dsi) && $data->flag_rac_dsi == '2')
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '1']) }}"
                                                class="btn btn-success btn-block">Lihat</a>
                                            @else
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '1']) }}"
                                                class="btn btn-secondary btn-block">Baru</a>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-center" scope="row">2</th>
                                        <td class="text-left">Lembar Perhitungan</td>
                                        <td class="text-center">
                                            @if ($data->updated_at_lembar_dsi)
                                                {{ $data->updated_at_lembar_dsi ? date('d-m-Y H:i:s', strtotime($data->updated_at_lembar_dsi)) : '-' }}
                                            @else
                                                {{ $data->created_at_lembar_dsi ? date('d-m-Y H:i:s', strtotime($data->created_at_lembar_dsi)) : '-' }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (!empty($data->flag_lembar_dsi) && $data->flag_lembar_dsi == '1')
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '2']) }}"
                                                class="btn btn-primary btn-block">Ubah</a>
                                            @elseif (!empty($data->flag_lembar_dsi) && $data->flag_lembar_dsi == '2')
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '2']) }}"
                                                class="btn btn-success btn-block">Lihat</a>
                                            @else
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '2']) }}"
                                                class="btn btn-secondary btn-block">Baru</a>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-center" scope="row">3</th>
                                        <td class="text-left">Resume Akad {{ $data ? $data->jenis_akad : ''}}</td>
                                        <td class="text-center">
                                            @if ($data->updated_at_resume_dsi)
                                                {{ $data->updated_at_resume_dsi ? date('d-m-Y H:i:s', strtotime($data->updated_at_resume_dsi)) : '-' }}
                                            @else
                                                {{ $data->created_at_resume_dsi ? date('d-m-Y H:i:s', strtotime($data->created_at_resume_dsi)) : '-' }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if (!empty($data->flag_resume_dsi) && $data->flag_resume_dsi == '1')
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '3']) }}"
                                                class="btn btn-primary btn-block">Ubah</a>
                                            @elseif (!empty($data->flag_resume_dsi) && $data->flag_resume_dsi == '2')
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '3']) }}"
                                                class="btn btn-success btn-block">Lihat</a>
                                            @else
                                            <a href="{{ route('analis.detail-pekerjaan', ['pengajuan_id' => $pengajuan_id ,'keterangan' => '3']) }}"
                                                class="btn btn-secondary btn-block">Baru</a>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            @if (empty($data) || !empty($data) && $data->status_analis != '6')
                            <div class="form-check form-check-inline mt-3">
                                <input class="form-check-input" type="checkbox" id="chk_agree">
                                <label class="form-check-label" for="chk_agree">Hasil kerja yang dikirim adalah yang
                                    sebenarnya dan dapat dipertanggung jawabkan</label>
                            </div>
                            @endif
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <a href="{!! route('analis.detail-pendanaan', ['pengajuan_id' => $pengajuan_id]) !!}" class="btn btn-danaSyariah text-white float-left rounded">Kembali</a>
                        @if (empty($data) || !empty($data) && $data->status_analis != '6')
                        <a href="{{ route('analis.pekerjaan-analis-update', ['pengajuan_id' => $pengajuan_id]) }}"
                            id="btn_submit" class="btn btn-danaSyariah text-white float-right rounded disabled"
                            disabled>Kirim</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

{{-- START: Modal Informasi Borrower --}}
@component('component.admin.modal_detail_borrower')
    @slot('modal_id', 'informasi_brw')
    @slot('brw_nama', $data->nama)
    @slot('brw_nik', $data->ktp)
    @slot('brw_jns_kelamin', $data->jns_kelamin)
    @slot('brw_pic', $data->brw_pic)
@endcomponent
{{-- END: Modal Informasi Borrower --}}

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(document).ready(function(){

        let rac_updated_at = '{!! !empty($data->created_at_rac_dsi) ? 1 : 0!!}'
        let lembar_updated_at = '{!! !empty($data->created_at_lembar_dsi) ? 1 : 0!!}'
        let resume_updated_at = '{!! !empty($data->created_at_resume_dsi) ? 1 : 0!!}'

        if (rac_updated_at == 1 && lembar_updated_at == 1 && resume_updated_at == 1) {
            $('#chk_agree').attr('disabled', false)
        } else {
            $('#chk_agree').attr('disabled', true)
        }
        // Agreement
        $('#chk_agree').change(() => {
            let rac_updated_at = '{!! !empty($data->created_at_rac_dsi) ? 1 : 0!!}'
            let lembar_updated_at = '{!! !empty($data->created_at_lembar_dsi) ? 1 : 0!!}'
            let resume_updated_at = '{!! !empty($data->created_at_resume_dsi) ? 1 : 0!!}'

            if ($('#chk_agree').is(":checked")) {
                $('#btn_submit').removeClass('disabled')
            } else {
                $('#btn_submit').addClass('disabled')
            }
        })

        $('#btn_submit').click(function(e) {
            e.preventDefault()
            $.ajax({
                url : $(this).attr('href'),
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function() {
                    swal.fire({
                        html: '<h5>Menyimpan Data...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function (response) {
                    if (response.status == 'success') {
                        swal.fire({
                            title: 'Berhasil',
                            type: 'success',
                            text: response.msg,
                            allowOutsideClick: () => false,
                        }).then(function (result) {
                            if (result.value) {
                                window.location = "{{ route('analis.list-pendanaan') }}"
                            }
                        })
                    } else {
                        swal.fire({
                            title: 'Oops',
                            type: 'error',
                            text: response.msg
                        })
                        console.log(response)
                    }
                },
                error: function(response) {
                    swal.fire({
                        title: 'Error',
                        type: 'error',
                        text: response.msg
                    })
                    console.log(response)
                }
            })
        })
    });

</script>
@endsection