@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('style')

@endsection

@section('content')

{{-- START: Content Title --}}
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Analisa Pendanaan</h1>
            </div>
        </div>
    </div>
</div>
{{-- END: Content Title --}}

<div class="content mt-3">
    <div class="container-fluid">

        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card" style="background-color: #F1F2F7">
                    <h6 class="pt-0 ml-4 pl-1 mb-n4"
                        style="margin-top: -0.7rem; width: 137px; background-color: #F1F2F7">Daftar Pendanaan </h6>
                    <div class="card-body">
                        <h6 class="pt-0 ml-4 pl-1 mb-n4 mt-0" style="margin-bottom: -5rem;"></h6>
                        <div class="table-responsive">
                            <table id="table_list_pendanaan" class="table table-striped table-bordered bg-white">
                                <thead class="align-middle text-center">
                                    <tr>
                                        <th>No</th>
                                        <th>ID Pengajuan</th>
                                        <th>Tanggal Pengajuan</th>
                                        <th>Tanggal Verifikasi </th>
                                        <th>Penerima Pendanaan</th>
                                        <th>Jenis Pendanaan</th>
                                        <th>Tujuan Pendanaan</th>
                                        <th>Nilai Pengajuan Pendanaan</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody class="align-middle text-center">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('admin/assets/js/lib/data-table/datetime.js') }}"></script>
<script src="{{ asset('admin/assets/js/lib/data-table/moment.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function(){

        let listPendanaan =  $('#table_list_pendanaan').DataTable({
            // "dom": '<f<"mt-4 pt-4"t>ip>'
            dom: '<f<"mt-4 pt-4"tr>ip>',
            searching: true,
            processing: true,
            // responsive: true,
            serverSide: false,
            // autoWidth: true,
            paging: true,
            info: false,
            pageLength: 5,
            order: [[ 2, "desc" ]],
            ajax: {
                url: '{{ route("analis.get-json-list-pendanaan") }}',
            },
            columnDefs:[
                { targets: [2, 3], render: $.fn.dataTable.render.moment( 'DD-MM-YYYY' ) },
                {   
                    "targets": [1],
                    "visible": false,
                    "searchable": false
                },
            ],
            columns: [
                {
                    data: 'pengajuan_id',
                    render: function (data, type, row, meta) {
                        return meta.row + 1;
                    }
                },
                {data: 'pengajuan_id', name: 'pengajuan_id'},
                {data: 'tanggal_pengajuan', name: 'tanggal_pengajuan'},
                {data: 'tanggal_verifikasi', name: 'tanggal_verifikasi'},
                {data: 'penerima_pendanaan', name: 'penerima_pendanaan'},
                {data: 'jenis_pendanaan', name: 'jenis_pendanaan'},
                {data: 'tujuan_pendanaan', name: 'tujuan_pendanaan'},
                {
                    data: 'nilai_pengajuan_pendanaan',
                    render: $.fn.dataTable.render.number('.', '.', 2, 'Rp')
                
                },
                {data: 'status', name: 'status', orderable:true, serachable:true, sClass:'text-center'},
            ],
        });

        $('#table_list_pendanaan').find('th').addClass('align-middle text-center')

    });

    // START: Other Function
    const formatNumber = (num) => {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    };
    // END: Other Function

</script>

@endsection