@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('style')
<style>
    .btn-danaSyariah {
        background-color: #16793E;
    }
    /* body {
        background-color: white;
    }

    .card {
        border-color: #000000;
        margin-bottom: 60px
    } */

    .box-title {
        margin-left: 8px;
        margin-top: -0.9rem;
        background-color: #F1F2F7;
        /* background-color: #FFFFFF; */
        width: -moz-fit-content;
        width: fit-content;
    }
</style>
@endsection

@section('content')

{{-- START: Content Title --}}
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Analisa Pendanaan</h1>
            </div>
        </div>
    </div>
</div>
{{-- END: Content Title --}}

<div class="content mt-3">
    <div class="container-fluid">

        <div class="row mt-5">
            <div id="layout-informasi-pendanaan" class="col-md-12">

                <div class="card">
                    <div class="box-title">
                        <label class="form-check-label text-black h6 text-bold pl-2 font-weight-bold">Informasi Objek Pendanaan &nbsp;</label>
                    </div>
                    <div class="card-body pt-4 pl-4">
                        {{-- START: Baris 1 --}}
                        <div class="row mt-4">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Tipe Pendanaan</label>
                                <p id="text_tipe_pendanaan">
                                    {{ !empty($data->pendanaan_nama) ? $data->pendanaan_nama : '-'}}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Tujuan Pendanaan</label>
                                <p id="text_tujuan_pendanaan">
                                    {{ !empty($data->pendanaan_tujuan) ? $data->tujuan_pembiayaan : '-'}}</p>
                            </div>
                        </div>
                        {{-- END: Baris 1 --}}

                        {{-- START: Baris 2 --}}
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Alamat Objek Pendanaan</label>
                                <p id="text_alamat_pendanaan">
                                    {{ !empty($data->lokasi_proyek) ? $data->lokasi_proyek : '-'}}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Provinsi</label>
                                <p id="text_provinsi_pendanaan">{{ !empty($data->provinsi) ? $data->provinsi : '-' }}
                                </p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kota</label>
                                <p id="text_kota_pendanaan">{{ !empty($data->kota) ? $data->kota : '-' }}</p>
                            </div>
                        </div>
                        {{-- END: Baris 2 --}}

                        {{-- START: Baris 3 --}}
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kecamatan</label>
                                <p id="text_kecamatan_pendanaan">{{ !empty($data->kecamatan) ? $data->kecamatan : '-' }}
                                </p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kelurahan</label>
                                <p id="text_kelurahan_pendanaan">{{ !empty($data->kelurahan) ? $data->kelurahan : '-' }}
                                </p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kode Pos</label>
                                <p id="text_kode_pos_pendanaan">{{ !empty($data->kode_pos) ? $data->kode_pos : '-' }}
                                </p>
                            </div>
                        </div>
                        {{-- END: Baris 3 --}}

                        {{-- START: Baris 4 --}}
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Harga Objek Pendanaan</label>
                                <p id="text_harga_objek_pendanaan">
                                    {{ !empty($data->harga_objek_pendanaan) ? 'Rp' .str_replace(',', '.', number_format($data->harga_objek_pendanaan)) : '-' }}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Uang Muka</label>
                                <p id="text_uang_muka_pendanaan">{{ !empty($data->uang_muka) ? 'Rp' .str_replace(',', '.', number_format($data->uang_muka)) : '-' }}
                                </p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Nilai Pengajuan Pendanaan</label>
                                <p id="text_nilai_pengajuan_pendanaan">
                                    {{ !empty($data->pendanaan_dana_dibutuhkan) ? 'Rp' .str_replace(',', '.', number_format($data->pendanaan_dana_dibutuhkan)) : '-' }}
                                </p>
                            </div>
                        </div>
                        {{-- END: Baris 4 --}}

                        {{-- START: Baris 5 --}}
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Janka Waktu (Bulan)</label>
                                <p id="text_jangka_waktu_pendanaan">
                                    {{ !empty($data->durasi_proyek) ? $data->durasi_proyek : '-' }}</p>
                            </div>
                        </div>
                        {{-- END: Baris 5 --}}

                        {{-- START: Baris 6 --}}
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <label class="font-weight-normal">Detail Informasi Objek Pendanaan</label>
                                @php
                                $detail_pendanaan = !empty($data->detail_pendanaan) ? $data->detail_pendanaan : '-';
                                $text_summary = substr($detail_pendanaan,0,600);
                                @endphp

                                @if(strlen($detail_pendanaan) > 600)
                                <p id="text_detail_informasi_pendanaan">
                                    {{ $text_summary }} <a href="#" id="more-text-pendanaan" class="text-primary"
                                        onclick="fullText(); return false">selengkapnya...</a>
                                </p>
                                @else
                                <p>
                                    {{ $detail_pendanaan }}
                                </p>
                                @endif
                            </div>
                        </div>
                        {{-- END: Baris 6 --}}
                    </div>
                </div>
            </div>

        </div>

        <div class="row mt-5">
            <div id="layout-informasi-pemilik-pendanaan" class="col-md-12">
                <div class="card">
                    <div class="box-title">
                        <label class="form-check-label text-black h6 text-bold pl-2 font-weight-bold">Informasi Pemilik Objek Pendanaan &nbsp;</label>
                    </div>
                    <div class="card-body pt-4 pl-4">
                        {{-- START: Baris 1 --}}
                        <div class="row mt-4">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Nama</label>
                                <p id="text_nama_pendana">{{ !empty($data->nm_pemilik) ? $data->nm_pemilik : '-' }}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">No. Telp/HP Pendanaan</label>
                                <p id="text_no_tlp_pendana">
                                    {{ !empty($data->no_tlp_pemilik) ? $data->no_tlp_pemilik : '-' }}</p>
                            </div>
                        </div>
                        {{-- END: Baris 1 --}}

                        {{-- START: Baris 2 --}}
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Alamat Objek Pendanaan</label>
                                <p id="text_alamat_pendana">
                                    {{ !empty($data->alamat_pemilik) ? $data->alamat_pemilik : '-' }}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Provinsi</label>
                                <p id="text_provinsi_pendana">
                                    {{ !empty($data->provinsi_pemilik) ? $data->provinsi_pemilik : '-' }}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kota</label>
                                <p id="text_kota_pendana">{{ !empty($data->kota_pemilik) ? $data->kota_pemilik : '-' }}
                                </p>
                            </div>
                        </div>
                        {{-- END: Baris 2 --}}

                        {{-- START: Baris 3 --}}
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kecamatan</label>
                                <p id="text_kecamatan_pendana">
                                    {{ !empty($data->kecamatan_pemilik) ? $data->kecamatan_pemilik : '-' }}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kelurahan</label>
                                <p id="text_kelurahan_pendana">
                                    {{ !empty($data->kelurahan_pemilik) ? $data->kelurahan_pemilik : '-' }}</p>
                            </div>
                            <div class="col-md-4">
                                <label class="font-weight-normal">Kode Pos</label>
                                <p id="text_kode_pos_pendana">
                                    {{ !empty($data->kd_pos_pemilik) ? $data->kd_pos_pemilik : '-' }}</p>
                            </div>
                        </div>
                        {{-- END: Baris 3 --}}
                    </div>
                </div>
            </div>
        </div>

        <div class="row mb-4">
            <div class="col-md-12">
                <a href="{!! route('analis.list-pendanaan') !!}"
                    class="btn btn-danaSyariah text-white float-left rounded">Kembali</a>
                <a href="{!! route('analis.daftar-pekerjaan', ['id_pengajuan' => $data->pengajuan_id]) !!}"
                    class="btn btn-danaSyariah text-white float-right rounded">Lanjutkan</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let full_text = `{!! $detail_pendanaan !!}`
    let text_summary = `{!! substr($detail_pendanaan,0,600) !!}`

    const fullText = () => {
        $('#text_detail_informasi_pendanaan').html(`
            ${full_text} <a href="" id="summary-text-pendanaan" class="text-primary" onclick="summaryText(); return false">lihat lebih sedikit...</a>
        `)
    }
    const summaryText = () => {
        $('#text_detail_informasi_pendanaan').html(`
            ${text_summary} <a href="" id="more-text-pendanaan" class="text-primary" onclick="fullText(); return false">selengkapnya...</a>
        `)
    } 
</script>
@endsection