{{-- START: Dokumen Legalitas --}}
<div class="row mt-3">
    <div id="layout-informasi-doc-legalitas" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Dokumen Legalitas &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_doc_legalitas" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 55%">Jenis Kriteria Resiko</th>
                                <th class="align-middle">Dokumen Pendukung</th>
                                <th class="align-middle" style="width: 5%">Ceklis Verifikator</th>
                                <th class="align-middle" style="width: 15%">Hasil Verifikasi</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Dokumen Legalitas --}}

{{-- START: Usia --}}
<div class="row mt-3">
    <div id="layout-informasi-usia" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Usia &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_usia" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 55%">Jenis Kriteria Resiko</th>
                                <th class="align-middle">Dokumen Pendukung</th>
                                <th class="align-middle" style="width: 5%">Ceklis Verifikator</th>
                                <th class="align-middle" style="width: 15%">Hasil Verifikator</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Usia --}}

{{-- START: Pekerjaan --}}
<div class="row mt-3">
    <div id="layout-informasi-pekerjaan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Pekerjaan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_pekerjaan" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 55%">Jenis Kriteria Resiko</th>
                                <th class="align-middle">Dokumen Pendukung</th>
                                <th class="align-middle" style="width: 5%">Ceklis Verifikator</th>
                                <th class="align-middle" style="width: 15%">Hasil Verifikator</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Pekerjaan --}}

{{-- START: Verifikasi --}}
<div class="row mt-3">
    <div id="layout-informasi-verifikasi" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Verifikasi &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_verifikasi" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 55%">Jenis Kriteria Resiko</th>
                                <th class="align-middle">Nilai Kelayakan</th>
                                <th class="align-middle" style="width: 5%">Ceklis Verifikator</th>
                                <th class="align-middle" style="width: 15%">Hasil Verifikator</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Verifikasi --}}

{{-- START: Pendapatan --}}
<div class="row mt-3">
    <div id="layout-informasi-pendapatan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Pendapatan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_pendapatan" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 55%">Jenis Kriteria Resiko</th>
                                <th class="align-middle">Dokumen Pendukung</th>
                                <th class="align-middle" style="width: 5%">Ceklis Verifikator</th>
                                <th class="align-middle" style="width: 15%">Hasil Verifikator</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Pendapatan --}}

{{-- START: Jaminan --}}
<div class="row mt-3">
    <div id="layout-informasi-jaminan" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Jaminan &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_jaminan" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 55%">Jenis Kriteria Resiko</th>
                                <th class="align-middle">Dokumen Pendukung</th>
                                <th class="align-middle" style="width: 5%">Ceklis Verifikator</th>
                                <th class="align-middle" style="width: 15%">Hasil Verifikator</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Jaminan --}}

{{-- START: Cash Rasio --}}
<div class="row mt-3">
    <div id="layout-informasi-cash-rasio" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp Cash Rasio (Pilih Yang Sesuai) &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_cash_rasio" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 75%">Jenis Kriteria Resiko</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Cash Rasio --}}

{{-- START: FTV --}}
<div class="row mt-3">
    <div id="layout-informasi-ftv" class="col-md-12">
        <div class="card">
            <div class="box-title">
                <label class="form-check-label text-black h6 text-bold pl-2">&nbsp FTV (Pilih Yang Sesuai) &nbsp</label>
            </div>
            <div class="card-body pt-4 pl-4">
                <div class="table-responsive px-1">
                    <table id="table_ftv" class="table table-striped table-bordered">
                        <thead class="text-center">
                            <tr>
                                <th class="align-middle" style="width: 5%">No</th>
                                <th class="align-middle" style="width: 75%">Jenis Kriteria Resiko</th>
                                <th class="align-middle" style="width: 5%">Ceklis Analis</th>
                                <th class="align-middle" style="width: 15%">Hasil Analis</th>
                            </tr>
                        </thead>
                        <tbody class="align-middle">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: FTV --}}

{{-- START: Modal View Doc --}}
<div class="modal fade bd-example-modal-lg" id="modal-doc-pendukung" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Lihat File</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="preview">
                    <p id="text-status" class="text-center">Mengambil Data..</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" id="btnUploadUlang" class="btn btn-secondary text-left" data-toggle="modal"
                    data-dismiss="modal">
                    Tutup </button>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal View Doc --}}

{{-- START: Modal View Verifikasi --}}
<div class="modal fade bd-example-modal-lg" id="modal-doc-verifikasi" tabindex="-1" role="dialog"
    aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Nilai Kelayakan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="preview-nilai">
                    <p id="text-status" class="text-center">Mengambil Data..</p>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary text-left" data-toggle="modal" data-dismiss="modal">
                    Tutup </button>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal View Verifikasi --}}

@include('layouts.modals.modal-verijelas')

@push('scripts')

<script>
    const showHasilVerif = (chk_doc_id, hasil_verif_id) => {
        if ($(`#${chk_doc_id}`).is(":checked")) {
            $('#'+hasil_verif_id).removeClass('d-none')
        } else {
            $('#'+hasil_verif_id).addClass('d-none')
        }
    } 

    const data_chk_analisa = '{!! $data_chk_analisa !!}'
    const data_chk = data_chk_analisa ? JSON.parse(data_chk_analisa) : []
    const is_single_income = "{!! $data_brw->skema_pembiayaan !!}"
    const status_kawin = "{!! $data_brw->status_kawin !!}"
    const pengajuan_id = `{!! $data_brw->pengajuan_id !!}`
    const brw_id = `{!! $data_brw->brw_id !!}`
    const nama_brw = `{!! $data_brw->nama_penerima_pendana !!}`
    const brw_type = `{!! $data_brw->brw_type !!}`
    // START: Doc Legalitas
    const doc_legalitas = [
        {"no":"1",  "doc_pendukung":"0", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"kartu_keluarga", "jenis_dokumen":"Form Aplikasi Permohonan Pendanaan Dana Rumah"},
        {"no":"2",  "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_user_detail", "field_name":"brw_pic_ktp", "jenis_dokumen":"KTP Penerima Dana"},
        {"no":"3",  "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"ktp_pasangan", "jenis_dokumen":"KTP Pasangan Penerima Dana (Suami/Istri)"},
        {"no":"4",  "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_user_detail", "field_name":"brw_pic_npwp", "jenis_dokumen":"NPWP Penerima Dana"},
        {"no":"6",  "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"kartu_keluarga", "jenis_dokumen":"Kartu Keluarga"},
        {"no":"8",  "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"surat_pemesanan_rumah", "jenis_dokumen":"Surat Pemesanan Rumah(SPR) yang telah di Tandatangani"},
        {"no":"9",  "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"bukti_bayar_uang_muka", "jenis_dokumen":"Bukti Bayar Uang Muka atau Down Payment(DP)/NUP(Printout Rekening Koran/Mutasi Rekening)"},
        {"no":"10", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_beda_nama", "jenis_dokumen":"Surat Keterangan Beda Nama atau Tanggal Lahir antara KTP / KK / Akta Nikah dari Kelurahan (jika berbeda salah satu nya) - Jika Ada"},
        {"no":"11", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"perjanjian_pra_nikah", "jenis_dokumen":"Perjanjian Pra-Nikah Penerima Dana dari Notaris yang di daftarkan ke KUA / catatan sipil (jika menikah dengan WNA atau membuat perjanjian pra nikah atas permintaan sendiri sebelum menikah) - Jika Ada"},

        // {"no":"1", "jenis_dokumen": "Form Aplikasi Permohonan Pendanaan Dana Rumah"}, 
        // {"no":"2", "jenis_dokumen": "KTP Penerima Dana"}, 
        // {"no":"3", "jenis_dokumen": "KTP Pasangan Penerima Dana (Suami/Istri)"},
        // {"no":"4", "jenis_dokumen": "NPWP Penerima Dana"},
        // {"no":"6", "jenis_dokumen": "Kartu Keluarga"},
        // {"no":"7", "jenis_dokumen": "Akta Nikah/Akta Perkawinan (jika Menikah), Akta Cerai/Akta Perceraian (jika Cerai Hidup), Surat Pernyataan Belum Pernah Menikah dari Kelurahan (Jika Belum Menikah), Surat Pernyataan Belum Menikah Lagi dari Kelurahan dan/atau Surat Kematian dari RS / Puskesmas / Kelurahan (Jika Cerai Mati) - Jika Ada"},
        // {"no":"8", "jenis_dokumen": "Surat Pemesanan Rumah (SPR) yang telah di Tandatangani"},
        // {"no":"9", "jenis_dokumen": "Bukti Bayar Uang Muka/Down Payment (DP)/NUP (Print Out Rekening Koran/Mutasi Rekening)"},
        // {"no":"10", "jenis_dokumen": "Surat Keterangan Beda Nama atau Tanggal Lahir antara KTP / KK / Akta Nikah dari Kelurahan (jika berbeda salah satu nya) - Jika Ada"},
        // {"no":"11", "jenis_dokumen": "Perjanjian Pra-Nikah Penerima Dana dari Notaris yang di daftarkan ke KUA / catatan sipil (jika menikah dengan WNA atau membuat perjanjian pra nikah atas permintaan sendiri sebelum menikah) - Jika Ada"},
    ];

    const doc_legalitas_joint_income = [
        {"no":"5", "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"npwp_pasangan", "jenis_dokumen":"NPWP Pasangan (Suami /Istri)"},
        // {"no":"5", "jenis_dokumen": "NPWP Pasangan (Suami/Istri)"},
    ]

    // start: status menikah
    let doc_legalitas_belum_menikah = [
        {"no":"7",  "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_keterangan_belum_menikah", "jenis_dokumen":"Akta Nikah/Akta Perkawinan (jika Menikah) Akta Cerai/Akta Perceraian (jika Cerai Hidup) Surat Pernyataan Belum Pernah Menikah dari Kelurahan (Jika Belum Menikah) Surat Pernyataan Belum Menikah Lagi dari Kelurahan dan/atau Surat Kematian dari RS / Puskesmas / Kelurahan (Jika Cerai Mati) - Jika Ada"},
    ]
    let doc_legalitas_cerai = [
        {"no":"7",  "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"akta_cerai", "jenis_dokumen":"Akta Nikah/Akta Perkawinan (jika Menikah) Akta Cerai/Akta Perceraian (jika Cerai Hidup) Surat Pernyataan Belum Pernah Menikah dari Kelurahan (Jika Belum Menikah) Surat Pernyataan Belum Menikah Lagi dari Kelurahan dan/atau Surat Kematian dari RS / Puskesmas / Kelurahan (Jika Cerai Mati) - Jika Ada"},
    ]
    let doc_legalitas_menikah = [
        {"no":"7",  "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"buku_nikah", "jenis_dokumen":"Akta Nikah/Akta Perkawinan (jika Menikah) Akta Cerai/Akta Perceraian (jika Cerai Hidup) Surat Pernyataan Belum Pernah Menikah dari Kelurahan (Jika Belum Menikah) Surat Pernyataan Belum Menikah Lagi dari Kelurahan dan/atau Surat Kematian dari RS / Puskesmas / Kelurahan (Jika Cerai Mati) - Jika Ada"},
    ]
    // end: status menikah

    // brw_user_detail_penghasilan = 2 (join income)
    let doc_legalitas_status = is_single_income == 2 ? doc_legalitas.concat(doc_legalitas_joint_income) : doc_legalitas
    if (status_kawin == 1) { // menikah
        doc_legalitas_status.push(...doc_legalitas_menikah)
    } else if(status_kawin == 2) { // belum menikah
        doc_legalitas_status.push(...doc_legalitas_belum_menikah)
    } else { // cerai
        doc_legalitas_status.push(...doc_legalitas_cerai)
    }
    console.log(status_kawin)
    // Sort by no
    doc_legalitas_status.sort((a, b) => (a.no - b.no))

    doc_legalitas_status.forEach((element, index) => {
        let doc_legal_verif         = data_chk[`dok_legal_${element.no}_verif`]
        let doc_legal_verif_hasil   = data_chk[`dok_legal_${element.no}_verif_hasil`] ? data_chk[`dok_legal_${element.no}_verif_hasil`] : ''
        let doc_legal_dsi           = data_chk[`dok_legal_${element.no}_dsi`]
        let doc_legal_dsi_hasil     = data_chk[`dok_legal_${element.no}_dsi_hasil`] ? data_chk[`dok_legal_${element.no}_dsi_hasil`] : ''

        let show_file_legalitas = ``
        if(element.doc_pendukung == 1) {
            show_file_legalitas = `
                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                    data-tableName="${element.table_name}" data-fieldName="${element.field_name}" data-fileType="${element.file_type}"
                    data-target="#modal-doc-pendukung">
                    Lihat File
                </button>
            `
        } else {
            show_file_legalitas = `-`
        }
        $('#table_doc_legalitas').find('tbody').append(`
        <tr>
            <th class="text-center align-middle" scope="row">${index+1}</th>
            <td class="text-left align-middle">${element.jenis_dokumen}</td>
            <td class="text-center align-middle">${show_file_legalitas}</td>
            <td class="text-center align-middle"><input type="checkbox" id="dok_legal_${element.no}_verif" name="dok_legal_${element.no}_verif" value="1" ${doc_legal_verif == 1 ? 'checked' : ''} class="align-middle" onclick="return false"/></td>
            <td class="text-center align-middle"><input type="text" id="dok_legal_${element.no}_verif_hasil" name="dok_legal_${element.no}_verif_hasil" value="${doc_legal_verif_hasil}" class="form-control ${doc_legal_verif == 1 ? '' : 'd-none'}" data-toggle="tooltip" title="${doc_legal_verif_hasil}" disabled/></td>
            <td class="text-center align-middle"><input type="checkbox" id="dok_legal_${element.no}_dsi" name="dok_legal_${element.no}_dsi" value="1" ${doc_legal_dsi == 1 ? 'checked' : ''} class="align-middle" onclick="showHasilVerif('dok_legal_${element.no}_dsi', 'dok_legal_${element.no}_dsi_hasil')"/></td>
            <td class="text-center align-middle"><input type="text" id="dok_legal_${element.no}_dsi_hasil" name="dok_legal_${element.no}_dsi_hasil" value="${doc_legal_dsi_hasil}" class="form-control ${doc_legal_dsi == 1 ? '' : 'd-none'}"/></td>
        </tr>`);
    });
    // END: Doc Legalitas

    // START: Usia
    const usia = [
        {"no":"1", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Minimum 21 Tahun dan pada saat jatuh tempo, maksimum umur 55 tahun(untuk pegawai)"}
        // {"no":"1", "jenis_dokumen": "Minimum 21 tahun dan pada saat Pendanaan jatuh tempo maksimum berumur 55 tahun (untuk pegawai)."}
    ]

    usia.forEach((element, index) => {
        let usia_verif          = data_chk[`usia_${element.no}_verif`]
        let usia_verif_hasil    = data_chk[`usia_${element.no}_verif_hasil`] ? data_chk[`usia_${element.no}_verif_hasil`] : ''
        let usia_dsi            = data_chk[`usia_${element.no}_dsi`]
        let usia_dsi_hasil      = data_chk[`usia_${element.no}_dsi_hasil`] ? data_chk[`usia_${element.no}_dsi_hasil`] : ''

        let show_file_usia = ``
        if(element.doc_pendukung == 1) {
            show_file_usia = `
                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                    data-tableName="${element.table_name}" data-fieldName="${element.field_name}" data-fileType="${element.file_type}"
                    data-target="#modal-doc-pendukung">
                    Lihat File
                </button>
            `
        } else {
            show_file_usia = `-`
        }

        $('#table_usia').find('tbody').append(`
        <tr>
            <th class="text-center align-middle" scope="row">${index+1}</th>
            <td class="text-left align-middle">${element.jenis_dokumen}</td>
            <td class="text-center align-middle">${show_file_usia}</td>
            <td class="text-center align-middle"><input type="checkbox" id="usia_${element.no}_verif" name="usia_${element.no}_verif" value="1" ${usia_verif == 1 ? 'checked' : ''} class="align-middle" onclick="return false"/></td>
            <td class="text-center align-middle"><input type="text" id="usia_${element.no}_verif_hasil" name="usia_${element.no}_verif_hasil" value="${usia_verif_hasil}" class="form-control ${usia_verif == 1 ? '' : 'd-none'}" data-toggle="tooltip" title="${usia_verif_hasil}" disabled/></td>
            <td class="text-center align-middle"><input type="checkbox" id="usia_${element.no}_dsi" name="usia_${element.no}_dsi" value="1" ${usia_dsi == 1 ? 'checked' : ''} class="align-middle" onclick="showHasilVerif('usia_${element.no}_dsi', 'usia_${element.no}_dsi_hasil')"/></td>
            <td class="text-center align-middle"><input type="text" id="usia_${element.no}_dsi_hasil" name="usia_${element.no}_dsi_hasil" value="${usia_dsi_hasil}" class="form-control ${usia_dsi == 1 ? '' : 'd-none'}"/></td>
        </tr>`);
    })
    // END: Usia

    // START: Pekerjaan
    const pekerjaan = [
        {"no":"1", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"surat_bekerja", "jenis_dokumen":"SK Terakhir/Surat Keterangan Kerja Asli"},
        {"no":"2", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Lama Usia Perusahaan minimal 2 tahun"},
        {"no":"3", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Pekerjaan dari Penerima Dana dan/atau Pasangan merupakan Pekerjaan/Profesi yang tidak berhubungan/sebagai :</b>
            <br>a. Hukum (Hakim, Pengacara, Petugas Pengadilan, Konsultan Hukum, Petugas Firma Hukum dan termasuk karyawan yang bekerja
            pada profesi tersebut);
            <br>b. Petugas Militer (Angkatan Udara, Angkatan Laut, Angkatan Darat, dan Polisi termasuk karyawan yang bekerja pada
            instansi terkait);
            <br>c. Petugas Surat Kabar (Wartawan, Editor dan Jurnalis);
            <br>d. Anggota dan/atau Petugas Legislatif (DPR, MPR, DPRD, DPD);
            <br>e. Penjual Multi Level Marketing (MLM);
            <br>f. Staff kantor kedutaan besar`
        },
        // {"no":"1", "jenis_dokumen": "SK Terakhir / Surat Keterangan Kerja Asli"},
        // {"no":"2", "jenis_dokumen": "Lama Usia Perusahaan Minimal 2 Tahun"},
        // {"no":"3", "jenis_dokumen": `<b>Pekerjaan dari Penerima Dana dan/atau Pasangan merupakan Pekerjaan/Profesi yang tidak berhubungan/sebagai :</b> <br>
        // a. Hukum (Hakim, Pengacara, Petugas Pengadilan, Konsultan Hukum, Petugas Firma Hukum dan termasuk karyawan yang bekerja
        // pada profesi tersebut)<br>
        // b. Petugas Militer (Angkatan Udara, Angkatan Laut, Angkatan Darat, dan Polisi termasuk karyawan yang bekerja pada
        // instansi terkait)<br>
        // c. Petugas Surat Kabar (Wartawan, Editor dan Jurnalis)<br>
        // d. Anggota dan/atau Petugas Legislatif (DPR, MPR, DPRD, DPD)<br>
        // e. Penjual Multi Level Marketing (MLM)<br>
        // f. Staff kantor kedutaan besar`},
    ]

    const pekerjaan_single_income = [
        {"no":"4", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Jika pegawai tetap di perusahaan saat ini :</b>
        <br>1. Minimum masa kerja 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap Kecuali PNS tidak melihat masa
        kerja), atau
        <br>2. Jika di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai pegawai tetap
        di perusahaan terakhir sebelumnya.`}
        // {"no":"4", "jenis_dokumen": `<b>Jika pegawai tetap di perusahaan saat ini :</b><br>
        // 1. Minimum masa kerja 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap Kecuali PNS tidak melihat masa
        // kerja), atau <br>
        // 2. Jika di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai pegawai tetap
        // di perusahaan terakhir sebelumnya.`}
    ]

    const pekerjaan_joint_income = [
        {"no":"5", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Penerima Dana dan pasangan merupakan Pegawai / Karyawan tetap di perusahaan saat ini :</b>
        <br>1. Minimum masa kerja 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap), atau
        <br>2. Jika di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai pegawai tetap
        di perusahaan terakhir sebelumnya`},
        {"no":"6", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":`<b>Penerima Dana merupakan Pegawai / Karyawan tetap dan Pasangan merupakan Pegawai / Karyawan kontrak di perusahaan saat
        ini : </b>
        <br>1. Penerima Dana memiliki masa kerja minimum 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap), atau
        <br>2. Jika Penerima Dana di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai
        pegawai tetap di perusahaan terakhir sebelumnya, atau
        <br>3. Pasangan minimum memiliki pengalaman kerja 2 tahun di perusahaan saat ini, atau
        <br>4. Pasangan minimum 1 tahun diperusahaan saat ini dengan memiliki pengalaman kerja minimal 2 tahun sebagai pegawai
        kontrak / tetap di perusahaan terakhir sebelumnya, atau
        <br>5. Pasangan minimum 6 bulan diperusahaan saat ini dengan memiliki pengalaman kerja minimal 3 tahun sebagai pegawai
        kontrak / tetap di perusahaan sebelumnya`},
        // {"no":"5", "jenis_dokumen":`<b >Penerima Dana dan pasangan merupakan Pegawai / Karyawan tetap di perusahaan saat ini :</b><br>
        // 1. Minimum masa kerja 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap), atau <br>
        // 2. Jika di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai pegawai tetap
        // di perusahaan terakhir sebelumnya`},
        // {"no":"6", "jenis_dokumen": `<b>Penerima Dana merupakan Pegawai / Karyawan tetap dan Pasangan merupakan Pegawai / Karyawan kontrak di perusahaan saat
        // ini : <br></b>
        // 1. Penerima Dana memiliki masa kerja minimum 1 tahun (termasuk masa kerja sebelum diangkat menjadi pegawai tetap), atau <br>
        // 2. Jika Penerima Dana di perusahaan saat ini masa kerja belum 1 tahun, maka minimum memiliki pengalaman 1 tahun sebagai
        // pegawai tetap di perusahaan terakhir sebelumnya, atau <br>
        // 3. Pasangan minimum memiliki pengalaman kerja 2 tahun di perusahaan saat ini, atau <br>
        // 4. Pasangan minimum 1 tahun diperusahaan saat ini dengan memiliki pengalaman kerja minimal 2 tahun sebagai pegawai
        // kontrak / tetap di perusahaan terakhir sebelumnya, atau <br>
        // 5. Pasangan minimum 6 bulan diperusahaan saat ini dengan memiliki pengalaman kerja minimal 3 tahun sebagai pegawai
        // kontrak / tetap di perusahaan sebelumnya <br>`} 
    ]

    let pekerjaan_status = is_single_income == 1 ? pekerjaan.concat(pekerjaan_single_income) : pekerjaan.concat(pekerjaan_joint_income)

    pekerjaan_status.forEach((element, index) => {
        let pekerjaan_verif         = data_chk[`pekerjaan_${element.no}_verif`]
        let pekerjaan_verif_hasil   = data_chk[`pekerjaan_${element.no}_verif_hasil`] ? data_chk[`pekerjaan_${element.no}_verif_hasil`] : ''
        let pekerjaan_dsi           = data_chk[`pekerjaan_${element.no}_dsi`]
        let pekerjaan_dsi_hasil     = data_chk[`pekerjaan_${element.no}_dsi_hasil`] ? data_chk[`pekerjaan_${element.no}_dsi_hasil`] : ''
        
        let show_file_pekerjaan = ``
        if(element.doc_pendukung == 1) {
            show_file_pekerjaan = `
                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                    data-tableName="${element.table_name}" data-fieldName="${element.field_name}" data-fileType="${element.file_type}"
                    data-target="#modal-doc-pendukung">
                    Lihat File
                </button>
            `
        } else {
            show_file_pekerjaan = `-`
        }

        $('#table_pekerjaan').find('tbody').append(`
        <tr>
            <th class="text-center align-middle" scope="row">${index+1}</th>
            <td class="text-left align-middle">${element.jenis_dokumen}</td>
            <td class="text-center align-middle">${show_file_pekerjaan}</td>
            <td class="text-center align-middle"><input type="checkbox" id="pekerjaan_${element.no}_verif" name="pekerjaan_${element.no}_verif" value="1" ${pekerjaan_verif == 1 ? 'checked' : ''} class="align-middle" onclick="return false"/></td>
            <td class="text-center align-middle"><input type="text" id="pekerjaan_${element.no}_verif_hasil" name="pekerjaan_${element.no}_verif_hasil" value="${pekerjaan_verif_hasil}" class="form-control ${pekerjaan_verif == 1 ? '' : 'd-none'}" data-toggle="tooltip" title="${pekerjaan_verif_hasil}" disabled/></td>
            <td class="text-center align-middle"><input type="checkbox" id="pekerjaan_${element.no}_dsi" name="pekerjaan_${element.no}_dsi" value="1" ${pekerjaan_dsi == 1 ? 'checked' : ''} class="align-middle" onclick="showHasilVerif('pekerjaan_${element.no}_dsi', 'pekerjaan_${element.no}_dsi_hasil')"/></td>
            <td class="text-center align-middle"><input type="text" id="pekerjaan_${element.no}_dsi_hasil" name="pekerjaan_${element.no}_dsi_hasil" value="${pekerjaan_dsi_hasil}" class="form-control ${pekerjaan_dsi == 1 ? '' : 'd-none'}""/></td>
        </tr>`);
    })
    // END: Pekerjaan

    // START: Verifikasi
    // const verifikasi = [
    //     'Tidak Memiliki Kredit / Pendanaan Bermasalah Baik di Bank Maupun Non Bank',
        
    //     `<b>Biro Kredit :</b> <br>`, //Title = 2
    //     `Biro Kredit Fasilitas Pendanaan Penerima Dana sesuai ketentuan yang ditetapkan (Score Minimal C3). <br>`,
    //     `Biro Kredit Fasilitas Pendanaan Pasangan sesuai ketentuan yang ditetapkan (Score Minimal C3). <br>`,
        
    //     `<b>Credolab Checking :</b> <br>`, //Title = 5
    //     `Hasil Kredolab Checking Penerima Dana sesuai ketentuan (terverifikasi) <br>`,
        
    //     `<b>Verijelas :</b> <br>`, //Title = 7
    //     `Hasil Verijelas Checking Penerima Dana sesuai ketentuan (terverifikasi) <br>`,
    //     `Hasil Verijelas Checking Pasangan sesuai ketentuan (terverifikasi) <br>`
    // ]

    const verifikasi = [
        {"no":"1", "nilai_kelayakan":"0", "is_title": "",  "jenis_dokumen":"Tidak Memiliki Kredit/Pendanaan Bermasalah Baik di Bank maupun Non Bank"},
        {"no":"2", "nilai_kelayakan":"0", "is_title": "1", "jenis_dokumen":"<b>Biro Kredit :</b>"},
        {"no":"2", "nilai_kelayakan":"2", "is_title": "",  "jenis_dokumen":"Biro Kredit Fasilitas Pendanaan Penerima Dana sesuai ketentuan yang ditetapkan (Score Minimal C3)."},

        {"no":"3", "nilai_kelayakan":"0", "is_title": "1", "jenis_dokumen":"<b>Credolab Checking :</b>"},
        {"no":"4", "nilai_kelayakan":"3", "is_title": "",  "jenis_dokumen":"Hasil Kredolab Checking Penerima Dana sesuai ketentuan (terverifikasi)"},

        {"no":"4", "nilai_kelayakan":"0",  "is_title": "1", "jenis_dokumen":"<b>Verijelas :</b>"},
        {"no":"5", "nilai_kelayakan":"4",  "is_title": "",  "jenis_dokumen":"Hasil Verijelas Checking Penerima Dana sesuai ketentuan (terverifikasi)"},
    ];

    const skor_biro_kredit = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->skor_pefindo !!}`
    const grade_biro_kredit = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->grade_pefindo !!}`
    const skor_personal_credolab = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->skor_personal_credolab !!}`
    const skor_pendanaan_credolab = `{!! $data_verifikasi == 0 ? '-' : $data_verifikasi[0]->skor_pendanaan_credolab !!}`   

    verifikasi.forEach((element, index) => {
        let is_title = ((element.no == '1') || element.is_title) != '1' ? '' : element.no
        // let is_title = 0

        // if (index+1 == 2 || index+1 == 5 || index+1 == 7) {
        //     is_title = 1
        // } else {
        //     number += 1
        // }
        let number = element.no
        let verifikasi_verif        = data_chk[`verifikasi_${number}_verif`]
        let verifikasi_verif_hasil  = data_chk[`verifikasi_${number}_verif_hasil`] ? data_chk[`verifikasi_${number}_verif_hasil`] : ''
        let verifikasi_dsi          = data_chk[`verifikasi_${number}_dsi`]
        let verifikasi_dsi_hasil    = data_chk[`verifikasi_${number}_dsi_hasil`] ? data_chk[`verifikasi_${number}_dsi_hasil`] : ''

        let show_hasil_verif = `onclick="showHasilVerif('verifikasi_${number}_dsi', 'verifikasi_${number}_dsi_hasil')"`

        if(element.nilai_kelayakan == 2) {
            
            button_show_verifikasi = `
                <button type="button" class="btn btn-primary rounded btn_show_nilai" data-skor="${element.nilai_kelayakan}" data-toggle="modal"
                    data-target="#modal-doc-verifikasi">
                    Lihat
                </button>
            `
        } else if(element.nilai_kelayakan == 3) {
        
            button_show_verifikasi = `
                <button type="button" class="btn btn-primary rounded btn_show_nilai" data-skor="${element.nilai_kelayakan}" data-toggle="modal"
                    data-target="#modal-doc-verifikasi">
                    Lihat
                </button>
            `
        } else if (element.nilai_kelayakan == 4) {
            button_show_verifikasi = `
                <button type="button" data-toggle="modal" id="btnCekVerijelas" data-target="#verijelas_modal" data-backdrop="static"
                    data-keyboard="false" href="#" class="btn btn-primary rounded btn_show_nilai" data-check="1"> 
                    Lihat 
                </button>
            `
        } else if (element.is_title == 1) {
            button_show_verifikasi = ``
        } else {
            button_show_verifikasi = `-`
        }

        $('#table_verifikasi').find('tbody').append(`
        <tr>
            <th class="text-center align-middle" scope="row">${is_title}</th>
            <td class="text-left align-middle">${element.jenis_dokumen}</td>
            <td class="text-center align-middle">${button_show_verifikasi}</td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="checkbox" id="verifikasi_'+number+'_verif" name="verifikasi_'+number+'_verif" value="1" '+(verifikasi_verif == 1 ? "checked" : "")+' class="align-middle" onclick="return false"/>' : ''}</td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="text" id="verifikasi_'+number+'_verif_hasil" name="verifikasi_'+number+'_verif_hasil" value=" '+verifikasi_verif_hasil+'" class="form-control '+(verifikasi_verif == 1 ? "" : "d-none") +'" data-toggle="tooltip" title="'+verifikasi_verif_hasil+'" disabled/>' : ''}</td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="checkbox" id="verifikasi_'+number+'_dsi" name="verifikasi_'+number+'_dsi" value="1" '+(verifikasi_dsi == 1 ? "checked" : "")+' class="align-middle"' +show_hasil_verif+'/>' : ''}</td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="text" id="verifikasi_'+number+'_dsi_hasil" name="verifikasi_'+number+'_dsi_hasil" value=" '+verifikasi_dsi_hasil+'" class="form-control '+(verifikasi_dsi == 1 ? "" : "d-none") +'"/>' : ''}</td>
        </tr>`)
    })
    // END: Verifikasi

    // START: Pendapatan
    const pendapatan = [
        {"no":"1", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "is_title": "", "jenis_dokumen":"Minimum THP Calon Penerima Pendanaan Adalah Sebesar UMR Sesuai Domisili Tempat Bekerja"},
        {"no":"2", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "is_title": "1", "jenis_dokumen":"<b>Dokumen Penghasilan</b>"},
        {"no":"2", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"slip_gaji", "is_title": "", "jenis_dokumen":"Asli Slip Gaji minimal 3 bulan terakhir"},
        {"no":"4", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"mutasi_rekening", "is_title": "", "jenis_dokumen":"Mutasi rekening tabungan pribadi yang mencerminkan gaji/pendapatan penerima dana 3 bulan terakhir"},
        {"no":"6", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"spt", "is_title": "", "jenis_dokumen":"SPT Pajak Tahunan Penerima Dana"},
        
        // {"no":"1", "is_title": "", "jenis_dokumen": `Minimum THP calon Penerima Dana adalah sebesar UMR sesuai domisili tempat bekerja (sebelum memperhitungkan pendapatan /
        // THP pasangan)`},
        // {"no":"2", "is_title": "1", "jenis_dokumen":  ` <b>Dokumen Penghasilan</b>`}, //Title = 2
        // {"no":"2", "is_title": "", "jenis_dokumen":  `Asli slip gaji minimal 3 bulan terakhir`},
        // {"no":"4", "is_title": "", "jenis_dokumen":  `Mutasi rekening tabungan pribadi yang mencerminkan gaji / pendapatan Penerima Dana 3 bulan terakhir`},
        // {"no":"6", "is_title": "", "jenis_dokumen":  `SPT pajak tahunan Penerima Dana`},
    ]

    const pendapatan_joint_income = [
        {"no":"3", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"slip_gaji_pasangan", "is_title": "", "jenis_dokumen":"Asli slip gaji minimal 3 bulan terakhir pasangan (jika joint income)"},
        {"no":"5", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"mutasi_rekening_pasangan", "is_title": "", "jenis_dokumen":"Mutasi rekening tabungan yang mencerminkan gaji / pendapatan pasangan Penerima Dana 3 bulan terakhir (jika joint income)"},
        {"no":"7", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_legalitas_pribadi", "field_name":"spt_pasangan", "is_title": "", "jenis_dokumen":"SPT pajak tahunan Pasangan (Suami/Istri) (jika joint income)"},
        
        // {"no":"3", "is_title": "", "jenis_dokumen": `Asli slip gaji minimal 3 bulan terakhir pasangan`},
        // {"no":"5", "is_title": "", "jenis_dokumen": `Mutasi rekening tabungan yang mencerminkan gaji / pendapatan pasangan Penerima Dana 3 bulan
        // terakhir`},
        // {"no":"7", "is_title": "", "jenis_dokumen": `SPT pajak tahunan Pasangan (Suami/Istri)`},
    ]

    // brw_user_detail_penghasilan = 2 (join income)
    let pendapatan_status = is_single_income == 2 ? pendapatan.concat(pendapatan_joint_income) : pendapatan
    
    // Sort by no
    pendapatan_status.sort((a, b) => (a.no - b.no))

    pendapatan_status.forEach((element, index) => {

        let is_title = ((element.no == '1') || element.is_title) != '1' ? '' : element.no

        let pendapatan_verif        = data_chk[`pendapatan_${element.no}_verif`]
        let pendapatan_verif_hasil  = data_chk[`pendapatan_${element.no}_verif_hasil`] ? data_chk[`pendapatan_${element.no}_verif_hasil`] : ''
        let pendapatan_dsi          = data_chk[`pendapatan_${element.no}_dsi`]
        let pendapatan_dsi_hasil    = data_chk[`pendapatan_${element.no}_dsi_hasil`] ? data_chk[`pendapatan_${element.no}_dsi_hasil`] : ''

        let show_hasil_verif = `onclick="showHasilVerif('pendapatan_${element.no}_dsi', 'pendapatan_${element.no}_dsi_hasil')"`

        let show_file_pendapatan = ``
        if(element.doc_pendukung == 1) {
            show_file_pendapatan = `
                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                    data-tableName="${element.table_name}" data-fieldName="${element.field_name}" data-fileType="${element.file_type}"
                    data-target="#modal-doc-pendukung">
                    Lihat File
                </button>
            `
        } else {
            show_file_pendapatan = `-`
        }

        $('#table_pendapatan').find('tbody').append(`
        <tr>
            <th class="text-center align-middle" scope="row">${is_title}</th>
            <td class="text-left">${element.jenis_dokumen}</td>
            <td class="text-center">${show_file_pendapatan}</td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="checkbox" id="pendapatan_'+element.no+'_verif" name="pendapatan_'+element.no+'_verif" value="1" '+(pendapatan_verif == 1 ? "checked" : "") +' class="align-middle" onclick="return false"/>' : ''} </td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="text" id="pendapatan_'+element.no+'_verif_hasil" name="pendapatan_'+element.no+'_verif_hasil" value="'+ pendapatan_verif_hasil +'" class="form-control '+(pendapatan_verif == 1 ? "" : "d-none") +'" data-toggle="tooltip" title="'+pendapatan_verif_hasil+'" disabled/>' : ''} </td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="checkbox" id="pendapatan_'+element.no+'_dsi" name="pendapatan_'+element.no+'_dsi" value="1" '+(pendapatan_dsi == 1 ? "checked" : "") +' class="align-middle" '+show_hasil_verif+' />' : ''} </td>
            <td class="text-center align-middle">${element.is_title != 1 ? '<input type="text" id="pendapatan_'+element.no+'_dsi_hasil" name="pendapatan_'+element.no+'_dsi_hasil" value="'+ pendapatan_dsi_hasil +'" class="form-control '+(pendapatan_dsi == 1 ? "" : "d-none") +'" />' : ''} </td>
        </tr>`);
    })
    // END: Pendapatan

    // START: Jaminan
    const jaminan = [
        {"no":"1", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"sertifikat", "jenis_dokumen":"Sertifikat Objek Pendanaan(SHM/SHGB)"},
        {"no":"2", "doc_pendukung":"1", "file_type":"pdf", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"imb", "jenis_dokumen":"IMB"},
        {"no":"3", "doc_pendukung":"1", "file_type":"pdf|jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"sptt_stts", "jenis_dokumen":"SPTT dan STTS Tahun Terakhir"},
        {"no":"4", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Nilai Taksasi Berdasarkan Appraisal Internal dan/atau KJPP (Nilai Pasar Wajar dan Nilai Likuidasi)"},
        {"no":"5", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Lebar Jalan di Depan Objek Jaminan Minimal 2,2 Meter atau Dapat Dilalui oleh 1 Mobil"},
        {"no":"6", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Jaminan tidak dekat sutet, kuburan/TPU, +- 30 Meter dan Tidak Tusuk Sate"},
        {"no":"7", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Lokasi Jaminan Tidak Terkena Banjir dalam 2 Tahun Terakhir"},
        {"no":"8", "doc_pendukung":"0", "file_type":"", "table_name":"", "field_name":"", "jenis_dokumen":"Tidak berlokasi di jalur hijau(Green Belt), Bantaran Sungai, Bantaran Rel Kereta Api"},
        {"no":"9", "doc_pendukung":"1", "file_type":"jpeg|jpg|png|bmp", "table_name":"brw_dokumen_objek_pendanaan", "field_name":"ktp_pemilik", "jenis_dokumen":"Jika Penjual Perorangan/Rumah Second maka wajib dilampirkan : KTP(Suami/Istri), KK, Surat Nikah, NPWP"},
        
        // {"no":"1", "is_title": "", "jenis_dokumen": `Sertipikat Objek Pendanaan/Pendanaan (SHM / SHGB)`},
        // {"no":"2", "is_title": "", "jenis_dokumen": `IMB`},
        // {"no":"3", "is_title": "", "jenis_dokumen": `SPTT dan STTS Tahun Terakhir`},
        // {"no":"4", "is_title": "", "jenis_dokumen": `Nilai Taksasi Berdasarkan Appraisal Internal dan/atau KJPP (Nilai Pasar Wajar dan Nilai Likuidasi)`},
        // {"no":"5", "is_title": "", "jenis_dokumen": `Lebar Jalan Di Depan Objek Jaminan Minimal 2,2 Meter atau Dapat Dilalui Oleh 1 Mobil`},
        // {"no":"6", "is_title": "", "jenis_dokumen": `Jaminan Tidak Dekat Sutet, Kuburan / TPU ± 30 Meter dan Tidak Tusuk Sate`},
        // {"no":"7", "is_title": "", "jenis_dokumen": `Lokasi Jaminan Tidak Terkena Banjir dalam 2 Tahun Terakhir`},
        // {"no":"8", "is_title": "", "jenis_dokumen": `Tidak Berlokasi di Jalur Hijau (Green Belt), Bantaran Sungai, Bantaran Rel Kereta Api`},
        // {"no":"9", "is_title": "", "jenis_dokumen": `Jika Penjual Perorangan / Rumah Second maka wajib dilampirkan : KTP (Suami / Istri), KK, Surat Nikah,NPWP`},
    ]

    jaminan.forEach((element, index) => {
        let jaminan_verif       = data_chk[`jaminan_${element.no}_verif`]
        let jaminan_verif_hasil = data_chk[`jaminan_${element.no}_verif_hasil`] ? data_chk[`jaminan_${element.no}_verif_hasil`] : ''
        let jaminan_dsi         = data_chk[`jaminan_${element.no}_dsi`]
        let jaminan_dsi_hasil   = data_chk[`jaminan_${element.no}_dsi_hasil`] ? data_chk[`jaminan_${element.no}_dsi_hasil`] : ''

        let show_file_jaminan = ``
        if(element.doc_pendukung == 1) {
            show_file_jaminan = `
                <button type="button" class="btn btn-primary rounded btn-show-file" data-toggle="modal"
                    data-tableName="${element.table_name}" data-fieldName="${element.field_name}" data-fileType="${element.file_type}"
                    data-target="#modal-doc-pendukung">
                    Lihat File
                </button>
            `
        } else {
            show_file_jaminan = `-`
        }
        $('#table_jaminan').find('tbody').append(`
        <tr>
            <th class="text-center align-middle" scope="row">${index+1}</th>
            <td class="text-left align-middle">${element.jenis_dokumen}</td>
            <td class="text-center align-middle">${show_file_jaminan}</td>
            <td class="text-center align-middle"><input type="checkbox" id="jaminan_${element.no}_verif" name="jaminan_${element.no}_verif" value="1" ${jaminan_verif == 1 ? 'checked' : ''} class="align-middle" onclick="return false"/></td>
            <td class="text-center align-middle"><input type="text" id="jaminan_${element.no}_verif_hasil" name="jaminan_${element.no}_verif_hasil" value="${jaminan_verif_hasil}" class="form-control ${jaminan_verif == 1 ? '' : 'd-none'}" data-toggle="tooltip" title="${jaminan_verif_hasil}" disabled/></td>
            <td class="text-center align-middle"><input type="checkbox" id="jaminan_${element.no}_dsi" name="jaminan_${element.no}_dsi" value="1" ${jaminan_dsi == 1 ? 'checked' : ''} class="align-middle" onclick="showHasilVerif('jaminan_${element.no}_dsi', 'jaminan_${element.no}_dsi_hasil')"/></td>
            <td class="text-center align-middle"><input type="text" id="jaminan_${element.no}_dsi_hasil" name="jaminan_${element.no}_dsi_hasil" value="${jaminan_dsi_hasil}" class="form-control ${jaminan_dsi == 1 ? '' : 'd-none'}"/></td>
        </tr>`);
    })
    // END: Jaminan

    // START: CASH RATIO
    const cash_ratio = [
        {"no":"1", "is_title": "", "jenis_dokumen": `Pendapatan Minimum Sesuai UMR - Rp.5.000.000 Maks Cash Ratio 40%`},
        {"no":"2", "is_title": "", "jenis_dokumen": `Pendapatan > Rp.5.000.000 - Rp.10.000.000 Maks Cash Ratio 45%`},
        {"no":"3", "is_title": "", "jenis_dokumen": `Pendapatan > Rp.10.000.000 Maks Cash Ratio 50%`},
        {"no":"4", "is_title": "", "jenis_dokumen": `Pendapatan > Rp.20.000.000 Maks Cash Ratio 60%`},
    ]

    cash_ratio.forEach((element, index) => {
        let cash_ratio_dsi          = data_chk[`cash_ratio_${element.no}_dsi`]
        let cash_ratio_dsi_hasil    = data_chk[`cash_ratio_${element.no}_dsi_hasil`] ? data_chk[`cash_ratio_${element.no}_dsi_hasil`] : ''

        $('#table_cash_rasio').find('tbody').append(`
        <tr>
            <th class="text-center" scope="row">${index+1}</th>
            <td class="text-left">${element.jenis_dokumen}</td>
            <td class="text-center"><input type="checkbox" id="cash_ratio_${element.no}_dsi" name="cash_ratio_${element.no}_dsi" value="1" ${cash_ratio_dsi == 1 ? 'checked' : ''} class=" align-middle" onclick="showHasilVerif('cash_ratio_${element.no}_dsi', 'cash_ratio_${element.no}_dsi_hasil')"/></td>
            <td class="text-center align-middle"><input type="text" id="cash_ratio_${element.no}_dsi_hasil" name="cash_ratio_${element.no}_dsi_hasil" value="${cash_ratio_dsi_hasil}" class="form-control ${cash_ratio_dsi == 1 ? '' : 'd-none'}"/></td>
        </tr>`);
    })
    // END: CASH RATIO

    // START: FTV
    const ftv = [
        // {"no":"1", "is_title": "1", "jenis_dokumen": `Rumah Baru (Ketika Pilih Rumah Baru Muncul Pilihan a dan b)`},
        // {"no":"8", "is_title": "", "jenis_dokumen": `Rumah Second (Ketika Pilih Rumah Second Muncul Pilihan c dan d)`},
    ]

    const ftv_rumah_baru = [
        {"no":"2", "text_no": '1', "is_title": "1", "jenis_dokumen": `<b>Luas Bangunan > 21 M² - 70 M²</b>`},
        {"no":"3", "text_no": '', "is_title": "", "jenis_dokumen": `Murabahah Fas 1 : 90 % Fas 2 dst : 80 %`},
        {"no":"4", "text_no": '', "is_title": "", "jenis_dokumen": `MMQ Fas 1 : 90 % Fas 2 dst : 85 %`},
        {"no":"5", "text_no": '2', "is_title": "1", "jenis_dokumen": `<b>Luas Bangunan > 70 M²</b>`},
        {"no":"6", "text_no": '', "is_title": "", "jenis_dokumen": `Murabahah Fas 1 : 90 %a Fas 2 dst : 80 %`},
        {"no":"7", "text_no": '', "is_title": "", "jenis_dokumen": `MMQ Fas 1 : 90 % Fas 2 dst : 85 %`},
    ]

    const ftv_rumah_bekasi = [
        {"no":"9",  "text_no" : '1', "is_title": "1", "jenis_dokumen": `<b>Luas Bangunan > 21 M² - 70 M²</b>`},
        {"no":"10", "text_no" : '',  "is_title": "", "jenis_dokumen": `Murabahah Fas 1 : 80 % Fas 2 dst : 75 %`},
        {"no":"11", "text_no" : '',  "is_title": "", "jenis_dokumen": `MMQ Fas 1 : 85 % Fas 2 dst : 80 %`},
        {"no":"12", "text_no" : '2',  "is_title": "1", "jenis_dokumen": `<b>Luas Bangunan > 70 M²</b>`},
        {"no":"13", "text_no" : '',  "is_title": "", "jenis_dokumen": `Murabahah Fas 1 : 80 % Fas 2 dst : 75 %`},
        {"no":"14", "text_no" : '',  "is_title": "", "jenis_dokumen": `MMQ Fas 1 : 85 % Fas 2 dst : 80 %`},
    ]

    // brw_pengajuan->jenis_properti = 1 baru, 2 = bekas
    is_rumah_baru = '{!! $data_brw ? $data_brw->jenis_properti : 0 !!}'
    let ftv_status = is_rumah_baru == 1 ? ftv_rumah_baru : ftv_rumah_bekasi
    
    // Sort by no
    ftv_status.sort((a, b) => (a.no - b.no))

    ftv_status.forEach((element, index) => {
        let ftv_dsi         = data_chk[`ftv_${element.no}_dsi`]
        let ftv_dsi_hasil   = data_chk[`ftv_${element.no}_dsi_hasil`] ? data_chk[`ftv_${element.no}_dsi_hasil`] : ''

        let is_title = ((element.no == '1') || element.is_title) != '1' ? '' : element.text_no
        $('#table_ftv').find('tbody').append(`
        <tr>
            <th class="text-center" scope="row">${is_title}</th>
            <td class="text-left">${element.jenis_dokumen}</td>
            <td class="text-center"><input type="checkbox" id="ftv_${element.no}_dsi" name="ftv_${element.no}_dsi" value="1" ${ftv_dsi == 1 ? 'checked' : ''} class=" align-middle" onclick="showHasilVerif('ftv_${element.no}_dsi', 'ftv_${element.no}_dsi_hasil')"/></td>
            <td class="text-center align-middle"><input type="text" id="ftv_${element.no}_dsi_hasil" name="ftv_${element.no}_dsi_hasil" value="${ftv_dsi_hasil}" class="form-control ${ftv_dsi == 1 ? '' : 'd-none'}"/></td>
        </tr>`);
    })
    // END: FTV

    $('.btn-show-file').click(function() {
        let tableName = $(this).attr('data-tableName')
        let fieldName = $(this).attr('data-fieldName')
        let filetype = $(this).attr('data-fileType')

        getFilePath(tableName, fieldName, "", "", "", "")

        // Set Params to upload doc
        $('#tablename').val(tableName)
        $('#fieldname').val(fieldName)
        $('#pengajuanId').val(pengajuan_id)
        $('#brw_id').val(brw_id)
        $('#filetype').val(filetype)
    })

    $('#verijelas_modal').on('show.bs.modal', function(event) {
        $('#loading_footer_data').html(`
            <button type="button" data-dismiss="modal" onclick="javascript:$(this).parent().addClass('hidden');"
                class="btn btn-secondary" id="btnCloseHasil">
                @lang('verify.btn_close')
            </button>
        `)
        var buttonTrigger = $(event.relatedTarget);
        var checkVerijelas = buttonTrigger.data('check');
        $('#verijelas_modal .modal-title').html('{{ trans('verify.hasil.title') }}' + ' - ' + nama_brw);
        
        if(checkVerijelas == '1') {
            $('.btnCheckNext').removeClass('d-none');
        }else{
            $('.btnCheckNext').addClass('d-none');
        }

        veriJelasData();
    });

    $('.btn_show_nilai').click(function() {
        let value = $(this).attr('data-skor')
        let preview_nilai = ``
        console.log(value)
        if (value == 2) {
            preview_nilai = `
            <div class="row">
                <div class="col">
                    <p class="text-left text-status text-dark">Skor Biro Kredit: ${skor_biro_kredit}</p>
                </div>
                <div class="col">
                    <p class="text-left text-status text-dark">Grade Biro Kredit: ${grade_biro_kredit}</p>
                </div>
            </div>
            `
        } else {
            preview_nilai = `
            <div class="row">
                <div class="col">
                    <p class="text-left text-status text-dark">Skor Personal Credolab: ${skor_personal_credolab}</p>
                </div>
                <div class="col">
                    <p class="text-left text-status text-dark">Skor Pendanaan Credolab: ${skor_pendanaan_credolab}</p>
                </div>
            </div>
            `
        }
        
       $('#preview-nilai').html(preview_nilai)
    });
    
    const getFilePath = (table_name, field_name, table_name2, field_name2, table_name_title, table_name2_title) => {
        
        let url = `{{ route('getUserFilePathForAdmin', ['tableName' => ':table_name', 'fieldName' => ':field_name', 'pengajuan_id' => ':pengajuan_id']) }}`
        url = url.replace(':table_name', table_name).replace(':field_name', field_name).replace(':pengajuan_id', pengajuan_id)

        requestFilePathAjax(url, table_name_title)

        if (table_name2 != '') {
            let url2 = `{{ route('getUserFilePathForAdmin', ['tableName' => ':table_name', 'fieldName' => ':field_name', 'pengajuan_id' => ':pengajuan_id']) }}`
            url2 = url2.replace(':table_name', table_name2).replace(':field_name', field_name2).replace(':pengajuan_id', pengajuan_id)

            requestFilePathAjax(url2, table_name2_title)
        }
    }

    const requestFilePathAjax = (url, title) => {
        let status = 0 , path = ''
        console.log(url)

        $.ajax({
            type: "get",
            url: url,
            contentType: false,
            processData: false,
            beforeSend: () => {
                $('#preview').html(`
                    <p class="text-center text-status">Mengambil Data..</p>
                `)
            },
            success: function(data) {
                status = 1 // success
                path = data
                
                if (data.status == 'failed') {
                    $('#preview').html(`
                        <p class="text-center text-status">Data Tidak Ada</p>
                    `)
                } else {
                    $('.text-status').text('')

                    showFile(status, path, title)
                }
                
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;

                if (xhr.status === 419) {
                    $('#preview').html(`
                        <p class="text-center text-status">Gagal Mengambil Data</p>
                        <p class="text-center text-status">Page expired. please re-login again</p>
                    `)
                } else {
                    $('#preview').html(`
                        <p class="text-center text-status">Tidak Ada Data Yang Ditampilkan</p>
                    `)
                }

                status = 2 // failed
                showFile(status, path)
            },
        })
    }

    const showFile = (status, path, title) => {
        let pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");
        let file = "{{ route('getUserFileForAdmin', ['filename' => ':filename']) }}"
        path = path.replaceAll('/', ':')
        file = file.replace(':filename', path)
        
        if(status == 1) { // Success
            if (path) {
                if (pdfSearch.test(path)) {
                    $('#preview').append($('<p>', {
                        class: 'font-weight-bold mt-2',
                        text: title
                    }))
                    $('#preview').append($('<embed>', {
                        class: 'pdf_data',
                        src: file,
                        frameborder: '0',
                        width: '100%',
                        height: '500px'
                    }))

                } else {
                    $('#preview').append($('<p>', {
                        class: 'font-weight-bold mt-2',
                        text: title
                    }))
                    $('#preview').append($('<img>', {
                        class: 'img_data img-fluid w-100',
                        src: file,
                    }))
                }
            } else {
                $('.text-status').text(`Tidak Ada Data Yang Ditampilkan`)
            }
        } else { //Failed
            $('.text-status').text(`Tidak Ada Data Yang Ditampilkan`)
        }
    }

    const veriJelasData = () => {

        $.ajax({
            url: "{{ route('verify.data') }}",
            type: "post",
            dataType: "html",
            data: {
                _token: "{{ csrf_token() }}",
                borrower_id: brw_id,
                brw_type: brw_type,
                vendor: 'verijelas'
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                if (xhr.status == '419') {
                    errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                }

                $('#loading_title').html('Data Failed');
                $('#loading_content_data').html(errorMessage);
                $('#loading_footer_data').removeClass('d-none');
            },
            success: function(result) {
                $('#loading_title').html('Preparing Result ...');
                $('#loading_content_data').html(result);
                $('#loading_title').html('');
                $('#loading_footer_data').removeClass('d-none');
            }
        });


    }
</script>

@endpush