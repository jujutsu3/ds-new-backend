@extends('layouts.admin.master')

@section('title', 'Penggalangan Dana')

@section('content')
    @component('pages.admin.helper.breadcrumbs', [
        'title' => 'Lihat Proyek'
    ])
        @if ($pengajuan->status === \App\MasterStatusPengajuan::V_PENGGALANGAN)
            <a href="{{ route('penggalangan_dana.edit', $pengajuan->pengajuan_id) }}">
                <button class="btn btn-success">Ubah</button>
            </a>
        @endif
    @endcomponent()

    <div class="col-lg-12">
        @if(session()->has('message'))
            @component('pages.admin.helper.alert', [
                'type' => session()->get('message_type'),
                'message' => session()->get('message')
            ])
            @endcomponent()
        @endif
    </div>

    <fieldset disabled class="col-lg-12">
        <div class="row">
            @include('pages.admin.penggalangan_dana.form', [
                'proyek' => $proyek,
                'show' => true
            ])
        </div>
    </fieldset>

    <script src="{{ asset('admin/assets/js/jquery.min.js') }}"></script>
    @include('pages.admin.helper.autonumeric')
    <script>
        $(document).ready(function () {
            let totalNeed = initAutoNumeric('input[name=total_need]');
            let initial = initAutoNumeric('input[name=terkumpul]');
            let hargaPaket = initAutoNumeric('input[name=harga_paket]');
            let margin = initAutoNumeric('input[name=profit_margin]');
        });
    </script>
@endsection
