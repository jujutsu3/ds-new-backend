@extends('layouts.admin.master')

@section('title', 'Penggalangan Dana')

@section('content')
    @component('pages.admin.helper.breadcrumbs', [
        'title' => 'Kelola Penggalangan Dana'
    ])
{{--        <h1>--}}
{{--            <a href="{{ route('admin.proyek.download') }}" class="btn btn-success btn-block"> Ekspor Excel</a>--}}
{{--        </h1>--}}
    @endcomponent()

    <div class="content mt-3">
        {{-- change this to backend --}}
        @if(session()->has('message'))
            @component('pages.admin.helper.alert', [
                'type' => session()->get('message_type'),
                'message' => session()->get('message')
            ])
            @endcomponent()
        @endif

        {!! $table->table(['style' => 'width:100%;', 'id' => 'table-project']) !!}
    </div>

    <script src="{{ asset('admin/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/lib/data-table/datatables.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/lib/data-table/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/assets/js/lib/data-table/datatables-init.js') }}"></script>
    {!! $table->scripts() !!}
    @include('pages.admin.helper.datatable_debouncer');
@endsection

