@if ($errors->any())
    <div class="col-lg-12">
        @foreach ($errors->all() as $error)
            @component('pages.admin.helper.alert', ['type' => 'danger'])
                {{ __($error) }}
            @endcomponent
        @endforeach
    </div>
@endif
<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><small> Penggalangan Dana </small><strong>Deskripsi</strong></div>
        <div class="card-body card-block col-lg-12">
            <input type="hidden" name="pengajuan_id" form="form-penggalangan" value="{{ request('pengajuan_id') }}">
            @component('pages.admin.helper.input_horizontal', ['title' => 'Nama Proyek', 'size' => 'col-lg-12'])
                <input form="form-penggalangan" type="text"
                       maxlength="191"
                       name="nama"
                       value="{{ \App\Helpers\Helper::oldVal('nama', $proyek->nama) }}"
                       placeholder="Nama Proyek"
                       class="form-control"
                       required>
            @endcomponent
            @component('pages.admin.helper.input_horizontal', ['title' => 'Alamat Proyek', 'size' => 'col-lg-6'])
                <input form="form-penggalangan" type="text"
                       maxlength="191"
                       name="alamat"
                       value="{{ \App\Helpers\Helper::oldVal('alamat', $proyek->alamat) }}"
                       placeholder="Alamat Proyek"
                       class="form-control"
                       required>
            @endcomponent
            @component('pages.admin.helper.input_horizontal', ['title' => 'Geocode Proyek', 'size' => 'col-lg-6'])
                <input form="form-penggalangan"
                       type="text"
                       maxlength="191"
                       name="geocode"
                       value="{{ \App\Helpers\Helper::oldVal('geocode', $proyek->geocode) }}"
                       placeholder="Geocode Proyek"
                       class="form-control"
                       required>
            @endcomponent
            @component('pages.admin.helper.input_horizontal', ['title' => 'Akad', 'size' => 'col-lg-6'])
                <select name="akad" form="form-penggalangan" class="form-control" required>
                    <option value="" selected disabled>Pilih Akad</option>
                    @foreach (\App\MasterAkad::all() as $akad)
                        <option {{ $akad->id == \App\Helpers\Helper::oldVal('akad', $proyek->akad) ? 'selected' : '' }} value="{{ $akad->id }}">{{ $akad->jenis_akad }}</option>
                    @endforeach
                </select>
            @endcomponent
            @component('pages.admin.helper.input_horizontal', ['title' => 'Margin Keuntungan', 'size' => 'col-lg-6'])
                <input form="form-penggalangan" type="text"
                       name="profit_margin"
                       value="{{ \App\Helpers\Helper::oldVal('profit_margin', $proyek->profit_margin) ?: 1 }}"
                       placeholder="Margin Keuntungan"
                       class="form-control text-right autonumeric-bind"
                       required>
                <div class="input-group-append">
                    <div class="input-group-text">%</div>
                </div>
            @endcomponent
            @component('pages.admin.helper.input_horizontal', ['title' => 'Dana Dibutuhkan Rp', 'size' => 'col-lg-6'])
                <input form="form-penggal Rpangan" type="text"
                       maxlength="191"
                       name="total_need"
                       disabled
                       value="{{ \App\Helpers\Helper::oldVal('total_need', $proyek->total_need) }}"
                       placeholder="Dana Dibutuhkan"
                       class="form-control text-right autonumeric-bind">
            @endcomponent
            @component('pages.admin.helper.input_horizontal', ['title' => 'Harga Paket Rp', 'size' => 'col-lg-6'])
                <input form="form-penggalangan" type="text"
                       name="harga_paket"
                       value="{{ \App\Helpers\Helper::oldVal('harga_paket', $proyek->harga_paket) ?: 1000000 }}"
                       placeholder="Harga Paket"
                       class="form-control text-right autonumeric-bind"
                       required>
            @endcomponent
            <div class="row">
                <div class="col-lg-12">
                    @component('pages.admin.helper.input_horizontal', ['title' => 'Initial Dana Rp', 'size' => 'col-lg-6'])
                        <input form="form-penggalangan" type="text"
                               maxlength="191"
                               name="terkumpul"
                               value="{{ \App\Helpers\Helper::oldVal('terkumpul', $proyek->terkumpul) }}"
                               placeholder="Initial / Terkumpul"
                               class="form-control text-right autonumeric-bind"
                               required>
                    @endcomponent
                </div>
            </div>
            @component('pages.admin.helper.input_vertical', ['title' => 'Tanggal Mulai Penggalangan', 'size' => 'col-lg-5'])
                <input form="form-penggalangan" type="date"
                       name="tgl_mulai_penggalangan"
                       min="{{ \Carbon\Carbon::now()->toDateString() }}"
                       value="{{ \App\Helpers\Helper::oldVal('tgl_mulai_penggalangan', $proyek->tgl_mulai_penggalangan) }}"
                       placeholder="Tanggal Mulai Penggalangan"
                       class="form-control"
                       required>
            @endcomponent
            @component('pages.admin.helper.input_vertical', ['title' => 'Tanggal Berakhir Penggalangan', 'size' => 'col-lg-5'])
                <input form="form-penggalangan" type="date"
                       name="tgl_selesai_penggalangan"
                       value="{{ \App\Helpers\Helper::oldVal('tgl_selesai_penggalangan', $proyek->tgl_selesai_penggalangan) }}"
                       placeholder="Tanggal Berakhir Penggalangan"
                       {{ $proyek->id === null ? "readonly" : '' }}
                       class="form-control"
                       required>
                <small class="form-text text-muted">Harap isi tanggal mulai dahulu</small>
            @endcomponent
            @component('pages.admin.helper.input_vertical', ['title' => 'Jumlah Hari', 'size' => 'col-lg-2'])
                <input form="form-penggalangan"
                       type="text"
                       name="jumlah_hari"
                       maxlength="191"
                       value="{{ \Carbon\Carbon::parse($proyek->tgl_selesai_penggalangan)->diffInDays(\Carbon\Carbon::parse($proyek->tgl_mulai_penggalangan)) }}"
                       disabled
                       placeholder="Jumlah Hari"
                       class="form-control text-right">
            @endcomponent
            @component('pages.admin.helper.input_vertical', ['title' => 'Tanggal Mulai Proyek', 'size' => 'col-lg-5'])
                <input form="form-penggalangan" type="date"
                       disabled
                       name="tgl_mulai"
                       value="{{ \App\Helpers\Helper::oldVal('tgl_mulai', \Carbon\Carbon::parse($proyek->tgl_mulai)->format('Y-m-d')) }}"
                       placeholder="Tanggal Mulai Proyek"
                       class="form-control"
                       required>
            @endcomponent
            @component('pages.admin.helper.input_vertical', ['title' => 'Tanggal Berakhir Proyek', 'size' => 'col-lg-5'])
                <input form="form-penggalangan" type="date"
                       disabled
                       name="tgl_selesai"
                       value="{{ \App\Helpers\Helper::oldVal('tgl_selesai', \Carbon\Carbon::parse($proyek->tgl_selesai)->format('Y-m-d')) }}"
                       placeholder="Tanggal Berakhir Proyek"
                       class="form-control"
                       required>
            @endcomponent
            @component('pages.admin.helper.input_vertical', ['title' => 'Tenor Proyek (bln)', 'size' => 'col-lg-2'])
                <input form="form-penggalangan"
                       type="number"
                       min="1"
                       max="180"
                       name="tenor_waktu"
                       value="{{ \App\Helpers\Helper::oldVal('tenor_waktu', $proyek->tenor_waktu) }}"
                       placeholder="Tenor Proyek (bln)"
                       class="form-control autonumeric-bind text-right"
                       required>
            @endcomponent
            @component('pages.admin.helper.input_vertical', ['title' => 'Tampilkan Iklan', 'size' => 'col-lg-2'])
                <div class="form-control">
                    <input form="form-penggalangan"
                           type="radio"
                           name="status_tampil"
                           {{ "1" == \App\Helpers\Helper::oldVal('status_tampil', $proyek->status_tampil) ? 'checked' : '' }}
                           id="status_tampil_ya"
                           required
                           value="1">
                    <label for="status_tampil_ya">Ya</label>
                    <input form="form-penggalangan"
                           type="radio"
                           name="status_tampil"
                           {{ "0" == \App\Helpers\Helper::oldVal('status_tampil', $proyek->status_tampil) ? 'checked' : '' }}
                           id="status_tampil_tidak"
                           required
                           value="0">
                    <label for="status_tampil_tidak">Tidak</label>
                </div>
            @endcomponent
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><small> Penggalangan Dana </small><strong>Details</strong></div>
        <div class="card-body card-block col-lg-12">
            <ul class="nav nav-pills" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active"
                       id="deskripsi-tab-nav"
                       data-toggle="tab"
                       href="#deskripsi-tab"
                       role="tab"
                       aria-controls="deskripsi-tab"
                       aria-selected="true">Deskripsi</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link"
                       id="pemilik-tab-nav"
                       data-toggle="tab"
                       href="#pemilik-tab"
                       role="tab"
                       aria-controls="pemilik-tab"
                       aria-selected="false">Pemilik</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link"
                       id="legalitas-tab-nav"
                       data-toggle="tab"
                       href="#legalitas-tab"
                       role="tab"
                       aria-controls="legalitas-tab"
                       aria-selected="false">Legalitas</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active"
                     id="deskripsi-tab"
                     role="tabpanel"
                     aria-labelledby="deskripsi-tab">
                    <div class="row">
                        @component('pages.admin.helper.input_vertical', ['title' => '', 'size' => 'col-lg-12'])
                            <textarea name="desc[deskripsi]"
                                      id="textarea-desc-deskripsi"
                                      form="form-penggalangan"
                                      class="form-control tinymce-validate">{{ \App\Helpers\Helper::oldVal('desc.deskripsi', $proyek->deskripsiProyekRel ? $proyek->deskripsiProyekRel->deskripsi : '') }}</textarea>
                        @endcomponent
                    </div>
                </div>
                <div class="tab-pane fade" id="pemilik-tab" role="tabpanel" aria-labelledby="pemilik-tab">
                    <div class="row">
                        @component('pages.admin.helper.input_vertical', ['title' => '', 'size' => 'col-lg-12'])
                            <textarea name="desc[pemilik]"
                                      id="textarea-desc-pemilik"
                                      form="form-penggalangan"
                                      class="form-control">{{ \App\Helpers\Helper::oldVal('desc.pemilik', $proyek->pemilikProyekRel ? $proyek->pemilikProyekRel->deskripsi_pemilik : '') }}</textarea>
                        @endcomponent
                    </div>
                </div>
                <div class="tab-pane fade" id="legalitas-tab" role="tabpanel" aria-labelledby="legalitas-tab">
                    <div class="row">
                        @component('pages.admin.helper.input_vertical', ['title' => '', 'size' => 'col-lg-12'])
                            <textarea name="desc[legalitas]"
                                      id="textarea-desc-legalitas"
                                      form="form-penggalangan"
                                      class="form-control tinymce-validate">{{ \App\Helpers\Helper::oldVal('desc.legalitas', $proyek->legalitasProyekRel ? $proyek->legalitasProyekRel->deskripsi_legalitas : '') }}</textarea>
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><small> Penggalangan Dana </small><strong>Gambar</strong></div>
        <div class="card-body card-block col-lg-12">
            @component('pages.admin.helper.input_vertical', ['title' => 'Thumbnail Profile Proyek 350x233', 'size' => 'col-lg-12'])
                @if ($show ?? false)
                    <br>
                    <img src="{{ \Illuminate\Support\Facades\Storage::url($proyek->gambar_utama) }}"
                         alt="Gambar Utama {{ $proyek->nama }}">
                @else
                    <input name="files[gambar_utama]"
                           type="file"
                           form="form-penggalangan"
                           placeholder="Gambar Utama"
                           class="form-control"
                        {{ $proyek->id === null ? 'required' : '' }}>
                    @if ($proyek->gambar_utama)
                        Foto Saat Ini :
                        <br>
                        <img src="{{ \Illuminate\Support\Facades\Storage::url($proyek->gambar_utama) }}"
                             alt="Gambar Utama {{ $proyek->nama }}">
                    @endif
                @endif
            @endcomponent
            @component('pages.admin.helper.input_vertical', ['title' => 'Slider Profile Proyek 730x486 (multiple)', 'size' => 'col-lg-12'])
                @if ($show ?? false)
                    @foreach ($proyek->gambarProyek as $gambar)
                        <br>
                        <img src="{{ \Illuminate\Support\Facades\Storage::url($gambar->gambar) }}"
                             alt="Gambar Proyek {{ $proyek->nama }}">
                    @endforeach
                @else
                    <input name="files[slider][]"
                           type="file"
                           multiple
                           form="form-penggalangan"
                           placeholder="Gambar Slider"
                           class="form-control"
                        {{ $proyek->id === null ? 'required' : '' }}>
                        @if ($proyek->gambarProyek()->exists())
                            Foto Saat Ini :
                            <br>
                            @foreach ($proyek->gambarProyek as $gambar)
                                <br>
                                <img src="{{ \Illuminate\Support\Facades\Storage::url($gambar->gambar) }}"
                                     alt="Gambar Proyek {{ $proyek->nama }}">
                            @endforeach
                        @endif
                @endif
            @endcomponent
        </div>
    </div>
</div>

<script src="{{ asset('admin/assets/js/lib/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/lib/moment/moment.min.js') }}"></script>
@include('pages.admin.helper.autonumeric')
<script>
    let totalNeed = initAutoNumeric('input[name=total_need]', {
        minimumValue: 0,
    });
    let initial = initAutoNumeric('input[name=terkumpul]', {
        minimumValue: 0,
    });
    let hargaPaket = initAutoNumeric('input[name=harga_paket]', {
        minimumValue: 1000000,
        wheelStep: 1000000
    });
    let margin = initAutoNumeric('input[name=profit_margin]', {
        minimumValue: 1,
        maximumValue: 99,
        wheelStep: 0.1
    });
    tinymce.init({
        selector: '#textarea-desc-deskripsi',
        setup: function (editor) {
            editor.on('init change', function () {
                editor.save();
            });
        },
        readonly: $('#textarea-desc-deskripsi').parents('fieldset').attr('disabled')
    });
    tinymce.init({
        selector: '#textarea-desc-pemilik',
        setup: function (editor) {
            editor.on('init change', function () {
                editor.save();
            });
        },
        readonly: $('#textarea-desc-pemilik').parents('fieldset').attr('disabled')
    });
    tinymce.init({
        selector: '#textarea-desc-legalitas',
        setup: function (editor) {
            editor.on('init change', function () {
                editor.save();
            });
        },
        readonly: $('#textarea-desc-legalitas').attr('disabled')
    });

    $(document).ready(function () {
        let $tglMulai = $('input[name=tgl_mulai_penggalangan]');
        let $tglSelesai = $('input[name=tgl_selesai_penggalangan]');
        let $tglMulaiProyek = $('input[name=tgl_mulai]');
        let $tglSelesaiProyek = $('input[name=tgl_selesai]');
        let $tenor = $('input[name=tenor_waktu]');

        $tglMulai.on('change', function () {
            $tglSelesai
                .attr('min', moment($tglMulai.val()).add(1, 'day').format('YYYY-MM-DD'))
                .removeAttr('readonly');
        })
            .trigger('change');

        $tglSelesai.on('change', function () {
            $('input[name=jumlah_hari]')
                .val(moment($tglSelesai.val()).diff(moment($tglMulai), 'days'));
            let momentTglSelesai = moment($tglSelesai.val()).add(1, 'day');
            $tglMulaiProyek
                .val(momentTglSelesai.format('YYYY-MM-DD'));
            $tenor.trigger('change');
            $('input[name=waktu_bagi]').val(momentTglSelesai.format('DD'))
        });

        $tenor.on('change', function () {
            $tglSelesaiProyek
                .val(moment($tglMulaiProyek.val()).add($(this).val(), 'month').format('YYYY-MM-DD'));
        });

        $('form').on('submit', function () {
            let allPass = true;
            $('.tinymce-validate').each(function () {
                if (($(this)).val() === '') {
                    allPass = false;
                    return false;
                }
            });

            if (! allPass) {
                alert('Harap isi semua field "Details"!');

                return false;
            }

            $(this).find('button[type=submit]').attr('disabled');
            return true;
        });
    });
</script>
