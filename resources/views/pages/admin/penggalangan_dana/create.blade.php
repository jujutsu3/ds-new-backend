@extends('layouts.admin.master')

@section('title', 'Penggalangan Dana')

@section('content')
    @component('pages.admin.helper.breadcrumbs', [
    'title' => 'Buat Penggalangan Dana Baru'
    ])
    @endcomponent()

    @php
        $proyek = new \App\Proyek();
        $proyek->forceFill([
            'nama' => $pengajuan->pendanaan_nama,
            'alamat' => $pengajuan->lokasi_lengkap,
            'akad' => $pengajuan->pendanaan_akad,
            'total_need' => $pengajuan->pendanaan_dana_dibutuhkan
        ]);
    @endphp

    <div class="col-lg-12">
        @if(session()->has('message'))
            @component('pages.admin.helper.alert', [
                'type' => session()->get('message_type'),
                'message' => session()->get('message')
            ])
            @endcomponent()
        @endif
    </div>

    <div class="col-lg-12">
        <form action="{{ route('penggalangan_dana.store') }}"
              method="POST"
              class="row"
              id="form-penggalangan"
              enctype="multipart/form-data">
            @csrf
            @include('pages.admin.penggalangan_dana.form', [
                'proyek' => $proyek
            ])
        </form>
    </div>
    <div class="col-lg-2">
        <a href="{{ route('penggalangan_dana.manage') }}">
            <button type="button"
                    form="form-penggalangan"
                    value="cancel"
                    class="btn btn-secondary btn-block">Batal
            </button>
        </a>
    </div>
    <div class="col-lg-2">
        <button type="submit" form="form-penggalangan" value="save" class="btn btn-primary btn-block">Simpan</button>
    </div>
{{--    <div class="col-lg-2">--}}
{{--        <button type="submit"--}}
{{--                name="approve"--}}
{{--                form="form-penggalangan"--}}
{{--                value="approve"--}}
{{--                class="btn btn-success btn-block">Setujui--}}
{{--        </button>--}}
{{--    </div>--}}

    <script src="{{ asset('admin/assets/js/jquery.min.js') }}"></script>
@endsection
