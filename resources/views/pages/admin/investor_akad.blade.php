@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('content')
    <style>
        .sweet_loader {
            width: 140px;
            height: 140px;
            margin: 0 auto;
            animation-duration: 0.5s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-name: ro;
            transform-origin: 50% 50%;
            transform: rotate(0) translate(0,0);
        }
        @keyframes ro {
            100% {
                transform: rotate(-360deg) translate(0,0);
            }
        }
    </style>

    {{-- START: Content Title --}}
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Kelola Akad Pendana</h1>
                </div>
            </div>
        </div>
    </div>
    {{-- END: Content Title --}}

    <div class="content mt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger col-sm-12 d-none" id="error_search">
                        Data tidak ditemukan
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="card" id="view_card_search">
                        <div class="card-header">
                            <strong class="card-title">Pencarian Pendana</strong>
                        </div>
                        <div class="card-body">
                            <form action="/admin/investor/data" method="POST" id="form_search">
                                <div class="form-group row">
                                    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="nama" class="form-control" id="nama" autocomplete="off" autofocus placeholder="masukan nama investor">
                                        <div id="nama_list"></div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="username" class="col-sm-3 col-form-label">Akun</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="username" class="form-control" id="username" autocomplete="off" placeholder="masukan username investor">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="searchEmail" class="col-sm-3 col-form-label">Email</label>
                                    <div class="col-sm-9">
                                        <input type="email" name="searchEmail" class="form-control" id="searchEmail" autocomplete="off" placeholder="masukan email investor">
                                    </div>
                                </div>

                                <button class="btn rounded btn-primary float-right" type="submit" id="search">Cari</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card d-none" id="view_card_table">
                        <div class="card-header">
                            <strong class="card-title">Data Pendana</strong>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="table_investor" class="table table-striped table-bordered">
                                    <thead class="align-middle text-center">
                                        <tr>
                                            <th class="d-none">Id</th>
                                            <th>No</th>
                                            <th>Nama Pendaan</th>
                                            <th>Email </th>
                                            <th>Akun</th>
                                            <th>Jumlah Pendanaan</th>
                                            <th>Total Pendanaan</th>
                                            <th>Dana tidak teralokasi</th>
                                            <th>Akad</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="align-middle text-center">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- START: Modal Akad --}}
            <div class="row">
                <div class="col-12">
                    <div id="modalWakalah"  class="modal fade in" role="dialog">
                        <div class="modal-dialog modal-lg modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
				    <h5 class="modal-title">Akad Wakalah Bil Ujroh</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" id="modalBodyWakalah">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe id="linkWakalah" class="embed-responsive-item" allowfullscreen></iframe>
                                    </div>
                                    {{-- <iframe id="linkWakalah" scrolling="yes" height="550"></iframe> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- END: Modal Akad --}}
        </div>
    </div>

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        numberFormat = (num) => {
            let str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
            if(str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for(let j = 0, len = str.length; j < len; j++) {
                if(str[j] != ",") {
                    output.push(str[j]);
                    if(i%3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");
            return(formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        }
        
        $(document).ready(function(){
            let investorTable =  $('#table_investor').DataTable({});
            let sweet_loader;

            $('#nama').on('keyup',function(e){
                e.preventDefault();
                let query = $(this).val();
                if (!query) {
                    $('#nama_list').addClass('d-none');
                    $('#nama_list').html();
                    $('#nama_list').fadeOut();
                } else {
                    $('#nama_list').removeClass('d-none');

                    $.ajax({
                        url : "/admin/investor/akad/nama_investor_autocomplete",
                        method : "post",
                        data : {query:query},
                        success:function(data)
                        {
                            $('#nama_list').fadeIn();
                            $('#nama_list').html(data);
                        }
                    });
                }
            })

            $(document).on('click','li',function(){
                $('#nama').val($(this).text());
                $('#nama_list').addClass('d-none');
                $('#nama_list').fadeOut();
            })

            $("#form_search").submit(function(e) {
                e.preventDefault();
                let name         = $.trim($('#nama').val());
                let unallocated  = null;
                let username     = $('#username').val();
                let search_email = $('#searchEmail').val();

                if (name == '') {
                    name = null;
                }
                if (username == '') {
                    username = null;
                }
                if (search_email == '') {
                    search_email = null;
                }

                if (name == null && username == null && search_email == null &&  unallocated == null) {
                    swal.fire({
                        title: "Peringatan",
                        text: "Salah satu field harus terisi",
                        type: "warning",
                    })

                    return false
                }

                sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';
                let dataSearch = { name:name, unallocated:unallocated, username:username, search_email:search_email };

                $.ajax({
                    url: $(this).attr("action"),
                    method: $(this).attr("method"),
                    dataType: 'json',
                    data : dataSearch,

                    beforeSend: function() {
                        swal.fire({
                            html: '<h5>Mencari...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onRender: function() {
                                $('.swal2-content').prepend(sweet_loader);
                            }
                        });
                    },

                    success: function(response){
                        swal.close();
                        investorTable.destroy();
                        let user_id;
                        if (response.status == 'Ada') {
                            $( "#view_card_table" ).removeClass( "d-none" );
                            $('#error_search').addClass('d-none');
                            investorTable = $('#table_investor').DataTable({
                                searching: false,
                                processing: true,
                                // serverSide: true,
                                ajax: {
                                    url: '/admin/investor/data_mutasi_datatables',
                                    type: 'post',
                                    data: dataSearch,
                                    dataSrc: 'data'
                                },
                                paging: true,
                                info: true,
                                lengthChange:false,
                                order: [ 1, 'asc' ],
                                pageLength: 10,
                                columns: [
                                    { data: 'idInvestor'},
                                    { data : null,
                                        render: function (data, type, row, meta) {
                                                return  meta.row+1;
                                        }
                                    },
                                    { data: null,
                                        render:function(data,type,row,meta){
                                            if (row.status == 'notfilled' || row.status == 'Not Active') {
                                                return '-'
                                            } else if (row.status == 'active' || row.status == 'pending' || row.status == 'suspend' || row.status == 'reject') {
                                                return row.nama_investor
                                            }
                                        }
                                    },
                                    { data: 'email'},
                                    { data: 'username'},
                                    { data: null,
                                        render:function(data,type,row,meta){
                                            if (row.total == null) {
                                                return 0
                                            } else {
                                                return row.total - row.total_log
                                            }
                                        }
                                    },
                                    { data: null,
                                        render:function(data,type,row,meta){
                                            if (row.jumlah_nominal == null) {
                                                return 0;
                                            }  else {
                                                return numberFormat(row.jumlah_nominal - row.jumlah_nominal_log);
                                            }
                                        }
                                    },
                                    { data: null,
                                        render:function(data,type,row,meta){
                                            if (row.unallocated == '') {
                                                return "Don't Have VA";
                                            }  else {
                                                if (row.unallocated == null) {
                                                    return 0
                                                } else {
                                                    return numberFormat(row.unallocated)
                                                }
                                            }
                                        }  
                                    },
                                    { data: null,
                                        render:function(data,type,row,meta){
                                            // return '<a class="btn btn-success" id="unduh_akad" target="_blank" href="/admin/investor/akad/docTokenExist/'+row['idInvestor']+'">download</a>';
                                            if (row['jumlah_nominal'] == null && row['unallocated'] == null) {
                                                return "-"
                                            } else {
                                                return '<button class="btn btn-success" id="unduh_akad"><span class="ti-import"></span></button>';
                                            }
                                        }
                                    },
                                    { data: null,
                                        render:function(data,type,row,meta){
                                            if (row.status == 'notfilled')  {
                                                return '<button class="btn btn-outline-danger btn-sm active" disabled>Notfilled</button>';
                                            }  else if(row.status == 'Not Active') {
                                                return '<button class="btn btn-outline-danger btn-sm active" disabled>Not Active</button>';
                                            } else if (row.status == 'pending') {
                                                return '<button class="btn btn-outline-danger btn-sm active" disabled>Pending</button>';
                                            }  else if (row.status == 'reject') {
                                                return '<button class="btn btn-outline-danger btn-sm active" disabled>Reject</button>';
                                            } else if (row.status == 'active') {
                                                return '<button class="btn btn-outline-success btn-sm active" disabled>Active</button>';
                                            } else if (row.status == 'suspend') {
                                                return '<button class="btn btn-outline-secondary btn-sm active" disabled>Suspend</button>';
                                            }
                                        }
                                    }
                                ],
                                columnDefs: [
                                    { targets: [0], visible: false}
                                ]
                            });
                        } else {
                            $('#error_search').removeClass('d-none');
                        }
                    },
                    
                    error:function(response) {
                        swal.close()
                        swal.fire({
                            title: "Error",
                            text: "Internal error",
                            type: "error",
                        })
                    }
                });
            });

            // START: Unduh Akad
            $('#table_investor tbody').on('click', '#unduh_akad', function(){
                let data = investorTable.row( $(this).parents('tr') ).data();
                let user_id = data.idInvestor;
                
                $.ajax({
                    url:'/admin/investor/akad/isTreshold/'+user_id,
                    method:'get',

                    beforeSend: function() {
                        swal.fire({
                            html: '<h5>Loading...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onRender: function() {
                                $('.swal2-content').prepend(sweet_loader);
                            }
                        });
                        $('#linkWakalah').attr('src', '');
                    },
                    
                    success: function (response) {
                        swal.close();
                        if (response.status == 'Berhasil') {
                            $('#modalWakalah').modal('show').addClass('modal fade show in');
                            $('#linkWakalah').attr('src',"{{ url('user/viewWakalah') }}/"+user_id);
                        } else if(response.status == 'privy') {
                            $('#modalWakalah').modal('show').addClass('modal fade show in');
                            $('#linkWakalah').attr('src', response.downloadURL);
                        } else {
                            swal.fire({
                                title: "Error",
                                text: "Internal Error",
                                type: "error",
                            })
                        }
                    },
                    error: function(response) {
                        swal.fire({
                            title: "Error",
                            text: "Internal Error",
                            type: "error",
                        })
                    }
                });
            })
            // END: Unduh Akad
        });

    </script>
@endsection