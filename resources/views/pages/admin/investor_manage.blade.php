@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('style')
<!-- <link rel="stylesheet" href="{{asset('css/jquery_step/jquery.steps.css')}}"> -->
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />

@push('add-more-style')
<style>
    .modal-mutasi {
        max-width: 80% !important;
    }

    .btn-upload {
        display: block;
        border-radius: 0px;
        box-shadow: 0px 4px 6px 2px rgba(0, 0, 0, 0.2);
        margin-top: -5px;
        width: 200px;
    }

    .imagePreview {
        width: 200px;
        height: 200px;
        background-position: center center;
        background: url(http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg);
        background-color: #fff;
        background-size: cover;
        background-repeat: no-repeat;
        display: inline-block;
        box-shadow: 0px -3px 6px 2px rgba(0, 0, 0, 0.2);
    }

    .imgUp {
        margin-bottom: 15px;
        margin-right: 85px;
    }

    #overlay {
        position: fixed;
        top: 0;
        left: 0;
        z-index: 900;
        width: 100%;
        height: 100%;
        display: none;
        background: rgba(0, 0, 0, 0.6);
    }

    .cv-spinner {
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .spinner {
        width: 40px;
        height: 40px;
        border: 4px #ddd solid;
        border-top: 4px #2e93e6 solid;
        border-radius: 50%;
        animation: sp-anime 0.8s infinite linear;
    }

    @keyframes sp-anime {
        100% {
            transform: rotate(360deg);
        }
    }

    .is-hide {
        display: none;
    }

    .bg-green-dsi {
        background-color: #18783A
    }

    .card-header-pengurus[aria-expanded=false] .fa-angle-up {
        display: none;
    }

    .card-header-pengurus[aria-expanded=true] .fa-angle-down {
        display: none;
    }

    .line {
        display: flex;
        flex-direction: row;
    }

    .line:after {
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #000;
        margin: auto;
    }

    .error {
        color: #F00;
        margin-top: 1px
    }

    #va_bsi-error,
    #va_bsi_bdn_hukum-error,
    #no_tlp_badan_hukum-error {
        position: absolute;
        top: 100%;
    }

    button:disabled {
        cursor: not-allowed;
        pointer-events: all !important;
    }
</style>
@endpush
@endsection

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Kelola Pendana</h1>
            </div>
        </div>
    </div>
</div>
<div id="overlay">
    <div class="cv-spinner">
        <span class="spinner"></span>
    </div>
</div>
<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if (session('error'))
            <div class="alert alert-danger col-sm-12">
                {{ session('error') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif (session('success'))
            <div class="alert alert-success col-sm-12">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session('updated'))
            <div class="alert alert-success col-sm-12">
                {{ session('updated') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session('exist'))
            <div class="alert alert-danger col-sm-12">
                {{ session('exist') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session('changepass'))
            <div class="alert alert-success col-sm-12">
                {{ session('changepass') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="alert alert-success col-sm-12" id="error_search" style="display: none;">
                Data Tidak Ditemukan !
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card" id="view_card_search">
                <div class="card-header">
                    <strong class="card-title">Pencarian Pendana</strong>
                </div>
                <div class="card-body">
                    <form class="form" id="form_search">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">Nama</label>
                            <div class="col-lg-10">
                                <input type="text" name="nama" class="form-control" id="nama" autocomplete="off"
                                    autofocus placeholder="Masukkan nama lender">
                                <div id="nama_list"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">Akun</label>
                            <div class="col-lg-10">
                                <input type="text" name="username" class="form-control" id="username" autocomplete="off"
                                    placeholder="Masukkan username lender"
                                    onkeyup="this.value = this.value.replace(/\s/g, '');">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">Email</label>
                            <div class="col-lg-10">
                                <input type="email" name="searchEmail" class="form-control" id="searchEmail"
                                    autocomplete="off" placeholder="Masukkan email lender" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">No VA</label>
                            <div class="col-lg-10">
                                <input type="text" name="in_va_number" class="form-control" id="in_va_number"
                                    autocomplete="off" placeholder="Masukkan nomor va lender"
                                    onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">No Telepon</label>
                            <div class="col-lg-10">
                                <div class="row">
                                    <div class="col-md-4 pr-md-0 pt-md-0 pt-2">
                                        <select name="in_kode_operator" id="in_kode_operator"
                                            class="form-control custom-select"
                                            onchange="$('#in_no_hp').val('');$('#card-va-number').addClass('d-none')">
                                            @foreach ($master_kode_operator as $item)
                                            <option value="{{$item->kode_operator}}" {{ $item->kode_operator == '62' ?
                                                'selected' : ''}}>
                                                ({{$item->kode_operator}}) {{$item->negara}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pt-md-0 pt-2">
                                        <input type="text" class="form-control ff-style" id="in_no_hp" maxlength="15"
                                            name="in_no_hp"
                                            placeholder="Masukkan no handphone lender, contoh: 831xxxxxx"
                                            onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label form-control-label">Dana Tidak Teralokasi</label>
                            <div class="col-lg-10">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="unallocated" id="yes"
                                        value="yes">
                                    <label class="form-check-label" for="yes">Ya</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="unallocated" id="no" value="no">
                                    <label class="form-check-label" for="no">Tidak</label>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary rounded float-right mx-2" type="submit" id="search"> <i
                                class="fa fa-search"></i> Cari</button>
                    </form>
                </div>
            </div>

            <div class="card" id="view_card_table" style="display: none;">
                <div class="card-header">
                    <strong class="card-title">Data Pendana</strong>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table_investor" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="display: none;">Id</th>
                                    <th>No</th>
                                    <th>Nama Pendana</th>
                                    <th>Email </th>
                                    <th>Akun</th>
                                    <th>Jumlah Pendanaan</th>
                                    <th>Total Pendanaan</th>
                                    <th>Dana tidak teralokasi</th>
                                    <th>Informasi Pendana</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- start modal detil investor -->
    <div class="modal fade" id="detil" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true"
        data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Detil Pendana : </h5>
                    <button type="button" class="close" id="close_modal_detil" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="form-detil-investor" action="{{route('admin.investor.update')}}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body detil_pendana">
                        <div class="col-lg-12">
                            <div class="card-body card-block">
                                <input type="hidden" name="investor_id" id="investor_id">
                                <input type="hidden" name="tipe_pengguna" id="tipe_pengguna">
                                <input type="hidden" name="tipe_proses" id="tipe_proses">

                                <p class="h6 my-4 line"><b>Informasi Akun &nbsp;</b></p>
                                <fieldset>
                                    <div class="form-row">
                                        <div class="form-group col-sm-6">
                                            <label id="label_username" for="username" class="font-weight-normal">Akun
                                                <span class="text-danger">*</span></label>
                                            <input type="hidden" id="is_username_unique">
                                            <input type="hidden" id="old_username" name="old_username">
                                            <input type="text" name="username" class="form-control"
                                                placeholder="Username"
                                                onkeyup="this.value = this.value.replace(/\s/g, '');"
                                                onchange="checkUserName(this.value, 'label_username', 'is_username_unique')"
                                                required>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label id="label_email" for="email" class="font-weight-normal">Email
                                                <span class="text-danger">*</span></label>
                                            <input type="hidden" id="is_email_unique">
                                            <input type="email" id="email" name="email" class="form-control"
                                                placeholder="Email"
                                                onchange="checkEmail(this.value, 'label_email', 'is_email_unique')"
                                                required>
                                            <div>
                                                {{-- <span id="error_email"
                                                    style="color:red;font-size:11px;margin-left:240px"></span> --}}
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>

                                <div id="layout-individu">
                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Informasi Pribadi &nbsp;</b></p>
                                    <fieldset>
                                        <div class="form-row">
                                            <div class="form-group col-sm-6">
                                                <label for="nama" class="font-weight-normal">Nama</label>
                                                <input type="text" name="nama" class="form-control" placeholder="Nama" required>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="jenis_kelamin" class="font-weight-normal">Jenis
                                                    Kelamin</label>
                                                <select name="jenis_kelamin" class="form-control custom-select">
                                                    <option value="">-- Pilih Satu--</option>
                                                    @foreach($master_jenis_kelamin as $b)
                                                    <option value="{{ $b->id_jenis_kelamin }}">{{ $b->jenis_kelamin
                                                        }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">

                                            <div class="form-group col-sm-6">
                                                <label for="tanggal_lahir" class="font-weight-normal">Tanggal
                                                    Lahir</label>
                                                <input type="date" class="form-control" id="tanggal_lahir"
                                                    name="tanggal_lahir" placeholder="Tanggal Lahir">

                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="tempat_lahir" class="font-weight-normal">Tempat
                                                    Lahir</label>
                                                <input type="text" name="tempat_lahir" class="form-control"
                                                    placeholder="Tempat Lahir">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-sm-6">
                                                <label for="jenis_kelamin" class="font-weight-normal">Status
                                                    Perkawinan</label>
                                                <select name="status_kawin" class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach($master_kawin as $kawin)
                                                    <option value="{{$kawin->id_kawin}}" {{!empty($detil->
                                                        status_kawin_investor)
                                                        && $kawin->id_kawin == $detil->status_kawin_investor ?
                                                        'selected=selected' : ''}}>
                                                        {{$kawin->jenis_kawin}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="agama" class="font-weight-normal">Agama</label>
                                                <select id="agama" name="agama" class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach ($master_agama as $b)
                                                    <option value="{{$b->id_agama}}">{{$b->agama}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label id="label_jenis_identitas" for="jenis_identitas"
                                                    class="font-weight-normal">Jenis Identitas</label>
                                                <select id="jenis_identitas" name="jenis_identitas"
                                                    class="form-control custom-select">
                                                    <option value="1">KTP</option>
                                                    <option value="2">Passport</option>

                                                </select>
                                                {{-- <input type="hidden" name="jenis_identitas" id="jenis_identitas">
                                                --}}
                                            </div>
                                            <div class="form-group col-sm-6 layout-ktp" id="data_perorangan_1">
                                                <label id="label_no_ktp" for="no_ktp" class="font-weight-normal">No
                                                    KTP</label>
                                                <input type="hidden" id="is_ktp_unique" value="0">
                                                <input type="hidden" id="old_no_ktp">
                                                <input type="text" name="no_ktp" id="no_ktp" class="form-control"
                                                    placeholder="No KTP" maxlength="16" minlength="16"
                                                    onkeyup="this.value = this.value.replace(/[^0-9]/g, '');"
                                                    onchange="checkKtporPassport(this.value, 'label_no_ktp', 1, 'is_ktp_unique')">
                                            </div>
                                            <div class="form-group col-sm-6 layout-passport" id="data_perorangan_1">
                                                <label id="label_no_passpor" for="no_passpor"
                                                    class="font-weight-normal">No
                                                    Passport</label>
                                                <input type="hidden" id="is_passpor_unique" value="0">
                                                <input type="hidden" id="old_no_passpor">
                                                <input type="text" name="no_passpor" id="no_passpor"
                                                    class="form-control" placeholder="No passport" 
                                                    onkeyup="this.value = this.value.replace(/[^0-9 a-z A-Z]/g, '');"
                                                    onchange="checkKtporPassport(this.value, 'label_no_passpor', 2, 'is_passpor_unique')">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="warga_negara" class="font-weight-normal">Warga
                                                    Negara</label>
                                                <select id="warga_negara" name="warga_negara"
                                                    class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach ($master_negara as $b)
                                                    <option value="{{$b->id_negara}}">
                                                        {{$b->negara}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-4">
                                                <label for="no_npwp" class="font-weight-normal">No. NPWP</label>
                                                <input type="text" name="no_npwp" class="form-control"
                                                    placeholder="No NPWP" minlength="15" maxlength="15"
                                                    onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                                                <input type="hidden" name="npwp_exist">
                                            </div>
                                            <div class="form-group col-sm-2">
                                                <label for="is_valid_npwp" class="font-weight-normal">NPWP Valid</label>
                                                <input type="text" name="is_valid_npwp" class="form-control"
                                                    placeholder="NPWP Valid" readonly>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label for="kode_operator">Kode Operator <i class="text-danger">*</i> </label>
                                                            <select class="form-control custom-select"
                                                                id="kode_operator" name="kode_operator"
                                                                onchange="checkPhoneNumber('kode_operator', $('#no_telp').val(), 'label_no_hp_inv', 'is_phone_inv_unique')"
                                                                required>

                                                                @foreach ($master_kode_operator as $b)
                                                                <option value="{{$b->kode_operator}}">
                                                                    ({{$b->kode_operator}}) {{$b->negara}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label id="label_no_hp_inv" for="no_telp"
                                                                class="font-weight-normal">No Telp / HP <i class="text-danger">*</i></label>
                                                            <input type="hidden" id="is_phone_inv_unique" value="0">
                                                            <input type="text" name="no_telp" id="no_telp"
                                                                onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); formatPhoneNumber('kode_operator','no_telp')"
                                                                onchange="checkPhoneNumber('kode_operator', this.value, 'label_no_hp_inv', 'is_phone_inv_unique');"
                                                                class="form-control" maxlength="15"
                                                                placeholder="No Telp / HP, Contoh:08xxxxxxxxxx"
                                                                required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="nama_ibu_kandung" class="font-weight-normal">Nama Ibu
                                                    Kandung </label>
                                                <input type="text" name="nama_ibu_kandung" id="nama_ibu_kandung"
                                                    class="form-control" placeholder="Nama Ibu Kandung">
                                            </div>
                                            <div class="form-group col-sm-6" id="data_perorangan_1">
                                                <label for="pendidikan" class="font-weight-normal">Pendidikan</label>
                                                <select name="pendidikan" class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach($master_pendidikan as $pendidikan)
                                                    <option value="{{$pendidikan->id_pendidikan}}" {{!empty($detil->
                                                        pendidikan_investor) && $pendidikan->id_pendidikan ==
                                                        $detil->pendidikan_investor ? 'selected=selected' : ''}}>
                                                        {{$pendidikan->pendidikan}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Informasi Pekerjaan &nbsp;</b></p>
                                    <fieldset>
                                        <div class="form-row">
                                            <div class="form-group col-sm-6">
                                                <label for="sumber_dana" class="font-weight-normal">Sumber Dana
                                                </label>
                                                <input type="text" maxlength="50" name="sumber_dana" id="sumber_dana"
                                                    class="form-control" placeholder="Sumber Dana">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="pekerjaan" class="font-weight-normal">Pekerjaan </label>
                                                <select name="pekerjaan" class="form-control custom-select">
                                                    <option value="">--Pilih--</option>
                                                    @foreach($master_pekerjaan as $pekerjaan)
                                                    <option value="{{$pekerjaan->id_pekerjaan}}" {{!empty($detil->
                                                        pekerjaan_investor) && $pekerjaan->id_pekerjaan ==
                                                        $detil->pekerjaan_investor ? 'selected=selected' : ''}}>
                                                        {{$pekerjaan->pekerjaan}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="bidang_pekerjaan" class="font-weight-normal">Bidang
                                                    Pekerjaan</label>
                                                <select name="bidang_pekerjaan" class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach ($master_bidang_pekerjaan as $b)
                                                    <option value="{{$b->kode_bidang_pekerjaan}}">
                                                        {{$b->bidang_pekerjaan}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="pendapatan" class="font-weight-normal">Pendapatan </label>
                                                <select name="pendapatan" class="form-control custom-select">
                                                    <option value="">--Pilih--</option>
                                                    @foreach($master_pendapatan as $pendapatan)
                                                    <option value="{{$pendapatan->id_pendapatan}}">
                                                        {{$pendapatan->pendapatan}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="pengalaman_kerja" class="font-weight-normal">Pengalaman
                                                    Kerja </label>
                                                <select id="pengalaman_kerja" name="pengalaman_kerja"
                                                    class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach ($master_pengalaman_kerja as $b)
                                                    <option value="{{$b->id_pengalaman_kerja}}">
                                                        {{$b->pengalaman_kerja}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="bidang_online" class="font-weight-normal">Bidang
                                                    Online</label>
                                                <select id="bidang_online" name="bidang_online"
                                                    class="form-control custom-select">
                                                    <option value="">-- Pilih Satu--</option>
                                                    @foreach ($master_online as $b)
                                                    <option value="{{$b->id_online}}">
                                                        {{$b->tipe_online}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Alamat Sesuai dengan KTP &nbsp;</b></p>
                                    <fieldset>
                                        <div class="form-group">
                                            <label for="alamat" class="font-weight-normal">Alamat </label>
                                            <textarea name="alamat" class="form-control col-sm-12" rows="3" id="alamat"
                                                placeholder="Alamat Lengkap"></textarea>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-sm-6">
                                                <label for="domisili negara" class="font-weight-normal">Domisili
                                                    Negara
                                                </label>
                                                <select id="domisili_negara" name="domisili_negara"
                                                    class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach ($master_negara as $b)
                                                    <option value="{{$b->id_negara}}">{{$b->negara}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-sm-6">
                                                <label for="provinsi" class="font-weight-normal">Provinsi</label>
                                                <select name="provinsi" class="form-control custom-select"
                                                    id="provinsi">
                                                    <option value="">-- Pilih Satu--</option>
                                                    @foreach ($master_provinsi as $data)
                                                    <option value={{$data->kode_provinsi}}>{{$data->nama_provinsi}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="kota" class="font-weight-normal">Kota</label>
                                                <select name="kota" class="form-control custom-select" id="kota">
                                                    <option value="">-- Pilih Satu--</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="kecamatan" class="font-weight-normal">Kecamatan </label>
                                                <input type="text" name="kecamatan" class="form-control" id="kecamatan"
                                                    placeholder="Kecamatan">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="kelurahan" class="font-weight-normal">Kelurahan </label>
                                                <input type="text" name="kelurahan" class="form-control" id="kelurahan"
                                                    placeholder="Kelurahan">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="kode_pos" class="font-weight-normal">Kode Pos</label>
                                                <input type="text" name="kode_pos" class="form-control" id="kode_pos"
                                                    maxlength="5" minlength="5"
                                                    onkeyup="this.value = this.value.replace(/[^0-9]/g, '');"
                                                    placeholder="Kode Pos">
                                            </div>
                                        </div>
                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Foto Pendana &nbsp;</b></p>
                                    <fieldset>
                                        <div class="form-row mx-auto">
                                            <div class="col-lg-4 text-center">
                                                <label class="font-weight-normal">Foto Diri <span
                                                        class="text-danger">*</span></label>
                                                <div class="mx-auto" style="width: 200px;">
                                                    <img class="imagePreview rounded float-left img-fluid" id="foto1">
                                                </div>
                                                {{-- <button type="button"
                                                    class="btn btn-block btn-success rounded mt-2"
                                                    onclick="$('#pic_investor').trigger('click');">Unggah
                                                    Foto</button> --}}
                                                <input type="hidden" id="pic_investor_val" name="pic_investor_val">
                                                <input type="file" name="pic_investor" id="pic_investor"
                                                    class="uploadFile imgUp d-none" accept=".jpg, .jpeg, .png,.bmp"
                                                    onchange="$('#foto1').attr('src', window.URL.createObjectURL(this.files[0]))">
                                            </div>
                                            <div class="col-lg-4 text-center">
                                                <label class="font-weight-normal">Foto KTP <span
                                                        class="text-danger">*</span></label>
                                                <div class="mx-auto" style="width: 200px;">
                                                    <img class="imagePreview rounded float-left img-fluid mx-auto"
                                                        id="foto2">
                                                </div>
                                                {{-- <button type="button"
                                                    class="btn btn-block btn-success rounded mt-2"
                                                    onclick="$('#pic_ktp_investor').trigger('click');">Unggah
                                                    Foto</button> --}}
                                                <input type="hidden" id="pic_ktp_investor_val"
                                                    name="pic_ktp_investor_val">
                                                <input type="file" id="pic_ktp_investor" name="pic_ktp_investor"
                                                    class="uploadFile imgUp d-none" accept=".jpg, .jpeg, .png,.bmp"
                                                    onchange="$('#foto2').attr('src', window.URL.createObjectURL(this.files[0]))">
                                            </div>
                                            <div class="col-lg-4 text-center">
                                                <label class="font-weight-normal">Foto Diri dengan
                                                    KTP <span class="text-danger">*</span></label>
                                                <div class="mx-auto imgUp" style="width: 200px;">
                                                    <img class="imagePreview rounded float-left img-fluid" id="foto3">
                                                </div>
                                                {{-- <button type="button"
                                                    class="btn btn-block btn-success rounded mt-2"
                                                    onclick="$('#pic_user_ktp_investor').trigger('click');">Unggah
                                                    Foto</button> --}}
                                                <input type="hidden" id="pic_user_ktp_investor_val"
                                                    name="pic_user_ktp_investor_val">
                                                <input type="file" id="pic_user_ktp_investor"
                                                    name="pic_user_ktp_investor" class="uploadFile imgUp d-none"
                                                    accept=".jpg, .jpeg, .png,.bmp"
                                                    onchange="$('#foto3').attr('src', window.URL.createObjectURL(this.files[0]))">
                                            </div>
                                        </div>
                                        <p class="mt-2 font-weight-normal">Format file .jpg, .jpeg, dan .png
                                        </p>
                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 mb-4 line"><b>Informasi Rekening &nbsp;</b></p>
                                    <fieldset>
                                        <div class="form-row">
                                            <div class="form-group col-sm-6 va_number">
                                                <label for="va_number" class="font-weight-normal">VA BNI </label>
                                                <input type="text" name="va_number" class="form-control"
                                                    value="{{!empty($detil->va_number) ? $detil->va_number : ''}}"
                                                    onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                            </div>
                                            <div class="form-group col-sm-6 va_cimbs">
                                                <label for="va_cimbs" class="font-weight-normal">VA CIMB Syariah</label>
                                                <input type="text" name="va_cimbs" class="form-control"
                                                    value="{{!empty($va->bank022) ? $va->bank022 : ''}}"
                                                    onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                            </div>
                                            <div class="form-group col-sm-6 va_bsi">
                                                <label for="va_bsi" class="font-weight-normal">VA BSI</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="pre_bsi">900</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="va_bsi" name="va_bsi"
                                                        onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                                </div>
                                                {{-- <label for="username" class="font-weight-normal">VA BSI</label>
                                                <input type="hidden" id="va_bsi_val" name="va_bsi_val">
                                                <input type="text" name="va_bsi" class="form-control"
                                                    value="{{!empty($va->bank451) ? $va->bank451 : ''}}"> --}}
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="rekening" class="font-weight-normal">No Rekening </label>
                                                <input type="text" name="rekening" id="rekening" class="form-control"
                                                    maxlength="16" placeholder="No Rekening"
                                                    onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="rekening" class="font-weight-normal">Nama Pemilik
                                                    Rekening </label>
                                                <input type="text" name="nama_pemilik_rek" class="form-control"
                                                    placeholder="Nama Pemilik Rekening">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="bank" class="font-weight-normal">Bank </label>
                                                <select name="bank" class="form-control custom-select">
                                                    <option value="">-- Pilih Satu --</option>
                                                    @foreach($master_bank as $b)
                                                    <option value="{{ $b->kode_bank }}">{{ $b->nama_bank }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6" id="nama_pemilik_rek_asli_label">
                                                <label for="bank" class="font-weight-normal">Nama Pemilik Rekening Asli </label>
                                                <input type="text" name="nama_pemilik_rek_asli" class="form-control"
                                                    placeholder="Nama Pemilik Rekening Asli" readonly>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <p id="is_valid_rekening" class="font-weight-normal"></p>
                                            </div>
                                        </div>
                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 mb-4 line"><b>Informasi Ahli Waris &nbsp;</b></p>
                                    <fieldset>
                                        <div class="form-row">
                                            <div class="form-group col-sm-6">
                                                <label for="nama_ahli_waris" class="font-weight-normal">Nama Ahli
                                                    Waris</label>
                                                <input type="text" id="nama_ahli_waris" name="nama_ahli_waris"
                                                    class="form-control" placeholder="Ketik disini">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="hub_ahli_waris" class="font-weight-normal">Hubungan Ahli
                                                    Waris</label>
                                                <select id="hub_ahli_waris" name="hub_ahli_waris"
                                                    class="form-control custom-select">
                                                    <option value="">-- Pilih Satu--</option>
                                                    @foreach ($master_hub_ahli_waris as $data)
                                                    <option value={{$data->id_hub_ahli_waris}}>{{$data->jenis_hubungan}}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="nik_ahli_waris" class="font-weight-normal">NIK Ahli
                                                    Waris</label>
                                                <input type="text" id="nik_ahli_waris" name="nik_ahli_waris"
                                                    class="form-control" placeholder="Ketik disini" maxlength="16"
                                                    minlength="16"
                                                    onkeyup="this.value = this.value.replace(/[^0-9]/g, '');">
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label for="kode_operator_ahli_waris">Kode Operator</label>
                                                            <select class="form-control custom-select"
                                                                id="kode_operator_ahli_waris"
                                                                name="kode_operator_ahli_waris">
                                                                <option value="">-- Pilih Satu --</option>
                                                                @foreach ($master_kode_operator as $b)
                                                                <option value="{{$b->kode_operator}}">
                                                                    ({{$b->kode_operator}}) {{$b->negara}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-sm-12">
                                                        <div class="form-group">
                                                            <label for="no_hp_ahli_waris" class="font-weight-normal">No
                                                                HP
                                                                Ahli</label>
                                                            <input type="text" name="no_hp_ahli_waris"
                                                                id="no_hp_ahli_waris"
                                                                {{-- onfocusout="checkPhoneNumber(this.value)" --}}
                                                                class="form-control no-zero" placeholder="Contoh:08xxxxxxxxxx"
                                                                onkeyup="this.value = this.value.replace(/[^\d/]/g,''); formatPhoneNumber('kode_operator_ahli_waris','no_hp_ahli_waris')"
                                                                 maxlength="13">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <label for="alamat_ahli_waris" class="font-weight-normal">Alamat</label>
                                                <textarea id="alamat_ahli_waris" name="alamat_ahli_waris"
                                                    class="form-control col-sm-12" rows="3" id="alamat"
                                                    placeholder="Ketik disini"></textarea>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div id="layout-badan-hukum">
                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Informasi Badan Hukum &nbsp;</b></p>
                                    <fieldset>
                                        <div class="form-row">
                                            <!--START: Baris 1 -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nama_badan_hukum">Nama Badan Hukum </label>
                                                    <input type="text" name="nama_badan_hukum" id="nama_badan_hukum"
                                                        class="form-control" placeholder="Masukkan nama badan hukums"
                                                        minlength="3" maxlength="50">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="nib_badan_hukum">Nomor Surat Izin</label>
                                                    <input class="form-control" type="text" id="nib_badan_hukum"
                                                        name="nib_badan_hukum" minlength="3" maxlength="50"
                                                        placeholder="SIUP/TDP/Nomor surat izin lainnya">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="npwp_bdn_hukum">NPWP Perusahaan</label>
                                                    <input class="form-control" type="text" pattern=".{15,15}"
                                                        onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                                                        minlength="15" maxlength="15" id="npwp_bdn_hukum"
                                                        name="npwp_bdn_hukum" placeholder="Masukkan nomor NPWP">
                                                </div>
                                            </div>
                                            <!-- END: Baris 1 -->

                                            <!-- START: Baris 2 -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="label_no_akta_pendirian_bdn_hukum"
                                                        for="no_akta_pendirian_bdn_hukum">No Akta Pendirian </label>
                                                    <input class="form-control" type="text"
                                                        id="no_akta_pendirian_bdn_hukum"
                                                        name="no_akta_pendirian_bdn_hukum" minlength="1"
                                                        placeholder="Masukkan No Akta Pendirian"
                                                        onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()">
                                                </div>
                                            </div>
                                            <div class=" col-md-6">
                                                <div class="form-group">
                                                    <label for="tgl_berdiri_badan_hukum">Tanggal Akta Pendirian </label>
                                                    <input class="form-control" type="date" id="tgl_berdiri_badan_hukum"
                                                        name="tgl_berdiri_badan_hukum" max="{{ date('Y-m-d')}}"
                                                        placeholder="Masukkan Tanggal Berdiri"
                                                        onchange="$(this).valid(); $('#tgl_akta_perubahan_bdn_hukum').attr('min', $(this).val())">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="label_no_akta_perubahan_bdn_hukum"
                                                        for="no_akta_perubahan_bdn_hukum">No Akta Perubahan
                                                        Terakhir</label>
                                                    <input class="form-control" type="text"
                                                        id="no_akta_perubahan_bdn_hukum"
                                                        name="no_akta_perubahan_bdn_hukum" minlength="1"
                                                        placeholder="Masukkan No Akta Perubahan"
                                                        onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="tgl_akta_perubahanbdn_hukum">Tanggal Akta Perubahan
                                                        Terakhir</label>
                                                    <input class="form-control" type="date"
                                                        id="tgl_akta_perubahan_bdn_hukum"
                                                        name="tgl_akta_perubahan_bdn_hukum" max="{{ date('Y-m-d')}}"
                                                        placeholder="Masukkan Tanggal Akta Perubahan Terakhir"
                                                        onchange="$(this).valid()">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label id="label_no_tlp_badan_hukum">Nomor Telepon Perusahaan
                                                    </label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text bg-green-dsi text-white"> +62
                                                            </span>
                                                        </div>
                                                        <input class="form-control no-zero" type="text"
                                                            onkeyup="this.value = this.value.replace(/[^\d/]/g,'')"
                                                            maxlength="13" id="no_tlp_badan_hukum"
                                                            name="no_tlp_badan_hukum"
                                                            placeholder="Masukkan nomor perusahaan">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--END: Baris 2 -->
                                        </div>

                                        <!-- START: Baris 3 -->
                                        <div class="form-row mx-auto">
                                            <div class="col-lg-4 text-center">
                                                <label class="font-weight-normal">Foto NPWP Perusahaan</label>
                                                <input type="hidden" id="foto_npwp_badan_hukum_val"
                                                    name="foto_npwp_badan_hukum_val">
                                                <div class="mx-auto" style="width: 200px;">
                                                    <img class="imagePreview rounded float-left img-fluid"
                                                        id="foto_npwp_badan_hukum">
                                                </div>
                                                {{-- <button type="button" class="btn btn-block btn-success rounded mt-2"
                                                    onclick="$('#pic_npwp_bdn_hukum').trigger('click');">Unggah
                                                    Foto</button> --}}
                                                <input type="file" name="pic_npwp_bdn_hukum" id="pic_npwp_bdn_hukum" class="d-none" accept=".jpg, .jpeg, .png,.bmp"
                                                    onchange="$('#foto_npwp_badan_hukum').attr('src', window.URL.createObjectURL(this.files[0]))">
                                            </div>
                                        </div>
                                        <!-- END: Baris 3 -->
                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Alamat Sesuai Dengan AKTA &nbsp;</b></p>
                                    <fieldset>
                                        <!-- START: Baris 4 -->
                                        <div class="form-row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-alamat">Alamat Lengkap </label>
                                                    <textarea class="form-control form-control-lg" maxlength="100"
                                                        id="alamat_bdn_hukum" name="alamat_bdn_hukum" rows="6"
                                                        placeholder="Masukkan alamat lengkap Anda.."></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-namapengguna">Provinsi </label>
                                                    <select class="form-control custom-select" id="provinsi_bdn_hukum"
                                                        name="provinsi_bdn_hukum"
                                                        onchange="provinsiChange(this.value, this.id, 'kota_bdn_hukum')">
                                                        <option value="">-- Pilih Satu --</option>
                                                        @foreach ($master_provinsi as $data)
                                                        <option value={{$data->kode_provinsi}}>
                                                            {{$data->nama_provinsi}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-kota">Kota/Kabupaten </label>
                                                    <select class="form-control custom-select" id="kota_bdn_hukum"
                                                        name="kota_bdn_hukum" onchange="kotaChange(this.value, this.id, 'kecamatan_bdn_hukum')">
                                                        <option value="">-- Pilih Satu --</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END: Baris 4 -->

                                        <!-- START: Baris 5 -->
                                        <div class="form-row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kecamatan_bdn_hukum">Kecamatan </label>
                                                    <select class="form-control custom-select" id="kecamatan_bdn_hukum" name="kecamatan_bdn_hukum" onchange="kecamatanChange(this.value, this.id, 'kelurahan_bdn_hukum')">
                                                        <option value="">-- Pilih Satu --</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kelurahan_bdn_hukum">Kelurahan <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kelurahan_bdn_hukum" name="kelurahan_bdn_hukum"
                                                        onchange="kelurahanChange(this.value, this.id, 'kode_pos_bdn_hukum')">
                                                        <option value="">-- Pilih Satu --</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kode_pos_bdn_hukum">Kode Pos </label>
                                                    <input class="form-control" type="text" maxlength="5"
                                                        onkeyup="this.value = this.value.replace(/[^0-9]/g, '');"
                                                        id="kode_pos_bdn_hukum" name="kode_pos_bdn_hukum"
                                                        placeholder="--" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END: Baris 5 -->

                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Informasi Rekening &nbsp;</b></p>
                                    <fieldset>
                                        {{-- START: Baris 6 --}}
                                        <div class="form-row">
                                            <div class="form-group col-sm-4 va_number_bdn_hukum">
                                                <label for="va_number_bdn_hukum" class="font-weight-normal">VA
                                                    BNI</label>
                                                <input type="text" name="va_number_bdn_hukum" class="form-control"
                                                    value="{{!empty($detil->va_number) ? $detil->va_number : ''}}"
                                                    onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                            </div>

                                            <div class="form-group col-sm-4 va_cimbs_bdn_hukum">
                                                <label for="va_cimbs_bdn_hukum" class="font-weight-normal">VA CIMB
                                                    Syariah</label>
                                                <input type="text" name="va_cimbs_bdn_hukum" class="form-control"
                                                    value="{{!empty($va->bank022) ? $va->bank022 : ''}}"
                                                    onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                            </div>

                                            <div class="form-group col-sm-4 va_bsi_bdn_hukum">
                                                {{-- <label for="username" class="font-weight-normal">VA BSI</label>
                                                --}}
                                                <label for="va_bsi_bdn_hukum" class="font-weight-normal">VA
                                                    BSI</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="pre_bsi">900</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="va_bsi_bdn_hukum"
                                                        name="va_bsi_bdn_hukum"
                                                        onkeyup="this.value = this.value.replace(/[^\d/]/g,'')">
                                                </div>

                                                {{-- <input type="text" name="va_bsi" class="form-control"
                                                    value="{{!empty($va->bank451) ? $va->bank451 : ''}}"> --}}
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="rekening_bdn_hukum">No Rekening </label>
                                                    <input type="text" maxlength="16"
                                                        name="rekening_bdn_hukum" id="rekening_bdn_hukum"
                                                        pattern=".{10,16}"
                                                        onkeyup="this.value = this.value.replace(/[^\d/]/g,''); $(this).valid()"
                                                        class="form-control" placeholder="No Rekening">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="rekening_bdn_hukum checkKarakterAneh">Nama Pemilik
                                                        Rekening</label>
                                                    <input type="text" maxlength="35" name="nama_pemilik_rek_bdn_hukum"
                                                        id="nama_pemilik_rek_bdn_hukum" class="form-control"
                                                        placeholder="Nama Pemilik Rekening">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="bank_bdn_hukum">Bank </label>
                                                    <select name="bank_bdn_hukum" class="form-control custom-select"
                                                        id="bank_bdn_hukum">
                                                        <option value="">--Pilih--</option>
                                                        @foreach($master_bank as $b)
                                                        <option value="{{ $b->kode_bank }}">
                                                            {{ $b->nama_bank }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END: Baris 6 --}}

                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Informasi Lain Lain &nbsp;</b></p>
                                    <fieldset>
                                        <!-- START: Baris 7 -->
                                        <div class="form-row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="bidang_pekerjaan_bdn_hukum">Bidang Usaha </label>
                                                    <select class="form-control custom-select"
                                                        id="bidang_pekerjaan_bdn_hukum"
                                                        name="bidang_pekerjaan_bdn_hukum">
                                                        @foreach ($master_bidang_pekerjaan as $b)
                                                        <option value="{{$b->kode_bidang_pekerjaan}}">
                                                            {{$b->bidang_pekerjaan}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="omset_tahun_terakhir">Omset Tahun Terakhir</label>
                                                    <input type="text" maxlength="30" name="omset_tahun_terakhir"
                                                        id="omset_tahun_terakhir" class="form-control no-zero"
                                                        placeholder="Masukkan omset tahun terakhir"
                                                        onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).val(numberFormat(this.value))">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="tot_aset_tahun_terakhr">Total Aset Tahun Terakhir
                                                    </label>
                                                    <input type="text" maxlength="30" name="tot_aset_tahun_terakhr"
                                                        id="tot_aset_tahun_terakhr" class="form-control no-zero"
                                                        placeholder="Masukkan total aset tahun terakhir"
                                                        onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).val(numberFormat($(this).val()))">
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="btn_laporan_keuangan"></label>
                                                    <input type="hidden" name="btn_laporan_keuangan_val"
                                                        id="btn_laporan_keuangan_val">
                                                    <a href="" id="btn_laporan_keuangan" name="btn_laporan_keuangan"
                                                        target="_blank" class="btn btn-success">Unduh Laporan
                                                        Keuangan</a>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END: Baris 7 -->
                                    </fieldset>

                                    {{--
                                    <hr class="my-4"> --}}
                                    <p class="h6 my-4 line"><b>Informasi Pengurus &nbsp;</b></p>
                                    <fieldset>
                                        <div id="layout-pengurus"></div>
                                    </fieldset>

                                </div>

                                {{--
                                <hr class="my-4"> --}}
                                <p class="h6 my-4 line"><b>Refferal &nbsp;</b></p>
                                <fieldset>
                                    <div class="form-row">
                                        <div class="form-group col-sm-6">
                                            <label id="label_kode_ref" for="kode_ref" class="font-weight-normal">Kode Refferal</label>
                                            <input type="hidden" id="kode_ref_exist" value="0">
                                            <input type="text" name="kode_ref" id="kode_ref" class="form-control"
                                            onchange="isReferalExist(this.value, 'label_kode_ref', 'kode_ref_exist', 'nama_ref')"
                                                placeholder="Ketik disini" readonly>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="rekening" class="font-weight-normal">Nama Marketer</label>
                                            <input type="text" name="nama_ref" id="nama_ref" class="form-control" readonly
                                                placeholder="Nama Marketer">
                                        </div>
                                    </div>
                                </fieldset>

                                <div class="form-group">
                                    <input type="hidden" id="status_saat_ini" name="status_saat_ini">
                                    <div id="pending" style="display: none;">
                                        <label for="terkumpul" class=" form-control-label">Tangguhkan Pendana
                                            ?</label>
                                        <br>
                                        <div class="alert alert-danger col-sm-12" id="deactived">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <small>Akun ini telah di aktifkan kembali oleh <b id="namaActived"></b>
                                                dengan
                                                alasan <b id="keteranganAktif"></b> pada tanggal <b
                                                    id="tanggalAktif"></b></small><br>
                                        </div>
                                        <input type="checkbox" id="checked_suspend" name="status" value="suspend">
                                        <label for="checked_suspend">Tangguhkan</label>

                                        <br>
                                        <div class="form-group col-sm-12" id="alasan_suspend" style="display:none">
                                            <label for="alasan_suspend" class="font-weight-bold">Alasan
                                                Ditangguhkan</label>
                                            <textarea name="alasan_suspend" class="form-control" rows="3"
                                                placeholder="Alasan Ditangguhkan"></textarea>
                                        </div>
                                    </div>
                                    <div id="suspend" style="display: none;">
                                        <label for="terkumpul" class=" form-control-label">Aktivasi Pendana
                                            ?</label>
                                        <br>
                                        <div class="alert alert-danger col-sm-12">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <small>Akun ini telah di Tangguhkan oleh <b id="namaSuspended"></b>
                                                dengan
                                                alasan <b id="keteranganSuspend"></b> pada tanggal <b
                                                    id="tanggalSuspend"></b>. Klik tombol aktif jika
                                                ingin
                                                mengaktifkan
                                                kembali.</small><br>
                                        </div>
                                        <input type="checkbox" id="checked_active" name="status" value="active">
                                        <label for="checked_active">Aktifkan</label>
                                        <br>
                                        <div class="form-group col-sm-12" id="alasan_active" style="display:none">
                                            <label for="alasan_aktive" class="font-weight-bold">Alasan
                                                Diaktifkan</label>
                                            <textarea name="alasan_active" class="form-control" rows="3"
                                                placeholder="Alasan Diaktifkan"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <p class="text-danger font-weight-normal">(*) Tidak boleh kosong <br> (*) jika NPWP Valid adalah NULL maka Status NPWP menunggu proses Verifikasi Sistem</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="submit_detil">Perbaharui
                            Pendana</button>
                        <button type="button" class="btn btn-secondary" id="submit_detil_close"
                            data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end of modal investor detil -->

    <!-- start modal changeStatus -->
    <div class="modal fade" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Ganti Status Pendana : </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.investor.changestatus')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="investor_id" id="investor_id4">
                        <div class="form-group">
                            <div id="notfilled">
                                <label for="terkumpul" class=" form-control-label">aktifkan Pendana ?</label>
                                <br>
                                <input type="radio" name="status" value="suspend"> Aktif
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Ganti Status</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal changeStatus -->

    <!-- start modal changepassword -->
    <div class="modal fade" id="change_password" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Ganti Password Pendana : </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('admin.investor.changepassword')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" name="investor_id" id="investor_id2">
                        <input type="text" class="form-control" name="newpassword">

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Ganti Password</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal changepassword -->

    {{-- modal tambahVA --}}
    <div class="modal fade" id="tambahVA" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Tambah Dana <span id="nama_user"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.addVA')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="input_sebesar" class="col-sm-3 col-form-label">Nominal</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control qty" value="" name="nominal">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="perihal" class="col-sm-3 col-form-label">Keterangan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="" name="perihal">
                            </div>
                        </div>
                        <input type="hidden" name="investor_id" id="investor_id3">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary">Tambah Dana</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end modal tambahVA --}}

    {{-- modal kurangDana --}}
    <div class="modal fade" id="kurangDana" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Kurang Dana <span id="nama_user_kurang"></span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('admin.minusDana')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="input_sebesar" class="col-sm-3 col-form-label">Nominal</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control qty" value="" name="nominal">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="perihal" class="col-sm-3 col-form-label">Keterangan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" value="" name="perihal">
                            </div>
                        </div>
                        <input type="hidden" name="investor_id" id="investor_id_kurang">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-sm btn-primary">Kurang Dana</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end modal kurangDana --}}

    <div class="modal fade" id="mutasi" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-mutasi" role="document">
            <div class="modal-content">
                <div class="modal-header mb-3">
                    <h5 class="modal-title" id="scrollmodalLabel">Detil Mutasi <span id="nama_user_mutasi"></span>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mutasi-search">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="startDate1">Tanggal Mulai</label>
                                <input type="date" class="form-control" id="startDate1" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="startDate2">Tanggal Selesai</label>
                                <input type="date" class="form-control" id="startDate2" required>
                            </div>
                            <div class="form-group col-md-12 m-2">
                                <button type="submit" class="btn btn-success btn-block" id="search_btn">Cari</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mutasi-body">
                    <table class="table" id="table_list_mutasi">
                        <thead>
                            <tr>
                                <th scope="col">Keterangan</th>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Debit</th>
                                <th scope="col">Kredit</th>
                                <th scope="col">Saldo</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <hr>
                </div>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div> --}}
        </div>
    </div>
</div>

<!-- mutasi proyek -->
<div class="modal fade" id="modalmutasi_proyek_investor" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-mutasi" role="document">
        <div class="modal-content">
            <div class="modal-header mb-3">
                <h5 class="modal-title" id="scrollmodalLabel">Detil Mutasi Proyek Pendana<span
                        id="nama_user_mutasi"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mutasi-body">
                    <table class="table" id="table_list_mutasi_proyek_investor">
                        <thead>
                            <tr>
                                <th scope="col">Nama Proyek</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Tanggal Mulai</th>
                                <th scope="col">Tanggal Pendanaan</th>
                                <th scope="col">Tanggal Selesai</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <hr>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<!-- end mutasi proyek -->

<div class="modal fade" id="detilPendanaan" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-extra" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Detil Proyek <span id="nama_user_detilPendanaan"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-aktif-tab" data-toggle="pill" href="#aktif" role="tab"
                            aria-controls="aktif" aria-selected="true">Proyek Aktif</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-selesai-tab" data-toggle="pill" href="#selesai" role="tab"
                            aria-controls="selesai" aria-selected="false">Proyek Selesai</a>
                    </li>
                </ul>
                <hr>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="aktif" role="tabpanel" aria-labelledby="pills-aktif-tab">
                        <table class="table">
                            <tr>
                                <td>No</td>
                                <td>Nama Proyek</td>
                                <td>Tanggal Mulai Proyek</td>
                                <td>Total Dana</td>
                                <!-- <td >Pengguna</td> -->
                                <td>Tanggal Pendanaan</td>
                                <td>Aksi</td>
                            </tr>
                        </table>
                        {{-- <div class="row">
                            <div class="col">No</div>
                            <div class="col">Nama Proyek</div>
                            <div class="col">Tanggal Mulai</div>
                            <div class="col">Total Dana</div>
                            <div class="col-1">Pengguna</div>
                            <div class="col-2">Tanggal Pendanaan</div>
                            <div class="col">Aksi</div>
                        </div> --}}
                        <hr>
                        <div class="detilPendanaan-body">
                            <div id="detilAktif"></div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="selesai" role="tabpanel" aria-labelledby="pills-selesai-tab">
                        <table class="table">
                            <tr>
                                <td>No</td>
                                <td>Nama Proyek</td>
                                <td>Tanggal Selesai Proyek</td>
                                <td>Total Dana</td>
                                <td>Aksi</td>
                            </tr>
                        </table>
                        <hr>
                        <div class="detilPendanaan-body">
                            <div id="detilSelesai"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div> --}}
        </div>
    </div>
</div>

<div class="modal fade" id="kelolaProyek" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-mutasi" role="document">
        <div class="modal-content">
            <div class="modal-header mb-3">
                <h5 class="modal-title" id="scrollmodalLabel">Kelola Proyek</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row ml-1 mb-3">
                    <label for="total_dana">Tersedia : </label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="total_dana" readonly="readonly">
                    </div>
                </div>
                <div class="row">
                    {{-- <div class="col-1">No</div> --}}
                    <div class="col-3">Nama Proyek</div>
                    <div class="col-1">Bagi Hasil</div>
                    <div class="col-1">Harga Paket</div>
                    <div class="col-2">Terkumpul</div>
                    <div class="col-1">Sisa Waktu</div>
                    <div class="col-1">Sisa Paket</div>
                    <div class="col-3">Aksi</div>
                </div>
                <hr>
                <div class="kelolaProyek-body"></div>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div> --}}
        </div>
    </div>
</div>

</div><!-- .content -->
@endsection

@section('js')
{{-- <script src="{{ asset('js/jquery_step/jquery.validate.min.js') }}"></script> --}}
<script src="{{ asset('assetsBorrower/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>

<script src="{{asset('js/sweetalert.js')}}"></script>
<!-- <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/jquery_step/jquery.steps.js')}}"></script>
<script src="{{asset('js/jquery_step/jquery.validate.min.js')}}"></script>

<script src="{{asset('admin/assets/js/lib/data-table/datatables.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/jszip.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/vfs_fonts.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/buttons.print.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/buttons.colVis.min.js')}}"></script>
<script src="{{asset('admin/assets/js/lib/data-table/datatables-init.js')}}"></script> -->


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        $.extend( $.validator.messages, {
            required: "Harus Diisi",
            minlength: $.validator.format( "Harap masukkan setidaknya {0} karakter." ),
            maxlength: $.validator.format( "Harap masukkan tidak lebih dari {0} karakter." ),
            min: $.validator.format("Harap masukkan nilai yang lebih besar dari atau sama dengan {0}."),
            max: $.validator.format("Harap masukkan nilai yang kurang dari atau sama dengan {0}."),
        } );

        $(".no-zero").on("input", function(){
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });

        $(".eight").on("input", function(){
            let thisValue = $(this).val()
            if (thisValue.charAt(0) != '8') {
                $(this).val(thisValue.substring(1))
            }
        });

        $('#jenis_identitas').change(function() {
            let this_value = $(this).val()
            if (this_value == '1') {
                $('.layout-ktp').removeClass('d-none')
                $('.layout-passport').addClass('d-none')
                $('#is_passpor_unique').val('0')
                $('#no_passpor').val('')
                $('#label_no_passpor').text('').html(`
                No Passport
                `).removeClass('text-danger')

                $('input[name=no_ktp]').attr('disabled', false);
                $('input[name=no_passpor]').attr('disabled', true);
                
            } else if (this_value == '2') {
                $('.layout-ktp').addClass('d-none')
                $('.layout-passport').removeClass('d-none')
                $('#is_ktp_unique').val('0')
                $('#no_ktp').val('')
                $('#label_no_ktp').text('').html(`
                No KTP
                `).removeClass('text-danger')

                $('input[name=no_ktp]').attr('disabled', true);
                $('input[name=no_passpor]').attr('disabled', false);
            }
        })

        // START: Change user Status
        var is_suspend = document.getElementById("checked_suspend");
        $("#checked_suspend").click(function() {
            if (is_suspend.checked == true) {
                $("#alasan_suspend").show();
            } else {
                $("#alasan_suspend").hide();
            }
        });

        
        var is_active = document.getElementById("checked_active");
        $("#checked_active").click(function() {
            if (is_active.checked == true) {
                $("#alasan_active").show();
            } else {
                $("#alasan_active").hide();
            }
        });
        // END: Change user Status

        // $("#no_telp").inputFilter(function(value) {
        //     return /^\d*$/.test(value);
        // });

        // $("#no_ktp").inputFilter(function(value) {
        //     return /^\d*$/.test(value);
        // });

        // $("#rekening").inputFilter(function(value) {
        //     return /^\d*$/.test(value);
        // });

        let validator = $('#form-detil-investor').validate({
            // lang: 'id',
            onfocusout: false,
            ignore: "hidden",
            messages: {
                tgl_akta_perubahan_bdn_hukum : {
                    max: "Harap Masukan Tanggal Lebih Kecil atau Sama Dengan Tanggal Akta Pendirian",
                    min: "Harap Masukan Tanggal Lebih Besar atau Sama Dengan Tanggal Akta Pendirian",
                }
            },
            invalidHandler: function(e, validator){
                if(validator.errorList.length){
                    $('.coll').addClass('show')
                    validator.errorList[0].element.focus()
                }
            }
        });

        $('#submit_detil').click(function() {

            $('#submit_detil').focus()
            $('input[name=no_telp]').attr('maxlength','13');
            $('input[name=no_telp_pengurus_0]').attr('maxlength','13');

            let is_username_unique          = $('#is_username_unique').val()
            let is_email_unique             = $('#is_email_unique').val()
            let is_phone_inv_unique         = $('#is_phone_inv_unique').val()
            let is_phone_pengurus_unique    = $('#is_phone_pengurus_unique').val()
            let is_ktp_unique               = $('#is_ktp_unique').val()
            let is_ktp_unique_pengurus      = $('#is_ktp_pengurus').val()
            let is_passpor_unique           = $('#is_passpor_unique').val()
            let is_referal_exist            = $('#kode_ref_exist').val()

            if (is_username_unique == 1) {
                $('input[name=username]').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else if (is_email_unique == 1) {
                $('input[name=email]').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else if (is_ktp_unique == 1) {
                $('input[name=no_ktp]').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else if (is_ktp_unique_pengurus == 1) {
                $('input[name=ktp_pengurus_0]').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else if (is_passpor_unique == 1) {
                $('input[name=no_passpor]').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else if (is_phone_inv_unique == 1) {
                $('input[name=no_telp]').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else if (is_phone_pengurus_unique == 1) {
                $('#no_telp_pengurus_0').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else if(is_referal_exist == 1) {
                $('#kode_ref').focus();
                $('input[name=no_telp]').attr('maxlength','15');
                $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
            } else {
                if($("#form-detil-investor").valid()) {
                    Swal.fire({
                        html: '<h5>Menyimpan Data ...</h5>',
                        onBeforeOpen: () => {
                            Swal.showLoading();
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                    $('#form-detil-investor').submit()
                    $('#is_username_unique').val('0')
                    $('#is_email_unique').val('0')
                    $('#is_phone_inv_unique').val('0')
                    $('#is_phone_pengurus_unique').val('0')

                    $('#is_ktp_unique').val('0')
                    $('#is_ktp_pengurus').val('0')
                    $('#is_passpor_unique').val('0')
                    $('#kode_ref_exist').val('0')
                } else {
                    Swal.close()
                    $('input[name=no_telp]').attr('maxlength','15');
                    $('input[name=no_telp_pengurus_0]').attr('maxlength','15');
                }
            }
        })

        $('body').on('show.bs.modal', function() {
        // $('#submit_detil_close, #close_modal_detil').click(function() {
            $('#is_username_unique').val('0')
            $('#is_email_unique').val('0')
            $('#is_phone_inv_unique').val('0')
            $('#is_phone_pengurus_unique').val('0')
            $('#is_ktp_unique').val('0')
            $('#is_passpor_unique').val('0')
            $('#kode_ref_exist').val('0')

            $('#label_username').html(`Akun <i class="text-danger">*</i>`).removeClass('text-danger')
            $('#label_email').html(`Email <i class="text-danger">*</i>`).removeClass('text-danger')
            $('#label_no_hp_inv').html(`No Telp / HP <i class="text-danger">*</i>`).removeClass('text-danger')
            $('#label_kode_operator_pengurus_0').html(`No Telepon <i class="text-danger">*</i>`).removeClass('text-danger')
            $('#label_no_ktp').html(`No KTP <i class="text-danger">*</i>`).removeClass('text-danger')
            $('#label_ktp_pengurus_0').html(`No KTP <i class="text-danger">*</i>`).removeClass('text-danger')
            $('#label_no_passpor').html(`No Passport <i class="text-danger">*</i>`).removeClass('text-danger')
            $('#label_kode_ref').text(`Kode Refferal`).removeClass('text-danger');

            validator.resetForm();
        })
    });

    // upload
    // $(document).on("change", ".uploadFile", function() {
    //     var uploadFile = $(this);
    //     var files = !!this.files ? this.files : [];
    //     if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support

    //     if (/^image/.test(files[0].type)) { // only image file
    //         var reader = new FileReader(); // instance of the FileReader
    //         reader.readAsDataURL(files[0]); // read the local file

    //         reader.onloadend = function() { // set image data as background of div
    //             //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
    //             uploadFile.closest(".imgUp").find('.imagePreview').attr("src", this.result);
    //             // $('.imagePreview').attr("src", this.result)
    //         }
    //     }

    // });
    // end upload

    $(document).ready(function() {
        var investorTable;

        $('#nama').on('keyup', function(e) {
            e.preventDefault();
            var query = $(this).val();
            if (query != '') {
                $.ajax({
                    url: "/admin/investor/nama_autocomplete",
                    method: "post",
                    data: {
                        query: query
                    },
                    success: function(data) {
                        $('#nama_list').fadeIn();
                        $('#nama_list').html(data);
                    }
                });
            } else {
                $('#nama_list').fadeOut();
                $('#nama_list').html();
            }
        })

        $(document).on('click', 'li', function() {
            $('#nama').val($(this).text());
            $('#nama_list').fadeOut();
        })

        $('#search').on('click', function(e) {
            e.preventDefault();
            var name = $.trim($('#nama').val());
            var unallocated = $('input:radio[name="unallocated"]:checked').val();
            var username = $('#username').val();
            var search_email = $('#searchEmail').val();
            let va_number = $('#in_va_number').val();
            let kode_operator = $('#in_kode_operator').val();
            let phone_number = $('#in_no_hp').val();
            let is_bsi = va_number.substring(0, 3)
            if (is_bsi == 900) {
                va_number = va_number.substring(3)
            }
            let dataSearch = {
                name: name ? name : null,
                unallocated: unallocated ? unallocated : null,
                username: username ? username : null,
                search_email: search_email ? search_email : null,

                va_number: va_number ? va_number : null,
                kode_operator: kode_operator ? kode_operator : null,
                phone_number: phone_number ? phone_number : null
            }

            if (name == '' && username == '' && search_email == '' && unallocated == undefined && va_number == '' && phone_number == '') {
                Swal.fire({
                    title: "Peringatan",
                    text: "Salah satu field harus terisi",
                    type: "warning",
                })
                return false
            }
            console.log(dataSearch)
            $.ajax({
                url: '/admin/investor/data',
                method: 'post',
                dataType: 'json',
                data: dataSearch,
                beforeSend: function() {
                    Swal.fire({
                        html: '<h5>Mencari Data ...</h5>',
                        onBeforeOpen: () => {
                            Swal.showLoading();
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                },
                success: function(data) {
                    Swal.close()
                    if (data.status == 'Ada') {
                        $('#view_card_search').attr('style', 'display: none');
                        $('#view_card_table').attr('style', 'display: block');
                        investorTable = $('#table_investor').DataTable({
                            searching: true,
                            processing: true,
                            // serverSide: true,
                            ajax: {
                                url: '/admin/investor/data_mutasi_datatables',
                                type: 'post',
                                data: dataSearch,
                                dataSrc: 'data'
                            },
                            paging: true,
                            info: true,
                            lengthChange: false,
                            order: [1, 'asc'],
                            pageLength: 10,
                            columns: [{
                                    data: 'idInvestor'
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        //I want to get row index here somehow
                                        return meta.row + 1;
                                    }
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        if (row.status == 'notfilled' || row.status == 'Not Active') {
                                            return '-'
                                        } else if (row.status == 'active' || row.status == 'pending' || row.status == 'suspend' || row.status == 'reject' || row.status == 'expired') {
                                            return row.nama_investor
                                        }
                                    }
                                },
                                {
                                    data: 'email'
                                },
                                {
                                    data: 'username'
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        // return row.total - row.total_log
                                        if (row.total == null || row.jumlah_pendanaan == null) {
                                            return 0 +
                                                '<button class="btn btn-success float-right tambahVA-btn" data-toggle="modal" data-target="#detilPendanaan" id="detil_pendanaan" value=""><span class="ti-align-justify"></span></button>';
                                        } else {
                                            // return row.total - row.total_log
                                            return row.jumlah_pendanaan +
                                                '<button class="btn btn-success float-right tambahVA-btn" data-toggle="modal" data-target="#detilPendanaan" id="detil_pendanaan" value=""><span class="ti-align-justify"></span></button>';
                                        }
                                    }
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        if (row.jumlah_nominal == null || row.total_pendanaan == null) {
                                            return 0;
                                        } else {
                                            // return numberFormat(row.jumlah_nominal - row.jumlah_nominal_log);
                                            return numberFormat(row.total_pendanaan);
                                        }
                                    }
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        if (row.unallocated == '') {
                                            return "Don't Have VA";
                                        } else {
                                            if (row.unallocated == null) {
                                                return 0
                                                // +'<br><br><button class="btn btn-success float-right tambahVA-btn" data-toggle="modal" data-target="#tambahVA" id="tambah_va" value=""><span class="ti-plus"></span></button><br><br>'
                                                // +'<button class="btn btn-success float-right kurangDana-btn" data-toggle="modal" data-target="#kurangDana" id="kurang_dana" value=""><span class="ti-minus"></span></button>';
                                            } else {
                                                return numberFormat(row.unallocated)
                                                // +'<br><br><button class="btn btn-success float-right tambahVA-btn" data-toggle="modal" data-target="#tambahVA" id="tambah_va" value=""><span class="ti-plus"></span></button><br><br>'
                                                // +'<button class="btn btn-success float-right kurangDana-btn" data-toggle="modal" data-target="#kurangDana" id="kurang_dana" value=""><span class="ti-minus"></span></button>';
                                            }
                                        }
                                    }
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        if (row.status == 'notfilled') {
                                            return '<button class="btn btn-info btn-sm active" id="detil_investor" disabled>Detil User</button><br><br>';
                                            //<button class="btn btn-warning btn-sm active" data-toggle="modal" data-target="#change_password" id="password">Change Password</button>';
                                        } else if (row.status == 'Not Active') {
                                            return '';
                                            //'<button class="btn btn-warning btn-sm active" data-toggle="modal" data-target="#change_password" id="password">Change Password</button>';
                                        } else if (row.status == 'suspend' && row.nama_investor == null) {
                                            return '<button class="btn btn-warning btn-sm active" data-toggle="modal" data-target="#changeStatus" id="change_status">Change Status</button>';
                                        } else if (row.status == 'active' || row.status == 'pending' || row.status == 'suspend' || row.status == 'reject' || row.status == 'expired') {
                                            return '<button class="btn btn-info btn-sm active" id="detil_investor">Detil User</button><br><br><button class="btn btn-primary btn-sm active" data-toggle="modal" data-target="#mutasi" id="mutasi_investor">Riwayat Mutasi</button><br><br><button class="btn btn-primary btn-sm active" data-toggle="modal" data-target="#modalmutasi_proyek_investor" id="mutasi_proyek_investor" onclick="getIdMutasi(' + row['idInvestor'] + ')">Riwayat Mutasi Proyek Pendana</button><br><br><button class="btn btn-primary btn-sm active" data-toggle="modal" data-target="#kelolaProyek" id="kelola_proyek">Kelola Proyek</button><!--br><br><button class="btn btn-primary btn-sm active" data-toggle="modal" data-target="#" id="reg_digisign">Daftar Digital Sign</button><br><br><button class="btn btn-primary btn-sm active" data-toggle="modal" data-target="#" id="send_digisign">Kirim Dokumen Digital Sign</button><br><br><button class="btn btn-primary btn-sm active" data-toggle="modal" data-target="#" id="sign_digisign">TTD Digital Sign</button><br><br><button class="btn btn-primary btn-sm active" data-toggle="modal" data-target="#" id="create_doc">Generate Dokumen Digital Sign</button-->';
                                            //<button class="btn btn-warning btn-sm active" data-toggle="modal" data-target="#change_password" id="password">Change Password</button><br><br>
                                        }
                                    }
                                },
                                {
                                    data: null,
                                    render: function(data, type, row, meta) {
                                        if (row.status == 'notfilled') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>notfilled</button>';
                                        } else if (row.status == 'Not Active') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>not active</button>';
                                        } else if (row.status == 'pending') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>Pending</button>';
                                        } else if (row.status == 'reject') {
                                            return '<button class="btn btn-outline-danger btn-sm active" disabled>Reject</button>';
                                        } else if (row.status == 'active') {
                                            return '<button class="btn btn-outline-success btn-sm active" disabled>active</button>';
                                        } else if (row.status == 'suspend') {
                                            return '<button class="btn btn-outline-secondary btn-sm active" disabled>suspend</button>';
                                        } else if (row.status == 'expired') {
                                            return '<button class="btn btn-outline-secondary btn-sm active" disabled>expired password</button>';
                                        }
                                    }
                                }
                            ],
                            columnDefs: [{
                                targets: [0],
                                visible: false
                            }]
                        });
                    } else {
                        $('#error_search').attr('style', 'display: block');
                    }
                },
                error: function(error) {
                    Swal.close()
                    alert('Koneksi Gagal, Periksa Koneksi Internet Anda !');
                    console.log(error)
                }
            })
        });

        // Validasi No HP
        $('#in_no_hp').on("input", function(){
            let in_kode_operator = $('#in_kode_operator').val()
            let thisValue = $(this).val()

            if (in_kode_operator == '62' ) {
                if (thisValue.charAt(0) == '6' && thisValue.charAt(1) == '2') {
                    $(this).val(thisValue.substring(2))
                } else {
                    if (thisValue.charAt(0) != '8') {
                        $(this).val(thisValue.substring(1))
                    }
                }
            } 
        })

        $('#table_investor tbody').on('click', '#detil_investor', function(e) {
            e.preventDefault()
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            nama = data.nama_investor;
            username = data.username;
            email = data.email;
            status = data.status;
            $.ajax({
                url: "/admin/investor/data_detil_investor/" + id,
                method: "get",
                beforeSend: function() {
                    Swal.fire({
                        html: '<h5>Mengambil Data ...</h5>',
                        onBeforeOpen: () => {
                            Swal.showLoading();
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                },
                success: function(data) {
                    data_all = data.detil_investor;
                    console.log(data_all)
                    if (data_all.tipe_pengguna == 2) {
                        // $('#layout-badan-hukum').removeClass('d-none');
                        // $("#layout-individu").addClass('d-none');
                        $('#layout-badan-hukum').removeClass('d-none').find('input, select, textarea, radio').prop("disabled", false);
                        $("#layout-individu").addClass('d-none').find('input, select, textarea, radio').prop("disabled", true);
                    } else {
                        // $('#layout-badan-hukum').addClass('d-none');
                        // $('#layout-individu').removeClass('d-none');
                        $('#layout-badan-hukum').addClass('d-none').find('input, select, textarea, radio').prop("disabled", true);
                        $('#layout-individu').removeClass('d-none').find('input, select, textarea, radio').prop("disabled", false);
                    }

                    $('#tipe_proses').val('');
                    if (data_all) {
                        tipe_pengguna = data_all.tipe_pengguna;
                        $('#investor_id').val(id);
                        $('#tipe_pengguna').val(tipe_pengguna);
                        $('#tipe_proses').val('lama');

                        kode_ref = data_all.ref_code;
                        nama_marketer = data_all.nama_marketer;
                        keteranganSuspend = data_all.keteranganSuspend;
                        tanggalSuspend = data_all.tanggalSuspend;
                        namaSuspended = data_all.namaSuspended;
                        namaActived = data_all.namaActived;

                        va_number = data_all.va_number;
                        // if (va_number == undefined) {
                        //     $('.va_number').addClass('d-none')
                        // } else if (va_number == null) {
                        //     $('.va_number').addClass('d-none')
                        // } else if (va_number == "") {
                        //     $('.va_number').addClass('d-none')
                        // } else {
                        //     $('.va_number').removeClass('d-none')
                        // }
                        // vanumber
                        data_va = data.va;
                        va_cimbs = data_va.bank022;
                        // (va_cimbs !== undefined) ? $('.va_cimbs').removeClass('d-none'): $('.va_cimbs').addClass('d-none');
                        va_bsi = data_va.bank451;
                        // (data_va.bank451 !== undefined) ? $('.va_bsi').removeClass('d-none'): $('.va_bsi').addClass('d-none');

                        if (tipe_pengguna == 1) {
                            jenis_identitas = data_all.jenis_identitas ? data_all.jenis_identitas : '';
                            // START: Individu
                            if (jenis_identitas == 1) {
                                no_ktp = data_all.no_ktp_investor;
                                no_passpor = ''
                            } else {
                                no_ktp = ''
                                no_passpor = data_all.no_passpor_investor;
                            }
                            no_npwp                 = data_all.no_npwp_investor != 0 ? data_all.no_npwp_investor : '';
                            is_valid_npwp           = data_all.is_valid_npwp == 1 ? 'Ya' : data_all.is_valid_npwp == 0 ? 'Tidak' : 'NULL';
                            npwp_disabled           = data_all.is_valid_npwp == 1 ? true : false;
                            kode_operator           = data_all.kode_operator ? data_all.kode_operator : '';
                            no_telp_before          = data_all.phone_investor ? data_all.phone_investor : '';
                            no_telp_after           = no_telp_before
                            if (no_telp_before) {
                                if (kode_operator == '62' && no_telp_after.substr(0,2) == '62') {
                                    no_telp_after = parseInt(no_telp_after.replace('62', ''))
                                } else if(kode_operator == '62') {
                                    no_telp_after = (parseInt(no_telp_after)).toString()
                                } else {
                                    no_telp_after = no_telp_after.replace(kode_operator, '')
                                }
                            }
                            no_telp                 = no_telp_after;
                            tempat_lahir            = data_all.tempat_lahir_investor ? data_all.tempat_lahir_investor : '';
                            tgl_lahir               = data_all.tgl_lahir_investor ? data_all.tgl_lahir_investor : '';
                            jenis_kelamin           = data_all.jenis_kelamin_investor ? data_all.jenis_kelamin_investor : '';
                            domisili_negara         = data_all.domisili_negara;
                            alamat                  = data_all.alamat_investor ? data_all.alamat_investor : '';
                            provinsi                = data_all.provinsi_investor ? data_all.provinsi_investor : '';
                            kota                    = data_all.kota_investor ? data_all.kota_investor : '';
                            kode_pos                = data_all.kode_pos_investor ? data_all.kode_pos_investor : '';
                            rekening                = data_all.rekening ? data_all.rekening : '';
                            bank                    = data_all.bank_investor ? data_all.bank_investor : '';
                            nama_pemilik_rek        = data_all.nama_pemilik_rek ? data_all.nama_pemilik_rek : '';
                            is_valid_rekening       = data_all.is_valid_rekening == null ? '' : data_all.is_valid_rekening ;
                            nama_pemilik_rek_asli   = data_all.pemilik_rekening_asli == null ? '' : data_all.pemilik_rekening_asli;
                            
                            foto1                   = data_all.pic_investor ? data_all.pic_investor : '';
                            foto2                   = data_all.pic_ktp_investor ? data_all.pic_ktp_investor : '';
                            foto3                   = data_all.pic_user_ktp_investor ? data_all.pic_user_ktp_investor : '';

                            status_kawin            = data_all.status_kawin_investor ? data_all.status_kawin_investor : '';
                            kecamatan               = data_all.kecamatan ? data_all.kecamatan : '';
                            kelurahan               = data_all.kelurahan ? data_all.kelurahan : '';
                            nama_ibu_kandung        = data_all.nama_ibu_kandung ? data_all.nama_ibu_kandung : '';
                            pendidikan              = data_all.pendidikan_investor ? data_all.pendidikan_investor : '';
                            pekerjaan               = data_all.pekerjaan_investor ? data_all.pekerjaan_investor : '';
                            bidang_pekerjaan        = data_all.bidang_pekerjaan ? data_all.bidang_pekerjaan : '';
                            pendapatan              = data_all.pendapatan_investor ? data_all.pendapatan_investor : '';

                            warga_negara            = data_all.warganegara;
                            agama                   = data_all.agama_investor ? data_all.agama_investor : ''
                            sumber_dana             = data_all.sumber_dana ? data_all.sumber_dana : ''
                            pengalaman_kerja        = data_all.pengalaman_investor ? data_all.pengalaman_investor : ''
                            bidang_online           = data_all.online_investor ? data_all.online_investor : ''

                            // Ahli Waris
                            nama_ahli_waris          = data_all.nama_ahli_waris ? data_all.nama_ahli_waris : '';
                            hub_ahli_waris           = data_all.hub_ahli_waris ? data_all.hub_ahli_waris : '';
                            nik_ahli_waris           = data_all.nik_ahli_waris ? data_all.nik_ahli_waris : '';
                            kode_operator_ahli_waris = data_all.kode_operator_ahli_waris ? data_all.kode_operator_ahli_waris : '';
                            no_hp_ahli_waris_before  = data_all.no_hp_ahli_waris ? data_all.no_hp_ahli_waris : '';
                            no_hp_ahli_waris_after   = no_hp_ahli_waris_before
                            if (no_hp_ahli_waris_before) {
                                if (kode_operator_ahli_waris == '62' && no_hp_ahli_waris_after.substr(0,2) == '62') {
                                    no_hp_ahli_waris_after = parseInt(no_hp_ahli_waris_after.replace('62', ''))
                                } else if(kode_operator_ahli_waris == '62') {
                                    no_hp_ahli_waris_after = (parseInt(no_hp_ahli_waris_after)).toString()
                                } else {
                                    no_hp_ahli_waris_after = no_hp_ahli_waris_after.replace(kode_operator_ahli_waris, '')
                                }
                            }
                            no_hp_ahli_waris         = no_hp_ahli_waris_after
                            // no_hp_ahli_waris         = data_all.no_hp_ahli_waris ? data_all.no_hp_ahli_waris : '';
                            alamat_ahli_waris        = data_all.alamat_ahli_waris ? data_all.alamat_ahli_waris : '';

                            $('input[name=tempat_lahir]').val(tempat_lahir);

                            if (tgl_lahir) {
                                console.log(tgl_lahir)
                                
                                data_tgl_lahir = tgl_lahir.split("-");
                                tgl_lahir = (data_tgl_lahir[0]).length  == 1 ? '0'+data_tgl_lahir[0] : data_tgl_lahir[0]
                                bln_lahir = (data_tgl_lahir[1]).length  == 1 ? '0'+data_tgl_lahir[1] : data_tgl_lahir[1]
                                thn_lahir = (data_tgl_lahir[2]).length  == 1 ? '0'+data_tgl_lahir[2] : data_tgl_lahir[2]
                                new_format = thn_lahir+'-'+bln_lahir+'-'+tgl_lahir
                                // new_format = new Date(thn_lahir, bln_lahir, tgl_lahir)
                                // console.log(new_format.toDateString())
                                $('#tanggal_lahir').val(new_format)

                                // format_date = Date.parse(tgl_lahir).toString("MMMM yyyy");
                                // let data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
                                // if (data_lahir[0].length > 2) {
                                //     $('input[name="tanggal_lahir"]').val(`${data_lahir[2]} ${data_bulan[data_lahir[1] - 1]} ${data_lahir[0]}`);
                                // } else {
                                //     $('input[name="tanggal_lahir"]').val(`${data_lahir[0]} ${data_bulan[data_lahir[1] - 1]} ${data_lahir[2]}`);
                                // }

                                // $('input[name=tgl_lahir]').val(data_lahir[0]);
                                // $('input[name=bln_lahir]').val(data_lahir[1]);
                                // $('input[name=thn_lahir]').val(data_lahir[2]);
                            }
                            // else {
                            // data_lahir = null;
                            // $('input[name=tgl_lahir]').val(data_lahir);
                            // $('input[name=bln_lahir]').val(data_lahir);
                            // $('input[name=thn_lahir]').val(data_lahir);
                            // }

                            // var option = '<option value="">--Pilih--</option>',
                            //     select_tgl = document.getElementById('tgl_lahir'),
                            //     data_tgl_lhr = (data_lahir !== null ? data_lahir[0] : 0);

                            // for (i = 1; i <= 31; i++) {
                            //     option += '<option value="' + i + '" ' + (i == data_tgl_lhr ? "selected" : "") + '>' + i + '</option>';
                            // }

                            // select_tgl.innerHTML = option;

                            // var data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'],
                            //     option_bln = '<option value="">--Pilih--</option>',
                            //     data_bln_lhr = (data_lahir !== null ? data_lahir[1] : 0),
                            //     select_bln = document.getElementById('bln_lahir');

                            // for (i = 0; i <= 11; i++) {
                            //     option_bln += '<option value="' + (i + 1) + '" ' + (i == data_bln_lhr - 1 ? "selected" : "") + '>' + data_bulan[i] + '</option>';
                            // }
                            // select_bln.innerHTML = option_bln;

                            // generate tahun
                            // var select = document.getElementById('thn_lahir'),
                            //     year = new Date().getFullYear(),
                            //     html = '<option value="">--Pilih--</option>',
                            //     data_awal = (data_lahir !== null ? data_lahir[2] : 0);
                            // for (i = year; i >= year - 100; i--) {
                            //     html += '<option value="' + i + '" ' + (i == data_awal ? "selected" : "") + '>' + i + '</option>';
                            // }
                            // select.innerHTML = html;
                            // end generate tahun

                            if (data_all.jenis_identitas == 1) {
                                $('input[name=no_ktp]').attr('disabled', false);
                                $('input[name=no_passpor]').attr('disabled', true);

                                $('.layout-ktp').removeClass('d-none')
                                $('.layout-passport').addClass('d-none')
                            } else {
                                $('input[name=no_ktp]').attr('disabled', true);
                                $('input[name=no_passpor]').attr('disabled', false);
                                // $('#label_no_ktp').text('No Paspor')

                                $('.layout-ktp').addClass('d-none')
                                $('.layout-passport').removeClass('d-none')
                            }
                            $('#old_no_ktp').val(no_ktp);
                            $('input[name=no_ktp]').val(no_ktp);
                            $('input[name=no_passpor]').val(no_passpor);
                            $('#old_no_passpor').val(no_passpor);
                            $('select[name=jenis_identitas]').val(jenis_identitas);
                            $('select[name=jenis_kelamin]').val(jenis_kelamin);
                            $('input[name=nama]').val(nama);
                            $('input[name=no_npwp]').val(no_npwp);
                            $('input[name=no_npwp]').attr('readonly',npwp_disabled);
                            $('input[name=is_valid_npwp]').val(is_valid_npwp);
                            $('input[name=npwp_exist]').val(no_npwp);
                            $('select[name=kode_operator]').val(kode_operator);
                            $('input[name=no_telp]').val(no_telp).attr('maxlength','15');
                            $('select[name=domisili_negara]').val(domisili_negara);
                            $('textarea[name=alamat]').val(alamat);
                            $('select[name=provinsi]').val(provinsi);

                            $('#kota').empty();
                            $('#kota').append($('<option>', { value: '', text : '-- Pilih Satu --'}));
                            $.ajax({
                                url: "/admin/getKota/" + provinsi,
                                method: "get",
                                async: false,
                                success: function(data) {
                                    $.each(data.kota, function(index, value) {
                                        if (value.kode_kota == kota) {
                                            var select = 'selected=selected';
                                        }
                                        $('#kota').append(
                                            '<option value="' + value.kode_kota + '"' + select + '>' + value.nama_kota + '</option>'
                                        );
                                    })
                                }
                            });

                            $('input[name=kode_pos]').val(kode_pos);
                            $('input[name=kecamatan]').val(kecamatan);
                            $('input[name=kelurahan]').val(kelurahan);

                            $('input[name=nama_ibu_kandung]').val(nama_ibu_kandung);
                            $('select[name=status_kawin]').val(status_kawin);
                            $('select[name=pendidikan]').val(pendidikan);
                            $('select[name=pekerjaan]').val(pekerjaan);
                            $('select[name=bidang_pekerjaan]').val(bidang_pekerjaan);
                            $('select[name=pendapatan]').val(pendapatan);

                            $('select[name=warga_negara]').val(warga_negara);
                            $('select[name=agama]').val(agama);
                            $('input[name=sumber_dana]').val(sumber_dana);
                            $('select[name=pengalaman_kerja]').val(pengalaman_kerja);
                            $('select[name=bidang_online]').val(bidang_online);

                            $('input[name=nama_pemilik_rek]').val(nama_pemilik_rek);
                            $('input[name=rekening]').val(rekening);
                            $('select[name=bank]').val(bank);
                            if(is_valid_rekening == 1){
                                $('#is_valid_rekening').addClass('text-info');
                                $('#is_valid_rekening').removeClass('text-danger');
                                $('#is_valid_rekening').html("<br> (*) Nomor Rekening Valid");
                                $('input[name=nama_pemilik_rek_asli]').val(nama_pemilik_rek_asli);
                            }else if(is_valid_rekening == 0){
                                $('#is_valid_rekening').addClass('text-danger');
                                if(nama_pemilik_rek_asli == ''){
                                    $('#is_valid_rekening').html("(*) Nomor Rekening dan Nama Pemilik Tidak Valid &nbsp &nbsp");
                                    $('input[name=nama_pemilik_rek_asli]').val('-');
                                }else{
                                    $('#is_valid_rekening').html("(*) Nomor Rekening Valid dan Nama Pemilik Rekening Tidak Valid");
                                    $('input[name=nama_pemilik_rek_asli]').val(nama_pemilik_rek_asli);
                                }
                            }else if(is_valid_rekening == ''){
                                $('#is_valid_rekening').addClass('text-danger');
                                $('#is_valid_rekening').html("(*) Nomor Rekening dan Nama Pemilik Belum di Validasi");
                                $('#nama_pemilik_rek_asli_label').addClass('d-none');
                            }

                            let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";

                            (foto1 !== '' && foto1 !== null ? $('#foto1').attr('src', url.replace(':path', foto1.replaceAll('/', ':'))) : $('#foto1').attr('src', ''));
                            (foto2 !== '' && foto2 !== null ? $('#foto2').attr('src', url.replace(':path', foto2.replaceAll('/', ':'))) : $('#foto2').attr('src', ''));
                            (foto3 !== '' && foto3 !== null ? $('#foto3').attr('src', url.replace(':path', foto3.replaceAll('/', ':'))) : $('#foto3').attr('src', ''));

                            $('#pic_investor_val').val(foto1)
                            $('#pic_ktp_investor_val').val(foto2)
                            $('#pic_user_ktp_investor_val').val(foto3)

                            // Ahli Waris
                            $('#nama_ahli_waris').val(nama_ahli_waris)
                            $('select[name="hub_ahli_waris"]').val(hub_ahli_waris);
                            $('#nik_ahli_waris').val(nik_ahli_waris)
                            $('#no_hp_ahli_waris').val(no_hp_ahli_waris)
                            $('select[name=kode_operator_ahli_waris]').val(kode_operator_ahli_waris);
                            $('#alamat_ahli_waris').val(alamat_ahli_waris)
                            // END: Individu
                        } else {

                            // START: Badan Hukum
                            let nama_badan_hukum                = data_all.nama_perusahaan ? data_all.nama_perusahaan : ''
                            let nib_badan_hukum                 = data_all.nib ? data_all.nib : ''
                            let npwp_bdn_hukum                  = data_all.npwp_perusahaan ? data_all.npwp_perusahaan : ''
                            let no_akta_pendirian_bdn_hukum     = data_all.no_akta_pendirian ? data_all.no_akta_pendirian : ''
                            let tgl_berdiri_badan_hukum         = data_all.tgl_berdiri ? data_all.tgl_berdiri : ''
                            let no_akta_perubahan_bdn_hukum     = data_all.nomor_akta_perubahan ? data_all.nomor_akta_perubahan : ''
                            let tgl_akta_perubahan_bdn_hukum    = data_all.tanggal_akta_perubahan ? data_all.tanggal_akta_perubahan : ''
                            let no_tlp_badan_hukum              = data_all.telpon_perusahaan ? (data_all.telpon_perusahaan).substr(2) : ''

                            let foto_npwp_badan_hukum           = data_all.foto_npwp_perusahaan ? data_all.foto_npwp_perusahaan : ''
                            let alamat_bdn_hukum                = data_all.alamat_perusahaan ? data_all.alamat_perusahaan : ''
                            let provinsi_bdn_hukum              = data_all.provinsi_perusahaan ? data_all.provinsi_perusahaan : ''
                            let kota_bdn_hukum                  = data_all.kota_perusahaan ? data_all.kota_perusahaan : ''
                            let kecamatan_bdn_hukum             = data_all.kecamatan_perusahaan ? data_all.kecamatan_perusahaan : ''
                            let kelurahan_bdn_hukum             = data_all.kelurahan_perusahaan ? data_all.kelurahan_perusahaan : ''
                            let kode_pos_bdn_hukum              = data_all.kode_pos_perusahaan ? data_all.kode_pos_perusahaan : ''

                            let rekening_bdn_hukum              = data_all.rekening ? data_all.rekening : ''
                            let nama_pemilik_rek_bdn_hukum      = data_all.nama_pemilik_rek ? data_all.nama_pemilik_rek : ''
                            let bank_bdn_hukum                  = data_all.bank_investor ? data_all.bank_investor : ''

                            let bidang_pekerjaan_bdn_hukum      = data_all.bidang_pekerjaan ? data_all.bidang_pekerjaan : ''
                            let omset_tahun_terakhir            = data_all.omset_tahun_terakhir ? numberFormat(parseInt(data_all.omset_tahun_terakhir)) : ''
                            let tot_aset_tahun_terakhr          = data_all.tot_aset_tahun_terakhr ? numberFormat(parseInt(data_all.tot_aset_tahun_terakhr)) : ''
                            let laporan_keuangan                = data_all.laporan_keuangan ? data_all.laporan_keuangan : ''

                            $('#nama_badan_hukum').val(nama_badan_hukum)
                            $('#nib_badan_hukum').val(nib_badan_hukum)
                            $('#npwp_bdn_hukum').val(npwp_bdn_hukum)
                            $('#no_akta_pendirian_bdn_hukum').val(no_akta_pendirian_bdn_hukum)
                            $('#tgl_berdiri_badan_hukum').val(tgl_berdiri_badan_hukum)
                            $('#no_akta_perubahan_bdn_hukum').val(no_akta_perubahan_bdn_hukum)
                            $('#no_akta_perubahan_bdn_hukum').val(no_akta_perubahan_bdn_hukum)
                            $('#tgl_akta_perubahan_bdn_hukum').val(tgl_akta_perubahan_bdn_hukum)
                            $('#no_tlp_badan_hukum').val(no_tlp_badan_hukum)

                            let url2 = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
                            foto_npwp_badan_hukum ? $('#foto_npwp_badan_hukum').attr('src', url2.replace(':path', foto_npwp_badan_hukum.replaceAll('/', ':'))) : $('#foto_npwp_badan_hukum').attr('src', '');
                            $('#foto_npwp_badan_hukum_val').val(foto_npwp_badan_hukum)
                            
                            $('#alamat_bdn_hukum').val(alamat_bdn_hukum)
                            $('select[name=provinsi_bdn_hukum]').val(provinsi_bdn_hukum);
                            $('#kota_bdn_hukum').empty();
                            $('#kota_bdn_hukum').append($('<option>', { value: '', text : '-- Pilih Satu --'}));

                            $('#kecamatan_bdn_hukum').empty();
                            $('#kecamatan_bdn_hukum').append($('<option>', { value: '', text : '-- Pilih Satu --'}));

                            $('#kelurahan_bdn_hukum').empty();
                            $('#kelurahan_bdn_hukum').append($('<option>', { value: '', text : '-- Pilih Satu --'}));
                            $.ajax({
                                url: "/admin/getKota/" + provinsi_bdn_hukum,
                                method: "get",
                                success: function(data) {
                                    $.each(data.kota, function(index, value) {
                                        if (value.kode_kota == kota_bdn_hukum) {
                                            var select = 'selected=selected';
                                        }
                                        $('#kota_bdn_hukum').append(
                                            '<option value="' + value.kode_kota + '"' + select + '>' + value.nama_kota + '</option>'
                                        );
                                    })
                                }
                            });

                            if (kota_bdn_hukum) {
                                $.ajax({
                                    url: "/getKecamatan/" + kota_bdn_hukum,
                                    method: "get",
                                    async: false,
                                    success: function(response) {
                                        let data_kecamatan = response ? JSON.parse(response) : []
                                        for(let i = 0; i<data_kecamatan.length; i++){ 
                                            $('#kecamatan_bdn_hukum').append($('<option>', {
                                                value: data_kecamatan[i].text,
                                                text : data_kecamatan[i].text
                                            }));
                                        
                                            $(`#kecamatan_bdn_hukum option[value='${kecamatan_bdn_hukum}']`).prop('selected', true);
                                        }
                                    }
                                });
                            }

                            if (kecamatan_bdn_hukum) {
                                $.ajax({
                                    url: "/borrower/data_kelurahan/" + kecamatan_bdn_hukum,
                                    method: "get",
                                    async: false,
                                    success: function(response) {
                                        let data_kelurahan = response ? JSON.parse(response) : []
                                        for(let i = 0; i<data_kelurahan.length; i++){
                                            $('#kelurahan_bdn_hukum').append($('<option>', { 
                                                value: data_kelurahan[i].text,
                                                text : data_kelurahan[i].text
                                            }));

                                            $(`#kelurahan_bdn_hukum option[value='${kelurahan_bdn_hukum}']`).prop('selected', true);
                                        }
                                    }
                                });
                            }
                            // $('#kecamatan_bdn_hukum').val(kecamatan_bdn_hukum);
                            // $('#kelurahan_bdn_hukum').val(kelurahan_bdn_hukum);
                            $('#kode_pos_bdn_hukum').val(kode_pos_bdn_hukum);

                            $('#rekening_bdn_hukum').val(rekening_bdn_hukum)
                            $('#nama_pemilik_rek_bdn_hukum').val(nama_pemilik_rek_bdn_hukum)
                            $('select[name=bank_bdn_hukum]').val(bank_bdn_hukum);

                            $('select[name=bidang_pekerjaan_bdn_hukum]').val(bidang_pekerjaan_bdn_hukum)
                            $('#omset_tahun_terakhir').val(omset_tahun_terakhir)
                            $('#tot_aset_tahun_terakhr').val(tot_aset_tahun_terakhr)
                            $('#btn_laporan_keuangan_val').val(laporan_keuangan)
                            let route = '{!! route("getUserFileForAdmin", ["filename" => "filename_val"]) !!}'
                            let file_src_rep = (laporan_keuangan) ? (laporan_keuangan).replaceAll('/', ':') : ''
                            route = route.replace('filename_val', file_src_rep)
                            $('#btn_laporan_keuangan').attr('href', route)

                            investor_pengurus = data.investor_pengurus ? data.investor_pengurus : []
                            $("#layout-pengurus").empty();
                            investor_pengurus.forEach((value, index) => {
                                console.log("index: ", index)
                                pengurus(value, index)
                                $("#add-foto-pengurus").empty();
                            });
                            // END: Badan Hukum
                        }

                        // Akun
                        $('input[name=old_username]').val(username);
                        $('input[name=username]').val(username);
                        $('input[name=email]').val(email);

                        // Rekening
                        $('input[name=va_number]').val(va_number);
                        
                        $('input[name=va_cimbs]').val(va_cimbs);
                        $('input[name=va_bsi]').val(va_bsi);
                        $('input[name=va_number_bdn_hukum]').val(va_number);
                        
                        $('input[name=va_cimbs_bdn_hukum]').val(va_cimbs);
                        $('input[name=va_bsi_bdn_hukum]').val(va_bsi);
                        // $('input[name=va_bsi_val]').val(data_va.bank451);

                        // Referral
                        $('input[name=kode_ref]').val(kode_ref);
                        $('input[name=nama_ref]').val(nama_marketer);

                        $('#keteranganSuspend').text(keteranganSuspend);
                        $('#tanggalSuspend').text(tanggalSuspend);
                        $('#namaSuspended').text(namaSuspended);
                        $('#status_saat_ini').val(status)
                        if (namaActived == "" || namaActived == null) {
                            $('#deactived').hide();
                        } else {
                            $('#deactived').show();
                            $('#namaActived').text(namaActived);
                            $('#keteranganAktif').text(keteranganSuspend);
                            $('#tanggalAktif').text(tanggalSuspend);
                        }

                        if (status == 'active' || status == 'pending') {
                            $('#pending').attr('style', 'display:block');
                            $('#suspend').attr('style', 'display:none');
                        } else if (status == 'suspend') {
                            $('#suspend').attr('style', 'display:block');
                            $('#pending').attr('style', 'display:none');
                        } else {
                            $('#suspend').attr('style', 'display:none');
                            $('#pending').attr('style', 'display:none');
                        }
                    } else {
                        $('#investor_id').val(id);

                        var option = '<option value="">--Pilih--</option>',
                            select_tgl = document.getElementById('tgl_lahir');

                        for (i = 1; i <= 31; i++) {
                            option += '<option value="' + i + '">' + i + '</option>';
                        }

                        select_tgl.innerHTML = option;

                        var data_bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'],
                            option_bln = '<option value="">--Pilih--</option>',
                            select_bln = document.getElementById('bln_lahir');

                        for (i = 0; i <= 11; i++) {
                            option_bln += '<option value="' + (i + 1) + '">' + data_bulan[i] + '</option>';
                        }
                        select_bln.innerHTML = option_bln;

                        // generate tahun
                        var select = document.getElementById('thn_lahir'),
                            year = new Date().getFullYear(),
                            html = '<option value="">--Pilih--</option>';
                        for (i = year; i >= year - 100; i--) {
                            html += '<option value="' + i + '">' + i + '</option>';
                        }
                        select.innerHTML = html;
                        // end generate tahun

                        $('input[name=username]').val(username);
                        $('input[name=email]').val(email);

                        if (status == 'active' || status == 'pending') {
                            $('#pending').attr('style', 'display:block');
                            $('#suspend').attr('style', 'display:none');
                        } else if (status == 'suspend') {
                            $('#suspend').attr('style', 'display:block');
                            $('#pending').attr('style', 'display:none');
                        } else {
                            $('#suspend').attr('style', 'display:none');
                            $('#pending').attr('style', 'display:none');
                        }

                        $('#tipe_proses').val('baru');
                        $('#submit_detil').text('').text('Insert Pendana');
                    }
                    Swal.close()
                    $('#detil').modal('show').addClass('show')
                    $('.modal-backdrop').addClass('show')
                },

                error: function(response) {
                    Swal.close()
                    Swal.fire({
                        title: "Error",
                        text: "Internal error",
                        type: "error",
                    })
                }
            });
        });

        $('#table_investor tbody').on('click', '#password', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;

            $('#investor_id2').val(id);
        });

        $('#table_investor tbody').on('click', '#change_status', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;

            $('#investor_id4').val(id);
        });

        $('#table_investor tbody').on('click', '#tambah_va', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            nama = data.nama_investor;
            console.log(id)
            $('#nama_user').html(nama);
            $('#investor_id3').val(id);
        });

        $('#table_investor tbody').on('click', '#kurang_dana', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            nama = data.nama_investor;
            console.log(id)
            $('#nama_user_kurang').html(nama);
            $('#investor_id_kurang').val(id);
        });


        $('#table_investor tbody').on('click', '#mutasi_investor', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            nama = data.nama_investor;
            console.log(nama)
            $('#nama_user_mutasi').html(nama);


            var table_mutasi = $('#table_list_mutasi').DataTable({
                "destroy": true,
                "bSort": false,
                "searching": false,
                "paging": false,
                "lengthChange": true,
                ajax: {
                    url: "/admin/investor/data_riwayat_mutasi/" + id,
                    method: "get",
                },

                "columns": [{
                        "data": "Keterangan"
                    },
                    {
                        "data": "Tanggal"
                    },
                    {
                        "data": "Debit"
                    },
                    {
                        "data": "Kredit"
                    },
                    {
                        "data": "Saldo"
                    },
                ],
                "columnDefs": [{
                        "targets": 0,
                        class: 'text-center',
                        //"visible" : false
                    },
                    {
                        "targets": 1,
                        class: 'text-center',
                        "render": function(data, type, row, meta) {
                            return row['Tanggal'];
                        }
                        //"visible" : false
                    },
                    {
                        "targets": 2,
                        class: 'text-center',
                        //"visible" : false
                    },
                    {
                        "targets": 3,
                        class: 'text-center',
                        //"visible" : false
                    },
                    {
                        "targets": 4,
                        class: 'text-center',
                        //"visible" : false
                    },
                ]

            });
            var dt1 = $('#startDate1').val('');
            var dt2 = $('#startDate2').val('');

            $('#search_btn').on('click', function() {

                var dt1 = $('#startDate1').val();
                var dt2 = $('#startDate2').val();

                if (dt1 == '' || dt2 == '') {
                    alert('Tanggal ada yang kosong.')
                } else {

                    var table_mutasi = $('#table_list_mutasi').DataTable({
                        "destroy": true,
                        "bSort": false,
                        "searching": false,
                        "paging": false,

                        ajax: {
                            url: "/admin/investor/data_riwayat_mutasi_date/" + id + "/" + dt1 + "/" + dt2,
                            method: "get",
                        },

                        "columns": [{
                                "data": "Keterangan"
                            },
                            {
                                "data": "Tanggal"
                            },
                            {
                                "data": "Debit"
                            },
                            {
                                "data": "Kredit"
                            },
                            {
                                "data": "Saldo"
                            },
                        ],
                        "columnDefs": [{
                                "targets": 0,
                                class: 'text-center',
                                //"visible" : false
                            },
                            {
                                "targets": 1,
                                class: 'text-center',
                                "render": function(data, type, row, meta) {
                                    return row['Tanggal'];
                                }
                                //"visible" : false
                            },
                            {
                                "targets": 2,
                                class: 'text-center',
                                //"visible" : false
                            },
                            {
                                "targets": 3,
                                class: 'text-center',
                                //"visible" : false
                            },
                            {
                                "targets": 4,
                                class: 'text-center',
                                //"visible" : false
                            },
                        ]

                    });

                }



            });


        });

        $('#table_investor tbody').on('click', '#detil_pendanaan', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            nama = data.nama_investor;
            console.log(nama)
            $('#nama_user_detilPendanaan').html(nama);
            $('#detilPendanaan').find('.tab-content').find('#aktif').find('.detilPendanaan-body').find('#detilAktif').html('');
            $('#detilPendanaan').find('.tab-content').find('#selesai').find('.detilPendanaan-body').find('#detilSelesai').html('');
            $.ajax({
                url: "/admin/investor/data_detil_pendanaan/" + id,
                method: "get",
                success: function(data) {
                    console.log(data.data_selesai)
                    var noAktif = 1,
                        tgl_invest, noSelesai = 1;
                    $.each(data.data, function(index, value) {
                        console.log(value.tgl_selesai)
                        tgl_invest = value.tanggal_invest.split(" ")[0].split("-");
                        tgl_mulai = value.tgl_mulai.split(" ")[0].split("-");
                        var route = "{{ route('admin.investor.edit_pendanaan_investor') }}";
                        $('#detilPendanaan').find('.tab-content').find('#aktif').find('.detilPendanaan-body').find('#detilAktif').append(
                            '<form action="' + route + '" method="POST">@csrf' +
                            '<table class="table text-left">' +
                            '<tr>' +
                            '<td style="width: 50px; word-wrap: break-word">' + noAktif + '</td>' +
                            '<td style="width: 210px; word-wrap: break-word">' + value.nama + '</td>' +
                            '<td style="width: 120px; word-wrap: break-word">' + tgl_mulai[2] + '-' + tgl_mulai[1] + '-' + tgl_mulai[0] + '</td>' +
                            '<td style="width: 200px; word-wrap: break-word">' + '<input type="text" class="form-control-plaintext" id="qty_' + value.id + '" name="nominal" value="' + numberFormat(value.total_dana.split(".")[0]) + '">' + '</td>' +
                            //   '<td style="width: 120px; word-wrap: break-word">'+ value.tipe_user+'</td>'+
                            '<td style="width: 150px; word-wrap: break-word">' + tgl_invest[2] + '-' + tgl_invest[1] + '-' + tgl_invest[0] + '</td>' +
                            '<td style="width: 100px; word-wrap: break-word">' +
                            '<input type="hidden" name="pendanaan_id" value="' + value.id + '">' +
                            '<button type="submit" class="btn btn-primary btn-sm" id="edit_pendanaan_' + value.id + '"disabled>Edit</button>' +
                            '</td>' +
                            '</tr>' +
                            '</table>' +
                            '</form>' +
                            //     '<div class="row">'+
                            //     '<form action="{{ route('admin.investor.edit_pendanaan_investor') }}" method="POST">@csrf'+
                            //     '<div class="col-1">'+ noAktif +'</div>'+
                            //     '<div class="col-3">'+ value.nama +'</div>'+
                            //     '<div class="col-3">'+ tgl_selesai[2]+'-'+tgl_selesai[1]+'-'+tgl_selesai[0] +'</div>'+
                            //     '<div class="col-3"><input type="text" class="form-control" id="qty_'+value.id+'" name="nominal" value="'+numberFormat(value.total_dana.split(".")[0])+'"></div>'+
                            //     // '<div ">'+ value.tipe_user +'</div>'+
                            //     // '<div class="col-2"><label >'+ tgl_invest[2]+'-'+tgl_invest[1]+'-'+tgl_invest[0] +'</label></div>'+
                            //     '<div class="col-1">'+
                            //         '<input type="hidden" name="pendanaan_id" value="'+value.id+'">'+
                            //         '<button class="btn btn-primary btn-sm" id="edit_pendanaan_'+value.id+'">Edit</button>'+
                            //     '</div>'+
                            // '</form>'+
                            // '</div>'+
                            '<hr>'
                        );
                        noAktif++

                        $("#qty_" + value.id).on('keyup', function() {
                            // 1
                            var $this = $(this);
                            var input = $this.val();

                            // 2
                            var input = input.replace(/[\D\s\._\-]/g, "");

                            // 3
                            input = input ? parseInt(input, 10) : 0;

                            // 4
                            $this.val(function() {
                                return (input === 0) ? 0 : input.toLocaleString("en-US");
                            });
                        })
                    })

                    $.each(data.data_selesai, function(index, value) {
                        // tgl_invest = value.tanggal_invest.split(" ")[0].split("-");
                        tgl_selesai = value.tgl_selesai.split(" ")[0].split("-");
                        var route = "{{ route('admin.investor.edit_pendanaan_selesai_investor') }}";
                        $('#detilPendanaan').find('.tab-content').find('#selesai').find('.detilPendanaan-body').find('#detilSelesai').append(
                            '<form action="' + route + '" method="POST">@csrf' +
                            '<table class="table text-left">' +
                            '<tr>' +
                            '<td style="width: 50px; word-wrap: break-word">' + noSelesai + '</td>' +
                            '<td style="width: 180px; word-wrap: break-word">' + value.nama + '</td>' +
                            '<td style="width: 120px; word-wrap: break-word">' + tgl_selesai[2] + '-' + tgl_selesai[1] + '-' + tgl_selesai[0] + '</td>' +
                            '<td style="width: 200px; word-wrap: break-word">' + '<input type="text" class="form-control-plaintext" id="qty_selesai_' + value.id + '" name="nominal_selesai" value="' + numberFormat(value.nominal.split(".")[0]) + '">' + '</td>' +
                            '<td style="width: 100px; word-wrap: break-word">' +
                            '<input type="hidden" name="pendanaan_id" value="' + value.id + '">' +
                            '<button type="submit" class="btn btn-primary btn-sm" id="edit_pendanaan_' + value.id + '"disabled>Edit</button>' +
                            '</td>' +
                            '</tr>' +
                            '</table>' +
                            '</form>' +
                            '<hr>'
                        );
                        noSelesai++

                        $("#qty_selesai_" + value.id).on('keyup', function() {
                            // 1
                            var $this = $(this);
                            var input = $this.val();

                            // 2
                            var input = input.replace(/[\D\s\._\-]/g, "");

                            // 3
                            input = input ? parseInt(input, 10) : 0;

                            // 4
                            $this.val(function() {
                                return (input === 0) ? 0 : input.toLocaleString("en-US");
                            });
                        })
                    })
                }
            });

        });
        /*
            $('#table_investor tbody').on( 'click', '#kelola_proyek', function () {
                var data = investorTable.row( $(this).parents('tr') ).data();
                id = data.idInvestor;
                // console.log(id)
                $('#kelolaProyek').find('.kelolaProyek-body').html('');
                $.ajax({
                    url : "/admin/investor/data_proyek/"+id,
                    method : "get",
                    success:function(data)
                    {
                        console.log(data.data_total)
                        console.log(data.data_proyek)
                        $('#total_dana').val(numberFormat(data.data_total.unallocated));
                        // var no = 1;
                        $.each(data.data_proyek,function(index,value){
                            var bagi_hasil = value.profit_margin.split(".");
                            var selisih = dateDiff(new Date(),value.tgl_selesai_penggalangan);
                            var sisaneed = (value.total_need - value.total_terkumpul)/1000000;
                            selisih += 1;
                            $('#kelolaProyek').find('.kelolaProyek-body').append(
                                '<div class="row">'+
                                    '<div class="col-3">'+value.nama+'</div>'+
                                    '<div class="col-1">'+bagi_hasil[0]+'%</div>'+
                                    '<div class="col-1">'+numberFormat(value.harga_paket)+'</div>'+
                                    '<div class="col-2">'+numberFormat(value.total_terkumpul)+'</div>'+
                                    '<div class="col-1">'+selisih+' hari</div>'+
                                    '<div class="col-1">'+sisaneed+' Paket</div>'+
                                    '<div class="col-3 mm form-inline">'+
                                        // '<form class="form-inline" action="{{ route('admin.pendanaan') }}" method="POST">@csrf'+   
                                        // '<form class="form-inline" action="{{ route('admin.pendanaan') }}" method="POST">@csrf'+                
                                        '<input type="hidden" name="investor_id" class="iid" value="'+data.data_total.investor_id+'">'+
                                        '<input type="hidden" name="proyek_id" class="pid" value="'+value.id+'">'+
                                        '<input type="hidden" name="tanggal_invest" value="'+new Date().toLocaleDateString("en-US")+'">'+
                                        '<input type="hidden" name="sisaneed" class="sisaneed"  value="'+sisaneed+'">'+
                                        '<input type="number" class="form-control col-sm-6 paket" name="jumlah_paket" placeholder="Paket">'+
                                        '<button class="btn btn-primary btn-sm ml-1 tet"  title="Paket Harus Kurang dari atau Sama dengan Sisa Paket">Tambah</button>'+
                                        // '</form>'+
                                    '</div>'+
                                '</div>'+
                                '<hr>'
                            );
                        // no++

                            // $('#tambah_proyek_'+value.id).on('click',function(e){
                            //     e.preventDefault();
                            //     $(this).attr('style','display:none');
                            // });
                        })
                        $('.tet').on('click',function(){
                            var x = parseInt($(this).closest('.mm').find('.sisaneed').val());
                            var iid = parseInt($(this).closest('.mm').find('.iid').val());
                            var pid = parseInt($(this).closest('.mm').find('.pid').val());
                            var jumlah = parseInt($(this).closest('.mm').find('.paket').val());
                            if (isNaN(jumlah) || jumlah == 0) {
                                swal('Data yang dimasukan tidak valid !');
                            }
                            else {
                                if (is_alphaDash(jumlah) == true)
                                {
                                    swal('Data yang dimasukan tidak valid !');
                                }
                                else
                                {
                                    if(jumlah <= x){
                                        $.ajax({
                                            url : "/admin/investor/addPendanaan/",
                                            method : "post",
                                            data : {investor_id:iid,proyek_id:pid,jumlah_paket:jumlah},
                                            success:function(data)
                                            {
                                                console.log('sukses');
                                                location.reload(); 
                                            }
                                        });
                                    }else if(jumlah > x){
                                        swal('Permintaan Paket Melebihi Paket Tersedia !');
                                    }
                                }
                            }
                            
                        });
                    }
                });  
            });
			*/

        // Nampilin Proyek yg di pendanaan aktif blm ada 16 september 2020 -Anas
        $('#table_investor tbody').on('click', '#kelola_proyek', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            // console.log(id)
            $('#kelolaProyek').find('.kelolaProyek-body').html('');
            $.ajax({
                url: "/admin/investor/data_proyek/" + id,
                method: "get",
                success: function(data) {
                    console.log(data.data_total)
                    console.log(data.data_proyek)
                    $('#total_dana').val(numberFormat(data.data_total.unallocated));
                    // var no = 1;
                    $.each(data.data_proyek, function(index, value) {
                        var total_terkumpul = " ";
                        var total_danapendanan = " ";
                        if (value.total_terkumpul == null || value.total_danapendanan == null) {
                            total_terkumpul = 0;
                            total_danapendanan = 0;
                        } else {
                            total_terkumpul = value.total_terkumpul;
                        }
                        var bagi_hasil = value.profit_margin.split(".");
                        var selisih = dateDiff(new Date(), value.tgl_selesai_penggalangan);
                        var sisaneed = (value.total_need - total_terkumpul) / 1000000;
                        selisih += 1;
                        $('#kelolaProyek').find('.kelolaProyek-body').append(
                            '<div class="row">' +
                            '<div class="col-3">' + value.nama + '</div>' +
                            '<div class="col-1">' + bagi_hasil[0] + '%</div>' +
                            '<div class="col-1">' + numberFormat(value.harga_paket) + '</div>' +
                            '<div class="col-2">' + numberFormat(total_terkumpul) + '</div>' +
                            '<div class="col-1">' + selisih + ' hari</div>' +
                            '<div class="col-1">' + sisaneed + ' Paket</div>' +
                            '<div class="col-3 mm form-inline">' +
                            // '<form class="form-inline" action="{{ route('admin.pendanaan') }}" method="POST">@csrf'+   
                            // '<form class="form-inline" action="{{ route('admin.pendanaan') }}" method="POST">@csrf'+                
                            '<input type="hidden" name="investor_id" class="iid" value="' + data.data_total.investor_id + '">' +
                            '<input type="hidden" name="proyek_id" class="pid" value="' + value.id + '">' +
                            '<input type="hidden" name="tanggal_invest" value="' + new Date().toLocaleDateString("en-US") + '">' +
                            '<input type="hidden" name="sisaneed" class="sisaneed"  value="' + sisaneed + '">' +
                            '<input type="number" class="form-control col-sm-6 paket" name="jumlah_paket" placeholder="Paket">' +
                            '<button class="btn btn-primary btn-sm ml-1 tet"  title="Paket Harus Kurang dari atau Sama dengan Sisa Paket">Tambah</button>' +
                            // '</form>'+
                            '</div>' +
                            '</div>' +
                            '<hr>'
                        );
                        // no++

                        // $('#tambah_proyek_'+value.id).on('click',function(e){
                        //     e.preventDefault();
                        //     $(this).attr('style','display:none');
                        // });
                    })
                    // Notifikasi tambah dana 18 September 2020 - Anas
                    $('.tet').on('click', function() {
                        var x = parseInt($(this).closest('.mm').find('.sisaneed').val());
                        var iid = parseInt($(this).closest('.mm').find('.iid').val());
                        var pid = parseInt($(this).closest('.mm').find('.pid').val());
                        var jumlah = parseInt($(this).closest('.mm').find('.paket').val());

                        if (isNaN(jumlah) || jumlah == 0) {
                            swal('Data yang dimasukan tidak valid !');
                        } else {
                            if (is_alphaDash(jumlah) == true) {
                                swal('Data yang dimasukan tidak valid !');
                            } else {
                                if (jumlah <= x) {
                                    $("#kelolaProyek").modal("hide");
                                    const imageURL = "{{ url('img/questionmark.png') }}";
                                    console.log(imageURL);
                                    swal({
                                            title: "Tambah Nominal Pendanaan",
                                            type: "warning",
                                            allowEscapeKey: false,
                                            allowOutsideClick: false,
                                            showCancelButton: true,
                                            cancelButtonText: "Batal",
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "Lanjutkan",
                                            showLoaderOnConfirm: true,
                                            closeOnConfirm: false
                                        },
                                        function(isConfirm) {
                                            if (isConfirm) {
                                                $.ajax({
                                                    url: "/admin/investor/addPendanaan/",
                                                    method: "post",
                                                    data: {
                                                        investor_id: iid,
                                                        proyek_id: pid,
                                                        jumlah_paket: jumlah
                                                    },
                                                    success: function(data) {
                                                        console.log('sukses');
                                                        console.log(data.status);
                                                        if (data.status == 0) {
                                                            $("#canceltambahdana").trigger("click");
                                                            swal({
                                                                    title: "Notifikasi",
                                                                    text: data.keterangan,
                                                                    type: "error",
                                                                    allowOutsideClick: false
                                                                },
                                                                function() {
                                                                    $("#overlay").css('display', 'block');
                                                                    location.reload();
                                                                }
                                                            );
                                                        } else {
                                                            swal({
                                                                    title: "Notifikasi",
                                                                    text: data.keterangan,
                                                                    type: "success",
                                                                    allowOutsideClick: false
                                                                },
                                                                function() {
                                                                    $("#overlay").css('display', 'block');
                                                                    location.reload();
                                                                }
                                                            );
                                                        }
                                                        // location.reload(); 
                                                    }
                                                });
                                                return true;
                                            }
                                            $("#kelolaProyek").modal("show");
                                            return false;
                                        }
                                    )
                                    $('.sweet-alert button.cancel').css('background-color', '#FFA500')
                                } else if (jumlah > x) {
                                    swal('Permintaan Paket Melebihi Paket Tersedia !');
                                }
                            }
                        }



                    });
                    /*
                        $('.tet').on('click',function(){
                            var x = parseInt($(this).closest('.mm').find('.sisaneed').val());
                            var iid = parseInt($(this).closest('.mm').find('.iid').val());
                            var pid = parseInt($(this).closest('.mm').find('.pid').val());
                            var jumlah = parseInt($(this).closest('.mm').find('.paket').val());
                            if (isNaN(jumlah) || jumlah == 0) {
                                swal('Data yang dimasukan tidak valid !');
                            }
                            else {
                                if (is_alphaDash(jumlah) == true)
                                {
                                    swal('Data yang dimasukan tidak valid !');
                                }
                                else
                                {
                                    if(jumlah <= x){
                                        $.ajax({
                                            url : "/admin/investor/addPendanaan/",
                                            method : "post",
                                            data : {investor_id:iid,proyek_id:pid,jumlah_paket:jumlah},
                                            success:function(data)
                                            {
                                                console.log('sukses');
                                                location.reload(); 
                                            }
                                        });
                                    }else if(jumlah > x){
                                        swal('Permintaan Paket Melebihi Paket Tersedia !');
                                    }
                                }
                            }
                            
                        });
						*/
                }
            });
        });

        function is_alphaDash(str) {
            regexp = /-/g;

            if (regexp.test(str)) {
                return true;
            } else {
                return false;
            }
        }
        $('#table_investor tbody').on('click', '#reg_digisign', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            console.log(id)
            $.ajax({
                url: "/admin/investor/regDigiSign/" + id,
                method: "get",
                beforeSend: function() {
                    $("#overlay").css('display', 'block');
                },
                success: function(data) {
                    $("#overlay").css('display', 'none');
                    var dataJSON = JSON.parse(data.status_all);
                    console.log(dataJSON);
                    swal({
                            title: "Notifikasi",
                            text: dataJSON.JSONFile.notif,
                            type: "info"
                        },
                        function() {
                            if (dataJSON.JSONFile.result == '00') {
                                if (dataJSON.JSONFile.info) {
                                    var url_notif = dataJSON.JSONFile.info.split('https://')[1];
                                    console.log(url_notif);
                                    $.ajax({
                                        url: "/admin/investor/callbackDigiSignInvestor/",
                                        method: "post",
                                        data: {
                                            user_id: id,
                                            provider_id: 1,
                                            status: dataJSON.JSONFile.notif,
                                            step: 'register',
                                            url: url_notif
                                        },
                                        success: function(data) {
                                            console.log(data.status)
                                        }
                                    });
                                    window.open(dataJSON.JSONFile.info, '_blank');
                                }
                            }
                        }
                    );
                },
                error: function(request, status, error) {
                    $("#overlay").css('display', 'none');
                    alert(status)
                }
            });

        });

        $('#table_investor tbody').on('click', '#send_digisign', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            console.log(id)
            $.ajax({
                url: "/admin/investor/sendDigiSign/" + id,
                method: "get",
                beforeSend: function() {
                    $("#overlay").css('display', 'block');
                },
                success: function(data) {
                    $("#overlay").css('display', 'none');
                    var dataJSON = JSON.parse(data.status_all);
                    console.log(dataJSON);
                    swal({
                            title: "Notifikasi",
                            text: dataJSON.JSONFile.notif,
                            type: "info"
                        },
                        function() {
                            investorTable.ajax.reload();
                        }
                    );

                },
                error: function(request, status, error) {
                    $("#overlay").css('display', 'none');
                    alert(status)
                }
            });

        });

        $('#table_investor tbody').on('click', '#sign_digisign', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            console.log(id)
            $.ajax({
                url: "/admin/investor/signDigiSign/" + id,
                method: "get",
                beforeSend: function() {
                    $("#overlay").css('display', 'block');
                },
                success: function(data) {
                    $("#overlay").css('display', 'none');
                    console.log(data.status_all)
                },
                error: function(request, status, error) {
                    $("#overlay").css('display', 'none');
                    alert(status)
                }
            });

        });

        $('#table_investor tbody').on('click', '#create_doc', function() {
            var data = investorTable.row($(this).parents('tr')).data();
            id = data.idInvestor;
            console.log(id)
            $.ajax({
                url: "/admin/investor/createDocDigisign/" + id,
                method: "get",
                success: function(data) {
                    console.log(data.status)

                }
            });

        });

        $(".qty").on('keyup', function() {
            // $('.qty').keyup(function() {
            // 1
            var $this = $(this);
            var input = $this.val();

            // 2
            var input = input.replace(/[\D\s\._\-]+/g, "");

            // 3
            input = input ? parseInt(input, 10) : 0;

            // 4
            $this.val(function() {
                return (input === 0) ? "" : input.toLocaleString("en-US");
            });
        })

        $('#provinsi').on('change', function(e) {
            e.preventDefault();
            var kode_provinsi = this.value;
            $('#kota').empty();
            $.ajax({
                url: "/admin/getKota/" + kode_provinsi,
                method: "get",
                success: function(data) {
                    $.each(data.kota, function(index, value) {
                        $('#kota').append(
                            '<option value="' + value.kode_kota + '">' + value.nama_kota + '</option>'
                        );
                    })
                }
            });
        });
    });

    $('#gantiRek').on('click',function(){
        alert('aw');
    });
</script>

{{-- START: Other Function --}}
<script>
    function email_filter_validation() {
        var email = document.getElementById("email").value;
        var email_instan1 = email.includes("mailinator");
        var email_instan2 = email.includes("urhen");
        var email_instan3 = email.includes("guerrillamail");
        var email_instan4 = email.includes("maildrop");
        var email_instan5 = email.includes("wemel");
        var email_instan6 = email.includes("mt2015");
        var email_instan7 = email.includes("dispostable");
        var email_instan8 = email.includes("tempr");
        var email_instan9 = email.includes("discard");
        var email_instan10 = email.includes("mailcatch");
        var email_instan11 = email.includes("einroit");
        var email_instan12 = email.includes("mailnesia");
        var email_instan13 = email.includes("yopmail");
        //alert(email);
        if (email_instan1 || email_instan2 || email_instan3 || email_instan4 || email_instan5 || email_instan6 || email_instan7 || email_instan8 || email_instan9 || email_instan10 || email_instan11 || email_instan12 || email_instan13) {
            //alert('Domain Email Anda tidak diizinkan. Silahkan gunakan domain email lain');
            $('#error_email').html('<b id="emailerror">Domain Email Anda tidak diizinkan.Silahkan gunakan domain email lain</b>');
            $('#submit_detil').attr('disabled', true);

        } else {
            $('#emailerror').hide();
            $('#submit_detil').attr('disabled', false);
            return true;
        }
    }

    function dateDiff(date1, date2) {
        dt1 = new Date(date1);
        dt2 = new Date(date2);
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
    }

    function numberFormat(num) {
        var str = num.toString().replace("$", ""),
            parts = false,
            output = [],
            i = 1,
            formatted = null;
        if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != ",") {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");
        return (formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    };

    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                }
            });
        };
    }(jQuery));

    function checkPhoneNumber(kd_operator_id, number, label, is_unique) {
        let kode_operator = $('#'+kd_operator_id).val()
        let number_length = number.length
        let investor_id   = $('#investor_id').val()

        if (number_length > 7) {
            $.ajax({
                url: "/user/checkPhoneForAdmin/" + kode_operator + number + "/" + investor_id,
                type: 'GET',
                beforeSend: function() {
                    $('#'+label).text('').append('<i class="fa fa-spin fa-spinner"></i>')
                    $('#'+is_unique).val('1')
                },
                success: function(data) {
                    console.log(data)
                    if (data.status != 'ada') {
                        $('#'+label).text('No Telp / HP').removeClass('text-danger')
                        $('#'+is_unique).val('0')
                    } else {
                        $('#'+label).text('No Telah Terdaftar').addClass('text-danger')
                        $('#'+is_unique).val('1')
                    }
                }
            });
        }
    }

    function checkUserName(this_value, label, is_unique) {
        let username        = this_value
        let username_length = this_value.length
        let investor_id     = $('#investor_id').val()

        if (username_length > 5) {
            $.ajax({
                url: "/user/checkUsernameForAdmin/" + this_value + "/" + investor_id,
                method: "get",
                beforeSend: function() {
                    $('#'+label).text('').append('<i class="fa fa-spin fa-spinner"></i>')
                    $('#'+is_unique).val('1')
                },
                success: function(data) {
                    console.log(data)
                    if (data.status == 'ada') {
                        $('#'+label).text('Akun Telah Terdaftar').addClass('text-danger')
                        $('#'+is_unique).val('1')
                    } else if (data.status == 'kosong') {
                        $('#'+label).text('Akun').removeClass('text-danger')
                        $('#'+is_unique).val('0')
                    } else {
                        $('#'+label).text('').append('<i class="fa fa-spin fa-spinner"></i>').removeClass('text-danger')
                    }
                }
            });
        }
    }

    function checkEmail(this_value, label, is_unique) {
        let email        = this_value
        let email_length = this_value ? this_value.length : 0
        let investor_id     = $('#investor_id').val()
        console.log(this_value, label, is_unique)
        if (email_length > 5) {
            $.ajax({
                url: "/user/checkEmailForAdmin/" + this_value + "/" + investor_id,
                method: "get",
                beforeSend: function() {
                    $('#'+label).text('').append('<i class="fa fa-spin fa-spinner"></i>')
                    $('#'+is_unique).val('1')
                },
                success: function(data) {
                    if (data.status == 'failed') {
                        $('#'+label).text(data.message).addClass('text-danger')
                        $('#'+is_unique).val('1')
                    } else {
                        if (data.status != 'ada') {
                            $('#'+label).text('Email').removeClass('text-danger')
                            $('#'+is_unique).val('0')
                        } else {
                            $('#'+label).text('Email Telah Terdaftar').addClass('text-danger')
                            $('#'+is_unique).val('1')
                        }
                    }
                }
            });
        }
    }

    const checkKtporPassport = (this_value, label, jenis_identitas, is_unique) => {
        let old_ktp     = $('#old_no_ktp').val()
        let new_ktp     = this_value
        let ktp_length  = new_ktp ? this_value.length : 0

        let old_passport    = $('#old_no_passpor').val()
        let new_passport    = this_value
        let passport_length = new_passport ? new_passport.length : 0
        let investor_id = $('#investor_id').val()

        if (jenis_identitas == 1) { //KTP
            if (old_ktp != new_ktp) {
                if (ktp_length == 16) {
                    $.ajax({
                        url: "/user/checkKtpOrPassportForAdmin/" + new_ktp + "/" + investor_id,
                        method: "get",
                        beforeSend: function() {
                            $('#'+label).text('').append('<i class="fa fa-spin fa-spinner"></i>')
                            $('#'+is_unique).val('1')
                        },
                        success: function(data) {
                            console.log(data)
                            if (data.status != 'ada') {
                                $('#'+label).text('No KTP').removeClass('text-danger')
                                $('#'+is_unique).val('0')
                            } else {
                                $('#'+label).text('No KTP Telah Terdaftar').addClass('text-danger')
                                $('#'+is_unique).val('1')
                            }
                        }
                    });
                }
            } else {
                $('#'+label).text('No KTP').removeClass('text-danger')
                $('#'+is_unique).val('0')
            }
        } else { //Passport
            if (old_passport != new_passport) {
                if (passport_length == 11) {
                    $.ajax({
                        url: "/user/checkKtpOrPassportForAdmin/" + new_passport + "/" + investor_id,
                        method: "get",
                        beforeSend: function() {
                            $('#'+label).text('').append('<i class="fa fa-spin fa-spinner"></i>')
                            $('#'+is_unique).val('1')
                        },
                        success: function(data) {
                            console.log(data)
                            if (data.status != 'ada') {
                                $('#'+label).text('No Passport').removeClass('text-danger')
                                $('#'+is_unique).val('0')
                            } else {
                                $('#'+label).text('No Passport Telah Terdaftar').addClass('text-danger')
                                $('#'+is_unique).val('1')
                            }
                        }
                    });
                }
            } else {
                $('#'+label).text('No Passport').removeClass('text-danger')
                $('#'+is_unique).val('0')
            }
        }
    }

    function getIdMutasi(id) {
        var table_mutasi = $('#table_list_mutasi_proyek_investor').DataTable({
            "destroy": true,
            "bSort": false,
            "searching": true,
            "paging": false,
            "lengthChange": true,
            ajax: {
                url: "/admin/investor/data_mutasi_proyek/" + id,
                method: "get",
            },

            "columns": [{
                    "data": "nama"
                },
                {
                    "data": "alamat"
                },
                {
                    "data": "tgl_mulai"
                },
                {
                    "data": "tanggal_invest"
                },
                {
                    "data": "tgl_selesai"
                },
                {
                    "data": "aksi"
                },
            ],
            "columnDefs": [{
                    "targets": 0,
                    class: 'text-left',
                    //"visible" : false
                },
                {
                    "targets": 1,
                    class: 'text-left',
                    // "render": function ( data, type, row, meta ) {
                    //   return row['nama'];
                    // }
                    //"visible" : false
                },
                {
                    "targets": 2,
                    class: 'text-left',
                    //"visible" : false
                },
                {
                    "targets": 3,
                    class: 'text-left',
                    //"visible" : false
                },
                {
                    "targets": 4,
                    class: 'text-left',
                    //"visible" : false
                },
                {
                    "targets": 5,
                    class: 'text-left',
                    "render": function(data, type, row, meta) {
                        return '<button class="btn btn-info btn-sm active" onclick="myFunction(' + row['pendanaanAktifId'] + ')" id="klik">Riwayat Mutasi Proyek</button>';
                    }
                    //"visible" : false
                },
            ]

        });
    }

    function myFunction(id) {

        var html = "";
        var htmladd = "";
        $.ajax({
            url: "/admin/investor/data_detil_mutasi_proyek/" + id,
            method: "get",
            success: function(data) {
                $.each(data.datadetil, function(index, value) {
                    htmladd = "<tr><td>" + value.tipe + "</td><td> Rp. " + value.nominal + "</td><td>" + value.created_at + "</td></tr>";
                    html += htmladd;
                })
                console.log(html);
                Swal.fire({
                    html: "<h1>Riwayat Mutasi Proyek</h1><br>" +
                        "<table border='1' align='center' width='95%'><thead><td width='40%'>Riwayat</td><td width='30%'>Nominal</td><td width='30%'>Tanggal</td></thead>" +
                        "<tbody>" +
                        html +
                        "</tbody></table>"

                })
            }
        });
    }

    const isReferalExist = (this_value, label, is_exist, referral_name) => {
        let referal_code = this_value

        if (referal_code) {
            $.ajax({
                url: "/user/checkReferalForAdmin/" + this_value + "/" + investor_id,
                method: "get",
                beforeSend: function() {
                    $('#'+label).text('').append('<i class="fa fa-spin fa-spinner"></i>')
                    $('#'+is_exist).val('1')
                },
                success: function(data) {
                    if (data.status == 'ada') {
                        $('#'+label).text('Kode Refferal').removeClass('text-danger')
                        $('#'+is_exist).val('0')
                        $('#'+referral_name).val(data.referral_name)
                    } else {
                        $('#'+label).text('Kode Refferal Tidak Terdaftar').addClass('text-danger')
                        $('#'+is_exist).val('1')
                        $('#'+referral_name).val(data.referral_name)
                    }
                }
            });
        } else {
            $('#'+label).text('Kode Refferal').removeClass('text-danger')
            $('#'+is_exist).val('0')
            $('#'+referral_name).val('')
        }
    }

    // START: Alamat
    const provinsiChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/admin/getKota/"+thisValue, function(data_kota){
            $.each(data_kota.kota,function(index,value){
                $('#'+nextId).append(
                    '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
                );
            })
        });
    }

    const kotaChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/getKecamatan/"+thisValue, function(data_kecamatan){
        for(let i = 0; i<data_kecamatan.length; i++){
            $('#'+nextId).append($('<option>', { 
                value: data_kecamatan[i].text,
                text : data_kecamatan[i].text
            }));
        }
        });
    }

    const kecamatanChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kelurahan/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kelurahanChange = (thisValue, thisId, nextId) => {
        $.getJSON("/borrower/data_kode_pos/"+thisValue+"/", function(data){
            $('#'+nextId).val(data[0].text)
        });
    }
    // END: Alamat

    const formatPhoneNumber = (kd_operator, number) => {
        let number_val = $('#'+number).val()
        let kd_operator_val = $('#'+kd_operator).val()
        
        if (kd_operator_val == '62' && number_val) {
            if (number_val.charAt(0) != '8') {
                $('#'+number).val(number_val.substring(1));
            }
        }
    }
    const pengurus = (data, n) => {
        let html = (`
        <div class="mt-4 layout-pengurus" id="layout-pengurus-${n}">
            <div class="card">
                <div class="card-header card-header-pengurus" id="card-pengurus-${n}" data-toggle="collapse" data-target="#collapse-${n}"
                    aria-expanded="true" aria-controls="collapse-${n}">
                    <p class="h6 font-weight-bold">
                        Pengurus ${n+1} ${n == 0 ? '(Mewakili Sebagai Perusahaan)' : ''} <span name="card-title[]"></span> <i class="fa fa-angle-up float-right"></i><i
                            class="fa fa-angle-down float-right"></i>
                    </p>
                    </h5>
                </div>
        
                <div id="collapse-${n}" class="collapse coll" aria-labelledby="card-pengurus-${n}"
                    data-parent="#layout-pengurus-${n}">
                    <div class="card-body">
        
                        <div id="pengurus">
                            <!-- START: Baris 1 -->
                            <div class="form-row">
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama_pengurus_${n}">Nama Pengurus</label>
                                        <input type="hidden" id="pengurus_id_${n}" name="pengurus_id[]" value="${data.pengurus_id}">
                                        <input class="form-control" type="text" id="nama_pengurus_${n}"
                                            maxlength="30" name="nama_pengurus[]" placeholder="Masukkan Nama Pengurus..."
                                            value="${data.nm_pengurus ? data.nm_pengurus : ''}"
                                            >
                                    </div>
                                </div>
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jns_kelamin_pengurus_${n}">Jenis Kelamin</label>
                                        <select id="jns_kelamin_pengurus_${n}" name="jns_kelamin_pengurus[]"
                                            class="form-control custom-select">
                                            <option value="">-- Pilih Satu --</option>
                                            @foreach ($master_jenis_kelamin as $b)
                                                <option value="{{$b->id_jenis_kelamin}}">
                                                    {{$b->jenis_kelamin}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_ktp_pengurus_${n}" for="ktp_pengurus">Nomor KTP</label>
                                        <input type="hidden" id="is_ktp_pengurus" value="0">
                                        <input class="form-control" type="text" id="ktp_pengurus_${n}" name="ktp_pengurus[]"
                                            placeholder="Masukkan nomor KTP" minlength="16" maxlength="16"
                                            onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid(); checkKtporPassport(this.value, 'label_ktp_pengurus_${n}', 1, 'is_ktp_pengurus')"
                                            value="${data.nik_pengurus ? data.nik_pengurus : ''}">
                                    </div>
                                </div>
                            </div>
                            <!-- END: Baris 1 -->
        
                            {{-- START: Baris 2 --}}
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tempat_lahir_pengurus_${n}">Tempat Lahir</label>
                                        <input class="form-control" type="text" maxlength="35" id="tempat_lahir_pengurus_${n}"
                                            name="tempat_lahir_pengurus[]" placeholder="Masukkan tempat lahir" 
                                            value="${data.tempat_lahir ? data.tempat_lahir : ''}">
                                    </div>
                                </div>
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="tanggal_lahir_pengurus_${n}">Tanggal Lahir</label>
                                        <input class="form-control" type="date" id="tanggal_lahir_pengurus_${n}"
                                            name="tanggal_lahir_pengurus[]" max="<?= date('Y-m-d'); ?>"
                                            placeholder="Masukkan tanggal lahir" onchange="$(this).valid()"
                                            value=${data.tgl_lahir ? data.tgl_lahir : ''}>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_kode_operator_pengurus_${n}" for="kode_operator_pengurus_${n}">No Telepon</label>
                                            <div class="row">
                                                <div class="col">
                                                    <select name="kode_operator_pengurus[]" id="kode_operator_pengurus_${n}" class="form-control custom-select" onchange="if(${n} == 0){ checkPhoneNumber('kode_operator_pengurus_${n}', $('#no_telp_pengurus_${n}').val(), 'label_kode_operator_pengurus_${n}', 'is_phone_pengurus_unique')}" required>
                                                        @foreach ($master_kode_operator as $item)
                                                        <option value="{{$item->kode_operator}}">
                                                            ({{$item->kode_operator}}) {{$item->negara}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <input type="hidden" id="is_phone_pengurus_unique" value="0">
                                                    <input type="text" maxlength="15" name="no_telp_pengurus[]" id="no_telp_pengurus_${n}"
                                                        onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); formatPhoneNumber('kode_operator_pengurus_${n}','no_telp_pengurus_${n}')"
                                                        onchange="if(${n} == 0){ checkPhoneNumber('kode_operator_pengurus_${n}', this.value, 'label_kode_operator_pengurus_${n}', 'is_phone_pengurus_unique')};"
                                                        class="form-control" placeholder="Contoh:8xxxxxxxxxx" value="${(data.kode_operator) && (data.no_tlp) ? parseInt((data.no_tlp).replace(data.kode_operator, '')) : ((data.no_tlp) ? parseInt(data.no_tlp) : '')}" required>
                                                </div>
                                            </div>
                                    </div>
        
                                </div>
        
                            </div>
                            {{-- END: Baris 2 --}}
        
                            {{-- START: Baris 3 --}}
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="agama_pengurus_${n}" class="col-12">Agama</label>
                                        <div class="col-12">
                                            <select class="form-control custom-select" id="agama_pengurus_${n}"
                                                name="agama_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_agama as $b)
                                                <option value="{{$b->id_agama}}"
                                                    {{ old('agama_pengurus[]') == $b->agama ? 'selected' : '' }}>
                                                    {{$b->agama}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pendidikan_terakhir_pengurus_${n}">Pendidikan Terakhir</label>
                                        <select class="form-control custom-select" id="pendidikan_terakhir_pengurus_${n}"
                                            name="pendidikan_terakhir_pengurus[]">
                                            <option value="">-- Pilih Satu --</option>
                                            @foreach ($master_pendidikan as $b)
                                            <option value="{{$b->id_pendidikan}}"
                                                {{ old('pendidikan_terakhir_pengurus[]') == $b->id_pendidikan ? 'selected' : '' }}>
                                                {{$b->pendidikan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
        
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="npwp_pengurus_${n}">Nomor NPWP</label>
                                        <input class="form-control " type="text"
                                            id="npwp_pengurus_${n}" name="npwp_pengurus[]" pattern=".{15,15}"
                                            minlength="15" maxlength="15"
                                            onkeyup="this.value = this.value.replace(/[^0-9]/g, '');$(this).valid()"
                                            placeholder="Masukkan nomor NPWP"
                                            value="${data.npwp ? data.npwp : ''}">
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 3 --}}
        
                            {{-- START: Baris 4 --}}
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="jabatan_pengurus_${n}">Jabatan</label>
                                        <select class="form-control custom-select" id="jabatan_pengurus_${n}"
                                            name="jabatan_pengurus[]">
                                            <option value="">-- Pilih Satu --</option>
                                            @foreach ($master_jabatan as $jabs)
                                            <option value="{{$jabs->id}}">
                                                {{$jabs->jabatan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-4"></div>
                            </div>
                            {{-- END: Baris 4 --}}
        
                            {{-- START: Baris 5 --}}
                            <div class="form-row">
                                <div class="col-12">
                                    <h6 class="line my-4 font-weight-bold">Alamat Domisili Pengurus &nbsp</h6>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="alamat_pengurus_${n}">Alamat</label>
                                        <textarea class="form-control form-control-lg" maxlength="90" id="alamat_pengurus_${n}"
                                            name="alamat_pengurus[]" rows="6" placeholder="Masukkan alamat lengkap Anda.."
                                            >${data.alamat ? data.alamat : ''}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="provinsi_pengurus_${n}">Provinsi </label>
                                        <select class="form-control custom-select" id="provinsi_pengurus_${n}"
                                            name="provinsi_pengurus[]" onchange="provinsiChange(this.value, this.id, 'kota_pengurus_${n}')">
                                            <option value="">-- Pilih Satu --</option>
                                            @foreach ($master_provinsi as $data)
                                            <option value={{$data->kode_provinsi}}>
                                                {{$data->nama_provinsi}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kota_pengurus_${n}">Kota/Kabupaten</label>
                                        <select class="form-control custom-select" id="kota_pengurus_${n}"
                                            name="kota_pengurus[]" onchange="kotaChange(this.value, this.id, 'kecamatan_pengurus_${n}')">
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 5 --}}
        
                            {{-- START: Baris 6 --}}
                            <div class="form-row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kecamatan_pengurus_${n}">Kecamatan</label>
                                        <select class="form-control custom-select" id="kecamatan_pengurus_${n}" name="kecamatan_pengurus[]" onchange="kecamatanChange(this.value, this.id, 'kelurahan_pengurus_${n}')">
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kelurahan_pengurus_${n}">Kelurahan</label>
                                        <select class="form-control custom-select" id="kelurahan_pengurus_${n}" name="kelurahan_pengurus[]" onchange="kelurahanChange(this.value, this.id, 'kode_pos_pengurus_${n}')">
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kode_pos_pengurus_${n}">Kode Pos</label>
                                        <input class="form-control" type="text" id="kode_pos_pengurus_${n}"
                                            name="kode_pos_pengurus[]" placeholder="--" value="${data.kode_pos ? data.kode_pos : ''}" maxlength="5" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" readonly>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 6 --}}
        
                            {{-- START: Baris 7 --}}
                            {{-- START: Foto Pengurus 1 --}}
                            <div class="row">
                                <div class="col-12">
                                    <h6 class="line my-4 font-weight-bold">Foto Pengurus &nbsp</h6>
                                </div>
                                
                                <div id="add-foto-pengurus-${n}"></div>
                            </div>
                            {{-- END: Foto Pengurus 1 --}}
                            {{-- END: Baris 7 --}}
                        </div>
        
                    </div>
        
                </div>
            </div>
        </div>
        `);

        $("#layout-pengurus").append(html);

        $(`select[id="kode_operator_pengurus_${n}"]`).val(data.kode_operator)
        $(`select[id="jns_kelamin_pengurus_${n}"]`).val(data.jenis_kelamin)
        $(`select[id="agama_pengurus_${n}"]`).val(data.agama)
        $(`select[id="pendidikan_terakhir_pengurus_${n}"]`).val(data.pendidikan_terakhir)
        $(`select[id="jabatan_pengurus_${n}"]`).val(data.jabatan)

        let index_pengurus      = n
        let provinsi_pengurus   = data.provinsi
        let kota_pengurus       = data.kota
        let kecamatan_pengurus  = data.kecamatan
        let kelurahan_pengurus  = data.kelurahan
        $(`select[id="provinsi_pengurus_${n}"]`).val(provinsi_pengurus)
        $(`#kota_pengurus_${n}`).empty();

        if (provinsi_pengurus) {
            $.ajax({
                url: "/getKota/" + provinsi_pengurus,
                method: "get",
                async: false,
                success: function(data_kota) {
                    $.each(data_kota.kota,function(index,value){
                        $('#kota_pengurus_'+index_pengurus).append(
                            '<option value="'+value.kode_kota+'">'+value.nama_kota+'</option>'
                        );
                    })
                    
                    $(`#kota_pengurus_${index_pengurus} option[value='${kota_pengurus}']`).prop('selected', true);
                }
            });
        }

        if (kota_pengurus) {
            $.ajax({
                url: "/getKecamatan/" + kota_pengurus,
                method: "get",
                async: false,
                success: function(response) {
                    let data_kecamatan = response ? JSON.parse(response) : []
                    for(let i = 0; i<data_kecamatan.length; i++){ 
                        $('#kecamatan_pengurus_'+index_pengurus).append($('<option>', {
                            value: data_kecamatan[i].text,
                            text : data_kecamatan[i].text
                        }));
                    
                        $(`#kecamatan_pengurus_${index_pengurus} option[value='${kecamatan_pengurus}']`).prop('selected', true);
                    }
                }
            });
        }

        if (kecamatan_pengurus) {
            $.ajax({
                url: "/borrower/data_kelurahan/" + kecamatan_pengurus,
                method: "get",
                async: false,
                success: function(response) {
                    let data_kelurahan = response ? JSON.parse(response) : []
                    for(let i = 0; i<data_kelurahan.length; i++){
                        $('#kelurahan_pengurus_'+index_pengurus).append($('<option>', { 
                            value: data_kelurahan[i].text,
                            text : data_kelurahan[i].text
                        }));

                        $(`#kelurahan_pengurus_${index_pengurus} option[value='${kelurahan_pengurus}']`).prop('selected', true);
                    }
                }
            });
        }

        if (n == '0') {
            let foto_diri = (`
            <div class="col-md-6 mt-3">
                <div class="form-row mx-auto">
                    <div class="col-md-12 text-center">
                        <label class="font-weight-normal mr-3">Foto Diri</label>
                        <div class="mx-auto" style="width: 200px;">
                            <img class="rounded float-left img-fluid"
                                src="{{ url('get-user-file-for-admin') }}/${(data.foto_diri) ? (data.foto_diri).replaceAll('/', ':') : ''}">
                        </div>
                    </div>
                </div>
            </div>
            `);

            $(`#add-foto-pengurus-${n}`).append(foto_diri);
        }

        let foto_ktp = (`
        <div class="col-md-6 mt-3">
            <div class="form-row mx-auto">
                <div class="col-md-12 text-center">
                    <label class="font-weight-normal mr-3">Foto KTP</label>
                    <div class="mx-auto" style="width: 200px;">
                        <img class="rounded float-left img-fluid"
                            src="{{ url('get-user-file-for-admin') }}/${(data.foto_ktp) ? (data.foto_ktp).replaceAll('/', ':') : ''}">
                    </div>
                </div>
            </div>
        </div>
        `);

        $(`#add-foto-pengurus-${n}`).append(foto_ktp);

        if (n == '0') {
            let foto_diri_dan_ktp = (`
            <div class="col-md-6 mt-3">
                <div class="form-row mx-auto">
                    <div class="col-md-12 text-center">
                        <label class="font-weight-normal mr-3">Foto Diri & KTP</label>
                        <div class="mx-auto" style="width: 200px;">
                            <img class="rounded float-left img-fluid" id="foto_diri_${n}"
                                src="{{ url('get-user-file-for-admin') }}/${(data.foto_diri_ktp) ? (data.foto_diri_ktp).replaceAll('/', ':') : ''}">
                        </div>
                    </div>
                </div>
            </div>
            `);

            $(`#add-foto-pengurus-${n}`).append(foto_diri_dan_ktp);
        }

        let foto_npwp = (`
        <div class="col-md-6 mt-3">
            <div class="form-row mx-auto">
                <div class="col-md-12 text-center">
                    <label class="font-weight-normal mr-3">Foto NPWP</label>
                    <div class="mx-auto" style="width: 200px;">
                        <img class="rounded float-left img-fluid" id="foto_diri_${n}"
                            src="{{ url('get-user-file-for-admin') }}/${(data.foto_npwp) ? (data.foto_npwp).replaceAll('/', ':') : ''}">
                    </div>
                </div>
            </div>
        </div>
        `);

        $(`#add-foto-pengurus-${n}`).append(foto_npwp);
    }
</script>
{{-- END: Other Function --}}
@endsection