@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('style')
<link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }} " rel="stylesheet" />
<link href="{{ url('assetsBorrower/js/plugins/select2/css/select2-bootstrap.min.css') }} " rel="stylesheet" />
<style>
    .ff-style {
        font-family: 'FontAwesome';
    }
</style>
@endsection

@section('content')
{{-- START: Content Title --}}
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Inquiry VA</h1>
            </div>
        </div>
    </div>
</div>
{{-- END: Content Title --}}

{{-- START: Content --}}
<div class="content mt-3">
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="alert alert-danger col-sm-12 d-none" id="error_search">
                    Data tidak ditemukan
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card rounded">
                    <h6 class="pt-0 ml-4 pl-1 font-weight-bold"
                        style="margin-top: -0.7rem; width: 160px; background-color: #F1F2F7">
                        Data Pendana
                    </h6>
                    <div class="card-body pt-4 pl-4">
                        <form id="form-get-va-number" name="form-get-va-number"
                            action="{{ route('admin.investor.get-va-number') }}" method="post">
                            <div class="form-group row">

                                <div class="col-lg-10 my-2 col-md-12">
                                    <div class="row">
                                        <div class="col-md-4 pr-md-0 pt-md-0 pt-2">
                                            <select name="in_kode_operator" id="in_kode_operator"
                                                class="form-control custom-select ff-style"
                                                onchange="$('#in_no_hp').val('');$('#card-va-number').addClass('d-none')"
                                                required>
                                                @foreach ($master_kode_operator as $item)
                                                <option value="{{$item->kode_operator}}"
                                                    {{ $item->kode_operator == '62' ? 'selected' : ''}}>
                                                    ({{$item->kode_operator}}) {{$item->negara}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-8 pt-md-0 pt-2">
                                            <input type="text" class="form-control ff-style" id="in_no_hp"
                                                maxlength="15" name="in_no_hp"
                                                placeholder="&#xF002;  Masukkan no handphone lender, contoh: 831xxxxxx"
                                                onkeyup="this.value = this.value.replace(/[^\d/]/g,'')" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 my-2 col-md-12 pt-md-0 pt-2">
                                    <button type="submit" id="btn-submit"
                                        class="btn btn-primary btn-block rounded">Cari</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

                <div id="card-va-number" class="card d-none">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="layout-va-number" class="card-group ff-style">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- END: Content --}}
@endsection

@section('js')
<script src="{{url('assetsBorrower/js/plugins/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $(document).ready(function () {
        $('#form-get-va-number').submit(function (e) {
            e.preventDefault()

            $.ajax({
                url : $(this).attr('action'),
                type: $(this).attr("method"),
                dataType: 'JSON',
                data: $(this).serialize(),
                beforeSend: function() {
                    $('#btn-submit').text('').prepend('<i class="fa fa-spin fa-spinner"></i>').attr('disabled', true);
                },
                success: function(response) {
                    $('#btn-submit').text('Cari').attr('disabled', false);

                    let va_1 = response.data[0]
                    let va_2 = response.data[1]

                    if (response.status == 'success') {

                        if (va_1.length > 0 || va_2.length > 0) {
                            $('#layout-va-number').html('')
                            $('#card-va-number').removeClass('d-none')

                            if (va_1) {
                                addVaNumber(va_1)
                            }
                            
                            if (va_2) {
                                addVaNumber(va_2)
                            }
                        } else {
                             Swal.fire({
                                title: 'Info',
                                text: 'Tidak ada nomor VA yang terdaftar',
                                type: 'warning',
                                allowOutsideClick: false
                            })

                            $('#card-va-number').addClass('d-none')
                        }
                        
                    } else {
                        Swal.fire({
                            title: 'Info',
                            text: response.msg,
                            type: 'warning',
                            allowOutsideClick: false
                        })

                        $('#card-va-number').addClass('d-none')
                    }
                },
                error: function() {
                    $('#btn-submit').text('Cari').attr('disabled', false);
                    $('#card-va-number').addClass('d-none')
                    Swal.fire({
                        title: 'Error!',
                        text: 'Internal Error',
                        type: 'error',
                        allowOutsideClick: false
                    })
                }
            })
        })

        $('#in_kode_operator').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%',
        });

        // Validasi No HP
        $('#in_no_hp').on("input", function(){
            let in_kode_operator = $('#in_kode_operator').val()
            let thisValue = $(this).val()

            if (in_kode_operator == '62' ) {
                console.log('asd')
                if (thisValue.charAt(0) == '6' && thisValue.charAt(1) == '2') {
                    $(this).val(thisValue.substring(2))
                } else {
                    if (thisValue.charAt(0) != '8') {
                        $(this).val(thisValue.substring(1))
                    }
                }
            } 
        })
    })

    const addVaNumber = (data) =>{
        data.forEach(element => {
            console.log(element.nama_bank)
            $('#layout-va-number').append(`
            <div class="col-lg-6 col-md-4 col-sm-12">
            
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">${element.nama_bank ? element.nama_bank : '-'}</h5>
                        <hr>
                        <p class="card-text h5">${element.va_number ? element.kode_bank == '009' ? element.va_number : (element.va_number).substring(4) : '-'}</p>
                        <p class="card-text"><small class="text-muted">Kode Bank : ${element.kode_bank ? element.kode_bank : '-'}</small>
                        </p>
                    </div>
                </div>
            </div>
            `)
        });
    }
</script>
@endsection