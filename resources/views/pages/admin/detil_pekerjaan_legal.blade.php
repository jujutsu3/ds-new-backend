@extends('layouts.admin.master')

@section('title', 'Legalitas')

@push('add-more-style')
    <link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <style>
        input[type=checkbox] {
            top: 1rem;
            width: 1rem;
            height: 1rem;
        }

        .btn-danaSyariah {
            background-color: #16793E;
        }

        .box-title {
            margin-top: -33px;
            background-color: #FFFFFF;
            width: -moz-fit-content;
            width: fit-content;
        }

        body {
            background-color: #FFFFFF;
        }

        .card {
            border-color: #000000;
            margin-bottom: 60px
        }

        .select2-selection {
            height: 35px !important;
        }


        label.error {
            color: red !important;
            font-size: 12px !important;
        }

        .mandatory_label {
            color: red;
        }

        .swal-wide {
            width: 850px !important;
        }


        .form-control,
        .input-group-text,
        .table {
            font-size: 13px !important;
        }

        .table-wrapper {
            max-height: 250px;
            overflow: auto;
            display:inline-block;
        }

    </style>
@endpush

@section('content')

    <div class="breadcrumbs mb-4">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Legalitas - {{ strtoupper(str_replace('_', ' ', request()->get('keterangan'))) }}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card px-3">
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="box-title">
                                    <label class="form-check-label text-black h6 text-bold pl-2">Informasi Legalitas
                                        &nbsp</label>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-5 col-12">Nama Legal <span class="float-right ml-1">:</span></div>
                            <p id="nama_verifikator" class="col-md-6 pl-md-0 text-dark col-12">
                                {{ $data->nama_legal ? $data->nama_legal : Auth::user()->firstname . ' ' . Auth::user()->lastname }}
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-12">Tanggal Legalitas <span class="float-right ml-1">:</span></div>
                            <p id="tanggal_verifikasi" class="col-md-6 pl-md-0 text-dark col-12">
                                {{ !empty($tanggal_legalitas) ? $tanggal_legalitas : '' }}
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-12">ID Penerima Pendanaan | ID Pengajuan Pendanaan <span
                                    class="float-right ml-1">:</span></div>
                            <p id="id_penerima_pengajuan" class="col-md-6 pl-md-0 text-dark col-12">
                                {{ !empty($data) ? $data->brw_id . ' | ' . $data->pengajuan_id : '' }}
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-12">Nama Penerima Dana <span class="float-right ml-1">:</span></div>
                            <p id="nama_penerima" class="col-md-6 pl-md-0 text-dark col-12">
                                {{ !empty($data) ? $data->nama : '' }}
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-12">Plafon Pengajuan Pendanaan <span class="float-right ml-1">:</span>
                            </div>
                            <p id="plafon_pengajuan" class="col-md-6 pl-md-0 text-dark col-12">
                                {{ !empty($data) ? 'Rp' . str_replace(',', '.', number_format($data->pendanaan_dana_dibutuhkan)) : '' }}
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-12">Fasilitas Pendanaan <span class="float-right ml-1">:</span></div>
                            <p id="tipe_pendanaan" class="col-md-6 pl-md-0 text-dark col-12">
                                {{ !empty($data) ? $data->tipe_pendanaan : '' }}
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-12">Jangka Waktu Pendanaan (Bulan) <span
                                    class="float-right ml-1">:</span></div>
                            <p id="tenor_pendanaan" class="col-md-6 pl-md-0 text-dark col-12">
                                {{ !empty($data) ? $data->durasi_proyek : '' }}
                            </p>
                        </div>

                        <div class="row">
                            <div class="col-md-5 col-12">Kategori Penerima Pendanaan <span class="float-right ml-1">:</span>
                            </div>
                            <p id="sumber_pengembalian_dana" class="col-md-6 pl-md-0 text-dark col-12">
                                @if ($data->sumber_pengembalian_dana == '1')
                                   Fixed Income (Penghasilan Tetap)
                                @else
                                   Non Fixed Income (Penghasilan Tidak Tetap)
                                @endif
                            </p>
                        </div>
                    </div>
                </div>

                @if (request()->get('keterangan') == 'form_naup')
                    <form method="POST" id="form_naup" name="form_naup"
                        action="{{ route('detail-pekerjaan-legal-update') }}">
                        @csrf
                        <input type="hidden" name="pengajuan_id" value="{{ request()->pengajuan_id }}" />
                        <input type="hidden" name="keterangan" value="{{ request()->keterangan }}" />
                        @include('pages.admin.legal.form_naup')
                    </form>

                @endif

                @if (request()->get('keterangan') == 'form_biaya')

                    <form method="POST" id="form_biaya" name="form_biaya"
                        action="{{ route('detail-pekerjaan-legal-update') }}">
                        @csrf
                        <input type="hidden" name="pengajuan_id" value="{{ request()->pengajuan_id }}" />
                        <input type="hidden" name="keterangan" value="{{ request()->keterangan }}" />
                        @include('pages.admin.legal.form_biaya')
                    </form>

                @endif

                <div class="d-flex flex-row-reverse">
                    <button type="button" id="btn_simpan" class="btn btn-success mb-2 ml-2"
                        {{ !empty($data->nama_legal) ? (Auth::user()->firstname . ' ' . Auth::user()->lastname != $data->nama_legal ? 'disabled' : '') : '' }}><i
                            class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                    <a href="{{ route('legal.daftarpekerjaan', request()->pengajuan_id) }}"><button type="submit"
                            class="btn btn-secondary mb-2 ml-2" id="kembali"><i class="fa fa-arrow-left"
                                aria-hidden="true"></i> Kembali</button></a>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @stack('naup-scripts')
    @stack('biaya-scripts')
    <script src="{{ url('assetsBorrower/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script type="text/javascript">
        const formatRupiah = (angka, prefix) => {
            let thisValue = angka.replace(/[^,\d]/g, '')
            angka = (parseInt(thisValue, 10)).toString()

            let number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
        }

        $(document).ready(function() {
            let keterangan = "{!! request()->get('keterangan') !!}";
            let status_legal = "{!! $status_legal !!}";

            $("#form_naup").validate({
                onfocusout: false,     
                invalidHandler: function(e, validator) {
                    if (validator.errorList.length) {
                        validator.errorList[0].element.focus()
                    }

                },
            });

            $('#btn_simpan').click(() => {
                if (keterangan == 'form_naup') {
                    if ($("#form_naup").valid()) {
                        var boxes = $('.pilih_struktur_pendanaan:checkbox');

                        if(boxes.length > 0) {
                            if( $('.pilih_struktur_pendanaan:checkbox:checked').length < 1) {
                                swal.fire({
                                    title: 'Struktur Pendanaan',
                                    type: 'error',
                                    text: 'Pilih salah satu atau lebih struktur pendanaan'
                                })
                                boxes[0].focus();
                                return false;
                            }else{
                                $('#form_naup').submit()
                            }
                        }
                      
                    }
                } else if (keterangan == 'form_biaya') {
                    $('#form_biaya').submit()
                }
            })


            if (status_legal == '9') {
                $('#btn_simpan').addClass('d-none');
            }

        });


        $('#form_naup').submit(function(e) {
            e.preventDefault()
            submitData('#form_naup')
        })


        $('#form_biaya').submit(function(e) {
            e.preventDefault()
            submitData('#form_biaya')
        })

        const submitData = (element_id) => {
            let form = $(element_id);
            let formData = new FormData($(element_id)[0]);
            $.ajax({
                url: $(element_id).attr('action'),
                type: $(element_id).attr("method"),
                dataType: 'JSON',
                data: formData,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    swal.fire({
                        html: '<h5>Menyimpan Data...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(response) {

                    if (response.status == 'success') {
                        swal.fire({
                            title: 'Berhasil',
                            type: 'success',
                            text: response.msg,
                            allowOutsideClick: () => false,
                        }).then(function(result) {
                            window.location =
                                "{{ route('legal.daftarpekerjaan', ['pengajuan_id' => request()->pengajuan_id]) }}"
                        })
                    } else {
                        swal.fire({
                            title: 'Oops',
                            type: 'error',
                            text: "Internal Error"
                        })
                    }
                },
                error: function(response) {
                    swal.fire({
                        title: 'Error',
                        type: 'error',
                        text: response.msg
                    })
                    console.log(response)
                }
            })
        }
    </script>
@endsection
