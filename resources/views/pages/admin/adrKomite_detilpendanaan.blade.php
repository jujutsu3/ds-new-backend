@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('content')

<style>
    .largerCheckbox {
        width: 15px;
        height: 15px;
    }

    .modal-xl {
        max-width: 95% !important;
    }
    td {
        padding: 10px 10px 10px 10px;
    }
</style>

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Persetujuan Komite Pendanaan</h1>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-11">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <!-- START: informasi compliance -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_informasi_legalitas">Informasi Resume &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_komite">Nama Komite </label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_komite" name="nama_komite" value="{{$data['namakomite']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_keputusan">Tanggal Keputusan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tanggal_keputusan" name="tanggal_keputusan" value="{{$data['tanggalkeputusan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-ipdipp">ID Penerima Dana |<br> ID Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="ipdipp" name="ipdipp" value="{{$data['brwId'].' | '.$pengajuan_id = $data['pengajuanId']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_penerima_dana">Nama Penerima Dana</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_penerima_dana" name="nama_penerima_dana" value="{{$data['penerima_pendanaan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-plafon_pengajuan_pendanaan">Plafon Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="plafon_pengajuan_pendanaan" name="plafon_pengajuan_pendanaan" value="{{$data['plafon']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-fasilitas_pendanaan">Fasilitas Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="fasilitas_pendanaan" name="fasilitas_pendanaan" value="{{$data['jenisPendanaan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_pendanaan">Jangka Waktu Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_pendanaan" name="jangka_waktu_pendanaan" value="{{$data['durasi_proyek']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kategori_penerima_pendanaan">Kategori Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="kategori_penerima_pendanaan" name="kategori_penerima_pendanaan" value="{{$data['kategoriPenerimaPendanaan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end informasi compliance -->
                    <!-- data penerima Pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_resume_fasilitas_pendanaan">Resume Fasilitas Pendanaan &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nomor_naup">Nomor NAUP</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nomor_naup" name="nomor_naup" value="{{$data['no_naup']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_resume_fasilitas_pendanaan">I. DATA PENERIMA PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-skema_pengajuan">Skema Pengajuan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="skema_pengajuan" name="skema_pengajuan" value="{{$data['skema_pembiayaan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_penerima_pendanaan">Nama Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_penerima_pendanaan" name="nama_penerima_pendanaan" value="{{$data['penerima_pendanaan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-maks_ftv_persen">Maks FTV (%)</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="maks_ftv_persen" name="maks_ftv_persen" value="{{$data['maks_ftv_persentase']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-maks_ftv_rupiah">Maks FTV (Rp)</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="maks_ftv_rupiah" name="maks_ftv_rupiah" value="{{$data['maks_ftv_nominal']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end data penerima pendanaan -->
                    <!-- fasilitas pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_fasilitas_pendanaan">II. FASILITAS PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <!-- start wakalah -->
                    <div class="wakalah {{($data['wakalah'] == 0)?'d-none':''}}">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_wakalah"><u>Wakalah</u></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_produk">Nama Produk</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nama_produk" name="nama_produk" value="{{$data['jenisPendanaan']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu">Jangka Waktu</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu" name="jangka_waktu" value="{{$data['jangka_waktu_wakalah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tujuan_penggunaan">Tujuan Penggunaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tujuan_penggunaan" name="tujuan_penggunaan" value="{{$data['tujuan_penggunaan_wakalah']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-sumber_pelunasan">Sumber Pelunasan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="sumber_pelunasan" name="sumber_pelunasan" value="Pencairan Fasilitas Pendanaan {{$data['pendanaan_tujuan'] }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pembelian_dari">Pembelian dari</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pembelian_dari" name="pembelian_dari" value="{{$data['pembelian_dari']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_pendanaan">Pengikatan Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pengikatan_pendanaan" name="pengikatan_pendanaan" value="{{$data['pengikatanPendanaanWakalah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jumlah_wakalah">Jumlah Wakalah</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jumlah_wakalah" name="jumlah_wakalah" value="{{$data['jumlah_wakalah']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_jaminan">Pengikatan Jaminan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pengikatan_jaminan" name="pengikatan_jaminan" value="{{$data['pengikatanJaminanWakalah']}}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="qardh {{($data['qardh'] == 0)?'d-none':''}}">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_qardh"><u>Qardh</u></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_produk_qardh">Nama Produk</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nama_produk_qardh" name="nama_produk_qardh" value="{{$data['jenisPendanaan']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_qardh">Jangka Waktu</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu_qardh" name="jangka_waktu_qardh" value="{{$data['jangka_waktu_qardh']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tujuan_penggunaan_qardh">Tujuan Penggunaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tujuan_penggunaan_qardh" name="tujuan_penggunaan_qardh" value="{{$data['tujuan_penggunaan_qardh']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-sumber_pelunasan_qardh">Sumber Pelunasan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="sumber_pelunasan_qardh" name="sumber_pelunasan_qardh" value="Pencairan Fasilitas Pendanaan {{$data['pendanaan_tujuan'] }}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pembelian_dari_qardh"><i>Take Over</i> dari</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="take_over_qardh" name="take_over_qardh" value="{{$data['take_over_dari_qardh']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_pendanaan_qardh">Pengikatan Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pengikatan_pendanaan_qardh" name="pengikatan_pendanaan_qardh" value="{{$data['pengikatanPendanaanQardh']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jumlah_wakalah_qardh">Jumlah Qardh</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jumlah_wakalah_qardh" name="jumlah_wakalah_qardh" value="{{$data['jumlah_qardh']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_jaminan_qardh">Pengikatan Jaminan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pengikatan_jaminan_qardh" name="pengikatan_jaminan_qardh" value="{{$data['pengikatanJaminanQardh']}}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="murabahah {{($data['murabahah'] == 0)?'d-none':''}}">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_murabahah"><u>Murabahah</u></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-skema_pendanaan_murabahah">Skema Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="skema_pendanaan_murabahah" name="skema_pendanaan_murabahah" value="Murabahah" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-totalKPP_murabahah">Total Kewajiban Penerima Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="totalKPP_murabahah" name="totalKPP_murabahah" value="{{$data['totalKPP_murabahah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_produk_murabahah">Nama Produk</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nama_produk_murabahah" name="nama_produk_murabahah" value="{{$data['jenisPendanaan']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pricing_pendanaan_murabahah">Pricing Pendanaan (Margin Rate)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group-append">
                                        <input class="form-control col-8" type="text" id="pricing_pendanaan_murabahah" name="pricing_pendanaan_murabahah" value="{{$data['pricing']}}" readonly>
                                        <span class="input-group-text input-group-text-dsi col-4">
                                            % effective p.a</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tujuan_penggunaan_murabahah">Tujuan Penggunaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tujuan_penggunaan_murabahah" name="tujuan_penggunaan_murabahah" value="{{$data['tujuan_penggunaan_murabahah']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-angsuran_pendanaan_murabahah">Angsuran Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="angsuran_pendanaan_murabahah" name="angsuran_pendanaan_murabahah" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-harga_beli_barang_murabahah">Harga Beli Barang</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="harga_beli_barang_murabahah" name="harga_beli_barang_murabahah" value="{{$data['harga_beli_barang_murabahah']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_murabahah">Jangka Waktu</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu_murabahah" name="jangka_waktu_murabahah" value="{{$data['jangka_waktu_murabahah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-uang_muka_penerima_pendanaan_murabahah">Uang Muka Penerima Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="uang_muka_penerima_pendanaan_murabahah" name="uang_muka_penerima_pendanaan_murabahah" value="{{$data['uang_muka_murabahah']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-denda_murabahah">Denda</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="denda_murabahah" name="denda_murabahah" value="{{$data['denda_murabahah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-harga_jual_barang_murabahah">Harga Jual Barang</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="harga_jual_barang_murabahah" name="harga_jual_barang_murabahah" value="{{$data['harga_jual_barang_murabahah']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_pendanaan_murabahah">Pengikatan Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pengikatan_pendanaan_murabahah" name="pengikatan_pendanaan_murabahah" value="{{$data['pengikatanPendanaanMurabahah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-plafon_pendanaan_murabahah">Porsi/Plafon Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="plafon_pendanaan_murabahah" name="plafon_pendanaan_murabahah" value="{{$data['plafond_rekomendasi']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_jaminan_murabahah">Pengikatan Jaminan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pengikatan_jaminan_murabahah" name="pengikatan_jaminan_murabahah" value="{{$data['pengikatanJaminanMurabahah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-margin_murabahah">Margin</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="margin_murabahah" name="margin_murabahah" value="{{$data['margin']}}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="imbt {{($data['imbt'] == 0)?'d-none':''}}">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_imbt"><u>Al Ijarah Al Muntahiyah bit Al-Tamlik (imbt)</u></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-skema_pendanaan_imbt">Skema Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="skema_pendanaan_imbt" name="skema_pendanaan_imbt" value="Al Ijarah Al Muntahiyah Bi Al Tamlik" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nilai_sewa_imbt">Nilai Sewa/Ujroh Perbulan (Rp)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nilai_sewa_imbt" name="nilai_sewa_imbt" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_produk_imbt">Nama Produk</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nama_produk_imbt" name="nama_produk_imbt" value="{{$data['jenisPendanaan']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-security_deposit_imbt">Security Deposit Penerima Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="security_deposit_imbt" name="security_deposit_imbt" value="{{$data['security_deposit_imbt']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tujuan_penggunaan_imbt">Tujuan Penggunaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tujuan_penggunaan_imbt" name="tujuan_penggunaan_imbt" value="{{$data['tujuan_penggunaan_imbt']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-porsi_pendanaan_imbt">Porsi Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="porsi_pendanaan_imbt" name="porsi_pendanaan_imbt" value="{{$data['plafond_rekomendasi']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_imbt">Jangka Waktu Sewa</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu_imbt" name="jangka_waktu_imbt" value="{{$data['durasi_proyek']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_pendanaan_imbt">Pengikatan Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" id="pengikatan_pendanaan_imbt" name="pengikatan_pendanaan_imbt" value="{{$data['pengikatanPendanaanimbt']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-harga_perolehan_aktiva_imbt">Harga Perolehan Aktiva</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="harga_perolehan_aktiva_imbt" name="harga_perolehan_aktiva_imbt" value="{{$data['plafond_rekomendasi']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-pengikatan_jaminan_imbt">Pengikatan Jaminan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" id="pengikatan_jaminan_imbt" name="pengikatan_jaminan_imbt" value="{{$data['pengikatanJaminanimbt']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-ekpektasi_nilai_sewa_imbt">Ekspektasi Nilai Sewa/Ujroh (%)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group-append">
                                        <input class="form-control" type="text" id="ekpektasi_nilai_sewa_imbt" name="ekpektasi_nilai_sewa_imbt" value="{{$data['pricing']}}" readonly>
                                        <span class="input-group-text input-group-text-dsi col-4">
                                            % P.A</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-angsuran_pendanaan_imbt">Angsuran Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="angsuran_pendanaan_imbt" name="angsuran_pendanaan_imbt" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nilai_ujroh_imbt">Nilai Ujroh</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nilai_ujroh_imbt" name="nilai_ujroh_imbt" value="{{$data['nilai_ujroh']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-denda_imbt">Denda</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="denda_imbt" name="denda_imbt" value="{{$data['denda_imbt']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-penetapan_nilai_ujroh_imbt">Penetapan Nilai Ujroh Selanjutanya dilakukan setiap (Bulan)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" id="penetapan_nilai_ujroh_imbt" name="penetapan_nilai_ujroh_imbt" value="{{$data['penetapan_nilai_ujroh_selanjutnya']}}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mmq {{($data['mmq'] == 0)?'d-none':''}}">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_imbt"><u>Musyarakah Mutanaqishah (MMQ)</u></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-skema_pendanaan_mmq">Skema Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="skema_pendanaan_mmq" name="skema_pendanaan_mmq" value="MMQ" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_pendanaan_mmq">Jangka Waktu Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu_pendanaan_mmq" name="jangka_waktu_pendanaan_mmq" value="{{$data['jangka_waktu_pendanaan_mmq']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nama_produk_mmq">Nama Produk</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nama_produk_mmq" name="nama_produk_mmq" value="{{$data['jenisPendanaan']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_penyerahan_objek_mmq">Jangka Waktu Penyerahan Objek MMQ</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu_penyerahan_objek_mmq" name="jangka_waktu_penyerahan_objek_mmq" value="{{$data['jangka_waktu_penyerahan_objek_mmq']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-tujuan_penggunaan_mmq">Tujuan Penggunaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="tujuan_penggunaan_mmq" name="tujuan_penggunaan_mmq" value="{{$data['tujuan_penggunaan_mmq']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-objek_bagi_hasil_mmq">Objek Bagi Hasil</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="objek_bagi_hasil_mmq" name="objek_bagi_hasil_mmq" value="{{$data['objek_bagi_hasil_mmq']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-harga_beli_asset_mmq">Harga Beli Asset</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="harga_beli_asset_mmq" name="harga_beli_asset_mmq" value="{{$data['harga_beli_asset_mmq']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-ekspektasi_nilai_sewa_mmq">Ekspektasi Nilai Sewa/Ujroh</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group-append">
                                        <input class="form-control" type="text" id="ekspektasi_nilai_sewa_mmq" name="ekspektasi_nilai_sewa_mmq" value="{{$data['ekspektasi_nilai_sewa_mmq']}}" readonly>
                                        <span class="input-group-text input-group-text-dsi col-4">
                                            % effective p.a</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-plafon_pendanaan_mmq">Plafon Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="plafon_pendanaan_mmq" name="plafon_pendanaan_mmq" value="{{$data['plafond_rekomendasi']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-proyeksi_total_pendanaan_mmq">Proyeksi Total Pendapatan Sewa/Ujroh</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="proyeksi_total_pendanaan_mmq" name="proyeksi_total_pendanaan_mmq" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-uang_muka_penerima_dana_mmq">Uang Muka/Modal Penerima Dana</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="uang_muka_penerima_dana_mmq" name="uang_muka_penerima_dana_mmq" value="{{$data['uang_muka_mmq']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nisbah_mmq"><b>Nisbah :</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-modal_syirkah_mmq">Modal Syirkah</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="modal_syirkah_mmq" name="modal_syirkah_mmq" value="{{$data['modal_syirkah']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nisbah_penyelenggara_mmq">1. Nisbah Penyelenggara</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nisbah_penyelenggara_mmq" name="nisbah_penyelenggara_mmq" value="{{$data['nisbah_penyelenggara_mmq']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-hishshah_perunit_mmq">Hishshah Per Unit</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="hishshah_perunit_mmq" name="hishshah_perunit_mmq" value="{{$data['hishshah_per_unit']}}" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nisbah_penerima_mmq">2. Nisbah Penerima Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nisbah_penerima_mmq" name="nisbah_penerima_mmq" value="{{$data['nisbah_penerima_mmq']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-hishshah_mmq"><b>Hishshah :</b></label>
                                </div>
                            </div>
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-denda_perhari_mmq">Denda (Per Hari Keterlambatan)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="denda_perhari_mmq" name="denda_perhari_mmq" value="{{$data['denda_mmq']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-hishshah_penyelenggara_mmq">1. Hishshah Penyelenggara</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="hishshah_penyelenggara_mmq" name="hishshah_penyelenggara_mmq" value="{{$data['hishshah_penyelenggara']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-hishshah_penerima_pendanaan_mmq">2. Hishshah Penerima Pendanaan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="hishshah_penerima_pendanaan_mmq" name="hishshah_penerima_pendanaan_mmq" value="{{$data['hishshah_penerima_pendanaan']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-total_hishshah_mmq">Total Hishshah</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="total_hishshah_mmq" name="total_hishshah_mmq" value="{{$data['total_hishshah']}}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ijarah {{($data['ijarah'] == 0)?'d-none':''}}">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_ijarah"><u>Ijarah</u></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-objek_sewa_ijarah">Objek Sewa</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="objek_sewa_ijarah" name="objek_sewa_ijarah" value="{{$data['objek_sewa']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_sewa_ijarah">Jangka Waktu Sewa (bulan)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu_sewa_ijarah" name="jangka_waktu_sewa_ijarah" value="{{$data['jangka_waktu_sewa']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-jangka_waktu_penyerahan_sewa_ijarah">Jangka Waktu Penyerahan Objek Sewa (bulan)</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="jangka_waktu_penyerahan_sewa_ijarah" name="jangka_waktu_penyerahan_sewa_ijarah" value="{{$data['jangka_waktu_penyerahan_ijarah']}}" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-nilai_sewa_ijarah">Nilai Sewa/Ujroh</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="nilai_sewa_ijarah" name="nilai_sewa_ijarah" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-penyesuaian_nilai_sewa_ijarah">Penyesuaian Nilai Sewa/Ujroh</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <textarea class="form-control" id="penyesuaian_nilai_sewa_ijarah" name="penyesuaian_nilai_sewa_ijarah" rows="3" readonly>{{$data['penyesuaian_nilai_sewa']}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end wakalah -->
                    <!-- end fasilitas pendanaan -->
                    <!-- objek pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_objek_pendanaan">III. OBJEK PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jenis_objek_pendanaan">Jenis Objek Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jenis_objek_pendanaan" name="jenis_objek_pendanaan" value="{{$data['jenis_objek_pendanaan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-status_jenis_sertifikat">Status/Jenis Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="status_jenis_sertifikat" name="status_jenis_sertifikat" value="{{$data['status_jenis_sertifikat']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-no_sertifikat">No Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="no_sertifikat" name="no_sertifikat" value="{{$data['nomor_agunan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_berakhir_hak">Tanggal Berakhir Hak</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tanggal_berakhir_hak" name="tanggal_berakhir_hak" value="{{$data['tanggal_jatuh_tempo']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-sertifikat_atas_nama">Sertifikat Atas Nama</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="sertifikat_atas_nama" name="sertifikat_atas_nama" value="{{$data['atas_nama_agunan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-alamat_sertifikat">Alamat Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <textarea class="form-control" type="text" id="alamat_sertifikat" name="alamat_sertifikat" rows="5" readonly>{{$data['alamat_sertifikat']}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-no_izin_mendirikan_bangunan">No Izin Mendirikan Bangunan (IMB)</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="no_izin_mendirikan_bangunan" name="no_izin_mendirikan_bangunan" value="{{$data['no_izin_mendirikan_bangunan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-imb_atas_nama">IMB Atas Nama</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="imb_atas_nama" name="imb_atas_nama" value="{{$data['imbt_atas_nama']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-luas_tanah">Luas Tanah m2</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="luas_tanah" name="luas_tanah" value="{{$data['luas_tanah']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-luas_bangunan">Luas Bangunan m2</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="luas_bangunan" name="luas_bangunan" value="{{$data['luas_bangunan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_penilaian_agunan">Tanggal Penilaian Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tanggal_penilaian_agunan" name="tanggal_penilaian_agunan" value="{{$data['tanggal_penilaian_agunan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-appraisal">Appraisal</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" id="appraisal" name="appraisal" type="text" value="{{$data['appraisal']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_pasar_wajar">Nilai Pasar Wajar</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_pasar_wajar" name="nilai_pasar_wajar" value="{{$data['nilai_pasar_wajar']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_likuidasi">Nilai Likuidasi</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" value="{{$data['nilai_likuidasi']}}" id="nilai_likuidasi" name="nilai_likuidasi" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-rekomendasi_appraisal">Rekomendasi Appraisal</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" id="rekomendasi_appraisal" name="rekomendasi_appraisal" value="{{$data['rekomendasi_appraisal']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jenis_pengikat_agunan">Jenis Pengikat Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" id="jenis_pengikat_agunan" name="jenis_pengikat_agunan" type="text" value="{{$data['jenis_pengikatan_agunan']}}" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_pengikat_agunan">Nilai Pengikat Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="input-group-append">
                                <input class="form-control col-12" type="text" id="nilai_pengikat_agunan" name="nilai_pengikat_agunan" value="{{$data['nilai_pengikatan_agunan']}}" readonly>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="input-group-append">
                                    % effective p.a
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4"></div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="plafon_pembiayaan" name="plafon_pembiayaan" value="{{$data['percent_pengikat_agunan']}}" readonly>
                        </div>
                        <div class="col-md-3">
                            % Plafon Pembiayaan
                        </div>
                    </div>
                    <!-- end objek pendanaan -->
                    <!-- biaya -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_biaya">IV. BIAYA - BIAYA &nbsp</label>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-md-12">
                            <table id="pengajuKPR_data" class="table table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th class="col-1">No</th>
                                        <th class="col-8">Rincian Biaya</th>
                                        <th class="col-3">Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php 
                                    $j=0
                                @endphp
                                @foreach ($biaya as $biaya)
                                    @php
                                        $j++;
                                    @endphp
                                    <tr>
                                        <td>{{$j}}</td>
                                        <td>{{$biaya->rincian_biaya}}</td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_administrasi" name="jumlah_biaya_administrasi" value="Rp.{{number_format($biaya->jumlah,2,'.',',')}}" readonly>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end biaya -->
                    <!-- start covenant persetujuan pembiayaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_biaya">V. COVENANT PERSETUJUAN PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                        <div class="col-sm-12">
                            <table id="pejabat_data" class="table-striped table-bordered table-responsive">
                                <thead>
                                    <tr align="center">
                                        <th>No</th>
                                        <th class="col-1">Jabatan</th>
                                        <th class="col-2">Nama Pejabat</th>
                                        <th class="col-1">Rekomendasi</th>
                                        <th class="col-8">Covenant/Catatan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($resume as $resume)
                                    <tr>
                                        <td>1</td>
                                        <td>ANALIS</td>
                                        <td>{{$resume->nama_kredit_analis}}</td>
                                        <td>{{$resume->rekomendasi_kredit_analis? 'Setuju' : 'tidak setuju'}}</td>
                                        <td style="word-break: break-word">{{$resume->catatan_kredit_analis}}</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>COMPLIANT</td>
                                        <td>{{$resume->nama_compliance}}</td>
                                        <td>{{$resume->rekomendasi_compliance? 'Setuju' : 'tidak setuju'}}</td>
                                        <td style="word-break: break-word">{{$resume->catatan_compliance}}</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>RISK MANAGEMENT</td>
                                        <td>{{$resume->nama_risk_management}}</td>
                                        <td>{{$resume->rekomendasi_risk_management? 'Setuju' : 'tidak setuju'}}</td>
                                        <td style="word-break: break-word">{{$resume->catatan_risk_management}}</td>
                                    </tr>
                                    <tr>    
                                        <td>4</td>
                                        <td>LEGAL</td>
                                        <td>{{$resume->nama_legal}}</td>
                                        <td>{{$resume->rekomendasi_legal? 'Setuju' : 'tidak setuju'}}</td>
                                        <td style="word-break: break-word">{{$resume->catatan_legal}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div></div>
                    </div>
                    <!-- end covenant persetujuan pembiayaan -->
                    <!-- start Deviasi Pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_biaya">VI. DEVIASI PENDANAAN &nbsp</label>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="col-md-12 mb-4">
                            <table id="pengajuKPR_datax" class="table table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th class="col-1">No</th>
                                        <th class="col-2">Jenis Deviasi</th>
                                        <th class="col-4">Resiko</th>
                                        <th class="col-4">Mitigasi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php 
                                        $k=0
                                    @endphp
                                    @foreach ($deviasi as $deviasi)
                                    @php
                                        $k++;
                                    @endphp
                                    <tr>
                                        <td>{{$k}}</td>
                                        <td>{{$deviasi->jenis_deviasi}}</td>
                                        <td>{{$deviasi->resiko}}</td>
                                        <td>{{$deviasi->mitigasi}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end Deviasi Pendanaan -->
                    <!-- start komite persetujuan pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_biaya">VII. KOMITE PERSETUJUAN PENDANAAN &nbsp</label>
                        </div>
                    </div> 
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="card analis">
                                <div class="card-header h6">DIREKTUR</div>
                                <div class="card-body">
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            Nama Direktur :
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" id="nama_user_direktur" name="nama_user_direktur" placeholder="Direktur" value="{{$data['nama_direktur']}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div class="col-md-6">
                                            Tanggal Keputusan :
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" id="tanggal_keputusan_direktur" name="tanggal_keputusan_direktur" value="{{$data['tanggal_keputusan_direktur']}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div class="col-md-6">
                                            Rekomendasi :
                                        </div>
                                        <div class="col-md-12"></div>
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <select class="form-control" name="rekomendasi_direktur" id="rekomendasi_direktur" {{$data['disabledD']}}>
                                                <option value="0" {{($data['rekomendasi_direktur'] == '0'? 'selected':'')}}>Tidak Setuju</option>
                                                <option value="1" {{($data['rekomendasi_direktur'] == '1'? 'selected':'')}}>Setuju</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div class="col-md-12">
                                            Metode Penggalangan Dana :
                                        </div>
                                        <div class="col-md-12">
                                            <select class="form-control" name="penggalangan_dana_direktur" id="penggalangan_dana_direktur" {{$data['disabledD']}}>
                                                <option value="0" {{($data['penggalangan_dana_direktur'] == '0'? 'selected':'')}}>Penggalangan Dana di Sistem Dana Syariah - Retail</option>
                                                <option value="1" {{($data['penggalangan_dana_direktur'] == '1'? 'selected':'')}}>Di Berikan Bank Rekanan</option>
                                                <option value="2" {{($data['penggalangan_dana_direktur'] == '2'? 'selected':'')}}>Penggalangan Dana di Sistem Dana Syariah - Internal Investor</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="wizard-progress2-catatan_direktur">Disposisi Putusan</label>
                                            <textarea class="form-control" name="catatan_direktur" id="catatan_direktur" cols="30" rows="5" {{$data['ROD']}}>{{$data['catatan_direktur']}}</textarea>
                                        </div>
                                    </div>
                                    @if ($cekUser->name == 'Direktur')
                                    <div class="col-md-12">
                                        <div class="form-group text-right">
                                            <button class="btn btn-warning btn-lg" id="btn_catatan" value="_direktur" type="submit">Simpan</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="card direktur_utama">
                                <div class="card-header h6">DIREKTUR UTAMA</div>
                                <div class="card-body">
                                    <div class="col-md-6">
                                        <div class="col-md-6">
                                            Nama Direktur Utama :
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="text" id="nama_user_direktur_utama" name="nama_user_direktur_utama" placeholder="Direktur Utama" value="{{$data['nama_direktur_utama']}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div class="col-md-6">
                                            Tanggal Keputusan :
                                        </div>
                                        <div class="col-md-6">
                                            <input class="form-control" type="date" id="tanggal_keputusan_direktur_utama" name="tanggal_keputusan_direktur_utama" value="{{$data['tanggal_keputusan_direktur_utama']}}" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div class="col-md-8">
                                            Rekomendasi :
                                        </div>
                                        <div class="col-md-12"></div>
                                        <div class="col-md-6"></div>
                                        <div class="col-md-6">
                                            <select class="form-control" name="rekomendasi_direktur_utama" id="rekomendasi_direktur_utama" {{$data['disabledDU']}}>
                                                <option value="0" {{($data['rekomendasi_direktur_utama'] == '0'? 'selected':'')}}>Tidak Setuju</option>
                                                <option value="1" {{($data['rekomendasi_direktur_utama'] == '1'? 'selected':'')}}>Setuju</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div class="col-md-12">
                                            Metode Penggalangan Dana :
                                        </div>
                                        <div class="col-md-12">
                                            <select class="form-control" name="penggalangan_dana_direktur_utama" id="penggalangan_dana_direktur_utama" {{$data['disabledDU']}}>
                                                <option value="0" {{($data['penggalangan_dana_direktur'] == '0'? 'selected':'')}}>Penggalangan Dana di Sistem Dana Syariah - Retail</option>
                                                <option value="1" {{($data['penggalangan_dana_direktur'] == '1'? 'selected':'')}}>Di Berikan Bank Rekanan</option>
                                                <option value="2" {{($data['penggalangan_dana_direktur'] == '2'? 'selected':'')}}>Penggalangan Dana di Sistem Dana Syariah - Internal Investor</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="wizard-progress2-catatan_direktur_utama">Disposisi Putusan</label>
                                            <textarea class="form-control" name="catatan_direktur_utama" id="catatan_direktur_utama" cols="30" rows="5" {{$data['RODU']}}>{{$data['catatan_direktur_utama']}}</textarea>
                                        </div>
                                    </div>
                                    @if ($cekUser->name == 'Direktur Utama')
                                    <div class="col-md-12">
                                        <div class="form-group text-right">
                                            <button class="btn btn-warning btn-lg" id="btn_catatan" value="_direktur_utama" type="submit">Simpan</button>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end komite persetujuan pendanaan -->

                    <!-- end covenant persetujuan pembiayaan -->
                    <!-- button kembali / simpan -->
                    @if ($cekUser->name == 'Direktur Utama' && $data['status_pendanaan'] != 16 && ($data['catatan_direktur'] && $data['catatan_direktur_utama']))
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('komite.listproyek')}}"><button type="submit" class="btn btn-block btn-success btn-lg" id="kembali">Kembali</button></a>
                    </div>
                    <div class="col-md-2">
                        <input type="hidden" value="{{auth::user()->firstname.' '.auth::user()->lastname}}" id="user">
                        <button type="button" class="btn btn-block btn-success btn-lg" id="Kirim" value="{{$pengajuan_id}}">Kirim</button>
                    </div>
                    @else
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('komite.listproyek')}}"><button type="submit" class="btn btn-block btn-success btn-lg" id="kembali">Kembali</button></a>
                    </div>
                    @endif
                    <!-- end button -->
                </div>
            </div>
        </div>
    </div>
</div>
</div><!-- .content -->

<!-- 2. GOOGLE JQUERY JS v3.2.1  JS !-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>


<script type="text/javascript">
     $.ajaxSetup({
         headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
     });
        $('#btn_catatan').on('click',function(){
            var role = $('#btn_catatan').val();
            var nama_user = $('#nama_user'+role).val();
            var tanggal_keputusan = $('#tanggal_keputusan'+role).val();
            var rekomendasi = $('#rekomendasi'+role).val();
            var penggalangan_dana = $('#penggalangan_dana'+role).val();
            var catatan = $('#catatan'+role).val()
            if (catatan == '') {
                Swal.fire({
                    title: "Peringatan",
                    text: "Disposisi Belum dilengkapi",
                    type: "warning",
                })
                return false
            };
            var pengajuan_id = {{$pengajuan_id}};

            $.ajax({
                url: "/admin/adr/updatekomitependanaan",
                method: "POST",
                dataType: 'JSON',
                data: {
                    'nama_user': nama_user,
                    'tanggal_keputusan': tanggal_keputusan,
                    'rekomendasi': rekomendasi,
                    'penggalangan_dana' : penggalangan_dana,
                    'catatan': catatan,
                    'pengajuan_id': pengajuan_id,
                    'role': role
                },
                success: function(msg) {
                    //if (msg.data == 1) {
                    if (msg == 1) {
                        Swal.fire({
                            title: "Notifikasi",
                            text: "Keputusan Telah Disimpan",
                            type: "success",
                        });
                        window.location.href = "/admin/adr/komitependanaan";
                    }
                    // console.log(msg.data);
                    // alert(msg.data);
                }
            });
        })
</script>
<script type="text/javascript">
    $(document).ready( function(){
        var token = "{{ csrf_token() }}";
        $('#Kirim').on('click',function(){
            var idPengajuan = $('#Kirim').val();
            var user = $('#user').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/adr/kirimKomite",
                method: "POST",
                dataType: 'JSON',
                data: {
                    'idPengajuan': idPengajuan,
                    'user_login' : user
                },
                success: function(msg) {
                    //if (msg.data == 1) {
                    if (msg == 1) {
                        window.location.href = "/admin/adr/komitependanaan";
                    }else if(msg == 2){
                        Swal.fire({
                            title: "Notifikasi",
                            text: "Disposisi Belum Dilengkapi",
                            type: "warning",
                        });
                    }
                    // console.log(msg.data);
                    // alert(msg.data);
                }
            });
        })
    });
</script>

@endsection
