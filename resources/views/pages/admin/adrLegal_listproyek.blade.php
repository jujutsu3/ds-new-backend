@extends('layouts.admin.master')

@section('title', 'Panel Admin')
{{-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> --}}

@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Legalitas Pendanaan</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('progressadd'))
                    <div class="alert alert-danger">
                        {{ session()->get('progressadd') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('updatedone'))
                    <div class="alert alert-success">
                        {{ session()->get('updatedone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('createdone'))
                    <div class="alert alert-info">
                        {{ session()->get('createdone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Daftar Pendanaan</strong>
                    </div>
                    <div class="card-body">

                        <!-- table select all admin -->
                        <table id="legalitasTableData" class="table table-striped table-bordered table-responsive-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Pengajuan</th>
                                    <th>Tanggal Pengajuan</th>
                                    <th>Tanggal Verifikasi</th>
                                    <th>Penerima Pendanaan</th>
                                    <th>Jenis Pendanaan</th>
                                    <th>Tujuan Pendanaan</th>
                                    <th>Nilai Pengajuan Pendanaan</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                        <!-- end of table select all -->
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .content -->

@endsection
<script type="text/javascript" src="{{url('assetsBorrower/js/plugins/moment/moment.min.js')}}"></script>
@section('js')

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function() {

            var legalitasTable = $('#legalitasTableData').DataTable({
                searching: true,
                processing: true,
                "order": [
                    [1, "desc"]
                ],
                ajax: {
                    url: '/admin/adr/legal/listPengajuan',
                    dataSrc: 'data'
                },
                paging: true,
                info: true,
                lengthChange: false,
                pageLength: 10,
                columns: [
                    {
                        data: null,
                        render: function(data, type, row, meta) {
                            return meta.row + 1;
                        }
                    },
                    {
                        data: 'pengajuan_id'
                    },
                    {
                        data: 'tgl_pengajuan',
                        'render': function(data, type) {
                         return type === 'sort' ? data : moment(data).format('D/MM/YYYY');
                        }
                    },
                    {
                        data: 'tanggal_verifikasi',
                        'render': function(data, type) {
                         return type === 'sort' ? data : moment(data).format('D/MM/YYYY');
                        }
                    },
                    {
                        data: 'penerima_pendanaan'
                    },
                    {
                        data: 'tipe_pendanaan'
                    },
                    {
                        data: 'tujuan_pembiayaan'
                    },
                    {
                        data: 'nilai_pengajuan',
                        render: $.fn.dataTable.render.number(',', '.', 2, 'Rp. ')
                    },
                    {
                        data: null,
                        render: function(data, type, row, meta) {
                            if (row.status_legal == 0 || row.status_legal == 7) {
                                return '<button id="baru" class="btn btn-secondary btn-block">Baru</button>';
                            } else if (row.status_legal == 8) {
                                return '<button id="proses" class="btn btn-primary btn-block">Proses</button>';
                            } else if (row.status_legal == 9) {
                                return '<button id="kirim" class="btn btn-success btn-block">Selesai</button>';
                            } else {
                                return '';
                            }
                        }
                    },

                ],
                columnDefs: [{
                    targets: [0],
                    visible: false
                }]
            })
            var id;

            $('#legalitasTableData tbody').on('click', '#baru, #proses, #kirim', function() {
                var data = legalitasTable.row($(this).parents('tr')).data();
                idPengajuan = data.pengajuan_id;
                window.location.href = "../adr/leganalisapendanaan/" + idPengajuan;
            })




        });
    </script>

@endsection
