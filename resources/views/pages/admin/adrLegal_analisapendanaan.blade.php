@extends('layouts.admin.master')

@section('title', 'Panel Admin')


@section('style')

<style>
     .btn-danaSyariah {
            background-color: #16793E;
        }
        
    ul.breadcrumb {
      padding: 10px 16px;
      list-style: none;
      background-color: #fff;
    }
    ul.breadcrumb li {
      display: inline;
      font-size: 18px;
    }
    ul.breadcrumb li+li:before {
      padding: 8px;
      color: black;
      content: "/\00a0";
    }
    ul.breadcrumb li a {
      color: #0275d8;
      text-decoration: none;
    }
    ul.breadcrumb li a:hover {
      color: #01447e;
      text-decoration: underline;
    }
    </style>
@endsection

@section('content')


<ul class="breadcrumb">
    <li><a href="{{ route('legal.listproyek') }}">Legalitas Pendanaan</a></li>
    <li>Lanjut</li>
</ul>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <!-- START: informasi objek pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_informasi_objek_pendanaan">Informasi Objek Pendanaan &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tipe_pendanaan">Tipe Pendanaan</label>
                                <input class="form-control" type="text" id="tipe_pendanaan" name="tipe_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tujuan_pendanaan">Tujuan Pendanaan</label>
                                <input class="form-control" type="text" id="tujuan_pendanaan" name="tujuan_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-alamat_objek_pendanaan">Alamat Objek Pendanaan</label>
                                <input class="form-control" type="text" id="alamat_objek_pendanaan" name="alamat_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-provinsi">Provinsi</label>
                                <input class="form-control" type="text" id="provinsi" name="provinsi" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kota">Kota / Kabupaten</label>
                                <input class="form-control" type="text" id="kota_kabupaten" name="kota_kabupaten" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kecamatan">Kecamatan</label>
                                <input class="form-control" type="text" id="kecamatan" name="kecamatan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kelurahan">Kelurahan</label>
                                <input class="form-control" type="text" id="kelurahan" name="kelurahan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kode_pos">Kode Pos</label>
                                <input class="form-control" type="text" id="kode_pos" name="kode_pos" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-harga_objek_pendanaan">Harga Objek Pendanaan</label>
                                <input class="form-control" type="text" id="harga_objek_pendanaan" name="harga_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-uang_muka">Uang Muka</label>
                                <input class="form-control" type="text" id="uang_muka" name="uang_muka" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_pengajuan_pendanaan">Nilai Pengajuan Pendanaan</label>
                                <input class="form-control" type="text" id="nilai_pengajuan_pendanaan" name="nilai_pengajuan_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-4">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu">Jangka Waktu (Maks. 12 Bulan)</label>
                                <input class="form-control" type="text" id="jangka_waktu" name="jangka_waktu" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu">Detail Informasi Objek Pendanaan</label>
                                <textarea class="form-control" id="detail_informasi_pendanaan" rows="3" readonly></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- end informasi objek pendanaan -->
                    <!-- start informasi pemilik objek pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_informasi_pemilik_objek_pendanaan">Informasi Pemilik Objek Pendanaan &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_pemilik">Nama</label>
                                <input class="form-control" type="text" id="nama_pemilik" name="nama_pemilik" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-notelp">No. Telp/HP</label>
                                <input class="form-control" type="text" id="no_telp_pemilik" name="no_telp_pemilik" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-alamat_pemilik_objek_pendanaan">Alamat Pemilik Objek Pendanaan</label>
                                <input class="form-control" type="text" id="alamat_pemilik_objek_pendanaan" name="alamat_pemilik_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-provinsi_pemilik">Provinsi</label>
                                <input class="form-control" type="text" id="provinsi_pemilik" name="provinsi_pemilik" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kota_pemilik">Kota / Kabupaten</label>
                                <input class="form-control" type="text" id="kota_pemilik" name="kota_pemilik" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kecamatan_pemilik">Kecamatan</label>
                                <input class="form-control" type="text" id="kecamatan_pemilik" name="kecamatan_pemilik" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kelurahan_pemilik">Kelurahan</label>
                                <input class="form-control" type="text" id="kelurahan_pemilik" name="kelurahan_pemilik" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kode_pos_pemilik">Kode Pos</label>
                                <input class="form-control" type="text" id="kode_pos_pemilik" name="kode_pos_pemilik" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end informasi pemilik objek pendanaan -->
                    <!-- button kembali / simpan -->
                    <div class="col-md-12 mt-4">
                        <hr>
                    </div>

                    <div class="col-md-10 mt-4">
                     
                    </div>
                    <div class="col-md-2 text-right">
                        <a href="{{route('legal.daftarpekerjaan', $pengajuan_id)}}"><button type="submit" class="btn btn-primary mb-2 ml-2" id="proseslegalpendanaan"> <i class="fa fa-arrow-right" aria-hidden="true"></i> Lanjut</button></a>
                    </div>
                    <!-- end button -->
                </div>
            </div>
        </div>
    </div>
</div>
</div><!-- .content -->
@endsection
@section('js')
<script type="text/javascript">
    var pengajuan_id = "{{ $pengajuan_id }}";
    var side = '/admin/borrower';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    
    const formatRupiah = (angka, prefix) => {
        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
    }
    $(document).ready(function() {
        $.ajax({
            url : side+'/getProsesVerifData/'+pengajuan_id,
            method: "get",
            success:function(data,value)
            {


                $("#pengajuan_id").val(pengajuan_id);
              
                let tipe_pendanaan = data.jenis_pendanaan;
                let tujuan_pendanaan = data.tujuan_pendanaan;
                let alamat_objek_pendanaan = data.data_pengajuan.lokasi_proyek;
                let provinsi = data.data_pengajuan.provinsi;
                let kota_kabupaten = data.data_pengajuan.kota;
                let kecamatan = data.data_pengajuan.kecamatan;
                let kelurahan = data.data_pengajuan.kelurahan;
                let kode_pos = data.data_pengajuan.kode_pos;
                let harga_objek_pendanaan = formatRupiah(parseFloat(data.data_pengajuan.harga_objek_pendanaan).toString(), 'Rp. ');
                let uang_muka = formatRupiah(parseFloat(data.data_pengajuan.uang_muka).toString(), 'Rp. ');
                let nilai_pengajuan_pendanaan = formatRupiah(parseFloat(data.data_pengajuan.pendanaan_dana_dibutuhkan).toString(), 'Rp. ');
                let jangka_waktu = data.data_pengajuan.durasi_proyek;
                let detail_informasi_pendanaan = data.data_pengajuan.detail_pendanaan;

                $("#tipe_pendanaan").val(tipe_pendanaan);
                $("#tujuan_pendanaan").val(tujuan_pendanaan);
                $("#alamat_objek_pendanaan").val(alamat_objek_pendanaan);
                $("#provinsi").val(provinsi);
                $("#kota_kabupaten").val(kota_kabupaten);
                $("#kecamatan").val(kecamatan);
                $("#kelurahan").val(kelurahan);
                $("#kode_pos").val(kode_pos);
                $("#harga_objek_pendanaan").val(harga_objek_pendanaan);
                $("#uang_muka").val(uang_muka);
                $("#nilai_pengajuan_pendanaan").val(nilai_pengajuan_pendanaan);
                $("#jangka_waktu").val(jangka_waktu);
                $("#detail_informasi_pendanaan").val(detail_informasi_pendanaan);

                let nama_pemilik = data.data_pengajuan.nm_pemilik;
                let no_telp_pemilik = data.data_pengajuan.no_tlp_pemilik;
                let alamat_pemilik = data.data_pengajuan.alamat_pemilik;
                let provinsi_pemilik = data.data_pengajuan.provinsi_pemilik;
                let kota_pemilik = data.data_pengajuan.kota_pemilik;
                let kecamatan_pemilik = data.data_pengajuan.kecamatan_pemilik;
                let kelurahan_pemilik = data.data_pengajuan.kelurahan_pemilik;
                let kode_pos_pemilik = data.data_pengajuan.kd_pos_pemilik;

                $("#nama_pemilik").val(nama_pemilik);
                $("#no_telp_pemilik").val(no_telp_pemilik);
                $("#alamat_pemilik_objek_pendanaan").val(alamat_pemilik);
                $("#provinsi_pemilik").val(provinsi_pemilik);
                $("#kota_pemilik").val(kota_pemilik);
                $("#kecamatan_pemilik").val(kecamatan_pemilik);
                $("#kelurahan_pemilik").val(kelurahan_pemilik);
                $("#kode_pos_pemilik").val(kode_pos_pemilik);
            }
        });

    });
</script>
@endsection
