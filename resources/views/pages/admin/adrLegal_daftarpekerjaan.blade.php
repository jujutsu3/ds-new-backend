@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('content')

    <style>
        .largerCheckbox {
            width: 15px;
            height: 15px;
        }

        .modal-xl {
            max-width: 95% !important;
        }

    </style>

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <a href="{{ route('legal.listproyek') }}" style="color:black;">
                        <h1>Legalitas Pendanaan</h1>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">

        <div class="row mt-2">
            <div class="col-md-12">
                <button type="button" data-toggle="modal" data-target="#informasi_brw"
                    class="btn btn-info mb-2 text-white float-right rounded">Informasi Penerima Pendanaan</button>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                @if (session()->has('progressadd'))
                    <div class="alert alert-danger">
                        {{ session()->get('progressadd') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('updatedone'))
                    <div class="alert alert-success">
                        {{ session()->get('updatedone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('createdone'))
                    <div class="alert alert-info">
                        {{ session()->get('createdone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Daftar Pekerjaan Legalitas</strong>
                    </div>
                    <div class="card-body">

                        <!-- table select all admin -->
                        <table id="TableVerifikasiLegal" class="table table-striped table-bordered table-responsive-sm">
                            <thead>
                                <tr>
                                    <th width="10%">No</th>
                                    <th width="40%">Keterangan</th>
                                    <th width="15%">Terakhir Diperbaharui</th>
                                    <th width="5%" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                        </table>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-10">
                            @if(in_array($status_legal, ['7', '8']))
                            <input type="checkbox" class="largerCheckbox" id="hasilkerja" name="hasilkerja" value="1">
                                Hasil kerja yang dikirim adalah hasil yang sebenarnya dan dapat dipertanggung jawabkan.
                            @endif    
                        </div>
                        <div class="col-md-2">
                            @if(in_array($status_legal, ['7', '8']))
                            <button type="submit" class="btn btn-block btn-primary btn-md"
                                id="btnKonfirmasiKirim" disabled> <i class="fa fa-paper-plane" aria-hidden="true"></i>  Kirim</button>
                                <button type="submit" id="btnKirimFinal" class="invisible"></button>
                            @endif   
                        </div>
                        <!-- end of table select all -->

                    </div>
                </div>
            </div>
        </div>
    </div><!-- .content -->
@endsection

{{-- START: Modal Informasi Borrower --}}
@component('component.admin.modal_detail_borrower')
    @slot('modal_id', 'informasi_brw')
    @slot('brw_nama', $data_borrower->nama)
    @slot('brw_nik', $data_borrower->ktp)
    @slot('brw_jns_kelamin', $data_borrower->jns_kelamin)
    @slot('brw_pic', $data_borrower->brw_pic)
@endcomponent
{{-- END: Modal Informasi Borrower --}}

@section('js')
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var pengajuan_id = "{{ $pengajuan_id }}";
        var status_legal = "{{ $status_legal }}";

        $(document).ready(function() {

            $('#btnKirimFinal').click(function(e) {
                e.preventDefault();

                $.ajax({
                    url: "{{ route('adr.kirim.legalitas') }}",
                    method: "post",
                    data: { pengajuan_id: "{{ $pengajuan_id }}"},
                    beforeSend: () => {
                        Swal.fire({
                            html: '<h5>Mengirim Data ...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                            }
                        });
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;
                        if (xhr.status === 419) {
                            window.alert('Halaman Session Expired. Mohon login ulang');
                        }

                    },
                    success: function(result) {
                        if (result == '1') {
                            location.href = "/admin/adr/legalitaspendanaan"
                        }else{
                            return false;
                        }

                    }
                   
                });


            });

            $.ajax({
                url:  '/admin/adr/legal_selesai/' + pengajuan_id,
                method: "get",
                success: function(data, value) {
                    var result = $.parseJSON(data);
                    if (result.data.jumlah_selesai == 2 && result.data.status_verifikator != 9) {
                        $('#hasilkerja').attr('disabled', false);
                    } else {
                        $('#hasilkerja').attr('disabled', true);
                    }

                }
            });

            $("#hasilkerja").click(function() {
                var checked_status = this.checked;
                if (checked_status == true) {
                    $("#btnKonfirmasiKirim").removeAttr("disabled");
                } else {
                    $("#btnKonfirmasiKirim").attr("disabled", "disabled");
                }
            });


            
            $('#btnKonfirmasiKirim').click(function(e) {
                Swal.fire({
                    title: "Notifikasi",
                    text: "Apakah anda yakin ingin mengirim data ini ?",
                    type: "warning",
                    buttons: true,
                    showCancelButton: true,
                }).then(result => {
                    if (result.value == true) {
                        $('#btnKirimFinal').trigger('click');
                    } else {
                        return false;
                    }
                });
            });





            var TableVerifikasiLegal = $('#TableVerifikasiLegal').DataTable({
                processing: false,
                serverSide: false,
                select: false,
                "searchable": false,
                "paging": false,
                bFilter: false,
                "bPaginate": false,
                "bInfo": false,
                autoWidth: false,
                ajax: {
                    url: '/admin/adr/legal/pekerjaan/' + pengajuan_id,
                    type: 'get',
                },
                columns: [{
                        data: "no"
                    },
                    {
                        data: "keterangan"
                    },
                    {
                        data: "terakhir_diperbaharui"
                    },
                    {
                        data: "item",
                        render: function(data, type, value, meta) {

                            let button = (data.status == 1) ? 'btn-primary' : 'btn-secondary';
                            let pengajuan_id = '{!! $pengajuan_id !!}'
                           
                            let view = (data.status == 1) ? 'Ubah' : 'Baru';

                            if(status_legal == '9'){
                                view = 'Lihat';
                                button = 'btn-success';
                            }

                            if (data.no == 1) {
                                return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                    onclick="move_page(${pengajuan_id}, 'form_naup')">${view}</button>
                                `
                            } else if (data.no == 2) {
                                return `<button type="button" class="btn btn-block ${button}" id="btnScoreUld"
                                    onclick="move_page(${pengajuan_id}, 'form_biaya')">${view}</button>
                                `
                            }
                         
                        }
                    }
                ]
            });


        });

        function move_page(pengajuan_id, page_name) {
            let route = "{{  route('next-page-verif-legal', ['page_name' => 'page_name_val','pengajuan_id' => 'pengajuan_id_val','brw_id' => 'brw_id_val','keterangan' => 'keterangan_val'])  }}";
            route = route.replace('page_name_val', page_name).replace('pengajuan_id_val', pengajuan_id).replace('keterangan_val', page_name).replaceAll("&amp;", "&")
            window.location.href = route
        }

    
    </script>

@endsection
