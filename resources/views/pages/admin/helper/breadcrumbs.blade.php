<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{ $title }}</h1>
            </div>
        </div>
        <div class="page-header float-right">
            <div class="page-title">
                {{ $slot }}
            </div>
        </div>
    </div>
</div>
