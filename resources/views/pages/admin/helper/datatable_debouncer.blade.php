<script>
    // Datatable debounce from https://datatables.net/forums/discussion/comment/127222/#Comment_127222
    $(document).ready(function () {
        /* from https://davidwalsh.name/javascript-debounce-function */
        function debounce(func, wait, immediate) {
            let timeout;
            return function () {
                let context = this, args = arguments;
                let later = () => {
                    timeout = null;
                    if (! immediate) func.apply(context, args);
                };
                let callNow = immediate && ! timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        }

        function debounceSearch(selector) {
            $(selector).each(function () {
                let id = '#' + $(this).attr('id');
                let $searchBox = $(id + "_filter input[type='search']");

                if ($(this).attr('data-search-box-id') !== undefined) {
                    $searchBox = $('#' + $(this).attr('data-search-box-id'));
                }

                $searchBox.off();

                let searchDebouncedFn = debounce(function () {
                    $(id).DataTable().search($searchBox.val()).draw();
                }, 400);

                $searchBox.on("keyup", searchDebouncedFn);
            });
        }

        debounceSearch('.dataTable');
    });
</script>
