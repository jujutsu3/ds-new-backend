<script src="{{ asset('admin/assets/js/autoNumeric.min.js') }}"></script>
<script>
    initAutoNumeric = function (selector, options = {}) {
        let defaultOptions = {
            unformatOnSubmit: true,
            showWarnings: false,
            allowDecimalPadding: false,
            decimalCharacter: ',',
            digitGroupSeparator: '.',
            watchExternalChanges: true
        };

        return new AutoNumeric(selector, Object.assign({}, defaultOptions, options));
    };
</script>
