<div class="form-group {{ $size ?? 'col-lg-12' }}">
    <label for="{{ $name ?? '' }}" class=" form-control-label">{{ $title }}</label>
    {{ $slot }}
</div>
