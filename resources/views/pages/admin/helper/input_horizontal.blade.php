<div class="{{ $size ?? 'col-lg-12' }} mb-4">
    <div class="input-group">
        <div class="input-group-prepend">
            <div class="input-group-text">{{ $title }}</div>
        </div>
        {{ $slot }}
    </div>
</div>
