<div class="container-fluid">
    <p class="h6 my-4 layout_informasi_akun line"><b>Informasi Akun &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_status_akun">
            <div class="col-sm-12">
                <label for="b_status_akun" class="font-weight-normal">Status Akun</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_status_akun" name="b_status_akun" class="form-control">
                <label class="text-danger font-weight-normal" id="b_label_status_akun" name="b_label_status_akun" for="b_status_akun"></label>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_status_akun" name="a_status_akun" class="form-control">
                <label class="text-danger font-weight-normal" id="a_label_status_akun" name="a_label_status_akun" for="a_status_akun"></label>
            </div>
        </div>
        
        <div class="row d-none" name="f_username">
            <div class="col-sm-12">
                <label for="b_username" class="font-weight-normal">Akun</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_username" name="b_username" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_username" name="a_username" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_email">
            <div class="col-sm-12">
                <label for="b_email" class="font-weight-normal">Email</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="email" id="b_email" name="b_email" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="email" id="a_email" name="a_email" class="form-control">
            </div>
        </div>
    </fieldset>

    {{-- <hr class="my-4 layout_informasi_akun"> --}}
    <p class="h6 my-4 layout_informasi_pribadi line"><b>Informasi Pribadi &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_nama" id="f_nama">
            <div class="col-sm-12">
                <label for="b_nama" class="font-weight-normal">Nama</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_nama" name="b_nama" class="form-control" value="">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_nama" name="a_nama" class="form-control" value="">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_jenis_kelamin" id="f_jenis_kelamin">
            <div class="col-sm-12">
                <label for="jenis_kelamin" class="font-weight-normal">Jenis Kelamin</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_jenis_kelamin" name="b_jenis_kelamin" class="form-control custom-select">
                    @foreach($master_jenis_kelamin as $b)
                    <option value="{{ $b->id_jenis_kelamin }}">{{ $b->jenis_kelamin }}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_jenis_kelamin" name="a_jenis_kelamin" class="form-control custom-select">
                    @foreach($master_jenis_kelamin as $b)
                    <option value="{{ $b->id_jenis_kelamin }}">{{ $b->jenis_kelamin }}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_tanggal_lahir" id="f_tanggal_lahir">
            <div class="col-sm-12">
                <label for="b_tanggal_lahir" class="font-weight-normal">Tanggal
                    Lahir</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" class="form-control" id="b_tanggal_lahir" name="b_tanggal_lahir" value="">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" class="form-control" id="a_tanggal_lahir" name="a_tanggal_lahir" value="">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_tempat_lahir" id="f_tempat_lahir">
            <div class="col-sm-12">
                <label for="b_tempat_lahir" class="font-weight-normal">Tempat
                    Lahir</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_tempat_lahir" name="b_tempat_lahir" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_tempat_lahir" name="a_tempat_lahir" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_status_kawin" id="f_status_kawin">
            <div class="col-sm-12">
                <label for="b_status_kawin" class="font-weight-normal">Status
                    Perkawinan</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_status_kawin" name="b_status_kawin" class="form-control custom-select">
                    @foreach($master_kawin as $kawin)
                    <option value="{{$kawin->id_kawin}}"
                        {{!empty($detil->status_kawin_investor) && $kawin->id_kawin == $detil->status_kawin_investor ? 'selected=selected' : ''}}>
                        {{$kawin->jenis_kawin}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_status_kawin" name="a_status_kawin" class="form-control custom-select">
                    @foreach($master_kawin as $kawin)
                    <option value="{{$kawin->id_kawin}}"
                        {{!empty($detil->status_kawin_investor) && $kawin->id_kawin == $detil->status_kawin_investor ? 'selected=selected' : ''}}>
                        {{$kawin->jenis_kawin}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_agama" id="f_agama">
            <div class="col-sm-12">
                <label for="b_agama" class="font-weight-normal">Agama</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_agama" name="b_agama" class="form-control custom-select">
                    @foreach ($master_agama as $b)
                    <option value="{{$b->id_agama}}">{{$b->agama}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_agama" name="a_agama" class="form-control custom-select">
                    @foreach ($master_agama as $b)
                    <option value="{{$b->id_agama}}">{{$b->agama}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_no_ktp" id="f_no_ktp">
            <div class="col-sm-12">
                <label id="label_no_ktp" for="no_ktp" class="font-weight-normal">No
                    KTP</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="hidden" name="jenis_identitas" id="jenis_identitas">
                <input type="text" name="b_no_ktp" id="b_no_ktp" class="form-control" value="">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_no_ktp" id="a_no_ktp" class="form-control" value="">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_no_passport" id="f_no_passport">
            <div class="col-sm-12">
                <label id="label_no_passport" for="no_passport" class="font-weight-normal">No
                    Passport</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="hidden" name="jenis_identitas" id="jenis_identitas">
                <input type="text" name="b_no_passport" id="b_no_passport" class="form-control" value="">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_no_passport" id="a_no_passport" class="form-control" value="">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_jenis_identitas" id="f_jenis_identitas">
            {{-- before --}}
            <div class="col">
                <div class="col-sm-12 pl-0">
                    <label id="b_label_jenis_identitas" for="b_jenis_identitas" class="font-weight-normal">No
                        Passport</label>
                </div>
                <input type="text" name="b_jenis_identitas" id="b_jenis_identitas" class="form-control" value="">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <div class="col-sm-12 pl-0">
                    <label id="a_label_jenis_identitas" for="b_jenis_identitas" class="font-weight-normal">No
                        KTP</label>
                </div>
                <input type="text" name="a_jenis_identitas" id="a_jenis_identitas" class="form-control" value="">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_warga_negara" id="f_warga_negara">
            <div class="col-sm-12">
                <label for="b_warga_negara" class="font-weight-normal">Warga
                    Negara</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_warga_negara" name="b_warga_negara" class="form-control custom-select">
                    @foreach ($master_negara as $b)
                    <option value="{{$b->id_negara}}">
                        {{$b->negara}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_warga_negara" name="a_warga_negara" class="form-control custom-select">
                    @foreach ($master_negara as $b)
                    <option value="{{$b->id_negara}}">
                        {{$b->negara}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_no_npwp" id="f_no_npwp">
            <div class="col-sm-12">
                <label for="b_no_npwp" class="font-weight-normal">No. NPWP</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_no_npwp" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_no_npwp" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_no_telp" id="f_no_telp">
            <div class="col-sm-12">
                <label for="b_no_telp" class="font-weight-normal">No Telp / HP</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_no_telp" id="b_no_telp" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_no_telp" id="a_no_telp" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_nama_ibu_kandung" id="f_nama_ibu_kandung">
            <div class="col-sm-12">
                <label for="b_nama_ibu_kandung" class="font-weight-normal">Nama Ibu
                    Kandung</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_nama_ibu_kandung" id="b_nama_ibu_kandung" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_nama_ibu_kandung" id="a_nama_ibu_kandung" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_pendidikan" id="f_pendidikan">
            <div class="col-sm-12">
                <label for="pendidikan" class="font-weight-normal">Pendidikan</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select name="b_pendidikan" class="form-control custom-select">
                    @foreach($master_pendidikan as $pendidikan)
                    <option value="{{$pendidikan->id_pendidikan}}"
                        {{!empty($detil->pendidikan_investor) && $pendidikan->id_pendidikan == $detil->pendidikan_investor ? 'selected=selected' : ''}}>
                        {{$pendidikan->pendidikan}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select name="a_pendidikan" class="form-control custom-select">
                    @foreach($master_pendidikan as $pendidikan)
                    <option value="{{$pendidikan->id_pendidikan}}"
                        {{!empty($detil->pendidikan_investor) && $pendidikan->id_pendidikan == $detil->pendidikan_investor ? 'selected=selected' : ''}}>
                        {{$pendidikan->pendidikan}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
    </fieldset>

    {{-- <hr class="my-4 layout_informasi_pekerjaan"> --}}
    <p class="h6 my-4 layout_informasi_pekerjaan line"><b>Informasi Pekerjaan &nbsp;</b></p>
    <fieldset>
        <div class="row mt-2 d-none" name="f_sumber_dana" id="f_sumber_dana">
            <div class="col-sm-12">
                <label for="b_sumber_dana" class="font-weight-normal">Sumber Dana</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_sumber_dana" id="b_sumber_dana" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_sumber_dana" id="a_sumber_dana" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_pekerjaan" id="f_pekerjaan">
            <div class="col-sm-12">
                <label for="b_pekerjaan" class="font-weight-normal">Pekerjaan</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select name="b_pekerjaan" class="form-control custom-select">
                    @foreach($master_pekerjaan as $pekerjaan)
                    <option value="{{$pekerjaan->id_pekerjaan}}"
                        {{!empty($detil->pekerjaan_investor) && $pekerjaan->id_pekerjaan == $detil->pekerjaan_investor ? 'selected=selected' : ''}}>
                        {{$pekerjaan->pekerjaan}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select name="a_pekerjaan" class="form-control custom-select">
                    @foreach($master_pekerjaan as $pekerjaan)
                    <option value="{{$pekerjaan->id_pekerjaan}}"
                        {{!empty($detil->pekerjaan_investor) && $pekerjaan->id_pekerjaan == $detil->pekerjaan_investor ? 'selected=selected' : ''}}>
                        {{$pekerjaan->pekerjaan}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_bidang_pekerjaan" id="f_bidang_pekerjaan">
            <div class="col-sm-12">
                <label for="b_bidang_pekerjaan" class="font-weight-normal">Bidang
                    Pekerjaan</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select name="b_bidang_pekerjaan" class="form-control custom-select">
                    @foreach ($master_bidang_pekerjaan as $b)
                    <option value="{{$b->kode_bidang_pekerjaan}}">
                        {{$b->bidang_pekerjaan}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select name="a_bidang_pekerjaan" class="form-control custom-select">
                    @foreach ($master_bidang_pekerjaan as $b)
                    <option value="{{$b->kode_bidang_pekerjaan}}">
                        {{$b->bidang_pekerjaan}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_pendapatan" id="f_pendapatan">
            <div class="col-sm-12">
                <label for="b_pendapatan" class="font-weight-normal">Pendapatan</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select name="b_pendapatan" class="form-control custom-select">
                    @foreach($master_pendapatan as $pendapatan)
                    <option value="{{$pendapatan->id_pendapatan}}">
                        {{$pendapatan->pendapatan}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select name="a_pendapatan" class="form-control custom-select">
                    @foreach($master_pendapatan as $pendapatan)
                    <option value="{{$pendapatan->id_pendapatan}}">
                        {{$pendapatan->pendapatan}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_pengalaman_kerja" id="f_pengalaman_kerja">
            <div class="col-sm-12">
                <label for="b_pengalaman_kerja" class="font-weight-normal">Pengalaman
                    Kerja</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_pengalaman_kerja" name="b_pengalaman_kerja" class="form-control custom-select">
                    <option value=""></option>
                    @foreach ($master_pengalaman_kerja as $b)
                    <option value="{{$b->id_pengalaman_kerja}}">
                        {{$b->pengalaman_kerja}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_pengalaman_kerja" name="a_pengalaman_kerja" class="form-control custom-select">
                    <option value=""></option>
                    @foreach ($master_pengalaman_kerja as $b)
                    <option value="{{$b->id_pengalaman_kerja}}">
                        {{$b->pengalaman_kerja}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_bidang_online" id="f_bidang_online">
            <div class="col-sm-12">
                <label for="b_bidang_online" class="font-weight-normal">Bidang Online</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_bidang_online" name="b_bidang_online" class="form-control custom-select">
                    @foreach ($master_online as $b)
                    <option value="{{$b->id_online}}">
                        {{$b->tipe_online}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_bidang_online" name="a_bidang_online" class="form-control custom-select">
                    @foreach ($master_online as $b)
                    <option value="{{$b->id_online}}">
                        {{$b->tipe_online}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>
    </fieldset>

    {{-- <hr class="my-4 layout_informasi_alamat_sesuai_ktp"> --}}
    <p class="h6 my-4 layout_informasi_alamat_sesuai_ktp line"><b>Alamat Sesuai dengan KTP &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_domisili_negara" id="f_domisili_negara">
            <div class="col-sm-12">
                <label for="b_domisili_negara" class="font-weight-normal">Domisili Negara</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_domisili_negara" name="b_domisili_negara" class="form-control custom-select">
                    <option value="">--Pilih--</option>
                    @foreach ($master_negara as $b)
                    <option value="{{$b->id_negara}}">{{$b->negara}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_domisili_negara" name="a_domisili_negara" class="form-control custom-select">
                    <option value="">--Pilih--</option>
                    @foreach ($master_negara as $b)
                    <option value="{{$b->id_negara}}">{{$b->negara}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_alamat" id="f_alamat">
            <div class="col-sm-12">
                <label for="b_alamat" class="font-weight-normal">Alamat</label>
            </div>
            {{-- before --}}
            <div class="col">
                <textarea name="b_alamat" class="form-control col-sm-12" rows="3" id="b_alamat"
                    placeholder="Alamat Lengkap"></textarea>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <textarea name="a_alamat" class="form-control col-sm-12" rows="3" id="a_alamat"
                    placeholder="Alamat Lengkap"></textarea>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_provinsi" id="f_provinsi">
            <div class="col-sm-12">
                <label for="b_provinsi" class="font-weight-normal">Provinsi</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select class="form-control custom-select" id="b_provinsi" name="b_provinsi">
                    <option value="">-- Pilih Satu --</option>
                    @foreach ($master_provinsi as $data)
                    <option value={{$data->kode_provinsi}}>
                        {{$data->nama_provinsi}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select class="form-control custom-select" id="a_provinsi" name="a_provinsi">
                    <option value="">-- Pilih Satu --</option>
                    @foreach ($master_provinsi as $data)
                    <option value={{$data->kode_provinsi}}>
                        {{$data->nama_provinsi}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_kota" id="f_kota">
            <div class="col-sm-12">
                <label for="b_kota" class="font-weight-normal">Kota</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select class="form-control custom-select" id="b_kota" name="b_kota">
                    <option value="">-- Pilih Satu --</option>
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select class="form-control custom-select" id="a_kota" name="a_kota">
                    <option value="">-- Pilih Satu --</option>
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_kecamatan" id="f_kecamatan">
            <div class="col-sm-12">
                <label for="b_kecamatan" class="font-weight-normal">Kecamatan</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_kecamatan" class="form-control" id="b_kecamatan">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_kecamatan" class="form-control" id="a_kecamatan">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_kelurahan" id="f_kelurahan">
            <div class="col-sm-12">
                <label for="b_kelurahan" class="font-weight-normal">Kelurahan</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_kelurahan" class="form-control" id="b_kelurahan">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_kelurahan" class="form-control" id="a_kelurahan">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_kode_pos" id="f_kode_pos">
            <div class="col-sm-12">
                <label for="b_kode_pos" class="font-weight-normal">Kode Pos</label>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="b_kode_pos" class="form-control" id="b_kode_pos" placeholder="Kode Pos">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_kode_pos" class="form-control" id="a_kode_pos" placeholder="Kode Pos">
            </div>
        </div>
    </fieldset>

    <p class="h6 my-4 layout_foto_pendana line"><b>Foto Pendana &nbsp;</b></p>
    <fieldset>
        <div class="row mt-2 d-none" name="f_pic_investor" id="f_pic_investor">
            <div class="col-sm-12">
                <label id="label_pic_investor" for="">Foto Diri <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="b_pic_investor" name="b_pic_investor">
                </div>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="a_pic_investor" name="a_pic_investor">
                </div>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_pic_ktp_investor" id="f_pic_ktp_investor">
            <div class="col-sm-12">
                <label id="label_pic_ktp_investor" for="">Foto KTP <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="b_pic_ktp_investor" name="b_pic_ktp_investor">
                </div>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="a_pic_ktp_investor" name="a_pic_ktp_investor">
                </div>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_pic_user_ktp_investor" id="f_pic_user_ktp_investor">
            <div class="col-sm-12">
                <label id="label_pic_user_ktp_investor" for="">Foto Diri dengan
                KTP <i class="text-danger">*</i></label>
            </div>
            {{-- before --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="b_pic_user_ktp_investor" name="b_pic_user_ktp_investor">
                </div>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <div class="mx-auto" style="width: 200px;">
                    <img class="rounded img-fluid" id="a_pic_user_ktp_investor" name="a_pic_user_ktp_investor">
                </div>
            </div>
        </div>
    </fieldset>

    {{-- <hr class="my-4 layout_informasi_rekening"> --}}
    <p class="h6 my-4 layout_informasi_rekening line"><b>Informasi Rekening &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_va_number" id="f_va_number">
            <div class="col-sm-12">
                <label for="b_va_number" class="font-weight-normal">VA BNI</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_va_number" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_va_number" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_va_cimbs">
            <div class="col-sm-12">
                <label for="b_va_cimbs" class="font-weight-normal">VA CIMB Syariah</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_va_cimbs" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_va_cimbs" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_va_bsi">
            <div class="col-sm-12">
                <label for="b_va_bsi" class="font-weight-normal">VA BSI</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_va_bsi" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_va_bsi" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_rekening">
            <div class="col-sm-12">
                <label for="b_rekening" class="font-weight-normal">No Rekening</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_rekening" id="b_rekening" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_rekening" id="a_rekening" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_nama_pemilik_rek">
            <div class="col-sm-12">
                <label for="b_nama_pemilik_rek" class="font-weight-normal">Nama Pemilik
                    Rekening</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" name="b_nama_pemilik_rek" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" name="a_nama_pemilik_rek" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_bank">
            <div class="col-sm-12">
                <label for="b_bank" class="font-weight-normal">Bank</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select name="b_bank" class="form-control custom-select">
                    @foreach($master_bank as $b)
                    <option value="{{ $b->kode_bank }}">{{ $b->nama_bank }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select name="a_bank" class="form-control custom-select">
                    @foreach($master_bank as $b)
                    <option value="{{ $b->kode_bank }}">{{ $b->nama_bank }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </fieldset>

    {{-- <hr class="my-4 layout_informasi_ahli_waris"> --}}
    <p class="h6 my-4 layout_informasi_ahli_waris line"><b>Informasi Ahli Waris &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_nama_ahli_waris" id="f_nama_ahli_waris">
            <div class="col-sm-12">
                <label for="b_nama_ahli_waris" class="font-weight-normal">Nama Ahli
                    Waris</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_nama_ahli_waris" name="b_nama_ahli_waris" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_nama_ahli_waris" name="a_nama_ahli_waris" class="form-control">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_hub_ahli_waris" id="f_hub_ahli_waris">
            <div class="col-sm-12">
                <label for="b_hub_ahli_waris" class="font-weight-normal">Hubungan Ahli
                    Waris</label>
            </div>
            {{-- before --}}
            <div class="col">
                <select id="b_hub_ahli_waris" name="b_hub_ahli_waris" class="form-control custom-select">
                    @foreach ($master_hub_ahli_waris as $data)
                    <option value={{$data->id_hub_ahli_waris}}>{{$data->jenis_hubungan}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <select id="a_hub_ahli_waris" name="a_hub_ahli_waris" class="form-control custom-select">
                    @foreach ($master_hub_ahli_waris as $data)
                    <option value={{$data->id_hub_ahli_waris}}>{{$data->jenis_hubungan}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_nik_ahli_waris" id="f_nik_ahli_waris">
            <div class="col-sm-12">
                <label for="b_nik_ahli_waris" class="font-weight-normal">NIK Ahli
                    Waris</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_nik_ahli_waris" name="b_nik_ahli_waris" class="form-control"
                    placeholder="NIK Ahli Waris">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_nik_ahli_waris" name="a_nik_ahli_waris" class="form-control"
                    placeholder="NIK Ahli Waris">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_no_hp_ahli_waris" id="f_no_hp_ahli_waris">
            <div class="col-sm-12">
                <label for="b_no_hp_ahli_waris" class="font-weight-normal">No. HP Ahli
                    Waris</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_no_hp_ahli_waris" name="b_no_hp_ahli_waris" class="form-control"
                    placeholder="No. Hp Ahli Waris">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_no_hp_ahli_waris" name="a_no_hp_ahli_waris" class="form-control"
                    placeholder="No. Hp Ahli Waris">
            </div>
        </div>

        <div class="row mt-2 d-none" name="f_alamat_ahli_waris" id="f_alamat_ahli_waris">
            <div class="col-sm-12">
                <label for="b_alamat_ahli_waris" class="font-weight-normal">Alamat</label>
            </div>
            {{-- before --}}
            <div class="col">
                <textarea id="b_alamat_ahli_waris" name="b_alamat_ahli_waris" class="form-control col-sm-12" rows="3"
                    id="alamat" placeholder="Alamat Lengkap"></textarea>
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- before --}}
            <div class="col">
                <textarea id="a_alamat_ahli_waris" name="a_alamat_ahli_waris" class="form-control col-sm-12" rows="3"
                    id="alamat" placeholder="Alamat Lengkap"></textarea>
            </div>
        </div>
    </fieldset>

    {{-- Referal --}}
    <p class="h6 my-4 layout_informasi_referal line"><b>Referal &nbsp;</b></p>
    <fieldset>
        <div class="row d-none" name="f_kode_referal" id="f_kode_referal">
            <div class="col-sm-12">
                <label for="b_kode_referal" class="font-weight-normal">Kode Referal</label>
            </div>
            {{-- before --}}
            <div class="col">
                <input type="text" id="b_kode_referal" name="b_kode_referal" class="form-control">
            </div>
            <div class="col-md-1 my-auto text-center">
                <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
            </div>
            {{-- after --}}
            <div class="col">
                <input type="text" id="a_kode_referal" name="a_kode_referal" class="form-control">
            </div>
        </div>
    </fieldset>
</div>