@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('content')

<style>
    .largerCheckbox {
        width: 15px;
        height: 15px;
    }

    .modal-xl {
        max-width: 95% !important;
    }
</style>

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Legalitas Pendanaan - NAUP</h1>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <!-- START: Baris 1 -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_informasi_legalitas">Informasi Legalitas &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_legal">Nama Legal</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_legal" name="nama_legal" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_legalitas">Tanggal Legalitas</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tanggal_legalitas" name="tanggal_legalitas" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-ipdipp">Id Penerima Dana | Id Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="ipdipp" name="ipdipp" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_penerima_dana">Nama Penerima Dana</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_penerima_dana" name="nama_penerima_dana" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-plafon_pengajuan_pendanaan">Plafon Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="plafon_pengajuan_pendanaan" name="plafon_pengajuan_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-fasilitas_pendanaan">Fasilitas Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="fasilitas_pendanaan" name="fasilitas_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_pendanaan">Jangka Waktu Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_pendanaan" name="jangka_waktu_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kategori_penerima_pendanaan">Kategori Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="kategori_penerima_pendanaan" name="kategori_penerima_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end informasi legalitas -->
                    <!-- nota analisa -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_nota_analisa_usulan_pendanaan">Nota Analisa Usulan Pendanaan &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nomor_naup">Nomor NAUP</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nomor_naup" name="nomor_naup" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_permohonan_penerima_pendanaan">Tanggal Permohonan Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tanggal_permohonan_penerima_pendanaan" name="tanggal_permohonan_penerima_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-fasilitas_property_ke">Fasilitas Property Ke-</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="fasilitas_property_ke" name="fasilitas_property_ke" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-maksimum_FTV">Maksimum FTV</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="maksimum_FTV" name="maksimum_FTV" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jumlah_permohonan_penerima_pendanaan">Jumlah Permohonan Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jumlah_permohonan_penerima_pendanaan" name="jumlah_permohonan_penerima_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jumlah_plafond_diusulkan">Jumlah Plafond Diusulkan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jumlah_plafond_diusulkan" name="jumlah_plafond_diusulkan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-uang_muka">Uang Muka / Security Deposit</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="uang_muka" name="uang_muka" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-presentase_uang_muka">Presentase Uang Muka / Security Deposit</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="presentase_uang_muka" name="presentase_uang_muka" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-angsuran_pendanaan">Angsuran Pendanaan / Sewa Sesuai Usulan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="angsuran_pendanaan" name="angsuran_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end nota analisa -->
                    <!-- start rekomendasi striktur -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_rekomendasi_struktur_pendanaan">Rekomendasi Struktur Pendanaan &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-struktur_pendanaan">Struktur Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="col-md-6">
                                <input type="checkbox" id="wakalah" name="wakalah" value="ss"> Wakalah
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="wakalah" name="wakalah" value="ss"> IMBT
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="wakalah" name="wakalah" value="ss"> Qard
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="wakalah" name="wakalah" value="ss"> MMQ
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="wakalah" name="wakalah" value="ss"> Murabahah
                            </div>
                            <div class="col-md-6">
                                <input type="checkbox" id="wakalah" name="wakalah" value="ss"> Ijarah
                            </div>
                        </div>
                    </div>
                    <!-- start wakalah -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_wakalah"><u>Wakalah</u></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_produk">Nama Produk</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_produk" name="nama_produk" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu">Jangka Waktu</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu" name="jangka_waktu" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tujuan_penggunaan">Tujuan Penggunaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tujuan_penggunaan" name="tujuan_penggunaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-sumber_pelunasan">Sumber Pelunasa</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="sumber_pelunasan" name="sumber_pelunasan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pembelian_dari">Pembelian dari</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="pembelian_dari" name="pembelian_dari" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_pendanaan">Pengikatan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" type="text" id="pengikatan_pendanaan" name="pengikatan_pendanaan" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jumlah_wakalah">Jumlah Wakalah</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jumlah_wakalah" name="jumlah_wakalah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_jaminan">Pengikatan Jaminan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" type="text" id="pengikatan_jaminan" name="pengikatan_jaminan" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end wakalah -->

                    <!-- start qardh -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_qardh"><u>Qardh</u></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_produk_qardh">Nama Produk</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_produk_qardh" name="nama_produk_qardh" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_qardh">Jangka Waktu</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_qardh" name="jangka_waktu_qardh" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tujuan_penggunaan_qardh">Tujuan Penggunaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tujuan_penggunaan_qardh" name="tujuan_penggunaan_qardh" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-sumber_pelunasan_qardh">Sumber Pelunasa</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="sumber_pelunasan_qardh" name="sumber_pelunasan_qardh" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pembelian_dari_qardh">Pembelian dari</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="pembelian_dari_qardh" name="pembelian_dari_qardh" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_pendanaan_qardh">Pengikatan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" type="text" id="pengikatan_pendanaan_qardh" name="pengikatan_pendanaan_qardh" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jumlah_wakalah_qardh">Jumlah Wakalah</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jumlah_wakalah_qardh" name="jumlah_wakalah_qardh" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_jaminan_qardh">Pengikatan Jaminan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" type="text" id="pengikatan_jaminan_qardh" name="pengikatan_jaminan_qardh" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end qardh -->

                    <!-- start murabahah -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_murabahah"><u>Murabahah</u></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-skema_pendanaan_murabahah">Skema Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="skema_pendanaan_murabahah" name="skema_pendanaan_murabahah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pricing_pendanaan_murabahah">Pricing Pendanaan (Margin Rate)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pricing_pendanaan_murabahah" name="pricing_pendanaan_murabahah" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                % effektive p.a
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_produk_murabahah">Nama Produk</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_produk_murabahah" name="nama_produk_murabahah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-plafon_pendanaan_murabahah">Porsi/Plafon Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="plafon_pendanaan_murabahah" name="plafon_pendanaan_murabahah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tujuan_penggunaan_murabahah">Tujuan Penggunaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tujuan_penggunaan_murabahah" name="tujuan_penggunaan_murabahah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-angsuran_pendanaan_murabahah">Angsuran Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="angsuran_pendanaan_murabahah" name="angsuran_pendanaan_murabahah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-harga_beli_barang_murabahah">Harga Beli Barang</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="harga_beli_barang_murabahah" name="harga_beli_barang_murabahah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_murabahah">Jangka Waktu</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_murabahah" name="jangka_waktu_murabahah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-margin_murabahah">Margin</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="margin_murabahah" name="margin_murabahah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-denda_murabahah">Denda</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="denda_murabahah" name="denda_murabahah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-harga_jual_barang_murabahah">Harga Jual Barang</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="harga_jual_barang_murabahah" name="harga_jual_barang_murabahah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_pendanaan_murabahah">Pengikatan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" type="text" id="pengikatan_pendanaan_murabahah" name="pengikatan_pendanaan_murabahah" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-uang_muka_penerima_pendanaan_murabahah">Uang Muka Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="uang_muka_penerima_pendanaan_murabahah" name="uang_muka_penerima_pendanaan_murabahah" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_jaminan_murabahah">Pengikatan Jaminan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" type="text" id="pengikatan_jaminan_murabahah" name="pengikatan_jaminan_murabahah" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-harga_jual_barang_setelah_murabahah">Harga Jual Barang setelah Uang Muka Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="harga_jual_barang_setelah_murabahah" name="harga_jual_barang_setelah_murabahah" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end murabahah -->
                    <!-- start IMTB -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_imtb"><u>Al Ijarah Al Muntahiyah bit Al-Tamlik (IMTB)</u></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-skema_pendanaan_imtb">Skema Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="skema_pendanaan_imtb" name="skema_pendanaan_imtb" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_sewa_imtb">Nilai Sewa/Ujroh Perbulan (Rp)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_sewa_imtb" name="nilai_sewa_imtb" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_produk_imtb">Nama Produk</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_produk_imtb" name="nama_produk_imtb" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-security_deposit_imtb">Security Deposit Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="security_deposit_imtb" name="security_deposit_imtb" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tujuan_penggunaan_imtb">Tujuan Penggunaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tujuan_penggunaan_imtb" name="tujuan_penggunaan_imtb" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-porsi_pendanaan_imtb">Porsi Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="porsi_pendanaan_imtb" name="porsi_pendanaan_imtb" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_imtb">Jangka Waktu Sewa</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_imtb" name="jangka_waktu_imtb" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_pendanaan_imtb">Pengikatan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="pengikatan_pendanaan_imtb" name="pengikatan_pendanaan_imtb" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-harga_perolehan_aktiva_imtb">Harga Perolehan Aktiva</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="harga_perolehan_aktiva_imtb" name="harga_perolehan_aktiva_imtb" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-pengikatan_jaminan_imtb">Pengikatan Jaminan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="pengikatan_jaminan_imtb" name="pengikatan_jaminan_imtb" readonly>
                                    <option value="">Test</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-ekpektasi_nilai_sewa_imtb">Ekspektasi Nilai Sewa/Ujroh (%)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="ekpektasi_nilai_sewa_imtb" name="ekpektasi_nilai_sewa_imtb" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-angsuran_pendanaan_imtb">Angsuran Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="angsuran_pendanaan_imtb" name="angsuran_pendanaan_imtb" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_ujroh_imtb">Nilai Ujroh</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_ujroh_imtb" name="nilai_ujroh_imtb" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-denda_imtb">Denda</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="denda_imtb" name="denda_imtb" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-penetapan_nilai_ujroh_imtb">Penetapan Nilai Ujroh Selanjutanya dilakukan setiap (Bulan)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="penetapan_nilai_ujroh_imtb" name="penetapan_nilai_ujroh_imtb" readonly>
                                    <option value="">Pilih</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end al ijarah -->
                    <!-- start musyarakah  -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_imtb"><u>Musyarakah Mutanaqishah (MMQ)</u></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-skema_pendanaan_mmq">Skema Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="skema_pendanaan_mmq" name="skema_pendanaan_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_pendanaan_mmq">Jangka Waktu Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_pendanaan_mmq" name="jangka_waktu_pendanaan_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_produk_mmq">Nama Produk</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_produk_mmq" name="nama_produk_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_penyerahan_objek_mmq">Jangka Waktu Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_penyerahan_objek_mmq" name="jangka_waktu_penyerahan_objek_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-tujuan_penggunaan_mmq">Tujuan Penggunaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tujuan_penggunaan_mmq" name="tujuan_penggunaan_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-objek_bagi_hasil_mmq">Objek Bagi Hasil</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="objek_bagi_hasil_mmq" name="objek_bagi_hasil_mmq" readonly>
                                    <option value="">Pilih</option>
                                    <option value="">Pendapatan Sewa Dari Pembayaran</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-harga_beli_asset_mmq">Harga Beli Asset</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="harga_beli_asset_mmq" name="harga_beli_asset_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-ekspektasi_nilai_sewa_mmq">Ekspektasi Nilai Sewa/Ujroh</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="ekspektasi_nilai_sewa_mmq" name="ekspektasi_nilai_sewa_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-plafon_pendanaan_mmq">Plafon Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="plafon_pendanaan_mmq" name="plafon_pendanaan_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-proyeksi_total_pendanaan_mmq">Proyeksi Total Pendapatan Sewa/Ujroh</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="proyeksi_total_pendanaan_mmq" name="proyeksi_total_pendanaan_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-uang_muka_penerima_dana_mmq">Uang Muka/Modal Penerima Dana</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="uang_muka_penerima_dana_mmq" name="uang_muka_penerima_dana_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nisbah_mmq"><b>Nisbah :</b></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-modal_syirkah_mmq">Modal Syirkah</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="modal_syirkah_mmq" name="modal_syirkah_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nisbah_penyelenggara_mmq">1. Nisbah Penyelenggara</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nisbah_penyelenggara_mmq" name="nisbah_penyelenggara_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-hishshah_perunit_mmq">Hishshah Per Unit</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="hishshah_perunit_mmq" name="hishshah_perunit_mmq" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nisbah_penerima_mmq">2. Nisbah Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nisbah_penerima_mmq" name="nisbah_penerima_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-hishshah_mmq"><b>Hishshah :</b></label>
                            </div>
                        </div>
                        <div class="col-md-4">

                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-denda_perhari_mmq">Denda (Per Hari Keterlambatan)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="denda_perhari_mmq" name="denda_perhari_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-hishshah_penyelenggara_mmq">1. Hishshah Penyelenggara</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="hishshah_penyelenggara_mmq" name="hishshah_penyelenggara_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-hishshah_penerima_pendanaan_mmq">2. Hishshah Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="hishshah_penerima_pendanaan_mmq" name="hishshah_penerima_pendanaan_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-total_hishshah_mmq">Total Hishshah</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="total_hishshah_mmq" name="total_hishshah_mmq" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end musyarakah -->
                    <!-- start ijarah -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h6" for="form_ijarah"><u>Ijarah</u></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-objek_sewa_ijarah">Objek Sewa</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="objek_sewa_ijarah" name="objek_sewa_ijarah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_sewa_ijarah">Jangka Waktu Sewa (bulan)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_sewa_ijarah" name="jangka_waktu_sewa_ijarah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_penyerahan_sewa_ijarah">Jangka Waktu Penyerahan Objek Sewa (bulan)</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_penyerahan_sewa_ijarah" name="jangka_waktu_penyerahan_sewa_ijarah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_sewa_ijarah">Nilai Sewa/Ujroh</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_sewa_ijarah" name="nilai_sewa_ijarah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-penyesuaian_nilai_sewa_ijarah">Penyesuaian Nilai Sewa/Ujroh</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="penyesuaian_nilai_sewa_ijarah" name="penyesuaian_nilai_sewa_ijarah" readonly>
                                    <option value="">Pilih</option>
                                    <option value="">Review Ujorh Dilakukan Pada Setiap</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- end ijarah -->
                    <!-- end rekomendasi struktur -->
                    <!-- start Data Objek Pendanaan -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_data_objek_pendanaan">Data Objek Pendanaan (Agunan/Jaminan) &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jenis_objek_pendanaan">Jenis Objek Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jenis_objek_pendanaan" name="jenis_objek_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-status_jenis_sertifikat">Status/Jenis Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="status_jenis_sertifikat" name="status_jenis_sertifikat" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-no_sertifikat">No Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="no_sertifikat" name="no_sertifikat" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_berakhir_hak">Tanggal Berakhir Hak</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tanggal_berakhir_hak" name="tanggal_berakhir_hak" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-sertifikat_atas_nama">Sertifikat Atas Nama</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="sertifikat_atas_nama" name="sertifikat_atas_nama" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-alamat_sertifikat">Alamat Sertifikat</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="alamat_sertifikat" name="alamat_sertifikat" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-no_izin_mendirikan_bangunan">No Izin Mendirikan Bangunan (IMB)</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="no_izin_mendirikan_bangunan" name="no_izin_mendirikan_bangunan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-imb_atas_nama">IMB Atas Nama</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="imb_atas_nama" name="imb_atas_nama" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-luas_tanah">Luas Tanah m2</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="luas_tanah" name="luas_tanah" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-luas_bangunan">Luas Bangunan m2</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="luas_bangunan" name="luas_bangunan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_penilaian_agunan">Tanggal Penilaian Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="date" id="tanggal_penilaian_agunan" name="tanggal_penilaian_agunan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-appraisal">Appraisal</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="appraisal" name="appraisal" readonly>
                                    <option value="">Pilih</option>
                                    <option value="">Internal</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_pasar_wajar">Nilai Pasar Wajar</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_pasar_wajar" name="nilai_pasar_wajar" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_likuidasi">Nilai Likuidasi</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nilai_likuidasi" name="nilai_likuidasi" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-rekomendasi_appraisal">Rekomendasi Appraisal</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="rekomendasi_appraisal" name="rekomendasi_appraisal" readonly>
                                    <option value="">Pilih</option>
                                    <option value="">Direkomendasikan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jenis_pengikat_agunan">Jenis Pengikat Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" id="jenis_pengikat_agunan" name="jenis_pengikat_agunan" readonly>
                                    <option value="">Pilih</option>
                                    <option value="">Notaril</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nilai_pengikat_agunan">Nilai Pengikat Agunan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-3">
                            <input class="form-control" type="text" id="nilai_pengikat_agunan" name="nilai_pengikat_agunan">
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-4">
                                <input class="form-control" type="number" id="plafon_pembiayaan" name="plafon_pembiayaan">
                            </div>
                            <div class="col-md-8">
                                % Plafon Pembiayaan
                            </div>
                        </div>
                    </div>
                    <!-- end data objek Pendanaan -->
                    <!-- start hasil Scorring -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_hasil_scoring">Hasil Scoring &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-biro_kredit">Biro Kredit</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-7">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="biro_kredit" name="biro_kredit" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-credolab">Credolab</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-7">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="score_personal" id="score_personal" name="score_personal" readonly>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="score_pendanaan" id="score_pendanaan" name="score_pendanaan" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-very_jelas">Very Jelas</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-7">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="very_jelas" name="very_jelas" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-hasil_scoring_akhir">Hasil Scoring Akhir</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-7">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" id="hasil_scoring_akhir" name="hasil_scoring_akhir" readonly>
                                        <option value="">Pilih</option>
                                        <option value="">Low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end hasil scorring -->
                    <!-- start catatan anilis kredit -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="catatan_analis_kredit">Usulan/Catatan Analis Kredit &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-12 mb-4">
                        <div class="form-group">
                            <textarea class="form-control" name="catatan_analis_kredit" id="catatan_analis_kredit" cols="30" rows="5">Catatan/Usulan Analis Kredit</textarea>
                        </div>
                    </div>
                    <!-- end catatan anilis kredit -->
                    <!-- button kembali / simpan -->
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('legal.daftarpekerjaan')}}"><button type="submit" class="btn btn-block btn-success btn-lg" id="kembali">Kembali</button></a>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-block btn-success btn-lg" id="simpan">Simpan</button>
                    </div>
                    <!-- end button -->
                </div>
            </div>
        </div>
    </div>
</div>
</div><!-- .content -->

<!-- 2. GOOGLE JQUERY JS v3.2.1  JS !-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>

<script src="/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/tinymce/js/tinymce/jquery.tinymce.min.js"></script>

<script type="text/javascript">
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        //     var pengajuKPR_table = $('#pengajuKPR_data').DataTable({
        //         searching: true,
        //         processing: true,
        //         "order": [
        //             [1, "asc"]
        //         ],
        //         ajax: {
        //             url: '/admin/adr/ListPengajuan',
        //             dataSrc: 'data'
        //         },
        //         paging: true,
        //         info: true,
        //         lengthChange: false,
        //         pageLength: 10,
        //         columns: [{
        //                 data: 'pengajuan_id'
        //             },
        //             {
        //                 data: null,
        //                 render: function(data, type, row, meta) {
        //                     return meta.row + 1;
        //                 }
        //             },
        //             {
        //                 data: 'tgl_pengajuan'
        //             },
        //             {
        //                 data: 'nama_cust'
        //             },
        //             {
        //                 data: 'tipe_pendanaan'
        //             },
        //             {
        //                 data: 'tujuan_pembiayaan'
        //             },
        //             {
        //                 data: null,
        //                 render: function(data, type, row, meta) {
        //                     return '<p class=text-right>' + row.nilai_pengajuan + '</p>';
        //                 }
        //             },
        //             {
        //                 data: null,
        //                 render: function(data, type, row, meta) {
        //                     if (row.stts == 0 || row.stts == 3) {
        //                         return '<button id="tombol" class="btn btn-secondary btn-block">Menunggu Verifikasi</button>';
        //                     } else if (row.stts == 2 || row.stts == 5) {
        //                         return '<button id="tombol" class="btn btn-danger btn-block">ditolak</button>';
        //                     } else if (row.stts == 1) {
        //                         return '<button id="tombol" class="btn btn-success btn-block">diterima</button>';
        //                     }
        //                 }
        //             },

        //         ],
        //         columnDefs: [{
        //             targets: [0],
        //             visible: false
        //         }]
        //     })
        //     var id;

        //     $('#pengajuKPR_data tbody').on('click', '#tombol', function() {
        //         var data = pengajuKPR_table.row($(this).parents('tr')).data();
        //         idPengajuan = data.pengajuan_id;
        //         brw_id = data.brw_id;
        //         window.location.href = "../adr/detailPengajuan/" + idPengajuan + "/" + brw_id;
        //     })

        //     // view
        //     $('#pengajuKPR_data tbody').on('click', '#generate', function() {
        //         var data = pengajuKPR_table.row($(this).parents('tr')).data();
        //         $(this).prop('disabled', true);
        //         did = data.id;
        //         $.ajax({
        //             url: "manage_payout/" + did,
        //             method: "post",
        //             success: function(data) {
        //                 if (data.Success) {
        //                     swal('Berhasil', 'Kembali Dana Selesai', 'success');
        //                     window.location.reload();

        //                 }
        //             }
        //         })

        //     })

    });
</script>


@endsection