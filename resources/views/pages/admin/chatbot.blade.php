@extends('layouts.admin.master')

@section('title', 'Chatbot admin')

@section('content')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Create Chatbot</h1>
            </div>
        </div>
    </div>
</div>

<form action="{{route('admin.storeChatbot')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="col-lg-12">
        <div class="card">
            @if(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card-header"><small> Form </small><strong>Chatbot</strong></div>
            <div class="card-body card-block">
                <div class="form-group">
                    <label for="nama" class=" form-control-label">Category</label>
                    <select class="form-control" name="category_id" required>
                        <option value="1">Umum</option>
                        <option value="2">Lender</option>
                        <option value="3">Borrower</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nama" class=" form-control-label">code</label>
                    <input type="text" name="code" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="nama" class=" form-control-label">question_id</label>
                    <input type="text" name="question_id" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="question" class=" form-control-label">question</label>
                    <textarea name="question"></textarea>
                </div>
                <div class="form-group">
                    <label for="nama" class=" form-control-label">answer_id</label>
                    <input type="text" name="answer_id" class="form-control" required>
                </div>
                <div class="form-group">
                    <label for="answer" class=" form-control-label">answer</label>
                    <textarea name="answer"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <button type="submit" class="btn btn-success btn-block">Kirim</button>
    </div>
</form>



<script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=pxhk4l5ikav9zeyfqw5zqjad3socezw0wupfw90lqb0vcrmt'></script>

<script>
    tinymce.init({
        selector: 'textarea',
        height: 300,
        theme: 'modern',
        skin: 'lightgray',
        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype == 'image') {
                $('#upload').trigger('click');
                $('#upload').on('change', function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        });
                    };
                    reader.readAsDataURL(file);
                });
            }
        },
        templates: [{
                title: 'Test template 1',
                content: 'Test 1'
            },
            {
                title: 'Test template 2',
                content: 'Test 2'
            }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>

@endsection