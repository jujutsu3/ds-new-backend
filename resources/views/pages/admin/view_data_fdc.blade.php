@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('content')
<style>
    .bg_red{
        color : "red"
    }
</style>
<div class="content mt-3">
<div class="row">
<div class="col-md-12">
    <div class="card">
        
        <div class="card-header d-flex p-0">
        <h4 class="card-title p-3">Data FDC</h4>
        <ul class="nav nav-pills ml-auto p-2">
            <li class="nav-item"><a class="nav-link active" href="#upload_data" data-toggle="tab">Upload Data</a></li>
            <li class="nav-item"><a class="nav-link" href="#inquiry" data-toggle="tab">Inquiry</a></li>
            {{-- <li class="nav-item"><a class="nav-link" href="#password" data-toggle="tab">Ganti Password</a></li> --}}
            
        </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane active" id="upload_data">
                    @if ($message = Session::get('status'))
                    
                        @if ($message == "gagal_ext")

                            <div class="alert alert-warning alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ Session::get('keterangan') }}</strong>
                            </div>
                        @endif
                        @if ($message == "file_hari_ini")

                            <div class="alert alert-warning alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ Session::get('keterangan') }}</strong>
                            </div>
                        @endif
                        
                        @if ($message == "status_pengiriman")

                            <div class="alert alert-info alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ Session::get('keterangan') }}</strong>
                            </div>
                        @endif
                        @if ($message == "sukses")

                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ Session::get('keterangan') }}</strong>
                            </div>
                        @endif
                        @if ($message == "sukses_upload_afpi")

                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ Session::get('keterangan') }}</strong>
                            </div>
                        @endif
                        @if ($message == "file_error")

                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ Session::get('keterangan') }}</strong>
                            </div>
                        @endif
                        @if ($message == "sukses_hapus")

                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button> 
                                <strong>{{ Session::get('keterangan') }}</strong>
                            </div>
                        @endif
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div id="upload_data">
                                <form method="post" action='{{route('admin.upload_file_fdc')}}' id="formData" enctype="multipart/form-data">
                                {{-- <form method="POST" id="formData" enctype="multipart/form-data"> --}}
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="file" name="file_fdc" id="file_fdc" required>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <button id="btn_proses" type="submit" class="btn btn-primary">
                                                    <span class="glyphicon glyphicon-ok"></span> Unggah Ke Internal
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    {{-- <button  id="btn_proses">Proses<button> --}}
                                </form>
            
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table id="tableListFileFDC" class="table table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Id FDC</th>
                                        <th>Tanggal File</th>
                                        <th>Tanggal Upload</th>
                                        <th>Filename</th>
                                        <th>Version</th>
                                        <th>Status Pengiriman</th>
                                        <th>Status File</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                            {{-- <div class="div_file">
                                <ul id="treeDemo" class="ztree"></ul>
                            </div> --}}
                        </div>
                    </div>
                    
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="inquiry">
                    {{-- <form method="POST"> --}}
                        @csrf
                        <div class="col-lg-12">
                                
                                <div class="form-group">
                                    <label for="nama" class=" form-control-label">NIK / NPWP <span style="color:red">*</span></label>
                                    <input type="text" name="txt_id" id="txt_id" maxlength="16" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="nama"  class=" form-control-label">Reason <span style="color:red">*</span></label>
                                    <select name="txt_reason" id="txt_reason" class=" form-control">
                                        <option value="">Pilih Reason</option>
                                        <option value="1">1 New Loan Reason</option>
                                        <option value="2">2 Existing Customer</option>
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="nama" class=" form-control-label">ReffID<span style="color:red">*</span></label>
                                    <input type="text" name="txt_reffid" id="txt_reffid" maxlength="20"  class="form-control" required>
                                </div>
                                
                            
                        </div>
                        <div class="col-lg-12">
                            <button id="btn_inquiry" type="button" class="btn btn-success btn-block">Submit</button>
                        </div>
                    {{-- </form> --}}

                    <div class="col-lg-12">
                        <table id="tableInquiryFDC" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                         
                                        <th>Id Penyelenggara</th>
                                        <th>Jenis Pengguna</th>
                                        <th>Nama Borrower</th>
                                        <th>No. Identitas</th>
                                        <th>NPWP</th>
                                        <th>Tgl Perjanjian</th>
                                        <th>Tgl Penyaluran</th>
                                        <th>Nilai Pendanaan</th>
                                        <th>Tgl Pelaporan</th>
                                        <th>Sisa Pinjaman Berjalan</th>
                                        <th>Tanggal Jatuh Tempo</th>
                                        <th>Kualitas Pinjaman</th>
                                        <th>DPD Terakhir</th>
                                        <th>DPD Maksimal</th>
                                        <th>Status Pinjaman</th>
                                        <th>Jenis Pengguna </th>
                                        <th>Kualitas Pinjaman</th>
                                        <th>Status Pinjaman</th>
                                    </tr>
                                </thead>
                            </table>
                    </div>
                    
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <table id="tableInquiryFDC" class="table table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                         
                                        <th>Id Penyelenggara</th>
                                        <th>Jenis Penyelenggara</th>
                                        <th>Nama Borrower</th>
                                        <th>No. Identitas</th>
                                        <th>NPWP</th>
                                        <th>Tgl Perjanjian</th>
                                        <th>Tgl Penyaluran</th>
                                        <th>Nilai Pendanaan</th>
                                        <th>Tgl Pelaporan</th>
                                        <th>Sisa Pinjaman Berjalan</th>
                                        <th>Tanggal Jatuh Tempo</th>
                                        <th>Kualitas Pinjaman</th>
                                        <th>DPD Terakhir</th>
                                        <th>DPD Maksimal</th>
                                        <th>Status Pinjaman</th>
                                        <th>Jenis Pengguna </th>
                                        <th>Kualitas Pinjaman</th>
                                        <th>Status Pinjaman</th>
                                        
                                         
                                    </tr>
                                </thead>
                            </table>
                            {{-- <div class="div_file">
                                <ul id="treeDemo" class="ztree"></ul>
                            </div> --}}
                        {{-- </div> --}}
                    {{-- </div> --}}
                    
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="password">
                    <div class="row">
                        <div class="col-lg-12">
                                
                            <div class="form-group">
                                <label for="nama" class=" form-control-label">Password Baru <span style="color:red">*</span></label>
                                <input type="password" name="txt_password" id="txt_password" maxlength="16" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="nama" class=" form-control-label">Konfirmasi Password Baru <span style="color:red">*</span></label>
                                <input type="password" name="txt_password_baru" id="txt_password_baru" maxlength="16" class="form-control" required>
                            </div>
                            
                        </div>
                        <div class="col-lg-12">
                            <button id="btn_password" type="button" class="btn btn-success btn-block">Submit</button>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
            </div>
        <!-- /.tab-content -->
        </div><!-- /.card-body -->

        
        <div class="modal fade" id="modal_upload_internal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <form method="post" action='../admin/upload_data_perbaikan' id="formData" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="txt_proyek_nama">Upload File Perbaikan</h5>
                    </div>
                    <div class="modal-body">

                        <input type="hidden" id="text_id_data" name="text_id_data">
                        <input type="hidden" id="text_id_filename" name="text_id_filename">
                        <input type="file" name="file_perbaikan" id="file_perbaikan">

                    </div>
                    <div class="modal-footer">
                       
                        <button type="button" id="btn_close_form" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="btn_submit_form" class="btn btn-info">Submit</button>

                    </div>
                </div>
            </form>
            </div>
        </div>

        <div class="modal fade" id="modal_upload_fdc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

            <div class="modal-dialog modal-lg" role="document">
                <form method="post" action='../upload_data_manual_ke_afpi' id="formData">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="txt_proyek_nama">Upload File Ke Server AFPI</h5>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="text_filename_afpi" name="text_filename_afpi">
                            <input type="hidden" id="text_id_filename_afpi" name="text_id_filename_afpi">
                            <p id="text_file_afpi"> </p>
                        </div>
                        <div class="modal-footer">
                        
                            <button type="button" id="btn_close_form" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" id="btn_submit_fdc" class="btn btn-info">Submit</button>
                            
                        </div>
                    </div>
                </form>
            </div>

        </div>

        <div class="modal fade" id="modal_delete_file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <form method="POST" action='../admin/delete_file_fdc' id="formDelete">
                    @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="txt_proyek_nama">Ambil Response</h5>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="text_delete_filename_afpi" name="text_delete_filename_afpi">
                        <input type="hidden" id="text_delete_id_file" name="text_delete_id_file">
                        <p id="text_delete_file"></p>
                    </div>
                    <div class="modal-footer">
                       
                        <button type="button" id="btn_close_form" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit"  class="btn btn-info">Submit</button>
                        
                    </div>
                </div>
            </form>
            </div>
        </div>
            
</div>
</div>
</div>


<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script>
    var data;
    
    var Table;

    var setting = { 
        
        view: 
            {
                expandSpeed:"",
				removeHoverDom: removeHoverDom,
				selectedMulti: false
			},
			
			data: {
				simpleData: {
					enable: true
				}
			},
			callback: {
				beforeRemove: beforeRemove,
				//beforeRename: beforeRename,
                //onClick: show
                beforeClick: actionFile
			}
    };

    

    function actionFile(treeId,treeNode,clickFlag){
        
        console.log(treeNode);
        if(!treeNode.isParent){

            Swal.fire({
                title: 'Anda Ingin Mengunggah file '+treeNode.name+'?',
                icon: 'warning',
                showCancelButton: false,
                showCloseButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Unggah Ke FDC',
                cancelButtonText: 'Hapus File',
            }).then((result) => {
                if (result) {
                    
                    $.ajax({
                        url: "../upload_data_manual/"+treeNode.name,
                        type: "GET"
                    }).done(function(response){
                        if(response.status == "sukses"){
                            Swal.fire({
                                icon: 'success',
                                title: 'Sukses',
                                text: response.keterangan 
                            })

                            location.href="";
                        }
                        else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Gagal ...',
                                text:  response.keterangan
                            });
                            location.href="";
                        }
                    });

                }
            });
            //return false;
        }
        // return true;
    }
    
   
    function beforeRemove(treeId, treeNode) {
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        zTree.selectNode(treeNode);
        return confirm("Confirm delete node '" + treeNode.name + "' it?");
    }		
   

    function removeHoverDom(treeId, treeNode) {
        $("#addBtn_"+treeNode.tId).unbind().remove();
    };

    function upload_file_perbaikan(id_fdc, filename){
        
        //$("#modal_detail_tarik").modal("show");
		$('#text_id_data').val(id_fdc)
        $('#text_id_filename').val(filename)

    }

    function delete_file(id, filename){
		
		console.log(id, filename );
        $("#text_delete_filename_afpi").val(filename);
        $("#text_delete_id_file").val(id);
        $("#text_delete_file").text("Melihat Response File : " + filename);
        //$("#modal_detail_tarik").modal("show");
	}
    
    var dataSet;
    

    $(document).ready(function(){
        
        // $.getJSON("{{route('admin.list_file_fdc')}}", function(data){
        //     $.fn.zTree.init($("#treeDemo"), setting, data);
        // });

        // var tableAjax = $('#table_personal').DataTable();

        // tableAjax.ajax.url('ajax_data.php?data=filterHargaTableCarter&unit='+unit+'&tujuan='+tujuan+'&type='+type_p).load();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        $("#btn_inquiry").click(function(){

            var txt_id      = $("#txt_id").val();
            var txt_reason  = $("#txt_reason option:selected").val();
            var txt_reffid  = $("#txt_reffid").val();
             
           if(txt_reason == "" || txt_reason == null){
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Invalid Inquery Reason'
                })
           }else{
            $.ajax({
                url: "../inquiry_fdc",
                type: "GET",
                data: {"txt_id" : txt_id, "txt_reason" : txt_reason, "txt_reffid" : txt_reffid, "_token": "{{ csrf_token() }}"},
                dataType: 'json'
            }).done(function(response) {
                 
                console.log(JSON.stringify(response));
                
                
                if(response.status == 'Found')
                {
                    
                    if(response.pinjaman.length>0){
                    
                    
                        $("#tableInquiryFDC").DataTable().clear();
                        
                        for(var i = 0; i < response.pinjaman.length; i++) {
                            var pinjaman = response.pinjaman[i];
                            
                            
                        
                            $("#tableInquiryFDC").DataTable().row.add( [

                                pinjaman.id_penyelenggara,
                                pinjaman.jenis_pengguna,
                                pinjaman.nama_borrower,
                                pinjaman.no_identitas,
                                pinjaman.no_npwp,
                                pinjaman.tgl_perjanjian_borrower,
                                pinjaman.tgl_penyaluran_dana,
                                pinjaman.nilai_pendanaan,
                                pinjaman.tgl_pelaporan_data,
                                pinjaman.sisa_pinjaman_berjalan,
                                pinjaman.tgl_jatuh_tempo_pinjaman,
                                pinjaman.kualitas_pinjaman,
                                pinjaman.dpd_terakhir,
                                pinjaman.dpd_max,
                                pinjaman.status_pinjaman,
                                pinjaman.jenis_pengguna_ket,
                                pinjaman.kualitas_pinjaman_ket,
                                pinjaman.status_pinjaman_ket
                            ]).draw(false) ; 
                            
                        }
                    
                    }

                } 
                else {
                    Swal.fire({
                        icon: 'success',
                        title: 'Information',
                        text: 'Data Tidak Ada'
                    })
                } 
            });
           }
            


        });

        $("#btn_password").click(function(){

            var txt_password        = $("#txt_password").val();
            var txt_password_baru   = $("#txt_password_baru").val();

            if(txt_password == "" || txt_password_baru == ""){
                
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Anda Belum Mengisi Password Baru Dan Konfirmasi Password'
                });

            }
            else if(txt_password != txt_password_baru){
                
                Swal.fire({
                    icon: 'error',
                    title: 'Gagal',
                    text: 'Password Tidak Sama'
                });

            }else{
                $.ajax({
                    url: "../ganti_password/"+txt_password,
                    type: "GET",
                    dataType: 'json'
                }).done(function(response) {
                    if(response.status == "Sukses"){
                        Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: response.status
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Gagal',
                            text: response.status
                        });
                    }
                });
            }

            
        });

        
        $('#tableListFileFDC').DataTable({
            
            processing: true,
            ajax : {
              url : "../admin/list_file_fdc",
              type : 'get',
            },


            "columns" : [
              {"data" : "No"},
              {"data" : "Id_FDC"},
              {"data" : "Tanggal_File"},
              {"data" : "Tanggal_Upload"},
              {"data" : "Filename"},
              {"data" : "Version"},
              {"data" : "Status_Pengiriman"}, 
              {"data" : "Status_File"},
              {"data" : "Action"},
          
            ],
            "columnDefs" :[
                {
                    "targets": 0,
                    class : 'text-left',
                    //"visible" : false,
                },
                {
                    "targets": 1,
                    class : 'text-left',
                "visible" : false,
                },
                {
                    "targets": 2,
                    class : 'text-left',
                    // "visible" : false,
                },
                {
                    "targets": 3,
                    class : 'text-left',
                    // "visible" : false,
                },
                {
                    "targets": 4,
                    class : 'text-left',
                    // "visible" : false,
                },
                {
                    "targets": 5,
                    class : 'text-left',
                    style : 'width:150px;',
                    
                },
                {
                    "targets": 6,
                    class : 'text-left',
                    style : 'width:150px;',
                    
                },
                {
                    "targets": 7,
                    class : 'text-left',
                    style : 'width:150px;',
                
                },
                {
                    "targets": 8,
                    class : 'text-left',
                    style : 'width:150px;',
                    //"visible" : false
                    "render" : function(data, type, value, meta){
                        
                        var disabled_upload_internal = "";
                        var disabled_upload_afpi = "";
                        var disabled_delete_file = "";

                        if(value['Status_Pengiriman'] == 1 && value['Status_File'] == 1){

                            disabled_upload_internal += "disabled";
                            disabled_upload_afpi += "disabled";
                            disabled_delete_file += "disabled";
                            

                        }else if(value['Status_Pengiriman'] == 0 && value['Status_File'] == 0){

                            disabled_upload_internal += "enabled";


                            $("#text_filename_afpi").val(value['Filename']);
                            $("#text_id_filename_afpi").val(value['Id_FDC']);
                            $("#text_file_afpi").text("Anda Yakin Ingin Mengunggah File " + value['Filename'] + " Ke server AFPI / FDC ?");

                            
                            //$("#text_delete_filename_afpi").val(value['Filename']);
                            //$("#text_delete_id_file").val(value['Id_FDC']);
                            //$("#text_delete_file").text("Anda Yakin Ingin Menghapus File " + value['Filename']);

                        }
                        else if(value['Status_Pengiriman'] == 1 && value['Status_File'] == 0){
                            
                            $("#text_id_data").val(value['Id_FDC']);
                            $("#text_id_filename").val(value['Filename']);
                        }

                         return " <button id='btn_proses' type='button' data-toggle='modal' data-target='#modal_upload_internal' onclick='upload_file_perbaikan("+ value['Id_FDC']+',\"'  + value['Filename'] + "\")' class='btn btn-success' "+disabled_upload_internal+">Upload File Perbaikan</button> <br><br>"+
                              //  " <button data-toggle='modal' data-target='#modal_upload_fdc' class='btn btn-primary' "+disabled_upload_afpi+">Upload Storage</button> <br><br>"+
                               " <button data-toggle='modal' data-target='#modal_delete_file' onclick='delete_file("+ value['Id_FDC']+',\"'  + value['Filename'] + "\")' "+disabled_delete_file+" class='btn btn-info'>Response</button>";                     
                    }
                }
              
             
            ]
        });


        
        
    });
        
</script>



@endsection