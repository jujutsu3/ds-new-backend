@extends('layouts.admin.master')

@section('title', 'Dashboard Borrower')
	
@section('content')
	
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
			<div class="page-title">
				<h1>Kelola Cicilan</h1>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="row">
		<div class="col-md-12">
			@if (session('error'))
				<div class="alert alert-danger col-sm-12">
					{{ session('error') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif (session('success'))
				<div class="alert alert-success col-sm-12">
					{{ session('success') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('updated'))
				<div class="alert alert-success col-sm-12">
					{{ session('updated') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif

			<div class="card">
				<div class="card-header">
					<strong class="card-title">Daftar Kelola Cicilan Penerima Pendanaan</strong>
				</div>
				<div class="card-body">
					<table id="tblCicilanDana" class="table table-striped table-bordered table-responsive-sm">
						<thead>
							<tr>
								<th>No</th>
								<th>ID Invoice</th>
								<th>ID BRW</th>
								<th>ID Pendanaan</th>
								<th>Nomor Invoice</th>
                                <th>Penerima Pendanaan</th>
								<th>Pendanaan</th>
								<th>Nominal</th>
								<th>Periode</th>
								<th>Tgl Jatuh Tempo</th>
								<th>Telat Bayar(Hari)</th>
								<th>Nominal Dibayar</th>
								<th>Nomor Referal Bank</th>
                                <th>Tgl Pembayaran</th>
								<th>Tgl Konfirmasi</th>
								<th>Konfirmasi Oleh</th>
								<th>Aksi</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- start modal konfirmasi invoice-->
<div class="modal fade" id="modalKonfirmasiInvoice" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Detail Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="{{route('admin.unduhBuktiTransferCicilan')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="col-lg-12">
					
					<div class="col-lg-12 mt-2" id="table_bukti_transfer">
						<div class="row">
							<div class="col-sm-12">
								<br>
								<table class="table compact table-striped table-bordered table-hover" id="table_list_bukti_bayar">
									<thead>
										<tr>
											<th>No</th>
											<th>Bukti ID</th>
											<th>Invoice ID</th>
											<th>Brw ID</th>
											<th>Nomor Invoice</th>
											<th>Tgl Pembayaran</th>
											<th>Nomor Referal</th>
											<th>Nominal</th>
											<th>Tgl Konfirmasi</th>
											<th>Konfirmasi Oleh</th>
											<th>Keterangan</th>
											<th>Aksi</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
                </div>
              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal konfirmasi invoice-->

<!-- start modal lihat bukti transfer-->
<div class="modal fade" id="modalLihatTransfer" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Detail Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="{{route('admin.unduhBuktiTransferCicilan')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="col-lg-12">
					
                    <div class="card-body card-block">
                        <div class="form-row">
                          <div class="col-lg-12">
                            <input type="hidden" id="brw_id_lihat" name="brw_id_lihat">
							<input type="hidden" id="invoice_id_lihat" name="invoice_id_lihat">
							<input type="hidden" id="pict_status_lihat" name="pict_status_lihat">
							<input type="hidden" id="routes_url" name="routes_url">
							<input type="hidden" id="bukti_id_lihat" name="bukti_id_lihat">
                            <div class="text-center" id="tambahUpload">
								<img id='image_pembayaran_lihat_tolak' src='' class="rounded" style="width: 400px; height: 600px; ">
							</div>
                          </div>
                        </div>
					</div>
                </div>
              </div>
            <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Unduh</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end of modal lihat bukti transfer-->

<!-- start modal konfirmasi bukti transfer pencairan-->
<div class="modal fade" id="modalConfirmTransfer" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Bukti Transfer Cicilan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="{{route('admin.konfirmasiTransfer')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card-body card-block">
                        <div class="form-row">
                          <div class="col-lg-12">
                            <input type="hidden" id="invoice_id_confirm" name="invoice_id_confirm">
                            <input type="hidden" id="bukti_id_confirm" name="bukti_id_confirm">
							<input type="hidden" id="pendanaan_id_confirm" name="pendanaan_id_confirm">
							<input type="hidden" id="brw_id_confirm" name="brw_id_confirm">
                            <div class="col-lg-12 mt-2" id="lihatFoto">
								<div class="text-center">
									<img id="image_transfer_confirm" src='' class="rounded" style="width:400px; height: 600px; ">
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
								  <label style="font-size:large; margin-top:10px" id="no_referal"></label>
								</div>
							</div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            <div class="modal-footer">
				<button id = "button_tolak" type="button" class="btn btn-danger">Tolak</button>
				<button id = "button_konfirmasi" type="button" class="btn btn-primary">Konfirmasi</button>
			</div>
				<div id="keterangan_tambah" >
					<div class="col-md-12">
						<div class="form-group">
							<label for="wizard-progress2-nik">Kenapa pembayaran ini ditolak?</label>
							<input type="hidden" id="status_confirm" name="status_confirm">
							<input class="form-control allowCharacter" type="text" id="keterangan_confirm" name="keterangan_confirm" placeholder="Isi Keterangan Ditolak..."> 
						</div>
					</div>
					<div class="col-md-12 my-2" >
						<button type="button" id="button_tolak_save" style="float : right; margin-bot:5px" class="btn btn-primary">Kirim</button>
					</div>
				</div>
				<div id="nominal_tambah" >
					<div class="col-md-12">
						<div class="form-group">
							<label for="wizard-progress2-nik">Nominal Transfer Cicilan</label>
							<input class="form-control allowCharacter" data-type="currency" type="text"  id="nominal_confirm" name="nominal_confirm" placeholder="Isi Nominal Transfer Cicilan Sesuai Bukti Transfer..." > 
						</div>
					</div>
					<div class="col-md-12 my-2" >
						<button type="button" id="button_setuju_save" style="float : right; margin-bot:5px" class="btn btn-primary">Kirim</button>
						<button type="submit" id="save_konfirmasi_cicilan" class="invisible"></button>
					</div>
				</div>
            </form>
        </div>
    </div>
</div>
<!-- end of modal konfirmasi bukti transfer pencairan-->

<style>
	.carousel-control-next-icon, 
	.carousel-control-prev-icon {
		height: 100px;
		width: 100px;
		background-size: 100%, 100%;
		background-image: none;;
	}
	.carousel-control-next, 
	.carousel-control-prev {
		width: 10%;
		background-color: white;
		opacity: .3;
	}
	.carousel-control-next-icon:after
	{
	content: '>';
	font-size: 55px;
	color: black;
	}
	.carousel-control-prev-icon:after {
	content: '<';
	font-size: 55px;
	color: black;
	}
	.swal-button--confirm {
		background: rgb(25, 150, 25);
	}
	.modal-xl {
		max-width: 80% !important;
	}
</style>

<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />

<script src="{{asset('js/sweetalert.js')}}"></script>
<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>
<!--<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>-->
<script src="{{url('admin/assets/js/lib/select2.full.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />

<script type="text/javascript">
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$(document).ready(function(){

		$("input[data-type='currency']").on({
			keyup: function() {
			formatCurrency($(this));
			},
			blur: function() { 
			formatCurrency($(this), "blur");
			}
		});

		$(document).on('hidden.bs.modal', function (event) {
			if ($('.modal:visible').length) {
			$('body').addClass('modal-open');
			}
		});

		$(document).on('show.bs.modal', '.modal', function (event) {
			var zIndex = 1040 + (10 * $('.modal:visible').length);
			$(this).css('z-index', zIndex);
			setTimeout(function() {
				$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
			}, 0);
		});

		function formatNumber(n) {
			// format number 1000000 to 1,234,567
			return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
		}


		function formatCurrency(input, blur) {
			var input_val = input.val();

			if (input_val === "") { return; }
			
			var original_len = input_val.length;
			var caret_pos = input.prop("selectionStart");
				
			if (input_val.indexOf(",") >= 0) {
				var decimal_pos = input_val.indexOf(",");

				var left_side = input_val.substring(0, decimal_pos);
				var right_side = input_val.substring(decimal_pos);

				left_side = formatNumber(left_side);
				right_side = formatNumber(right_side);

				right_side = right_side.substring(0, 2);
				input_val = left_side + "." + right_side;

			} else {
				input_val = formatNumber(input_val);
				input_val = input_val;
			}
			input.val(input_val);

			var updated_len = input_val.length;
			caret_pos = updated_len - original_len + caret_pos;
			input[0].setSelectionRange(caret_pos, caret_pos);
		}
		
		var tblCicilanDana = $('#tblCicilanDana').DataTable({
		
			processing: true,
			// serverSide: true,
			"ajax" : {
				url : '/admin/borrower/listVerifikasiCicilan/',
				type : 'get',
			},
			"columnDefs" :[
			  
			  {
				"targets": 0,
				class : 'text-left',
				// "visible" : false,
			  },
			  {
				"targets": 1,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			  {
				"targets": 2,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			  {
				"targets": 3,
				class : 'text-left',
				"visible" : false,
			  },
			  {
				"targets": 4,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets": 5,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets":6,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets": 7,
				class : 'text-left',
				style : 'width:150px;',
                render : $.fn.dataTable.render.number(',', '.', 2, '')
				//"visible" : false
			  },
              {
				"targets": 8,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets": 9,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets": 10,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets": 11,
				class : 'text-left',
				style : 'width:150px;',
				render : $.fn.dataTable.render.number(',', ''),
				// "visible" : false
			  },
			  {
				"targets": 12,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
              {
				"targets": 13,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			  {
				"targets": 14,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			  {
				"targets": 15,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
              {
				"targets": 16,
				class : 'text-left',
				style : 'width:150px;',
                "render": function ( data, type, value, meta ) {
					if(value[16] == 5 || value[16] == 3){
                    	return '<button type="button" class="btn btn-block btn-success" id="btnConfirm" data-toggle="modal" data-target="#modalKonfirmasiInvoice" onclick="confirmTransfer(\''+value[0]+'\', \''+value[1]+'\', \''+value[2]+'\', \''+value[3]+'\', \''+value[7]+'\', \''+value[16]+'\', \''+value[12]+'\', \''+value[11]+'\', \''+"konfirm"+'\')" >Konfirmasi</button>'
					}else if(value[16] == 1){
						return '<button type="button" class="btn btn-block btn-primary" id="btnLihat" data-toggle="modal" data-target="#modalKonfirmasiInvoice" onclick="getLihatTransfer( \''+value[1]+'\', \''+value[2]+'\', \''+value[16]+'\', \''+"lihat"+'\')" title="Lihat Bukti Transfer">Lihat</button>'
					}else{
						return null;
					}
				},
				//"visible" : false
			  },
			]
		});

		$('#keterangan_tambah').hide();
		$('#nominal_tambah').hide();

		$("#button_tolak").click(function(){
			$("#keterangan_confirm").attr("required", "required");
			$("#keterangan_tambah").slideToggle('fast', callbackKeterangan);
		});

		$("#button_konfirmasi").click(function(){
			$("#nominal_confirm").attr("required", "required");
			$("#nominal_tambah").slideToggle('fast', callbackNominal);
		});
		
	});

	function callbackKeterangan(){

		if($(this).is(":visible")){
			$("#button_konfirmasi").attr("disabled", true);
			$('#status_confirm').val('tolak');
			$("#keterangan_confirm").prop('required', true);
		}else{
			$("#button_konfirmasi").attr("disabled", false);
			$("#keterangan_confirm").removeAttr('required');
		}
	}

	function callbackNominal(){

		if($(this).is(":visible")){
			$("#button_tolak").attr("disabled", true);
			$('#status_confirm').val('konfirm');
			$("#nominal_confirm").prop('required', true);
		}else{
			$("#button_tolak").attr("disabled", false);
			$("#nominal_confirm").removeAttr('required');
		}
	}

	function confirmTransfer(no_invoice_, invoice_id_, brw_id_, pendanaan_id_, nominal_transfer_, image_url_, no_referal_, nominal_transfer_tagihan_, pict_status_){
		let no_invoice = no_invoice_;
        let invoice_id = invoice_id_;
        let brw_id = brw_id_;
        let pendanaan_id = pendanaan_id_;
		let nominal_transfer = nominal_transfer_;
		let image_url = '{{asset("storage/invoice/buktibayar/")}}/'+image_url_;
		let no_referal = no_referal_;
		let pict_status = pict_status_;

		createBuktiPembayaran(invoice_id_);
	}

    function getLihatTransfer(invoice_id_, brw_id_, image_url_, pict_status_)
    {
		let routes_url='';
		let image_url = '{{asset("storage/invoice/buktibayar/")}}/'+image_url_;
		let pict_status = pict_status_;
		
		createBuktiPembayaran(invoice_id_);
	}
	
	$('#button_setuju_save').click(function(e){
		if($('#nominal_confirm').val()== '' ){
			swal({
				title: "Perhatian",
				text: "Nominal Transfer harus diisi",
				icon: "warning",
			}).then(results=>{
				$('#nominal_confirm').focus();
			});
		}else{
			swal({
				title: "Notifikasi",   
				text: "Apakah anda yakin pembayaran ini sudah sesuai?",   
				icon: "success",   
				buttons: true,
				showCancelButton: true,
			}).then(results=>{
				if(results){
					$('#save_konfirmasi_cicilan').trigger('click');
				}else{
					return false;
				}
			});
		}
	});

	$('#button_tolak_save').click(function(e){
		if($('#keterangan_confirm').val()== '' ){
			swal({
				title: "Perhatian",
				text: "Keterangan ditolak harus diisi",
				icon: "warning",
			}).then(results=>{
				$('#keterangan_confirm').focus();
			});
		}else{
			swal({
				title: "Notifikasi",   
				text: "Apakah anda yakin menolak verifikasi cicilan ini?",   
				icon: "warning",   
				buttons: true,
				showCancelButton: true,
			}).then(results=>{
				if(results){
					$('#save_konfirmasi_cicilan').trigger('click');
				}else{
					return false;
				}
			});
		}
	});

	createBuktiPembayaran=(id_invoice)=>{
		if ($.fn.DataTable.isDataTable("#table_list_bukti_bayar")) {
			$('#table_list_bukti_bayar').DataTable().clear().destroy();
		}

		$('#table_list_bukti_bayar tbody').empty();

		$('#table_list_bukti_bayar').DataTable({
			ajax : {
			url : '/admin/borrower/tableGetBuktiBayar/'+id_invoice,
			type : 'get',
			},
			"searchable": false,								   
			"paging":   false,					 
			bFilter: false,
			"bPaginate": false,
			"bInfo": false,
			autoWidth: false,
			"columns" : [
			{"data" : "no"},
			{"data" : "bukti_id"},
			{"data" : "invoice_id"},
			{"data" : "brw_id"},
			{"data" : "no_invoice"},
			{"data" : "tgl_bukti_bayar"},
			{"data" : "ref_no"}, 
			{"data" : "nominal"},
			{"data" : "tgl_konfirmasi"},
			{"data" : "confirmed_by"},
			{"data" : "keterangan"},
			{"data" : "status"},
			{"data" : "pic_pembayaran"},	
			{"data" : "pendanaan_id"}	
			],
			"columnDefs" :[
			{
				"targets": 0,
				class : 'text-left',
				// "visible" : false,
			},
			{
				"targets": 1,
				class : 'text-left',
				"visible" : false,
			},
			{
				"targets": 2,
				class : 'text-left',
				"visible" : false,
			},
			{
				"targets": 3,
				class : 'text-left',
				"visible" : false,
			},
			{
				"targets": 4,
				class : 'text-left',
				// "visible" : false,
			},
			{
				"targets": 5,
				class : 'text-left',
				// "visible" : false,
			},
			{
				"targets": 6,
				class : 'text-left',
				// "visible" : false,
			},
			{
				"targets": 7,
				class : 'text-left',
				render : $.fn.dataTable.render.number(',', '.', 2, ''),
				// "visible" : false,
			},
			{
				"targets": 8,
				class : 'text-left',
				// "visible" : false
			},
			{
				"targets": 9,
				class : 'text-left',
				// "visible" : false
			},
			{
				"targets": 10,
				class : 'text-left',
				// "visible" : false
			},
			{
				"targets": 11,
				class : 'text-left',
				"render" : function(data, type, value, meta){
					if(value["status"]==5){
						return '<button class="btn btn-success" type="button" data-toggle="modal" data-target="#modalConfirmTransfer" id="BtnKonfirmasiBuktiBayar" onclick="konfirmasiData(this)"> Bukti Bayar </button>';
					}
					else{
						let status_button = value["status"]==6 ? 'Ditolak' : 'Diterima';
						let style_button = value["status"]==6 ? 'danger' : 'info';
						return '<button class="btn btn-'+style_button+'" type="button" data-toggle="modal" data-target="#modalLihatTransfer" id="BtnLihatBuktiBayar" onclick="lihatData(this)"> '+status_button+' </button>';
					}
				}
				// "visible" : false
			},
			{
				"targets": 12,
				class : 'text-left',
				"visible" : false
			},
			{
				"targets": 13,
				class : 'text-left',
				"visible" : false
			},
			]
		});

    }

	konfirmasiData=(e)=>{
		let table = $('#table_list_bukti_bayar').DataTable();
 
		$('#table_list_bukti_bayar tbody').on( 'click', 'tr', function () {

			$('#bukti_id_confirm').val('');
			$('#invoice_id_confirm').val('');
			$('#pendanaan_id_confirm').val('');
			$('#brw_id_confirm').val('');
			$('#image_transfer_confirm').val('');
			$('#status_confirm').val('konfirm');

			setTimeout(()=>{
				let bukti_id = table.row( this ).data().bukti_id;
				let pendanaan_id = table.row( this ).data().pendanaan_id;
				let brw_id = table.row( this ).data().brw_id;
				let invoice_id = table.row( this ).data().invoice_id;
				let pic_pembayaran = table.row( this ).data().pic_pembayaran;
				let ref_no = table.row( this ).data().ref_no;
				let image_confirm = '{{asset("storage/invoice/buktibayar/")}}/'+pic_pembayaran;

				$('#bukti_id_confirm').val(bukti_id);
				$('#pendanaan_id_confirm').val(pendanaan_id);
				$('#brw_id_confirm').val(brw_id);
				$('#invoice_id_confirm').val(invoice_id);
				$('#no_referal').html('No Referal Bank : '+ref_no);
				$('#image_transfer_confirm').attr('src',image_confirm);
			},250);	
		} );
    }

	lihatData=(e)=>{
		let table = $('#table_list_bukti_bayar').DataTable();
 
		$('#table_list_bukti_bayar tbody').on( 'click', 'tr', function () {

			$('#bukti_id_lihat').val('');
			$('#brw_id_lihat').val('');
			$('#invoice_id_lihat').val('');
			$('#routes_url').val('');
			$('#image_pembayaran_lihat_tolak').val('');

			setTimeout(()=>{
				let bukti_id = table.row( this ).data().bukti_id;
				let brw_id = table.row( this ).data().brw_id;
				let invoice_id = table.row( this ).data().invoice_id;
				let pic_pembayaran = table.row( this ).data().pic_pembayaran;
				let no_invoice = table.row( this ).data().no_invoice;
				let image_confirm = '{{asset("storage/invoice/buktibayar/")}}/'+pic_pembayaran;

				$('#bukti_id_lihat').val(bukti_id);
				$('#brw_id_lihat').val(brw_id);
				$('#invoice_id_lihat').val(invoice_id);
				$('#routes_url').val(pic_pembayaran);
				$('#image_pembayaran_lihat_tolak').attr('src',image_confirm);
			},250);
		} );
    }



</script>

@endsection