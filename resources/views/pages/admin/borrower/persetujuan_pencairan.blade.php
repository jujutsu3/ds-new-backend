@extends('layouts.admin.master')

@section('title', 'Dashboard Borrower')
	
@section('content')
	
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
			<div class="page-title">
				<h1>Persetujuan Pencairan Dana</h1>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="row">
		<div class="col-md-12">
			@if (session('error'))
				<div class="alert alert-danger col-sm-12">
					{{ session('error') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif (session('success'))
				<div class="alert alert-success col-sm-12">
					{{ session('success') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@elseif(session('updated'))
				<div class="alert alert-success col-sm-12">
					{{ session('updated') }}
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
					</button>
				</div>
			@endif

			<div class="card">
				<div class="card-header">
					<strong class="card-title">Daftar Persetujuan Pencairan Dana</strong>
				</div>
				<div class="card-body">
					<table id="tblPencairanDana" class="table table-striped table-bordered table-responsive-sm">
						<thead>
							<tr>
                                <th>No</th>
                                <th>ID Pencairan</th>
								<th>ID BRW</th>
                                <th>ID Proyek</th>
                                <th>Penerima Pendanaan</th>
								<th>Pendanaan</th>
                                <th>Nominal Pencairan</th>
                                <th>No Akad</th>
                                <th>Tgl Akad</th>
								<th>No Rekening</th>
                                <th>Tgl Permintaan Pencairan</th>
                                <th>Tgl Pencairan</th>
								<th>Dicairkan Oleh</th>
								<th>Status</th>
								<th>Bukti Transfer</th>
								<th>Keterangan</th>
								<th>Aksi</th>
								<th>Nama Pemilik Rekening</th>
								<th>Nama Bank</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- start modal lihat bukti transfer pencairan-->
<div class="modal fade" id="myLihatTransfer" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Unduh Bukti Pencairan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="{{route('admin.unduhBuktiPencairan')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="col-lg-12">
                    <div class="card-body card-block">
                        <div class="form-row">
                          <div class="col-sm-9 col-lg-10 ml-3 ml-sm-0">
                            <input type="hidden" id="brw_id_lihat" name="brw_id_lihat">
                            <input type="hidden" id="pencairan_id_lihat" name="pencairan_id_lihat">
                            <input type="hidden" id="proyek_id_lihat" name="proyek_id_lihat">
                            <input type="hidden" id="routes_url" name="routes_url">
                            <div class="row" id="tambahUpload">

                              <img id='id_image' height="100%" width="100%" >
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
        	</div>
            <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
				<button type="submit" class="btn btn-primary">Unduh</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end of modal lihat dokumen scoring pendanaan-->

<!-- start modal upload bukti transfer pencairan-->
<div class="modal fade" id="myUploadTransfer" tabindex="-1" role="dialog" aria-labelledby="scrollmodalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="scrollmodalLabel">Persetujuan Pencairan Dana</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <form action="{{route('admin.savePersetujuanPencairan')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
				<div class="row">
					
					<div class="col-sm-12 pt-1 pb-3" >
						<label class=" form-control-label" id="judul_pendanaan"></label>
					</div>
					<br>
					<div class="col-sm-6">
						<label>Nomor Akad</label>
					</div>
					<div class="col-sm-6 float-left ">
						<label class=" form-control-label" id="no_akad"></label>
					</div>
					<div class="col-sm-6">
						<label>Nama Penerima Pendanaan</label>
					</div>
					<div class="col-sm-6 float-left ">
						<label class=" form-control-label" id="nama_pendana"></label>
					</div>
					<div class="col-sm-6 ">
						<label>Jumlah Dana</label>
					</div>
					<div class="col-sm-6 float-left ">
						<label class=" form-control-label" id="jumlah_dana"></label>
					</div>
					<div class="col-sm-6 ">
						<label>No Rekening</label>
					</div>
					<div class="col-sm-6 float-left ">
						<label class=" form-control-label" id="no_rekening"></label>
					</div>
					<div class="col-sm-6 ">
						<label>Nama Pemilik Rekening</label>
					</div>
					<div class="col-sm-6 float-left ">
						<label class=" form-control-label" id="nama_pemilik"></label>
					</div>
					<div class="col-sm-6 ">
						<label>Nama Bank</label>
					</div>
					<div class="col-sm-6 float-left ">
						<label class=" form-control-label" id="nama_bank"></label>
					</div>
				</div>
                <div class="col-lg-12">
                    <div class="card-body card-block">
                        <div class="form-row">
                          <div class="col-sm-9 col-lg-10 ml-3 ml-sm-0">
                            <input type="hidden" id="brw_id" name="brw_id">
                            <input type="hidden" id="pencairan_id" name="pencairan_id">
							<input type="hidden" id="proyek_id" name="proyek_id">
							<input type="hidden" id="pendanaan_id" name="pendanaan_id">
                            <input type="hidden" id="pendanaan_nama" name="pendanaan_nama">
                            <input type="hidden" id="nominal_pencairan" name="nominal_pencairan">
                            <input type="hidden" id="tgl_req_pencairan" name="tgl_req_pencairan">
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            <div class="modal-footer">
                <button type="button" id="button_konfirmasi" class="btn btn-success">Setuju</button>
				<button type="button" id = "button_tolak" class="btn btn-outline-danger">Tolak</button>
				<button type="submit" id="save_persetujuan_data" class="invisible"></button>
            </div>
            <div id="keterangan_tambah" >
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="wizard-progress2-nik">Keterangan</label>
                        <input type="hidden" id="status_confirm" name="status_confirm">
                        <input class="form-control allowCharacter" type="text" id="keterangan_confirm" name="keterangan_confirm" placeholder="Isi Keterangan Ditolak..."> 
                    </div>
                </div>
                <div class="col-md-12" >
					<button type="submit" style="float : right; margin-bot:5px" class="btn btn-primary">Simpan</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- end of modal upload dokumen scoring pendanaan-->

<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />

<script src="{{asset('js/sweetalert.js')}}"></script>
<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>
<!--<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>-->
<script src="{{url('admin/assets/js/lib/select2.full.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />
<style>
	.swal-button--confirm {
		background: rgb(25, 150, 25);
	}
</style>

<script type="text/javascript">
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$(document).ready(function(){
		
		var tblPencairanDana = $('#tblPencairanDana').DataTable({
		
			processing: true,
			// serverSide: true,
			"ajax" : {
				url : '/admin/borrower/listPersetujuanPencairan/',
				type : 'get',
			},
			"columnDefs" :[
              {
				"targets": 0,
				class : 'text-left',
				// "visible" : false,
			  },
			  {
				"targets": 1,
				class : 'text-left',
				"visible" : false,
			  },
			  {
				"targets": 2,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			  {
				"targets": 3,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			  {
				"targets": 4,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets": 5,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets": 6,
				class : 'text-left',
				style : 'width:150px;',
                render : $.fn.dataTable.render.number(',', '.', 2, '')
				//"visible" : false
              },
              {
				"targets":7,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
              },
              {
				"targets":8,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets":9,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets":10,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets": 11,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets": 12,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
              {
				"targets": 13,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
              {
				"targets": 14,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false,
                "render": function ( data, type, value, meta ) {
					if(value[12]=='-'){
						return '<button type="button" class="btn btn-block btn-success" id="btnLihatDisabled" data-toggle="modal" data-target="#myLihatTransfer" title="Lihat Bukti Transfer" disabled>Lihat</button>'
					}else{
						return '<button type="button" class="btn btn-block btn-success" id="btnLihat" data-toggle="modal" data-target="#myLihatTransfer" onclick="getLihatTransfer(\''+value[1]+'\', \''+value[2]+'\', \''+value[3]+'\', \''+value[14]+'\')" title="Lihat Bukti Transfer">Lihat</button>'
					}
				}
			  },
			  {
				"targets": 15,
				class : 'text-left',
				style : 'width:150px;',
				//"visible" : false
			  },
			  {
				"targets": 16,
				class : 'text-left',
				style : 'width:150px;',
				"render": function ( data, type, value, meta ) {
				 
                 if(value[13]==1){
                    return '<button type="button" class="btn btn-block btn-success" id="btnScoreUld" data-toggle="modal" data-target="#myUploadTransfer" onclick="getDataBorrower(\''+value[1]+'\',\''+value[2]+'\',\''+value[3]+'\',\''+value[5]+'\',\''+value[6]+'\',\''+value[7]+'\', \''+value[16]+'\', \''+value[17]+'\', \''+value[18]+'\', \''+value[9]+'\', \''+value[4]+'\', \''+value[7]+'\')">Setujui</button>'
                 }else if(value[13]==2){
                    return '<button type="button" class="btn btn-block btn-primary" id="btnScoreUld" data-toggle="modal" data-target="#myLihatTransfer" onclick="getLihatTransfer(\''+value[1]+'\',\''+value[2]+'\',\''+value[3]+'\',\''+value[14]+'\')">Sudah Cair</button>'
                 }else if(value[13]==3){
                    return '<button type="button" class="btn btn-block btn-info" id="btnScoreUld" data-toggle="modal" data-target="#myUploadTransfer" onclick="getDataBorrower(\''+value[1]+'\',\''+value[2]+'\',\''+value[3]+'\',\''+value[5]+'\',\''+value[6]+'\',\''+value[7]+'\', \''+value[16]+'\', \''+value[17]+'\', \''+value[18]+'\', \''+value[9]+'\', \''+value[4]+'\', \''+value[7]+'\')" disabled>Disetujui</button>'
                 }else{
                    return '<button type="button" class="btn btn-block btn-danger" id="btnDisabledScoreUld" data-toggle="modal" data-target="#myUploadTransfer" disabled>Ditolak</button>';
                 }
				}
			  },
			  {
				"targets": 17,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			  {
				"targets": 18,
				class : 'text-left',
				style : 'width:150px;',
				"visible" : false
			  },
			]
		});
		
		$('#tableDataBorrower').on( 'click', 'tr', function () {
			if ( $(this).hasClass('selected') ) {
				$(this).removeClass('selected');
			}
			else {
				tableDataBorrower.$('tr.selected').removeClass('selected');
				$(this).addClass('selected');
				//var id = this.id;
				///console.log($(this));
			}
        });
        
        $('#keterangan_tambah').hide();
		$('#status_confirm').val('setuju');

		$("#button_tolak").click(function(){
			$("#keterangan_confirm").attr("required", "required");
			$("#keterangan_tambah").slideToggle('fast', callbackKeterangan);
		});
    });
    
    function callbackKeterangan(){
        if($(this).is(":visible")){
            $("#button_konfirmasi").attr("disabled", true);
            $('#status_confirm').val('tolak');
            $("#keterangan_confirm").prop('required', true);
        }else{
            $("#button_konfirmasi").attr("disabled", false);
            $('#status_confirm').val('setuju');
            $("#keterangan_confirm").removeAttr('required');
        }
    }

    function getDataBorrower(pencairan_id, brw_id, proyek_id, pendanaan_nama, nominal_pencairan, tgl_req_pencairan, pendanaan_id, nama_pemilik_rekening, nama_bank, no_rekening, nama_pendana, no_akad)
    {
        var pencairan_id = pencairan_id;
        var id_brw = brw_id;
        var proyek_id = proyek_id;
        var pendanaan_nama = pendanaan_nama;
        var nominal_pencairan = nominal_pencairan;
        var tgl_req_pencairan = tgl_req_pencairan;
		var pendanaan_id = pendanaan_id;

        console.log({
            'pencairan_id' : pencairan_id,
            'brw_id' : brw_id,
            'proyek_id' : proyek_id,
            'pendanaan_nama' : pendanaan_nama,
            'nominal_pencairan' : nominal_pencairan,
            'tgl_req_pencairan' : tgl_req_pencairan,
			'pendanaan_id':pendanaan_id
		});
		
		$('#nama_pendana').html(': '+nama_pendana);
		$('#jumlah_dana').html(': Rp.'+parseInt(nominal_pencairan).toLocaleString());
		$('#no_rekening').html(': '+no_rekening);
		$('#nama_pemilik').html(': '+nama_pemilik_rekening);
        $('#nama_bank').html(': '+nama_bank);
        $('#no_akad').html(': '+no_akad);
		$('#judul_pendanaan').html('Permintaan Pencairan Dana untuk '+pendanaan_nama+' dengan detil sebagai berikut :');

        //alert(brw_type);
        $('#brw_id').val(id_brw);
        $('#pencairan_id').val(pencairan_id);
        $('#proyek_id').val(proyek_id);
        $('#pendanaan_nama').val(pendanaan_nama);
        $('#nominal_pencairan').val(nominal_pencairan);
        $('#tgl_req_pencairan').val(tgl_req_pencairan);
		$('#pendanaan_id').val(pendanaan_id);
    }

    function getLihatTransfer(pencairan_id_, brw_id_, proyek_id_, link)
    {

        let pencairan_id = pencairan_id_;
        let id_brw = brw_id_;
        let proyek_id = proyek_id_;
        let url = '{{asset("/storage")}}/'+link+'';

        console.log({
            'pencairan_id' : pencairan_id,
            'brw_id' : brw_id,
            'proyek_id' : proyek_id,
            'url' : url,
        })

        //alert(brw_type);
        $('#brw_id_lihat').val(id_brw);
        $('#pencairan_id_lihat').val(pencairan_id);
        $('#proyek_id_lihat').val(proyek_id);
        $('#routes_url').val(link);
        $('#id_image').attr('src', url);
	}
	
	$('#button_konfirmasi').click(function(e){
		swal({
			title: "Notifikasi",   
			text: "Apakah anda yakin ingin mencairkan dana pendanaan ini?",   
			icon: "success",   
			buttons: true,
			showCancelButton: true,
		}).then(results=>{
			if(results){
				$('#save_persetujuan_data').trigger('click');
			}else{
				return false;
			}
		});
	});

</script>

@endsection