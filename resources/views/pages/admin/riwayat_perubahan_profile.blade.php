@extends('layouts.admin.master')

@section('title', 'Daftar Permintaan Perubahan Profile Pendana')

@push('add-more-style')
<style>
    input[type=checkbox] {
        top: 1rem;
        width: 1rem;
        height: 1rem;
    }

    .line {
        display: flex;
        flex-direction: row;
    }

    .line:after {
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #000;
        margin: auto;
    }

    .btn-danaSyariah {
        background-color: #16793E;
    }

    .card-header-pengurus[aria-expanded=false] .fa-angle-up {
        display: none;
    }

    .card-header-pengurus[aria-expanded=true] .fa-angle-down {
        display: none;
    }
</style>
@endpush

@section('content')

<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Daftar Permintaan Perubahan Profile Pengguna</h1>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <section id="layout-table-permintaan-perubahan">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table id="list-table-permintaan-perubahan" class="table table-striped table-bordered">
                                <thead>
                                    <tr class="text-center">
                                        <th class="align-middle">Request ID</th>
                                        <th class="align-middle">Client ID</th>
                                        <th class="align-middle">No</th>
                                        <th class="align-middle">Nama Pengguna</th>
                                        <th class="align-middle">Akun Pengguna</th>
                                        <th class="align-middle">Jenis Pengguna</th>
                                        <th class="align-middle">Tanggal Permintaan</th>
                                        <th class="align-middle">Oleh CSO : (Nama)</th>
                                        <th class="align-middle">Tanggal Perbaikan</th>
                                        <th class="align-middle">Oleh Finance : (Nama)</th>
                                        <th class="align-middle" style="width: 9%">Status</th>
                                        <th class="align-middle" style="width: 9%">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<button id="modal-click" type="button" class="btn btn-primary d-none" data-toggle="modal"
    data-target="#modal-detail-profile"></button>

<div id="modal-detail-profile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="detailProfile"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Permintaan Perubahan Data Pengguna</h5>
                <button type="button" id="modal-close" class="close" data-backdrop="false" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="blank_data" class="d-none">
                    <p class="font-weight-normal text-center align-middle text-black">Tidak ada data yang berubah</p>
                </div>

                <div id="layout-investor-individu" class="d-none">
                    <form id="form-investor-individu">
                        @csrf
                        <input type="hidden" name="hist_investor_id">
                        <input type="hidden" name="investor_id">
                        <input type="hidden" name="status">
                        <input type="hidden" name="user_type">
                        <input type="hidden" name="client_type">
                        @include('pages.admin.riwayat_perubahan_profile_investor_individu')
                    </form>
                </div>
                <div id="layout-investor-bdn-hukum" class="d-none">
                    <form id="form-investor-badan-hukum">
                        @csrf
                        <input type="hidden" name="hist_investor_id">
                        <input type="hidden" name="investor_id">
                        <input type="hidden" name="status">
                        <input type="hidden" name="user_type">
                        <input type="hidden" name="client_type">
                        <input type="hidden" name="total_pengurus">
                        @include('pages.admin.riwayat_perubahan_profile_investor_badan_hukum')
                    </form>
                </div>
            </div>

            <div id="modal-footer" class="modal-footer">
                {{-- @if (Auth::user()->role == '4') --}}
                <button type="button" id="btn-reject" class="btn btn-secondary">Tolak</button>
                <button type="button" id="btn-accept" class="btn btn-primary">Terima</button>
                {{-- @else --}}
                <button type="button" id="btn-close" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                {{-- @endif --}}
            </div>
        </div>

    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('admin/assets/js/lib/data-table/datetime.js') }}"></script>
<script src="{{ asset('admin/assets/js/lib/data-table/moment.min.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let list_table_permintaan_perubahan = ''
    let user_type = ''
    let client_type = ''
    let is_pengurus_exist = 0

    $(document).ready(function(){
        loadData()

        $('#btn-accept').click(function() {
            $('[name="status"]').val('2') //status diterima
            $('[name="client_type"]').val(client_type)
            $('[name="user_type"]').val(user_type)

            if (user_type == '1') { // Submit Individu
                $('#form-investor-individu').submit()
            } else { // Submit Badan hukum
                $('#form-investor-badan-hukum').submit()
            }
        }) 
        $('#btn-reject').click(function() {
            $('[name="status"]').val('3') //status ditolak
            $('[name="client_type"]').val(client_type)
            $('[name="user_type"]').val(user_type)

            if (user_type == '1') { // Submit Individu
                $('#form-investor-individu').submit()
            } else { // Submit Badan hukum
                $('#form-investor-badan-hukum').submit()
            }
        })
        
        $('#form-investor-individu').submit(function(e) {
            e.preventDefault()
            saveDetailProfile('#form-investor-individu')
        })

        $('#form-investor-badan-hukum').submit(function(e){
            e.preventDefault()
            saveDetailProfile('#form-investor-badan-hukum')
        })

        $('body').on('show.bs.modal', function() {
            $('.modal-open').css('padding-right', '0px');
        });
        
    })

    // Rupiah Currency Format
    const formatRupiah = (angka, prefix) => {
        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
    }

    const loadData = () => {
        list_table_permintaan_perubahan = $('#list-table-permintaan-perubahan').DataTable({
            // dom: '<f<t>ip>',
            searching: true,
            processing: true,
            serverSide: true,
            paging: true,
            info: true,
            lengthChange: false,
            pageLength: 5,
            order: [[ 0, "desc" ]],
            ajax: {
                url: '{{ route("get-json-list-perubahan-profile") }}'
            },
             columnDefs:[
                {   
                    "targets": [0, 1],
                    "visible": false,
                    "searchable": false
                },
                {   
                    "targets": "_all",
                    "className": 'align-middle text-center',
                },
            ],
            columns: [
                {
                    data: 'request_id',
                    name: 'request_id'
                },
                {
                    data: 'client_id',
                    name: 'client_id'
                },
                {
                    data: null,
                    orderable:false,
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'client_name',
                    name: 'client_name'
                },
                {
                    data: 'client_account',
                    name: 'client_account'
                },
                {
                    data: null,
                    orderable:false,
                    name: 'client_type',
                    render: function (data, type, row, meta) {
                        let client_type = data.client_type
                        let user_type   = data.user_type

                        if (client_type == '1' && user_type == '1') {
                            return 'Pendana Individu'
                        } else if ((client_type == '1' && user_type == '2')) {
                            return 'Pendana Badan Hukum'
                        } else if (client_type == '2' && user_type == '1') {
                            return 'Penerima Pendanaan Individu'
                        }else if (client_type == '2' && user_type == '2') {
                            return 'Penerima Pendanaan Badan Hukum'
                        } else {
                            return '-'
                        }
                    }
                },
                {
                    data: 'edit_date',
                    name: 'edit_date'
                },
                {
                    data: 'edit_by',
                    name: 'edit_by'
                },
                {
                    data: 'approve_date',
                    name: 'approve_date'
                },
                {
                    data: 'approve_by',
                    name: 'approve_by'
                },
                {
                    data: 'badge_status', name: 'badge_status', orderable:true, serachable:false
                },
                {
                    data: 'btn_detail', name: 'btn_detail', orderable:true, serachable:false
                },
            ]


        })

    }

    const getJsonDetailProfile = (request_id) => {
        let route = "{{ route('get-json-detail-profile', ['request_id' => 'request_id_val']) }}"
        route = route.replace('request_id_val', request_id)
        console.log(route)
        $.ajax({
            url: route,
            method: 'GET',
            dataType: 'json',
            data: request_id,
            beforeSend: function() {
                Swal.fire({
                    html: '<h5>Mengambil Data ...</h5>',
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    },
                    allowOutsideClick: () => !Swal.isLoading()
                })
            },
            success: function(response) {
                Swal.close()
                showDetailProfile(response.data)
            },
            error: function(response) {
                Swal.close()
                console.log('error', response)
            }
        })
    }

    const showDetailProfile = (data) => {
        
        user_type                               = data.user_type
        client_type                             = data.client_type
        let user_roles                          = `{{ Auth::user()->role }}`
        let client_id                           = data.client_id
        let hist_investor_id                    = data.hist_investor_id
        let status_data                         = data.status_data

        let data_investor                       = data.data_investor
        let data_riwayat_investor               = data.data_riwayat_investor
        let data_riwayat_investor_individu      = data.data_riwayat_investor_individu
        let data_riwayat_investor_bdn_hukum     = data.data_riwayat_investor_bdn_hukum

        let data_pengurus_investor              = data.data_pengurus_investor
        let data_riwayat_pengurus_investor      = data.data_riwayat_pengurus_investor
        let total_pengurus                      = data.total_pengurus

        let data_ahli_waris                     = data.data_ahli_waris
        let data_riwayat_ahli_waris             = data.data_riwayat_ahli_waris

        let data_nomor_va                       = data.data_nomor_va
        let data_riwayat_nomor_va               = data.data_riwayat_nomor_va
        
        $('[name="total_pengurus"]').val(total_pengurus)
        // START: Status Data
        if (status_data == 1 && user_roles == 4) {
            $('#btn-reject').removeClass('d-none')
            $('#btn-accept').removeClass('d-none')

            $('#btn-close').addClass('d-none')
        } else {
            $('#btn-close').removeClass('d-none')

            $('#btn-reject').addClass('d-none')
            $('#btn-accept').addClass('d-none')
        }
        // END: Status Data

        // START: Layout Title Investor Individu
        let total_informasi_akun                = 0
        let total_informasi_pribadi             = 0
        let total_informasi_pekerjaan           = 0
        let total_informasi_alamat_sesuai_ktp   = 0
        let total_foto_pendana                  = 0
        let total_informasi_rekening            = 0
        let total_informasi_ahli_waris          = 0
        let total_informasi_referal             = 0
        // END: Layout Title Investor Individu

        // START: Layout Title Investor Badan Hukum
        let total_informasi_bdn_hukum                       = 0
        let total_informasi_alamat_sesuai_ktp_bdn_hukum     = 0
        let total_informasi_lain_lain_bdn_hukum             = 0
        // END: Layout Title Investor Badan Hukum
        
        // Investor
        if (client_type == '1') {

            $('.form-control').prop('disabled', true)
            $('input[type=search]').prop('disabled', false)

            // START: Informasi Akun
            $('[name="hist_investor_id"]').val(hist_investor_id)
            $('[name="investor_id"]').val(client_id)

            let data_status = data_investor.status ? data_investor.status : ''
            let data_riwayat_status = data_riwayat_investor.status ? data_riwayat_investor.status : ''
            if (data_status != data_riwayat_status) {
                $('[name="f_status_akun"]').removeClass('d-none')
                $('[name="b_status_akun"]').val(data_status)
                // if (data_investor.keterangan) {
                //     $('[name="b_label_status_akun"]').text('Ket: '+data_investor.keterangan)
                // }
                $('[name="a_status_akun"]').val(data_riwayat_status)
                if(data_riwayat_investor.keterangan) { 
                    $('[name="a_label_status_akun"]').text(data_riwayat_investor.keterangan)
                }
                
                total_informasi_akun += 1
            } else {
                $('[name="f_status_akun"]').addClass('d-none')
            }

            let data_account_name = data_investor.account_name ? data_investor.account_name : ''
            let data_riwayat_account_name = data_riwayat_investor.account_name ? data_riwayat_investor.account_name : ''
            if (data_account_name != data_riwayat_account_name) {
                $('[name="f_username"]').removeClass('d-none')
                $('[name="b_username"]').val(data_account_name)
                $('[name="a_username"]').val(data_riwayat_account_name)

                total_informasi_akun += 1
            } else {
                $('[name="f_username"]').addClass('d-none')
            }

            let data_email = data_investor.email ? data_investor.email : ''
            let data_riwayat_email = data_riwayat_investor.email ? data_riwayat_investor.email : ''
            if (data_email != data_riwayat_email) {
                $('[name="f_email"]').removeClass('d-none')
                $('[name="b_email"]').val(data_email)
                $('[name="a_email"]').val(data_riwayat_email)

                total_informasi_akun += 1
            } else {
                $('[name="f_email"]').addClass('d-none')
            }

            let data_ref_number = data_investor.ref_number ? data_investor.ref_number : ''
            let data_riwayat_ref_number = data_riwayat_investor.ref_number ? data_riwayat_investor.ref_number : ''
            if (data_ref_number != data_riwayat_ref_number) {
                $('[name="f_kode_referal"]').removeClass('d-none')
                $('[name="b_kode_referal"]').val(data_ref_number)
                $('[name="a_kode_referal"]').val(data_riwayat_ref_number)

                total_informasi_referal += 1
            } else {
                $('[name="f_kode_referal"]').addClass('d-none')
            }
            // END: Informasi Akun

            // START: Informasi Rekening
            let data_va_bni = ''
            let data_va_bsi = ''
            let data_va_cimb = ''
            
            let data_riwayat_va_bni = ''
            let data_riwayat_va_bsi = ''
            let data_riwayat_va_cimb = ''

            data_nomor_va.forEach(element => {
                if (element.kode_bank == '009') {
                    data_va_bni =  element.va_bank ? element.va_bank : ''
                }
                if (element.kode_bank == '022') {
                    data_va_cimb =  element.va_bank ? element.va_bank : ''
                }
                if (element.kode_bank == '451') {
                    data_va_bsi =  element.va_bank ? element.va_bank : ''
                }
            })

            data_riwayat_nomor_va.forEach(element => {
                if (element.kode_bank == '009') {
                    data_riwayat_va_bni =  element.va_bank ? element.va_bank : ''
                }
                if (element.kode_bank == '022') {
                    data_riwayat_va_cimb =  element.va_bank ? element.va_bank : ''
                }
                if (element.kode_bank == '451') {
                    data_riwayat_va_bsi =  element.va_bank ? element.va_bank : ''
                }
            })

            console.log(data_va_bsi, data_riwayat_va_bsi)
            if (data_va_bni != data_riwayat_va_bni) {
                $('[name="f_va_number"]').removeClass('d-none')
                $('[name="b_va_number"]').val(data_va_bni)
                $('[name="a_va_number"]').val(data_riwayat_va_bni)

                total_informasi_rekening += 1
            } else {
                $('[name="f_va_number"]').addClass('d-none')
            }

            if (data_va_bsi != data_riwayat_va_bsi) {
                $('[name="f_va_bsi"]').removeClass('d-none')
                $('[name="b_va_bsi"]').val(data_va_bsi)
                $('[name="a_va_bsi"]').val(data_riwayat_va_bsi)

                total_informasi_rekening += 1
            } else {
                $('[name="f_va_bsi"]').addClass('d-none')
            }

            if (data_va_cimb != data_riwayat_va_cimb) {
                $('[name="f_va_cimbs"]').removeClass('d-none')
                $('[name="b_va_cimbs"]').val(data_va_cimb)
                $('[name="a_va_cimbs"]').val(data_riwayat_va_cimb)

                total_informasi_rekening += 1
            } else {
                $('[name="f_va_cimbs"]').addClass('d-none')
            }
            // END: Informasi Rekening

            //user_type 1 = Individu
            let data_no_rekening = data_investor.rekening ? data_investor.rekening : ''
            let no_rekening = user_type == '1' ? (data_riwayat_investor_individu.rekening ? data_riwayat_investor_individu.rekening : '') : (data_riwayat_investor_bdn_hukum.rekening ? data_riwayat_investor_bdn_hukum.rekening : '')
            if (data_no_rekening != no_rekening) {
                $('[name="f_rekening"]').removeClass('d-none')
                $('[name="b_rekening"]').val(data_no_rekening)
                $('[name="a_rekening"]').val(no_rekening)

                total_informasi_rekening += 1
            } else {
                $('[name="f_rekening"]').addClass('d-none')
            }

            let data_nama_pemilik_rek = data_investor.nama_pemilik_rek ? data_investor.nama_pemilik_rek : ''
            let nama_pemilik_rek = user_type == '1' ? (data_riwayat_investor_individu.nama_pemilik_rek ? data_riwayat_investor_individu.nama_pemilik_rek : '') : (data_riwayat_investor_bdn_hukum.nama_pemilik_rek ? data_riwayat_investor_bdn_hukum.nama_pemilik_rek : '')
            if (data_nama_pemilik_rek != nama_pemilik_rek) {
                $('[name="f_nama_pemilik_rek"]').removeClass('d-none')
                $('[name="b_nama_pemilik_rek"]').val(data_nama_pemilik_rek)
                $('[name="a_nama_pemilik_rek"]').val(nama_pemilik_rek)

                total_informasi_rekening += 1
            } else {
                $('[name="f_nama_pemilik_rek"]').addClass('d-none')
            }

            let data_bank_investor = data_investor.bank_investor ? data_investor.bank_investor : ''
            let bank_investor = user_type == '1' ? (data_riwayat_investor_individu.bank_investor ? data_riwayat_investor_individu.bank_investor : '') : (data_riwayat_investor_bdn_hukum.bank_investor ? data_riwayat_investor_bdn_hukum.bank_investor : '')
            if (data_bank_investor != bank_investor) {
                $('[name="f_bank"]').removeClass('d-none')
                $('[name="b_bank"]').val(data_bank_investor)
                $('[name="a_bank"]').val(bank_investor)

                total_informasi_rekening += 1
            } else {
                $('[name="f_bank"]').addClass('d-none')
            }
            // END: Informasi Rekening
            
            // Layout Title 
            if (total_informasi_akun > 0) {
                $('.layout_informasi_akun').removeClass('d-none')
            } else {
                $('.layout_informasi_akun').addClass('d-none')
            }
            if (total_informasi_referal > 0) {
                $('.layout_informasi_referal').removeClass('d-none')
            } else {
                $('.layout_informasi_referal').addClass('d-none')
            }

            if (user_type == '1') { // Individu
                
                $('#layout-investor-individu').removeClass('d-none')
                $('#layout-investor-bdn-hukum').addClass('d-none')

                // START: Informasi Pribadi
                let data_nama_investor = data_investor.nama_investor ? data_investor.nama_investor : ''
                let data_riwayat_nama_investor = data_riwayat_investor_individu.nama_investor ? data_riwayat_investor_individu.nama_investor : ''
                if (data_nama_investor != data_riwayat_nama_investor) {
                    $('[name="f_nama"]').removeClass('d-none')
                    $('[name="b_nama"]').val(data_nama_investor)
                    $('[name="a_nama"]').val(data_riwayat_nama_investor)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_nama"]').addClass('d-none')
                }

                let data_jenis_kelamin          = data_investor.jenis_kelamin_investor ? data_investor.jenis_kelamin_investor : ''
                let data_riwayat_jenis_kelamin  = data_riwayat_investor_individu.jenis_kelamin_investor ? data_riwayat_investor_individu.jenis_kelamin_investor : ''
                if (data_jenis_kelamin != data_riwayat_jenis_kelamin) {
                    $('[name="f_jenis_kelamin"]').removeClass('d-none')
                    $('[name="b_jenis_kelamin"]').val(data_jenis_kelamin)
                    $('[name="a_jenis_kelamin"]').val(data_riwayat_jenis_kelamin)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_jenis_kelamin"]').addClass('d-none')
                }

                let data_tgl_lahir = data_investor.tgl_lahir_investor && data_investor.tgl_lahir_investor != '0000-00-00' && data_investor.tgl_lahir_investor != '00-00-0000' ? data_investor.tgl_lahir_investor.split("-") : '';
                let tgl_lahir =  data_tgl_lahir ? ((data_tgl_lahir[0]).length == 1 ? '0'+data_tgl_lahir[0] : data_tgl_lahir[0]) : ''
                let bln_lahir =  data_tgl_lahir ? ((data_tgl_lahir[1]).length == 1 ? '0'+data_tgl_lahir[1] : data_tgl_lahir[1]) : ''
                let thn_lahir =  data_tgl_lahir ? ((data_tgl_lahir[2]).length == 1 ? '0'+data_tgl_lahir[2] : data_tgl_lahir[2]) : ''
                let new_format = data_tgl_lahir ? thn_lahir+'-'+bln_lahir+'-'+tgl_lahir : ''
                // console.log(new_format)
                // console.log(data_riwayat_investor_individu.tgl_lahir_investor)

                let data_riwayat_tgl_lahir_investor = data_riwayat_investor_individu.tgl_lahir_investor && data_riwayat_investor_individu.tgl_lahir_investor != '0000-00-00' ? data_riwayat_investor_individu.tgl_lahir_investor : ''
                if (new_format != data_riwayat_tgl_lahir_investor) {
                    
                    $('[name="f_tanggal_lahir"]').removeClass('d-none')
                    $('[name="b_tanggal_lahir"]').val(new_format)
                    $('[name="a_tanggal_lahir"]').val(data_riwayat_tgl_lahir_investor)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_tanggal_lahir"]').addClass('d-none')
                }

                let data_tempat_lahir_investor = data_investor.tempat_lahir_investor ? data_investor.tempat_lahir_investor : ''
                let data_riwayat_tempat_lahir_investor = data_riwayat_investor_individu.tempat_lahir_investor ? data_riwayat_investor_individu.tempat_lahir_investor : ''
                if (data_tempat_lahir_investor != data_riwayat_tempat_lahir_investor) {
                    $('[name="f_tempat_lahir"]').removeClass('d-none')
                    $('[name="b_tempat_lahir"]').val(data_tempat_lahir_investor)
                    $('[name="a_tempat_lahir"]').val(data_riwayat_tempat_lahir_investor)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_tempat_lahir"]').addClass('d-none')
                }

                let data_status_kawin_investor = data_investor.status_kawin_investor ? data_investor.status_kawin_investor : ''
                let data_riwayat_status_kawin_investor = data_riwayat_investor_individu.status_kawin_investor ? data_riwayat_investor_individu.status_kawin_investor : ''
                if (data_status_kawin_investor != data_riwayat_status_kawin_investor) {
                    $('[name="f_status_kawin"]').removeClass('d-none')
                    $('[name="b_status_kawin"]').val(data_status_kawin_investor).attr('selected', true)
                    $('[name="a_status_kawin"]').val(data_riwayat_status_kawin_investor).attr('selected', true)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_status_kawin"]').addClass('d-none')
                }
                
                let data_agama = data_investor.agama_investor == 0 ? null : data_investor.agama_investor
                let data_riwayat_agama = data_riwayat_investor_individu.agama_investor == 0 ? null : data_riwayat_investor_individu.agama_investor
                let a_agama = data_riwayat_investor_individu.agama_investor == 0 ? null : data_riwayat_investor_individu.agama_investor
                if (data_agama != data_riwayat_agama) {
                    $('[name="f_agama"]').removeClass('d-none')
                    $('[name="a_agama"]').val(data_riwayat_agama).attr('selected', true)
                    $('[name="b_agama"]').val(data_agama).attr('selected', true)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_agama"]').addClass('d-none')
                }

                // Jenis Identitas
                let data_jenis_identitas = data_investor.jenis_identitas ? data_investor.jenis_identitas : ''
                let data_riwayat_jenis_identitas = data_riwayat_investor_individu.jenis_identitas ? data_riwayat_investor_individu.jenis_identitas : ''
                if (data_riwayat_jenis_identitas != data_jenis_identitas) {
                    $('[name="f_no_ktp"]').addClass('d-none')
                    $('[name="f_no_passport"]').addClass('d-none')
                    
                    if (data_jenis_identitas == 1) {
                        $('#b_label_jenis_identitas').text('No KTP')
                        $('#a_label_jenis_identitas').text('No Passpor')

                        $('[name="f_jenis_identitas"]').removeClass('d-none')
                        $('[name="b_jenis_identitas"]').val(data_investor.no_ktp_investor)
                        $('[name="a_jenis_identitas"]').val(data_riwayat_investor_individu.no_passpor_investor)   
                    } else {
                        $('#b_label_jenis_identitas').text('No Passpor')
                        $('#a_label_jenis_identitas').text('No KTP')

                        $('[name="f_jenis_identitas"]').removeClass('d-none')
                        $('[name="b_jenis_identitas"]').val(data_investor.no_passpor_investor)
                        $('[name="a_jenis_identitas"]').val(data_riwayat_investor_individu.no_ktp_investor)
                    }
                    
                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_jenis_identitas"]').addClass('d-none')

                    let riwayat_no_ktp  = data_riwayat_investor_individu.no_ktp_investor ? data_riwayat_investor_individu.no_ktp_investor : ''
                    let data_no_ktp     = data_investor.no_ktp_investor ? data_investor.no_ktp_investor : ''
                    if (data_no_ktp != riwayat_no_ktp) {
                        $('[name="f_no_ktp"]').removeClass('d-none')
                        $('[name="b_no_ktp"]').val(data_no_ktp)
                        $('[name="a_no_ktp"]').val(riwayat_no_ktp)

                        total_informasi_pribadi += 1
                    } else {
                        $('[name="f_no_ktp"]').addClass('d-none')
                    }

                    let riwayat_no_passpor = data_riwayat_investor_individu.no_passpor_investor ? data_riwayat_investor_individu.no_passpor_investor : ''
                    let data_no_passpor    = data_investor.no_passpor_investor ? data_investor.no_passpor_investor : ''
                    if (data_no_passpor != riwayat_no_passpor) {
                        $('[name="f_no_passport"]').removeClass('d-none')
                        $('[name="b_no_passport"]').val(data_no_passpor)
                        $('[name="a_no_passport"]').val(riwayat_no_passpor)

                        total_informasi_pribadi += 1
                    } else {
                        $('[name="f_no_passport"]').addClass('d-none')
                    }
                }

                let data_warganegara = data_investor.warganegara ? data_investor.warganegara : ''
                let data_riwayat_warganegara = data_riwayat_investor_individu.warganegara ? data_riwayat_investor_individu.warganegara : ''
                if (data_warganegara != data_riwayat_warganegara) {
                    $('[name="f_warga_negara"]').removeClass('d-none')
                    $('[name="b_warga_negara"]').val(data_warganegara).attr('selected', true)
                    $('[name="a_warga_negara"]').val(data_riwayat_warganegara).attr('selected', true)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_warga_negara"]').addClass('d-none')
                }

                let data_npwp_investor = data_investor.no_npwp_investor ? data_investor.no_npwp_investor != 0 ? data_investor.no_npwp_investor : '' : ''
                let riwayat_npwp_investor = data_riwayat_investor_individu.no_npwp_investor ? data_riwayat_investor_individu.no_npwp_investor != 0 ? data_riwayat_investor_individu.no_npwp_investor : '' : ''
                if (data_npwp_investor != riwayat_npwp_investor) {
                    $('[name="f_no_npwp"]').removeClass('d-none')
                    $('[name="b_no_npwp"]').val(data_npwp_investor)
                    $('[name="a_no_npwp"]').val(riwayat_npwp_investor)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_no_npwp"]').addClass('d-none')
                }

                let data_phone_investor = data_investor.phone_investor ? data_investor.phone_investor : ''
                let data_riwayat_phone_investor = data_riwayat_investor_individu.phone_investor ? data_riwayat_investor_individu.phone_investor : ''
                if (data_phone_investor != data_riwayat_phone_investor) {
                    $('[name="f_no_telp"]').removeClass('d-none')
                    $('[name="b_no_telp"]').val(data_phone_investor)
                    $('[name="a_no_telp"]').val(data_riwayat_phone_investor)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_no_telp"]').addClass('d-none')
                }

                let data_nama_ibu_kandung = data_investor.nama_ibu_kandung ? data_investor.nama_ibu_kandung : ''
                let data_riwayat_nama_ibu_kandung = data_riwayat_investor_individu.nama_ibu_kandung ? data_riwayat_investor_individu.nama_ibu_kandung : ''
                if (data_nama_ibu_kandung != data_riwayat_nama_ibu_kandung) {
                    $('[name="f_nama_ibu_kandung"]').removeClass('d-none')
                    $('[name="b_nama_ibu_kandung"]').val(data_nama_ibu_kandung)
                    $('[name="a_nama_ibu_kandung"]').val(data_riwayat_nama_ibu_kandung)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_nama_ibu_kandung"]').addClass('d-none')
                }

                let data_pendidikan_investor = data_investor.pendidikan_investor ? data_investor.pendidikan_investor : ''
                let data_riwayat_pendidikan_investor = data_riwayat_investor_individu.pendidikan_investor ? data_riwayat_investor_individu.pendidikan_investor : ''
                if (data_pendidikan_investor != data_riwayat_pendidikan_investor) {
                    $('[name="f_pendidikan"]').removeClass('d-none')
                    $('[name="b_pendidikan"]').val(data_pendidikan_investor).attr('selected', true)
                    $('[name="a_pendidikan"]').val(data_riwayat_pendidikan_investor).attr('selected', true)

                    total_informasi_pribadi += 1
                } else {
                    $('[name="f_pendidikan"]').addClass('d-none')
                }
                // END: Informasi Pribadi
                
                // START: Informasi Pekerjaan
                let data_sumber_dana = data_investor.sumber_dana ? data_investor.sumber_dana : ''
                let data_riwayat_sumber_dana = data_riwayat_investor_individu.sumber_dana ? data_riwayat_investor_individu.sumber_dana : ''
                if (data_sumber_dana != data_riwayat_sumber_dana) {
                    $('[name="f_sumber_dana"]').removeClass('d-none')
                    $('[name="b_sumber_dana"]').val(data_sumber_dana)
                    $('[name="a_sumber_dana"]').val(data_riwayat_sumber_dana)

                    total_informasi_pekerjaan += 1
                } else {
                    $('[name="f_sumber_dana"]').addClass('d-none')
                }

                let data_pekerjaan_investor = data_investor.pekerjaan_investor ? data_investor.pekerjaan_investor : ''
                let data_riwayat_pekerjaan_investor = data_riwayat_investor_individu.pekerjaan_investor ? data_riwayat_investor_individu.pekerjaan_investor : ''
                if (data_pekerjaan_investor != data_riwayat_pekerjaan_investor) {
                    $('[name="f_pekerjaan"]').removeClass('d-none')
                    $('[name="b_pekerjaan"]').val(data_pekerjaan_investor)
                    $('[name="a_pekerjaan"]').val(data_riwayat_pekerjaan_investor)

                    total_informasi_pekerjaan += 1
                } else {
                    $('[name="f_pekerjaan"]').addClass('d-none')
                }

                let data_bidang_pekerjaan = data_investor.bidang_pekerjaan ? data_investor.bidang_pekerjaan : ''
                let data_riwayat_bidang_pekerjaan = data_riwayat_investor_individu.bidang_pekerjaan ? data_riwayat_investor_individu.bidang_pekerjaan : ''
                if (data_bidang_pekerjaan != data_riwayat_bidang_pekerjaan) {
                    $('[name="f_bidang_pekerjaan"]').removeClass('d-none')
                    $('[name="b_bidang_pekerjaan"]').val(data_bidang_pekerjaan)
                    $('[name="a_bidang_pekerjaan"]').val(data_riwayat_bidang_pekerjaan)

                    total_informasi_pekerjaan += 1
                } else {
                    $('[name="f_bidang_pekerjaan"]').addClass('d-none')
                }

                let data_pendapatan_investor = data_investor.pendapatan_investor ? data_investor.pendapatan_investor : ''
                let data_riwayat_pendapatan_investor = data_riwayat_investor_individu.pendapatan_investor ? data_riwayat_investor_individu.pendapatan_investor : ''
                if (data_pendapatan_investor != data_riwayat_pendapatan_investor) {
                    $('[name="f_pendapatan"]').removeClass('d-none')
                    $('[name="b_pendapatan"]').val(data_pendapatan_investor)
                    $('[name="a_pendapatan"]').val(data_riwayat_pendapatan_investor)

                    total_informasi_pekerjaan += 1
                } else {
                    $('[name="f_pendapatan"]').addClass('d-none')
                }

                let data_pengalaman_kerja = data_investor.pengalaman_investor == 0 ? null : data_investor.pengalaman_investor
                let data_riwayat_pengalaman_kerja = data_riwayat_investor_individu.pengalaman_investor == 0 ? null : data_riwayat_investor_individu.pengalaman_investor
                if (data_pengalaman_kerja != data_riwayat_pengalaman_kerja) {
                    $('[name="f_pengalaman_kerja"]').removeClass('d-none')
                    $('[name="b_pengalaman_kerja"]').val(data_pengalaman_kerja).attr('selected', true)
                    $('[name="a_pengalaman_kerja"]').val(data_riwayat_pengalaman_kerja).attr('selected', true)

                    total_informasi_pekerjaan += 1
                } else {
                    $('[name="f_pengalaman_kerja"]').addClass('d-none')
                }

                let data_bidang_online = data_investor.online_investor == 0 ? null : data_investor.online_investor
                let data_riwayat_bidang_online = data_riwayat_investor_individu.online_investor == 0 ? null : data_riwayat_investor_individu.online_investor
                if (data_bidang_online != data_riwayat_bidang_online) {
                    $('[name="f_bidang_online"]').removeClass('d-none')
                    $('[name="b_bidang_online"]').val(data_bidang_online).attr('selected', true)
                    $('[name="a_bidang_online"]').val(data_riwayat_bidang_online).attr('selected', true)

                    total_informasi_pekerjaan += 1
                } else {
                    $('[name="f_bidang_online"]').addClass('d-none')
                }
                // END: Informasi Pekerjaan

                // START: Informasi Alamat Sesuai KTP
                let data_domisili_negara = data_investor.domisili_negara ? data_investor.domisili_negara : ''
                let data_riwayat_domisili_negara = data_riwayat_investor_individu.domisili_negara ? data_riwayat_investor_individu.domisili_negara : ''
                if (data_domisili_negara != data_riwayat_domisili_negara) {
                    $('[name="f_domisili_negara"]').removeClass('d-none')
                    $('[name="b_domisili_negara"]').val(data_domisili_negara)
                    $('[name="a_domisili_negara"]').val(data_riwayat_domisili_negara)

                    total_informasi_alamat_sesuai_ktp += 1
                } else {
                    $('[name="f_domisili_negara"]').addClass('d-none')
                }

                let data_alamat_investor = data_investor.alamat_investor ? data_investor.alamat_investor : ''
                let data_riwayat_alamat_investor = data_riwayat_investor_individu.alamat_investor ? data_riwayat_investor_individu.alamat_investor : ''
                if (data_alamat_investor != data_riwayat_alamat_investor) {
                    $('[name="f_alamat"]').removeClass('d-none')
                    $('[name="b_alamat"]').val(data_alamat_investor)
                    $('[name="a_alamat"]').val(data_riwayat_alamat_investor)

                    total_informasi_alamat_sesuai_ktp += 1
                } else {
                    $('[name="f_alamat"]').addClass('d-none')
                }

                let data_provinsi_investor = data_investor.provinsi_investor ? data_investor.provinsi_investor : ''
                let data_riwayat_provinsi_investor = data_riwayat_investor_individu.provinsi_investor ? data_riwayat_investor_individu.provinsi_investor : ''
                if (data_provinsi_investor != data_riwayat_provinsi_investor) {
                    $('[name="f_provinsi"]').removeClass('d-none')
                    $('[name="b_provinsi"]').val(data_provinsi_investor)
                    $('[name="a_provinsi"]').val(data_riwayat_provinsi_investor)

                    total_informasi_alamat_sesuai_ktp += 1
                } else {
                    $('[name="f_provinsi"]').addClass('d-none')
                }

                let data_kota_investor = data_investor.kota_investor ? data_investor.kota_investor : ''
                let data_riwayat_kota_investor = data_riwayat_investor_individu.kota_investor ? data_riwayat_investor_individu.kota_investor : ''
                if (data_kota_investor != data_riwayat_kota_investor) {
                    $('[name="f_kota"]').removeClass('d-none')
                    getKota(data_investor.provinsi_investor, data_kota_investor, `b_kota`)
                    getKota(data_riwayat_investor_individu.provinsi_investor, data_riwayat_kota_investor, `a_kota`)
                    // $('[name="b_kota"]').val(data_kota_investor)
                    // $('[name="a_kota"]').val(data_riwayat_kota_investor)

                    total_informasi_alamat_sesuai_ktp += 1
                } else {
                    $('[name="f_kota"]').addClass('d-none')
                }

                let data_kecamatan = data_investor.kecamatan ? data_investor.kecamatan : ''
                let data_riwayat_kecamatan = data_riwayat_investor_individu.kecamatan ? data_riwayat_investor_individu.kecamatan : ''
                if (data_kecamatan != data_riwayat_kecamatan) {
                    $('[name="f_kecamatan"]').removeClass('d-none')
                    $('[name="b_kecamatan"]').val(data_kecamatan)
                    $('[name="a_kecamatan"]').val(data_riwayat_kecamatan)

                    total_informasi_alamat_sesuai_ktp += 1
                } else {
                    $('[name="f_kecamatan"]').addClass('d-none')
                }

                let data_kelurahan = data_investor.kelurahan ? data_investor.kelurahan : ''
                let data_riwayat_kelurahan = data_riwayat_investor_individu.kelurahan ? data_riwayat_investor_individu.kelurahan : ''
                if (data_kelurahan != data_riwayat_kelurahan) {
                    $('[name="f_kelurahan"]').removeClass('d-none')
                    $('[name="b_kelurahan"]').val(data_kelurahan)
                    $('[name="a_kelurahan"]').val(data_riwayat_kelurahan)

                    total_informasi_alamat_sesuai_ktp += 1
                } else {
                    $('[name="f_kelurahan"]').addClass('d-none')
                }

                let data_kode_pos_investor = data_investor.kode_pos_investor ? data_investor.kode_pos_investor : ''
                let data_riwayat_kode_pos_investor = data_riwayat_investor_individu.kode_pos_investor ? data_riwayat_investor_individu.kode_pos_investor : ''
                if (data_kode_pos_investor != data_riwayat_kode_pos_investor) {
                    $('[name="f_kode_pos"]').removeClass('d-none')
                    $('[name="b_kode_pos"]').val(data_kode_pos_investor)
                    $('[name="a_kode_pos"]').val(data_riwayat_kode_pos_investor)

                    total_informasi_alamat_sesuai_ktp += 1
                } else {
                    $('[name="f_kode_pos"]').addClass('d-none')
                }
                // END: Informasi Alamat Sesuai KTP

                // START: Foto Pendana
                let data_pic_investor = data_investor.pic_investor ? data_investor.pic_investor : ''
                let data_riwayat_pic_investor = data_riwayat_investor_individu.pic_investor ? data_riwayat_investor_individu.pic_investor : ''
                if (data_pic_investor != data_riwayat_pic_investor) {
                    let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
                    data_pic_investor ? $('#b_pic_investor').attr('src', url.replace(':path', data_pic_investor.replaceAll('/', ':'))) : $('#b_pic_investor').attr('src', '');
                    data_riwayat_pic_investor ? $('#a_pic_investor').attr('src', url.replace(':path', data_riwayat_pic_investor.replaceAll('/', ':'))) : $('#a_pic_investor').attr('src', '');
                    
                    $('[name="f_pic_investor"]').removeClass('d-none')

                    total_foto_pendana += 1
                } else {
                    $('[name="f_pic_investor"]').addClass('d-none')
                }

                let data_pic_ktp_investor = data_investor.pic_ktp_investor ? data_investor.pic_ktp_investor : ''
                let data_riwayat_pic_ktp_investor = data_riwayat_investor_individu.pic_ktp_investor ? data_riwayat_investor_individu.pic_ktp_investor : ''
                if (data_pic_ktp_investor != data_riwayat_pic_ktp_investor) {
                    let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
                    data_pic_ktp_investor ? $('#b_pic_ktp_investor').attr('src', url.replace(':path', data_pic_ktp_investor.replaceAll('/', ':'))) : $('#b_pic_ktp_investor').attr('src', '');
                    data_riwayat_pic_ktp_investor ? $('#a_pic_ktp_investor').attr('src', url.replace(':path', data_riwayat_pic_ktp_investor.replaceAll('/', ':'))) : $('#a_pic_ktp_investor').attr('src', '');
                    
                    $('[name="f_pic_ktp_investor"]').removeClass('d-none')

                    total_foto_pendana += 1
                } else {
                    $('[name="f_pic_ktp_investor"]').addClass('d-none')
                }

                let data_pic_user_ktp_investor = data_investor.pic_user_ktp_investor ? data_investor.pic_user_ktp_investor : ''
                let data_riwayat_pic_user_ktp_investor = data_riwayat_investor_individu.pic_user_ktp_investor ? data_riwayat_investor_individu.pic_user_ktp_investor : ''
                if (data_pic_user_ktp_investor != data_riwayat_pic_user_ktp_investor) {
                    let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
                    data_pic_user_ktp_investor ? $('#b_pic_user_ktp_investor').attr('src', url.replace(':path', data_pic_user_ktp_investor.replaceAll('/', ':'))) : $('#b_pic_user_ktp_investor').attr('src', '');
                    data_riwayat_pic_user_ktp_investor ? $('#a_pic_user_ktp_investor').attr('src', url.replace(':path', data_riwayat_pic_user_ktp_investor.replaceAll('/', ':'))) : $('#a_pic_user_ktp_investor').attr('src', '');
                    
                    $('[name="f_pic_user_ktp_investor"]').removeClass('d-none')

                    total_foto_pendana += 1
                } else {
                    $('[name="f_pic_user_ktp_investor"]').addClass('d-none')
                }

                // END: Foto Pendana

                // START: Informasi Ahli Waris
                let data_nama_ahli_waris = data_ahli_waris ? (data_ahli_waris.nama_ahli_waris ? data_ahli_waris.nama_ahli_waris : '') : ''
                let data_riwayat_nama_ahli_waris = data_riwayat_ahli_waris ? data_riwayat_ahli_waris.nama_ahli_waris : ''
                if (data_nama_ahli_waris != data_riwayat_nama_ahli_waris) {
                    $('[name="f_nama_ahli_waris"]').removeClass('d-none')
                    $('[name="b_nama_ahli_waris"]').val(data_nama_ahli_waris)
                    $('[name="a_nama_ahli_waris"]').val(data_riwayat_nama_ahli_waris)

                    total_informasi_ahli_waris += 1
                } else {
                    $('[name="f_nama_ahli_waris"]').addClass('d-none')
                }

                let data_hubungan_keluarga_ahli_waris = data_ahli_waris ? (data_ahli_waris.hubungan_keluarga_ahli_waris ? data_ahli_waris.hubungan_keluarga_ahli_waris : '') : ''
                let data_riwayat_hubungan_keluarga_ahli_waris = data_riwayat_ahli_waris ? data_riwayat_ahli_waris.hubungan_keluarga_ahli_waris : ''
                if (data_hubungan_keluarga_ahli_waris != data_riwayat_hubungan_keluarga_ahli_waris) {
                    $('[name="f_hub_ahli_waris"]').removeClass('d-none')
                    $('[name="b_hub_ahli_waris"]').val(data_hubungan_keluarga_ahli_waris)
                    $('[name="a_hub_ahli_waris"]').val(data_riwayat_hubungan_keluarga_ahli_waris)

                    total_informasi_ahli_waris += 1
                } else {
                    $('[name="f_hub_ahli_waris"]').addClass('d-none')
                }

                let data_nik_ahli_waris = data_ahli_waris ? (data_ahli_waris.nik_ahli_waris ? data_ahli_waris.nik_ahli_waris : '') : ''
                let data_riwayat_nik_ahli_waris = data_riwayat_ahli_waris ? data_riwayat_ahli_waris.nik_ahli_waris : ''
                if (data_nik_ahli_waris != data_riwayat_nik_ahli_waris) {
                    $('[name="f_nik_ahli_waris"]').removeClass('d-none')
                    $('[name="b_nik_ahli_waris"]').val(data_nik_ahli_waris)
                    $('[name="a_nik_ahli_waris"]').val(data_riwayat_nik_ahli_waris)

                    total_informasi_ahli_waris += 1
                } else {
                    $('[name="f_nik_ahli_waris"]').addClass('d-none')
                }

                let data_no_hp_ahli_waris = data_ahli_waris ? (data_ahli_waris.no_hp_ahli_waris ? data_ahli_waris.no_hp_ahli_waris : '') : ''
                let data_riwayat_no_hp_ahli_waris = data_riwayat_ahli_waris ? data_riwayat_ahli_waris.no_hp_ahli_waris : ''
                if (data_no_hp_ahli_waris != data_riwayat_no_hp_ahli_waris) {
                    $('[name="f_no_hp_ahli_waris"]').removeClass('d-none')
                    $('[name="b_no_hp_ahli_waris"]').val(data_no_hp_ahli_waris)
                    $('[name="a_no_hp_ahli_waris"]').val(data_riwayat_no_hp_ahli_waris)

                    total_informasi_ahli_waris += 1
                } else {
                    $('[name="f_no_hp_ahli_waris"]').addClass('d-none')
                }

                let data_alamat_ahli_waris = data_ahli_waris ? (data_ahli_waris.alamat_ahli_waris ? data_ahli_waris.alamat_ahli_waris : '') : ''
                let data_riwayat_alamat_ahli_waris = data_riwayat_ahli_waris ? data_riwayat_ahli_waris.alamat_ahli_waris : ''
                if (data_alamat_ahli_waris != data_riwayat_alamat_ahli_waris) {
                    $('[name="f_alamat_ahli_waris"]').removeClass('d-none')
                    $('[name="b_alamat_ahli_waris"]').val(data_alamat_ahli_waris)
                    $('[name="a_alamat_ahli_waris"]').val(data_riwayat_alamat_ahli_waris)

                    total_informasi_ahli_waris += 1
                } else {
                    $('[name="f_alamat_ahli_waris"]').addClass('d-none')
                }
                // END: Informasi Ahli Waris

                // Layout Title
                if (total_informasi_pribadi > 0) {
                    console.log(total_informasi_pribadi)
                    $('.layout_informasi_pribadi').removeClass('d-none')
                } else {
                    $('.layout_informasi_pribadi').addClass('d-none')
                }
                
                if (total_informasi_pekerjaan > 0) {
                    console.log(total_informasi_pekerjaan)
                    $('.layout_informasi_pekerjaan').removeClass('d-none')
                } else {
                    $('.layout_informasi_pekerjaan').addClass('d-none')
                }

                if (total_informasi_alamat_sesuai_ktp > 0) {
                    console.log(total_informasi_alamat_sesuai_ktp)
                    $('.layout_informasi_alamat_sesuai_ktp').removeClass('d-none')
                } else {
                    $('.layout_informasi_alamat_sesuai_ktp').addClass('d-none')
                }

                if (total_foto_pendana > 0) {
                    $('.layout_foto_pendana').removeClass('d-none')
                } else {
                    $('.layout_foto_pendana').addClass('d-none')
                }

                if (total_informasi_rekening > 0) {
                    $('.layout_informasi_rekening').removeClass('d-none')
                } else {
                    $('.layout_informasi_rekening').addClass('d-none')
                }

                if (total_informasi_ahli_waris > 0) {
                    $('.layout_informasi_ahli_waris').removeClass('d-none')
                } else {
                    $('.layout_informasi_ahli_waris').addClass('d-none')
                }

                if (total_informasi_akun + total_informasi_pribadi + total_informasi_pekerjaan + total_informasi_alamat_sesuai_ktp + total_informasi_rekening + total_informasi_ahli_waris + total_foto_pendana + total_informasi_referal == 0) { // Tidak ada perubahan data
                    $('#blank_data').removeClass('d-none')
                } else {
                    $('#blank_data').addClass('d-none')
                }
                // END: Layout Title
            } else if (user_type == '2') { // Badan Hukum
                $('#layout-investor-individu').addClass('d-none')
                $('#layout-investor-bdn-hukum').removeClass('d-none')

                // START: Informasi Badan Hukum
                let data_nama_perusahaan = data_investor.nama_perusahaan ? data_investor.nama_perusahaan : ''
                let data_riwayat_nama_perusahaan= data_riwayat_investor_bdn_hukum.nama_perusahaan ? data_riwayat_investor_bdn_hukum.nama_perusahaan : ''
                if (data_nama_perusahaan != data_riwayat_nama_perusahaan) {
                    $('[name="f_nama_badan_hukum"]').removeClass('d-none')
                    $('[name="b_nama_badan_hukum"]').val(data_nama_perusahaan)
                    $('[name="a_nama_badan_hukum"]').val(data_riwayat_nama_perusahaan)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_nama_badan_hukum"]').addClass('d-none')
                }

                let data_nib = data_investor.nib ? data_investor.nib : ''
                let data_riwayat_nib = data_riwayat_investor_bdn_hukum.nib ? data_riwayat_investor_bdn_hukum.nib : ''
                if (data_nib != data_riwayat_nib) {
                    $('[name="f_nib_badan_hukum"]').removeClass('d-none')
                    $('[name="b_nib_badan_hukum"]').val(data_nib)
                    $('[name="a_nib_badan_hukum"]').val(data_riwayat_nib)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_nib_badan_hukum"]').addClass('d-none')
                }

                let data_npwp_perusahaan = data_investor.npwp_perusahaan ? data_investor.npwp_perusahaan : ''
                let data_riwayat_npwp_perusahaan = data_riwayat_investor_bdn_hukum.npwp_perusahaan ? data_riwayat_investor_bdn_hukum.npwp_perusahaan : ''
                if (data_npwp_perusahaan != data_riwayat_npwp_perusahaan) {
                    $('[name="f_npwp_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_npwp_bdn_hukum"]').val(data_npwp_perusahaan)
                    $('[name="a_npwp_bdn_hukum"]').val(data_riwayat_npwp_perusahaan)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_npwp_bdn_hukum"]').addClass('d-none')
                }

                let data_no_akta_pendirian = data_investor.no_akta_pendirian ? data_investor.no_akta_pendirian : ''
                let data_riwayat_no_akta_pendirian = data_riwayat_investor_bdn_hukum.no_akta_pendirian ? data_riwayat_investor_bdn_hukum.no_akta_pendirian : ''
                if (data_no_akta_pendirian != data_riwayat_no_akta_pendirian) {
                    $('[name="f_no_akta_pendirian_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_no_akta_pendirian_bdn_hukum"]').val(data_no_akta_pendirian)
                    $('[name="a_no_akta_pendirian_bdn_hukum"]').val(data_riwayat_no_akta_pendirian)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_no_akta_pendirian_bdn_hukum"]').addClass('d-none')
                }

                let data_tgl_berdiri = data_investor.tgl_berdiri && data_investor.tgl_berdiri != '0000-00-00' ? data_investor.tgl_berdiri : ''
                let data_riwayat_tgl_berdiri = data_riwayat_investor_bdn_hukum.tgl_berdiri && data_riwayat_investor_bdn_hukum.tgl_berdiri != '0000-00-00' ? data_riwayat_investor_bdn_hukum.tgl_berdiri : ''
                if (data_tgl_berdiri != data_riwayat_tgl_berdiri) {
                    $('[name="f_tgl_berdiri_badan_hukum"]').removeClass('d-none')
                    $('[name="b_tgl_berdiri_badan_hukum"]').val(data_tgl_berdiri)
                    $('[name="a_tgl_berdiri_badan_hukum"]').val(data_riwayat_tgl_berdiri)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_tgl_berdiri_badan_hukum"]').addClass('d-none')
                }

                let data_nomor_akta_perubahan = data_investor.nomor_akta_perubahan ? data_investor.nomor_akta_perubahan : ''
                let data_riwayat_nomor_akta_perubahan = data_riwayat_investor_bdn_hukum.nomor_akta_perubahan ? data_riwayat_investor_bdn_hukum.nomor_akta_perubahan : ''
                if (data_nomor_akta_perubahan != data_riwayat_nomor_akta_perubahan) {
                    $('[name="f_no_akta_perubahan_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_no_akta_perubahan_bdn_hukum"]').val(data_nomor_akta_perubahan)
                    $('[name="a_no_akta_perubahan_bdn_hukum"]').val(data_riwayat_nomor_akta_perubahan)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_no_akta_perubahan_bdn_hukum"]').addClass('d-none')
                }

                let data_tanggal_akta_perubahan = data_investor.tanggal_akta_perubahan && data_investor.tanggal_akta_perubahan != '0000-00-00' ? data_investor.tanggal_akta_perubahan : ''
                let data_riwayat_tanggal_akta_perubahan = data_riwayat_investor_bdn_hukum.tanggal_akta_perubahan && data_riwayat_investor_bdn_hukum.tanggal_akta_perubahan != '0000-00-00' ? data_riwayat_investor_bdn_hukum.tanggal_akta_perubahan : ''
                if (data_tanggal_akta_perubahan != data_riwayat_tanggal_akta_perubahan) {
                    $('[name="f_tgl_akta_perubahan_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_tgl_akta_perubahan_bdn_hukum"]').val(data_tanggal_akta_perubahan)
                    $('[name="a_tgl_akta_perubahan_bdn_hukum"]').val(data_riwayat_tanggal_akta_perubahan)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_tgl_akta_perubahan_bdn_hukum"]').addClass('d-none')
                }

                let data_telpon_perusahaan = data_investor.telpon_perusahaan ? data_investor.telpon_perusahaan : ''
                let data_riwayat_telpon_perusahaan = data_riwayat_investor_bdn_hukum.telpon_perusahaan ? data_riwayat_investor_bdn_hukum.telpon_perusahaan : ''
                if (data_telpon_perusahaan != data_riwayat_telpon_perusahaan) {
                    $('[name="f_no_tlp_badan_hukum"]').removeClass('d-none')
                    $('[name="b_no_tlp_badan_hukum"]').val(data_telpon_perusahaan)
                    $('[name="a_no_tlp_badan_hukum"]').val(data_riwayat_telpon_perusahaan)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_no_tlp_badan_hukum"]').addClass('d-none')
                }

                let data_foto_npwp_perusahaan = data_investor.foto_npwp_perusahaan ? data_investor.foto_npwp_perusahaan : ''
                let data_riwayat_foto_npwp_perusahaan = data_riwayat_investor_bdn_hukum.foto_npwp_perusahaan ? data_riwayat_investor_bdn_hukum.foto_npwp_perusahaan : ''
                if (data_foto_npwp_perusahaan != data_riwayat_foto_npwp_perusahaan) {
                    let url = "{{ route('getUserFileForAdmin', ['filename' => ':path']) }}";
                    data_foto_npwp_perusahaan ? $('#b_foto_npwp_badan_hukum').attr('src', url.replace(':path', data_foto_npwp_perusahaan.replaceAll('/', ':'))) : $('#b_foto_npwp_badan_hukum').attr('src', '');
                    data_riwayat_foto_npwp_perusahaan ? $('#a_foto_npwp_badan_hukum').attr('src', url.replace(':path', data_riwayat_foto_npwp_perusahaan.replaceAll('/', ':'))) : $('#a_foto_npwp_badan_hukum').attr('src', '');
                    
                    $('[name="f_foto_npwp_badan_hukum"]').removeClass('d-none')
                    // $('[name="b_no_tlp_badan_hukum"]').attr('src', data_foto_npwp_perusahaan)
                    // $('[name="a_no_tlp_badan_hukum"]').attr('src', data_riwayat_foto_npwp_perusahaan)

                    total_informasi_bdn_hukum += 1
                } else {
                    $('[name="f_foto_npwp_badan_hukum"]').addClass('d-none')
                }
                // END: Informasi Badan Hukum

                // START: Informasi Alamat Sesuai Dengan Akta
                let data_alamat_perusahaan = data_investor.alamat_perusahaan ? data_investor.alamat_perusahaan : ''
                let data_riwayat_alamat_perusahaan = data_riwayat_investor_bdn_hukum.alamat_perusahaan ? data_riwayat_investor_bdn_hukum.alamat_perusahaan : ''
                if (data_alamat_perusahaan != data_riwayat_alamat_perusahaan) {
                    $('[name="f_alamat_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_alamat_bdn_hukum"]').val(data_alamat_perusahaan)
                    $('[name="a_alamat_bdn_hukum"]').val(data_riwayat_alamat_perusahaan)

                    total_informasi_alamat_sesuai_ktp_bdn_hukum += 1
                } else {
                    $('[name="f_alamat_bdn_hukum"]').addClass('d-none')
                }

                let data_provinsi_perusahaan = data_investor.provinsi_perusahaan ? data_investor.provinsi_perusahaan : ''
                let data_riwayat_provinsi_perusahaan = data_riwayat_investor_bdn_hukum.provinsi_perusahaan ? data_riwayat_investor_bdn_hukum.provinsi_perusahaan : ''
                if (data_provinsi_perusahaan != data_riwayat_provinsi_perusahaan) {
                    $('[name="f_provinsi_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_provinsi_bdn_hukum"]').val(data_provinsi_perusahaan)
                    $('[name="a_provinsi_bdn_hukum"]').val(data_riwayat_provinsi_perusahaan)

                    total_informasi_alamat_sesuai_ktp_bdn_hukum += 1
                } else {
                    $('[name="f_provinsi_bdn_hukum"]').addClass('d-none')
                }

                let data_kota_perusahaan = data_investor.kota_perusahaan ? data_investor.kota_perusahaan : ''
                let data_riwayat_kota_perusahaan = data_riwayat_investor_bdn_hukum.kota_perusahaan ? data_riwayat_investor_bdn_hukum.kota_perusahaan : ''
                if (data_kota_perusahaan != data_riwayat_kota_perusahaan) {
                    $('[name="f_kota_bdn_hukum"]').removeClass('d-none')
                    getKota(data_investor.provinsi_perusahaan, data_kota_perusahaan, `b_kota_bdn_hukum`)
                    getKota(data_riwayat_investor_bdn_hukum.provinsi_perusahaan, data_riwayat_kota_perusahaan, `a_kota_bdn_hukum`)
                    // $('[name="b_kota_bdn_hukum"]').val(data_kota_perusahaan)
                    // $('[name="a_kota_bdn_hukum"]').val(data_riwayat_kota_perusahaan)

                    total_informasi_alamat_sesuai_ktp_bdn_hukum += 1
                } else {
                    $('[name="f_kota_bdn_hukum"]').addClass('d-none')
                }

                let data_kecamatan_perusahaan = data_investor.kecamatan_perusahaan ? data_investor.kecamatan_perusahaan : ''
                let data_riwayat_kecamatan_perusahaan = data_riwayat_investor_bdn_hukum.kecamatan_perusahaan ? data_riwayat_investor_bdn_hukum.kecamatan_perusahaan : ''
                if (data_kecamatan_perusahaan != data_riwayat_kecamatan_perusahaan) {
                    $('[name="f_kecamatan_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_kecamatan_bdn_hukum"]').val(data_kecamatan_perusahaan)
                    $('[name="a_kecamatan_bdn_hukum"]').val(data_riwayat_kecamatan_perusahaan)

                    total_informasi_alamat_sesuai_ktp_bdn_hukum += 1
                } else {
                    $('[name="f_kecamatan_bdn_hukum"]').addClass('d-none')
                }

                let data_kelurahan_perusahaan = data_investor.kelurahan_perusahaan ? data_investor.kelurahan_perusahaan : ''
                let data_riwayat_kelurahan_perusahaan = data_riwayat_investor_bdn_hukum.kelurahan_perusahaan ? data_riwayat_investor_bdn_hukum.kelurahan_perusahaan : ''
                if (data_kelurahan_perusahaan != data_riwayat_kelurahan_perusahaan) {
                    $('[name="f_kelurahan_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_kelurahan_bdn_hukum"]').val(data_kelurahan_perusahaan)
                    $('[name="a_kelurahan_bdn_hukum"]').val(data_riwayat_kelurahan_perusahaan)

                    total_informasi_alamat_sesuai_ktp_bdn_hukum += 1
                } else {
                    $('[name="f_kelurahan_bdn_hukum"]').addClass('d-none')
                }

                let data_kode_pos_perusahaan = data_investor.kode_pos_perusahaan ? data_investor.kode_pos_perusahaan : ''
                let data_riwayat_kode_pos_perusahaan = data_riwayat_investor_bdn_hukum.kode_pos_perusahaan ? data_riwayat_investor_bdn_hukum.kode_pos_perusahaan : ''
                if (data_kode_pos_perusahaan != data_riwayat_kode_pos_perusahaan) {
                    $('[name="f_kode_pos_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_kode_pos_bdn_hukum"]').val(data_kode_pos_perusahaan)
                    $('[name="a_kode_pos_bdn_hukum"]').val(data_riwayat_kode_pos_perusahaan)

                    total_informasi_alamat_sesuai_ktp_bdn_hukum += 1
                } else {
                    $('[name="f_kode_pos_bdn_hukum"]').addClass('d-none')
                }

                // END: Informasi Alamat Sesuai Dengan Akta

                // START: Informasi Lain Lain
                let data_bidang_pekerjaan = data_investor.bidang_pekerjaan ? data_investor.bidang_pekerjaan : ''
                let data_riwayat_bidang_pekerjaan = data_riwayat_investor_bdn_hukum.bidang_pekerjaan ? data_riwayat_investor_bdn_hukum.bidang_pekerjaan : ''
                if (data_bidang_pekerjaan != data_riwayat_bidang_pekerjaan) {
                    $('[name="f_bidang_pekerjaan_bdn_hukum"]').removeClass('d-none')
                    $('[name="b_bidang_pekerjaan_bdn_hukum"]').val(data_bidang_pekerjaan)
                    $('[name="a_bidang_pekerjaan_bdn_hukum"]').val(data_riwayat_bidang_pekerjaan)

                    total_informasi_lain_lain_bdn_hukum += 1
                } else {
                    $('[name="f_bidang_pekerjaan_bdn_hukum"]').addClass('d-none')
                }

                let data_omset_tahun_terakhir = data_investor.omset_tahun_terakhir ? parseInt(data_investor.omset_tahun_terakhir) : ''
                let data_riwayat_omset_tahun_terakhir = data_riwayat_investor_bdn_hukum.omset_tahun_terakhir ? parseInt(data_riwayat_investor_bdn_hukum.omset_tahun_terakhir) : ''
                if (data_omset_tahun_terakhir != data_riwayat_omset_tahun_terakhir) {
                    $('[name="f_omset_tahun_terakhir"]').removeClass('d-none')
                    $('[name="b_omset_tahun_terakhir"]').val(formatRupiah(data_omset_tahun_terakhir.toString(), 'Rp'))
                    $('[name="a_omset_tahun_terakhir"]').val(formatRupiah(data_riwayat_omset_tahun_terakhir.toString(), 'Rp'))

                    total_informasi_lain_lain_bdn_hukum += 1
                } else {
                    $('[name="f_omset_tahun_terakhir"]').addClass('d-none')
                }

                let data_tot_aset_tahun_terakhr = data_investor.tot_aset_tahun_terakhr ? parseInt(data_investor.tot_aset_tahun_terakhr) : ''
                let data_riwayat_tot_aset_tahun_terakhr = data_riwayat_investor_bdn_hukum.tot_aset_tahun_terakhr ? parseInt(data_riwayat_investor_bdn_hukum.tot_aset_tahun_terakhr) : ''
                if (data_tot_aset_tahun_terakhr != data_riwayat_tot_aset_tahun_terakhr) {
                    $('[name="f_tot_aset_tahun_terakhr"]').removeClass('d-none')
                    $('[name="b_tot_aset_tahun_terakhr"]').val(formatRupiah(data_tot_aset_tahun_terakhr.toString(), 'Rp'))
                    $('[name="a_tot_aset_tahun_terakhr"]').val(formatRupiah(data_riwayat_tot_aset_tahun_terakhr.toString(), 'Rp'))

                    total_informasi_lain_lain_bdn_hukum += 1
                } else {
                    $('[name="f_tot_aset_tahun_terakhr"]').addClass('d-none')
                }
                // END: Informasi Lain Lain

                // START: Layout Title
                if (total_informasi_bdn_hukum > 0) {
                    $('.layout_informasi_bdn_hukum').removeClass('d-none')
                } else  {
                    $('.layout_informasi_bdn_hukum').addClass('d-none')
                }

                if (total_informasi_alamat_sesuai_ktp_bdn_hukum > 0) {
                    $('.layout_informasi_alamat_sesuai_ktp_bdn_hukum').removeClass('d-none')
                } else  {
                    $('.layout_informasi_alamat_sesuai_ktp_bdn_hukum').addClass('d-none')
                }

                if (total_informasi_rekening > 0) {
                    $('.layout_informasi_rekening_bdn_hukum').removeClass('d-none')
                } else {
                    $('.layout_informasi_rekening_bdn_hukum').addClass('d-none')
                }

                if (total_informasi_lain_lain_bdn_hukum > 0) {
                    $('.layout_informasi_lain_lain_bdn_hukum').removeClass('d-none')
                    // $('.badge_layout_informasi_lain_lain_bdn_hukum').text(total_informasi_lain_lain_bdn_hukum)
                } else  {
                    $('.layout_informasi_lain_lain_bdn_hukum').addClass('d-none')
                }
                
                // END: Layout Title

                // START: Informasi Pengurus
                $("#layout-pengurus").html('')
                for (let i = 0; i < total_pengurus; i++) {
                    addPengurus(i, data_pengurus_investor[i], data_riwayat_pengurus_investor[i])
                }
                // END: Informasi Pengurus

                if (is_pengurus_exist + total_informasi_akun + total_informasi_rekening + total_informasi_bdn_hukum + total_informasi_alamat_sesuai_ktp_bdn_hukum + total_informasi_lain_lain_bdn_hukum + total_informasi_referal == 0) { // Tidak ada perubahan data
                    $('#blank_data').removeClass('d-none')
                } else {
                    $('#blank_data').addClass('d-none')
                }
                is_pengurus_exist = 0
            }
        }

        $('#modal-click').trigger('click')
    }

    const saveDetailProfile = (element_id) => {
        let route = "{!! route('save-permintaan-perubahan-data') !!}"
          $.ajax({
            url : route,
            type: "GET",
            dataType: 'JSON',
            data: $(element_id).serialize(),
            beforeSend: function() {
                swal.fire({
                    html: '<h5>Menyimpan Data...</h5>',
                    showConfirmButton: false,
                    allowOutsideClick: () => false,
                    onBeforeOpen: () => {
                        swal.showLoading();
                    }
                });
            },
            success: function (response) {
                console.log(response)
                if (response.status == 'success') {
                    swal.fire({
                        title: 'Berhasil',
                        type: 'success',
                        text: response.message,
                        allowOutsideClick: () => false,
                    }).then(function (result) {
                        if (result.value) {
                            list_table_permintaan_perubahan.clear()
                            list_table_permintaan_perubahan.destroy()
                            loadData()
                            document.getElementById('modal-close').click()
                        }
                    })
                } else {
                    swal.fire({
                        title: 'Oops',
                        type: 'error',
                        text: response.message
                    })
                    console.log(response)
                }
            },
            error: function(response) {
                let msg = JSON.parse(response.responseText)
                swal.fire({
                    title: 'Error',
                    type: 'error',
                    text: msg.message
                })
                console.log(msg.message)
            }
        })

    }
    
    let total_informasi_pengurus = 0
    let total_informasi_alamat_pengurus = 0

    const addPengurus = (n, data_pengurus, data_riwayat_pengurus) => {
        let data_pengurus_investor = data_pengurus
        let data_riwayat_pengurus_investor = data_riwayat_pengurus

        if (cal_informasi_pengurus(1, n, data_pengurus_investor, data_riwayat_pengurus_investor) != 0) {
            let data_nama_pengurus = data_pengurus_investor.nama_pengurus ? data_pengurus_investor.nama_pengurus : ''
            let data_riwayat_nama_pengurus = data_riwayat_pengurus_investor.nama_pengurus ? data_riwayat_pengurus_investor.nama_pengurus : ''

            let data_jenis_kelamin = data_pengurus_investor.jenis_kelamin ? data_pengurus_investor.jenis_kelamin : ''
            let data_riwayat_jenis_kelamin = data_riwayat_pengurus_investor.jenis_kelamin ? data_riwayat_pengurus_investor.jenis_kelamin : ''

            let data_nomor_ktp = data_pengurus_investor.nomor_ktp ? data_pengurus_investor.nomor_ktp : ''
            let data_riwayat_nomor_ktp = data_riwayat_pengurus_investor.nomor_ktp ? data_riwayat_pengurus_investor.nomor_ktp : ''

            let data_tempat_lahir = data_pengurus_investor.tempat_lahir ? data_pengurus_investor.tempat_lahir : ''
            let data_riwayat_tempat_lahir = data_riwayat_pengurus_investor.tempat_lahir ? data_riwayat_pengurus_investor.tempat_lahir : ''

            let data_tgl_lahir = data_pengurus_investor.tgl_lahir && data_pengurus_investor.tgl_lahir != "0000-00-00" ? data_pengurus_investor.tgl_lahir : ''
            let data_riwayat_tgl_lahir = data_riwayat_pengurus_investor.tgl_lahir && data_riwayat_pengurus_investor.tgl_lahir != "0000-00-00" ? data_riwayat_pengurus_investor.tgl_lahir : ''

            let data_no_hp= data_pengurus_investor.no_hp? data_pengurus_investor.no_hp: ''
            let data_riwayat_no_hp= data_riwayat_pengurus_investor.no_hp? data_riwayat_pengurus_investor.no_hp: ''

            let data_agama= data_pengurus_investor.agama? data_pengurus_investor.agama: ''
            let data_riwayat_agama= data_riwayat_pengurus_investor.agama? data_riwayat_pengurus_investor.agama: ''
            
            let data_pendidikan_terakhir= data_pengurus_investor.pendidikan_terakhir? data_pengurus_investor.pendidikan_terakhir: ''
            let data_riwayat_pendidikan_terakhir= data_riwayat_pengurus_investor.pendidikan_terakhir? data_riwayat_pengurus_investor.pendidikan_terakhir: ''

            let data_npwp= data_pengurus_investor.npwp? data_pengurus_investor.npwp: ''
            let data_riwayat_npwp= data_riwayat_pengurus_investor.npwp? data_riwayat_pengurus_investor.npwp: ''

            let data_jabatan= data_pengurus_investor.jabatan? data_pengurus_investor.jabatan: ''
            let data_riwayat_jabatan= data_riwayat_pengurus_investor.jabatan? data_riwayat_pengurus_investor.jabatan: ''

            let data_alamat= data_pengurus_investor.alamat? data_pengurus_investor.alamat: ''
            let data_riwayat_alamat= data_riwayat_pengurus_investor.alamat? data_riwayat_pengurus_investor.alamat: ''

            let data_provinsi= data_pengurus_investor.provinsi? data_pengurus_investor.provinsi: ''
            let data_riwayat_provinsi= data_riwayat_pengurus_investor.provinsi? data_riwayat_pengurus_investor.provinsi: ''

            let data_kota= data_pengurus_investor.kota? data_pengurus_investor.kota: ''
            let data_riwayat_kota= data_riwayat_pengurus_investor.kota? data_riwayat_pengurus_investor.kota: ''

            let data_kecamatan= data_pengurus_investor.kecamatan? data_pengurus_investor.kecamatan: ''
            let data_riwayat_kecamatan= data_riwayat_pengurus_investor.kecamatan? data_riwayat_pengurus_investor.kecamatan: ''

            let data_kelurahan= data_pengurus_investor.kelurahan? data_pengurus_investor.kelurahan: ''
            let data_riwayat_kelurahan= data_riwayat_pengurus_investor.kelurahan? data_riwayat_pengurus_investor.kelurahan: ''

            let data_kode_pos= data_pengurus_investor.kode_pos? data_pengurus_investor.kode_pos: ''
            let data_riwayat_kode_pos= data_riwayat_pengurus_investor.kode_pos? data_riwayat_pengurus_investor.kode_pos: ''
            let pengurus_html = () =>  ` 
                <div class="mt-4 layout-pengurus" id="layout-pengurus-${n}">
                    
                        <div class="card card-header card-header-pengurus" id="card-pengurus-${n}" data-toggle="collapse"
                            data-target="#collapse-${n}" aria-expanded="true" aria-controls="collapse-${n}">
                            <p class="h6 font-weight-bold">
                                Pengurus ${n+1} ${n == 0 ? '(Mewakili Sebagai Perusahaan)' : ''} <span name="card-title[]"></span> <i
                                    class="fa fa-angle-up float-right"></i><i class="fa fa-angle-down float-right"></i>
                            </p>
                            </h5>
                        </div>
                
                        <div id="collapse-${n}" class="collapse" aria-labelledby="card-pengurus-${n}"
                            data-parent="#layout-pengurus-${n}">
                            <div class="card card-body">
                
                                <div id="pengurus">
                                    ${data_nama_pengurus != data_riwayat_nama_pengurus ?
                                    `<div class="row mt-2" id="f_nama_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_nama_pengurus_${n}">Nama Pengurus <i class="text-danger">*</i></label>
                                            <input type="hidden" id="pengurus_id_${n}" name="b_pengurus_id[]">
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control" type="text" id="b_nama_pengurus_${n}" minlength="3"
                                                maxlength="30" name="b_nama_pengurus[]" placeholder="Masukkan Nama Pengurus..."
                                                value="${data_nama_pengurus}">
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control" type="text" id="a_nama_pengurus_${n}" minlength="3"
                                                maxlength="30" name="a_nama_pengurus[]" placeholder="Masukkan Nama Pengurus..."
                                                value="${data_riwayat_nama_pengurus}">
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_jenis_kelamin != data_riwayat_jenis_kelamin ?
                                    `<div class="row mt-2" id="f_jns_kelamin_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_jns_kelamin_pengurus_${n}">Jenis Kelamin <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <select id="b_jns_kelamin_pengurus_${n}" name="b_jns_kelamin_pengurus[]"
                                                class="form-control custom-select">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_jenis_kelamin as $b)
                                                <option value="{{$b->id_jenis_kelamin}}">
                                                    {{$b->jenis_kelamin}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <select id="a_jns_kelamin_pengurus_${n}" name="a_jns_kelamin_pengurus[]"
                                                class="form-control custom-select">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_jenis_kelamin as $b)
                                                <option value="{{$b->id_jenis_kelamin}}">
                                                    {{$b->jenis_kelamin}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_nomor_ktp != data_riwayat_nomor_ktp ?
                                    `<div class="row mt-2" id="f_ktp_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_ktp_pengurus_${n}">Nomor KTP <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control" type="text" id="b_ktp_pengurus_${n}" name="b_ktp_pengurus[]"
                                                placeholder="Masukkan nomor KTP" minlength="16" maxlength="16" pattern=".{16,16}"
                                                onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                                                value="${data_nomor_ktp}">
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control" type="text" id="a_ktp_pengurus_${n}" name="a_ktp_pengurus[]"
                                                placeholder="Masukkan nomor KTP" minlength="16" maxlength="16" pattern=".{16,16}"
                                                onkeyup="this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                                                value="${data_riwayat_nomor_ktp}">
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_tempat_lahir != data_riwayat_tempat_lahir ?
                                    `<div class="row mt-2" id="f_tempat_lahir_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_tempat_lahir_pengurus_${n}">Tempat Lahir <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control" type="text" maxlength="35" id="b_tempat_lahir_pengurus_${n}"
                                                name="b_tempat_lahir_pengurus[]" placeholder="Masukkan tempat lahir"
                                                value="${data_tempat_lahir}">
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control" type="text" maxlength="35" id="a_tempat_lahir_pengurus_${n}"
                                                name="a_tempat_lahir_pengurus[]" placeholder="Masukkan tempat lahir"
                                                value="${data_riwayat_tempat_lahir}">
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_tgl_lahir != data_riwayat_tgl_lahir ?
                                    `<div class="row mt-2" id="f_tanggal_lahir_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_tanggal_lahir_pengurus_${n}">Tanggal Lahir <i
                                                    class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control" type="date" id="b_tanggal_lahir_pengurus_${n}"
                                                name="b_tanggal_lahir_pengurus[]" max="<?= date("Y-m-d"); ?>"
                                                placeholder="Masukkan tanggal lahir" onchange="$(this).valid()" value=${data_tgl_lahir}>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control" type="date" id="a_tanggal_lahir_pengurus_${n}"
                                                name="a_tanggal_lahir_pengurus[]" max="<?= date("Y-m-d"); ?>"
                                                placeholder="Masukkan tanggal lahir" onchange="$(this).valid()"
                                                value=${data_riwayat_tgl_lahir}>
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_no_hp != data_riwayat_no_hp ?
                                    `<div class="row mt-2" id="f_no_telp_pengurus_${n}">
                                        <div class="col-sm-12">
                
                                            <label for="b_no_telp_pengurus_${n}">Nomor Telepon <i class="text-danger">*</i>
                                            </label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input type="text" minlength="9" maxlength="13" name="b_no_telp_pengurus[]"
                                                id="b_no_telp_pengurus_${n}"
                                                onkeyup="if ($(this).val().charAt(0) == '0') {$(this).val($(this).val().substring(1))} else if ($(this).val().charAt(0) != '8') { $(this).val($(this).val().substring(1))}; this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                                                class="form-control no-zero eight"
                                                value="${data_no_hp}">
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input type="text" minlength="9" maxlength="13" name="a_no_telp_pengurus[]"
                                                id="a_no_telp_pengurus_${n}"
                                                onkeyup="if ($(this).val().charAt(0) == '0') {$(this).val($(this).val().substring(1))} else if ($(this).val().charAt(0) != '8') { $(this).val($(this).val().substring(1))}; this.value = this.value.replace(/[^0-9]/g, ''); $(this).valid()"
                                                class="form-control no-zero eight" placeholder="Contoh:8xxxxxxxxxx"
                                                value="${data_riwayat_no_hp}">
                                        </div>
                
                                    </div>` : ''
                                    }
                
                                    ${data_agama != data_riwayat_agama ?
                                    `<div class="row mt-2" id="f_agama_pengurus_${n}">
                                        <div class="col-sm-12 row">
                                            <label for="b_agama_pengurus_${n}" class="col-12">Agama <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="b_agama_pengurus_${n}"
                                                name="b_agama_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_agama as $b)
                                                <option value="{{$b->id_agama}}"
                                                    {{ old('agama_pengurus[]') == $b->agama ? 'selected' : '' }}>
                                                    {{$b->agama}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="a_agama_pengurus_${n}"
                                                name="a_agama_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_agama as $b)
                                                <option value="{{$b->id_agama}}"
                                                    {{ old('agama_pengurus[]') == $b->agama ? 'selected' : '' }}>
                                                    {{$b->agama}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_pendidikan_terakhir != data_riwayat_pendidikan_terakhir ?
                                    `<div class="row mt-2" id="f_pendidikan_terakhir_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_pendidikan_terakhir_pengurus_${n}">Pendidikan Terakhir <i
                                                    class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="b_pendidikan_terakhir_pengurus_${n}"
                                                name="b_pendidikan_terakhir_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_pendidikan as $b)
                                                <option value="{{$b->id_pendidikan}}"
                                                    {{ old('pendidikan_terakhir_pengurus[]') == $b->id_pendidikan ? 'selected' : '' }}>
                                                    {{$b->pendidikan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="a_pendidikan_terakhir_pengurus_${n}"
                                                name="a_pendidikan_terakhir_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_pendidikan as $b)
                                                <option value="{{$b->id_pendidikan}}"
                                                    {{ old('pendidikan_terakhir_pengurus[]') == $b->id_pendidikan ? 'selected' : '' }}>
                                                    {{$b->pendidikan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_npwp != data_riwayat_npwp ?
                                    `<div class="row mt-2" id="f_npwp_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_npwp_pengurus_${n}">Nomor NPWP <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control " type="text" maxlength="15" minlength="15"
                                                id="b_npwp_pengurus_${n}" name="b_npwp_pengurus[]" pattern=".{15,15}"
                                                onkeyup="this.value = this.value.replace(/[^0-9]/g, '');$(this).valid()"
                                                placeholder="Masukkan nomor NPWP" value="${data_npwp}">
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control " type="text" maxlength="15" minlength="15"
                                                id="a_npwp_pengurus_${n}" name="a_npwp_pengurus[]" pattern=".{15,15}"
                                                onkeyup="this.value = this.value.replace(/[^0-9]/g, '');$(this).valid()"
                                                placeholder="Masukkan nomor NPWP" value="${data_riwayat_npwp}">
                                        </div>
                                    </div>` : '' 
                                    }

                                    ${data_jabatan != data_riwayat_jabatan ?
                                    `<div class="row mt-2" id="f_jabatan_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_jabatan_pengurus_${n}">Jabatan <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="b_jabatan_pengurus_${n}"
                                                name="b_jabatan_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_jabatan as $jabs)
                                                <option value="{{$jabs->id}}">
                                                    {{$jabs->jabatan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="a_jabatan_pengurus_${n}"
                                                name="a_jabatan_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_jabatan as $jabs)
                                                <option value="{{$jabs->id}}">
                                                    {{$jabs->jabatan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>` : ''
                                    }
                                    
                                    <div class="row layout_informasi_alamat_pengurus_${n}">
                                        <div class="col-12">
                                            <h6 class="line my-4 font-weight-bold">Alamat Domisili Pengurus &nbsp</h6>
                                        </div>
                                    </div>
                                    
                                    ${data_alamat != data_riwayat_alamat ?
                                    `<div class="row mt-2" id="f_alamat_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_alamat_pengurus_${n}">Alamat<i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <textarea class="form-control form-control-lg" maxlength="90" id="b_alamat_pengurus_${n}"
                                                name="b_alamat_pengurus[]" rows="6"
                                                placeholder="Masukkan alamat lengkap Anda..">${data_alamat}</textarea>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <textarea class="form-control form-control-lg" maxlength="90" id="a_alamat_pengurus_${n}"
                                                name="a_alamat_pengurus[]" rows="6"
                                                placeholder="Masukkan alamat lengkap Anda..">${data_riwayat_alamat}</textarea>
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_provinsi != data_riwayat_provinsi ?
                                    `<div class="row mt-2" id="f_provinsi_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_provinsi_pengurus_${n}">Provinsi <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="b_provinsi_pengurus_${n}"
                                                name="b_provinsi_pengurus[]"
                                                onchange="provinsiChange(this.value, this.id, 'kota_pengurus_${n}')">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_provinsi as $data)
                                                <option value={{$data->kode_provinsi}}>
                                                    {{$data->nama_provinsi}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="a_provinsi_pengurus_${n}"
                                                name="a_provinsi_pengurus[]"
                                                onchange="provinsiChange(this.value, this.id, 'kota_pengurus_${n}')">
                                                <option value="">-- Pilih Satu --</option>
                                                @foreach ($master_provinsi as $data)
                                                <option value={{$data->kode_provinsi}}>
                                                    {{$data->nama_provinsi}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_kota != data_riwayat_kota ?
                                    `<div class="row mt-2" id="f_kota_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_kota_pengurus_${n}">Kota/Kabupaten <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="b_kota_pengurus_${n}"
                                                name="b_kota_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <select class="form-control custom-select" id="a_kota_pengurus_${n}"
                                                name="a_kota_pengurus[]">
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>` : '' 
                                    }
                                    
                                    ${data_kecamatan != data_riwayat_kecamatan ?
                                    `<div class="row mt-2" id="f_kecamatan_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_kecamatan_pengurus_${n}">Kecamatan <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control" id="b_kecamatan_pengurus_${n}" name="b_kecamatan_pengurus[]"
                                                value="${data_kecamatan}">
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control" id="a_kecamatan_pengurus_${n}" name="a_kecamatan_pengurus[]"
                                                value="${data_riwayat_kecamatan}">
                                        </div>
                                    </div>` : ''
                                    }
                
                                    ${data_kelurahan != data_riwayat_kelurahan ?
                                    `<div class="row mt-2" id="f_kelurahan_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_kelurahan_pengurus_${n}">Kelurahan <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control" id="b_kelurahan_pengurus_${n}" name="b_kelurahan_pengurus[]"
                                                value="${data_kelurahan}">
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control" id="a_kelurahan_pengurus_${n}" name="a_kelurahan_pengurus[]"
                                                value="${data_riwayat_kelurahan}">
                                        </div>
                                    </div>` : '' 
                                    }
                
                                    ${data_kode_pos != data_riwayat_kode_pos ?
                                    `<div class="row mt-2" id="f_kode_pos_pengurus_${n}">
                                        <div class="col-sm-12">
                                            <label for="b_kode_pos_pengurus_${n}">Kode Pos <i class="text-danger">*</i></label>
                                        </div>
                                        {{-- before --}}
                                        <div class="col">
                                            <input class="form-control" type="text" id="b_kode_pos_pengurus_${n}"
                                                name="b_kode_pos_pengurus[]" placeholder="--" value=${data_kode_pos}>
                                        </div>
                                        <div class="col-md-1 my-auto text-center">
                                            <i class="fa fa-arrow-right m-0 p-0" aria-hidden="true"></i>
                                        </div>
                                        {{-- after --}}
                                        <div class="col">
                                            <input class="form-control" type="text" id="a_kode_pos_pengurus_${n}"
                                                name="a_kode_pos_pengurus[]" placeholder="--"
                                                value="${data_riwayat_kode_pos}">
                                        </div>
                                    </div>` : ''
                                    }
                                    
                                    {{-- START: Foto Pengurus 1 --}}
                                    {{-- <div class="row">
                                        <div class="col-12">
                                            <h6 class="line my-4 font-weight-bold">Foto Pengurus &nbsp</h6>
                                        </div>
                
                                        <div id="add-foto-pengurus-${n}"></div>
                                    </div> --}}
                                    {{-- END: Foto Pengurus 1 --}}
                                    {{-- END: Baris 7 --}}
                                </div>
                
                            </div>
                
                        </div>
                </div>
            `
            $("#layout-pengurus").append(pengurus_html);

            // Jenis kelamin
            $(`#b_jns_kelamin_pengurus_${n}`).val(data_jenis_kelamin)
            $(`#a_jns_kelamin_pengurus_${n}`).val(data_riwayat_jenis_kelamin)

            // Pendidikan Terkahir
            $(`#b_pendidikan_terakhir_pengurus_${n}`).val(data_pendidikan_terakhir)
            $(`#a_pendidikan_terakhir_pengurus_${n}`).val(data_riwayat_pendidikan_terakhir)

            //Jabatan
            $(`#b_jabatan_pengurus_${n}`).val(data_jabatan)
            $(`#a_jabatan_pengurus_${n}`).val(data_riwayat_jabatan)

            // Provinsi
            $(`#b_provinsi_pengurus_${n}`).val(data_provinsi)
            $(`#a_provinsi_pengurus_${n}`).val(data_riwayat_provinsi)

            // Kota
            getKota(data_provinsi, data_kota, `b_kota_pengurus_${n}`)
            getKota(data_riwayat_provinsi, data_riwayat_kota, `a_kota_pengurus_${n}`)

            $(`#b_kota_pengurus_${n}`).val(data_kota)
            $(`#a_kota_pengurus_${n}`).val(data_riwayat_kota)

            // Agama
            $(`#b_agama_pengurus_${n}`).val(data_agama)
            $(`#a_agama_pengurus_${n}`).val(data_riwayat_agama)
            
            is_pengurus_exist += 1
            
        } else {
            
        }

        if (is_pengurus_exist > 0) {
            $('.layout_informasi_pengurus').removeClass('d-none')
            $('#blank_data').addClass('d-none')
        } else {
            $('.layout_informasi_pengurus').addClass('d-none')
            $('#blank_data').removeClass('d-none')
        }

        if (total_informasi_alamat_pengurus > 0) {
            $(`.layout_informasi_alamat_pengurus_${n}`).removeClass('d-none')
        } else {
            $(`.layout_informasi_alamat_pengurus_${n}`).addClass('d-none')
        }

        $('.form-control').prop('disabled', true)
        $('input[type=search]').prop('disabled', false)
        total_informasi_alamat_pengurus = 0
        total_informasi_pengurus = 0
    }

    const getKota = (provinsi, kota, kota_id) => {
        $.ajax({
            url: "/admin/getKota/" + provinsi,
            method: "get",
            success: function(data) {
                $.each(data.kota, function(index, value) {
                    if (value.kode_kota == kota) {
                        var select = 'selected=selected';
                    }
                    $('#'+kota_id).append(
                        '<option value="' + value.kode_kota + '"' + select + '>' + value.nama_kota + '</option>'
                    );
                })
            }
        });
    }

    const cal_informasi_pengurus = (x, n, data_pengurus_investor, data_riwayat_pengurus_investor) => {
        
        if (x == 1) { // pengurus investor
            
            (data_pengurus_investor.nama_pengurus ? data_pengurus_investor.nama_pengurus : '')                                              != (data_riwayat_pengurus_investor.nama_pengurus ? data_riwayat_pengurus_investor.nama_pengurus : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.jenis_kelamin ? data_pengurus_investor.jenis_kelamin : '')                                              != (data_riwayat_pengurus_investor.jenis_kelamin ? data_riwayat_pengurus_investor.jenis_kelamin : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.nomor_ktp ? data_pengurus_investor.nomor_ktp : '')                                                      != (data_riwayat_pengurus_investor.nomor_ktp ? data_riwayat_pengurus_investor.nomor_ktp : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.tempat_lahir ? data_pengurus_investor.tempat_lahir : '')                                                != (data_riwayat_pengurus_investor.tempat_lahir ? data_riwayat_pengurus_investor.tempat_lahir : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.tgl_lahir && data_pengurus_investor.tgl_lahir != "0000-00-00" ? data_pengurus_investor.tgl_lahir : '')  != (data_riwayat_pengurus_investor.tgl_lahir && data_riwayat_pengurus_investor.tgl_lahir != "0000-00-00" ? data_riwayat_pengurus_investor.tgl_lahir : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.no_hp ? data_pengurus_investor.no_hp : '')                                                              != (data_riwayat_pengurus_investor.no_hp ? data_riwayat_pengurus_investor.no_hp : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.agama ? data_pengurus_investor.agama : '')                                                              != (data_riwayat_pengurus_investor.agama ? data_riwayat_pengurus_investor.agama : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.pendidikan_terakhir ? data_pengurus_investor.pendidikan_terakhir : '')                                  != (data_riwayat_pengurus_investor.pendidikan_terakhir ? data_riwayat_pengurus_investor.pendidikan_terakhir : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.npwp ? data_pengurus_investor.npwp : '')                                                                != (data_riwayat_pengurus_investor.npwp ? data_riwayat_pengurus_investor.npwp : '') ? (total_informasi_pengurus += 1) : '';
            (data_pengurus_investor.jabatan ? data_pengurus_investor.jabatan : '')                                                          != (data_riwayat_pengurus_investor.jabatan ? data_riwayat_pengurus_investor.jabatan : '') ? (total_informasi_pengurus += 1) : '';
            
            (data_pengurus_investor.alamat ? data_pengurus_investor.alamat : '')                                                            != (data_riwayat_pengurus_investor.alamat ? data_riwayat_pengurus_investor.alamat : '') ? (total_informasi_alamat_pengurus += 1) : '';
            (data_pengurus_investor.provinsi  ? data_pengurus_investor.provinsi : '')                                                       != (data_riwayat_pengurus_investor.provinsi ? data_riwayat_pengurus_investor.provinsi : '') ? (total_informasi_alamat_pengurus += 1) : '';
            (data_pengurus_investor.kota ? data_pengurus_investor.kota : '')                                                                != (data_riwayat_pengurus_investor.kota ? data_riwayat_pengurus_investor.kota : '') ? (total_informasi_alamat_pengurus += 1) : '';
            (data_pengurus_investor.kecamatan ? data_pengurus_investor.kecamatan : '')                                                      != (data_riwayat_pengurus_investor.kecamatan ? data_riwayat_pengurus_investor.kecamatan : '') ? (total_informasi_alamat_pengurus += 1) : '';
            (data_pengurus_investor.kelurahan ? data_pengurus_investor.kelurahan : '')                                                      != (data_riwayat_pengurus_investor.kelurahan ? data_riwayat_pengurus_investor.kelurahan : '') ? (total_informasi_alamat_pengurus += 1) : '';
            (data_pengurus_investor.kode_pos ? data_pengurus_investor.kode_pos : '')                                                        != (data_riwayat_pengurus_investor.kode_pos ? data_riwayat_pengurus_investor.kode_pos : '') ? (total_informasi_alamat_pengurus += 1) : '';

        } else { // Pengurus Badan Hukum

        }
        return total_informasi_pengurus + total_informasi_alamat_pengurus
    }
</script>
@endsection