@extends('layouts.admin.master')

@section('title', 'Panel Admin')

@section('content')

<style>
    .largerCheckbox {
        width: 15px;
        height: 15px;
    }

    .modal-xl {
        max-width: 95% !important;
    }
</style>

<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Legalitas Pendanaan - Biaya-biaya</h1>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <!-- START: Baris 1 -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_informasi_legalitas">Informasi Legalitas &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_legal">Nama Legal</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_legal" name="nama_legal" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tanggal_legalitas">Tanggal Legalitas</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="tanggal_legalitas" name="tanggal_legalitas" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-ipdipp">Id Penerima Dana | Id Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="ipdipp" name="ipdipp" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_penerima_dana">Nama Penerima Dana</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_penerima_dana" name="nama_penerima_dana" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-plafon_pengajuan_pendanaan">Plafon Pengajuan Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="plafon_pengajuan_pendanaan" name="plafon_pengajuan_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-fasilitas_pendanaan">Fasilitas Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="fasilitas_pendanaan" name="fasilitas_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-jangka_waktu_pendanaan">Jangka Waktu Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="jangka_waktu_pendanaan" name="jangka_waktu_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kategori_penerima_pendanaan">Kategori Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-1">:</div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="kategori_penerima_pendanaan" name="kategori_penerima_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- end informasi legalitas -->
                    <!-- start biaya biaya -->
                    <div class="col-12 mb-4">
                        <br>
                        <div class="form-check form-check-inline line">
                            <label class="form-check-label text-black h3" for="form_biaya_biaya">Biaya - biaya &nbsp</label>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nomor_naup">Nomor NAUP</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nomor_naup" name="nomor_naup" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-skema_pengajuan">Skema Pengajuan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="skema_pengajuan" name="skema_pengajuan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_penerima_pendanaan">Nama Penerima Pendanaan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_penerima_pendanaan" name="nama_penerima_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-nama_pasangan">Nama Pasangan</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nama_pasangan" name="nama_pasangan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <br>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="wizard-progress2-judul_biaya">Biaya - biaya yang timbul untuk Pengajuan Pendanaan ini adalah sebagai berikut : &nbsp</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-md-12">
                            <table id="pengajuKPR_data" class="table table-striped table-bordered table-responsive-sm">
                                <thead>
                                    <tr>
                                        <th class="col-1">No</th>
                                        <th class="col-3">Rincian Biaya</th>
                                        <th class="col-3">Keterangan</th>
                                        <th class="col-2">Jumlah</th>
                                        <th class="col-3">Nama Rekanan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Biaya Administrasi</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_administrasi" name="ket_biaya_administrasi" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_administrasi" name="jumlah_biaya_administrasi" readonly>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Biaya KJPP</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_kjpp" name="ket_biaya_kjpp" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_kjpp" name="jumlah_biaya_kjpp" readonly>
                                        </td>
                                        <td>
                                            <select class="form-control" id="rekanan_biaya_kjpp" name="rekanan_biaya_kjpp" readonly>
                                                <option value="">Pilih</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Biaya Notaris</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_notaris" name="ket_biaya_notaris" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_notaris" name="jumlah_biaya_notaris" readonly>
                                        </td>
                                        <td>
                                            <select class="form-control" id="rekanan_biaya_notaris" name="rekanan_biaya_notaris" readonly>
                                                <option value="">Pilih</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Biaya Asuransi Kebakaran</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_asuransi_kebakaran" name="ket_biaya_asuransi_kebakaran" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_asuransi_kebakaran" name="jumlah_biaya_asuransi_kebakaran" readonly>
                                        </td>
                                        <td>
                                            <select class="form-control" id="rekanan_biaya_asuransi_kebakaran" name="rekanan_biaya_asuransi_kebakaran" readonly>
                                                <option value="">Pilih</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Biaya Asuransi Jiwa</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_asuransi_jiwa" name="ket_biaya_asuransi_jiwa" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_asuransi_jiwa" name="jumlah_biaya_asuransi_jiwa" readonly>
                                        </td>
                                        <td>
                                            <select class="form-control" id="rekanan_biaya_asuransi_jiwa" name="rekanan_biaya_asuransi_jiwa" readonly>
                                                <option value="">Pilih</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Biaya Asuransi Pembiayaan</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_asuransi_pembiayaan" name="ket_biaya_asuransi_pembiayaan" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_asuransi_pembiayaan" name="jumlah_biaya_asuransi_pembiayaan" readonly>
                                        </td>
                                        <td>
                                            <select class="form-control" id="rekanan_biaya_asuransi_pembiayaan" name="rekanan_biaya_asuransi_pembiayaan" readonly>
                                                <option value="">Pilih</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Biaya Materai</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_materai" name="ket_biaya_materai" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_materai" name="jumlah_biaya_materai" readonly>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Biaya Lain-lain</td>
                                        <td>
                                            <input class="form-control" type="text" id="ket_biaya_lain_lain" name="ket_biaya_lain_lain" readonly>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="jumlah_biaya_lain_lain" name="jumlah_biaya_lain_lain" readonly>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <h6 style="margin-top:10px;">Total Biaya-biaya</h6>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" id="total_biaya" name="total_biaya" readonly>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <div align="justify">
                                Biaya-biaya yang timbul dalam pengajuan Pendanaan ini, harap disetorkan/ditransfer ke Rekening DSI, apabila Pendanaan tersebut tidak di terima oleh keputusan komita DSI, maka biaya-biaya tersebut akan di kembalikan seluruhnya kecuali biaya appraisal. Dan apabila Penerima Pendanaan yang mengundurkan diri setelah persetujuan Pendanaan, maka biaya tersebut tidak dapt dikembalikan. Syarat dan Ketentuan Berlaku.
                            </div>
                        </div>
                    </div>
                    <!-- end biaya biaya -->
                    <!-- start keterangan -->
                    <!-- end keterangan -->
                    <!-- button kembali / simpan -->
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('legal.daftarpekerjaan')}}"><button type="submit" class="btn btn-block btn-success btn-lg" id="kembali">Kembali</button></a>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-block btn-success btn-lg" id="simpan">Simpan</button>
                    </div>
                    <!-- end button -->
                </div>

            </div>
        </div>
    </div>
</div>
</div><!-- .content -->

<!-- 2. GOOGLE JQUERY JS v3.2.1  JS !-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
<script src="/admin/assets/js/lib/data-table/jszip.min.js"></script>
<script src="/admin/assets/js/lib/data-table/pdfmake.min.js"></script>
<script src="/admin/assets/js/lib/data-table/vfs_fonts.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.html5.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.print.min.js"></script>
<script src="/admin/assets/js/lib/data-table/buttons.colVis.min.js"></script>
<script src="/admin/assets/js/lib/data-table/datatables-init.js"></script>

<script src="/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/tinymce/js/tinymce/jquery.tinymce.min.js"></script>

<script type="text/javascript">
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });
</script>
<script type="text/javascript">
    $(document).ready(function() {

        //     var pengajuKPR_table = $('#pengajuKPR_data').DataTable({
        //         searching: true,
        //         processing: true,
        //         "order": [
        //             [1, "asc"]
        //         ],
        //         ajax: {
        //             url: '/admin/adr/ListPengajuan',
        //             dataSrc: 'data'
        //         },
        //         paging: true,
        //         info: true,
        //         lengthChange: false,
        //         pageLength: 10,
        //         columns: [{
        //                 data: 'pengajuan_id'
        //             },
        //             {
        //                 data: null,
        //                 render: function(data, type, row, meta) {
        //                     return meta.row + 1;
        //                 }
        //             },
        //             {
        //                 data: 'tgl_pengajuan'
        //             },
        //             {
        //                 data: 'nama_cust'
        //             },
        //             {
        //                 data: 'tipe_pendanaan'
        //             },
        //             {
        //                 data: 'tujuan_pembiayaan'
        //             },
        //             {
        //                 data: null,
        //                 render: function(data, type, row, meta) {
        //                     return '<p class=text-right>' + row.nilai_pengajuan + '</p>';
        //                 }
        //             },
        //             {
        //                 data: null,
        //                 render: function(data, type, row, meta) {
        //                     if (row.stts == 0 || row.stts == 3) {
        //                         return '<button id="tombol" class="btn btn-secondary btn-block">Menunggu Verifikasi</button>';
        //                     } else if (row.stts == 2 || row.stts == 5) {
        //                         return '<button id="tombol" class="btn btn-danger btn-block">ditolak</button>';
        //                     } else if (row.stts == 1) {
        //                         return '<button id="tombol" class="btn btn-success btn-block">diterima</button>';
        //                     }
        //                 }
        //             },

        //         ],
        //         columnDefs: [{
        //             targets: [0],
        //             visible: false
        //         }]
        //     })
        //     var id;

        //     $('#pengajuKPR_data tbody').on('click', '#tombol', function() {
        //         var data = pengajuKPR_table.row($(this).parents('tr')).data();
        //         idPengajuan = data.pengajuan_id;
        //         brw_id = data.brw_id;
        //         window.location.href = "../adr/detailPengajuan/" + idPengajuan + "/" + brw_id;
        //     })

        //     // view
        //     $('#pengajuKPR_data tbody').on('click', '#generate', function() {
        //         var data = pengajuKPR_table.row($(this).parents('tr')).data();
        //         $(this).prop('disabled', true);
        //         did = data.id;
        //         $.ajax({
        //             url: "manage_payout/" + did,
        //             method: "post",
        //             success: function(data) {
        //                 if (data.Success) {
        //                     swal('Berhasil', 'Kembali Dana Selesai', 'success');
        //                     window.location.reload();

        //                 }
        //             }
        //         })

        //     })

    });
</script>


@endsection