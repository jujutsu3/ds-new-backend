@extends('layouts.admin.master')

@section('title', 'Analisa Kelengkapan Data')
<link href="{{ asset('assetsBorrower/js/plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
@section('content')
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Analisa Kelengkapan Data</h1>
                </div>
            </div>
        </div>
    </div>


    <div class="content mt-3">
        <div class="row">
            <div class="col-12 collapse" id="box-search">
                <div class="card panel-default">
                    <div class="card-header clearfix">
                        <strong>Pencarian Lanjut </strong>
                        <div class="float-right">
                            <button class="btn btn-md btn-default" data-toggle="collapse" data-target="#box-search"><i
                                    class="fa fa-minus icon"></i> Hide</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            {!! Form::label('Nama', 'Nama Penerima Pendana ', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::text('filter_nama_penerima', null, ['id' => 'filter_nama_penerima', 'class' => 'form-control', 'placeholder' => 'ketik disini']) !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('Jenis Pendanaan', 'Jenis Pendanaan', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('filter_tipe_id', $tipe_pendanaan, null, ['id' => 'filter_tipe_id', 'placeholder' => 'Pilih Semua Jenis Pendanaan', 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group row">
                            {!! Form::label('Tanggal Pengajuan', 'Periode Tanggal Pengajuan', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"
                                            style="font-size: 1.5rem; !important;"> <i
                                                class="glyphicon glyphicon-calendar fa fa-calendar"></i> </span>
                                    </div>
                                    <input type="text" class="form-control" id="filterDate" autocomplete="off" placeholder="klik tanggal disini" />
                                </div>
                            </div>
                            <div class="col-sm-3"><input type="checkbox" value="1" id="filter_all_tanggal"
                                    class="form-control-sm" /> Pilih Semua Periode Tanggal Pengajuan </div>
                        </div>
                        <div class="form-group row">
                            {!! Form::label('Status', 'Status', ['class' => 'col-sm-3 col-form-label']) !!}
                            <div class="col-sm-9">
                                {!! Form::select('filter_status', ['11' => 'Baru', '12' => 'Ditolak', '3'=> 'Selesai'], null, ['id' => 'filter_status', 'placeholder' => 'Pilih Semua Status', 'class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3">
                                &nbsp;
                            </div>
                            <div class="col-sm-9">
                                <button type="button" class="btn btn-primary pull-left" id="js-Cari"><i
                                        class="fa fa-search icon" style="font-size:14px"></i> Cari</button>&nbsp;
                                <button type="button" onclick="location.reload();" class="btn btn-secondary">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('progressadd'))
                    <div class="alert alert-danger">
                        {{ session()->get('progressadd') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('updatedone'))
                    <div class="alert alert-success">
                        {{ session()->get('updatedone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('createdone'))
                    <div class="alert alert-info">
                        {{ session()->get('createdone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Daftar Pengajuan</strong>
                        <div class="float-right">
                            <a href="#" data-toggle="collapse" data-target="#box-search">
                                <button class="btn btn-md btn-default"><i class="fa fa-search icon"></i> Pencarian
                                    Lanjut</button></a>
                        </div>
                    </div>
                    <div class="card-body">

                        <!-- table select all admin -->
                        <table id="pengajuKPR_data" class="table table-striped table-bordered table-responsive-sm">
                            <thead>
                                <tr>
                                    <th style="display: none;">Id</th>
                                    <th>No</th>
                                    <th>Tanggal Pengajuan</th>
                                    <th>Penerima Pendanaan</th>
                                    <th>Jenis Pendanaan</th>
                                    <th>Tujuan Pendanaan</th>
                                    <th>Nilai Pengajuan Pendanaan</th>
                                    <th>Pekerjaan</th>
                                    <th class="text-center">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <!-- end of table select all -->
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .content -->



    <style>
        .modal-xl {
            max-width: 95% !important;
        }

    </style>


    <div class="modal fade" id="view_detil" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Daftar Pendana</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row ">
                        <div class="col-lg-2">Nama</div>
                        <div class="col-lg-2">Nama Rek</div>
                        <div class="col-lg-2">Bank Rek</div>
                        <div class="col-lg-1">Nomer Rek</div>
                        <div class="col-lg-2">Tanggal Pendanaan</div>
                        <div class="col-lg-2">Jumlah Pendanaan</div>
                        <div class="col-lg-1">Detil</div>

                    </div>
                    <hr>
                    <div class="detil_payout p-1">
                    </div>

                </div>
                <div class="modal-footer footer_payout">
                </div>
            </div>
        </div>
    </div>



    <!-- start modal show payout -->
    <div class="modal fade" id="proses_detil" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mediumModalLabel">Proses Pembayaran <button class="btn btn-info"
                            id='cetak_payout'>Cetak Payout</button></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row ">
                        <div class="col-lg-3">Bulan Imbal Hasil</div>
                        <div class="col-lg-2">Tanggal Imbal Hasil</div>
                        <div class="col-lg-2">Status</div>
                        <!-- <div class="col-lg-2">Keterangan</div> -->
                        <div class="col-lg-1">Detil</div>
                        {{-- <div class="col-lg-1">Aksi</div> --}}
                    </div>
                    <hr>
                    <div class="show_payout p-1"></div>
                    <div class="payout_footer p-1"></div>
                </div>
                {{-- <div class="modal-footer payout_footer"> --}}
            </div>
        </div>
    </div>
    </div>
    <!-- end modal show payout -->

    <script type="text/javascript" src="{{url('assetsBorrower/js/plugins/moment/moment.min.js')}}"></script>
    <script src="{{url('assetsBorrower/js/plugins/daterangepicker/js/daterangepicker.js')}}"></script>
    <!-- end modal payout terdekat -->
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            var d = new Date();
            var n = d.getTime();

            var start = moment();
            var end = moment();

            var startDate;
            var endDate;

            $("#filter_all_tanggal").click(function() {
                var checked_status = this.checked;
                if (checked_status == true) {
                    $("#filterDate").prop("disabled", true);
                    $("#filterDate").val("");
                } else {
                    $("#filterDate").prop("disabled", false);
                }
            });

            $("#filterDate").daterangepicker({
                autoUpdateInput: false,
                startDate: start,
                endDate: end,
                locale: {
                    cancelLabel: 'Clear',
                    format: 'DD/MM/YYYY'
                },
                ranges: {
                    'Hari ini': [moment(), moment()],
                    'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
                    '1 Bulan Terakhir': [moment().subtract(29, 'days'), moment()],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                        'month').endOf('month')]
                }
            }, function(start, end) {
                startDate = start.format('YYYY-MM-DD');
                endDate = end.format('YYYY-MM-DD');
            });


            $('#filterDate').on('apply.daterangepicker', function(ev, picker) {
                startDate = picker.startDate.format('YYYY-MM-DD');
                endDate = picker.endDate.format('YYYY-MM-DD');
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            });


            $('#js-Cari').click(function(event) {
                event.preventDefault();
                var search_filter = {}
                search_filter['filter_nama_penerima'] = $('#filter_nama_penerima').val();
                search_filter['filter_tipe_id'] = $('#filter_tipe_id').val();

                if ($('#filterDate').val() != '') {
                    search_filter['filter_start_date'] = startDate;
                    search_filter['filter_end_date'] = endDate;
                }

                search_filter['filter_status'] = $('#filter_status').val();
                search_filter['filter_all_tanggal'] =  $("#filter_all_tanggal").is(":checked") ? true : false;

                $('#pengajuKPR_data').DataTable().destroy();
                fill_datatable(search_filter);
            });


            fill_datatable();

            function fill_datatable(search_filter = {}) {

                var pengajuKPR_table = $('#pengajuKPR_data').DataTable({
                    bAutoWidth: false,
                    dom: 'Bfrtip',
                    searching: true,
                    processing: true,
                    "order": [
                        [1, "asc"]
                    ],
                    ajax: {
                        url: '/admin/analisa/ListPengajuan/kelengkapan',
                        data: { 'search_filter': JSON.stringify(search_filter)}
                    },
                    paging: true,
                    info: true,
                    lengthChange: false,
                    pageLength: 10,
                    buttons: [{
                        extend: 'excelHtml5',
                        exportOptions: {
                            modifier: {
                                page: 'all',
                                search: 'none',
                                selected: false
                            },
                            stripNewlines: false
                        },
                        text: 'Export Excel',
                        className: "btn btn-primary",
                        title: 'Daftar Pengajuan',
                        filename: function() {
                            return 'daftar_analisa_kelengkapan_' + d.getDate() + '_' + d
                                .getTime();
                        },

                    }],
                    columns: [{
                            data: 'pengajuan_id'
                        },
                        {
                            data: null,
                            render: function(data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {
                            data: 'tgl_pengajuan',
                            'render': function(data, type) {
                                return type === 'sort' ? data : moment(data).format('D/MM/YYYY');
                            }
                        },
                        {
                            data: 'nama_cust'
                        },
                        {
                            data: 'tipe_pendanaan'
                        },
                        {
                            data: 'tujuan_pembiayaan'
                        },
                        {
                            data: 'nilai_pengajuan',
                            render: $.fn.dataTable.render.number(',', '.', 2, 'Rp ')
                        },
                        {
                            data: 'pekerjaan'
                        },
                        {
                            data: null,
                            render: function(data, type, row, meta) {

                                let link_detail_pengajuan = "../analisa/kelengkapan/" + row.pengajuan_id + "/" + row.brw_id;
                            
                                if (row.stts == 11) {
                                    return '<a href="'+link_detail_pengajuan+'"  class="btn btn-secondary btn-block">Baru</a>';
                                } else if (row.stts == 12) {
                                    return '<a href="'+link_detail_pengajuan+'"  class="btn btn-danger btn-block">Ditolak</a>';
                                } else if (row.stts == 3) {
                                    return '<a href="'+link_detail_pengajuan+'"  class="btn btn-success btn-block">Selesai</a>';
                                }else{
                                    return '';
                                }
                              
                            }
                        },

                    ],
                    columnDefs: [{
                        targets: [0],
                        visible: false
                    }]
                })

            }
            var id;





        });
    </script>


@endsection
