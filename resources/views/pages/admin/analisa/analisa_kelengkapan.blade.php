@extends('layouts.admin.master')
@section('title', 'Detail Analisa Kelengkapan Data')

@section('style')

    <style>
        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            background-color: #fff;
        }

        ul.breadcrumb li {
            display: inline;
            font-size: 18px;
        }

        ul.breadcrumb li+li:before {
            padding: 8px;
            color: black;
            content: "/\00a0";
        }

        ul.breadcrumb li a {
            color: #0275d8;
            text-decoration: none;
        }

        ul.breadcrumb li a:hover {
            color: #01447e;
            text-decoration: underline;
        }

        button[type=submit] {
            font-size: 14px;
        }

    </style>
@endsection
@section('content')


    <!-- Main Container -->
    <ul class="breadcrumb">
        <li><a href="{{ route('analisa.kelengkapan') }}">Analisa Kelengkapan Data</a></li>
        <li>Detail</li>
    </ul>
    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                @if (session()->has('progressadd'))
                    <div class="alert alert-danger">
                        {{ session()->get('progressadd') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('updatedone'))
                    <div class="alert alert-success">
                        {{ session()->get('updatedone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session()->has('createdone'))
                    <div class="alert alert-info">
                        {{ session()->get('createdone') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="form-group" style="width:25%; float:left;">

                            @if($brw_type  === 1)
                                <button type="button"
                                class="btnProfile btn btn-default btn-md btn-block btn-success disabled"><strong
                                    class="card-title">Profile Pribadi</strong></button>

                                @else 
                                    <button type="button"
                                    class="btnProfile btn btn-default btn-md btn-block btn-success disabled"><strong
                                        class="card-title">Profile Badan Hukum/Perusahaan</strong></button>
                                @endif 
                           
                          </div>

                        <div class="form-group" style="width:25%; float:left;">
                            <button type="button" class="btnPengajuan btn btn-default btn-md btn-block btn-success"><strong
                                    class="card-title">Informasi Objek Pendanaan</strong></button>
                        </div>


                        @if($pengajuan->tipe_id === 1)
                            <div class="form-group" style="width:25%; float:left;">
                                <button type="button" class="btnRab btn btn-default btn-md btn-block btn-success"><strong
                                        class="card-title">Rencana Anggaran Belanja Barang</strong></button>
                            </div>

                        @endif

                        <div class="form-group" style="width:25%; float:left;">
                            <button type="button"
                                class="btnDokumenPendukung btn btn-default btn-md btn-block btn-success"><strong
                                    class="card-title">Dokumen Pendukung</strong></button>
                        </div>
                        <div class="form-group" style="width:25%; float:left;">
                            <button type="button" class="btnVerifikasi btn btn-default btn-md btn-block btn-success"><strong
                                    class="card-title">Verifikasi Kelayakan</strong></button>
                        </div>

                    </div>
            
                    @if($brw_type === 1)
                       @include('pages.admin.analisa._info_profile_individu')
                    @endif

                    @if($brw_type === 2)
                       @include('pages.admin.analisa._info_profile_badanhukum')
                    @endif 


                    @if($pengajuan->tipe_id === 1)
                      @include('pages.admin.analisa._info_rab')
                    @endif 

                    <!-- END TAB Profile -->
                    <!-- TAB PENGAJUAN -->

                    
                    @if($pengajuan->tipe_id === 1)
                        @include('pages.admin.analisa._info_pengajuan_danakonstruksi')
                    @endif 

                    @if($pengajuan->tipe_id === 2)
                      @include('pages.admin.analisa._info_pengajuan_danarumah')
                   @endif 

                    <!--Tab Dokumen Pendukung -->

                    @include('pages.admin.analisa.dokumen_pendukung')

                    <!-- TAB PENGAJUAN SELESAI-->
                    <!-- TAB VERIFIKASI -->
                    <div class="card-body tabverifikasi d-none">
                        <div class="row">
                            <div class="col-12 mb-4">
                                <br>
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Form
                                        Verifikasi Kelayakan &nbsp</label>
                                </div>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6" for="form_informasi_objek_Pendanaan">Biro
                                        Kredit &nbsp</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-namapengguna">Nilai &nbsp</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="number" min='1' max="5000" id="nilai_pefindo"
                                        name="nilai_pefindo" placeholder="ketik disini.." required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="wizard-progress2-namapengguna">Grade &nbsp</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control custom-select" name="grade_pefindo" id="grade_pefindo"
                                        required>
                                        <option value="" active> Pilih </option>
                                        <option value="A1"> A1 </option>
                                        <option value="A2"> A2 </option>
                                        <option value="A3"> A3 </option>
                                        <option value="B1"> B1 </option>
                                        <option value="B2"> B2 </option>
                                        <option value="B3"> B3 </option>
                                        <option value="C1"> C1 </option>
                                        <option value="C2"> C2 </option>
                                        <option value="C3"> C3 </option>
                                        <option value="D1"> D1 </option>
                                        <option value="D2"> D2 </option>
                                        <option value="D3"> D3 </option>
                                        <option value="E1"> E1 </option>
                                        <option value="E2"> E2 </option>
                                        <option value="E3"> E3 </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6"
                                        for="form_informasi_objek_Pendanaan">CredoLab &nbsp</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="skor_personal">Skor Personal &nbsp</label><label class="mandatory_label"
                                        style="color:red;">*</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" id="skor_personal" name="skor_personal" readonly>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="skor_pendanaan">Skor Pendanaan &nbsp</label><label class="mandatory_label"
                                        style="color:red;">*</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" id="skor_pendanaan" name="skor_pendanaan" readonly>
                                </div>
                            </div>
                        </div>
                        <!-- verijelas -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h6"
                                        for="verifikasi_data_verijelas">Verifikasi
                                        Data Verijelas &nbsp</label><label class="form-check-label">:</label>
                                </div>
                            </div>
                            <div class="col-md-2">

                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="checkbox" id="complete_id" value="complete_id" name="verijelas">
                                    <label for="complete_id"> COMPLETE ID</label> <br>
                                    <input type="checkbox" id="ocr" value="ocr" name="verijelas">
                                    <label for="ocr"> OCR </label><br>
                                    <input type="checkbox" id="npwp_verify" value="npwp_verify" name="verijelas">
                                    <label for="npwp_verify"> NPWP</label><br>
                                    {{-- <input type="checkbox" id="npwpa" value="npwpa" name="verijelas">
                                    <label for="npwpa"> NPWP A </label><br>
                                    <input type="checkbox" id="npwpc" value="npwpc" name="verijelas">
                                    <label for="npwpc"> NPWP C </label><br> --}}
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    {{-- <input type="checkbox" id="npwpd" value="npwpd" name="verijelas">
                                    <label for="npwpd"> NPWP D </label><br> --}}
                                    <input type="checkbox" id="workplace_verification_f" value="workplace_verification_f"
                                        name="verijelas">
                                    <label for="workplace_verification_f"> Workplace Verification F </label><br>
                                    <input type="checkbox" id="ngetive_list_verification_j"
                                        value="negative_list_verification_j" name="verijelas">
                                    <label for="ngetive_list_verification_j"> Negative List Verification J </label><br>
                                    {{-- <input type="checkbox" id="verify_property_k" value="verify_property_k"
                                        name="verijelas">
                                    <label for="verify_property_k"> Verify Property K </label><br> --}}
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-2"></div>
                            {{-- <div class="col-md-2">
                            <button type="button" data-toggle="modal" data-target="#loading_modal" data-backdrop="static" data-keyboard="false" href="#" class="btn btn-block btn-primary btn-sm"> <i class="fa fa-check" aria-hidden="true"></i>
                                Cek Verijelas </button>
                        </div> --}}
                            <div class="col-md-2">
                                <button type="button" data-toggle="modal" data-target="#verijelas_modal"
                                    class="btn btn-block btn-success btn-sm" id="resultVeriJelas"> <i
                                        class="fa fa-folder" aria-hidden="true"></i> Hasil Verijelas</button>
                            </div>


                        </div>
                        <!-- verijelas -->
                        <div class="row py-3">
                            <div class="col-md-4">
                                <label for="ftv">Financing To Value (LTV/FTV) &nbsp</label><label class="mandatory_label"
                                    style="color:red;">*</label>
                                <div class="form-group">
                                    <input class="form-control" type="number" min="1" max="100" id="ftv" name="ftv"
                                        placeholder="Prosentase %" required>
                                </div>
                            </div>


                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="catatan_verifikasi">Catatan Verifikasi Kelayakan &nbsp</label><label
                                        class="mandatory_label" style="color:red;">*</label>
                                    <textarea class="form-control" rows="4" cols="80" id="catatan_verifikasi"
                                        name="catatan_verifikasi" required></textarea>
                                </div>
                                <br>
                              
                            </div>

                         @if($brw_verifikator_pengajuan)   
                            <div class="col-12 mb-4">
                                <br>
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h5">Rekomendasi
                                        Verifikator &nbsp</label>
                                </div>
                                <hr>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="catatan_rekomendasi">Catatan Rekomendasi Verifikator &nbsp</label>
                                    <textarea class="form-control" rows="4" cols="80" id="catatan_rekomendasi"
                                        name="catatan_rekomendasi" readonly>{!! $brw_verifikator_pengajuan->catatan_rekomendasi !!}</textarea>
                                </div>
                                <br>
                             
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="catatan_rekomendasi" class="font-weight-bold">File Rekomendasi Verifikator &nbsp</label>
                                    <a href="{{ route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_verifikator_pengajuan->rekomendasi_file) ]) }}" class="btn btn-primary form-control" target="_blank"><i
                                        class="fa fa-eye" aria-hidden="true"></i> Lihat File</a>
                                </div>
                                <br>
                             
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tgl_kirim" class="font-weight-bold">Tanggal Kirim &nbsp</label>
                                    <input type="text" name="tgl_kirim" value="{{  \Carbon\Carbon::parse($brw_verifikator_pengajuan->tgl_kirim)->format('d-m-Y H:i:s') }}" class="form-control-plaintext" readonly>
                                </div>
                                <br>
                             
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="tgl_kirim" class="font-weight-bold"> Verifikator &nbsp</label>
                                    <input type="text" name="updated_by_verifikator" value="{{ $brw_verifikator_pengajuan->updated_by  }}" class="form-control-plaintext" readonly>
                                </div>
                                <br>
                             
                            </div>
                          
                         @endif    


                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="keputusan"><strong>Berdasarkan hasil Verifikasi ini, maka keputusan analis
                                            PT. Dana Syariah Indonesia adalah &nbsp;</strong></label><label
                                        class="mandatory_label" style="color:red;">*</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select class="form-control" name="keputusan" id="keputusan" required>
                                        {{-- @foreach($optionKeputusan as $key=>$val)
                                            <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach --}}
                                        <option value="">Pilih</option>
                                        <option value="1">Lanjut</option>
                                        <option value="12">Tolak</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>

                        
                            <div class="col-md-2">
                                <button class="btn btn-block btn-md btn-success" id="sebelumnya4">
                                    <i class="fa fa-fw fa-angle-left"></i>&nbsp;Sebelumnya</button>
                            </div>
                            <div class="col-md-8">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-block btn-success btn-lg" id="submitverfikasi"> <i
                                        class="fa fa-paper-plane" aria-hidden="true"></i> Kirim </button>
                            </div>
                        </div>
                    </div>
                    <!-- TAB VERIFIKASI SELESAI -->


                </div>

            </div>
        </div>
    </div>

    </div><!-- .content -->
    <!-- Modal box -->
    @include('layouts.modals.modal-loading')
    @include('layouts.modals.modal-verijelas')
    <!-- End modal box -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script> --}}
    <script>
        $('#selanjutnya1').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').removeClass('d-none');
            $('.btnProfile').removeClass('disabled');
            $('.btnPengajuan').addClass('disabled');
        });

        $('.jsSelanjutnya2').on('click',function(){

            if($("button").hasClass("btnRab")){
                $('.tabprofile').addClass('d-none');
                $('.tabverifikasi').addClass('d-none');
                $('.tabRab').removeClass('d-none');
                $('.tabpengajuan').addClass('d-none');

                $('.btnProfile').removeClass('disabled');
                $('.btnVerifikasi').removeClass('disabled');
                $('.btnRab').addClass('disabled');
                $('.btnPengajuan').removeClass('disabled');
                $('.btnVerifikasi').removeClass('disabled');
            }else{
                $('.tabpengajuan').addClass('d-none');
                $('.tabverifikasi').addClass('d-none');
                $('.tabDokumenPendukung').removeClass('d-none');

                $('.btnDokumenPendukung').addClass('disabled');
                $('.btnPengajuan').removeClass('disabled');
                $('.btnVerifikasi').removeClass('disabled');

            }
               
        });


        $('.jsSebelumnya2').on('click',function(){
            $('.tabprofile').removeClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabRab').addClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabDokumenPendukung').addClass('d-none');


            $('.btnProfile').addClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
   
       });

        $('#selanjutnya2').on('click', function() {

            $('.tabpengajuan').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabRab').addClass('d-none');
            $('.tabDokumenPendukung').removeClass('d-none');

            $('.btnDokumenPendukung').addClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnRab').removeClass('disabled');

        });

        $('#selanjutnya3').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabverifikasi').removeClass('d-none');
            $('.tabDokumenPendukung').addClass('d-none');

            $('.btnDokumenPendukung').removeClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').addClass('disabled');
            $('.btnProfile').removeClass('disabled');
        });

        $('#sebelumnya2').on('click', function() {

            if($("button").hasClass("btnPengajuan")){
                $('.tabprofile').addClass('d-none');
                $('.tabpengajuan').removeClass('d-none');
                $('.tabDokumenPendukung').addClass('d-none');
                $('.tabverifikasi').addClass('d-none');
                $('.tabRab').addClass('d-none');

                $('.btnProfile').removeClass('disabled');
                $('.btnPengajuan').addClass('disabled');
                $('.btnVerifikasi').removeClass('disabled');
                $('.btnDokumenPendukung').removeClass('disabled');
                $('.btnRab').removeClass('disabled');

            }else{
                $('.tabpengajuan').addClass('d-none');
                $('.tabprofile').removeClass('d-none');
                $('.btnPengajuan').removeClass('disabled');
                $('.btnProfile').addClass('disabled');

            }
           
        });


        $('#sebelumnya3').on('click', function() {

            if($("button").hasClass("btnRab")){
                $('.tabprofile').addClass('d-none');
                $('.tabpengajuan').addClass('d-none');
                $('.tabDokumenPendukung').addClass('d-none');
                $('.tabverifikasi').addClass('d-none');
                $('.tabRab').removeClass('d-none');


                $('.btnProfile').removeClass('disabled');
                $('.btnPengajuan').removeClass('disabled');
                $('.btnVerifikasi').removeClass('disabled');
                $('.btnDokumenPendukung').removeClass('disabled');
                $('.btnRab').addClass('disabled');

            }else{

                $('.tabverifikasi').addClass('d-none');
                $('.tabpengajuan').removeClass('d-none');
                $('.tabDokumenPendukung').addClass('d-none');

                $('.btnVerifikasi').removeClass('disabled');
                $('.btnPengajuan').addClass('disabled');
                $('.btnDokumenPendukung').removeClass('disabled');

            }

        });

        $('#sebelumnya4').on('click', function() {
            $('.tabverifikasi').addClass('d-none');
            $('.tabDokumenPendukung').removeClass('d-none');

            $('.btnVerifikasi').removeClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnDokumenPendukung').addClass('disabled');
        });
        $('.btnProfile').on('click', function() {
            $('.tabprofile').removeClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabDokumenPendukung').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabRab').addClass('d-none');

            $('.btnProfile').addClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnDokumenPendukung').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
        });
        $('.btnPengajuan').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').removeClass('d-none');
            $('.tabDokumenPendukung').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabRab').addClass('d-none');

            $('.btnProfile').removeClass('disabled');
            $('.btnPengajuan').addClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnDokumenPendukung').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
        });
        $('.btnVerifikasi').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabDokumenPendukung').addClass('d-none');
            $('.tabverifikasi').removeClass('d-none');

            $('.btnProfile').removeClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').addClass('disabled');
            $('.btnDokumenPendukung').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
        });

        $('.btnDokumenPendukung ').on('click', function() {

            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabDokumenPendukung').removeClass('d-none');
            $('.tabRab').addClass('d-none');

            $('.btnProfile').removeClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnDokumenPendukung').addClass('disabled');
            $('.btnRab').removeClass('disabled');

        });

        $('.btnRab').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabDokumenPendukung').addClass('d-none');
            $('.tabRab').removeClass('d-none');

            $('.btnProfile').removeClass('disabled');
            $('.btnRab').addClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnDokumenPendukung').removeClass('disabled');

        });

    
        $('#submitverfikasi').on('click', function() {
            let msg_box = ($('#keputusan').val() == '12'  ? 'Anda yakin ingin menolak pengajuan ini?' : 'Anda yakin akan melanjutkan proses ini ?');
                Swal.fire({
                    title: "Notifikasi",
                    text: msg_box,
                    type: "warning",
                    buttons: true,
                    showCancelButton: true,
                }).then(result => {
                    if (result.value == true) {
                        kirimData();
                    } else {
                        return false;
                    }
                });

            function kirimData() {

                var complete_id = 0;
                var npwp_verify = 0;
                var ocr = 0;
                var npwpa = 0;
                var npwpc = 0;
                var npwpd = 0;
                var brw_id = "{{ $brw_id }}";
                var workplace_verification_f = 0;
                var ngetive_list_verification_j = 0;
                var verify_property_k = 0;

                var pengajuanId = "{{ $pengajuanId }}";
                var nilai_pefindo = $('#nilai_pefindo').val();
                var grade_pefindo = $('#grade_pefindo').val();
                var skor_personal = $('#skor_personal').val();
                var skor_pendanaan = $('#skor_pendanaan').val();
                var ftv = $('#ftv').val();
                var catatan_verifikasi = $('#catatan_verifikasi').val();
                var keputusan = $('#keputusan').val();
                var id_field_verifikator_vendor = $('#id_field_verifikator_vendor').val();

                $.each($("input[name='verijelas']:checked"), function() {
                    if ($(this).val() == 'complete_id') {
                        complete_id = 1;
                    } else if ($(this).val() == 'ocr') {
                        ocr = 1;
                    } else if ($(this).val() == 'npwpa') {
                        npwpa = 1;
                    } else if ($(this).val() == 'npwpc') {
                        npwpc = 1;
                    } else if ($(this).val() == 'npwpd') {
                        npwpd = 1;
                    } else if ($(this).val() == 'workplace_verification_f') {
                        workplace_verification_f = 1;
                    } else if ($(this).val() == 'ngetive_list_verification_j') {
                        ngetive_list_verification_j = 1;
                    } else if ($(this).val() == 'verify_property_k') {
                        verify_property_k = 1;
                    } else if ($(this).val() == 'npwp_verify') {
                        npwp_verify = 1;
                    }
                });


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "/admin/analisa/kirimKelengkapan",
                    method: "POST",
                    dataType: 'JSON',
                    data: {
                        'brw_id': brw_id,
                        'pengajuanId': pengajuanId,
                        'nilai_pefindo': nilai_pefindo,
                        'grade_pefindo': grade_pefindo,
                        'complete_id': complete_id,
                        'ocr': ocr,
                        'npwpa': npwpa,
                        'npwpc': npwpc,
                        'npwpd': npwpd,
                        'npwp_verify': npwp_verify,
                        'workplace_verification_f': workplace_verification_f,
                        'ngetive_list_verification_j': ngetive_list_verification_j,
                        'verify_property_k': verify_property_k,
                        'skor_personal': skor_personal,
                        'skor_pendanaan': skor_pendanaan,
                        'ftv': ftv,
                        'catatan_verifikasi': catatan_verifikasi,
                        'id_field_verifikator_vendor': id_field_verifikator_vendor,
                        'keputusan': keputusan
                    },

                    beforeSend: () => {
                        Swal.fire({
                            html: '<h5>Mengirim Data ...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                Swal.showLoading();
                            }
                        });
                    },
                    // dataType: "text",
                    success: function(msg) {
                        if (msg.data == 1) {
                            window.location.href = "/admin/analisa/kelengkapan";
                        }
                        // console.log(msg.data);
                        // alert(msg.data);
                    }
                });
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            var brw_id = `{!! $brw_id !!}`;
            var pengajuanId = `{!! $pengajuanId !!}`;
            var status_pengajuan = `{!! $status_pengajuan !!}`;
            var brw_type = '{{ $brw_type }}';
            // informasi pribadi
            var nama = '{{ $nama }}';
            var jns_kelamin = `{!! $jns_kelamin !!}`;
            // var jabatan = `{!! $jabatan !!}`;
            var ktp = `{!! $ktp !!}`;
            var kk = `{!! $kk !!}`;
            var tempat_lahir = `{!! $tempat_lahir !!}`;
            var tgl_lahir = `{!! $tgl_lahir !!}`;
            var telepon = `{!! $telepon !!}`;
            var agama = `{!! $agama !!}`;
            var pendidikan_terakhir = `{!! $pendidikan_terakhir !!}`;
            var id_kawin = `{!! $id_kawin !!}`;
            if (id_kawin == 1) {
                $('.blokPasangan').removeClass('d-none');
            }
            var status_kawin = `{!! $status_kawin !!}`;
            var nm_ibu = `{!! $nm_ibu !!}`;
            var npwp = `{!! $npwp !!}`;

            // informasi alamat
            var alamat = `{!! $alamat !!}`;
            var kota = `{!! $kota !!}`;
            var kecamatan = `{!! $kecamatan !!}`;
            var kelurahan = `{!! $kelurahan !!}`;
            var kode_pos = `{!! $kode_pos !!}`;
            var provinsi = `{!! $provinsi !!}`;
            var status_rumah = `{!! $status_rumah !!}`;


            var domisili_alamat = `{!! $domisili_alamat !!}`;
            var domisili_provinsi = `{!! $domisili_provinsi !!}`;
            var domisili_kota = `{!! $domisili_kota !!}`;
            var domisili_kecamatan = `{!! $domisili_kecamatan !!}`;
            var domisili_kelurahan = `{!! $domisili_kelurahan !!}`;
            var domisili_kode_pos = `{!! $domisili_kode_pos !!}`;
            var domisili_status_rumah = `{!! $domisili_status_rumah !!}`;

            // informasi alamat domisili
            if (domisili_alamat != alamat && domisili_kode_pos != kode_pos) {
                $(".blokDomisili").removeClass('d-none');
            }

            // informasi rekening
            var brw_norek = `{!! $brw_norek !!}`;
            var brw_nm_pemilik = `{!! $brw_nm_pemilik !!}`;
            var brw_kd_bank = `{!! $brw_kd_bank !!}`;
            var kantorcabangpembuka = `{!! $kantorcabangpembuka !!}`;

            // informasi pasangan
            var pasangan_nama = `{!! $pasangan_nama !!}`;
            var pasangan_jenis_kelamin = `{!! $pasangan_jenis_kelamin !!}`;
            var pasangan_ktp = `{!! $pasangan_ktp !!}`;
            var pasangan_tempat_lahir = `{!! $pasangan_tempat_lahir !!}`;
            var pasangan_tanggal_lahir = `{!! $pasangan_tanggal_lahir !!}`;
            var pasangan_telepon = `{!! $pasangan_telepon !!}`;
            var pasangan_agama = `{!! $pasangan_agama !!}`;
            var pasangan_pendidikan_terakhir = `{!! $pasangan_pendidikan_terakhir !!}`;
            var pasangan_npwp = `{!! $pasangan_npwp !!}`;
            var pasangan_alamat = `{!! $pasangan_alamat !!}`;
            var pasangan_provinsi = `{!! $pasangan_provinsi !!}`;
            var pasangan_kota = `{!! $pasangan_kota !!}`;
            var pasangan_kecamatan = `{!! $pasangan_kecamatan !!}`;
            var pasangan_kelurahan = `{!! $pasangan_kelurahan !!}`;
            var pasangan_kode_pos = `{!! $pasangan_kode_pos !!}`;

            // informasi pasangan pekerjaan
            var pekerjaan_pasangan = `{!! $pekerjaan_pasangan !!}`;
            var bidang_pekerjaan_pasangan = `{!! $bidang_pekerjaan_pasangan !!}`;
            var bidang_online_pasangan = `{!! $bidang_online_pasangan !!}`;
            $('#pekerjaan_pasangan').val(pekerjaan_pasangan);
            $('#bidang_pekerjaan_pasangan').val(bidang_pekerjaan_pasangan);
            $('#bidang_online_pasangan').val(bidang_online_pasangan);

            var nama_perusahaan_pasangan = `{!! $nama_perusahaan_pasangan !!}`;
            var sumberpengembaliandana_pasangan = `{!! $sumberpengembaliandana_pasangan !!}`;
            var bentuk_badan_usaha_pasangan = `{!! $bentuk_badan_usaha_pasangan !!}`;
            var status_kepegawaian_pasangan = `{!! $status_kepegawaian_pasangan !!}`;
            var usia_perusahaan_pasangan = `{!! $usia_perusahaan_pasangan !!}`;
            var departemen_pasangan = `{!! $departemen_pasangan !!}`;
            var jabatan_pasangan = `{!! $jabatan_pasangan !!}`;
            var tahun_bekerja_pasangan = `{!! $tahun_bekerja_pasangan !!}`;
            var bulan_bekerja_pasangan = `{!! $bulan_bekerja_pasangan !!}`;
            var nip_pasangan = `{!! $nip_pasangan !!}`;
            var nama_hrd_pasangan = `{!! $nama_hrd_pasangan !!}`;
            var no_fixed_line_hrd_pasangan = `{!! $no_fixed_line_hrd_pasangan !!}`;
            var no_telpon_usaha_pasangan = `{!! $no_telpon_usaha_pasangan !!}`;
            var alamat_perusahaan_pasangan = `{!! $alamat_perusahaan_pasangan !!}`;
            var rt_perusahaan_pasangan = `{!! $rt_perusahaan_pasangan !!}`;
            var rw_perusahaan_pasangan = `{!! $rw_perusahaan_pasangan !!}`;
            var provinsi_perusahaan_pasangan = `{!! $provinsi_perusahaan_pasangan !!}`;
            var kabupaten_perusahaan_pasangan = `{!! $kabupaten_perusahaan_pasangan !!}`;
            var kecamatan_perusahaan_pasangan = `{!! $kecamatan_perusahaan_pasangan !!}`;
            var kelurahan_perusahaan_pasangan = `{!! $kelurahan_perusahaan_pasangan !!}`;
            var kodepos_perusahaan_pasangan = `{!! $kodepos_perusahaan_pasangan !!}`;
            var tahun_pengalaman_bekerja_pasangan = `{!! $tahun_pengalaman_bekerja_pasangan !!}`;
            var bulan_pengalaman_bekerja_pasangan = `{!! $bulan_pengalaman_bekerja_pasangan !!}`;
            var penghasilan_pasangan = `{!! $penghasilan_pasangan !!}`;
            var biaya_hidup_pasangan = `{!! $biaya_hidup_pasangan !!}`;
            // fix
            $('#nama_perusahaan_pasangan').val(nama_perusahaan_pasangan);
            $('#bentuk_badan_usaha_pasangan').val(bentuk_badan_usaha_pasangan);
            $('#status_kepegawaian_pasangan').val(status_kepegawaian_pasangan);
            $('#usia_perusahaan_pasangan').val(usia_perusahaan_pasangan);
            $('#departemen_pasangan').val(departemen_pasangan);
            $('#jabatan_pasangan').val(jabatan_pasangan);
            $('#tahun_bekerja_pasangan').val(tahun_bekerja_pasangan);
            $('#bulan_bekerja_pasangan').val(bulan_bekerja_pasangan);
            $('#nip_pasangan').val(nip_pasangan);
            $('#nama_hrd_pasangan').val(nama_hrd_pasangan);
            if (no_fixed_line_hrd_pasangan.substring(0, 3) == '620') {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(3));
            } else if (no_fixed_line_hrd_pasangan.substring(0, 2) == '62') {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(2));
            } else if (no_fixed_line_hrd_pasangan.substring(0, 1) == '0') {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(1));
            } else {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan);
            }
            if (no_telpon_usaha_pasangan.substring(0, 3) == '620') {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(3));
            } else if (no_telpon_usaha_pasangan.substring(0, 2) == '62') {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(2));
            } else if (no_telpon_usaha_pasangan.substring(0, 1) == '0') {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(1));
            } else {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan);
            }

            $('#alamat_perusahaan_pasangan').val(alamat_perusahaan_pasangan);
            $('#rt_perusahaan_pasangan').val(rt_perusahaan_pasangan);
            $('#rw_perusahaan_pasangan').val(rw_perusahaan_pasangan);
            $('#provinsi_perusahaan_pasangan').val(provinsi_perusahaan_pasangan);
            $('#kabupaten_perusahaan_pasangan').val(kabupaten_perusahaan_pasangan);
            $('#kecamatan_perusahaan_pasangan').val(kecamatan_perusahaan_pasangan);
            $('#kelurahan_perusahaan_pasangan').val(kelurahan_perusahaan_pasangan);
            $('#kodepos_perusahaan_pasangan').val(kodepos_perusahaan_pasangan);
            $('#tahun_pengalaman_bekerja_pasangan').val(tahun_pengalaman_bekerja_pasangan);
            $('#bulan_pengalaman_bekerja_pasangan').val(bulan_pengalaman_bekerja_pasangan);
            $('#penghasilan_pasangan').val(penghasilan_pasangan);
            $('#biaya_hidup_pasangan').val(biaya_hidup_pasangan);
            // non fix
            var nama_perusahaan_pasangan_non = `{!! $nama_perusahaan_pasangan_non !!}`;
            var lama_usaha_pasangan_non = `{!! $lama_usaha_pasangan_non !!}`;
            var lama_tempat_usaha_pasangan_non = `{!! $lama_tempat_usaha_pasangan_non !!}`;
            var no_telpon_pasangan_non = `{!! $no_telpon_pasangan_non !!}`;
            var no_hp_pasangan_non = `{!! $no_hp_pasangan_non !!}`;
            var alamat_perusahaan_pasangan_non = `{!! $alamat_perusahaan_pasangan_non !!}`;
            var rt_perusahaan_pasangan_non = `{!! $rt_perusahaan_pasangan_non !!}`;
            var rw_perusahaan_pasangan_non = `{!! $rw_perusahaan_pasangan_non !!}`;
            var provinsi_perusahaan_pasangan_non = `{!! $provinsi_perusahaan_pasangan_non !!}`;
            var kabupaten_perusahaan_pasangan_non = `{!! $kabupaten_perusahaan_pasangan_non !!}`;
            var kecamatan_perusahaan_pasangan_non = `{!! $kecamatan_perusahaan_pasangan_non !!}`;
            var kelurahan_perusahaan_pasangan_non = `{!! $kelurahan_perusahaan_pasangan_non !!}`;
            var kodepos_perusahaan_pasangan_non = `{!! $kodepos_perusahaan_pasangan_non !!}`;
            var surat_ijin_pasangan_non = `{!! $surat_ijin_pasangan_non !!}`;
            var nomor_ijin_pasangan_non = `{!! $nomor_ijin_pasangan_non !!}`;
            var penghasilan_pasangan_non = `{!! $penghasilan_pasangan_non !!}`;
            var biaya_hidup_pasangan_non = `{!! $biaya_hidup_pasangan_non !!}`;

            $('#nama_perusahaan_pasangan_non').val(nama_perusahaan_pasangan_non);
            $('#lama_usaha_pasangan_non').val(lama_usaha_pasangan_non);
            $('#lama_tempat_usaha_pasangan_non').val(lama_tempat_usaha_pasangan_non);
            if (no_telpon_pasangan_non.substring(0, 3) == '620') {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(3));
            } else if (no_telpon_pasangan_non.substring(0, 2) == '62') {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(2));
            } else if (no_telpon_pasangan_non.substring(0, 1) == '0') {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(1));
            } else {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non);
            }
            if (no_hp_pasangan_non.substring(0, 3) == '620') {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(3));
            } else if (no_hp_pasangan_non.substring(0, 2) == '62') {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(2));
            } else if (no_hp_pasangan_non.substring(0, 1) == '0') {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(1));
            } else {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non);
            }
            $('#alamat_perusahaan_pasangan_non').val(alamat_perusahaan_pasangan_non);
            $('#rt_perusahaan_pasangan_non').val(rt_perusahaan_pasangan_non);
            $('#rw_perusahaan_pasangan_non').val(rw_perusahaan_pasangan_non);
            $('#provinsi_perusahaan_pasangan_non').val(provinsi_perusahaan_pasangan_non);
            $('#kabupaten_perusahaan_pasangan_non').val(kabupaten_perusahaan_pasangan_non);
            $('#kecamatan_perusahaan_pasangan_non').val(kecamatan_perusahaan_pasangan_non);
            $('#kelurahan_perusahaan_pasangan_non').val(kelurahan_perusahaan_pasangan_non);
            $('#kodepos_perusahaan_pasangan_non').val(kodepos_perusahaan_pasangan_non);
            $('#surat_ijin_pasangan_non').val(surat_ijin_pasangan_non);
            $('#nomor_ijin_pasangan_non').val(nomor_ijin_pasangan_non);
            if (surat_ijin_pasangan_non == 2) {
                $('.bloksuratIjinPasangan').addClass("d-none");
            } else if (surat_ijin_pasangan_non == 1) {
                $('#surat_ijin_pasangan_non').val("Ya");
            }
            $('#penghasilan_pasangan_non').val(penghasilan_pasangan_non);
            $('#biaya_hidup_pasangan_non').val(biaya_hidup_pasangan_non);

            var pekerjaan = `{!! $pekerjaan !!}`;
            var bidang_perusahaan = `{!! $bidang_perusahaan !!}`;
            var bidang_pekerjaan = `{!! $bidang_pekerjaan !!}`;

            var pengalaman_pekerjaan = `{!! $pengalaman_pekerjaan !!}`;
            var pendapatan = `{!! $pendapatan !!}`;
            var total_aset = `{!! $total_aset !!}`;
            var kewarganegaraan = `{!! $kewarganegaraan !!}`;

            var brw_pic = `{!! $brw_pic !!}`;
            $('#brw_pic').val(brw_pic);
            var brw_pic_ktp = `{!! $brw_pic_ktp !!}`;
            $('#brw_pic_ktp').val(brw_pic_ktp);
            var brw_pic_user_ktp = `{!! $brw_pic_user_ktp !!}`;
            $('#brw_pic_user_ktp').val(brw_pic_user_ktp);
            var brw_pic_npwp = `{!! $brw_pic_npwp !!}`;
            $('#brw_pic_npwp').val(brw_pic_npwp);

            // informasi pekerjaan
            // fix
            var jenis_pekerjaan = `{!! $jenis_pekerjaan !!}`;
            var jenis_properti = "{{ $jenis_properti }}";
            var sumberpengembaliandana = `{!! $sumberpengembaliandana !!}`;
            var skemapembiayaan = `{!! $skemapembiayaan !!}`;
            var nama_perusahaan = `{!! $nama_perusahaan !!}`;
            var bidang_online = `{!! $bidang_online !!}`;
            var bentuk_badan_usaha = `{!! $bentuk_badan_usaha !!}`;
            var status_kepegawaian = `{!! $status_kepegawaian !!}`;
            var usia_perusahaan = `{!! $usia_perusahaan !!}`;
            var departemen = `{!! $departemen !!}`;
            var jabatan = `{!! $jabatan !!}`;
            var tahun_bekerja = `{!! $tahun_bekerja !!}`;
            var bulan_bekerja = `{!! $bulan_bekerja !!}`;
            var nip = `{!! $nip !!}`;
            var nama_hrd = `{!! $nama_hrd !!}`;
            var no_fixed_line_hrd = `{!! $no_fixed_line_hrd !!}`;
            var alamat_perusahaan = `{!! $alamat_perusahaan !!}`;
            var rt_perusahaan = `{!! $rt_perusahaan !!}`;
            var rw_perusahaan = `{!! $rw_perusahaan !!}`;
            var provinsi_perusahaan = `{!! $provinsi_perusahaan !!}`;
            var kabupaten_perusahaan = `{!! $kabupaten_perusahaan !!}`;
            var kecamatan_perusahaan = `{!! $kecamatan_perusahaan !!}`;
            var kelurahan_perusahaan = `{!! $kelurahan_perusahaan !!}`;
            var kodepos_perusahaan = `{!! $kodepos_perusahaan !!}`;
            var no_telpon_usaha = `{!! $no_telpon_usaha !!}`;
            var tahun_pengalaman_bekerja = `{!! $tahun_pengalaman_bekerja !!}`;
            var bulan_pengalaman_bekerja = `{!! $bulan_pengalaman_bekerja !!}`;
            var penghasilan = `{!! $penghasilan !!}`;
            var biaya_hidup = `{!! $biaya_hidup !!}`;
            var detail_penghasilan_lain_lain = `{!! $detail_penghasilan_lain_lain !!}`;
            var total_penghasilan_lain_lain = `{!! $total_penghasilan_lain_lain !!}`;
            var nilai_spt = `{!! $nilai_spt !!}`;
            // nonfix
            var nama_perusahaan_non = `{!! $nama_perusahaan_non !!}`;
            var bidang_online_non = `{!! $bidang_online_non !!}`;
            var lama_usaha_non = `{!! $lama_usaha_non !!}`;
            var lama_tempat_usaha_non = `{!! $lama_tempat_usaha_non !!}`;
            var no_telpon_non = `{!! $no_telpon_non !!}`;
            var no_hp_non = `{!! $no_hp_non !!}`;
            var alamat_perusahaan_non = `{!! $alamat_perusahaan_non !!}`;
            var rt_perusahaan_non = `{!! $rt_perusahaan_non !!}`;
            var rw_perusahaan_non = `{!! $rw_perusahaan_non !!}`;
            var provinsi_perusahaan_non = `{!! $provinsi_perusahaan_non !!}`;
            var kabupaten_perusahaan_non = `{!! $kabupaten_perusahaan_non !!}`;
            var kecamatan_perusahaan_non = `{!! $kecamatan_perusahaan_non !!}`;
            var kelurahan_perusahaan_non = `{!! $kelurahan_perusahaan_non !!}`;
            var kodepos_perusahaan_non = `{!! $kodepos_perusahaan_non !!}`;
            var surat_ijin_non = `{!! $surat_ijin_non !!}`;
            var nomor_ijin_non = `{!! $nomor_ijin_non !!}`;
            var penghasilan_non = `{!! $penghasilan_non !!}`;
            var biaya_hidup_non = `{!! $biaya_hidup_non !!}`;

            $('#nama_perusahaan_non').val(nama_perusahaan_non);
            $('#bidang_online_non').val(bidang_online_non);
            $('#lama_usaha_non').val(lama_usaha_non);
            $('#lama_tempat_usaha_non').val(lama_tempat_usaha_non);
            if (no_telpon_non.substring(0, 3) == '620') {
                $('#no_telpon_non').val(no_telpon_non.substring(3));
            } else if (no_telpon_non.substring(0, 2) == '62') {
                $('#no_telpon_non').val(no_telpon_non.substring(2));
            } else if (no_telpon_non.substring(0, 1) == '0') {
                $('#no_telpon_non').val(no_telpon_non.substring(1));
            } else {
                $('#no_telpon_non').val(no_telpon_non);
            }
            if (no_hp_non.substring(0, 3) == '620') {
                $('#no_hp_non').val(no_hp_non.substring(3));
            } else if (no_hp_non.substring(0, 2) == '62') {
                $('#no_hp_non').val(no_hp_non.substring(2));
            } else if (no_hp_non.substring(0, 1) == '0') {
                $('#no_hp_non').val(no_hp_non.substring(1));
            } else {
                $('#no_hp_non').val(no_hp_non);
            }
            $('#alamat_perusahaan_non').val(alamat_perusahaan_non);
            $('#rt_perusahaan_non').val(rt_perusahaan_non);
            $('#rw_perusahaan_non').val(rw_perusahaan_non);
            $('#provinsi_perusahaan_non').val(provinsi_perusahaan_non);
            $('#kabupaten_perusahaan_non').val(kabupaten_perusahaan_non);
            $('#kecamatan_perusahaan_non').val(kecamatan_perusahaan_non);
            $('#kelurahan_perusahaan_non').val(kelurahan_perusahaan_non);
            $('#kodepos_perusahaan_non').val(kodepos_perusahaan_non);
            if (surat_ijin_non == 2) {
                $('.bloksuratIjin').addClass("d-none");
            } else if (surat_ijin_non == 1) {
                $('#surat_ijin_non').val("Ya");
            }
            $('#nomor_ijin_non').val(nomor_ijin_non);
            $('#penghasilan_non').val(penghasilan_non);
            $('#biaya_hidup_non').val(biaya_hidup_non);

            // PENGAJUAN 
            var type_pendanaan = `{!! $type_pendanaan !!}`;
            var tujuan_pendanaan = `{!! $tujuan_pendanaan !!}`;
            var detail_pendanaan = `{!! $detail_pendanaan !!}`;
            var alamat_objek_pendanaan = `{!! $alamat_objek_pendanaan !!}`;
            var provinsi_objek_pendanaan = `{!! $provinsi_objek_pendanaan !!}`;
            var kota_objek_pendanaan = `{!! $kota_objek_pendanaan !!}`;
            var kecamatan_objek_pendanaan = `{!! $kecamatan_objek_pendanaan !!}`;
            var kelurahan_objek_pendanaan = `{!! $kelurahan_objek_pendanaan !!}`;
            var kodepos_objek_pendanaan = `{!! $kodepos_objek_pendanaan !!}`;
            var harga_objek_pendanaan = `{!! $harga_objek_pendanaan !!}`;
            var uang_muka = `{!! $uang_muka !!}`;
            var nilai_pengajuan = `{!! $nilai_pengajuan !!}`;
            var jangka_waktu = `{!! $jangka_waktu !!}`;
            var nama_pemilik = `{!! $nama_pemilik !!}`;
            var tlp_pemilik = `{!! $tlp_pemilik !!}`;
            var alamat_pemilik = `{!! $alamat_pemilik !!}`;
            var provinsi_pemilik = `{!! $provinsi_pemilik !!}`;
            var kota_pemilik = `{!! $kota_pemilik !!}`;
            var kecamatan_pemilik = `{!! $kecamatan_pemilik !!}`;
            var kelurahan_pemilik = `{!! $kelurahan_pemilik !!}`;
            var kodepos_pemilik = `{!! $kodepos_pemilik !!}`;

            $('.type_pendanaan').val(type_pendanaan);
            $('.tujuan_pendanaan').val(tujuan_pendanaan);
            $('#detail_pendanaan').text(detail_pendanaan);
            $('#alamat_objek_pendanaan, #alamat_agunan').val(alamat_objek_pendanaan);
            $('#provinsi_objek_pendanaan, #provinsi_agunan').val(provinsi_objek_pendanaan);
            $('#kota_objek_pendanaan, #kota_agunan').val(kota_objek_pendanaan);
            $('#kecamatan_objek_pendanaan, #kecamatan_agunan').val(kecamatan_objek_pendanaan);
            $('#kelurahan_objek_pendanaan, #kelurahan_agunan').val(kelurahan_objek_pendanaan);
            $('#kodepos_objek_pendanaan, #kodepos_agunan').val(kodepos_objek_pendanaan);
            $('#harga_objek_pendanaan').val(harga_objek_pendanaan);
            $('#uang_muka').val(uang_muka);
            $('#nilai_pengajuan').val(nilai_pengajuan);
            $('#jangka_waktu').val(jangka_waktu);
            $('#nama_pemilik').val(nama_pemilik);
            if (tlp_pemilik.substring(0, 3) == '620') {
                $('#tlp_pemilik').val(tlp_pemilik.substring(3));
            } else if (tlp_pemilik.substring(0, 2) == '62') {
                $('#tlp_pemilik').val(tlp_pemilik.substring(2));
            } else if (tlp_pemilik.substring(0, 1) == '0') {
                $('#tlp_pemilik').val(tlp_pemilik.substring(1));
            } else {
                $('#tlp_pemilik').val(tlp_pemilik);
            }
            $('#alamat_pemilik').val(alamat_pemilik);
            $('#provinsi_pemilik').val(provinsi_pemilik);
            $('#kota_pemilik').val(kota_pemilik);
            $('#kecamatan_pemilik').val(kecamatan_pemilik);
            $('#kelurahan_pemilik').val(kelurahan_pemilik);
            $('#kodepos_pemilik').val(kodepos_pemilik);

            // data verif
            var nilai_pefindo = `{!! $nilai_pefindo !!}`;
            var grade_pefindo = `{!! $grade_pefindo !!}`;
            var skor_personal = `{!! $skor_personal !!}`;
            var skor_pendanaan = `{!! $skor_pendanaan !!}`;
            var complete_id = `{!! $complete_id !!}`;
            var ocr = `{!! $ocr !!}`;
            var npwpa = `{!! $npwpa !!}`;
            var npwpc = `{!! $npwpc !!}`;
            var npwpd = `{!! $npwpd !!}`;
            var npwp_verify = `{!! $npwp_verify !!}`;
            var workplace_verification_f = `{!! $workplace_verification_f !!}`;
            var negative_list_verification_j = `{!! $negative_list_verification_j !!}`;
            var verify_property_k = `{!! $verify_property_k !!}`;
            var ftv = `{!! $ftv !!}`;
            var catatan_verifikasi = `{!! $catatan_verifikasi !!}`;
            var keputusan = `{!! $keputusan !!}`;
            var isiVerif = `{!! $isiVerif !!}`;
            var status_pengajuan = `{!! $status_pengajuan !!}`;
            var status_keputusan = `{!! $status_keputusan !!}`;


            if ($("#jenis_agunan option:selected").val() == '1') {
                $("#div_tanggal_jatuh_tempo").addClass('d-none');
            } else {
                $("#div_tanggal_jatuh_tempo").removeClass('d-none');
            }

            if (isiVerif == 1) {
                $('#nilai_pefindo').val(nilai_pefindo);
                $("#grade_pefindo option[value='" + grade_pefindo + "']").attr('selected', 'selected');
                $('#skor_personal').val(skor_personal);
                $('#skor_pendanaan').val(skor_pendanaan);

                if (complete_id == 1) {
                    $("input[name=verijelas][value='complete_id']").attr('checked', 'checked');
                }
                if (ocr == 1) {
                    $("input[name=verijelas][value='ocr']").attr('checked', 'checked');
                }
                if (npwpa == 1) {
                    $("input[name=verijelas][value='npwpa']").attr('checked', 'checked');
                }
                if (npwpc == 1) {
                    $("input[name=verijelas][value='npwpc']").attr('checked', 'checked');
                }
                if (npwpd == 1) {
                    $("input[name=verijelas][value='npwpd']").attr('checked', 'checked');
                }
                if (npwp_verify == 1) {
                    $("input[name=verijelas][value='npwp_verify']").attr('checked', 'checked');
                }
                if (workplace_verification_f == 1) {
                    $("input[name=verijelas][value='workplace_verification_f']").attr('checked', 'checked');
                }
                if (negative_list_verification_j == 1) {
                    $("input[name=verijelas][value='negative_list_verification_j']").attr('checked', 'checked');
                }
                if (verify_property_k == 1) {
                    $("input[name=verijelas][value='verify_property_k']").attr('checked', 'checked');
                }

                $('#ftv').val(ftv);
                $('#catatan_verifikasi').val(catatan_verifikasi);
                $("#keputusan option[value='" + status_keputusan + "']").attr('selected', 'selected');

                // disabled form verifikasi
                $('#nilai_pefindo').attr("disabled", true);
                $('#grade_pefindo').attr("disabled", true);
                $('#ftv').attr("disabled", true);
                $('#catatan_verifikasi').attr("disabled", true);


                if (status_pengajuan == "11") {
                    $('#keputusan').attr("disabled", false);
                    $('#id_field_verifikator_vendor').attr("disabled", false);
                    $("#submitverfikasi").attr("disabled", false);
                } else {
                    $('#keputusan').attr("disabled", true);
                    $('#id_field_verifikator_vendor').attr("disabled", true);
                    $("#submitverfikasi").attr("disabled", true);
                }


                if (status_keputusan == '2') {
                    $('#submitverfikasi').html('Tolak');
                    $('#submitverfikasi').removeClass('btn-success');
                    $('#submitverfikasi').addClass('btn-danger');
                } else {
                    $('#submitverfikasi').removeClass('btn-danger');
                    $('#submitverfikasi').addClass('btn-success');
                    $('#submitverfikasi').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Kirim');
                }

                $("input[name=verijelas]").attr("disabled", true);

            } else {
                // credolab

                // let reference_number_personal = '288';
                // let reference_number_pendanaan = '288_80';
                let reference_number_personal = brw_id;
                let reference_number_pendanaan = brw_id + '_' + pengajuanId;
                var url_per = '{{ route('get-credolab-score', ':reference_number') }}';

                // Borrower Score personal
                var v_url = url_per.replace(':reference_number', reference_number_personal);
                $.getJSON(v_url, function(response) {
                    console.log([v_url, response]);
                    if (response.status_code == 200) {
                        $('#skor_personal').val(response.credolab_score);
                    } else {
                        $('#skor_personal').val('0');
                    }
                });

                // Borrower Score pendanaan
                var url_pen = '{{ route('get-credolab-score', ':reference_number ') }}';
                var vpen_url = url_pen.replace(':reference_number', reference_number_pendanaan);
                $.getJSON(vpen_url, function(response) {
                    console.log([vpen_url, response]);
                    if (response.status_code == 200) {
                        $('#skor_pendanaan').val(response.credolab_score);
                    } else {
                        $('#skor_pendanaan').val('0');
                    }
                });
            }



            // START: Informasi Pribadi
            $('#nama').val(nama);
            if (jns_kelamin == 1) {
                jns_kelamin = "Laki-laki";
            } else {
                jns_kelamin = "Perempuan";
            }
            $('#jns_kelamin').val(jns_kelamin);
            $('#ktp').val(ktp);
            $('#kk').val(kk);
            $('#tempat_lahir').val(tempat_lahir);
            $("#tanggal_lahir").val(tgl_lahir);
            $('#ibukandung').val(nm_ibu);
            $('#npwp').val(npwp);

            let no_tlp_check = telepon;
            if (no_tlp_check.substring(0, 3) == '620') {
                $('#telepon').val(no_tlp_check.substring(3));
            } else if (no_tlp_check.substring(0, 2) == '62') {
                $('#telepon').val(no_tlp_check.substring(2));
            } else if (no_tlp_check.substring(0, 1) == '0') {
                $('#telepon').val(no_tlp_check.substring(1));
            } else {
                $('#telepon').val(no_tlp_check);
            }

            $("#pasangan_agama").val(pasangan_agama);
            $("#agama").val(agama);

            $("#pasangan_pendidikan_terakhir").val(pasangan_pendidikan_terakhir);
            $("#pendidikan_terakhir").val(pendidikan_terakhir);
            $("#status_kawin").val(status_kawin);

            // killl2
            $('#provinsi').val(provinsi);
            $('#domisili_provinsi').val(domisili_provinsi);
            $('#pasangan_provinsi').val(pasangan_provinsi);
            $("#kota").val(kota);
            $('#kecamatan').val(kecamatan);
            $("#kelurahan").val(kelurahan);
            $("#kode_pos").val(kode_pos);
            $('#domisili_alamat').text(domisili_alamat);
            $('#pasangan_alamat').text(pasangan_alamat);

            if (status_rumah == 1) {
                status_rumah = "Milik Pribadi";
            } else if (status_rumah == 2) {
                status_rumah = "Sewa";
            } else {
                status_rumah = "Milik Keluarga";
            }
            $('#status_rumah').val(status_rumah);

            if (domisili_status_rumah == 1) {
                domisili_status_rumah = "Milik Pribadi";
            } else if (domisili_status_rumah == 2) {
                domisili_status_rumah = "Sewa";
            } else {
                domisili_status_rumah = "Milik Keluarga";
            }
            $('#domisili_status_rumah').val(domisili_status_rumah);


            // END: Alamat 

            // START: Domisili ALAMAT
            $('#domisili_kota').val(domisili_kota);
            $("#pasangan_kota").val(pasangan_kota);

            $("#domisili_kecamatan").val(domisili_kecamatan);
            $("#pasangan_kecamatan").val(pasangan_kecamatan);

            $("#domisili_kelurahan").val(domisili_kelurahan);
            $("#pasangan_kelurahan").val(pasangan_kelurahan);

            $("#domisili_kode_pos").val(domisili_kode_pos);
            $('#pasangan_kode_pos').val(pasangan_kode_pos);

            // END: Alamat domisili

            // START: Pasangan
            $('#pasangan_nama').val(pasangan_nama);
            if (pasangan_jenis_kelamin == 1) {
                pasangan_jenis_kelamin = "Laki-laki";
            } else {
                pasangan_jenis_kelamin = "Perempuan";
            }
            $('#pasangan_jenis_kelamin').val(pasangan_jenis_kelamin);
            $('#pasangan_ktp').val(pasangan_ktp);
            $('#pasangan_tempat_lahir').val(pasangan_tempat_lahir);
            $('#pasangan_tanggal_lahir').val(pasangan_tanggal_lahir);

            if (pasangan_telepon.substring(0, 3) == '620') {
                $('#pasangan_telepon').val(pasangan_telepon.substring(3));
            } else if (pasangan_telepon.substring(0, 2) == '62') {
                $('#pasangan_telepon').val(pasangan_telepon.substring(2));
            } else if (pasangan_telepon.substring(0, 1) == '0') {
                $('#pasangan_telepon').val(pasangan_telepon.substring(1));
            } else {
                $('#pasangan_telepon').val(pasangan_telepon);
            }

            $('#pasangan_npwp').val(pasangan_npwp);

            // END: Informasi Pribadi

            //START: Rekening
            $('#norekening').val(brw_norek);
            $('#namapemilikrekening').val(brw_nm_pemilik);
            $('#bank').val(brw_kd_bank);
            $('#kantorcabangpembuka').val(kantorcabangpembuka);
            //END: Rekening


            // START: Pekerjaan Individu
            $('#jenis_pekerjaan').val(jenis_pekerjaan);
            var txt_sumberpengembaliandana;
            var txt_skemapembiayaan;
            if (sumberpengembaliandana == 1) {
                txt_sumberpengembaliandana = "Fixed Income (Penghasilan Tetap)";
            } else {
                $('.blokNonFixedIncome').removeClass('d-none');
                $('.blokFixedIncome').addClass('d-none');
                txt_sumberpengembaliandana = "Non Fixed Income (Penghasilan Tidak Tetap)";
            }
            $('#sumberpengembaliandana').val(txt_sumberpengembaliandana);
            if (id_kawin == 1 && skemapembiayaan == 2) {
                if (sumberpengembaliandana_pasangan == 1) {
                    $('.blokPekerjaanPasangan').removeClass('d-none');
                    $('.blokFixedIncomePasangan').removeClass('d-none');
                } else if (sumberpengembaliandana_pasangan == 2) {
                    $('.blokPekerjaanPasangan').removeClass('d-none');
                    $('.blokNonFixedIncomePasangan').removeClass('d-none');
                }
            }
            if (skemapembiayaan == 1) {
                txt_skemapembiayaan = "Single Income (Peghasilan Sendiri)";
            } else {
                txt_skemapembiayaan = "Joint Income (Penghasilan Bersama)";
            }
            $('#skemapembiayaan').val(txt_skemapembiayaan);
            $('#nama_perusahaan').val(nama_perusahaan);
            $('#bidang_online').val(bidang_online);
            $('#bentuk_badan_usaha').val(bentuk_badan_usaha);

            $('#status_kepegawaian').val(status_kepegawaian);
            $('#usia_perusahaan').val(usia_perusahaan);
            $('#departemen').val(departemen);
            $('#jabatan').val(jabatan);
            $('#tahun_bekerja').val(tahun_bekerja);
            $('#bulan_bekerja').val(bulan_bekerja);
            $('#nip').val(nip);
            $('#nama_hrd').val(nama_hrd);
            if (no_fixed_line_hrd.substring(0, 3) == '620') {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(3));
            } else if (no_fixed_line_hrd.substring(0, 2) == '62') {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(2));
            } else if (no_fixed_line_hrd.substring(0, 1) == '0') {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(1));
            } else {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd);
            }
            $('#alamat_perusahaan').val(alamat_perusahaan);
            $('#rt_perusahaan').val(rt_perusahaan);
            $('#rw_perusahaan').val(rw_perusahaan);
            $('#provinsi_perusahaan').val(provinsi_perusahaan);
            $('#kabupaten_perusahaan').val(kabupaten_perusahaan);
            $('#kecamatan_perusahaan').val(kecamatan_perusahaan);
            $('#kelurahan_perusahaan').val(kelurahan_perusahaan);
            $('#kodepos_perusahaan').val(kodepos_perusahaan);
            $('#no_telpon_usaha').val(no_telpon_usaha);
            if (no_telpon_usaha.substring(0, 3) == '620') {
                $('#no_telpon_usaha').val(no_telpon_usaha.substring(3));
            } else if (no_telpon_usaha.substring(0, 2) == '62') {
                $('#no_telpon_usaha').val(no_telpon_usaha.substring(2));
            } else if (no_telpon_usaha.substring(0, 1) == '0') {
                $('#no_telpon_usaha').val(no_telpon_usaha.substring(1));
            } else {
                $('#no_telpon_usaha').val(no_telpon_usaha);
            }
            $('#tahun_pengalaman_bekerja').val(tahun_pengalaman_bekerja);
            $('#bulan_pengalaman_bekerja').val(bulan_pengalaman_bekerja);
            $('#penghasilan').val(penghasilan);
            $('#biaya_hidup').val(biaya_hidup);
            $('#detail_penghasilan_lain_lain').val(detail_penghasilan_lain_lain);
            $('#total_penghasilan_lain_lain').val(total_penghasilan_lain_lain);
            $('#nilai_spt').val(nilai_spt);
            if (detail_penghasilan_lain_lain == "" || detail_penghasilan_lain_lain == null) {
                $('.blokDetailPenghasilan').addClass('d-none');
            }
            // END: Pekerjaan Individu


            $('#verijelas_modal').on('show.bs.modal', function(event) {
                $('.modal-title').html('{{ trans('verify.hasil.title') }}' + ' - ' + nama);
                veriJelasData();
            });

            $('body').on('show.bs.modal', function() {
                $('.modal-open').css('padding-right', '0px');
            });

            $('#loading_modal').on('show.bs.modal', function(event) {
                $('.modal-title').html('{{ trans('verify.title') }}');
                veriJelasCheck();
            });

            $('#loading_modal').on('hidden.bs.modal', function(e) {
                $('#loading_footer').addClass('d-none');
            });

            $('#keputusan').on('change', function(event) {
                if ($(this).val() == '12') {
                    $('#submitverfikasi').html('Tolak');
                    $('#submitverfikasi').removeClass('btn-success');
                    $('#submitverfikasi').addClass('btn-danger');
                } else {
                    $('#submitverfikasi').removeClass('btn-danger');
                    $('#submitverfikasi').addClass('btn-success');
                    $('#submitverfikasi').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Kirim');
                }

            });

            function veriJelasData() {

                $.ajax({
                    url: "{{ route('verify.data') }}",
                    type: "post",
                    dataType: "html",
                    data: {
                        _token: "{{ csrf_token() }}",
                        borrower_id: brw_id,
                        brw_type: brw_type,
                        vendor: 'verijelas'
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        if (xhr.status == '419') {
                            errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                        }

                        $('#loading_title').html('Data Failed');
                        $('#loading_content_data').html(errorMessage);
                        $('#loading_footer_data').removeClass('d-none');
                    },
                    success: function(result) {
                        $('#loading_title').html('Preparing Result ...');
                        $('#loading_content_data').html(result);
                        $('#loading_title').html('');
                        $('#loading_footer_data').removeClass('d-none');
                    }
                });


            }

            function veriJelasCheck() {

                const checkCompleteId = $("#complete_id").is(":checked") ? 1 : 0;
                const checkOcr = $("#ocr").is(":checked") ? 1 : 0;
                const checkNpwp = $("#npwp_verify").is(":checked") ? 1 : 0;
                const checkTempatKerja = $("#workplace_verification_f").is(":checked") ? 1 : 0;
                const checkNegativeList = $("#ngetive_list_verification_j").is(":checked") ? 1 : 0;

                $.ajax({
                    url: "{{ route('verify.check') }}",
                    type: "post",
                    dataType: "html",
                    data: {
                        _token: "{{ csrf_token() }}",
                        borrower_id: brw_id,
                        brw_type: brw_type,
                        complete_id: checkCompleteId,
                        ocr: checkOcr,
                        npwp_verify: checkNpwp,
                        tempat_kerja: checkTempatKerja,
                        negativelist: checkNegativeList,
                        vendor: 'verijelas',
                        origin: "WEB"
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText

                        if (xhr.status == '419') {
                            errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                        }

                        $('#loading_title').html('Check Failed');
                        $('#loading_content').html(errorMessage);
                        $('#loading_footer').removeClass('d-none');
                    },
                    success: function(result) {

                        $('#loading_title').html('Preparing Result ...');
                        $('#loading_content').html(result);
                        $('#loading_title').html('');
                        $('#loading_footer').removeClass('d-none');
                    }
                });
            }
        });
    </script>

@endsection
