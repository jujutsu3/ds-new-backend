<div class="card-body tabDokumenPendukung d-none">
    <div class="block-title text-black mb-20">


        <form id="form_dokumen_pendanaan">
            <div class="row">
                @if (isset($data_dokumen_persyaratan) && count($data_dokumen_persyaratan) > 0)
                    @foreach ($data_dokumen_persyaratan as $keyPageTitle => $valCategoryTitle)
                        <div class="col-12 mb-4">
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h3"
                                    for="form_informasi_objek_Pendanaan">{{ $keyPageTitle }}</label>
                            </div>
                        </div>

                        @foreach ($valCategoryTitle as $keyCategoryTitle => $valFieldName)
                            <div class="col-12 mb-4">
                                <h5 class="block-title text-black mb-10 font-w500">{{ $keyCategoryTitle }}</h5>
                            </div>
                            @foreach ($valFieldName as $keyFieldName => $value_field)
                                @php $field_name = $value_field->field_name @endphp
                                @if ($field_name == 'imb')
                                    @if (isset($dokumen_pendukung->no_imb) && !empty($dokumen_pendukung->no_imb))
                                        <div class="col-12 col-md-12">
                                            <div class="col-8">
                                                <label>{{ $value_field->persyaratan_nama }} </label>
                                                <small> ({{ $value_field->file_type }})</small>
                                            </div>
                                            <div class="col-4 text-right">
                                                @if (isset($dokumen_pendukung->$field_name) && !empty($dokumen_pendukung->$field_name))
                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#div_view_dokumen_pendukung"
                                                        data-fieldname="{{ $value_field->field_name }}"
                                                        data-tablename="{{ $value_field->table_name }}"
                                                        data-pengajuanid="{{ $pengajuan_id }}"
                                                        data-brwid="{{ $brw_id }}"
                                                        data-filedata="{{ route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $dokumen_pendukung->$field_name)]) }}"
                                                        id="btn_dokumen_{{ $value_field->field_name }}"
                                                        data-filetype="{{ $value_field->file_type }}"><i
                                                            class="fa fa-eye" aria-hidden="true"></i>
                                                        Lihat File</button>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-12 col-md-12">
                                        <div class="col-8">
                                            <label>{{ $value_field->persyaratan_nama }} </label>
                                            <small> ({{ $value_field->file_type }})</small>
                                        </div>
                                        <div class="col-4 text-right">
                                            @if (isset($dokumen_pendukung->$field_name) && !empty($dokumen_pendukung->$field_name))
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                    data-target="#div_view_dokumen_pendukung"
                                                    data-fieldname="{{ $value_field->field_name }}"
                                                    data-tablename="{{ $value_field->table_name }}"
                                                    data-pengajuanid="{{ $pengajuan_id }}"
                                                    data-brwid="{{ $brw_id }}"
                                                    data-filedata="{{ route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $dokumen_pendukung->$field_name)]) }}"
                                                    id="btn_dokumen_{{ $value_field->field_name }}"
                                                    data-filetype="{{ $value_field->file_type }}"><i
                                                        class="fa fa-eye" aria-hidden="true"></i>
                                                    Lihat File</button>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                                <div class="col-12 col-md-12">&nbsp;</div>
                            @endforeach
                            <div class="col-12 col-md-12">&nbsp;</div>
                        @endforeach
                    @endforeach



                @endif

            </div>
        </form>

        {{-- <form id="form_dokumen_pendanaan">
        <div class="row">
            @if (isset($data_dokumen_persyaratan) && count($data_dokumen_persyaratan) > 0)
                @foreach ($data_dokumen_persyaratan as $key => $val)
                  @foreach ($val as $key => $value_field)
                        @php $field_name = $value_field->field_name @endphp
                        @if (isset($value_field->page_title) && !empty($value_field->page_title))
                            <div class="col-12 mb-4">
                                <div class="form-check form-check-inline line">
                                    <label class="form-check-label text-black h3" for="form_informasi_objek_Pendanaan">{{ $value_field->page_title }}</label>
                                </div>
                            </div>
                        @endif
                        <div class="col-12 mb-4"> <h5 class="block-title text-black mb-10 font-w500">{{ $value_field->category_title }}</h5></div>
                        @if ($field_name == 'imb') 
                             @if (isset($dokumen_pendukung->no_imb) && !empty($dokumen_pendukung->no_imb))
                                    <div class="col-12 col-md-12">
                                        <div class="col-8">
                                            <label>{{ $value_field->persyaratan_nama }} </label>
                                            <small> ({{ $value_field->file_type }})</small>
                                        </div>
                                        <div class="col-4 text-right"> 
                                            @if (isset($dokumen_pendukung->$field_name) && !empty($dokumen_pendukung->$field_name))
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#div_view_dokumen_pendukung" data-fieldname="{{ $value_field->field_name }}"
                                                data-tablename="{{ $value_field->table_name }}"
                                                data-pengajuanid="{{ $pengajuan_id }}"
                                                data-brwid="{{ $brw_id }}"
                                                data-filedata="{{ route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $dokumen_pendukung->$field_name)]) }}"
                                                id="btn_dokumen_{{ $value_field->field_name }}" data-filetype="{{ $value_field->file_type }}"><i
                                                    class="fa fa-eye" aria-hidden="true"></i>
                                                Lihat File</button>
                                            @endif 
                                        </div>
                                </div>
                             @endif
                        @else 
                                 <div class="col-12 col-md-12">
                                        <div class="col-8">
                                            <label>{{ $value_field->persyaratan_nama }} </label>
                                            <small> ({{ $value_field->file_type }})</small>
                                        </div>
                                        <div class="col-4 text-right"> 
                                            @if (isset($dokumen_pendukung->$field_name) && !empty($dokumen_pendukung->$field_name))
                                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#div_view_dokumen_pendukung" data-fieldname="{{ $value_field->field_name }}"
                                                data-tablename="{{ $value_field->table_name }}"
                                                data-pengajuanid="{{ $pengajuan_id }}"
                                                data-brwid="{{ $brw_id }}"
                                                data-filedata="{{ route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $dokumen_pendukung->$field_name)]) }}"
                                                id="btn_dokumen_{{ $value_field->field_name }}" data-filetype="{{ $value_field->file_type }}"><i
                                                    class="fa fa-eye" aria-hidden="true"></i>
                                                Lihat File</button>
                                            @endif 
                                        </div>
                                </div>
                            @endif
                    @endforeach
                    <div class="col-12 col-md-12">&nbsp;</div>
                @endforeach
            @endif
        </div>
    </form> --}}

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-block btn-success btn-md" id="sebelumnya3"><i
                        class="fa fa-fw fa-chevron-left"></i>Sebelumnya</button>
            </div>
            <div class="col-md-8">
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-block btn-success btn-md" id="selanjutnya3">Selanjutnya
                    &nbsp; <i class="fa fa-fw fa-chevron-right"></i></button>
            </div>
        </div>
    </div>

    <!-- modal view file -->
    <div id="div_view_dokumen_pendukung" tabindex="-1" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered"
            style="max-height: calc(100vh - 200px);max-width: 50%;overflow-y: initial !important;">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Lihat File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="preview"></div>
                </div>
            </div>

        </div>

    </div>

</div>

<script>
    $(function() {


        $('#form_dokumen_pendanaan button').each(function() {
            var button_id = $(this).attr("id");
            $('#' + button_id).click(function() {

                var field_name = $(this).attr('data-fieldname');
                var table_name = $(this).attr('data-tablename');
                var pengajuan_id = $(this).attr('data-pengajuanid');
                var file_data = $(this).attr('data-filedata');
                var file_type = $(this).attr('data-filetype');
                var brw_id = $(this).attr('data-brwid');

                $('#div_view_dokumen_pendukung').on('show.bs.modal', function(e) {
                    var pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");
                    view_dokumen_pendukung(file_data, field_name, table_name,
                        pengajuan_id, brw_id, pdfSearch);
                });

            });
        });



        $('#div_view_dokumen_pendukung').on('show.bs.modal', function(event) {

            var button = $(event.relatedTarget)

            var fieldName = button.data('fieldname');
            var tableName = button.data('tablename');
            var pengajuanId = button.data('pengajuanid');
            var fileData = button.data('filedata');
            var brwId = button.data('brwid');

            var pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");
            view_dokumen_pendukung(fileData, fieldName, tableName, pengajuanId, brwId, pdfSearch);

        });


        function view_dokumen_pendukung(fileData, fieldName, tableName, pengajuanId, brwId, searchFilter) {
            $('#preview').empty();
            if (searchFilter.test(fileData)) {
                $('#preview').append($('<embed>', {
                    class: 'pdf_data',
                    src: fileData,
                    frameborder: '0',
                    width: '100%',
                    height: '500px'
                }))

            } else {
                $('#preview').append($('<img>', {
                    class: 'img_data',
                    src: fileData,
                    width: '100%',
                    height: '500px'
                }))
            }


        }


    });
</script>
