<div class="card-body tabverifikasi d-none">
    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5"
                    for="form_informasi_objek_Pendanaan   ">Form Verifikasi Kelayakan &nbsp</label>
            </div>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h6" for="form_informasi_objek_Pendanaan">Biro
                    Kredit &nbsp</label>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Nilai &nbsp</label>
                {{-- <label class="mandatory_label" style="color:red;">*</label> --}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <input class="form-control" type="number" min='1' max="5000" id="nilai_pefindo"
                    name="nilai_pefindo" value="0" placeholder="ketik disini..">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Grade &nbsp</label>
                {{-- <label class="mandatory_label" style="color:red;">*</label> --}}
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <select class="form-control custom-select" name="grade_pefindo" id="grade_pefindo"
                    required>
                    <option value="" active> Pilih </option>
                    <option value="A1"> A1 </option>
                    <option value="A2"> A2 </option>
                    <option value="A3"> A3 </option>
                    <option value="B1"> B1 </option>
                    <option value="B2"> B2 </option>
                    <option value="B3"> B3 </option>
                    <option value="C1"> C1 </option>
                    <option value="C2"> C2 </option>
                    <option value="C3"> C3 </option>
                    <option value="D1"> D1 </option>
                    <option value="D2"> D2 </option>
                    <option value="D3"> D3 </option>
                    <option value="E1"> E1 </option>
                    <option value="E2"> E2 </option>
                    <option value="E3"> E3 </option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h6"
                    for="form_informasi_objek_Pendanaan">CredoLab &nbsp</label>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="skor_personal">Skor Personal &nbsp</label><label class="mandatory_label"
                    style="color:red;">*</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <input class="form-control" id="skor_personal" name="skor_personal" readonly>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="skor_pendanaan">Skor Pendanaan &nbsp</label><label class="mandatory_label"
                    style="color:red;">*</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <input class="form-control" id="skor_pendanaan" name="skor_pendanaan" readonly>
            </div>
        </div>
    </div>
    <!-- verijelas -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h6"
                    for="verifikasi_data_verijelas">Verifikasi
                    Data Verijelas &nbsp</label><label class="form-check-label">:</label>
            </div>
        </div>
        <div class="col-md-2">

        </div>
        <div class="col-md-5">
            <div class="form-group">
                <input type="checkbox" id="complete_id" value="complete_id" name="verijelas">
                <label for="complete_id"> COMPLETE ID</label> <br>
                <input type="checkbox" id="ocr" value="ocr" name="verijelas">
                <label for="ocr"> OCR </label><br>
                <input type="checkbox" id="npwp_verify" value="npwp_verify" name="verijelas">
                <label for="npwp_verify"> NPWP</label><br>
                {{-- <input type="checkbox" id="npwpa" value="npwpa" name="verijelas">
                <label for="npwpa"> NPWP A </label><br>
                <input type="checkbox" id="npwpc" value="npwpc" name="verijelas">
                <label for="npwpc"> NPWP C </label><br> --}}
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                {{-- <input type="checkbox" id="npwpd" value="npwpd" name="verijelas">
                <label for="npwpd"> NPWP D </label><br> --}}
                <input type="checkbox" id="workplace_verification_f" value="workplace_verification_f"
                    name="verijelas">
                <label for="workplace_verification_f"> Workplace Verification F </label><br>
                <input type="checkbox" id="negative_list_verification_j"
                    value="negative_list_verification_j" name="verijelas">
                <label for="negative_list_verification_j"> Negative List Verification J </label><br>
                {{-- <input type="checkbox" id="verify_property_k" value="verify_property_k"
                    name="verijelas">
                <label for="verify_property_k"> Verify Property K </label><br> --}}
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-2">
            {{-- <button type="button" data-toggle="modal" id="btnCekVerijelas" data-target="#loading_modal"
                data-backdrop="static" data-keyboard="false" href="#"
                class="btn btn-block btn-primary btn-sm"> <i class="fa fa-check"
                    aria-hidden="true"></i>
                Cek Verijelas </button> --}}

            <button type="button" data-toggle="modal" id="btnCekVerijelas" data-target="#verijelas_modal"
                data-backdrop="static" data-keyboard="false" href="#"
                class="btn btn-block btn-primary btn-sm" data-check="1"> <i class="fa fa-check"
                    aria-hidden="true"></i>
                Cek Verijelas </button>
        </div>
        <div class="col-md-2">
            <button type="button" data-toggle="modal" data-target="#verijelas_modal"
                class="btn btn-block btn-success btn-sm" id="resultVeriJelas"> <i
                    class="fa fa-folder" aria-hidden="true"></i> Hasil Verijelas</button>
        </div>


    </div>
    <!-- verijelas -->
    <div class="row py-3">
        <div class="col-md-4">
            <label for="ftv">Financing To Value (LTV/FTV) &nbsp</label><label class="mandatory_label"
                style="color:red;">*</label>
            <div class="form-group">
                <input class="form-control" type="number" min="1" max="100" id="ftv" name="ftv"
                    placeholder="Prosentase %" required value="0">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="catatan_verifikasi">Catatan Verifikasi Kelayakan &nbsp</label><label
                    class="mandatory_label" style="color:red;">*</label>
                <textarea class="form-control" rows="4" cols="80" id="catatan_verifikasi"
                    name="catatan_verifikasi" required></textarea>
            </div>
            <br>
            <hr>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            &nbsp;
        </div>
        <div class="col-md-4">
            <label for="ftv">Verifikator</label><label class="mandatory_label"
                style="color:red;">*</label>
            <div class="form-group">
                {!! Form::select('id_field_verifikator_vendor', $verifikator_vendor, $id_field_verifikator_vendor, ['id' => 'id_field_verifikator_vendor', 'class' => 'form-control custom-select', 'placeholder' => 'Pilih Vendor']) !!}
            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label for="keputusan"><strong>Berdasarkan hasil Verifikasi ini, maka keputusan analis
                        PT. Dana Syariah Indonesia adalah &nbsp;</strong></label><label
                    class="mandatory_label" style="color:red;">*</label>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <select class="form-control" name="keputusan" id="keputusan" required>
                    <option value="">Pilih</option>
                    <option value="1">Lanjut</option>
                    <option value="2">Tolak</option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-2">
            <button class="btn btn-block btn-md btn-success" id="sebelumnya3">
                <i class="fa fa-fw fa-angle-left"></i>&nbsp;Sebelumnya</button>
        </div>
        <div class="col-md-8">
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-block btn-danger btn-md" id="submitverfikasi"><i
                    class="fa fa-paper-plane" aria-hidden="true"></i> Kirim</button>
        </div>
    </div>
</div>