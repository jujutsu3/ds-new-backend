@extends('layouts.admin.master')
@section('title', 'Detail Analisa Kelayakan')

@section('style')

    <style>
        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            background-color: #fff;
        }

        ul.breadcrumb li {
            display: inline;
            font-size: 18px;
        }

        ul.breadcrumb li+li:before {
            padding: 8px;
            color: black;
            content: "/\00a0";
        }

        ul.breadcrumb li a {
            color: #0275d8;
            text-decoration: none;
        }

        ul.breadcrumb li a:hover {
            color: #01447e;
            text-decoration: underline;
        }

        button[type=submit] {
            font-size: 14px;
        }

    </style>
@endsection


@section('content')

    <!-- Main Container -->
    <ul class="breadcrumb">
        <li><a href="{{ route('analisa.kelayakan') }}">Analisa Kelayakan</a></li>
        <li>Detail</li>
    </ul>

    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
           
                <div class="card">
                    <div class="card-header">

                        @if($pengajuan->tipe_id  === 2)
                            <div class="form-group" style="width:50%; float:left;">
                                <button type="button"
                                    class="btnProfile btn btn-default btn-lg btn-block btn-success disabled"><strong
                                        class="card-title">Data Pengajuan Pendanaan</strong></button>
                            </div> 

                            <div class="form-group" style="width:50%; float:left;">
                                <button type="button" class="btnVerifikasi btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Verifikasi Kelayakan</strong></button>
                            </div>
                        @endif 

                        @if($pengajuan->tipe_id === 1)
                            <div class="form-group" style="width:40%; float:left;">
                                <button type="button"
                                    class="btnProfile btn btn-default btn-lg btn-block btn-success disabled"><strong
                                        class="card-title">Data Pengajuan Pendanaan</strong></button>
                            </div> 

                            <div class="form-group" style="width:30%; float:left;">
                                <button type="button" class="btnRab btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Rencana Anggaran Belanja Barang</strong></button>
                            </div>
                            <div class="form-group" style="width:30%; float:left;">
                                <button type="button" class="btnVerifikasi btn btn-default btn-lg btn-block btn-success"><strong
                                        class="card-title">Verifikasi Kelayakan</strong></button>
                            </div>
                        @endif 
                     
                    </div>
                    <!-- TAB Profile -->
                    @if($brw_type === 1)
                      @include('pages.admin.analisa._info_profile_individu')
                    @endif

                   @if($brw_type === 2)
                      @include('pages.admin.analisa._info_profile_badanhukum')
                   @endif 
                    <!-- END TAB Profile -->

                    <!-- TAB RAB -->
                    @if($pengajuan->tipe_id === 1)
                       @include('pages.admin.analisa._info_rab')
                    @endif 

                    <!-- END TAB RAB -->
                    
                    <!-- TAB VERIFIKASI -->
                    @if($brw_type  === 1)
                       @include('pages.admin.analisa._tab_verifikasi_individu')
                    @endif

                    @if($brw_type === 2)
                        @include('pages.admin.analisa._tab_verifikasi_badanhukum')
                    @endif

            
                    
                
                </div>

            </div>
        </div>
    </div>

    </div><!-- .content -->
    <!-- Modal box -->
    @include('layouts.modals.modal-loading')
    @include('layouts.modals.modal-verijelas')
    <!-- End modal box -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <script>
        $('#selanjutnya1').on('click', function() {

            //check if tab rab exists 
            if($("button").hasClass("btnRab")){
                $('.tabprofile').addClass('d-none');
                $('.tabverifikasi').addClass('d-none');
                $('.tabRab').removeClass('d-none');

                $('.btnProfile').removeClass('disabled');
                $('.btnVerifikasi').removeClass('disabled');
                $('.btnRab').addClass('disabled');
            }else{
            
                $('.tabprofile').addClass('d-none');
                $('.tabverifikasi').removeClass('d-none');
                $('.btnVerifikasi').addClass('disabled');
                $('.btnProfile').removeClass('disabled');
            }
           

        });

        $('#selanjutnya2').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabRab').addClass('d-none');
            $('.tabverifikasi').removeClass('d-none');

            $('.btnVerifikasi').addClass('disabled');
            $('.btnProfile').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
        });

        $('#sebelumnya3').on('click', function() {

              //check if tab rab exists 
              if($("button").hasClass("btnRab")){

                $('.tabprofile').addClass('d-none');
                $('.tabverifikasi').addClass('d-none');
                $('.tabRab').removeClass('d-none');

                $('.btnProfile').removeClass('disabled');
                $('.btnVerifikasi').removeClass('disabled');
                $('.btnRab').addClass('disabled');

              }else{
                $('.tabprofile').removeClass('d-none');
                $('.tabverifikasi').addClass('d-none');
                $('.btnVerifikasi').removeClass('disabled');
                $('.btnProfile').addClass('disabled');

              }
           
        });

        $('#sebelumnya2').on('click', function() {
            $('.tabprofile').removeClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabRab').addClass('d-none');


            $('.btnVerifikasi').removeClass('disabled');
            $('.btnProfile').addClass('disabled');
            $('.btnRab').removeClass('disabled');
          
        });

        $('.btnProfile').on('click', function() {
            $('.tabprofile').removeClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabRab').addClass('d-none');

            $('.btnProfile').addClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
        });
        $('.btnPengajuan').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').removeClass('d-none');
            $('.tabverifikasi').addClass('d-none');

            $('.btnProfile').removeClass('disabled');
            $('.btnPengajuan').addClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
        });
        $('.btnVerifikasi').on('click', function() {
            $('.tabprofile').addClass('d-none');
            $('.tabpengajuan').addClass('d-none');
            $('.tabRab').addClass('d-none');
            
            $('.tabverifikasi').removeClass('d-none');

            $('.btnProfile').removeClass('disabled');
            $('.btnPengajuan').removeClass('disabled');
            $('.btnRab').removeClass('disabled');
            
            $('.btnVerifikasi').addClass('disabled');
        });

        $('.btnRab').on('click',function(){
            $('.tabprofile').addClass('d-none');
            $('.tabverifikasi').addClass('d-none');
            $('.tabRab').removeClass('d-none');

            $('.btnProfile').removeClass('disabled');
            $('.btnVerifikasi').removeClass('disabled');
            $('.btnRab').addClass('disabled');
        });

        $('#keputusan').on('change', function(event) {
                if ($(this).val() == '2') {
                    $('#submitverfikasi').html('Tolak');
                    $('#submitverfikasi').removeClass('btn-success');
                    $('#submitverfikasi').addClass('btn-danger');
                } else {
                    $('#submitverfikasi').removeClass('btn-danger');
                    $('#submitverfikasi').addClass('btn-success');
                    $('#submitverfikasi').html(
                        '<i class="fa fa-paper-plane" aria-hidden="true"></i> Kirim');
                }

            });


        $('#submitverfikasi').on('click', function() {
            if ($('#skor_personal').val() ==
                "" || $('#skor_pendanaan').val() == "" || $('#ftv').val() == "" || $('#catatan_verifikasi').val() ==
                "" || $('#keputusan').val() == "" || $('#id_field_verifikator_vendor').val() == "") {
                alert('Pastikan Form Terisi !');
            } else if ($('#nilai_pefindo').val() > 1000) {
                alert('Nilai Pefindo Tidak Boleh Lebih dari 1000');
            } else if ($('#ftv').val() > 100) {
                alert('Financing To Value tidak boleh lebih dari 100 %');
            } else {

                let msg_box = '';
                if ($('#keputusan').val() == '2') {
                    msg_box = 'Anda yakin ingin menolak pengajuan ini?';
                } else {
                    msg_box = 'Sistem akan mengirim Surat Perintah Kerja serta Data pendanaan ini kepada tim Verifikator secara otomatis';
                }

                Swal.fire({
                    title: "Notifikasi",
                    text: msg_box,
                    type: "warning",
                    buttons: true,
                    showCancelButton: true,
                }).then(result => {
                    if (result.value == true) {
                        kirimData();
                    } else {
                        return false;
                    }
                });

            }

            function kirimData() {

                var complete_id = 0;
                var npwp_verify = 0;
                var ocr = 0;
                var npwpa = 0;
                var npwpc = 0;
                var npwpd = 0;
                var brw_id = "{{ $brw_id }}";
                var workplace_verification_f = 0;
                var ngetive_list_verification_j = 0;
                var verify_property_k = 0;

                var pengajuanId = "{{ $pengajuanId }}";
                var nilai_pefindo = $('#nilai_pefindo').val();
                var grade_pefindo = $('#grade_pefindo').val();
                var skor_personal = $('#skor_personal').val();
                var skor_pendanaan = $('#skor_pendanaan').val();
                var ftv = $('#ftv').val();
                var catatan_verifikasi = $('#catatan_verifikasi').val();
                var keputusan = $('#keputusan').val();

                $.each($("input[name='verijelas']:checked"), function() {
                    if ($(this).val() == 'complete_id') {
                        complete_id = 1;
                    } else if ($(this).val() == 'ocr') {
                        ocr = 1;
                    } else if ($(this).val() == 'npwpa') {
                        npwpa = 1;
                    } else if ($(this).val() == 'npwpc') {
                        npwpc = 1;
                    } else if ($(this).val() == 'npwpd') {
                        npwpd = 1;
                    } else if ($(this).val() == 'workplace_verification_f') {
                        workplace_verification_f = 1;
                    } else if ($(this).val() == 'negative_list_verification_j') {
                        ngetive_list_verification_j = 1;
                    } else if ($(this).val() == 'verify_property_k') {
                        verify_property_k = 1;
                    } else if ($(this).val() == 'npwp_verify') {
                        npwp_verify = 1;
                    }
                });
                // alert([complete_id, ocr, npwpa, npwpc, npwpd, workplace_verification_f, ngetive_list_verification_j, verify_property_k]);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "/admin/analisa/kirimKelayakan",
                    method: "POST",
                    dataType: 'JSON',
                    data: {
                        'brw_id': brw_id,
                        'pengajuanId': pengajuanId,
                        'nilai_pefindo': nilai_pefindo,
                        'grade_pefindo': grade_pefindo,
                        'complete_id': complete_id,
                        'ocr': ocr,
                        'npwpa': npwpa,
                        'npwpc': npwpc,
                        'npwpd': npwpd,
                        'npwp_verify': npwp_verify,
                        'workplace_verification_f': workplace_verification_f,
                        'ngetive_list_verification_j': ngetive_list_verification_j,
                        'verify_property_k': verify_property_k,
                        'skor_personal': skor_personal,
                        'skor_pendanaan': skor_pendanaan,
                        'ftv': ftv,
                        'catatan_verifikasi': catatan_verifikasi,
                        'keputusan': keputusan
                    },
                    beforeSend: () => {
                        Swal.fire({
                            html: '<h5>Menyimpan Data ...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                Swal.showLoading();
                            }
                        });
                    },
                    success: function(msg) {
                        if (msg.data == 1) {
                            window.location.href = "/admin/analisa/kelayakan";
                        }

                    }
                });
            }
        });
    </script>
    <script>
        $(document).ready(function() {
            var brw_id = `{!! $brw_id !!}`;
            var pengajuanId = `{!! $pengajuanId !!}`;
            var brw_type = '{{ $brw_type }}';
            // informasi pribadi
         
            // var ktp = `{!! $ktp !!}`;
            var kk = `{!! $kk !!}`;
         
            var telepon = `{!! $telepon !!}`;
            var agama = `{!! $agama !!}`;
            var pendidikan_terakhir = `{!! $pendidikan_terakhir !!}`;
            var id_kawin = `{!! $id_kawin !!}`;
            if (id_kawin == 1) {
                $('.blokPasangan').removeClass('d-none');
            }
            var status_kawin = `{!! $status_kawin !!}`;
            var nm_ibu = `{!! $nm_ibu !!}`;
          
            // informasi alamat
            var alamat = `{!! $alamat !!}`;
            var kode_pos = `{!! $kode_pos !!}`;
            var status_rumah = `{!! $status_rumah !!}`;

            var domisili_alamat = `{!! $domisili_alamat !!}`;
            // var domisili_provinsi = `{!! $domisili_provinsi !!}`;
            // var domisili_kota = `{!! $domisili_kota !!}`;
            // var domisili_kecamatan = `{!! $domisili_kecamatan !!}`;
            // var domisili_kelurahan = `{!! $domisili_kelurahan !!}`;
            var domisili_kode_pos = `{!! $domisili_kode_pos !!}`;
            var domisili_status_rumah = `{!! $domisili_status_rumah !!}`;

            // informasi alamat domisili
            if (domisili_alamat != alamat && domisili_kode_pos != kode_pos) {
                $(".blokDomisili").removeClass('d-none');
            }

         
            // informasi pasangan
            var pasangan_nama = `{!! $pasangan_nama !!}`;
            var pasangan_jenis_kelamin = `{!! $pasangan_jenis_kelamin !!}`;
            var pasangan_ktp = `{!! $pasangan_ktp !!}`;
            var pasangan_tempat_lahir = `{!! $pasangan_tempat_lahir !!}`;
            var pasangan_tanggal_lahir = `{!! $pasangan_tanggal_lahir !!}`;
            var pasangan_telepon = `{!! $pasangan_telepon !!}`;
            var pasangan_agama = `{!! $pasangan_agama !!}`;
            var pasangan_pendidikan_terakhir = `{!! $pasangan_pendidikan_terakhir !!}`;
            var pasangan_npwp = `{!! $pasangan_npwp !!}`;
            var pasangan_alamat = `{!! $pasangan_alamat !!}`;
            var pasangan_provinsi = `{!! $pasangan_provinsi !!}`;
            var pasangan_kota = `{!! $pasangan_kota !!}`;
            var pasangan_kecamatan = `{!! $pasangan_kecamatan !!}`;
            var pasangan_kelurahan = `{!! $pasangan_kelurahan !!}`;
            var pasangan_kode_pos = `{!! $pasangan_kode_pos !!}`;

            // informasi pasangan pekerjaan
            var pekerjaan_pasangan = `{!! $pekerjaan_pasangan !!}`;
            var bidang_pekerjaan_pasangan = `{!! $bidang_pekerjaan_pasangan !!}`;
            var bidang_pekerjaan = `{!! $bidang_pekerjaan !!}`;
            var bidang_online_pasangan = `{!! $bidang_online_pasangan !!}`;
            $('#pekerjaan_pasangan').val(pekerjaan_pasangan);
            $('#bidang_pekerjaan').val(bidang_pekerjaan);
            $('#bidang_pekerjaan_pasangan').val(bidang_pekerjaan_pasangan);
            $('#bidang_online_pasangan').val(bidang_online_pasangan);

            var nama_perusahaan_pasangan = `{!! $nama_perusahaan_pasangan !!}`;
            var sumberpengembaliandana_pasangan = `{!! $sumberpengembaliandana_pasangan !!}`;
            var bentuk_badan_usaha_pasangan = `{!! $bentuk_badan_usaha_pasangan !!}`;
            var status_kepegawaian_pasangan = `{!! $status_kepegawaian_pasangan !!}`;
            var usia_perusahaan_pasangan = `{!! $usia_perusahaan_pasangan !!}`;
            var departemen_pasangan = `{!! $departemen_pasangan !!}`;
            var jabatan_pasangan = `{!! $jabatan_pasangan !!}`;
            var tahun_bekerja_pasangan = `{!! $tahun_bekerja_pasangan !!}`;
            var bulan_bekerja_pasangan = `{!! $bulan_bekerja_pasangan !!}`;
            var nip_pasangan = `{!! $nip_pasangan !!}`;
            var nama_hrd_pasangan = `{!! $nama_hrd_pasangan !!}`;
            var no_fixed_line_hrd_pasangan = `{!! $no_fixed_line_hrd_pasangan !!}`;
            var no_telpon_usaha_pasangan = `{!! $no_telpon_usaha_pasangan !!}`;
            var alamat_perusahaan_pasangan = `{!! $alamat_perusahaan_pasangan !!}`;
            var rt_perusahaan_pasangan = `{!! $rt_perusahaan_pasangan !!}`;
            var rw_perusahaan_pasangan = `{!! $rw_perusahaan_pasangan !!}`;
            var provinsi_perusahaan_pasangan = `{!! $provinsi_perusahaan_pasangan !!}`;
            var kabupaten_perusahaan_pasangan = `{!! $kabupaten_perusahaan_pasangan !!}`;
            var kecamatan_perusahaan_pasangan = `{!! $kecamatan_perusahaan_pasangan !!}`;
            var kelurahan_perusahaan_pasangan = `{!! $kelurahan_perusahaan_pasangan !!}`;
            var kodepos_perusahaan_pasangan = `{!! $kodepos_perusahaan_pasangan !!}`;
            var tahun_pengalaman_bekerja_pasangan = `{!! $tahun_pengalaman_bekerja_pasangan !!}`;
            var bulan_pengalaman_bekerja_pasangan = `{!! $bulan_pengalaman_bekerja_pasangan !!}`;
            var penghasilan_pasangan = `{!! $penghasilan_pasangan !!}`;
            var biaya_hidup_pasangan = `{!! $biaya_hidup_pasangan !!}`;
            var uang_muka_percentage = `{!! $uang_muka_percentage  !!}`;
            // fix
            $('#nama_perusahaan_pasangan').val(nama_perusahaan_pasangan);
            $('#bentuk_badan_usaha_pasangan').val(bentuk_badan_usaha_pasangan);
            $('#status_kepegawaian_pasangan').val(status_kepegawaian_pasangan);
            $('#usia_perusahaan_pasangan').val(usia_perusahaan_pasangan);
            $('#departemen_pasangan').val(departemen_pasangan);
            $('#jabatan_pasangan').val(jabatan_pasangan);
            $('#tahun_bekerja_pasangan').val(tahun_bekerja_pasangan);
            $('#bulan_bekerja_pasangan').val(bulan_bekerja_pasangan);
            $('#nip_pasangan').val(nip_pasangan);
            $('#nama_hrd_pasangan').val(nama_hrd_pasangan);
            $('#uang_muka_percentage').text(uang_muka_percentage);

            if (no_fixed_line_hrd_pasangan.substring(0, 3) == '620') {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(3));
            } else if (no_fixed_line_hrd_pasangan.substring(0, 2) == '62') {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(2));
            } else if (no_fixed_line_hrd_pasangan.substring(0, 1) == '0') {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(1));
            } else {
                $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan);
            }
            if (no_telpon_usaha_pasangan.substring(0, 3) == '620') {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(3));
            } else if (no_telpon_usaha_pasangan.substring(0, 2) == '62') {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(2));
            } else if (no_telpon_usaha_pasangan.substring(0, 1) == '0') {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(1));
            } else {
                $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan);
            }

            $('#alamat_perusahaan_pasangan').val(alamat_perusahaan_pasangan);
            $('#rt_perusahaan_pasangan').val(rt_perusahaan_pasangan);
            $('#rw_perusahaan_pasangan').val(rw_perusahaan_pasangan);
            $('#provinsi_perusahaan_pasangan').val(provinsi_perusahaan_pasangan);
            $('#kabupaten_perusahaan_pasangan').val(kabupaten_perusahaan_pasangan);
            $('#kecamatan_perusahaan_pasangan').val(kecamatan_perusahaan_pasangan);
            $('#kelurahan_perusahaan_pasangan').val(kelurahan_perusahaan_pasangan);
            $('#kodepos_perusahaan_pasangan').val(kodepos_perusahaan_pasangan);
            $('#tahun_pengalaman_bekerja_pasangan').val(tahun_pengalaman_bekerja_pasangan);
            $('#bulan_pengalaman_bekerja_pasangan').val(bulan_pengalaman_bekerja_pasangan);
            $('#penghasilan_pasangan').val(penghasilan_pasangan);
            $('#biaya_hidup_pasangan').val(biaya_hidup_pasangan);
            // non fix
            var nama_perusahaan_pasangan_non = "{{ $nama_perusahaan_pasangan_non }}";
            var lama_usaha_pasangan_non = "{{ $lama_usaha_pasangan_non }}";
            var lama_tempat_usaha_pasangan_non = "{{ $lama_tempat_usaha_pasangan_non }}";
            var no_telpon_pasangan_non = "{{ $no_telpon_pasangan_non }}";
            var no_hp_pasangan_non = "{{ $no_hp_pasangan_non }}";
            var alamat_perusahaan_pasangan_non = `{!! $alamat_perusahaan_pasangan_non !!}`;
            var rt_perusahaan_pasangan_non = "{{ $rt_perusahaan_pasangan_non }}";
            var rw_perusahaan_pasangan_non = "{{ $rw_perusahaan_pasangan_non }}";
            var provinsi_perusahaan_pasangan_non = "{{ $provinsi_perusahaan_pasangan_non }}";
            var kabupaten_perusahaan_pasangan_non = "{{ $kabupaten_perusahaan_pasangan_non }}";
            var kecamatan_perusahaan_pasangan_non = "{{ $kecamatan_perusahaan_pasangan_non }}";
            var kelurahan_perusahaan_pasangan_non = "{{ $kelurahan_perusahaan_pasangan_non }}";
            var kodepos_perusahaan_pasangan_non = "{{ $kodepos_perusahaan_pasangan_non }}";
            var surat_ijin_pasangan_non = "{{ $surat_ijin_pasangan_non }}";
            var nomor_ijin_pasangan_non = "{{ $nomor_ijin_pasangan_non }}";
            var penghasilan_pasangan_non = "{{ $penghasilan_pasangan_non }}";
            var biaya_hidup_pasangan_non = "{{ $biaya_hidup_pasangan_non }}";

            $('#nama_perusahaan_pasangan_non').val(nama_perusahaan_pasangan_non);
            $('#lama_usaha_pasangan_non').val(lama_usaha_pasangan_non);
            $('#lama_tempat_usaha_pasangan_non').val(lama_tempat_usaha_pasangan_non);
            if (no_telpon_pasangan_non.substring(0, 3) == '620') {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(3));
            } else if (no_telpon_pasangan_non.substring(0, 2) == '62') {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(2));
            } else if (no_telpon_pasangan_non.substring(0, 1) == '0') {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(1));
            } else {
                $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non);
            }
            if (no_hp_pasangan_non.substring(0, 3) == '620') {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(3));
            } else if (no_hp_pasangan_non.substring(0, 2) == '62') {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(2));
            } else if (no_hp_pasangan_non.substring(0, 1) == '0') {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(1));
            } else {
                $('#no_hp_pasangan_non').val(no_hp_pasangan_non);
            }
            $('#alamat_perusahaan_pasangan_non').val(alamat_perusahaan_pasangan_non);
            $('#rt_perusahaan_pasangan_non').val(rt_perusahaan_pasangan_non);
            $('#rw_perusahaan_pasangan_non').val(rw_perusahaan_pasangan_non);
            $('#provinsi_perusahaan_pasangan_non').val(provinsi_perusahaan_pasangan_non);
            $('#kabupaten_perusahaan_pasangan_non').val(kabupaten_perusahaan_pasangan_non);
            $('#kecamatan_perusahaan_pasangan_non').val(kecamatan_perusahaan_pasangan_non);
            $('#kelurahan_perusahaan_pasangan_non').val(kelurahan_perusahaan_pasangan_non);
            $('#kodepos_perusahaan_pasangan_non').val(kodepos_perusahaan_pasangan_non);
            $('#surat_ijin_pasangan_non').val(surat_ijin_pasangan_non);
            $('#nomor_ijin_pasangan_non').val(nomor_ijin_pasangan_non);
            if (surat_ijin_pasangan_non == 2) {
                $('.bloksuratIjinPasangan').addClass("d-none");
            } else if (surat_ijin_pasangan_non == 1) {
                $('#surat_ijin_pasangan_non').val("Ya");
            }
            $('#penghasilan_pasangan_non').val(penghasilan_pasangan_non);
            $('#biaya_hidup_pasangan_non').val(biaya_hidup_pasangan_non);

            var pekerjaan = "{{ $pekerjaan }}";
            var bidang_perusahaan = "{{ $bidang_perusahaan }}";
            var bidang_pekerjaan = "{{ $bidang_pekerjaan }}";

            var pengalaman_pekerjaan = "{{ $pengalaman_pekerjaan }}";
            var pendapatan = "{{ $pendapatan }}";
            var total_aset = "{{ $total_aset }}";
            var kewarganegaraan = "{{ $kewarganegaraan }}";

            var brw_pic = "{{ $brw_pic }}";
            $('#brw_pic').val(brw_pic);
            var brw_pic_ktp = "{{ $brw_pic_ktp }}";
            $('#brw_pic_ktp').val(brw_pic_ktp);
            var brw_pic_user_ktp = "{{ $brw_pic_user_ktp }}";
            $('#brw_pic_user_ktp').val(brw_pic_user_ktp);
            var brw_pic_npwp = "{{ $brw_pic_npwp }}";
            $('#brw_pic_npwp').val(brw_pic_npwp);

            // informasi pekerjaan
            // fix
            var pekerjaan_id = `{!! $pekerjaan_id !!}`;
            var jenis_pekerjaan = "{{ $jenis_pekerjaan }}";
            var jenis_properti = "{{ $jenis_properti }}";
          
            var nama_perusahaan = "{{ $nama_perusahaan }}";
            var bidang_online = "{{ $bidang_online }}";
            var bentuk_badan_usaha = "{{ $bentuk_badan_usaha }}";
            var status_kepegawaian = "{{ $status_kepegawaian }}";
            var usia_perusahaan = "{{ $usia_perusahaan }}";
            var departemen = "{{ $departemen }}";
            var jabatan = "{{ $jabatan }}";
            var tahun_bekerja = "{{ $tahun_bekerja }}";
            var bulan_bekerja = "{{ $bulan_bekerja }}";
            var nip = "{{ $nip }}";
            var nama_hrd = "{{ $nama_hrd }}";
            var no_fixed_line_hrd = "{{ $no_fixed_line_hrd }}";
            var alamat_perusahaan = `{!! $alamat_perusahaan !!}`;
            var rt_perusahaan = "{{ $rt_perusahaan }}";
            var rw_perusahaan = "{{ $rw_perusahaan }}";
            var provinsi_perusahaan = "{{ $provinsi_perusahaan }}";
            var kabupaten_perusahaan = "{{ $kabupaten_perusahaan }}";
            var kecamatan_perusahaan = "{{ $kecamatan_perusahaan }}";
            var kelurahan_perusahaan = "{{ $kelurahan_perusahaan }}";
            var kodepos_perusahaan = "{{ $kodepos_perusahaan }}";
            var no_telpon_usaha = "{{ $no_telpon_usaha }}";
            var tahun_pengalaman_bekerja = "{{ $tahun_pengalaman_bekerja }}";
            var bulan_pengalaman_bekerja = "{{ $bulan_pengalaman_bekerja }}";
            var penghasilan = "{{ $penghasilan }}";
            var biaya_hidup = "{{ $biaya_hidup }}";
            var detail_penghasilan_lain_lain = "{{ $detail_penghasilan_lain_lain }}";
            var total_penghasilan_lain_lain = "{{ $total_penghasilan_lain_lain }}";
            var nilai_spt = "{{ $nilai_spt }}";
            // nonfix
            // var nama_perusahaan_non = "{{ $nama_perusahaan_non }}";
            var bidang_online_non = "{{ $bidang_online_non }}";
            var lama_usaha_non = "{{ $lama_usaha_non }}";
            var lama_tempat_usaha_non = "{{ $lama_tempat_usaha_non }}";
            var no_telpon_non = "{{ $no_telpon_non }}";
            var no_hp_non = "{{ $no_hp_non }}";
            var alamat_perusahaan_non = `{!! $alamat_perusahaan_non !!}`;
            var rt_perusahaan_non = "{{ $rt_perusahaan_non }}";
            var rw_perusahaan_non = "{{ $rw_perusahaan_non }}";
            var provinsi_perusahaan_non = "{{ $provinsi_perusahaan_non }}";
            var kabupaten_perusahaan_non = "{{ $kabupaten_perusahaan_non }}";
            var kecamatan_perusahaan_non = "{{ $kecamatan_perusahaan_non }}";
            var kelurahan_perusahaan_non = "{{ $kelurahan_perusahaan_non }}";
            var kodepos_perusahaan_non = "{{ $kodepos_perusahaan_non }}";
            var surat_ijin_non = "{{ $surat_ijin_non }}";
            var nomor_ijin_non = "{{ $nomor_ijin_non }}";
            var penghasilan_non = "{{ $penghasilan_non }}";
            var biaya_hidup_non = "{{ $biaya_hidup_non }}";

            // $('#nama_perusahaan_non').val(nama_perusahaan_non);
            $('#bidang_online_non').val(bidang_online_non);
            $('#lama_usaha_non').val(lama_usaha_non);
            $('#lama_tempat_usaha_non').val(lama_tempat_usaha_non);
            if (no_telpon_non.substring(0, 3) == '620') {
                $('#no_telpon_non').val(no_telpon_non.substring(3));
            } else if (no_telpon_non.substring(0, 2) == '62') {
                $('#no_telpon_non').val(no_telpon_non.substring(2));
            } else if (no_telpon_non.substring(0, 1) == '0') {
                $('#no_telpon_non').val(no_telpon_non.substring(1));
            } else {
                $('#no_telpon_non').val(no_telpon_non);
            }
            if (no_hp_non.substring(0, 3) == '620') {
                $('#no_hp_non').val(no_hp_non.substring(3));
            } else if (no_hp_non.substring(0, 2) == '62') {
                $('#no_hp_non').val(no_hp_non.substring(2));
            } else if (no_hp_non.substring(0, 1) == '0') {
                $('#no_hp_non').val(no_hp_non.substring(1));
            } else {
                $('#no_hp_non').val(no_hp_non);
            }
            $('#alamat_perusahaan_non').val(alamat_perusahaan_non);
            $('#rt_perusahaan_non').val(rt_perusahaan_non);
            $('#rw_perusahaan_non').val(rw_perusahaan_non);
            $('#provinsi_perusahaan_non').val(provinsi_perusahaan_non);
            $('#kabupaten_perusahaan_non').val(kabupaten_perusahaan_non);
            $('#kecamatan_perusahaan_non').val(kecamatan_perusahaan_non);
            $('#kelurahan_perusahaan_non').val(kelurahan_perusahaan_non);
            $('#kodepos_perusahaan_non').val(kodepos_perusahaan_non);
            if (surat_ijin_non == 2) {
                $('.bloksuratIjin').addClass("d-none");
            } else if (surat_ijin_non == 1) {
                $('#surat_ijin_non').val("Ya");
            }
            $('#nomor_ijin_non').val(nomor_ijin_non);
            $('#penghasilan_non').val(penghasilan_non);
            $('#biaya_hidup_non').val(biaya_hidup_non);

            // PENGAJUAN 
            var type_pendanaan = `{!! $type_pendanaan !!}`;
            var tujuan_pendanaan = `{!! $tujuan_pendanaan !!}`;
            var detail_pendanaan = `{!! $detail_pendanaan !!}`;
            var alamat_objek_pendanaan = `{!! $alamat_objek_pendanaan !!}`;
            var provinsi_objek_pendanaan = `{!! $provinsi_objek_pendanaan !!}`;
            var kota_objek_pendanaan = `{!! $kota_objek_pendanaan !!}`;
            var kecamatan_objek_pendanaan = `{!! $kecamatan_objek_pendanaan !!}`;
            var kelurahan_objek_pendanaan = `{!! $kelurahan_objek_pendanaan !!}`;
            var kodepos_objek_pendanaan = `{!! $kodepos_objek_pendanaan !!}`;
            var harga_objek_pendanaan = `{!! $harga_objek_pendanaan !!}`;
            var uang_muka = `{!! $uang_muka !!}`;
            var nilai_pengajuan = `{!! $nilai_pengajuan !!}`;
            var jangka_waktu = `{!! $jangka_waktu !!}`;
            var nama_pemilik = `{!! $nama_pemilik !!}`;
            var tlp_pemilik = `{!! $tlp_pemilik !!}`;
            var alamat_pemilik = `{!! $alamat_pemilik !!}`;
            var provinsi_pemilik = `{!! $provinsi_pemilik !!}`;
            var kota_pemilik = `{!! $kota_pemilik !!}`;
            var kecamatan_pemilik = `{!! $kecamatan_pemilik !!}`;
            var kelurahan_pemilik = `{!! $kelurahan_pemilik !!}`;
            var kodepos_pemilik = `{!! $kodepos_pemilik !!}`;

            $('#type_pendanaan').val(type_pendanaan);
            $('#tujuan_pendanaan').val(tujuan_pendanaan);
            $('#detail_pendanaan').text(detail_pendanaan);
            $('#alamat_objek_pendanaan').val(alamat_objek_pendanaan);
            $('#provinsi_objek_pendanaan').val(provinsi_objek_pendanaan);
            $('#kota_objek_pendanaan').val(kota_objek_pendanaan);
            $('#kecamatan_objek_pendanaan').val(kecamatan_objek_pendanaan);
            $('#kelurahan_objek_pendanaan').val(kelurahan_objek_pendanaan);
            $('#kodepos_objek_pendanaan').val(kodepos_objek_pendanaan);
            $('#harga_objek_pendanaan').val(harga_objek_pendanaan);
            $('#uang_muka').val(uang_muka);
            $('#nilai_pengajuan').val(nilai_pengajuan);
            $('#jangka_waktu').val(jangka_waktu);
            $('#nama_pemilik').val(nama_pemilik);
            $('#jenis_properti').val(jenis_properti);
            if (tlp_pemilik.substring(0, 3) == '620') {
                $('#tlp_pemilik').val(tlp_pemilik.substring(3));
            } else if (tlp_pemilik.substring(0, 2) == '62') {
                $('#tlp_pemilik').val(tlp_pemilik.substring(2));
            } else if (tlp_pemilik.substring(0, 1) == '0') {
                $('#tlp_pemilik').val(tlp_pemilik.substring(1));
            } else {
                $('#tlp_pemilik').val(tlp_pemilik);
            }
            $('#alamat_pemilik').val(alamat_pemilik);
            $('#provinsi_pemilik').val(provinsi_pemilik);
            $('#kota_pemilik').val(kota_pemilik);
            $('#kecamatan_pemilik').val(kecamatan_pemilik);
            $('#kelurahan_pemilik').val(kelurahan_pemilik);
            $('#kodepos_pemilik').val(kodepos_pemilik);

            // data verif
            var nilai_pefindo = `{!! $nilai_pefindo !!}`;
            var grade_pefindo = `{!! $grade_pefindo !!}`;
            var skor_personal = `{!! $skor_personal !!}`;
            var skor_pendanaan = `{!! $skor_pendanaan !!}`;
            var complete_id = `{!! $complete_id !!}`;
            var ocr = `{!! $ocr !!}`;
            var npwpa = `{!! $npwpa !!}`;
            var npwpc = `{!! $npwpc !!}`;
            var npwpd = `{!! $npwpd !!}`;
            var npwp_verify = `{!! $npwp_verify !!}`;
            var workplace_verification_f = `{!! $workplace_verification_f !!}`;
            var negative_list_verification_j = `{!! $negative_list_verification_j !!}`;
            var verify_property_k = `{!! $verify_property_k !!}`;
            var ftv = `{!! $ftv !!}`;
            var catatan_verifikasi = `{!! $catatan_verifikasi !!}`;
            var keputusan = `{!! $keputusan !!}`;
            var isiVerif = `{!! $isiVerif !!}`;
            if (isiVerif == 1) {
                $('#nilai_pefindo').val(nilai_pefindo);
                $("#grade_pefindo option[value='" + grade_pefindo + "']").attr('selected', 'selected');
                $('#skor_personal').val(skor_personal);
                $('#skor_pendanaan').val(skor_pendanaan);

                if (complete_id == 1) {
                    $("input[name=verijelas][value='complete_id']").attr('checked', 'checked');
                }
                if (ocr == 1) {
                    $("input[name=verijelas][value='ocr']").attr('checked', 'checked');
                }
                if (npwpa == 1) {
                    $("input[name=verijelas][value='npwpa']").attr('checked', 'checked');
                }
                if (npwpc == 1) {
                    $("input[name=verijelas][value='npwpc']").attr('checked', 'checked');
                }
                if (npwpd == 1) {
                    $("input[name=verijelas][value='npwpd']").attr('checked', 'checked');
                }
                if (npwp_verify == 1) {
                    $("input[name=verijelas][value='npwp_verify']").attr('checked', 'checked');
                }
                if (workplace_verification_f == 1) {
                    $("input[name=verijelas][value='workplace_verification_f']").attr('checked', 'checked');
                }
                if (negative_list_verification_j == 1) {
                    $("input[name=verijelas][value='negative_list_verification_j']").attr('checked', 'checked');
                }
                if (verify_property_k == 1) {
                    $("input[name=verijelas][value='verify_property_k']").attr('checked', 'checked');
                }

                $('#ftv').val(ftv);
                $('#catatan_verifikasi').val(catatan_verifikasi);
                $("#keputusan option[value='" + keputusan + "']").attr('selected', 'selected');

                // disabled form verifikasi
                $('#nilai_pefindo').attr("disabled", true);
                $('#grade_pefindo').attr("disabled", true);
                $('#ftv').attr("disabled", true);
                $('#catatan_verifikasi').attr("disabled", true);
                $('#keputusan').attr("disabled", true);
                $("input[name=verijelas]").attr("disabled", true);
                $("#submitverfikasi").attr("disabled", true);

                $("#btnCekVerijelas").attr('disabled', true);
            } else {
                // credolab

                // let reference_number_personal = '288';
                // let reference_number_pendanaan = '288_80';
                
                // let reference_number_personal = brw_id;
                // let reference_number_pendanaan = brw_id + '_' + pengajuanId;
                // var url_per = '{{ route('get-credolab-score', ':reference_number') }}';

                // Borrower Score personal
                // Disable credolab
                // var v_url = url_per.replace(':reference_number', reference_number_personal);
                // $.getJSON(v_url, function(response) {
                //     console.log([v_url, response]);
                //     if (response.status_code == 200) {
                //         $('#skor_personal').val(response.credolab_score);
                //     } else {
                //         $('#skor_personal').val('0');
                //     }
                // });

                // Borrower Score pendanaan
                // var url_pen = '{{ route('get-credolab-score', ':reference_number ') }}';
                // var vpen_url = url_pen.replace(':reference_number', reference_number_pendanaan);
                // $.getJSON(vpen_url, function(response) {
                //     console.log([vpen_url, response]);
                //     if (response.status_code == 200) {
                //         $('#skor_pendanaan').val(response.credolab_score);
                //     } else {
                //         $('#skor_pendanaan').val('0');
                //     }
                // });

                $("#btnCekVerijelas").attr('disabled', false);
            }


            $('#skor_personal').val('0');
            $('#skor_pendanaan').val('0');
            // START: Informasi Pribadi
        
            // $('#ktp').val(ktp);
            $('#kk').val(kk);
            $('#ibukandung').val(nm_ibu);
           
            let no_tlp_check = telepon;
            if (no_tlp_check.substring(0, 3) == '620') {
                $('#telepon').val(no_tlp_check.substring(3));
            } else if (no_tlp_check.substring(0, 2) == '62') {
                $('#telepon').val(no_tlp_check.substring(2));
            } else if (no_tlp_check.substring(0, 1) == '0') {
                $('#telepon').val(no_tlp_check.substring(1));
            } else {
                $('#telepon').val(no_tlp_check);
            }

            $("#pasangan_agama").val(pasangan_agama);
            $("#agama").val(agama);

            $("#pasangan_pendidikan_terakhir").val(pasangan_pendidikan_terakhir);
            $("#pendidikan_terakhir").val(pendidikan_terakhir);
            $("#status_kawin").val(status_kawin);

           
            $('#pasangan_provinsi').val(pasangan_provinsi);
            $('#pasangan_alamat').text(pasangan_alamat);

            if (status_rumah == 1) {
                status_rumah = "Milik Pribadi";
            } else if (status_rumah == 2) {
                status_rumah = "Sewa";
            } else {
                status_rumah = "Milik Keluarga";
            }
            $('#status_rumah').val(status_rumah);

            if (domisili_status_rumah == 1) {
                domisili_status_rumah = "Milik Pribadi";
            } else if (domisili_status_rumah == 2) {
                domisili_status_rumah = "Sewa";
            } else {
                domisili_status_rumah = "Milik Keluarga";
            }
            $('#domisili_status_rumah').val(domisili_status_rumah);


            // END: Alamat 

            // START: Domisili ALAMAT
            $("#pasangan_kota").val(pasangan_kota);

            $("#pasangan_kecamatan").val(pasangan_kecamatan);

            $("#pasangan_kelurahan").val(pasangan_kelurahan);

            $("#domisili_kode_pos").val(domisili_kode_pos);
            $('#pasangan_kode_pos').val(pasangan_kode_pos);

            // END: Alamat domisili

            // START: Pasangan
            $('#pasangan_nama').val(pasangan_nama);
            if (pasangan_jenis_kelamin == 1) {
                pasangan_jenis_kelamin = "Laki-laki";
            } else {
                pasangan_jenis_kelamin = "Perempuan";
            }
            $('#pasangan_jenis_kelamin').val(pasangan_jenis_kelamin);
            $('#pasangan_ktp').val(pasangan_ktp);
            $('#pasangan_tempat_lahir').val(pasangan_tempat_lahir);
            $('#pasangan_tanggal_lahir').val(pasangan_tanggal_lahir);

            if (pasangan_telepon.substring(0, 3) == '620') {
                $('#pasangan_telepon').val(pasangan_telepon.substring(3));
            } else if (pasangan_telepon.substring(0, 2) == '62') {
                $('#pasangan_telepon').val(pasangan_telepon.substring(2));
            } else if (pasangan_telepon.substring(0, 1) == '0') {
                $('#pasangan_telepon').val(pasangan_telepon.substring(1));
            } else {
                $('#pasangan_telepon').val(pasangan_telepon);
            }

            $('#pasangan_npwp').val(pasangan_npwp);

            // END: Informasi Pribadi

            // START: Pekerjaan Individu
            $('#jenis_pekerjaan').val(jenis_pekerjaan);
         
            if (pekerjaan_id == '5') { //Wiraswasta
                $('.blokNonFixedIncome').removeClass('d-none');
                $('.blokFixedIncome').addClass('d-none');
            } 

           
            // $('#nama_perusahaan').val(nama_perusahaan);
            $('#bidang_online').val(bidang_online);
            $('#bentuk_badan_usaha').val(bentuk_badan_usaha);

            $('#status_kepegawaian').val(status_kepegawaian);
            $('#usia_perusahaan').val(usia_perusahaan);
            $('#departemen').val(departemen);
            $('#jabatan').val(jabatan);
            $('#tahun_bekerja').val(tahun_bekerja);
            $('#bulan_bekerja').val(bulan_bekerja);
            $('#nip').val(nip);
            $('#nama_hrd').val(nama_hrd);
            if (no_fixed_line_hrd.substring(0, 3) == '620') {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(3));
            } else if (no_fixed_line_hrd.substring(0, 2) == '62') {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(2));
            } else if (no_fixed_line_hrd.substring(0, 1) == '0') {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(1));
            } else {
                $('#no_fixed_line_hrd').val(no_fixed_line_hrd);
            }
            $('#alamat_perusahaan').val(alamat_perusahaan);
            $('#rt_perusahaan').val(rt_perusahaan);
            $('#rw_perusahaan').val(rw_perusahaan);
            $('#provinsi_perusahaan').val(provinsi_perusahaan);
            $('#kabupaten_perusahaan').val(kabupaten_perusahaan);
            $('#kecamatan_perusahaan').val(kecamatan_perusahaan);
            $('#kelurahan_perusahaan').val(kelurahan_perusahaan);
            $('#kodepos_perusahaan').val(kodepos_perusahaan);
            $('#no_telpon_usaha').val(no_telpon_usaha);
            if (no_telpon_usaha.substring(0, 3) == '620') {
                $('#no_telpon_usaha').val(no_telpon_usaha.substring(3));
            } else if (no_telpon_usaha.substring(0, 2) == '62') {
                $('#no_telpon_usaha').val(no_telpon_usaha.substring(2));
            } else if (no_telpon_usaha.substring(0, 1) == '0') {
                $('#no_telpon_usaha').val(no_telpon_usaha.substring(1));
            } else {
                $('#no_telpon_usaha').val(no_telpon_usaha);
            }
            $('#tahun_pengalaman_bekerja').val(tahun_pengalaman_bekerja);
            $('#bulan_pengalaman_bekerja').val(bulan_pengalaman_bekerja);
            $('#penghasilan').val(penghasilan);
            $('#biaya_hidup').val(biaya_hidup);
            $('#detail_penghasilan_lain_lain').val(detail_penghasilan_lain_lain);
            $('#total_penghasilan_lain_lain').val(total_penghasilan_lain_lain);
            $('#nilai_spt').val(nilai_spt);
            if (detail_penghasilan_lain_lain == "" || detail_penghasilan_lain_lain == null) {
                $('.blokDetailPenghasilan').addClass('d-none');
            }
            // END: Pekerjaan Individu

            $('#verijelas_modal').on('show.bs.modal', function(event) {
               
                var buttonTrigger = $(event.relatedTarget);
                var checkVerijelas = buttonTrigger.data('check');
                $('.modal-title').html('{{ trans('verify.hasil.title') }}' + ' - ' + nama);
                
                if(checkVerijelas == '1') {
                    $('.btnCheckNext').removeClass('d-none');
                }else{
                    $('.btnCheckNext').addClass('d-none');
                }

                veriJelasData();
            });


            $('.btnCheckNext').click(function(){
                Swal.fire({
                    title: "Notifikasi",
                    text: "Apakah anda yakin ingin melanjutkan ?",
                    type: "warning",
                    buttons: true,
                    showCancelButton: true,
                }).then(result => {
                    if (result.value == true) {
                        $('#verijelas_modal').modal('hide');
                        $('#loading_modal').modal('show');
                        $('.modal-title').html('{{ trans('verify.title') }}');
                        veriJelasCheck();
                        document.getElementById('btnCloseHasil').click();
                    } else {
                        document.getElementById('btnCloseLoading').click();
                    }
                });

            });

            $('body').on('show.bs.modal', function() {
                $('.modal-open').css('padding-right', '0px');
            });

            // $('#loading_modal').on('show.bs.modal', function(event) {
            //     $('.modal-title').html('{{ trans('verify.title') }}');
            //     veriJelasCheck();
            // });

            $('#loading_modal').on('hidden.bs.modal', function(e) {
                $('#loading_footer').addClass('d-none');
            });


            function veriJelasData() {

                $.ajax({
                    url: "{{ route('verify.data') }}",
                    type: "post",
                    dataType: "html",
                    data: {
                        _token: "{{ csrf_token() }}",
                        borrower_id: brw_id,
                        brw_type: brw_type,
                        vendor: 'verijelas'
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        if (xhr.status == '419') {
                            errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                        }

                        $('#loading_title').html('Data Failed');
                        $('#loading_content_data').html(errorMessage);
                        $('#loading_footer_data').removeClass('d-none');
                    },
                    success: function(result) {
                        $('#loading_title').html('Preparing Result ...');
                        $('#loading_content_data').html(result);
                        $('#loading_title').html('');
                        $('#loading_footer_data').removeClass('d-none');
                    }
                });


            }


            function veriJelasTimeout(errorMessage){
                $.ajax({
                    url: "{{ route('verify.timeout') }}",
                    type: "post",
                    dataType: "json",
                    data: {
                        _token: "{{ csrf_token() }}",
                        description: errorMessage
                        
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText
                        if (xhr.status === 419) {
                            errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                        }

                    },
                    success: function(result) {
                        // console.log("Log timeout" + result);
                    }
                });
            }

            function veriJelasCheck() {

                const checkCompleteId = $("#complete_id").is(":checked") ? 1 : 0;
                const checkOcr = $("#ocr").is(":checked") ? 1 : 0;
                const checkNpwp = $("#npwp_verify").is(":checked") ? 1 : 0;
                const checkTempatKerja = $("#workplace_verification_f").is(":checked") ? 1 : 0;
                const checkNegativeList = $("#negative_list_verification_j").is(":checked") ? 1 : 0;

                let verijelas_timeout = `{{ config('app.verijelas_timeout') }}`;
                let timeout_value = (verijelas_timeout * 1000) / 60000; //menit

                $.ajax({
                    url: "{{ route('verify.check') }}",
                    type: "post",
                    dataType: "html",
                    timeout: verijelas_timeout * 1000,
                    //timeout: 120000, //120 seconds
                    // timeout: 30000,//30 seconds
                    data: {
                        _token: "{{ csrf_token() }}",
                        borrower_id: brw_id,
                        pengajuan_id: pengajuanId,
                        brw_type: brw_type,
                        complete_id: checkCompleteId,
                        ocr: checkOcr,
                        npwp_verify: checkNpwp,
                        tempat_kerja: checkTempatKerja,
                        negativelist: checkNegativeList,
                        vendor: 'verijelas',
                        origin: "WEB"
                    },
                    error: function(xhr, status, error) {

                        var errorMessage = xhr.status + ': ' + xhr.statusText

                        if (xhr.status === 419) {
                            errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                        }

                        if(status == "timeout"){
                            errorMessage = "Error: Proses verifikasi melebihi waktu > "+timeout_value+" menit ";
                            veriJelasTimeout(errorMessage);
                        }

                        $('#loading_title').html('Check Failed');
                        $('#loading_content').html(errorMessage);
                        $('#loading_footer').removeClass('d-none');
                    },
                    success: function(result) {

                        $('#loading_title').html('Preparing Result ...');
                        $('#loading_content').html(result);
                        $('#loading_title').html('');
                        $('#loading_footer').removeClass('d-none');
                    }
                });
            }
        });
    </script>

@endsection
