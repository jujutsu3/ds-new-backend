<div class="card-body tabpengajuan d-none">
    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan   ">Form Informasi
                    Objek Pendanaan &nbsp</label>
            </div>
            <hr>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Tipe Pendanaan</label><label class="mandatory_label">*</label>
                <input class="form-control type_pendanaan" id="type_pendanaan" name="type_pendanaan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Tujuan Pendanaan</label><label
                    class="mandatory_label">*</label>
                <input class="form-control tujuan_pendanaan" id="tujuan_pendanaan" name="tujuan_pendanaan" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-tempatlahir">Alamat Objek Pendanaan</label> <label
                    class="mandatory_label">*</label>
                <input class="form-control" id="alamat_objek_pendanaan" name="alamat_objek_pendanaan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Provinsi</label>
                <label class="mandatory_label">*</label>
                <input class="form-control" id="provinsi_objek_pendanaan" name="provinsi_objek_pendanaan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kota">Kota / Kabupaten</label> <label class="mandatory_label">*</label>
                <input class="form-control" id="kota_objek_pendanaan" name="kota_objek_pendanaan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Kecamatan</label>
                <label class="mandatory_label">*</label>
                <input class="form-control" id="kecamatan_objek_pendanaan" name="kecamatan_objek_pendanaan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Kelurahan</label>
                <label class="mandatory_label">*</label>
                <input class="form-control" id="kelurahan_objek_pendanaan" name="kelurahan_objek_pendanaan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Kode
                    Pos</label> <label class="mandatory_label">*</label>
                <input class="form-control" id="kodepos_objek_pendanaan" name="kodepos_objek_pendanaan" readonly>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Harga Objek Pendanaan </label> <label
                    class="mandatory_label">*</label>
                <input type="text" class="form-control" id="harga_objek_pendanaan" name="harga_objek_pendanaan"
                    readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Uang Muka </label> <label class="mandatory_label">*</label>
                <input type="text" class="form-control" id="uang_muka" name="uang_muka" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Nilai Pengajuan Pendanaan </label> <label
                    class="mandatory_label">*</label>
                <input type="text" class="form-control" id="nilai_pengajuan" name="nilai_pengajuan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Jangka Waktu (Bulan) </label> <label
                    class="mandatory_label">*</label>
                <input type="text" class="form-control" id="jangka_waktu" name="jangka_waktu" readonly>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Detail Informasi Objek Pendanaan</label>
                <textarea class="form-control" rows="4" cols="80" id="detail_pendanaan" name="detail_pendanaan" disabled></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="informasi_pemilik_objek_Pendanaan">Informasi Pemilik
                    Objek Pendanaan
                    &nbsp</label>
            </div>
            <hr>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Nama</label>
                <input class="form-control" id="nama_pemilik" name="nama_pemilik" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">No. Telepon/HP</label>
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text input-group-text-dsi">
                            +62 </span>
                    </div>
                    <input class="form-control" id="tlp_pemilik" name="tlp_pemilik" disabled>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Alamat</label>
                <textarea class="form-control" rows="4" cols="80" id="alamat_pemilik" name="alamat_pemilik" disabled></textarea>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Provinsi</label>
                <input class="form-control" id="provinsi_pemilik" name="provinsi_pemilik" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Kota/Kabupaten</label>
                <input class="form-control" id="kota_pemilik" name="kota_pemilik" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Kecamatan</label>
                <input class="form-control" id="kecamatan_pemilik" name="kecamatan_pemilik" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Kelurahan</label>
                <input class="form-control" id="kelurahan_pemilik" name="kelurahan_pemilik" disabled>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapendanaan">Kode Pos</label>
                <input class="form-control" id="kodepos_pemilik" name="kodepos_pemilik" disabled>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi Pendanaan
                    Berjalan </label>
            </div>
            <hr>
        </div>


        <div class="col-12 col-md-12">
            <div class="col-8 my-auto">
                <label>Ini merupakan status kepemilikan
                    rumah ke - ?</label>
            </div>
            <div class="col-4 text-right my-auto">
                <input type="text" readonly value="{{ $rumah_ke }}" name="kepemilikan_rumah_ke"
                    class="form-control" readonly />
            </div>

        </div>


        <div class="col-12 col-md-12">
            <div class="col-8 my-auto">
                <label>Kepemilikan rumah lainnya sebanyak ?
                </label>
            </div>
            <div class="col-4 text-right my-auto">
                <input type="text" readonly value="{{ isset($rumah_ke) && $rumah_ke > 0 ? $rumah_ke - 1 : 0 }}"
                    name="kepemilikan_rumah_ke" class="form-control" />
            </div>

        </div>

        <div class="col-12 col-md-12">
            &nbsp;
        </div>
        <div class="col-12 col-md-12">
            <div class="table-responsive">
                <table class="table table-sm" id="table_dana_rumah_lain">
                    <thead class="thead-dark" style="font-size: 12px;">
                        <tr>
                            <th>No.</th>
                            <th>Status</th>
                            <th>Bank</th>
                            <th>Plafond</th>
                            <th>Jangka Waktu</th>
                            <th>Outstanding</th>
                            <th>Angsuran</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($data_rumah_lain as $key => $val)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $val->status == '1' ? 'Sedang Berjalan' : 'Lunas' }}</td>
                                <td>{{ $val->bank }}</td>
                                <td>Rp{{ number_format($val->plafond, 0, '.', '.') }}</td>
                                <td>{{ $val->jangka_waktu }}</td>
                                <td>Rp{{ number_format($val->outstanding, 0, '.', '.') }}</td>
                                <td>Rp{{ number_format($val->angsuran, 0, '.', '.') }}</td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>
        <div class="col-12 col-md-12">
            <div class="col-8 my-auto">
                <label class="form-label">Apakah anda memiliki fasilitas
                    pembiayaan berjalan selain kepemilikan rumah ?
                </label>
            </div>
            <div class="col-4 text-right my-auto">
                <select name="status_bank" id="status_bank" class="form-control custom-select" disabled>
                    <option value="1" {{ $status_bank_non_rumah_lain == '1' ? 'selected' : '' }}>Ya
                    </option>
                    <option value="2" {{ $status_bank_non_rumah_lain == '2' ? 'selected' : '' }}>Tidak
                    </option>
                </select>
            </div>

        </div>

        <div class="col-12 col-md-12">
            &nbsp;
        </div>

        <div class="col-12 col-md-12">
            <div class="table-responsive">
                <table class="table table-sm" id="table_dana_non_rumah_lain">
                    <thead class="thead-dark" style="font-size: 12px;">
                        <tr>
                            <th>Bank</th>
                            <th>Plafond (Rp)</th>
                            <th>Jangka Waktu</th>
                            <th>Outstanding (Rp)</th>
                            <th>Angsuran (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($data_non_rumah_lain as $key => $val)
                            <tr>
                                <td>{{ $val->bank }}</td>
                                <td>Rp{{ number_format($val->plafond, 0, '.', '.') }}</td>
                                <td>{{ $val->jangka_waktu }}</td>
                                <td>Rp{{ number_format($val->outstanding, 0, '.', '.') }}</td>
                                <td>Rp{{ number_format($val->angsuran, 0, '.', '.') }}</td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>

    </div>


    <div class="row">
        <div class="col-12 mb-4">
            <br>
            <div class="form-check form-check-inline line">
                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan">Informasi Agunan
                </label>
            </div>
            <hr>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label for="jenis_agunan" class="form-label">Jenis
                    Agunan</label>
                {!! Form::select('jenis_agunan', ['1' => 'SHM', '2' => 'HGB'], $dokumen_pendukung->jenis_agunan, ['id' => 'jenis_agunan', 'class' => 'form-control custom-select', 'disabled' => 'true']) !!}
            </div>
        </div>


        <div class="col-md-4">
            <div class="form-group">
                <label for="nomor_agunan" class="form-label">Nomor
                    Agunan</label>
                <input type="text" name="nomor_agunan" value="{{ $dokumen_pendukung->nomor_agunan }}"
                    id="nomor_agunan" class="form-control" readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="nomor_agunan" class="form-label">Atas Nama
                    Agunan</label>
                <input type="text" name="atas_nama_agunan" id="atas_nama_agunan"
                    value="{{ $dokumen_pendukung->atas_nama_agunan }}" class="form-control" readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="luas_bangunan" class="form-label">Luas Bangunan
                    (m2)</label>
                <input type="text" name="luas_bangunan" id="luas_bangunan"
                    value="{{ $dokumen_pendukung->luas_bangunan }}" class="form-control numberOnly no-zero"
                    readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="luas_tanah" class="form-label">Luas Tanah
                    (m2)</label>
                <input type="text" name="luas_tanah" id="luas_tanah" value="{{ $dokumen_pendukung->luas_tanah }}"
                    class="form-control numberOnly no-zero" readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="nomor_surat_ukur" class="form-label">Nomor Surat
                    Ukur </label>
                <input type="text" name="nomor_surat_ukur" id="nomor_surat_ukur"
                    value="{{ $dokumen_pendukung->nomor_surat_ukur }}" class="form-control" readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="nomor_surat_ukur" class="form-label">Tanggal Surat
                    Ukur </label>
                <input type="date" class="form-control" id="tanggal_surat_ukur" data-date-format="dd-mm-YYYY"
                    name="tanggal_surat_ukur" value="{{ $dokumen_pendukung->tanggal_surat_ukur }}"
                    id="tanggal_surat_ukur" data-allow-input="false" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="tanggal_terbit" class="form-label">Tanggal Terbit
                </label>

                <input type="date" class="form-control" id="tanggal_terbit" data-date-format="dd-mm-YYYY"
                    name="tanggal_terbit" value="{{ $dokumen_pendukung->tanggal_terbit }}" data-allow-input="false"
                    readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="kantor_penerbit" class="form-label">Kantor
                    Penerbit</label>

                {!! Form::select('kantor_penerbit', $kantor_bpn, $dokumen_pendukung->kantor_penerbit, ['id' => 'kantor_penerbit', 'class' => 'form-control custom-select', 'disabled' => 'true']) !!}
            </div>
        </div>

        <div class="col-md-12">
            &nbsp;
        </div>

        <div class="col-md-4 d-none" id="div_tanggal_jatuh_tempo">
            <div class="form-group">
                <label for="nomor_surat_ukur" class="form-label">Tanggal Jatuh Tempo
                </label>
                <input type="date" class="form-control" id="tanggal_jatuh_tempo" data-date-format="d-m-Y"
                    name="tanggal_jatuh_tempo" value="{{ $dokumen_pendukung->tanggal_jatuh_tempo }}"
                    placeholder="dd-mm-YYYY" data-allow-input="false" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="alamat_agunan" class="form-label">Alamat
                    Agunan</label>
                <input type="text" name="alamat_agunan" id="alamat_agunan"
                    value="{{ $dokumen_pendukung->lokasi_proyek }}" class="form-control" readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="blok_nomor" class="form-label">Blok/Nomor
                </label>
                <input type="text" id="blok_nomor" name="blok_nomor" value="{{ $dokumen_pendukung->blok_nomor }}"
                    class="form-control" readonly />
            </div>
        </div>

        <div class="col-md-12">
            &nbsp;
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="RT" class="form-label">RT
                </label>
                <input type="text" name="RT" value="{{ $dokumen_pendukung->RT }}" id="RT" class="form-control"
                    readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="RW" class="form-label">RW
                </label>
                <input type="text" name="RW" value="{{ $dokumen_pendukung->RW }}" id="RW" class="form-control"
                    readonly />
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Provinsi</label>
                <label class="mandatory_label">*</label>
                <input class="form-control" id="provinsi_agunan" name="provinsi_agunan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-kota">Kota / Kabupaten</label> <label class="mandatory_label">*</label>
                <input class="form-control" id="kota_agunan" name="kota_agunan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Kecamatan</label>
                <label class="mandatory_label">*</label>
                <input class="form-control" id="kecamatan_agunan" name="kecamatan_agunan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Kelurahan</label>
                <label class="mandatory_label">*</label>
                <input class="form-control" id="kelurahan_agunan" name="kelurahan_agunan" readonly>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="wizard-progress2-namapengguna">Kode
                    Pos</label> <label class="mandatory_label">*</label>
                <input class="form-control" id="kodepos_agunan" name="kodepos_agunan" readonly>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="cek_imb" class="form-label">IMB
                </label>
                @if (!empty($dokumen_pendukung->no_imb))
                    <label class="form-control" readonly> Ada </label>
                @else
                    <label class="form-control" readonly> Tidak Ada </label>
                @endif


            </div>
        </div>

        <div class="col-md-4" id="div_no_imb">
            <div class="form-group">
                <label for="no_imb" class="form-label">Nomor IMB
                </label>
                <input type="text" name="no_imb" id="no_imb" value="{{ $dokumen_pendukung->no_imb }}"
                    class="form-control" readonly />
            </div>
        </div>


        <div class="col-md-4" id="div_tanggal_imb">
            <div class="form-group">
                <label for="tanggal_terbit_imb" class="form-label">Tanggal
                    Terbit IMB
                </label>
                <input type="date" class="form-control" id="tanggal_terbit_imb" data-min-date="today"
                    data-date-format="d-m-Y" name="tanggal_terbit_imb"
                    value="{{ $dokumen_pendukung->tanggal_terbit_imb }}" placeholder="dd-mm-YYYY"
                    data-allow-input="false" readonly>
            </div>
        </div>

    </div>
    <div class="col-md-12">
        <hr>
    </div>
    <div class="col-md-2">
        <button type="submit" class="btn btn-block btn-success btn-lg" id="sebelumnya2"><i
                class="fa fa-fw fa-chevron-left"></i>Sebelumnya</button>
    </div>
    <div class="col-md-8">
    </div>
    <div class="col-md-2">
        <button type="submit" class="btn btn-block btn-success btn-lg" id="selanjutnya2">Selanjutnya
            &nbsp; <i class="fa fa-fw fa-chevron-right"></i></button>
    </div>
</div>
