@extends('layouts.borrower.master')

@section('title', 'Selamat Datang Penerima Dana')
<style>
  .dataTables_paginate { 
     float: right; 
     text-align: right; 
  }
  #allDetilImbal:hover{
    background-color: forestgreen !important;
  }
  #overlay{   
      position: fixed;
      top: 0;
      left: 0;
      z-index: 900;
      width: 100%;
      height:100%;
      display: none;
      background: rgba(0,0,0,0.6);
  }
  .cv-spinner {
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;  
  }
  .spinner {
      width: 40px;
      height: 40px;
      border: 4px #ddd solid;
      border-top: 4px #2e93e6 solid;
      border-radius: 50%;
      animation: sp-anime 0.8s infinite linear;
  }
  @keyframes sp-anime {
      100% { 
          transform: rotate(360deg); 
      }
  }
  .is-hide{
      display:none;
  }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="assets/js/plugins/slick/slick.css">
        <link rel="stylesheet" href="assets/js/plugins/slick/slick-theme.css">
        <script src="assets/js/plugins/slick/slick.min.js"></script>

        <!-- Page JS Helpers (Slick Slider plugin) -->
        <script>jQuery(function(){ Codebase.helpers('slick'); });</script>

@section('content')
    <div id="overlay">
    <div class="cv-spinner">
          <span class="spinner"></span>
    </div>
    </div>
    <!-- Main Container -->
    <main id="main-container">        
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: 0em;" >{{$namaproyek}}</h1> 
                            <p class="pull-right mt-20 pt-5 text-dark"> 
                            <!-- <i class="fa fa-circle text-primary"></i> Aktif -->
                            <?php 
                                if($status == 0){ //pengajuan
                                    echo "<i class='fa fa-circle text-pengajuan pull-right ml-4 mt-0 pt-0 mr-10'></i> Pengajuan"; 
                                }elseif($status == 1){ //approved / aktif
                                    echo "<i class='fa fa-circle text-success pull-right ml-4 mt-0 pt-0 mr-10'></i> Aktif"; 
                                }elseif($status == 2 || $status == 3){ //peggalangan dana
                                    echo "<i class='fa fa-circle text-penggalangandana pull-right ml-4 mt-0 pt-0 mr-10'></i> Penggalangan Dana";
                                }elseif($status == 6){ //proses tanda tangan
                                    echo "<i class='fa fa-circle text-ttd pull-right ml-4 mt-0 pt-0 mr-10'></i> Proses TTD";
                                }elseif($status == 4){ //selesai
                                    echo "<i class='fa fa-circle text-selesai pull-right ml-4 mt-0 pt-0 mr-10'></i> Selesai";
                                }elseif($status == 7){
                                    echo "<i class='fa fa-circle text-proyekberjalan pull-right ml-4 mt-0 pt-0 mr-10'></i> Proyek Berjalan";
                                }
                            ?>
                            </p>                   
                        </span>
                    </div>
                </div>
                <div class="row mt-5 pt-5">
                    <div class="col-md-12 mt-5 pt-5">
                        <div class="row">
                           
                            <div class="col-12 col-md-12">

                                <center>
                                    <div class="col-md-6">
                                        <h3 class="block-title text-muted mb-10 font-w600">Pendanaan Gallery</h3>
                                        
                                        
                                                <div class="js-slider slick-nav-white slick-nav-hover" data-dots="true" data-arrows="true">
                                                   {{-- @foreach ($gambarProyek as $row) --}}
                                                        <div>
                                                            {{-- <img class="img-fluid" src='{{url("storage/$row->gambar")}}' alt=""> --}}
                                                            <img class="img-fluid" src="https://imgix.bustle.com/uploads/image/2020/6/1/916038fe-c1a3-4ce6-a42a-31c5a868dbd3-ezuww7uuwamdefi.jfif?w=757&h=607&auto=format%2Ccompress&cs=srgb&q=70&fit=crop&crop=faces">
                                                        </div>
                                                        <div>
                                                            {{-- <img class="img-fluid" src='{{url("storage/$row->gambar")}}' alt=""> --}}
                                                            <img class="img-fluid" src="https://www.geeklawblog.com/wp-content/uploads/sites/528/2018/12/liprofile-656x369.png">
                                                        </div>
                                                    {{-- @endforeach --}}
                                                </div>
                                            <!-- END Slider with dots and white hover arrows -->
                                       
                                        <!-- Slider with multiple images and center mode -->
                                        
                                        <!-- div class="block">
                                            <div class="js-slider slick-nav-black slick-nav-hover p-20" data-dots="true" data-arrows="true" data-slides-to-show="4" data-center-mode="false" data-autoplay="true" data-autoplay-speed="3000">
                                                <div class="js-slider slick-nav-black slick-nav-hover slick-initialized slick-slider slick-dotted" data-dots="true" data-arrows="true" data-slides-to-show="4" data-center-mode="false" data-autoplay="true" data-autoplay-speed="3000">
                                                {{-- @foreach ($gambarProyek as $row) --}}
                                                <div>
                                                    {{-- <img class="img-fluid" src='{{url("storage/$row->gambar")}}' alt=""> --}}
                                                </div>
                                                {{-- @endforeach --}}
                                            </div>
                                        </div -->
                                        <!-- END Slider with multiple images and center mode -->
                                    </div>
                                </center>
                                <!-- DISPLAY PROGRESS UNTUK DESKTOP -->

                                <!-- DISPLAY PROGRESS UNTUK DESKTOP -->
                                <!-- DISPLAY PROGRESS UNTUK MOBILE -->
                                <!-- STATUS DETAIL PENDANAAN -->
                                <!-- 
                                <div class="block hidden-xs-up">
                                    <div class="block-header block-header-default">
                                        <h3 class="block-title text-dark">Status Detail</h3>
                                        <div class="block-options">
                                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                            <button type="button" class="btn-block-option" data-toggle="block-option" data-action="state_toggle" data-action-mode="demo">
                                                <i class="si si-refresh"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="block-content">
                                    <div class="col-xs pl-30 pb-30 pt-30">
                                        <p class="font-w600 {{$tc_pengajuan}} mb-0  ">1. Pengajuan Pendanaan
                                            <i class="fa {{$icon_pengajuan}}"></i>
                                            <p class="pl-20 text-dark">Proses tahap pertama, pengajuan pendanaan dan memberikan dokumen pendukung</p>
                                        </p>
                                        <p class="font-w600 {{$tc_verifikasi}} mb-0 ">2. Verifikasi Berhasil
                                            <i class="fa {{$icon_verifikasi}}"></i>
                                            @if ($statusproyek == 1 && ($statusLogSP3 == 1 || $statusLogSP3 == 0))
                                                <button id="sp3" class="btn btn-success">SP3</button>
                                            @endif
                                            <p class="pl-20 text-dark">Proses tahap dua, proses verifikasi kelayakan oleh pihak Dana Syariah & Persetujuan SP3
                                            </p>
                                        </p>
                                        <p class="font-w600 {{$tc_penggalangandana}} mb-0 ">3. Masa Penggalangan Dana
                                            <i class="fa {{$icon_penggalangandana}}"></i>
                                            <p class="pl-20 text-dark">Proses tahap tiga, proses penggalangan dana oleh pihak Dana Syariah</p>
                                        </p>
                                        @if ($status == 6)
                                            <p class="font-w600 {{$tc_ttd}} mb-0 ">4. Daftar dan TTD Pencairan 
                                                <button class="btn btn-success btn_small" onclick="fct()">Daftar</button>
												@php echo $showKontrak; @endphp
                                                @if ($showKontrak == 'buka' || $cekRegDigiSign == null)
                                                <button id="kontrak" class="btn btn-success">Daftar Akad Wakalah</button>
                                                @elseif ($showKontrak == 'ttd_akhir')
                                                <button id="kontrak_ttd_akhir" class="btn btn-success">TTD Akad Wakalah</button>
                                                @elseif ($showKontrak == 'ttd_awal')
                                                <button id="kontrak_ttd_awal" class="btn btn-success">TTD Akad Wakalah</button>
                                                @elseif ($showKontrak == 'unduh')
                                                <button id="kontrak_unduh" class="btn btn-success">Unduh Akad Wakalah</button>
                                                @endif
                                                <p class="pl-20 text-dark">Proses tahap empat, proses Pendaftaran Digisign dan Pencairan Dana oleh pihak Pemohon Pembiayaan</p> 
                                            </p>
                                        @else
                                            <p class="font-w600 {{$tc_ttd}} mb-0 ">4. Daftar dan TTD Pencairan 
                                                <i class="fa {{$icon_ttd}}"></i>
                                                <p class="pl-20 text-dark">Proses tahap empat, proses Pendaftaran Digisign dan Pencairan Dana oleh pihak Pemohon Pembiayaan</p> 
                                            </p>
                                        @endif
                                        <p class="font-w600 mb-0 {{$tc_proyekberjalan}}">5. Proyek Berjalan
                                            <i class="fa {{$icon_proyekberjalan}}"></i>
                                            <p class="pl-20 text-dark">Proses tahap lima, proses pembangunan dan cicilan bagi hasil kepada Pendana (*jika jenis cicilan bulanan) oleh pihak (*Nama Perusahaan)</p>
                                        </p>
                                        <p class="font-w600 mb-0 {{$tc_proyekselesai}}">6. Proyek Selesai
                                            <i class="fa {{$icon_proyekselesai}}"></i>
                                            <p class="pl-20 text-dark">Proses tahap enam, proses pembangunan selesai dan pelunasan pembiayaan oleh pihak (*Nama Perusahaan)</p>
                                        </p>
                                    </div>
                                    </div>
                                </div> -->
                                <!-- DISPLAY PROGRESS UNTUK MOBILE -->
                            </div>
                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w400 d-none d-lg-block">Progress dan Status Pendanaan Proyek</h3>
                                <div class="block d-none d-lg-block">
                                    <div class="row pt-30 mt-10 bs-wizard" style="border-bottom:0;">
                                    <!-- step 1 -->
                                        <div class="bs-wizard-step complete d-none d-lg-block"><!-- complete -->
                                            <div class="progress" style="">
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                        <div class="col-2 bs-wizard-step {{$pengajuan}} d-none d-lg-block">
                                            <div class="text-center bs-wizard-stepnum">1</div>
                                            <p class="text-center text-dark">Pengajuan Pendanaan</p>
                                                <div class="progress" style="margin-left: 40px; border-radius: 10px 0px 0px 10px;">
                                                    <div class="progress-bar"></div>
                                                </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 2 -->
                                        <div class="col-2 bs-wizard-step {{$verifikasi}} d-none d-lg-block"><!-- complete -->
                                            <div class="text-center bs-wizard-stepnum">2</div>
                                            <p class="text-center text-dark">Verifikasi Berhasil</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 3 -->
                                        <div class="col-2 bs-wizard-step {{$penggalangandana}} d-none d-lg-block"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">3</div>
                                            <p class="text-center text-dark">Penggalangan Dana</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 4 -->
                                        <div class="col-2 bs-wizard-step {{$ttd}} d-none d-lg-block"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">4</div>
                                            <p class="text-center text-dark">Pencairan Dana</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                        </div>
                                        <!-- step 5 -->
                                        <div class="col-2 bs-wizard-step {{$proyekberjalan}} d-none d-lg-block"><!-- disable -->
                                            <div class="text-center bs-wizard-stepnum">5</div>
                                            <p class="text-center text-dark">Proyek Berjalan</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 6 -->
                                        <div class="col-2 bs-wizard-step {{$proyekselesai}} d-none d-lg-block"><!-- disable -->
                                            <div class="text-center bs-wizard-stepnum">6</div>
                                            <p class="text-center text-dark">Proyek Selesai</p>
                                            <div class="progress" style="margin-right: 40px; border-radius: 0px 10px 10px 0px;">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <div class="bs-wizard-step disabled d-none d-lg-block"><!-- complete -->
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                        
                                    </div>  
                                                                      
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w400">Deskripsi Detail</h3>
                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                    <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#wizard-progress2-step1" data-toggle="tab">Detail Pendanaan</a>
                                        </li>
                                        <!-- <li class="nav-item hide">
                                            <a class="nav-link"  href="#wizard-progress2-step2" data-toggle="tab">Unggah Progress</a>
                                        </li> -->
                                        <!-- <li class="nav-item">
                                            <a class="nav-link" href="#wizard-progress2-step3" data-toggle="tab">Pembayaran</a>
                                            <a class="nav-link" href="#testpembayaran" data-toggle="tab">Pembayaran</a>
                                        </li> -->
                                        <li class="nav-item">
                                            <a class="nav-link" href="#wizard-progress2-step4" data-toggle="tab">Riwayat Mutasi</a>
                                        </li>
                                    </ul>
                                    <!-- END Step Tabs -->
                                    <!-- Form -->
                                    <div>
                                        <!-- Steps Content -->
                                        <div class="block-content block-content-full tab-content pl-30 pr-30" style="min-height: 274px;">
                                            <!-- Step 1 -->
                                            <div class="tab-pane active pb-30" id="wizard-progress2-step1" role="tabpanel">
                                                <!-- Pribadi -->
                                                <!-- satuBaris -->
                                                <div class="row justify-content-between">
                                                    <div class="col-6">
                                                         <h3 class="block-title text-dark mb-10 font-w600">Dana Terkumpul
                                                            
                                                        </h3>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <p class="text-dark font-w600 ">{{$persendana}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mt-5 mb-5">
                                                    <div class="col-12 col-md-12 ml-5 ">
                                                        <div class="progress" style="border-radius: 10px; height: 10px">
                                                            <div class="progress-bar" role="progressbar" style="width: {{$persendana}};" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="pb-5">
                                                <div class="layout">
                                                    <h3 class="block-title text-muted mb-10 font-w600">Detail Pendanaan</h3>
                                                    <div class="row ml-30 mt-30">
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Tipe Pendanaan</h6>
                                                            <p class="font-w600 text-dark">{{$pendanaanTipe}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Dana Dibutuhkan</h6>
                                                            <p class="font-w600 text-dark">{{$danadibutuhkan}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Imbal Hasil</h6>
                                                            <p class="font-w600 text-dark">{{$imbalhasil}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Durasi Proyek</h6>
                                                            <p class="font-w600 text-dark">{{$durasiproyek}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Tanggal Mulai Proyek</h6>
                                                            <p class="font-w600 text-dark">{{$tgl_mulai_proyek}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Tanggal Selesai Proyek</h6>
                                                            <p class="font-w600 text-dark">{{$tgl_selesai_proyek}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Lokasi Proyek</h6>
                                                            <p class="font-w600 text-dark">{{$lokasi_proyek}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Jenis Akad</h6>
                                                            <p class="font-w600 text-dark">{{$pendanaanakad}}</p>
                                                        </div>
                                                        <!-- <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Grade Pendanaan</h6>
                                                            <p class="font-size-sm font-w600 text-muted mb-0">
                                                                <i class="fa fa-star mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star-half-full mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star-o mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                            </p>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="layout mt-30">
                                                    <h3 class="block-title text-muted mb-10 font-w600">Detail Deskripsi</h3>
                                                    <div class="row ml-30 mt-30">
                                                        <div class="col-12 col-md-12 pl-10">
                                                            <p class="font-size-md font-w300 text-muted mb-0">
                                                                {!! !empty($deskripsi)?$deskripsi:''!!}
                                                            </p>
                                                            <p class="font-size-sm mt-30 font-w600 text-dark">
                                                               Powered by : Dana Syariah Indonesia | #HijrahFinansial
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Step 1 -->
                                            <div class="tab-pane" id="wizard-progress2-step2" role="tabpanel">
                                                <h4 class="pl-10 text-dark">Unggah Progress</h4>
                                                <!-- satuBaris -->
                                                <div class="form-group row">
                                                    <div class="p-10 pl-30">
                                                        <image class="img-side" src="{{url('')}}/assetsBorrower/media/photos/photo10.jpg">
                                                    </div>
                                                    <label class="col-12">Pilih Gambar Utama</label>
                                                    <div class="col-8">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input js-custom-file-input-enabled" id="example-file-input-custom" name="example-file-input-custom" data-toggle="custom-file-input">
                                                            <label class="custom-file-label" for="example-file-input-custom">Pilih Gambar</label>
                                                        </div>
                                                    </div>
                                                </div> 

                                                <!-- Image Default -->
                                                <h6 class="content-heading text-muted font-w600 mt-0 pt-0" style="font-size: 1em;">Media Gallery</h6>
                                                <div class="row items-push">
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END Image Default -->
                                                
                                                <div class="form-group row mt-20 mb-10">
                                                
                                                    <div class="col-12">
                                                        <h6 class="content-heading text-muted font-w600 mt-0 pt-0" style="font-size: 1em;">Tambah Gallery</h6>
                                                        <!-- Dropzone.js -->
                                                        <form ></form> 
                                                        <form action="/upload-target" class="dropzone" id="drop" >
                                                            <div class="dz-message needsclick">
                                                                <i class="fa fa-picture-o fa-5x text-primary" aria-hidden="true"></i>
                                                                <p class="text-dark">
                                                                    Tarik Gambar ke sini! <br>
                                                                    <small>file type : .jpeg, .jpg, .png</small>
                                                                </p>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div> 
                                            </div>

                                            <div class="tab-pane" id="wizard-progress2-step3" role="tabpanel">
                                                <h4 class="pl-20 text-dark">Pembayaran</h4>
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                                                
                                                <div class="block">
                                                    <div class="block-content">                                                        
                                                        <table class="table table-hover" id="list_tbl_invoice">
                                                            <thead>
                                                                <tr>
                                                                    <th>brw_id</th>
                                                                    <th>proyek_id</th>
                                                                    <th>STATUS</th>
                                                                    <th>JATUH TEMPO</th>
                                                                    <th>DANA POKOK</th>
                                                                    <th>IMBAL HASIL</th>
                                                                    <th>BAYAR</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                                <!-- END Table Sections -->
                                            </div>

                                            <div class="tab-pane" id="testpembayaran" role="tabpanel">
                                            @php 
                                                if($status == 7){
                                                    echo "<p align='center'><button class='btn btn-lg btn-primary' id='btnpembayaran' type='button'>Pembayaran</button>";
                                                }else{
                                                    echo "<p align='center'><button class='btn btn-lg btn-primary' type='button' disabled>Pembayaran</button>";
                                                }  
                                            @endphp
                                                
                                            </div>

                                            <div class="tab-pane" id="wizard-progress2-step4" role="tabpanel">
                                                <h4 class="pl-20 text-dark">Riwayat Mutasi</h4>
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                                                
                                                <div class="block">
                                                    <div class="block-content">                                                        
                                                        <table class="js-table-sections table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">NO</th>
                                                                    <th class="text-center">KETERANGAN</th>
                                                                    <th style="width: 15%;" class="text-center">TANGGAL</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">DEBIT</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">KREDIT</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">SALDO</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="js-table-sections-header">
                                                                <?php $no=1;?>
                                                                @foreach($mutasiProyek as $row)
                                                                <tr>
                                                                    <td class="text-center">{{$no++}}</td>
                                                                    <td class="text-center">
                                                                        {{$row['keterangan']}}
                                                                    </td>
                                                                    <td class="font-w600 text-center">
                                                                        <em class="text-muted">{{date('d F Y',strtotime($row['tgl']))}}</em>
                                                                    </td>
                                                                    <td class="d-none d-sm-table-cell text-right">
                                                                        @if($row['debit'] == '-')
                                                                            Rp. 0
                                                                        @else 
                                                                            Rp. {{number_format($row['debit'])}}
                                                                        @endif
                                                                    </td>
                                                                    <td class="d-none d-sm-table-cell text-right">
                                                                        @if($row['kredit'] == '-')
                                                                            Rp. 0
                                                                        @else 
                                                                            Rp. {{number_format($row['kredit'])}}
                                                                        @endif
                                                                    </td>
                                                                    <td class="d-none d-sm-table-cell text-right">
                                                                        Rp. {{number_format($row['total_sisa'])}}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>                                                            
                                                            
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- END Table Sections -->
                                            </div>
                                        </div>   
                                        
                                    </div>
                                    <!-- END Form -->
                                </div>
                                <!-- END Progress Wizard 2 -->                                

                            </div>
                            <!-- DETAIL JAMINAN -->
                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w400">Data Jaminan</h3>
                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                       
                                                <div class="block">
                                                    <div class="block-content">
                                                        <table class="js-table-sections table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 5%;" class="text-center">No</th>
                                                                    <th style="width: 15%;" class="text-center">Nama Pemilik Jaminan</th>
                                                                    <th style="width: 15%;" class="text-center">Jenis Jaminan</th>
                                                                    <th style="width: 10%;" class="text-center">Nomor Surat</th>
                                                                    <th style="width: 15%;" class="text-center">Kantor Penerbit</th>
                                                                    <th style="width: 15%;" class="text-center">Nomor Objek Pajak</th>
                                                                    <th style="width: 15%;" class="text-center">Nilai Objek Pajak</th>
                                                                    <th style="width: 10%;" class="text-center">Sertifikat</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="js-table-sections-header show table-success">
                                                            @if($dataJaminan == null)
                                                                <tr><td colspan="8" align="center"><i>Tidak ada Jaminan</i></td></tr>
                                                            @else
                                                            @php $a=1; @endphp
                                                                @foreach ($dataJaminan as $row)
                                                                    <tr>
                                                                        <td class="text-center">
                                                                            {{$a++}}
                                                                        </td>
                                                                        <td class="text-center" >
                                                                            {{ $row->jaminan_nama }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            {{ $row->jenis_jaminan }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            {{ $row->jaminan_nomor }}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            {{ $row->kantor_penerbit }}
                                                                        </td>
                                                                        <td>
                                                                            <span class="font-w700 text-primary">{{$row->NOP}} </span>
                                                                        </td>
                                                                        <td class="d-none d-sm-table-cell">Rp. {{number_format($row->jaminan_nilai,0,'','.')}}
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <input type="hidden" name="sertifikat" value='{{ $row->sertifikat }}'>
                                                                            <button id='sertifikat' class='btn btn-primary'>Lihat</button>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            </tbody> 

                                                        </table>
                                                    </div>
                                                </div>

                                </div>
                            </div> 
                            <!-- END DETAIL JAMINAN -->
                            <!-- listpendana -->
                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w400">List Pendana</h3>
                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                       
                                                <div class="block">
                                                    <div class="block-content">
                                                        <table class="js-table-sections table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 10%;" class="text-center">No</th>
                                                                    <th hidden="hidden">ID Proyek</th>
                                                                    <th hidden="hidden">ID Borrower</th>
                                                                    <th hidden="hidden">ID Investor</th>
                                                                    <th style="width: 50%;" class="text-center">Nama Pendana</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">Nominal Pendanan</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="js-table-sections-header show table-success">
                                                            @if($dataPendana == null)
                                                                <tr><td colspan="3" align="center"><i>Belum ada pendana</i></td></tr>
                                                            @else
                                                                @foreach ($dataPendana as $row)
                                                                    <tr>
                                                                        <td class="text-center">
                                                                            <i class="fa fa-angle-right"></i>
                                                                        </td>
                                                                        <td class="text-center" hidden="hidden">
                                                                            {{ $row->proyek_id }}
                                                                        </td>
                                                                        <td class="text-center" hidden="hidden">
                                                                            {{ $row->investor_id }}
                                                                        </td>
                                                                        <td class="text-center" hidden="hidden">
                                                                            {{ $brwId }}
                                                                        </td>
                                                                        <td>
                                                                            <span class="font-w700 text-primary">{{$row->nama_investor}} </span>
                                                                        </td>
                                                                        <td class="d-none d-sm-table-cell">Rp. {{number_format($row->total_dana,0,'','.')}}
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @endif
                                                            </tbody> 

                                                        </table>
                                                    </div>
                                                </div>

                                </div>
                            </div> 
                            <!-- endlistpendana -->
                            <br><br>
                        </div>
                    </div>                           
                </div>
            </div>
        </div>
        <!-- END Page Content -->
        @include('includes.borrower.modal_invoice')
		<div id="modalTermCondition"  class="modal fade show" role="dialog">
			<div class="modal-dialog modal-lg modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="scrollmodalLabel">Syarat & Ketentuan</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form id="agree">
					@csrf
						<div class="modal-body">
							{{-- <textarea rows="10" cols="200" readonly> --}}

								<div style="padding:20px" style="overflow:scroll;">
									<p style="text-align: justify;">Anda akan menggunakan tanda tangan elektronik untuk menandatanganani dokumen elektronik dengan PT. Dana Syariah Indonesia</p>
									<p style="text-align: justify;"><strong>PT. Dana Syariah Indonesia</strong> bekerjasama dengan <strong>PT Privy Identitas Digital</strong> selaku Penyelenggara Tanda Tangan Elektronik dan Penyelenggara Sertifikasi Elektronik Indonesia yang mendapatkan pengakuan tersertifikasi pada Kementerian Komunikasi dan Informatika Republik Indonesia dengan merk PrivyID. </p>
									<p style="text-align: justify;">Anda menyatakan setuju untuk mendaftar sebagai pengguna PrivyID untuk dibuatkan data pembuatan tanda tangan elektronik dan diterbitkan sertifikat elektronik oleh <strong>PT Privy Identitas Digital</strong> dalam rangka penggunaan layanan Tanda Tangan Elektronik untuk menandatangani dokumen elektronik.</p>
									<p style="text-align: justify;">Anda memberi kuasa kepada <strong>PT. Dana Syariah Indonesia</strong> untuk meneruskan data KTP, swafoto, nomor ponsel dan alamat surel Anda sebagai data pendaftaran kepada <strong>PT Privy Identitas Digital</strong> guna memenuhi ketentuan Peraturan Perundang-undangan, yaitu Peraturan Pemerintah Nomor 71 Tahun 2019 tentang Penyelenggara Sistem dan Transaksi Elektronik, dan <strong>Peraturan Kementerian Informasi dan Komunikasi Nomor 11 Tahun 2018</strong>.</p>
									<p style="text-align: justify;">Dengan ini anda juga menyatakan setuju untuk terikat pada syarat dan ketentuan layanan PrivyID yang terdapat pada tautan berikut: Syarat dan Ketentuan PrivyID</p>
								</div>

							{{-- </textarea> --}}
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success" onclick="location.href=''">Batal</button>
							<button type="button" class="btn btn-success" id="setujuTermCondition">Saya Setuju</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
        {{-- Modal Aktivasi --}}
        <div class="modal fade" id="modalAktivasi" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Aktivasi DigiSign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modalBodyAktivasi">
                    
                </div>
                {{-- <div class="modal-footer">
                
                </div> --}}
            </div>
            </div>
        </div>
        {{-- Modal Aktivasi End --}}

        {{-- Modal TTD --}}
        <div class="modal fade" id="modalTTD" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>TTD DigiSign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modalBodyTTD">
                    
                </div>
                {{-- <div class="modal-footer">
                
                </div> --}}
            </div>
            </div>
        </div>
        {{-- Modal TTD End --}}

        {{-- Modal SP3 --}}
        <div id="modalSP3"  class="modal fade in" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">SP3</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="agree">
                    @csrf
                        <div class="modal-body" id="modalBodySP3">
                            {{-- <iframe src="{{ url('perjanjian') }}" scrolling="yes" width="100%" height="500" id="iprem"></iframe> --}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="setujuSP3">Saya Setuju</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal SP3 End --}}
		{{-- Modal Download Document --}}
        <div id="modalDownloadDocument"  class="modal fade in" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Unduh Document Wakalah</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="agree">
                    
                        <div class="modal-body" id="modalBodyDocument">
                            
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal SP3 End --}}
    </main>
   
    <!-- END Main Container -->
    <!--script>
    function fct() {
        var brw_id = "{{Auth::guard('borrower')->user()->brw_id}}";
        $.ajax({
            url : "/admin/borrower/regDigiSignborrower/"+brw_id,
            method : "get",
            success:function(data)
            {
                var dataJSON = JSON.parse(data.status_all);
                console.log(dataJSON);
                swal.fire({title:"Notifikasi",text:dataJSON.JSONFile.notif,type:"info"})
                .then((result) => {
                        if (dataJSON.JSONFile.result == '00')
                        {
                            if (dataJSON.JSONFile.info)
                            {
                                var url_notif = dataJSON.JSONFile.info.split('https://')[1];
                                console.log(url_notif);
                                $.ajax({
                                    url : "/admin/borrower/callbackDigiSignBorrower/",
                                    method : "post",
                                    data : {brw_id:brw_id,provider_id:1,status:dataJSON.JSONFile.notif,step:'register',url:url_notif},
                                    success:function(data)
                                    {
                                        console.log(data.status)
                                    },
                                    error: function(xhr, status, error){
                                        var errorMessage = xhr.status + ': ' + xhr.statusText
                                        console.log('Error - ' + errorMessage);
                                    }
                                });
                                window.open(dataJSON.JSONFile.info,'_blank');
                            }
                        }
                });
                
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });
    }
    </script-->
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />
    <!--<script src="/js/jquery-3.3.1.min.js"></script> -->
    <script src="{{asset('js/sweetalert.js')}}"></script>
    <!--<script src="/admin/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="/admin/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="/admin/assets/js/lib/data-table/dataTables.buttons.min.js"></script>-->
    <style>
        .modal-dialog{
        min-width: 80%;
        }

        .btn-cancel {
            background-color: #C0392B;
            color: #FFFF;
        }
    </style>
    <script>
    
		$('#kontrak').on('click',function(){
			var brw_id = {{  Auth::guard('borrower')->user()->brw_id }};
			id_proyek = {{ $id }};
			var text = "{{ $teks }}";
			Swal.fire({
                title: "Informasi",   
                text: text,   
                type: "info",   
                showCancelButton: true,
                cancelButtonClass: 'btn-cancel',
                confirmButtonText: "Setuju",   
                cancelButtonText: "Batal",   
                closeOnConfirm: false,   
                closeOnCancel: true
            }).then((result) => {
					
                    if (result.value) 
                    {
                        // check registrasi Privy ID
						$.ajax({
                            url:"/user/checkRegistrasi/"+brw_id+"/2",
                            method:'get',
                            dataType:'json',
                            beforeSend: function() {
                                $("#overlay").css('display','block');
                                //swal.fire.close()
                            },
                            success:function(responseCekRegister)
                            {
                                
								console.log(responseCekRegister);
								if(responseCekRegister.status == "belum_terdaftar"){ // jika belum terdaftar
								
									$("#modalTermCondition").modal("show").addClass('modal fade show in').attr('style','display:block');

									$("#setujuTermCondition").on("click", function(){ // jika klik button setuju Term & Condition Privy
										$("#modalTermCondition .close").click();
						
											$.ajax({
												url:'/user/RegisterPrivyID/'+brw_id+'/2', // 1 sama dengan investor, 2 sama dengan borrower
												method:'get',
												dataType:'json',
												beforeSend: function() {
													$("#overlay").css('display','block');
												},
												success:function(responseRegistrasi)
												{
													console.log(responseRegistrasi);
													$('#overlay').css('display','none');
													var code = responseRegistrasi.code;
													if(code == 422){ // status error
														response_error_privy(responseRegistrasi);
													}
													else if(code == 201 && responseRegistrasi.data.status == "waiting"){ // jika status registrasi berhasil namun masih menunggu
														Swal.fire({title:"Pending",text:responseRegistrasi.message,type:"info"}).then(function(){
																	//
														});
														$('#overlay').css('display','none')
													}
													else if(code == 201 && responseRegistrasi.data.status == "rejected"){ // jika status registrasi berhasil namun masih direjected
														let text_info = 'Silahkan ubah data anda terlebih dahulu dan lakukan pendanaan lagi.';;
														Swal.fire({title:"Data Ditolak",text:responseRegistrasi.data.reject.reason+'. '+text_info,type:"error"}).then(function(){
																	//
														});
														$('#overlay').css('display','none');
													}
													else if(code == 201 &&  responseRegistrasi.data.status == "invalid"){ // jika status registrasi berhasil namun masih direjected
														let text_email = 'Silahkan cek email anda untuk upload file pendukung(KTP/KK/Pasport).';														
														Swal.fire({title:"Data Tidak Valid",text:responseRegistrasi.data.reject.reason+'. '+text_email,type:"error"}).then(function(){
																	//
														});
														$('#overlay').css('display','none');
													}
													else if(code == 201 && responseRegistrasi.data.status == "verified" || responseRegistrasi.data.status == "registered"){ // jika status user terverikasi maka akan generate 
														
														Swal.fire({title:"Sukes",text:responseRegistrasi.data.status,type:"success"}).then(function(){
																	//
														});
														$('#overlay').css('display','none');
														
														
													} // end IF verified
												}
											});
									});
									
								}else{ // jika sudah terdaftar
									
									// kirim document 
									$.ajax({
									  url : "/borrower/sendDigiSignWakalahBorrower/"+brw_id+"/"+id_proyek,
									  method : "get",
									  beforeSend: function() {
										  $("#overlay").css('display','block');
									  },
									  success:function(data)
									  {
										  $("#overlay").css('display','none');
										  var dataJSON = JSON.parse(data.status_all);
										  if (dataJSON.JSONFile.result == '00')
										  {
											  $.ajax({
												  url:'/borrower/signDigiSignWakalahBorrower/'+brw_id+"/"+id_proyek,
												  method:'get',
												  dataType:'json',
												  beforeSend: function() {
													  $("#overlay").css('display','block');
													  swal.close()
												  },
												  success:function(data)
												  {
													  $("#overlay").css('display','none');
													  var dataJSON = JSON.parse(data.status_all);
													  console.log(dataJSON);
													  if (dataJSON.JSONFile.result == '00')
													  {
														  $('#modalTTD').modal('show').addClass('modal fade show in').attr('style','display:block')
														  $('.modal-backdrop').addClass('modal-backdrop fade show in')
														  $('#modalBodyTTD').append('<iframe id="linkTTD" width="100%" height="750"></iframe>');
														  $('#linkTTD').attr('src',dataJSON.JSONFile.link)
														  $("#modalTTD").appendTo('body');
														  $('#linkTTD').load(function(){
															  var myFrame = $("#linkTTD").contents().find('body').text();
															  console.log(myFrame)
															  if (myFrame !== '')
															  {
																  $('#modalTTD').modal('show').addClass('modal fade').attr('style','display:none')
																  $('.modal-backdrop').remove()
																  location.reload(true);
															  }
														  })
													  }
													  else
													  {
														  Swal.fire({title:"Notifikasi",text:'TTD Gagal',type:"info"})
															.then(function(){
																		
															});
													  }
												  },
												  error: function(request, status, error)
												  {
													  $("#overlay").css('display','none');
													  alert(status)
												  }
											  })
										  }
										  else
										  {
											  Swal.fire({title:"Notifikasi",text:dataJSON.JSONFile.notif,type:"info"})
												.then(function(){
													 location.reload(true)
												 });
										  }
										  
									  },
									  error: function(request, status, error)
									  {
										  $("#overlay").css('display','none');
										  alert(status)
									  } 
								  });  
								}
                                
                            },
                            error: function(request, status, error)
                            {
                                $("#overlay").css('display','none');
                                alert(status)
                            }
                        })  
                    }
			});
    });

    $('#kontrak_ttd_awal').on('click',function(){
        var brw_id = {{  Auth::guard('borrower')->user()->brw_id }}
        id_proyek = {{ $id }};
        $.ajax({
			url : "/borrower/sendDigiSignWakalahBorrower/"+brw_id+"/"+id_proyek,
			method : "get",
			beforeSend: function() {
				$("#overlay").css('display','block');
			},
			success:function(responseSendDocPrivy)
			{
				console.log(responseSendDocPrivy);
				var responseSend = JSON.parse(responseSendDocPrivy);
				$("#overlay").css('display','none');
				if (responseSend.code == '201')
				{
					$('#modalTTD').modal('show').addClass('modal fade show in').attr('style','display:block')
					$('.modal-backdrop').addClass('modal-backdrop fade show in')
					$('#modalBodyTTD').append('<iframe id="linkTTD" width="100%" height="750"></iframe>');
					$('#linkTTD').attr('src',"/user/viewTTD_wakalah_borrower/"+responseSend.data.recipients[1].privyId+"/450/600/2/"+responseSend.data.docToken);
				}
				else
				{
					Swal.fire({title:"Notifikasi",text:responseSend.message,type:"info"})
                    .then(function(){
                         location.reload(true)
                     });
				}
              
			},
			error: function(request, status, error)
			{
				$("#overlay").css('display','none');
				alert(status)
			} 
      });  
              
    });

    $('#kontrak_ttd_akhir').on('click',function(){
		
        var brw_id = {{  Auth::guard('borrower')->user()->brw_id }}
        id_proyek = {{ $id }};
		
        $.ajax({
			url:'/borrower/signDigiSignWakalahBorrower/'+brw_id+"/"+id_proyek,
			method:'get',
			dataType:'json',
			beforeSend: function() {
				$("#overlay").css('display','block');
				swal.close()
			},
			success:function(responseGetSign)
			{
				$("#overlay").css('display','none');
			
				console.log(responseGetSign);
				if(responseGetSign.status == "ada"){ // jika document 
					$('#modalTTD').modal('show').addClass('modal fade show in').attr('style','display:block')
					$('.modal-backdrop').addClass('modal-backdrop fade show in')
					$('#modalBodyTTD').append('<iframe id="linkTTD" width="100%" height="750"></iframe>');
					$('#linkTTD').attr('src',"/user/viewTTD_wakalah_borrower/"+responseGetSign.privyID+"/450/600/2/"+responseGetSign.docToken);
				}
				else{
					
					Swal.fire({title:"Notifikasi",text:'Document Tidak Ditemukan',type:"info"}).then(function(){
								
					});
					
				}
                
			  
			  
			},
			error: function(request, status, error)
			{
				$("#overlay").css('display','none');
				alert(status)
			}
		});       
    });

    $('#kontrak_unduh').on('click',function(){
		var brw_id = {{  Auth::guard('borrower')->user()->brw_id }}
        var id_proyek = {{ $id }}
		
		
        $.ajax({
          url:'/borrower/downloadWakalahBorrower/'+brw_id+'/'+id_proyek,
          method:'get',
          // data:{id_brw:brw_id,proyek_id:id_proyek},
          beforeSend: function(jqXHR,settings) {
              $("#overlay").css('display','block');
          },
          success: function (response) {
			 
            $('#overlay').css('display','none');
			$('#modalDownloadDocument').modal('show').addClass('modal fade show in').attr('style','display:block')
			$('.modal-backdrop').addClass('modal-backdrop fade show in')
			$('#modalBodyDocument').append('<iframe id="linkTTD" width="100%" height="750"></iframe>');
			$('#linkTTD').attr('src', response.downloadURL);
             
          },
          error: function(request, status, error)
          {
              $("#overlay").css('display','none');
              alert(status)
          }
      })  
              
    });

  	
	// generate SP3
    $('#sp3').on('click',function(){
        var id_proyek = {{ $id }};
        var brw_id = {{ $brwId }};
        $.ajax({
          url:'/borrower/generateSP3/'+brw_id+'/'+id_proyek,
          method:'get',
          success: function (response) {
                if (response.status == 'Berhasil')
                {
                    
                    $('#modalSP3').modal('show');
                    $('#modalBodySP3').append('<iframe id="linkSP3" scrolling="yes" width="100%" height="500" id="iprem"></iframe>');
                    $('#linkSP3').attr('src',"{{ url('viewSP3') }}/"+brw_id)
                }
                else
                {
                    alert(response.status)
                }

          },
          error: function(request, status, error)
          {
              alert(status)
          }
        });
    })

    
	
	// proses setuju SP3
    $('#setujuSP3').on('click',function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var id_proyek = {{ $id }};
        $.ajax({
              url:'/borrower/updateSP3/',
              method:'post',
              data: {'proyek_id': id_proyek},
              success: function (response) {
                    // console.log(response.jsonFile.message)
                    if (response.jsonFile.status == '00')
                    {
                        Swal.fire({title:"Notifikasi",text:response.jsonFile.message,type:"info"})
                            .then(function(){
                                 location.reload(true)
                            })
                    }
                    else
                    {
                        Swal.fire({title:"Notifikasi",text:response.jsonFile.message,type:"info"})
                            .then(function(){
                                 location.reload(true)
                            })
                    }

              },
              error: function(request, status, error)
              {
                  alert(status)
              }
        });
    })

    $('#modalAktivasi .close,#modalTTD .close,#modalSP3 .close').on('click',function(){
          location.reload(true);
          console.log('test')
    })

    $('.akad-murobahah').on('click',function(){
          $('#kontrak').trigger('click');
    });
	
	function response_error_privy(response){
		var error_message = '';
		var temp_message = '';
		
		for(let i=0; i<response.errors.length; i++){
			if(response.errors[i].field=='identity.nama'){
				temp_message = (`${i+1}. Nama terlalu pendek (minimal 3 karakter), `);
			}else if(response.errors[i].field=='selfie'){
				if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid'){
					temp_message = (`${i+1}. Foto KTP dan Selfie tidak boleh kosong, `);
				}else{
					temp_message = (`${i+1}. Foto KTP/Selfie harus format JPG/PNG, `);
				}
			}else if(response.errors[i].field=='email'){
				if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid'){
					temp_message = (`${i+1}. Email tidak boleh kosong/Format Email Salah, `);
				}else{
					temp_message = (`${i+1}. Email anda sudah terdaftar, `);
				}
			}else if(response.errors[i].field=='identity.nik'){
				if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid'){
					temp_message = (`${i+1}. NIK tidak boleh kosong, `);
				}else{
					temp_message = (`${i+1}. Format Nomor NIK harus 16 digit, `);
				}
			}else{
				if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid' || response.errors[i].messages[0]=='invalid phone number'){
					temp_message = (`${i+1}. Nomor HP tidak boleh kosong/Format Nomor Hp anda salah, `);
				}else{
					temp_message = (`${i+1}. Nomor HP anda sudah terdaftar, `);
				}
			}
			error_message += temp_message;
		}
		Swal.fire({title:"Notifikasi",text:error_message,type:"info"}).then(function(){
					//
		});
		$('#overlay').css('display','none');
	};

    function btn_send_digital_sign(proyek_id,investor_id,user_id){
      $.ajax({
          url:'/borrower/sendDigiSignMurobahahBorrower/'+proyek_id+'/'+investor_id,
          method:'get',
          dataType:'json',
          beforeSend: function() {
              $("#overlay").css('display','block');
              swal.close()
          },
          success:function(data)
          {
              $("#overlay").css('display','none');
              var dataJSON = JSON.parse(data.status_all);
              console.log(dataJSON);
              if (dataJSON.JSONFile.result == '00')
              {
                  $.ajax({
                      url:'/borrower/signDigiSignMurobahahBorrower/'+user_id+'/'+investor_id+'/'+proyek_id,
                      method:'get',
                      dataType:'json',
                      beforeSend: function() {
                          $("#overlay").css('display','block');
                          swal.close()
                      },
                      success:function(data)
                      {
                          $("#overlay").css('display','none');
                          var dataJSON = JSON.parse(data.status_all);
                          console.log(dataJSON);
                          if (dataJSON.JSONFile.result == '00')
                          {
                              $('#modalTTD').modal('show').addClass('modal fade show in').attr('style','display:block')
                              $('.modal-backdrop').addClass('modal-backdrop fade show in')
                              $('#modalBodyTTD').append('<iframe id="linkTTD" width="100%" height="750"></iframe>');
                              $('#linkTTD').attr('src',dataJSON.JSONFile.link)
                              $("#modalTTD").appendTo('body');
                              $('#linkTTD').load(function(){
                                  var myFrame = $("#linkTTD").contents().find('body').text();
                                  console.log(myFrame)
                                  if (myFrame !== '')
                                  {
                                      $('#modalTTD').modal('show').addClass('modal fade').attr('style','display:none')
                                      $('.modal-backdrop').remove()
                                      location.reload(true);
                                  }
                              })
                          }
                          else
                          {
                              Swal.fire({title:"Notifikasi",text:'TTD Gagal',type:"info"})
                                .then(function(){
                                     swal.close()
                                })
                          }
                      },
                      error: function(request, status, error)
                      {
                          $("#overlay").css('display','none');
                          alert(status)
                      }
                  })
              }
              else
              {
                  Swal.fire({title:"Notifikasi",text:'TTD Gagal',type:"info"})
                    .then(function(){
                         swal.close()
                    })
              }
          },
          error: function(request, status, error)
          {
              $("#overlay").css('display','none');
              alert(status)
          }
      })
    }

    function btn_sign_digital_sign(user_id, investor_id, proyek_id){
        
		$.ajax({
			url:'/borrower/signDigiSignMurobahahBorrower/'+user_id+'/'+investor_id+'/'+proyek_id,
			method:'get',
			dataType:'json',
			beforeSend: function() {
				$("#overlay").css('display','block');
				swal.close()
			},
			success:function(responseSign)
			{
				$("#overlay").css('display','none');
				console.log(responseSign);
             
				if(responseSign.status == "ada"){ // jika document 
					$('#modalTTD').modal('show').addClass('modal fade show in').attr('style','display:block')
					$('.modal-backdrop').addClass('modal-backdrop fade show in')
					$('#modalBodyTTD').append('<iframe id="linkTTD" width="100%" height="750"></iframe>');
					$('#linkTTD').attr('src',"/user/viewTTD_wakalah_borrower/"+responseSign.privyID+"/150/550/13/"+responseSign.docToken);
				}
				else{
					
					Swal.fire({title:"Notifikasi",text:'Document Tidak Ditemukan',type:"info"}).then(function(){
								
					});
					
				}
			},
			error: function(request, status, error)
			{
				$("#overlay").css('display','none');
				alert(status)
			}
		})
    };
	

    function btn_download_murobahah(proyek_id,investor_id){
		      
		$.ajax({
			  url:'/borrower/downloadMurabahahBorrower/'+proyek_id+'/'+investor_id,
			  method:'get',
			  // data:{proyek_id:proyek_id,investor_id:investor_id},
			  beforeSend: function(jqXHR,settings) {
				  $("#overlay").css('display','block');
			  },
			  success: function (response) {
				$('#overlay').css('display','none');
				$('#modalDownloadDocument').modal('show').addClass('modal fade show in').attr('style','display:block')
				$('.modal-backdrop').addClass('modal-backdrop fade show in')
				$('#modalBodyDocument').append('<iframe id="linkTTD" width="100%" height="750"></iframe>');
				$('#linkTTD').attr('src', response.downloadURL);
				  
			  },
			  error: function(request, status, error)
			  {
				  $("#overlay").css('display','none');
				  alert(status)
			  }
		  })
    }

    </script>
    <script>
        $(document).ready(function() {   
            var brw_id      = "{{Auth::guard('borrower')->user()->brw_id}}";
            var proyek_id   = "{{$id}}";
            var list_tbl_invoice = $('#list_tbl_invoice').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                bFilter: false, 
                bInfo: false,
                // ajax : {
                  // url : '/borrower/list_invoice'+brw_id+'/'+proyek_id,
                  // type : 'get',
                // },
                
                "columnDefs" :[
                    {
                        "targets": 0,
                        class : 'text-left',
                        "visible" : false,
                    },
                    {
                        "targets": 1,
                        class : 'text-left',
                        "visible" : false,
                    },
                    {
                        "targets": 2,
                        class : 'text-left',
                        // "visible" : false,
                    },
                    {
                        "targets": 3,
                        class : 'text-left',
                        // "visible" : false,
                    },
                    {
                        "targets": 4,
                        class : 'text-left',
                        // "visible" : false,
                    },
                    {
                        "targets": 5,
                        class : 'text-left',
                        // "visible" : false,
                    },
                    {
                        "targets": 6,
                        class : 'text-left',
                        "visible" : false,
                    },
                    {
                        "targets": 7,
                        class : 'text-left'
                        //"visible" : false
                        
                    }
                ]
             });
            $("#radio_normal, #radio_percepatan").change(function () {
                if ($("#radio_normal").is(":checked")) {
                    $('#tr_percepatan').hide();
                }
                else if ($("#radio_percepatan").is(":checked")) {
                    $('#tr_percepatan').show();
                }
                else 
                    $('#tr_percepatan').hide();
            }); 
            $("#radio_lunas, #radio_sebagian").change(function () {
                if ($("#radio_lunas").is(":checked")) {
                    $('#input_mode').attr("disabled", "disabled");
                }
                else if ($("#radio_sebagian").is(":checked")) {
                    $('#input_mode').removeAttr("disabled", "disabled");
                    $('#input_mode').focus();
                }
                else 
                $('#input_mode').attr("disabled", "disabled");
            });        
        });

        $('#btnpembayaran').on('click',function(){
            Swal.fire({
            title: 'Lunasi Pembayaran Proyek?',
            //text: "Rp. "+<?php echo $danadicairkan?>,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Batalkan !',
            confirmButtonText: 'Ya, Lunasi !'
            }).then((result) => {
            // if (result.value) {
            //     Swal.fire(
            //     'Deleted!',
            //     'Your file has been deleted.',
            //     'success'
            //     )
            // }
            })
        });
    </script>
@endsection