@extends('layouts.borrower.master')

@section('title', 'Pembayaran Cicilan')
<style>
    .bg-gradient-hijau{
    background: rgb(47,122,21);
    background: -moz-linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    background: linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#2f7a15",endColorstr="#067757",GradientType=1);
  }
  .bg-gradient-hijau-2{
    background: rgb(21,122,89);
    background: -moz-linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    background: linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#157a59",endColorstr="#065f77",GradientType=1);
  }
  .bg-gradient-blue{
    background: rgb(21,85,122);
    background: -moz-linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    background: linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#15557a",endColorstr="#063777",GradientType=1);
  }
}
</style>

@section('content')
    <!-- Main Container -->
    @if(Auth::guard('borrower')->user()->status == 'pending')
    <main id="main-container">

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h1 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;" >Silahkan Tunggu Verifikasi dari Danasyariah</h1>                    
                    </span>
                </div>
            </div>
			<div class="row">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h4 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;" >Terimakasih telah bergabung untuk maju bersama kami</h4>                    
                    </span>
					
					
                </div>
            </div>
            
            <!-- END Frequently Asked Questions -->
        </div>
        <!-- END Page Content -->

    </main>
    @elseif(Auth::guard('borrower')->user()->status == 'active') 
    <main id="main-container">
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">
                <div class="row mt-5 pt-5">
                    <div id="col2" class="col-md-12 mt-5 pt-5">
                    <span class="mb-10 pb-10 ">
                        <h3 class="no-paddingTop font-w300 judul" style="float: left; " >Daftar Periode Tagihan</h3>
                    </span>
                    <br><br>
                    <table class="table border" id="table_akad">
                        <thead class="bg-dark text-light">
                            <th class="text-center" >No</th>
                            <th hidden>idpencairan</th>
                            <th hidden>number</th>
                            <th class="text-center" >Pendanaan</th>
                            <th class="text-center" >Tenor</th>
                            <th class="text-center" >Jumlah Dana</th>
                            <th class="text-center" >Imbal Hasil</th>
                            <th class="text-center" >Aksi</th>
                        </thead>
                        <tbody>
                               
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main> 
    @endif
    <!-- END Main Container -->
    @php $date = date('Y-d'); @endphp
    <style>
        @media (min-width: 1800px)
        .modal-xl {
            max-width: 1800px;
        }   
    </style>
    <div class="modal fade" id="payout_detil" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="payout_detil" aria-hidden="true">
        <div class="modal-dialog modal-xl" >
          <div class="modal-content modal-xl" >
            <input type="hidden" id="id_val_pay">
              <div class="modal-header">
              <h3>List Tagihan</h3>
                <button type="button" class="close" data-dismiss="modal" id='close' aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body" style="overflow: auto;">
                <table class="table" id="tabelModal">
                <thead class="bg-light text-dark">
                    <tr>
                      <td scope="col" class="text-center">No</td>
                      <td scope="col" class="text-center">No Invoice</td>
                      <td scope="col" class="text-center">Tgl Jatuh Tempo</td>
                      <td scope="col" class="text-center">Periode</td>
                      <td scope="col" class="text-center">Jumlah Tagihan</td>
                      <td scope="col" class="text-center">Tanggal Pembayaran</td>
                      <td scope="col" class="text-center">Nominal Dibayar</td>
                      <td scope="col" class="text-center">Nomor Referal</td>
                      <td scope="col" class="text-center">Telat Bayar</td>
					  <!-- <td scope="col" class="text-center">Status</td> -->
                      <td scope="col" class="text-center">Keterangan</td>
                      <td scope="col" class="text-center">Bayar Cicilan</td>
                    </tr>
                  </thead>
                <tbody id="urutan"></tbody>
                </table>
            </div>
          </div>
        </div>
    </div>

    {{-- Modal invoice --}}
        <div id="modalInvoice"  class="modal fade in" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Invoice</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="agree">
                    @csrf
                        <div class="modal-body" id="modalBodyInvoice">
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <button type="button" class="btn btn-info" id='dinvoice'>Download Invoice</button>
                            <button type="button" class="btn btn-success" id="bayar">Upload Bukti Pembayaran</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {{-- Modal invoice End --}}

     {{-- Modal buktitransfer --}}
        <div id="modalBt"  class="modal fade in" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Bukti Pembayaran Cicilan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="agree">
                    @csrf
                        <div class="modal-body" id="modalBodyBt">
                            <div class="col-lg-12">
                                <div class="card-body card-block">
                                    <div class="form-row">
                                      <div class="col-lg-12">
                                        <div class="row" id="tambahUpload">
                                            <div class="row">
                                                <div id="carousel_image_pembayaran_lihat_tolak" class="carousel slide" data-interval="false">
                                                    {{-- <ol class="carousel-indicators">
                                                    </ol> --}}
                                                    <div id="gambarBT" class="carousel-inner">
                                                    </div>
                                                    <a class="carousel-control-prev" href="#carousel_image_pembayaran_lihat_tolak" role="button" data-slide="prev">
                                                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                      <span class="sr-only">Sebelumnya</span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#carousel_image_pembayaran_lihat_tolak" role="button" data-slide="next">
                                                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                      <span class="sr-only">Selanjutnya</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn-lg" data-dismiss="modal" aria-label="Close">OK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {{-- Modal buktitransfer End --}}

    {{-- Modal notif bayar --}}
        <div id="modalbayar"  class="modal fade in" role="dialog" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white " >
                        <h5 class="modal-title" id="scrollmodalLabel">Bayar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="agree" method="post" enctype="multipart/form-data">
                    @csrf
                        <div class="modal-body" id="modalBodyInvoice">
                    		<h5 align='center'>Upload Bukti Pembayaran</h5>

	                    	<div id='sukses'></div>
                            <div id='norefeal'></div>
	                            <p align='center'>
	                            	<button type="button" class="btn btn-info" id="btnBT" onclick="document.getElementById('uploadBT').click();">Upload Bukti Transfer</button></p><p align="center">
	                            	<!-- <input type="file" style="display: none;" id="file_name" name="file_name"> -->
                                    <input type="file" style="display: none;" name="file_name" accept=".png, .jpg, .jpeg" id="uploadBT">

	                            	<!-- <button type="button" class="btn btn-success" id="uploadBT">Upload Bukti Transfer</button> -->
	                            </p>
	                    	<div class="modal-footer ">
                            	<button type="button" class="btn btn-success btn-block" id="kirimBT" disabled>Kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    {{-- Modal notif bayar --}}

    <script type="text/javascript">
    $(document).ready( function(){
        var a = {{$idbrw}};
        var akadTable = $('#table_akad').DataTable({
            ajax: {
                url: '/borrower/list_pembayaran_cicilan/'+a,
                method:'get',
            },
            columns: [
                { data : null,
                  render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  meta.row+1;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  row.pencairan_id;
                    }
                },
                { data: 'number',
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  "<input type='hidden' value='2' id='number_id'>";
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  row.pendanaan_nama;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  row.durasi_proyek+' bulan';
                    }
                },
                { data: null,
                    render:function(data,type,row,meta)
                    {
                        if (row.pendanaan_dana_dibutuhkan == null)
                        {
                            return 'Rp. 0';
                        }
                        else
                        {
                            return 'Rp.'+ parseInt(row.pendanaan_dana_dibutuhkan).toLocaleString();
                        }
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  parseInt(row.estimasi_imbal_hasil) +' %';
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  '<button class="btn sm btn-primary lihat_tagihan" data-toggle="modal"  data-target="#payout_detil" >Lihat Tagihan</button>';
                          // data-target="#payout_detil"
                    }
                }
            ]
            ,
            columnDefs: [
                { targets: [1,2], visible: false}
            ]
        });
        //var tbakad = $('#table_akad').DataTable();
        $('#table_akad tbody').on( 'click', 'tr', function () {
            console.log( akadTable.row( this ).data() );
            // alert(pencairanid);
            var data = akadTable.row( this ).data();
            var pencairanid = data['pencairan_id'];
            // alert(pencairanid);
            $("#urutan").html(" ");

                    $.ajax({
                        url: "/borrower/list_tagihan/"+pencairanid,
                        method: "get",
                        success:function(data)
                        {
                            var xx=1;
                            console.log(data);
                            list_tagihan = data.data;
                            const monthNames = ["Januari", "Februari", "Maret", "April", "Mai", "Juni","Juli", "Agustus", "September", "Oktober", "November", "Desember"];
                            $.each(list_tagihan,function(index,value){

                                // $("#urutan").append("<hr>");
                                $("#urutan").append("<tr>");
                                var mdate = value.tgl_jatuh_tempo;
                                var ndate = mdate.split("-");
                                var pdate = ndate[2].substr(0,2);
                                var tdate = ndate[0]+"-"+ndate[1]+"-"+pdate; 
                                let current_datetime = new Date(tdate);
                                // let formatted_date = current_datetime.getDate() + " " + monthNames[(current_datetime.getMonth())] + " " + current_datetime.getFullYear();
                                let tglJT = current_datetime.getDate() + "/" + (current_datetime.getMonth()+1) + "/" + current_datetime.getFullYear();
                                $("#urutan").append("<td class='text-right'>"+(xx++)+"</td>");
                                $("#urutan").append("<td class='text-right'>"+value.no_invoice+"</td>");
                                $("#urutan").append("<td class='text-right'>"+tglJT+"</td>");
                                $("#urutan").append("<td class='text-right'> bulan ke "+value.bulan_ke+"</td>");
                                $("#urutan").append("<td class='text-right'>Rp."+formatNumber(parseInt(value.nominal_tagihan_perbulan))+"</td>");
                                if(value.tgl_pembayaran == null){
                                    $("#urutan").append("<td class='text-center'> - </td>");
                                }else{
                                    let gettByr = new Date(value.tgl_pembayaran);
                                    let tglByr = gettByr.getDate() + "/" + (gettByr.getMonth()+1) + "/" + gettByr.getFullYear() +' '+gettByr.getHours()+":"+gettByr.getMinutes()+":"+gettByr.getSeconds();
                                    $("#urutan").append("<td class='text-center'>"+tglByr+"</td>");
                                }
                                $("#urutan").append("<td class='text-center'>Rp."+formatNumber(parseInt(value.nominal_transfer_tagihan))+"</td>");
                                if(value.ref_no == null){
                                	$("#urutan").append("<td class='text-right'>-</td>");
                                }else{
                                	$("#urutan").append("<td class='text-right'>"+value.ref_no+"</td>");	
                                }
                                
                                // now
                                let dateNow = new Date();
                                var sub = String(dateNow);
                                var subu = String(current_datetime);
                                let noww = dateNow.getFullYear() +"-"+ (dateNow.getMonth()+1) +"-"+ sub.substr(8,2);
                                // jatuh tempo
                                var tjt = current_datetime.getFullYear() +"-"+ (current_datetime.getMonth()+1) + "-" + subu.substr(8,2);
                                var tgl = new Date(tjt);
                                var now = new Date(noww);
                                if(value.status == 4 || value.status == 3){
                                        var tglPertama = Date.parse(tgl);
                                        var tglKedua = Date.parse(now);
                                        var miliday = 24 * 60 * 60 * 1000;

                                    if( tgl.getTime() < now.getTime()){
                                        // console.log({tglPertama,tgl_kedua,miliday});
                                        var selisih = (tglKedua - tglPertama)/ miliday;
                                        if(selisih <= 0 ){
                                        	var vSelisih = "-";	
                                        }else{
                                        	var vSelisih = selisih+" hari";
                                    	}
                                    }else{
                                        var vSelisih = "-";
                                    }

                                }else if(value.status == 5){
                                        var tglPertama = Date.parse(tgl);

                                        var p1date = value.tgl_bukti_bayar;
                                        var p2date = p1date.split("-");
                                        var p3date = p2date[2].substr(0,2);
                                        var p4date = p2date[0]+"-"+p2date[1]+"-"+p3date; 
                                        let pbyDate = new Date(p4date);

                                        var tglKedua = Date.parse(pbyDate);
                                        var miliday = 24 * 60 * 60 * 1000;
                                        var selisih = (tglKedua - tglPertama)/ miliday;
                                        if(selisih <= 0){
                                        	var vSelisih = "-";	
                                        }else{
                                        	var vSelisih = selisih+" hari";
                                    	}
                                }else{
                                		var vSelisih = "-";
                                }
								
								$("#urutan").append("<td class='text-center'>"+vSelisih+"</td>");
                                // var status = value.status;
                                // var dstat;
                                // if(status == 1){
                                //     dstat = "Pembayaran Lunas";
                                // }else if(status == 3){
                                //     dstat = "Pembayaran Parsial";
                                // }else if(status == 5){
                                //     dstat = "Verifikasi Admin";
                                // }else{
                                //     dstat = "Belum Terbayar";
                                // }
                                // $("#urutan").append("<td class='text-right'>"+ dstat +"</td>");

                                var keterangan = value.ket;
                                var dket;
                                if(keterangan === "" || keterangan === null){dket = '-';}else{dket = keterangan;}
                                $("#urutan").append("<td class='text-right'>"+ dket +"</td>");

                                // console.log(tgl.getTime()+"=="+now.getTime());

                                var warna;
                                var text;
                                var aktif;
								
								var flag;
                                if(value.statusBukti == null){
                                    flag = value.status;
                                }else{
                                    flag = value.statusBukti;
                                }
                                // console.log(flag);

								if (1 == flag)
								{
									 warna = 'badge-success'; 
									 text = 'LUNAS';
									 id = 'bukticicil';
									 aktif = '';
                                }
								else if(5 == flag)
								{
									 warna = 'badge-success'; 
									 text = 'PROSES VERIFIKASI';
									 id = '';
									 aktif = 'disabled';
								}
								else if(3 == flag)
								{
									if( tgl.getTime() > now.getTime())
                                    {
                                         warna = 'badge-notif'; 
                                         text = 'BELUM JATUH TEMPO';
                                         id = '';
                                         aktif = 'disabled';
                                    }else{
                                     warna = 'badge-warning'; 
									 text = 'BELUM LUNAS';
									 id = 'cicil';
									 aktif = ' ';
                                    }
								}
								else if(4 == flag)
								{
									if( tgl.getTime() > now.getTime())
									{
										 warna = 'badge-notif'; 
										 text = 'BELUM JATUH TEMPO';
										 id = '';
										 aktif = 'disabled';
									}
									else
									{
										warna = 'badge-warning'; 
                                        text = 'BAYAR';
                                        id = 'cicil';
                               	        aktif = '';
									}
								}else if(6 == flag){
                                    warna = 'badge-warning'; 
                                    text = 'BAYAR ULANG';
                                    id = 'cicil';
                                    aktif = ' ';
                                }
								
								
                                $("#urutan").append("<td class='text-center'><input type='hidden' id='bT' value="+value.invoice_id+"><button type='button' id="+id+" value='"+pencairanid+"-"+value.invoice_id+"' class='badge "+warna+"' "+aktif+">"+text+"</button></td>");
                                $("#urutan").append("</tr>");
                            });
                        }
                    });
			$('#close').on('click',function() {
		        location.href="";
		    });

            $(document).on('click','#bukticicil',function(){
                var invoiceId = " ";
                var brwId = a;
                invoiceId = $('#bT').val();
                console.log(invoiceId);
                $('#gambarBT').html(" ");


                $('#modalBt').modal('show');
                
                // $('#modalBodyBt').append('<iframe id="linkInvoice"  width="100%" height="500" id="iprem"></iframe>');
                // $('#linkInvoice').attr('src',"{{ url('viewInvoiceBt') }}/"+bT+"#toolbar=0&navpanes=0");
                $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: "{{route('borrower.getGambarBT')}}",
                        method : "POST",
                        data: {'invoiceId': invoiceId},
                        dataType: 'JSON',
                    success:function(data)
                    {
                        console.log();
                        var ulang = data.length;
                        let html ="";
                        let active = '';

                        if(ulang>0){
                            for (let i =0; i<ulang; i++) {
                                image_url = '{{asset("storage/invoice/buktibayar/")}}/'+data[i].pic_pembayaran;
                        //         routes_url = "invoice/buktibayar/"+result[i].pic_pembayaran;

                                console.log(image_url);
                                if(i==0){
                                    active = 'active';
                                }else{
                                    active = ''
                                }
                        //         // $("ol").append('<li data-target="#carousel_image_pembayaran_lihat_tolak" data-slide='+i+' '+active+'>Appended item</li>');
                                html += '<div class="carousel-item '+active+'">'
                                            +'<img src='+image_url+' class="d-block w-100" >'
                                                +'<div class="carousel-caption d-none d-md-block"></div>'
                                        +'</div>';
                            }
                            $('#gambarBT').append(html);
                        // }else{
                        //     $('#image_pembayaran_lihat_tolak').append(html);
                        }

                        // console.log({
                        //     'routes_url' : routes_url,
                        //     'image_url' : image_url,
                        //     'pict_status' :pict_status
                        // });

                        // $('#routes_url').val(routes_url);
                        // $('#id_image').attr('src', image_url);
                        // $('#invoice_id_lihat').val(invoice_id_);
                        // $('#brw_id_lihat').val(brw_id_);
                        // $('#pict_status_lihat').val(pict_status);
                    }
                })
            
            });

			$(document).on('click','#cicil',function(e){
				 // console.log($(this).val());
				var sendId = $(this).val();
                var pencairanId = sendId.split('-');
				$('#modalBodyInvoice').html(" ");
            	
			    $.ajax({
                        url: "/borrower/createInvoice/"+sendId,
                        method: "get",
                        success:function(data)
                        {
                        	console.log(data.data);
                        	// var id = id_proyek+'-'+pendanaan_id+'-'+response.nosp3;
                            $('#modalInvoice').modal('show');
                            $('#modalBodyInvoice').append('<iframe id="linkInvoice"  width="100%" height="500" id="iprem"></iframe>');
                            $('#linkInvoice').attr('src',"{{ url('viewInvoice') }}/"+data.data+"#toolbar=0&navpanes=0");
                            $('#bayar').attr('value',pencairanId[0]);
                        }
                });

			});		

			$('#bayar').on('click',function(){
				$('#modalbayar').modal('show');
                $('#modalInvoice').modal('hide');
                $('#payout_detil').modal('hide');
            });

            $(document).on("change","#uploadBT", function(){
                var pencairanId = $('#bayar').val();
                var uploadFile = $(this);
                var brwId = a;
                console.log(uploadFile);
                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) return;

                if (/^image/.test( files[0].type)){ // only image file
                    var reader = new FileReader(); // instance of the FileReader
                    reader.readAsDataURL(files[0]); // read the local file
         
                    // reader.onloadend = function(){ // set image data as background of div
                    //     //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                    //   uploadFile.closest(".imgUp").find('.imagePreview').attr("src", this.result);
                    // }
                    var fileUpload = $(this).get(0);
                    var filess = fileUpload.files[0];
                    var form_data = new FormData();
                    form_data.append('file_name', filess);
                    // brwId+"/"+invoiceId+"/"+
                    form_data.append('idBrw', a);
                    form_data.append('pencairanId', pencairanId);
                    console.log(filess);
                    
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url: "{{route('borrower.uploadBT')}}",
                        method : "POST",
                        dataType: 'JSON',
                        data: form_data,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                          console.log(data)
                          if(data.Sukses){
                            // alert(data.Sukses);
                            swal.fire({
                              title: "Berhasil",
                              text: data.Sukses,
                              type: "success",
                              timer: 2000,
                              showConfirmButton: false
                            });
                            $("#sukses").html("<p align='center'>"+data.filename+"</p>");
                            $("#norefeal").html("<p align='center'><input type='text'  id='inputnorefeal' placeholder='masukan no refeal' required></p>");
                            $("#kirimBT").attr('disabled', false);
                             // document.getElementById('kirimBT').disabled = false
                          }else{
                            alert(data.failed);
                          }
                        }
                    });
                }
            });

            $('#kirimBT').on('click',function(){
                var file_name = $("#sukses").text();
    			var pencairanId = $('#bayar').val();
                var refealnumb = $('#inputnorefeal').val();
                // console.log(pencairanId);
                if(refealnumb == " " || refealnumb == 0){
                    swal.fire({title:"Peringatan !", html:"<p>Anda Harus Mengisikan No Refeal !</p>", type:"danger"});
                }else{
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url:'/borrower/updateBT',
                        method:'POST',
                        data: {'file_name': file_name,'pencairanId':pencairanId,'idBrw':a, 'refealnumb':refealnumb},
                        dataType: 'JSON',
                        // processData :false,
                        // contentType:false,
                        success: function (response) {
                                console.log(response);
                                if (response.status == '1')
                                {
                                    swal.fire({title:"Pembayaran Tagihan", html:"<p>Proses Upload Bukti Pembayaran Berhasil, Admin akan memverifikasi pembayaran Tagihan</p><p>Terimakasih</p>", type:"success"}).then(function(){
                                            location.reload(true)
                                        });
                                    // location.href="";
                                }
                                else
                                {
                                    swal.fire("Pembayaran Tagihan", "<p>Proses Upload Bukti Pembayaran Gagal, Coba Upload Ulang Bukti Pembayaran</p><p>Terimakasih</p>", "error");
                                    
                                }

                        },
                        error: function(request, status, error)
                        {   
                            alert('gagal kirim update bukti transfer')
                        }
                    });
                }
            });

            $('#dinvoice').on('click',function(){
                var pencairanId = $('#bayar').val();
                location.href = '/borrower/dinvoice/'+a+'/'+pencairanId;
            }); 
        });


    });

</script>
@endsection