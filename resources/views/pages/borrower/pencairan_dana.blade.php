@extends('layouts.borrower.master')

@section('title', 'Kelola Pencairan Dana')
<style>
    .bg-gradient-hijau{
    background: rgb(47,122,21);
    background: -moz-linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    background: linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#2f7a15",endColorstr="#067757",GradientType=1);
  }
  .bg-gradient-hijau-2{
    background: rgb(21,122,89);
    background: -moz-linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    background: linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#157a59",endColorstr="#065f77",GradientType=1);
  }
  .bg-gradient-blue{
    background: rgb(21,85,122);
    background: -moz-linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    background: linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#15557a",endColorstr="#063777",GradientType=1);
  }
  #overlay{   
      position: fixed;
      top: 0;
      left: 0;
      z-index: 900;
      width: 100%;
      height:100%;
      display: none;
      background: rgba(0,0,0,0.6);
    }

    .cv-spinner {
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;  
    }
   .spinner {
      width: 40px;
      height: 40px;
      border: 4px #ddd solid;
      border-top: 4px #2e93e6 solid;
      border-radius: 50%;
      animation: sp-anime 0.8s infinite linear;
    }
    @keyframes sp-anime {
      100% { 
          transform: rotate(360deg); 
      }
    }
    .is-hide{
        display:none;
    }
</style>

@section('content')
    <!-- Main Container -->

    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    @if(Auth::guard('borrower')->user()->status == 'pending')
    <main id="main-container">

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h1 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;" >Silahkan Tunggu Verifikasi dari Danasyariah</h1>                    
                    </span>
                </div>
            </div>
			<div class="row">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h4 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;" >Terimakasih telah bergabung untuk maju bersama kami</h4>                    
                    </span>
					
					
                </div>
            </div>
            
            <!-- END Frequently Asked Questions -->
        </div>
        <!-- END Page Content -->

    </main>
    @elseif(Auth::guard('borrower')->user()->status == 'active') 
    <main id="main-container">
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">
                <div class="row mt-5 pt-5">
                    <div id="col2" class="col-md-12 mt-5 pt-5">
                    <span class="mb-10 pb-10 ">
                        <h3 class="no-paddingTop font-w300 judul" style="float: left; " >Menuju Pencairan Dana</h3>
                    </span>
                    <br><br>
                    <table class="table border" id="table_akad">
                        <thead class="bg-dark text-light">
                            <th class="text-center" >No</th>
                            <th class="text-center" >Pendanaan</th>
                            <th class="text-center" >Tenor</th>
                            <th class="text-center" >Jumlah Dana</th>
                            <th class="text-center" >Imbal Hasil</th>
                            <th class="text-center" >Aksi</th>
                        </thead>
                        <tbody>
                               
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main> 
    @endif
    {{-- Modal TTD --}}
    <div class="modal fade" id="modalTTD" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5>TTD Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modalBodyTTD">
                
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="location.href=''">Batal</button>
                <button type="button" class="btn btn-success" id="prosesPenarikanDana">Proses Pendanaan</button>
            </div>-->
        </div>
        </div>
    </div>
    <div id="modalTermCondition"  class="modal fade show" role="dialog">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Syarat & Ketentuan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="agree">
                @csrf
                    <div class="modal-body">
                        {{-- <textarea rows="10" cols="200" readonly> --}}
    
                            <div style="padding:20px" style="overflow:scroll;">
                                <p style="text-align: justify;">Anda akan menggunakan tanda tangan elektronik untuk menandatanganani dokumen elektronik dengan PT. Dana Syariah Indonesia</p>
                                <p style="text-align: justify;"><strong>PT. Dana Syariah Indonesia</strong> bekerjasama dengan <strong>PT Privy Identitas Digital</strong> selaku Penyelenggara Tanda Tangan Elektronik dan Penyelenggara Sertifikasi Elektronik Indonesia yang mendapatkan pengakuan tersertifikasi pada Kementerian Komunikasi dan Informatika Republik Indonesia dengan merk PrivyID. </p>
                                <p style="text-align: justify;">Anda menyatakan setuju untuk mendaftar sebagai pengguna PrivyID untuk dibuatkan data pembuatan tanda tangan elektronik dan diterbitkan sertifikat elektronik oleh <strong>PT Privy Identitas Digital</strong> dalam rangka penggunaan layanan Tanda Tangan Elektronik untuk menandatangani dokumen elektronik.</p>
                                <p style="text-align: justify;">Anda memberi kuasa kepada <strong>PT. Dana Syariah Indonesia</strong> untuk meneruskan data KTP, swafoto, nomor ponsel dan alamat surel Anda sebagai data pendaftaran kepada <strong>PT Privy Identitas Digital</strong> guna memenuhi ketentuan Peraturan Perundang-undangan, yaitu Peraturan Pemerintah Nomor 71 Tahun 2019 tentang Penyelenggara Sistem dan Transaksi Elektronik, dan <strong>Peraturan Kementerian Informasi dan Komunikasi Nomor 11 Tahun 2018</strong>.</p>
                                <p style="text-align: justify;">Dengan ini anda juga menyatakan setuju untuk terikat pada syarat dan ketentuan layanan PrivyID yang terdapat pada tautan berikut: Syarat dan Ketentuan PrivyID</p>
                            </div>
    
                        {{-- </textarea> --}}
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="location.href=''">Batal</button>
                        <button type="button" class="btn btn-success" id="setujuTermCondition">Saya Setuju</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal TTD End --}}
    <!-- END Main Container -->
    <script type="text/javascript">

    function response_error_privy(response){
        var error_message = '';
        var temp_message = '';
        
        for(let i=0; i<response.errors.length; i++){
            if(response.errors[i].field=='identity.nama'){
                temp_message = (`${i+1}. Nama terlalu pendek (minimal 3 karakter), `);
            }else if(response.errors[i].field=='selfie'){
                if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid'){
                    temp_message = (`${i+1}. Foto KTP dan Selfie tidak boleh kosong, `);
                }else{
                    temp_message = (`${i+1}. Foto KTP/Selfie harus format JPG/PNG, `);
                }
            }else if(response.errors[i].field=='email'){
                if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid'){
                    temp_message = (`${i+1}. Email tidak boleh kosong/Format Email Salah, `);
                }else{
                    temp_message = (`${i+1}. Email anda sudah terdaftar, `);
                }
            }else if(response.errors[i].field=='identity.nik'){
                if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid'){
                    temp_message = (`${i+1}. NIK tidak boleh kosong, `);
                }else{
                    temp_message = (`${i+1}. Format Nomor NIK harus 16 digit, `);
                }
            }else if(response.errors[i].field=='identity.phone'){
                if(response.errors[i].messages[0]=='is required' || response.errors[i].messages[0]=='cannot be blank' || response.errors[i].messages[0]=='is invalid' || response.errors[i].messages[0]=='invalid phone number'){
                    temp_message = (`${i+1}. Nomor HP tidak boleh kosong/Format Nomor Hp anda salah, `);
                }else{
                    temp_message = (`${i+1}. Nomor HP anda sudah terdaftar, `);
                }
            }else{
                temp_message = (`${i+1}. ${response.errors[i].field} ${response.errors[i].messages.join()}, `)
            }
            error_message += temp_message;
        }
        //swal("Gagal", error_message, "error");
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: error_message
        });
        $('#overlay').css('display','none');
    };

    $(document).ready( function(){
        var a = {{$idbrw}};
        var marketerTable = $('#table_akad').DataTable({
            // searching: true,
            // processing: true,
            // serverSide: true,
            ajax: {
                url: '/borrower/pencairan_dana_data/'+a,
                method:'get',
                // dataSrc: 'data'
            },
            // paging: true,
            // info: true,
            // lengthChange:false,
            // order: [ 1, 'asc' ],
            // pageLength: 15,
            columns: [
                { data : null,
                    render: function (data, type, row, meta) {
                            //I want to get row index here somehow
                            return  meta.row+1;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                            //I want to get row index here somehow
                            return  row.nama;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                            //I want to get row index here somehow
                            return  row.tenor_waktu+' bulan';
                    }
                },
                { data: null,
                    render:function(data,type,row,meta)
                    {
                        if (row.total_need == null)
                        {
                            return 'Rp. 0';
                        }
                        else
                        {
                            return 'Rp.'+ parseInt(row.total_need).toLocaleString();
                        }
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                            //I want to get row index here somehow
                            return  parseInt(row.profit_margin)+5 +' %';
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                            //I want to get row index here somehow
                            return  '<button class="btn sm btn-primary" data-toggle="modal" data-target="#" id="btn_cairkan_dana">Cairkan Dana</button>';
                    }
                }
            ]
            // ,
            // columnDefs: [
            //     { targets: [0,7,8], visible: false}
            // ]
        });

        $('#table_akad tbody').on( 'click', 'tr', function () {
            var idbrw = {{ $idbrw }};
            var marketerTable = $('#table_akad').DataTable();
            var data = marketerTable.row( this ).data();

            var id_proyek = data['id'];
            var pendanaan_id = data['pendanaan_id'];

            $.ajax({
                url:'/user/checkRegistrasi/'+idbrw+'/2',
                method:'get',
                dataType:'json',
                beforeSend: function() {
                    $("#overlay").css('display','block');
                    swal.close();
                },
                success:function(response){
                
                
                    // munculin term n conditions jika user baru
                    if(response.status == "belum_terdaftar"){ // proses 

                        $("#modalTermCondition").modal("show").addClass('modal fade show in').attr('style','display:block');

                        $("#setujuTermCondition").on("click", function(){ // jika klik button setuju term dan condition
                            
                            $("#modalTermCondition .close").click();
                            
                            $.ajax({
                                url:'/user/RegisterPrivyID/'+idbrw+'/2', // 1 sama dengan investor, 2 sama dengan borrower
                                method:'get',
                                dataType:'json',
                                beforeSend: function() {
                                    $("#overlay").css('display','block');
                                },
                                success:function(responseRegistrasi)
                                {
                                    $("#overlay").css('display','none');
                                    var code = responseRegistrasi.code;
                                    
                                    //var messages = new Array();
                                    if(code == 422){ // status error

                                        console.log('privy atas');
                                        response_error_privy(responseRegistrasi);
                                        
                                    }
                                    else if(code == 201 && responseRegistrasi.data.status == "waiting"){ // jika status registrasi berhasil namun masih menunggu
                                        //swal("Pending", responseRegistrasi.message, "info");
                                        // alert('wow');
                                        Swal.fire({
                                            icon: 'info',
                                            title: 'Pending',
                                            text: responseRegistrasi.message
                                        });
                                        // $('#overlay').css('display','none');
                                        
                                    }
                                    else if(code == 201 && responseRegistrasi.data.status == "rejected"){ // jika status registrasi berhasil namun masih direjected
                                        let text_info = 'Silahkan ubah data anda terlebih dahulu dan lakukan pendanaan lagi.';
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Data Ditolak',
                                            text: responseRegistrasi.data.reject.reason + '. '+text_info
                                        });
                                        // $('#overlay').css('display','none');

                                    }
                                    else if(code == 201 &&  responseRegistrasi.data.status == "invalid"){ // jika status registrasi berhasil namun masih direjected
                                        let text_email = 'Silahkan cek email anda untuk upload file pendukung(KTP/KK/Pasport).';
                                        // swal("Data Tidak Valid", responseRegistrasi.data.reject.reason+'. '+text_email, "error");
                                        // $('#overlay').css('display','none');

                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Data Tidak Valid',
                                            text: responseRegistrasi.data.reject.reason+'. '+text_email
                                        });
                                        // $('#overlay').css('display','none');
                                    }
                                    else if(code == 201 && responseRegistrasi.data.status == "verified" || responseRegistrasi.data.status == "registered"){ // jika status user terverikasi maka akan generate 
                                        
                                        // swal("Registrasi Berhasil", responseRegistrasi.data.status, "success");
                                        // $('#overlay').css('display','none');

                                        Swal.fire({
                                            title: 'Pendaftaran Privy ID Berhasil',
                                            text: "User Anda Sudah diverifikasi, Proses Selanjutnya Tanda Tangan Akad Murabahah",
                                            icon: 'success',
                                            //showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            //confirmButtonText: 'Yes, delete it!'
                                        });
                                    } // end IF verified
                                }
                            });
                        });
                    }// end if belum terdaftar privy id
                
                    else{ // jika proses sudah mendafatar, tetapi nunggu verifikasi privy ID
                        console.log(response);
                        var code = response.code;
                        if(code == 422){ // status error

                            console.log('privy atas');
                            response_error_privy(response);

                        }
                        else if(code == 201 && response.data.status == "waiting"){ // jika status registrasi berhasil namun masih menunggu
                            //swal("Pending", responseRegistrasi.message, "info");
                            // alert('wow');
                            Swal.fire({
                                icon: 'info',
                                title: 'Pending',
                                text: response.message
                            });
                            // $('#overlay').css('display','none');

                        }
                        else if(code == 201 && response.data.status == "rejected"){ // jika status registrasi berhasil namun masih direjected
                            let text_info = 'Silahkan ubah data anda terlebih dahulu dan lakukan pendanaan lagi.';
                            Swal.fire({
                                icon: 'error',
                                title: 'Data Ditolak',
                                text: response.data.reject.reason + '. '+text_info
                            });
                            // $('#overlay').css('display','none');

                        }
                        else if(code == 201 &&  response.data.status == "invalid"){ // jika status registrasi berhasil namun masih direjected
                            let text_email = 'Silahkan cek email anda untuk upload file pendukung(KTP/KK/Pasport).';
                            // swal("Data Tidak Valid", responseRegistrasi.data.reject.reason+'. '+text_email, "error");
                            // $('#overlay').css('display','none');

                            Swal.fire({
                                icon: 'error',
                                title: 'Data Tidak Valid',
                                text: response.data.reject.reason+'. '+text_email
                            });
                            // $('#overlay').css('display','none');
                        }
                        else if(code == 201 && response.data.status == "verified" || response.data.status == "registered"){ // jika status user terverikasi maka akan generate 
                           
                            $.ajax({
                                url: '/borrower/sendDocMurabahahBorrower/'+idbrw+'/'+id_proyek,
                                method : "get",
                                beforeSend: function() {
                                    
                                    $("#overlay").css('display','block');

                                },

                                success:function(responseSendDocMurobahah)
                                {
                                    
                                    $('#overlay').css('display','block');
                                    var responseDoc = JSON.parse(responseSendDocMurobahah); // parsing response
                                    if(responseDoc.code == 201){ // sukses 
                                        
                                        // proses tanda tangan
                                        $('#modalTTD').modal('show').addClass('modal fade show in').attr('style','display:block')
                                        $('.modal-backdrop').addClass('modal-backdrop fade show in')
                                        $('#modalBodyTTD').append('<iframe id="linkTTD" sandbox="allow-forms allow-modals allow-scripts allow-same-origin" width="600" height="750"></iframe>');
                                        $('#linkTTD').attr('src',"/user/viewTTD_murabahah_borrower/"+responseDoc.data.recipients[1].privyId+"/130/280/13/"+responseDoc.data.docToken); // view ttd investor signing fixed posisi 
                                        
                                        var RunTimerTTD = setInterval(function(){

                                            $.ajax({
                                                url:'/user/checkStatusDocumentPrivy/'+responseDoc.data.docToken,
                                                method:'get',
                                                dataType:'json',
                                                success:function(response)
                                                {
                                                    console.log(response);
                                                    // if(response.data.recipients[0].signatoryStatus == "Completed"){
                                                    if(response.status_murabahah_borrower == "Completed"){
                                                        
                                                        
                                                        console.log('Completed');
                                                        clearInterval(RunTimerTTD);
                                                        
                                                        
                                                    }else{
                                                        console.log('lanjut');
                                                    }
                                                    
                                                },
                                            
                                            })
                                        }, 6000);
                                        
                                    }
                                }
                            });
                        }
                        else{

                            swal("Gagal", response.message, "error");
                            $('#overlay').css('display','none');
                        
                        }
                    }
                

                }
            });	

            
        });
    });
</script>
@endsection
