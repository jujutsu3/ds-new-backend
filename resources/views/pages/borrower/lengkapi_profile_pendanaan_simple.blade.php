@extends('layouts.borrower.master')
@section('title', 'Welcome Borrower')

@section('style')
<link href="{{ asset('vendor/smartWizard/smart_wizard_all.min.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css" type="text/css" rel="stylesheet" />
{{-- START: Style --}}
<style>
    .mandatory_label {
        color: red;
    }

    .line {
        display: flex;
        flex-direction: row;
    }

    .line:after {
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #000;
        margin: auto;
    }

    .input-hidden {
        height: 0;
        width: 0;
        visibility: hidden;
        padding: 0;
        margin: 0;
        float: right;
    }

    .input-hidden2 {
        opacity: 0;
        height: 0;
        width: 0;
        padding: 0;
        margin: 0;
        float: right;
    }

    .green-dsi {
        background-color: #057758;
    }

    .error {
        color: #ff0000;
    }

    .sweet_loader {
        width: 140px;
        height: 140px;
        margin: 0 auto;
        animation-duration: 0.5s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        animation-name: ro;
        transform-origin: 50% 50%;
        transform: rotate(0) translate(0, 0);
    }

    .radio-group {
        position: relative;
    }

    #status_rumah-error,
    #domisili_status_rumah-error,
    #no_hrd-error,
    #no_hrd_pasangan-error,
    #telepon-error,
    #telepon_pasangan-error,
    #no_telpon_usaha-error,
    #no_telpon_usaha_pasangan-error,
    #no_hp_usaha-error,
    #no_hp_usaha_pasangan-error,
    #gender-error,
    #jns_kelamin_pasangan-error,
    #status_kepemilikan_rumah-error,
    #usia_perusahaan-error {
        position: absolute;
        top: 100%;
    }

    #bank-error {
        position: absolute;
        top: 50%;
        left: 10px;
    }

    #status_kawin-error {
        position: absolute;
        top: 200%;
    }

    @keyframes ro {
        100% {
            transform: rotate(-360deg) translate(0, 0);
        }
    }
</style>
{{-- END: Style --}}
@endsection

@section('content')
<!-- START: Main Container -->
<main id="main-container">
    <!-- START: Page Content -->
    <div id="detect-screen" class="content-full-right">
        <div class="container">
            <div class="row mb-4">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h3 class="no-paddingTop font-w400 d-none" id="label-individu" style="float: left; margin-block-end: 0em; color: #31394D">
                            Form Ajukan Pendanaan</h3>
                        <h3 class="no-paddingTop font-w400 d-none" id="label-badan-hukum" style="float: left; margin-block-end: 0em; color: #31394D">
                            Form Lengkapi Profile Badan Hukum</h3>

                    </span>
                </div>
            </div>

            {{-- START: Individu --}}
            <div id="layout-individu" class="row mt-5 pt-5 d-none">
                @include('pages.borrower.lengkapi_profile_pendanaan_simple_individu')
            </div>
            {{-- END: Individu --}}
            {{-- START: Badan Hukum --}}
            <div id="layout-badan-hukum" class="row mt-5 pt-5 d-none">
                @include('pages.borrower.lengkapi_profile_pendanaan_simple_badan_hukum')
            </div>
            {{-- END: Badan Hukum --}}
        </div>
    </div>
    <!-- END: Page Content -->

    {{-- START: Modal OTP --}}
    <div class="modal fade" id="otp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header mb-3">
                    <h5 class="modal-title" id="scrollmodalLabel">Verifikasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetTimeout()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success col-sm-12" id="alertError" style="display: none;">
                </div>
                <div class="modal-body">
                    <form id="form_otp">
                        @csrf

                        <div class="form-group">
                            <label class="col-sm-12" id="set_no_hp" style="font-size:16px;"></label>
                        </div>

                        <div class="form-group row ml-1">
                            <label class="col-sm-3 col-form-label">No HP</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control col-sm-10" value="" name="no_hp" id="no_hp" disabled>
                            </div>
                        </div>
                        <div class="form-group row ml-1">
                            <label class="col-sm-3 col-form-label">Kode OTP</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control col-sm-6" value="" name="kode_otp" id="kode_otp">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="kirim_lagi">Kirim Lagi <span id="count"></span></button>
                    <button type="button" class="btn btn-success" id="kirim_data" disabled>Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- END: Modal OTP --}}

    <!-- START: Modal Confirmation -->
    <div class="modal fade" id="modal_action_lengkapi_profile_pendanaan_simple" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Simpan Data</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option closemodaldata" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p id="confirm-data">Anda yakin ingin menyimpan data ini ?</p>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Ubah</button>
                    <button type="button" id="btn_proses_lengkapi_profile" class="btn btn-alt-success">Kirim</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_action_lengkapi_profile_pendanaan_simple_badan_hukum" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Simpan Data</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option closemodaldata" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p id="confirm-data">Anda yakin ingin menyimpan data ini ?</p>


                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Ubah</button>
                    <button type="button" id="btn_lengkapi_profile_p" class="btn btn-alt-success">Kirim</button> -->
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Batal</button>
                    <button type="button" id="btn_lengkapi_profile_p" class="btn btn-alt-success">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Modal Confirmation -->
</main>
<!-- END: Main Container -->
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script src="{{ asset('vendor/credoApp/credoappsdk.js') }}"></script>
<script src="{{ asset('vendor/smartWizard/jquery.smartWizard.min.js') }}"></script>

{{-- START: Init --}}
<script>
    let a = $('#smartwizard')
    let timer = null; // For referencing the timer

    // START: Unique Field
    let is_phone_number_unique = 0
    let is_nik_unique = 0
    let is_npwp_unique = 0

    if ("{{ $data_individu ? $data_individu->no_tlp : '' }}") {
        is_phone_number_unique = 1
    }

    if ("{{ $data_individu ? $data_individu->ktp : '' }}") {
        is_nik_unique = 1
    }

    if ("{{ $data_individu ? $data_individu->npwp : '' }}") {
        is_npwp_unique = 1
    }
    // END: Unique Field

    Webcam.set({
        width: 180,
        height: 200,
        image_format: 'jpg',
        jpeg_quality: 90,
        flip_horiz: false
    });

    let brw_type = "{{ $data_individu ? $data_individu->brw_type : ''}}"

    if (brw_type == '') {
        swal.fire({
            title: "Lengkapi Profile",
            text: "Mendaftar sebagai ?",
            type: "question",
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: "Individu",
            cancelButtonText: "Perusahaan",
            confirmButtonColor: "#70B29C",
            cancelButtonColor: "#FFCA28",
        }).then(function(response) {
            // console.log(response.isConfirmed)
            // console.log(response)
            if (response.value) {
                $('#borrower_type').val("1");
                $('#label_tipe_borrower').text('Individu');
                $('#layout-individu').removeClass('d-none');
                $('#label-individu').removeClass('d-none');
                $('#layout-badan-hukum').addClass('d-none');
                brw_type = 1;
            } else if (response.dismiss == 'cancel') {
                $('#borrower_type').val("2");
                $('#label_tipe_borrower').text('Perusahaan');
                $('#layout-badan-hukum').removeClass('d-none');
                $('#label-badan-hukum').removeClass('d-none');
                $('#layout-individu').addClass('d-none');
                brw_type = 2;
            } else {
                location.href = "/borrower/beranda";
            }

            $.getJSON("/borrower/tipe_pendanaan_dr/" + brw_type, function(data_tipe_pendanaan) {

                $('#type_pendanaan_select').prepend("<option selected></option>").select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Tipe Pendanaan",
                    allowClear: true,
                    data: data_tipe_pendanaan,
                    width: '100%'
                });

            });
        });
    } else {
        if (brw_type == 1) {
            $('#borrower_type').val("1");
            $('#label_tipe_borrower').text('Individu');
            $('#layout-individu').removeClass('d-none');
            $('#layout-badan-hukum').addClass('d-none');
            $('#ktp').attr('disabled', true);
            $('#npwp').attr('disabled', true);
            $('#nama_individu').attr('disabled', true);
            $('#telepon').attr('disabled', true);


        } else if (brw_type == 2) {
            $('#borrower_type').val("2");
            $('#label_tipe_borrower').text('Perusahaan');
            $('#layout-badan-hukum').removeClass('d-none');
            $('#layout-individu').addClass('d-none');
        }
    }
</script>
{{-- End: Init --}}

<!-- START: CredoApp -->
<script>
    var authKey = "{{ config('app.credo_auth_key') }}";
    var url = "{{ config('app.credo_url') }}";
    var recordNumber = "{{ Auth::guard('borrower')->user()->brw_id }}";
    var credoAppService = new credoappsdk.CredoAppService(url, authKey);
    credoAppService.startEventTracking();

    function collectData() {
        credoAppService.stopEventTracking();
        credoAppService.collectAsync(recordNumber).then((recordNumber) => {});
    }
</script>
<!-- END: CredoApp -->

{{-- START: Camera --}}
<script>
    // Camera Click Action
    const btnCameraClick = (cameraAttachId, previewCameraId, takeCameraId) => {
        Webcam.attach('#' + cameraAttachId);
        $('#' + previewCameraId).addClass('d-none');
        $('#' + takeCameraId).removeClass('d-none');
    }

    // Take Snapshot Camera
    const takeSnapshot = (resultCamera, userCamera, userCameraVal, result) => {
        $('#' + resultCamera).removeClass('d-none');
        Webcam.snap(function(data_uri) {
            let filename = ''
            let msgName = ''
            if (userCamera == 'user_camera_diri') {
                filename = 'pic_brw'
                msgName = 'Diri'
            } else if (userCamera == 'user_camera_ktp') {
                filename = 'pic_brw_ktp'
                msgName = 'KTP'
            } else if (userCamera == 'user_camera_diri_dan_ktp') {
                filename = 'pic_brw_dan_ktp'
                msgName = 'Diri & KTP'
            } else if (userCamera == 'user_camera_npwp') {
                filename = 'pic_brw_npwp'
                msgName = 'NPWP'
            }

            $('#' + userCamera).val(data_uri);
            $('#' + userCameraVal).val('1');
            $('#' + result).html(`
                <img class="img-thumbnail" src="${data_uri}" style="width: 180px; height: 138px;" />
            `);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url: "/borrower/upload_foto",
                method: "POST",
                dataType: 'JSON',
                data: {
                    'img': data_uri,
                    'file_name': filename
                },
                success: function(data) {
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto ' + msgName + ' Berhasil',
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#' + userCamera).val(data.url);
                        console.log($('#' + userCamera).val())
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>
{{-- END: Camera --}}

{{-- START: Get Json Data --}}
<script>
    $(document).ready(function() {
        $('#txt_provinsi_pengurus, #txt_provinsi_pengurus_2').prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%'
        });

        $.getJSON("/borrower/kode_telepon/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#kode_telepon').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
            }

            let kode_telepon = "{{ $data_individu ? $data_individu->kode_telpon : ''}}"
            $("#kode_telepon option[value='" + kode_telepon + "']").prop('selected', true);
        });

        $.getJSON("/borrower/data_provinsi/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#provinsi').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
                $('#provinsi_domisili').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
                $('#provinsi_perusahaan').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));

                $('#provinsi_perusahaan_pasangan').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
                $('#provinsi_pasangan').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));

                // START: Badan Hukum
                $('#txt_provinsi_badan_hukum').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
                $('#txt_provinsi_domisili_badan_hukum').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
                $('#txt_provinsi_pengurus').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
                $('#txt_provinsi_pengurus_2').append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
                // END: Badan Hukum
            }

            let provinsi = "{{ $data_individu ? $data_individu->provinsi : ''}}"
            $("#provinsi option[value='" + provinsi + "']").prop('selected', true);

            let provinsi_domisili = "{{ $data_individu ? $data_individu->domisili_provinsi : ''}}"
            $("#provinsi_domisili option[value='" + provinsi_domisili + "']").prop('selected', true);

            let provinsi_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->provinsi : ''}}"
            $("#provinsi_perusahaan option[value='" + provinsi_perusahaan + "']").prop('selected', true);

            let provinsi_pasangan = "{{ $data_pasangan ? $data_pasangan->provinsi : ''}}"
            $("#provinsi_pasangan option[value='" + provinsi_pasangan + "']").prop('selected', true);

            let provinsi_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->provinsi : ''}}"
            $("#provinsi_perusahaan_pasangan option[value='" + provinsi_perusahaan_pasangan + "']").prop('selected', true);

        });

        $.getJSON("/borrower/agama/", function(data) {
            for (var i = 0; i < data.length; i++) {
                $('#agama').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
                $('#agama_pasangan').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
            }

            let agama = "{{ $data_individu ? $data_individu->agama : ''}}"
            $("#agama option[value='" + agama + "']").prop('selected', true);

            let agama_pasangan = "{{ $data_pasangan ? $data_pasangan->agama : ''}}"
            $("#agama_pasangan option[value='" + agama_pasangan + "']").prop('selected', true);
        })

        $.getJSON("/borrower/data_pendidikan/", function(data) {
            for (var i = 0; i < data.length; i++) {
                $('#pendidikan_terakhir').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
                $('#pendidikan_terakhir_pasangan').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
            }

            let pendidikan_terakhir = "{{ $data_individu ? $data_individu->pendidikan_terakhir : ''}}"
            $("#pendidikan_terakhir option[value='" + pendidikan_terakhir + "']").prop('selected', true);

            let pendidikan_terakhir_pasangan = "{{ $data_pasangan ? $data_pasangan->pendidikan : ''}}"
            $("#pendidikan_terakhir_pasangan option[value='" + pendidikan_terakhir_pasangan + "']").prop('selected', true);
        });

        $('#bank, #txt_bank_pribadi').prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%'
        });

        $.getJSON("/borrower/data_bank/", function(data) {
            let bank = "{{ $data_rekening ? $data_rekening->brw_kd_bank : ''}}";
            for (let i = 0; i < data.length; i++) {
                $('#bank').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
                $('#txt_bank_pribadi').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
            }
            if (bank) {
                $('#bank').val(bank);
                $('#bank').select2().trigger('change');
                $('#txt_bank_pribadi').val(bank);
                $('#txt_bank_pribadi').select2().trigger('change');
            }
        });

        $.getJSON("/borrower/data_pekerjaan/", function(data) {
            for (var i = 0; i < data.length; i++) {
                let id = data[i].id
                if (id == 6 || id == 7 || id == 8) {
                    // Remove
                } else {
                    $('#pekerjaan').append($('<option>', {
                        value: data[i].id,
                        text: data[i].text
                    }));

                    $('#pekerjaan_pasangan').append($('<option>', {
                        value: data[i].id,
                        text: data[i].text
                    }));
                }
            }

            let pekerjaan = "{{ $data_individu ? $data_individu->pekerjaan : ''}}"
            if (pekerjaan) {
                $("#pekerjaan option[value='" + pekerjaan + "']").prop('selected', true);
                $('#pekerjaan').trigger("change")
            }

            let pekerjaan_pasangan = "{{ $data_individu ? $data_individu->pekerjaan_pasangan : ''}}"
            if (pekerjaan_pasangan) {
                $("#pekerjaan_pasangan option[value='" + pekerjaan_pasangan + "']").prop('selected', true);
                $('#pekerjaan_pasangan').trigger("change")
            }
        });

        $.getJSON("/borrower/data_bidang_pekerjaan/", function(data) {
            for (var i = 0; i < data.length; i++) {
                let id = data[i].id
                if (id == 19 || id == 20) {
                    //Remove
                } else {
                    $('#bidang_pekerjaan').append($('<option>', {
                        value: data[i].id,
                        text: data[i].text
                    }));
                    $('#bidang_pekerjaan_pasangan').append($('<option>', {
                        value: data[i].id,
                        text: data[i].text
                    }));
                }
            }

            let bidang_pekerjaan = "{{ $data_individu ? $data_individu->bidang_pekerjaan : ''}}"
            $("#bidang_pekerjaan option[value='" + bidang_pekerjaan + "']").prop('selected', true);

            let bidang_pekerjaan_pasangan = "{{ $data_individu ? $data_individu->bd_pekerjaan_pasangan : ''}}"
            $("#bidang_pekerjaan_pasangan option[value='" + bidang_pekerjaan_pasangan + "']").prop('selected', true);
        });

        $.getJSON("/borrower/bidang_pekerjaan_online/", function(data) {
            for (var i = 0; i < data.length; i++) {
                $('#bidang_online').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
                $('#bidang_online_pasangan').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
            }

            let bidang_online = "{{ $data_individu ? $data_individu->bidang_online : ''}}"
            $("#bidang_online option[value='" + bidang_online + "']").prop('selected', true);

            let bidang_online_pasangan = "{{ $data_individu ? $data_individu->bd_pekerjaanO_pasangan : ''}}"
            $("#bidang_online_pasangan option[value='" + bidang_online_pasangan + "']").prop('selected', true);
        });

        $.getJSON("/borrower/bentuk_badan_usaha/", function(data) {
            for (var i = 0; i < data.length; i++) {
                $('#badan_usaha').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));

                $('#badan_usaha_pasangan').append($('<option>', {
                    value: data[i].id,
                    text: data[i].text
                }));
            }

            let badan_usaha = "{{ $data_penghasilan ? $data_penghasilan->bentuk_badan_usaha : ''}}"
            $("#badan_usaha option[value='" + badan_usaha + "']").prop('selected', true);

            let badan_usaha_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->bentuk_badan_usaha : ''}}"
            $("#badan_usaha_pasangan option[value='" + badan_usaha_pasangan + "']").prop('selected', true);
        });

        $.getJSON("/borrower/tipe_pendanaan_dr/{{ Session::get('brw_type') }}", function(data_tipe_pendanaan) {
            $('#type_pendanaan_select').prepend("<option selected></option>").select2({
                theme: "bootstrap",
                placeholder: "Pilih Tipe Pendanaan",
                allowClear: true,
                data: data_tipe_pendanaan,
                width: '100%'
            });

        });

        const jProperty = [];
        jProperty[0] = {
            id: '1',
            text: 'Baru'
        };
        jProperty[1] = {
            id: '2',
            text: 'Bekas'
        };
        $('#jenis_property').prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Jenis Rumah",
            allowClear: true,
            data: jProperty,
            width: '100%'
        });

    });

    $(`#txt_provinsi_badan_hukum, #txt_kota_badan_hukum, #txt_kecamatan_badan_hukum, #txt_kelurahan_badan_hukum, 
    #txt_provinsi_domisili_badan_hukum, #txt_kota_domisili_badan_hukum, #txt_kecamatan_domisili_badan_hukum, #txt_kelurahan_domisili_badan_hukum
    `).prepend('<option selected></option>').select2({
        theme: "bootstrap",
        placeholder: "-- Pilih Satu --",
        allowClear: true,
        width: '100%'
    });

    const provinsiChange = (thisValue, thisId, nextId) => {
        $('#' + nextId).empty(); // set null
        $('#' + nextId).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        // let provinsi =  $('#'+thisId+' option:selected').text();
        $.getJSON("/borrower/data_kota/" + thisValue + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#' + nextId).append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
            }
        });
    }

    const kotaChange = (thisValue, thisId, nextId) => {
        $('#' + nextId).empty(); // set null
        $('#' + nextId).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kecamatan/" + thisValue + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#' + nextId).append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
            }
        });
    }

    const kecamatanChange = (thisValue, thisId, nextId) => {
        $('#' + nextId).empty(); // set null
        $('#' + nextId).append($('<option>', {
            value: '',
            text: '-- Pilih Satu --'
        }));
        $.getJSON("/borrower/data_kelurahan/" + thisValue + "/", function(data) {
            for (let i = 0; i < data.length; i++) {
                $('#' + nextId).append($('<option>', {
                    value: data[i].text,
                    text: data[i].text
                }));
            }
        });
    }

    // const kelurahanChange = (thisValue, thisId, nextId) => {
    //     // $('#'+nextId).val(thisValue)
    //     $.getJSON("/borrower/data_kode_pos/" + thisValue + "/", function(data) {
    //         $('#' + nextId).val(data[0].text)
    //     });
    // }
    const kelurahanChange = (thisValue, thisId, nextId, vkec, vkota, vprov) => {
        // $('#'+nextId).val(thisValue)
        $.getJSON("/borrower/data_kode_pos_v2/" + thisValue + "/" + vkec + "/" + vkota + "/" + vprov, function(data) {
            $('#' + nextId).val(data[0].text)
        });
    }
</script>
{{-- END: Get Json Data --}}

{{-- START: Smartwizard --}}
<script>
    $("#form-individu").validate({
        onfocusout: false,
        invalidHandler: function(e, validator) {
            if (validator.errorList.length) {
                validator.errorList[0].element.focus()
            }
        }
    });

    // START: Individu
    $('#smartwizard').smartWizard({
        theme: 'dots',
        enableURLhash: false,
        autoAdjustHeight: false,
        enableAllSteps: true,
        enableFinishButton: true,
        hideButtonsOnDisabled: true,
        toolbarSettings: {
            toolbarExtraButtons: [
                $('<button type="button" id="finish" class="d-none" disabled></button>').text('Kirim')
                .addClass('btn btn-rounded btn-big btn-noborder btn-success btn-lg p-2 px-3 pull-right sw-btn-group-extra')
                .on('click', function() {
                    onFinishCallback()
                }),
            ]
        },
        keyboardSettings: {
            keyNavigation: false,
        },
        lang: {
            next: 'Selanjutnya',
            previous: 'Kembali',
            finish: 'selesai'
        },
        // hiddenSteps: [1],
    });

    $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
        // $('#nama_pasangan').focus()
        $('#nama_individu').focus()
        if ($('button.btn.sw-btn-prev').hasClass('disabled')) {
            $('.sw-btn-group-extra').removeClass('d-none');
            $('.sw-btn-next').addClass('d-none');
            $('#chk-agreement').removeClass('d-none')
            $('.sw-btn-prev').removeClass('d-none');
        } else {
            $('.sw-btn-group-extra').addClass('d-none');
            $('.sw-btn-next').removeClass('d-none');
            $('#chk-agreement').addClass('d-none');
            $('.sw-btn-prev').addClass('d-none');

        }

    });

    $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
        console.log("valid=" + $("#form-individu").valid());
        if (stepDirection === 'backward') {
            $('input[name="status"]').val('1')
            return true
        } else {
            console.log("valid=" + $("#form-individu").valid());
            if ($("#form-individu").valid()) {
                console.log(currentStepIndex);
                if (currentStepIndex == 0) {
                    if (is_nik_unique == 1) {
                        if (is_phone_number_unique == 1) {
                            if (is_npwp_unique == 1) {
                                // $('#submitindividu').click()
                                $('input[name="status"]').val('2')
                                return true
                            }
                        } else {

                        }
                    } else {

                    }
                }
                $('input[name="status"]').val('2')
                return true
            } else {
                return false
            }
        }

    });
    // END: Individu
</script>
{{-- END: Smartwizard --}}

{{-- START: Dom Manipulation --}}
<script>
    $(document).ready(function() {

        $('.sw-btn-next').addClass('d-none btn btn-rounded btn-big btn-noborder btn-success btn-lg p-2 px-3 pull-right');
        $('.sw-btn-prev').addClass('d-none btn btn-rounded btn-big btn-noborder btn-success btn-lg p-2 px-3 pull-left');
        $('.sw-btn-group-extra').removeClass('d-none');

        // if($('#step-informasi-pribadi-title').hasClass('active')){
        //     $('.sw-btn-prev').addClass('d-none');
        // }else if($('#step-informasi-pribadi-title').hasClass('done')){
        //     $('.sw-btn-prev').removeClass('d-none');
        // }

        // START: Individu

        // Check Agreement
        $('#chk_agreement').change(function() {
            let chk_agreement = $("input[name='chk_agreement']:checked").val()
            if (chk_agreement == '1') {
                $('#finish').prop('disabled', false)
            } else {
                $('#finish').prop('disabled', true)
            }
        })
        // Validasi No HP
        $(".no-zero").on("input", function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });

        $(".no-four").on("input", function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '4') {
                $(this).val(thisValue.substring(1))
            }
        });

        $(".eight").on("input", function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) != '8') {
                $(this).val(thisValue.substring(1))
            }
        });

        // Check No Hp
        $('#telepon').on('keyup', function() {
            let no_hp_individu = $(this).val()
            if (no_hp_individu.length >= 9) {
                $.ajax({
                    url: "{{ url('borrower/check_no_tlp') }}" + "/" + no_hp_individu,
                    method: "GET",
                    success: (response) => {
                        if (response.status == 'ada') {
                            $('#label_telepon').text('').append('No. Telepon Selular <i class="text-danger">Nomor sudah terdaftar</i>')
                            is_phone_number_unique = 0
                        } else {
                            $('#label_telepon').text('').append('No. Telepon Selular <i class="text-danger">*</i>')
                            is_phone_number_unique = 1
                        }
                    },
                    error: (response) => {
                        console.log(response)
                    }
                })
            }
        })

        // Check NIK
        $('#ktp').on('keyup', function() {
            let no_ktp = $(this).val()
            if (no_ktp.length == 16) {
                $.ajax({
                    url: "{{ url('borrower/check_nik') }}" + "/" + no_ktp,
                    method: "GET",
                    success: (response) => {
                        if (response.status == 'ada') {
                            $('#label_ktp').text('').append('<i class="text-danger">NIK sudah terdaftar</i>')
                            is_nik_unique = 0
                        } else {
                            $('#label_ktp').text('').append('NIK <i class="text-danger">*</i>')
                            is_nik_unique = 1
                        }
                    },
                    error: (response) => {
                        console.log(response)
                    }
                })
            }
        })

        // Check NPWP
        $('#npwp').on('keyup', function() {
            let npwp = $(this).val()
            let brw_type = $('#borrower_type').val()
            if (npwp.length == 15) {
                $.ajax({
                    url: "{{ url('borrower/check_npwp') }}" + "/" + npwp + "/" + brw_type,
                    method: "GET",
                    success: (response) => {
                        if (response.status == 'ada') {
                            $('#label_npwp').text('').append('<i class="text-danger">No. NPWP sudah terdaftar</i>')
                            is_npwp_unique = 0
                        } else {
                            $('#label_npwp').text('').append('No. NPWP <i class="text-danger">*</i>')
                            is_npwp_unique = 1
                        }
                    },
                    error: (response) => {
                        console.log(response)
                    }
                })
            }
        })

        $('#domisili_status').change(() => {
            if ($('#domisili_status').is(':checked')) {
                $('#domisili_status').val("1");
                // $("#layout-alamat-domisili").addClass('d-none').find('input, select, textarea, radio').prop("disabled", true);

                let alamat = $("#alamat").val();
                let provinsi = $("#provinsi option:selected").val();
                let kota = $("#kota option:selected").val();
                let kecamatan = $("#kecamatan option:selected").val();
                let kelurahan = $("#kelurahan option:selected").val();
                let kode_pos = $("#kode_pos").val();
                let status_rumah = $("input[name=status_rumah]:checked").val();

                console.log(alamat)

                // appen value
                $('#alamat_domisili').text(alamat);
                $('#provinsi_domisili').empty()
                $('#provinsi_domisili').append($('<option>', {
                    value: provinsi,
                    text: provinsi
                }));
                $('#kota_domisili').append($('<option>', {
                    value: kota,
                    text: kota
                }));
                $("#kecamatan_domisili").append($('<option>', {
                    value: kecamatan,
                    text: kecamatan
                }));
                $("#kelurahan_domisili").append($('<option>', {
                    value: kelurahan,
                    text: kelurahan
                }));
                $("#kode_pos_domisili").val(kode_pos);

                // Set selected
                $("#provinsi_domisili option[value='" + provinsi + "']").attr({
                    selected: 'selected'
                })
                $("#kota_domisili option[value='" + kota + "']").attr({
                    selected: 'selected'
                })
                $("#kecamatan_domisili option[value='" + kecamatan + "']").attr({
                    selected: 'selected'
                })
                $("#kelurahan_domisili option[value='" + kelurahan + "']").attr({
                    selected: 'selected'
                })
                $('input[name="domisili_status_rumah"][value="' + status_rumah + '"]').prop('checked', true);

                // Add readonly
                $('#alamat_domisili').attr('disabled', true);
                $("#provinsi_domisili").attr('disabled', true);
                $("#kota_domisili").attr('disabled', true);
                $("#kecamatan_domisili").attr('disabled', true);
                $("#kelurahan_domisili").attr('disabled', true);
                $('input[name=domisili_status_rumah]').attr("disabled", true);

                // Make it valid
                $('#alamat_domisili').valid()
                $('#provinsi_domisili').valid()
                $('#kota_domisili').valid()
                $('#kecamatan_domisili').valid()
                $('#kelurahan_domisili').valid()
                $('input[name=domisili_status_rumah]').valid()

            } else {
                $('#domisili_status').val("0");
                // $("#layout-alamat-domisili").removeClass('d-none').find('input, select, textarea, radio').prop("disabled", false);

                $('#alamat_domisili').text('');
                $('#provinsi_domisili').empty().append($('<option>', {
                    value: '',
                    text: '-- Pilih Satu --'
                }));
                $.getJSON("/borrower/data_provinsi/", function(data) {
                    for (let i = 0; i < data.length; i++) {
                        $('#provinsi_domisili').append($('<option>', {
                            value: data[i].text,
                            text: data[i].text
                        }));
                    }
                });
                $('#kota_domisili').empty().append($(' <option>', {
                    value: '',
                    text: '-- Pilih Satu --'
                }));
                $('#kecamatan_domisili').empty().append($('<option>', {
                    value: '',
                    text: '-- Pilih Satu --'
                }));
                $('#kelurahan_domisili').empty().append($('<option>', {
                    value: '',
                    text: '-- Pilih Satu --'
                }));
                $('#kode_pos_domisili').val('');
                $('input[name="domisili_status_rumah"]').prop('checked', false);

                $('#alamat_domisili').attr('disabled', false);
                $("#provinsi_domisili").attr('disabled', false);
                $("#kota_domisili").attr('disabled', false);
                $("#kecamatan_domisili").attr('disabled', false);
                $("#kelurahan_domisili").attr('disabled', false);
                $('input[name=domisili_status_rumah]').attr("disabled", false);
            }
        })


        // Status Kawin
        $('input[type=radio][name="status_kawin"]').change(() => {
            let status_kawin = $("input[name='status_kawin']:checked").val()
            if (status_kawin == '1') {
                // let jenis_kelamin = $("input[name='gender']:checked").val()

                // if(jenis_kelamin == '1') {
                //    $('#jns_kelamin_pasangan_2').prop("checked", true);
                // } else if(jenis_kelamin == '2'){
                //    $('#jns_kelamin_pasangan_1').prop("checked", true);
                // }

                // $('.sw-btn-next').removeClass('d-none');
                // $('.sw-btn-group-extra').addClass('d-none');
                // $('#chk-agreement').addClass('d-none')

                $("#skema_pembiayaan option[value='2']").remove();
                $('#skema_pembiayaan').append($('<option>', {
                    value: "2",
                    text: "Penghasilan Bersama"
                }));
            } else {

                // $('.sw-btn-next').addClass('d-none'); 
                // $('.sw-btn-group-extra').removeClass('d-none');
                // $('#chk-agreement').removeClass('d-none')

                // set skema pembiayaan sendiri
                $("#skema_pembiayaan option[value='2']").remove();
            }
        })

        // $('input[type=radio][name="gender"]').change(() => {
        //    let jenis_kelamin = $("input[name='gender']:checked").val()

        //    if(jenis_kelamin == '1') {
        //        $('#jns_kelamin_pasangan_2').prop("checked", true);
        //    } else if(jenis_kelamin == '2'){
        //        $('#jns_kelamin_pasangan_1').prop("checked", true);
        //    }
        // })

        // Jenis Kelamin Pasangan 
        $('input[name="jns_kelamin_pasangan"]').click(function(e) {
            e.preventDefault();
        });

        $('#surat_ijin_usaha').change((e) => {
            let thisValue = e.target.value
            if (thisValue == '1') {
                $('#no_ijin_usaha').prop('disabled', false).removeClass('d-none');
                $('#label_no_ijin_usaha').removeClass('d-none');
                $('#no_ijin_usaha-error').removeClass('d-none')
            } else if (thisValue == '2') {
                $('#no_ijin_usaha').prop('disabled', true).addClass('d-none');
                $('#label_no_ijin_usaha').addClass('d-none');
                $('#no_ijin_usaha-error').addClass('d-none')
            } else {
                $('#no_ijin_usaha').prop('disabled', false).removeClass('d-none');
                $('#label_no_ijin_usaha').removeClass('d-none');
                $('#no_ijin_usaha-error').removeClass('d-none')
            }
        })

        $('#sumber_penghasilan_dana').change((e) => {
            let thisValue = e.target.value
            if (thisValue == '1') {
                $('#label_nama_perusahaan').text('').append('Nama Perusahaan <i class="text-danger">*</i>');
                $('#label_no_telpon_usaha').text('').append('No. Telepon Perusahaan <i class="text-danger">*</i>');

                $('#nama_perusahaan').prop("placeholder", "Masukkan nama perusahaan");
                $('#no_telpon_usaha').prop("placeholder", "Masukkan no. telp perusahaan");

                $('.non-fixed').addClass('d-none').find('input, select, textarea, radio').prop('disabled', true)
                $('.fixed-income').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false)
            } else if (thisValue == '2') {
                $('#label_nama_perusahaan').text('').append('Nama Usaha/Profesional (Dokter dll) <i class="text-danger">*</i>');
                $('#label_no_telpon_usaha').text('').append('No. Telepon Usaha/Praktek <i class="text-danger">*</i>');

                $('#nama_perusahaan').prop("placeholder", "Masukkan nama Usaha");
                $('#no_telpon_usaha').prop("placeholder", "Masukkan no. telp Usaha");

                $('.non-fixed').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false)
                $('.fixed-income').addClass('d-none').find('input, select, textarea, radio').prop('disabled', true)
            }
        })

        $('#pekerjaan').change((e) => {
            let thisValue = e.target.value
            $('#detail-penghasilan').removeClass('d-none')
            // if(thisValue == '6' || thisValue == '7' || thisValue == '8') {
            //     $('#detail-penghasilan-lain-lain').removeClass('d-none')
            //     $('#detail-penghasilan').addClass('d-none')

            //     swal.fire({
            //         title: "Informasi",
            //         type: "warning",
            //         text: "Maaf anda tidak bisa melanjutkan ke tahap berikutnya karena data pekerjaan yang diinputkan tidak memenuhi syarat",
            //         allowOutsideClick: false,
            //         showCancelButton: false,
            //         confirmButtonClass: "btn-primary",
            //     }).then(() => {
            //         $("#pekerjaan option[value='']").prop('selected', true);
            //     });
            // } else {
            //     $('#detail-penghasilan-lain-lain').addClass('d-none')

            // }

            if (thisValue == '5') {
                $('.nonfixed').removeClass('d-none');
                $('.fixedin').addClass('d-none');
            }
            if (thisValue != '5') {
                $('.nonfixed').addClass('d-none');
                $('.fixedin').removeClass('d-none');
            }
            // else{

            // }
        })

        // data persyaratan pendanaan pribadi
        $(function() {
            getPendanaanType(2)

            $('#type_pendanaan_select').change(function() {

                var tipe_borrower_val = $("#type_borrower option:selected").val();
                var tipe_borrower_text = $("#type_borrower option:selected").text();
                var tipe_pendanaan = $('#type_pendanaan_select option:selected').val();
                var tipe_pendanaan_text = $('#type_pendanaan_select option:selected').text();
                var tipe_borrower = "{{ Session::get('brw_type') }}";
                var brw_id = "{{ Session::get('brw_id') }}";

                $('#txt_harga_objek_pendanaan').val("");
                getPendanaanType(tipe_pendanaan)

                if (tipe_pendanaan == 2 && tipe_pendanaan_text == "Dana Rumah") {

                    $('.dana-rumah').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false);
                    $('.dana-konstruksi').addClass('d-none').find('input,  select, textarea, radio').prop('disabled', false);
                    $('#type_tujuan_pendanaan').empty().append($('<option>', {
                        value: "",
                        text: "-- Pilih --"
                    }));
                    $('#txt_jangka_waktu').empty().append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    $('#label_txt_jangka_waktu').text('Jangka Waktu (Bulan)')


                } else if (tipe_pendanaan_text == "Dana Konstruksi") {
                    // txt_tipe_borrower

                    $('.dana-konstruksi').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false);
                    $('.dana-rumah').addClass('d-none').find('input,  select, textarea, radio').prop('disabled', false);

                    $("#txt_nilai_pengajuan").val("").attr({
                        "readonly": true,
                        required: true
                    });

                    $('#txt_jangka_waktu').empty().append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));

                    for (let i = 1; i <= 24; i++) {
                        $('#txt_jangka_waktu').append($('<option>', {
                            value: i,
                            text: i
                        }));
                    }
                    $('#label_txt_jangka_waktu').text('Jangka Waktu (Maksimal 24 Bulan)')

                } else {
                    // location.href = "/borrower/ajukanPendanaan";
                }

            });
        });

        // on change tujuan pendanaan
        $(function() {
            $('#type_tujuan_pendanaan').change(function() {
                var id = this.options[this.selectedIndex].value;
                var val = this.options[this.selectedIndex].text;
                var tipe_pendanaan_text = $('#type_pendanaan_select option:selected').text();

                $(this).valid()


                if (id == '1' && val == "Pra Kepemilikan Rumah") {

                    $("#txt_uang_muka").attr("disabled", true).val(0);
                    $("#txt_persen").attr("disabled", true).val(0);
                    $("#txt_harga_objek_pendanaan").val("");


                    $("#txt_nilai_pengajuan").val("").attr({
                        disabled: false,
                        "readonly": false,
                        required: true
                    });

                    $('#txt_jangka_waktu').empty().append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));

                    for (let i = 1; i <= 12; i++) {
                        $('#txt_jangka_waktu').append($('<option>', {
                            value: i,
                            text: i
                        }));
                    }
                    $('#label_txt_jangka_waktu').text('Jangka Waktu (Maksimal 12 Bulan)')

                } else {
                    $("#txt_persen").attr("disabled", false).val(0);
                    $("#txt_uang_muka").attr("disabled", false);

                    $("#txt_harga_objek_pendanaan").val("");
                    $("#txt_nilai_pengajuan").val("").attr({
                        "readonly": true,
                        disabled: true,
                        required: false,
                    });

                    $('#txt_jangka_waktu').empty().append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    if (tipe_pendanaan_text == "Dana Konstruksi") {
                        for (let i = 1; i <= 24; i++) {
                            $('#txt_jangka_waktu').append($('<option>', {
                                value: i,
                                text: i
                            }));
                        }
                        $('#label_txt_jangka_waktu').text('Jangka Waktu (Maksimal 24 Bulan)')
                    } else {
                        for (let i = 5; i <= 15; i += 5) {
                            $('#txt_jangka_waktu').append($('<option>', {
                                value: i * 12,
                                text: i
                            }));

                        }
                        $('#label_txt_jangka_waktu').text('Jangka Waktu (Maksimal 15 Tahun)')
                    }
                }
            });
        });

        // on change value uang muka

        $('#txt_uang_muka').keyup(function() {
            var tujuan = $("#type_tujuan_pendanaan option:selected").val();
            if (tujuan != 1) {

                var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
                var uang_muka = $("#txt_uang_muka").val();

                let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
                let int_uang_muka = parseInt(uang_muka.replaceAll('.', ''))
                let persen = (int_uang_muka / int_harga_pendanaan) * 100;
                let total_pendanaan = int_harga_pendanaan - int_uang_muka
                $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
                if (uang_muka == "") {
                    $("#txt_persen").val('0.00')
                } else {
                    $("#txt_persen").val(persen.toFixed(2));
                }
            }

        });


        $('#txt_persen').keyup(function() {
            var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
            // var uang_muka = $("#txt_uang_muka").val();
            var persen = Math.round($(this).val());

            let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
            // let int_uang_muka = parseInt(uang_muka.replaceAll('.', ''))

            let uangmuka = uangMuka(int_harga_pendanaan, persen / 100);
            let total_pendanaan = int_harga_pendanaan - uangmuka;
            $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
            $("#txt_uang_muka").val(formatRupiah(uangmuka.toString())).valid();

        });

        $('#txt_persen').change(function() {
            var persen = Math.round($(this).val());
            var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
            var int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''));
            var uang_muka = uangMuka(int_harga_pendanaan, 0.05);
            if (persen < 5) {
                swal.fire({
                    title: "Warning",
                    type: "warning",
                    text: "Minimal Uang muka Sebesar 5.00 %",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                });
                $("#txt_uang_muka").val(formatRupiah(uang_muka.toString())).valid();
                $("#txt_persen").val("5.00");
                let total_pendanaan = int_harga_pendanaan - uang_muka;
                $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
            }
        });

        $('#txt_uang_muka').change(function() {
            var uang_muka = $(this).val();
            var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
            var int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''));
            var int_uang_muka = parseInt(uang_muka.replaceAll('.', ''));
            var minUangmuka = uangMuka(int_harga_pendanaan, 0.05);
            if (int_uang_muka < minUangmuka || uang_muka == "") {
                swal.fire({
                    title: "Warning",
                    type: "warning",
                    text: "Minimal Uang muka Sebesar 5.00 %",
                    showCancelButton: false,
                    confirmButtonClass: "btn-success",
                });
                $("#txt_uang_muka").val(formatRupiah(minUangmuka.toString())).valid();
                $("#txt_persen").val("5.00");
                let total_pendanaan = int_harga_pendanaan - minUangmuka;
                $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
            }
        });

        $("#txt_nilai_pengajuan").keyup(function() {

            var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
            let nilai_pengajuan = $(this).val()

            let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
            let int_nilai_pengajuan = parseInt(nilai_pengajuan.replaceAll('.', ''))

        })


        $("#txt_harga_objek_pendanaan").blur(function() {

            if (brw_type == 1 && $('#type_pendanaan_select option:selected').text() == 'Dana Rumah') {
                var harga_pendanaan_rumah = $(this).val().replaceAll('.', '');
            }

        })

        $("#txt_harga_objek_pendanaan").keyup(function() {
            var tujuan = $("#type_tujuan_pendanaan option:selected").val();
            if (tujuan != 1) {
                var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
                // var uang_muka = $("#txt_uang_muka").val();

                let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
                let minUangmuka = uangMuka(int_harga_pendanaan, 0.05);
                // console.log(minUangmuka);

                // let int_uang_muka = parseInt(uang_muka.replaceAll('.', ''))
                if ($('#type_pendanaan_select option:selected').text() == 'Dana Konstruksi') {
                    minUangmuka = 0;
                }
                if (int_harga_pendanaan > 0) {
                    var uang_muka = $("#txt_uang_muka").val(formatRupiah(minUangmuka.toString())).valid();
                    // var uang_muka = uangMuka(int_harga_pendanaan, 0.05);

                    // var minPersen = (minUangmuka / int_harga_pendanaan) * 100;
                    $("#txt_persen").val("5.00");
                    let total_pendanaan = int_harga_pendanaan - minUangmuka
                    $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
                } else {
                    $("#txt_nilai_pengajuan").val('0');
                }
            }


            // if (int_uang_muka > 0 && int_harga_pendanaan > int_uang_muka) {
            //     let total_pendanaan = int_harga_pendanaan - int_uang_muka
            //     $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
            // } else {
            //     $("#txt_nilai_pengajuan").val('0');
            // }
        })

        function uangMuka(nominal, persen) {
            var getUm = nominal * persen;
            var hasil = Math.round(getUm)
            return hasil;
        }


        let sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';


        // $("form#form-individu").submit(function(e) {
        //     e.preventDefault();
        //     let status_kawin = $("input[name='status_kawin']:checked").val()
        //     $.ajax({
        //         type: $(this).attr('method'),
        //         url: "{{ route('borrower.action_lengkapi_profile_pendanaan_simple') }}",
        //         data: $(this).serialize(),
        //         success: function(data) {
        //             if (data.history_data_status != 0) {
        //                 collectData();
        //                 swal.fire({
        //                     title: "Pendaftaran Berhasil",
        //                     type: "success",
        //                     showCancelButton: false,
        //                     confirmButtonClass: "btn-success",
        //                 }).then((result) => {
        //                     location.href = "/borrower/beranda";
        //                 });
        //             }
        //         },
        //         error: function(data) {
        //             console.log(data)
        //             swal.fire({
        //                 title: "Pendaftaran Gagal",
        //                 type: "error",
        //                 text: data,
        //                 showCancelButton: false,
        //                 confirmButtonClass: "btn-danger",
        //             })
        //         },
        //     });
        // })

        $('.sw-btn-next').removeClass('d-none');
        $('.sw-btn-group-extra').addClass('d-none');
        $('#chk-agreement').addClass('d-none');

        // Get History Inserted Data
        // Status Kawin
        let status_kawin = "{{ $data_individu ? $data_individu->status_kawin : ''}}"
        if (status_kawin) {
            $('input:radio[name="status_kawin"][value="' + status_kawin + '"]').prop('checked', true);

            if (status_kawin == '1') {
                let jenis_kelamin = $('#jns_kelamin').val()
                if (jenis_kelamin == '1') {
                    $('#jns_kelamin_pasangan_2').prop("checked", true);
                } else if (jenis_kelamin == '2') {
                    $('#jns_kelamin_pasangan_1').prop("checked", true);
                }


                // $('.sw-btn-next').removeClass('d-none');
                // $('.sw-btn-group-extra').addClass('d-none');
                // $('#chk-agreement').addClass('d-none');
            } else {

                // $('.sw-btn-next').addClass('d-none');
                // $('.sw-btn-group-extra').removeClass('d-none');
                // $('#chk-agreement').removeClass('d-none');
            }
        }

        // Jenis Kelamin
        //        let jns_kelamin = "{{ $data_individu ? $data_individu->jns_kelamin : ''}}"
        //        $('input:radio[name="gender"][value="' + jns_kelamin + '"]').prop('checked', true);
        //        $('#jns_kelamin').val(jns_kelamin);
        //
        //        jns_kelamin_pasangan = 0
        //        if (jns_kelamin == '1') {
        //            jns_kelamin_pasangan = 2
        //        } else {
        //            jns_kelamin_pasangan = 1
        //        }
        //        $('input:radio[name="jns_kelamin_pasangan"][value="' + jns_kelamin_pasangan + '"]').prop('checked', true);

        // START: History Alamat
        let provinsi = "{{ $data_individu ? $data_individu->provinsi : '' }}"
        let kota = "{{ $data_individu ? $data_individu->kota : '' }}"
        let kecamatan = "{{ $data_individu ? $data_individu->kecamatan : '' }}"
        let kelurahan = "{{ $data_individu ? $data_individu->kelurahan : '' }}"

        if (kota) {
            $.getJSON("/borrower/data_kota/" + provinsi + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kota').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                for (let i = 0; i < data.length; i++) {
                    $('#txt_kota_badan_hukum').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                $("#kota option[value='" + kota + "']").attr('selected', 'selected');
            });
        }
        if (kecamatan) {
            $.getJSON("/borrower/data_kecamatan/" + kota + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kecamatan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                for (let i = 0; i < data.length; i++) {
                    $('#txt_kecamatan_badan_hukum').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                $("#kecamatan option[value='" + kecamatan + "']").attr('selected', 'selected');
            });
        }
        if (kelurahan) {
            $.getJSON("/borrower/data_kelurahan/" + kecamatan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kelurahan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                for (let i = 0; i < data.length; i++) {
                    $('#txt_kelurahan_badan_hukum').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kelurahan option[value='" + kelurahan + "']").attr('selected', 'selected');
            });
        }

        $("#txt_provinsi_badan_hukum").prepend('<option value="'+ provinsi +'" selected>'+provinsi+'</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Provinsi",
            width: '100%'
        });
        $('#txt_provinsi_badan_hukum').valid()

        $("#txt_kota_badan_hukum").prepend('<option value="' + kota + '" selected>' + kota + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kota",
            width: '100%'
        });
        $('#txt_kota_badan_hukum').valid()

        $("#txt_kecamatan_badan_hukum").prepend('<option value="' + kecamatan + '" selected>' + kecamatan + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kecamatan",
            width: '100%'
        });
        $('#txt_kecamatan_badan_hukum').valid()

        $("#txt_kelurahan_badan_hukum").prepend('<option value="' + kelurahan + '" selected>' + kelurahan + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kelurahan",
            width: '100%'
        });
        $('#txt_kelurahan_badan_hukum').valid()


        let provinsi_domisili = "{{ $data_individu ? $data_individu->domisili_provinsi : '' }}"
        let kota_domisili = "{{ $data_individu ? $data_individu->domisili_kota : '' }}"
        let kecamatan_domisili = "{{ $data_individu ? $data_individu->domisili_kecamatan : '' }}"
        let kelurahan_domisili = "{{ $data_individu ? $data_individu->domisili_kelurahan : '' }}"

        if (provinsi == provinsi_domisili && kota == kota_domisili && kecamatan == kecamatan_domisili && kelurahan == kelurahan_domisili && provinsi != '' && kota != '' && kecamatan != '' && kelurahan != '') {
            $('#domisili_status').prop('checked', true);
            $('#domisili_badan_hukum').trigger('click');

            $("#txt_provinsi_domisili_badan_hukum").prepend('<option value="' + provinsi_domisili + '" selected>' + provinsi_domisili + '</option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Provinsi",
                width: '100%'
            });

            $("#txt_kota_domisili_badan_hukum").prepend('<option value="' + kota_domisili + '" selected>' + kota_domisili + '</option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Kota",
                width: '100%'
            });

            $("#txt_kecamatan_domisili_badan_hukum").prepend('<option value="' + kecamatan_domisili + '" selected>' + kecamatan_domisili + '</option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Kecamatan",
                width: '100%'
            });

            $("#txt_kelurahan_domisili_badan_hukum").prepend('<option value="' + kelurahan_domisili + '" selected>' + kelurahan_domisili + '</option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Kelurahan",
                width: '100%'
            });

            $('#txt_alamat_domisili_badan_hukum').attr('disabled', true).valid()
            $('#txt_provinsi_domisili_badan_hukum').attr('disabled', true).valid()
            $('#txt_kota_domisili_badan_hukum').attr('disabled', true).valid()
            $('#txt_kecamatan_domisili_badan_hukum').attr('disabled', true).valid()
            $('#txt_kelurahan_domisili_badan_hukum').attr('disabled', true).valid()

            // $('#domisili_status').trigger("change")
            $("#layout-alamat-domisili").find('input, select, textarea, radio').prop("disabled", true);
        }
        if (kota_domisili) {
            $.getJSON("/borrower/data_kota/" + provinsi_domisili + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kota_domisili').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                $("#kota_domisili option[value='" + kota_domisili + "']").attr('selected', 'selected');
            });
        }
        if (kecamatan_domisili) {
            $.getJSON("/borrower/data_kecamatan/" + kota_domisili + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kecamatan_domisili').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                $("#kecamatan_domisili option[value='" + kecamatan_domisili + "']").attr('selected', 'selected');
            });
        }
        if (kelurahan_domisili) {
            $.getJSON("/borrower/data_kelurahan/" + kecamatan_domisili + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kelurahan_domisili').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }

                $("#kelurahan_domisili option[value='" + kelurahan_domisili + "']").attr('selected', 'selected');
            });
        }

        let provinsi_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->provinsi : '' }}"
        let kota_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->kab_kota : '' }}"
        let kecamatan_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->kecamatan : '' }}"
        let kelurahan_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->kelurahan : '' }}"

        if (kota_perusahaan) {
            $.getJSON("/borrower/data_kota/" + provinsi_perusahaan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kota_perusahaan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kota_perusahaan option[value='" + kota_perusahaan + "']").attr('selected', 'selected');
            });
        }
        if (kecamatan_perusahaan) {
            $.getJSON("/borrower/data_kecamatan/" + kota_perusahaan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kecamatan_perusahaan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kecamatan_perusahaan option[value='" + kecamatan_perusahaan + "']").attr('selected', 'selected');
            });
        }
        if (kelurahan_perusahaan) {
            $.getJSON("/borrower/data_kelurahan/" + kecamatan_perusahaan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kelurahan_perusahaan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kelurahan_perusahaan option[value='" + kelurahan_perusahaan + "']").attr('selected', 'selected');
            });
        }

        let provinsi_pasangan = "{{ $data_pasangan ? $data_pasangan->provinsi : '' }}"
        let kota_pasangan = "{{ $data_pasangan ? $data_pasangan->kota : '' }}"
        let kecamatan_pasangan = "{{ $data_pasangan ? $data_pasangan->kecamatan : '' }}"
        let kelurahan_pasangan = "{{ $data_pasangan ? $data_pasangan->kelurahan : '' }}"
        if (provinsi == provinsi_pasangan && kota == kota_pasangan && kecamatan == kecamatan_pasangan && kelurahan ==
            kelurahan_pasangan && provinsi != '' && kota != '' && kecamatan != '' && kelurahan != '') {
            $('#domisili_pasangan_status').prop('checked', true)
            $('#domisili_pasangan_status').trigger("change")
        }
        if (kota_pasangan) {
            $.getJSON("/borrower/data_kota/" + provinsi_pasangan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kota_pasangan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kota_pasangan option[value='" + kota_pasangan + "']").attr('selected', 'selected');
            });
        }
        if (kecamatan_pasangan) {
            $.getJSON("/borrower/data_kecamatan/" + kota_pasangan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kecamatan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kecamatan_pasangan option[value='" + kecamatan_pasangan + "']").attr('selected', 'selected');
            });
        }
        if (kelurahan_pasangan) {
            $.getJSON("/borrower/data_kelurahan/" + kecamatan_pasangan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kelurahan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kelurahan_pasangan option[value='" + kelurahan_pasangan + "']").attr('selected', 'selected');
            });
        }
        let provinsi_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->provinsi : '' }}"
        let kota_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->kota : '' }}"
        let kecamatan_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->kecamatan : '' }}"
        let kelurahan_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->kelurahan : '' }}"

        if (kota_perusahaan_pasangan) {
            $.getJSON("/borrower/data_kota/" + provinsi_perusahaan_pasangan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kota_perusahaan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kota_perusahaan_pasangan option[value='" + kota_perusahaan_pasangan + "']").attr('selected', 'selected');
            });
        }
        if (kecamatan_perusahaan_pasangan) {
            $.getJSON("/borrower/data_kecamatan/" + kota_perusahaan_pasangan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kecamatan_perusahaan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kecamatan_perusahaan_pasangan option[value='" + kecamatan_perusahaan_pasangan + "']").attr('selected', 'selected');
            });
        }
        if (kelurahan_perusahaan_pasangan) {
            $.getJSON("/borrower/data_kelurahan/" + kecamatan_perusahaan_pasangan + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#kelurahan_perusahaan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
                $("#kelurahan_perusahaan_pasangan option[value='" + kelurahan_perusahaan_pasangan + "']").attr('selected', 'selected');
            });
        }

        // alamat pengurus 1
        let txt_provinsi_pengurus = "{{ $data_pengurus_1 ? $data_pengurus_1->provinsi : '' }}"
        let txt_kota_pengurus = "{{ $data_pengurus_1 ? $data_pengurus_1->kota : '' }}"
        let txt_kecamatan_pengurus = "{{ $data_pengurus_1 ? $data_pengurus_1->kecamatan : '' }}"
        let txt_kelurahan_pengurus = "{{ $data_pengurus_1 ? $data_pengurus_1->kelurahan : '' }}"

        $("#txt_provinsi_pengurus").prepend('<option value="' + txt_provinsi_pengurus + '" selected>' + txt_provinsi_pengurus + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Provinsi",
            width: '100%'
        });
        $('#txt_provinsi_pengurus').valid()

        if (txt_kota_pengurus) {
            $.getJSON("/borrower/data_kota/" + txt_provinsi_pengurus + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#txt_kota_pengurus').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
            });
        }
        $("#txt_kota_pengurus").prepend('<option value="' + txt_kota_pengurus + '" selected>' + txt_kota_pengurus + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kota",
            width: '100%'
        });
        $('#txt_kota_pengurus').valid()

        if (txt_kecamatan_pengurus) {
            $.getJSON("/borrower/data_kecamatan/" + txt_kota_pengurus + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#txt_kecamatan_pengurus').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
            });
        }
        $("#txt_kecamatan_pengurus").prepend('<option value="' + txt_kecamatan_pengurus + '" selected>' + txt_kecamatan_pengurus + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kecamatan",
            width: '100%'
        });
        $('#txt_kecamatan_pengurus').valid()

        if (txt_kelurahan_pengurus) {
            $.getJSON("/borrower/data_kelurahan/" + txt_kecamatan_pengurus + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#txt_kelurahan_pengurus').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
            });
        }
        $("#txt_kelurahan_pengurus").prepend('<option value="' + txt_kelurahan_pengurus + '" selected>' + txt_kelurahan_pengurus + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kelurahan",
            width: '100%'
        });
        $('#txt_kelurahan_pengurus').valid()

        // alamat pengurus 2
        let txt_provinsi_pengurus_2 = "{{ $data_pengurus_2 ? $data_pengurus_2->provinsi : '' }}"
        let txt_kota_pengurus_2 = "{{ $data_pengurus_2 ? $data_pengurus_2->kota : '' }}"
        let txt_kecamatan_pengurus_2 = "{{ $data_pengurus_2 ? $data_pengurus_2->kecamatan : '' }}"
        let txt_kelurahan_pengurus_2 = "{{ $data_pengurus_2 ? $data_pengurus_2->kelurahan : '' }}"

        $("#txt_provinsi_pengurus_2").prepend('<option value="' + txt_provinsi_pengurus_2 + '" selected>' + txt_provinsi_pengurus_2 + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Provinsi",
            width: '100%'
        });
        $('#txt_provinsi_pengurus_2').valid()

        if (txt_kota_pengurus_2) {
            $.getJSON("/borrower/data_kota/" + txt_provinsi_pengurus_2 + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#txt_kota_pengurus_2').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
            });
        }
        $("#txt_kota_pengurus_2").prepend('<option value="' + txt_kota_pengurus_2 + '" selected>' + txt_kota_pengurus_2 + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kota",
            width: '100%'
        });
        $('#txt_kota_pengurus_2').valid()

        if (txt_kecamatan_pengurus_2) {
            $.getJSON("/borrower/data_kecamatan/" + txt_kota_pengurus_2 + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#txt_kecamatan_pengurus_2').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
            });
        }
        $("#txt_kecamatan_pengurus_2").prepend('<option value="' + txt_kecamatan_pengurus_2 + '" selected>' + txt_kecamatan_pengurus_2 + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kecamatan",
            width: '100%'
        });
        $('#txt_kecamatan_pengurus_2').valid()

        if (txt_kelurahan_pengurus_2) {
            $.getJSON("/borrower/data_kelurahan/" + txt_kecamatan_pengurus_2 + "/", function(data) {
                for (let i = 0; i < data.length; i++) {
                    $('#txt_kelurahan_pengurus_2').append($('<option>', {
                        value: data[i].text,
                        text: data[i].text
                    }));
                }
            });
        }
        $("#txt_kelurahan_pengurus_2").prepend('<option value="' + txt_kelurahan_pengurus_2 + '" selected>' + txt_kelurahan_pengurus_2 + '</option>').select2({
            theme: "bootstrap",
            placeholder: "Pilih Kelurahan",
            width: '100%'
        });
        $('#txt_kelurahan_pengurus_2').valid()




        // END: History Alamat

        // START: History Pekerjaan
        // Status Kepemilikan Rumah
        let status_kepemilikan_rumah = "{{ $data_individu ? $data_individu->status_rumah : ''}}"
        $('input:radio[name="status_kepemilikan_rumah"][value="' + status_kepemilikan_rumah + '"]').prop('checked', true);

        let domisili_status_kepemilikan_rumah = "{{ $data_individu ? $data_individu->domisili_status_rumah : ''}}"
        $('input:radio[name="domisili_status_rumah"][value="' + domisili_status_kepemilikan_rumah + '"]').prop('checked', true);

        // Pendidikan Terakhir
        let pendidikan_terakhir = "{{ $data_individu ? $data_individu->agama : ''}}"
        $("#pendidikan_terakhir option[value='" + pendidikan_terakhir + "']").prop('selected', true);

        // Sumber Penghasilan Dana
        let sumber_penghasilan_dana = "{{ $data_penghasilan ? $data_penghasilan->sumber_pengembalian_dana : ''}}"
        if (sumber_penghasilan_dana) {
            $("#sumber_penghasilan_dana option[value='" + sumber_penghasilan_dana + "']").prop('selected', true);
            $("#sumber_penghasilan_dana").trigger('change')
        }

        let sumber_penghasilan_dana_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->sumber_pengembalian_dana : ''}}"
        if (sumber_penghasilan_dana_pasangan) {
            $("#sumber_penghasilan_dana_pasangan option[value='" + sumber_penghasilan_dana_pasangan + "']").prop('selected', true);
            $("#sumber_penghasilan_dana_pasangan").trigger('change')
        }

        // Skema Pembiayaan
        let skema_pembiayaan = "{{ $data_penghasilan ? $data_penghasilan->skema_pembiayaan : ''}}"
        $("#skema_pembiayaan option[value='" + skema_pembiayaan + "']").prop('selected', true);
        $("#skema_pembiayaan").trigger('change')

        // Status Kepegawaiaan
        let status_kepegawaian = "{{ $data_penghasilan ? $data_penghasilan->status_pekerjaan : ''}}"
        $("#status_kepegawaian option[value='" + status_kepegawaian + "']").attr("selected", "selected");

        let status_kepegawaian_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->status_pekerjaan : ''}}"
        $("#status_kepegawaian_pasangan option[value='" + status_kepegawaian_pasangan + "']").attr("selected", "selected");

        // Surat Ijin
        let surat_ijin = "{{ $data_penghasilan ? $data_penghasilan->surat_ijin : ''}}"
        if (surat_ijin) {
            $("#surat_ijin_usaha option[value='" + surat_ijin + "']").attr("selected", "selected");
            $("#surat_ijin_usaha").trigger('change')
        }

        let surat_ijin_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->surat_ijin : ''}}"
        if (surat_ijin_pasangan) {
            $("#surat_ijin_usaha_pasangan option[value='" + surat_ijin_pasangan + "']").attr("selected", "selected");
            $("#surat_ijin_usaha_pasangan").trigger('change')
        }

        // penghasilan lain lain
        let penghasilan_lain_lain = "{{ $data_penghasilan ? $data_penghasilan->detail_penghasilan_lain_lain : ''}}"
        if (penghasilan_lain_lain) {
            $('#penghasilan_lain_lain').prop('checked', true)
            $('#penghasilan_lain_lain').trigger("change")
        }

        let penghasilan_lain_lain_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->detail_penghasilan_lain_lain : ''}}"
        if (penghasilan_lain_lain_pasangan) {
            $('#penghasilan_lain_lain_pasangan').prop('checked', true)
            $('#penghasilan_lain_lain_pasangan').trigger("change")
        }
        // END: History Pekerjaan
        // END: Individu
    })
</script>
{{-- END: Dom Manipulation --}}

{{-- START: Other Function --}}
<script>
    // Reset Timeout
    const resetTimeout = () => {
        clearTimeout(timer);
    }

    // Rupiah Currency Format
    function formatRupiah(angka, prefix) {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    let onFinishCallback = () => {
        var hop = $('#txt_harga_objek_pendanaan').val();
        var valHop = parseInt(hop.replaceAll('.', ''));
        if ($("#form-individu").valid()) {
            if (valHop > 100000000 && valHop < 2000000000) {
                if (is_nik_unique == 1) {
                    if (is_phone_number_unique == 1) {
                        if (is_npwp_unique == 1) {
                            let status_kawin = ($("input[name='status_kawin']:checked").val() == 1) ? "Sudah Menikah" : ($("input[name='status_kawin']:checked").val() == 2) ? "Belum Menikah" : "Duda/Janda";
                            let status_kepemilikan_rumah = ($("input[name='status_kepemilikan_rumah']:checked").val() == 1) ? "Milik Pribadi" : ($("input[name='status_kepemilikan_rumah']:checked").val() == 2) ? "Sewa" : "Milik Keluarga";
                            let jenis_property = ($("#jenis_property").val() == 1) ? "Baru" : "Bekas";
                            let jangka_waktu = ($("#type_tujuan_pendanaan option:selected").val() == 1) ? $("#txt_jangka_waktu").val() + " Bulan" : $("#txt_jangka_waktu").val() / 12 + " Tahun";
                            let jangka_waktu_kons = $("#txt_jangka_waktu").val() + " Bulan";
                            var getEstimasi = new Date($('#txt_estimasi_proyek').val()).toString();
                            let estimasi = getEstimasi.substr(8, 2) + ' ' + getEstimasi.substr(4, 3) + ' ' + getEstimasi.substr(11, 4);

                            let infoPendanaan = "";
                            let lamaUsaha = "";
                            if ($("#pekerjaan option:selected").val() == '5') {
                                lamaUsaha = "<tr><td>Lama Usaha/Praktek</td><td> :</td><td> " + $("#usia_perusahaan").val() + " tahun </td></tr>";
                            } else {
                                lamaUsaha = "<tr><td>Lama Bekerja</td><td> :</td><td> " + $("#tahun_bekerja").val() + " tahun " + $("#bulan_bekerja").val() + " bulan " + "</td></tr>";
                            }

                            if ($("#type_pendanaan_select option:selected").val() == 2) {
                                infoPendanaan = "<tr><td width=45%>Tujuan Pendanaan</td><td width=5%>:</td><td width=50%> " + $("#type_tujuan_pendanaan option:selected").text() + "</td></tr>" +
                                    "<tr><td>Jenis Rumah</td><td> :</td><td>" + jenis_property + "</td></tr>" +
                                    "<tr><td>Harga Rumah</td><td> :</td><td> Rp" + $("#txt_harga_objek_pendanaan").val() + "</td></tr>" +
                                    "<tr><td>Uang Muka</td><td> :</td><td> Rp" + $("#txt_uang_muka").val() + " (" + $("#txt_persen").val() + " %)</td></tr>" +
                                    "<tr><td>Nilai Pengajuan Pendanaan</td><td> :</td><td> Rp" + $("#txt_nilai_pengajuan").val() + "</td></tr>" +
                                    "<tr><td>Jangka Waktu</td><td> :</td><td> " + jangka_waktu + " </td></tr>";
                            } else if ($("#type_pendanaan_select option:selected").val() == 1) {
                                infoPendanaan = "<tr><td>Harga</td><td> :</td><td> Rp" + $("#txt_harga_objek_pendanaan").val() + "</td></tr>" +
                                    "<tr><td>Nilai Pengajuan Pendanaan</td><td> :</td><td> Rp" + $("#txt_nilai_pengajuan").val() + "</td></tr>" +
                                    "<tr><td>Jangka Waktu</td><td> :</td><td> " + jangka_waktu_kons + " </td></tr>" +
                                    "<tr><td>Tanggal Estimasi Mulai</td><td> :</td><td> " + estimasi + " </td></tr>";
                            }

                            let confirm_text = "Pastikan data yang Anda isi sudah benar karena data pengajuan yang sudah terkirim tidak bisa diubah dan dihapus dari sistem !" +
                                "<br /><br /><b>INFORMASI DATA DIRI</b> <br />" +
                                "<table align='center' width=100% border='0'><tr><td width=45%>Nama Lengkap</td><td width=5%>:</td><td width=50%>" + $("#nama_individu").val() + "</td></tr>" +
                                "<tr><td>NIK</td><td> :</td><td> " + $("#ktp").val() + "</td></tr>" +
                                "<tr><td>NPWP</td><td> :</td><td> " + $("#npwp").val() + "</td></tr>" +
                                "<tr><td>Agama</td><td> :</td><td> " + $("#agama option:selected").text() + "</td></tr>" +
                                "<tr><td>Status Perkawinan</td><td> :</td><td> " + status_kawin + "</td></tr>" +
                                "<tr><td>Pendidikan Terakhir</td><td> :</td><td> " + $("#pendidikan_terakhir option:selected").text() + "</td></tr>" +
                                "<tr><td>Tempat Lahir</td><td> :</td><td>" + $("#tempat_lahir").val() + "</td></tr>" +
                                "<tr><td>No. Telepon Selular</td><td> :</td><td> +62" + $("#telepon").val() + "</td></tr>" +
                                "<tr><td>Alamat</td><td> :</td><td> " + $("#alamat").val() + "</td></tr>" +
                                "<tr><td>Kelurahan</td><td> :</td><td> " + $("#kelurahan").val() + "</td></tr>" +
                                "<tr><td>Kecamatan</td><td> :</td><td> " + $("#kecamatan").val() + "</td></tr>" +
                                "<tr><td>Kabupaten/Kota</td><td> :</td><td> " + $("#kota").val() + "</td></tr>" +
                                "<tr><td>Propinsi</td><td> :</td><td> " + $("#provinsi").val() + "</td></tr>" +
                                "<tr><td>Kode Pos</td><td> :</td><td> " + $("#kode_pos").val() + "</td></tr>" +
                                "<tr><td>Status Kepemilikan Rumah</td><td> :</td><td> " + status_kepemilikan_rumah + "</td></tr>" +
                                "<tr><td>Pekerjaan</td><td> :</td><td> " + $("#pekerjaan option:selected").text() + "</td></tr>" +
                                "<tr><td>Bidang Pekerjaan</td><td> :</td><td> " + $("#bidang_pekerjaan option:selected").text() + "</td></tr>" +
                                "<tr><td>Bidang Pekerjaan Online</td><td> :</td><td> " + $("#bidang_online option:selected").text() + "</td></tr>" +
                                "<tr><td>Nama Perusahaan</td><td> :</td><td> " + $("#nama_perusahaan").val() + "</td></tr>" +
                                // "<tr><td>Bentuk Badan Usaha</td><td> :</td><td> "+$("#badan_usaha").val() +"</td></tr>"+
                                lamaUsaha +
                                "<tr><td>Nomor Telepon Perusahaan</td><td> :</td><td>" + $("#no_telpon_usaha").val() + "</td></tr>" +
                                "<tr><td>Penghasilan (Per Bulan)</td><td> :</td><td> Rp" + $("#penghasilan").val() + "</td></tr></table>" +
                                "<br /><br /><b>INFORMASI PENDANAAN</b> <br />" +
                                "<table align='center' width=100% border='0'>" +
                                "<tr><td width=45%>Tipe Pendanaan</td><td width=5%>:</td><td width=50%> " + $("#type_pendanaan_select option:selected").text() + "</td></tr>" + infoPendanaan +
                                "</table>";

                            $("#confirm-data").html(confirm_text);



                            $("#modal_action_lengkapi_profile_pendanaan_simple").modal('show');
                        } else {
                            $('#npwp').focus()
                            swal.fire({
                                title: "Warning",
                                type: "warning",
                                text: "Pastikan NPWP Terisi dan belum pernah digunakan",
                                allowOutsideClick: true,
                                confirmButtonClass: "btn-warning",
                            });
                            //    return false
                        }
                    } else {
                        $('#telepon').focus()
                        swal.fire({
                            title: "Warning",
                            type: "warning",
                            text: "Pastikan Telepon Selular Terisi dan belum pernah digunakan",
                            allowOutsideClick: true,
                            confirmButtonClass: "btn-warning",
                        });
                        // return false
                    }
                } else {
                    $('#ktp').focus();
                    swal.fire({
                        title: "Warning",
                        type: "warning",
                        text: "Pastikan KTP Terisi dan belum pernah digunakan",
                        allowOutsideClick: true,
                        confirmButtonClass: "btn-warning",
                    });
                    // return false
                }
            } else {
                $('#txt_harga_objek_pendanaan').focus();
                swal.fire({
                    title: "Warning",
                    type: "warning",
                    text: "Kebutuhan Dana Harus Lebih Dari Rp100.000.000 dan Tidak Boleh Lebih dari Rp2.000.000.000",
                    allowOutsideClick: true,
                    confirmButtonClass: "btn-warning",
                });
            }
        }
    }

    let sendOtp = () => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#kirim_lagi').prop('disabled', true);

        var tipeBrw = $('#borrower_type').val()
        let no_hp_individu = $('#telepon').val()
        if (tipeBrw == '1') {
            $('#set_no_hp').text('Silakan masukkan kode OTP yang telah dikirim ke nomor ' + '+62' + no_hp_individu);
            $('#no_hp').val('62' + no_hp_individu);
            console.log($('#telepon').val())
        }

        noHP = '62' + no_hp_individu;
        console.log(no_hp_individu)

        $.ajax({
            url: "/borrower/kirimOTP/",
            method: "post",
            dataType: 'JSON',
            data: {
                hp: noHP
            },
            success: function(data) {
                console.log(data.status)
            }
        });
        timerDisableButton();
    }

    let timerDisableButton = () => {
        var spn = document.getElementById("count");
        var btn = document.getElementById("kirim_lagi");

        var count = 30; // Set count

        (function countDown() {
            // Display counter and start counting down
            spn.textContent = count;

            // Run the function again every second if the count is not zero
            if (count !== 0) {
                timer = setTimeout(countDown, 1000);
                count--; // decrease the timer
            } else {
                // Enable the button
                btn.removeAttribute("disabled");
                spn.removeText
            }
        }());
    }
</script>
{{-- END: Other Function --}}

<!----------------------------------------------- Badan Hukum ---------------------------------------------->
<script language="JavaScript">
    $(document).ready(function() {
        $("#webcam_npwp_badan_hukum").hide();
        $("#take_camera_npwp_badan_hukum").click(function() {
            Webcam.attach('#camera_npwp_badan_hukum');

            $("#webcam_npwp_badan_hukum").fadeIn();
            $("#take_camera_npwp_badan_hukum").hide();
            $("#results_npwp_badan_hukum").show();
            // $("#webcam_npwp_badan_hukum").css({
            //     "position": "-100px;"
            // });
        });
    });

    $(document).ready(function() {
        $("#webcam_pengurus").hide();
        $("#take_camera_pengurus").click(function() {
            Webcam.attach('#camera_pengurus');

            $("#webcam_pengurus").fadeIn();
            $("#take_camera_pengurus").hide();
            $("#results_foto_pengurus").show();
            $("#webcam_pengurus").css({
                "position": "-100px;"
            });
        });
    });

    $(document).ready(function() {
        $("#webcam_foto_ktp_pengurus").hide();
        $("#take_camera_foto_ktp_pengurus").click(function() {
            Webcam.attach('#camera_ktp_pengurus');

            $("#webcam_foto_ktp_pengurus").fadeIn();
            $("#take_camera_foto_ktp_pengurus").hide();
            $("#results_foto_ktp_pengurus").show();
            $("#webcam_foto_ktp_pengurus").css({
                "position": "-100px;"
            });
        });
    });

    $(document).ready(function() {
        $("#webcam_foto_ktpdiri_pengurus").hide();
        $("#take_camera_foto_ktpdiri_pengurus").click(function() {
            Webcam.attach('#camera_ktpdiri_pengurus');

            $("#webcam_foto_ktpdiri_pengurus").fadeIn();
            $("#take_camera_foto_ktpdiri_pengurus").hide();
            $("#results_foto_ktp_diri_pengurus").show();
            $("#webcam_foto_ktpdiri_pengurus").css({
                "position": "-100px;"
            });
        });
    });

    $(document).ready(function() {
        $("#webcam_foto_npwp_pengurus").hide();
        $("#take_camera_foto_npwp_pengurus").click(function() {
            Webcam.attach('#camera_npwp_pengurus');

            $("#webcam_foto_npwp_pengurus").fadeIn();
            $("#take_camera_foto_npwp_pengurus").hide();
            $("#results_foto_npwp_pengurus").show();
            $("#webcam_foto_npwp_pengurus").css({
                "position": "-100px;"
            });
        });
    });

    // PENGURUS 2
    $(document).ready(function() {
        $("#webcam_pengurus_2").hide();
        $("#take_camera_pengurus_2").click(function() {
            Webcam.attach('#camera_pengurus_2');

            $("#webcam_pengurus_2").fadeIn();
            $("#take_camera_pengurus_2").hide();
            $("#results_foto_pengurus_2").show();
            $("#webcam_pengurus_2").css({
                "position": "-100px;"
            });
        });
    });

    $(document).ready(function() {
        $("#webcam_foto_ktp_pengurus_2").hide();
        $("#take_camera_foto_ktp_pengurus_2").click(function() {
            Webcam.attach('#camera_ktp_pengurus_2');

            $("#webcam_foto_ktp_pengurus_2").fadeIn();
            $("#take_camera_foto_ktp_pengurus_2").hide();
            $("#results_foto_ktp_pengurus_2").show();
            $("#webcam_foto_ktp_pengurus_2").css({
                "position": "-100px;"
            });
        });
    });

    $(document).ready(function() {
        $("#webcam_foto_ktpdiri_pengurus_2").hide();
        $("#take_camera_foto_ktpdiri_pengurus_2").click(function() {
            Webcam.attach('#camera_ktpdiri_pengurus_2');

            $("#webcam_foto_ktpdiri_pengurus_2").fadeIn();
            $("#take_camera_foto_ktpdiri_pengurus_2").hide();
            $("#results_foto_ktp_diri_pengurus_2").show();
            $("#webcam_foto_ktpdiri_pengurus_2").css({
                "position": "-100px;"
            });
        });
    });

    $(document).ready(function() {
        $("#webcam_foto_npwp_pengurus_2").hide();
        $("#take_camera_foto_npwp_pengurus_2").click(function() {
            Webcam.attach('#camera_npwp_pengurus_2');

            $("#webcam_foto_npwp_pengurus_2").fadeIn();
            $("#take_camera_foto_npwp_pengurus_2").hide();
            $("#results_foto_npwp_pengurus_2").show();
            $("#webcam_foto_npwp_pengurus_2").css({
                "position": "-100px;"
            });
        });
    });
</script>

<!--------------------------------------------- Foto Badan Hukum ------------------------------------------->
<script language="JavaScript">
    function take_snapshot_npwp_badan_hukum() {

        Webcam.snap(function(data_uri) {
            document.getElementById('image_npwp_badan_hukum').value = data_uri;
            document.getElementById('results_npwp_badan_hukum').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_npwp_bdn_hukum",
                method: "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto NPWP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_npwp_badan_hukum').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_pengurus() {
        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_pengurus').value = data_uri;
            document.getElementById('results_foto_pengurus').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_foto_pengurus_1",
                method: "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    // console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_pengurus').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktp_pengurus() {

        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_ktp_pengurus').value = data_uri;
            document.getElementById('results_foto_ktp_pengurus').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_foto_ktp_pengurus_1",
                method: "POST",
                dataType: "JSON",
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_ktp_pengurus').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktpdiri_pengurus() {

        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_ktp_diri_pengurus').value = data_uri;
            document.getElementById('results_foto_ktp_diri_pengurus').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_foto_ktp_diri_pengurus_1",
                method: "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto Diri Dengan KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_brw_dengan_ktp_pengurus').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_npwp_pengurus() {

        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_npwp_pengurus').value = data_uri;
            document.getElementById('results_foto_npwp_pengurus').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_npwp_pengurus_1",
                method: "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto NPWP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_npwp_pengurus').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<!------------------------------------------------ Pengurus 2 --------------------------------------------->
<script language="JavaScript">
    function take_snapshot_foto_pengurus_2() {
        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_pengurus_2').value = data_uri;
            document.getElementById('results_foto_pengurus_2').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_foto_pengurus_2",
                method: "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    // console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_pengurus_2').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktp_pengurus_2() {

        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_ktp_pengurus_2').value = data_uri;
            document.getElementById('results_foto_ktp_pengurus_2').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_foto_ktp_pengurus_2",
                method: "POST",
                dataType: "JSON",
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_ktp_pengurus_2').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktpdiri_pengurus_2() {

        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_ktp_diri_pengurus_2').value = data_uri;
            document.getElementById('results_foto_ktp_diri_pengurus_2').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_foto_ktp_diri_pengurus_2",
                method: "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto Diri Dengan KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_brw_dengan_ktp_pengurus_2').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_npwp_pengurus_2() {

        Webcam.snap(function(data_uri) {
            document.getElementById('image_foto_npwp_pengurus_2').value = data_uri;
            document.getElementById('results_foto_npwp_pengurus_2').innerHTML = '<label class="font-weight-bold" style="margin-left:0">Hasil</label><br><img class="img-thumbnail" src="' + data_uri + '" style="width: 180px; height: 138px; top: 32px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url: "/borrower/webcam_npwp_pengurus_2",
                method: "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        swal.fire({
                            title: 'Unggah Foto NPWP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type: "success",
                            showConfirmButton: true
                        });
                        $('#url_pic_npwp_pengurus_2').val(data.url);
                    } else {
                        alert(data.failed);
                    }

                }
            });
        });
    }
</script>

<!------------------------------------------ valisasi Badan Hukum ------------------------------------------>
<script language="JavaScript">
    var toNIKPengurus = false;

    function CheckNIKPengurus() {
        var nik = $(".checkNIKPengurus").val();
        //var regEx = /^[09]/;

        $("#divNoKTPPengurus").removeClass("has-error has-success is-invalid");
        $("#divNoKTPPengurus label").remove();
        $("#divNoKTPPengurus").prepend('<label class="control-label" for="idPengguna"><i class="fa fa-spin fa-spinner"></i></label>');

        if ($(".checkNIKPengurus").val().length == 16) {

            if (toNIKPengurus) {
                clearTimeout(toNIKPengurus);
            }
            toNIKPengurus = setTimeout(function() {
                $.get("/borrower/check_nik/" + nik, function(data) {
                    $("#divNoKTPPengurus label").remove();
                    if (data.status == "ada") {
                        $("#divNoKTPPengurus").addClass("has-error is-invalid");
                        $("#divNoKTPPengurus").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NIK Sudah Terdaftar</label>');
                    } else {

                        $("#divNoKTPPengurus").removeClass("has-error is-invalid");
                        $("#divNoKTPPengurus").addClass("has-success is-valid");
                        $("#divNoKTPPengurus").prepend('<label for="txt_no_ktp_pribadi"><i class="fa fa-check"></i> NIK Belum Terdaftarkan</label>');
                    }
                });

            }, 100);

        } else {
            $("#divNoKTPPengurus label").remove();
            $("#divNoKTPPengurus").addClass("has-error is-invalid");
            $("#divNoKTPPengurus").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NIK Harus 16 Digit</label>');

        }
    }


    // check npwp pribadi n perusahaan
    var toNPWP = false;

    function CheckNPWP() {
        var npwp = $("#txt_npwp_pribadi").val();
        var type_borrower = $("#label_tipe_borrower").text();
        var type = "";
        if (type_borrower == "Individu") {
            type = 1
        } else {
            type = 2
        }

        $("#divNPWP").removeClass("has-error has-success is-invalid");
        $("#divNPWP label").remove();
        $("#divNPWP").prepend('<label class="control-label" for="idPengguna"><i class="fa fa-spin fa-spinner"></i></label>');

        if ($("#txt_npwp_pribadi").val().length == 15) {

            if (toNPWP) {
                clearTimeout(toNPWP);
            }
            toNPWP = setTimeout(function() {
                $.get("/borrower/check_npwp/" + npwp + '/' + type, function(data) {
                    $("#divNPWP label").remove();
                    if (data.status == "ada") {
                        $("#divNPWP").addClass("has-error is-invalid");
                        $("#divNPWP").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NPWP Sudah Terdaftar</label>');
                    } else {
                        $("#divNPWP").removeClass("has-error is-invalid");
                        $("#divNPWP").addClass("has-success is-valid");
                        $("#divNPWP").prepend('<label for="txt_no_ktp_pribadi"><i class="fa fa-check"></i> NPWP Belum Terdaftarkan</label>');
                    }
                });

            }, 100);

        } else {
            $("#divNPWP label").remove();
            $("#divNPWP").addClass("has-error is-invalid");
            $("#divNPWP").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NPWP Harus 15 Digit</label>');

        }
    }

    var checkNOHPPengurus = false;

    function check_hp_pengurus() {

        var notlp = $(".checkNOHPPengurus").val();
        if (notlp.indexOf('0') == 0) {

            $('.checkNOHPPengurus').val('');
        }
        var regex = new RegExp(/^[1-9]*$/);
        $("#div_hp_pengurus").removeClass("has-error has-success is-invalid");
        $("#div_hp_pengurus label").remove();
        $("#div_hp_pengurus").prepend('<label class="control-label" for="txt_notlp_pribadi"><i class="fa fa-spin fa-spinner"></i></label>');
        //if ($("#txt_notlp_pribadi").val().length == 13 && ) {


        if (checkNOHPPengurus) {
            clearTimeout(checkNOHPPengurus);
        }
        checkNOHPPengurus = setTimeout(function() {

            $.get("/borrower/check_no_tlp/" + notlp, function(data) {
                //$.get("http://core.danasyariah.id/borrower/check_no_tlp/"+notlp, function( data ) {
                $("#div_hp_pengurus label").remove();
                //var obj = jQuery.parseJSON( data );

                if (data.status == "ada") {
                    $("#div_hp_pengurus").addClass("has-error is-invalid");
                    $("#div_hp_pengurus").prepend('<label style="color:red;" for="txt_notlp_pribadi"><i class="fa fa-times-circle-o"></i> No HP Sudah Terdaftar</label>');
                } else {
                    $("#div_hp_pengurus").removeClass("has-error is-invalid");
                    $("#div_hp_pengurus").addClass("has-success is-valid");
                    $("#div_hp_pengurus").prepend('<label for="txt_notlp_pribadi"><i class="fa fa-check"></i> No HP Belum Terdaftar</label>');
                }
            });



        }, 50);


    }

    $(document).ready(function() {

        // validasi check
        $('.checkKarakterAneh').on('input', function(event) {
            this.value = this.value.replace(/[^a-zA-Z 0-9 _.]/g, '');
        });

        $('.checkNIB').on('input', function(event) {
            this.value = this.value.replace(/[^a-zA-Z 0-9 _./-]/g, '');
        });


        $('.validasiString').on('input', function(event) {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        $('.hanyaAlfabet').on('input', function(event) {
            this.value = this.value.replace(/[^a-zA-Z ]/g, '');
        });

        // data agama
        $.getJSON("/borrower/agama", function(data_agama) {

            $('#txt_agama').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Agama",
                allowClear: true,
                data: data_agama,
                width: '100%'
            });

            let agama_1 = "{{ $data_pengurus_1 ? $data_pengurus_1->agama : '' }}"
            $('#txt_agama_pengurus_1').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Agama",
                allowClear: true,
                data: data_agama,
                width: '100%'
            });
            if (agama_1) {
                $("#txt_agama_pengurus_1").val(agama_1);
                $("#txt_agama_pengurus_1").trigger('change')
            }

            let agama_2 = "{{ $data_pengurus_2 ? $data_pengurus_2->agama : '' }}"
            $('#txt_agama_pengurus_2').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Agama",
                allowClear: true,
                data: data_agama,
                width: '100%'
            });
            if (agama_2) {
                $("#txt_agama_pengurus_2").val(agama_2);
                $("#txt_agama_pengurus_2").trigger('change')
            }

        });

        // data pendidikan
        $.getJSON("/borrower/data_pendidikan", function(data_pendidikan) {
            $('#txt_pendidikanT_pribadi').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });

            $('#txt_pendidikanT_pasangan').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });

            $('#txt_pendidikanT_ahli_waris').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });

            let txt_pendidikanT_pengurus = "{{ $data_pengurus_1 ? $data_pengurus_1->pendidikan_terakhir : '' }}"
            $('#txt_pendidikanT_pengurus').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });
            if (txt_pendidikanT_pengurus) {
                $("#txt_pendidikanT_pengurus").val(txt_pendidikanT_pengurus);
                $("#txt_pendidikanT_pengurus").trigger('change')
            }

            let txt_pendidikanT_pengurus_2 = "{{ $data_pengurus_2 ? $data_pengurus_2->pendidikan_terakhir : '' }}"
            $('#txt_pendidikanT_pengurus_2').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });
            if (txt_pendidikanT_pengurus_2) {
                $("#txt_pendidikanT_pengurus_2").val(txt_pendidikanT_pengurus_2);
                $("#txt_pendidikanT_pengurus_2").trigger('change')
            }


        });

        $.getJSON("/borrower/data_jabatan", function(data_jabatan) {
            let txt_jabatan_pengurus = "{{ $data_pengurus_1 ? $data_pengurus_1->jabatan : '' }}"
            let txt_jabatan_pengurus_2 = "{{ $data_pengurus_2 ? $data_pengurus_2->jabatan : '' }}"
            $('.txt_jabatan_pengurus').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Jabatan",
                allowClear: true,
                data: data_jabatan,
                //multiple: true,
                width: '100%'
            });
            if (txt_jabatan_pengurus) {
                $("#txt_jabatan_pengurus").val(txt_jabatan_pengurus);
                $("#txt_jabatan_pengurus").trigger('change')
            }
            if (txt_jabatan_pengurus_2) {
                $("#txt_jabatan_pengurus_2").val(txt_jabatan_pengurus_2);
                $("#txt_jabatan_pengurus_2").trigger('change')
            }
        })

        // $(`#txt_provinsi_pengurus, #txt_kota_pengurus, #txt_kecamatan_pengurus, #txt_kelurahan_pengurus,
        // #txt_provinsi_pengurus_2, #txt_kota_pengurus_2, #txt_kecamatan_pengurus_2, #txt_kelurahan_pengurus_2
        // `).prepend('<option selected></option>').select2({
        //     theme: "bootstrap",
        //     placeholder: "-- Pilih Satu --",
        //     allowClear: true,
        //     width: '100%'
        // });

        // data pekerjaan badan hukum
        $.getJSON("/borrower/data_pekerjaan_bdn_hukum", function(data_bidang_usaha) {
            for (let i = 0; i < data_bidang_usaha.length; i++) {
                $('#txt_bd_pekerjaan_badan_hukum').append($('<option>', {
                    value: data_bidang_usaha[i].id,
                    text: data_bidang_usaha[i].text
                }));
            }
            let bidang_usaha = "{{$data_individu ? $data_individu->bidang_usaha : ''}}";
            if (bidang_usaha) {
                $('#txt_bd_pekerjaan_badan_hukum').val(bidang_usaha);
                $('#txt_bd_pekerjaan_badan_hukum').select2().trigger('change');
            }
        });

        let jenisKelaminPengurus1 = "{{$data_pengurus_1 ? $data_pengurus_1->jenis_kelamin : ''}}";
        if (jenisKelaminPengurus1 == 1) {
            $('#txt_jns_kelamin_pengurus_l').prop("checked", true);
        } else if (jenisKelaminPengurus1 == 2) {
            $('#txt_jns_kelamin_pengurus_p').prop("checked", true);
        }

        let jenisKelaminPengurus2 = "{{$data_pengurus_2 ? $data_pengurus_2->jenis_kelamin : ''}}";
        if (jenisKelaminPengurus2 == 1) {
            $('#txt_jns_kelamin_pengurus_2l').prop("checked", true);
        } else if (jenisKelaminPengurus2 == 2) {
            $('#txt_jns_kelamin_pengurus_2p').prop("checked", true);
        }


        // hide & show domisili badan hukum
        $("#domisili_badan_hukum").change(function() {
            if ($('#domisili_badan_hukum').is(":checked")) {

                var alamat_ktp = $('#txt_alamat_badan_hukum').val();
                var provinsi_ktp_text = $("#txt_provinsi_badan_hukum option:selected").val();
                var kota_ktp_text = $("#txt_kota_badan_hukum option:selected").val();
                var kecamatan_ktp_text = $("#txt_kecamatan_badan_hukum option:selected").val();
                var kelurahan_ktp_text = $("#txt_kelurahan_badan_hukum option:selected").val();
                var kdPOS_ktp_text = $("#txt_kd_pos_badan_hukum").val();


                $("#txt_alamat_domisili_badan_hukum").val(alamat_ktp).attr('disabled', true).valid()

                $("#txt_provinsi_domisili_badan_hukum").empty().trigger('change')
                $("#txt_provinsi_domisili_badan_hukum").prepend('<option value=' + provinsi_ktp_text + ' selected>' + provinsi_ktp_text + '</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Provinsi",
                    width: '100%'
                });

                $("#txt_kota_domisili_badan_hukum").empty().trigger('change')
                $("#txt_kota_domisili_badan_hukum").prepend('<option value=' + kota_ktp_text + ' selected>' + kota_ktp_text + '</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Kota",
                    width: '100%'
                });

                $("#txt_kecamatan_domisili_badan_hukum").empty().trigger('change')
                $("#txt_kecamatan_domisili_badan_hukum").prepend('<option value=' + kecamatan_ktp_text + ' selected>' + kecamatan_ktp_text + '</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Kecamatan",
                    width: '100%'
                });

                $("#txt_kelurahan_domisili_badan_hukum").empty().trigger('change')
                $("#txt_kelurahan_domisili_badan_hukum").prepend('<option value=' + kelurahan_ktp_text + ' selected>' + kelurahan_ktp_text + '</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Kelurahan",
                    width: '100%'
                });

                $('#txt_provinsi_domisili_badan_hukum').attr('disabled', true).valid()
                $('#txt_kota_domisili_badan_hukum').attr('disabled', true).valid()
                $('#txt_kecamatan_domisili_badan_hukum').attr('disabled', true).valid()
                $('#txt_kelurahan_domisili_badan_hukum').attr('disabled', true).valid()

                $("#txt_kd_pos_domisili_badan_hukum").val(kdPOS_ktp_text)

            } else {
                $("#txt_alamat_domisili_badan_hukum").attr('disabled', false).val('');
                $("#txt_provinsi_domisili_badan_hukum").empty().attr('disabled', false)
                $('#txt_provinsi_domisili_badan_hukum').prepend('<option selected></option>').select2({
                    theme: "bootstrap",
                    placeholder: "-- Pilih Satu --",
                    allowClear: true,
                    width: '100%'
                });
                $.getJSON("/borrower/data_provinsi", function(data_provinsi) {
                    for (let i = 0; i < data_provinsi.length; i++) {
                        $('#txt_provinsi_domisili_badan_hukum').append($('<option>', {
                            value: data_provinsi[i].text,
                            text: data_provinsi[i].text
                        }));
                    }
                });
                $("#txt_kota_domisili_badan_hukum").empty().attr('disabled', false);
                $("#txt_kecamatan_domisili_badan_hukum").empty().attr('disabled', false);
                $("#txt_kelurahan_domisili_badan_hukum").empty().attr('disabled', false);
                $("#txt_kd_pos_domisili_badan_hukum").val('')


            }
        });


    });



    // proses lengkapi profile
    $("#form_lengkapi_profile").submit(function(e) {
        e.preventDefault()
        var tipe_borrower = $("#label_tipe_borrower").text();

        if ($("#form_lengkapi_profile").valid()) {
            if ($("#divNoKTPPengurus").hasClass("has-error")) {
                $('#txt_no_ktp_pengurus').focus()
            } else if ($("#div_hp_pengurus").hasClass("has-error")) {
                $('#txt_noHP_pengurus').focus()
            } else {
                $("#modal_action_lengkapi_profile_pendanaan_simple_badan_hukum").modal('show');
            }
        }
    });

    // $('#kirim_lagi').on('click',function(){
    //     $('#kirim_lagi').prop('disabled',true);

    //     var tipeBrw = $("#label_tipe_borrower").text() ;
    //     if (tipeBrw == 'Individu')
    //     {
    //         $('#no_hp').val('62'+$('#txt_noHP_pribadi').val());
    //     }
    //     else
    //     {
    //         $('#no_hp').val('62'+$('#txt_noHP_pengurus').val());
    //     }

    //     noHP = $('#no_hp').val();
    //     console.log(noHP)
    //     $.ajax({
    //         url : "/borrower/kirimOTP/",
    //         method : "post",
    //         dataType: 'JSON',
    //         data: {hp:noHP},
    //         success:function(data)
    //         {
    //             console.log(data.status)
    //         }
    //     });
    //     timerDisableButton();
    // });

    // $('#kode_otp').on('keyup',function(){
    //     // console.log($('#kode_otp').val().length)
    //     if ($('#kode_otp').val().length == 6)
    //     {
    //         $('#kirim_data').removeAttr('disabled');

    //     }
    //     else
    //     {
    //         $('#kirim_data').prop('disabled',true);
    //     }
    // });

    let sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';

    // proses submit lengkapi profile
    $('#btn_proses_lengkapi_profile').on('click', function() {
        // $('#kirim_data').on('click',function(){
        // var type_borrower = $("#label_tipe_borrower").text() ;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: "{{ route('borrower.action_lengkapi_profile_pendanaan_simple') }}",
            method: "POST",
            dataType: 'JSON',
            data: {
                borrower_type: 1, //individu
                nama: $("#nama_individu").val(),
                ktp: $("#ktp").val(),
                npwp: $("#npwp").val(),
                agama: $("#agama").val(),
                status_pernikahan: $("input[name='status_kawin']:checked").val(),
                status_kepemilikan_rumah: $("input[name='status_kepemilikan_rumah']:checked").val(),
                pendidikan: $("#pendidikan_terakhir").val(),
                telepon: $("#telepon").val(),
                alamat: $("#alamat").val(),
                kelurahan: $("#kelurahan").val(),
                kecamatan: $("#kecamatan").val(),
                kota: $("#kota").val(),
                provinsi: $("#provinsi").val(),
                kode_pos: $("#kode_pos").val(),
                pekerjaan: $("#pekerjaan").val(),
                bidang_pekerjaan: $("#bidang_pekerjaan").val(),
                bidang_online: $("#bidang_online").val(),
                nama_perusahaan: $("#nama_perusahaan").val(),
                usia_perusahaan: $("#usia_perusahaan").val(),
                tahun_bekerja: $("#tahun_bekerja").val(),
                bulan_bekerja: $("#bulan_bekerja").val(),
                no_telpon_usaha: $("#no_telpon_usaha").val(),
                penghasilan: $("#penghasilan").val(),
                type_pendanaan: $("#type_pendanaan_select").val(),
                text_type_pendanaan: $("#type_pendanaan_select option:selected").text(),
                tujuan_pendanaan: $("#type_tujuan_pendanaan").val(),
                jenis_property: $("#jenis_property").val(),
                harga_objek_pendanaan: $("#txt_harga_objek_pendanaan").val(),
                uang_muka: $("#txt_uang_muka").val(),
                persen: $("#txt_persen").val(),
                nilai_pengajuan: $("#txt_nilai_pengajuan").val(),
                jangka_waktu: $("#txt_jangka_waktu").val(),
                txt_estimasi_proyek: $('#txt_estimasi_proyek').val(),

                tempat_lahir: $("#tempat_lahir").val(),
                user_camera_diri: $("#user_camera_diri").val(),
                user_camera_ktp: $("#user_camera_ktp").val(),
                user_camera_diri_dan_ktp: $("#user_camera_diri_dan_ktp").val(),
                user_camera_npwp: $("#user_camera_npwp").val(),

            },
            beforeSend: () => {
                console.log($("#tempat_lahir").val())
                $('.closemodaldata').trigger('click');
                swal.fire({
                    html: '<h5>Menyimpan Data...</h5>',
                    showConfirmButton: false,
                    allowOutsideClick: () => false,
                    onRender: () => {
                        $('.swal2-content').prepend(sweet_loader);
                    },
                });
            },
            success: function(data) {
                if (data.stts == '00') {
                    swal.close();
                    swal.fire({
                        title: "Berhasil",
                        type: "success",
                        text: data.message,
                        showCancelButton: false,
                        allowOutsideClick: false,
                        confirmButtonClass: "btn-success",
                    }).then(function(response) {
                        if (response.value) {
                            location.href = "/borrower/beranda";
                        }
                    })
                } else {

                    // console.log(data)
                    if(data.status == 'limit'){
                        swal.fire({
                            title: "Gagal Menyimpan Data",
                            type: "error",
                            text: data.message,
                            showCancelButton: false,
                            allowOutsideClick: false,
                            confirmButtonClass: "btn-danger",
                        });

                    }else{
                        swal.fire({
                            title: "Gagal Menyimpan Data",
                            type: "error",
                            text: "Pastikan semua form telah terisi",
                            showCancelButton: false,
                            allowOutsideClick: false,
                            confirmButtonClass: "btn-danger",
                        });

                    }

                    
                    // $('#modal_action_lengkapi_profile_pendanaan_simple').close();    
                   
                    // .then( function (response) {
                    //     console.log(response)
                    //     if (response.value){
                    //         $('#otp').modal('show');
                    //     }
                    // })
                }
            },
            error: function(response) {
                console.log(response)
                swal.fire({
                    title: 'Error',
                    text: 'Internal Error',
                    type: 'error',
                    showConfirmButton: true,
                });
            }
        });
    });

    // $('#btn_proses_lengkapi_profile').on('click',function(){
    //     var tipeBrw = $("#label_tipe_borrower").text() ;

    //     if(tipeBrw == 'Individu'){
    //         sendOtp()
    //     } else{ 

    //         $.ajaxSetup({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             }
    //         });

    //         $('#kirim_lagi').prop('disabled',true);

    //         $('#no_hp').val("62"+$('#txt_noHP_pengurus').val());
    //         $('#set_no_hp').text('Silakan masukkan kode OTP yang telah dikirim ke nomor ' + '62'+$('#txt_noHP_pengurus').val() );

    //         noHP = $('#no_hp').val();
    //         console.log(noHP)
    //         $.ajax({
    //             url : "/borrower/kirimOTP/",
    //             method : "post",
    //             dataType: 'JSON',
    //             data: {hp:noHP},
    //             success:function(data)
    //             {
    //                 console.log(data.status)
    //             }
    //         });
    //         timerDisableButton();
    //     }
    // });
    $('#btn_lengkapi_profile_p').on('click', function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var tipeBrw = $("#label_tipe_borrower").text(); //Perusahaan
        if (tipeBrw == 'Perusahaan') {
            var borrower_type = 2
        }
        var txt_nm_badan_hukum = $("#txt_nm_badan_hukum").val();
        var txt_nib_badan_hukum = $("#txt_nib_badan_hukum").val();
        var txt_npwp_badan_hukum = $("#txt_npwp_badan_hukum").val();
        var txt_akta_badan_hukum = $("#txt_akta_badan_hukum").val();
        var txt_tgl_berdiri_badan_hukum = $("#txt_tgl_berdiri_badan_hukum").val();
        var kode_telepon = $("#kode_telepon option:selected").val();
        var txt_tlp_badan_hukum = $("#txt_tlp_badan_hukum").val();
        var url_npwp_badan_hukum = $("#url_npwp_badan_hukum").val();
        var txt_alamat_badan_hukum = $("#txt_alamat_badan_hukum").val();
        var txt_provinsi_badan_hukum = $("#txt_provinsi_badan_hukum option:selected").text();
        var txt_kota_badan_hukum = $("#txt_kota_badan_hukum option:selected").text();
        var txt_kecamatan_badan_hukum = $("#txt_kecamatan_badan_hukum option:selected").text();
        var txt_kelurahan_badan_hukum = $("#txt_kelurahan_badan_hukum option:selected").text();
        var txt_kd_pos_badan_hukum = $("#txt_kd_pos_badan_hukum").val();

        // domisili
        var txt_alamat_domisili_badan_hukum = $("#txt_alamat_domisili_badan_hukum").val();
        var txt_provinsi_domisili_badan_hukum = $("#txt_provinsi_domisili_badan_hukum option:selected").text();
        var txt_kota_domisili_badan_hukum = $("#txt_kota_domisili_badan_hukum option:selected").text();
        var txt_kecamatan_domisili_badan_hukum = $("#txt_kecamatan_domisili_badan_hukum option:selected").text();
        var txt_kelurahan_domisili_badan_hukum = $("#txt_kelurahan_domisili_badan_hukum option:selected").text();
        var txt_kd_pos_domisili_badan_hukum = "";
        if ($('#domisili_badan_hukum').is(":checked")) {
            txt_kd_pos_domisili_badan_hukum = $("#txt_kd_pos_badan_hukum").val()
        } else {
            txt_kd_pos_domisili_badan_hukum = $("#txt_kd_pos_domisili_badan_hukum").val();
        };

        // Rekening
        var txt_nm_pemilik_badan_hukum = $("#txt_nm_pemilik_badan_hukum").val();
        var txt_no_rekening_badan_hukum = $("#txt_no_rekening_badan_hukum").val();
        var txt_bank_badan_hukum = $("#txt_bank_pribadi option:selected").val();
        var txt_kantor_cabang_pembuka_badan_hukum = $("#txt_kantor_cabang_pembuka").val();

        // dan lain lain
        var txt_bd_pekerjaan_badan_hukum = $("#txt_bd_pekerjaan_badan_hukum option:selected").val();
        var txt_omset_thn_akhir = $("#txt_omset_thn_akhir").val();
        var txt_aset_thn_akhir = $("#txt_aset_thn_akhir").val();

        var no_akta_perubahan = $("#no_akta_perubahan").val();
        var tgl_akta_perubahan = $("#tgl_akta_perubahan").val();

        var is_tbk = $("#is_tbk option:selected").val();

        // pengurus

        var pengurus_nama = [];
        pengurus_jk = [];
        pengurus_ktp = [];
        pengurus_tmp_lahir = [];
        pengurus_tgl_lahir = [];
        pengurus_no_hp = [];
        pengurus_agama = [];
        pengurus_pendidikan = [];
        pengurus_npwp = [];
        pengurus_alamat = [];
        pengurus_provinsi = [];
        pengurus_kota = [];
        pengurus_kecamatan = [];
        pengurus_kelurahan = [];
        pengurus_kd_pos = [];
        pengurus_foto = [];
        pengurus_foto_ktp = [];
        pengurus_fotoKTP = [];
        pengurus_foto_npwp = [];
        pengurus_jabatan = [];

        

        // pengurus_nama
        $('input[name="txt_nm_pengurus[]"]').each(function() {
            pengurus_nama.push(this.value);
        });

        // pengurus_jk
        
        pengurus_jk.push($("input:radio[name=txt_jns_kelamin_pengurus]:checked").val());
        pengurus_jk.push($("input:radio[name=txt_jns_kelamin_pengurus2]:checked").val());

        //pengurus_ktp
        $('input[name="txt_no_ktp_pengurus[]"]').each(function() {
            pengurus_ktp.push(this.value);
        });

        //pengurus_tmp_lahir
        $('input[name="txt_tmpt_lahir_pengurus[]"]').each(function() {
            pengurus_tmp_lahir.push(this.value);
        });

        //pengurus_tgl_lahir
        $('input[name="txt_tgl_lahir_pengurus[]"]').each(function() {
            pengurus_tgl_lahir.push(this.value);
        });

        //pengurus_no_hp
        $('input[name="txt_noHP_pengurus[]"]').each(function() {
            pengurus_no_hp.push(this.value);
        });

        //pengurus_agama
        $('select[name="txt_agama_pengurus[]"]').each(function() {
            pengurus_agama.push(this.value);
            console.log(this.value);

        });

        //pengurus_pendidikan
        $('select[name="txt_pendidikanT_pengurus[]"]').each(function() {
            pengurus_pendidikan.push(this.value);
        });

        //pengurus_npwp
        $('input[name="txt_npwp_pengurus[]"]').each(function() {
            pengurus_npwp.push(this.value);
        });

        //pengurus jabatan
        $('select[name="txt_jabatan_pengurus[]"]').each(function() {
            pengurus_jabatan.push(this.value);
        });

        //pengurus_alamat
        $('textarea[name="txt_alamat_pengurus[]"]').each(function() {
            pengurus_alamat.push(this.value);
        });

        //pengurus_provinsi
        $('select[name="txt_provinsi_pengurus[]"]').each(function() {
            pengurus_provinsi.push(this.value);
        });

        //pengurus_kota
        $('select[name="txt_kota_pengurus[]"]').each(function() {
            pengurus_kota.push(this.value);
        });

        //pengurus_kecamatan
        $('select[name="txt_kecamatan_pengurus[]"]').each(function() {
            pengurus_kecamatan.push(this.value);
        });

        //pengurus_kelurahan
        $('select[name="txt_kelurahan_pengurus[]"]').each(function() {
            pengurus_kelurahan.push(this.value);
        });

        //pengurus_kd_pos
        $('input[name="txt_kd_pos_pengurus[]"]').each(function() {
            pengurus_kd_pos.push(this.value);
        });

        //pengurus_foto
        $('input[name="url_pic_pengurus[]"]').each(function() {
            pengurus_foto.push(this.value);
        });

        //pengurus_foto_ktp
        $('input[name="url_pic_ktp_pengurus[]"]').each(function() {
            pengurus_foto_ktp.push(this.value);
        });

        //pengurus_fotoKTP
        $('input[name="url_pic_brw_dengan_ktp_pengurus[]"]').each(function() {
            pengurus_fotoKTP.push(this.value);
        });

        //pengurus_foto_npwp
        $('input[name="url_pic_npwp_pengurus[]"]').each(function() {
            pengurus_foto_npwp.push(this.value);
        });

        var pengurus = [];
        for (var u = 0; u < pengurus_nama.length; u++) {
            pengurus[u] = [
                pengurus_nama[u] + "@",
                "@" + pengurus_jk[u] + "@", "@" + pengurus_ktp[u] + "@", "@" + pengurus_tmp_lahir[u] + "@", "@" + pengurus_tgl_lahir[u] + "@", "@" + "62" + pengurus_no_hp[u] + "@", "@" + pengurus_agama[u] + "@", "@" + pengurus_pendidikan[u] + "@",
                "@" + pengurus_npwp[u] + "@", "@" + pengurus_jabatan[u] + "@", "@" + pengurus_alamat[u] + "@", "@" + pengurus_provinsi[u] + "@", "@" + pengurus_kota[u] + "@", "@" + pengurus_kecamatan[u] + "@", "@" + pengurus_kelurahan[u] + "@", "@" + pengurus_kd_pos[u] + "@",
                "@" + pengurus_foto[u] + "@", "@" + pengurus_foto_ktp[u] + "@", "@" + pengurus_fotoKTP[u] + "@", "@" + pengurus_foto_npwp[u]
            ];
        }
        var pengurus_arr = pengurus.join("^~");
        if (pengurus_nama.length < 2) {
            $('#otp').modal('hide');
            Swal.fire('Daftarkan Pengurus Minimal 2');
            $('html, body').animate({
                scrollTop: $("#divCountPengurus_1").offset().top
            }, 500);
            return false;
        } else {
            var dataBadanHukum = new FormData();
            dataBadanHukum.append('_token', "{{ csrf_token() }}");
            dataBadanHukum.append('borrower_type', borrower_type);
            dataBadanHukum.append('txt_nm_badan_hukum', txt_nm_badan_hukum);
            dataBadanHukum.append('txt_nib_badan_hukum', txt_nib_badan_hukum);
            dataBadanHukum.append('txt_npwp_badan_hukum', txt_npwp_badan_hukum);
            dataBadanHukum.append('txt_akta_badan_hukum', txt_akta_badan_hukum);
            dataBadanHukum.append('txt_tgl_berdiri_badan_hukum', txt_tgl_berdiri_badan_hukum);

             //No akta perubahan 
            dataBadanHukum.append('no_akta_perubahan', no_akta_perubahan);
            dataBadanHukum.append('tgl_akta_perubahan', tgl_akta_perubahan);
            dataBadanHukum.append('is_tbk', is_tbk);

            dataBadanHukum.append('kode_telepon', kode_telepon);
            dataBadanHukum.append('txt_tlp_badan_hukum', txt_tlp_badan_hukum);
            dataBadanHukum.append('url_npwp_badan_hukum', url_npwp_badan_hukum);
            dataBadanHukum.append('txt_alamat_badan_hukum', txt_alamat_badan_hukum);
            dataBadanHukum.append('txt_provinsi_badan_hukum', txt_provinsi_badan_hukum);
            dataBadanHukum.append('txt_kota_badan_hukum', txt_kota_badan_hukum);
            dataBadanHukum.append('txt_kecamatan_badan_hukum', txt_kecamatan_badan_hukum);
            dataBadanHukum.append('txt_kelurahan_badan_hukum', txt_kelurahan_badan_hukum);
            dataBadanHukum.append('txt_kd_pos_badan_hukum', txt_kd_pos_badan_hukum);


            // domisili
            dataBadanHukum.append('txt_alamat_domisili_badan_hukum', txt_alamat_domisili_badan_hukum);
            dataBadanHukum.append('txt_provinsi_domisili_badan_hukum', txt_provinsi_domisili_badan_hukum);
            dataBadanHukum.append('txt_kota_domisili_badan_hukum', txt_kota_domisili_badan_hukum);
            dataBadanHukum.append('txt_kecamatan_domisili_badan_hukum', txt_kecamatan_domisili_badan_hukum);
            dataBadanHukum.append('txt_kelurahan_domisili_badan_hukum', txt_kelurahan_domisili_badan_hukum);
            dataBadanHukum.append('txt_kd_pos_domisili_badan_hukum', txt_kd_pos_domisili_badan_hukum);

            // Rekening
            dataBadanHukum.append('txt_nm_pemilik_badan_hukum', txt_nm_pemilik_badan_hukum);
            dataBadanHukum.append('txt_no_rekening_badan_hukum', txt_no_rekening_badan_hukum);
            dataBadanHukum.append('txt_bank_badan_hukum', txt_bank_badan_hukum);
            dataBadanHukum.append('txt_kantor_cabang_pembuka_badan_hukum', txt_kantor_cabang_pembuka_badan_hukum);

            // dan lain lain
            dataBadanHukum.append('txt_bd_pekerjaan_badan_hukum', txt_bd_pekerjaan_badan_hukum);
            dataBadanHukum.append('txt_omset_thn_akhir', txt_omset_thn_akhir);
            dataBadanHukum.append('txt_aset_thn_akhir', txt_aset_thn_akhir);

            // pengurus 
            dataBadanHukum.append('pengurus', pengurus_arr);


            console.log(pengurus_arr);

            // proses simpan lengkapi profile
            $.ajax({
                url: "{{route('borrower.action_lengkapi_profile_pendanaan_simple')}}",
                type: "POST",
                data: dataBadanHukum,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $("#overlay").css('display', 'block');
                },
                success: function(response) {
                    console.log(response);
                    if (response.status == "sukses") {
                        // collectData();
                        // $("#otp").modal("hide");
                        swal.fire({
                            title: 'Berhasil',
                            allowOutsideClick: false,
                            // html: "Hi {{ Session::get('brw_nama') }} , <br/> Lengkapi Profile Berhasil, Silahkan Ajukan Pendanaan Baru ! <br><br/><span><button class='btn btn-primary btn-lg' id='btn_confirm'>OK</button></span>",
                            html: "Hi {{ Session::get('brw_nama') }} , <br/> Perubahan Data Anda Sukses <br><br/><span><button class='btn btn-primary btn-lg' id='btn_confirm'>OK</button></span>",
                            type: "success",
                            showConfirmButton: false
                        });
                        $('#btn_confirm').on('click', function() {
                            location.href = "/borrower/beranda";
                        });
                    }
                },
                error: function(response) {
                    console.log(response)
                    swal.fire({
                        title: 'Error',
                        text: 'Internal Error',
                        type: 'error',
                        showConfirmButton: false,
                    });
                }
            });
        }
    });

    const getPendanaanType = (tipe_pendanaan) => {
        // $('#type_tujuan_pendanaan').empty()
        $('#type_tujuan_pendanaan').empty().append($('<option>', {
            value: '',
            text: '-- Pilih --'
        }));
        $.ajax({
            url: '/borrower/tujuan_pendanaan_kpr',
            method: 'GET',
            data: {
                'tipe_pendanaan': tipe_pendanaan
            },
            success: (data_tujuan_pendanaan) => {
                let this_data = JSON.parse(data_tujuan_pendanaan)

                this_data.forEach(element => {
                    $('#type_tujuan_pendanaan').append($('<option>', {
                        value: element.id,
                        text: element.text
                    }));
                });
            },
            error: (response) => {
                alert('erroor')
            }
        })
    }

    function validalamat() {
        var alamat = $('#alamat').val();
        if (alamat.trim() == "") {
            $('#alamat').val("");
        }
    }
</script>
@endsection