<div class="col-12">
    <!-- START: Form Badan Hukum -->
    <form id="form_lengkapi_profile" class="js-wizard-validation-classic-form" method="POST" enctype="multipart/form-data">
        @csrf
        <div id="layout-perusahaan" class="layout">
            <div class="row">
                <div class="col-md-12">
                    <!-- START: Informasi Perusahaan -->
                    <div class="js-wizard-simple block border">
                        <div class="block-content p-0 block-content-full tab-content">
                            <div class="block">
                                <div class="block-header block-header-default">
                                    <a class="input-hidden" id="label_tipe_borrower"></a>
                                    <h3 class="block-title">Informasi Perusahaan</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Nama
                                                    Badan Hukum</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" type="text" id="txt_nm_badan_hukum" maxlength="50" name="txt_nm_badan_hukum" placeholder="Masukkan Nama Badan Hukum..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_nib_perusahaan">Nomor Surat
                                                    Izin</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkNIB" type="text" maxlength="40" id="txt_nib_badan_hukum" name="txt_nib_badan_hukum" placeholder="SIUP/TDP/Nomor surat izin lainnya" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_npwp_perusahaan">NPWP</label>
                                                <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" minlength="15" maxlength="15" id="txt_npwp_badan_hukum" name="txt_npwp_badan_hukum" placeholder="Masukkan Nomor NPWP" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-ktp">Nomor Akta
                                                    Pendirian</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" maxlength="5" id="txt_akta_badan_hukum" name="txt_akta_badan_hukum" placeholder="Masukkan nomor Akta Pendirian" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label class="wizard-progress2-namapengguna">Tanggal
                                                    Berdiri</label> <label class="mandatory_label">*</label>
                                                <input class="form-control" type="date" id="txt_tgl_berdiri_badan_hukum" name="txt_tgl_berdiri_badan_hukum" max="<?= date("Y-m-d"); ?>" placeholder="Masukkan Tanggal Berdiri Perusahaan..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_no_tlp_perusahaan">Nomor
                                                    Telepon <i class="text-danger">*</i></label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi">
                                                            021 </span>
                                                    </div>
                                                    <input class="form-control validasiString" type="text" minlength="7" maxlength="13" id="txt_tlp_badan_hukum" name="txt_tlp_badan_hukum" placeholder="Masukkan Nomor Telepon" required>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="col-12" id="lbl_foto_npwp_perusahaan">Foto
                                                    NPWP Perusahaan <label class="mandatory_label">*</label>
                                                </label>
                                                <div id="take_camera_npwp_badan_hukum">
                                                    <label class="btn btn-success col-md-11"><i class="fa fa-camera">
                                                            Kamera</i></label>
                                                </div>
                                                <div class="col-md-12" id="webcam_npwp_badan_hukum">
                                                    <div id="camera_npwp_badan_hukum" style="z-index:-1 !important; margin-top: -30px;">
                                                    </div>
                                                    <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                    <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_npwp_badan_hukum(); $('#url_npwp_badan_hukum_val').val('1').valid()">Ambil
                                                        Foto</button>
                                                    <input type="hidden" name="image_npwp_badan_hukum" id="image_npwp_badan_hukum" class="image-tag"><br />
                                                    <input type="hidden" name="url_npwp_badan_hukum" id="url_npwp_badan_hukum">
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                    <div id="results_npwp_badan_hukum"></div>
                                                </div><br />
                                                <input type="text" class="input-hidden2" id="url_npwp_badan_hukum_val" name="url_npwp_badan_hukum_val" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat Sesuai AKTA
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <!-- satuBaris -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Alamat</label>
                                                        <label class="mandatory_label">*</label>
                                                        <textarea class="form-control form-control-lg" maxlength="90" id="txt_alamat_badan_hukum" name="txt_alamat_badan_hukum" rows="2" placeholder="Masukkan alamat lengkap sesuai Akta Anda.." onchange="$('#domisili_badan_hukum').trigger('change')" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Provinsi</label>
                                                        <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_provinsi_badan_hukum" name="txt_provinsi_badan_hukum" onchange="provinsiChange(this.value, this.id, 'txt_kota_badan_hukum'); $(this).valid(); $('#domisili_badan_hukum').trigger('change')" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-kota">Kota /
                                                            Kabupaten</label> <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_kota_badan_hukum" name="txt_kota_badan_hukum" onchange="kotaChange(this.value, this.id, 'txt_kecamatan_badan_hukum'); $(this).valid(); $('#domisili_badan_hukum').trigger('change')" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                                        <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_kecamatan_badan_hukum" name="txt_kecamatan_badan_hukum" onchange="kecamatanChange(this.value, this.id, 'txt_kelurahan_badan_hukum'); $(this).valid(); $('#domisili_badan_hukum').trigger('change')" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                                        <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_kelurahan_badan_hukum" name="txt_kelurahan_badan_hukum" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_badan_hukum'); $(this).valid(); $('#domisili_badan_hukum').trigger('change');" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Kode
                                                            Pos</label> <label class="mandatory_label">*</label>
                                                        <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_badan_hukum" name="txt_kd_pos_badan_hukum" placeholder="--" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat
                                                    Domisili Sama Dengan AKTA <input type="checkbox" id="domisili_badan_hukum">
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="div_domisili_badan_hukum">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <!-- satuBaris -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Alamat <i class="text-danger">*</i> </label>
                                                        <textarea class="form-control form-control-lg" maxlength="90" id="txt_alamat_domisili_badan_hukum" name="txt_alamat_domisili_badan_hukum" rows="2" placeholder="Masukkan alamat domisili sesuai dengan akta.." required></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_provinsi_domisili_badan_hukum">Provinsi <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_provinsi_domisili_badan_hukum" name="txt_provinsi_domisili_badan_hukum" onchange="provinsiChange(this.value, this.id, 'txt_kota_domisili_badan_hukum'); $(this).valid()" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kota_domisili_badan_hukum">Kota /
                                                            Kabupaten <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_kota_domisili_badan_hukum" name="txt_kota_domisili_badan_hukum" onchange="kotaChange(this.value, this.id, 'txt_kecamatan_domisili_badan_hukum'); $(this).valid()" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kecamatan_domisili_badan_hukum">Kecamatan <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_kecamatan_domisili_badan_hukum" name="txt_kecamatan_domisili_badan_hukum" onchange="kecamatanChange(this.value, this.id, 'txt_kelurahan_domisili_badan_hukum'); $(this).valid()" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kelurahan_domisili_badan_hukum">Kelurahan <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_kelurahan_domisili_badan_hukum" name="txt_kelurahan_domisili_badan_hukum" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_domisili_badan_hukum'); $(this).valid()" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kd_pos_domisili_badan_hukum">Kode
                                                            Pos <i class="text-danger">*</i></label>
                                                        <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_domisili_badan_hukum" name="txt_kd_pos_domisili_badan_hukum" placeholder="--" readonly>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- START: Rekening -->
                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Informasi
                                                    Rekening
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END: Rekening -->

                                    <div class="row" id="div_hide_rek_bank_badan_hukum">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">No
                                                    Rekening </label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" minlength="10" maxlength="15" id="txt_no_rekening_badan_hukum" name="txt_no_rekening_badan_hukum" placeholder="Masukkan No Rekening..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Nama Pemilik
                                                    Rekening </label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" type="text" maxlength="35" id="txt_nm_pemilik_badan_hukum" name="txt_nm_pemilik_badan_hukum" placeholder="Masukkan Nama Pemilik Rekening..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Bank </label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_bank_pribadi" name="txt_bank_pribadi" onchange="$(this).valid()" required>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_kantor_cabang_pembuka">Kantor Cabang
                                                    Pembuka <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" maxlength="35" id="txt_kantor_cabang_pembuka" name="txt_kantor_cabang_pembuka" placeholder="Masukkan Kantor Cabang Pembuka" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Rekening -->

                                    <!-- START: Informasi Lain Lain -->
                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Informasi
                                                    Lain Lain
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <!-- satuBaris -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-kota">Bidang Usaha
                                                        </label> <label class="mandatory_label">*</label>
                                                        <select class="form-control" id="txt_bd_pekerjaan_badan_hukum" name="txt_bd_pekerjaan_badan_hukum" onchange="$(this).valid()" required></select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-kota">Omset Tahun
                                                            Terakhir </label> <label class="mandatory_label">*</label>
                                                        <input class="form-control no-zero" type="text" maxlength="30" id="txt_omset_thn_akhir" name="txt_omset_thn_akhir" placeholder="Masukkan Omset Tahun Terakhir" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_aset_thn_akhir">Total Aset Tahun
                                                            Terakhir </label> <label class="mandatory_label">*</label>
                                                        <input class="form-control no-zero" type="text" maxlength="30" id="txt_aset_thn_akhir" name="txt_aset_thn_akhir" placeholder="Masukkan Omset Tahun Terakhir" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" required>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <!-- END: Informasi Lain Lain -->
                                    <!-- END Informasi Perusahaan -->
                                </div>
                                {{-- END: Content --}}
                            </div>
                        </div>
                    </div>
                    <!-- END: Informasi Perusahaan -->
                    <!-- START: Informasi Pengurus 1-->
                    <div class="js-wizard-simple block border">
                        <div class="block-content p-0 block-content-full tab-content">
                            <div class="block ClassPengurus" id="divCountPengurus_1">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Informasi Pengurus</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Nama Pengurus
                                                </label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="txt_nm_pengurus" name="txt_nm_pengurus[]" placeholder="Masukkan Nama Pengurus ..." required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_jns_kelamin_pengurus_1">Jenis Kelamin <i class="text-danger">*</i></label>
                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus[]" id="txt_jns_kelamin_pengurus_1" value="1" required>
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_1">Laki -
                                                                Laki</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus[]" id="txt_jns_kelamin_pengurus_4" value="2">
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_4">Perempuan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group" id="divNoKTPPengurus">
                                                <label id="lbl_no_ktp_pengurus">Nomor Kartu Tanda Penduduk
                                                    (KTP)</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString checkNIKPengurus" type="text" minlength="16" maxlength="16" id="txt_no_ktp_pengurus" name="txt_no_ktp_pengurus[]" placeholder="Masukkan nomor KTP" onkeyup="CheckNIKPengurus()" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tempat
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="txt_tmpt_lahir_pengurus" name="txt_tmpt_lahir_pengurus[]" placeholder="Masukkan Tempat Lahir..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tanggal
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control" type="date" id="txt_tgl_lahir_pengurus" name="txt_tgl_lahir_pengurus[]" max="{{date("Y-m-d")}}" placeholder="Masukkan Tanggal Lahir..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group" id="div_hp_pengurus">
                                                <label id="lbl_no_hp_pengurus">Nomor HP</label> <label class="mandatory_label">*</label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi">
                                                            +62 </span>
                                                    </div>
                                                    <input class="form-control no-zero eight checkNOHPPengurus validasiString" type="text" size="13" minlength="9" maxlength="13" id="txt_noHP_pengurus" name="txt_noHP_pengurus[]" onkeyup="if($(this).val().length > 8){check_hp_pengurus()}" placeholder="Masukkan Nomor HP Pengurus" required>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Agama</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_agama_pengurus" id="txt_agama_pengurus" name="txt_agama_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_pendidikanT_pengurus">Pendidikan
                                                    Terakhir</label> <label class="mandatory_label">*</label>
                                                <select class="form-control txt_pendidikanT_pengurus" id="txt_pendidikanT_pengurus" name="txt_pendidikanT_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_npwp_pengurus">Nomor NPWP</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString txt_npwp_pengurus" type="text" minlength="15" maxlength="15" id="txt_npwp_pengurus" name="txt_npwp_pengurus[]" placeholder="Masukkan nomor NPWP" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Jabatan</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_jabatan_pengurus" id="txt_jabatan_pengurus" name="txt_jabatan_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat Sesuai KTP
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Alamat Sesuai
                                                    KTP</label> <label class="mandatory_label">*</label>
                                                <textarea class="form-control form-control-lg txt_alamat_pengurus" maxlength="90" id="txt_alamat_pengurus" name="txt_alamat_pengurus[]" rows="4" placeholder="Masukkan alamat lengkap sesuai KTP Anda.." required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Provinsi</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_provinsi_pengurus" name="txt_provinsi_pengurus[]" onchange="provinsiChange(this.value, this.id, 'txt_kota_pengurus'); $(this).valid()" required>
                                                    {{-- <option value="">-- Pilih Satu --</option> --}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Kota / Kabupaten</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kota_pengurus" name="txt_kota_pengurus[]" onchange="kotaChange(this.value, this.id, 'txt_kecamatan_pengurus'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kecamatan_pengurus" name="txt_kecamatan_pengurus[]" onchange="kecamatanChange(this.value, this.id, 'txt_kelurahan_pengurus'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kelurahan_pengurus" name="txt_kelurahan_pengurus[]" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_pengurus'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kode Pos</label>
                                                <label class="mandatory_label">*</label>
                                                <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_pengurus" name="txt_kd_pos_pengurus[]" placeholder="--" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Foto Diri Individu -->
                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Foto Pengurus
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" id="div_foto">
                                            <div class="row">
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_pengurus">Foto
                                                            Pengurus <label class="mandatory_label">*</label>
                                                        </label>
                                                        <div class="col-12">
                                                            <div id="take_camera_pengurus">
                                                                <label class="btn btn-success col-md-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_pengurus">
                                                                <img id="user-guide" src="{{URL::to('assets/img/user-guide.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_pengurus" style="z-index:-1 !important;"></div>
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_pengurus(); $('#url_pic_pengurus_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_pengurus[]" id="image_foto_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_pengurus[]" id="url_pic_pengurus">
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_pengurus"></div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_pengurus_val" name="url_pic_pengurus_val" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_ktp_pengurus">Foto KTP
                                                            Pengurus <label class="mandatory_label">*</label>
                                                        </label>
                                                        <div class="col-12">

                                                            <div id="take_camera_foto_ktp_pengurus">
                                                                <label class="btn btn-success col-md-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_foto_ktp_pengurus">
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_ktp_pengurus" style="z-index:-1 !important;">
                                                                </div>
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktp_pengurus(); $('#url_pic_ktp_pengurus_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_pengurus[]" id="image_foto_ktp_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_ktp_pengurus[]" id="url_pic_ktp_pengurus">
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_ktp_pengurus"></div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_ktp_pengurus_val" name="url_pic_ktp_pengurus_val" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_ktp_pengurus">Foto
                                                            Pengurus Dengan
                                                            KTP <label class="mandatory_label">*</label>
                                                        </label>
                                                        <div class="col-12">

                                                            <div id="take_camera_foto_ktpdiri_pengurus">
                                                                <label class="btn btn-success col-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_foto_ktpdiri_pengurus">
                                                                <div id="camera_ktpdiri_pengurus" style="z-index:-1 !important;">
                                                                </div>
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-diridanktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktpdiri_pengurus(); $('#url_pic_brw_dengan_ktp_pengurus_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_diri_pengurus[]" id="image_foto_ktp_diri_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_brw_dengan_ktp_pengurus[]" id="url_pic_brw_dengan_ktp_pengurus">
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_ktp_diri_pengurus">
                                                                </div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_brw_dengan_ktp_pengurus_val" name="url_pic_brw_dengan_ktp_pengurus_val" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_npwp_pengurus">Foto
                                                            NPWP
                                                            Pengurus
                                                            <label class="mandatory_label">*</label></label>
                                                        <div class="col-12">
                                                            <div id="take_camera_foto_npwp_pengurus">
                                                                <label class="btn btn-success col-md-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_foto_npwp_pengurus">
                                                                <div id="camera_npwp_pengurus" style="z-index:-1 !important;">
                                                                </div>
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_npwp_pengurus(); $('#url_pic_npwp_pengurus_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_npwp_pengurus[]" id="image_foto_npwp_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_npwp_pengurus[]" id="url_pic_npwp_pengurus">
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_npwp_pengurus"></div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_npwp_pengurus_val" name="url_pic_npwp_pengurus_val" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Foto Diri Pengurus -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Informasi Pengurus 1-->
                    <!-- START: Informasi Pengurus 2-->
                    <div class="js-wizard-simple block border">
                        <div class="block-content p-0 block-content-full tab-content">
                            <div class="block ClassPengurus" id="divCountPengurus_2">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Informasi Pengurus 2</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Nama
                                                    Pengurus</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="txt_nm_pengurus_2" name="txt_nm_pengurus[]" placeholder="Masukkan Nama Pengurus ..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_jns_kelamin_pengurus_2">Jenis Kelamin <i class="text-danger">*</i></label>
                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus2[]" id="txt_jns_kelamin_pengurus_2" value="1" required>
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_2">Laki -
                                                                Laki</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus2[]" id="txt_jns_kelamin_pengurus_6" value="2">
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_6">Perempuan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_no_ktp_pengurus_2">Nomor Kartu Tanda Penduduk
                                                    (KTP)</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" minlength="16" maxlength="16" id="txt_no_ktp_pengurus_2" name="txt_no_ktp_pengurus[]" placeholder="Masukkan nomor KTP" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tempat
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="txt_tmpt_lahir_pengurus_2" name="txt_tmpt_lahir_pengurus[]" placeholder="Masukkan Tempat Lahir..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tanggal
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control" type="date" id="txt_tgl_lahir_pengurus_2" name="txt_tgl_lahir_pengurus[]" max="{{date("Y-m-d")}}" placeholder="Masukkan Tanggal Lahir..." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_no_hp_pengurus_2">Nomor HP</label> <label class="mandatory_label">*</label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi">
                                                            +62 </span>
                                                    </div>
                                                    <input class="form-control no-zero eight checkNOHPPengurus validasiString" type="text" minlength="9" maxlength="13" id="txt_noHP_pengurus_2" name="txt_noHP_pengurus[]" placeholder="Masukkan Nomor HP Pengurus" required>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Agama</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_agama_pengurus_2" id="txt_agama_pengurus_2" name="txt_agama_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-npwp">Pendidikan
                                                    Terakhir</label> <label class="mandatory_label">*</label>
                                                <select class="form-control txt_pendidikanT_pengurus_2" id="txt_pendidikanT_pengurus_2" name="txt_pendidikanT_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_npwp_pengurus_2">Nomor NPWP</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString txt_npwp_pengurus" type="text" minlength="15" maxlength="15" id="txt_npwp_pengurus_2" name="txt_npwp_pengurus[]" placeholder="Masukkan nomor NPWP" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Jabatan</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_jabatan_pengurus" id="txt_jabatan_pengurus_2" name="txt_jabatan_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat Sesuai KTP
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Alamat Sesuai
                                                    KTP</label> <label class="mandatory_label">*</label>
                                                <textarea class="form-control form-control-lg txt_alamat_pengurus" maxlength="90" id="txt_alamat_pengurus_2" name="txt_alamat_pengurus[]" rows="4" placeholder="Masukkan alamat lengkap sesuai KTP Anda.." required></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Provinsi</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_provinsi_pengurus_2" name="txt_provinsi_pengurus[]" onchange="provinsiChange(this.value, this.id, 'txt_kota_pengurus_2'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Kota / Kabupaten</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kota_pengurus_2" name="txt_kota_pengurus[]" onchange="kotaChange(this.value, this.id, 'txt_kecamatan_pengurus_2'); $(this).valid()" required>
                                                    <option>-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kecamatan_pengurus_2" name="txt_kecamatan_pengurus[]" onchange="kecamatanChange(this.value, this.id, 'txt_kelurahan_pengurus_2'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kelurahan_pengurus_2" name="txt_kelurahan_pengurus[]" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_pengurus_2'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kode Pos</label>
                                                <label class="mandatory_label">*</label>
                                                <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_pengurus_2" name="txt_kd_pos_pengurus[]" placeholder="--" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Foto Pengurus
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" id="div_foto">
                                            <div class="row">
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_pengurus_2">Foto
                                                            Pengurus <i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="take_camera_pengurus_2">
                                                                <label class="btn btn-success col-md-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_pengurus_2">
                                                                <img id="user-guide" src="{{URL::to("assets/img/user-guide.png")}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_pengurus_2" style="z-index:-1 !important;">
                                                                </div>
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_pengurus_2(); $('#url_pic_pengurus_2_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="results_foto_pengurus[]" id="image_foto_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_pengurus[]" id="url_pic_pengurus_2" required>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_pengurus_2"></div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_pengurus_2_val" name="url_pic_pengurus_2_val" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_ktp_pengurus_2">Foto
                                                            KTP Pengurus <i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="take_camera_foto_ktp_pengurus_2">
                                                                <label class="btn btn-success col-md-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_foto_ktp_pengurus_2">
                                                                <img id="user-guide" src="{{URL::to("assets/img/guide-ktp.png")}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_ktp_pengurus_2" style="z-index:-1 !important;">
                                                                </div>
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktp_pengurus_2(); $('#url_pic_ktp_pengurus_2_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_pengurus[]" id="image_foto_ktp_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_ktp_pengurus[]" id="url_pic_ktp_pengurus_2" required>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_ktp_pengurus_2"></div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_ktp_pengurus_2_val" name="url_pic_ktp_pengurus_2_val" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_ktp_pengurus_2">Foto
                                                            Pengurus
                                                            Dengan KTP <i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="take_camera_foto_ktpdiri_pengurus_2">
                                                                <label class="btn btn-success col-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_foto_ktpdiri_pengurus_2">
                                                                <div id="camera_ktpdiri_pengurus_2" style="z-index:-1 !important;">
                                                                </div>
                                                                <img id="user-guide" src="{{URL::to("assets/img/guide-diridanktp.png")}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktpdiri_pengurus_2(); $('#url_pic_brw_dengan_ktp_pengurus_2_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_diri_pengurus[]" id="image_foto_ktp_diri_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_brw_dengan_ktp_pengurus[]" id="url_pic_brw_dengan_ktp_pengurus_2" required>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_ktp_diri_pengurus_2">
                                                                </div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_brw_dengan_ktp_pengurus_2_val" name="url_pic_brw_dengan_ktp_pengurus_2_val" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_npwp_pengurus_2">Foto
                                                            NPWP Pengurus
                                                            <i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="take_camera_foto_npwp_pengurus_2">
                                                                <label class="btn btn-success col-md-11"><i class="fa fa-camera">
                                                                        Kamera</i></label>
                                                            </div>
                                                            <div class="col-md-12" id="webcam_foto_npwp_pengurus_2">
                                                                <div id="camera_npwp_pengurus_2" style="z-index:-1 !important;">
                                                                </div>
                                                                <img id="user-guide" src="{{URL::to("assets/img/guide-ktp.png")}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-12" style="margin-top:20px ; margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_npwp_pengurus_2(); $('#url_pic_npwp_pengurus_2_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_npwp_pengurus[]" id="image_foto_npwp_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_npwp_pengurus[]" id="url_pic_npwp_pengurus_2" required>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="font-weight-bold" style="margin-left:0">Hasil</label>
                                                                <div id="results_foto_npwp_pengurus_2">
                                                                </div>
                                                            </div><br />
                                                            <input type="text" class="input-hidden2" id="url_pic_npwp_pengurus_2_val" name="url_pic_npwp_pengurus_2_val" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Informasi Pengurus 2-->
                    <!-- START: Steps Navigation -->
                    <div class="block-content block-content-sm block-content-full bg-body-light">
                        <div class="row">
                            <div class="col-6 text-left">
                                <button type="reset" class="btn btn-alt-info">
                                    <i class="fa fa-refresh mr-5"></i> Atur Ulang
                                </button>

                            </div>
                            <div class="col-6 text-right">

                                <button type="button" id="btn_lengkapi_profile_p" class="btn btn-alt-primary">
                                    <i class="fa fa-check mr-5"></i> Kirim
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- END: Steps Navigation -->
                </div>
            </div>
        </div>
    </form>
    <!-- END: Form Badan Hukum -->
</div>