@extends('layouts.admin.master')
@section('title', 'Verifikasi Kelayakan Dana Rumah')
@section('content')

<!-- Main Container -->
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Verifikasi Kelayakan Dana Rumah</h1>
            </div>
        </div>
    </div>
</div>
<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            @if (session()->has('progressadd'))
            <div class="alert alert-danger">
                {{ session()->get('progressadd') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('updatedone'))
            <div class="alert alert-success">
                {{ session()->get('updatedone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @elseif(session()->has('createdone'))
            <div class="alert alert-info">
                {{ session()->get('createdone') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <div class="form-group" style="width:33%; float:left;">
                        <button type="button" class="btnProfile btn btn-default btn-lg btn-block btn-success disabled"><strong class="card-title">Profile Pribadi</strong></button>
                    </div>
                    <div class="form-group" style="width:33%; float:left;">
                        <button type="button" class="btnPengajuan btn btn-default btn-lg btn-block btn-success"><strong class="card-title">Informasi Objek Pendanan</strong></button>
                    </div>
                    <div class="form-group" style="width:33%; float:left;">
                        <button type="button" class="btnVerifikasi btn btn-default btn-lg btn-block btn-success"><strong class="card-title">Verifikasi Kelayakan</strong></button>
                    </div>
                </div>
                <!-- TAB Profile -->
                <div class="card-body tabprofile">
                    <div class="row">
                        <!-- START: Baris 1 -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h3" for="form_informasi_objek_Pendanaan">Informasi Pribadi &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Nama Lengkap Sesuai KTP <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="nama" maxlength="30" name="wizard-progress2-namapengguna" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Jenis Kelamin <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="jns_kelamin" name="jns_kelamin" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-ktp">Nomor KTP <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="ktp" name="wizard-progress2-ktp" readonly>
                            </div>
                        </div>

                        <!-- END: Baris 1 -->

                        <!-- START: Baris 2 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-ktp">Nomor Kartu Keluarga (KK) <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="kk" name="wizard-progress2-kk" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tempatlahir">Tempat Lahir
                                    <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="tempat_lahir" name="wizard-progress2-tempatlahir" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tempatlahir">Tanggal Lahir
                                    <i class="text-danger">*</i></label>
                                <input class="form-control" type="date" id="tanggal_lahir" name="wizard-progress2-tanggallahir" placeholder="Masukkan tanggal lahir" readonly>
                            </div>
                        </div>
                        <!-- END: Baris 2 -->

                        <!-- START: Baris 3 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="lbl_no_hp">Nomor Telepon <i class="text-danger">*</i></label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-dsi">
                                            +62 </span>
                                    </div>
                                    <input class="form-control checkNOHP validasiString" type="text" id="telepon" name="telepon" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="col-12">Agama <i class="text-danger">*</i></label>
                                <div class="col-12">
                                    <input class="form-control" type="text" id="agama" name="agama" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-pendidikanterakhir">Pendidikan
                                    Terakhir <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="pendidikan_terakhir" name="pendidikan_terakhir" readonly>
                            </div>
                        </div>
                        <!-- END: Baris 3 -->

                        <!-- START: Baris 4 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="wizard-progress2-ibukandung">Nama Gadis Ibu Kandung</label>
                                    <input class="form-control" type="text" id="ibukandung" name="ibukandung" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-npwp">Nomor NPWP</label>
                                <input class="form-control " type="text" id="npwp" name="npwp" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="col-12">Status Perkawinan <i class="text-danger">*</i></label>
                                <div class="col-12">
                                    <input class="form-control " type="text" id="status_kawin" name="status_kawin" readonly>
                                </div>
                            </div>
                        </div>
                        <!-- END: Baris 4 -->

                        <!-- START: Baris 5 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-alamat">Alamat Sesuai KTP <i class="text-danger">*</i></label>
                                <textarea class="form-control" maxlength="90" id="alamat" name="alamat" rows="3" readonly>{{ $alamat }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Provinsi <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" id="provinsi" name="provinsi" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kota">Kota/Kabupaten <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" id="kota" name="kota" disabled>
                            </div>
                        </div>
                        <!-- END: Baris 5 -->

                        <!-- START: Baris 6 -->

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kecamatan">Kecamatan <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" id="kecamatan" name="kecamatan" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kelurahan">Kelurahan <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" id="kelurahan" name="kelurahan" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kode_pos">Kode Pos <i class="text-danger">*</i></label>
                                <input type="text" class="form-control" id="kode_pos" name="kode_pos" disabled>
                            </div>
                        </div>
                        <!-- END: Baris 6 -->

                        <!-- START: Baris 7 -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-status_rumah">Status Kepemilikan Rumah <i class="text-danger">*</i></label>
                                <input class="form-control" type="text" id="status_rumah" name="status_rumah" disabled>
                            </div>
                        </div>

                        <!-- END: Baris 7 -->

                        <!-- START: Alamat Domisili Title -->

                        <div class="col-12 mb-4 blokDomisili d-none">
                            <br>
                            <div class="form-check form-check-inline line">
                                <!-- <input class="form-check-input" type="checkbox" name="domisili_status" id="domisili_status" value="1"> -->
                                <label class="form-check-label text-black h5" for="domisili_status">Alamat Domisili
                                    &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <!-- END: Alamat Domisili Title -->

                        <!-- START: Alamat Domisili -->
                        <div class="col-12 blokDomisili d-none">
                            <div class="row">
                                <!-- START: Baris 8 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-alamat">Alamat</label>
                                        <textarea class="form-control " maxlength="90" id="domisili_alamat" name="domisili_alamat" rows="3" readonly></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-namapengguna">Provinsi</label>
                                        <input type="text" class="form-control" id="domisili_provinsi" name="domisili_provinsi" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kota">Kota/Kabupaten</label>
                                        <input type="text" class="form-control" id="domisili_kota" name="domisili_kota" disabled>
                                    </div>
                                </div>
                                <!-- END: Baris 8 -->

                                <!-- START: Baris 9 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kecamatan">Kecamatan</label>
                                        <input type="text" class="form-control" id="domisili_kecamatan" name="domisili_kecamatan" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kelurahan">Kelurahan</label>
                                        <input type="text" class="form-control" id="domisili_kelurahan" name="domisili_kelurahan" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kode_pos">Kode
                                            Pos</label>
                                        <input type="text" class="form-control" id="domisili_kode_pos" name="domisili_kode_pos" disabled>
                                    </div>
                                </div>
                                <!-- END: Baris 9 -->

                                <!-- START: Baris 10 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-status_kepemilikan_rumah">Status Kepemilikan
                                            Rumah</label>
                                        <input class="form-control" type="text" id="domisili_status_rumah" name="domisili_status_rumah" disabled>
                                    </div>
                                </div>
                                <!-- END: Baris 10 -->
                            </div>
                        </div>
                        <!-- END: Alamat Domisili -->

                        <!-- START: Rekening Rekanan Title -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h3" for="informasi_rekening">Informasi
                                    Rekening &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <!-- END: Rekening Rekanan Title -->

                        <!-- START: Rekening Rekanan -->
                        <div class="col-12">
                            <div class="row">
                                <!-- START: Baris 11 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-norekening">Nomor Rekening <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" id="norekening" name="norekening" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-namapemilikrekening">Nama Pemilik Rekening <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" id="namapemilikrekening" name="namapemilikrekening" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-bank">Nama Bank <i class="text-danger">*</i></label>
                                        <input type="text" class="form-control" id="bank" name="bank" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-bank">Kantor Cabang Pembuka <i class="text-danger">*</i></label>
                                        <input type="text" class="form-control" id="kantorcabangpembuka" name="kantorcabangpembuka" readonly>
                                    </div>
                                </div>
                                <!-- END: Baris 11 -->
                            </div>
                        </div>
                        <!-- END: Rekening Rekanan -->

                        <!-- START: Rekening Rekanan Title -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h3" for="informasi_penghasilan">Informasi
                                    Pekerjaan</label>
                            </div>
                            <hr>
                        </div>
                        <!-- END: Rekening Rekanan Title -->

                        <!-- START: Baris 12 -->
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-pekerjaan">Jenis Pekerjaan <i class="text-danger">*</i></label>
                                        <input type="text" class="form-control" id="jenis_pekerjaan" name="jenis_pekerjaan" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-bidang_pekerjaan">Sumber Pengembalian Dana<i class="text-danger">*</i></label>
                                        <input type="text" class="form-control" id="sumberpengembaliandana" name="sumberpengembaliandana" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-bidang_pekerjaan">Skema Pembiayaan<i class="text-danger">*</i></label>
                                        <input type="text" class="form-control" id="skemapembiayaan" name="skemapembiayaan" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END: Baris 12 -->

                        <!-- hidden form -->
                        <div class="blokFixedIncome">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_nama_perusahaan" for="nama_perusahaan" class="ml-0">Nama
                                                Perusahaan <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="nama_perusahaan" maxlength="35" name="nama_perusahaan" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-bidang_online">Bidang Pekerjaan Online <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="bidang_online" maxlength="35" name="bidang_online" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="bentuk_badan_usaha" class="ml-0">Bentuk Badan Usaha <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="bentuk_badan_usaha" maxlength="35" name="bentuk_badan_usaha" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="status_kepegawaian" class="ml-0">Status Kepegawaian
                                        <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="status_kepegawaian" maxlength="35" name="status_kepegawaian" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label id="label_usia_perusahaan" for="usia_perusahaan" class="ml-0">Usia
                                        Perusahaan (Tahun)
                                        <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="usia_perusahaan" maxlength="35" name="usia_perusahaan" placeholder="Masukan usia perusahaan" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="departemen" class="ml-0">Departemen <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="departemen" maxlength="35" name="departemen" placeholder="Masukan nama departemen" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="jabatan" class="ml-0">Jabatan <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="jabatan" maxlength="35" name="jabatan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="ml-0">Lama Bekerja (Tahun) <i class="text-danger">*</i></label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                </div>
                                                <input type="number" id="tahun_bekerja" name="tahun_bekerja" class="form-control" aria-label="tahun_bekerja" aria-describedby="basic-addon1" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                </div>
                                                <input type="number" id="bulan_bekerja" name="bulan_bekerja" class="form-control" aria-label="bulan_bekerja" aria-describedby="basic-addon1" value="" readonly>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nip">NIP/NRP/NIK<i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="nip" name="nip" value="" readonly>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nama_hrd">Nama HRD <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="nama_hrd" maxlength="35" name="nama_hrd" placeholder="Masukkan nama hrd" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="no_fixed_line_hrd" class="ml-0">Nomor Fixed Line HRD <i class="text-danger">*</i></label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi">
                                                        +62 </span>
                                                </div>
                                                <input class="form-control" type="text" id="no_fixed_line_hrd" name="no_fixed_line_hrd" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="no_telpon_usaha" class="ml-0">No. Telepon Perusahaan <i class="text-danger">*</i></label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi"> +62
                                                    </span>
                                                </div>
                                                <input class="form-control" type="text" id="no_telpon_usaha" name="no_telpon_usaha" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label id="label_alamat_perusahaan" for="alamat_perusahaan" class="ml-0">Alamat
                                        Perusahaan/Tempat Usaha <i class="text-danger">*</i> </label>
                                    <textarea class="form-control" id="alamat_perusahaan" name="alamat_perusahaan" rows="3" readonly></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="rt_perusahaan" class="ml-0">RT <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="rt_perusahaan" name="rt_perusahaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="rw_perusahaan" class="ml-0">RW <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="rw_perusahaan" name="rw_perusahaan" readonly>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="provinsi_perusahaan" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                            <input class="form-control" maxlength="90" id="provinsi_perusahaan" name="provinsi_perusahaan" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kabupaten_perusahaan" class="ml-0">Kabupaten <i class="text-danger">*</i></label>
                                            <input class="form-control" maxlength="90" id="kabupaten_perusahaan" name="kabupaten_perusahaan" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kecamatan_perusahaan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                            <input class="form-control" maxlength="90" id="kecamatan_perusahaan" name="kecamatan_perusahaan" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kelurahan_perusahaan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="kelurahan_perusahaan" name="kelurahan_perusahaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kodepos_perusahaan" class="ml-0">Kode Pos <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="kodepos_perusahaan" name="kodepos_perusahaan" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="ml-0">Pengalaman Kerja Di Tempat Lain <i class="text-danger">*</i></label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                </div>
                                                <input type="number" id="tahun_pengalaman_bekerja" name="tahun_pengalaman_bekerja" class="form-control" aria-label="tahun_pengalaman_bekerja" aria-describedby="basic-addon1" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                </div>
                                                <input type="number" id="bulan_pengalaman_bekerja" name="bulan_pengalaman_bekerja" class="form-control" aria-label="bulan_pengalaman_bekerja" aria-describedby="basic-addon1" value="" readonly>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="penghasilan" class="ml-0">Penghasilan (Per Bulan) <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="penghasilan" name="penghasilan" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="biaya_hidup" class="ml-0">Biaya Hidup (Per Bulan) <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="biaya_hidup" name="biaya_hidup" readonly>
                                </div>
                            </div>
                        </div>
                        <!-- bloknonfixedincome -->
                        <div class="blokNonFixedIncome d-none">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label id="label_nama_perusahaan" for="nama_usaha_non" class="ml-0">Nama
                                        Usaha/Profesional (Dokter dll) <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="nama_perusahaan_non" maxlength="35" name="nama_perusahaan_non" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="wizard-progress2-bidang_online">Bidang Pekerjaan Online <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="bidang_online_non" maxlength="35" name="bidang_online_non" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="penghasilan" class="ml-0">Lama Usaha/Praktek (Tahun) <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="lama_usaha_non" name="lama_usaha_non" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="penghasilan" class="ml-0">Lama Tempat Usaha/Praktek (Tahun) <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="lama_tempat_usaha_non" name="lama_tempat_usaha_non" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="no_telpon_non" class="ml-0">No. Telepon Usaha/Praktek <i class="text-danger">*</i></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text input-group-text-dsi"> +62
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" id="no_telpon_non" name="no_telpon_non" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="no_hp_non" class="ml-0">No. Hp Usaha/Praktek <i class="text-danger">*</i></label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <span class="input-group-text input-group-text-dsi"> +62
                                            </span>
                                        </div>
                                        <input class="form-control" type="text" id="no_hp_non" name="no_hp_non" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label id="label_alamat_perusahaan" for="alamat_perusahaan_non" class="ml-0">Alamat
                                        Usaha/Praktek <i class="text-danger">*</i> </label>
                                    <textarea class="form-control" id="alamat_perusahaan_non" name="alamat_perusahaan_non" rows="3" readonly></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="rt_perusahaan" class="ml-0">RT <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="rt_perusahaan_non" name="rt_perusahaan_non" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="rw_perusahaan" class="ml-0">RW <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="rw_perusahaan_non" name="rw_perusahaan_non" readonly>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="provinsi_perusahaan" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                            <input class="form-control" maxlength="90" id="provinsi_perusahaan_non" name="provinsi_perusahaan_non" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kabupaten_perusahaan" class="ml-0">Kabupaten <i class="text-danger">*</i></label>
                                            <input class="form-control" maxlength="90" id="kabupaten_perusahaan_non" name="kabupaten_perusahaan_non" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kecamatan_perusahaan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                            <input class="form-control" maxlength="90" id="kecamatan_perusahaan_non" name="kecamatan_perusahaan_non" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kelurahan_perusahaan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="kelurahan_perusahaan_non" name="kelurahan_perusahaan_non" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="kodepos_perusahaan" class="ml-0">Kode Pos <i class="text-danger">*</i></label>
                                    <input class="form-control" maxlength="90" id="kodepos_perusahaan_non" name="kodepos_perusahaan_non" readonly>
                                </div>
                            </div>
                            <div class="col-md-4 bloksuratIjin">
                                <div class="form-group">
                                    <label for="penghasilan" class="ml-0">Surat Ijin Usaha <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="surat_ijin_non" name="surat_ijin_non" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4 bloksuratIjin">
                                <div class="form-group">
                                    <label for="penghasilan" class="ml-0">Nomor Ijin Usaha <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="nomor_ijin_non" name="nomor_ijin_non" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="penghasilan" class="ml-0">Penghasilan Per Bulan <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="penghasilan_non" name="penghasilan_non" value="" readonly>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="biaya_hidup" class="ml-0">Biaya Hidup Per Bulan <i class="text-danger">*</i></label>
                                    <input class="form-control" type="text" id="biaya_hidup_non" name="biaya_hidup_non" readonly>
                                </div>
                            </div>
                        </div>
                        <!-- // bloknonFixedIncome -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label id="label_nilai_spt" for="nilai_spt" class="ml-0">Nilai SPT Tahun Terakhir
                                    <i class="text-danger">*</i></label>
                                <input class="form-control " maxlength="90" id="nilai_spt" name="nilai_spt" placeholder="Masukan nilai Spt" readonly>
                            </div>
                        </div>
                        <div class="col-12 mb-4 blokDetailPenghasilan">
                            <br>
                            <div class="form-check form-check-inline line">
                                <!-- <input class="form-check-input" type="checkbox" name="penghasilan_lain_lain" id="penghasilan_lain_lain" value="1"> -->
                                <label class="form-check-label text-black h5" for="penghasilan_lain_lain">Penghasilan
                                    Lain Lain &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-4 blokDetailPenghasilan">
                            <div class="form-group">
                                <label for="penghasilan_lain_lain" class="ml-0">Detail Penghasilan Lain Lain <i class="text-danger">*</i></label>
                                <textarea class="form-control " maxlength="90" id="detail_penghasilan_lain_lain" name="detail_penghasilan_lain_lain" rows="3" readonly></textarea>
                            </div>
                        </div>
                        <div class="col-md-4 blokDetailPenghasilan">
                            <div class="form-group">
                                <label for="total_penghasilan_lain_lain" class="ml-0">Total Penghasilan Lain Lain Per
                                    Bulan <i class="text-danger">*</i></label>
                                <input class="form-control " maxlength="90" id="total_penghasilan_lain_lain" name="total_penghasilan_lain_lain" placeholder="Masukan total penghasilan lain lain" value="" readonly>
                            </div>
                        </div>
                        <!-- end hidden form -->

                        <!-- START: Foto Title -->
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h3" for="foto">Foto &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <!-- END: Foto Title -->

                        <!-- START: Foto -->
                        <div class="col-md-12">
                            <div class="col-md-3 col-sm-6 imgUp">
                                <div class="col imgUp" id="foto_diri">
                                    <div class="d-flex flex-column">
                                        <div class="mx-auto">
                                            <label class="font-weight-bold ml-0 flex-row">Foto
                                                Diri <i class="text-danger">*</i></label>
                                        </div>
                                        <div class="mx-auto">
                                            <img class="imagePreview img-fluid rounded" src="{{ !empty($brw_pic) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_pic)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}" width="150px" height="145px">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 imgUp">
                                <div class="col imgUp text-center" id="foto_ktp">
                                    <div class="d-flex flex-column">
                                        <div class="mx-auto">
                                            <label class="font-weight-bold ml-0">Foto KTP <i class="text-danger">*</i></label>
                                        </div>
                                        <div class="mx-auto">
                                            <img class="imagePreview img-fluid rounded" src="{{ !empty($brw_pic_ktp) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_pic_ktp)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}" width="150px" height="145px">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 imgUp">
                                <div class="col imgUp text-center" id="foto_ktp_diri">
                                    <div class="d-flex flex-column">
                                        <div class="mx-auto">
                                            <label class="font-weight-bold ml-0">Foto Diri &
                                                KTP <i class="text-danger">*</i></label>
                                        </div>
                                        <div class="mx-auto">
                                            <img class="imagePreview img-fluid rounded" src="{{ !empty($brw_pic_user_ktp) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_pic_user_ktp)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}" width="150px" height="145px">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 imgUp">
                                <div class="col imgUp text-center" id="foto_npwp">
                                    <div class="d-flex flex-column">
                                        <div class="mx-auto">
                                            <label class="font-weight-bold ml-0">Foto NPWP
                                                <i class="text-danger">*</i></label>
                                        </div>
                                        <div class="mx-auto">
                                            <img class="imagePreview img-fluid rounded" src="{{ !empty($brw_pic_npwp) ? route('getUserFileForAdmin', ['filename' => str_replace('/', ':', $brw_pic_npwp)]) . '?t=' . date('Y-m-d h:i:sa') : '' }}" width="150px" height="145px">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- mulai informasi pasangan -->
                        <div class="row blokPasangan d-none">
                            <div class="col-md-12">
                                <div class="col-12 mb-4">
                                    <br><br>
                                    <div class="form-check form-check-inline line">
                                        <label class=" text-black h3" for="informasi_pasangan">Informasi
                                            Pasangan</label>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                            <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama" class="ml-0" for="">Nama Lengkap Sesuai KTP <i class="text-danger">*</i></label>
                                        <input class="form-control checkKarakterAneh" type="text" maxlength="35" id="pasangan_nama" name="pasangan_nama" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama" class="ml-0" for="">Jenis Kelamin<i class="text-danger">*</i></label>
                                        <input class="form-control checkKarakterAneh" type="text" maxlength="35" id="pasangan_jenis_kelamin" name="pasangan_jenis_kelamin" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="ktp" class="ml-0">No. KTP <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" id="pasangan_ktp" name="pasangan_ktp" readonly>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->
                            <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0" for="tempat_lahir">Tempat Lahir <i class="text-danger">*</i></label>
                                        <input class="form-control" maxlength="35" type="text" id="pasangan_tempat_lahir" name="pasangan_tempat_lahir" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0" for="tanggal_lahir">Tanggal Lahir
                                            <i class="text-danger">*</i></label>
                                        <input class="form-control" type="date" id="pasangan_tanggal_lahir" name="pasangan_tanggal_lahir" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">No. Hp <i class="text-danger">*</i></label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-dsi">
                                                    +62 </span>
                                            </div>
                                            <input class="form-control" type="text" id="pasangan_telepon" name="pasangan_telepon" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->
                            <!-- <div class="row"> -->
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">Agama <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" id="pasangan_agama" name="pasangan_agama" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0" for="pasangan_pendidikan_terakhir">Pendidikan
                                            Terakhir <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" id="pasangan_pendidikan_terakhir" name="pasangan_pendidikan_terakhir" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="npwp_pasangan">No. NPWP <i class="text-danger">*</i> </label>
                                        <input class="form-control " type="text" id="pasangan_npwp" name="pasangan_npwp" minlength="15" maxlength="15" pattern=".{15,15}" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" placeholder="Masukkan nomor NPWP" value="" readonly>
                                    </div>
                                </div>
                            </div>
                            <!-- </div> -->

                            <div class="blokPekerjaanPasangan d-none">
                                <div class="col-md-12">
                                    <div class="col-12 mb-4">
                                        <br><br>
                                        <div class="form-check form-check-inline line">
                                            <label class="form-check-label text-black h3" for="informasi_pekerjaan_pasangan">Informasi Pekerjaan Pasangan</label>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_nama_perusahaan" for="nama_perusahaan" class="ml-0">Jenis
                                                Pekerjaan<i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="pekerjaan_pasangan" maxlength="35" name="pekerjaan_pasangan" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="bentuk_badan_usaha" class="ml-0">Bidang Pekerjaan<i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="bidang_pekerjaan_pasangan" maxlength="35" name="bidang_pekerjaan_pasangan" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="status_kepegawaian" class="ml-0">Bidang Pekerjaan Online
                                                <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="bidang_online_pasangan" maxlength="35" name="bidang_online_pasangan" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="blokFixedIncomePasangan d-none">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="label_nama_perusahaan" for="nama_perusahaan" class="ml-0">Nama Perusahaan <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="nama_perusahaan_pasangan" maxlength="35" name="nama_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="bentuk_badan_usaha" class="ml-0">Bentuk Badan Usaha <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="bentuk_badan_usaha_pasangan" maxlength="35" name="bentuk_badan_usaha_pasangan" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="status_kepegawaian" class="ml-0">Status Kepegawaian
                                                    <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="status_kepegawaian_pasangan" maxlength="35" name="status_kepegawaian_pasangan" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="label_usia_perusahaan" for="usia_perusahaan" class="ml-0">Usia
                                                    Perusahaan (Tahun)
                                                    <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="usia_perusahaan_pasangan" maxlength="35" name="usia_perusahaan_pasangan" placeholder="Masukan usia perusahaan" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="departemen" class="ml-0">Departemen <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="departemen_pasangan" maxlength="35" name="departemen_pasangan" placeholder="Masukan nama departemen" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="jabatan" class="ml-0">Jabatan <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="jabatan_pasangan" maxlength="35" name="jabatan_pasangan" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="ml-0">Lama Bekerja (Tahun) <i class="text-danger">*</i></label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                            </div>
                                                            <input type="number" id="tahun_bekerja_pasangan" name="tahun_bekerja_pasangan" class="form-control" aria-label="tahun_bekerja_pasangan" aria-describedby="basic-addon1" value="" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                            </div>
                                                            <input type="number" id="bulan_bekerja_pasangan" name="bulan_bekerja_pasangan" class="form-control" aria-label="bulan_bekerja_pasangan" aria-describedby="basic-addon1" value="" readonly>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="nip" class="ml-0">NIP/NRP/NIK<i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="nip_pasangan" name="nip_pasangan" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="nama_hrd">Nama HRD <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="nama_hrd_pasangan" maxlength="35" name="nama_hrd_pasangan" placeholder="Masukkan nama hrd" value="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="no_fixed_line_hrd" class="ml-0">Nomor Fixed Line HRD <i class="text-danger">*</i></label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi">
                                                            +62 </span>
                                                    </div>
                                                    <input class="form-control" type="text" id="no_fixed_line_hrd_pasangan" name="no_fixed_line_hrd_pasangan" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="no_telpon_usaha" class="ml-0">No. Telepon Perusahaan <i class="text-danger">*</i></label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi"> +62
                                                        </span>
                                                    </div>
                                                    <input class="form-control" type="text" id="no_telpon_usaha_pasangan" name="no_telpon_usaha_pasangan" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="label_alamat_perusahaan" for="alamat_perusahaan" class="ml-0">Alamat
                                                    Perusahaan/Tempat Usaha <i class="text-danger">*</i> </label>
                                                <textarea class="form-control" id="alamat_perusahaan_pasangan" name="alamat_perusahaan_pasangan" rows="3" readonly></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rt_perusahaan" class="ml-0">RT <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="rt_perusahaan_pasangan" name="rt_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rw_perusahaan" class="ml-0">RW <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="rw_perusahaan_pasangan" name="rw_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="provinsi_perusahaan" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="provinsi_perusahaan_pasangan" name="provinsi_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kabupaten_perusahaan" class="ml-0">Kabupaten <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kabupaten_perusahaan_pasangan" name="kabupaten_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kecamatan_perusahaan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kecamatan_perusahaan_pasangan" name="kecamatan_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kelurahan_perusahaan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kelurahan_perusahaan_pasangan" name="kelurahan_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kodepos_perusahaan" class="ml-0">Kode Pos <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kodepos_perusahaan_pasangan" name="kodepos_perusahaan_pasangan" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="ml-0">Pengalaman Kerja Di Tempat Lain <i class="text-danger">*</i></label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                            </div>
                                                            <input type="number" id="tahun_pengalaman_bekerja_pasangan" name="tahun_pengalaman_bekerja_pasangan" class="form-control" aria-label="tahun_pengalaman_bekerja" aria-describedby="basic-addon1" value="" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                            </div>
                                                            <input type="number" id="bulan_pengalaman_bekerja_pasangan" name="bulan_pengalaman_bekerja_pasangan" class="form-control" aria-label="bulan_pengalaman_bekerja" aria-describedby="basic-addon1" value="" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="penghasilan" class="ml-0">Penghasilan (Per Bulan) <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="penghasilan_pasangan" name="penghasilan_pasangan" value="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="biaya_hidup" class="ml-0">Biaya Hidup (Per Bulan) <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="biaya_hidup_pasangan" name="biaya_hidup_pasangan" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="blokNonFixedIncomePasangan d-none">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="label_nama_perusahaan" for="nama_usaha_non" class="ml-0">Nama
                                                    Usaha/Profesional (Dokter dll) <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="nama_perusahaan_pasangan_non" maxlength="35" name="nama_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="penghasilan" class="ml-0">Lama Usaha/Praktek (Tahun) <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="lama_usaha_pasangan_non" name="lama_usaha_pasangan_non" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="penghasilan" class="ml-0">Lama Tempat Usaha/Praktek (Tahun)
                                                    <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="lama_tempat_usaha_pasangan_non" name="lama_tempat_usaha_pasangan_non" value="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="no_telpon_non" class="ml-0">No. Telepon Usaha/Praktek <i class="text-danger">*</i></label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi"> +62
                                                        </span>
                                                    </div>
                                                    <input class="form-control" type="text" id="no_telpon_pasangan_non" name="no_telpon_pasangan_non" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="no_hp_non" class="ml-0">No. Hp Usaha/Praktek <i class="text-danger">*</i></label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi"> +62
                                                        </span>
                                                    </div>
                                                    <input class="form-control" type="text" id="no_hp_pasangan_non" name="no_hp_pasangan_non" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="label_alamat_perusahaan" for="alamat_perusahaan_non" class="ml-0">Alamat
                                                    Usaha/Praktek <i class="text-danger">*</i> </label>
                                                <textarea class="form-control" id="alamat_perusahaan_pasangan_non" name="alamat_perusahaan_pasangan_non" rows="3" readonly></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rt_perusahaan" class="ml-0">RT <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="rt_perusahaan_pasangan_non" name="rt_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="rw_perusahaan" class="ml-0">RW <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="rw_perusahaan_pasangan_non" name="rw_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="provinsi_perusahaan" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="provinsi_perusahaan_pasangan_non" name="provinsi_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kabupaten_perusahaan" class="ml-0">Kabupaten <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kabupaten_perusahaan_pasangan_non" name="kabupaten_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kecamatan_perusahaan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kecamatan_perusahaan_pasangan_non" name="kecamatan_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kelurahan_perusahaan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kelurahan_perusahaan_pasangan_non" name="kelurahan_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="kodepos_perusahaan" class="ml-0">Kode Pos <i class="text-danger">*</i></label>
                                                <input class="form-control" maxlength="90" id="kodepos_perusahaan_pasangan_non" name="kodepos_perusahaan_pasangan_non" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4 bloksuratIjinPasangan">
                                            <div class="form-group">
                                                <label for="penghasilan" class="ml-0">Surat Ijin Usaha <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="surat_ijin_pasangan_non" name="surat_ijin_pasangan_non" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4 bloksuratIjinPasangan">
                                            <div class="form-group">
                                                <label for="penghasilan" class="ml-0">Nomor Ijin Usaha <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="nomor_ijin_pasangan_non" name="nomor_ijin_pasangan_non" value="" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="penghasilan" class="ml-0">Penghasilan Per Bulan <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="penghasilan_pasangan_non" name="penghasilan_pasangan_non" value="" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="biaya_hidup" class="ml-0">Biaya Hidup Per Bulan <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" id="biaya_hidup_pasangan_non" name="biaya_hidup_pasangan_non" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end blok pekerjaan pasangan -->
                            <div class="col-md-12">
                                <div class="col-12 mb-4">
                                    <br>
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h5" for="pasangan_domisili_status">Alamat Pasangan &nbsp</label>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-alamat">Alamat</label>
                                        <textarea class="form-control" maxlength="90" id="pasangan_alamat" name="pasangan_alamat" rows="3" readonly></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-namapengguna">Provinsi</label>
                                        <input type="text" class="form-control" id="pasangan_provinsi" name="pasangan_provinsi" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kota">Kota/Kabupaten</label>
                                        <input type="text" class="form-control" id="pasangan_kota" name="pasangan_kota" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kecamatan">Kecamatan</label>
                                        <input type="text" class="form-control" id="pasangan_kecamatan" name="pasangan_kecamatan" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kelurahan">Kelurahan</label>
                                        <input type="text" class="form-control" id="pasangan_kelurahan" name="pasangan_kelurahan" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="wizard-progress2-kode_pos">Kode
                                            Pos</label>
                                        <input type="text" class="form-control" id="pasangan_kode_pos" name="pasangan_kode_pos" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end informasi pasangan -->

                    <!-- button lanjut -->
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-10">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-block btn-success btn-lg" id="selanjutnya1">Selanjutnya
                            &nbsp; <i class="fa fa-fw fa-chevron-right"></i></button>
                    </div>

                    <!-- end button lanjut -->
                </div>

                <!-- END TAB Profile -->
                <!-- TAB PENGAJUAN -->
                <div class="card-body tabpengajuan d-none">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan   ">Form Informasi Objek Pendanaan &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Tipe Pendanaan</label><label class="mandatory_label">*</label>
                                <input class="form-control" id="type_pendanaan" name="type_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Tujuan Pendanaan</label><label class="mandatory_label">*</label>
                                <input class="form-control" id="tujuan_pendanaan" name="tujuan_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-tempatlahir">Alamat Objek Pendanaan</label> <label class="mandatory_label">*</label>
                                <input class="form-control" id="alamat_objek_pendanaan" name="alamat_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Provinsi</label>
                                <label class="mandatory_label">*</label>
                                <input class="form-control" id="provinsi_objek_pendanaan" name="provinsi_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-kota">Kota / Kabupaten</label> <label class="mandatory_label">*</label>
                                <input class="form-control" id="kota_objek_pendanaan" name="kota_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                <label class="mandatory_label">*</label>
                                <input class="form-control" id="kecamatan_objek_pendanaan" name="kecamatan_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                <label class="mandatory_label">*</label>
                                <input class="form-control" id="kelurahan_objek_pendanaan" name="kelurahan_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Kode
                                    Pos</label> <label class="mandatory_label">*</label>
                                <input class="form-control" id="kodepos_objek_pendanaan" name="kodepos_objek_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Harga Objek Pendanaan </label> <label class="mandatory_label">*</label>
                                <input type="text" class="form-control" id="harga_objek_pendanaan" name="harga_objek_pendanaan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Uang Muka </label> <label class="mandatory_label">*</label>
                                <input type="text" class="form-control" id="uang_muka" name="uang_muka" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Nilai Pengajuan Pendanaan </label> <label class="mandatory_label">*</label>
                                <input type="text" class="form-control" id="nilai_pengajuan" name="nilai_pengajuan" readonly>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Jangka Waktu (Bulan) </label> <label class="mandatory_label">*</label>
                                <input type="text" class="form-control" id="jangka_waktu" name="jangka_waktu" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Detail Informasi Objek Pendanaan</label>
                                <textarea class="form-control" rows="4" cols="80" id="detail_pendanaan" name="detail_pendanaan" disabled></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h5" for="informasi_pemilik_objek_Pendanaan">Informasi Pemilik Objek Pendanaan
                                    &nbsp</label>
                            </div>
                            <hr>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Nama</label>
                                <input class="form-control" id="nama_pemilik" name="nama_pemilik" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">No. Telepon/HP</label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <span class="input-group-text input-group-text-dsi">
                                            +62 </span>
                                    </div>
                                    <input class="form-control" id="tlp_pemilik" name="tlp_pemilik" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Alamat</label>
                                <textarea class="form-control" rows="4" cols="80" id="alamat_pemilik" name="alamat_pemilik" disabled></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Provinsi</label>
                                <input class="form-control" id="provinsi_pemilik" name="provinsi_pemilik" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Kota/Kabupaten</label>
                                <input class="form-control" id="kota_pemilik" name="kota_pemilik" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Kecamatan</label>
                                <input class="form-control" id="kecamatan_pemilik" name="kecamatan_pemilik" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Kelurahan</label>
                                <input class="form-control" id="kelurahan_pemilik" name="kelurahan_pemilik" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="wizard-progress2-namapendanaan">Kode Pos</label>
                                <input class="form-control" id="kodepos_pemilik" name="kodepos_pemilik" disabled>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-block btn-success btn-lg" id="sebelumnya2"><i class="fa fa-fw fa-chevron-left"></i>Sebelumnya</button>
                    </div>
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <button type="submit" class="btn btn-block btn-success btn-lg" id="selanjutnya2">Selanjutnya
                            &nbsp; <i class="fa fa-fw fa-chevron-right"></i></button>
                    </div>
                </div>
                <!-- TAB PENGAJUAN SELESAI-->
                <!-- TAB VERIFIKASI -->
                <div class="card-body tabverifikasi d-none">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <br>
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h5" for="form_informasi_objek_Pendanaan   ">Form Verifikasi Kelayakan &nbsp</label>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_informasi_objek_Pendanaan   ">Pefindo &nbsp</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Nilai &nbsp</label><label class="mandatory_label" style="color:red;">*</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="number" min='1' max="5000" id="nilai_pefindo" name="nilai_pefindo" placeholder="ketik disini.." required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="wizard-progress2-namapengguna">Grade &nbsp</label><label class="mandatory_label" style="color:red;">*</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control custom-select" name="grade_pefindo" id="grade_pefindo" required>
                                    <option value="" active> Pilih </option>
                                    <option value="A1"> A1 </option>
                                    <option value="A2"> A2 </option>
                                    <option value="A3"> A3 </option>
                                    <option value="B1"> B1 </option>
                                    <option value="B2"> B2 </option>
                                    <option value="B3"> B3 </option>
                                    <option value="C1"> C1 </option>
                                    <option value="C2"> C2 </option>
                                    <option value="C3"> C3 </option>
                                    <option value="D1"> D1 </option>
                                    <option value="D2"> D2 </option>
                                    <option value="D3"> D3 </option>
                                    <option value="E1"> E1 </option>
                                    <option value="E2"> E2 </option>
                                    <option value="E3"> E3 </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="form_informasi_objek_Pendanaan">CredoLab &nbsp</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="skor_personal">Skor Personal &nbsp</label><label class="mandatory_label" style="color:red;">*</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" id="skor_personal" name="skor_personal" readonly>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label for="skor_pendanaan">Skor Pendanaan &nbsp</label><label class="mandatory_label" style="color:red;">*</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" id="skor_pendanaan" name="skor_pendanaan" readonly>
                            </div>
                        </div>
                    </div>
                    <!-- verijelas -->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-check form-check-inline line">
                                <label class="form-check-label text-black h6" for="verifikasi_data_verijelas">Verifikasi
                                    Data Verijelas &nbsp</label><label class="form-check-label">:</label>
                            </div>
                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="checkbox" id="complete_id" value="complete_id" name="verijelas">
                                <label for="complete_id"> COMPLETE ID</label> <br>
                                <input type="checkbox" id="ocr" value="ocr" name="verijelas">
                                <label for="ocr"> OCR </label><br>
                                <input type="checkbox" id="npwp_verify" value="npwp_verify" name="verijelas">
                                <label for="npwp_verify"> NPWP</label><br>
                                {{-- <input type="checkbox" id="npwpa" value="npwpa" name="verijelas">
                                    <label for="npwpa"> NPWP A </label><br>
                                    <input type="checkbox" id="npwpc" value="npwpc" name="verijelas">
                                    <label for="npwpc"> NPWP C </label><br> --}}
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                {{-- <input type="checkbox" id="npwpd" value="npwpd" name="verijelas">
                                    <label for="npwpd"> NPWP D </label><br> --}}
                                <input type="checkbox" id="workplace_verification_f" value="workplace_verification_f" name="verijelas">
                                <label for="workplace_verification_f"> Workplace Verification F </label><br>
                                <input type="checkbox" id="ngetive_list_verification_j" value="ngetive_list_verification_j" name="verijelas">
                                <label for="ngetive_list_verification_j"> Negative List Verification J </label><br>
                                {{-- <input type="checkbox" id="verify_property_k" value="verify_property_k"
                                        name="verijelas">
                                    <label for="verify_property_k"> Verify Property K </label><br> --}}
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                            {{-- <button type="button" class="btn btn-block btn-primary btn-sm" id="checkVeriJelas"> <i
                                        class="fa fa-check" aria-hidden="true"></i> Cek Verijelas</button> --}}
                            <button type="button" data-toggle="modal" data-target="#loading_modal" data-backdrop="static" data-keyboard="false" href="#" class="btn btn-block btn-primary btn-sm"> <i class="fa fa-check" aria-hidden="true"></i>
                                Cek Verijelas </button>
                        </div>
                        <div class="col-md-2">
                            <button type="button" data-toggle="modal" data-target="#verijelas_modal" class="btn btn-block btn-success btn-sm" id="resultVeriJelas"> <i class="fa fa-folder" aria-hidden="true"></i> Hasil Verijelas</button>
                        </div>


                    </div>
                    <!-- verijelas -->
                    <div class="row py-3">
                        <div class="col-md-4">
                            <label for="ftv">Financing To Value (LTV/FTV) &nbsp</label><label class="mandatory_label" style="color:red;">*</label>
                            <div class="form-group">
                                <input class="form-control" type="number" min="1" max="100" id="ftv" name="ftv" placeholder="Prosentase %" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="catatan_verifikasi">Catatan Verifikasi Kelayakan &nbsp</label><label class="mandatory_label" style="color:red;">*</label>
                                <textarea class="form-control" rows="4" cols="80" id="catatan_verifikasi" name="catatan_verifikasi" required></textarea>
                            </div>
                            <br>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label for="keputusan"><strong>Berdasarkan hasil Verifikasi ini, maka keputusan analis
                                        PT. Dana Syariah Indonesia adalah &nbsp;</strong></label><label class="mandatory_label" style="color:red;">*</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" name="keputusan" id="keputusan" required>
                                    <option value="">Pilih</option>
                                    <option value="1">Lanjut</option>
                                    <option value="2">Tolak</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-block btn-lg btn-success" id="sebelumnya3">
                                <i class="fa fa-fw fa-angle-left"></i>&nbsp;Sebelumnya</button>
                        </div>
                        <div class="col-md-8">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-block btn-danger btn-lg" id="submitverfikasi">Kirim</button>
                        </div>
                    </div>
                </div>
                <!-- TAB VERIFIKASI SELESAI -->
            </div>

        </div>
    </div>
</div>

</div><!-- .content -->
<!-- Modal box -->
@include('layouts.modals.modal-loading')
@include('layouts.modals.modal-verijelas')
<!-- End modal box -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script> --}}
<script>
    $('#selanjutnya1').on('click', function() {
        $('.tabprofile').addClass('d-none');
        $('.tabpengajuan').removeClass('d-none');
        $('.btnProfile').removeClass('disabled');
        $('.btnPengajuan').addClass('disabled');
    });
    $('#selanjutnya2').on('click', function() {
        $('.tabpengajuan').addClass('d-none');
        $('.tabverifikasi').removeClass('d-none');
        $('.btnPengajuan').removeClass('disabled');
        $('.btnVerifikasi').addClass('disabled');
    });
    $('#sebelumnya2').on('click', function() {
        $('.tabpengajuan').addClass('d-none');
        $('.tabprofile').removeClass('d-none');
        $('.btnPengajuan').removeClass('disabled');
        $('.btnProfile').addClass('disabled');
    });
    $('#sebelumnya3').on('click', function() {
        $('.tabverifikasi').addClass('d-none');
        $('.tabpengajuan').removeClass('d-none');
        $('.btnVerifikasi').removeClass('disabled');
        $('.btnPengajuan').addClass('disabled');
    });
    $('.btnProfile').on('click', function() {
        $('.tabprofile').removeClass('d-none');
        $('.tabpengajuan').addClass('d-none');
        $('.tabverifikasi').addClass('d-none');

        $('.btnProfile').addClass('disabled');
        $('.btnPengajuan').removeClass('disabled');
        $('.btnVerifikasi').removeClass('disabled');
    });
    $('.btnPengajuan').on('click', function() {
        $('.tabprofile').addClass('d-none');
        $('.tabpengajuan').removeClass('d-none');
        $('.tabverifikasi').addClass('d-none');

        $('.btnProfile').removeClass('disabled');
        $('.btnPengajuan').addClass('disabled');
        $('.btnVerifikasi').removeClass('disabled');
    });
    $('.btnVerifikasi').on('click', function() {
        $('.tabprofile').addClass('d-none');
        $('.tabpengajuan').addClass('d-none');
        $('.tabverifikasi').removeClass('d-none');

        $('.btnProfile').removeClass('disabled');
        $('.btnPengajuan').removeClass('disabled');
        $('.btnVerifikasi').addClass('disabled');
    });

    $('#submitverfikasi').on('click', function() {
        if ($('#nilai_pefindo').val() == "" || $('#grade_pefindo').val() == "" || $('#skor_personal').val() ==
            "" || $('#skor_pendanaan').val() == "" || $('#ftv').val() == "" || $('#catatan_verifikasi').val() ==
            "" || $('#keputusan').val() == "") {
            alert('Pastikan Form Terisi !');
        } else if ($('#nilai_pefindo').val() > 1000) {
            alert('Nilai Pefindo Tidak Boleh Lebih dari 1000');
        } else if ($('#ftv').val() > 100) {
            alert('Financing To Value tidak boleh lebih dari 100 %');
        } else {
            var complete_id = 0;
            var npwp_verify = 0;
            var ocr = 0;
            var npwpa = 0;
            var npwpc = 0;
            var npwpd = 0;
            var brw_id = "{{ $brw_id }}";
            var workplace_verification_f = 0;
            var ngetive_list_verification_j = 0;
            var verify_property_k = 0;

            var pengajuanId = "{{ $pengajuanId }}";
            var nilai_pefindo = $('#nilai_pefindo').val();
            var grade_pefindo = $('#grade_pefindo').val();
            var skor_personal = $('#skor_personal').val();
            var skor_pendanaan = $('#skor_pendanaan').val();
            var ftv = $('#ftv').val();
            var catatan_verifikasi = $('#catatan_verifikasi').val();
            var keputusan = $('#keputusan').val();

            $.each($("input[name='verijelas']:checked"), function() {
                if ($(this).val() == 'complete_id') {
                    complete_id = 1;
                } else if ($(this).val() == 'ocr') {
                    ocr = 1;
                } else if ($(this).val() == 'npwpa') {
                    npwpa = 1;
                } else if ($(this).val() == 'npwpc') {
                    npwpc = 1;
                } else if ($(this).val() == 'npwpd') {
                    npwpd = 1;
                } else if ($(this).val() == 'workplace_verification_f') {
                    workplace_verification_f = 1;
                } else if ($(this).val() == 'ngetive_list_verification_j') {
                    ngetive_list_verification_j = 1;
                } else if ($(this).val() == 'verify_property_k') {
                    verify_property_k = 1;
                } else if ($(this).val() == 'npwp_verify') {
                    npwp_verify = 1;
                }
            });
            // alert([complete_id, ocr, npwpa, npwpc, npwpd, workplace_verification_f, ngetive_list_verification_j, verify_property_k]);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/adr/simpanVerifikasi",
                method: "POST",
                dataType: 'JSON',
                data: {
                    'brw_id': brw_id,
                    'pengajuanId': pengajuanId,
                    'nilai_pefindo': nilai_pefindo,
                    'grade_pefindo': grade_pefindo,
                    'complete_id': complete_id,
                    'ocr': ocr,
                    'npwpa': npwpa,
                    'npwpc': npwpc,
                    'npwpd': npwpd,
                    'npwp_verify': npwp_verify,
                    'workplace_verification_f': workplace_verification_f,
                    'ngetive_list_verification_j': ngetive_list_verification_j,
                    'verify_property_k': verify_property_k,
                    'skor_personal': skor_personal,
                    'skor_pendanaan': skor_pendanaan,
                    'ftv': ftv,
                    'catatan_verifikasi': catatan_verifikasi,
                    'keputusan': keputusan
                },
                // dataType: "text",
                success: function(msg) {
                    if (msg.data == 1) {
                        window.location.href = "/admin/adr/dashboard";
                    }
                    // console.log(msg.data);
                    // alert(msg.data);
                }
            });
        }
    });
</script>
<script>
    $(document).ready(function() {
        var brw_id = "{{ $brw_id }}";
        var pengajuanId = "{{ $pengajuanId }}";
        var brw_type = '{{ $brw_type }}';
        // informasi pribadi
        var nama = '{{ $nama }}';
        var jns_kelamin = "{{ $jns_kelamin }}";
        // var jabatan = "{{ $jabatan }}";
        var ktp = "{{ $ktp }}";
        var kk = "{{ $kk }}";
        var tempat_lahir = "{{ $tempat_lahir }}";
        var tgl_lahir = "{{ $tgl_lahir }}";
        var telepon = "{{ $telepon }}";
        var agama = "{{ $agama }}";
        var pendidikan_terakhir = "{{ $pendidikan_terakhir }}";
        var id_kawin = "{{ $id_kawin }}";
        if (id_kawin == 1) {
            $('.blokPasangan').removeClass('d-none');
        }
        var status_kawin = "{{ $status_kawin }}";
        var nm_ibu = "{{ $nm_ibu }}";
        var npwp = "{{ $npwp }}";

        // informasi alamat
        var alamat = `{!! $alamat !!}`;
        var kota = "{{ $kota }}";
        var kecamatan = "{{ $kecamatan }}";
        var kelurahan = "{{ $kelurahan }}";
        var kode_pos = "{{ $kode_pos }}";
        var provinsi = "{{ $provinsi }}";
        var status_rumah = "{{ $status_rumah }}";


        var domisili_alamat = `{!! $domisili_alamat !!}`;
        var domisili_provinsi = "{{ $domisili_provinsi }}";
        var domisili_kota = "{{ $domisili_kota }}";
        var domisili_kecamatan = "{{ $domisili_kecamatan }}";
        var domisili_kelurahan = "{{ $domisili_kelurahan }}";
        var domisili_kode_pos = "{{ $domisili_kode_pos }}";
        var domisili_status_rumah = "{{ $domisili_status_rumah }}";

        // informasi alamat domisili
        if (domisili_alamat != alamat && domisili_kode_pos != kode_pos) {
            $(".blokDomisili").removeClass('d-none');
        }

        // informasi rekening
        var brw_norek = "{{ $brw_norek }}";
        var brw_nm_pemilik = "{{ $brw_nm_pemilik }}";
        var brw_kd_bank = "{{ $brw_kd_bank }}";
        var kantorcabangpembuka = "{{ $kantorcabangpembuka }}";

        // informasi pasangan
        var pasangan_nama = "{{ $pasangan_nama }}";
        var pasangan_jenis_kelamin = "{{ $pasangan_jenis_kelamin }}";
        var pasangan_ktp = "{{ $pasangan_ktp }}";
        var pasangan_tempat_lahir = "{{ $pasangan_tempat_lahir }}";
        var pasangan_tanggal_lahir = "{{ $pasangan_tanggal_lahir }}";
        var pasangan_telepon = "{{ $pasangan_telepon }}";
        var pasangan_agama = "{{ $pasangan_agama }}";
        var pasangan_pendidikan_terakhir = "{{ $pasangan_pendidikan_terakhir }}";
        var pasangan_npwp = "{{ $pasangan_npwp }}";
        var pasangan_alamat = `{!! $pasangan_alamat !!}`;
        var pasangan_provinsi = "{{ $pasangan_provinsi }}";
        var pasangan_kota = "{{ $pasangan_kota }}";
        var pasangan_kecamatan = "{{ $pasangan_kecamatan }}";
        var pasangan_kelurahan = "{{ $pasangan_kelurahan }}";
        var pasangan_kode_pos = "{{ $pasangan_kode_pos }}";

        // informasi pasangan pekerjaan
        var pekerjaan_pasangan = "{{ $pekerjaan_pasangan }}";
        var bidang_pekerjaan_pasangan = "{{ $bidang_pekerjaan_pasangan }}";
        var bidang_online_pasangan = "{{ $bidang_online_pasangan }}";
        $('#pekerjaan_pasangan').val(pekerjaan_pasangan);
        $('#bidang_pekerjaan_pasangan').val(bidang_pekerjaan_pasangan);
        $('#bidang_online_pasangan').val(bidang_online_pasangan);

        var nama_perusahaan_pasangan = "{{ $nama_perusahaan_pasangan }}";
        var sumberpengembaliandana_pasangan = "{{ $sumberpengembaliandana_pasangan }}";
        var bentuk_badan_usaha_pasangan = "{{ $bentuk_badan_usaha_pasangan }}";
        var status_kepegawaian_pasangan = "{{ $status_kepegawaian_pasangan }}";
        var usia_perusahaan_pasangan = "{{ $usia_perusahaan_pasangan }}";
        var departemen_pasangan = "{{ $departemen_pasangan }}";
        var jabatan_pasangan = "{{ $jabatan_pasangan }}";
        var tahun_bekerja_pasangan = "{{ $tahun_bekerja_pasangan }}";
        var bulan_bekerja_pasangan = "{{ $bulan_bekerja_pasangan }}";
        var nip_pasangan = "{{ $nip_pasangan }}";
        var nama_hrd_pasangan = "{{ $nama_hrd_pasangan }}";
        var no_fixed_line_hrd_pasangan = "{{ $no_fixed_line_hrd_pasangan }}";
        var no_telpon_usaha_pasangan = "{{ $no_telpon_usaha_pasangan }}";
        var alamat_perusahaan_pasangan = `{!! $alamat_perusahaan_pasangan !!}`;
        var rt_perusahaan_pasangan = "{{ $rt_perusahaan_pasangan }}";
        var rw_perusahaan_pasangan = "{{ $rw_perusahaan_pasangan }}";
        var provinsi_perusahaan_pasangan = "{{ $provinsi_perusahaan_pasangan }}";
        var kabupaten_perusahaan_pasangan = "{{ $kabupaten_perusahaan_pasangan }}";
        var kecamatan_perusahaan_pasangan = "{{ $kecamatan_perusahaan_pasangan }}";
        var kelurahan_perusahaan_pasangan = "{{ $kelurahan_perusahaan_pasangan }}";
        var kodepos_perusahaan_pasangan = "{{ $kodepos_perusahaan_pasangan }}";
        var tahun_pengalaman_bekerja_pasangan = "{{ $tahun_pengalaman_bekerja_pasangan }}";
        var bulan_pengalaman_bekerja_pasangan = "{{ $bulan_pengalaman_bekerja_pasangan }}";
        var penghasilan_pasangan = "{{ $penghasilan_pasangan }}";
        var biaya_hidup_pasangan = "{{ $biaya_hidup_pasangan }}";
        // fix
        $('#nama_perusahaan_pasangan').val(nama_perusahaan_pasangan);
        $('#bentuk_badan_usaha_pasangan').val(bentuk_badan_usaha_pasangan);
        $('#status_kepegawaian_pasangan').val(status_kepegawaian_pasangan);
        $('#usia_perusahaan_pasangan').val(usia_perusahaan_pasangan);
        $('#departemen_pasangan').val(departemen_pasangan);
        $('#jabatan_pasangan').val(jabatan_pasangan);
        $('#tahun_bekerja_pasangan').val(tahun_bekerja_pasangan);
        $('#bulan_bekerja_pasangan').val(bulan_bekerja_pasangan);
        $('#nip_pasangan').val(nip_pasangan);
        $('#nama_hrd_pasangan').val(nama_hrd_pasangan);
        if (no_fixed_line_hrd_pasangan.substring(0, 3) == '620') {
            $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(3));
        } else if (no_fixed_line_hrd_pasangan.substring(0, 2) == '62') {
            $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(2));
        } else if (no_fixed_line_hrd_pasangan.substring(0, 1) == '0') {
            $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan.substring(1));
        } else {
            $('#no_fixed_line_hrd_pasangan').val(no_fixed_line_hrd_pasangan);
        }
        if (no_telpon_usaha_pasangan.substring(0, 3) == '620') {
            $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(3));
        } else if (no_telpon_usaha_pasangan.substring(0, 2) == '62') {
            $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(2));
        } else if (no_telpon_usaha_pasangan.substring(0, 1) == '0') {
            $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan.substring(1));
        } else {
            $('#no_telpon_usaha_pasangan').val(no_telpon_usaha_pasangan);
        }

        $('#alamat_perusahaan_pasangan').val(alamat_perusahaan_pasangan);
        $('#rt_perusahaan_pasangan').val(rt_perusahaan_pasangan);
        $('#rw_perusahaan_pasangan').val(rw_perusahaan_pasangan);
        $('#provinsi_perusahaan_pasangan').val(provinsi_perusahaan_pasangan);
        $('#kabupaten_perusahaan_pasangan').val(kabupaten_perusahaan_pasangan);
        $('#kecamatan_perusahaan_pasangan').val(kecamatan_perusahaan_pasangan);
        $('#kelurahan_perusahaan_pasangan').val(kelurahan_perusahaan_pasangan);
        $('#kodepos_perusahaan_pasangan').val(kodepos_perusahaan_pasangan);
        $('#tahun_pengalaman_bekerja_pasangan').val(tahun_pengalaman_bekerja_pasangan);
        $('#bulan_pengalaman_bekerja_pasangan').val(bulan_pengalaman_bekerja_pasangan);
        $('#penghasilan_pasangan').val(penghasilan_pasangan);
        $('#biaya_hidup_pasangan').val(biaya_hidup_pasangan);
        // non fix
        var nama_perusahaan_pasangan_non = "{{ $nama_perusahaan_pasangan_non }}";
        var lama_usaha_pasangan_non = "{{ $lama_usaha_pasangan_non }}";
        var lama_tempat_usaha_pasangan_non = "{{ $lama_tempat_usaha_pasangan_non }}";
        var no_telpon_pasangan_non = "{{ $no_telpon_pasangan_non }}";
        var no_hp_pasangan_non = "{{ $no_hp_pasangan_non }}";
        var alamat_perusahaan_pasangan_non = `{!! $alamat_perusahaan_pasangan_non !!}`;
        var rt_perusahaan_pasangan_non = "{{ $rt_perusahaan_pasangan_non }}";
        var rw_perusahaan_pasangan_non = "{{ $rw_perusahaan_pasangan_non }}";
        var provinsi_perusahaan_pasangan_non = "{{ $provinsi_perusahaan_pasangan_non }}";
        var kabupaten_perusahaan_pasangan_non = "{{ $kabupaten_perusahaan_pasangan_non }}";
        var kecamatan_perusahaan_pasangan_non = "{{ $kecamatan_perusahaan_pasangan_non }}";
        var kelurahan_perusahaan_pasangan_non = "{{ $kelurahan_perusahaan_pasangan_non }}";
        var kodepos_perusahaan_pasangan_non = "{{ $kodepos_perusahaan_pasangan_non }}";
        var surat_ijin_pasangan_non = "{{ $surat_ijin_pasangan_non }}";
        var nomor_ijin_pasangan_non = "{{ $nomor_ijin_pasangan_non }}";
        var penghasilan_pasangan_non = "{{ $penghasilan_pasangan_non }}";
        var biaya_hidup_pasangan_non = "{{ $biaya_hidup_pasangan_non }}";

        $('#nama_perusahaan_pasangan_non').val(nama_perusahaan_pasangan_non);
        $('#lama_usaha_pasangan_non').val(lama_usaha_pasangan_non);
        $('#lama_tempat_usaha_pasangan_non').val(lama_tempat_usaha_pasangan_non);
        if (no_telpon_pasangan_non.substring(0, 3) == '620') {
            $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(3));
        } else if (no_telpon_pasangan_non.substring(0, 2) == '62') {
            $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(2));
        } else if (no_telpon_pasangan_non.substring(0, 1) == '0') {
            $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non.substring(1));
        } else {
            $('#no_telpon_pasangan_non').val(no_telpon_pasangan_non);
        }
        if (no_hp_pasangan_non.substring(0, 3) == '620') {
            $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(3));
        } else if (no_hp_pasangan_non.substring(0, 2) == '62') {
            $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(2));
        } else if (no_hp_pasangan_non.substring(0, 1) == '0') {
            $('#no_hp_pasangan_non').val(no_hp_pasangan_non.substring(1));
        } else {
            $('#no_hp_pasangan_non').val(no_hp_pasangan_non);
        }
        $('#alamat_perusahaan_pasangan_non').val(alamat_perusahaan_pasangan_non);
        $('#rt_perusahaan_pasangan_non').val(rt_perusahaan_pasangan_non);
        $('#rw_perusahaan_pasangan_non').val(rw_perusahaan_pasangan_non);
        $('#provinsi_perusahaan_pasangan_non').val(provinsi_perusahaan_pasangan_non);
        $('#kabupaten_perusahaan_pasangan_non').val(kabupaten_perusahaan_pasangan_non);
        $('#kecamatan_perusahaan_pasangan_non').val(kecamatan_perusahaan_pasangan_non);
        $('#kelurahan_perusahaan_pasangan_non').val(kelurahan_perusahaan_pasangan_non);
        $('#kodepos_perusahaan_pasangan_non').val(kodepos_perusahaan_pasangan_non);
        $('#surat_ijin_pasangan_non').val(surat_ijin_pasangan_non);
        $('#nomor_ijin_pasangan_non').val(nomor_ijin_pasangan_non);
        if (surat_ijin_pasangan_non == 2) {
            $('.bloksuratIjinPasangan').addClass("d-none");
        } else if (surat_ijin_pasangan_non == 1) {
            $('#surat_ijin_pasangan_non').val("Ya");
        }
        $('#penghasilan_pasangan_non').val(penghasilan_pasangan_non);
        $('#biaya_hidup_pasangan_non').val(biaya_hidup_pasangan_non);

        var pekerjaan = "{{ $pekerjaan }}";
        var bidang_perusahaan = "{{ $bidang_perusahaan }}";
        var bidang_pekerjaan = "{{ $bidang_pekerjaan }}";

        var pengalaman_pekerjaan = "{{ $pengalaman_pekerjaan }}";
        var pendapatan = "{{ $pendapatan }}";
        var total_aset = "{{ $total_aset }}";
        var kewarganegaraan = "{{ $kewarganegaraan }}";

        var brw_pic = "{{ $brw_pic }}";
        $('#brw_pic').val(brw_pic);
        var brw_pic_ktp = "{{ $brw_pic_ktp }}";
        $('#brw_pic_ktp').val(brw_pic_ktp);
        var brw_pic_user_ktp = "{{ $brw_pic_user_ktp }}";
        $('#brw_pic_user_ktp').val(brw_pic_user_ktp);
        var brw_pic_npwp = "{{ $brw_pic_npwp }}";
        $('#brw_pic_npwp').val(brw_pic_npwp);

        // informasi pekerjaan
        // fix
        var jenis_pekerjaan = "{{ $jenis_pekerjaan }}";
        var sumberpengembaliandana = "{{ $sumberpengembaliandana }}";
        var skemapembiayaan = "{{ $skemapembiayaan }}";
        var nama_perusahaan = "{{ $nama_perusahaan }}";
        var bidang_online = "{{ $bidang_online }}";
        var bentuk_badan_usaha = "{{ $bentuk_badan_usaha }}";
        var status_kepegawaian = "{{ $status_kepegawaian }}";
        var usia_perusahaan = "{{ $usia_perusahaan }}";
        var departemen = "{{ $departemen }}";
        var jabatan = "{{ $jabatan }}";
        var tahun_bekerja = "{{ $tahun_bekerja }}";
        var bulan_bekerja = "{{ $bulan_bekerja }}";
        var nip = "{{ $nip }}";
        var nama_hrd = "{{ $nama_hrd }}";
        var no_fixed_line_hrd = "{{ $no_fixed_line_hrd }}";
        var alamat_perusahaan = `{!! $alamat_perusahaan !!}`;
        var rt_perusahaan = "{{ $rt_perusahaan }}";
        var rw_perusahaan = "{{ $rw_perusahaan }}";
        var provinsi_perusahaan = "{{ $provinsi_perusahaan }}";
        var kabupaten_perusahaan = "{{ $kabupaten_perusahaan }}";
        var kecamatan_perusahaan = "{{ $kecamatan_perusahaan }}";
        var kelurahan_perusahaan = "{{ $kelurahan_perusahaan }}";
        var kodepos_perusahaan = "{{ $kodepos_perusahaan }}";
        var no_telpon_usaha = "{{ $no_telpon_usaha }}";
        var tahun_pengalaman_bekerja = "{{ $tahun_pengalaman_bekerja }}";
        var bulan_pengalaman_bekerja = "{{ $bulan_pengalaman_bekerja }}";
        var penghasilan = "{{ $penghasilan }}";
        var biaya_hidup = "{{ $biaya_hidup }}";
        var detail_penghasilan_lain_lain = "{{ $detail_penghasilan_lain_lain }}";
        var total_penghasilan_lain_lain = "{{ $total_penghasilan_lain_lain }}";
        var nilai_spt = "{{ $nilai_spt }}";
        // nonfix
        var nama_perusahaan_non = "{{ $nama_perusahaan_non }}";
        var bidang_online_non = "{{ $bidang_online_non }}";
        var lama_usaha_non = "{{ $lama_usaha_non }}";
        var lama_tempat_usaha_non = "{{ $lama_tempat_usaha_non }}";
        var no_telpon_non = "{{ $no_telpon_non }}";
        var no_hp_non = "{{ $no_hp_non }}";
        var alamat_perusahaan_non = `{!! $alamat_perusahaan_non !!}`;
        var rt_perusahaan_non = "{{ $rt_perusahaan_non }}";
        var rw_perusahaan_non = "{{ $rw_perusahaan_non }}";
        var provinsi_perusahaan_non = "{{ $provinsi_perusahaan_non }}";
        var kabupaten_perusahaan_non = "{{ $kabupaten_perusahaan_non }}";
        var kecamatan_perusahaan_non = "{{ $kecamatan_perusahaan_non }}";
        var kelurahan_perusahaan_non = "{{ $kelurahan_perusahaan_non }}";
        var kodepos_perusahaan_non = "{{ $kodepos_perusahaan_non }}";
        var surat_ijin_non = "{{ $surat_ijin_non }}";
        var nomor_ijin_non = "{{ $nomor_ijin_non }}";
        var penghasilan_non = "{{ $penghasilan_non }}";
        var biaya_hidup_non = "{{ $biaya_hidup_non }}";

        $('#nama_perusahaan_non').val(nama_perusahaan_non);
        $('#bidang_online_non').val(bidang_online_non);
        $('#lama_usaha_non').val(lama_usaha_non);
        $('#lama_tempat_usaha_non').val(lama_tempat_usaha_non);
        if (no_telpon_non.substring(0, 3) == '620') {
            $('#no_telpon_non').val(no_telpon_non.substring(3));
        } else if (no_telpon_non.substring(0, 2) == '62') {
            $('#no_telpon_non').val(no_telpon_non.substring(2));
        } else if (no_telpon_non.substring(0, 1) == '0') {
            $('#no_telpon_non').val(no_telpon_non.substring(1));
        } else {
            $('#no_telpon_non').val(no_telpon_non);
        }
        if (no_hp_non.substring(0, 3) == '620') {
            $('#no_hp_non').val(no_hp_non.substring(3));
        } else if (no_hp_non.substring(0, 2) == '62') {
            $('#no_hp_non').val(no_hp_non.substring(2));
        } else if (no_hp_non.substring(0, 1) == '0') {
            $('#no_hp_non').val(no_hp_non.substring(1));
        } else {
            $('#no_hp_non').val(no_hp_non);
        }
        $('#alamat_perusahaan_non').val(alamat_perusahaan_non);
        $('#rt_perusahaan_non').val(rt_perusahaan_non);
        $('#rw_perusahaan_non').val(rw_perusahaan_non);
        $('#provinsi_perusahaan_non').val(provinsi_perusahaan_non);
        $('#kabupaten_perusahaan_non').val(kabupaten_perusahaan_non);
        $('#kecamatan_perusahaan_non').val(kecamatan_perusahaan_non);
        $('#kelurahan_perusahaan_non').val(kelurahan_perusahaan_non);
        $('#kodepos_perusahaan_non').val(kodepos_perusahaan_non);
        if (surat_ijin_non == 2) {
            $('.bloksuratIjin').addClass("d-none");
        } else if (surat_ijin_non == 1) {
            $('#surat_ijin_non').val("Ya");
        }
        $('#nomor_ijin_non').val(nomor_ijin_non);
        $('#penghasilan_non').val(penghasilan_non);
        $('#biaya_hidup_non').val(biaya_hidup_non);

        // PENGAJUAN 
        var type_pendanaan = `{!!  $type_pendanaan !!}`;
        var tujuan_pendanaan = `{!!  $tujuan_pendanaan !!}`;
        var detail_pendanaan = `{!!  $detail_pendanaan !!}`;
        var alamat_objek_pendanaan = `{!! $alamat_objek_pendanaan !!}`;
        var provinsi_objek_pendanaan = `{!!  $provinsi_objek_pendanaan !!}`;
        var kota_objek_pendanaan = `{!!  $kota_objek_pendanaan !!}`;
        var kecamatan_objek_pendanaan = `{!!  $kecamatan_objek_pendanaan !!}`;
        var kelurahan_objek_pendanaan = `{!!  $kelurahan_objek_pendanaan !!}`;
        var kodepos_objek_pendanaan = `{!!  $kodepos_objek_pendanaan !!}`;
        var harga_objek_pendanaan = `{!!  $harga_objek_pendanaan !!}`;
        var uang_muka = `{!!  $uang_muka !!}`;
        var nilai_pengajuan = `{!!  $nilai_pengajuan !!}`;
        var jangka_waktu = `{!!  $jangka_waktu !!}`;
        var nama_pemilik = `{!!  $nama_pemilik !!}`;
        var tlp_pemilik = `{!!  $tlp_pemilik !!}`;
        var alamat_pemilik = `{!! $alamat_pemilik !!}`;
        var provinsi_pemilik = `{!!  $provinsi_pemilik !!}`;
        var kota_pemilik = `{!!  $kota_pemilik !!}`;
        var kecamatan_pemilik = `{!!  $kecamatan_pemilik !!}`;
        var kelurahan_pemilik = `{!!  $kelurahan_pemilik !!}`;
        var kodepos_pemilik = `{!!  $kodepos_pemilik !!}`;

        $('#type_pendanaan').val(type_pendanaan);
        $('#tujuan_pendanaan').val(tujuan_pendanaan);
        $('#detail_pendanaan').text(detail_pendanaan);
        $('#alamat_objek_pendanaan').val(alamat_objek_pendanaan);
        $('#provinsi_objek_pendanaan').val(provinsi_objek_pendanaan);
        $('#kota_objek_pendanaan').val(kota_objek_pendanaan);
        $('#kecamatan_objek_pendanaan').val(kecamatan_objek_pendanaan);
        $('#kelurahan_objek_pendanaan').val(kelurahan_objek_pendanaan);
        $('#kodepos_objek_pendanaan').val(kodepos_objek_pendanaan);
        $('#harga_objek_pendanaan').val(harga_objek_pendanaan);
        $('#uang_muka').val(uang_muka);
        $('#nilai_pengajuan').val(nilai_pengajuan);
        $('#jangka_waktu').val(jangka_waktu);
        $('#nama_pemilik').val(nama_pemilik);
        if (tlp_pemilik.substring(0, 3) == '620') {
            $('#tlp_pemilik').val(tlp_pemilik.substring(3));
        } else if (tlp_pemilik.substring(0, 2) == '62') {
            $('#tlp_pemilik').val(tlp_pemilik.substring(2));
        } else if (tlp_pemilik.substring(0, 1) == '0') {
            $('#tlp_pemilik').val(tlp_pemilik.substring(1));
        } else {
            $('#tlp_pemilik').val(tlp_pemilik);
        }
        $('#alamat_pemilik').val(alamat_pemilik);
        $('#provinsi_pemilik').val(provinsi_pemilik);
        $('#kota_pemilik').val(kota_pemilik);
        $('#kecamatan_pemilik').val(kecamatan_pemilik);
        $('#kelurahan_pemilik').val(kelurahan_pemilik);
        $('#kodepos_pemilik').val(kodepos_pemilik);

        // data verif
        var nilai_pefindo = "{{ $nilai_pefindo }}";
        var grade_pefindo = "{{ $grade_pefindo }}";
        var skor_personal = "{{ $skor_personal }}";
        var skor_pendanaan = "{{ $skor_pendanaan }}";
        var complete_id = "{{ $complete_id }}";
        var ocr = "{{ $ocr }}";
        var npwpa = "{{ $npwpa }}";
        var npwpc = "{{ $npwpc }}";
        var npwpd = "{{ $npwpd }}";
        var npwp_verify = "{{ $npwp_verify }}";
        var workplace_verification_f = "{{ $workplace_verification_f }}";
        var negative_list_verification_j = "{{ $negative_list_verification_j }}";
        var verify_property_k = "{{ $verify_property_k }}";
        var ftv = "{{ $ftv }}";
        var catatan_verifikasi = "{{ $catatan_verifikasi }}";
        var keputusan = "{{ $keputusan }}";
        var isiVerif = "{{ $isiVerif }}";

        if (isiVerif == 1) {
            $('#nilai_pefindo').val(nilai_pefindo);
            $("#grade_pefindo option[value='" + grade_pefindo + "']").attr('selected', 'selected');
            $('#skor_personal').val(skor_personal);
            $('#skor_pendanaan').val(skor_pendanaan);

            if (complete_id == 1) {
                $("input[name=verijelas][value='complete_id']").attr('checked', 'checked');
            }
            if (ocr == 1) {
                $("input[name=verijelas][value='ocr']").attr('checked', 'checked');
            }
            if (npwpa == 1) {
                $("input[name=verijelas][value='npwpa']").attr('checked', 'checked');
            }
            if (npwpc == 1) {
                $("input[name=verijelas][value='npwpc']").attr('checked', 'checked');
            }
            if (npwpd == 1) {
                $("input[name=verijelas][value='npwpd']").attr('checked', 'checked');
            }
            if (npwp_verify == 1) {
                $("input[name=verijelas][value='npwp_verify']").attr('checked', 'checked');
            }
            if (workplace_verification_f == 1) {
                $("input[name=verijelas][value='workplace_verification_f']").attr('checked', 'checked');
            }
            if (negative_list_verification_j == 1) {
                $("input[name=verijelas][value='negative_list_verification_j']").attr('checked', 'checked');
            }
            if (verify_property_k == 1) {
                $("input[name=verijelas][value='verify_property_k']").attr('checked', 'checked');
            }

            $('#ftv').val(ftv);
            $('#catatan_verifikasi').val(catatan_verifikasi);
            $("#keputusan option[value='" + keputusan + "']").attr('selected', 'selected');

            // disabled form verifikasi
            $('#nilai_pefindo').attr("disabled", true);
            $('#grade_pefindo').attr("disabled", true);
            $('#ftv').attr("disabled", true);
            $('#catatan_verifikasi').attr("disabled", true);
            $('#keputusan').attr("disabled", true);
            $("input[name=verijelas]").attr("disabled", true);
            $("#submitverfikasi").attr("disabled", true);
        } else {
            // credolab

            // let reference_number_personal = '288';
            // let reference_number_pendanaan = '288_80';
            let reference_number_personal = brw_id;
            let reference_number_pendanaan = brw_id + '_' + pengajuanId;
            var url_per = '{{ route("get-credolab-score", ":reference_number") }}';

            // Borrower Score personal
            var v_url = url_per.replace(':reference_number', reference_number_personal);
            $.getJSON(v_url, function(response) {
                console.log([v_url, response]);
                if (response.status_code == 200) {
                    $('#skor_personal').val(response.credolab_score);
                } else {
                    $('#skor_personal').val('0');
                }
            });

            // Borrower Score pendanaan
            var url_pen = '{{ route("get-credolab-score", ":reference_number ") }}';
            var vpen_url = url_pen.replace(':reference_number', reference_number_pendanaan);
            $.getJSON(vpen_url, function(response) {
                console.log([vpen_url, response]);
                if (response.status_code == 200) {
                    $('#skor_pendanaan').val(response.credolab_score);
                } else {
                    $('#skor_pendanaan').val('0');
                }
            });
        }



        // START: Informasi Pribadi
        $('#nama').val(nama);
        if (jns_kelamin == 1) {
            jns_kelamin = "Laki-laki";
        } else {
            jns_kelamin = "Perempuan";
        }
        $('#jns_kelamin').val(jns_kelamin);
        $('#ktp').val(ktp);
        $('#kk').val(kk);
        $('#tempat_lahir').val(tempat_lahir);
        $("#tanggal_lahir").val(tgl_lahir);
        $('#ibukandung').val(nm_ibu);
        $('#npwp').val(npwp);

        let no_tlp_check = telepon;
        if (no_tlp_check.substring(0, 3) == '620') {
            $('#telepon').val(no_tlp_check.substring(3));
        } else if (no_tlp_check.substring(0, 2) == '62') {
            $('#telepon').val(no_tlp_check.substring(2));
        } else if (no_tlp_check.substring(0, 1) == '0') {
            $('#telepon').val(no_tlp_check.substring(1));
        } else {
            $('#telepon').val(no_tlp_check);
        }

        $("#pasangan_agama").val(pasangan_agama);
        $("#agama").val(agama);

        $("#pasangan_pendidikan_terakhir").val(pasangan_pendidikan_terakhir);
        $("#pendidikan_terakhir").val(pendidikan_terakhir);
        $("#status_kawin").val(status_kawin);

        // killl2
        $('#provinsi').val(provinsi);
        $('#domisili_provinsi').val(domisili_provinsi);
        $('#pasangan_provinsi').val(pasangan_provinsi);
        $("#kota").val(kota);
        $('#kecamatan').val(kecamatan);
        $("#kelurahan").val(kelurahan);
        $("#kode_pos").val(kode_pos);
        $('#domisili_alamat').text(domisili_alamat);
        $('#pasangan_alamat').text(pasangan_alamat);

        if (status_rumah == 1) {
            status_rumah = "Milik Pribadi";
        } else if (status_rumah == 2) {
            status_rumah = "Sewa";
        } else {
            status_rumah = "Milik Keluarga";
        }
        $('#status_rumah').val(status_rumah);

        if (domisili_status_rumah == 1) {
            domisili_status_rumah = "Milik Pribadi";
        } else if (domisili_status_rumah == 2) {
            domisili_status_rumah = "Sewa";
        } else {
            domisili_status_rumah = "Milik Keluarga";
        }
        $('#domisili_status_rumah').val(domisili_status_rumah);


        // END: Alamat 

        // START: Domisili ALAMAT
        $('#domisili_kota').val(domisili_kota);
        $("#pasangan_kota").val(pasangan_kota);

        $("#domisili_kecamatan").val(domisili_kecamatan);
        $("#pasangan_kecamatan").val(pasangan_kecamatan);

        $("#domisili_kelurahan").val(domisili_kelurahan);
        $("#pasangan_kelurahan").val(pasangan_kelurahan);

        $("#domisili_kode_pos").val(domisili_kode_pos);
        $('#pasangan_kode_pos').val(pasangan_kode_pos);

        // END: Alamat domisili

        // START: Pasangan
        $('#pasangan_nama').val(pasangan_nama);
        if (pasangan_jenis_kelamin == 1) {
            pasangan_jenis_kelamin = "Laki-laki";
        } else {
            pasangan_jenis_kelamin = "Perempuan";
        }
        $('#pasangan_jenis_kelamin').val(pasangan_jenis_kelamin);
        $('#pasangan_ktp').val(pasangan_ktp);
        $('#pasangan_tempat_lahir').val(pasangan_tempat_lahir);
        $('#pasangan_tanggal_lahir').val(pasangan_tanggal_lahir);

        if (pasangan_telepon.substring(0, 3) == '620') {
            $('#pasangan_telepon').val(pasangan_telepon.substring(3));
        } else if (pasangan_telepon.substring(0, 2) == '62') {
            $('#pasangan_telepon').val(pasangan_telepon.substring(2));
        } else if (pasangan_telepon.substring(0, 1) == '0') {
            $('#pasangan_telepon').val(pasangan_telepon.substring(1));
        } else {
            $('#pasangan_telepon').val(pasangan_telepon);
        }

        $('#pasangan_npwp').val(pasangan_npwp);

        // END: Informasi Pribadi

        //START: Rekening
        $('#norekening').val(brw_norek);
        $('#namapemilikrekening').val(brw_nm_pemilik);
        $('#bank').val(brw_kd_bank);
        $('#kantorcabangpembuka').val(kantorcabangpembuka);
        //END: Rekening


        // START: Pekerjaan Individu
        $('#jenis_pekerjaan').val(jenis_pekerjaan);
        var txt_sumberpengembaliandana;
        var txt_skemapembiayaan;
        if (sumberpengembaliandana == 1) {
            txt_sumberpengembaliandana = "Fixed Income (Penghasilan Tetap)";
        } else {
            $('.blokNonFixedIncome').removeClass('d-none');
            $('.blokFixedIncome').addClass('d-none');
            txt_sumberpengembaliandana = "Non Fixed Income (Penghasilan Tidak Tetap)";
        }
        $('#sumberpengembaliandana').val(txt_sumberpengembaliandana);
        if (id_kawin == 1 && skemapembiayaan == 2) {
            if (sumberpengembaliandana_pasangan == 1) {
                $('.blokPekerjaanPasangan').removeClass('d-none');
                $('.blokFixedIncomePasangan').removeClass('d-none');
            } else if (sumberpengembaliandana_pasangan == 2) {
                $('.blokPekerjaanPasangan').removeClass('d-none');
                $('.blokNonFixedIncomePasangan').removeClass('d-none');
            }
        }
        if (skemapembiayaan == 1) {
            txt_skemapembiayaan = "Single Income (Peghasilan Sendiri)";
        } else {
            txt_skemapembiayaan = "Joint Income (Penghasilan Bersama)";
        }
        $('#skemapembiayaan').val(txt_skemapembiayaan);
        $('#nama_perusahaan').val(nama_perusahaan);
        $('#bidang_online').val(bidang_online);
        $('#bentuk_badan_usaha').val(bentuk_badan_usaha);

        $('#status_kepegawaian').val(status_kepegawaian);
        $('#usia_perusahaan').val(usia_perusahaan);
        $('#departemen').val(departemen);
        $('#jabatan').val(jabatan);
        $('#tahun_bekerja').val(tahun_bekerja);
        $('#bulan_bekerja').val(bulan_bekerja);
        $('#nip').val(nip);
        $('#nama_hrd').val(nama_hrd);
        if (no_fixed_line_hrd.substring(0, 3) == '620') {
            $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(3));
        } else if (no_fixed_line_hrd.substring(0, 2) == '62') {
            $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(2));
        } else if (no_fixed_line_hrd.substring(0, 1) == '0') {
            $('#no_fixed_line_hrd').val(no_fixed_line_hrd.substring(1));
        } else {
            $('#no_fixed_line_hrd').val(no_fixed_line_hrd);
        }
        $('#alamat_perusahaan').val(alamat_perusahaan);
        $('#rt_perusahaan').val(rt_perusahaan);
        $('#rw_perusahaan').val(rw_perusahaan);
        $('#provinsi_perusahaan').val(provinsi_perusahaan);
        $('#kabupaten_perusahaan').val(kabupaten_perusahaan);
        $('#kecamatan_perusahaan').val(kecamatan_perusahaan);
        $('#kelurahan_perusahaan').val(kelurahan_perusahaan);
        $('#kodepos_perusahaan').val(kodepos_perusahaan);
        $('#no_telpon_usaha').val(no_telpon_usaha);
        if (no_telpon_usaha.substring(0, 3) == '620') {
            $('#no_telpon_usaha').val(no_telpon_usaha.substring(3));
        } else if (no_telpon_usaha.substring(0, 2) == '62') {
            $('#no_telpon_usaha').val(no_telpon_usaha.substring(2));
        } else if (no_telpon_usaha.substring(0, 1) == '0') {
            $('#no_telpon_usaha').val(no_telpon_usaha.substring(1));
        } else {
            $('#no_telpon_usaha').val(no_telpon_usaha);
        }
        $('#tahun_pengalaman_bekerja').val(tahun_pengalaman_bekerja);
        $('#bulan_pengalaman_bekerja').val(bulan_pengalaman_bekerja);
        $('#penghasilan').val(penghasilan);
        $('#biaya_hidup').val(biaya_hidup);
        $('#detail_penghasilan_lain_lain').val(detail_penghasilan_lain_lain);
        $('#total_penghasilan_lain_lain').val(total_penghasilan_lain_lain);
        $('#nilai_spt').val(nilai_spt);
        if (detail_penghasilan_lain_lain == "" || detail_penghasilan_lain_lain == null) {
            $('.blokDetailPenghasilan').addClass('d-none');
        }
        // END: Pekerjaan Individu


        $('#verijelas_modal').on('show.bs.modal', function(event) {
            $('.modal-title').html('{{ trans("verify.hasil.title") }}' + ' - ' + nama);
            veriJelasData();
        });

        $('body').on('show.bs.modal', function() {
            $('.modal-open').css('padding-right', '0px');
        });

        $('#loading_modal').on('show.bs.modal', function(event) {
            $('.modal-title').html('{{ trans("verify.title") }}');
            veriJelasCheck();
        });

        $('#loading_modal').on('hidden.bs.modal', function(e) {
            $('#loading_footer').addClass('d-none');
        });


        function veriJelasData() {

            $.ajax({
                url: "{{ route('verify.data') }}",
                type: "post",
                dataType: "html",
                data: {
                    _token: "{{ csrf_token() }}",
                    borrower_id: brw_id,
                    brw_type: brw_type,
                    vendor: 'verijelas'
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    if (xhr.status == '419') {
                        errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                    }

                    $('#loading_title').html('Data Failed');
                    $('#loading_content_data').html(errorMessage);
                    $('#loading_footer_data').removeClass('d-none');
                },
                success: function(result) {
                    $('#loading_title').html('Preparing Result ...');
                    $('#loading_content_data').html(result);
                    $('#loading_title').html('');
                    $('#loading_footer_data').removeClass('d-none');
                }
            });


        }

        function veriJelasCheck() {

            const checkCompleteId = $("#complete_id").is(":checked") ? 1 : 0;
            const checkOcr = $("#ocr").is(":checked") ? 1 : 0;
            const checkNpwp = $("#npwp_verify").is(":checked") ? 1 : 0;
            const checkTempatKerja = $("#workplace_verification_f").is(":checked") ? 1 : 0;
            const checkNegativeList = $("#ngetive_list_verification_j").is(":checked") ? 1 : 0;

            $.ajax({
                url: "{{ route('verify.check') }}",
                type: "post",
                dataType: "html",
                data: {
                    _token: "{{ csrf_token() }}",
                    borrower_id: brw_id,
                    brw_type: brw_type,
                    complete_id: checkCompleteId,
                    ocr: checkOcr,
                    npwp_verify: checkNpwp,
                    tempat_kerja: checkTempatKerja,
                    negativelist: checkNegativeList,
                    vendor: 'verijelas',
                    origin: "WEB"
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText

                    if (xhr.status == '419') {
                        errorMessage = xhr.status + ': Session expired. Silahkan re-login kembali.';
                    }

                    $('#loading_title').html('Check Failed');
                    $('#loading_content').html(errorMessage);
                    $('#loading_footer').removeClass('d-none');
                },
                success: function(result) {

                    $('#loading_title').html('Preparing Result ...');
                    $('#loading_content').html(result);
                    $('#loading_title').html('');
                    $('#loading_footer').removeClass('d-none');
                }
            });
        }
    });
</script>

@endsection