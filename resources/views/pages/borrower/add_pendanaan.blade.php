@extends('layouts.borrower.master')

@section('title', 'Selamat Datang Penerima Dana')

@section('style')
    <style>
        #overlay {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 900;
            width: 100%;
            height: 100%;
            display: none;
            background: rgba(0, 0, 0, 0.6);
        }

        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #2e93e6 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }

        @keyframes sp-anime {
            100% {
                transform: rotate(360deg);
            }
        }

        .mandatory_label {
            color: red;
        }
    </style>
@endsection

@section('content')
    <!-- select2 -->
    <!-- START: Main Container -->

    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <main id="main-container">
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="no-paddingTop font-w400" style="float: left; margin-block-end: 0em; color: #31394D">
                                Lengkapi Data Pengajuan</h1>
                            <span class="pull-right">
                                <h6 class=" font-w700" style="float: left; margin-block-end: 0em; color: #31394D">1 dari 2
                                    Langkah</h6>
                            </span>
                        </span>
                    </div>
                </div>
                <div class="row mt-5 pt-5">
                    <div class="col-md-12 mt-5 pt-5">
                        <div class="row">

                            <div class="col-12 col-md-12">
                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-validation-classic block">
                                    {{-- <div> --}}
                                    <!-- Wizard Progress Bar -->
                                    <div class="progress rounded-0" data-wizard="progress" style="height: 8px;">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated"
                                            role="progressbar" style="width: 30%;" aria-valuenow="30" aria-valuemin="0"
                                            aria-valuemax="100"></div>
                                    </div>
                                    <!-- END Wizard Progress Bar -->

                                    <!-- Step Tabs -->
                                    <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#wizard-validation-classic-step2" data-toggle="tab">1.
                                                Informasi Pendanaan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#wizard-validation-classic-step3" data-toggle="tab">2.
                                                Dokumen Pendukung</a>
                                        </li>
                                    </ul>
                                    <!-- END Step Tabs -->

                                    <!-- Form -->



                                    <form id="form_lengkapi_profile" method="POST"
                                        class="js-wizard-validation-classic-form" enctype="multipart/form-data">
                                        <!-- Steps Content -->
                                        @csrf
                                        <div class="block-content block-content-full tab-content"
                                            style="min-height: 274px;">
                                            <!-- Step 1 -->

                                            <!-- END Step 1 -->

                                            <!-- Step 2 -->
                                            <div class="tab-pane active" id="wizard-validation-classic-step2"
                                                role="tabpanel">
                                                <div id="layout-x">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-6 layout type_pendanaan_individu"
                                                                    id="layout-pribadi">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-namapengguna">Tipe
                                                                            Pendanaan</label><label
                                                                            id="txt_tipe_borrower"></label> <label
                                                                            class="mandatory_label">*</label>
                                                                        <select class="form-control"
                                                                            id="type_pendanaan_select"
                                                                            name="type_pendanaan_select" required></select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 layout type_pendanaan_perusahaan"
                                                                    style="display: none;" id="layout-badanhukum">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-namapengguna">Tipe
                                                                            Pendanaan (Badan Hukum)</label>
                                                                        <select class="form-control"
                                                                            id="type_pendanaan_select_bdn_hukum"
                                                                            name="type_pendanaan_select_bdn_hukum" required
                                                                            onchange="$(this).valid()"></select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-namapengguna">Tujuan
                                                                            Pendanaan</label><label
                                                                            id="txt_tujuan_pendanaan"></label>
                                                                        <label class="mandatory_label">*</label>
                                                                        <select class="form-control custom-select"
                                                                            id="type_tujuan_pendanaan"
                                                                            name="type_tujuan_pendanaan" required></select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-namapendanaan">Nama
                                                                            Pendanaan </label> <label
                                                                            class="mandatory_label">*</label>
                                                                        <input class="form-control allowCharacter"
                                                                            type="text" id="txt_nm_pendanaan"
                                                                            name="txt_nm_pendanaan"
                                                                            placeholder="Masukkan nama pendanaan" required>
                                                                    </div>
                                                                </div>
                                                                <!-- satuBaris -->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-ktp">Akad </label>
                                                                        <label class="mandatory_label">*</label>
                                                                        <select class="form-control custom-select"
                                                                            id="txt_jenis_akad_pendanaan"
                                                                            name="txt_jenis_akad_pendanaan" required>
                                                                            <option value="1">Murabahah</option>
                                                                            <option value="3">MMQ</option>
                                                                            <option value="4">IMBT</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-ktp">Estimasi Mulai
                                                                            Proyek </label> <label
                                                                            class="mandatory_label">*</label>
                                                                        <input type="date"
                                                                            class="form-control bg-white allowCharacterdate"
                                                                            id="txt_estimasi_proyek"
                                                                            min="<?= date('Y-m-d') ?>"
                                                                            data-date-format="d-m-Y"
                                                                            name="txt_estimasi_proyek"
                                                                            placeholder="dd-mm-YYYY"
                                                                            data-allow-input="false" required>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group pb-0 mb-0"
                                                                        id="div_durasi_proyek">
                                                                        <label for="wizard-progress2-ktp">Durasi
                                                                            Proyek/Tenor </label> <label
                                                                            class="mandatory_label">*</label>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <input class="form-control" type="text"
                                                                                id="txt_durasi_pendanaan"
                                                                                name="txt_durasi_pendanaan" maxlength="2"
                                                                                placeholder="Estimasi Bulan" required>
                                                                            <div class="input-group-append">
                                                                                <span
                                                                                    class="input-group-text input-group-text-dsi">
                                                                                    Bulan
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group" id="div_dana_dibutuhkan">
                                                                        <label for="wizard-progress2-tempatlahir">Dana
                                                                            Dibutuhkan (RP) </label> <label
                                                                            class="mandatory_label">*</label>
                                                                        <input class="form-control" maxlength="20"
                                                                            id="txt_dana_pendanaan"
                                                                            name="txt_dana_pendanaan"
                                                                            onkeyup="check_dana_dibutuhkan(); this.value = formatRupiah(this.value);"
                                                                            placeholder="Masukkan dana yang anda butuhkan..."
                                                                            required style="text-align:right;" />
                                                                    </div>
                                                                </div>
                                                                {{-- <div class="col-md-4">
                                                                <div class="form-group" id="div_imbal_hasil">
                                                                    <label for="wizard-progress2-tempatlahir">Imbal
                                                                        hasil yang ditawarkan (% per tahun)</label>
                                                                    <label class="mandatory_label">*</label>
                                                                    <input class="form-control validasiString" type="text" onkeyup="createSimulasi(); checkImbalHasil()" id="txt_imbal_hasil" name="txt_imbal_hasil" maxlength="2" placeholder="Masukkan nilai imbal hasil %..." required>
                                                                </div>
                                                            </div> --}}
                                                            </div>

                                                            <div style="display:none" id="Divsimulasi" class="row">

                                                                <div class="col-md-12">
                                                                    <h6 class="content-heading text-muted font-w600"
                                                                        style="font-size: 1em;">Simulasi Cicilan</h6>

                                                                    <table
                                                                        class="table compact table-striped table-bordered table-hover table-sm"
                                                                        id="list_simulasi">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>No</th>
                                                                                <th>Tgl Jatuh Tempo</th>
                                                                                <th>Periode</th>
                                                                                <th>Tagihan Pokok + Imbal Hasil</th>
                                                                                <th>Sisa Tagihan</th>
                                                                            </tr>
                                                                        </thead>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <br />

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-tempatlahir">Lokasi
                                                                            Proyek</label> <label
                                                                            class="mandatory_label">*</label>
                                                                        <textarea class="form-control" rows="4" cols="80" id="txt_lokasi_proyek" name="txt_lokasi_proyek"
                                                                            required></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            {{-- <h6 class="content-heading text-muted font-w600" style="font-size: 1em;">Gambar Utama (.png/.jpeg/.jpg)
                                                            <label class="mandatory_label">*</label>
                                                        </h6>
                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="col-8">
                                                                    <div id="listGambarUtama">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input gambar_check" id="gambar_utama" name="gambar_utama" accept="image/*" data-toggle="custom-file-input" onchange="javascript:showGambarUtama(); $('#url_gambar_utama').val('1').valid()" required>
                                                                            <label class="custom-file-label" for="example-file-input-custom">Choose
                                                                                file</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> --}}
                                                            {{-- 
                                                        <h6 class="content-heading text-muted font-w600" style="font-size: 1em;">Gambar Detail (.png/.jpeg/.jpg)
                                                            <label class="mandatory_label">*</label>
                                                        </h6>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="col-8">
                                                                    <div id="listGambarSlider">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="custom-file">
                                                                            <input type="file" class="custom-file-input gambar_check" id="gambar_slider1" name="gambar_slider[]" accept="image/*" onchange="javascript:showGambarSlider()" data-toggle="custom-file-input" required>
                                                                            <label class="custom-file-label" for="example-file-input-custom">Choose
                                                                                file</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="custom-file my-3">
                                                                            <input type="file" class="custom-file-input gambar_check" id="gambar_slider2" name="gambar_slider[]" accept="image/*" onchange="javascript:showGambarSlider()" data-toggle="custom-file-input" required>
                                                                            <label class="custom-file-label" for="example-file-input-custom">Choose
                                                                                file</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> --}}

                                                            <h6 class="content-heading text-muted font-w600"
                                                                style="font-size: 1em;">Detil Pendanaan <label
                                                                    class="mandatory_label">*</label></h6>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <textarea id="txt_detail_pendanaan" rows="4" cols="50" class="form-control" name="txt_detail_pendanaan"
                                                                            required></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <h6 class="content-heading text-muted font-w600"
                                                                style="font-size: 1em;">Jaminan Pendanaan</h6>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-12 ClassJaminan"
                                                                            id="divCountJaminan_1">
                                                                            <div class="row" id="tambahJaminan">
                                                                                <!-- satuBaris -->
                                                                                <div class="col-12 ">
                                                                                    <div class="row ">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label
                                                                                                    for="wizard-progress2-namapengurus">Nama
                                                                                                    Pemilik Jaminan </label>
                                                                                                <label
                                                                                                    class="mandatory_label">*</label>
                                                                                                <input
                                                                                                    class="form-control allowCharacter"
                                                                                                    type="text"
                                                                                                    name="txt_nm_jaminan_pendanaan[]"
                                                                                                    placeholder="Nama Pemilik Jaminan..."
                                                                                                    required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label
                                                                                                    for="wizard-progress2-nik">Jenis
                                                                                                    Jaminan </label> <label
                                                                                                    class="mandatory_label">*</label>
                                                                                                <select
                                                                                                    class="form-control jenisjaminan"
                                                                                                    id="txt_jenis_jaminan_pendanaan"
                                                                                                    name="txt_jenis_jaminan_pendanaan[]"
                                                                                                    required
                                                                                                    onchange="$(this).valid()"></select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label
                                                                                                    for="wizard-progress2-nik">Nomor
                                                                                                    Sertifikat </label>
                                                                                                <label
                                                                                                    class="mandatory_label">*</label>
                                                                                                <input
                                                                                                    class="form-control validasiString"
                                                                                                    type="text"
                                                                                                    id="txt_nomor_surat"
                                                                                                    name="txt_nomor_surat[]"
                                                                                                    placeholder="Nomor Sertifikat..."
                                                                                                    required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group"
                                                                                                id="div_tlp">
                                                                                                <label
                                                                                                    for="wizard-progress2-tempatlahir">Kantor
                                                                                                    Penerbit </label> <label
                                                                                                    class="mandatory_label">*</label>
                                                                                                <div class="input-group">
                                                                                                    <div
                                                                                                        class="input-group-append">
                                                                                                        <span
                                                                                                            class="input-group-text input-group-text-dsi text-center"
                                                                                                            style="height:27px">
                                                                                                            BPN </span>
                                                                                                        <select
                                                                                                            class="form-control txt_provinsi_penerbit"
                                                                                                            id="txt_provinsi_penerbit"
                                                                                                            name="txt_provinsi_penerbit[]"
                                                                                                            required
                                                                                                            onchange="$(this).valid()"></select>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>

                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label
                                                                                                    for="wizard-progress2-nik">NOP
                                                                                                    (Nomor Objek Pajak)
                                                                                                </label> <label
                                                                                                    class="mandatory_label">*</label>
                                                                                                <input
                                                                                                    class="form-control validasiString"
                                                                                                    maxlength="18"
                                                                                                    type="text"
                                                                                                    id="txt_nomor_wajib_pajak"
                                                                                                    name="txt_nomor_wajib_pajak[]"
                                                                                                    placeholder="Nomor Wajib Pajak..."
                                                                                                    required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label
                                                                                                    for="wizard-progress2-nik">Nilai
                                                                                                    Objek Pajak (Rp)
                                                                                                </label> <label
                                                                                                    class="mandatory_label">*</label>
                                                                                                <input
                                                                                                    class="form-control allowCharacter"
                                                                                                    maxlength="20"
                                                                                                    id="txt_nilai_objek_pajak"
                                                                                                    name="txt_nilai_objek_pajak[]"
                                                                                                    onkeyup="this.value = formatRupiah(this.value);"
                                                                                                    placeholder="Nilai Objek Pajak..."
                                                                                                    required>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="row ">
                                                                                        <div class="col-md-4">
                                                                                            <label
                                                                                                for="wizard-progress2-nik">Unggah
                                                                                                Sertifikat 3 halaman pertama
                                                                                                (.pdf) </label> <label
                                                                                                class="mandatory_label">*</label>
                                                                                            <div id="listFileJaminan">
                                                                                            </div>
                                                                                            <div class="form-group">
                                                                                                <div class="custom-file">
                                                                                                    <input type="file"
                                                                                                        class="custom-file-input doc_check"
                                                                                                        id="file_jaminan"
                                                                                                        name="file_jaminan[]"
                                                                                                        onchange="javascript:showFilenameJaminan()"
                                                                                                        accept="application/pdf"
                                                                                                        data-toggle="custom-file-input"
                                                                                                        required>
                                                                                                    <label
                                                                                                        class="custom-file-label"
                                                                                                        for="example-file-input-custom">Choose
                                                                                                        file</label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-8">
                                                                                            <div class="form-group">
                                                                                                <label
                                                                                                    for="wizard-progress2-nik">Detail
                                                                                                    Jaminan </label> <label
                                                                                                    class="mandatory_label">*</label>
                                                                                                <textarea class="form-control detailjaminan allowCharacter" rows="4" cols="80"
                                                                                                    id="txt_detail_jaminan_pendanaan" name="txt_detail_jaminan_pendanaan[]" required></textarea>
                                                                                                <!-- <input class="form-control" type="text" id="txt_detail_jaminan_pendanaan" name="txt_detail_jaminan_pendanaan[]" placeholder="Nilai Jaminan Pendanaan...">  -->
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <!-- new row -->
                                                                        <div class="col-12">
                                                                            <button type="button"
                                                                                class="btn btn-rounded btn-primary btn-dsi min-width-200 mb-10 push-right"
                                                                                onclick="add_jaminan()">Tambah
                                                                                Jaminan</button>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Step 2 -->

                                            <!-- Step 3 -->
                                            <div class="tab-pane" id="wizard-validation-classic-step3" role="tabpanel">
                                                <!-- 1 -->
                                                <div class="layout-dokumen">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <h3
                                                                        class="content-heading text-dark mt-0 pt-0 font-w600">
                                                                        Kelengkapan Dokumen</h3>
                                                                    <p>Dalam mengajukan pendanaan <span class="font-w700"
                                                                            id="txt_judul_pendanaan"> </span> berikut
                                                                        dokumen pendukung yang harus anda miliki, centang
                                                                        jika file tersedia dan
                                                                        siap untuk di periksa tim Dana Syariah Indonesia.
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group row"
                                                                        id="checklist_persyaratan">

                                                                    </div>
                                                                    <label><b>Tanda</b></label> <label style="color:red"> *
                                                                    </label> <label><b>Dokumen harus ada</b></label>
                                                                    <label><button type="button"
                                                                            onclick="printPersyaratan()">Print
                                                                            Persyaratan</button></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- ./ 1 -->
                                                </div>
                                                <!-- END Steps Content -->
                                            </div>
                                        </div>
                                        <!-- Steps Navigation -->
                                        <div class="block-content block-content-sm block-content-full bg-body-light">
                                            <div class="row">
                                                <div class="col-6">
                                                    <button type="button" class="btn btn-alt-secondary"
                                                        data-wizard="prev">
                                                        <i class="fa fa-angle-left mr-5"></i> Sebelumnya
                                                    </button>
                                                </div>
                                                <div class="col-6 text-right">
                                                    <button type="button" class="btn btn-alt-secondary"
                                                        id="btn_selanjutnya" data-wizard="next">
                                                        Selanjutnya <i class="fa fa-angle-right ml-5"></i>
                                                    </button>
                                                    <button type="button" id="btn_pengajuan"
                                                        class="btn btn-alt-primary d-none" data-wizard="finish">
                                                        <i class="fa fa-check mr-5"></i> Kirim
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END Steps Navigation -->
                                    </form>
                                    <!-- END Form -->
                                </div>
                                <!-- END Progress Wizard 2 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END: Main Container -->

    <!-- STATRT: Pop In Modal -->
    <div class="modal fade" id="modal_action_lengkapi_profile" tabindex="-1" role="dialog"
        aria-labelledby="modal-popin" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Pengajuan Pendanaan</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Anda yakin ingin melanjutkan pendanaan ini?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" id="btn_proses_pendanaan" class="btn btn-alt-success"
                        data-dismiss="modal">Proses</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Pop In Modal -->

@endsection

@section('script')
    <script src="/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <script>
        // active layout type untuk borrower

        // data tipe pendanaan
        var type_borrower = "{{ Session::get('brw_type') }}";
        //var plafon_sisa = "{{ Session::get('brw_psisa') }}";

        if (type_borrower == 1) {
            $('#txt_tipe_borrower').text('Individu');
        } else if (type_borrower == 2) {
            $('#txt_tipe_borrower').text('Wirausaha');
        } else {
            $('#txt_tipe_borrower').text('Badan Hukum');
        }

        $.getJSON("/borrower/data_kota_list", function(data_list_kota) {

            $('.txt_provinsi_penerbit').prepend('<option selected></option>').select2({
                placeholder: "Pilih Provinsi",
                allowClear: true,
                data: data_list_kota,
                //multiple: true,
                width: 270
            });
        });

        $.getJSON("/borrower/tipe_pendanaan/{{ Session::get('brw_type') }}", function(data_tipe_pendanaan) {

            if (type_borrower == 1) {
                $('#type_pendanaan_select').prepend('<option selected></option>').select2({
                    placeholder: "Pilih Tipe Pendanaan",
                    allowClear: true,
                    data: data_tipe_pendanaan,

                    width: 500
                });
            } else {
                $('.type_pendanaan_individu').hide();
                $('.type_pendanaan_perusahaan').show();
                $('#type_pendanaan_select_bdn_hukum').prepend('<option selected></option>').select2({
                    placeholder: "Pilih Tipe Pendanaan",
                    allowClear: true,
                    data: data_tipe_pendanaan,

                    width: 500
                });
                $('#type_pendanaan_select_bdn_hukum option[value="2"]').remove()
            }

        });

        //var match = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'image/jpeg', 'image/png', 'image/jpg'];
        var tipe_file_gambar = ['image/jpeg', 'image/png', 'image/jpg'];
        $(".gambar_check").change(function() {
            for (i = 0; i < this.files.length; i++) {
                var file = this.files[i];
                var fileType = file.type;

                if (!((fileType == tipe_file_gambar[0]) || (fileType == tipe_file_gambar[1]) || (fileType ==
                        tipe_file_gambar[2]))) {
                    Swal.fire({
                        position: 'center',
                        icon: 'failed',
                        title: 'Pastikan File Format JPG, JPEG, & PNG',
                        showConfirmButton: true,
                    });
                    $(".gambar_check").val('');
                    return false;
                }
            }
        });

        var tipe_file_doc = ['application/pdf'];
        $(document).on("change", "input[name='file_jaminan[]']", function() {
            for (i = 0; i < this.files.length; i++) {
                var file = this.files[i];
                var filename = file.name;
                var fileType = file.type;

                if (!((fileType == tipe_file_doc[0]))) {
                    Swal.fire({
                        position: 'center',
                        icon: 'failed',
                        title: 'Pastikan Sertifikat yang anda unggah dengan Format PDF',
                        showConfirmButton: true,
                    });
                    $("input[name='file_jaminan[]']").val('');
                    return false;
                } else {

                    //$("#file_jaminan").val(filename);

                }
            }
        });

        showGambarUtama = function() {
            var file = document.getElementById('gambar_utama');
            var output = document.getElementById('listGambarUtama');
            var children = "";
            // for (var i = 0; i < file.files.length; ++i) {
            //     children += '<li>' + file.files.item(i).name + '</li>';
            // }
            output.innerHTML = '<ul><li>' + file.files.item(0).name + '</li></ul>';
        }

        showGambarSlider = function() {
            var file = document.getElementById('gambar_slider');
            var output = document.getElementById('listGambarSlider');
            var children = "";
            for (var i = 0; i < file.files.length; ++i) {
                children += '<li>' + file.files.item(i).name + '</li>';
            }
            output.innerHTML = '<ul>' + children + '</ul>';
        }

        showFilenameJaminan = function() {
            var file = document.getElementById('file_jaminan');
            var output = document.getElementById('listFileJaminan');
            var children = "";
            for (var i = 0; i < file.files.length; ++i) {
                children += '<li>' + file.files.item(i).name + '</li>';
            }
            output.innerHTML = '<ul>' + children + '</ul>';
        }


        // data persyaratan pendanaan pribadi
        $(function() {
            $('#type_pendanaan_select').change(function() {
                this.valid()
                var tipe_borrower_val = $("#type_borrower option:selected").val();
                var tipe_borrower_text = $("#type_borrower option:selected").text();
                var tipe_pendanaan = $('#type_pendanaan_select option:selected').val();
                var tipe_pendanaan_text = $('#type_pendanaan_select option:selected').text();
                var tipe_borrower = "{{ Session::get('brw_type') }}";
                var brw_id = "{{ Session::get('brw_id') }}";

                if (tipe_pendanaan == 2 && tipe_pendanaan_text == "Dana Rumah") {
                    location.href = "/borrower/ajukanPendanaanDanaRumah";
                } else {
                    $.getJSON("/borrower/persyaratan_pendanaan_proses_pengajuan/" + brw_id + "/" +
                        tipe_borrower + "/" + tipe_pendanaan,
                        function(data_persyaratan) {
                            $("#txt_judul_pendanaan").text(tipe_pendanaan_text);

                            var html = "";
                            $('#checklist_persyaratan').html('');
                            for (var i = 0, len = data_persyaratan.length; i < len; ++i) {
                                var mandatory = "";
                                var checked = "";
                                var disabled = "";
                                if (data_persyaratan[i].persyaratan_mandatory == 1) {
                                    mandatory += '<span class="text-danger">*</span>';
                                    checked += 'checked';
                                    disabled += 'disabled';
                                }


                                html += '<div class="col-6">' +
                                    '<label class="css-control css-control-primary css-radio mr-10 text-dark">' +
                                    '<input type="checkbox" ' + disabled + ' ' + checked +
                                    ' class="css-control-input" id="txt_persyaratan_pendanaan" name="txt_persyaratan_pendanaan" value=' +
                                    data_persyaratan[i].persyaratan_id + '>' +
                                    '<span class="css-control-indicator"></span> ' + data_persyaratan[i]
                                    .persyaratan_nama + ' ' +
                                    mandatory +
                                    '</label>' +
                                    '</div>';
                            }

                            $('#checklist_persyaratan').append(html);
                        });
                }
            });

            // data persyaratan pendanaan badan hukum
            getPendanaanType(1)
            $('#type_pendanaan_select_bdn_hukum').change(function() {

                var tipe_borrower_val = $("#type_borrower option:selected").val();
                var tipe_borrower_text = $("#type_borrower option:selected").text();
                var tipe_pendanaan = $('#type_pendanaan_select_bdn_hukum option:selected').val();
                var tipe_pendanaan_text = $('#type_pendanaan_select_bdn_hukum option:selected').text();
                getPendanaanType(tipe_pendanaan)

                var tipe_borrower = "{{ Session::get('brw_type') }}";

                //$("#txt_kota_bdn_hukum").empty().trigger('change'); // set null
                $.getJSON("/borrower/persyaratan_pendanaan/" + tipe_borrower + "/" + tipe_pendanaan,
                    function(data_persyaratan) {
                        $("#txt_judul_pendanaan").text(tipe_pendanaan_text);

                        var html = "";
                        $('#checklist_persyaratan').html('');
                        for (var i = 0, len = data_persyaratan.length; i < len; ++i) {
                            var mandatory = "";
                            var checked = "";
                            var disabled = "";
                            if (data_persyaratan[i].persyaratan_mandatory == 1) {
                                mandatory += '<span class="text-danger">*</span>';
                                checked += 'checked';
                                disabled += 'disabled';
                            }
                            html += '<div class="col-6">' +
                                '<label class="css-control css-control-primary css-radio mr-10 text-dark">' +
                                '<input type="checkbox" ' + disabled + ' ' + checked +
                                ' class="css-control-input" id="txt_persyaratan_pendanaan" name="txt_persyaratan_pendanaan" value=' +
                                data_persyaratan[i].persyaratan_id + '>' +
                                '<span class="css-control-indicator"></span> ' + data_persyaratan[i]
                                .persyaratan_nama + ' ' +
                                mandatory +
                                '</label>' +
                                '</div>';
                        }

                        $('#checklist_persyaratan').append(html);
                    });

            });


            $('#txt_durasi_pendanaan').on('keyup', function() {
                //    alert( $('#txt_durasi_pendanaan').val());
                var check_type_tujuan_pendanaan = $("#type_tujuan_pendanaan option:selected").val();

                var durasi_pendanaan = parseInt($('#txt_durasi_pendanaan').val());
                $.get("/borrower/tujuan_pendanaan_tenor/" + check_type_tujuan_pendanaan, function(data) {

                    if (durasi_pendanaan < 1 || durasi_pendanaan > parseInt(data)) {
                        Swal.fire({
                            position: 'center',
                            icon: 'failed',
                            title: 'Pastikan Durasi Pendanaan Minimal 1 s/d ' + data +
                                ' bulan',
                            showConfirmButton: false,
                            timer: 3500
                        });

                        $('#txt_durasi_pendanaan').val('');
                        return false;
                    }


                });

            });

        });

        const getPendanaanType = (tipe_pendanaan) => {
            $('#type_tujuan_pendanaan').empty()
            $.ajax({
                url: '/borrower/tujuan_pendanaan_kpr',
                method: 'GET',
                data: {
                    'tipe_pendanaan': tipe_pendanaan
                },
                success: (data_tujuan_pendanaan) => {
                    let this_data = JSON.parse(data_tujuan_pendanaan)

                    this_data.forEach(element => {
                        $('#type_tujuan_pendanaan').append($('<option>', {
                            value: element.id,
                            text: element.text
                        }));
                    });
                },
                error: (response) => {
                    alert('erroor')
                }
            })
        }



        $('.validasiString').on('input', function(event) {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        var check_imbal = false;

        function checkImbalHasil() {
            var imbal_hasil = $('#txt_imbal_hasil').val();
            $("#div_imbal_hasil").removeClass("has-error has-success is-invalid");
            $("#div_imbal_hasil label").remove();
            $("#div_imbal_hasil").prepend('<label class="control-label" for="txt_dana_pendanaan"></label>');

            if (imbal_hasil < 18) {

                if (check_imbal) {
                    clearTimeout(check_imbal);
                }
                check_imbal = setTimeout(function() {

                    //$("#div_dana_dibutuhkan").remove();
                    $("#div_imbal_hasil").addClass("has-error is-invalid");
                    $("#div_imbal_hasil").prepend(
                        '<label style="color:red;" for="txt_dana_pendanaan"> <i class="fa fa-times-circle-o"></i> Imbal Hasil Minimal 18%</label>'
                        );

                }, 100);

            } else {

                $("#div_imbal_hasil").addClass("has-success is-valid");
                $("#div_imbal_hasil").removeClass("is-invalid");
                $("#div_imbal_hasil").prepend(
                    '<label for="txt_dana_pendanaan"> <i class="fa fa-check"> </i> Imbal hasil yang ditawarkan (% per tahun) </label> <label class="mandatory_label"> * </label>'
                    );

            }
        }

        var tenor_proyek = false;

        function checkDurasi() {
            var durasi = $('#txt_durasi_pendanaan').val();

            $("#div_durasi_proyek").removeClass("has-error has-success is-invalid");
            $("#div_durasi_proyek label").remove();
            $("#div_durasi_proyek").prepend('<label class="control-label" for="txt_dana_pendanaan"></label>');

            // var dana = $("#txt_dana_pendanaan").val();
            var getDana = $('#txt_dana_pendanaan').val();
            var dana = parseInt(getDana.replaceAll('.', ''));
            var plafon = 2000000000;
            if (durasi < 3 || durasi > 12) {

                if (tenor_proyek) {
                    clearTimeout(tenor_proyek);
                }
                tenor_proyek = setTimeout(function() {

                    //$("#div_dana_dibutuhkan").remove();
                    $("#div_durasi_proyek").addClass("has-error is-invalid");
                    $("#div_durasi_proyek").prepend(
                        '<label style="color:red;" for="txt_dana_pendanaan"><i class="fa fa-times-circle-o"></i> Durasi Minimal 3 s/d 12 bulan</label>'
                        );
                    // $('#txt_durasi_pendanaan').val(0);
                }, 100);

            } else {

                $("#div_durasi_proyek").addClass("has-success is-valid");
                $("#div_durasi_proyek").removeClass("is-invalid");
                $("#div_durasi_proyek").prepend(
                    '<label for="txt_dana_pendanaan"><i class="fa fa-check"></i> Durasi Proyek/Tenor </label> <label class="mandatory_label"> * </label>'
                    );

            }

        }



        function createSimulasi() {

            var EstimasiWaktu = $('#txt_estimasi_proyek').val();
            var getDanaDibutuhkan = $('#txt_dana_pendanaan').val();
            var DanaDibutuhkan = parseInt(getDanaDibutuhkan.replaceAll('.', ''));
            var Durasi = $('#txt_durasi_pendanaan').val();
            var ImbalHasil = $('#txt_imbal_hasil').val();
            var cicilanBulanan = 2;
            var imbalPercent = DanaDibutuhkan * ((ImbalHasil / 100) * (Durasi / 12));
            var totalPinjaman = parseInt(DanaDibutuhkan) + parseInt(imbalPercent);
            var percentCicilan = (DanaDibutuhkan * cicilanBulanan) / 100;
            var nTenor = Durasi - 1;
            var endCicilan = totalPinjaman - (nTenor * percentCicilan);
            var tagihanPokokAkhir = totalPinjaman - (percentCicilan * nTenor);
            var plafon_sisa = $("#nilai_tersedia").val();




            $("#Divsimulasi").fadeIn(1000, function() {
                $("#list_simulasi").DataTable().clear().draw();
                if (Durasi < 3 || Durasi > 12) {
                    console.log('durasi dbawah atau diatas');
                    $("#Divsimulasi").html();
                    $("#list_simulasi").DataTable().clear().draw();
                } else if (ImbalHasil < 18) {
                    console.log('imbal hasil dbawah 18');
                    $("#Divsimulasi").html();
                    $("#list_simulasi").DataTable().clear().draw();
                } else if (plafon_sisa < DanaDibutuhkan) {
                    console.log('imbal hasil dbawah 18');
                    $("#Divsimulasi").html();
                    $("#list_simulasi").DataTable().clear().draw();
                } else {

                    let total_bayar = 0;
                    let sisa_tagihan = totalPinjaman;

                    let table_simulasi = $("#list_simulasi").DataTable({
                        destroy: true,
                        "searchable": false,
                        "paging": false,
                        bFilter: false,
                        "bPaginate": false,
                        "bInfo": false,
                    });

                    for (var i = 1; i <= Durasi; i++) {

                        let tgl_mulai_proyek = new Date(EstimasiWaktu);
                        let newDate = new Date(tgl_mulai_proyek.setMonth(tgl_mulai_proyek.getMonth() + i));

                        let month = newDate.getUTCMonth() + 1; //months from 1-12
                        let day = newDate.getUTCDate();
                        let year = newDate.getUTCFullYear();

                        let newdate = day + "/" + month + "/" + year;
                        if (i == Durasi) {
                            total_bayar += endCicilan;
                            sisa_tagihan -= endCicilan;
                            table_simulasi.row.add([
                                i,
                                newdate,
                                'Bulan ke ' + i,
                                parseInt(tagihanPokokAkhir).toLocaleString(),
                                parseInt(sisa_tagihan).toLocaleString(),
                            ]).draw();

                        } else {
                            total_bayar += percentCicilan;
                            sisa_tagihan -= percentCicilan;
                            table_simulasi.row.add([
                                i,
                                newdate,
                                'Bulan ke ' + i,
                                parseInt(percentCicilan).toLocaleString(),
                                parseInt(sisa_tagihan).toLocaleString(),
                            ]).draw();
                        }
                    };
                    // let text_total_bayar = 0;
                    // text_total_bayar = 'Rp.'+parseInt(total_bayar).toLocaleString()+',-';
                    // $("#simulasi_total_bayar").html(text_total_bayar);
                }
            });
        }

        // check format email
        var dana_dibutuhkan = false;

        function check_dana_dibutuhkan() {
            $("#div_dana_dibutuhkan").removeClass("has-error has-success is-invalid");
            $("#div_dana_dibutuhkan label").remove();
            $("#div_dana_dibutuhkan").prepend('<label class="control-label" for="txt_dana_pendanaan"></label>');

            var getDana = $('#txt_dana_pendanaan').val();
            var dana = parseInt(getDana.replaceAll('.', ''));
            console.log(dana)
            var plafon = 2000000000;
            var plafon_sisa = ($("#nilai_tersedia").val());


            if (dana < 100000000) {

                if (dana_dibutuhkan) {
                    clearTimeout(dana_dibutuhkan);
                }
                dana_dibutuhkan = setTimeout(function() {

                    //$("#div_dana_dibutuhkan").remove();
                    $("#div_dana_dibutuhkan").addClass("has-error is-invalid");
                    $("#div_dana_dibutuhkan").prepend(
                        '<label style="color:red;" for="txt_dana_pendanaan"><i class="fa fa-times-circle-o"></i> Minimal 100.000.000 (RP)</label>'
                        );

                }, 100);


            } else if (dana > plafon) {

                if (dana_dibutuhkan) {
                    clearTimeout(dana_dibutuhkan);
                }
                dana_dibutuhkan = setTimeout(function() {

                    //$("#div_dana_dibutuhkan").remove();
                    $("#div_dana_dibutuhkan").addClass("has-error is-invalid");
                    $("#div_dana_dibutuhkan").prepend(
                        '<label style="color:red;" for="txt_dana_pendanaan"><i class="fa fa-times-circle-o"></i> Max RP 2.000.000.000</label>'
                        );

                }, 100);
            } else if (dana > plafon_sisa) {

                clearTimeout(dana_dibutuhkan);

                dana_dibutuhkan = setTimeout(function() {

                    $("#div_dana_dibutuhkan").removeClass("is-invalid");
                    $("#div_dana_dibutuhkan").addClass("has-error is-invalid");
                    $("#div_dana_dibutuhkan").prepend(
                        '<label style="color:red;" for="txt_dana_pendanaan"><i class="fa fa-times-circle-o"></i> Dana dibutuhkan melebihi dana sisa </label>'
                        );

                }, 100);
            } else {

                $("#div_dana_dibutuhkan").addClass("has-success is-valid");
                $("#div_dana_dibutuhkan").removeClass("is-invalid");
                $("#div_dana_dibutuhkan").prepend(
                    '<label for="txt_dana_pendanaan"><i class="fa fa-check"></i> Dana Dibutuhkan (RP)</label>');

            }
        }

        // validasi hanya angka


        $('#txt_asset_bdn_hukum').on('input', function(event) {
            this.value = this.value.replace(/[^0-9]/g, '');
        });
        $('#txt_dana_pendanaan').on('input', function(event) {
            this.value = this.value.replace(/[^0-9.]/g, '');
        });
        $('#txt_durasi_pendanaan').on('input', function(event) {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        $("#btn_pengajuan").click(function() {
            $("#modal_action_lengkapi_profile").modal({
                backdrop: 'static',
                keyboard: false
            });
        });

        //validasi form
        $('.checkKarakterAneh').on('input', function(event) {
            this.value = this.value.replace(/[^a-zA-Z ]/g, '');
        });

        $('.allowCharacter').on('input', function(event) {
            this.value = this.value.replace(/[^a-zA-Z0-9.,-/ ]/g, '');
        });

        $('.allowCharacterdate').on('input', function(event) {
            this.value = this.value.replace(/[^0-9.,-/]/g, '');
        });

        // data jenis jaminan
        $.getJSON("/borrower/jenis_jaminan", function(data_jenis_jaminan) {
            $('.jenisjaminan').prepend('<option selected></option>').select2({
                // $('.jenisjaminan').prepend('<option selected></option>').select2({
                placeholder: "Pilih Jenis Jaminan",
                allowClear: true,
                data: data_jenis_jaminan,
                //multiple: true,
                width: 330
            });
        });

        // print persyaratan
        function printPersyaratan() {
            var divToPrint = document.getElementById('checklist_persyaratan');
            var newWin = window.open('', 'Print-Window');

            newWin.document.open();

            newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

            newWin.document.close();

        }

        function add_jaminan() {
            var jumlahBox = $(".ClassJaminan:last").attr("id"); // dapetin last id urut
            var split_id = jumlahBox.split("_"); // misahin _
            var nextID = Number(split_id[1]) + 1; // id terakhir + 1


            var tambahJaminan =
                // data jenis jaminan
                '<div class="col-12 mt-3" id="divCountJaminan_' + nextID + '">' +
                '<h6 class="content-heading text-muted font-w600" style="font-size: 1em;">Jaminan Pendanaan</h6>' +
                '<div class="row">' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label for="wizard-progress2-namapengurus">Nama Pemilik Jaminan</label>' +
                '<input class="form-control allowCharacter" type="text"  name="txt_nm_jaminan_pendanaan[]" placeholder="Nama Pemilik Jaminan..." required> ' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label for="wizard-progress2-nik">Jenis Jaminan</label><br/>' +
                '<select class="form-control jenisjaminan" id="txt_jenis_jaminan_pendanaan" name="txt_jenis_jaminan_pendanaan[]" required></select>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label for="wizard-progress2-nik">Nomor Sertifikat</label>' +
                '<input class="form-control validasiString" type="text" id="txt_nomor_surat" name="txt_nomor_surat[]" placeholder="Nomor Sertifikat..." required> ' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group" id="div_tlp">' +
                '<label for="wizard-progress2-tempatlahir">Kantor Penerbit</label> <label class="mandatory_label">*</label>' +
                '<div class="input-group">' +
                '<div class="input-group-append">' +
                '<span class="input-group-text input-group-text-dsi text-center" style="height:27px"> BPN </span>' +
                '<select class="form-control txt_provinsi_penerbit" id="txt_provinsi_penerbit" name="txt_provinsi_penerbit[]" required></select>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>'

                +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label for="wizard-progress2-nik">NOP (Nomor Objek Pajak)</label>' +
                '<input class="form-control validasiString " type="text" id="txt_nomor_wajib_pajak" name="txt_nomor_wajib_pajak[]" placeholder="Nomor Wajib Pajak..." required>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-4">' +
                '<div class="form-group">' +
                '<label for="wizard-progress2-nik">Nilai Objek Pajak (Rp)</label>' +
                '<input class="form-control allowCharacter"  max="10000000000"  id="txt_nilai_objek_pajak" name="txt_nilai_objek_pajak[]" placeholder="Nilai Objek Pajak..." required>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col-md-4">'

                +
                '<label for="wizard-progress2-nik">Unggah Sertifikat 3 halaman pertama (.pdf)</label>' +
                '<div id="listFileJaminan"></div><div class="form-group">' +
                '<div class="custom-file">' +
                '<input type="file" class="custom-file-input doc_check" id="file_jaminan" name="file_jaminan[]" onchange="javascript:showFilenameJaminan()" accept="application/pdf" data-toggle="custom-file-input" required>' +
                '<label class="custom-file-label" id="txt_filename_' + nextID +
                '" for="example-file-input-custom">Choose file</label>' +
                ' </div></div>' +
                '</div>' +
                '<div class="col-md-8">' +
                '<div class="form-group">' +
                '<label for="wizard-progress2-nik">Detail Jaminan</label>' +
                '<textarea class="form-control detailjaminan allowCharacter" rows="4" cols="80" id="txt_detail_jaminan_pendanaan" name="txt_detail_jaminan_pendanaan[]" required></textarea>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<button type="button" class="btn btn-rounded btn-danger mb-10 push-right" id="delete_jaminan"> <i class="fa fa-times"></i>  Hapus Jaminan Ini</button><hr>' +
                '</div>';

            $('#tambahJaminan').append(tambahJaminan);

            $.getJSON("/borrower/jenis_jaminan", function(data_jenis_jaminan) {
                $('.jenisjaminan').prepend('<option selected></option>').select2({
                    // $('.jenisjaminan').prepend('<option selected></option>').select2({
                    placeholder: "Pilih Jenis Jaminan",
                    allowClear: true,
                    data: data_jenis_jaminan,
                    //multiple: true,
                    width: 330
                });
            });

            $.getJSON("/borrower/data_provinsi", function(data_provinsi) {

                $('.txt_provinsi_penerbit').prepend('<option selected></option>').select2({
                    placeholder: "Pilih Provinsi",
                    allowClear: true,
                    data: data_provinsi,
                    width: 270
                });
            });


            showFilenameJaminan = function() {
                var file = document.getElementById('file_jaminan');
                // for (var i = 0; i < file.files.length; ++i) {
                //     children += '<li>' + file.files.item(i).name + '</li>';
                // }
                // output.innerHTML = '<ul><li>' + file.files.item(0).name + '</li></ul>';
                $("#txt_filename_" + nextID + "").text(file.files.item.name);
            }
            // showFilenameJaminan = function() {
            // var file = document.getElementById('file_jaminan');
            // var output = document.getElementById('listFileJaminan');
            // var children = "";
            // for (var i = 0; i < file.files.length; ++i) {
            // children += '<li>' + file.files.item(i).name + '</li>';
            // }
            // output.innerHTML = '<ul>'+children+'</ul>';
            // }


        }

        $(document).on("click", "#delete_jaminan", function() {
            $(this).parent().remove();
        });

        $("#btn_proses_pendanaan").click(function() {

            // $("#btn_proses_pendanaan").attr("disabled", true);

            var type_borrower = "{{ Session::get('brw_type') }}";
            var plafon_sisa = $("#nilai_tersedia").val();
            //pendanaan;
            var plafon = 2000000000;
            var type_pendanaan_select_individu = $("#type_pendanaan_select option:selected").val();
            var type_pendanaan_select_bdn_hukum = $("#type_pendanaan_select_bdn_hukum option:selected").val();
            //alert(type_pendanaan_select_bdn_hukum);
            var type_pendanaan_select = "";
            if (type_borrower == 1) {
                type_pendanaan_select = type_pendanaan_select_individu;
            } else {
                type_pendanaan_select = type_pendanaan_select_bdn_hukum;
            }
            var type_tujuan_pendanaan = $("#type_tujuan_pendanaan option:selected").val();
            // alert(type_tujuan_pendanaan);
            var txt_nm_pendanaan = $("#txt_nm_pendanaan").val();
            var txt_jenis_akad_pendanaan = $("#txt_jenis_akad_pendanaan option:selected").val();
            var getDanaval = $("#txt_dana_pendanaan").val();
            var txt_dana_pendanaan = parseInt(getDanaval.replaceAll('.', ''));
            var estimasi_proyek = $("#txt_estimasi_proyek").val();
            // var gambar_utama = $('#gambar_utama')[0].files;
            // var gambar_slider1 = $('#gambar_slider1')[0].files;
            // var gambar_slider2 = $('#gambar_slider2')[0].files;
            var txt_lokasi_proyek = $('#txt_lokasi_proyek').val();
            var txt_geocode = $('#txt_geocode').val();
            var txt_estimasi_proyek = estimasi_proyek.split("-").reverse().join("-");
            var txt_estimasi_imbal_hasil = $("#txt_imbal_hasil").val();
            var txt_durasi_pendanaan = $("#txt_durasi_pendanaan").val();
            var txt_pembayaran_pendanaan = $("input[name=txt_pembayaran_pendanaan]:checked").val();
            var txt_metode_pembayaran_pendanaan = $("input[name=txt_metode_pembayaran_pendanaan]:checked").val();
            var txt_detail_pendanaan = $('#txt_detail_pendanaan').val();
            // var txt_detail_pendanaan = tinyMCE.get('txt_detail_pendanaan').getContent();
            // if(txt_detail_pendanaan == ""){
            // alert('kosong bro');
            // }
            // $("#txt_detail_pendanaan").val();
            var jaminan_nama = [];
            jaminan_nomor = [];
            jaminan_jenis = [];
            jaminan_nilai = [];
            jaminan_detail = [];
            jaminan_status = [];
            jaminan_kantor_penerbit = [];
            jaminan_nomor_wajib_pajak = [];

            // jaminan_nama
            $('input[name="txt_nm_jaminan_pendanaan[]"]').each(function() {
                jaminan_nama.push(this.value);
            });

            // jaminan_nomor
            $('input[name="txt_nomor_surat[]"]').each(function() {
                jaminan_nomor.push(this.value);
            });

            // jaminan_jenis
            $('.jenisjaminan').each(function() {
                jaminan_jenis.push(this.value);
            });
            // alert(jaminan_jenis);
            //jaminan_nilai
            $('input[name="txt_nilai_objek_pajak[]"]').each(function() {
                jaminan_nilai.push(this.value);
            });

            $('.txt_provinsi_penerbit option:selected').each(function(i, u) {
                jaminan_kantor_penerbit.push(this.text);
            });

            console.log(jaminan_kantor_penerbit);
            $('input[name="txt_nomor_wajib_pajak[]"]').each(function() {
                jaminan_nomor_wajib_pajak.push(this.value);
            });
            // jaminan_detail
            $('.detailjaminan').each(function() {
                jaminan_detail.push(this.value);
            });

            var jaminan = [];
            for (var u = 0; u < jaminan_nama.length; u++) {
                jaminan[u] = [
                    jaminan_nama[u] + "@",
                    "@" + jaminan_jenis[u] + "@",
                    "@" + jaminan_nomor[u] + "@",
                    "@" + jaminan_kantor_penerbit[u] + "@",
                    "@" + jaminan_nomor_wajib_pajak[u] + "@",
                    "@" + jaminan_nilai[u] + "@",
                    "@" + jaminan_detail[u],

                ];
            }
            var jaminan_arr = jaminan.join("^~");

            // persyaratan
            var persyaratanList = [];
            var i = 0;

            $("#txt_persyaratan_pendanaan:checked").each(function() {

                if (this.checked) {
                    persyaratanList.push(this.value);
                    i++;
                }

            });

            var persyaratan_arr = persyaratanList.join(",");



            var dataForm = new FormData();
            var file_jaminan_jumlah = document.getElementById('file_jaminan').files.length;


            dataForm.append('_token', "{{ csrf_token() }}");
            dataForm.append('type_borrower', type_borrower);
            dataForm.append('txt_nm_pendanaan', txt_nm_pendanaan);
            dataForm.append('txt_jenis_akad_pendanaan', txt_jenis_akad_pendanaan);
            dataForm.append('txt_dana_pendanaan', txt_dana_pendanaan);


            for (var i = 0; i < file_jaminan_jumlah; i++) {
                dataForm.append("file_jaminan[]", document.getElementById('file_jaminan').files[i]);
            }
            dataForm.append('txt_lokasi_proyek', txt_lokasi_proyek);
            dataForm.append('txt_geocode', txt_geocode);
            dataForm.append('txt_estimasi_proyek', txt_estimasi_proyek);
            dataForm.append('txt_estimasi_imbal_hasil', txt_estimasi_imbal_hasil);
            dataForm.append('txt_durasi_pendanaan', txt_durasi_pendanaan);
            dataForm.append('txt_pembayaran_pendanaan', txt_pembayaran_pendanaan);
            dataForm.append('txt_metode_pembayaran_pendanaan', txt_metode_pembayaran_pendanaan);
            dataForm.append('txt_detail_pendanaan', txt_detail_pendanaan);
            dataForm.append('type_pendanaan_select', type_pendanaan_select);
            dataForm.append('type_tujuan_pendanaan', type_tujuan_pendanaan);

            dataForm.append('jaminan', jaminan_arr);

            dataForm.append('persyaratan_arr', persyaratan_arr);


            $.ajax({
                url: "{{ route('borrower.action_pendanaan') }}",
                type: "POST",
                data: dataForm,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    $("#overlay").css('display', 'block');
                },
                success: function(response) {

                    console.log(response);
                    if (response.status == "sukses") {
                        $("#overlay").css('display', 'none');
                        swal.fire({
                            title: 'Pengajuan Berhasil dibuat',
                            allowOutsideClick: false,
                            html: "Hi {{ Session::get('brw_nama') }} , <br/> Pengajuan Pendanaan anda berhasil dikirim, tim kami akan segera menghubungi anda. Harap mempersiapkan aplikasi zoom meeting untuk melakukan verifikasi data secara video conference. <br/> Terima Kasih! <br/> <br/><span><button class='btn btn-primary btn-lg' id='btn_confirm'>OK</button></span>",
                            type: "success",
                            showConfirmButton: false
                        });

                        $('#btn_confirm').on('click', function() {

                            location.href = "/borrower/beranda";
                        });


                    } else {

                        $("#overlay").css('display', 'none');

                        if (response.status == 'limit') {
                            swal.fire({
                                    title: "Gagal Menyimpan Data",
                                    type: "error",
                                    text: response.message,
                                    showCancelButton: false,
                                    allowOutsideClick: false,
                                    confirmButtonClass: "btn-danger",
                                });
                        } else {
                            Swal.fire({
                                title: 'Do you want to save the changes?',
                                showDenyButton: false,
                                showCancelButton: false,
                                confirmButtonText: `Ok`,
                                denyButtonText: `Don't save`,
                            }).then((result) => {
                                /* Read more about isConfirmed, isDenied below */
                                if (result.isConfirmed) {
                                    location.href = "/borrower/beranda";
                                }
                            })

                        }




                    }
                }
            });


        });
    </script>
    <script>
        tinymce.init({
            selector: '#txt_detail_pendanaan_old',
            height: 300,
            theme: 'modern',
            skin: 'lightgray',
            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
            toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
            image_advtab: true,
            file_picker_callback: function(callback, value, meta) {
                if (meta.filetype == 'image') {
                    $('#upload').trigger('click');
                    $('#upload').on('change', function() {
                        var file = this.files[0];
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            callback(e.target.result, {
                                alt: ''
                            });
                        };
                        reader.readAsDataURL(file);
                    });
                }
            },
            imagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions',
            templates: [{
                    title: 'Test template 1',
                    content: 'Test 1'
                },
                {
                    title: 'Test template 2',
                    content: 'Test 2'
                }
            ],
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });

        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }
    </script>
@endsection
