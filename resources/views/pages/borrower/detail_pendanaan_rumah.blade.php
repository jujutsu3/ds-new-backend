@extends('layouts.borrower.master')
{{-- s --}}
@section('title', 'Detail Pendanaan')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
        type="text/css" rel="stylesheet" />
    <style>
        .modal-dialog {
            max-width: 80%;
        }

        .btn-cancel {
            background-color: #C0392B;
            color: #FFFF;
        }

        label {
            font-size: 14px !important;
        }

        label.error {
            font-size: 10px !important;
            color: red !important;
        }

        .status-tolak {
            background: red !important;
        }

        .custom-file-label::after {
            content: "Pilih File" !important;
        }

        .bs-wizard>.bs-wizard-step+.bs-wizard-step {
            margin: 50px auto;
        }

        
    </style>
@endsection

@section('content')

    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="no-paddingTop font-w300 text-dark" style="float: left; margin-block-end: 0em;">
                                {{ $title }}</h1>

                        </span>
                    </div>
                </div>
                <div class="row mt-5 pt-5">
                    <div class="col-md-12 mt-5 pt-5">
                        <div class="row">

                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w200 d-none d-lg-block">Progress dan Status
                                    Pendanaan</h3>
                                <div class="block d-none d-lg-block">

                                    
                                    <div class="row pt-30 mt-10 bs-wizard" style="border-bottom:0;">
                                        <!-- step 1 -->
                                        <div class="bs-wizard-step complete d-none d-lg-block">
                                            <!-- complete -->
                                            <div class="progress" style="">
                                                <div class="progress-bar status-tolak "></div>
                                            </div>
                                        </div>
                                        <div class="col-1 bs-wizard-step {{ $pengajuan_awal }} d-none d-lg-block">
                                            <div class="text-center bs-wizard-stepnum">1</div>
                                            <p class="text-center text-dark">Pengajuan</p>
                                            {{-- <div class="progress" style="margin-left: 10px; border-radius: 10px 0px 0px 10px;height: 20px;">
                                                <div class="progress-bar {{ $status_tolak }}"></div>
                                            </div> --}}
                                            <a href="#" class="bs-wizard-dot"></a>

                                        </div>
                                        <!-- step 2 -->
                                        <div class="col-1 bs-wizard-step {{ $verifikasi_awal }} d-none d-lg-block">
                                            <!-- complete -->
                                            <div class="text-center bs-wizard-stepnum">2</div>
                                            <p class="text-center text-dark">Verifikasi Awal</p>
                                            {{-- <div class="progress" style="height: 20px;">
                                                <div class="progress-bar {{ $status_tolak }}"></div>
                                            </div> --}}
                                            <a href="#" class="bs-wizard-dot"></a>

                                        </div>

                                        <!-- step 3 -->
                                        <div class="col-1 bs-wizard-step {{ $verifikasi_lanjut }} d-none d-lg-block">
                                            <!-- active -->
                                            <div class="text-center bs-wizard-stepnum">3</div>
                                            <p class="text-center text-dark">Verifikasi Lanjutan</p>
                                            {{-- <div class="progress" style="height: 20px;">
                                                <div class="progress-bar {{ $status_tolak }}"></div>
                                            </div> --}}
                                            <a href="#" class="bs-wizard-dot"></a>

                                        </div>
                                        <!-- step 4 -->
                                        <div class="col-1 bs-wizard-step  {{ $penggalangan_dana }}  d-none d-lg-block">
                                            <!-- active -->
                                            <div class="text-center bs-wizard-stepnum">4</div>
                                            <p class="text-center text-dark">Penggalangan Dana</p>
                                            {{-- <div class="progress"
                                                style="height: 20px;">
                                                <div class="progress-bar {{ $status_tolak }}"></div>
                                            </div> --}}
                                            <a href="#" class="bs-wizard-dot"></a>
                                        </div>
                                        <!-- step 5 -->
                                        <div class="col-1  bs-wizard-step {{ $pencairan_dana }} d-none d-lg-block">
                                            <!-- disable -->
                                            <div class="text-center bs-wizard-stepnum">5</div>
                                            <p class="text-center text-dark">Pencairan dana</p>
                                            {{-- <div class="progress" style="height: 20px;">
                                                <div class="progress-bar {{ $status_tolak }}"></div>
                                            </div> --}}
                                            <a href="#" class="bs-wizard-dot"></a>

                                        </div>
                                        <!-- step 6 -->
                                        <div class="col-1 bs-wizard-step {{ $pendanaan_berjalan }} d-none d-lg-block">
                                            <!-- disable -->
                                            <div class="text-center bs-wizard-stepnum">6</div>
                                            <p class="text-center text-dark">Pendanaan Berjalan</p>
                                            {{-- <div class="progress" style="height: 20px;">
                                                <div class="progress-bar {{ $status_tolak }}"></div>
                                            </div> --}}
                                            <a href="#" class="bs-wizard-dot"></a>

                                        </div>

                                        <div class="col-1 bs-wizard-step {{ $pendanaan_selesai }}  d-none d-lg-block">
                                            <!-- disable -->
                                            <div class="text-center bs-wizard-stepnum">7</div>
                                            <p class="text-center text-dark">Pendanaan Selesai</p>

                                            {{-- <div class="progress" style="border-radius: 0px 10px 10px 0px;height: 20px;">
                                                <div class="progress-bar {{ $status_tolak }}"></div>
                                            </div> --}}
                                            <a href="#" class="bs-wizard-dot"></a>

                                        </div>

                                    
                                    </div>

                                    <div class="progress" style="margin-top: -60px !important;">
                                        <div class="progress-bar {{ $status_tolak }}" style="width:{{ $widthProgress }}%;"></div>
                                    </div>
                                </div>

                              
                            </div>
                            <div class="col-12 col-md-12">

                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                    <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                        <li class="nav-item hide">
                                            <a class="nav-link" href="#informasi-pendanaan" data-toggle="tab"
                                                style="font-size: 20px !important;">Data Pengajuan (1)</a>
                                        </li>

                                       @if(Auth::user()->status == 'active') 
                                            @if (!in_array($pengajuan->status, [0, 2, 12]))
                                                <li class="nav-item ">
                                                    <a class="nav-link" href="#pengajuan-pendanaan" data-toggle="tab"
                                                        style="font-size: 20px !important;">Data Pengajuan (2)</a>
                                                </li>

                                                <li class="nav-item hide">
                                                    <a class="nav-link" href="#dokumen-pendanaan" data-toggle="tab"
                                                        style="font-size: 20px !important;">Dokumen Pendukung</a>
                                                </li>
                                            @endif
                                        @endif

                                    </ul>
                                    <!-- END Step Tabs -->
                                    <!-- Form -->
                                    <div>
                                        <!-- Steps Content -->
                                        <div class="block-content block-content-full tab-content pl-30 pr-30"
                                            style="min-height: 274px;">

                                            <!-- Tab 1 -->

                                            @if($pengajuan->pendanaan_tipe === 1)
                                              @include('pages.borrower.info_pendanaan_konstruksi')
                                            @endif

                                            @if($pengajuan->pendanaan_tipe === 2)
                                              @include('pages.borrower.info_pendanaan_rumah')
                                            @endif


                                            {{-- @if ($pengajuan->status >= 1 && $pengajuan->status != '2') --}}
                                            @if(!in_array($pengajuan->status, [0, 2, 12]) && Auth::user()->status == 'active')
                                            <!-- END Tab 1 -->
                                                <!-- Tab 2 -->

                                                @if($pengajuan->pendanaan_tipe === 1)
                                                   @include('pages.borrower.pengajuan_pendanaan_konstruksi')                                            
                                                @endif


                                                @if($pengajuan->pendanaan_tipe === 2)
                                                    @include('pages.borrower.pengajuan_pendanaan_rumah')                                            
                                                @endif

                                                <!-- End Tab 2 -->
                                                <!-- Tab 3 -->
                                                @include('pages.borrower.dokumen_pendanaan_rumah')
                                                <!-- End Tab 3 -->
                                            @endif

                                        </div>

                                    </div>
                                    <!-- END Form -->
                                </div>
                                <!-- END Progress Wizard 2 -->

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
@endsection
<!-- END Main Container -->
@section('script')
    @stack('info-scripts')
    @stack('pengajuan-scripts')
    @stack('upload-scripts')
@endsection
