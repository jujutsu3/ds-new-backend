@extends('layouts.borrower.master')

@section('title', 'Add Material Order ')

@section('style')

    <link href="{{ asset('assetsBorrower/js/plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }} " rel="stylesheet" />
    <link href="{{ url('assetsBorrower/js/plugins/select2/css/select2-bootstrap.min.css') }} " rel="stylesheet" />
    <style>
        .scroll {
            overflow-y: scroll;
            height: 600px;
            display: block;
        }
    </style>

@endsection

@section('content')

    <main id="main-container">
        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h2 class="no-paddingTop font-w300 text-dark" style="float: left; margin-block-end: 0em;">
                                RAB - Pilih Barang </h2>
                        </span>
                    </div>
                </div>
                <div id="rab-add-products" class="row mt-5 pt-5">
                    <div class="col-12 mt-5 pt-5">
                        <div class="block-content border layout pt-4" id="block-add-products" disabled="">
                            <div class="block-content block-content-full tab-content pb-4 mb-4">
                                <div class="sw sw-theme-dots sw-justified">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item" aria-current="page"><a href="/borrower/beranda"
                                                    style="text-decoration: underline">List</a></li>

                                            <li class="breadcrumb-item" aria-current="page"><a
                                                    href="{{ route('borrower.rab.detail', $pengajuanId) }}"
                                                    style="text-decoration: underline">Detail </a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Pilih Barang</li>
                                        </ol>
                                    </nav>
                                   
                                    <div class="tab-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header text-white p-10 mb-4"
                                                        style="background-color: #18783A">
                                                        <i class="fa fa-search"></i> Pilih Barang
                                                    </div>

                                                    <div class="card-body">
                                                        <div class="col-md-12">
                                                            <div class="form-group">

                                                                <div class="row">
                                                                    <div class="col-12 mb-4">
                                                                        <div class="col-md-3 control-label">
                                                                            <label> Name </label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::text('name', '', [
                                                                                'id' => 'nameSearch',
                                                                                'placeholder' => 'Ketik disini',
                                                                                'class' => 'form-control',
                                                                            ]) !!}
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col-12 mb-4">
                                                                        <div class="col-md-3 control-label">
                                                                            <label> Kategori </label>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            {!! Form::select('kategori_id', $categories, '', [
                                                                                'id' => 'kategori_id',
                                                                                'placeholder' => 'Pilih Kategori',
                                                                                'class' => 'form-control select2',
                                                                            ]) !!}
                                                                        </div>

                                                                    </div>
                                                                </div>


                                                                <div class="col-md-12 mb-4">
                                                                    <label>Produk / Barang : </label>
                                                                    <section style="background-color: #eaf2d7;">
                                                                        <div class="container py-20 scroll"
                                                                            id="productList">
                                                                        </div>
                                                                    </section>
                                                                </div>

                                                                <div class="col-12 mb-4">
                                                                    <a href="{{ route('borrower.detail.cart', $pengajuanId) }}"
                                                                        class="col-12 btn btn-danger text-lg-center"><i
                                                                            class="fa fa-shopping-cart"></i> Keranjang <span
                                                                            id="totalItemCart">({!! $countItems !!})</span>
                                                                    </a>
                                                                </div>



                                                            </div>

                                                        </div>

                                                    </div>


                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>



            </div>

        </div>


    </main>

    <div class="modal fade" tabindex="-1" role="dialog" id="modalAddKeranjang">
        <div class="modal-dialog modal-lg" role="document">
            <form method="post" id="formAddKeranjang" action="{{ route('borrower.rab.add_cart') }}">
                <input type="hidden" name="pengajuan_id" id="pengajuan_id" value="{{ $pengajuanId }}" />
                <input type="hidden" name="material_item_id" id="material_item_id" />
                <input type="hidden" name="material_item_name" id="material_item_name" />
                <input type="hidden" name="material_item_desc" id="material_item_desc" />
                <input type="hidden" name="material_item_url" id="material_item_url" />
                <input type="hidden" name="item_category_id" id="item_category_id" />
                <input type="hidden" name="price" id="price" />
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title headerTitleBarang"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 mb-4">
                                    <div class="col-md-4 control-label">
                                        <label class="col-form-label-lg"> Tahap Pekerjaan </label>
                                    </div>
                                    <div class="col-md-8">
                                        {!! Form::select('task_id', $materialTaskList, '', [
                                            'id' => 'task_id',
                                            'placeholder' => 'Pilih',
                                            'class' => 'form-control',
                                            'required' => 'required',
                                        ]) !!}
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary btnAddToCart"> <i class="fa fa-shopping-bag"></i>
                            Lanjutkan</button>
                        <button type="button" class="btn btn-secondary" id="btnCloseAddToCart"
                            data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script type="text/javascript" src="{{ url('assetsBorrower/js/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ url('assetsBorrower/js/plugins/daterangepicker/js/daterangepicker.js') }}"></script>
    <script src="{{ url('assetsBorrower/js/plugins/select2/js/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $('body').on('hidden.bs.modal', '.modal', function() {
                $(this).find('form').trigger('reset');
            });

            $("#kategori_id").select2({
                width: '100%',
                placeholder: "Pilih Kategori",
                allowClear: true
            });

            $("#kategori_id").change(function(e) {
                productListing($(this).val());
            });

            $("#nameSearch").keyup(function(e) {
                productListing($(this).val());
            });

            const productListing = (kategori_id) => {

                let kategori = $("#kategori_id").val();
                let name = $("#nameSearch").val();

                $.ajax({
                    url: "/admin/material_orders/ajax/products",
                    type: "get",
                    dataType: "html",
                    data: {
                        _token: "{{ csrf_token() }}",
                        category_id: kategori,
                        name: name
                    },

                    beforeSend: () => {
                        swal.fire({
                            html: '<h5>Loading List Produk ... </h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                            }
                        });
                    },
                    error: function(xhr, status, error) {

                        var errorMessage = xhr.status + ': ' + xhr.statusText

                        if (xhr.status === 419) {
                            errorMessage = xhr.status +
                                ': Session expired. Silahkan re-login kembali.';
                        }

                        swal.fire({
                            title: "Proses Gagal",
                            text: errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        });
                    },
                    success: function(result) {
                        $('#productList').html('Preparing Result ...');
                        $('#productList').html(result);
                        swal.close();
                    }
                });

            }

            productListing("");

            $('#modalAddKeranjang').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget);
                $(".headerTitleBarang").html(button.data('title'));
                $("#price").val(button.data('price'));
                $("#material_item_id").val(button.data('id'));
                $("#item_category_id").val(button.data('category'));
                $("#material_item_name").val(button.data('title'));
                $("#material_item_desc").val(button.data('desc'));
                $("#material_item_url").val(button.data('image'));
            });

            $("form#formAddKeranjang").submit(function(e) {
                e.preventDefault();

                $.ajax({
                    type: $(this).attr('method'),
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: () => {
                        swal.fire({
                            html: '<h5>Menambahkan ke Keranjang ... </h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                            }
                        });
                    },
                    success: function(data) {

                        if (data.status == 'success') {
                            swal.fire({
                                title: "<h5>Produk Berhasil Ditambahkan ke Keranjang</h5>",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                            }).then((result) => {
                                $('#totalItemCart').html(data.countItemCart);
                                document.getElementById('btnCloseAddToCart').click();
                            });
                        } else {
                            let error_msg = data.message
                            console.log(error_msg)
                            swal.fire({
                                title: "Proses Gagal",
                                text: "Simpan Data Gagal",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            }).then((result) => {

                            })
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.status === 419) {
                            swal.fire({
                                title: "Proses Gagal",
                                type: "error",
                                text: "Session expired. Please re-login",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            })

                        }
                    },
                })

            });


        });
    </script>


@endsection
