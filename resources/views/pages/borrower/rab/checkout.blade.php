@extends('layouts.borrower.master')

@section('title', 'Checkout RAB')

@section('style')
    <link href="{{ asset('assetsBorrower/js/plugins/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ url('assetsBorrower/js/plugins/select2/css/select2.min.css') }} " rel="stylesheet" />
    <link href="{{ url('assetsBorrower/js/plugins/select2/css/select2-bootstrap.min.css') }} " rel="stylesheet" />
    <style>
        .scroll {
            overflow-y: scroll;
            height: 600px;
            display: block;
        }
    </style>
@endsection

@section('content')
    <main id="main-container">

        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">

                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h2 class="no-paddingTop font-w300 text-dark" style="float: left; margin-block-end: 0em;">
                                RAB - Checkout </h2>
                        </span>
                    </div>
                </div>

                <div id="layout-individu" class="row mt-5 pt-5">
                    <div class="col-12 mt-5 pt-5">

                        <div class="block-content border layout pt-4" id="layout-informasi-pribadi" disabled="">
                            <div class="block-content block-content-full tab-content pb-4 mb-4">
                                <div class="sw sw-theme-dots sw-justified">

                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item" aria-current="page"><a href="/borrower/beranda"
                                                    style="text-decoration: underline">List</a></li>
                                            <li class="breadcrumb-item" aria-current="page"><a
                                                    href="{{ route('borrower.rab.detail', $pengajuan->pengajuan_id) }}"
                                                    style="text-decoration: underline">Detail</a></li>
                                            <li class="breadcrumb-item" aria-current="page"
                                                style="text-decoration: underline"><a
                                                    href="{{ route('borrower.rab.add', $pengajuan->pengajuan_id) }}"
                                                    style="text-decoration: underline">Pilih Barang</a></li>
                                            <li class="breadcrumb-item" aria-current="page"
                                                style="text-decoration: underline"><a
                                                    href="{{ route('borrower.detail.cart', $pengajuan->pengajuan_id) }}">Keranjang</a>
                                            </li>
                                            <li class="breadcrumb-item active" aria-current="page">Checkout </li>

                                        </ol>
                                    </nav>
                                    <hr class="line mb-2 pb-4">
                                    <div class="tab-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header text-white p-10 mb-4" style="background-color: #18783A">
                                                        <i class="fa fa-shopping-bag"></i> Checkout
                                                    </div>
                                                    <form name="formKonfirmasiCheckout" id="formKonfirmasiCheckout" method="post"
                                                        action="{{ route('material_order.konfirmasi.checkout') }}">
                                                        <div class="card-body">
                                                            <input type="hidden" id="pengajuanId" name="pengajuanId"
                                                                value="{{ $pengajuan->pengajuan_id }}" />
                                
                                                            <input type="hidden" id="brwId" name="brwId" value="{{ $pengajuan->brw_id }}" />
                                                            <div class="col-md-12 mb-4">
                                                                <section style="background-color: #eaf2d7;">
                                                                    <div class="container py-20 scroll" id="productCart">
                                                                    </div>
                                                                </section>
                                                            </div>
                                
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="date_estimasi" class="font-weight-bold font-size-h5">Total Belanja </label>
                                                                    <h3 style="color: #18783A;margin-left: 10px;">Rp.
                                                                        {{ number_format($cartOrder->total_belanja, 0, ',', '.') }}</h3>
                                                                </div>
                                                            </div>
                                
                                
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="date_estimasi" class="font-weight-bold font-size-h5">Tanggal Permintaan </label>
                                                                    <input type="date" name="tglPengiriman" required class="form-control"
                                                                        value="{{ $materialHeader && $materialHeader->requisition_date ? $materialHeader->requisition_date : '' }}" />
                                                                </div>
                                                            </div>
                                
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label for="date_estimasi" class="font-weight-bold font-size-h5">Alamat Pengiriman</label>
                                                                    <textarea name="alamatPengiriman" class="form-control" placeholder="Ketik disini..." cols="50" rows="4"> {!! $materialHeader && $materialHeader->delivery_to_address ? $materialHeader->delivery_to_address : '' !!} </textarea>
                                                                </div>
                                                            </div>
                                
                                                            <div class="col-12 mb-4">
                                                                <button type="submit" class="col-12 btn btn-success text-lg-center btnKonfirmasi"
                                                                    style="background-color: #18783A"><i class="fa fa-check"></i> Konfirmasi <span
                                                                        id="countItems"></span> </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>

                            </div>

                        </div>


                    </div>

                </div>

            </div>

        </div>

    </main>


    <script type="text/javascript" src="{{ url('assetsBorrower/js/plugins/moment/moment.min.js') }}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            const productCartItems = () => {

                $.ajax({
                    url: "/admin/material_orders/ajax/cart",
                    type: "get",
                    dataType: "json",
                    data: {
                        _token: "{{ csrf_token() }}",
                        pengajuan_id: $("#pengajuanId").val(),
                        page: "checkout"
                    },

                    beforeSend: () => {
                        swal.fire({
                            html: '<h5>Loading Keranjang ... </h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                            }
                        });
                    },
                    error: function(xhr, status, error) {

                        var errorMessage = xhr.status + ': ' + xhr.statusText

                        if (xhr.status === 419) {
                            errorMessage = xhr.status +
                                ': Session expired. Silahkan re-login kembali.';
                        }

                        swal.fire({
                            title: "Proses Gagal",
                            text: errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        });
                    },
                    success: function(result) {

                        $('#productCart').html(result.message);

                        if (result.count == 0) {
                            $(".btnKonfirmasi").addClass('d-none');
                        } else {
                            $(".btnKonfirmasi").removeClass('d-none');
                        }
                        swal.close();
                    }
                });

            }

            productCartItems();


            $("form#formKonfirmasiCheckout").submit(function(e) {
                e.preventDefault();

                $.ajax({
                    type: $(this).attr('method'),
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend: () => {
                        swal.fire({
                            html: '<h5>Loading ... </h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                            }
                        });
                    },
                    success: function(data) {

                        if (data.status == 'success') {
                            swal.fire({
                                title: "<h5>Konfirmasi Pesanan Selesai </h5>",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                            }).then((result) => {
                                var linkDetilRab =
                                    '{{ route('borrower.rab.detail', ':pengajuan_id') }}';
                                linkDetilRab = linkDetilRab.replace(':pengajuan_id',
                                    data.pengajuan_id);
                                location.href = linkDetilRab;

                            });
                        } else {
                            let error_msg = data.message
                            console.log(error_msg)
                            swal.fire({
                                title: "Konfirmasi Pesanan Gagal",
                                text: "Simpan Data Gagal",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            }).then((result) => {

                            })
                        }
                    },
                    error: function(xhr, status, error) {
                        if (xhr.status === 419) {
                            swal.fire({
                                title: "Proses Gagal",
                                type: "error",
                                text: "Session expired. Please re-login",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            })

                        }
                    },
                })

            });

        });
    </script>


@endsection
