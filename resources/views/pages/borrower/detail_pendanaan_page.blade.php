@extends('layouts.borrower.master')

@section('title', 'Selamat Datang Penerima Dana')
<style>
  .dataTables_paginate { 
     float: right; 
     text-align: right; 
  }
  #allDetilImbal:hover{
    background-color: forestgreen !important;
  }
  #overlay{   
      position: fixed;
      top: 0;
      left: 0;
      z-index: 900;
      width: 100%;
      height:100%;
      display: none;
      background: rgba(0,0,0,0.6);
  }
  .cv-spinner {
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;  
  }
  .spinner {
      width: 40px;
      height: 40px;
      border: 4px #ddd solid;
      border-top: 4px #2e93e6 solid;
      border-radius: 50%;
      animation: sp-anime 0.8s infinite linear;
  }
  @keyframes sp-anime {
      100% { 
          transform: rotate(360deg); 
      }
  }
  .is-hide{
      display:none;
  }
</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="assets/js/plugins/slick/slick.css">
        <link rel="stylesheet" href="assets/js/plugins/slick/slick-theme.css">
        <script src="assets/js/plugins/slick/slick.min.js"></script>

        <!-- Page JS Helpers (Slick Slider plugin) -->
        <script>jQuery(function(){ Codebase.helpers('slick'); });</script>

@section('content')
    <div id="overlay">
    <div class="cv-spinner">
          <span class="spinner"></span>
    </div>
    </div>
    <!-- Main Container -->
    <main id="main-container">        
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: 0em;" >{{ $title }}</h1> 
                            <p class="pull-right mt-20 pt-5 text-dark"> 
                            <!-- <i class="fa fa-circle text-primary"></i> Aktif -->
                            <?php 
                                if($status == 0){ //pengajuan
                                    echo "<i class='fa fa-circle text-pengajuan pull-right ml-4 mt-0 pt-0 mr-10'></i> Pengajuan"; 
                                }elseif($status == 1){ //approved / aktif
                                    echo "<i class='fa fa-circle text-success pull-right ml-4 mt-0 pt-0 mr-10'></i> Aktif"; 
                                }elseif($status == 2 || $status == 3){ //peggalangan dana
                                    echo "<i class='fa fa-circle text-penggalangandana pull-right ml-4 mt-0 pt-0 mr-10'></i> Penggalangan Dana";
                                }elseif($status == 6){ //proses tanda tangan
                                    echo "<i class='fa fa-circle text-ttd pull-right ml-4 mt-0 pt-0 mr-10'></i> Proses TTD";
                                }elseif($status == 4){ //selesai
                                    echo "<i class='fa fa-circle text-selesai pull-right ml-4 mt-0 pt-0 mr-10'></i> Selesai";
                                }elseif($status == 7){
                                    echo "<i class='fa fa-circle text-proyekberjalan pull-right ml-4 mt-0 pt-0 mr-10'></i> Proyek Berjalan";
                                }
                            ?>
                            </p>                   
                        </span>
                    </div>
                </div>
                <div class="row mt-5 pt-5">
                    <div class="col-md-12 mt-5 pt-5">
                        <div class="row">
                           
                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w400 d-none d-lg-block">Progress dan Status Pendanaan</h3>
                                <div class="block d-none d-lg-block">
                                    <div class="row pt-30 mt-10 bs-wizard" style="border-bottom:0;">
                                    <!-- step 1 -->
                                        <div class="bs-wizard-step complete d-none d-lg-block"><!-- complete -->
                                            <div class="progress" style="">
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                        <div class="col-2 bs-wizard-step {{$pengajuan}} d-none d-lg-block">
                                            <div class="text-center bs-wizard-stepnum">1</div>
                                            <p class="text-center text-dark">Pengajuan Pendanaan</p>
                                                <div class="progress" style="margin-left: 40px; border-radius: 10px 0px 0px 10px;">
                                                    <div class="progress-bar"></div>
                                                </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 2 -->
                                        <div class="col-2 bs-wizard-step {{$verifikasi}} d-none d-lg-block"><!-- complete -->
                                            <div class="text-center bs-wizard-stepnum">2</div>
                                            <p class="text-center text-dark">Verifikasi Berhasil</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 3 -->
                                        <div class="col-2 bs-wizard-step {{$penggalangandana}} d-none d-lg-block"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">3</div>
                                            <p class="text-center text-dark">Penggalangan Dana</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 4 -->
                                        <div class="col-2 bs-wizard-step {{$ttd}} d-none d-lg-block"><!-- active -->
                                            <div class="text-center bs-wizard-stepnum">4</div>
                                            <p class="text-center text-dark">Pencairan Dana</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                        </div>
                                        <!-- step 5 -->
                                        <div class="col-2 bs-wizard-step {{$proyekberjalan}} d-none d-lg-block"><!-- disable -->
                                            <div class="text-center bs-wizard-stepnum">5</div>
                                            <p class="text-center text-dark">Proyek Berjalan</p>
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <!-- step 6 -->
                                        <div class="col-2 bs-wizard-step {{$proyekselesai}} d-none d-lg-block"><!-- disable -->
                                            <div class="text-center bs-wizard-stepnum">6</div>
                                            <p class="text-center text-dark">Proyek Selesai</p>
                                            <div class="progress" style="margin-right: 40px; border-radius: 0px 10px 10px 0px;">
                                                <div class="progress-bar"></div>
                                            </div>
                                            <a href="#" class="bs-wizard-dot"></a>
                                            
                                        </div>
                                        <div class="bs-wizard-step disabled d-none d-lg-block"><!-- complete -->
                                            <div class="progress">
                                                <div class="progress-bar"></div>
                                            </div>
                                        </div>
                                        
                                    </div>  
                                                                      
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                               
                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                    <ul class="nav nav-tabs nav-tabs-alt nav-fill" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#wizard-progress2-step1" data-toggle="tab">Informasi Pendanaan</a>
                                        </li>
                                        <!-- <li class="nav-item hide">
                                            <a class="nav-link"  href="#wizard-progress2-step2" data-toggle="tab">Unggah Progress</a>
                                        </li> -->
                                        <!-- <li class="nav-item">
                                            <a class="nav-link" href="#wizard-progress2-step3" data-toggle="tab">Pembayaran</a>
                                            <a class="nav-link" href="#testpembayaran" data-toggle="tab">Pembayaran</a>
                                        </li> -->
                                        <li class="nav-item">
                                            <a class="nav-link" href="#wizard-progress2-step4" data-toggle="tab">Riwayat Mutasi</a>
                                        </li>
                                    </ul>
                                    <!-- END Step Tabs -->
                                    <!-- Form -->
                                    <div>
                                        <!-- Steps Content -->
                                        <div class="block-content block-content-full tab-content pl-30 pr-30" style="min-height: 274px;">
                                            <!-- Step 1 -->
                                            <div class="tab-pane active pb-30" id="wizard-progress2-step1" role="tabpanel">
                                             
                                                <div class="layout">
                                                    <h3 class="block-title text-muted mb-10 font-w600">Informasi Objek Pendanaan</h3>
                                                    <div class="row ml-30 mt-30">
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Tipe Pendanaan</h6>
                                                            <p class="font-w600 text-dark">{{$pendanaanTipe}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Dana Dibutuhkan</h6>
                                                            <p class="font-w600 text-dark">{{$danadibutuhkan}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Imbal Hasil</h6>
                                                            <p class="font-w600 text-dark">{{$imbalhasil}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Durasi Proyek</h6>
                                                            <p class="font-w600 text-dark">{{$durasiproyek}}</p>
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Tanggal Mulai Proyek</h6>
                                                            {{-- <p class="font-w600 text-dark">{{$tgl_mulai_proyek}}</p> --}}
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Tanggal Selesai Proyek</h6>
                                                            {{-- <p class="font-w600 text-dark">{{$tgl_selesai_proyek}}</p> --}}
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Lokasi Proyek</h6>
                                                            {{-- <p class="font-w600 text-dark">{{$lokasi_proyek}}</p> --}}
                                                        </div>
                                                        <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Jenis Akad</h6>
                                                            {{-- <p class="font-w600 text-dark">{{$pendanaanakad}}</p> --}}
                                                        </div>
                                                        <!-- <div class="col-12 col-md-3 pl-10">
                                                            <h6 class="mb-0 text-muted font-w300">Grade Pendanaan</h6>
                                                            <p class="font-size-sm font-w600 text-muted mb-0">
                                                                <i class="fa fa-star mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star-half-full mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                                <i class="fa fa-star-o mt-5 text-primary" style="font-size: 1.5em;"></i>
                                                            </p>
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <div class="layout mt-30">
                                                    <h3 class="block-title text-muted mb-10 font-w600">Detail Deskripsi</h3>
                                                    <div class="row ml-30 mt-30">
                                                        <div class="col-12 col-md-12 pl-10">
                                                            <p class="font-size-md font-w300 text-muted mb-0">
                                                                {!! !empty($deskripsi)?$deskripsi:''!!}
                                                            </p>
                                                            <p class="font-size-sm mt-30 font-w600 text-dark">
                                                               Powered by : Dana Syariah Indonesia | #HijrahFinansial
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END Step 1 -->
                                            <div class="tab-pane" id="wizard-progress2-step2" role="tabpanel">
                                                <h4 class="pl-10 text-dark">Unggah Progress</h4>
                                                <!-- satuBaris -->
                                                <div class="form-group row">
                                                    <div class="p-10 pl-30">
                                                        <image class="img-side" src="{{url('')}}/assetsBorrower/media/photos/photo10.jpg">
                                                    </div>
                                                    <label class="col-12">Pilih Gambar Utama</label>
                                                    <div class="col-8">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input js-custom-file-input-enabled" id="example-file-input-custom" name="example-file-input-custom" data-toggle="custom-file-input">
                                                            <label class="custom-file-label" for="example-file-input-custom">Pilih Gambar</label>
                                                        </div>
                                                    </div>
                                                </div> 

                                                <!-- Image Default -->
                                                <h6 class="content-heading text-muted font-w600 mt-0 pt-0" style="font-size: 1em;">Media Gallery</h6>
                                                <div class="row items-push">
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 animated fadeIn">
                                                        <div class="options-container">
                                                            <img class="img-fluid options-item" src="{{url('')}}/assetsBorrower/media/photos/photo1.jpg" alt="">
                                                            <div class="options-overlay bg-primary-dark-op">
                                                                <div class="options-overlay-content">
                                                                    <a class="btn btn-sm btn-rounded btn-alt-danger min-width-75" href="javascript:void(0)">
                                                                        <i class="fa fa-times"></i> Hapus
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END Image Default -->
                                                
                                                <div class="form-group row mt-20 mb-10">
                                                
                                                    <div class="col-12">
                                                        <h6 class="content-heading text-muted font-w600 mt-0 pt-0" style="font-size: 1em;">Tambah Gallery</h6>
                                                        <!-- Dropzone.js -->
                                                        <form ></form> 
                                                        <form action="/upload-target" class="dropzone" id="drop" >
                                                            <div class="dz-message needsclick">
                                                                <i class="fa fa-picture-o fa-5x text-primary" aria-hidden="true"></i>
                                                                <p class="text-dark">
                                                                    Tarik Gambar ke sini! <br>
                                                                    <small>file type : .jpeg, .jpg, .png</small>
                                                                </p>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div> 
                                            </div>

                                            <div class="tab-pane" id="wizard-progress2-step3" role="tabpanel">
                                                <h4 class="pl-20 text-dark">Pembayaran</h4>
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                                                
                                                <div class="block">
                                                    <div class="block-content">                                                        
                                                        <table class="table table-hover" id="list_tbl_invoice">
                                                            <thead>
                                                                <tr>
                                                                    <th>brw_id</th>
                                                                    <th>proyek_id</th>
                                                                    <th>STATUS</th>
                                                                    <th>JATUH TEMPO</th>
                                                                    <th>DANA POKOK</th>
                                                                    <th>IMBAL HASIL</th>
                                                                    <th>BAYAR</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                                
                                                <!-- END Table Sections -->
                                            </div>

                                          

                                            <div class="tab-pane" id="wizard-progress2-step4" role="tabpanel">
                                                <h4 class="pl-20 text-dark">Riwayat Mutasi</h4>
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                                                
                                                <div class="block">
                                                    <div class="block-content">                                                        
                                                        <table class="js-table-sections table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="text-center">NO</th>
                                                                    <th class="text-center">KETERANGAN</th>
                                                                    <th style="width: 15%;" class="text-center">TANGGAL</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">DEBIT</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">KREDIT</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">SALDO</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="js-table-sections-header">
                                                              
                                                            </tbody>                                                            
                                                            
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- END Table Sections -->
                                            </div>
                                        </div>   
                                        
                                    </div>
                                    <!-- END Form -->
                                </div>
                                <!-- END Progress Wizard 2 -->                                

                            </div>
                            <!-- DETAIL JAMINAN -->
                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w400">Data Jaminan</h3>
                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                       
                                                <div class="block">
                                                    <div class="block-content">
                                                        <table class="js-table-sections table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 5%;" class="text-center">No</th>
                                                                    <th style="width: 15%;" class="text-center">Nama Pemilik Jaminan</th>
                                                                    <th style="width: 15%;" class="text-center">Jenis Jaminan</th>
                                                                    <th style="width: 10%;" class="text-center">Nomor Surat</th>
                                                                    <th style="width: 15%;" class="text-center">Kantor Penerbit</th>
                                                                    <th style="width: 15%;" class="text-center">Nomor Objek Pajak</th>
                                                                    <th style="width: 15%;" class="text-center">Nilai Objek Pajak</th>
                                                                    <th style="width: 10%;" class="text-center">Sertifikat</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="js-table-sections-header show table-success">
                                                          
                                                            </tbody> 

                                                        </table>
                                                    </div>
                                                </div>

                                </div>
                            </div> 
                            <!-- END DETAIL JAMINAN -->
                            <!-- listpendana -->
                            <div class="col-12 col-md-12">
                                <h3 class="block-title text-muted mb-10 font-w400">List Pendana</h3>
                                <!-- Progress Wizard 2 -->
                                <div class="js-wizard-simple block">
                                    <!-- Step Tabs -->
                                                <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->                       
                                                <div class="block">
                                                    <div class="block-content">
                                                        <table class="js-table-sections table table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width: 10%;" class="text-center">No</th>
                                                                    <th hidden="hidden">ID Proyek</th>
                                                                    <th hidden="hidden">ID Borrower</th>
                                                                    <th hidden="hidden">ID Investor</th>
                                                                    <th style="width: 50%;" class="text-center">Nama Pendana</th>
                                                                    <th class="d-none d-sm-table-cell text-center" style="width: 20%;">Nominal Pendanan</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="js-table-sections-header show table-success">
                                                   
                                                            </tbody> 

                                                        </table>
                                                    </div>
                                                </div>

                                </div>
                            </div> 
                            <!-- endlistpendana -->
                            <br><br>
                        </div>
                    </div>                           
                </div>
            </div>
        </div>
        <!-- END Page Content -->
 
	
    </main>
   
    <!-- END Main Container -->
 
    <link rel="stylesheet" href="{{asset('css/sweetalert.css')}}" />
    <script src="{{asset('js/sweetalert.js')}}"></script>
    <style>
        .modal-dialog{
        min-width: 80%;
        }

        .btn-cancel {
            background-color: #C0392B;
            color: #FFFF;
        }
    </style>
  
  
@endsection