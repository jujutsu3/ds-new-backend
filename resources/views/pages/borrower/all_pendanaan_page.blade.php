@extends('layouts.borrower.master')

@section('title', 'Lihat Pendanaan')

@section('content')
<!-- Side Overlay-->
<!--@include('includes.borrower.right_menu_all_pendanaan')-->
<!-- END Side Overlay -->
<style>
    .css-radio .css-control-input~.css-control-indicator {
        width: 15px;
        height: 15px;
        background-color: #ddd;
        border: 0px solid #ddd;
        border-radius: 50%;
    }

    .css-radio.css-control-elegance .css-control-input:checked~.css-control-indicator::after {
        background-color: #d262e3;
    }


    .css-radio.css-control-pendanaanberjalan .css-control-input:checked~.css-control-indicator::after {
        background-color: #55efc4;
    }

    .css-radio.css-control-penggalangan .css-control-input:checked~.css-control-indicator::after {
        background-color: #3498db;
    }

    .css-radio.css-control-pencairan .css-control-input:checked~.css-control-indicator::after {
        background-color: #9b59b6;
    }

    .css-radio.css-control-verifikasiawal .css-control-input:checked~.css-control-indicator::after {
        background-color: yellow;
    }


    .css-radio.css-control-verifikasilanjut .css-control-input:checked~.css-control-indicator::after {
        background-color: #f39c12;
    }

    .card-badge {
        right: 15px;
        top: -2.3px;
        border-radius: 0 !important;
    }
</style>
<!-- Main Container -->
<main id="main-container">
    <!-- Page Content -->
    <div id="detect-screen" class="content-full-right">
        <div class="container">
            <div class="row">

                <div id="col" class="col-12 col-md-9 mt-30">
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block" role="alert">
                        {!! $message !!}
                        <button class="close" data-dismiss="alert"></button>
                    </div>
                    @endif
                </div>
                <div id="col" class="col-12 col-md-9 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h1 class="no-paddingTop font-w400 text-dark">Pendanaan Anda</h1>
                    </span>
                </div>
                <div id="col" class="col-12">
                    <span class="mb-10">
                        <div class="form-material floating">
                            <input type="text" class="form-control col-12 col-md-10"
                                style="height: calc(1.5em + .957143rem + 3px);" id="cariPendanaan"
                                name="material-text2">
                            <label for="material-text2" style="color: #8B8B8B!important;" class="font-w300"> <i
                                    class="fa fa-search"></i> Cari ID atau tipe atau tujuan pendanaan </label>
                        </div>

                    </span>

                    <div class="col-12 mt-20" style="padding-left: 0px;">
                        <label class="css-control css-control-secondary css-radio mr-5 text-dark">
                            <input type="checkbox" checked class="css-control-input" id="chk_pengajuan"
                                name="pengajuan">
                            <span class="css-control-indicator"></span> Pengajuan
                        </label>
                        <label class="css-control css-control-verifikasiawal css-radio mr-5 text-dark">
                            <input type="checkbox" checked class="css-control-input" id="chk_verifikasi_awal"
                                name="verifikasi_awal">
                            <span class="css-control-indicator"></span> Verifikasi Awal
                        </label>
                        <label class="css-control css-control-verifikasilanjut css-radio mr-5 text-dark">
                            <input type="checkbox" checked class="css-control-input" id="chk_verifikasi_lanjut"
                                name="verifikasi_lanjut">
                            <span class="css-control-indicator"></span> Verifikasi Lanjutan
                        </label>
                        <label class="css-control css-control-penggalangan css-radio mr-5 text-dark">
                            <input type="checkbox" checked class="css-control-input" id="chk_penggalangan"
                                name="penggalangan">
                            <span class="css-control-indicator"></span> Penggalangan Dana
                        </label>
                        <label class="css-control css-control-pencairan css-radio mr-5 text-dark">
                            <input type="checkbox" checked class="css-control-input" id="chk_pencairan"
                                name="pencairan">
                            <span class="css-control-indicator"></span> Pencairan Dana
                        </label>
                        <label class="css-control css-control-pendanaanberjalan css-radio mr-5 text-dark">
                            <input type="checkbox" checked class="css-control-input" id="chk_pendanaan_berjalan"
                                name="pendanaan_berjalan">
                            <span class="css-control-indicator"></span> Pendanaan Berjalan
                        </label>
                        <label class="css-control css-control-success css-radio mr-5 text-dark">
                            <input type="checkbox" checked class="css-control-input" id="chk_pendanaan_selesai"
                                name="pendanaan_selesai">
                            <span class="css-control-indicator"></span> Pendanaan Selesai
                        </label>
                    </div>
                </div>
            </div>


            <div class="row mt-10 pt-5">
                <div id="col" class="col-md-12 mt-5 pt-5">
                    <div class="row" id="pendanaanData">

                        @foreach ($list_pengajuan_baru as $rowPengajuan)
                        <div class="DivPengajuanBaru col-md-4 d-flex flex-row mb-20">
                            <div class="position-absolute card-badge"><span
                                    class="badge badge-success text-white">PENGAJUAN BARU</span></div>
                            <div class="p-2 bg-success"><img src="{{ asset('img/logoOnly.png') }}"
                                    style="max-width: 60px;"></div>
                            <div class="block-content block-content-full block-content-sm bg-light">
                                <span class="font-w500 text-black"> {{ $rowPengajuan->pengajuan_id }} -
                                    {{ $rowPengajuan->pendanaan_nama }} -
                                    {{ $rowPengajuan->tujuan_pembiayaan }}</span><br><br>
                                <a href="{{ route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) }}">Lihat Detail</a>
                            </div>

                            <div class="w-25 p-2 bg-secondary">&nbsp;</div>
                        </div>
                        @endforeach


                        @foreach ($list_verifikasi_awal as $rowPengajuan)
                        <div class="DivVerifikasiAwal col-md-4 d-flex flex-row mb-20">
                            @if ($rowPengajuan->status == '1')
                            <div class="position-absolute  card-badge"><span
                                    class="badge badge-success text-white">LOLOS TAHAP VERIFIKASI AWAL</span>
                            </div>
                            @elseif ($rowPengajuan->status == '2')
                            <div class="position-absolute card-badge"><span
                                    class="badge badge-success text-white">GAGAL TAHAP I</span>
                            </div>
                            @elseif ($rowPengajuan->status == '3')
                            <div class="position-absolute card-badge"><span
                                    class="badge badge-success text-white">PROSES VERIFIKASI</span>
                            </div>
                            @endif
                            <div class="p-2 bg-success"><img src="{{ asset('img/logoOnly.png') }}"
                                    style="max-width: 60px;"></div>
                            <div class="block-content block-content-full block-content-sm bg-light">
                                <span class="font-w500 text-black"> {{ $rowPengajuan->pengajuan_id }} -
                                    {{ $rowPengajuan->pendanaan_nama }} -
                                    {{ $rowPengajuan->tujuan_pembiayaan }}</span><br><br>
                                <a href="{{ route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) }}">Lihat Detail</a>
                            </div>
                            <div class="w-25 p-2" style="background-color: yellow;">&nbsp;</div>
                        </div>
                        @endforeach


                        @foreach ($list_verifikasi_lanjut as $rowPengajuan)
                        <div class="DivVerifikasiLanjut col-md-4 d-flex flex-row mb-20">
                            @if ($rowPengajuan->status == '5')
                            <div class="position-absolute card-badge"><span
                                    class="badge badge-success text-white">DITERIMA</span>
                            </div>

                            @elseif ($rowPengajuan->status == '12')
                            <div class="position-absolute card-badge"><span
                                    class="badge badge-danger text-white">GAGAL TAHAP II</span>
                            </div>

                            @elseif ($rowPengajuan->status == '6')
                            <div class="position-absolute card-badge"><span
                                    class="badge badge-danger text-white">DITOLAK</span>
                            </div>
                            @elseif ($rowPengajuan->status == '11' || $rowPengajuan->status == '3')
                            <div class="position-absolute card-badge"><span
                                    class="badge badge-success text-white">PROSES VERIFIKASI</span>
                            </div>
                            @endif
                            <div class="p-2 bg-success"><img src="{{ asset('img/logoOnly.png') }}"
                                    style="max-width: 60px;"></div>
                            <div class="block-content block-content-full block-content-sm bg-light">
                                <span class="font-w500 text-black"> {{ $rowPengajuan->pengajuan_id }} -
                                    {{ $rowPengajuan->pendanaan_nama }} -
                                    {{ $rowPengajuan->tujuan_pembiayaan }}</span><br><br>
                                    <a href="{{ route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) }}">Lihat Detail</a>
                            </div>

                            <div class="w-25 p-2" style="background-color: #f39c12;">&nbsp;</div>
                        </div>

                        @endforeach

                        @foreach ($list_penggalangan_dana as $rowPengajuan)
                        <div class="DivPenggalanganDana col-md-4 d-flex flex-row mb-20">
                            <div class="p-2 bg-success"><img src="{{ asset('img/logoOnly.png') }}"
                                    style="max-width: 60px;"></div>
                            <div class="block-content block-content-full block-content-sm bg-light">
                                <span class="font-w500 text-black"> {{ $rowPengajuan->pengajuan_id }} -
                                    {{ $rowPengajuan->pendanaan_nama }} -
                                    {{ $rowPengajuan->tujuan_pembiayaan }}</span><br><br>
                                <a href="{{ route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) }}">Lihat Detail</a>
                            </div>
                            <div class="w-25 p-2" style="background-color: #3498db;">&nbsp;</div>
                        </div>

                        @endforeach


                        @foreach ($list_pencairan_dana as $rowPengajuan)
                        <div class="DivPencairanDana col-md-4 d-flex flex-row mb-20">
                            <div class="p-2 bg-success"><img src="{{ asset('img/logoOnly.png') }}"
                                    style="max-width: 60px;"></div>
                            <div class="block-content block-content-full block-content-sm bg-light">
                                <span class="font-w500 text-black"> {{ $rowPengajuan->pengajuan_id }} -
                                    {{ $rowPengajuan->pendanaan_nama }} -
                                    {{ $rowPengajuan->tujuan_pembiayaan }}</span><br><br>
                                <a href="{{ route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) }}">Lihat Detail</a>
                            </div>
                            <div class="w-25 p-2" style="background-color: #9b59b6;">&nbsp;</div>
                        </div>

                        @endforeach

                        @foreach ($list_pendanaan_berjalan as $rowPengajuan)
                        <div class="DivPendanaanBerjalan col-md-4 d-flex flex-row mb-20">
                            <div class="p-2 bg-success"><img src="{{ asset('img/logoOnly.png') }}"
                                    style="max-width: 60px;"></div>
                            <div class="block-content block-content-full block-content-sm bg-light">
                                <span class="font-w500 text-black"> {{ $rowPengajuan->pengajuan_id }} -
                                    {{ $rowPengajuan->pendanaan_nama }} -
                                    {{ $rowPengajuan->tujuan_pembiayaan }}</span><br><br>
                                <a href="{{ route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) }}">Lihat Detail</a>
                            </div>
                            <div class="w-25 p-2" style="background-color: #55efc4;">&nbsp;</div>
                        </div>

                        @endforeach

                        @foreach ($list_pendanaan_selesai as $rowPengajuan)
                        <div class="DivPendanaanSelesai col-md-4 d-flex flex-row mb-20">
                            <div class="p-2 bg-success"><img src="{{ asset('img/logoOnly.png') }}"
                                    style="max-width: 60px;"></div>
                            <div class="block-content block-content-full block-content-sm bg-light">
                                <span class="font-w500 text-black"> {{ $rowPengajuan->pengajuan_id }} -
                                    {{ $rowPengajuan->pendanaan_nama }} -
                                    {{ $rowPengajuan->tujuan_pembiayaan }}</span><br><br>
                                <a href="{{ route('detail_pendanaan_rumah', Crypt::encrypt($rowPengajuan->pengajuan_id)) }}">Lihat Detail</a>
                            </div>
                            <div class="w-25 p-2 bg-success">&nbsp;</div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
</main>
<!-- END Main Container -->
@endsection
@section('script')
<script>
    $("document").ready(function() {
            setTimeout(function() {
                $("div.alert").remove();
            }, 3000); // 3 secs

        });
        $(document).on("keyup", "#cariPendanaan", function() {
            var txtSearch = $(this).val();

            $.ajax({
                url: "{{ route('borrower.pendanaan.search') }}",
                type: "post",
                dataType: "html",
                data: {
                    _token: "{{ csrf_token() }}",
                    txtSearch: txtSearch
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    if (xhr.status === 419) {
                        alert('Page expired. Please re-login again.');
                        return;
                    }

                },
                success: function(result) {
                    if (result) {
                        $('#pendanaanData').html(result);
                    } else {
                        $('#pendanaanData').html('Data tidak ditemukan');
                    }

                }
            });

        });
        $(document).on("click", "#chk_pengajuan", function() {

            if ($('input[name="pengajuan"]').is(':checked')) {
                $("#chk_pengajuan").prop("checked", true);
                $(".DivPengajuanBaru").addClass("d-flex").removeClass("d-none");
            } else {
                $("#chk_pengajuan").prop("checked", false);

                $(".DivPengajuanBaru").removeClass("d-flex").addClass("d-none");
            }
        });
        $(document).on("click", "#chk_verifikasi_awal", function() {

            if ($('input[name="verifikasi_awal"]').is(':checked')) {
                $("#chk_verifikasi_awal").prop("checked", true);
                $(".DivVerifikasiAwal").addClass("d-flex").removeClass("d-none");
            } else {
                $("#chk_verifikasi_awal").prop("checked", false);
                $(".DivVerifikasiAwal").removeClass("d-flex").addClass("d-none");
            }
        });


        $(document).on("click", "#chk_verifikasi_lanjut", function() {

            if ($('input[name="verifikasi_lanjut"]').is(':checked')) {
                $("#chk_verifikasi_lanjut").prop("checked", true);
                $(".DivVerifikasiLanjut").addClass("d-flex").removeClass("d-none");
            } else {
                $("#chk_verifikasi_lanjut").prop("checked", false);
                $(".DivVerifikasiLanjut").removeClass("d-flex").addClass("d-none");
            }
        });

        $(document).on("click", "#chk_penggalangan", function() {

            if ($('input[name="penggalangan"]').is(':checked')) {
                $("#chk_penggalangan").prop("checked", true);
                $(".DivPenggalanganDana").addClass("d-flex").removeClass("d-none");
            } else {
                $("#chk_penggalangan").prop("checked", false);
                $(".DivPenggalanganDana").removeClass("d-flex").addClass("d-none");
            }
        });

        $(document).on("click", "#chk_pencairan", function() {

            if ($('input[name="pencairan"]').is(':checked')) {
                $("#chk_pencairan").prop("checked", true);
                $(".DivPencairanDana").addClass("d-flex").removeClass("d-none");
            } else {
                $("#chk_pencairan").prop("checked", false);
                $(".DivPencairanDana").removeClass("d-flex").addClass("d-none");
            }
        });

        $(document).on("click", "#chk_pendanaan_berjalan", function() {

            if ($('input[name="pendanaan_berjalan"]').is(':checked')) {
                $("#chk_pendanaan_berjalan").prop("checked", true);
                $(".DivPendanaanBerjalan").addClass("d-flex").removeClass("d-none");
            } else {
                $("#chk_pendanaan_berjalan").prop("checked", false);
                $(".DivPendanaanBerjalan").removeClass("d-flex").addClass("d-none");
            }
        });

        $(document).on("click", "#chk_pendanaan_selesai", function() {

            if ($('input[name="pendanaan_selesai"]').is(':checked')) {
                $("#chk_pendanaan_selesai").prop("checked", true);
                $(".DivPendanaanSelesai").addClass("d-flex").removeClass("d-none");
            } else {
                $("#chk_pendanaan_selesai").prop("checked", false);
                $(".DivPendanaanSelesai").removeClass("d-flex").addClass("d-none");
            }
        });
</script>
@endsection
