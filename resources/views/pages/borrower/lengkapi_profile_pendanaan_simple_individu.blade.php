<div class="col-12 mt-5 pt-5">
    <form id="form-individu" name="form-individu" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="js-wizard-simple block border layout pt-4" id="layout-informasi-pribadi" disabled>
            <div class="block-content block-content-full tab-content pb-4 mb-4">
                <div id="smartwizard">
                    {{-- START: Tab Title --}}
                    <ul class="nav" style="border: 0;">
                        <li id="li-step-informasi-pribadi">
                            <a class="nav-link active" id="step-informasi-pribadi-title" href="#step-informasi-pribadi" style="border: 0;">
                                Informasi Pribadi
                            </a>
                        </li>
                        <li id="li-step-informasi-pengajuan">
                            <a class="nav-link" id="step-informasi-pengajuan-title" href="#step-informasi-pengajuan" style="border: 0;">
                                Informasi Pengajuan
                            </a>
                        </li>
                    </ul>
                    {{-- END: Tab Title --}}

                    <hr class="line mb-4 pb-4">

                    {{-- START: Content --}}
                    <div class="tab-content">
                        <div id="step-informasi-pribadi" class="tab-pane" role="tabpanel">
                            {{-- Brw TYpe --}}
                            <input type="hidden" id="borrower_type" name="borrower_type">
                            <input name="status" type="hidden" value="1">

                            {{-- START: Baris 1 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama_individu" class="ml-0" for="">Nama Lengkap <i class="text-danger">*</i></label>
                                        <input class="form-control checkKarakterAneh" type="text" maxlength="35" id="nama_individu" name="nama_individu" placeholder="Masukkan nama anda" value="{{ $data_individu ? $data_individu->nama : ''}}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_ktp" for="ktp" class="ml-0">NIK <i class="text-danger">*</i></label>
                                        <input class="form-control no-zero no-four" type="text" id="ktp" name="ktp" minlength="16" maxlength="16" placeholder="NIK" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')" pattern=".{16,16}" maxlength="16" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" value="{{ $data_individu ? $data_individu->ktp : ''}}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_npwp" for="npwp">NPWP <i class="text-danger">*</i> </label>
                                        <input class="form-control " type="text" id="npwp" name="npwp" minlength="15" maxlength="15" pattern=".{15,15}" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" placeholder="NPWP" value="{{ $data_individu ? $data_individu->npwp : ''}}" required>
                                    </div>
                                </div>
                            </div>
                            {{-- START: Baris 1 --}}

                            {{-- START: Baris 3 --}}
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">Agama <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="agama" name="agama" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">Status Pernikahan <i class="text-danger">*</i></label>
                                        <div class="input-group">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kawin" id="status_kawin_0" value="1" required>
                                                <label class="form-check-label text-muted" for="status_kawin_0">Sudah
                                                    Menikah</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kawin" id="status_kawin_1" value="2">
                                                <label class="form-check-label text-muted" for="status_kawin_1">Belum
                                                    Menikah</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kawin" id="status_kawin_2" value="3">
                                                <label class="form-check-label text-muted" for="status_kawin_2">Duda/Janda</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0" for="pendidikan_terakhir">Pendidikan
                                            Terakhir <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="pendidikan_terakhir" name="pendidikan_terakhir" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 3 --}}

                            {{-- START: Baris 4 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="text_tempat_lahir" class="ml-0">Tempat Lahir <i class="text-danger">*</i></label>
                                        <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" required placeholder="Masukkan Tempat Lahir" value="{{ $data_individu ? $data_individu->tempat_lahir : ''}}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_telepon" class="ml-0">No. Telepon Selular <i class="text-danger">*</i></label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-dsi">
                                                    +62 </span>
                                            </div>
                                            <input class="form-control no-zero eight" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" minlength="9" pattern=".{9,13}" maxlength="13" id="telepon" name="telepon" placeholder="Masukkan No. Handphone" value="{{ $data_individu ? substr($data_individu->no_tlp, 2) : ''}}" required>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            {{-- END: Baris 4 --}}

                            {{-- START: Baris 5 --}}
                            <div class="row">
                                <div class="col-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6 ml-0">Alamat Sesuai KTP
                                            &nbsp</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="alamat">Alamat Sesuai KTP <i class="text-danger">*</i></label>
                                        <textarea class="form-control form-control-lg" maxlength="90" onchange="$(this).valid();validalamat();" id="alamat" name="alamat" rows="6" placeholder="Masukkan alamat lengkap Anda" required>{{ $data_individu ? $data_individu->alamat : ''}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="provinsi">Provinsi <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="provinsi" name="provinsi" onChange="provinsiChange(this.value, this.id, 'kota')" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kota">Kota/Kabupaten <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="kota" name="kota" onChange="kotaChange(this.value, this.id, 'kecamatan')" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 5 --}}

                            {{-- START: Baris 6 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kecamatan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="kecamatan" name="kecamatan" onChange="kecamatanChange(this.value, this.id, 'kelurahan')" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kelurahan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="kelurahan" name="kelurahan" onChange="kelurahanChange(this.value, this.id, 'kode_pos',$('#kecamatan').val(),$('#kota').val(),$('#provinsi').val())" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kode_pos" class="ml-0">Kode Pos <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" maxlength="30" id="kode_pos" name="kode_pos" placeholder="--" value="{{ $data_individu ? $data_individu->kode_pos : ''}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">Status Kepemilikan Rumah <i class="text-danger">*</i></label>
                                        <div class="input-group">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kepemilikan_rumah" id="status_kepemilikan_rumah_0" value="1" required>
                                                <label class="form-check-label text-muted" for="status_kepemilikan_rumah_0">Milik Pribadi</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kepemilikan_rumah" id="status_kepemilikan_rumah_1" value="2">
                                                <label class="form-check-label text-muted" for="status_kepemilikan_rumah_1">Sewa</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kepemilikan_rumah" id="status_kepemilikan_rumah_2" value="3">
                                                <label class="form-check-label text-muted" for="status_kepemilikan_rumah_2">Milik Keluarga</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 6 --}}


                            {{-- START: Baris 13 --}}
                            <div class="row">
                                <div class="col-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6 ml-0">Informasi Pekerjaan
                                            &nbsp</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pekerjaan" class="ml-0">Pekerjaan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="pekerjaan" name="pekerjaan" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bidang_pekerjaan" class="ml-0">Bidang
                                            Pekerjaan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="bidang_pekerjaan" name="bidang_pekerjaan" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bidang_online" class="ml-0">Bidang
                                            Pekerjaan Online <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="bidang_online" name="bidang_online" required>
                                            <option value="">-- Pilih Satu --
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- START: Baris 13 --}}

                            {{-- START: Detail Penghasilan --}}
                            <div id="detail-penghasilan" class="d-none">
                                <div class="row">
                                    {{--START: Baris 14 --}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_nama_perusahaan" for="nama_perusahaan" class="ml-0 fixedin">Nama
                                                Perusahaan<i class="text-danger">*</i></label>
                                            <label id="label_nama_usaha" for="nama_perusahaan" class="ml-0 nonfixed">Nama
                                                Usaha/Praktek <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="nama_perusahaan" maxlength="50" name="nama_perusahaan" placeholder="Masukkan nama perusahaan" value="{{ $data_penghasilan ? $data_penghasilan->nama_perusahaan : ''}}" required>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="badan_usaha" class="ml-0 ">Bentuk Badan Usaha <i
                                                    class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="badan_usaha"
                                                name="badan_usaha" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div> -->

                                    {{--END: Baris 14 --}}


                                    {{--START: Baris 16 --}}
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label id="lama_bekerja" class="ml-0 fixedin">Lama Bekerja (Tahun)<i class="text-danger">*</i></label>
                                            <label id="lama_usaha" class="ml-0 nonfixed">Lama Usaha/Praktek (Tahun) <i class="text-danger ">*</i></label>
                                            <div class="row fixedin">
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                        </div>
                                                        <input type="number" id="tahun_bekerja" name="tahun_bekerja" class="form-control" aria-label="tahun_bekerja" aria-describedby="basic-addon1" min="0" value="{{ $data_penghasilan ? $data_penghasilan->masa_kerja_tahun : ''}}" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                        </div>
                                                        <input type="number" id="bulan_bekerja" name="bulan_bekerja" class="form-control" aria-label="bulan_bekerja" aria-describedby="basic-addon1" min="0" max="11" value="{{ $data_penghasilan ? $data_penghasilan->masa_kerja_bulan : ''}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row nonfixed">
                                                <div class="col-md-12">
                                                    <div class="input-group mb-6">
                                                        <input type="number" id="usia_perusahaan" name="usia_perusahaan" class="form-control" aria-label="usia_perusahaan" aria-describedby="basic-addon1" min="0" value="{{ $data_penghasilan ? $data_penghasilan->usia_perusahaan : ''}}" required>
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_no_telpon_perusahaan" for="no_telpon_perusahaan" class="ml-0 fixedin">No.
                                                Telepon Perusahaan<i class="text-danger">*</i></label>
                                            <label id="label_no_telpon_usaha" for="no_telpon_usaha" class="ml-0 nonfixed">No. Telepon/HP Usaha/Praktek<i class="text-danger">*</i></label>

                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi"> +62
                                                    </span>
                                                </div>
                                                <input class="form-control no-zero" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'');" pattern=".{9,13}" maxlength="13" id="no_telpon_usaha" name="no_telpon_usaha" placeholder="Masukkan nomor telpon perusahaan" value="{{ $data_penghasilan ? substr($data_penghasilan->no_telp, 2) : ''}}" required>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4 non-fixed d-none">
                                        <div class="form-group">
                                            <label for="no_hp_usaha" class="ml-0">No. HP Usaha/Praktek <i class="text-danger">*</i></label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi"> +62
                                                    </span>
                                                </div>
                                                <input class="form-control no-zero eight" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" pattern=".{9,13}" maxlength="13" id="no_hp_usaha" name="no_hp_usaha" placeholder="Masukkan no. hp Usaha" value="{{ $data_penghasilan ? substr($data_penghasilan->no_hp, 2) : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    {{--END: Baris 17 --}}
                                </div>

                                {{-- START: Baris 22 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="penghasilan" class="ml-0">Penghasilan (Per Bulan) <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="penghasilan" name="penghasilan" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" placeholder="Masukkan penghasilan anda" value="{{ $data_penghasilan ? (int)$data_penghasilan->pendapatan_borrower : ''}}" required>
                                        </div>
                                    </div>

                                </div>
                                {{-- END: Baris 22 --}}

                            </div>
                            {{-- END: Detail Penghasilan --}}

                            {{-- START: Baris 23 --}}
                            <div class="row">
                                <div class="col-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6 ml-0">Foto &nbsp</label>
                                    </div>
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto Diri <i class="text-danger">*</i></label>
                                    <div id="preview_camera_diri" name="preview_camera_diri" class="pt-3">
                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" type="button" id="btn_camera_diri" name="btn_camera_diri" onclick="btnCameraClick('camera_diri', 'preview_camera_diri', 'take_camera_diri')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_diri" name="take_camera_diri" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{ asset('assets/img/user-guide.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_diri" name="camera_diri"></div>
                                            <input class="btn btn-primary green-dsi mx-0" type="button" value="Ambil Foto" id="take_snapshot_diri" name="take_snapshot_diri" onclick="takeSnapshot('result_camera_diri', 'user_camera_diri', 'user_camera_diri_val', 'result_diri'); $('#user_camera_diri_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_diri" name="user_camera_diri" value="{{ $data_individu ?  $data_individu->brw_pic : ''}}" required class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_diri" name="result_camera_diri" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_diri" name="result_diri"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_diri_val" required name="user_camera_diri_val" value="{{ $data_individu ?  $data_individu->brw_pic : ''}}">
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto KTP <i class="text-danger">*</i></label>
                                    <div id="preview_camera_ktp" name="preview_camera_ktp" class="pt-3">
                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic_ktp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" type="button" id="btn_camera_ktp" name="btn_camera_ktp" onclick="btnCameraClick('camera_ktp', 'preview_camera_ktp', 'take_camera_ktp')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_ktp" name="take_camera_ktp" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{asset('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_ktp" name="camera_ktp"></div>
                                            <input class="btn btn-primary green-dsi" type="button" value="Ambil Foto" id="take_snapshot_ktp" name="take_snapshot_ktp" onclick="takeSnapshot('result_camera_ktp', 'user_camera_ktp', 'user_camera_ktp_val', 'result_ktp'); $('#user_camera_ktp_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_ktp" name="user_camera_ktp" value="{{ $data_individu ?  $data_individu->brw_pic_ktp : ''}}" required class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_ktp" name="result_camera_ktp" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_ktp" name="result_ktp"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_ktp_val" required name="user_camera_ktp_val" value="{{ $data_individu ?  $data_individu->brw_pic_ktp : ''}}">
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto Diri & KTP <i class="text-danger">*</i></label>
                                    <div id="preview_camera_diri_dan_ktp" name="preview_camera_diri_dan_ktp" class="pt-3">
                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic_user_ktp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" id="btn_camera_diri_dan_ktp" name="btn_camera_diri_dan_ktp" type="button" onclick="btnCameraClick('camera_diri_dan_ktp', 'preview_camera_diri_dan_ktp', 'take_camera_diri_dan_ktp')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_diri_dan_ktp" name="take_camera_diri_dan_ktp" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{asset('assets/img/guide-diridanktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_diri_dan_ktp" name="camera_diri_dan_ktp"></div>
                                            <input class="btn btn-primary green-dsi" type="button" value="Ambil Foto" id="take_snapshot_diri_dan_ktp" name="take_snapshot_diri_dan_ktp" onclick="takeSnapshot('result_camera_diri_dan_ktp', 'user_camera_diri_dan_ktp', 'user_camera_diri_dan_ktp_val', 'result_diri_dan_ktp'); $('#user_camera_diri_dan_ktp_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_diri_dan_ktp" value="{{ $data_individu ?  $data_individu->brw_pic_user_ktp : ''}}" required name="user_camera_diri_dan_ktp" class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_diri_dan_ktp" name="result_camera_diri_dan_ktp" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_diri_dan_ktp" name="result_diri_dan_ktp"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_diri_dan_ktp_val" name="user_camera_diri_dan_ktp_val" required value="{{ $data_individu ?  $data_individu->brw_pic_user_ktp : ''}}">
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto NPWP <i class="text-danger">*</i></label>
                                    <div id="preview_camera_npwp" name="preview_camera_npwp" class="pt-3">
                                        <img class="imagePreview mb-3" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic_npwp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" id="btn_camera_npwp" name="btn_camera_npwp" type="button" onclick="btnCameraClick('camera_npwp', 'preview_camera_npwp', 'take_camera_npwp')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_npwp" name="take_camera_npwp" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{asset('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_npwp" name="camera_npwp"></div>
                                            <input class="btn btn-primary green-dsi" type="button" value="Ambil Foto" id="take_snapshot_npwp" name="take_snapshot_npwp" onclick="takeSnapshot('result_camera_npwp', 'user_camera_npwp', 'user_camera_npwp_val', 'result_npwp'); $('#user_camera_npwp_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_npwp" required name="user_camera_npwp" value="{{ $data_individu ?  $data_individu->brw_pic_npwp : ''}}" class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_npwp" name="result_camera_npwp" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_npwp" name="result_npwp"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_npwp_val" required name="user_camera_npwp_val" value="{{ $data_individu ?  $data_individu->brw_pic_npwp : ''}}">
                                </div>
                            </div>
                            {{-- END: Baris 23 --}}
                        </div>
                        <div id="step-informasi-pengajuan" class="tab-pane" role="tabpanel">
                            <div id="layout-informasi-pengajuan">
                                {{-- START: Baris 1 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapengguna">Tipe
                                                Pendanaan</label><label id="txt_pendanaan_select"></label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="type_pendanaan_select" name="type_pendanaan_select" required></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapengguna">Tujuan
                                                Pendanaan</label><label id="txt_tujuan_pendanaan"></label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="type_tujuan_pendanaan" name="type_tujuan_pendanaan" required></select>
                                        </div>
                                    </div>
                                </div>
                                {{-- START: Baris 2 --}}
                                <div class="row">
                                    <div class="col-md-4 dana-rumah">
                                        <div class="form-group">
                                            <label for="wizard-progress2-jenis_property">Jenis
                                                Rumah</label><label id="txt_jenis_property"></label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="jenis_property" name="jenis_property" required></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="dana-rumah">
                                                <label for="wizard-progress2-namapendanaan">Harga Rumah / Kebutuhan Dana </label> <label class="mandatory_label">*</label>
                                            </div>
                                            <div class="dana-konstruksi d-none">
                                                <label for="wizard-progress2-namapendanaan">Kebutuhan Dana</label> <label class="mandatory_label">*</label>
                                            </div>
                                            <input type="text" class="form-control numberOnly no-zero" id="txt_harga_objek_pendanaan" name="txt_harga_objek_pendanaan" required onkeyup="this.value = formatRupiah(this.value);" placeholder="Masukkan Dana yang di Butuhkan">
                                        </div>
                                    </div>
                                </div>
                                {{-- START: Baris 3 --}}

                                <div class="row">
                                    <div class="col-md-4 dana-rumah">
                                        <div class="form-group">
                                            <label for="txt_uang_muka">Uang
                                                Muka </label> <label class="mandatory_label">*</label>
                                            <input type="text" class="form-control numberOnly no-zero" id="txt_uang_muka" name="txt_uang_muka" onkeyup="this.value = formatRupiah(this.value);" value="0" required>
                                        </div>
                                    </div>

                                    <div class="col-md-2 dana-rumah">
                                        <div class="form-group">
                                            <label for="txt_persen">&nbsp</label> <label class="mandatory_label">&nbsp</label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control numberOnly no-zero" id="txt_persen" name="txt_persen" value="0" required>
                                                <div class="input-group-append">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="txt_nilai_pengajuan">Nilai
                                                Pengajuan Pendanaan </label> <label class="mandatory_label">*</label>
                                            <input type="text" class="form-control numberOnly no-zero" id="txt_nilai_pengajuan" name="txt_nilai_pengajuan" onkeyup="this.value = formatRupiah(this.value);" placeholder="Nilai Pengajuan Pendanaan" disabled required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_txt_jangka_waktu" for="txt_jangka_waktu">Jangka
                                                Waktu (Bulan) </label> <label class="mandatory_label">*</label>
                                            <select name="txt_jangka_waktu" id="txt_jangka_waktu" class="form-control custom-select" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 dana-konstruksi d-none">
                                        <div class="form-group">
                                            <label for="txt_estimasi_proyek">Tanggal
                                                Estimasi Mulai Proyek</label> <label class="mandatory_label">*</label>
                                            <input class="form-control" type="date" onchange="$(this).valid()" id="txt_estimasi_proyek" name="txt_estimasi_proyek" min="<?= date("Y-m-d"); ?>" placeholder="Masukkan Tanggal Estimasi Mulai Proyek..." required>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div id="chk-agreement">
                            <div class="row mt-4 mb-4 px-3">
                                <div class="col-12">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="chk_agreement" id="chk_agreement" value="1">
                                        <label class="form-check-label text-secondary h6" for="chk_agreement">Saya telah
                                            membaca
                                            dan menyetujui <a href="#agreement" data-toggle="modal" data-target="#agreement" class="text-success">Syarat dan Ketentuan</a>
                                            yang
                                            berlaku, dan
                                            dengan ini menyatakan bahwa data yang dikirim adalah benar dan menyetujui
                                            pelaksanaan proses selanjutnya oleh PT. Dana Syariah Indonesia &nbsp</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="agreement" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header" style="border-bottom-width: 1px; border-bottom-color: #e6ecec">
                                        <p class="modal-title h4" id="exampleModalLongTitle">Syarat dan Ketentuan
                                            Pengisian Aplikasi Pengajuan Pinjaman</p>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="h5 font-weight-normal">
                                            Sehubungan dengan pengajuan pinjaman kepada PT. Dana Syariah Indonesia,
                                            dengan ini menyatakan sebagai berikut : <br><br>

                                            1. Memberikan persetujuan dan kuasa kepada PT. Dana Syariah Indonesia untuk
                                            memperoleh referensi dari sumber manapun dan
                                            dengan cara yang dianggap layak oleh PT. Dana Syariah Indonesia.<br><br>
                                            2. Memberikan persetujuan dan kuasa kepada PT. Dana Syariah Indonesia untuk
                                            memberikan data kepada pihak ketiga untuk
                                            kebutuhan risk assessment (penilaian resiko).<br><br>
                                            3. PT. Dana Syariah Indonesia berhak untuk menolak permohonan dengan tanpa
                                            harus/kewajiban untuk menunjukkan
                                            alasan-alasan penolakan.<br><br>
                                            4. Menyampaikan dokumen sesuai dengan yang disyaratkan oleh PT. Dana Syariah
                                            Indonesia.<br><br>
                                            5. Semua dokumen pendukung yang telah diserahkan kepada PT. Dana Syariah
                                            Indonesia tidak akan ditarik kembali.<br><br>
                                            6. Memberikan informasi terbaru apabila ada perubahan data dalam aplikasi
                                            ini sehubungan dengan permohonan kredit
                                            tersebut.<br><br>
                                            7. Apabila permohonan disetujui, akan tunduk dan terikat pada ketentuan dan
                                            syarat-syarat kemudian yang dikeluarkan oleh
                                            PT. Dana Syariah Indonesia.<br><br>
                                            8. Pembiayaan tidak digunakan untuk kegiatan yang bertentangan dengan
                                            prinsip Syariah dan kegiatan usaha yang dilarang
                                            dan bertentangan dengan Undang-undang yang berlaku yang meliputi tetapi
                                            tidak terbatas pada: tindak pidana pencucian
                                            uang (TPPU), Pendanaan kegiatan yang terkait dengan tindakan terorisme
                                            (TPPT), Penipuan, Prostitusi, perdagangan
                                            obat-obat psikotrapis (narkotik dan jenis obat terlarang lainnya). <br><br>

                                            <b>"Sesuai dengan NOMOR 77 /POJK.01/2016 bahwa batas maksimum total
                                                pemberian pinjaman dana kepada setiap peminjam dana
                                                adalah sebesar Rp. 2.000.000.000,00 (dua miliar rupiah)."</b>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-noborder btn-success btn-lg p-2 px-3 my-3" data-dismiss="modal">Setuju</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Content --}}
                </div>
            </div>
        </div>
        <input type="submit" id="submitindividu" class="d-none" value="submit">
    </form>
</div>