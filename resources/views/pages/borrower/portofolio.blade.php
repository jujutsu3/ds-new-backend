@extends('layouts.borrower.master')
@section('title', 'Portofolio')
@section('style')
    <style>
        .bg-gradient-hijau {
            background: rgb(47, 122, 21);
            background: -moz-linear-gradient(51deg, rgba(47, 122, 21, 1) 0%, rgba(6, 119, 87, 1) 100%);
            background: -webkit-linear-gradient(51deg, rgba(47, 122, 21, 1) 0%, rgba(6, 119, 87, 1) 100%);
            background: linear-gradient(51deg, rgba(47, 122, 21, 1) 0%, rgba(6, 119, 87, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#2f7a15", endColorstr="#067757", GradientType=1);
        }

        .bg-gradient-hijau-2 {
            background: rgb(21, 122, 89);
            background: -moz-linear-gradient(51deg, rgba(21, 122, 89, 1) 0%, rgba(6, 95, 119, 1) 100%);
            background: -webkit-linear-gradient(51deg, rgba(21, 122, 89, 1) 0%, rgba(6, 95, 119, 1) 100%);
            background: linear-gradient(51deg, rgba(21, 122, 89, 1) 0%, rgba(6, 95, 119, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#157a59", endColorstr="#065f77", GradientType=1);
        }

        .bg-gradient-blue {
            background: rgb(21, 85, 122);
            background: -moz-linear-gradient(51deg, rgba(21, 85, 122, 1) 0%, rgba(6, 55, 119, 1) 100%);
            background: -webkit-linear-gradient(51deg, rgba(21, 85, 122, 1) 0%, rgba(6, 55, 119, 1) 100%);
            background: linear-gradient(51deg, rgba(21, 85, 122, 1) 0%, rgba(6, 55, 119, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#15557a", endColorstr="#063777", GradientType=1);
        }

        #overlay {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 900;
            width: 100%;
            height: 100%;
            display: none;
            background: rgba(0, 0, 0, 0.6);
        }

        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #2e93e6 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }

        @keyframes sp-anime {
            100% {
                transform: rotate(360deg);
            }
        }

        .is-hide {
            display: none;
        }

        .slick-prev::before {
            color: #fff !important;
        }

        .slick-next::before {
            color: #fff !important;
        }

        .block {
            border-radius: 10px !important;
        }

    </style>
@endsection

@section('content')
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>
    <!-- Main Container -->
    @if (Auth::guard('borrower')->user()->status == 'pending')
        <main id="main-container">

            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;">
                                Silahkan Tunggu Verifikasi dari Danasyariah</h1>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h4 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;">
                                Terimakasih telah bergabung untuk maju bersama kami</h4>
                        </span>


                    </div>
                </div>

                <!-- END Frequently Asked Questions -->
            </div>
            <!-- END Page Content -->

        </main>
    @elseif(Auth::guard('borrower')->user()->status == 'active')
        <main id="main-container">
            <!-- Page Content -->
            <div id="detect-screen" class="content-full-right">
                <div class="container col-12">
                    <div class="row my-5">
                        <div id="col" class="col-12 col-md-12 mt-30">
                            <span class="mb-10 pb-10 ">
                                <h1 class="no-paddingTop font-w400 judul" style="float: left; margin-block-end: 0em;">
                                    Portofolio</h1>

                            </span>
                        </div>
                    </div>
                    <div class="row mt-5 pt-5">
                        <div id="col" class="col-md-12 mt-5 pt-5">

                            <div class="div-total lazy pt-2 slick-slider">
                                <div class="slide-wrap slick-slider">
                                    <div class="block dsiBgGreen">
                                        <div class="block-content text-white pt-30">
                                            <h3 class="text-white text-center font-bold-x2 font-w500"
                                                style="font-size: 14px; !important;">Saldo</h3>
                                            <h5 class="text-white text-center font-w300" id=''>Rp 0</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="slide-wrap slick-slider">
                                    <div class="block bg-gradient-hijau">
                                        <div class="block-content text-white pt-30">
                                            <h3 class="text-white text-center font-bold-x2 font-w500"
                                                style="font-size: 14px; !important;">Total Pinjaman Pokok</h3>
                                            <h5 class="text-white text-center font-w300" id=''>Rp 0</h5>
                                        </div>
                                    </div>
                                </div>


                                <div class="slide-wrap slick-slider">
                                    <div class="block bg-gradient-hijau-2">
                                        <div class="block-content text-white pt-30">
                                            <h3 class="text-white text-center font-bold-x2 font-w500"
                                                style="font-size: 14px; !important;">Total Margin</h3>
                                            <h5 class="text-white text-center font-w300" id=''>Rp 0</h5>
                                        </div>
                                    </div>
                                </div>


                                <div class="slide-wrap slick-slider">
                                    <div class="block bg-gradient-blue">
                                        <div class="block-content text-white pt-30">
                                            <h3 class="text-white text-center font-bold-x2 font-w500"
                                                style="font-size: 14px; !important;">Total Pinjaman</h3>
                                            <h5 class="text-white text-center font-w300" id=''>Rp 0</h5>
                                        </div>
                                    </div>
                                </div>


                                <div class="slide-wrap slick-slider">
                                    <div class="block bg-gradient-blue">
                                        <div class="block-content text-white pt-30">
                                            <h3 class="text-white text-center font-bold-x2 font-w500"
                                                style="font-size: 14px; !important;">Total Tagihan</h3>
                                            <h5 class="text-white text-center font-w300" id=''>Rp 0</h5>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="row mt-5 pt-5">
                        <div class="col-md-12 mt-5 pt-5">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <h3 class="no-paddingTop font-w300 judul">Informasi Penerima Pendanaan</h3>
                                    <div class="col">
                                        <div class="row ml-30 mt-30">
                                            <div class="col-12 col-md-4 pl-10 pt-10">
                                                <h6 class="mb-0 text-muted font-w300">Nama :</h6>
                                                <p class="font-w600 text-dark">Test
                                                </p>
                                            </div>

                                            <div class="col-12 col-md-4 pl-10 pt-10">
                                                <h6 class="mb-0 text-muted font-w300">Tempat/ Tanggal Lahir :</h6>
                                                <p class="font-w600 text-dark">Jakarta, 1 Juni 1980</p>
                                            </div>


                                            <div class="col-12 col-md-4 pl-10 pt-10">
                                                <h6 class="mb-0 text-muted font-w300">Nomor KTP :</h6>
                                                <p class="font-w600 text-dark">0812938129387</p>
                                            </div>



                                        </div>


                                        <div class="row ml-30 mt-10">
                                            <div class="col-12 col-md-4 pl-10 pt-10">
                                                <h6 class="mb-0 text-muted font-w300">Jenis Kelamin :</h6>
                                                <p class="font-w600 text-dark">Laki - Laki</p>
                                            </div>

                                            <div class="col-12 col-md-4 pl-10 pt-10">
                                                <h6 class="mb-0 text-muted font-w300">Alamat :</h6>
                                                <p class="font-w600 text-dark">Pinang Griya Tangerang</p>
                                            </div>


                                        </div>


                                        <div class="row ml-30 mt-10">

                                            <div class="col-12 col-md-4 pl-10 pt-10">
                                                <h6 class="mb-0 text-muted font-w300">Nomor Rekening/VA : </h6>
                                                <p class="font-w600 text-dark">
                                                    1201930198</p>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="row mt-20 pt-20">
                        <div id="col2" class="col-md-12 mt-5 pt-5">
                            <span class="mb-10 pb-10 ">
                                <h3 class="no-paddingTop font-w300 judul" style="float: left; ">Daftar Pendanaan</h3>
                            </span>
                            <div class="table-responsive">
                                <table class="table border" id="table_pendanaan">
                                    <thead class="bg-dark text-light">
                                        <th class="text-center">Id Pendanaan</th>
                                        <th class="text-center">Pendanaan</th>
                                        <th class="text-center">Tipe Pendanaan</th>
                                        <th class="text-center">Nomor Akad</th>
                                        <th class="text-center">Tenor (Bulan)</th>
                                        <th class="text-center">Pokok Pinjaman</th>
                                        <th class="text-center">Sisa Pokok</th>
                                        <th class="text-center">Tanggal Mulai</th>
                                        <th class="text-center">Tanggal Selesai</th>
                                        <th class="text-center">Lihat Detil</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>77</td>
                                            <td>Dana Rumah</td>
                                            <td>Kepemilikan Rumah</td>
                                            <td>Nomor 123</td>
                                            <td>180</td>
                                            <td>Rp. 150.000.000</td>
                                            <td>Rp. 50.000.000</td>
                                            <td>24-08-2021</td>
                                            <td>24-08-2025</td>
                                            <td> <a href="/borrower/portofolio/77"
                                                    class="btn btn-sm btn-secondary">Lihat</a>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>81</td>
                                            <td>Dana Rumah</td>
                                            <td>Kepemilikan Rumah</td>
                                            <td>Nomor 456</td>
                                            <td>180</td>
                                            <td>Rp. 200.000.000</td>
                                            <td>Rp. 100.000.000</td>
                                            <td>24-08-2021</td>
                                            <td>24-08-2025</td>
                                            <td> <a href="/borrower/portofolio/77"
                                                    class="btn btn-sm btn-secondary">Lihat</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>



                </div>
            </div>
            <!-- END Page Content -->
        </main>
    @endif


    <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            var idbrw = {{ $idbrw }};
            $('.div-total').slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 2,
                adaptiveHeight: true,
                centerPadding: '60px',
                responsive: [{
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1
                        }
                    }
                ]
            });
        });
    </script>
@endsection
