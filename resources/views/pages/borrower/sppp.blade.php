@extends('layouts.borrower.master')

@section('title', 'Kelola Pencairan Dana')
<style>
    .bg-gradient-hijau{
    background: rgb(47,122,21);
    background: -moz-linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    background: linear-gradient(51deg, rgba(47,122,21,1) 0%, rgba(6,119,87,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#2f7a15",endColorstr="#067757",GradientType=1);
  }
  .bg-gradient-hijau-2{
    background: rgb(21,122,89);
    background: -moz-linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    background: linear-gradient(51deg, rgba(21,122,89,1) 0%, rgba(6,95,119,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#157a59",endColorstr="#065f77",GradientType=1);
  }
  .bg-gradient-blue{
    background: rgb(21,85,122);
    background: -moz-linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    background: -webkit-linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    background: linear-gradient(51deg, rgba(21,85,122,1) 0%, rgba(6,55,119,1) 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#15557a",endColorstr="#063777",GradientType=1);
  }
</style>

@section('content')
    <!-- Main Container -->
    @if(Auth::guard('borrower')->user()->status == 'pending')
    <main id="main-container">

        <!-- Page Content -->
        <div class="content">
            <div class="row">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h1 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;" >Silahkan Tunggu Verifikasi dari Danasyariah</h1>                    
                    </span>
                </div>
            </div>
			<div class="row">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h4 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;" >Terimakasih telah bergabung untuk maju bersama kami</h4>                    
                    </span>
					
					
                </div>
            </div>
            
            <!-- END Frequently Asked Questions -->
        </div>
        <!-- END Page Content -->

    </main>
    @elseif(Auth::guard('borrower')->user()->status == 'active') 
    <main id="main-container">
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container col-12">
                <div class="row mt-5 pt-5">
                    <div id="col2" class="col-md-12 mt-5 pt-5">
                    <span class="mb-10 pb-10 ">
                        <h3 class="no-paddingTop font-w300 judul" style="float: left; " >Pendanaan Terverifikasi</h3>
                    </span>
                    <br><br>
                    <table class="table border" id="table_sp3">
                        <thead class="bg-dark text-light">
                            <tr>
                            <th class="text-center" rowspan="2" style="vertical-align: middle;">No</th>
                            <th class="text-center" rowspan="2" style="vertical-align: middle;">Pendanaan</th>
                            <th hidden rowspan="2">id_proyek</th>
                            <th hidden rowspan="2">pendanaan_id</th>
                            <th class="text-center" rowspan="2" style="vertical-align: middle;">Tenor</th>
                            <th class="text-center" colspan='2'>Pengajuan</th>
                            <th class="text-center" colspan='2'>Terverifikasi</th>
                            <th class="text-center" rowspan="2" style="vertical-align: middle;">Setujui SP3</th>
                        </tr>
                        <tr class="bg-dark text-light">
                            <th class="text-center" >Nominal</th>
                            <th class="text-center" >Imbal Hasil</th>
                            <th class="text-center" >Nominal</th>
                            <th class="text-center" >Imbal Hasil</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                               
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main> 
    @endif
    <!-- END Main Container -->

    {{-- Modal SP3 --}}
        <div id="modalSP3"  class="modal fade in" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">SP3</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="agree">
                    @csrf
                        <div class="modal-body" id="modalBodySP3">
                            {{-- <iframe src="{{ url('perjanjian') }}" scrolling="yes" width="100%" height="500" id="iprem"></iframe> --}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-success" id="setujuSP3">Saya Setuju</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal SP3 End --}}


    <script type="text/javascript">
    $(document).ready( function(){
    var a = {{$idbrw}};
    var spppTable = $('#table_sp3').DataTable({
            // searching: true,
            // processing: true,
            // serverSide: true,
            ajax: {
                url: '/borrower/sppp_data/'+a,
                method:'get',
                // dataSrc: 'data'
            },
            // paging: true,
            // info: true,
            // lengthChange:false,
            // order: [ 1, 'asc' ],
            // pageLength: 15,
            columns: [
                { data : null,
                  render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  meta.row+1;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  row.pendanaan_nama;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return row.id_proyek;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return row.pendanaan_id;
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  row.durasi_proyek+' bulan';
                    }
                },
                { data: null,
                    render:function(data,type,row,meta)
                    {
                        if (row.p_dana == null)
                        {
                            return 'Rp. 0';
                        }
                        else
                        {
                            return 'Rp.'+ parseInt(row.p_dana).toLocaleString();
                        }
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  parseInt(row.p_imbal) +' %';
                    }
                },
                { data: null,
                    render:function(data,type,row,meta)
                    {
                        if (row.v_dana == null)
                        {
                            return 'Rp. 0';
                        }
                        else
                        {
                            return 'Rp.'+ parseInt(row.v_dana).toLocaleString();
                        }
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  parseInt(row.v_imbal) +' %';
                    }
                },
                { data: null,
                    render: function (data, type, row, meta) {
                          //I want to get row index here somehow
                          return  '<button class="btn sm btn-primary sp3" id="sp3">SP3</button>';
                    }
                }
            ]
            ,
            columnDefs: [
                { targets: [2,3], visible: false}
            ]
        });

        $('#table_sp3 tbody').on( 'click', 'tr', function () {
            console.log( spppTable.row( this ).data() );
            var data = spppTable.row( this ).data();
             var id_proyek = data['id_proyek'];
            var pendanaan_id = data['pendanaan_id'];
            var idbrw = a;

            // console.log(id_proyek+'-'+pendanaan_id+'-'+a);
                $('#modalBodySP3').html(" ");
                
                $.ajax({
                    url:'/borrower/generateSP3/'+idbrw+'/'+id_proyek+'/'+pendanaan_id,
                    method:'get',
                    success: function (response) {
                            if (response.status == 'Berhasil')
                            {
                                var id = id_proyek+'-'+pendanaan_id+'-'+response.nosp3;
                                $('#modalSP3').modal('show');
                                $('#modalBodySP3').append('<iframe id="linkSP3" scrolling="yes" width="100%" height="500" id="iprem"></iframe>');
                                $('#linkSP3').attr('src',"{{ url('viewSP3') }}/"+idbrw);
                                $('#setujuSP3').attr('value',id);
                            }
                            else
                            {
                                alert(response.status)
                            }

                    },
                    error: function(request, status, error)
                    {
                        alert(status)
                    }
                });
        });

        $('#setujuSP3').on('click',function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                var idbrw = a;
                var id = $('#setujuSP3').val();
                var pecahid = id.split("-");
                var pendanaan_id = pecahid[1];
                var id_proyek = pecahid[0];
                var sp3 = pecahid[2];

                $.ajax({
                    url:'/borrower/updateSP3/',
                    method:'post',
                    data: {'proyek_id': id_proyek,'idbrw':idbrw,'pendanaan_id':pendanaan_id,'sp3':sp3},
                    success: function (response) {
                            // console.log(response.jsonFile.message)
                            if (response.jsonFile.status == '00')
                            {
                                Swal.fire({title:"Notifikasi",text:response.jsonFile.message,type:"info"})
                                    .then(function(){
                                        location.reload(true)
                                    })
                            }
                            else
                            {
                                Swal.fire({title:"Notifikasi",text:response.jsonFile.message,type:"info"})
                                    .then(function(){
                                        location.reload(true)
                                    })
                            }

                    },
                    error: function(request, status, error)
                    {
                        alert(status)
                    }
                });
            });
    });
</script>
@endsection
