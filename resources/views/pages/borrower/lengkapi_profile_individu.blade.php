<div class="col-12 mt-5 pt-5">
    <form id="form-individu" name="form-individu" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="js-wizard-simple block border layout pt-4" id="layout-informasi-pribadi" disabled>
            <div class="block-content block-content-full tab-content pb-4 mb-4">
                <div id="smartwizard">
                    {{-- START: Tab Title --}}
                    <ul class="nav" style="border: 0;">
                        <li id="li-step-informasi-pribadi">
                            <a class="nav-link" id="step-informasi-pribadi-title" href="#step-informasi-pribadi" style="border: 0;">
                                Informasi Pribadi
                            </a>
                        </li>
                        <li id="li-step-informasi-pasangan" class="d-none">
                            <a class="nav-link" id="step-informasi-pasangan-title" href="#step-informasi-pasangan" style="border: 0;">
                                Informasi pasangan
                            </a>
                        </li>
                    </ul>
                    {{-- END: Tab Title --}}

                    <hr class="line mb-4 pb-4">

                    {{-- START: Content --}}
                    <div class="tab-content">
                        <div id="step-informasi-pribadi" class="tab-pane" role="tabpanel">
                            {{-- Brw TYpe --}}
                            <input type="hidden" id="borrower_type" name="borrower_type">
                            <input name="status" type="hidden" value="1">

                            {{-- START: Baris 1 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama_individu" class="ml-0" for="">Nama Lengkap Sesuai KTP <i class="text-danger">*</i></label>
                                        <input class="form-control checkKarakterAneh" type="text" id="nama_individu" name="nama_individu" placeholder="Masukkan nama anda" value="{{ $data_individu ? $data_individu->nama : ''}}" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="gender">Jenis Kelamin <i class="text-danger">*</i></label>
                                        <div class="col-12">
                                            <div class="input-group">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="gender" id="gender1" value="1" required>
                                                    <label class="form-check-label text-muted" for="gender1">Laki -
                                                        Laki</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="gender" id="gender2" value="2">
                                                    <label class="form-check-label text-muted" for="gender2">Perempuan</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_ktp" for="ktp" class="ml-0">No. KTP <i class="text-danger">*</i></label>
                                        <input class="form-control no-zero no-four" type="text" id="ktp" name="ktp" placeholder="Masukkan nomor KTP" value="{{ $data_individu ? $data_individu->ktp : ''}}" readonly>
                                    </div>
                                </div>
                            </div>
                            {{-- START: Baris 1 --}}

                            {{-- START: Baris 2 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">No. Kartu Keluarga (KK)</label> <i class="text-danger">*</i>
                                        <input class="form-control" type="text" id="kk" name="kk" minlength="16" maxlength="16" placeholder="Masukkan No. Kartu Keluarga" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Nomor Kartu Keluarga tidak boleh kurang dari 16 digit')" pattern=".{16,16}" maxlength="16" minlength="16" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" value="{{ $data_individu ? $data_individu->nomor_kk_pribadi : ''}}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0" for="tempat_lahir">Tempat Lahir <i class="text-danger">*</i></label>
                                        <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="tempat_lahir" name="tempat_lahir" placeholder="Masukkan tempat lahir" value="{{ $data_individu ? $data_individu->tempat_lahir : ''}}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0" for="tanggal_lahir">Tanggal Lahir
                                            <i class="text-danger">*</i></label>
                                        <input class="form-control" type="date" id="tanggal_lahir" name="tanggal_lahir" max="<?= date("Y-m-d", strtotime('-21 year')); ?>" placeholder="Masukkan tanggal lahir" onchange="$(this).valid()" value="{{ $data_individu ? $data_individu->tgl_lahir : ''}}" required>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 2 --}}

                            {{-- START: Baris 3 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_telepon" class="ml-0">No. Hp <i class="text-danger">*</i></label>
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <span class="input-group-text input-group-text-dsi">
                                                    +62 </span>
                                            </div>
                                            <input class="form-control no-zero eight" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" minlength="9" pattern=".{9,13}" maxlength="13" id="telepon" name="telepon" placeholder="Masukkan nomor Telepon" value="{{ $data_individu ? substr($data_individu->no_tlp, 2) : ''}}" required>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">Agama <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="agama" name="agama" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0" for="pendidikan_terakhir">Pendidikan
                                            Terakhir <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="pendidikan_terakhir" name="pendidikan_terakhir" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 3 --}}

                            {{-- START: Baris 4 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label class="ml-0" for="nama_ibu">Nama Gadis Ibu
                                                Kandung <i class="text-danger">*</i> </label>
                                            <input class="form-control checkKarakterAneh" type="text" maxlength="35" id="nama_ibu" name="nama_ibu" placeholder="Masukkan Nama Ibu Kandung Anda" value="{{ $data_individu ? $data_individu->nm_ibu : ''}}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label id="label_npwp" for="npwp">No. NPWP <i class="text-danger">*</i> </label>
                                        <input class="form-control " type="text" id="npwp" name="npwp" placeholder="Masukkan nomor NPWP" value="{{ $data_individu ? $data_individu->npwp : ''}}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="ml-0">Status Perkawinan <i class="text-danger">*</i></label>
                                        <div class="input-group">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kawin" id="status_kawin_0" value="1" required>
                                                <label class="form-check-label text-muted" for="status_kawin_0">Sudah
                                                    Menikah</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kawin" id="status_kawin_1" value="2">
                                                <label class="form-check-label text-muted" for="status_kawin_1">Belum
                                                    Menikah</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_kawin" id="status_kawin_2" value="3">
                                                <label class="form-check-label text-muted" for="status_kawin_2">Duda/Janda</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 4 --}}

                            {{-- START: Baris 5 --}}
                            <div class="row">
                                <div class="col-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6 ml-0">Alamat Sesuai KTP
                                            &nbsp</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="alamat">Alamat Lengkap <i class="text-danger">*</i></label>
                                        <textarea class="form-control form-control-lg" maxlength="90" id="alamat" name="alamat" rows="6" placeholder="Masukkan alamat lengkap Anda" required>{{ $data_individu ? $data_individu->alamat : ''}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="provinsi">Provinsi <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="provinsi" name="provinsi" onChange="provinsiChange(this.value, this.id, 'kota')" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kota">Kota/Kabupaten <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="kota" name="kota" onChange="kotaChange(this.value, this.id, 'kecamatan')" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 5 --}}

                            {{-- START: Baris 6 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kecamatan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="kecamatan" name="kecamatan" onChange="kecamatanChange(this.value, this.id, 'kelurahan')" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kelurahan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="kelurahan" name="kelurahan" onChange="kelurahanChange(this.value, this.id, 'kode_pos',$('#kecamatan').val(),$('#kota').val(),$('#provinsi').val())" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kode_pos" class="ml-0">Kode Pos <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" maxlength="30" id="kode_pos" name="kode_pos" placeholder="--" value="{{ $data_individu ? $data_individu->kode_pos : ''}}" readonly>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 6 --}}

                            {{-- START: Baris 7 --}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="status_rumah" class="ml-0">Status Kepemilikan Rumah <i class="text-danger">*</i></label>

                                        <div class="input-group">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_rumah" id="status_rumah_0" value="1" required>
                                                <label class="form-check-label text-muted" for="status_rumah_0">Milik
                                                    Pribadi</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_rumah" id="status_rumah_1" value="2">
                                                <label class="form-check-label text-muted" for="status_rumah_1">Sewa</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="status_rumah" id="status_rumah_2" value="3">
                                                <label class="form-check-label text-muted" for="status_rumah_2">Milik
                                                    Keluarga</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 7 --}}

                            <div class="row">
                                <div class="col-md-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <input class="form-check-input" type="checkbox" name="domisili_status" id="domisili_status" value="1">
                                        <label class="form-check-label text-black h6" for="domisili_status">Alamat
                                            Domisili Sama dengan
                                            KTP ? &nbsp</label>
                                    </div>
                                </div>
                            </div>

                            <div id="layout-alamat-domisili">
                                {{-- START: Baris 8 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="alamat_domisili" class="ml-0">Alamat
                                                Lengkap <i class="text-danger">*</i></label>
                                            <textarea class="form-control form-control-lg" maxlength="90" id="alamat_domisili" name="alamat_domisili" rows="6" placeholder="Masukkan alamat lengkap Anda.." required>{{ $data_individu ? $data_individu->domisili_alamat : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="provinsi_domisili" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="provinsi_domisili" name="provinsi_domisili" onchange="provinsiChange(this.value, this.id, 'kota_domisili')" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kota_domisili" class="ml-0">Kota/Kabupaten <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kota_domisili" name="kota_domisili" onchange="kotaChange(this.value, this.id, 'kecamatan_domisili')" required>
                                                <option value="">-- Pilih Satu --
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 8 --}}

                                {{-- START: Baris 9 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kecamatan_domisili" class="ml-0">Kecamatan <i class="text-danger">*</i> </label>
                                            <select class="form-control custom-select" id="kecamatan_domisili" name="kecamatan_domisili" onchange="kecamatanChange(this.value, this.id, 'kelurahan_domisili')" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kelurahan_domisili" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kelurahan_domisili" name="kelurahan_domisili" onchange="kelurahanChange(this.value, this.id, 'kode_pos_domisili',$('#kecamatan_domisili').val(),$('#kota_domisili').val(),$('#provinsi_domisili').val())" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kode_pos_domisili" class="ml-0">Kode
                                                Pos <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" maxlength="30" id="kode_pos_domisili" name="kode_pos_domisili" placeholder="--" value="{{ $data_individu ? $data_individu->domisili_kd_pos : ''}}" readonly>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 9 --}}

                                {{-- START: Baris 10 --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="ml-0">Status Kepemilikan
                                                Rumah</label>
                                            <div class="input-group">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="domisili_status_rumah" id="domisili_status_rumah_0" value="1" required>
                                                    <label class="form-check-label text-muted" for="domisili_status_rumah_0">Milik
                                                        Pribadi</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="domisili_status_rumah" id="domisili_status_rumah_1" value="2">
                                                    <label class="form-check-label text-muted" for="domisili_status_rumah_1">Sewa</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="domisili_status_rumah" id="domisili_status_rumah_2" value="3">
                                                    <label class="form-check-label text-muted" for="domisili_status_rumah_2">Milik Keluarga</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 10 --}}
                            </div>

                            {{-- START: Baris 11 --}}
                            <div class="row">
                                <div class="col-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6 ml-0">Informasi Rekening
                                            &nbsp</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="no_rekening" class="ml-0">No
                                            Rekening <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" id="no_rekening" name="no_rekening" placeholder="Masukkan No Rekening" pattern=".{10,16}" minlength="10" maxlength="16" value="{{ $data_rekening ? $data_rekening->brw_norek : '' }}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nama_pemilik_rekening" class="ml-0">Nama
                                            Pemilik Rekening <i class="text-danger">*</i></label>
                                        <input class="form-control checkKarakterAneh" type="text" id="nama_pemilik_rekening" name="nama_pemilik_rekening" placeholder="Masukkan Nama Pemilik Rekening" maxlength="35" value="{{ $data_rekening ? $data_rekening->brw_nm_pemilik : '' }}" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bank" class="ml-0">Bank <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" onchange="$(this).valid()" id="bank" name="bank" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 11 --}}

                            {{-- START: Baris 12 --}}
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="kantor_cabang" class="ml-0">Kantor Cabang Pembuka <i class="text-danger">*</i></label>
                                        <input class="form-control" type="text" id="kantor_cabang" maxlength="35" name="kantor_cabang" placeholder="Masukkan kantor cabang pembuka" maxlength="50" value="{{ $data_rekening ? $data_rekening->kantor_cabang_pembuka : '' }}" required>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 12 --}}

                            {{-- START: Baris 13 --}}
                            <div class="row">
                                <div class="col-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6 ml-0">Informasi Pekerjaan
                                            &nbsp</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="pekerjaan" class="ml-0">Pekerjaan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="pekerjaan" name="pekerjaan" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="sumber_penghasilan_dana" class="ml-0">Sumber Penghasilan Dana <i class="text-danger">*</i></label>
                                    <select class="form-control custom-select" id="sumber_penghasilan_dana" name="sumber_penghasilan_dana" required>
                                        <option value="">-- Pilih Satu --</option>
                                        <option value="1">Penghasilan Tetap</option>
                                        <option value="2">Penghasilan Tidak Tetap</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="skema_pembiayaan" class="ml-0">Skema Pembiayaan <i class="text-danger">*</i></label>
                                    <select class="form-control custom-select" id="skema_pembiayaan" name="skema_pembiayaan" required>
                                        <option value="">-- Pilih Satu --</option>
                                        <option value="1">Penghasilan Sendiri</option>
                                        <option value="2">Penghasilan Bersama</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bidang_pekerjaan" class="ml-0">Bidang
                                            Pekerjaan <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="bidang_pekerjaan" name="bidang_pekerjaan" required>
                                            <option value="">-- Pilih Satu --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="bidang_online" class="ml-0">Bidang
                                            Pekerjaan Online <i class="text-danger">*</i></label>
                                        <select class="form-control custom-select" id="bidang_online" name="bidang_online" required>
                                            <option value="">-- Pilih Satu --
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {{-- START: Baris 13 --}}

                            {{-- START: Detail Penghasilan --}}
                            <div id="detail-penghasilan" class="d-none">
                                <div class="row">
                                    {{--START: Baris 14 --}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_nama_perusahaan" for="nama_perusahaan" class="ml-0">Nama
                                                Perusahaan <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="nama_perusahaan" maxlength="50" name="nama_perusahaan" placeholder="Masukkan nama perusahaan" value="{{ $data_penghasilan ? $data_penghasilan->nama_perusahaan : ''}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="badan_usaha" class="ml-0 ">Bentuk Badan Usaha <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="badan_usaha" name="badan_usaha" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="status_kepegawaian" class="ml-0">Status Kepegawaian <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="status_kepegawaian" name="status_kepegawaian" required>
                                                <option value="">-- Pilih Satu --</option>
                                                <option value="1">Kontrak</option>
                                                <option value="2">Tetap</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{--END: Baris 14 --}}

                                    {{--START: Baris 15 --}}
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_usia_perusahaan" for="usia_perusahaan" class="ml-0">Usia
                                                Perusahaan(Tahun)
                                                <i class="text-danger">*</i></label>
                                            <input class="form-control" type="number" id="usia_perusahaan" maxlength="5" name="usia_perusahaan" placeholder="Masukkan usia perusahaan" value="{{ $data_penghasilan ? $data_penghasilan->usia_perusahaan : ''}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="departemen" class="ml-0">Departemen <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="departemen" maxlength="50" name="departemen" placeholder="Masukkan nama departemen" value="{{ $data_penghasilan ? $data_penghasilan->departemen : ''}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="jabatan" class="ml-0">Jabatan <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="jabatan" maxlength="50" name="jabatan" placeholder="Masukkan jabatan" value="{{ $data_penghasilan ? $data_penghasilan->jabatan : ''}}" required>
                                        </div>
                                    </div>
                                    {{--END: Baris 15 --}}

                                    {{--START: Baris 16 --}}
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label class="ml-0">Lama Bekerja (Tahun) <i class="text-danger">*</i></label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                        </div>
                                                        <input type="number" id="tahun_bekerja" name="tahun_bekerja" class="form-control" aria-label="tahun_bekerja" aria-describedby="basic-addon1" min="0" value="{{ $data_penghasilan ? $data_penghasilan->masa_kerja_tahun : ''}}" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                        </div>
                                                        <input type="number" id="bulan_bekerja" name="bulan_bekerja" class="form-control" aria-label="bulan_bekerja" aria-describedby="basic-addon1" min="0" max="11" value="{{ $data_penghasilan ? $data_penghasilan->masa_kerja_bulan : ''}}" required>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="nip" class="ml-0">NIP/NRP/NIK <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="nip" name="nip" maxlength="35" placeholder="Masukkan nip/nrp/nik" value="{{ $data_penghasilan ? $data_penghasilan->nip_nrp_nik : ''}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="nama_hrd">Nama HRD <i class="text-danger">*</i></label>
                                            <input class="form-control check checkKarakterAneh" type="text" id="nama_hrd" maxlength="35" name="nama_hrd" placeholder="Masukkan nama hrd" value="{{ $data_penghasilan ? $data_penghasilan->nama_hrd : ''}}" required>
                                        </div>
                                    </div>
                                    {{--END: Baris 16 --}}

                                    {{--START: Baris 17 --}}
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label for="no_hrd" class="ml-0">Nomor Fixed Line HRD <i class="text-danger">*</i></label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi"> +62
                                                    </span>
                                                </div>
                                                <input class="form-control no-zero" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" id="no_hrd" name="no_hrd" placeholder="Masukkan nomor fixed line hrd" value="{{ $data_penghasilan ? substr($data_penghasilan->no_fixed_line_hrd, 2) : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_no_telpon_usaha" for="no_telpon_usaha" class="ml-0">No.
                                                Telepon Perusahaan <i class="text-danger">*</i></label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi"> +62
                                                    </span>
                                                </div>
                                                <input class="form-control no-zero" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'');" pattern=".{9,13}" maxlength="13" id="no_telpon_usaha" name="no_telpon_usaha" placeholder="Masukkan nomor telpon perusahaan" value="{{ $data_penghasilan ? substr($data_penghasilan->no_telp, 2) : ''}}" required>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4 non-fixed d-none">
                                        <div class="form-group">
                                            <label for="no_hp_usaha" class="ml-0">No. HP Usaha/Praktek <i class="text-danger">*</i></label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi"> +62
                                                    </span>
                                                </div>
                                                <input class="form-control no-zero eight" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" pattern=".{9,13}" maxlength="13" id="no_hp_usaha" name="no_hp_usaha" placeholder="Masukkan no. hp Usaha" value="{{ $data_penghasilan ? substr($data_penghasilan->no_hp, 2) : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    {{--END: Baris 17 --}}
                                </div>

                                {{-- START: baris 18 --}}
                                <div class="row non-fixed d-none">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="surat_ijin_usaha" class="ml-0">Surat Ijin Usaha/Praktek <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="surat_ijin_usaha" name="surat_ijin_usaha" required>
                                                <option value="">-- Pilih Satu --</option>
                                                <option value="1">Ya</option>
                                                <option value="2">Tidak</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_no_ijin_usaha" for="no_ijin_usaha" class="ml-0">Nomor Ijin
                                                Usaha/Praktek <i class="text-danger">*</i></label>
                                            <input class="form-control" id="no_ijin_usaha" type="text" name="no_ijin_usaha" placeholder="Masukkan no ijin usaha" value="{{ $data_penghasilan ? $data_penghasilan->no_surat_ijin : ''}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="lama_tempat_usaha" class="ml-0">Lama Tempat Usaha/Praktek
                                                (Tahun) <i class="text-danger">*</i></label>
                                            <input class="form-control" id="lama_tempat_usaha" type="number" maxlength="5" name="lama_tempat_usaha" placeholder="Masukkan lama tempat usaha" value="{{ $data_penghasilan ? $data_penghasilan->usia_tempat_usaha : ''}}" required>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: baris 18 --}}

                                {{-- START: Alamat Perusahaan --}}
                                {{-- START: Baris 19 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_alamat_perusahaan" for="alamat_perusahaan" class="ml-0">Alamat
                                                Perusahaan/Tempat Usaha <i class="text-danger">*</i> </label>
                                            <textarea class="form-control form-control-lg" maxlength="90" id="alamat_perusahaan" name="alamat_perusahaan" rows="6" placeholder="Masukkan alamat perusahaan" required>{{ $data_penghasilan ? $data_penghasilan->alamat_perusahaan : ''}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="rt_perusahaan" class="ml-0">RT <i class="text-danger">*</i></label>
                                            <input class="form-control form-control-lg" maxlength="5" id="rt_perusahaan" name="rt_perusahaan" placeholder="Masukkan rt" value="{{ $data_penghasilan ? $data_penghasilan->rt : ''}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="rw_perusahaan" class="ml-0">RW <i class="text-danger">*</i></label>
                                            <input class="form-control form-control-lg" maxlength="5" id="rw_perusahaan" name="rw_perusahaan" placeholder="Masukkan rw" value="{{ $data_penghasilan ? $data_penghasilan->rw : ''}}" required>
                                        </div>
                                    </div>

                                </div>
                                {{-- END: Baris 19 --}}

                                {{-- START: Baris 20 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="provinsi_perusahaan" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="provinsi_perusahaan" name="provinsi_perusahaan" onchange="provinsiChange(this.value, this.id, 'kota_perusahaan')" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kota_perusahaan" class="ml-0">Kota/Kabupaten <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kota_perusahaan" name="kota_perusahaan" onchange="kotaChange(this.value, this.id, 'kecamatan_perusahaan')" required>
                                                <option value="">-- Pilih Satu --
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kecamatan_perusahaan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kecamatan_perusahaan" name="kecamatan_perusahaan" onchange="kecamatanChange(this.value, this.id, 'kelurahan_perusahaan')" required>
                                                <option value="">Pilih Kecamatan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 20 --}}

                                {{-- START: Baris 21 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kelurahan_perusahaan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kelurahan_perusahaan" name="kelurahan_perusahaan" onchange="kelurahanChange(this.value, this.id, 'kode_pos_perusahaan',$('#kecamatan_perusahaan').val(),$('#kota_perusahaan').val(),$('#provinsi_perusahaan').val())" required>
                                                <option value="">Pilih Kelurahan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kode_pos_perusahaan" class="ml-0">Kode
                                                Pos <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" maxlength="30" id="kode_pos_perusahaan" name="kode_pos_perusahaan" placeholder="--" value="{{ $data_penghasilan ? $data_penghasilan->kode_pos : '' }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 21 --}}
                                {{-- END: Alamat Perusahaan --}}

                                {{-- START: Baris 22 --}}
                                <div class="row">
                                    <div class="col-md-4 fixed-income">
                                        <div class="form-group">
                                            <label class="ml-0">Pengalaman Kerja Di Tempat Lain <i class="text-danger">*</i></label>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                        </div>
                                                        <input type="number" id="tahun_bekerja_ditempat_lain" name="tahun_bekerja_ditempat_lain" class="form-control" aria-label="tahun_bekerja_ditempat_lain" aria-describedby="basic-addon1" min="0" value="{{ $data_penghasilan ? $data_penghasilan->pengalaman_kerja_tahun : '' }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                        </div>
                                                        <input type="number" id="bulan_bekerja_ditempat_lain" name="bulan_bekerja_ditempat_lain" class="form-control" aria-label="bulan_bekerja_ditempat_lain" aria-describedby="basic-addon1" min="0" max="11" value="{{ $data_penghasilan ? $data_penghasilan->pengalaman_kerja_tahun : ''}}" required>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="penghasilan" class="ml-0">Penghasilan (Per Bulan) <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="penghasilan" name="penghasilan" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" placeholder="Masukkan penghasilan anda" value="{{ !empty($data_penghasilan->pendapatan_borrower) ? number_format((int)$data_penghasilan->pendapatan_borrower,'0',',','.') : ''}}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="biaya_hidup" class="ml-0">Biaya Hidup (Per Bulan) <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="biaya_hidup" name="biaya_hidup" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" placeholder="Masukkan biaya hidup anda" value="{{ !empty($data_penghasilan->biaya_hidup) ? number_format((int)$data_penghasilan->biaya_hidup,'0',',','.') : '' }}" required>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 22 --}}

                                {{-- START: Baris 23 --}}

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_nilai_spt" for="nilai_spt" class="ml-0">Nilai SPT Tahun
                                                Terakhir
                                                <i class="text-danger">*</i></label>
                                            <input class="form-control form-control-lg" id="nilai_spt" name="nilai_spt" placeholder="Masukkan nilai Spt" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" value="{{ !empty($data_penghasilan->nilai_spt) ? number_format((int)$data_penghasilan->nilai_spt,'0',',','.') : '' }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 mb-4 pb-4">
                                        <div class="form-check form-check-inline line">
                                            <input class="form-check-input" type="checkbox" name="penghasilan_lain_lain" id="penghasilan_lain_lain" value="1">
                                            <label class="form-check-label text-black h6" for="penghasilan_lain_lain">Saya Memiliki
                                                Penghasilan Lain
                                                Lain &nbsp</label>
                                        </div>
                                    </div>
                                </div>

                                <div id="more-penghasilan-lain-lain" class="d-none">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="detail_penghasilan_lain_lain" class="ml-0">Detail
                                                    Penghasilan Lain
                                                    Lain <i class="text-danger">*</i></label>
                                                <textarea class="form-control form-control-lg" maxlength="90" id="detail_penghasilan_lain_lain" name="detail_penghasilan_lain_lain" rows="6" placeholder="Contoh: Jualan bakso, jualan jus buah, ngojek" required>{{ $data_penghasilan ? $data_penghasilan->detail_penghasilan_lain_lain : '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="total_penghasilan_lain_lain" class="ml-0">Total Penghasilan
                                                    Lain Lain (Per Bulan) <i class="text-danger">*</i></label>
                                                <input class="form-control form-control-lg" maxlength="90" id="total_penghasilan_lain_lain" name="total_penghasilan_lain_lain" placeholder="Masukkan total penghasilan lain lain" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" value="{{ !empty($data_penghasilan->total_penghasilan_lain_lain) ? number_format((int)$data_penghasilan->total_penghasilan_lain_lain,'0',',','.') : '' }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{-- END: Baris 23 --}}
                            </div>
                            {{-- END: Detail Penghasilan --}}

                            {{-- START: Penghasilan Lain Lain --}}
                            {{-- START: Baris 14 --}}
                            <div id="detail-penghasilan-lain-lain" class="d-none">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="detail_penghasilan_lain_lain2" class="ml-0">Detail Pekerjaan <i class="text-danger">*</i></label>
                                            <textarea class="form-control form-control-lg" maxlength="90" id="detail_penghasilan_lain_lain2" name="detail_penghasilan_lain_lain2" rows="6" placeholder="Contoh: Jualan bakso, jualan jus buah, ngojek" required>{{ $data_penghasilan ? $data_penghasilan->detail_penghasilan_lain_lain : ''}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="penghasilan2" class="ml-0">Penghasilanaa (Per Bulan) <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="penghasilan2" name="penghasilan2" placeholder="Masukkan penghasilan anda" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); this.value = formatRupiah(this.value)" maxlength="13" value="{{ $data_penghasilan ? (int)$data_penghasilan->pendapatan_borrower : '' }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="biaya_hidup2" class="ml-0">Biaya Hidup (Per Bulan) <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" id="biaya_hidup2" name="biaya_hidup2" placeholder="Masukkan biaya hidup anda" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); this.value = formatRupiah(this.value)" maxlength="13" value="{{ $data_penghasilan ? (int)$data_penghasilan->biaya_hidup : '' }}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END: Baris 14 --}}
                            {{-- END: Penghasilan Lain Lain --}}

                            {{-- START: Baris 15 --}}
                            <div class="row">
                                <div class="col-12 mb-4 pb-4">
                                    <div class="form-check form-check-inline line">
                                        <label class="form-check-label text-black h6 ml-0">Foto &nbsp</label>
                                    </div>
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto Diri <i class="text-danger">*</i></label>
                                    <div id="preview_camera_diri" name="preview_camera_diri" class="pt-3">
                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic)]) .'?t='.date("Y-m-d h:i:sa") : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" type="button" id="btn_camera_diri" name="btn_camera_diri" onclick="btnCameraClick('camera_diri', 'preview_camera_diri', 'take_camera_diri')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_diri" name="take_camera_diri" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{ asset('assets/img/user-guide.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_diri" name="camera_diri"></div>
                                            <input class="btn btn-primary green-dsi mx-0" type="button" value="Ambil Foto" id="take_snapshot_diri" name="take_snapshot_diri" onclick="takeSnapshot('result_camera_diri', 'user_camera_diri', 'user_camera_diri_val', 'result_diri'); $('#user_camera_diri_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_diri" name="user_camera_diri" value="{{ $data_individu ?  $data_individu->brw_pic : ''}}" required class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_diri" name="result_camera_diri" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_diri" name="result_diri"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_diri_val" name="user_camera_diri_val" value="{{ $data_individu ?  $data_individu->brw_pic : ''}}" required>
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto KTP <i class="text-danger">*</i></label>
                                    <div id="preview_camera_ktp" name="preview_camera_ktp" class="pt-3">
                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic_ktp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" type="button" id="btn_camera_ktp" name="btn_camera_ktp" onclick="btnCameraClick('camera_ktp', 'preview_camera_ktp', 'take_camera_ktp')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_ktp" name="take_camera_ktp" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{asset('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_ktp" name="camera_ktp"></div>
                                            <input class="btn btn-primary green-dsi" type="button" value="Ambil Foto" id="take_snapshot_ktp" name="take_snapshot_ktp" onclick="takeSnapshot('result_camera_ktp', 'user_camera_ktp', 'user_camera_ktp_val', 'result_ktp'); $('#user_camera_ktp_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_ktp" name="user_camera_ktp" value="{{ $data_individu ?  $data_individu->brw_pic_ktp : ''}}" class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_ktp" name="result_camera_ktp" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_ktp" name="result_ktp"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_ktp_val" name="user_camera_ktp_val" value="{{ $data_individu ?  $data_individu->brw_pic_ktp : ''}}" required>
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto Diri & KTP <i class="text-danger">*</i></label>
                                    <div id="preview_camera_diri_dan_ktp" name="preview_camera_diri_dan_ktp" class="pt-3">
                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic_user_ktp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" id="btn_camera_diri_dan_ktp" name="btn_camera_diri_dan_ktp" type="button" onclick="btnCameraClick('camera_diri_dan_ktp', 'preview_camera_diri_dan_ktp', 'take_camera_diri_dan_ktp')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_diri_dan_ktp" name="take_camera_diri_dan_ktp" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{asset('assets/img/guide-diridanktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_diri_dan_ktp" name="camera_diri_dan_ktp"></div>
                                            <input class="btn btn-primary green-dsi" type="button" value="Ambil Foto" id="take_snapshot_diri_dan_ktp" name="take_snapshot_diri_dan_ktp" onclick="takeSnapshot('result_camera_diri_dan_ktp', 'user_camera_diri_dan_ktp', 'user_camera_diri_dan_ktp_val', 'result_diri_dan_ktp'); $('#user_camera_diri_dan_ktp_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_diri_dan_ktp" value="{{ $data_individu ?  $data_individu->brw_pic_user_ktp : ''}}" name="user_camera_diri_dan_ktp" class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_diri_dan_ktp" name="result_camera_diri_dan_ktp" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_diri_dan_ktp" name="result_diri_dan_ktp"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_diri_dan_ktp_val" name="user_camera_diri_dan_ktp_val" value="{{ $data_individu ?  $data_individu->brw_pic_user_ktp : ''}}" required>
                                </div>

                                <div class="col-xl-3 col-md-6 col-sm-12 mb-4">
                                    <label class="ml-0">Foto NPWP <i class="text-danger">*</i></label>
                                    <div id="preview_camera_npwp" name="preview_camera_npwp" class="pt-3">
                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->brw_pic_npwp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                        <div>
                                            <button class="btn btn-primary green-dsi" id="btn_camera_npwp" name="btn_camera_npwp" type="button" onclick="btnCameraClick('camera_npwp', 'preview_camera_npwp', 'take_camera_npwp')">
                                                Kamera
                                            </button>
                                        </div>
                                    </div>

                                    <div id="take_camera_npwp" name="take_camera_npwp" class="d-none">
                                        <div class="col p-0">
                                            <img id="user-guide img-fluid" src="{{asset('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                            <div id="camera_npwp" name="camera_npwp"></div>
                                            <input class="btn btn-primary green-dsi" type="button" value="Ambil Foto" id="take_snapshot_npwp" name="take_snapshot_npwp" onclick="takeSnapshot('result_camera_npwp', 'user_camera_npwp', 'user_camera_npwp_val', 'result_npwp'); $('#user_camera_npwp_val-error').addClass('d-none')">
                                            <input type="hidden" id="user_camera_npwp" name="user_camera_npwp" value="{{ $data_individu ?  $data_individu->brw_pic_npwp : ''}}" class="image-tag"><br />
                                        </div>
                                    </div>

                                    <div id="result_camera_npwp" name="result_camera_npwp" class="d-none">
                                        <label class="my-3">Hasil</label>
                                        <div id="result_npwp" name="result_npwp"></div>
                                    </div>
                                    <input type="text" class="input-hidden" id="user_camera_npwp_val" name="user_camera_npwp_val" value="{{ $data_individu ?  $data_individu->brw_pic_npwp : ''}}" required>
                                </div>
                            </div>
                            {{-- END: Baris 15 --}}
                        </div>

                        <div id="step-informasi-pasangan" class="tab-pane" role="tabpanel">
                            <div id="layout-informasi-pasangan">
                                {{-- START: Baris 1 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nama_pasangan" class="ml-0" for="">Nama Lengkap Sesuai KTP <i class="text-danger">*</i></label>
                                            <input class="form-control checkKarakterAneh" type="text" maxlength="35" id="nama_pasangan" name="nama_pasangan" placeholder="Masukkan nama pasangan anda" value="{{ $data_pasangan ? $data_pasangan->nama : '' }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="jns_kelamin_pasangan">Jenis Kelamin <i class="text-danger">*</i></label>
                                            <div class="col-12">
                                                <div class="input-group">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="jns_kelamin_pasangan" id="jns_kelamin_pasangan_1" value="1">
                                                        <label class="form-check-label text-muted" for="jns_kelamin_pasangan_1">Laki -
                                                            Laki</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="jns_kelamin_pasangan" id="jns_kelamin_pasangan_2" value="2">
                                                        <label class="form-check-label text-muted" for="jns_kelamin_pasangan_2">Perempuan</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="ktp_pasangan" class="ml-0">No. KTP <i class="text-danger">*</i></label>
                                            <input class="form-control no-zero no-four" type="text" id="ktp_pasangan" name="ktp_pasangan" minlength="16" maxlength="16" placeholder="Masukkan nomor KTP" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')" pattern=".{16,16}" maxlength="16" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" value="{{ $data_pasangan ? $data_pasangan->ktp : '' }}" required>
                                        </div>
                                    </div>
                                </div>
                                {{-- START: Baris 1 --}}

                                {{-- START: Baris 2 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="ml-0" for="tempat_lahir_pasangan">Tempat Lahir <i class="text-danger">*</i></label>
                                            <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="tempat_lahir_pasangan" name="tempat_lahir_pasangan" placeholder="Masukkan tempat lahir" value="{{ $data_pasangan ? $data_pasangan->tempat_lahir : '' }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="ml-0" for="tanggal_lahir_pasangan">Tanggal Lahir
                                                <i class="text-danger">*</i></label>
                                            <input class="form-control" type="date" id="tanggal_lahir_pasangan" name="tanggal_lahir_pasangan" max="<?= date("Y-m-d"); ?>" placeholder="Masukkan tanggal lahir" onchange="$(this).valid()" value="{{ $data_pasangan ? $data_pasangan->tgl_lahir : '' }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="telepon_pasangan" class="ml-0">No. Hp <i class="text-danger">*</i></label>
                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <span class="input-group-text input-group-text-dsi">
                                                        +62 </span>
                                                </div>
                                                <input class="form-control no-zero eight" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" minlength="9" id="telepon_pasangan" name="telepon_pasangan" pattern=".{9,13}" placeholder="Masukkan nomor Telepon" value="{{ $data_pasangan ? substr($data_pasangan->no_hp, 2) : '' }}" required>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 2 --}}

                                {{-- START: Baris 3 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="ml-0" for="kk_pasangan">No. Kartu Keluarga (KK)</label> <i class="text-danger">*</i>
                                            <input class="form-control" type="text" id="kk_pasangan" name="kk_pasangan" maxlength="16" placeholder="Masukkan No. Kartu Keluarga" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Nomor Kartu Keluarga tidak boleh kurang dari 16 digit')" pattern=".{16,16}" maxlength="16" minlength="16" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" value="{{ !empty($data_pasangan->no_kk) ? $data_pasangan->no_kk : (!empty($data_individu->nomor_kk_pribadi) ? $data_individu->nomor_kk_pribadi : '') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="agama_pasangan" class="ml-0">Agama <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="agama_pasangan" name="agama_pasangan" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="pendidikan_terakhir_pasangan" class="ml-0" for="pendidikan_terakhir">Pendidikan
                                                Terakhir <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="pendidikan_terakhir_pasangan" name="pendidikan_terakhir_pasangan" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="npwp_pasangan">No. NPWP </label>
                                            <input class="form-control " type="text" id="npwp_pasangan" name="npwp_pasangan" minlength="15" maxlength="15" pattern=".{15,15}" oninput="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" placeholder="Masukkan nomor NPWP" value="{{ $data_pasangan ? $data_pasangan->npwp : '' }}">
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 3 --}}

                                {{-- START: Baris 5 --}}
                                <div class="row">
                                    <div class="col-md-12 mb-4 pb-4">
                                        <div class="form-check form-check-inline line">
                                            <input class="form-check-input" type="checkbox" name="domisili_pasangan_status" id="domisili_pasangan_status" value="1">
                                            <label class="form-check-label text-black h6" for="domisili_pasangan_status">Alamat Sama dengan
                                                Pemohon &nbsp</label>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="alamat_pasangan" class="ml-0">Alamat
                                                Lengkap <i class="text-danger">*</i></label>
                                            <textarea class="form-control form-control-lg" maxlength="90" id="alamat_pasangan" name="alamat_pasangan" rows="6" placeholder="Masukkan alamat lengkap Anda" required>{{ $data_pasangan ? $data_pasangan->alamat : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="provinsi_pasangan" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="provinsi_pasangan" name="provinsi_pasangan" onchange="provinsiChange(this.value, this.id, 'kota_pasangan')" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kota_pasangan" class="ml-0">Kota/Kabupaten <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kota_pasangan" name="kota_pasangan" onchange="kotaChange(this.value, this.id, 'kecamatan_pasangan')" required>
                                                <option value="">-- Pilih Satu --
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 5 --}}

                                {{-- START: Baris 6 --}}
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kecamatan_pasangan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kecamatan_pasangan" name="kecamatan_pasangan" onchange="kecamatanChange(this.value, this.id, 'kelurahan_pasangan')" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kelurahan_pasangan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="kelurahan_pasangan" name="kelurahan_pasangan" onchange="kelurahanChange(this.value, this.id, 'kode_pos_pasangan',$('#kecamatan_pasangan').val(),$('#kota_pasangan').val(),$('#provinsi_pasangan').val())" required>
                                                <option value="">-- Pilih Satu --</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="kode_pos_pasangan" class="ml-0">Kode
                                                Pos <i class="text-danger">*</i></label>
                                            <input class="form-control" type="text" maxlength="30" id="kode_pos_pasangan" name="kode_pos_pasangan" placeholder="--" value="{{ $data_pasangan ? $data_pasangan->kode_pos : ''}}" readonly>
                                        </div>
                                    </div>
                                </div>
                                {{-- END: Baris 6 --}}

                                <div id="informasi-pekerjaan-pasangan" class="d-none">
                                    {{-- START: Baris 7 --}}
                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Informasi Pekerjaan
                                                    &nbsp</label>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="pekerjaan_pasangan" class="ml-0">Pekerjaan <i class="text-danger">*</i></label>
                                                <select class="form-control custom-select" id="pekerjaan_pasangan" name="pekerjaan_pasangan" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="sumber_penghasilan_dana_pasangan" class="ml-0">Sumber
                                                Penghasilan
                                                Dana
                                                <i class="text-danger">*</i></label>
                                            <select class="form-control custom-select" id="sumber_penghasilan_dana_pasangan" name="sumber_penghasilan_dana_pasangan" required>
                                                <option value="">-- Pilih Satu --</option>
                                                <option value="1">Penghasilan Tetap</option>
                                                <option value="2">Penghasilan Tidak Tetap</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="bidang_pekerjaan_pasangan" class="ml-0">Bidang
                                                    Pekerjaan <i class="text-danger">*</i></label>
                                                <select class="form-control custom-select" id="bidang_pekerjaan_pasangan" name="bidang_pekerjaan_pasangan" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="bidang_online_pasangan" class="ml-0">Bidang
                                                    Pekerjaan Online <i class="text-danger">*</i></label>
                                                <select class="form-control custom-select" id="bidang_online_pasangan" name="bidang_online_pasangan" required>
                                                    <option value="">-- Pilih Satu --
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- START: Baris 7 --}}

                                    {{-- START: Detail Penghasilan --}}
                                    <div id="detail-penghasilan-pasangan" class="d-none">
                                        <div class="row">
                                            {{--START: Baris 14 --}}
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="label_nama_perusahaan_pasangan" for="nama_perusahaan_pasangan" class="ml-0">Nama
                                                        Perusahaan <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="nama_perusahaan_pasangan" maxlength="50" name="nama_perusahaan_pasangan" placeholder="Masukkan nama perusahaan" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->nama_perusahaan : '' }}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label for="badan_usaha_pasangan" class="ml-0">Bentuk
                                                        Badan Usaha <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="badan_usaha_pasangan" name="badan_usaha_pasangan" required>
                                                        <option value="">-- Pilih Satu --</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label for="status_kepegawaian_pasangan" class="ml-0">Status
                                                        Kepegawaian
                                                        <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="status_kepegawaian_pasangan" name="status_kepegawaian_pasangan" required>
                                                        <option value="">-- Pilih Satu --</option>
                                                        <option value="1">Kontrak</option>
                                                        <option value="2">Tetap</option>
                                                    </select>
                                                </div>
                                            </div>
                                            {{--END: Baris 14 --}}

                                            {{-- START: Baris 15 --}}
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="label_usia_perusahaan_pasangan" for="usia_perusahaan_pasangan" class="ml-0">Usia
                                                        Perusahaan(Tahun)
                                                        <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="number" id="usia_perusahaan_pasangan" maxlength="5" name="usia_perusahaan_pasangan" placeholder="Masukkan usia perusahaan" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->usia_perusahaan : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label for="departemen_pasangan" class="ml-0">Departemen <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="departemen_pasangan" maxlength="50" name="departemen_pasangan" placeholder="Masukkan nama departemen" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->departemen : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label for="jabatan_pasangan" class="ml-0">Jabatan <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="jabatan_pasangan" maxlength="50" name="jabatan_pasangan" placeholder="Masukkan jabatan" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->jabatan : ''}}" required>
                                                </div>
                                            </div>
                                            {{-- END: Baris 15 --}}

                                            {{-- START: Baris 16 --}}
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label class="ml-0">Lama Bekerja (Tahun) <i class="text-danger">*</i></label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                                </div>
                                                                <input type="number" id="tahun_bekerja_pasangan" name="tahun_bekerja_pasangan" class="form-control" aria-label="tahun_bekerja_pasangan" aria-describedby="basic-addon1" min="0" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->masa_kerja_tahun : ''}}" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                                </div>
                                                                <input type="number" id="bulan_bekerja_pasangan" name="bulan_bekerja_pasangan" class="form-control" aria-label="bulan_bekerja_pasangan" aria-describedby="basic-addon1" min="0" max="11" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->masa_kerja_bulan : ''}}" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label for="nip_pasangan" class="ml-0">NIP/NRP/NIK <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="nip_pasangan" name="nip_pasangan" maxlength="35" placeholder="Masukkan nip/nrp/nik" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->nip_nrp_nik : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label for="nama_hrd_pasangan">Nama HRD <i class="text-danger">*</i></label>
                                                    <input class="form-control checkKarakterAneh" type="text" id="nama_hrd_pasangan" maxlength="35" name="nama_hrd_pasangan" placeholder="Masukkan nama hrd" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->nama_hrd : ''}}" required>
                                                </div>
                                            </div>
                                            {{-- END: Baris 16 --}}

                                            {{-- START: Baris 17 --}}
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label for="no_hrd_pasangan" class="ml-0">Nomor Fixed Line HRD
                                                        <i class="text-danger">*</i></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text input-group-text-dsi"> +62
                                                            </span>
                                                        </div>
                                                        <input class="form-control no-zero" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" id="no_hrd_pasangan" name="no_hrd_pasangan" placeholder="Masukkan nomor fixed line hrd" value="{{ $data_penghasilan_pasangan ? substr($data_penghasilan_pasangan->no_fixed_line_hrd, 2) : ''}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 ">
                                                <div class="form-group">
                                                    <label id="label_no_telpon_usaha_pasangan" for="no_telpon_usaha_pasangan" class="ml-0">No. Telepon
                                                        Perusahaan <i class="text-danger">*</i></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text input-group-text-dsi"> +62
                                                            </span>
                                                        </div>
                                                        <input class="form-control no-zero" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" id="no_telpon_usaha_pasangan" name="no_telpon_usaha_pasangan" placeholder="Masukkan nomor telpon perusahaan" value="{{ $data_penghasilan_pasangan ? substr($data_penghasilan_pasangan->no_telp_pekerjaan_pasangan, 2) : ''}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 non-fixed-pasangan d-none">
                                                <div class="form-group">
                                                    <label for="no_hp_usaha_pasangan" class="ml-0">No. HP
                                                        Usaha/Praktek
                                                        <i class="text-danger">*</i></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text input-group-text-dsi"> +62
                                                            </span>
                                                        </div>
                                                        <input class="form-control no-zero eight" type="text" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" id="no_hp_usaha_pasangan" name="no_hp_usaha_pasangan" placeholder="Masukkan no. hp Usaha" value="{{ $data_penghasilan_pasangan ? substr($data_penghasilan_pasangan->no_hp_pekerjaan_pasangan, 2) : ''}}" required>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- END: Baris 17 --}}
                                        </div>



                                        {{-- START: baris 18 --}}
                                        <div class="row non-fixed-pasangan d-none">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="label_surat_ijin_usaha_pasangan" for="surat_ijin_usaha_pasangan" class="ml-0">Surat Ijin
                                                        Usaha/Praktek
                                                        <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="surat_ijin_usaha_pasangan" name="surat_ijin_usaha_pasangan" required>
                                                        <option value="">-- Pilih Satu --</option>
                                                        <option value="1">Ya</option>
                                                        <option value="2">Tidak</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="label_no_ijin_usaha_pasangan" for="no_ijin_usaha_pasangan" class="ml-0">Nomor Ijin
                                                        Usaha/Praktek <i class="text-danger">*</i></label>
                                                    <input class="form-control form-control-lg" id="no_ijin_usaha_pasangan" name="no_ijin_usaha_pasangan" type="text" placeholder="Masukkan no ijin usaha" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->no_surat_ijin : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="lama_tempat_usaha_pasangan" class="ml-0">Lama Tempat
                                                        Usaha/Praktek (Tahun) <i class="text-danger">*</i></label>
                                                    <input class="form-control form-control-lg" id="lama_tempat_usaha_pasangan" type="number" name="lama_tempat_usaha_pasangan" maxlength="5" placeholder="Masukkan lama tempat usaha" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->usia_tempat_usaha : ''}}" required>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END: baris 18 --}}

                                        {{-- START: Baris 19 --}}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="label_alamat_perusahaan_pasangan" for="alamat_perusahaan_pasangan" class="ml-0">Alamat
                                                        Perusahaan/Tempat Usaha <i class="text-danger">*</i> </label>
                                                    <textarea class="form-control form-control-lg" maxlength="90" id="alamat_perusahaan_pasangan" name="alamat_perusahaan_pasangan" rows="6" placeholder="Masukkan alamat perusahaan" required>{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->alamat_perusahaan : ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="rt_perusahaan_pasangan" class="ml-0">RT <i class="text-danger">*</i></label>
                                                    <input class="form-control form-control-lg" maxlength="5" id="rt_perusahaan_pasangan" name="rt_perusahaan_pasangan" rows="6" placeholder="Masukkan rt" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->rt_pekerjaan_pasangan : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="rw_perusahaan_pasangan" class="ml-0">RW <i class="text-danger">*</i></label>
                                                    <input class="form-control form-control-lg" maxlength="5" id="rw_perusahaan_pasangan" name="rw_perusahaan_pasangan" rows="6" placeholder="Masukkan rw" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->rw_pekerjaan_pasangan : ''}}" required>
                                                </div>
                                            </div>

                                        </div>
                                        {{-- END: Baris 19 --}}

                                        {{-- START: Baris 20 --}}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="provinsi_perusahaan_pasangan" class="ml-0">Provinsi <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="provinsi_perusahaan_pasangan" name="provinsi_perusahaan_pasangan" onchange="provinsiChange(this.value, this.id, 'kota_perusahaan_pasangan')" required>
                                                        <option value="">-- Pilih Satu --</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kota_perusahaan_pasangan" class="ml-0">Kota/Kabupaten <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kota_perusahaan_pasangan" name="kota_perusahaan_pasangan" onchange="kotaChange(this.value, this.id, 'kecamatan_perusahaan_pasangan')" required>
                                                        <option value="">-- Pilih Satu --
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kecamatan_perusahaan_pasangan" class="ml-0">Kecamatan <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kecamatan_perusahaan_pasangan" name="kecamatan_perusahaan_pasangan" onchange="kecamatanChange(this.value, this.id, 'kelurahan_perusahaan_pasangan')" required>
                                                        <option value="">Pilih Kecamatan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END: Baris 20 --}}

                                        {{-- START: Baris 21 --}}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kelurahan_perusahaan_pasangan" class="ml-0">Kelurahan <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kelurahan_perusahaan_pasangan" name="kelurahan_perusahaan_pasangan" onchange="kelurahanChange(this.value, this.id, 'kode_pos_perusahaan_pasangan',$('#kecamatan_perusahaan_pasangan').val(),$('#kota_perusahaan_pasangan').val(),$('#provinsi_perusahaan_pasangan').val())" required>
                                                        <option value="">Pilih Kelurahan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kode_pos_perusahaan_pasangan" class="ml-0">Kode
                                                        Pos <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" maxlength="30" id="kode_pos_perusahaan_pasangan" name="kode_pos_perusahaan_pasangan" placeholder="--" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->kode_pos : ''}}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END: Baris 21 --}}

                                        {{-- START: Baris 22 --}}
                                        <div class="row">
                                            <div class="col-md-4 fixed-income-pasangan">
                                                <div class="form-group">
                                                    <label class="ml-0">Pengalaman Kerja Di Tempat Lain <i class="text-danger">*</i></label>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">Tahun</span>
                                                                </div>
                                                                <input type="number" id="tahun_bekerja_ditempat_lain_pasangan" name="tahun_bekerja_ditempat_lain_pasangan" class="form-control" aria-label="tahun_bekerja_ditempat_lain_pasangan" aria-describedby="basic-addon1" min="0" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->pengalaman_kerja_tahun : '' }}" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="input-group mb-3">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text" id="basic-addon1">Bulan</span>
                                                                </div>
                                                                <input type="number" id="bulan_bekerja_ditempat_lain_pasangan" name="bulan_bekerja_ditempat_lain_pasangan" class="form-control" aria-label="bulan_bekerja_ditempat_lain" aria-describedby="basic-addon1" min="0" max="11" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->pengalaman_kerja_bulan : '' }}" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="penghasilan_pasangan" class="ml-0">Penghasilan (Per
                                                        Bulan)
                                                        <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="penghasilan_pasangan" name="penghasilan_pasangan" placeholder="Masukkan penghasilan anda" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" value="{{ $data_penghasilan_pasangan ? (int)$data_penghasilan_pasangan->pendapatan_borrower : '' }}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="biaya_hidup_pasangan" class="ml-0">Biaya Hidup (Per
                                                        Bulan)
                                                        <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="biaya_hidup_pasangan" name="biaya_hidup_pasangan" placeholder="Masukkan biaya hidup anda" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" value="{{ $data_penghasilan_pasangan ? (int)$data_penghasilan_pasangan->biaya_hidup : '' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END: Baris 22 --}}

                                        {{-- START: Baris 23 --}}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="label_nilai_spt_pasangan" for="nilai_spt_pasangan" class="ml-0">Nilai SPT Tahun Terakhir<i class="text-danger">*</i></label>
                                                    <input class="form-control form-control-lg" maxlength="90" id="nilai_spt_pasangan" name="nilai_spt_pasangan" placeholder="Masukkan nilai spt " onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" value="{{ $data_penghasilan_pasangan ? (int)$data_penghasilan_pasangan->nilai_spt : '' }}" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 mb-4 pb-4">
                                                <div class="form-check form-check-inline line">
                                                    <input class="form-check-input" type="checkbox" name="penghasilan_lain_lain_pasangan" id="penghasilan_lain_lain_pasangan" value="1">
                                                    <label class="form-check-label text-black h6" for="penghasilan_lain_lain_pasangan">Saya
                                                        Memiliki
                                                        Penghasilan Lain
                                                        Lain &nbsp</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="more-penghasilan-lain-lain-pasangan" class="d-none">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="detail_penghasilan_lain_lain_pasangan" class="ml-0">Detail
                                                            Penghasilan Lain Lain <i class="text-danger">*</i></label>
                                                        <textarea class="form-control form-control-lg" maxlength="90" id="detail_penghasilan_lain_lain_pasangan" name="detail_penghasilan_lain_lain_pasangan" rows="6" placeholder="Contoh: Jualan bakso, jualan jus buah, ngojek" required>{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->detail_penghasilan_lain_lain : '' }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="total_penghasilan_lain_lain_pasangan" class="ml-0">Total
                                                            Penghasilan Lain Lain (Per Bulan) <i class="text-danger">*</i></label>
                                                        <input class="form-control form-control-lg" maxlength="90" id="total_penghasilan_lain_lain_pasangan" name="total_penghasilan_lain_lain_pasangan" placeholder="Masukkan total penghasilan lain lain" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" value="{{ $data_penghasilan_pasangan ? (int)$data_penghasilan_pasangan->total_penghasilan_lain_lain : '' }}" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END: Baris 23 --}}
                                    </div>
                                    {{-- END: Detail Penghasilan --}}

                                    {{-- START: Penghasilan Lain Lain --}}
                                    {{-- START: Baris 8 --}}
                                    <div id="detail-penghasilan-lain-lain-pasangan" class="d-none">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="detail_pekerjaan_pasangan" class="ml-0">Detail Pekerjaan
                                                        <i class="text-danger">*</i></label>
                                                    <textarea class="form-control form-control-lg" maxlength="90" id="detail_pekerjaan_pasangan" name="detail_penghasilan_lain_lain_pasangan2" rows="6" placeholder="Contoh: Jualan bakso, jualan jus buah, ngojek" required>{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->detail_penghasilan_lain_lain : ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="penghasilan_pasangan2" class="ml-0">Penghasilan (Per
                                                        Bulan)
                                                        <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="penghasilan_pasangan2" name="penghasilan_pasangan2" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" placeholder="Masukkan penghasilan anda" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->pendapatan_borrower : '' }}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="biaya_hidup_pasangan2" class="ml-0">Biaya Hidup (Per
                                                        Bulan)
                                                        <i class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="biaya_hidup_pasangan2" name="biaya_hidup_pasangan2" placeholder="Masukkan biaya hidup anda" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')" maxlength="13" value="{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->biaya_hidup : '' }}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- END: Baris 8 --}}
                                    {{-- END: Penghasilan Lain Lain --}}
                                </div>
                            </div>
                        </div>

                        <div id="chk-agreement">
                            <div class="row mt-4 mb-4 px-3">
                                <div class="col-12">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" name="chk_agreement" id="chk_agreement" value="1">
                                        <label class="form-check-label text-secondary h6" for="chk_agreement">Saya telah
                                            membaca
                                            dan menyetujui <a href="#agreement" data-toggle="modal" data-target="#agreement" class="text-success">Syarat dan Ketentuan</a>
                                            yang
                                            berlaku, dan
                                            dengan ini menyatakan bahwa data yang dikirim adalah benar dan menyetujui
                                            pelaksanaan proses selanjutnya oleh PT. Dana Syariah Indonesia &nbsp</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="agreement" class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header" style="border-bottom-width: 1px; border-bottom-color: #e6ecec">
                                        <p class="modal-title h4" id="exampleModalLongTitle">Syarat dan Ketentuan
                                            Pengisian Aplikasi Pengajuan Pinjaman</p>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="h5 font-weight-normal">
                                            Sehubungan dengan pengajuan pinjaman kepada PT. Dana Syariah Indonesia,
                                            dengan ini menyatakan sebagai berikut : <br><br>

                                            1. Memberikan persetujuan dan kuasa kepada PT. Dana Syariah Indonesia untuk
                                            memperoleh referensi dari sumber manapun dan
                                            dengan cara yang dianggap layak oleh PT. Dana Syariah Indonesia.<br><br>
                                            2. Memberikan persetujuan dan kuasa kepada PT. Dana Syariah Indonesia untuk
                                            memberikan data kepada pihak ketiga untuk
                                            kebutuhan risk assessment (penilaian resiko).<br><br>
                                            3. PT. Dana Syariah Indonesia berhak untuk menolak permohonan dengan tanpa
                                            harus/kewajiban untuk menunjukkan
                                            alasan-alasan penolakan.<br><br>
                                            4. Menyampaikan dokumen sesuai dengan yang disyaratkan oleh PT. Dana Syariah
                                            Indonesia.<br><br>
                                            5. Semua dokumen pendukung yang telah diserahkan kepada PT. Dana Syariah
                                            Indonesia tidak akan ditarik kembali.<br><br>
                                            6. Memberikan informasi terbaru apabila ada perubahan data dalam aplikasi
                                            ini sehubungan dengan permohonan kredit
                                            tersebut.<br><br>
                                            7. Apabila permohonan disetujui, akan tunduk dan terikat pada ketentuan dan
                                            syarat-syarat kemudian yang dikeluarkan oleh
                                            PT. Dana Syariah Indonesia.<br><br>
                                            8. Pembiayaan tidak digunakan untuk kegiatan yang bertentangan dengan
                                            prinsip Syariah dan kegiatan usaha yang dilarang
                                            dan bertentangan dengan Undang-undang yang berlaku yang meliputi tetapi
                                            tidak terbatas pada: tindak pidana pencucian
                                            uang (TPPU), Pendanaan kegiatan yang terkait dengan tindakan terorisme
                                            (TPPT), Penipuan, Prostitusi, perdagangan
                                            obat-obat psikotrapis (narkotik dan jenis obat terlarang lainnya). <br><br>

                                            <b>"Sesuai dengan NOMOR 77 /POJK.01/2016 bahwa batas maksimum total
                                                pemberian pinjaman dana kepada setiap peminjam dana
                                                adalah sebesar Rp. 2.000.000.000,00 (dua miliar rupiah)."</b>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-noborder btn-success btn-lg p-2 px-3 my-3" data-dismiss="modal">Setuju</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Content --}}
                </div>
            </div>
        </div>
        <input type="submit" id="submitindividu" class="d-none" value="submit">
    </form>
</div>