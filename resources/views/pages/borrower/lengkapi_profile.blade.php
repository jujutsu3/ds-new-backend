@extends('layouts.borrower.master')
@section('title', 'Welcome Borrower')

@section('style')
<link href="{{ asset('vendor/smartWizard/smart_wizard_all.min.css') }}" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
    type="text/css" rel="stylesheet" />
{{-- START: Style --}}
<style>
    .mandatory_label {
    color: red;
    }
    .line {
        display: flex;
        flex-direction: row;
    }

    .line:after {
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #000;
        margin: auto;
    }

    .input-hidden {
        height: 0;
        width: 0;
        visibility: hidden;
        padding: 0;
        margin: 0;
        float: right;
    }

    .input-hidden2 {
        opacity: 0;
        height: 0;
        width: 0;
        padding: 0;
        margin: 0;
        float: right;
    }

    .green-dsi {
        background-color: #057758;
    }

    .error {
        color: #ff0000;
    }

    .sweet_loader {
        width: 140px;
        height: 140px;
        margin: 0 auto;
        animation-duration: 0.5s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        animation-name: ro;
        transform-origin: 50% 50%;
        transform: rotate(0) translate(0, 0);
    }

    .radio-group {
        position: relative;
    }

    #status_rumah-error,
    #domisili_status_rumah-error,
    #no_hrd-error,
    #no_hrd_pasangan-error,
    #telepon-error,
    #telepon_pasangan-error,
    #no_telpon_usaha-error,
    #no_telpon_usaha_pasangan-error,
    #no_hp_usaha-error,
    #no_hp_usaha_pasangan-error,
    #gender-error,
    #jns_kelamin_pasangan-error {
        position: absolute;
        top: 100%;
    }

    #bank-error {
        position: absolute;
        top: 50%;
        left: 10px;
    }

    #status_kawin-error {
        position: absolute;
        top: 200%;
    }

    @keyframes ro {
        100% {
            transform: rotate(-360deg) translate(0, 0);
        }
    }
</style>
{{-- END: Style --}}
@endsection

@section('content')
<!-- START: Main Container -->
<main id="main-container">
    <!-- START: Page Content -->
    <div id="detect-screen" class="content-full-right">
        <div class="container">
            <div class="row mb-4">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h3 class="no-paddingTop font-w400" style="float: left; margin-block-end: 0em; color: #31394D">
                            Form Pendaftaran Penerima Pendanaan</h3>
                    </span>
                </div>
            </div>

            {{-- START: Individu --}}
            <div id="layout-individu" class="row mt-5 pt-5 d-none">
                @include('pages.borrower.lengkapi_profile_individu')
            </div>
            {{-- END: Individu --}}
            {{-- START: Badan Hukum --}}
            <div id="layout-badan-hukum" class="row mt-5 pt-5 d-none">
                @include('pages.borrower.lengkapi_profile_badan_hukum')
            </div>
            {{-- END: Badan Hukum --}}
        </div>
    </div>
    <!-- END: Page Content -->

    {{-- START: Modal OTP --}}
    <div class="modal fade" id="otp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
        data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header mb-3">
                    <h5 class="modal-title" id="scrollmodalLabel">Verifikasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="resetTimeout()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-success col-sm-12" id="alertError" style="display: none;">
                </div>
                <div class="modal-body">
                    <form id="form_otp">
                        @csrf

                        <div class="form-group">
                            <label class="col-sm-12" id="set_no_hp" style="font-size:16px;"></label>
                        </div>

                        <div class="form-group row ml-1">
                            <label class="col-sm-3 col-form-label">No HP</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control col-sm-10" value="" name="no_hp" id="no_hp"
                                    disabled>
                            </div>
                        </div>
                        <div class="form-group row ml-1">
                            <label class="col-sm-3 col-form-label">Kode OTP</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control col-sm-6" value="" name="kode_otp" id="kode_otp">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="kirim_lagi">Kirim Lagi <span
                            id="count"></span></button>
                    <button type="button" class="btn btn-success" id="kirim_data" disabled>Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    {{-- END: Modal OTP --}}

    <!-- START: Modal Confirmation -->
    <div class="modal fade" id="modal_action_lengkapi_profile" tabindex="-1" role="dialog" aria-labelledby="modal-popin"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Simpan Data</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Anda yakin ingin menyimpan data ini ?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cek Lagi</button>
                    <button type="button" id="btn_proses_lengkapi_profile" class="btn btn-alt-success"
                        data-dismiss="modal" data-toggle="modal" data-target="#otp">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Modal Confirmation -->
</main>
<!-- END: Main Container -->
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script src="{{ asset('vendor/credoApp/credoappsdk.js') }}"></script>
<script src="{{ asset('vendor/smartWizard/jquery.smartWizard.min.js') }}"></script>

{{-- START: Init --}}
<script>
    let a = $('#smartwizard')
    let timer = null; // For referencing the timer

    // START: Unique Field
    let is_phone_number_unique = 0
    let is_nik_unique = 0
    let is_npwp_unique = 0

    if ("{{ $data_individu ? $data_individu->no_tlp : '' }}") {
        is_phone_number_unique = 1
    }

    if ("{{ $data_individu ? $data_individu->ktp : '' }}") {
        is_nik_unique = 1
    }

    if ("{{ $data_individu ? $data_individu->npwp : '' }}") {
        is_npwp_unique = 1
    }
    // END: Unique Field

    Webcam.set({
        width:180,
        height:200,
        image_format: 'jpg',
        jpeg_quality: 90,
        flip_horiz: false
    });

    let brw_type = "{{ $data_individu ? $data_individu->brw_type : ''}}"

    if (!brw_type) {
        swal.fire({
            title: "Lengkapi Profile",
            text: "Mendaftar sebagai ?",
            type : "question",
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: "Individu",
            cancelButtonText: "Perusahaan",
            confirmButtonColor: "#70B29C",
            cancelButtonColor: "#FFCA28",
        }).then( function (response) {
            if (response.value){
                $('#borrower_type').val("1");
                $('#label_tipe_borrower').text('Individu');
                $('#layout-individu').removeClass('d-none');
                $('#layout-badan-hukum').addClass('d-none');
            } else {
                $('#borrower_type').val("2");
                $('#label_tipe_borrower').text('Perusahaan');
                $('#layout-badan-hukum').removeClass('d-none');
                $('#layout-individu').addClass('d-none');
            }
        });
    } else {
        if (brw_type == 1) {
            $('#borrower_type').val("1");
            $('#label_tipe_borrower').text('Individu');
            $('#layout-individu').removeClass('d-none');
            $('#layout-badan-hukum').addClass('d-none');
            
        } else {
            $('#borrower_type').val("2");
            $('#label_tipe_borrower').text('Perusahaan');
            $('#layout-badan-hukum').removeClass('d-none');
            $('#layout-individu').addClass('d-none');
        }
    }
    
</script>
{{-- End: Init --}}

<!-- START: CredoApp -->
<script>
    var authKey = "{{ config('app.credo_auth_key') }}";
    var url = "{{ config('app.credo_url') }}";
    var recordNumber = "{{ Auth::guard('borrower')->user()->brw_id }}";
    var credoAppService = new credoappsdk.CredoAppService(url, authKey);
    credoAppService.startEventTracking();
    
    function collectData() {       
        credoAppService.stopEventTracking();
            credoAppService.collectAsync(recordNumber).then((recordNumber) => {
        });
    }
</script>
<!-- END: CredoApp -->

{{-- START: Camera --}}
<script>
    // Camera Click Action
    const btnCameraClick = (cameraAttachId, previewCameraId, takeCameraId) => {
        Webcam.attach('#'+cameraAttachId);
        $('#'+previewCameraId).addClass('d-none');
        $('#'+takeCameraId).removeClass('d-none');
    }

    // Take Snapshot Camera
    const takeSnapshot = (resultCamera, userCamera, userCameraVal, result) => {
        $('#'+resultCamera).removeClass('d-none');
        Webcam.snap( function(data_uri) {
            let filename = ''
            let msgName = ''
            if (userCamera == 'user_camera_diri') {
                filename = 'pic_brw'
                msgName = 'Diri'
            } else if (userCamera == 'user_camera_ktp') {
                filename = 'pic_brw_ktp'
                msgName = 'KTP'
            } else if (userCamera == 'user_camera_diri_dan_ktp') {
                filename = 'pic_brw_dan_ktp'
                msgName = 'Diri & KTP'
            } else if (userCamera == 'user_camera_npwp') {
                filename = 'pic_brw_npwp'
                msgName = 'NPWP'
            }

            $('#'+userCamera).val(data_uri);
            $('#'+userCameraVal).val('1');
            $('#'+result).html(`
                <img class="img-thumbnail" src="${data_uri}" style="width: 180px; height: 138px;" />
            `);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            $.ajax({

                url : "/borrower/upload_foto",
                method : "POST",
                dataType: 'JSON',
                data: {'img': data_uri, 'file_name': filename},
                success:function(data)
                {
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto '+msgName+' Berhasil',
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#'+userCamera).val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        });
    }
</script>
{{-- END: Camera --}}

{{-- START: Get Json Data --}}
<script>
    $(document).ready(function() {
         $('#txt_provinsi_pengurus, #txt_provinsi_pengurus_2').prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%'
        });
        $.getJSON("/borrower/data_provinsi/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#provinsi').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
                $('#provinsi_domisili').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
                $('#provinsi_perusahaan').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));

                $('#provinsi_perusahaan_pasangan').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
                $('#provinsi_pasangan').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));

                // START: Badan Hukum
                $('#txt_provinsi_badan_hukum').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
                $('#txt_provinsi_domisili_badan_hukum').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
                $('#txt_provinsi_pengurus').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
                $('#txt_provinsi_pengurus_2').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
                // END: Badan Hukum
            }

            let provinsi = "{{ $data_individu ? $data_individu->provinsi : ''}}"
            $("#provinsi option[value='"+provinsi+"']").prop('selected', true);

            let provinsi_domisili = "{{ $data_individu ? $data_individu->domisili_provinsi : ''}}"
            $("#provinsi_domisili option[value='"+provinsi_domisili+"']").prop('selected', true);

            let provinsi_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->provinsi : ''}}"
            $("#provinsi_perusahaan option[value='"+provinsi_perusahaan+"']").prop('selected', true);

            let provinsi_pasangan = "{{ $data_pasangan ? $data_pasangan->provinsi : ''}}"
            $("#provinsi_pasangan option[value='"+provinsi_pasangan+"']").prop('selected', true);

            let provinsi_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->provinsi : ''}}"
            $("#provinsi_perusahaan_pasangan option[value='"+provinsi_perusahaan_pasangan+"']").prop('selected', true);
        });

        $.getJSON("/borrower/agama/", function(data){
            for(var i = 0; i<data.length; i++){
                $('#agama').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text
                }));
                $('#agama_pasangan').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text
                }));
            }

            let agama = "{{ $data_individu ? $data_individu->agama : ''}}"
            $("#agama option[value='"+agama+"']").prop('selected', true);

            let agama_pasangan = "{{ $data_pasangan ? $data_pasangan->agama : ''}}"
            $("#agama_pasangan option[value='"+agama_pasangan+"']").prop('selected', true);
        })

        $.getJSON("/borrower/data_pendidikan/", function(data){
            for(var i = 0; i<data.length; i++){
                $('#pendidikan_terakhir').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text
                }));
                $('#pendidikan_terakhir_pasangan').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text
                }));
            }

            let pendidikan_terakhir = "{{ $data_individu ? $data_individu->pendidikan_terakhir : ''}}"
            $("#pendidikan_terakhir option[value='"+pendidikan_terakhir+"']").prop('selected', true);

            let pendidikan_terakhir_pasangan = "{{ $data_pasangan ? $data_pasangan->pendidikan : ''}}"
            $("#pendidikan_terakhir_pasangan option[value='"+pendidikan_terakhir_pasangan+"']").prop('selected', true);
        });

        $('#bank, #txt_bank_pribadi').prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%'
        });

        $.getJSON("/borrower/data_bank/", function(data){
            let bank = "{{ $data_rekening ? $data_rekening->brw_kd_bank : ''}}";
            for(let i = 0; i<data.length; i++){
                $('#bank').append($('<option>', {
                    value: data[i].id,
                    text : data[i].text
                }));
                $('#txt_bank_pribadi').append($('<option>', {
                    value: data[i].id,
                    text : data[i].text
                }));
            }
            if (bank) {
                $('#bank').val(bank);
                $('#bank').select2().trigger('change');
            }
        });

        $.getJSON("/borrower/data_pekerjaan/", function(data){
            for(var i = 0; i<data.length; i++){
                let id = data[i].id
                if (id == 6 || id == 7 || id == 8) {
                    // Remove
                } else {
                    $('#pekerjaan').append($('<option>', { 
                        value: data[i].id,
                        text : data[i].text   
                    }));

                    $('#pekerjaan_pasangan').append($('<option>', {
                        value: data[i].id,
                        text : data[i].text
                    }));
                }
            }

            let pekerjaan = "{{ $data_individu ? $data_individu->pekerjaan : ''}}"
            if (pekerjaan) {
                $("#pekerjaan option[value='"+pekerjaan+"']").prop('selected', true);
                $('#pekerjaan').trigger("change")
            }

            let pekerjaan_pasangan = "{{ $data_individu ? $data_individu->pekerjaan_pasangan : ''}}"
            if (pekerjaan_pasangan) {
                $("#pekerjaan_pasangan option[value='"+pekerjaan_pasangan+"']").prop('selected', true);
                $('#pekerjaan_pasangan').trigger("change")
            }
        });

        $.getJSON("/borrower/data_bidang_pekerjaan/", function(data){
            for(var i = 0; i<data.length; i++){
                let id = data[i].id
                if (id == 19 || id == 20) {
                    //Remove
                } else {
                    $('#bidang_pekerjaan').append($('<option>', { 
                        value: data[i].id,
                        text : data[i].text
                    }));
                    $('#bidang_pekerjaan_pasangan').append($('<option>', { 
                        value: data[i].id,
                        text : data[i].text
                    }));
                }
            }

            let bidang_pekerjaan = "{{ $data_individu ? $data_individu->bidang_pekerjaan : ''}}"
            $("#bidang_pekerjaan option[value='"+bidang_pekerjaan+"']").prop('selected', true);

            let bidang_pekerjaan_pasangan = "{{ $data_individu ? $data_individu->bd_pekerjaan_pasangan : ''}}"
            $("#bidang_pekerjaan_pasangan option[value='"+bidang_pekerjaan_pasangan+"']").prop('selected', true);
        });

        $.getJSON("/borrower/bidang_pekerjaan_online/", function(data){
            for(var i = 0; i<data.length; i++){
                $('#bidang_online').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text 
                }));
                $('#bidang_online_pasangan').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text 
                }));
            }

            let bidang_online = "{{ $data_individu ? $data_individu->bidang_online : ''}}"
            $("#bidang_online option[value='"+bidang_online+"']").prop('selected', true);

            let bidang_online_pasangan = "{{ $data_individu ? $data_individu->bd_pekerjaanO_pasangan : ''}}"
            $("#bidang_online_pasangan option[value='"+bidang_online_pasangan+"']").prop('selected', true);
        });

        $.getJSON("/borrower/bentuk_badan_usaha/", function(data){
            for(var i = 0; i<data.length; i++){
                $('#badan_usaha').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text 
                }));

                $('#badan_usaha_pasangan').append($('<option>', { 
                    value: data[i].id,
                    text : data[i].text 
                }));
            }

            let badan_usaha = "{{ $data_penghasilan ? $data_penghasilan->bentuk_badan_usaha : ''}}"
            $("#badan_usaha option[value='"+badan_usaha+"']").prop('selected', true);
            
            let badan_usaha_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->bentuk_badan_usaha : ''}}"
            $("#badan_usaha_pasangan option[value='"+badan_usaha_pasangan+"']").prop('selected', true);
        });
    });

    $(`#txt_provinsi_badan_hukum, #txt_kota_badan_hukum, #txt_kecamatan_badan_hukum, #txt_kelurahan_badan_hukum, 
    #txt_provinsi_domisili_badan_hukum, #txt_kota_domisili_badan_hukum, #txt_kecamatan_domisili_badan_hukum, #txt_kelurahan_domisili_badan_hukum
    `).prepend('<option selected></option>').select2({
		theme: "bootstrap",
		placeholder: "-- Pilih Satu --",
		allowClear: true,
		width: '100%'
	});
    
    const provinsiChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kota/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kotaChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kecamatan/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kecamatanChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kelurahan/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kelurahanChange = (thisValue, thisId, nextId,vkec,vkota,vprov) => {
        // $('#'+nextId).val(thisValue)
        $.getJSON("/borrower/data_kode_pos_v2/"+thisValue+"/"+vkec+"/"+vkota+"/"+vprov, function(data){
            $('#'+nextId).val(data[0].text)
        });
    }
</script>
{{-- END: Get Json Data --}}

{{-- START: Smartwizard --}}
<script>
    $("#form-individu").validate({
        onfocusout: false,
        invalidHandler: function(e, validator){
        if(validator.errorList.length){
            validator.errorList[0].element.focus()
        }
        }
    });

    // START: Individu
    $('#smartwizard').smartWizard({
        theme: 'dots',
        enableURLhash: false,
        autoAdjustHeight: false,
        enableAllSteps:true,
        enableFinishButton: true,
        hideButtonsOnDisabled: true,
        toolbarSettings: {
            toolbarExtraButtons: [
                $('<button type="button" id="finish" class="d-none" disabled></button>').text('Selesai')
                    .addClass('btn btn-rounded btn-big btn-noborder btn-success btn-lg p-2 px-3 pull-right sw-btn-group-extra')
                    .on('click', function(){ 
                        onFinishCallback()                            
                    }),
            ]
        },
        keyboardSettings: {
            keyNavigation: false,
        },
        lang: {
            next: 'Selanjutnya',
            previous: 'Kembali',
            finish: 'selesai'
        },
        hiddenSteps: [1],
    });

    $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
        $('#nama_pasangan').focus()
        $('#nama_individu').focus()
        if($('button.btn.sw-btn-prev').hasClass('disabled')){
            $('.sw-btn-group-extra').removeClass('d-none');
            $('.sw-btn-next').addClass('d-none');
            $('#chk-agreement').removeClass('d-none')
        }else{
            $('.sw-btn-group-extra').addClass('d-none');
            $('.sw-btn-next').removeClass('d-none');
            $('#chk-agreement').addClass('d-none')
        }

    });
    
    $("#smartwizard").on("leaveStep", function(e, anchorObject, currentStepIndex, nextStepIndex, stepDirection) {
        if (stepDirection === 'backward') {
            $('input[name="status"]').val('1')
            return true
        } else {
            if ($("#form-individu").valid()) {
                if (currentStepIndex == 0) {
                    if (is_nik_unique == 1) {
                        if (is_phone_number_unique == 1) {
                            if (is_npwp_unique == 1) {
                                $('#submitindividu').click()
                            } else {
                                $('#npwp').focus()
                                return false
                            }
                        } else {
                            $('#telepon').focus()
                            return false
                        }
                    } else {
                        $('#ktp').focus()
                        return false
                    }
                }
                $('input[name="status"]').val('2')
                return true
            } else {
                return false
            }
        }
    });
    // END: Individu
</script>
{{-- END: Smartwizard --}}

{{-- START: Dom Manipulation --}}
<script>
    $(document).ready(function() {

        $('.sw-btn-next').addClass('d-none btn btn-rounded btn-big btn-noborder btn-success btn-lg p-2 px-3 pull-right');
        $('.sw-btn-prev').addClass('btn btn-rounded btn-big btn-noborder btn-success btn-lg p-2 px-3 pull-left');
        $('.sw-btn-group-extra').removeClass('d-none');

        // START: Individu

        // Check Agreement
        $('#chk_agreement').change(function () {
            let chk_agreement = $("input[name='chk_agreement']:checked").val()
            if (chk_agreement == '1') {
                $('#finish').prop('disabled', false)
            } else {
                $('#finish').prop('disabled', true)
            }
        })
        // Validasi No HP
        $(".no-zero").on("input", function(){
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });

        $(".no-four").on("input", function(){
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '4') {
                $(this).val(thisValue.substring(1))
            }
        });

        $(".eight").on("input", function(){
            let thisValue = $(this).val()
            if (thisValue.charAt(0) != '8') {
                $(this).val(thisValue.substring(1))
            }
        });

        // Check No Hp
        $('#telepon').on('keyup',function(){
            let no_hp_individu = $(this).val()
            if (no_hp_individu.length >=9) {
                $.ajax({
                    url: "{{ url('borrower/check_no_tlp') }}"+"/"+no_hp_individu,
                    method: "GET",
                    success: (response) => {
                        if(response.status == 'ada') {
                            $('#label_telepon').text('').append('<i class="text-danger">Nomor sudah terdaftar</i>')
                            is_phone_number_unique = 0
                        } else {
                            $('#label_telepon').text('').append('No. Hp<i class="text-danger">*</i>')
                            is_phone_number_unique = 1
                        }
                    }, 
                    error: (response) => {
                        console.log(response)
                    }
                })
            }
        })

        // Check NIK
        $('#ktp').on('keyup', function() {
            let no_ktp = $(this).val()
            if (no_ktp.length == 16) {
                $.ajax({
                    url: "{{ url('borrower/check_nik') }}"+"/"+no_ktp,
                    method: "GET",
                    success: (response) => {
                        if(response.status == 'ada') {
                            $('#label_ktp').text('').append('<i class="text-danger">No. KTP sudah terdaftar</i>')
                            is_nik_unique = 0
                        } else {
                            $('#label_ktp').text('').append('No. KTP<i class="text-danger">*</i>')
                            is_nik_unique = 1
                        }
                    }, 
                    error: (response) => {
                        console.log(response)
                    }
                })
            }
        })

        // Check NPWP
        $('#npwp').on('keyup', function() {
            let npwp = $(this).val()
            let brw_type = $('#borrower_type').val()
            if (npwp.length == 15) {
                $.ajax({
                    url: "{{ url('borrower/check_npwp') }}"+"/"+npwp+"/"+brw_type,
                    method: "GET",
                    success: (response) => {
                        if(response.status == 'ada') {
                            $('#label_npwp').text('').append('<i class="text-danger">No. NPWP sudah terdaftar</i>')
                            is_npwp_unique = 0
                        } else {
                            $('#label_npwp').text('').append('No. NPWP<i class="text-danger">*</i>')
                            is_npwp_unique = 1
                        }
                    }, 
                    error: (response) => {
                        console.log(response)
                    }
                })
            }
        })

        $('#domisili_status').change(() => {
            if($('#domisili_status').is(':checked')){
                $('#domisili_status').val("1");
                // $("#layout-alamat-domisili").addClass('d-none').find('input, select, textarea, radio').prop("disabled", true);

                let alamat = $("#alamat").val();
                let provinsi = $("#provinsi option:selected").val();
                let kota = $("#kota option:selected").val();
                let kecamatan = $("#kecamatan option:selected").val();
                let kelurahan = $("#kelurahan option:selected").val();
                let kode_pos = $("#kode_pos").val();
                let status_rumah = $("input[name=status_rumah]:checked").val();

                console.log(alamat)
                
                // appen value
                $('#alamat_domisili').text(alamat);
                $('#provinsi_domisili').empty()
                $('#provinsi_domisili').append($('<option>', { value: provinsi, text : provinsi}));
                $('#kota_domisili').append($('<option>', { value: kota, text : kota}));
                $("#kecamatan_domisili").append($('<option>', { value: kecamatan, text : kecamatan}));
                $("#kelurahan_domisili").append($('<option>', { value: kelurahan, text : kelurahan}));
                $("#kode_pos_domisili").val(kode_pos);

                // Set selected
                $("#provinsi_domisili option[value='"+provinsi+"']").attr({selected:'selected'})
                $("#kota_domisili option[value='"+kota+"']").attr({selected:'selected'})
                $("#kecamatan_domisili option[value='"+kecamatan+"']").attr({selected:'selected'})
                $("#kelurahan_domisili option[value='"+kelurahan+"']").attr({selected:'selected'})
                $('input[name="domisili_status_rumah"][value="'+status_rumah+'"]').prop('checked', true);

                // Add readonly
                $('#alamat_domisili').attr('disabled', true);
                $("#provinsi_domisili").attr('disabled', true);
                $("#kota_domisili").attr('disabled', true);
                $("#kecamatan_domisili").attr('disabled', true);
                $("#kelurahan_domisili").attr('disabled', true);
                $('input[name=domisili_status_rumah]').attr("disabled",true);

                // Make it valid
                $('#alamat_domisili').valid()
                $('#provinsi_domisili').valid()
                $('#kota_domisili').valid()
                $('#kecamatan_domisili').valid()
                $('#kelurahan_domisili').valid()
                $('input[name=domisili_status_rumah]').valid()

            } else {
                $('#domisili_status').val("0");
                // $("#layout-alamat-domisili").removeClass('d-none').find('input, select, textarea, radio').prop("disabled", false);

                $('#alamat_domisili').text('');
                $('#provinsi_domisili').empty().append($('<option>', { value: '', text : '-- Pilih Satu --' }));
                $.getJSON("/borrower/data_provinsi/", function(data){
                    for(let i = 0; i<data.length; i++){ 
                        $('#provinsi_domisili').append($('<option>', {
                            value: data[i].text,
                            text : data[i].text
                        }));
                    }
                });
                $('#kota_domisili').empty().append($(' <option>', { value: '', text : '-- Pilih Satu --'}));
                $('#kecamatan_domisili').empty().append($('<option>', { value: '', text : '-- Pilih Satu --'}));
                $('#kelurahan_domisili').empty().append($('<option>', { value: '', text : '-- Pilih Satu --'}));
                $('#kode_pos_domisili').val('');
                $('input[name="domisili_status_rumah"]').prop('checked', false);
                    
                $('#alamat_domisili').attr('disabled', false);
                $("#provinsi_domisili").attr('disabled', false);
                $("#kota_domisili").attr('disabled', false);
                $("#kecamatan_domisili").attr('disabled', false);
                $("#kelurahan_domisili").attr('disabled', false);
                $('input[name=domisili_status_rumah]').attr("disabled",false);
            }
        })

        $('#domisili_pasangan_status').change(() => {
            if($('#domisili_pasangan_status').is(':checked')){
                let alamat = ""
                let provinsi = ""
                let kota = ""
                let kecamatan = ""
                let kelurahan = ""
                let kode_pos = ""

                if ($('#domisili_status').is(":checked")) {
                    alamat = $("#alamat").val();
                    provinsi = $("#provinsi option:selected").val();
                    kota = $("#kota option:selected").val();
                    kecamatan = $("#kecamatan option:selected").val();
                    kelurahan = $("#kelurahan option:selected").val();
                    kode_pos = $("#kode_pos").val();
                } else {
                    alamat = $("#alamat_domisili").val();
                    provinsi = $("#provinsi_domisili option:selected").val();
                    kota = $("#kota_domisili option:selected").val();
                    kecamatan = $("#kecamatan_domisili option:selected").val();
                    kelurahan = $("#kelurahan_domisili option:selected").val();
                    kode_pos = $("#kode_pos_domisili").val();
                }

                $('#domisili_pasangan_status').val("1");
                $('#alamat_pasangan').text(alamat);
                $("#provinsi_pasangan").append($('<option>', { value: provinsi, text : provinsi}));
                $('#kota_pasangan').append($('<option>', { value: kota, text : kota}));
                $("#kecamatan_pasangan").append($('<option>', { value: kecamatan, text : kecamatan}));
                $("#kelurahan_pasangan").append($('<option>', { value: kelurahan, text : kelurahan}));
                $("#kode_pos_pasangan").val(kode_pos);

                $("#provinsi_pasangan option[value='"+provinsi+"']").attr({selected:'selected'})
                $("#kota_pasangan option[value='"+kota+"']").attr({selected:'selected'})
                $("#kecamatan_pasangan option[value='"+kecamatan+"']").attr({selected:'selected'})
                $("#kelurahan_pasangan option[value='"+kelurahan+"']").attr({selected:'selected'})

                $('#alamat_pasangan').attr('readonly', 'readonly');
                $("#provinsi_pasangan").attr('readonly', 'readonly');
                $("#kota_pasangan").attr('readonly', 'readonly');
                $("#kecamatan_pasangan").attr('readonly', 'readonly');
                $("#kelurahan_pasangan").attr('readonly', 'readonly');

                $('#alamat_pasangan').valid()
                $('#provinsi_pasangan').valid()
                $('#kota_pasangan').valid()
                $('#kecamatan_pasangan').valid()
                $('#kelurahan_pasangan').valid()

            } else {
                $('#domisili_pasangan_status').val("0");
                $('#alamat_pasangan').text('');
                $('#provinsi_pasangan').empty().append($('<option>', { value: '', text : '-- Pilih Satu --' }));
                $.getJSON("/borrower/data_provinsi/", function(data){
                    for(let i = 0; i<data.length; i++){ 
                        $('#provinsi_pasangan').append($('<option>', {
                            value: data[i].text,
                            text : data[i].text
                        }));
                    }
                });
                $('#kota_pasangan').empty().append($(' <option>', { value: '', text : '-- Pilih Satu --'}));
                $('#kecamatan_pasangan').empty().append($('<option>', { value: '', text : '-- Pilih Satu --'}));
                $('#kelurahan_pasangan').empty().append($('<option>', { value: '', text : '-- Pilih Satu --'}));
                $('#kode_pos_pasangan').text('');
                    
                $('#alamat_pasangan').removeAttr('readonly');
                $("#provinsi_pasangan").removeAttr('readonly');
                $("#kota_pasangan").removeAttr('readonly');
                $("#kecamatan_pasangan").removeAttr('readonly');
                $("#kelurahan_pasangan").removeAttr('readonly');
            }
        })

        // KK
        $('#kk').keyup((e) => {
            $('#kk_pasangan').val(e.target.value)
        })

        // Status Kawin
        $('input[type=radio][name="status_kawin"]').change(() => {
            let status_kawin = $("input[name='status_kawin']:checked").val()
            if(status_kawin == '1'){
                let jenis_kelamin = $("input[name='gender']:checked").val()

                if(jenis_kelamin == '1') {
                    $('#jns_kelamin_pasangan_2').prop("checked", true);
                } else if(jenis_kelamin == '2'){
                    $('#jns_kelamin_pasangan_1').prop("checked", true);
                }
                $("#layout-informasi-pasangan").removeClass('d-none').find('input, select, textarea, radio').prop("disabled", false);
                a.smartWizard("stepState", [1], "show");
                $('#li-step-informasi-pasangan').removeClass('d-none');

                $('.sw-btn-next').removeClass('d-none');
                $('.sw-btn-group-extra').addClass('d-none');
                $('#chk-agreement').addClass('d-none')

                $("#skema_pembiayaan option[value='2']").remove();
                $('#skema_pembiayaan').append($('<option>', {
                    value: "2",
                    text : "Penghasilan Bersama"
                }));
            }else {
                $("#layout-informasi-pasangan").addClass('d-none').find('input, select, textarea, radio').prop("disabled", true);
                a.smartWizard("stepState", [1], "hide");
                $('#li-step-informasi-pasangan').addClass('d-none');

                $('.sw-btn-next').addClass('d-none');
                $('.sw-btn-group-extra').removeClass('d-none');
                $('#chk-agreement').removeClass('d-none')

                // set skema pembiayaan sendiri
                $("#skema_pembiayaan option[value='2']").remove();
            }
        })

        $('input[type=radio][name="gender"]').change(() => {
            let jenis_kelamin = $("input[name='gender']:checked").val()

            if(jenis_kelamin == '1') {
                $('#jns_kelamin_pasangan_2').prop("checked", true);
            } else if(jenis_kelamin == '2'){
                $('#jns_kelamin_pasangan_1').prop("checked", true);
            }
        })

        // Jenis Kelamin Pasangan 
        $('input[name="jns_kelamin_pasangan"]').click(function(e){
            e.preventDefault();
        });

        // Pekerjaan
        $('#skema_pembiayaan').change((e) => {
            let thisValue = e.target.value
            if(thisValue == '1') {
                $('#informasi-pekerjaan-pasangan').prop('disabled', true).addClass('d-none');
            } else if(thisValue == '2'){
                $('#informasi-pekerjaan-pasangan').prop('disabled', false).removeClass('d-none');
            } else {
                $('#informasi-pekerjaan-pasangan').prop('disabled', true).addClass('d-none');;
            }
        })
        $('#surat_ijin_usaha').change((e) => {
            let thisValue = e.target.value
            if(thisValue == '1') {
                $('#no_ijin_usaha').prop('disabled', false).removeClass('d-none');
                $('#label_no_ijin_usaha').removeClass('d-none');
                $('#no_ijin_usaha-error').removeClass('d-none')
            } else if(thisValue == '2'){
                $('#no_ijin_usaha').prop('disabled', true).addClass('d-none');
                $('#label_no_ijin_usaha').addClass('d-none');
                $('#no_ijin_usaha-error').addClass('d-none')
            } else {        
                $('#no_ijin_usaha').prop('disabled', false).removeClass('d-none');
                $('#label_no_ijin_usaha').removeClass('d-none');
                $('#no_ijin_usaha-error').removeClass('d-none')
            }
        })

        $('#sumber_penghasilan_dana').change((e) => {
            let thisValue = e.target.value
            if(thisValue == '1') {
                $('#label_nama_perusahaan').text('').append('Nama Perusahaan <i class="text-danger">*</i>');
                $('#label_alamat_perusahaan').text('').append('Alamat Perusahaan <i class="text-danger">*</i>');
                $('#label_no_telpon_usaha').text('').append('No. Telepon Perusahaan <i class="text-danger">*</i>');
                $('#label_usia_perusahaan').text('').append('Usia Perusahaan(Tahun) <i class="text-danger">*</i>');
                $('#label_nilai_spt').text('').append('Nilai SPT Tahun Terakhir<i class="text-danger">*</i>');

                $('#nama_perusahaan').prop("placeholder", "Masukkan nama perusahaan");
                $('#alamat_perusahaan').prop("placeholder", "Masukkan alamat perusahaan");
                $('#no_telpon_usaha').prop("placeholder", "Masukkan no. telp perusahaan");
                $('#usia_perusahaan').prop("placeholder", "Masukkan usia perusahaan");
                $('#nilai_spt').prop("placeholder", "Masukkan nilai Spt");

                $('.non-fixed').addClass('d-none').find('input, select, textarea, radio').prop('disabled', true)
                $('.fixed-income').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false)
            } else if(thisValue == '2'){
                $('#label_nama_perusahaan').text('').append('Nama Usaha/Profesional (Dokter dll) <i class="text-danger">*</i>');
                $('#label_alamat_perusahaan').text('').append('').append('Alamat Usaha/Praktek <i class="text-danger">*</i>');
                $('#label_no_telpon_usaha').text('').append('No. Telepon Usaha/Praktek <i class="text-danger">*</i>');
                $('#label_usia_perusahaan').text('').append('Lama Usaha/Praktek (Tahun) <i class="text-danger">*</i>');
                $('#label_nilai_spt').text('').append('Nilai SPT Usaha/Praktek Tahun Terakhir<i class="text-danger">*</i>');

                $('#nama_perusahaan').prop("placeholder", "Masukkan nama Usaha");
                $('#alamat_perusahaan').prop("placeholder", "Masukkan alamat Usaha");
                $('#no_telpon_usaha').prop("placeholder", "Masukkan no. telp Usaha");
                $('#usia_perusahaan').prop("placeholder", "Masukkan usia Usaha");
                $('#nilai_spt').prop("placeholder", "Masukkan nilai Spt Usaha");

                $('.non-fixed').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false)
                $('.fixed-income').addClass('d-none').find('input, select, textarea, radio').prop('disabled', true)
            }
        })

        $('#pekerjaan').change((e) => {
            let thisValue = e.target.value
            if(thisValue == '6' || thisValue == '7' || thisValue == '8') {
                $('#detail-penghasilan-lain-lain').removeClass('d-none')
                $('#detail-penghasilan').addClass('d-none')

                swal.fire({
                    title: "Informasi",
                    type: "warning",
                    text: "Maaf anda tidak bisa melanjutkan ke tahap berikutnya karena data pekerjaan yang diinputkan tidak memenuhi syarat",
                    allowOutsideClick: false,
                    showCancelButton: false,
                    confirmButtonClass: "btn-primary",
                }).then(() => {
                    $("#pekerjaan option[value='']").prop('selected', true);
                });
            } else {
                $('#detail-penghasilan-lain-lain').addClass('d-none')
                $('#detail-penghasilan').removeClass('d-none')
            }

            if (thisValue == '5') {
                $('#sumber_penghasilan_dana').empty();
                $('#sumber_penghasilan_dana').append($('<option>', {
                    value: "2",
                    text : "Penghasilan Tidak Tetap"
                }));
                $('#sumber_penghasilan_dana').trigger('change')
            } else {
                $('#sumber_penghasilan_dana').empty();
                $('#sumber_penghasilan_dana').append($('<option>', {
                    value: "1",
                    text : "Penghasilan Tetap"
                }));
                $('#sumber_penghasilan_dana').trigger('change')
            }
        })

        $('#penghasilan_lain_lain').change(() => {
            if($('#penghasilan_lain_lain').is(':checked')){
                $('#more-penghasilan-lain-lain').removeClass('d-none')
            } else {
                $('#more-penghasilan-lain-lain').addClass('d-none')
            }
        })

        // Pekerjaan pasangan
        $('#surat_ijin_usaha_pasangan').change((e) => {
            let thisValue = e.target.value
            if(thisValue == '1') {
                $('#no_ijin_usaha_pasangan').prop('disabled', false).removeClass('d-none');
                $('#label_no_ijin_usaha_pasangan').removeClass('d-none');
                $('#no_ijin_usaha_pasangan-error').removeClass('d-none')
            } else if(thisValue == '2'){
                $('#no_ijin_usaha_pasangan').prop('disabled', true).addClass('d-none');
                $('#label_no_ijin_usaha_pasangan').addClass('d-none');
                $('#no_ijin_usaha_pasangan-error').addClass('d-none')
            } else {
                $('#no_ijin_usaha_pasangan').prop('disabled', false).removeClass('d-none');
                $('#label_no_ijin_usaha_pasangan').removeClass('d-none');
                $('#no_ijin_usaha_pasangan-error').removeClass('d-none')
            }
        })

        $('#sumber_penghasilan_dana_pasangan').change((e) => {
            let thisValue = e.target.value
            if(thisValue == '1') {
                $('#label_nama_perusahaan_pasangan').text('').append('Nama Perusahaan <i class="text-danger">*</i>');
                $('#label_alamat_perusahaan_pasangan').text('').append('Alamat Perusahaan <i class="text-danger">*</i>');
                $('#label_no_telpon_usaha_pasangan').text('').append('No. Telepon Perusahaan <i class="text-danger">*</i>');
                $('#label_usia_perusahaan_pasangan').text('').append('Usia Perusahaan(Tahun) <i class="text-danger">*</i>');
                $('#label_nilai_spt_pasangan').text('').append('Nilai SPT Tahun Terakhir<i class="text-danger">*</i>');

                $('#nama_perusahaan_pasangan').prop("placeholder", "Masukkan nama perusahaan");
                $('#alamat_perusahaan_pasangan').prop("placeholder", "Masukkan alamat perusahaan");
                $('#no_telpon_usaha_pasangan').prop("placeholder", "Masukkan no. telp perusahaan");
                $('#usia_perusahaan_pasangan').prop("placeholder", "Masukkan usia perusahaan");
                $('#nilai_spt_pasangan').prop("placeholder", "Masukkan nilai Spt");

                $('.non-fixed-pasangan').addClass('d-none').find('input, select, textarea, radio').prop('disabled', true)
                $('.fixed-income-pasangan').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false)
            } else if(thisValue == '2'){
                $('#label_nama_perusahaan_pasangan').text('').append('Nama Usaha/Profesional (Dokter dll) <i class="text-danger">*</i>');
                $('#label_alamat_perusahaan_pasangan').text('').append('').append('Alamat Usaha/Praktek <i class="text-danger">*</i>');
                $('#label_no_telpon_usaha_pasangan').text('').append('No. Telepon Usaha/Praktek <i class="text-danger">*</i>');
                $('#label_usia_perusahaan_pasangan').text('').append('Lama Usaha/Praktek (Tahun) <i class="text-danger">*</i>');
                $('#label_nilai_spt_pasangan').text('').append('Nilai SPT Usaha/Praktek Tahun Terakhir<i class="text-danger">*</i>');

                $('#nama_perusahaan_pasangan').prop("placeholder", "Masukkan nama Usaha");
                $('#alamat_perusahaan_pasangan').prop("placeholder", "Masukkan alamat Usaha");
                $('#no_telpon_usaha_pasangan').prop("placeholder", "Masukkan no. telp Usaha");
                $('#usia_perusahaan_pasangan').prop("placeholder", "Masukkan usia Usaha");
                $('#nilai_spt_pasangan').prop("placeholder", "Masukkan nilai Spt Usaha");

                $('.non-fixed-pasangan').removeClass('d-none').find('input, select, textarea, radio').prop('disabled', false)
                $('.fixed-income-pasangan').addClass('d-none').find('input, select, textarea, radio').prop('disabled', true)
            }
        })

        $('#pekerjaan_pasangan').change((e) => {
            let thisValue = e.target.value
            if(thisValue == '6' || thisValue == '7' || thisValue == '8') {
                $('#detail-penghasilan-lain-lain-pasangan').removeClass('d-none')
                $('#detail-penghasilan-pasangan').addClass('d-none')

                swal.fire({
                    title: "Informasi",
                    type: "warning",
                    text: "Maaf anda tidak bisa melanjutkan ke tahap berikutnya karena data pekerjaan yang diinputkan tidak memenuhi syarat",
                    allowOutsideClick: false,
                    showCancelButton: false,
                    confirmButtonClass: "btn-primary",
                }).then(() => {
                    $("#pekerjaan_pasangan option[value='']").prop('selected', true);
                });

            } else {
                $('#detail-penghasilan-lain-lain-pasangan').addClass('d-none')
                $('#detail-penghasilan-pasangan').removeClass('d-none')
            }

            if (thisValue == '5') {
                $('#sumber_penghasilan_dana_pasangan').empty();
                $('#sumber_penghasilan_dana_pasangan').append($('<option>', {
                    value: "2",
                    text : "Penghasilan Tidak Tetap"
                }));
                $('#sumber_penghasilan_dana_pasangan').trigger('change')
            } else {
                $('#sumber_penghasilan_dana_pasangan').empty();
                $('#sumber_penghasilan_dana_pasangan').append($('<option>', {
                    value: "1",
                    text : "Penghasilan Tetap"
                }));
                $('#sumber_penghasilan_dana_pasangan').trigger('change')
            }
        })

        $('#penghasilan_lain_lain_pasangan').change(() => {
            if($('#penghasilan_lain_lain_pasangan').is(':checked')){
                $('#more-penghasilan-lain-lain-pasangan').removeClass('d-none')
            } else {
                $('#more-penghasilan-lain-lain-pasangan').addClass('d-none')
            }
        })

        // Send OTP
        // $('#btn_proses_lengkapi_profile, #kirim_lagi').click(() => {
        //     sendOtp()
        // });

        // $('#kode_otp').on('keyup',function(){

        //     if ($('#kode_otp').val().length == 6) {

        //         $('#kirim_data').removeAttr('disabled');
        //     } else {

        //         $('#kirim_data').prop('disabled',true);
        //     }
        // });

        let sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';

        // Save Data
        // $('#kirim_data').click(() =>{
        //     $.ajax({
        //             url : "/borrower/cekOTP/",
        //             method : "post",
        //             dataType: 'JSON',
        //             data: {otp:$('#kode_otp').val()
        //         },
        //         beforeSend: () => {
        //             $('#otp').modal('hide');;
        //             swal.fire({
        //                 html: '<h5>Menyimpan Data...</h5>',
        //                 showConfirmButton: false,
        //                 allowOutsideClick: () => false,
        //                 onRender: () => {
        //                     $('.swal2-content').prepend(sweet_loader);
        //                 },
        //             });
        //         },
        //         success:(data) => {
        //             console.log(data.status)
        //             if (data.status == '00') {
        //                 console.log('submitessd')
        //                 $('#submitindividu').click()
        //             } else {
        //                 swal.fire({
        //                     title: "Kode OTP tidak sama",
        //                     type: "error",
        //                     showCancelButton: false,
        //                     confirmButtonClass: "btn-danger",
        //                 })
        //             }
        //         },
        //         error: (data) => {
        //             swal.close()
        //             console.log(data);
        //         }   
        //     })
        // })

        $("form#form-individu").submit(function(e){
            e.preventDefault();
            let status_kawin = $("input[name='status_kawin']:checked").val()
            $.ajax({
                type: $(this).attr('method'),
                url: "{{ route('borrower.action_lengkapi_profile') }}",
                data: $(this).serialize(),
                success: function (data) {
                    if (data.history_data_status != 0) {
                        collectData();
                        swal.fire({
                            title: "Pendaftaran Berhasil",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        }).then((result) => {
                            location.href = "/borrower/beranda";
                        });
                    }
                },
                error: function (data) {
                    console.log(data)
                    swal.fire({
                        title: "Pendaftaran Gagal",
                        type: "error",
                        text: data,
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                    })
                },
            });
        })

        // Get History Inserted Data
        // Status Kawin
        let status_kawin = "{{ $data_individu ? $data_individu->status_kawin : ''}}"
        if(status_kawin) {
            $('input:radio[name="status_kawin"][value="' + status_kawin + '"]').prop('checked', true);

            if(status_kawin == '1'){
                let jenis_kelamin = $('#jns_kelamin').val()
                if(jenis_kelamin == '1') {
                    $('#jns_kelamin_pasangan_2').prop("checked", true);
                } else if(jenis_kelamin == '2'){
                    $('#jns_kelamin_pasangan_1').prop("checked", true);
                }

                $("#layout-informasi-pasangan").removeClass('d-none').find('input, select, textarea, radio').prop("disabled", false);
                a.smartWizard("stepState", [1], "show");
                $('#li-step-informasi-pasangan').removeClass('d-none');

                $('.sw-btn-next').removeClass('d-none');
                $('.sw-btn-group-extra').addClass('d-none');
                $('#chk-agreement').addClass('d-none');

                // START: Penambahan  03-11-2021
                $("#skema_pembiayaan option[value='2']").remove();
                $('#skema_pembiayaan').append($('<option>', {
                    value: "2",
                    text : "Penghasilan Bersama"
                }));
                // END: Penambahan  03-11-2021
            }else {
                $("#layout-informasi-pasangan").addClass('d-none').find('input, select, textarea, radio').prop("disabled", true);
                a.smartWizard("stepState", [1], "hide");
                $('#li-step-informasi-pasangan').addClass('d-none');

                $('.sw-btn-next').addClass('d-none');
                $('.sw-btn-group-extra').removeClass('d-none');
                $('#chk-agreement').removeClass('d-none');

                // START: Penambahan  03-11-2021
                $("#skema_pembiayaan option[value='2']").remove();
                // END: Penambahan  03-11-2021

            }
        }

        // Jenis Kelamin
        let jns_kelamin = "{{ $data_individu ? $data_individu->jns_kelamin : ''}}"
        $('input:radio[name="gender"][value="' + jns_kelamin + '"]').prop('checked', true);
        $('#jns_kelamin').val(jns_kelamin);

        jns_kelamin_pasangan = 0
        if (jns_kelamin == '1') {
            jns_kelamin_pasangan = 2
        } else {
            jns_kelamin_pasangan = 1
        }
        $('input:radio[name="jns_kelamin_pasangan"][value="' + jns_kelamin_pasangan + '"]').prop('checked', true);

        // START: History Alamat
        let provinsi = "{{ $data_individu ? $data_individu->provinsi : '' }}"
        let kota = "{{ $data_individu ? $data_individu->kota : '' }}"
        let kecamatan = "{{ $data_individu ? $data_individu->kecamatan : '' }}"
        let kelurahan = "{{ $data_individu ? $data_individu->kelurahan : '' }}"
        if (kota) {
            $.getJSON("/borrower/data_kota/"+provinsi+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kota').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kota option[value='"+kota+"']").attr('selected', 'selected');
            });
        }
        if (kecamatan) {
            $.getJSON("/borrower/data_kecamatan/"+kota+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kecamatan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kecamatan option[value='"+kecamatan+"']").attr('selected', 'selected');
            });
        }
        if (kelurahan) {
            $.getJSON("/borrower/data_kelurahan/"+kecamatan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kelurahan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kelurahan option[value='"+kelurahan+"']").attr('selected', 'selected');
            });
        }

        let provinsi_domisili = "{{ $data_individu ? $data_individu->domisili_provinsi : '' }}"
        let kota_domisili = "{{ $data_individu ? $data_individu->domisili_kota : '' }}"
        let kecamatan_domisili = "{{ $data_individu ? $data_individu->domisili_kecamatan : '' }}"
        let kelurahan_domisili = "{{ $data_individu ? $data_individu->domisili_kelurahan : '' }}"
        
        if (provinsi == provinsi_domisili && kota == kota_domisili && kecamatan == kecamatan_domisili && kelurahan == kelurahan_domisili && provinsi != ''  && kota != '' && kecamatan != '' && kelurahan != '') {
            $('#domisili_status').prop('checked', true)
            // $('#domisili_status').trigger("change")
            $("#layout-alamat-domisili").find('input, select, textarea, radio').prop("disabled", true);
        }
        if (kota_domisili) {
            $.getJSON("/borrower/data_kota/"+provinsi_domisili+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kota_domisili').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kota_domisili option[value='"+kota_domisili+"']").attr('selected', 'selected');
            });
        }
        if (kecamatan_domisili) {
            $.getJSON("/borrower/data_kecamatan/"+kota_domisili+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kecamatan_domisili').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kecamatan_domisili option[value='"+kecamatan_domisili+"']").attr('selected', 'selected');
            });
        }
        if (kelurahan_domisili) {
            $.getJSON("/borrower/data_kelurahan/"+kecamatan_domisili+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kelurahan_domisili').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kelurahan_domisili option[value='"+kelurahan_domisili+"']").attr('selected', 'selected');
            });
        }

        let provinsi_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->provinsi : '' }}"
        let kota_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->kab_kota : '' }}"
        let kecamatan_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->kecamatan : '' }}"
        let kelurahan_perusahaan = "{{ $data_penghasilan ? $data_penghasilan->kelurahan : '' }}"

        if (kota_perusahaan) {
            $.getJSON("/borrower/data_kota/"+provinsi_perusahaan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kota_perusahaan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kota_perusahaan option[value='"+kota_perusahaan+"']").attr('selected', 'selected');
            });
        }
        if (kecamatan_perusahaan) {
            $.getJSON("/borrower/data_kecamatan/"+kota_perusahaan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kecamatan_perusahaan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kecamatan_perusahaan option[value='"+kecamatan_perusahaan+"']").attr('selected', 'selected');
            });
        }
        if (kelurahan_perusahaan) {
            $.getJSON("/borrower/data_kelurahan/"+kecamatan_perusahaan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kelurahan_perusahaan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kelurahan_perusahaan option[value='"+kelurahan_perusahaan+"']").attr('selected', 'selected');
            });
        }

        let provinsi_pasangan = "{{ $data_pasangan ? $data_pasangan->provinsi : '' }}"
        let kota_pasangan = "{{ $data_pasangan ? $data_pasangan->kota : '' }}"
        let kecamatan_pasangan = "{{ $data_pasangan ? $data_pasangan->kecamatan : '' }}"
        let kelurahan_pasangan = "{{ $data_pasangan ? $data_pasangan->kelurahan : '' }}"
        if (provinsi == provinsi_pasangan && kota == kota_pasangan && kecamatan == kecamatan_pasangan && kelurahan ==
            kelurahan_pasangan && provinsi != '' && kota != '' && kecamatan != '' && kelurahan != '') {
            $('#domisili_pasangan_status').prop('checked', true)
            $('#domisili_pasangan_status').trigger("change")
        }
        if (kota_pasangan) {
            $.getJSON("/borrower/data_kota/"+provinsi_pasangan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kota_pasangan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kota_pasangan option[value='"+kota_pasangan+"']").attr('selected', 'selected');
            });
        }
        if (kecamatan_pasangan) {
            $.getJSON("/borrower/data_kecamatan/"+kota_pasangan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kecamatan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kecamatan_pasangan option[value='"+kecamatan_pasangan+"']").attr('selected', 'selected');
            });
        }
        if (kelurahan_pasangan) {
            $.getJSON("/borrower/data_kelurahan/"+kecamatan_pasangan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kelurahan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kelurahan_pasangan option[value='"+kelurahan_pasangan+"']").attr('selected', 'selected');
            });
        }
        let provinsi_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->provinsi : '' }}"
        let kota_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->kota : '' }}"
        let kecamatan_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->kecamatan : '' }}"
        let kelurahan_perusahaan_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->kelurahan : '' }}"

        if (kota_perusahaan_pasangan) {
            $.getJSON("/borrower/data_kota/"+provinsi_perusahaan_pasangan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kota_perusahaan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kota_perusahaan_pasangan option[value='"+kota_perusahaan_pasangan+"']").attr('selected', 'selected');
            });
        }
        if (kecamatan_perusahaan_pasangan) {
            $.getJSON("/borrower/data_kecamatan/"+kota_perusahaan_pasangan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kecamatan_perusahaan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kecamatan_perusahaan_pasangan option[value='"+kecamatan_perusahaan_pasangan+"']").attr('selected', 'selected');
            });
        }
        if (kelurahan_perusahaan_pasangan) {
            $.getJSON("/borrower/data_kelurahan/"+kecamatan_perusahaan_pasangan+"/", function(data){
                for(let i = 0; i<data.length; i++){ 
                    $('#kelurahan_perusahaan_pasangan').append($('<option>', {
                        value: data[i].text,
                        text : data[i].text
                    }));
                }
                $("#kelurahan_perusahaan_pasangan option[value='"+kelurahan_perusahaan_pasangan+"']").attr('selected', 'selected');
            });
        }
        // END: History Alamat

        // START: History Pekerjaan
        // Status Kepemilikan Rumah
        let status_kepemilikan_rumah = "{{ $data_individu ? $data_individu->status_rumah : ''}}"
        $('input:radio[name="status_rumah"][value="' + status_kepemilikan_rumah + '"]').prop('checked', true);

        let domisili_status_kepemilikan_rumah = "{{ $data_individu ? $data_individu->domisili_status_rumah : ''}}"
        $('input:radio[name="domisili_status_rumah"][value="' + domisili_status_kepemilikan_rumah + '"]').prop('checked', true);

        // Pendidikan Terakhir
        let pendidikan_terakhir = "{{ $data_individu ? $data_individu->agama : ''}}"
        $("#pendidikan_terakhir option[value='"+pendidikan_terakhir+"']").prop('selected', true);

        // Sumber Penghasilan Dana
        let sumber_penghasilan_dana = "{{ $data_penghasilan ? $data_penghasilan->sumber_pengembalian_dana : ''}}"
        if (sumber_penghasilan_dana) {
            $("#sumber_penghasilan_dana option[value='"+sumber_penghasilan_dana+"']").prop('selected', true);
            $("#sumber_penghasilan_dana").trigger('change')
        }

        let sumber_penghasilan_dana_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->sumber_pengembalian_dana : ''}}"
        if (sumber_penghasilan_dana_pasangan) {
            $("#sumber_penghasilan_dana_pasangan option[value='"+sumber_penghasilan_dana_pasangan+"']").prop('selected', true);
            $("#sumber_penghasilan_dana_pasangan").trigger('change')
        }

        // Skema Pembiayaan
        let skema_pembiayaan = "{{ $data_penghasilan ? $data_penghasilan->skema_pembiayaan : ''}}"
        $("#skema_pembiayaan option[value='"+skema_pembiayaan+"']").prop('selected', true);
        $("#skema_pembiayaan").trigger('change')

        // Status Kepegawaiaan
        let status_kepegawaian = "{{ $data_penghasilan ? $data_penghasilan->status_pekerjaan : ''}}"
        $("#status_kepegawaian option[value='" + status_kepegawaian +"']").attr("selected","selected");

        let status_kepegawaian_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->status_pekerjaan : ''}}"
        $("#status_kepegawaian_pasangan option[value='" + status_kepegawaian_pasangan +"']").attr("selected","selected");

        // Surat Ijin
        let surat_ijin = "{{ $data_penghasilan ? $data_penghasilan->surat_ijin : ''}}"
        if (surat_ijin) {
            $("#surat_ijin_usaha option[value='" + surat_ijin +"']").attr("selected","selected");
            $("#surat_ijin_usaha").trigger('change')
        }

        let surat_ijin_pasangan = "{{ $data_penghasilan_pasangan ? $data_penghasilan_pasangan->surat_ijin : ''}}"
        if (surat_ijin_pasangan) {
            $("#surat_ijin_usaha_pasangan option[value='" + surat_ijin_pasangan +"']").attr("selected","selected");
            $("#surat_ijin_usaha_pasangan").trigger('change')
        }

        // penghasilan lain lain
        let penghasilan_lain_lain = `{!! $data_penghasilan ? $data_penghasilan->detail_penghasilan_lain_lain : '' !!}`
        if (penghasilan_lain_lain) {
            $('#penghasilan_lain_lain').prop('checked', true)
            $('#penghasilan_lain_lain').trigger("change")
        }

        let penghasilan_lain_lain_pasangan = `{!! $data_penghasilan_pasangan ? $data_penghasilan_pasangan->detail_penghasilan_lain_lain : '' !!}`
        if (penghasilan_lain_lain_pasangan) {
            $('#penghasilan_lain_lain_pasangan').prop('checked', true)
            $('#penghasilan_lain_lain_pasangan').trigger("change")
        }
        // END: History Pekerjaan
        // END: Individu
    })
</script>
{{-- END: Dom Manipulation --}}

{{-- START: Other Function --}}
<script>
    // Reset Timeout
    const resetTimeout = () => {
        clearTimeout(timer);
    }
    
    // Rupiah Currency Format
    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);
        
        if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
    
    let onFinishCallback = () => {
        if ($("#form-individu").valid()) {
            if (is_nik_unique == 1) {
                if (is_phone_number_unique == 1) {
                    if (is_npwp_unique == 1) {
                        $("#modal_action_lengkapi_profile").modal('show');
                    } else {
                        $('#npwp').focus()
                        return false
                    }
                } else {
                    $('#telepon').focus()
                    return false
                }
            } else {
                $('#ktp').focus()
                return false
            }
        } 
    }

    let sendOtp = () => {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#kirim_lagi').prop('disabled',true);
        
        var tipeBrw = $('#borrower_type').val()
        let no_hp_individu = $('#telepon').val()
        if (tipeBrw == '1') {
            $('#set_no_hp').text('Silakan masukkan kode OTP yang telah dikirim ke nomor ' + '+62'+no_hp_individu);
            $('#no_hp').val('62'+no_hp_individu);
            console.log($('#telepon').val())
        }

        noHP = '62'+no_hp_individu;
        console.log(no_hp_individu)

        $.ajax({
            url : "/borrower/kirimOTP/",
            method : "post",
            dataType: 'JSON',
            data: {hp:noHP},
            success:function(data) {
                console.log(data.status)
            }
        });
        timerDisableButton();
    }

    let timerDisableButton = () => {
        var spn = document.getElementById("count");
        var btn = document.getElementById("kirim_lagi");
        
        var count = 30; // Set count
        
        (function countDown(){
            // Display counter and start counting down
            spn.textContent = count;
            
            // Run the function again every second if the count is not zero
            if(count !== 0){
                timer = setTimeout(countDown, 1000);
                count--; // decrease the timer
            } else {
                // Enable the button
                btn.removeAttribute("disabled");
                spn.removeText
            }
        }());
    }
</script>
{{-- END: Other Function --}}

<!----------------------------------------------- Badan Hukum ---------------------------------------------->
<script language="JavaScript">
    $(document).ready(function(){
        $("#webcam_npwp_badan_hukum").hide();
        $("#take_camera_npwp_badan_hukum").click(function(){
            Webcam.attach( '#camera_npwp_badan_hukum' );

            $("#webcam_npwp_badan_hukum").fadeIn();
            $("#take_camera_npwp_badan_hukum").hide();
            $("#results_npwp_badan_hukum").show();
            $("#webcam_npwp_badan_hukum").css( { "position" : "-100px;"} );
        });
    });

    $(document).ready(function(){
        $("#webcam_pengurus").hide();
        $("#take_camera_pengurus").click(function(){
            Webcam.attach( '#camera_pengurus' );

            $("#webcam_pengurus").fadeIn();
            $("#take_camera_pengurus").hide();
            $("#results_foto_pengurus").show();
            $("#webcam_pengurus").css( { "position" : "-100px;"} );
        });
    });
    
    $(document).ready(function(){
        $("#webcam_foto_ktp_pengurus").hide();
        $("#take_camera_foto_ktp_pengurus").click(function(){
            Webcam.attach( '#camera_ktp_pengurus' );

            $("#webcam_foto_ktp_pengurus").fadeIn();
            $("#take_camera_foto_ktp_pengurus").hide();
            $("#results_foto_ktp_pengurus").show();
            $("#webcam_foto_ktp_pengurus").css( { "position" : "-100px;"} );
        });
    });

    $(document).ready(function(){
        $("#webcam_foto_ktpdiri_pengurus").hide();
        $("#take_camera_foto_ktpdiri_pengurus").click(function(){
            Webcam.attach( '#camera_ktpdiri_pengurus' );

            $("#webcam_foto_ktpdiri_pengurus").fadeIn();
            $("#take_camera_foto_ktpdiri_pengurus").hide();
            $("#results_foto_ktp_diri_pengurus").show();
            $("#webcam_foto_ktpdiri_pengurus").css( { "position" : "-100px;"} );
        });
    });

    $(document).ready(function(){
        $("#webcam_foto_npwp_pengurus").hide();
        $("#take_camera_foto_npwp_pengurus").click(function(){
            Webcam.attach( '#camera_npwp_pengurus' );

            $("#webcam_foto_npwp_pengurus").fadeIn();
            $("#take_camera_foto_npwp_pengurus").hide();
            $("#results_foto_npwp_pengurus").show();
            $("#webcam_foto_npwp_pengurus").css( { "position" : "-100px;"} );
        });
    });

    // PENGURUS 2
    $(document).ready(function(){
        $("#webcam_pengurus_2").hide();
        $("#take_camera_pengurus_2").click(function(){
            Webcam.attach( '#camera_pengurus_2' );

            $("#webcam_pengurus_2").fadeIn();
            $("#take_camera_pengurus_2").hide();
            $("#results_foto_pengurus_2").show();
            $("#webcam_pengurus_2").css( { "position" : "-100px;"} );
        });
    });
    
    $(document).ready(function(){
        $("#webcam_foto_ktp_pengurus_2").hide();
        $("#take_camera_foto_ktp_pengurus_2").click(function(){
            Webcam.attach( '#camera_ktp_pengurus_2' );

            $("#webcam_foto_ktp_pengurus_2").fadeIn();
            $("#take_camera_foto_ktp_pengurus_2").hide();
            $("#results_foto_ktp_pengurus_2").show();
            $("#webcam_foto_ktp_pengurus_2").css( { "position" : "-100px;"} );
        });
    });

    $(document).ready(function(){
        $("#webcam_foto_ktpdiri_pengurus_2").hide();
        $("#take_camera_foto_ktpdiri_pengurus_2").click(function(){
            Webcam.attach( '#camera_ktpdiri_pengurus_2' );

            $("#webcam_foto_ktpdiri_pengurus_2").fadeIn();
            $("#take_camera_foto_ktpdiri_pengurus_2").hide();
            $("#results_foto_ktp_diri_pengurus_2").show();
            $("#webcam_foto_ktpdiri_pengurus_2").css( { "position" : "-100px;"} );
        });
    });

    $(document).ready(function(){
        $("#webcam_foto_npwp_pengurus_2").hide();
        $("#take_camera_foto_npwp_pengurus_2").click(function(){
            Webcam.attach( '#camera_npwp_pengurus_2' );

            $("#webcam_foto_npwp_pengurus_2").fadeIn();
            $("#take_camera_foto_npwp_pengurus_2").hide();
            $("#results_foto_npwp_pengurus_2").show();
            $("#webcam_foto_npwp_pengurus_2").css( { "position" : "-100px;"} );
        });
    });
        
</script>

<!--------------------------------------------- Foto Badan Hukum ------------------------------------------->
<script language="JavaScript">
    function take_snapshot_npwp_badan_hukum()
    {

        Webcam.snap( function(data_uri) {
            document.getElementById('image_npwp_badan_hukum').value = data_uri;
            document.getElementById('results_npwp_badan_hukum').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_npwp_bdn_hukum",
                method : "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto NPWP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_npwp_badan_hukum').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_pengurus()
    {
        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_pengurus').value = data_uri;
            document.getElementById('results_foto_pengurus').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_foto_pengurus_1",
                method : "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    // console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_pengurus').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktp_pengurus()
    {

        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_ktp_pengurus').value = data_uri;
            document.getElementById('results_foto_ktp_pengurus').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_foto_ktp_pengurus_1",
                method : "POST",
                dataType: "JSON",
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_ktp_pengurus').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktpdiri_pengurus()
    {
    
        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_ktp_diri_pengurus').value = data_uri;
            document.getElementById('results_foto_ktp_diri_pengurus').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_foto_ktp_diri_pengurus_1",
                method : "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto Diri Dengan KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_brw_dengan_ktp_pengurus').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_npwp_pengurus()
    {
    
        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_npwp_pengurus').value = data_uri;
            document.getElementById('results_foto_npwp_pengurus').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_npwp_pengurus_1",
                method : "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto NPWP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_npwp_pengurus').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<!------------------------------------------------ Pengurus 2 --------------------------------------------->
<script language="JavaScript">
    function take_snapshot_foto_pengurus_2()
    {
        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_pengurus_2').value = data_uri;
            document.getElementById('results_foto_pengurus_2').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_foto_pengurus_2",
                method : "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    // console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_pengurus_2').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktp_pengurus_2()
    {
    
        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_ktp_pengurus_2').value = data_uri;
            document.getElementById('results_foto_ktp_pengurus_2').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_foto_ktp_pengurus_2",
                method : "POST",
                dataType: "JSON",
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_ktp_pengurus_2').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_ktpdiri_pengurus_2()
    {
    
        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_ktp_diri_pengurus_2').value = data_uri;
            document.getElementById('results_foto_ktp_diri_pengurus_2').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_foto_ktp_diri_pengurus_2",
                method : "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto Diri Dengan KTP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_brw_dengan_ktp_pengurus_2').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<script language="JavaScript">
    function take_snapshot_foto_npwp_pengurus_2()
    {
    
        Webcam.snap( function(data_uri) {
            document.getElementById('image_foto_npwp_pengurus_2').value = data_uri;
            document.getElementById('results_foto_npwp_pengurus_2').innerHTML = '<img class="image--cover" src="'+data_uri+'" style="width:200px;height:160px;"/>';

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var foto = data_uri;
            $.ajax({

                url : "/borrower/webcam_npwp_pengurus_2",
                method : "POST",
                dataType: 'JSON',
                data: foto,
                contentType: false,
                processData: false,
                success:function(data)
                {
                    console.log(data);
                    if(data.success){
                        swal.fire({
                            title: 'Unggah Foto NPWP Berhasil',
                            // html: "<span><button class='btn btn-primary btn-lg' id='btn_individu'>Individu</button></span> <span><button id='btn_perusahaan' class='btn btn-warning btn-lg'>Perusahaan</button></span>",
                            type:"success",
                            showConfirmButton:true
                        });
                        $('#url_pic_npwp_pengurus_2').val(data.url);
                    }else{
                        alert(data.failed);
                    }
                    
                }
            });
        } );
    }
</script>

<!------------------------------------------ valisasi Badan Hukum ------------------------------------------>
<script language="JavaScript">

    var toNIKPengurus = false;
    function CheckNIKPengurus() {
        var nik = $(".checkNIKPengurus").val() ;
        //var regEx = /^[09]/;

        $("#divNoKTPPengurus").removeClass("has-error has-success is-invalid");
        $("#divNoKTPPengurus label").remove();
        $("#divNoKTPPengurus").prepend('<label class="control-label" for="idPengguna"><i class="fa fa-spin fa-spinner"></i></label>');
        
        if ($(".checkNIKPengurus").val().length == 16) {
            
            if (toNIKPengurus) {
                clearTimeout(toNIKPengurus);
            }
            toNIKPengurus = setTimeout(function () {
                $.get("/borrower/check_nik/"+nik, function( data ) {
                    $("#divNoKTPPengurus label").remove();
                    if (data.status == "ada") {
                        $("#divNoKTPPengurus").addClass("has-error is-invalid");
                        $("#divNoKTPPengurus").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NIK Sudah Terdaftar</label>');
                    } else {

                        $("#divNoKTPPengurus").removeClass("has-error is-invalid");
                        $("#divNoKTPPengurus").addClass("has-success is-valid");
                        $("#divNoKTPPengurus").prepend('<label for="txt_no_ktp_pribadi"><i class="fa fa-check"></i> NIK Belum Terdaftarkan</label>');
                    }
                });
                
            }, 100);
            
        } else {
            $("#divNoKTPPengurus label").remove();
            $("#divNoKTPPengurus").addClass("has-error is-invalid");
            $("#divNoKTPPengurus").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NIK Harus 16 Digit</label>');
                            
        } 
    }
    
		
    // check npwp pribadi n perusahaan
    var toNPWP = false;
    function CheckNPWP() {
        var npwp = $("#txt_npwp_pribadi").val() ;
        var type_borrower = $("#label_tipe_borrower").text() ;
        var type = "";
        if(type_borrower == "Individu"){type = 1} else {type = 2}

        $("#divNPWP").removeClass("has-error has-success is-invalid");
        $("#divNPWP label").remove();
        $("#divNPWP").prepend('<label class="control-label" for="idPengguna"><i class="fa fa-spin fa-spinner"></i></label>');
        
        if ($("#txt_npwp_pribadi").val().length == 15) {
            
            if (toNPWP) {
                clearTimeout(toNPWP); 
            }
            toNPWP = setTimeout(function () {
                $.get("/borrower/check_npwp/"+npwp+'/'+type, function( data ) {
                    $("#divNPWP label").remove();
                    if (data.status == "ada") {
                        $("#divNPWP").addClass("has-error is-invalid");
                        $("#divNPWP").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NPWP Sudah Terdaftar</label>');
                    } else {
                        $("#divNPWP").removeClass("has-error is-invalid");
                        $("#divNPWP").addClass("has-success is-valid");
                        $("#divNPWP").prepend('<label for="txt_no_ktp_pribadi"><i class="fa fa-check"></i> NPWP Belum Terdaftarkan</label>');
                    }
                });
                
            }, 100);
            
        } else {
            $("#divNPWP label").remove();
            $("#divNPWP").addClass("has-error is-invalid");
            $("#divNPWP").prepend('<label style="color:red;" for="txt_no_ktp_pribadi"><i class="fa fa-times-circle-o"></i> NPWP Harus 15 Digit</label>');
                            
        } 
    }

    var checkNOHPPengurus = false; 
    function check_hp_pengurus() {
        
        var notlp = $(".checkNOHPPengurus").val() ;
        if(notlp.indexOf('0') == 0){
            
            $('.checkNOHPPengurus').val('');
        }
        var regex = new RegExp (/^[1-9]*$/);
        $("#div_hp_pengurus").removeClass("has-error has-success is-invalid");
        $("#div_hp_pengurus label").remove();
        $("#div_hp_pengurus").prepend('<label class="control-label" for="txt_notlp_pribadi"><i class="fa fa-spin fa-spinner"></i></label>');
        //if ($("#txt_notlp_pribadi").val().length == 13 && ) {
            
            
            if (checkNOHPPengurus) {
                clearTimeout(checkNOHPPengurus);
            }
            checkNOHPPengurus = setTimeout(function () {
                
                $.get("/borrower/check_no_tlp/"+notlp, function( data ) {
                //$.get("http://core.danasyariah.id/borrower/check_no_tlp/"+notlp, function( data ) {
                    $("#div_hp_pengurus label").remove();
                    //var obj = jQuery.parseJSON( data );
                    
                    if (data.status == "ada") {
                        $("#div_hp_pengurus").addClass("has-error is-invalid");
                        $("#div_hp_pengurus").prepend('<label style="color:red;" for="txt_notlp_pribadi"><i class="fa fa-times-circle-o"></i> No HP Sudah Terdaftar</label>');
                    } else {
                        $("#div_hp_pengurus").removeClass("has-error is-invalid");
                        $("#div_hp_pengurus").addClass("has-success is-valid");
                        $("#div_hp_pengurus").prepend('<label for="txt_notlp_pribadi"><i class="fa fa-check"></i> No HP Belum Terdaftar</label>');
                    }
                });
                
                
                
            }, 50);
                
        
    }
			
    $(document).ready(function(){
        
        // validasi check
        $('.checkKarakterAneh').on('input', function (event) { 
            this.value = this.value.replace(/[^a-zA-Z 0-9 _.]/g, '');
        });

        $('.checkNIB').on('input', function (event) { 
            this.value = this.value.replace(/[^a-zA-Z 0-9 _./-]/g, '');
        });
        
        
        $('.validasiString').on('input', function (event) { 
            this.value = this.value.replace(/[^0-9]/g, '');
        });
        
        // data agama
        $.getJSON("/borrower/agama", function(data_agama){
            
            $('#txt_agama').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Agama",
                allowClear: true,
                data: data_agama,
                width: '100%'
            });

            $('#txt_agama_pengurus').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Agama",
                allowClear: true,
                data: data_agama,
                width: '100%'
            });

            $('#txt_agama_pengurus_2').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Agama",
                allowClear: true,
                data: data_agama,
                width: '100%'
            });
            
        });

        // data pendidikan
        $.getJSON("/borrower/data_pendidikan", function(data_pendidikan){
            $('#txt_pendidikanT_pribadi').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });

            $('#txt_pendidikanT_pasangan').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });

            $('#txt_pendidikanT_ahli_waris').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });

            $('#txt_pendidikanT_pengurus').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });

            $('#txt_pendidikanT_pengurus_2').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Pendidikan",
                allowClear: true,
                data: data_pendidikan,
                width: '100%'
            });
            
            
        });
        
        $.getJSON("/borrower/data_jabatan", function(data_jabatan){
            $('.txt_jabatan_pengurus').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Jabatan",
                allowClear: true,
                data: data_jabatan,
                //multiple: true,
                width: '100%'
            });
        })

        $(`#txt_provinsi_pengurus, #txt_kota_pengurus, #txt_kecamatan_pengurus, #txt_kelurahan_pengurus,
        #txt_provinsi_pengurus_2, #txt_kota_pengurus_2, #txt_kecamatan_pengurus_2, #txt_kelurahan_pengurus_2
        `).prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%'
        });

        // data pekerjaan badan hukum
        $.getJSON("/borrower/data_pekerjaan_bdn_hukum", function(data_bidang_usaha){
            
            $('#txt_bd_pekerjaan_badan_hukum').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Bidang Usaha",
                allowClear: true,
                data: data_bidang_usaha,
                width: '100%'
            });

        });
        
        // hide & show domisili badan hukum
        $("#domisili_badan_hukum").change(function(){
            if($('#domisili_badan_hukum').is(":checked"))  {
                
                var alamat_ktp 		= $('#txt_alamat_badan_hukum').val();
                var provinsi_ktp_text 	= $("#txt_provinsi_badan_hukum option:selected").val();
                var kota_ktp_text	= $("#txt_kota_badan_hukum option:selected").val();
                var kecamatan_ktp_text 	= $("#txt_kecamatan_badan_hukum option:selected").val();
                var kelurahan_ktp_text 	= $("#txt_kelurahan_badan_hukum option:selected").val();
                var kdPOS_ktp_text 		= $("#txt_kd_pos_badan_hukum").val();
                
                
                $("#txt_alamat_domisili_badan_hukum").val(alamat_ktp).attr('disabled', true).valid()
                
                $("#txt_provinsi_domisili_badan_hukum").empty().trigger('change')
                $("#txt_provinsi_domisili_badan_hukum").prepend('<option value='+provinsi_ktp_text+' selected>'+provinsi_ktp_text+'</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Provinsi",
                    width: '100%'
                });
                
                $("#txt_kota_domisili_badan_hukum").empty().trigger('change')
                $("#txt_kota_domisili_badan_hukum").prepend('<option value='+kota_ktp_text+' selected>'+kota_ktp_text+'</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Kota",
                    width: '100%'
                });
                
                $("#txt_kecamatan_domisili_badan_hukum").empty().trigger('change')
                $("#txt_kecamatan_domisili_badan_hukum").prepend('<option value='+kecamatan_ktp_text+' selected>'+kecamatan_ktp_text+'</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Kecamatan",
                    width: '100%'
                });
                
                $("#txt_kelurahan_domisili_badan_hukum").empty().trigger('change')
                $("#txt_kelurahan_domisili_badan_hukum").prepend('<option value='+kelurahan_ktp_text+' selected>'+kelurahan_ktp_text+'</option>').select2({
                    theme: "bootstrap",
                    placeholder: "Pilih Kelurahan",
                    width: '100%'
                });
                
                $('#txt_provinsi_domisili_badan_hukum').attr('disabled', true).valid()
                $('#txt_kota_domisili_badan_hukum').attr('disabled', true).valid()
                $('#txt_kecamatan_domisili_badan_hukum').attr('disabled', true).valid()
                $('#txt_kelurahan_domisili_badan_hukum').attr('disabled', true).valid()

                $("#txt_kd_pos_domisili_badan_hukum").val(kdPOS_ktp_text)
                
            }
            else{
                $("#txt_alamat_domisili_badan_hukum").attr('disabled', false).val('');
                $("#txt_provinsi_domisili_badan_hukum").empty().attr('disabled', false)
                $('#txt_provinsi_domisili_badan_hukum').prepend('<option selected></option>').select2({
                    theme: "bootstrap",
                    placeholder: "-- Pilih Satu --",
                    allowClear: true,
                    width: '100%'
                });
                $.getJSON("/borrower/data_provinsi", function(data_provinsi){
                    for(let i = 0; i<data_provinsi.length; i++){ 
                        $('#txt_provinsi_domisili_badan_hukum').append($('<option>', {
                            value: data_provinsi[i].text,
                            text : data_provinsi[i].text
                        }));
                    }
                });
                $("#txt_kota_domisili_badan_hukum").empty().attr('disabled', false).trigger('change');
                $("#txt_kecamatan_domisili_badan_hukum").empty().attr('disabled', false).trigger('change');
                $("#txt_kelurahan_domisili_badan_hukum").empty().attr('disabled', false).trigger('change');
                $("#txt_kd_pos_domisili_badan_hukum").val('')

                
            }
            
        });
    });

    // proses lengkapi profile
    $("#form_lengkapi_profile").submit(function(e){
        e.preventDefault()
        var tipe_borrower = $("#label_tipe_borrower").text();
        // $("#modal_action_lengkapi_profile").modal('show');
       
        if($("#form_lengkapi_profile").valid()){
            if ($("#divNoKTPPengurus").hasClass("has-error")) {
                $('#txt_no_ktp_pengurus').focus()
            }else if ($("#div_hp_pengurus").hasClass("has-error")) {
                $('#txt_noHP_pengurus').focus()
            }  else {
                $("#modal_action_lengkapi_profile").modal('show');
            }
        } 
    });
    
    $('#btn_proses_lengkapi_profile').on('click',function(){

        var tipeBrw = $("#label_tipe_borrower").text() ;

        if(tipeBrw == 'Individu'){
            sendOtp()
        } else{ 

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#kirim_lagi').prop('disabled',true);
            
            $('#no_hp').val("62"+$('#txt_noHP_pengurus').val());
            $('#set_no_hp').text('Silakan masukkan kode OTP yang telah dikirim ke nomor ' + '62'+$('#txt_noHP_pengurus').val() );
            
            noHP = $('#no_hp').val();
            console.log(noHP)
            $.ajax({
                url : "/borrower/kirimOTP/",
                method : "post",
                dataType: 'JSON',
                data: {hp:noHP},
                success:function(data)
                {
                    console.log(data.status)
                }
            });
            timerDisableButton();
        }
    });
    
    $('#kirim_lagi').on('click',function(){
        $('#kirim_lagi').prop('disabled',true);
        
        var tipeBrw = $("#label_tipe_borrower").text() ;
        if (tipeBrw == 'Individu')
        {
            $('#no_hp').val('62'+$('#txt_noHP_pribadi').val());
        }
        else
        {
            $('#no_hp').val('62'+$('#txt_noHP_pengurus').val());
        }

        noHP = $('#no_hp').val();
        console.log(noHP)
        $.ajax({
            url : "/borrower/kirimOTP/",
            method : "post",
            dataType: 'JSON',
            data: {hp:noHP},
            success:function(data)
            {
                console.log(data.status)
            }
        });
        timerDisableButton();
    });

    $('#kode_otp').on('keyup',function(){
        // console.log($('#kode_otp').val().length)
        if ($('#kode_otp').val().length == 6)
        {
            $('#kirim_data').removeAttr('disabled');

        }
        else
        {
            $('#kirim_data').prop('disabled',true);
        }
    });
    
    let sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';

    // proses submit lengkapi profile
    $('#kirim_data').on('click',function(){
        // var type_borrower = $("#label_tipe_borrower").text() ;
        
        $.ajax({
            url : "/borrower/cekOTP/",
            method : "post",
            dataType: 'JSON',
            data: {otp:$('#kode_otp').val()},
            beforeSend: () => {
                $('#otp').modal('hide');;
                swal.fire({
                    html: '<h5>Menyimpan Data...</h5>',
                    showConfirmButton: false,
                    allowOutsideClick: () => false,
                    onRender: () => {
                        $('.swal2-content').prepend(sweet_loader);
                    },
                });
            },
            success:function(data)
            {
                if (data.status == '00')
                {                      
                    <!-- ---------------------------------------------- VARIABLE PRIBADI ------------------------------>
                    var plafon = 2000000000;
                    
                    var type_borrower = $("#label_tipe_borrower").text() ;
            
                    // foto pribadi
                    // var url_pic_brw = $("#url_pic_brw").val();
                    // var url_pic_brw_ktp = $("#url_pic_brw_ktp").val();
                    // var url_pic_brw_dengan_ktp = $("#url_pic_brw_dengan_ktp").val(); 
                    // var url_pic_brw_npwp = $("#url_pic_brw_npwp").val();
                    
                    <!-- ---------------------------------------------- VARIABLE BADAN HUKUM / PERUSAHAAN ---------------------------- -->
                    
                    var txt_nm_badan_hukum = $("#txt_nm_badan_hukum").val();
                    var txt_nib_badan_hukum = $("#txt_nib_badan_hukum").val();
                    var txt_npwp_badan_hukum = $("#txt_npwp_badan_hukum").val();
                    var txt_akta_badan_hukum = $("#txt_akta_badan_hukum").val();
                    var txt_tgl_berdiri_badan_hukum = $("#txt_tgl_berdiri_badan_hukum").val();
                    var txt_tlp_badan_hukum = "021"+$("#txt_tlp_badan_hukum").val();
                    var url_npwp_badan_hukum = $("#url_npwp_badan_hukum").val();
                    var txt_alamat_badan_hukum = $("#txt_alamat_badan_hukum").val();
                    var txt_provinsi_badan_hukum = $("#txt_provinsi_badan_hukum option:selected").text();
                    var txt_kota_badan_hukum = $("#txt_kota_badan_hukum option:selected").text();
                    var txt_kecamatan_badan_hukum = $("#txt_kecamatan_badan_hukum option:selected").text();
                    var txt_kelurahan_badan_hukum = $("#txt_kelurahan_badan_hukum option:selected").text();
                    var txt_kd_pos_badan_hukum = $("#txt_kd_pos_badan_hukum").val();
                    
                    // domisili
                    var txt_alamat_domisili_badan_hukum = $("#txt_alamat_domisili_badan_hukum").val();
                    var txt_provinsi_domisili_badan_hukum = $("#txt_provinsi_domisili_badan_hukum option:selected").text();
                    var txt_kota_domisili_badan_hukum = $("#txt_kota_domisili_badan_hukum option:selected").text();
                    var txt_kecamatan_domisili_badan_hukum = $("#txt_kecamatan_domisili_badan_hukum option:selected").text();
                    var txt_kelurahan_domisili_badan_hukum = $("#txt_kelurahan_domisili_badan_hukum option:selected").text();
                    var txt_kd_pos_domisili_badan_hukum= "";
                    if($('#domisili_badan_hukum').is(":checked")) {
                        txt_kd_pos_domisili_badan_hukum = $("#txt_kd_pos_badan_hukum").val()
                    }else{
                        txt_kd_pos_domisili_badan_hukum = $("#txt_kd_pos_domisili_badan_hukum").val();
                    };
                    
                    // Rekening
                    var txt_nm_pemilik_badan_hukum = $("#txt_nm_pemilik_badan_hukum").val();
                    var txt_no_rekening_badan_hukum = $("#txt_no_rekening_badan_hukum").val();
                    var txt_bank_badan_hukum = $("#txt_bank_pribadi option:selected").val();
                    var txt_kantor_cabang_pembuka_badan_hukum = $("#txt_kantor_cabang_pembuka").val();
                    
                    // dan lain lain
                    var txt_bd_pekerjaan_badan_hukum = $("#txt_bd_pekerjaan_badan_hukum option:selected").val();
                    var txt_omset_thn_akhir = $("#txt_omset_thn_akhir").val();
                    var txt_aset_thn_akhir = $("#txt_aset_thn_akhir").val();
                    
                    // pengurus
                    
                    var pengurus_nama= []; pengurus_jk = []; pengurus_ktp = []; pengurus_tmp_lahir = []; pengurus_tgl_lahir= []; pengurus_no_hp = []; pengurus_agama = []; pengurus_pendidikan = []; 
                    pengurus_npwp = []; pengurus_alamat = []; pengurus_provinsi = []; pengurus_kota = []; pengurus_kecamatan = []; pengurus_kelurahan = []; pengurus_kd_pos = [];
                    pengurus_foto = []; pengurus_foto_ktp = []; pengurus_fotoKTP = []; pengurus_foto_npwp = []; pengurus_jabatan = [];
                    
                    
                    
                    // pengurus_nama
                    $('input[name="txt_nm_pengurus[]"]').each(function() {
                        pengurus_nama.push(this.value);
                    });

                    // pengurus_jk
                    $('input[name="txt_jns_kelamin_pengurus[]"]').each(function() {
                        pengurus_jk.push(this.value);
                    });

                    $('input[name="txt_jns_kelamin_pengurus2[]"]').each(function() {
                        pengurus_jk.push(this.value);
                    });

                    //pengurus_ktp
                    $('input[name="txt_no_ktp_pengurus[]"]').each(function() {
                        pengurus_ktp.push(this.value);
                    });
                    
                    //pengurus_tmp_lahir
                    $('input[name="txt_tmpt_lahir_pengurus[]"]').each(function() {
                        pengurus_tmp_lahir.push(this.value);
                    });
                    
                    //pengurus_tgl_lahir
                    $('input[name="txt_tgl_lahir_pengurus[]"]').each(function() {
                        pengurus_tgl_lahir.push(this.value);
                    });
                    
                    //pengurus_no_hp
                    $('input[name="txt_noHP_pengurus[]"]').each(function() {
                        pengurus_no_hp.push(this.value);
                    });
                    
                    //pengurus_agama
                    $('select[name="txt_agama_pengurus[]"]').each(function() {
                        pengurus_agama.push(this.value);
                        console.log(this.value);
                        
                    });
                    
                    //pengurus_pendidikan
                    $('select[name="txt_pendidikanT_pengurus[]"]').each(function() {
                        pengurus_pendidikan.push(this.value);
                    });
                    
                    //pengurus_npwp
                    $('input[name="txt_npwp_pengurus[]"]').each(function() {
                        pengurus_npwp.push(this.value);
                    });
                    
                    //pengurus jabatan
                    $('select[name="txt_jabatan_pengurus[]"]').each(function() {
                        pengurus_jabatan.push(this.value);
                    });
                    
                    //pengurus_alamat
                    $('textarea[name="txt_alamat_pengurus[]"]').each(function() {
                        pengurus_alamat.push(this.value);
                    });
                    
                    //pengurus_provinsi
                    $('select[name="txt_provinsi_pengurus[]"]').each(function() {
                        pengurus_provinsi.push(this.value);
                    });
                    
                    //pengurus_kota
                    $('select[name="txt_kota_pengurus[]"]').each(function() {
                        pengurus_kota.push(this.value);
                    });
                    
                    //pengurus_kecamatan
                    $('select[name="txt_kecamatan_pengurus[]"]').each(function() {
                        pengurus_kecamatan.push(this.value);
                    });
                    
                    //pengurus_kelurahan
                    $('select[name="txt_kelurahan_pengurus[]"]').each(function() {
                        pengurus_kelurahan.push(this.value);
                    });
                    
                    //pengurus_kd_pos
                    $('input[name="txt_kd_pos_pengurus[]"]').each(function() {
                        pengurus_kd_pos.push(this.value);
                    });
                    
                    //pengurus_foto
                    $('input[name="url_pic_pengurus[]"]').each(function() {
                        pengurus_foto.push(this.value);
                    });
                    
                    //pengurus_foto_ktp
                    $('input[name="url_pic_ktp_pengurus[]"]').each(function() {
                        pengurus_foto_ktp.push(this.value);
                    });
                    
                    //pengurus_fotoKTP
                    $('input[name="url_pic_brw_dengan_ktp_pengurus[]"]').each(function() {
                        pengurus_fotoKTP.push(this.value);
                    });
                    
                    //pengurus_foto_npwp
                    $('input[name="url_pic_npwp_pengurus[]"]').each(function() {
                        pengurus_foto_npwp.push(this.value);
                    });

                    var pengurus = [];
                    for(var u=0; u < pengurus_nama.length; u++){
                        pengurus[u] = [
                            pengurus_nama[u]+"@",
                            "@"+pengurus_jk[u]+"@", "@"+pengurus_ktp[u]+"@", "@"+pengurus_tmp_lahir[u]+"@", "@"+pengurus_tgl_lahir[u]+"@", "@"+"62"+pengurus_no_hp[u]+"@", "@"+pengurus_agama[u]+"@", "@"+pengurus_pendidikan[u]+"@",
                            "@"+pengurus_npwp[u]+"@" ,"@"+pengurus_jabatan[u]+"@" , "@"+pengurus_alamat[u]+"@", "@"+pengurus_provinsi[u]+"@", "@"+pengurus_kota[u]+"@", "@"+pengurus_kecamatan[u]+"@", "@"+pengurus_kelurahan[u]+"@", "@"+pengurus_kd_pos[u]+"@", 
                            "@"+pengurus_foto[u]+"@", "@"+pengurus_foto_ktp[u]+"@", "@"+pengurus_fotoKTP[u]+"@", "@"+pengurus_foto_npwp[u]                             
                        ];
                    }
                    var pengurus_arr = pengurus.join("^~");
                    
                    
                    // type individu
                    if(type_borrower == "Individu"){
                        $('#submitindividu').click()
                        type_borrower = 1; // perorangan pegawai 
                    } // end type individu
                    else{ // type perusahaan
                        type_borrower = 2;

                        if(pengurus_nama.length < 2){ 
                            $('#otp').modal('hide'); 
                            Swal.fire('Daftarkan Pengurus Minimal 2'); 
                            $('html, body').animate({ scrollTop: $("#divCountPengurus_1").offset().top }, 500); 
                            return false; 
                        }
                        else{ 
                            var dataBadanHukum=new FormData(); dataBadanHukum.append('_token', "{{ csrf_token() }}" );
                            dataBadanHukum.append('type_borrower', type_borrower); dataBadanHukum.append('txt_nm_badan_hukum', txt_nm_badan_hukum); 
                            dataBadanHukum.append('txt_nib_badan_hukum', txt_nib_badan_hukum);
                            dataBadanHukum.append('txt_npwp_badan_hukum', txt_npwp_badan_hukum); dataBadanHukum.append('txt_akta_badan_hukum', txt_akta_badan_hukum); 
                            dataBadanHukum.append('txt_tgl_berdiri_badan_hukum', txt_tgl_berdiri_badan_hukum);
                            dataBadanHukum.append('txt_tlp_badan_hukum', txt_tlp_badan_hukum); 
                            dataBadanHukum.append('url_npwp_badan_hukum', url_npwp_badan_hukum); dataBadanHukum.append('txt_alamat_badan_hukum', txt_alamat_badan_hukum);
                            dataBadanHukum.append('txt_provinsi_badan_hukum', txt_provinsi_badan_hukum);
                            dataBadanHukum.append('txt_kota_badan_hukum', txt_kota_badan_hukum);
                            dataBadanHukum.append('txt_kecamatan_badan_hukum', txt_kecamatan_badan_hukum);
                            dataBadanHukum.append('txt_kelurahan_badan_hukum', txt_kelurahan_badan_hukum);
                            dataBadanHukum.append('txt_kd_pos_badan_hukum', txt_kd_pos_badan_hukum); 
                            
                            // domisili
                            dataBadanHukum.append('txt_alamat_domisili_badan_hukum', txt_alamat_domisili_badan_hukum);
                            dataBadanHukum.append('txt_provinsi_domisili_badan_hukum', txt_provinsi_domisili_badan_hukum);
                            dataBadanHukum.append('txt_kota_domisili_badan_hukum', txt_kota_domisili_badan_hukum);
                            dataBadanHukum.append('txt_kecamatan_domisili_badan_hukum', txt_kecamatan_domisili_badan_hukum);
                            dataBadanHukum.append('txt_kelurahan_domisili_badan_hukum', txt_kelurahan_domisili_badan_hukum);
                            dataBadanHukum.append('txt_kd_pos_domisili_badan_hukum', txt_kd_pos_domisili_badan_hukum); 
                            
                            // Rekening
                            dataBadanHukum.append('txt_nm_pemilik_badan_hukum', txt_nm_pemilik_badan_hukum);
                            dataBadanHukum.append('txt_no_rekening_badan_hukum', txt_no_rekening_badan_hukum); 
                            dataBadanHukum.append('txt_bank_badan_hukum', txt_bank_badan_hukum); 
                            dataBadanHukum.append('txt_kantor_cabang_pembuka_badan_hukum', txt_kantor_cabang_pembuka_badan_hukum);
                            
                            // dan lain lain
                            dataBadanHukum.append('txt_bd_pekerjaan_badan_hukum', txt_bd_pekerjaan_badan_hukum);
                            dataBadanHukum.append('txt_omset_thn_akhir', txt_omset_thn_akhir);
                            dataBadanHukum.append('txt_aset_thn_akhir', txt_aset_thn_akhir);
                            
                            // pengurus 
                            dataBadanHukum.append('pengurus', pengurus_arr); 
                            
                            // proses simpan lengkapi profile
                            $.ajax({ 
                                url: "{{route('borrower.action_lengkapi_profile')}}" ,
                                type: "POST" ,
                                data : dataBadanHukum, 
                                processData: false, 
                                contentType: false, 
                                beforeSend: function() { 
                                    $("#overlay").css('display','block'); }, 
                                success: function (response) { 
                                    console.log(response); 
                                    if(response.status=="sukses" ){
                                        collectData();
                                        $("#otp").modal("hide"); 
                                        swal.fire({
                                            title: 'Pendaftaran Berhasil' , 
                                            allowOutsideClick: false,
                                            html: "Hi {{Session::get("+brw_nama+")}} , <br/>  Selamat pendaftaran anda berhasil, tim kami akan menghubungi anda, <br/><span><button class='btn btn-primary btn-lg' id='btn_confirm'>OK</button></span>", 
                                            type:"success", 
                                            showConfirmButton:false 
                                        }); 
                                        $('#btn_confirm').on('click',function(){
                                            location.href="/borrower/beranda" ; 
                                        }); 
                                    } 
                                },
                                error:function(response) {
                                    console.log(response)
                                    swal.fire({
                                        title: 'Error',
                                        text: 'Internal Error',
                                        type: 'error',
                                        showConfirmButton: false,
                                    });
                                }
                            }); 
                        } 
                    } // end type perusahaan 
                    // $('#otp').modal('show'); 
                }
                else {
                    swal.fire({
                        title: "Kode OTP tidak sama",
                        type: "error",
                        showCancelButton: false,
                        allowOutsideClick: false,
                        confirmButtonClass: "btn-danger",
                    }).then( function (response) {
                        console.log(response)
                        if (response.value){
                            $('#otp').modal('show');
                        }
                    })
                } 
            },
            error:function(response) {
                console.log(response)
                swal.fire({
                    title: 'Error',
                    text: 'Internal Error',
                    type: 'error',
                    showConfirmButton: false,
                });
            }
        });
    }); 
</script> 
@endsection
