@extends('layouts.borrower.master')
@section('title', 'Beranda')
@section('style')
<style>
    .bg-gradient-hijau {
        background: rgb(47, 122, 21);
        background: -moz-linear-gradient(51deg, rgba(47, 122, 21, 1) 0%, rgba(6, 119, 87, 1) 100%);
        background: -webkit-linear-gradient(51deg, rgba(47, 122, 21, 1) 0%, rgba(6, 119, 87, 1) 100%);
        background: linear-gradient(51deg, rgba(47, 122, 21, 1) 0%, rgba(6, 119, 87, 1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#2f7a15", endColorstr="#067757", GradientType=1);
    }

    .bg-gradient-hijau-2 {
        background: rgb(21, 122, 89);
        background: -moz-linear-gradient(51deg, rgba(21, 122, 89, 1) 0%, rgba(6, 95, 119, 1) 100%);
        background: -webkit-linear-gradient(51deg, rgba(21, 122, 89, 1) 0%, rgba(6, 95, 119, 1) 100%);
        background: linear-gradient(51deg, rgba(21, 122, 89, 1) 0%, rgba(6, 95, 119, 1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#157a59", endColorstr="#065f77", GradientType=1);
    }

    .bg-gradient-blue {
        background: rgb(21, 85, 122);
        background: -moz-linear-gradient(51deg, rgba(21, 85, 122, 1) 0%, rgba(6, 55, 119, 1) 100%);
        background: -webkit-linear-gradient(51deg, rgba(21, 85, 122, 1) 0%, rgba(6, 55, 119, 1) 100%);
        background: linear-gradient(51deg, rgba(21, 85, 122, 1) 0%, rgba(6, 55, 119, 1) 100%);
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#15557a", endColorstr="#063777", GradientType=1);
    }
</style>
@endsection

@section('content')
<div id="overlay">
    <div class="cv-spinner">
        <span class="spinner"></span>
    </div>
</div>
<!-- Main Container -->

{{-- @if(Auth::guard('borrower')->user()->status == 'active') --}}
<main id="main-container">
    <!-- Page Content -->
    <div id="detect-screen" class="content-full-right">
        <div class="container col-12">
            <div class="row my-5">

                <div id="col" class="col-12 col-md-9 mt-30">
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block" role="alert">
                        {!! $message !!}
                        <button class="close" data-dismiss="alert"></button>
                    </div>
                    @endif
                </div>

                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h1 class="no-paddingTop font-w400 judul" style="float: left; margin-block-end: 0em;">
                            Beranda</h1>
                        <span id="btn-ajukan-pendanaan" class="pull-right">
                            @if (Auth::guard('borrower')->user()->detilBorrower['brw_type'] == 2)
                            <a href="/borrower/ajukanPendanaan" class="btn btn-rounded btn-big btn-noborder min-width-150 text-white"><span class="p-5">Ajukan Pendanaan Baru</span></a>
                            @else
                            <a href="/borrower/lengkapi_profile_pendanaan_simple" class="btn btn-rounded btn-big btn-noborder min-width-150 text-white"><span class="p-5">Ajukan Pendanaan Baru </span></a>
                            @endif
                            {{-- <a href="/borrower/portofolio"
                                class="btn btn-rounded btn-big btn-noborder min-width-150 text-white"><span
                                    class="p-5">Portofolio</span></a> --}}
                        </span>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 mt-4 pt-4 mx-auto">
                    <div id="alert-msg" class="alert alert-warning d-none" role="alert">
                        <div class="row">
                            <div class="col-10">
                                <div id="text-msg"></div>
                            </div>
                            <div class="col-2 my-auto">
                                <button type="button" class="close my-auto" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4 pt-4 mb-4 pb-4">
                <div id="layout-informasi-pendanaan" class="col-md-12">
                    <div class="card border border-dark">
                        {{-- START: Table Pendanaan --}}
                        <div class="row">
                            <div class="col">
                                <p class="pt-0 pl-4" style="margin-top: -0.8rem"> <span class="bg-white h5 font-weight-normal">&nbsp; Pendanaan
                                        Aktif &nbsp;</span></p>
                            </div>
                        </div>

                        <div class="card-body pt-4 px-4">
                            <div class="row mb-4">
                                <div class="col-md-12">

                                    <div class="table-responsive">
                                        <table id="table_pendanaan" class="table table-striped table-bordered dt-responsive" cellpadding="0" cellspacing="0" width="100%">
                                            {{-- <table class="table table-bordered table-striped" id="table_pendanaan"> --}}
                                            <thead>
                                                <th class="align-middle text-center text-capitalize">ID Pengajuan</th>
                                                <th class="align-middle text-center text-capitalize">Tipe Pendanaan</th>
                                                <th class="align-middle text-center text-capitalize">Tujuan Pendanaan
                                                </th>
                                                <th class="align-middle text-center text-capitalize">Nomor Akad</th>
                                                <th class="align-middle text-center text-capitalize">Tenor (Bulan)</th>
                                                <th class="align-middle text-center text-capitalize">Pokok Pinjaman</th>
                                                <th class="align-middle text-center text-capitalize">Sisa Pokok</th>
                                                <th class="align-middle text-center text-capitalize">Tanggal Mulai</th>
                                                <th class="align-middle text-center text-capitalize">Tanggal Selesai
                                                </th>
                                                <th class="align-middle text-center text-capitalize">Status</th>
                                                <th class="align-middle text-center text-capitalize">Aksi</th>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- END: Table Pendanaan --}}
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
</main>
{{-- @endif --}}
{{-- <!-- END Main Container --> --}}

<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        const brw_id = "{{ $idbrw }}"
        let route_list_pendanaan = '{{ route("get-json-list-pendanaan", ["brw_id" => "brw_id_val"]) }}'
        route_list_pendanaan = route_list_pendanaan.replace('brw_id_val', brw_id)

        let list_pendanaan_active = $('#table_pendanaan').DataTable({
            dom: '<f<tr>ip>',
            searching: true,
            processing: true,
            serverSide: true,
            paging: true,
            info: false,
            pageLength: 10,
            order: [
                [0, "desc"]
            ],
            ajax: {
                url: route_list_pendanaan
            },
            columnDefs: [
                // { "width": "2%", "targets": 0 },
                {
                    className: "text-center",
                    targets: "_all"
                },
            ],
            columns: [{
                    data: 'pengajuan_id',
                    name: 'pengajuan_id'
                },
                {
                    data: 'pendanaan_nama',
                    name: 'pendanaan_nama'
                },
                {
                    data: 'tujuan_pembiayaan',
                    name: 'tujuan_pembiayaan'
                },
                {
                    data: null,
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return '-';
                    }
                },
                {
                    data: 'durasi_proyek',
                    name: 'durasi_proyek'
                },
                {
                    data: 'pendanaan_dana_dibutuhkan',
                    render: $.fn.dataTable.render.number(',', '.', 2, 'Rp')
                },
                {
                    data: null,
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return '-';
                    }
                },
                {
                    data: null,
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return '-';
                    }
                },
                {
                    data: null,
                    orderable: false,
                    render: function(data, type, row, meta) {
                        return `-`;
                    }
                },
                {
                    data: 'status',
                    data: null,
                    name: 'status',
                    render: function(data, type, row, meta) {
                        let status = data.status
                        let btn_text = ''
                        if (status == '0') {
                            btn_text = 'Pengajuan Baru'
                        } else if (status == '1') {
                            btn_text = 'Lolos Tahap 1'
                        } else if (status == '3') {
                            btn_text = 'Proses Verifikasi'
                        } else if (status == '11') {
                            btn_text = 'Proses Verifikasi'
                        } else if (status == '5') {
                            btn_text = 'Diterima'
                        } 
                        return btn_text
                    }
                },
                {
                    data: 'detail_pendanaan',
                    name: 'detail_pendanaan',
                    orderable: false,
                    serachable: false
                }
            ]
        })

        let route_get_notif_pendanaan = "{{ route('get-notif-pendanaan', ['brw_id' => 'brw_id_val']) }}"
        route_get_notif_pendanaan = route_get_notif_pendanaan.replace('brw_id_val', brw_id)
        $.ajax({
            url: route_get_notif_pendanaan,
            method: "GET",
            dataType: 'JSON',
            success: function(response) {
                let status_user = response.data.status_user
                let total_pengajuan = response.data.total_pengajuan
                let msg = ''
                if (status_user == 'active') {

                    if (total_pengajuan > 0) {
                        msg = `
                        Anda memiliki ${response.data.total_pengajuan} Pengajuan Pendanaan yang telah <span class="text-success font-weight-bold">LOLOS</span>
                        Verifikasi Tahap Awal, segera <u><a href="/borrower/pendanaan" class="text-success font-weight-bold">Lengkapi Data Pengajuan Anda
                        </a></u>untuk lanjut ke tahap berikutnya
                        `

                        $('#alert-msg').removeClass('d-none')
                        $('#text-msg').html(msg)
                    }

                } else if (status_user == 'notfilled') {
                    if (total_pengajuan > 0) {
                        msg = `
                        Selamat pengajun pendanaan Anda telah <span class="text-success font-weight-bold">LOLOS</span>
                        Verifikasi Tahap Awal, silahkan <u><a href="/borrower/lengkapi_profile" class="text-success font-weight-bold">Lengkapi Data Registrasi
                        </a></u>untuk lanjut ke tahap berikutnya
                        `

                        $('#alert-msg').removeClass('d-none')
                        $('#text-msg').html(msg)
                    }
                }
            }
        });
    });

    const formatRupiah = (angka, prefix) => {
        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? prefix + rupiah : '');
    }
</script>
@endsection