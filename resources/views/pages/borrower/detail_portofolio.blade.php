@extends('layouts.borrower.master')
@section('title', 'Portofolio - Detail Pendanaan')
@section('content')
    <!-- Main Container -->
    @if (Auth::guard('borrower')->user()->status == 'pending')
        <main id="main-container">

            <!-- Page Content -->
            <div class="content">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;">
                                Silahkan Tunggu Verifikasi dari Danasyariah</h1>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h4 class="no-paddingTop font-w400 text-dark" style="float: left; margin-block-end: .6em;">
                                Terimakasih telah bergabung untuk maju bersama kami</h4>
                        </span>


                    </div>
                </div>

                <!-- END Frequently Asked Questions -->
            </div>
            <!-- END Page Content -->

        </main>
    @elseif(Auth::guard('borrower')->user()->status == 'active')
        <main id="main-container">
            <!-- Page Content -->
            <div id="detect-screen" class="content-full-right">
                <div class="container col-12">
                    <div class="row my-5">
                        <div id="col" class="col-12 col-md-12 mt-30">
                            <span class="mb-10 pb-10 ">
                                <h1 class="no-paddingTop font-w400 judul" style="float: left; margin-block-end: 0em;">
                                    Portofolio - Detail Pendanaan</h1>

                            </span>
                        </div>
                    </div>
                    
                
                    <div class="row mt-20 pt-20">
                        <div id="col2" class="col-md-12 mt-5 pt-5">
                            <span class="mb-10 pb-10 ">
                                <h3 class="no-paddingTop font-w300 judul" style="float: left; ">Detail Pendanaan</h3>
                            </span>
                            <div class="table-responsive">
                                <table class="table border" id="table_pendanaan">
                                    <thead class="bg-dark text-light">
                                        <th>No</th>
                                        <th >Bulan</th>
                                        <th >Pinjaman Pokok</th>
                                        <th >Margin (%)</th>
                                        <th >Angsuran Pokok</th>
                                        <th >Angsuran Margin</th>
                                        <th >Total Angsuran</th>
                                        <th >Tanggal Jatuh Tempo</th>
                                        <th >Tanggal Pembayaran</th>
                                        <th>No Referensi DSI</th>
                                        <th> Status Pembayaran </th>
                                        <th> Status Kolektabilitas </th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Agustus</td>
                                            <td>Rp. 150.000.000</td>
                                            <td>2</td>
                                            <td>Rp. 465.000</td>
                                            <td>Rp. 300.000</td>
                                            <td>Rp. 765.000</td>
                                            <td>24-08-2021</td>
                                            <td>24-08-2025</td>
                                            <td>DSI-837373</td>
                                            <td>Paid</td>
                                            <td>Status Kolek</td>    
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>September</td>
                                            <td>Rp. 150.000.000</td>
                                            <td>2</td>
                                            <td>Rp. 465.000</td>
                                            <td>Rp. 300.000</td>
                                            <td>Rp. 765.000</td>
                                            <td>24-08-2021</td>
                                            <td>24-08-2025</td>
                                            <td>DSI-837373</td>
                                            <td>Paid</td>
                                            <td>Status Kolek</td>    
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>



                </div>
            </div>
            <!-- END Page Content -->
        </main>
    @endif


    <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            var idbrw = {{ $idbrw }};

        });
    </script>
@endsection
