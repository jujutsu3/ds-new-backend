@extends('layouts.borrower.master')

@section('title', 'Dasbor Penerima Dana')

@section('content')
    <!-- Side Overlay-->
    <!--@include('includes.borrower.right_menu_all_pendanaan')-->
    <!-- END Side Overlay -->
    <style>
    .css-radio .css-control-input~.css-control-indicator {
        width: 15px;
        height: 15px;
        background-color: #ddd;
        border: 0px solid #ddd;
        border-radius: 50%;
    }
    .css-radio.css-control-elegance .css-control-input:checked ~ .css-control-indicator::after {
        background-color: #d262e3;
    }
    </style>
    <!-- Main Container -->
    <main id="main-container">
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container">
                <div class="row">
                    <div id="col" class="col-12 col-md-9 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="no-paddingTop font-w400 text-dark" >Pendanaan Anda</h1>                            
                        </span>
                    </div>
                    <div id="col" class="col-12 col-md-10">
                        <span class="mb-10">
                            <div class="form-material floating">
                                <input type="text" class="form-control col-12 col-md-10" style="height: calc(1.5em + .957143rem + 3px);" id="material-text2" name="material-text2">
                                <label for="material-text2" style="color: #8B8B8B!important;" class="font-w300"> <i class="fa fa-search"></i> Cari Berdasarkan Nama atau Lokasi</label>
                            </div>
                        </span>

                        <div class="col-12 mt-20" style="padding-left: 0px;">
                            <label class="css-control css-control-secondary css-radio mr-11 text-dark">
                                <input type="checkbox" checked class="css-control-input" id="chk_pengajuan" name="pengajuan">
                                <span class="css-control-indicator"></span> Pengajuan
                            </label>
                            <label class="css-control css-control-danger css-radio mr-10 text-dark">
                                <input type="checkbox" checked class="css-control-input" id="chk_ditolak" name="ditolak">
                                <span class="css-control-indicator"></span> Ditolak
                            </label>
							<label class="css-control css-control-info css-radio mr-10 text-dark">
                                <input type="checkbox" checked class="css-control-input" id="chk_terverifikasi" name="terverifikasi">
                                <span class="css-control-indicator"></span> Terferivikasi
                            </label>
                            <label class="css-control css-control-warning css-radio mr-10 text-dark">
                                <input type="checkbox" checked class="css-control-input" id="chk_penggalangan" name="penggalangan">
                                <span class="css-control-indicator"></span> Penggalangan Dana
                            </label>
                            <label class="css-control css-control-primary css-radio text-dark">
                                <input type="checkbox" checked class="css-control-input" id="chk_pencairan" name="pencairan">
                                <span class="css-control-indicator"></span> Pencairan Dana
                            </label>
                            <label class="css-control css-control-elegance css-radio mr-10 text-dark">
                                <input type="checkbox" checked class="css-control-input" id="chk_proyek_berjalan" name="proyek_berjalan">
                                <span class="css-control-indicator"></span> Proyek Berjalan
                            </label>
                            <label class="css-control css-control-success css-radio mr-10 text-dark">
                                <input type="checkbox" checked class="css-control-input" id="chk_proyek_selesai" name="proyek_selesai">
                                <span class="css-control-indicator"></span> Selesai
                            </label>
                        </div>
                    </div>
                    <!-- kanan 
                    <div id="col" class="col-12 col-md-3 pt-30 d-none d-xl-block">
                        <span class="pt-30 ">
                            <h6 class="text-muted font-w300"></h6>                         
                        </span>
                    </div>-->
                </div>


                <div class="row mt-10 pt-5">
                    <div id="col" class="col-md-8 mt-5 pt-5">
                        <div class="row">
							
							@foreach ($list_pengajuan as $rowPengajuan)
								<div class="col-md-4 DivPengajuan">
									<a class="block text-center" href="/borrower/detilProyek/{{$rowPengajuan->pengajuan_id}}">
										<div class="block-content block-content-full block-content-sm bg-secondary">
											<span class="font-w600 text-white"> {{$rowPengajuan->pendanaan_nama}}</span>
										</div>
										<div class="block-content block-content-full bg-primary-lighter">
											<img class="img-avatar img-avatar-thumb" src="{{url("storage/$rowPengajuan->gambar_utama")}}" alt="">
										</div>
										
									</a>
								</div>
							@endforeach
							
							@foreach ($list_ditolak as $rowTolak)
							<div class="col-md-4 DivTolak">
								<a class="block text-center" href="/borrower/detilProyek/{{$rowTolak->pengajuan_id}}">
									<div class="block-content block-content-full block-content-sm bg-danger">
										<span class="font-w600 text-white">{{$rowTolak->pendanaan_nama }}</span>
									</div>
									<div class="block-content block-content-full bg-primary-lighter">
										<img class="img-avatar img-avatar-thumb" src="{{url("storage/$rowTolak->gambar_utama")}}" alt="">
									</div>
									
								</a>
							</div>
							@endforeach
							
							@foreach ($list_aktif as $rowAktif)
							<div class="col-md-4 DivVerifikasi">
								<a class="block text-center" href="/borrower/detilProyek/{{$rowAktif->pendanaan_id}}">
									<div class="block-content block-content-full block-content-sm bg-info">
										<span class="font-w600 text-white">{{$rowAktif->pendanaan_nama}}</span>
									</div>
									<div class="block-content block-content-full bg-primary-lighter">
										<img class="img-avatar img-avatar-thumb" src="{{url("storage/$rowAktif->gambar_utama")}}" alt="">
									</div>
								   
								</a>
							</div>
							@endforeach
							
							@foreach ($list_penggalangan as $rowPenggalangan)
							<div class="col-md-4 DivPenggalangan">
								<a class="block text-center" href="/borrower/detilProyek/{{$rowPenggalangan->proyek_id}}">
									<div class="block-content block-content-full block-content-sm bg-warning">
										<span class="font-w600 text-white">{{$rowPenggalangan->pendanaan_nama}}</span>
									</div>
									<div class="block-content block-content-full bg-primary-lighter">
										<img class="img-avatar img-avatar-thumb" src="{{url("storage/$rowPenggalangan->gambar_utama")}}" alt="">
									</div>
									
								</a>
							</div>
							@endforeach
							
							@foreach ($list_pencairan as $rowPencairan)
							<div class="col-md-4 DivPencairan">
								<a class="block text-center" href="/borrower/detilProyek/{{$rowPencairan->proyek_id}}">
									<div class="block-content block-content-full block-content-sm bg-primary">
										<span class="font-w600 text-white">{{$rowPencairan->pendanaan_nama}}</span>
									</div>
									<div class="block-content block-content-full bg-primary-lighter">
										<img class="img-avatar img-avatar-thumb" src="{{url("storage/$rowPencairan->gambar_utama")}}" alt="">
									</div>
									
								</a>
							</div>
							@endforeach
							
							@foreach ($list_berjalan as $rowBerjalan)
							<div class="col-md-4 DivProyekBerjalan">
								<a class="block text-center" href="/borrower/detilProyek/{{$rowBerjalan->proyek_id}}">
									<div class="block-content block-content-full block-content-sm bg-elegance">
										<span class="font-w600 text-white">{{$rowBerjalan->pendanaan_nama}}</span>
									</div>
									<div class="block-content block-content-full bg-primary-lighter">
                                        
										<img class="img-avatar img-avatar-thumb" src="{{url("storage/$rowBerjalan->gambar_utama")}}" alt="">
									</div>
									
								</a>
							</div>
							@endforeach
							
							@foreach ($list_selesai as $rowSelesai)
							<div class="col-md-4 DivProyekSelesai">
								<a class="block text-center" href="/borrower/detilProyek/{{$rowSelesai->proyek_id}}">
									<div class="block-content block-content-full block-content-sm bg-success">
										<span class="font-w600 text-white">{{$rowSelesai->pendanaan_nama}}</span>
									</div>
									<div class="block-content block-content-full bg-primary-lighter">
										<img class="img-avatar img-avatar-thumb" src="{{url("storage/$rowSelesai->gambar_utama")}}" alt="">
									</div>
									
								</a>
							</div>
							@endforeach
                        </div>
                    </div>                           
                </div>

                

                
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->

    <script>
        $(document).on("click", "#chk_pengajuan", function(){
            
            if($('input[name="pengajuan"]').is(':checked'))
            {
                $("#chk_pengajuan").prop( "checked", true );
                $('.DivPengajuan').show( "slow", function() {
                    // hide Div Pengajuan
                });
            }
            else
            {
                $("#chk_pengajuan").prop( "checked", false );
               
                $( ".DivPengajuan").hide( "slow", function() {
                    // hide Div Pengajuan
                });
            }
        });
		$(document).on("click", "#chk_ditolak", function(){
            
            if($('input[name="ditolak"]').is(':checked'))
            {
                $("#chk_ditolak").prop( "checked", true );
                $('.DivTolak').show( "slow", function() {
                    // hide Div Pengajuan
                });
            }
            else
            {
                $("#chk_ditolak").prop( "checked", false );
               
                $( ".DivTolak").hide( "slow", function() {
                    // hide Div Pengajuan
                });
            }
        });
		
		
        $(document).on("click", "#chk_terverifikasi", function(){
            
            if($('input[name="terverifikasi"]').is(':checked'))
            {
                $("#chk_terverifikasi").prop( "checked", true );
                $(".DivVerifikasi").show( "slow", function() {
                    // hide Div Pengajuan
                });
            }else
            {
                $("#chk_terverifikasi").prop( "checked", false );
                $(".DivVerifikasi").hide( "slow", function() {
                    // hide Div Pengajuan
                });
            }
        });

        $(document).on("click", "#chk_penggalangan", function(){
            
            if($('input[name="penggalangan"]').is(':checked'))
            {
                $("#chk_penggalangan").prop( "checked", true );
                $(".DivPenggalangan").show( "slow", function() {
                    // hide Div Pengajuan
                });
            }else
            {
                $("#chk_penggalangan").prop( "checked", false );
                $(".DivPenggalangan").hide( "slow", function() {
                    // hide Div Pengajuan
                });
            }
        });

        $(document).on("click", "#chk_pencairan", function(){
            
            if($('input[name="pencairan"]').is(':checked'))
            {
                $("#chk_pencairan").prop( "checked", true );
                $(".DivPencairan").show( "slow", function() {
                    // hide Div Pengajuan
                });
            }else
            {
                $("#chk_pencairan").prop( "checked", false );
                $(".DivPencairan").hide( "slow", function() {
                    // hide Div Pengajuan
                });
            }
        });

        $(document).on("click", "#chk_proyek_berjalan", function(){
            
            if($('input[name="proyek_berjalan"]').is(':checked'))
            {
                $("#chk_proyek_berjalan").prop( "checked", true );
                $(".DivProyekBerjalan").show( "slow", function() {
                    // hide Div Pengajuan
                });
            }else
            {
                $("#chk_proyek_berjalan").prop( "checked", false );
                $(".DivProyekBerjalan").hide( "slow", function() {
                    // hide Div Pengajuan
                });
            }
        });

        $(document).on("click", "#chk_proyek_selesai", function(){
            
            if($('input[name="proyek_selesai"]').is(':checked'))
            {
                $("#chk_proyek_selesai").prop( "checked", true );
                $(".DivProyekSelesai").show( "slow", function() {
                    // hide Div Pengajuan
                });
            }else
            {
                $("#chk_proyek_selesai").prop( "checked", false );
                $(".DivProyekSelesai").hide( "slow", function() {
                    // hide Div Pengajuan
                });
            }
        });

    </script>   
@endsection
    