@extends('layouts.borrower.master')

@section('title', 'Edit Penerima Dana')

@section('content')
    <!-- Main Container -->
    <main id="main-container">
        <style>
            .line { 
                display: flex; 
                flex-direction: row; 
            }
            .line:after { 
                content: ""; 
                flex: 1 1; 
                border-bottom: 1px solid #000; 
                margin: auto;
            } 
            .sweet_loader {
                width: 140px;
                height: 140px;
                margin: 0 auto;
                animation-duration: 0.5s;
                animation-timing-function: linear;
                animation-iteration-count: infinite;
                animation-name: ro;
                transform-origin: 50% 50%;
                transform: rotate(0) translate(0,0);
            }
            @keyframes ro {
                100% {
                    transform: rotate(-360deg) translate(0,0);
                }
            }
        </style>
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="">
                <div class="row">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="font-size-h3 pull-left ml-2">Ubah Kata Sandi</h1>            
                        </span>
                    </div>
                </div>
                <div class="row mt-5 pt-5">
                    <div class="col-md-12 mt-5 pt-5">
                        <div class="row">
                            <div class="col-12 col-md-12">
                                <!-- Progress Wizard Password -->
                                <div class="js-wizard-simple block">
                                    <!-- Form -->
                                    <form action="#" method="post">
                                        <div class="block-content block-content-full tab-content" style="min-height: 274px;">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <!-- notif  -->
                                                    <div class="alert alert-danger text-center p-10 h5 text-success d-none" id="notifg" role="alert">
                                                        Kata Sandi Gagal di Ganti
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <!-- notif -->
                                                    <div class="form-group">
                                                        <label for="install-admin-email">Kata Sandi Lama</label>
                                                        <input type="password" id="old_password"  class="form-control form-control-lg" placeholder="Kata Sandi lama...">
                                                    </div>
                                                    <div class="form-group mb-4">
                                                        <label for="side-overlay-profile-password">Kata Sandi Baru</label>

                                                        <div class="input-group">
                                                            <input type="password" class="form-control notallowCharacter" id="side-overlay-profile-password" name="side-overlay-profile-password" placeholder="Kata Sandi Baru.." disabled>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-asterisk"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-15">
                                                        <label for="side-overlay-profile-password-confirm">Konfirmasi Sandi Baru <span id='message'></span></label>
                                                        <div class="input-group">
                                                            <input type="password" class="form-control" id="side-overlay-profile-password-confirm" name="side-overlay-profile-password-confirm" placeholder="Konfirmasi Kata Sandi.." disabled>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="fa fa-asterisk"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>   

                                                    <div class="input-group"> 
                                                        <span id="8char" class="fa fa-times mr-2" style="color:#FF0004;"><span class="text-secondary pl-1">Minimal 8 Karakter</span></span>
                                                        <input type="hidden" id = "char">
                                                    
                                                        <span id="ucase" class="fa fa-times mr-2" style="color:#FF0004;"><span class="text-secondary pl-1">Huruf Besar</span></span>
                                                        <input type="hidden" id = "upper">
                                                    
                                                        <span id="lcase" class="fa fa-times mr-2" style="color:#FF0004;"><span class="text-secondary pl-1">Huruf Kecil</span> </span> 
                                                        <input type="hidden" id = "lower">
                                                    
                                                        <span id="num" class="fa fa-times mr-2" style="color:#FF0004;"> <span class="text-secondary pl-1">Karakter Angka</span></span>
                                                        <input type="hidden" id = "int">
                                                    
                                                        <span id="special" class="fa fa-times" style="color:#FF0004;"><span class="text-secondary pl-1">Karakter Spesial (@#$%..)</span> </span>
                                                        <input type="hidden" id = "spc">
                                                    </div> 

                                                    <div class="float-right">
                                                        <button type="button" class="btn btn-rounded btn-big btn-noborder btn-success min-width-150 mt-20 mb-20" id="btnsubmitpwd" disabled><span class="p-5">Simpan</span></button>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>  
                                    </form>
                                    <!-- END Form -->
                                </div>
                                <!-- END Progress Wizard Password -->                                

                            </div>
                            
                        </div>
                    </div>                           
                </div>
            </div>
        </div>
        <!-- END Page Content -->
    </main>
    <!-- END Main Container -->
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
        $('.notallowCharacter').on('input', function (event) { 
            this.value = this.value.replace(/ /g, '');
        });
        $('#side-overlay-profile-password, #side-overlay-profile-password-confirm').on('keyup', function () {
        if (true) {
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");
            var spchar = new RegExp("[!@#$%^&*]");

            if($("#side-overlay-profile-password").val().length >= 8){
                $("#8char").removeClass("fa fa-times");
                $("#8char").addClass("fa fa-check");
                $("#8char").css("color","#00A41E");
                $("#char").val(1);
            }else{
                $("#8char").removeClass("fa fa-check");
                $("#8char").addClass("fa fa-times");
                $("#8char").css("color","#FF0004");
                $("#char").val(0);
            }

            if(ucase.test($("#side-overlay-profile-password").val())){
                $("#ucase").removeClass("fa fa-times");
                $("#ucase").addClass("fa fa-check");
                $("#ucase").css("color","#00A41E");
                $("#upper").val(1);
            }else{
                $("#ucase").removeClass("fa fa-check");
                $("#ucase").addClass("fa fa-times");
                $("#ucase").css("color","#FF0004");
                $("#upper").val(0);
            }
            if(lcase.test($("#side-overlay-profile-password").val())){
                $("#lcase").removeClass("fa fa-times");
                $("#lcase").addClass("fa fa-check");
                $("#lcase").css("color","#00A41E");
                $("#lower").val(1);
            }else{
                $("#lcase").removeClass("fa fa-check");
                $("#lcase").addClass("fa fa-times");
                $("#lcase").css("color","#FF0004");
                $("#lower").val(0);
            }
            if(num.test($("#side-overlay-profile-password").val())){
                $("#num").removeClass("fa fa-times");
                $("#num").addClass("fa fa-check");
                $("#num").css("color","#00A41E");
                $("#int").val(1);
            }else{
                $("#num").removeClass("fa fa-check");
                $("#num").addClass("fa fa-times");
                $("#num").css("color","#FF0004");
                $("#int").val(0);
            }
            if(spchar.test($("#side-overlay-profile-password").val())){
                $("#special").removeClass("fa fa-times");
                $("#special").addClass("fa fa-check");
                $("#special").css("color","#00A41E");
                $("#spc").val(1);
            }else{
                $("#special").removeClass("fa fa-check");
                $("#special").addClass("fa fa-times");
                $("#special").css("color","#FF0004");
                $("#spc").val(0);
            }

            if($("#side-overlay-profile-password").val() == $("#side-overlay-profile-password-confirm").val() && $("#int").val()== 1 && $("#lower").val()== 1 && $("#upper").val()== 1 && $("#char").val()== 1 && $("#spc").val()==1 )
                {
                    $('#message').html(' - Sesuai standard minimum kata sandi').css('color', 'green');
                    $('#btnsubmitpwd').attr('disabled', false);
                }
                else{
                    $('#message').html(' - Tidak sama / tidak sesuai standar minimum kata sandi').css('color', 'red');
                    $('#btnsubmitpwd').attr('disabled', true);
                }
        } else 
            $('#message').html(' - Tidak Sesuai').css('color', 'red');
        });

        var timer, delay = 1000;
        $('#old_password').on('keyup', function(e) {
            if ($(this).val().length > 8) {
                var matchvalue = $(this).val();
                clearTimeout(timer);
                timer = setTimeout(function() {
                    Swal.fire({
                        html: '<h5>Cek kata sandi lama...</h5>',
                        onBeforeOpen: () => {
                            Swal.showLoading();
                            $.ajax({ 
                                url: '/borrower/cek_password',
                                method: 'post',
                                data: {"_token": "{{ csrf_token() }}", "matchvalue": matchvalue},
                                success : function(response){
                                    if (response.status == 0) {
                                        $("#old_password").focus();
                                        $("#old_password").addClass('is-invalid');
                                        Swal.fire({
                                            position: 'center',
                                            type: 'error',
                                            title: 'Kata sandi yang Anda Masukan Salah !',
                                            showConfirmButton: false,
                                            timer: 3000
                                        })
                                    } else {
                                        swal.close();
                                        $("#old_password").prop( "disabled", true );;
                                        $("#old_password").removeClass('is-invalid');
                                        $("#side-overlay-profile-password").removeAttr('disabled');
                                        $("#side-overlay-profile-password-confirm").removeAttr('disabled');
                                    }
                                },
                                error : function(response) {
                                    Swal.fire({
                                        position: 'center',
                                        type: 'error',
                                        title: 'Error',
                                        text: response.status,
                                        showConfirmButton: false,
                                    })
                                }
                            });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                }, delay );
            }
        });

        $("#btnsubmitpwd").on('click', function postinput(){
            var newpwd = $('#side-overlay-profile-password-confirm').val();
            var newpwdconfirm = $('#side-overlay-profile-password').val();
            if(newpwdconfirm != newpwd){
                Swal.fire({
                    position: 'center',
                    type: 'error',
                    title: 'Kata Sandi dan Konfirmasi Kata Sandi Tidak Sesuai..',
                    showConfirmButton: false,
                    timer: 5000
                })
                $('#btnsubmitpwd').attr('disabled', true);
            }else{
                var ucase = new RegExp("[A-Z]+");
                var lcase = new RegExp("[a-z]+");
                var num = new RegExp("[0-9]+");
                var spchar = new RegExp("[!@#$%^&*]");
        
                var char1 = 0;
                var upper1 = 0;
                var lower1 = 0;
                var int1 = 0;
                var spc1 = 0;
                    
                if(newpwd.length >= 8){
                    char1 = 1;
                }
                if(ucase.test(newpwd)){
                    upper1 = 1; 
                }
        
                if(lcase.test(newpwd)){
                    lower1 = 1;
                }

                if(num.test(newpwd)){
                    int1 = 1;
                }
                if(spchar.test(newpwd)){
                    spc1 = 1;
                }
                
                if( int1 == 1 && lower1 == 1 && upper1== 1 && char1 == 1 && spc1 ==1 )
                {
                    $.ajax({ 
                        url: '{{ route('ubah.proses') }}',
                        method: 'POST',
                        data: {"_token": "{{ csrf_token() }}", "newpwd": newpwd},
                        success : function(response){
                            if(response.status == 0){
                                Swal.fire({
                                    position: 'center',
                                    type: 'success',
                                    title: 'Sukses',
                                    text: 'Ubah Kata Sandi Berhasil, Silahkan Login Kembali..',
                                    showConfirmButton: false,
                                    timer: 5000
                                })

                                location.href = "/borrower/logout";
                            }else{
                                $("#notifg").show();
                                $("#old_password").val('');
                                $("#side-overlay-profile-password").val('');
                                $("#side-overlay-profile-password-confirm").val('');
                                $("#side-overlay-profile-password").attr('disabled',true);
                                $("#side-overlay-profile-password-confirm").attr('disabled',true);
                            }
                        },
                        error: function (response) {
                            Swal.fire({
                                position: 'center',
                                type: 'error',
                                title: 'Error',
                                text: 'Ubah Kata Sandi gagal, silahkan coba lagi..',
                                showConfirmButton: false,
                                timer: 5000
                            })
                        }
                    });
                }
                else{
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Minimal 8 Karakter/huruf dan wajib berisi huruf besar, huruf Kecil, bilangan angka, dan karakter spesial !',
                        showConfirmButton: false,
                        timer: 5000
                    })
                    $('#btnsubmitpwd').attr('disabled', true);
                    return;
                }
            }
        });
    </script>
@endsection
