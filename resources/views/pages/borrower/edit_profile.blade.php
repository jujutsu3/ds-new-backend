@extends('layouts.borrower.master')

@section('title', 'Edit Penerima Dana')

@section('content')
<!-- Main Container -->
<main id="main-container">
    <style>
        .line {
            display: flex;
            flex-direction: row;
        }

        .line:after {
            content: "";
            flex: 1 1;
            border-bottom: 1px solid #000;
            margin: auto;
        }

        .sweet_loader {
            width: 140px;
            height: 140px;
            margin: 0 auto;
            animation-duration: 0.5s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-name: ro;
            transform-origin: 50% 50%;
            transform: rotate(0) translate(0, 0);
        }

        @keyframes ro {
            100% {
                transform: rotate(-360deg) translate(0, 0);
            }
        }
    </style>
    <!-- Page Content -->
    <div id="detect-screen" class="content-full-right">
        <div class="">
            <div class="row">
                <div id="col" class="col-12 col-md-12 mt-30">
                    <span class="mb-10 pb-10 ">
                        <h1 class="font-size-h3 pull-left ml-2">Profile Saya</h1>
                    </span>
                </div>
            </div>
            <div class="row mt-5 pt-5">
                <div class="col-md-12 mt-5 pt-5">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <!-- START: Form -->
                            <form action="#" id="form_profile" method="post">
                                <!-- START: Informasi Pribadi -->
                                <div class="js-wizard-simple block border pt-0 layout" id="layout-pribadi">
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content pt-0"
                                        style="min-height: 274px;">
                                        <!-- satuBaris -->
                                        <input type="hidden" id="id" value="{{$id}}">
                                        <input type="hidden" id="brw_type" value={{$brw_type}}>
                                        <div class="row">
                                            <div class="col">
                                                <h6 class="pt-0" style="margin-top: -0.7rem"> <span
                                                        class="bg-white h6">Informasi Pribadi</span></h6>
                                            </div>
                                        </div>

                                        <!-- START: Layout Pribadi -->
                                        <div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <!-- START: Baris 1 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label for="wizard-progress2-namapengguna">Nama
                                                                        Pengguna <i class="text-danger">*</i></label>
                                                                    <input class="form-control" type="text" id="nama"
                                                                        maxlength="30"
                                                                        name="wizard-progress2-namapengguna"
                                                                        placeholder="Masukkan Nama Anda..." required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group row">
                                                                <label class="col-12">Jenis Kelamin <i
                                                                        class="text-danger">*</i></label>
                                                                <div class="col-12">
                                                                    <div class="input-group">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio"
                                                                                name="jns_kelamin" id="jns_kelamin1"
                                                                                value="1" required>
                                                                            <label class="form-check-label text-muted"
                                                                                for="jns_kelamin1">Laki - Laki</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio"
                                                                                name="jns_kelamin" id="jns_kelamin2"
                                                                                value="2">
                                                                            <label class="form-check-label text-muted"
                                                                                for="jns_kelamin2">Perempuan</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-ktp">Nomor KTP <i
                                                                        class="text-danger">*</i></label>
                                                                <input class="form-control" type="text"
                                                                    oninput="this.setCustomValidity('')"
                                                                    oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')"
                                                                    pattern=".{16,16}" maxlength="16"
                                                                    onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                    id="ktp" name="wizard-progress2-ktp"
                                                                    placeholder="Masukkan nomor KTP" required>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 1 -->

                                                        <!-- START: Baris 2 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-tempatlahir">Tempat Lahir
                                                                    <i class="text-danger">*</i></label>
                                                                <input class="form-control" type="text" maxlength="35"
                                                                    id="tempat_lahir"
                                                                    name="wizard-progress2-tempatlahir"
                                                                    placeholder="Masukkan tempat lahir" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-tempatlahir">Tanggal Lahir
                                                                    <i class="text-danger">*</i></label>
                                                                <input class="form-control" type="date"
                                                                    id="tanggal_lahir"
                                                                    name="wizard-progress2-tanggallahir"
                                                                    max="<?= date("Y-m-d"); ?>"
                                                                    placeholder="Masukkan tanggal lahir" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label id="lbl_no_hp">Nomor Telepon <i
                                                                        class="text-danger">*</i></label>
                                                                <div class="input-group">
                                                                    <div class="input-group-append">
                                                                        <span
                                                                            class="input-group-text input-group-text-dsi">
                                                                            +62 </span>
                                                                    </div>
                                                                    <input class="form-control checkNOHP validasiString"
                                                                        type="text"
                                                                        onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                        maxlength="13" id="telepon" name="telepon"
                                                                        placeholder="Masukkan nomor Telepon" disabled>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 2 -->

                                                        <!-- START: Baris 3 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group row">
                                                                <label class="col-12">Agama <i
                                                                        class="text-danger">*</i></label>
                                                                <div class="col-12">
                                                                    <select class="form-control custom-select"
                                                                        id="agama" name="agama" required>
                                                                        <option value="">Pilih Agama</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label
                                                                    for="wizard-progress2-pendidikanterakhir">Pendidikan
                                                                    Terakhir <i class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="pendidikan_terakhir" name="pendidikan_terakhir"
                                                                    required>
                                                                    <option value="">Pilih Pendidikan Terakhir</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label for="wizard-progress2-ibukandung">Nama Ibu
                                                                        Kandung</label>
                                                                    <input class="form-control" type="text"
                                                                        maxlength="30" id="ibukandung" name="ibukandung"
                                                                        placeholder="Masukkan Nama Ibu Kandung Anda...">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 3 -->

                                                        <!-- START: Baris 4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-npwp">Nomor NPWP</label>
                                                                <input class="form-control " type="text" id="npwp"
                                                                    name="npwp" maxlength="15" pattern=".{15,15}"
                                                                    oninput="this.setCustomValidity('')"
                                                                    oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')"
                                                                    onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                    placeholder="Masukkan nomor NPWP" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="form-group row">
                                                                <label class="col-12">Status Perkawinan <i
                                                                        class="text-danger">*</i></label>
                                                                <div class="col-12">
                                                                    <div class="input-group">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio"
                                                                                name="status_kawin" id="status_kawin0"
                                                                                value="1" required>
                                                                            <label class="form-check-label text-muted"
                                                                                for="status_kawin0">Sudah
                                                                                Menikah</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio"
                                                                                name="status_kawin" id="status_kawin1"
                                                                                value="2">
                                                                            <label class="form-check-label text-muted"
                                                                                for="status_kawin1">Belum
                                                                                Menikah</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio"
                                                                                name="status_kawin" id="status_kawin2"
                                                                                value="3">
                                                                            <label class="form-check-label text-muted"
                                                                                for="status_kawin2">Duda/Janda</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 4 -->

                                                        <!-- START: Baris 5 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-alamat">Alamat Lengkap <i
                                                                        class="text-danger">*</i></label>
                                                                <textarea class="form-control form-control-lg"
                                                                    maxlength="90" id="alamat" name="alamat" rows="6"
                                                                    placeholder="Masukkan alamat lengkap Anda.."
                                                                    required></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-namapengguna">Provinsi <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select" id="provinsi"
                                                                    name="provinsi" required>
                                                                    <option value="">Pilih Provinsi</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kota">Kota/Kabupaten <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select" id="kota"
                                                                    name="kota" required>
                                                                    <option value="">Pilih Kota / Kabupaten</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 5 -->

                                                        <!-- START: Baris 6 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kecamatan">Kecamatan <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="kecamatan" name="kecamatan" required>
                                                                    <option value="">Pilih Kecamatan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kelurahan">Kelurahan <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="kelurahan" name="kelurahan" required>
                                                                    <option value="">Pilih Kelurahan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kode_pos">Kode Pos <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select" id="kode_pos"
                                                                    name="kode_pos" required>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 6 -->

                                                        <!-- START: Baris 7 -->
                                                        <div class="col-md-12">
                                                            <div class="form-group row">
                                                                <label class="col-12">Status Kepemilikan Rumah <i
                                                                        class="text-danger">*</i></label>
                                                                <div class="col-12">
                                                                    <div class="input-group">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio"
                                                                                name="status_rumah" id="status_rumah0"
                                                                                value="1" required>
                                                                            <label class="form-check-label text-muted"
                                                                                for="status_rumah0">Milik
                                                                                Pribadi</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio"
                                                                                name="status_rumah" id="status_rumah1"
                                                                                value="2">
                                                                            <label class="form-check-label text-muted"
                                                                                for="status_rumah1">Sewa Atau
                                                                                Kontrak</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 7 -->

                                                        <!-- START: Alamat Domisili Title -->
                                                        <div class="col-12 mb-4">
                                                            <div class="form-check form-check-inline line">
                                                                <input class="form-check-input" type="checkbox"
                                                                    name="domisili_status" id="domisili_status"
                                                                    value="1">
                                                                <label class="form-check-label text-black h6"
                                                                    for="domisili_status">Alamat Domisili Sama dengan
                                                                    KTP &nbsp</label>
                                                            </div>
                                                        </div>
                                                        <!-- END: Alamat Domisili Title -->

                                                        <!-- START: Alamat Domisili -->
                                                        <div class="col-12" id="domisili_alamat_lengkap">
                                                            <div class="row">
                                                                <!-- START: Baris 8 -->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-alamat">Alamat
                                                                            Lengkap</label>
                                                                        <textarea class="form-control form-control-lg"
                                                                            maxlength="90" id="domisili_alamat"
                                                                            name="domisili_alamat" rows="6"
                                                                            placeholder="Masukkan alamat lengkap Anda.."></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-namapengguna">Provinsi</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_provinsi"
                                                                            name="domisili_provinsi">
                                                                            <option value="">Pilih Provinsi</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-kota">Kota/Kabupaten</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kota" name="domisili_kota">
                                                                            <option value="">Pilih Kota/Kabupaten
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!-- END: Baris 8 -->

                                                                <!-- START: Baris 9 -->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-kecamatan">Kecamatan</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kecamatan"
                                                                            name="domisili_kecamatan">
                                                                            <option value="">Pilih Kecamatan</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-kelurahan">Kelurahan</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kelurahan"
                                                                            name="domisili_kelurahan">
                                                                            <option value="">Pilih Kelurahan</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-kode_pos">Kode
                                                                            Pos</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kode_pos"
                                                                            name="domisili_kode_pos">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!-- END: Baris 9 -->

                                                                <!-- START: Baris 10 -->
                                                                <div class="col-md-12">
                                                                    <div class="form-group row">
                                                                        <label class="col-12">Status Kepemilikan
                                                                            Rumah</label>
                                                                        <div class="col-12">
                                                                            <div class="input-group">
                                                                                <div
                                                                                    class="form-check form-check-inline">
                                                                                    <input class="form-check-input"
                                                                                        type="radio"
                                                                                        name="domisili_status_rumah"
                                                                                        id="domisili_status_rumah0"
                                                                                        value="1">
                                                                                    <label
                                                                                        class="form-check-label text-muted"
                                                                                        for="domisili_status_rumah0">Milik
                                                                                        Pribadi</label>
                                                                                </div>
                                                                                <div
                                                                                    class="form-check form-check-inline">
                                                                                    <input class="form-check-input"
                                                                                        type="radio"
                                                                                        name="domisili_status_rumah"
                                                                                        id="domisili_status_rumah1"
                                                                                        value="2">
                                                                                    <label
                                                                                        class="form-check-label text-muted"
                                                                                        for="domisili_status_rumah1">Sewa
                                                                                        Atau Kontrak</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- END: Baris 10 -->
                                                            </div>
                                                        </div>
                                                        <!-- END: Alamat Domisili -->

                                                        <!-- START: Rekening Rekanan Title -->
                                                        <div class="col-12 mb-4">
                                                            <div class="form-check form-check-inline line">
                                                                <input class="form-check-input" type="checkbox"
                                                                    name="bank_rekanan" id="bank_rekanan" value="0"
                                                                    disabled>
                                                                <label class="form-check-label text-black h6"
                                                                    for="bank_rekanan">Sudah Memiliki Rekening di Bank
                                                                    Rekanan ? &nbsp</label>
                                                            </div>
                                                        </div>
                                                        <!-- END: Rekening Rekanan Title -->

                                                        <!-- START: Rekening Rekanan -->
                                                        <div class="col-12" id="informasi_bank_rekanan">
                                                            <div class="row">
                                                                <!-- START: Baris 11 -->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-namapemilikrekening">Nama
                                                                            Pemilik Rekening <i
                                                                                class="text-danger">*</i></label>
                                                                        <input class="form-control" type="text"
                                                                            id="namapemilikrekening"
                                                                            name="namapemilikrekening"
                                                                            placeholder="Masukkan Nama Pemilik Rekening"
                                                                            disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-norekening">No
                                                                            Rekening <i
                                                                                class="text-danger">*</i></label>
                                                                        <input class="form-control" type="text"
                                                                            onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                            id="norekening" name="norekening"
                                                                            placeholder="Masukkan No Rekening" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-bank">Bank <i
                                                                                class="text-danger">*</i></label>
                                                                        <select class="form-control custom-select"
                                                                            id="bank" name="bank" disabled>
                                                                            <option value="">Pilih Bank</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!-- END: Baris 11 -->
                                                            </div>
                                                        </div>
                                                        <!-- END: Rekening Rekanan -->

                                                        <!-- START: Informasi Lain Lain Title -->
                                                        <div class="col-12 mb-4">
                                                            <div class="form-check form-check-inline line">
                                                                <label class="form-check-label text-black h6"
                                                                    for="bank_rekanan">Informasi Lain Lain &nbsp</label>
                                                            </div>
                                                        </div>
                                                        <!-- END: Informasi Lain Lain -->

                                                        <!-- START: Baris 12 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-pekerjaan">Pekerjaan <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="pekerjaan" name="pekerjaan" required>
                                                                    <option value="">Pilih Pekerjaan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-bidang_pekerjaan">Bidang
                                                                    Pekerjaan <i class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="bidang_pekerjaan" name="bidang_pekerjaan"
                                                                    required>
                                                                    <option value="">Pilih Bidang Pekerjaan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-bidang_online">Bidang
                                                                    Pekerjaan Online <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="bidang_online" name="bidang_online" required>
                                                                    <option value="">pilih Bidang Pekerjaan Online
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 12 -->

                                                        <!-- START: Baris 13 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-pengalamankerja">Pengalaman
                                                                    Kerja <i class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="pengalaman_kerja" name="pengalaman_kerja"
                                                                    required>
                                                                    <option value="">Pilih Pengalaman Kerja</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label
                                                                    for="wizard-progress2-pendapatan_bulanan">Pendapatan
                                                                    Bulanan <i class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="pendapatan_bulanan" name="pendapatan_bulanan"
                                                                    required>
                                                                    <option value="">Pilih Pendapatan Bulanan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 13 -->

                                                        <!-- START: Foto Title -->
                                                        <div class="col-12">
                                                            <h6 class="line mt-3">Foto &nbsp</h6>
                                                        </div>
                                                        <!-- END: Foto Title -->

                                                        <!-- START: Foto -->
                                                        <div class="col-md-3 col-sm-6 imgUp">
                                                            <div class="col imgUp" id="foto_diri">
                                                                <div class="d-flex flex-column">
                                                                    <div class="mx-auto">
                                                                        <label
                                                                            class="font-weight-bold ml-0 flex-row">Foto
                                                                            Diri <i class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="mx-auto">
                                                                        {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                        <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($brw_pic) ? route('getUserFile', ['filename' => str_replace('/',':', $brw_pic)]) .'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px">
                                                                        {{-- <img class="imagePreview img-fluid rounded" src="{{ !empty($brw_pic) ? asset('/storage').'/'.$brw_pic.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                        width="150px" height="145px"> --}}
                                                                    </div>
                                                                    <div class="mt-2 mx-auto" id="camera_foto_diri">
                                                                        <label class="btn btn-primary ml-0"><i
                                                                                class="fa fa-camera"></i> Ubah
                                                                            Foto</label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col imgUp d-none" id="take_camera_foto_diri">
                                                                <div class="d-flex flex-column">
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0">Foto Diri
                                                                            <i class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="d-flex justify-content-center">
                                                                        <img style="z-index:2;"
                                                                            class="position-absolute rounded"
                                                                            id="user-guide"
                                                                            src="{{URL::to('assets/img/user-guide.png')}}"
                                                                            width="150px" height="145px" alt="guide">
                                                                        <div style="z-index:1 !important;"
                                                                            class="img-fluid rounded"
                                                                            id="my_camera_foto"></div>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <input class="btn btn-primary mt-2"
                                                                            type="button" value="Ambil Foto"
                                                                            onClick="take_snapshot_foto_diri()">
                                                                        <input type="hidden" name="image_foto_diri"
                                                                            id="user_foto_diri" class="image-tag">
                                                                        <input type="hidden" name="url_pic_brw"
                                                                            id="url_pic_brw">
                                                                        <input type="hidden" name="brw_pic" id="brw_pic"
                                                                            value="{{$brw_pic}}">
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0 mt-4 d-none"
                                                                            id="label_hasil_foto_diri">Hasil</label>
                                                                        <div id="results_foto_diri"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-6 imgUp">
                                                            <div class="col imgUp text-center" id="foto_ktp">
                                                                <div class="d-flex flex-column">
                                                                    <div class="mx-auto">
                                                                        <label class="font-weight-bold ml-0">Foto KTP <i
                                                                                class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="mx-auto">
                                                                        {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                        <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($brw_pic_ktp) ? route('getUserFile', ['filename' => str_replace('/',':', $brw_pic_ktp)]) .'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px">
                                                                        {{-- <img class="imagePreview img-fluid rounded" src="{{ !empty($brw_pic_ktp) ? asset('/storage').'/'.$brw_pic_ktp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                        width="150px" height="145px"> --}}
                                                                    </div>
                                                                    <div class="mt-2 mx-auto" id="camera_foto_ktp">
                                                                        <label class="btn btn-primary ml-0"><i
                                                                                class="fa fa-camera"></i> Ubah
                                                                            Foto</label>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col imgUp d-none" id="take_camera_foto_ktp">
                                                                <div class="d-flex flex-column">
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0">Foto KTP <i
                                                                                class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="d-flex justify-content-center">
                                                                        <img style="z-index:2;"
                                                                            class="position-absolute rounded"
                                                                            id="user-guide"
                                                                            src="{{URL::to('assets/img/guide-ktp.png')}}"
                                                                            width="150px" height="145px" alt="guide">
                                                                        <div style="z-index:1 !important;"
                                                                            class="img-fluid rounded"
                                                                            id="my_camera_ktp"></div>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <input class="btn btn-primary mt-2"
                                                                            type="button" value="Ambil Foto"
                                                                            onClick="take_snapshot_foto_ktp()">
                                                                        <input type="hidden" name="image_foto_ktp"
                                                                            id="user_foto_ktp" class="image-tag">
                                                                        <input type="hidden" name="url_pic_brw_ktp"
                                                                            id="url_pic_brw_ktp">
                                                                        <input type="hidden" name="brw_pic_ktp"
                                                                            id="brw_pic_ktp" value="{{$brw_pic_ktp}}">
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0 mt-4 d-none"
                                                                            id="label_hasil_foto_ktp">Hasil</label>
                                                                        <div id="results_foto_ktp"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-6 imgUp">
                                                            <div class="col imgUp text-center" id="foto_ktp_diri">
                                                                <div class="d-flex flex-column">
                                                                    <div class="mx-auto">
                                                                        <label class="font-weight-bold ml-0">Foto Diri &
                                                                            KTP <i class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="mx-auto">
                                                                        {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                        <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($brw_pic_user_ktp) ? route('getUserFile', ['filename' => str_replace('/',':', $brw_pic_user_ktp)]) .'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px">
                                                                        {{-- <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($brw_pic_user_ktp) ? asset('/storage').'/'.$brw_pic_user_ktp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                        width="150px" height="145px"> --}}
                                                                    </div>
                                                                    <div class="mt-2 mx-auto" id="camera_foto_ktp_diri">
                                                                        <label class="btn btn-primary ml-0"><i
                                                                                class="fa fa-camera"></i> Ubah
                                                                            Foto</label>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div class="col imgUp d-none"
                                                                id="take_camera_foto_ktp_diri">
                                                                <div class="d-flex flex-column">
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0">Foto Diri &
                                                                            KTP <i class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="d-flex justify-content-center">
                                                                        <img style="z-index:2;"
                                                                            class="position-absolute rounded"
                                                                            id="user-guide"
                                                                            src="{{URL::to('assets/img/guide-diridanktp.png')}}"
                                                                            alt="guide" width="150px" height="145px">
                                                                        <div style="z-index:1 !important;"
                                                                            class="img-fluid rounded"
                                                                            id="my_camera_ktp_diri"></div>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <input class="btn btn-primary mt-2"
                                                                            type="button" value="Ambil Foto"
                                                                            onClick="take_snapshot_foto_ktp_diri()">
                                                                        <input type="hidden" name="image_foto_ktp_diri"
                                                                            id="user_foto_ktp_diri" class="image-tag">
                                                                        <input type="hidden"
                                                                            name="url_pic_brw_dengan_ktp"
                                                                            id="url_pic_brw_dengan_ktp">
                                                                        <input type="hidden" name="brw_pic_user_ktp"
                                                                            id="brw_pic_user_ktp"
                                                                            value="{{$brw_pic_user_ktp}}">
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0 mt-4 d-none"
                                                                            id="label_hasil_foto_ktp_diri">Hasil</label>
                                                                        <div id="results_foto_ktp_diri"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3 col-sm-6 imgUp">
                                                            <div class="col imgUp text-center" id="foto_npwp">
                                                                <div class="d-flex flex-column">
                                                                    <div class="mx-auto">
                                                                        <label class="font-weight-bold ml-0">Foto NPWP
                                                                            <i class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="mx-auto">
                                                                        {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                        <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($brw_pic_npwp) ? route('getUserFile', ['filename' => str_replace('/',':', $brw_pic_npwp)]) .'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px">
                                                                        {{-- <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($brw_pic_npwp) ? asset('/storage').'/'.$brw_pic_npwp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                        width="150px" height="145px"> --}}
                                                                    </div>

                                                                    <div class="mt-2 mx-auto" id="camera_foto_npwp">
                                                                        <label class="btn btn-primary ml-0"><i
                                                                                class="fa fa-camera"></i> Ubah
                                                                            Foto</label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col imgUp d-none" id="take_camera_foto_npwp">
                                                                <div class="d-flex flex-column">
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0">Foto NPWP
                                                                            <i class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="d-flex justify-content-center">
                                                                        <img style="z-index:2;"
                                                                            class="position-absolute rounded"
                                                                            id="user-guide"
                                                                            src="{{URL::to('assets/img/guide-ktp.png')}}"
                                                                            alt="guide" width="150px" height="145px">
                                                                        <div style="z-index:1 !important;"
                                                                            class="img-fluid rounded"
                                                                            id="my_camera_npwp"></div>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <input class="btn btn-primary mt-2"
                                                                            type="button" value="Ambil Foto"
                                                                            onClick="take_snapshot_foto_npwp()">
                                                                        <input type="hidden" name="image_foto_npwp"
                                                                            id="user_foto_npwp" class="image-tag">
                                                                        <input type="hidden" name="url_pic_brw_npwp"
                                                                            id="url_pic_brw_npwp">
                                                                        <input type="hidden" name="brw_pic_npwp"
                                                                            id="brw_pic_npwp" value="{{$brw_pic_npwp}}">
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0 mt-4 d-none"
                                                                            id="label_hasil_foto_npwp">Hasil</label>
                                                                        <div id="results_foto_npwp"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 text-left mt-4 mb-4">
                                                            <label class="font-weight-bold text-black ml-0">Format file
                                                                .jpg, .jpeg, .gif, dan .png</label>
                                                        </div>
                                                        <!-- END: Foto -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- END: Layout Pribadi -->
                                    </div>
                                </div>
                                <!-- END: Informasi Pribadi -->

                                <!-- START: Informasi Pasangan -->
                                <div class="js-wizard-simple block border pt-0 layout d-none" id="layout-pasangan">
                                    <div class="block-content block-content-full tab-content pt-0"
                                        style="min-height: 274px;">
                                        <div class="row">
                                            <div class="col">
                                                <h6 class="pt-0" style="margin-top: -0.7rem"> <span class="h6"
                                                        style="background: linear-gradient(180deg, #F7F7FA 50%, #ffffff 50%);">Informasi
                                                        Pasangan</span></h6>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <!-- START: Baris 1 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label for="nama_pasangan">Nama Pasangan <i
                                                                class="text-danger">*</i></label>
                                                        <input class="form-control" type="text" id="nama_pasangan"
                                                            maxlength="30" name="nama_pasangan"
                                                            value="{{ $pasanganBrw->nama }}"
                                                            placeholder="Masukkan Nama pasangan Anda...">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label class="col-12">Jenis Kelamin <i
                                                            class="text-danger">*</i></label>
                                                    <div class="col-12">
                                                        <div class="input-group">
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio"
                                                                    name="jns_kelamin_pasangan"
                                                                    id="jns_kelamin_pasangan1" value="1">
                                                                <label class="form-check-label text-muted"
                                                                    for="jns_kelamin_pasangan1">Laki - Laki</label>
                                                            </div>
                                                            <div class="form-check form-check-inline">
                                                                <input class="form-check-input" type="radio"
                                                                    name="jns_kelamin_pasangan"
                                                                    id="jns_kelamin_pasangan2" value="2">
                                                                <label class="form-check-label text-muted"
                                                                    for="jns_kelamin_pasangan2">Perempuan</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="ktp_pasangan">Nomor KTP <i
                                                            class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" maxlength="16"
                                                        onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                        id="ktp_pasangan" name="ktp_pasangan"
                                                        value="{{ $pasanganBrw->ktp }}"
                                                        placeholder="Masukkan nomor KTP">
                                                </div>
                                            </div>
                                            <!-- END: Baris 1 -->

                                            <!-- START: Baris 2 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="tempat_lahir_pasangan">Tempat Lahir <i
                                                            class="text-danger">*</i></label>
                                                    <input class="form-control" type="text" id="tmpt_lahir_pasangan"
                                                        maxlength="35" name="tmpt_lahir_pasangan"
                                                        value="{{ $pasanganBrw->tempat_lahir }}"
                                                        placeholder="Masukkan tempat lahir">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="tanggal_lahir_pasangan">Tanggal Lahir <i
                                                            class="text-danger">*</i></label>
                                                    <input class="form-control" type="date" id="tgl_lahir_pasangan"
                                                        name="tgl_lahir_pasangan" value="{{ $pasanganBrw->tgl_lahir }}"
                                                        max="<?= date("Y-m-d"); ?>"
                                                        placeholder="Masukkan tanggal lahir">
                                                </div>
                                            </div>
                                            {{-- kall --}}
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_no_hp">Nomor Telepon <i
                                                            class="text-danger">*</i></label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text input-group-text-dsi"> +62
                                                            </span>
                                                        </div>
                                                        {{-- sall --}}
                                                        <input class="form-control checkNOHP validasiString" type="text"
                                                            onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                            maxlength="13" id="telepon_pasangan" name="telepon_pasangan"
                                                            placeholder="Masukkan nomor Telepon">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END: Baris 2 -->

                                            <!-- START: Baris 3 -->
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label class="col-12">Agama <i class="text-danger">*</i></label>
                                                    <div class="col-12">
                                                        <select class="form-control custom-select" id="agama_pasangan"
                                                            name="agama_pasangan">
                                                            <option value="">Pilih Agama</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-pendidikanterakhir">Pendidikan Terakhir
                                                        <i class="text-danger">*</i></label>
                                                    <select class="form-control custom-select"
                                                        id="pendidikan_terakhir_pasangan"
                                                        name="pendidikan_terakhir_pasangan">
                                                        <option value="">Pilih Pendidikan Terakhir</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="npwp_pasangan">Nomor NPWP <i
                                                            class="text-danger">*</i></label>
                                                    <input class="form-control " type="text" id="npwp_pasangan"
                                                        name="npwp_pasangan"
                                                        onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                        maxlength="15" value="{{$pasanganBrw->npwp }}"
                                                        placeholder="Masukkan nomor NPWP">
                                                </div>
                                            </div>
                                            <!-- END: Baris 3 -->

                                            <!-- START: Baris 4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="alamat_pasangan">Alamat Lengkap <i
                                                            class="text-danger">*</i></label>
                                                    <textarea class="form-control form-control-lg" maxlength="90"
                                                        id="alamat_pasangan" name="alamat_pasangan" rows="6"
                                                        placeholder="Masukkan alamat lengkap pasangan Anda..">{{ $pasanganBrw->alamat }}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="provinsi_pasangan">Provinsi <i
                                                            class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="provinsi_pasangan"
                                                        name="provinsi_pasangan">
                                                        <option value="">Pilih Provinsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kota_pasangan">Kota/Kabupaten <i
                                                            class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kota_pasangan"
                                                        name="kota_pasangan">
                                                        <option value="">Pilih Kota / Kabupaten</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- END: Baris 4 -->

                                            <!-- START: Baris 5 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kecamatan_pasangan">Kecamatan <i
                                                            class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kecamatan_pasangan"
                                                        name="kecamatan_pasangan">
                                                        <option value="">Pilih Kecamatan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kelurahan_pasangan">Kelurahan <i
                                                            class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kelurahan_pasangan"
                                                        name="kelurahan_pasangan">
                                                        <option value="">Pilih Kelurahan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="kode_pos_pasangan">Kode Pos <i
                                                            class="text-danger">*</i></label>
                                                    <select class="form-control custom-select" id="kode_pos_pasangan"
                                                        name="kode_pos_pasangan">
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- END: Baris 5 -->
                                        </div>
                                    </div>
                                </div>
                                <!-- END: Informasi Pasangan -->

                                <!-- START: Ahli Waris -->
                                <!-- takeout 8maret2022-->
                                <!-- END: Ahli Waris -->

                                <!-- START: Button Submit Layout Pribadi -->
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <button type="submit" name="form_profile_submit" value="pribadi"
                                            class="btn btn-rounded btn-big btn-noborder btn-success min-width-150 mt-20 mb-20"
                                            id="btnsubmitpribadi"><span class="p-5">Ubah</span></button>
                                    </div>
                                </div>
                                <!-- END: Button Submit Layout Pribadi -->
                            </form>
                            <!-- END: Form -->

                            <!-- START: Form Badan Hukum -->
                            <form action="#" id="form_bdn_hukum" method="POST">
                                <!-- START: Badan Hukum -->
                                <div class="js-wizard-simple block border pt-0 layout" id="layout-badanhukum">
                                    <!-- Steps Content -->
                                    <div class="block-content block-content-full tab-content pt-0"
                                        style="min-height: 274px;">
                                        <!-- satuBaris -->
                                        <div class="row">
                                            <div class="col">
                                                <h6 class="pt-0" style="margin-top: -0.7rem"> <span
                                                        class="bg-white h6">Informasi Badan Hukum</span></h6>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <!-- satuBaris -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-namabadanhukum">Nama Badan
                                                                    Hukum <i class="text-danger">*</i></label>
                                                                <input class="form-control " type="text"
                                                                    id="nm_bdn_hukum" maxlength="30"
                                                                    name="wizard-progress2-namabadanhukum"
                                                                    placeholder="Masukkan Nama Perusahaan anda..."
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="nib">Nomor Surat Ijin <i
                                                                        class="text-danger">*</i></label>
                                                                <input class="form-control" type="text" id="nib"
                                                                    name="nib" placeholder="Masukkan nomor NPWP"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-npwp">NPWP <i
                                                                        class="text-danger">*</i></label>
                                                                <input class="form-control" type="text"
                                                                    pattern=".{15,15}"
                                                                    oninput="this.setCustomValidity('')"
                                                                    oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')"
                                                                    onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                    maxlength="15" id="npwp_bdn_hukum"
                                                                    name="wizard-progress2-npwp"
                                                                    placeholder="Masukkan nomor NPWP" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="no_akta_pendirian">No Akta Pendirian <i
                                                                        class="text-danger">*</i></label>
                                                                <input class="form-control" type="text"
                                                                    id="no_akta_pendirian" name="no_akta_pendirian"
                                                                    placeholder="Masukkan nomor NPWP" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="tgl_berdiri">Tanggal Berdiri <i
                                                                        class="text-danger">*</i></label>
                                                                <input class="form-control" type="date" id="tgl_berdiri"
                                                                    name="tgl_berdiri" placeholder="Masukkan nomor NPWP"
                                                                    required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label id="lbl_no_hp">Nomor TLP Perusahaan <i
                                                                    class="text-danger">*</i></label>
                                                            <div class="input-group">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text input-group-text-dsi">
                                                                        021 </span>
                                                                </div>
                                                                <input class="form-control checkNOHP validasiString"
                                                                    type="text"
                                                                    onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                    maxlength="13" id="hpPendaftar" name="hpPendaftar"
                                                                    placeholder="Masukkan nomor Handphone" disabled>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 imgUp ml-0">
                                                            <div class="col imgUp text-center" id="foto_npwp_bdn_hukum">
                                                                <div class="d-flex flex-column">
                                                                    <div class="mx-auto">
                                                                        <label class="font-weight-bold ml-0">Foto NPWP
                                                                            Perusahaan <i
                                                                                class="text-danger">*</i></label>
                                                                    </div>
                                                                    <div class="mx-auto">
                                                                        {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                        <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($foto_npwp_perusahaan) ? route('getUserFile', ['filename' => str_replace('/',':', $foto_npwp_perusahaan)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                            width="150px" height="145px">
                                                                        {{-- <img class="imagePreview img-fluid rounded"
                                                                            src="{{ !empty($foto_npwp_perusahaan) ? asset('/storage').'/'.$foto_npwp_perusahaan.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                        width="150px" height="145px"> --}}
                                                                    </div>

                                                                    <div class="mt-2 mx-auto"
                                                                        id="camera_foto_npwp_bdn_hukum">
                                                                        <label class="btn btn-primary ml-0"><i
                                                                                class="fa fa-camera"></i> Ubah
                                                                            Foto</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col imgUp d-none"
                                                                id="take_camera_foto_npwp_bdn_hukum">
                                                                <div class="d-flex flex-column">
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0">Foto NPWP
                                                                            Perusahaan</label>
                                                                    </div>
                                                                    <div class="d-flex justify-content-center">
                                                                        <img style="z-index:2;"
                                                                            class="position-absolute rounded"
                                                                            id="user-guide"
                                                                            src="{{URL::to('assets/img/guide-ktp.png')}}"
                                                                            alt="guide" width="150px" height="145px">
                                                                        <div style="z-index:1 !important;"
                                                                            class="img-fluid rounded"
                                                                            id="my_camera_npwp_bdn_hukum"></div>
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <input class="btn btn-primary mt-2"
                                                                            type="button" value="Ambil Foto"
                                                                            onClick="take_snapshot_foto_npwp_bdn_hukum()">
                                                                        <input type="hidden"
                                                                            name="image_foto_npwp_bdn_hukum"
                                                                            id="user_foto_npwp_bdn_hukum"
                                                                            class="image-tag">
                                                                        <input type="hidden"
                                                                            name="url_pic_brw_npwp_bdn_hukum"
                                                                            id="url_pic_brw_npwp_bdn_hukum">
                                                                        <input type="hidden"
                                                                            name="brw_pic_npwp_bdn_hukum"
                                                                            id="brw_pic_npwp_bdn_hukum"
                                                                            value="{{$foto_npwp_perusahaan}}">
                                                                    </div>
                                                                    <div class="text-center">
                                                                        <label class="font-weight-bold ml-0 mt-4 d-none"
                                                                            id="label_hasil_foto_npwp_bdn_hukum">Hasil</label>
                                                                        <div id="results_foto_npwp_bdn_hukum"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4"></div>
                                                        <div class="col-md-4"></div>
                                                        <!-- baris pemisah -->
                                                        {{-- alamat bdn hukum --}}
                                                        <!-- START: Baris 5 -->
                                                        <div class="col-12">
                                                            <h6 class="line mt-4">Alamat sesuai dengan AKTA &nbsp</h6>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-alamat">Alamat Lengkap <i
                                                                        class="text-danger">*</i></label>
                                                                <textarea class="form-control form-control-lg"
                                                                    maxlength="90" id="alamat_bdn_hukum" name="alamat"
                                                                    rows="6"
                                                                    placeholder="Masukkan alamat lengkap Anda.."
                                                                    required></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-namapengguna">Provinsi <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="provinsi_bdn_hukum" name="provinsi" required>
                                                                    <option value="">Pilih Provinsi</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kota">Kota/Kabupaten <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="kota_bdn_hukum" name="kota" required>
                                                                    <option value="">Pilih Kota / Kabupaten</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 5 -->

                                                        <!-- START: Baris 6 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kecamatan">Kecamatan <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="kecamatan_bdn_hukum" name="kecamatan" required>
                                                                    <option value="">Pilih Kecamatan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kelurahan">Kelurahan <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="kelurahan_bdn_hukum" name="kelurahan" required>
                                                                    <option value="">Pilih Kelurahan</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-kode_pos">Kode Pos <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="kode_pos_bdn_hukum" name="kode_pos" required>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 6 -->

                                                        <!-- START: Alamat Domisili Title -->
                                                        <div class="col-12 mb-4">
                                                            <div class="form-check form-check-inline line">
                                                                <input class="form-check-input" type="checkbox"
                                                                    name="domisili_status"
                                                                    id="domisili_status_bdn_hukum" value="1">
                                                                <label class="form-check-label text-black h6"
                                                                    for="domisili_status">Alamat Domisili Sama dengan
                                                                    AKTA &nbsp</label>
                                                            </div>
                                                        </div>
                                                        <!-- END: Alamat Domisili Title -->

                                                        <!-- START: Alamat Domisili -->
                                                        <div class="col-12" id="domisili_alamat_lengkap">
                                                            <div class="row">
                                                                <!-- START: Baris 8 -->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-alamat">Alamat
                                                                            Lengkap</label>
                                                                        <textarea class="form-control form-control-lg"
                                                                            maxlength="90"
                                                                            id="domisili_alamat_bdn_hukum"
                                                                            name="domisili_alamat" rows="6"
                                                                            placeholder="Masukkan alamat lengkap Anda.."></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-namapengguna">Provinsi</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_provinsi_bdn_hukum"
                                                                            name="domisili_provinsi">
                                                                            <option value="">Pilih Provinsi</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-kota">Kota/Kabupaten</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kota_bdn_hukum"
                                                                            name="domisili_kota">
                                                                            <option value="">Pilih Kota/Kabupaten
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!-- END: Baris 8 -->

                                                                <!-- START: Baris 9 -->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-kecamatan">Kecamatan</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kecamatan_bdn_hukum"
                                                                            name="domisili_kecamatan">
                                                                            <option value="">Pilih Kecamatan</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label
                                                                            for="wizard-progress2-kelurahan">Kelurahan</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kelurahan_bdn_hukum"
                                                                            name="domisili_kelurahan">
                                                                            <option value="">Pilih Kelurahan</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-kode_pos">Kode
                                                                            Pos</label>
                                                                        <select class="form-control custom-select"
                                                                            id="domisili_kode_pos_bdn_hukum"
                                                                            name="domisili_kode_pos">
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!-- END: Baris 9 -->

                                                            </div>
                                                        </div>


                                                        {{-- START: Rekening Rekanan badan hukum & lain lain --}}
                                                        <!-- START: Rekening Rekanan Title -->
                                                        <div class="col-12 mb-4">
                                                            <div class="form-check form-check-inline line">
                                                                <input class="form-check-input" type="checkbox"
                                                                    name="bank_rekanan_bdn_hukum"
                                                                    id="bank_rekanan_bdn_hukum" value="0" disabled>
                                                                <label class="form-check-label text-black h6"
                                                                    for="bank_rekanan_bdn_hukum">Sudah Memiliki Rekening
                                                                    di Bank Rekanan ? &nbsp</label>
                                                            </div>
                                                        </div>
                                                        <!-- END: Rekening Rekanan Title -->

                                                        <!-- START: Rekening Rekanan -->
                                                        <div class="col-12" id="informasi_bank_rekanan_bdn_hukum">
                                                            <div class="row">
                                                                <!-- START: Baris 11 -->
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="namapemilikrekening_bdnhkm">Nama
                                                                            Pemilik Rekening</label>
                                                                        <input class="form-control" type="text"
                                                                            id="namapemilikrekening_bdnhkm"
                                                                            name="namapemilikrekening_bdnhkm"
                                                                            placeholder="Masukkan Nama Pemilik Rekening"
                                                                            disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="wizard-progress2-norekening">No
                                                                            Rekening</label>
                                                                        <input class="form-control" type="text"
                                                                            onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                            id="norekening_bdnhkm"
                                                                            name="norekening_bdnhkm"
                                                                            placeholder="Masukkan No Rekening" disabled>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="bank_bdnhkm">Bank</label>
                                                                        <select class="form-control custom-select"
                                                                            id="bank_bdnhkm" name="bank_bdnhkm"
                                                                            disabled>
                                                                            <option value="">Pilih Bank</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <!-- END: Baris 11 -->
                                                            </div>
                                                        </div>
                                                        <!-- END: Rekening Rekanan -->

                                                        <!-- START: Informasi Lain Lain Title -->
                                                        <div class="col-12 mb-4">
                                                            <div class="form-check form-check-inline line">
                                                                <label class="form-check-label text-black h6"
                                                                    for="bank_rekanan">Informasi Lain Lain &nbsp</label>
                                                            </div>
                                                        </div>
                                                        <!-- END: Informasi Lain Lain -->

                                                        <!-- START: Baris 12 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-namapengguna">Bidang Usahaa
                                                                    <i class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="bidang_pekerjaan_bdn_hukum"
                                                                    name="bidang_pekerjaan_bdn_hukum" required>
                                                                    <option value="">Pilih Bidang Usaha</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="wizard-progress2-revenue">Omset Tahun
                                                                    Terakhir Bulanan <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="pendapatan_bdn_hukum"
                                                                    name="pendapatan_bdn_hukum" required>
                                                                    <option value="">Pilih Omset Tahun Terkahir</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="total_aset">Pendapatan Bulanan <i
                                                                        class="text-danger">*</i></label>
                                                                <select class="form-control custom-select"
                                                                    id="total_aset" name="total_aset" required>
                                                                    <option value="">Pilih Total aset</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <!-- END: Baris 12 -->
                                                        {{-- END: Rekening Rekanan badan hukum & lain lain --}}


                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END: Badan Hukum -->

                                <!-- START: Layout Pengurus -->
                                <div class="js-wizard-simple block border pt-0 layout" id="layout-pengurus">
                                    <div class="block-content block-content-full tab-content pt-0"
                                        style="min-height: 274px;">
                                        <div class="row">
                                            <div class="col">
                                                <h6 class="pt-0" style="margin-top: -0.7rem"> <span class="h6"
                                                        style="background: linear-gradient(180deg, #F7F7FA 50%, #ffffff 50%);">Informasi
                                                        Pengurus</span></h6>
                                            </div>
                                        </div>
                                        <!-- baris pemisah -->
                                        <div class="row " id="tambahPenanggungJawab">
                                            <!-- START: Pengurus 1 -->
                                            <div class="col-12 ">
                                                <div class="row ">
                                                    <div class="col-12">
                                                        <h6 class="line mt-3">Pengurus 1 &nbsp</h6>
                                                        {{-- <h6 class="content-heading text-muted font-w600 mt-0 pt-0" style="font-size: 1em;">Pengurus 1</h6>                                                                                             --}}
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <!-- START: Baris 1 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <div class="form-group">
                                                                        <label for="namapengurus1">Nama Pengurus <i
                                                                                class="text-danger">*</i></label>
                                                                        <input type="hidden" id="id_pengurus1"
                                                                            name="id_pengurus1"
                                                                            value="{{ $pengurusBrw1->pengurus_id }}">
                                                                        <input class="form-control" type="text"
                                                                            id="nama_pengurus1" maxlength="30"
                                                                            name="nama_pengurus1"
                                                                            value="{{ $pengurusBrw1->nm_pengurus }}"
                                                                            placeholder="Masukkan Nama Pengurus..."
                                                                            required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group row">
                                                                    <label class="col-12">Jenis Kelamin <i
                                                                            class="text-danger">*</i></label>
                                                                    <div class="col-12">
                                                                        <div class="input-group">
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input"
                                                                                    type="radio"
                                                                                    name="jns_kelamin_pengurus1"
                                                                                    id="jns_kelamin_pengurus3" value="1"
                                                                                    required>
                                                                                <label
                                                                                    class="form-check-label text-muted"
                                                                                    for="jns_kelamin_pengurus3">Laki -
                                                                                    Laki</label>
                                                                            </div>
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input"
                                                                                    type="radio"
                                                                                    name="jns_kelamin_pengurus1"
                                                                                    id="jns_kelamin_pengurus4"
                                                                                    value="2">
                                                                                <label
                                                                                    class="form-check-label text-muted"
                                                                                    for="jns_kelamin_pengurus4">Perempuan</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="ktp_pengurus1">Nomor KTP <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control" type="text"
                                                                        id="ktp_pengurus1"
                                                                        value="{{$pengurusBrw1->nik_pengurus}}"
                                                                        name="ktp_pengurus1"
                                                                        placeholder="Masukkan nomor KTP" maxlength="16"
                                                                        pattern=".{16,16}"
                                                                        oninput="this.setCustomValidity('')"
                                                                        oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')"
                                                                        onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                        required>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 1 -->

                                                            <!-- START: Baris 2 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="tmpt_lahir_pengurus1">Tempat Lahir <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control" type="text"
                                                                        maxlength="35" id="tmpt_lahir_pengurus1"
                                                                        value="{{ $pengurusBrw1->tempat_lahir }}"
                                                                        name="tmpt_lahir_pengurus1"
                                                                        placeholder="Masukkan tempat lahir" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="tgl_lahir_pengurus1">Tanggal Lahir <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control" type="date"
                                                                        id="tgl_lahir_pengurus1"
                                                                        value="{{ $pengurusBrw1->tgl_lahir }}"
                                                                        name="tgl_lahir_pengurus1"
                                                                        max="<?= date("Y-m-d"); ?>"
                                                                        placeholder="Masukkan tanggal lahir" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label id="lbl_no_hp">Nomor Telopon <i
                                                                            class="text-danger">*</i></label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-append">
                                                                            <span
                                                                                class="input-group-text input-group-text-dsi">
                                                                                +62 </span>
                                                                        </div>
                                                                        <input
                                                                            class="form-control checkNOHP validasiString"
                                                                            type="text" id="tlp_pengurus1"
                                                                            name="tlp_pengurus1"
                                                                            placeholder="Masukkan nomor Telepon"
                                                                            maxlength="15" pattern=".{9,15}"
                                                                            oninput="this.setCustomValidity('')"
                                                                            oninvalid="this.setCustomValidity('Nomor Hp tidak boleh kurang dari 10 digit')"
                                                                            onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                            required>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 2 -->

                                                            <!-- START: Baris 3 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group row">
                                                                    <label class="col-12">Agama <i
                                                                            class="text-danger">*</i></label>
                                                                    <div class="col-12">
                                                                        <select class="form-control custom-select"
                                                                            id="agama_pengurus1" name="agama_pengurus1"
                                                                            required>
                                                                            <option value="">Pilih Agama</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label
                                                                        for="wizard-progress2-pendidikanterakhir">Pendidikan
                                                                        Terakhir <i class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="pendidikan_terakhir_pengurus1"
                                                                        name="pendidikan_terakhir_pengurus1" required>
                                                                        <option value="">Pilih Pendidikan Terakhir
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 3 -->

                                                            <!-- START: Baris 4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="npwp_pengurus1">Nomor NPWP <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control " type="text"
                                                                        pattern=".{15,15}"
                                                                        oninput="this.setCustomValidity('')"
                                                                        oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')"
                                                                        onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                        maxlength="15" value="{{ $pengurusBrw1->npwp }}"
                                                                        id="npwp_pengurus1" name="npwp_pengurus1"
                                                                        placeholder="Masukkan nomor NPWP" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="jabatan_pengurus1">Jabatan <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="jabatan_pengurus1" name="jabatan_pengurus1"
                                                                        required>
                                                                        <option value="">Pilih Jabatan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4"></div>
                                                            <div class="col-md-4"></div>
                                                            <!-- END: Baris 4 -->

                                                            <!-- START: Baris 5 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="alamat_pengurus1">Alamat Lengkap
                                                                        pengurus 1 <i class="text-danger">*</i></label>
                                                                    <textarea class="form-control form-control-lg"
                                                                        maxlength="90" id="alamat_pengurus1"
                                                                        name="alamat_pengurus1" rows="6"
                                                                        placeholder="Masukkan alamat lengkap Anda.."
                                                                        required>{{ $pengurusBrw1->alamat }}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="provinsi_pengurus1">Provinsi <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="provinsi_pengurus1"
                                                                        name="provinsi_pengurus1" required>
                                                                        <option value="">Pilih Provinsi</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kota_pengurus1">Kota/Kabupaten <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kota_pengurus1" name="kota_pengurus1"
                                                                        required>
                                                                        <option value="">Pilih Kota / Kabupaten</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 5 -->

                                                            <!-- START: Baris 6 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kecamatan_pengurus1">Kecamatan <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kecamatan_pengurus1"
                                                                        name="kecamatan_pengurus1" required>
                                                                        <option value="">Pilih Kecamatan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kelurahan_pengurus1">Kelurahan <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kelurahan_pengurus1"
                                                                        name="kelurahan_pengurus1" required>
                                                                        <option value="">Pilih Kelurahan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kode_pos_pengurus1">Kode Pos <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kode_pos_pengurus1"
                                                                        name="kode_pos_pengurus1" required>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 6 -->

                                                            <!-- START: Foto Pengurus 1 Title -->
                                                            <div class="col-12">
                                                                <h6 class="line mt-3">Foto Pengurus 1 &nbsp</h6>
                                                            </div>
                                                            <!-- END: Foto Title -->

                                                            <!-- START: Foto -->
                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp" id="foto_diri_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label
                                                                                class="font-weight-bold ml-0 flex-row">Foto
                                                                                Diri <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_diri) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw1->foto_diri)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_diri) ? asset('/storage').'/'.$pengurusBrw1->foto_diri.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>
                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_diri_pengurus1">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_diri_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                Diri <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/user-guide.png')}}"
                                                                                width="150px" height="145px"
                                                                                alt="guide">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_foto_diri_pengurus1">
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_diri_pengurus1()">
                                                                            <input type="hidden"
                                                                                name="image_foto_diri_pengurus1"
                                                                                id="user_foto_diri_pengurus1"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_pengurus1"
                                                                                id="url_pic_brw_pengurus1">
                                                                            <input type="hidden"
                                                                                name="brw_pic_pengurus1"
                                                                                id="brw_pic_pengurus1"
                                                                                value="{{$pengurusBrw1->foto_diri}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_diri_pengurus1">Hasil</label>
                                                                            <div id="results_foto_diri_pengurus1"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp text-center"
                                                                    id="foto_ktp_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                KTP <i class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_ktp) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw1->foto_ktp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_ktp) ? asset('/storage').'/'.$pengurusBrw1->foto_ktp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>
                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_ktp_pengurus1">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_ktp_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                KTP <i class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/guide-ktp.png')}}"
                                                                                width="150px" height="145px"
                                                                                alt="guide">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_ktp_pengurus1"></div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_ktp_pengurus1()">
                                                                            <input type="hidden"
                                                                                name="image_foto_ktp_pengurus1"
                                                                                id="user_foto_ktp_pengurus1"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_ktp_pengurus1"
                                                                                id="url_pic_brw_ktp_pengurus1">
                                                                            <input type="hidden"
                                                                                name="brw_pic_ktp_pengurus1"
                                                                                id="brw_pic_ktp_pengurus1"
                                                                                value="{{$pengurusBrw1->foto_ktp}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_ktp_pengurus1">Hasil</label>
                                                                            <div id="results_foto_ktp_pengurus1"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp text-center"
                                                                    id="foto_ktp_diri_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                Diri & KTP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_diri_ktp) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw1->foto_diri_ktp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_diri_ktp) ? asset('/storage').'/'.$pengurusBrw1->foto_diri_ktp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>
                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_ktp_diri_pengurus1">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_ktp_diri_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                Diri & KTP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/guide-diridanktp.png')}}"
                                                                                alt="guide" width="150px"
                                                                                height="145px">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_ktp_diri_pengurus1"></div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_ktp_diri_pengurus1()">
                                                                            <input type="hidden"
                                                                                name="image_foto_ktp_diri_pengurus1"
                                                                                id="user_foto_ktp_diri_pengurus1"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_dengan_ktp_pengurus1"
                                                                                id="url_pic_brw_dengan_ktp_pengurus1">
                                                                            <input type="hidden"
                                                                                name="brw_pic_user_ktp_pengurus1"
                                                                                id="brw_pic_user_ktp_pengurus1"
                                                                                value="{{$pengurusBrw1->foto_diri_ktp}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_ktp_diri_pengurus1">Hasil</label>
                                                                            <div id="results_foto_ktp_diri_pengurus1">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp text-center"
                                                                    id="foto_npwp_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                NPWP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_npwp) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw1->foto_npwp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw1->foto_npwp) ? asset('/storage').'/'.$pengurusBrw1->foto_npwp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>

                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_npwp_pengurus1">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_npwp_pengurus1">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                NPWP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/guide-ktp.png')}}"
                                                                                alt="guide" width="150px"
                                                                                height="145px">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_npwp_pengurus1"></div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_npwp_pengurus1()">
                                                                            <input type="hidden"
                                                                                name="image_foto_npwp_pengurus1"
                                                                                id="user_foto_npwp_pengurus1"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_npwp_pengurus1"
                                                                                id="url_pic_brw_npwp_pengurus1">
                                                                            <input type="hidden"
                                                                                name="brw_pic_npwp_pengurus1"
                                                                                id="brw_pic_npwp_pengurus1"
                                                                                value="{{$pengurusBrw1->foto_npwp}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_npwp_pengurus1">Hasil</label>
                                                                            <div id="results_foto_npwp_pengurus1"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 text-left mt-4 mb-4">
                                                                <label class="font-weight-bold text-black ml-0">Format
                                                                    file .jpg, .jpeg, .gif, dan .png</label>
                                                            </div>
                                                            <!-- END: Foto -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END: Pengurus 1 -->

                                            <!-- START: Pengurus 2 -->
                                            <div class="col-12 ">
                                                <div class="row ">
                                                    <div class="col-12">
                                                        <h6 class="line mt-3">Pengurus 2 &nbsp</h6>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <!-- START: Baris 1 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <div class="form-group">
                                                                        <label for="nama_pengurus2">Nama Pengurus <i
                                                                                class="text-danger">*</i></label>
                                                                        <input type="hidden" id="id_pengurus2"
                                                                            name="id_pengurus2"
                                                                            value="{{ $pengurusBrw2->pengurus_id }}">
                                                                        <input class="form-control" type="text"
                                                                            id="nama_pengurus2" maxlength="30"
                                                                            name="nama_pengurus2"
                                                                            value="{{ $pengurusBrw2->nm_pengurus }}"
                                                                            placeholder="Masukkan Nama Pengurus..."
                                                                            required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group row">
                                                                    <label class="col-12">Jenis Kelamin <i
                                                                            class="text-danger">*</i></label>
                                                                    <div class="col-12">
                                                                        <div class="input-group">
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input"
                                                                                    type="radio"
                                                                                    name="jns_kelamin_pengurus2"
                                                                                    id="jns_kelamin_pengurus5" value="1"
                                                                                    required>
                                                                                <label
                                                                                    class="form-check-label text-muted"
                                                                                    for="jns_kelamin_pengurus5">Laki -
                                                                                    Laki</label>
                                                                            </div>
                                                                            <div class="form-check form-check-inline">
                                                                                <input class="form-check-input"
                                                                                    type="radio"
                                                                                    name="jns_kelamin_pengurus2"
                                                                                    id="jns_kelamin_pengurus6"
                                                                                    value="2">
                                                                                <label
                                                                                    class="form-check-label text-muted"
                                                                                    for="jns_kelamin_pengurus6">Perempuan</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="ktp_pengurus2">Nomor KTP <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control" type="text"
                                                                        id="ktp_pengurus2" name="ktp_pengurus2"
                                                                        placeholder="Masukkan nomor KTP"
                                                                        value="{{ $pengurusBrw2->nik_pengurus }}"
                                                                        maxlength="16" pattern=".{16,16}"
                                                                        oninput="this.setCustomValidity('')"
                                                                        oninvalid="this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')"
                                                                        onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                        required>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 1 -->

                                                            <!-- START: Baris 2 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="tmpt_lahir_pengurus2">Tempat Lahir <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control" type="text"
                                                                        maxlength="35" id="tmpt_lahir_pengurus2"
                                                                        name="tmpt_lahir_pengurus2"
                                                                        value="{{ $pengurusBrw2->tempat_lahir }}"
                                                                        placeholder="Masukkan tempat lahir" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="tgl_lahir_pengurus2">Tanggal Lahir <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control" type="date"
                                                                        id="tgl_lahir_pengurus2"
                                                                        name="tgl_lahir_pengurus2"
                                                                        value="{{ $pengurusBrw2->tgl_lahir }}"
                                                                        max="<?= date("Y-m-d"); ?>"
                                                                        placeholder="Masukkan tanggal lahir" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label id="lbl_no_hp">Nomor Telopon <i
                                                                            class="text-danger">*</i></label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-append">
                                                                            <span
                                                                                class="input-group-text input-group-text-dsi">
                                                                                +62 </span>
                                                                        </div>
                                                                        <input
                                                                            class="form-control checkNOHP validasiString"
                                                                            type="text" id="tlp_pengurus2"
                                                                            name="tlp_pengurus2"
                                                                            placeholder="Masukkan nomor Telepon"
                                                                            maxlength="15" pattern=".{9,15}"
                                                                            oninput="this.setCustomValidity('')"
                                                                            oninvalid="this.setCustomValidity('Nomor Hp tidak boleh kurang dari 10 digit')"
                                                                            onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                            required>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 2 -->

                                                            <!-- START: Baris 3 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group row">
                                                                    <label class="col-12">Agama <i
                                                                            class="text-danger">*</i></label>
                                                                    <div class="col-12">
                                                                        <select class="form-control custom-select"
                                                                            id="agama_pengurus2" name="agama_pengurus2"
                                                                            required>
                                                                            <option value="">Pilih Agama</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label
                                                                        for="wizard-progress2-pendidikanterakhir">Pendidikan
                                                                        Terakhir <i class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="pendidikan_terakhir_pengurus2"
                                                                        name="pendidikan_terakhir_pengurus2" required>
                                                                        <option value="">Pilih Pendidikan Terakhir
                                                                        </option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 3 -->

                                                            <!-- START: Baris 4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="npwp_pengurus2">Nomor NPWP <i
                                                                            class="text-danger">*</i></label>
                                                                    <input class="form-control " type="text"
                                                                        id="npwp_pengurus2" pattern=".{15,15}"
                                                                        oninput="this.setCustomValidity('')"
                                                                        oninvalid="this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')"
                                                                        onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                                        maxlength="15" name="npwp_pengurus2"
                                                                        value="{{ $pengurusBrw2->npwp }}"
                                                                        placeholder="Masukkan nomor NPWP" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="jabatan_pengurus2">Jabatan <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="jabatan_pengurus2" name="jabatan_pengurus2"
                                                                        required>
                                                                        <option value="">Pilih Jabatan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4"></div>
                                                            <div class="col-md-4"></div>
                                                            <!-- END: Baris 4 -->

                                                            <!-- START: Baris 5 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="alamat_pengurus2">Alamat Lengkap <i
                                                                            class="text-danger">*</i></label>
                                                                    <textarea class="form-control form-control-lg"
                                                                        maxlength="90" id="alamat_pengurus2"
                                                                        name="alamat_pengurus2" rows="6"
                                                                        placeholder="Masukkan alamat lengkap Anda.."
                                                                        required>{{ $pengurusBrw2->alamat }}</textarea>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="provinsi_pengurus2">Provinsi <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="provinsi_pengurus2"
                                                                        name="provinsi_pengurus2" required>
                                                                        <option value="">Pilih Provinsi</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kota_pengurus2">Kota/Kabupaten <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kota_pengurus2" name="kota_pengurus2"
                                                                        required>
                                                                        <option value="">Pilih Kota / Kabupaten</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 5 -->

                                                            <!-- START: Baris 6 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kecamatan_pengurus2">Kecamatan <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kecamatan_pengurus2"
                                                                        name="kecamatan_pengurus2" required>
                                                                        <option value="">Pilih Kecamatan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kelurahan_pengurus2">Kelurahan <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kelurahan_pengurus2"
                                                                        name="kelurahan_pengurus2" required>
                                                                        <option value="">Pilih Kelurahan</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label for="kode_pos_pengurus2">Kode Pos <i
                                                                            class="text-danger">*</i></label>
                                                                    <select class="form-control custom-select"
                                                                        id="kode_pos_pengurus2"
                                                                        name="kode_pos_pengurus2" required>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!-- END: Baris 6 -->

                                                            <!-- START: Foto Pengurus 1 Title -->
                                                            <div class="col-12">
                                                                <h6 class="line mt-3">Foto Pengurus 2 &nbsp</h6>
                                                            </div>
                                                            <!-- END: Foto Title -->

                                                            <!-- START: Foto -->
                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp" id="foto_diri_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label
                                                                                class="font-weight-bold ml-0 flex-row">Foto
                                                                                Pengurus 2 <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_diri) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw2->foto_diri)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_diri) ? asset('/storage').'/'.$pengurusBrw2->foto_diri.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>
                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_diri_pengurus2">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_diri_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                Diri<i class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/user-guide.png')}}"
                                                                                width="150px" height="145px"
                                                                                alt="guide">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_foto_diri_pengurus2">
                                                                            </div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_diri_pengurus2()">
                                                                            <input type="hidden"
                                                                                name="image_foto_diri_pengurus2"
                                                                                id="user_foto_diri_pengurus2"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_pengurus2"
                                                                                id="url_pic_brw_pengurus2">
                                                                            <input type="hidden"
                                                                                name="brw_pic_pengurus2"
                                                                                id="brw_pic_pengurus2"
                                                                                value="{{$pengurusBrw2->foto_diri}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_diri_pengurus2">Hasil</label>
                                                                            <div id="results_foto_diri_pengurus2"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp text-center"
                                                                    id="foto_ktp_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                KTP <i class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_ktp) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw2->foto_ktp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_ktp) ? asset('/storage').'/'.$pengurusBrw2->foto_ktp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>
                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_ktp_pengurus2">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_ktp_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                KTP <i class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/guide-ktp.png')}}"
                                                                                width="150px" height="145px"
                                                                                alt="guide">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_ktp_pengurus2"></div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_ktp_pengurus2()">
                                                                            <input type="hidden"
                                                                                name="image_foto_ktp_pengurus2"
                                                                                id="user_foto_ktp_pengurus2"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_ktp_pengurus2"
                                                                                id="url_pic_brw_ktp_pengurus2">
                                                                            <input type="hidden"
                                                                                name="brw_pic_ktp_pengurus2"
                                                                                id="brw_pic_ktp_pengurus2"
                                                                                value="{{$pengurusBrw2->foto_ktp}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_ktp_pengurus2">Hasil</label>
                                                                            <div id="results_foto_ktp_pengurus2"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp text-center"
                                                                    id="foto_ktp_diri_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                Diri & KTP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_diri_ktp) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw2->foto_diri_ktp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_diri_ktp) ? asset('/storage').'/'.$pengurusBrw2->foto_diri_ktp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>
                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_ktp_diri_pengurus2">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_ktp_diri_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                Diri & KTP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/guide-diridanktp.png')}}"
                                                                                alt="guide" width="150px"
                                                                                height="145px">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_ktp_diri_pengurus2"></div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_ktp_diri_pengurus2()">
                                                                            <input type="hidden"
                                                                                name="image_foto_ktp_diri_pengurus2"
                                                                                id="user_foto_ktp_diri_pengurus2"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_dengan_ktp_pengurus2"
                                                                                id="url_pic_brw_dengan_ktp_pengurus2">
                                                                            <input type="hidden"
                                                                                name="brw_pic_user_ktp_pengurus2"
                                                                                id="brw_pic_user_ktp_pengurus2"
                                                                                value="{{$pengurusBrw2->foto_diri_ktp}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_ktp_diri_pengurus2">Hasil</label>
                                                                            <div id="results_foto_ktp_diri_pengurus2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3 col-sm-6 imgUp">
                                                                <div class="col imgUp text-center"
                                                                    id="foto_npwp_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="mx-auto">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                NPWP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="mx-auto">
                                                                            {{-- ! PERUBAHAN PATH FOTO KE PRIVATE 20210517 --}}
                                                                            <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_npwp) ? route('getUserFile', ['filename' => str_replace('/',':', $pengurusBrw2->foto_npwp)]) .'?t='.date("Y-m-d h:i:sa") : '' }}"
                                                                                width="150px" height="145px">
                                                                            {{-- <img class="imagePreview img-fluid rounded"
                                                                                src="{{ !empty($pengurusBrw2->foto_npwp) ? asset('/storage').'/'.$pengurusBrw2->foto_npwp.'?t='.date("Y-m-d h:i:sa") : ''}}"
                                                                            width="150px" height="145px"> --}}
                                                                        </div>

                                                                        <div class="mt-2 mx-auto"
                                                                            id="camera_foto_npwp_pengurus2">
                                                                            <label class="btn btn-primary ml-0"><i
                                                                                    class="fa fa-camera"></i> Ubah
                                                                                Foto</label>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col imgUp d-none"
                                                                    id="take_camera_foto_npwp_pengurus2">
                                                                    <div class="d-flex flex-column">
                                                                        <div class="text-center">
                                                                            <label class="font-weight-bold ml-0">Foto
                                                                                NPWP <i
                                                                                    class="text-danger">*</i></label>
                                                                        </div>
                                                                        <div class="d-flex justify-content-center">
                                                                            <img style="z-index:2;"
                                                                                class="position-absolute rounded"
                                                                                id="user-guide"
                                                                                src="{{URL::to('assets/img/guide-ktp.png')}}"
                                                                                alt="guide" width="150px"
                                                                                height="145px">
                                                                            <div style="z-index:1 !important;"
                                                                                class="img-fluid rounded"
                                                                                id="my_camera_npwp_pengurus2"></div>
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <input class="btn btn-primary mt-2"
                                                                                type="button" value="Ambil Foto"
                                                                                onClick="take_snapshot_foto_npwp_pengurus2()">
                                                                            <input type="hidden"
                                                                                name="image_foto_npwp_pengurus2"
                                                                                id="user_foto_npwp_pengurus2"
                                                                                class="image-tag">
                                                                            <input type="hidden"
                                                                                name="url_pic_brw_npwp_pengurus2"
                                                                                id="url_pic_brw_npwp_pengurus2">
                                                                            <input type="hidden"
                                                                                name="brw_pic_npwp_pengurus2"
                                                                                id="brw_pic_npwp_pengurus2"
                                                                                value="{{$pengurusBrw2->foto_npwp}}">
                                                                        </div>
                                                                        <div class="text-center">
                                                                            <label
                                                                                class="font-weight-bold ml-0 mt-4 d-none"
                                                                                id="label_hasil_foto_npwp_pengurus2">Hasil</label>
                                                                            <div id="results_foto_npwp_pengurus2"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 text-left mt-4 mb-4">
                                                                <label class="font-weight-bold text-black ml-0">Format
                                                                    file .jpg, .jpeg, .gif, dan .png</label>
                                                            </div>
                                                            <!-- END: Foto -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- END: Pengurus 2 -->
                                        </div>
                                        <!-- baris pemisah -->
                                    </div>
                                </div>
                                <!-- END: Layout Pengurus -->

                                <!-- START: Button Submit Layout Badan Hukum -->
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <button type="submit" name="form_profile_submit" value="bdn_hukum"
                                            class="btn btn-rounded btn-big btn-noborder btn-success min-width-150 mt-20 mb-20"
                                            id="btnSubmitBdnHukum"><span class="p-5">Simpan</span></button>
                                    </div>
                                </div>
                                <!-- END: Button Submit Layout Badan Hukum -->
                            </form>
                            <!-- END: Form Badan Hukum -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->
</main>
<!-- END Main Container -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
<script>
    // $(document).ready(function(){
            Webcam.set({
                width:190,
                height:145,
                crop_width:150,
                crop_height:145,
                image_format: 'jpg',
                jpeg_quality: 90,
                flip_horiz: true
            });

            // Foto diri
            $("#camera_foto_diri").click(function(){
                Webcam.attach( '#my_camera_foto' );
                $("#take_camera_foto_diri").fadeIn();
                $("#take_camera_foto_diri").removeClass("d-none");
                $("#foto_diri").fadeOut();
                $("#foto_diri").addClass("d-none");
            });

            $("#cancel_foto_diri").click(function(){
                $("#my_camera_foto").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto diri

            // Foto ktp
            $("#camera_foto_ktp").click(function(){
                Webcam.attach( '#my_camera_ktp' );
                $("#take_camera_foto_ktp").fadeIn();
                $("#take_camera_foto_ktp").removeClass("d-none");
                $("#foto_ktp").fadeOut();
                $("#foto_ktp").addClass("d-none");
            });

            $("#cancel_foto_ktp").click(function(){
                $("#my_camera_ktp").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto ktp

            // Foto ktp & diri
            $("#camera_foto_ktp_diri").click(function(){
                Webcam.attach( '#my_camera_ktp_diri' );
                $("#take_camera_foto_ktp_diri").fadeIn();
                $("#take_camera_foto_ktp_diri").removeClass("d-none");
                $("#foto_ktp_diri").fadeOut();
                $("#foto_ktp_diri").addClass("d-none");
            });
            $("#cancel_foto_ktp_diri").click(function(){
                $("#my_camera_ktp_diri").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto ktp & diri

            // Foto npwp
            $("#camera_foto_npwp").click(function(){
                Webcam.attach( '#my_camera_npwp' );
                $("#take_camera_foto_npwp").fadeIn();
                $("#take_camera_foto_npwp").removeClass("d-none");
                $("#foto_npwp").fadeOut();
                $("#foto_npwp").addClass("d-none");
            });
            $("#cancel_foto_npwp").click(function(){
                $("#my_camera_npwp").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto npwp

            // Foto diri badan hukum
            $("#camera_foto_diri_bdn_hukum").click(function(){
                Webcam.attach( '#my_camera_foto_bdn_hukum' );
                $("#take_camera_foto_diri_bdn_hukum").fadeIn();
                $("#take_camera_foto_diri_bdn_hukum").removeClass("d-none");
                $("#foto_diri_bdn_hukum").fadeOut();
                $("#foto_diri_bdn_hukum").addClass("d-none");
            });
        
            $("#cancel_foto_diri_bdn_hukum").click(function(){
                $("#my_camera_foto_bdn_hukum").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto diri badan hukum

            // Foto ktp badan hukum
            $("#camera_foto_ktp_bdn_hukum").click(function(){
                Webcam.attach( '#my_camera_ktp_bdn_hukum' );
                $("#take_camera_foto_ktp_bdn_hukum").fadeIn();
                $("#take_camera_foto_ktp_bdn_hukum").removeClass("d-none");
                $("#foto_ktp_bdn_hukum").fadeOut();
                $("#foto_ktp_bdn_hukum").addClass("d-none");
            });
            $("#cancel_foto_ktp_bdn_hukum").click(function(){
                $("#my_camera_ktp_bdn_hukum").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto ktp badan hukum
            
            // Foto ktp diri & ktp badan hukum
            $("#camera_foto_ktp_diri_bdn_hukum").click(function(){
                Webcam.attach( '#my_camera_ktp_diri_bdn_hukum' );
                $("#take_camera_foto_ktp_diri_bdn_hukum").fadeIn();
                $("#take_camera_foto_ktp_diri_bdn_hukum").removeClass("d-none");
                $("#foto_ktp_diri_bdn_hukum").fadeOut();
                $("#foto_ktp_diri_bdn_hukum").addClass("d-none");
            });

            $("#cancel_foto_ktp_diri_bdn_hukum").click(function(){
                $("#my_camera_ktp_diri_bdn_hukum").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto ktp diri & ktp badan hukum

            // Foto npwp badan hukum
            $("#camera_foto_npwp_bdn_hukum").click(function(){
                Webcam.attach( '#my_camera_npwp_bdn_hukum' );
                $("#take_camera_foto_npwp_bdn_hukum").fadeIn();
                $("#take_camera_foto_npwp_bdn_hukum").removeClass("d-none");
                $("#foto_npwp_bdn_hukum").fadeOut();
                $("#foto_npwp_bdn_hukum").addClass("d-none");
            });
            $("#cancel_foto_npwp_bdn_hukum").click(function(){
                $("#my_camera_npwp_bdn_hukum").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto npwp badan hukum

            // START: Pengurus
            // Foto diri pengurus 1
            $("#camera_foto_diri_pengurus1").click(function(){
                Webcam.attach( '#my_camera_foto_diri_pengurus1' );
                $("#take_camera_foto_diri_pengurus1").fadeIn();
                $("#take_camera_foto_diri_pengurus1").removeClass("d-none");
                $("#foto_diri_pengurus1").fadeOut();
                $("#foto_diri_pengurus1").addClass("d-none");
            });

            $("#cancel_foto_diri_pengurus1").click(function(){
                $("#my_camera_foto_diri_pengurus1").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto diri pengurus 1
 
            // Foto ktp pengurus 1
            $("#camera_foto_ktp_pengurus1").click(function(){
                Webcam.attach( '#my_camera_ktp_pengurus1' );
                $("#take_camera_foto_ktp_pengurus1").fadeIn();
                $("#take_camera_foto_ktp_pengurus1").removeClass("d-none");
                $("#foto_ktp_pengurus1").fadeOut();
                $("#foto_ktp").addClass("d-none");
            });

            $("#cancel_foto_ktp_pengurus1").click(function(){
                $("#my_camera_ktp_pengurus1").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto ktp 1

            // Foto ktp & diri pengurus 1
            $("#camera_foto_ktp_diri_pengurus1").click(function(){
                Webcam.attach( '#my_camera_ktp_diri_pengurus1' );
                $("#take_camera_foto_ktp_diri_pengurus1").fadeIn();
                $("#take_camera_foto_ktp_diri_pengurus1").removeClass("d-none");
                $("#foto_ktp_diri_pengurus1").fadeOut();
                $("#foto_ktp_diri_pengurus1").addClass("d-none");
            });
            $("#cancel_foto_ktp_diri_pengurus1").click(function(){
                $("#my_camera_ktp_diri_pengurus1").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto ktp & diri 1

            // Foto npwp pengurus 1
            $("#camera_foto_npwp_pengurus1").click(function(){
                Webcam.attach( '#my_camera_npwp_pengurus1' );
                $("#take_camera_foto_npwp_pengurus1").fadeIn();
                $("#take_camera_foto_npwp_pengurus1").removeClass("d-none");
                $("#foto_npw_pengurus1p").fadeOut();
                $("#foto_npwp_pengurus1").addClass("d-none");
            });
            $("#cancel_foto_npwp_pengurus1").click(function(){
                $("#my_camera_npwp_pengurus1").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto npwp pengurus  1

            // Foto diri pengurus 2
            $("#camera_foto_diri_pengurus2").click(function(){
                Webcam.attach( '#my_camera_foto_diri_pengurus2' );
                $("#take_camera_foto_diri_pengurus2").fadeIn();
                $("#take_camera_foto_diri_pengurus2").removeClass("d-none");
                $("#foto_diri_pengurus2").fadeOut();
                $("#foto_diri_pengurus2").addClass("d-none");
            });

            $("#cancel_foto_diri_pengurus2").click(function(){
                $("#my_camera_foto_diri_pengurus2").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto diri pengurus 2

            // Foto ktp pengurus 2
            $("#camera_foto_ktp_pengurus2").click(function(){
                Webcam.attach( '#my_camera_ktp_pengurus2' );
                $("#take_camera_foto_ktp_pengurus2").fadeIn();
                $("#take_camera_foto_ktp_pengurus2").removeClass("d-none");
                $("#foto_ktp_pengurus2").fadeOut();
                $("#foto_ktp").addClass("d-none");
            });

            $("#cancel_foto_ktp_pengurus2").click(function(){
                $("#my_camera_ktp_pengurus2").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto ktp 2

            // Foto ktp & diri pengurus 2
            $("#camera_foto_ktp_diri_pengurus2").click(function(){
                Webcam.attach( '#my_camera_ktp_diri_pengurus2' );
                $("#take_camera_foto_ktp_diri_pengurus2").fadeIn();
                $("#take_camera_foto_ktp_diri_pengurus2").removeClass("d-none");
                $("#foto_ktp_diri_pengurus2").fadeOut();
                $("#foto_ktp_diri_pengurus2").addClass("d-none");
            });
            $("#cancel_foto_ktp_diri_pengurus2").click(function(){
                $("#my_camera_ktp_diri_pengurus2").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            })
            // End Foto ktp & diri 2

            // Foto npwp pengurus 2
            $("#camera_foto_npwp_pengurus2").click(function(){
                Webcam.attach( '#my_camera_npwp_pengurus2' );
                $("#take_camera_foto_npwp_pengurus2").fadeIn();
                $("#take_camera_foto_npwp_pengurus2").removeClass("d-none");
                $("#foto_npw_pengurus2").fadeOut();
                $("#foto_npwp_pengurus2").addClass("d-none");
            });
            $("#cancel_foto_npwp_pengurus2").click(function(){
                $("#my_camera_npwp_pengurus2").fadeIn();
                //$("#take_camera_foto_diri").css( { "margin-left" : "-290px"} );
                //$("#foto_diri").fadeOut();;
            });
            // End Foto npwp pengurus  2
            // END: Pengurus

            function take_snapshot_foto_diri(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_diri').value = data_uri;
                    document.getElementById('results_foto_diri').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_foto").hide();
                    $("#label_hasil_foto_diri").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var foto = data_uri;
                    //var brw_pic = $("#brw_pic").val();
                    $.ajax({
                        url : "/borrower/update_webcam_1",
                        method : "POST",
                        dataType: 'JSON',
                        data: foto,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto diri berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp').value = data_uri;
                    document.getElementById('results_foto_ktp').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp").hide();
                    $("#label_hasil_foto_ktp").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp = data_uri;
                    $.ajax({

                        url : "/borrower/update_webcam_2",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_ktp').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp_diri(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp_diri').value = data_uri;
                    document.getElementById('results_foto_ktp_diri').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp_diri").hide();
                    $("#label_hasil_foto_ktp_diri").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp_diri = data_uri;
                    $.ajax({

                        url : "/borrower/update_webcam_3",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp_diri,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp & diri berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_dengan_ktp').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                                alert(data.failed);
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_npwp(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_npwp').value = data_uri;
                    document.getElementById('results_foto_npwp').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_npwp").hide();
                    $("#label_hasil_foto_npwp").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var npwp = data_uri;
                    $.ajax({
                        url : "/borrower/update_webcam_4",
                        method : "POST",
                        dataType: 'JSON',
                        data: npwp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto npwp berhasil di ubah",
                                    type: "success",
                                });
                                $('#url_pic_brw_npwp').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                });
                            } 
                        }
                    });
                });
            }

            function take_snapshot_foto_diri_bdn_hukum(){   
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_diri_bdn_hukum').value = data_uri;
                    document.getElementById('results_foto_diri_bdn_hukum').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_npwp_bdn_hukum").hide();
                    $("#label_hasil_foto_diri_bdn_hukum").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var foto = data_uri;
                    //var brw_pic = $("#brw_pic").val();
                    $.ajax({

                        url : "/borrower/update_webcam_1_bdn_hukum",
                        method : "POST",
                        dataType: 'JSON',
                        data: foto,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto diri badan hukum berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_bdn_hukum').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp_bdn_hukum() {
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp_bdn_hukum').value = data_uri;
                    document.getElementById('results_foto_ktp_bdn_hukum').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp_bdn_hukum").hide();
                    $("#label_hasil_foto_ktp_bdn_hukum").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp = data_uri;
                    $.ajax({

                        url : "/borrower/update_webcam_2_bdn_hukum",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp badan hukum berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_ktp_bdn_hukum').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp_diri_bdn_hukum() {  
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp_diri_bdn_hukum').value = data_uri;
                    document.getElementById('results_foto_ktp_diri_bdn_hukum').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_npwp_bdn_hukum").hide();
                    $("#label_hasil_foto_ktp_diri_bdn_hukum").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp_diri = data_uri;
                    $.ajax({

                        url : "/borrower/update_webcam_3_bdn_hukum",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp_diri,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp & diri badan hukum berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_dengan_ktp_bdn_hukum').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_npwp_bdn_hukum() {
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_npwp_bdn_hukum').value = data_uri;
                    document.getElementById('results_foto_npwp_bdn_hukum').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp_diri_bdn_hukum").hide();
                    $("#label_hasil_foto_npwp_bdn_hukum").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var npwp = data_uri;
                    $.ajax({

                        url : "/borrower/update_webcam_4_bdn_hukum",
                        method : "POST",
                        dataType: 'JSON',
                        data: npwp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto npwp badan hukum berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_npwp_bdn_hukum').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            // START: Take Snapshot Foto Pengurus 1
            function take_snapshot_foto_diri_pengurus1(){
                Webcam.snap( function(data_uri) {
                    
                    document.getElementById('user_foto_diri_pengurus1').value = data_uri;
                    document.getElementById('results_foto_diri_pengurus1').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_foto").hide();
                    $("#label_hasil_foto_diri_pengurus1").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var foto = data_uri;
                    //var brw_pic = $("#brw_pic").val();
                    $.ajax({
                        url : "/borrower/webcam_foto_pengurus_1",
                        method : "POST",
                        dataType: 'JSON',
                        data: foto,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto diri berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_pengurus1').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp_pengurus1(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp_pengurus1').value = data_uri;
                    document.getElementById('results_foto_ktp_pengurus1').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp").hide();
                    $("#label_hasil_foto_ktp_pengurus1").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp = data_uri;
                    $.ajax({

                        url : "/borrower/webcam_foto_ktp_pengurus_1",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_ktp_pengurus1').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp_diri_pengurus1(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp_diri_pengurus1').value = data_uri;
                    document.getElementById('results_foto_ktp_diri_pengurus1').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp_diri").hide();
                    $("#label_hasil_foto_ktp_diri_pengurus1").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp_diri = data_uri;
                    $.ajax({

                        url : "/borrower/webcam_foto_ktp_diri_pengurus_1",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp_diri,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp & diri berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_dengan_ktp_pengurus1').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                                alert(data.failed);
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_npwp_pengurus1(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_npwp_pengurus1').value = data_uri;
                    document.getElementById('results_foto_npwp_pengurus1').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_npwp").hide();
                    $("#label_hasil_foto_npwp_pengurus1").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var npwp = data_uri;
                    $.ajax({
                        url : "/borrower/webcam_npwp_pengurus_1",
                        method : "POST",
                        dataType: 'JSON',
                        data: npwp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto npwp berhasil di ubah",
                                    type: "success",
                                });
                                $('#url_pic_brw_npwp_pengurus1').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                });
                            } 
                        }
                    });
                });
            }
            // END: Take Snapshot Foto Pengurus 1

             // START: Take Snapshot Foto Pengurus 2
            function take_snapshot_foto_diri_pengurus2(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_diri_pengurus2').value = data_uri;
                    document.getElementById('results_foto_diri_pengurus2').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_foto").hide();
                    $("#label_hasil_foto_diri_pengurus2").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var foto = data_uri;
                    //var brw_pic = $("#brw_pic").val();
                    $.ajax({
                        url : "/borrower/webcam_foto_pengurus_2",
                        method : "POST",
                        dataType: 'JSON',
                        data: foto,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto diri berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_pengurus2').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp_pengurus2(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp_pengurus2').value = data_uri;
                    document.getElementById('results_foto_ktp_pengurus2').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp").hide();
                    $("#label_hasil_foto_ktp_pengurus2").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp = data_uri;
                    $.ajax({

                        url : "/borrower/webcam_foto_ktp_pengurus_2",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_ktp_pengurus2').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_ktp_diri_pengurus2(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_ktp_diri_pengurus2').value = data_uri;
                    document.getElementById('results_foto_ktp_diri_pengurus2').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_ktp_diri").hide();
                    $("#label_hasil_foto_ktp_diri_pengurus2").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var ktp_diri = data_uri;
                    $.ajax({

                        url : "/borrower/webcam_foto_ktp_diri_pengurus_2",
                        method : "POST",
                        dataType: 'JSON',
                        data: ktp_diri,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto ktp & diri berhasil di ubah",
                                    type: "success",
                                })
                                $('#url_pic_brw_dengan_ktp_pengurus2').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                })
                                alert(data.failed);
                            }
                            
                        }
                    });
                });
            }

            function take_snapshot_foto_npwp_pengurus2(){
                Webcam.snap( function(data_uri) {
                    document.getElementById('user_foto_npwp_pengurus2').value = data_uri;
                    document.getElementById('results_foto_npwp_pengurus2').innerHTML = '<img src="'+data_uri+'" class="img-fluid rounded" style="width:150px;height:145px;"/>';
                    //$("#my_camera_npwp").hide();
                    $("#label_hasil_foto_npwp_pengurus2").removeClass("d-none");

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var npwp = data_uri;
                    $.ajax({
                        url : "/borrower/webcam_npwp_pengurus_2",
                        method : "POST",
                        dataType: 'JSON',
                        data: npwp,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            if(data.success){
                                swal.fire({
                                    title: "Sukses",
                                    text: "Foto npwp berhasil di ubah",
                                    type: "success",
                                });
                                $('#url_pic_brw_npwp_pengurus2').val(data.url);
                            }else{
                                swal.fire({
                                    title: "Failed",
                                    text: data.failed,
                                    type: "error",
                                });
                            } 
                        }
                    });
                });
            }
            // END: Take Snapshot Foto Pengurus 2
        // });
        
        // active layout type untuk borrower
        $(function() {
            $('#type-select').change(function(){
                $('.layout').hide();
                $('#' + $(this).val()).show();
                $( '#layout-x >' + 'div >' + 'div >' + 'div >' + '#' + $(this).val()).show();
            });
        });
        // active layout type dokumen
        $(function() {
            $('#type-pendanaan-select').change(function(){
                $('.layout-dokumen').hide();
                $('#' + $(this).val()).show();
            });
        });
        // start
        // tambah penanggung jawab untuk tipe Badan Hukum
        var tambahPenanggungJawab = '<div class="col-12 mt-3" id="containerPenanggungJawab">'
                                        +'<div class="row">'
                                            +'<div class="col-12">'
                                                +'<h6 class="content-heading text-muted font-w600 mt-0 pt-0" style="font-size: 1em;">Pengurus ke-n</h6>'
                                            +'</div>'
                                            +'<div class="col-md-4">'
                                                +'<div class="form-group">'
                                                    +'<label for="wizard-progress2-namapengurus">Nama Pengurus</label>'
                                                    +'<input class="form-control" type="text" id="wizard-progress2-namapengurus" name="wizard-progress2-namapengurus" placeholder="Masukkan nama pengurus...">'
                                                +'</div>'
                                            +'</div>'
                                            +'<div class="col-md-5">'
                                                +'<div class="form-group">'
                                                    +'<label for="wizard-progress2-nik">NIK</label>'
                                                    +'<input class="form-control" type="text" id="wizard-progress2-namapengurus" name="wizard-progress2-nik" placeholder="Masukkan NIK pengurus...">'
                                                +'</div>'
                                            +'</div>'
                                            +'<div class="col-md-4">'
                                                +'<div class="form-group">'
                                                    +'<label for="wizard-progress2-nomorhp">No Telepon / HP</label>'
                                                    +'<input class="form-control" type="number" id="wizard-progress2-nomorhp" name="wizard-progress2-nomorhp" placeholder="Masukkan nomor telepon">'
                                                +'</div>'
                                            +'</div> '
                                            +'<div class="col-md-5">'
                                                +'<div class="form-group">'
                                                    +'<label for="wizard-progress2-jabatan">Jabatan</label>'
                                                    +'<input class="form-control" type="text" id="wizard-progress2-jabatan" name="wizard-progress2-jabatan" placeholder="Masukkan nomor telepon">'
                                                +'</div>'
                                            +'</div>'  
                                        +'</div>'
                                        +'<button type="button" class="btn btn-rounded btn-danger mb-10 push-right" id="delete_penanggung_jawab"> <i class="fa fa-times"></i>  Hapus Pengurus Ini</button><hr>'
                                    +'</div>';
        function add_penanggung_jawab() {
            $('#tambahPenanggungJawab').append(tambahPenanggungJawab);
        }
        $(document).on("click", "#delete_penanggung_jawab", function() { 
            $(this).parent().remove();
        });

        $(document).ready(function(){
            var id = '{{$id}}';
            var brw_type = '{{$brw_type}}';
            var nama = '{{$nama}}';
            var jabatan = "{{$jabatan}}";
            var nm_ibu = "{{$nm_ibu}}";
            var ktp = "{{$ktp}}";
            var npwp = "{{$npwp}}";
            var tgl_lahir = "{{$tgl_lahir}}";
            var no_tlp = "{{$no_tlp}}";
            var jns_kelamin = "{{$jns_kelamin}}";
            var status_kawin = "{{$status_kawin}}";
            var status_rumah = "{{$status_rumah}}";
            var alamat = "{{$alamat}}";
            var domisili_alamat = "{{$domisili_alamat}}";
            var domisili_provinsi = "{{$domisili_provinsi}}";
            var domisili_kota = "{{$domisili_kota}}";
            var domisili_kecamatan = "{{$domisili_kecamatan}}";
            var domisili_kelurahan = "{{$domisili_kelurahan}}";
            var domisili_kd_pos = "{{$domisili_kd_pos}}";
            var domisili_status_rumah = "{{$domisili_status_rumah}}";
            var provinsi = "{{$provinsi}}";
            var kota = "{{$kota}}";
            var kecamatan = "{{$kecamatan}}";
            var kelurahan = "{{$kelurahan}}";
            var kode_pos = "{{$kode_pos}}";
            var agama = "{{$agama}}";
            var tempat_lahir = "{{$tempat_lahir}}";
            var pendidikan_terakhir = "{{$pendidikan_terakhir}}";
            var pekerjaan = "{{$pekerjaan}}";
            var bidang_perusahaan = "{{$bidang_perusahaan}}";
            var bidang_pekerjaan = "{{$bidang_pekerjaan}}";
            var bidang_online = "{{$bidang_online}}";
            var pengalaman_pekerjaan = "{{$pengalaman_pekerjaan}}";
            var pendapatan = "{{$pendapatan}}";
            var total_aset = "{{$total_aset}}";
            var kewarganegaraan = "{{$kewarganegaraan}}";
            var brw_online = "{{$brw_online}}";
            var brw_pic = "{{$brw_pic}}";
            var brw_pic_ktp = "{{$brw_pic_ktp}}";
            var brw_pic_user_ktp= "{{$brw_pic_user_ktp}}";
            var brw_pic_npwp = "{{$brw_pic_npwp}}";

            //perusahaan | badan hukum
            var nib = "{{$nib}}";
            var nm_bdn_hukum = "{{$nm_bdn_hukum}}";
            var npwp_perusahaan = "{{$npwp_perusahaan}}";
            var no_akta_pendirian = "{{$no_akta_pendirian}}";
            var tgl_berdiri = "{{$tgl_berdiri}}";
            var telpon_perusahaan = "{{$telpon_perusahaan}}";
            var foto_npwp_perusahaan = "{{$foto_npwp_perusahaan}}";
            var omset_tahun_terakhir = "{{ $omset_tahun_terakhir }}";
            var tot_aset_tahun_terakhir = "{{ $tot_aset_tahun_terakhir }}";

            //rekening
            var brw_norek = "{{$brw_norek}}";
            var brw_nm_pemilik = "{{$brw_nm_pemilik}}";
            var brw_kd_bank = "{{$brw_kd_bank}}";

            // START: Brw type
            if(brw_type == '2'){
                $("#layout-pribadi").attr('style',"display: none");
                $("#btnsubmitpribadi").attr('style',"display: none");
            }else{
                $("#btnSubmitBdnHukum").attr('style',"display: none");
                $("#layout-badanhukum").attr('style',"display: none");
                $("#layout-pengurus").attr('style',"display: none");
            }
            // END: Brw type

            // START: Informasi Pribadi
            $('#nama').val(nama);
            $("input[name=jns_kelamin][value='" + jns_kelamin + "']").attr('checked', 'checked');
            $('#ktp').val(ktp);
            $('#tempat_lahir').val(tempat_lahir);
            $("#tanggal_lahir").val(tgl_lahir);
            $('#ibukandung').val(nm_ibu);
            $('#npwp').val(npwp);

            let no_tlp_check = no_tlp;
            if(no_tlp_check.substring(0,3) == '620') {
                $('#telepon').val(no_tlp_check.substring(3));
            } else if(no_tlp_check.substring(0,2) == '62') {
                $('#telepon').val(no_tlp_check.substring(2));
            } else if(no_tlp_check.substring(0,1) == '0') {
                $('#telepon').val(no_tlp_check.substring(1));
            } else {
                $('#telepon').val(no_tlp_check);
            }

            // $('#telepon').val(no_tlp.substring(2));
            $.getJSON("/borrower/agama/", function(data_agama){
                for(var i = 0; i<data_agama.length; i++){
                    $('#agama').append($('<option>', { 
                        value: data_agama[i].id,
                        text : data_agama[i].text
                    }));
                    $('#agama_pasangan').append($('<option>', { 
                        value: data_agama[i].id,
                        text : data_agama[i].text
                    }));
                    $('#agama_pengurus1').append($('<option>', { 
                        value: data_agama[i].id,
                        text : data_agama[i].text
                    }));
                    $('#agama_pengurus2').append($('<option>', { 
                        value: data_agama[i].id,
                        text : data_agama[i].text
                    }));
                }

                var agama_pasangan = '{{$pasanganBrw->agama}}';
                var agama_pengurus1 = '{{$pengurusBrw1->agama}}';
                var agama_pengurus2 = '{{$pengurusBrw2->agama}}';

                $("#agama_pengurus1 option[value='"+agama_pengurus1+"']").attr('selected', 'selected');
                $("#agama_pengurus2 option[value='"+agama_pengurus2+"']").attr('selected', 'selected');
                $("#agama_pasangan option[value='"+agama_pasangan+"']").attr('selected', 'selected');
                $("#agama option[value='"+agama+"']").attr('selected', 'selected');
            });

            $.getJSON("/borrower/data_pendidikan/", function(data_pendidikan){
                for(var i = 0; i<data_pendidikan.length; i++){
                    $('#pendidikan_terakhir').append($('<option>', { 
                        value: data_pendidikan[i].id,
                        text : data_pendidikan[i].text
                    }));
                    $('#pendidikan_terakhir_pasangan').append($('<option>', { 
                        value: data_pendidikan[i].id,
                        text : data_pendidikan[i].text
                    }));
                    $('#pendidikan_terakhir_pengurus1').append($('<option>', { 
                        value: data_pendidikan[i].id,
                        text : data_pendidikan[i].text
                    }));
                    $('#pendidikan_terakhir_pengurus2').append($('<option>', { 
                        value: data_pendidikan[i].id,
                        text : data_pendidikan[i].text
                    }));
                }

                var pendidikan_pasangan = '{{$pasanganBrw->pendidikan}}';
                var pendidikan_terakhir_pengurus1 = '{{$pengurusBrw1->pendidikan_terakhir}}';
                var pendidikan_terakhir_pengurus2 = '{{$pengurusBrw2->pendidikan_terakhir}}';

                $("#pendidikan_terakhir_pengurus1 option[value='"+pendidikan_terakhir_pengurus1+"']").attr('selected', 'selected');
                $("#pendidikan_terakhir_pengurus2 option[value='"+pendidikan_terakhir_pengurus2+"']").attr('selected', 'selected');
                $("#pendidikan_terakhir_pasangan option[value='"+pendidikan_pasangan+"']").attr('selected', 'selected');
                $("#pendidikan_terakhir option[value='"+pendidikan_terakhir+"']").attr('selected', 'selected');
            });

            //START: Status Kawin 
            $.getJSON("/borrower/status_perkawinan/", function(data_status_perkawinan){
                $("input[name=status_kawin][value='" + status_kawin + "']").attr('checked', 'checked');
            });
            if (status_kawin == '1') {
                $("input[name=status_kawin][value='" + status_kawin + "']").attr('checked', 'checked');
                $("#layout-pasangan").removeClass('d-none');

                // pasangan required
                $("#nama_pasangan").attr("required", true);
                $("#jns_kelamin_pasangan").attr("required", true);

                $("#ktp_pasangan").attr({
                    'required': true,
                    'pattern': '.{16,16}',
                    'oninvalid': "this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')",
                    'oninput': "this.setCustomValidity('')",
                });
                $("#tmpt_lahir_pasangan").attr("required", true);
                $("#tgl_lahir_pasangan").attr("required", true);
                $("#telepon_pasangan").attr({
                    'required': true,
                    'pattern': '.{9,15}',
                    'oninvalid': "this.setCustomValidity('Nomor Telepon tidak boleh kurang dari 10 digit')",
                    'oninput': "this.setCustomValidity('')",
                });
                $("#npwp_pasangan").attr({
                    'required': true,
                    'pattern': '.{15,15}',
                    'oninvalid': "this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')",
                    'oninput': "this.setCustomValidity('')",
                });
                $("#agama_pasangan").attr("required", true);
                $("#pendidikan_terakhir_pasangan").attr("required", true);
                $("#alamat_pasangan").attr("required", true);
                $("#provinsi_pasangan").attr("required", true);
                $("#kecamatan_pasangan").attr("required", true);
                $("#kelurahan_pasangan").attr("required", true);
                $("#kode_pos_pasangan").attr("required", true);
            }
            // killl2
            // No Tlp pasangan
            $("#telepon_pasangan").on("input", function() {
                if (/^0/.test(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
            $("input[name=status_kawin]").change(function(){
                if($(this).val() == '2' || $(this).val() == '3') {
                    $("#layout-pasangan").addClass('d-none');
                    
                    $("#nama_pasangan").attr("required", false);
                    $("#jns_kelamin_pasangan").attr("required", false);
                    $("#ktp_pasangan").attr("required", false);
                    $("#tmpt_lahir_pasangan").attr("required", false);
                    $("#tgl_lahir_pasangan").attr("required", false);
                    $("#telepon_pasangan").attr("required", false);
                    $("#telepon_pasangan").removeAttr("pattern oninvalid oninput");
                    $("#agama_pasangan").attr("required", false);
                    $("#pendidikan_terakhir_pasangan").attr("required", false);
                    $("#npwp_pasangan").attr("required", false);
                    $("#alamat_pasangan").attr("required", false);
                    $("#provinsi_pasangan").attr("required", false);
                    $("#kecamatan_pasangan").attr("required", false);
                    $("#kelurahan_pasangan").attr("required", false);
                    $("#kode_pos_pasangan").attr("required", false);
                } else {
                    let jns_kelamin_pribadi = $("input[name=jns_kelamin]:checked").val();

                    $("#layout-pasangan").removeClass('d-none');
                    // pasangan required
                    $("#nama_pasangan").attr("required", true);
                    $("#jns_kelamin_pasangan").attr("required", true);
                    if (jns_kelamin_pribadi == 1) {
                        $("input[name=jns_kelamin_pasangan][value='2']").attr('checked', 'checked');
                    } else if (jns_kelamin_pribadi == 2) {
                        $("input[name=jns_kelamin_pasangan][value='1']").attr('checked', 'checked');
                    }

                    $("#ktp_pasangan").attr({
                        'required': true,
                        'pattern': '.{16,16}',
                        'oninvalid': "this.setCustomValidity('Nomor KTP tidak boleh kurang dari 16 digit')",
                        'oninput': "this.setCustomValidity('')",
                    });
                    $("#tmpt_lahir_pasangan").attr("required", true);
                    $("#tgl_lahir_pasangan").attr("required", true);
                    if (status_kawin != 1) {
                        $("#telepon_pasangan").attr("disabled", false);
                    }
                    $("#telepon_pasangan").attr({
                        'required': true,
                        'pattern': '.{10,15}',
                        'oninvalid': "this.setCustomValidity('Nomor Telepon tidak boleh kurang dari 10 digit')",
                        'oninput': "this.setCustomValidity('')",
                    });
                    $("#npwp_pasangan").attr({
                        'required': true,
                        'pattern': '.{15,15}',
                        'oninvalid': "this.setCustomValidity('Nomor NPWP tidak boleh kurang dari 15 digit')",
                        'oninput': "this.setCustomValidity('')",
                    });
                    $("#agama_pasangan").attr("required", true);
                    $("#pendidikan_terakhir_pasangan").attr("required", true);
                    $("#alamat_pasangan").attr("required", true);
                    $("#provinsi_pasangan").attr("required", true);
                    $("#kecamatan_pasangan").attr("required", true);
                    $("#kelurahan_pasangan").attr("required", true);
                    $("#kode_pos_pasangan").attr("required", true);
                }
            });
            // END: Status Kawin
            // START: Alamat
            // START: Provinsi
            $.getJSON("/borrower/data_provinsi/", function(data_provinsi){
                var i 
                for(var i = 0; i<data_provinsi.length; i++){
                    $('#provinsi').append($('<option>', { 
                        value: data_provinsi[i].text,
                        text : data_provinsi[i].text
                    }));

                    $('#provinsi_pasangan').append($('<option>', { 
                        value: data_provinsi[i].text,
                        text : data_provinsi[i].text
                    }));

                    $('#provinsi_bdn_hukum').append($('<option>', { 
                        value: data_provinsi[i].text,
                        text : data_provinsi[i].text
                    }));

                    $('#domisili_provinsi, #domisili_provinsi_bdn_hukum').append($('<option>', { 
                        value: data_provinsi[i].text,
                        text : data_provinsi[i].text
                    }));
                    
                    let provinsi_pengurus1 = '{{ $pengurusBrw1->provinsi }}';
                    let provinsi_pengurus2 = '{{ $pengurusBrw2->provinsi }}';
                    $('#provinsi_pengurus1').append($('<option>', { 
                        value: data_provinsi[i].id,
                        text : data_provinsi[i].text
                    }));
                    $('#provinsi_pengurus2').append($('<option>', { 
                        value: data_provinsi[i].id,
                        text : data_provinsi[i].text
                    }));
                }
                $("#domisili_provinsi option[value='"+domisili_provinsi+"']").attr('selected', 'selected');
                $("#domisili_provinsi_bdn_hukum option[value='"+domisili_provinsi+"']").attr('selected', 'selected');
                $("#provinsi_pasangan option[value='{{$pasanganBrw->provinsi}}']").attr('selected', 'selected');
                $("#provinsi_pengurus1 option[value='"+provinsi_pengurus1+"']").attr('selected', 'selected');
                $("#provinsi_pengurus2 option[value='"+provinsi_pengurus2+"']").attr('selected', 'selected');
                $("#provinsi_bdn_hukum option[value='"+provinsi+"']").attr('selected', 'selected');
                $("#provinsi option[value='"+ provinsi +"']").attr('selected', 'selected');
            });

            $('#provinsi').change(function(){
                var provinsi = $('#provinsi option:selected').text();
                $('#kota').empty().trigger('change'); // set null
                $('#kota').append($('<option>', { value: '', text : 'Pilih kota'}));
                $.getJSON("/borrower/data_kota/"+provinsi+"/", function(data_kota){
                    for(var i = 0; i<data_kota.length; i++){
                        $('#kota').append($('<option>', { 
                            value: data_kota[i].text,
                            text : data_kota[i].text
                        }));
                    }
                });
            });
            $('#provinsi_bdn_hukum').change(function(){
                var provinsi = $('#provinsi_bdn_hukum option:selected').text();
                $('#kota_bdn_hukum').empty().trigger('change'); // set null
                $('#kota_bdn_hukum').append($('<option>', { value: '', text : 'Pilih kota'}));
                $.getJSON("/borrower/data_kota/"+provinsi+"/", function(data_kota){
                    for(var i = 0; i<data_kota.length; i++){
                        $('#kota_bdn_hukum').append($('<option>', { 
                            value: data_kota[i].text,
                            text : data_kota[i].text
                        }));
                    }
                });
            });
            // END: Provinsi

            // START: Kota
            $.getJSON("/borrower/data_kota/"+provinsi+"/", function(data_kota){
                for(var i = 0; i<data_kota.length; i++){
                    $('#kota, #kota_bdn_hukum').append($('<option>', { 
                        value: data_kota[i].text,
                        text : data_kota[i].text
                    }));
                }
                $("#kota option[value='"+kota+"']").attr('selected', 'selected');
                $("#kota_bdn_hukum option[value='"+kota+"']").attr('selected', 'selected');
            });

            $('#kota').change(function(){
                var kota = $('#kota option:selected').text();
                $("#kecamatan").empty().trigger('change'); // set null
                $('#kecamatan').append($('<option>', {  value : '', text : 'Pilih Kecamatan' }));
                $.getJSON("/borrower/data_kecamatan/"+kota+"/", function(data_kecamatan){
            
                    for(var i = 0; i<data_kecamatan.length; i++){
                        $('#kecamatan').append($('<option>', { 
                            value: data_kecamatan[i].text,
                            text : data_kecamatan[i].text
                        }));
                    }
                });
            });
            $('#kota_bdn_hukum').change(function(){
                var kota = $('#kota_bdn_hukum option:selected').text();
                $("#kecamatan_bdn_hukum").empty().trigger('change'); // set null
                $('#kecamatan_bdn_hukum').append($('<option>', {  value : '', text : 'Pilih Kecamatan' }));
                $.getJSON("/borrower/data_kecamatan/"+kota+"/", function(data_kecamatan){
            
                    for(var i = 0; i<data_kecamatan.length; i++){
                        $('#kecamatan_bdn_hukum').append($('<option>', { 
                            value: data_kecamatan[i].text,
                            text : data_kecamatan[i].text
                        }));
                    }
                });
            });
            // END: Kota

            // START: Kecamatan
            $.getJSON("/borrower/data_kecamatan/"+kota+"/", function(data_kecamatan){
                for(var i = 0; i<data_kecamatan.length; i++){
                    $('#kecamatan, #kecamatan_bdn_hukum').append($('<option>', { 
                        value: data_kecamatan[i].text,
                        text : data_kecamatan[i].text
                    }));
                }
                $("#kecamatan option[value='"+kecamatan+"']").attr('selected', 'selected');
                $("#kecamatan_bdn_hukum option[value='"+kecamatan+"']").attr('selected', 'selected');
            });

            $('#kecamatan').change(function(){
                var kecamatan = $('#kecamatan option:selected').text();
                $('#kelurahan').empty().trigger('change'); // set null
                $('#kelurahan').append($('<option>', { value: '', text : 'Pilih Kelurahan'}));
                $.getJSON("/borrower/data_kelurahan/"+kecamatan+"/", function(data_kelurahan){
            
                    for(var i = 0; i<data_kelurahan.length; i++){
                        $('#kelurahan').append($('<option>', { 
                            value: data_kelurahan[i].text,
                            text : data_kelurahan[i].text
                        }));
                    }
                });
            });
            $('#kecamatan_bdn_hukum').change(function(){
                var kecamatan = $('#kecamatan_bdn_hukum option:selected').text();
                $('#kelurahan_bdn_hukum').empty().trigger('change'); // set null
                $('#kelurahan_bdn_hukum').append($('<option>', { value: '', text : 'Pilih Kelurahan'}));
                $.getJSON("/borrower/data_kelurahan/"+kecamatan+"/", function(data_kelurahan){
            
                    for(var i = 0; i<data_kelurahan.length; i++){
                        $('#kelurahan_bdn_hukum').append($('<option>', { 
                            value: data_kelurahan[i].text,
                            text : data_kelurahan[i].text
                        }));
                    }
                });
            });
            // END: Kecamatan

            // START: Kelurahan
            $.getJSON("/borrower/data_kelurahan/"+kecamatan+"/", function(data_kelurahan){
                for(var i = 0; i<data_kelurahan.length; i++){
                    $('#kelurahan, #kelurahan_bdn_hukum').append($('<option>', { 
                        value: data_kelurahan[i].text,
                        text : data_kelurahan[i].text
                    }));
                }
                $("#kelurahan option[value='"+kelurahan+"']").attr('selected', 'selected');
                $("#kelurahan_bdn_hukum option[value='"+kelurahan+"']").attr('selected', 'selected');
            });

            $('#kelurahan').change(function(){
                var kelurahan = $('#kelurahan option:selected').text();
                $('#kode_pos').empty().trigger('change'); // set null
                $.getJSON("/borrower/data_kode_pos/"+kelurahan+"/", function(data_kode_pos){
            
                    for(var i = 0; i<data_kode_pos.length; i++){
                        $('#kode_pos').append($('<option>', { 
                            value: data_kode_pos[i].id,
                            text : data_kode_pos[i].text
                        }));
                    }
                });
            });
            $('#kelurahan_bdn_hukum').change(function(){
                var kelurahan = $('#kelurahan_bdn_hukum option:selected').text();
                $('#kode_pos_bdn_hukum').empty().trigger('change'); // set null
                $.getJSON("/borrower/data_kode_pos/"+kelurahan+"/", function(data_kode_pos){
            
                    for(var i = 0; i<data_kode_pos.length; i++){
                        $('#kode_pos_bdn_hukum').append($('<option>', { 
                            value: data_kode_pos[i].id,
                            text : data_kode_pos[i].text
                        }));
                    }
                });
            });
            // END: Kelurahan

            // START: Kode Pos
            $.getJSON("/borrower/data_kode_pos/"+kelurahan+"/", function(data_kode_pos){
                let kode_pos = "{{ $kode_pos }}";
                for(var i = 0; i<data_kode_pos.length; i++){
                    $('#kode_pos, #kode_pos_bdn_hukum').append($('<option>', { 
                        value: data_kode_pos[i].id,
                        text : data_kode_pos[i].text
                    }));
                }
                $("#kode_pos option[value='"+kode_pos+"']").attr('selected', 'selected');
                $("#kode_pos_bdn_hukum option[value='"+kode_pos+"']").attr('selected', 'selected');
            });
            // END: Kode Pos
            $('#alamat, #alamat_bdn_hukum').text(alamat);
            
            $("input[name=status_rumah][value='" + status_rumah + "']").attr('checked', 'checked');
            // END: Alamat 

            // START: Alamat domisili
            $('#domisili_alamat, #domisili_alamat_bdn_hukum').text(domisili_alamat);
            if (alamat == domisili_alamat && provinsi == domisili_provinsi && kota == domisili_kota && kecamatan == domisili_kecamatan && kelurahan == domisili_kelurahan && kode_pos == domisili_kd_pos) {
                $("#domisili_status, #domisili_status_bdn_hukum").attr('checked', 'checked');
                $("#domisili_alamat, #domisili_alamat_bdn_hukum").attr('disabled', 'true');
                $("#domisili_provinsi, #domisili_provinsi_bdn_hukum").attr('disabled', 'true');
                $("#domisili_kota, #domisili_kota_bdn_hukum").attr('disabled', 'true');
                $("#domisili_kelurahan, #domisili_kelurahan_bdn_hukum").attr('disabled', 'true');
                $("#domisili_kecamatan, #domisili_kecamatan_bdn_hukum").attr('disabled', 'true');
                $("#domisili_kode_pos, #domisili_kode_pos_bdn_hukum").attr('disabled', 'true');
                
                // $("#domisili_alamat_lengkap").addClass("d-none");
            } else {
                $("#domisili_alamat, #domisili_alamat_bdn_hukum").prop('disabled', false);
                $("#domisili_provinsi, #domisili_provinsi_bdn_hukum").prop('disabled', false);
                $("#domisili_kota, #domisili_kota_bdn_hukum").prop('disabled', false);
                $("#domisili_kelurahan, #domisili_kelurahan_bdn_hukum").prop('disabled', false);
                $("#domisili_kecamatan, #domisili_kecamatan_bdn_hukum").prop('disabled', false);
                $("#domisili_kode_pos, #domisili_kode_pos_bdn_hukum").prop('disabled', false);
                // $("#domisili_alamat_lengkap").removeClass("d-none");
            }

            // START: Domisili Provinsi
            $('#domisili_provinsi').change(function(){
                var domisili_provinsi = $('#domisili_provinsi option:selected').text();
                $('#domisili_kota').empty().trigger('change'); // set null
                $('#domisili_kota').append($('<option>', { value: '', text : 'Pilih Kota'}));
                $.getJSON("/borrower/data_kota/"+domisili_provinsi+"/", function(data_kota){
                    for(var i = 0; i<data_kota.length; i++){
                        $('#domisili_kota').append($('<option>', { 
                            value: data_kota[i].text,
                            text : data_kota[i].text
                        }));
                    }
                });
            });
            $('#domisili_provinsi_bdn_hukum').change(function(){
                var domisili_provinsi = $('#domisili_provinsi_bdn_hukum option:selected').text();
                $('#domisili_kota_bdn_hukum').empty().trigger('change'); // set null
                $('#domisili_kota_bdn_hukum').append($('<option>', { value: '', text : 'Pilih Kota'}));
                $.getJSON("/borrower/data_kota/"+domisili_provinsi+"/", function(data_kota){
                    for(var i = 0; i<data_kota.length; i++){
                        $('#domisili_kota_bdn_hukum').append($('<option>', { 
                            value: data_kota[i].text,
                            text : data_kota[i].text
                        }));
                    }
                });
            });
            // END: Domisli Provinsi

            // START: Domisili kota
            $.getJSON("/borrower/data_kota/"+domisili_provinsi+"/", function(data_kota){
                for(var i = 0; i<data_kota.length; i++){
                    $('#domisili_kota, #domisili_kota_bdn_hukum').append($('<option>', { 
                        value: data_kota[i].text,
                        text : data_kota[i].text
                    }));
                }
                $("#domisili_kota option[value='"+domisili_kota+"']").attr('selected', 'selected');
                $("#domisili_kota_bdn_hukum option[value='"+domisili_kota+"']").attr('selected', 'selected');
            });

            $('#domisili_kota').change(function(){
                var domisili_kota = $('#domisili_kota option:selected').text();
                $('#domisili_kecamatan').empty().trigger('change'); // set null
                $('#domisili_kecamatan').append($('<option>', { value: '', text : 'Pilih Kecamatan'}));
                $.getJSON("/borrower/data_kecamatan/"+domisili_kota+"/", function(data_kecamatan){
                    for(var i = 0; i<data_kecamatan.length; i++){
                        $('#domisili_kecamatan').append($('<option>', { 
                            value: data_kecamatan[i].text,
                            text : data_kecamatan[i].text
                        }));
                    }
                });
            });
            $('#domisili_kota_bdn_hukum').change(function(){
                var domisili_kota = $('#domisili_kota_bdn_hukum option:selected').text();
                $('#domisili_kecamatan_bdn_hukum').empty().trigger('change'); // set null
                $('#domisili_kecamatan_bdn_hukum').append($('<option>', { value: '', text : 'Pilih Kecamatan'}));
                $.getJSON("/borrower/data_kecamatan/"+domisili_kota+"/", function(data_kecamatan){
                    for(var i = 0; i<data_kecamatan.length; i++){
                        $('#domisili_kecamatan_bdn_hukum').append($('<option>', { 
                            value: data_kecamatan[i].text,
                            text : data_kecamatan[i].text
                        }));
                    }
                });
            });
            // END: Domisili Kota

            // START: DOmisili Kecamatan
            $.getJSON("/borrower/data_kecamatan/"+domisili_kota+"/", function(data_kecamatan){
                for(var i = 0; i<data_kecamatan.length; i++){
                    $('#domisili_kecamatan, #domisili_kecamatan_bdn_hukum').append($('<option>', { 
                        value: data_kecamatan[i].text,
                        text : data_kecamatan[i].text
                    }));
                }
                $("#domisili_kecamatan option[value='"+domisili_kecamatan+"']").attr('selected', 'selected');
                $("#domisili_kecamatan_bdn_hukum option[value='"+domisili_kecamatan+"']").attr('selected', 'selected');
            });

            $('#domisili_kecamatan').change(function(){
                var domisili_kecamatan = $('#domisili_kecamatan option:selected').text();
                $('#domisili_kelurahan').empty().trigger('change'); // set null
                $('#domisili_kelurahan').append($('<option>', { value: '', text : 'Pilih Kelurahan'}));
                $.getJSON("/borrower/data_kelurahan/"+domisili_kecamatan+"/", function(data_kelurahan){
                    for(var i = 0; i<data_kelurahan.length; i++){
                        $('#domisili_kelurahan').append($('<option>', { 
                            value: data_kelurahan[i].text,
                            text : data_kelurahan[i].text
                        }));
                    }
                });
            });
            $('#domisili_kecamatan_bdn_hukum').change(function(){
                var domisili_kecamatan = $('#domisili_kecamatan_bdn_hukum option:selected').text();
                $('#domisili_kelurahan_bdn_hukum').empty().trigger('change'); // set null
                $('#domisili_kelurahan_bdn_hukum').append($('<option>', { value: '', text : 'Pilih Kelurahan'}));
                $.getJSON("/borrower/data_kelurahan/"+domisili_kecamatan+"/", function(data_kelurahan){
                    for(var i = 0; i<data_kelurahan.length; i++){
                        $('#domisili_kelurahan_bdn_hukum').append($('<option>', { 
                            value: data_kelurahan[i].text,
                            text : data_kelurahan[i].text
                        }));
                    }
                });
            });
            // END: DOmisili Kecamatan

            // START: Domisili Kelurahan
            $.getJSON("/borrower/data_kelurahan/"+domisili_kecamatan+"/", function(data_kelurahan){
                for(var i = 0; i<data_kelurahan.length; i++){
                    $('#domisili_kelurahan, #domisili_kelurahan_bdn_hukum').append($('<option>', { 
                        value: data_kelurahan[i].text,
                        text : data_kelurahan[i].text
                    }));
                }
                $("#domisili_kelurahan option[value='"+domisili_kelurahan+"']").attr('selected', 'selected');
                $("#domisili_kelurahan_bdn_hukum option[value='"+domisili_kelurahan+"']").attr('selected', 'selected');
            });

            $('#domisili_kelurahan').change(function(){
                var domisili_kelurahan = $('#domisili_kelurahan option:selected').text();
                $('#domisili_kode_pos').empty().trigger('change'); // set null
                $.getJSON("/borrower/data_kode_pos/"+domisili_kelurahan+"/", function(data_kode_pos){
                    for(var i = 0; i<data_kode_pos.length; i++){
                        $('#domisili_kode_pos').append($('<option>', { 
                            value: data_kode_pos[i].id,
                            text : data_kode_pos[i].text
                        }));
                    }
                });
            });
            $('#domisili_kelurahan_bdn_hukum').change(function(){
                var domisili_kelurahan = $('#domisili_kelurahan_bdn_hukum option:selected').text();
                $('#domisili_kode_pos_bdn_hukum').empty().trigger('change'); // set null
                $.getJSON("/borrower/data_kode_pos/"+domisili_kelurahan+"/", function(data_kode_pos){
                    for(var i = 0; i<data_kode_pos.length; i++){
                        $('#domisili_kode_pos_bdn_hukum').append($('<option>', { 
                            value: data_kode_pos[i].id,
                            text : data_kode_pos[i].text
                        }));
                    }
                });
            });
            // END: Domisili Kelurahan

            // START: Domisili Kode Pos
            $.getJSON("/borrower/data_kode_pos/"+domisili_kelurahan+"/", function(data_kode_pos){
                for(var i = 0; i<data_kode_pos.length; i++){
                    $('#domisili_kode_pos, #domisili_kode_pos_bdn_hukum').append($('<option>', { 
                        value: data_kode_pos[i].id,
                        text : data_kode_pos[i].text
                    }));
                }
                $("#domisili_kode_pos option[value='"+domisili_kd_pos+"']").attr('selected', 'selected');
                $("#domisili_kode_pos_bdn_hukum option[value='"+domisili_kd_pos+"']").attr('selected', 'selected');
            });
            // END: Domisili Kode Pos
            
            // START: Domisili Status Rumah
            $("input[name=domisili_status_rumah][value='" + domisili_status_rumah + "']").attr('checked', 'checked');
            
            $('#domisili_status, #domisili_status_bdn_hukum').change(function(){
                
                // kill
                if($('#domisili_status').is(':checked')){
                    var alamat 			                = $("#alamat").val();
                    var provinsi 	            		= $("#provinsi option:selected").val();
                    var kota 				            = $("#kota option:selected").val();
                    var kecamatan                       = $("#kecamatan option:selected").val();
                    var kelurahan                       = $("#kelurahan option:selected").val();
                    var kode_pos 			            = $("#kode_pos option:selected").val();
                    
                    $('#domisili_status').val("1");
                    $('#domisili_alamat').text(alamat);
                    $('#domisili_provinsi').append($('<option>', { value: provinsi, text : provinsi}));
                    $('#domisili_kota').append($('<option>', { value: kota, text : kota}));
                    $('#domisili_kecamatan').append($('<option>', { value: kecamatan, text : kecamatan}));
                    $('#domisili_kelurahan').append($('<option>', { value: kelurahan, text : kelurahan}));
                    $('#domisili_kode_pos').append($('<option>', { value: kode_pos, text : kode_pos}));

                    $("#domisili_provinsi option[value='"+provinsi+"']").attr('selected', 'selected');
                    $("#domisili_kota option[value='"+kota+"']").attr('selected', 'selected');
                    $("#domisili_kecamatan option[value='"+kecamatan+"']").attr('selected', 'selected');
                    $("#domisili_kelurahan option[value='"+kelurahan+"']").attr('selected', 'selected');
                    $("#domisili_kode_pos option[value='"+kode_pos+"']").attr('selected', 'selected');
                    
                    $("#domisili_alamat").attr('disabled', 'true');
                    $("#domisili_provinsi").attr('disabled', 'true');
                    $("#domisili_kota").attr('disabled', 'true');
                    $("#domisili_kelurahan").attr('disabled', 'true');
                    $("#domisili_kecamatan").attr('disabled', 'true');
                    $("#domisili_kode_pos").attr('disabled', 'true');
                    // $("#domisili_alamat_lengkap").addClass("d-none");
                } else {
                    $('#domisili_status').val("0");
                    $("#domisili_alamat").prop('disabled', false);
                    $("#domisili_provinsi").prop('disabled', false);
                    $("#domisili_kota").prop('disabled', false);
                    $("#domisili_kelurahan").prop('disabled', false);
                    $("#domisili_kecamatan").prop('disabled', false);
                    $("#domisili_kode_pos").prop('disabled', false);
                    // $("#domisili_alamat_lengkap").removeClass("d-none");
                }

                // kill
                if($('#domisili_status_bdn_hukum').is(':checked')){
                    var alamat 			                = $("#alamat_bdn_hukum").val();
                    var provinsi 	            		= $("#provinsi_bdn_hukum option:selected").val();
                    var kota 				            = $("#kota_bdn_hukum option:selected").val();
                    var kecamatan                       = $("#kecamatan_bdn_hukum option:selected").val();
                    var kelurahan                       = $("#kelurahan_bdn_hukum option:selected").val();
                    var kode_pos 			            = $("#kode_pos_bdn_hukum option:selected").val();

                    $('#domisili_status').val("1");
                    $('#domisili_alamat_bdn_hukum').text(alamat);
                    $('#domisili_provinsi_bdn_hukum').append($('<option>', { value: provinsi, text : provinsi}));
                    $('#domisili_kota_bdn_hukum').append($('<option>', { value: kota, text : kota}));
                    $('#domisili_kecamatan_bdn_hukum').append($('<option>', { value: kecamatan, text : kecamatan}));
                    $('#domisili_kelurahan_bdn_hukum').append($('<option>', { value: kelurahan, text : kelurahan}));
                    $('#domisili_kode_pos_bdn_hukum').append($('<option>', { value: kode_pos, text : kode_pos}));

                    $("#domisili_provinsi_bdn_hukum option[value='"+provinsi+"']").attr('selected', 'selected');
                    $("#domisili_kota_bdn_hukum option[value='"+kota+"']").attr('selected', 'selected');
                    $("#domisili_kecamatan_bdn_hukum option[value='"+kecamatan+"']").attr('selected', 'selected');
                    $("#domisili_kelurahan_bdn_hukum option[value='"+kelurahan+"']").attr('selected', 'selected');
                    $("#domisili_kode_pos_bdn_hukum option[value='"+kode_pos+"']").attr('selected', 'selected');
                    
                    $("#domisili_alamat_bdn_hukum").attr('disabled', 'true');
                    $("#domisili_provinsi_bdn_hukum").attr('disabled', 'true');
                    $(" #domisili_kota_bdn_hukum").attr('disabled', 'true');
                    $(" #domisili_kelurahan_bdn_hukum").attr('disabled', 'true');
                    $("#domisili_kecamatan_bdn_hukum").attr('disabled', 'true');
                    $("#domisili_kode_pos_bdn_hukum").attr('disabled', 'true');
                    // $("#domisili_alamat_lengkap").addClass("d-none");
                } else {
                    $('#domisili_status').val("0");
                    $("#domisili_alamat_bdn_hukum").prop('disabled', false);
                    $("#domisili_provinsi_bdn_hukum").prop('disabled', false);
                    $("#domisili_kota_bdn_hukum").prop('disabled', false);
                    $("#domisili_kelurahan_bdn_hukum").prop('disabled', false);
                    $("#domisili_kecamatan_bdn_hukum").prop('disabled', false);
                    $("#domisili_kode_pos_bdn_hukum").prop('disabled', false);
                    // $("#domisili_alamat_lengkap").removeClass("d-none");
                }
            });
            // END: Domisili Status Rumah
            // END: Alamat domisili

            // START: Pasangan
            var jns_kelamin_pasangan = '{{ $pasanganBrw->jenis_kelamin }}' ;
            $("input[name=jns_kelamin_pasangan][value='"+jns_kelamin_pasangan+"']").attr('checked', 'checked');

            let no_hp_pasangan = '{{ $pasanganBrw->no_hp }}';
            if(no_hp_pasangan.substring(0,3) == '620') {
                $('#telepon_pasangan').val(no_hp_pasangan.substring(3));
            } else if(no_hp_pasangan.substring(0,2) == '62') {
                $('#telepon_pasangan').val(no_hp_pasangan.substring(2));
            } else if(no_hp_pasangan.substring(0,1) == '0') {
                $('#telepon_pasangan').val(no_hp_pasangan.substring(1));
            } else {
                $('#telepon_pasangan').val(no_hp_pasangan);
            }

            // START: Alamat Pasangan
            var provinsi_pasangan = '{{$pasanganBrw->provinsi}}';
            var kota_pasangan = '{{$pasanganBrw->kota}}';
            var kecamatan_pasangan = '{{$pasanganBrw->kecamatan}}';
            var kelurahan_pasangan = '{{$pasanganBrw->kelurahan}}';
            var kode_pos_pasangan = '{{$pasanganBrw->kode_pos}}';
            // START: Provinsi
            $('#provinsi_pasangan').change(function(){
                var provinsi = $('#provinsi_pasangan option:selected').text();
                $('#kota_pasangan').empty().trigger('change'); // set null
                $('#kota_pasangan').append($('<option>', { value: '', text : 'Pilih kota'}));
                $.getJSON("/borrower/data_kota/"+provinsi+"/", function(data_kota){
                    for(var i = 0; i<data_kota.length; i++){
                        $('#kota_pasangan').append($('<option>', { 
                            value: data_kota[i].text,
                            text : data_kota[i].text
                        }));
                    }
                });
            });
            // END: Provinsi

            // START: Kota
            $.getJSON("/borrower/data_kota/"+provinsi_pasangan+"/", function(data_kota){
                for(var i = 0; i<data_kota.length; i++){
                    $('#kota_pasangan').append($('<option>', { 
                        value: data_kota[i].text,
                        text : data_kota[i].text
                    }));
                }
                $("#kota_pasangan option[value='"+kota_pasangan+"']").attr('selected', 'selected');
            });

            $('#kota_pasangan').change(function(){
                var kota = $('#kota_pasangan option:selected').text();
                $("#kecamatan_pasangan").empty().trigger('change'); // set null
                $('#kecamatan_pasangan').append($('<option>', {  value : '', text : 'Pilih Kecamatan' }));
                $.getJSON("/borrower/data_kecamatan/"+kota+"/", function(data_kecamatan){
            
                    for(var i = 0; i<data_kecamatan.length; i++){
                        $('#kecamatan_pasangan').append($('<option>', { 
                            value: data_kecamatan[i].text,
                            text : data_kecamatan[i].text
                        }));
                    }
                });
            });
            // END: Kota

            // START: Kecamatan
            $.getJSON("/borrower/data_kecamatan/"+kota_pasangan+"/", function(data_kecamatan){
                for(var i = 0; i<data_kecamatan.length; i++){
                    $('#kecamatan_pasangan').append($('<option>', { 
                        value: data_kecamatan[i].text,
                        text : data_kecamatan[i].text
                    }));
                }
                $("#kecamatan_pasangan option[value='"+kecamatan_pasangan+"']").attr('selected', 'selected');
            });

            $('#kecamatan_pasangan').change(function(){
                var kecamatan = $('#kecamatan_pasangan option:selected').text();
                $('#kelurahan_pasangan').empty().trigger('change'); // set null
                $('#kelurahan_pasangan').append($('<option>', { value: '', text : 'Pilih Kelurahan'}));
                $.getJSON("/borrower/data_kelurahan/"+kecamatan+"/", function(data_kelurahan){
            
                    for(var i = 0; i<data_kelurahan.length; i++){
                        $('#kelurahan_pasangan').append($('<option>', { 
                            value: data_kelurahan[i].text,
                            text : data_kelurahan[i].text
                        }));
                    }
                });
            });
            // END: Kecamatan

            // START: Kelurahan
            $.getJSON("/borrower/data_kelurahan/"+kecamatan_pasangan+"/", function(data_kelurahan){
                for(var i = 0; i<data_kelurahan.length; i++){
                    $('#kelurahan_pasangan').append($('<option>', { 
                        value: data_kelurahan[i].text,
                        text : data_kelurahan[i].text
                    }));
                }
                $("#kelurahan_pasangan option[value='"+kelurahan_pasangan+"']").attr('selected', 'selected');
            });

            $('#kelurahan_pasangan').change(function(){
                var kelurahan = $('#kelurahan_pasangan option:selected').text();
                $('#kode_pos_pasangan').empty().trigger('change'); // set null
                $.getJSON("/borrower/data_kode_pos/"+kelurahan+"/", function(data_kode_pos){
            
                    for(var i = 0; i<data_kode_pos.length; i++){
                        $('#kode_pos_pasangan').append($('<option>', { 
                            value: data_kode_pos[i].id,
                            text : data_kode_pos[i].text
                        }));
                    }
                });
            });
            // END: Kelurahan

            // START: Kode Pos
            $.getJSON("/borrower/data_kode_pos/"+kelurahan_pasangan+"/", function(data_kode_pos){
                for(var i = 0; i<data_kode_pos.length; i++){
                    $('#kode_pos_pasangan').append($('<option>', { 
                        value: data_kode_pos[i].id,
                        text : data_kode_pos[i].text
                    }));
                }
                $("#kode_pos_pasangan option[value='"+kode_pos_pasangan+"']").attr('selected', 'selected');
            });
            // END: Kode Pos
            let alamat_pasangan = "{{ $pasanganBrw->alamat }}";
            $('#alamat_pasangan').text(alamat_pasangan);
            
            $("input[name=status_rumah][value='" + status_rumah + "']").attr('checked', 'checked');
            // END: Alamat Pasangan
            // END: Pasangan
            // END: Informasi Pribadi

            //START: Rekening
            if (brw_nm_pemilik || brw_norek || brw_kd_bank) {
                $("#bank_rekanan").attr('checked', 'checked');
                $("#informasi_bank_rekanan").removeClass("d-none");
            } else {
                $("#informasi_bank_rekanan").addClass("d-none");
            }

            $('#bank_rekanan').change(function(){
                if($('#bank_rekanan').is(':checked')){
                    $("#informasi_bank_rekanan").removeClass("d-none");
                } else {
                    $("#informasi_bank_rekanan").addClass("d-none");
                }
            });
            
            $('#norekening').val(brw_norek);
            $('#namapemilikrekening').val(brw_nm_pemilik);
            $.getJSON("/borrower/data_bank/", function(data_bank){
                for(var i = 0; i<data_bank.length; i++){
                    $('#bank').append($('<option>', { 
                        value: data_bank[i].id,
                        text : data_bank[i].text
                    }));
                }
                $("#bank option[value='"+brw_kd_bank+"']").attr('selected', 'selected');
            });
            //END: Rekening

            //start rekening_bdnhkm
            if (brw_nm_pemilik || brw_norek || brw_kd_bank) {
                $("#bank_rekanan_bdn_hukum").attr('checked', 'checked');
                $("#informasi_bank_rekanan_bdn_hukum").removeClass("d-none");
            } else {
                $("#informasi_bank_rekanan_bdn_hukum").addClass("d-none");
            }

            $('#bank_rekanan_bdn_hukum').change(function(){
                if($('#bank_rekanan_bdn_hukum').is(':checked')){
                    $("#informasi_bank_rekanan_bdn_hukum").removeClass("d-none");
                } else {
                    $("#informasi_bank_rekanan_bdn_hukum").addClass("d-none");
                }
            });

            $('#norekening_bdnhkm').val(brw_norek);
            $('#namapemilikrekening_bdnhkm').val(brw_nm_pemilik);
            $.getJSON("/borrower/data_bank/", function(data_bank){
                for(var i = 0; i<data_bank.length; i++){
                    $('#bank_bdnhkm').append($('<option>', { 
                        value: data_bank[i].id,
                        text : data_bank[i].text
                    }));
                }
                $("#bank_bdnhkm option[value='"+brw_kd_bank+"']").attr('selected', 'selected');
            });
            //end rekening brw

            // START: Pekerjaan Individu
            $.getJSON("/borrower/data_pekerjaan/", function(data_pekerjaan){
                for(var i = 0; i<data_pekerjaan.length; i++){
                    $('#pekerjaan').append($('<option>', { 
                        value: data_pekerjaan[i].id,
                        text : data_pekerjaan[i].text   
                    }));
                }
                $("#pekerjaan option[value='"+pekerjaan+"']").attr('selected', 'selected');
            });
            $.getJSON("/borrower/data_bidang_pekerjaan/", function(data_bidang_pekerjaan){
                for(var i = 0; i<data_bidang_pekerjaan.length; i++){
                    $('#bidang_pekerjaan').append($('<option>', { 
                        value: data_bidang_pekerjaan[i].id,
                        text : data_bidang_pekerjaan[i].text
                    }));
                }
                if (pekerjaan != '8') {
                    $("#bidang_pekerjaan option[value='"+bidang_pekerjaan+"']").attr('selected', 'selected');
                }
            });

            $.getJSON("/borrower/bidang_pekerjaan_online/", function(data_bidang_online){
                for(var i = 0; i<data_bidang_online.length; i++){
                    $('#bidang_online').append($('<option>', { 
                        value: data_bidang_online[i].id,
                        text : data_bidang_online[i].text 
                    }));
                }
                if (pekerjaan != '8') {
                    $("#bidang_online option[value='"+bidang_online+"']").attr('selected', 'selected');
                }
            });

            $.getJSON("/borrower/data_pengalaman_pekerjaan/", function(data_pengalaman){
                for(var i = 0; i<data_pengalaman.length; i++){
                    $('#pengalaman_kerja').append($('<option>', { 
                        value: data_pengalaman[i].id,
                        text : data_pengalaman[i].text 
                    }));
                }
                if (pekerjaan != '8') {
                    $("#pengalaman_kerja option[value='"+pengalaman_pekerjaan+"']").attr('selected', 'selected');
                }
            });

            $.getJSON("/borrower/data_pendapatan/", function(data_pendapatan){
                for(var i = 0; i<data_pendapatan.length; i++){  
                    $('#pendapatan_bulanan').append($('<option>', { 
                        value: data_pendapatan[i].id,
                        text : data_pendapatan[i].text 
                    }));
                }
                if (pekerjaan != '8') {
                    $("#pendapatan_bulanan option[value='"+pendapatan+"']").attr('selected', 'selected');
                }
            });

            $('#pekerjaan').change(function(){
                var pekerjaan = $('#pekerjaan option:selected').val();
                if (pekerjaan != '8') {
                    $("#bidang_pekerjaan").attr('disabled', false);
                    $("#bidang_online").attr('disabled', false);
                    $("#pengalaman_kerja").attr('disabled', false);
                    $("#pendapatan_bulanan").attr('disabled', false);

                    $("#pengalaman_kerja").val(pengalaman_pekerjaan);
                    $("#pendapatan_bulanan").val(pendapatan);
                    $("#bidang_pekerjaan").val(bidang_pekerjaan);
                    $("#bidang_online").val(bidang_online);

                } else {
                    $("#bidang_pekerjaan").attr('disabled', true);
                    $("#bidang_online").attr('disabled', true);
                    $("#pengalaman_kerja").attr('disabled', true);
                    $("#pendapatan_bulanan").attr('disabled', true);

                    $("#pengalaman_kerja").val('');
                    $("#pendapatan_bulanan").val('');
                    $("#bidang_pekerjaan").val('');
                    $("#bidang_online").val('');

                }
            });
            if (pekerjaan == '8') {
                $("#bidang_pekerjaan").attr('disabled', true);
                $("#bidang_online").attr('disabled', true);
                $("#pengalaman_kerja").attr('disabled', true);
                $("#pendapatan_bulanan").attr('disabled', true);
            }
            // END: Pekerjaan Individu

            // bdn hukum
            $('#nm_bdn_hukum').val(nm_bdn_hukum);
            $('#nib').val(nib);
            $('#nama_pendaftar').val(nama);
            $('#nikPendaftar').val(ktp);
            $('#no_akta_pendirian').val(no_akta_pendirian);
            $('#tgl_berdiri').val(tgl_berdiri);
            $('#hpPendaftar').val(telpon_perusahaan.substring(3));
            $('#jabatanPendaftar').val(jabatan);
            $('#npwp_bdn_hukum').val(npwp_perusahaan);

            // START: Pengurus
            let jabatan_pengurus1 = '{{ $pengurusBrw1->jabatan }}';
            let jabatan_pengurus2 = '{{ $pengurusBrw2->jabatan }}';
            $.getJSON("/borrower/data_jabatan", function(data_jabatan){
                for(var i = 0; i<data_jabatan.length; i++){
                    $('#jabatan_pengurus1').append($('<option>', { 
                        value: data_jabatan[i].id,
                        text : data_jabatan[i].text
                    }));
                    $('#jabatan_pengurus2').append($('<option>', { 
                        value: data_jabatan[i].id,
                        text : data_jabatan[i].text
                    }));
                }
                $("#jabatan_pengurus1 option[value='"+jabatan_pengurus1+"']").attr('selected', 'selected');
                $("#jabatan_pengurus2 option[value='"+jabatan_pengurus2+"']").attr('selected', 'selected');

			})
            // START: Pengurus 1
            let jns_kelamin_pengurus1 = '{{ $pengurusBrw1->jenis_kelamin }}';
            let no_tlp_pengurus1_check = '{{ $pengurusBrw1->no_tlp }}';
            if(no_tlp_pengurus1_check.substring(0,3) == '620') {
                $('#tlp_pengurus1').val(no_tlp_pengurus1_check.substring(3));
            } else if(no_tlp_pengurus1_check.substring(0,2) == '62') {
                $('#tlp_pengurus1').val(no_tlp_pengurus1_check.substring(2));
            } else if(no_tlp_pengurus1_check.substring(0,1) == '0') {
                $('#tlp_pengurus1').val(no_tlp_pengurus1_check.substring(1));
            } else {
                $('#tlp_pengurus1').val(no_tlp_pengurus1_check);
            }
            $("#tlp_pengurus1").on("input", function() {
                if (/^0/.test(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
            $("input[name=jns_kelamin_pengurus1][value='" + jns_kelamin_pengurus1 + "']").attr('checked', 'checked');

            // START: Provinsi
            let provinsi_pengurus1 = '{{ $pengurusBrw1->provinsi }}';
            let kota_pengurus1 = '{{ $pengurusBrw1->kota }}';
            let kecamatan_pengurus1 = '{{ $pengurusBrw1->kecamatan }}';
            let kelurahan_pengurus1 = '{{ $pengurusBrw1->kelurahan }}';
            let kode_pos_pengurus1 = '{{ $pengurusBrw1->kode_pos }}';
            $('#provinsi_pengurus1').change(function(){
                var provinsi = $('#provinsi_pengurus1 option:selected').val();
                $('#kota_pengurus1').empty().trigger('change'); // set null
                $('#kota_pengurus1').append($('<option>', { value: '', text : 'Pilih kota'}));
                $.getJSON("/borrower/data_kota_pengurus/"+provinsi+"/", function(data_kota){
                    for(var i = 0; i<data_kota.length; i++){
                        $('#kota_pengurus1').append($('<option>', { 
                            value: data_kota[i].id,
                            text : data_kota[i].text
                        }));
                    }
                });
            });
            // END: Provinsi

            // START: Kota
            $.getJSON("/borrower/data_kota_pengurus/"+provinsi_pengurus1+"/", function(data_kota){
                for(var i = 0; i<data_kota.length; i++){
                    $('#kota_pengurus1').append($('<option>', { 
                        value: data_kota[i].id,
                        text : data_kota[i].text
                    }));
                }
                $("#kota_pengurus1 option[value='"+kota_pengurus1+"']").attr('selected', 'selected');
            });

            $('#kota_pengurus1').change(function(){
                var kota = $('#kota_pengurus1 option:selected').val();
                $("#kecamatan_pengurus1").empty().trigger('change'); // set null
                $('#kecamatan_pengurus1').append($('<option>', {  value : '', text : 'Pilih Kecamatan' }));
                $.getJSON("/borrower/data_kecamatan_pengurus/"+kota+"/", function(data_kecamatan){
            
                    for(var i = 0; i<data_kecamatan.length; i++){
                        $('#kecamatan_pengurus1').append($('<option>', { 
                            value: data_kecamatan[i].id,
                            text : data_kecamatan[i].text
                        }));
                    }
                });
            });
            // END: Kota

            // START: Kecamatan
            $.getJSON("/borrower/data_kecamatan_pengurus/"+kota_pengurus1+"/", function(data_kecamatan){
                for(var i = 0; i<data_kecamatan.length; i++){
                    $('#kecamatan_pengurus1').append($('<option>', { 
                        value: data_kecamatan[i].id,
                        text : data_kecamatan[i].text
                    }));
                }
                $("#kecamatan_pengurus1 option[value='"+kecamatan_pengurus1+"']").attr('selected', 'selected');
            });

            $('#kecamatan_pengurus1').change(function(){
                var kecamatan = $('#kecamatan_pengurus1 option:selected').val();
                $('#kelurahan_pengurus1').empty().trigger('change'); // set null
                $('#kelurahan_pengurus1').append($('<option>', { value: '', text : 'Pilih Kelurahan'}));
                $.getJSON("/borrower/data_kelurahan_pengurus/"+kecamatan+"/", function(data_kelurahan){
            
                    for(var i = 0; i<data_kelurahan.length; i++){
                        $('#kelurahan_pengurus1').append($('<option>', { 
                            value: data_kelurahan[i].id,
                            text : data_kelurahan[i].text
                        }));
                    }
                });
            });
            // END: Kecamatan

            // START: Kelurahan
            $.getJSON("/borrower/data_kelurahan_pengurus/"+kecamatan_pengurus1+"/", function(data_kelurahan){
                for(var i = 0; i<data_kelurahan.length; i++){
                    $('#kelurahan_pengurus1').append($('<option>', { 
                        value: data_kelurahan[i].id,
                        text : data_kelurahan[i].text
                    }));
                }
                $("#kelurahan_pengurus1 option[value='"+kelurahan_pengurus1+"']").attr('selected', 'selected');
            });

            $('#kelurahan_pengurus1').change(function(){
                var kelurahan = $('#kelurahan_pengurus1 option:selected').val();
                $('#kode_pos_pengurus1').empty().trigger('change'); // set null
                $.getJSON("/borrower/data_kode_pos_pengurus/"+kelurahan+"/", function(data_kode_pos){
            
                    for(var i = 0; i<data_kode_pos.length; i++){
                        $('#kode_pos_pengurus1').append($('<option>', { 
                            value: data_kode_pos[i].id,
                            text : data_kode_pos[i].text
                        }));
                    }
                });
            });
            // END: Kelurahan

            // START: Kode Pos
            $.getJSON("/borrower/data_kode_pos_pengurus/"+kelurahan_pengurus1+"/", function(data_kode_pos){
                for(var i = 0; i<data_kode_pos.length; i++){
                    $('#kode_pos_pengurus1').append($('<option>', { 
                        value: data_kode_pos[i].id,
                        text : data_kode_pos[i].text
                    }));
                }
                $("#kode_pos_pengurus1 option[value='"+kode_pos_pengurus1+"']").attr('selected', 'selected');
            });
            // END: pengurus 1

            // START: Pengurus 2
            let jns_kelamin_pengurus2 = '{{ $pengurusBrw2->jenis_kelamin }}';
            let no_tlp_pengurus2_check = '{{ $pengurusBrw2->no_tlp }}';
            if(no_tlp_pengurus2_check.substring(0,3) == '620') {
                $('#tlp_pengurus2').val(no_tlp_pengurus2_check.substring(3));
            } else if(no_tlp_pengurus2_check.substring(0,2) == '62') {
                $('#tlp_pengurus2').val(no_tlp_pengurus2_check.substring(2));
            } else if(no_tlp_pengurus2_check.substring(0,1) == '0') {
                $('#tlp_pengurus2').val(no_tlp_pengurus2_check.substring(1));
            } else {
                $('#tlp_pengurus2').val(no_tlp_pengurus2_check);
            }
            $("#tlp_pengurus2").on("input", function() {
                if (/^0/.test(this.value)) {
                    this.value = this.value.replace(/^0/, "")
                }
            })
            $("input[name=jns_kelamin_pengurus2][value='" + jns_kelamin_pengurus2 + "']").attr('checked', 'checked');

            // START: Provinsi

            let provinsi_pengurus2 = '{{ $pengurusBrw2->provinsi }}';
            let kota_pengurus2 = '{{ $pengurusBrw2->kota }}';
            let kecamatan_pengurus2 = '{{ $pengurusBrw2->kecamatan }}';
            let kelurahan_pengurus2 = '{{ $pengurusBrw2->kelurahan }}';
            let kode_pos_pengurus2 = '{{ $pengurusBrw2->kode_pos }}';
            $('#provinsi_pengurus2').change(function(){
                var provinsi = $('#provinsi_pengurus2 option:selected').val();
                $('#kota_pengurus2').empty().trigger('change'); // set null
                $('#kota_pengurus2').append($('<option>', { value: '', text : 'Pilih kota'}));
                $.getJSON("/borrower/data_kota_pengurus/"+provinsi+"/", function(data_kota){
                    for(var i = 0; i<data_kota.length; i++){
                        $('#kota_pengurus2').append($('<option>', { 
                            value: data_kota[i].id,
                            text : data_kota[i].text
                        }));
                    }
                });
            });
            // END: Provinsi

            // START: Kota
            $.getJSON("/borrower/data_kota_pengurus/"+provinsi_pengurus2+"/", function(data_kota){
                for(var i = 0; i<data_kota.length; i++){
                    $('#kota_pengurus2').append($('<option>', { 
                        value: data_kota[i].id,
                        text : data_kota[i].text
                    }));
                }
                $("#kota_pengurus2 option[value='"+kota_pengurus2+"']").attr('selected', 'selected');
            });

            $('#kota_pengurus2').change(function(){
                var kota = $('#kota_pengurus2 option:selected').val();
                $("#kecamatan_pengurus2").empty().trigger('change'); // set null
                $('#kecamatan_pengurus2').append($('<option>', {  value : '', text : 'Pilih Kecamatan' }));
                $.getJSON("/borrower/data_kecamatan_pengurus/"+kota+"/", function(data_kecamatan){
            
                    for(var i = 0; i<data_kecamatan.length; i++){
                        $('#kecamatan_pengurus2').append($('<option>', { 
                            value: data_kecamatan[i].id,
                            text : data_kecamatan[i].text
                        }));
                    }
                });
            });
            // END: Kota

            // START: Kecamatan
            $.getJSON("/borrower/data_kecamatan_pengurus/"+kota_pengurus2+"/", function(data_kecamatan){
                for(var i = 0; i<data_kecamatan.length; i++){
                    $('#kecamatan_pengurus2').append($('<option>', { 
                        value: data_kecamatan[i].id,
                        text : data_kecamatan[i].text
                    }));
                }
                $("#kecamatan_pengurus2 option[value='"+kecamatan_pengurus2+"']").attr('selected', 'selected');
            });

            $('#kecamatan_pengurus2').change(function(){
                var kecamatan = $('#kecamatan_pengurus2 option:selected').val();
                $('#kelurahan_pengurus2').empty().trigger('change'); // set null
                $('#kelurahan_pengurus2').append($('<option>', { value: '', text : 'Pilih Kelurahan'}));
                $.getJSON("/borrower/data_kelurahan_pengurus/"+kecamatan+"/", function(data_kelurahan){
            
                    for(var i = 0; i<data_kelurahan.length; i++){
                        $('#kelurahan_pengurus2').append($('<option>', { 
                            value: data_kelurahan[i].id,
                            text : data_kelurahan[i].text
                        }));
                    }
                });
            });
            // END: Kecamatan

            // START: Kelurahan
            $.getJSON("/borrower/data_kelurahan_pengurus/"+kecamatan_pengurus2+"/", function(data_kelurahan){
                for(var i = 0; i<data_kelurahan.length; i++){
                    $('#kelurahan_pengurus2').append($('<option>', { 
                        value: data_kelurahan[i].id,
                        text : data_kelurahan[i].text
                    }));
                }
                $("#kelurahan_pengurus2 option[value='"+kelurahan_pengurus2+"']").attr('selected', 'selected');
            });

            $('#kelurahan_pengurus2').change(function(){
                var kelurahan = $('#kelurahan_pengurus2 option:selected').val();
                $('#kode_pos_pengurus2').empty().trigger('change'); // set null
                $.getJSON("/borrower/data_kode_pos_pengurus/"+kelurahan+"/", function(data_kode_pos){
            
                    for(var i = 0; i<data_kode_pos.length; i++){
                        $('#kode_pos_pengurus2').append($('<option>', { 
                            value: data_kode_pos[i].id,
                            text : data_kode_pos[i].text
                        }));
                    }
                });
            });
            // END: Kelurahan

            // START: Kode Pos
            $.getJSON("/borrower/data_kode_pos_pengurus/"+kelurahan_pengurus2+"/", function(data_kode_pos){
                for(var i = 0; i<data_kode_pos.length; i++){
                    $('#kode_pos_pengurus2').append($('<option>', { 
                        value: data_kode_pos[i].id,
                        text : data_kode_pos[i].text
                    }));
                }
                $("#kode_pos_pengurus2 option[value='"+kode_pos_pengurus2+"']").attr('selected', 'selected');
            });
            // END: Kode Pos

            //Bidang Usaha Badan Hukum
            $.getJSON("/borrower/data_bidang_pekerjaan/", function(data_bidang_pekerjaan){
                let bidang_usaha = "{{ $bidang_usaha }}"
                for(var i = 0; i<data_bidang_pekerjaan.length; i++){
                    $('#bidang_pekerjaan_bdn_hukum').append($('<option>', { 
                        value: data_bidang_pekerjaan[i].id,
                        text : data_bidang_pekerjaan[i].text
                    }));
                }
                $("#bidang_pekerjaan_bdn_hukum option[value='"+bidang_usaha+"']").attr('selected', 'selected');
            });

            $.getJSON("/borrower/bidang_pekerjaan_online/", function(data_bidang_online){
                for(var i = 0; i<data_bidang_online.length; i++){
                    $('#bidang_online_bdn_hukum').append($('<option>', { 
                        value: data_bidang_online[i].id,
                        text : data_bidang_online[i].text 
                    }));
                }
                $("#bidang_online_bdn_hukum option[value='"+bidang_online+"']").attr('selected', 'selected');
            });

            $.getJSON("/borrower/data_omset/", function(data_pendapatan){
                let omset_tahun_terakhir = "{{ $omset_tahun_terakhir }}";
                for(var i = 0; i<data_pendapatan.length; i++){  
                    $('#pendapatan_bdn_hukum').append($('<option>', { 
                        value: data_pendapatan[i].id,
                        text : data_pendapatan[i].text 
                    }));
                }
                if (bidang_usaha != '20') {
                    $("#pendapatan_bdn_hukum option[value='"+omset_tahun_terakhir+"']").attr('selected', 'selected');
                }
            });

            $.getJSON("/borrower/data_omset/", function(data_pendapatan){
                let tot_aset_tahun_terakhir = "{{ $tot_aset_tahun_terakhir }}"
                for(var i = 0; i<data_pendapatan.length; i++){  
                    $('#total_aset').append($('<option>', { 
                        value: data_pendapatan[i].id,
                        text : data_pendapatan[i].text 
                    }));
                }
                if (bidang_usaha != '20') {
                    $("#total_aset option[value='"+tot_aset_tahun_terakhir+"']").attr('selected', 'selected');
                }
            });

            $('#bidang_pekerjaan_bdn_hukum').change(function(){
                var bidang_pekerjaan_bdn_hukum = $('#bidang_pekerjaan_bdn_hukum option:selected').val();
                if (bidang_pekerjaan_bdn_hukum != '20') {
                    $("#pendapatan_bdn_hukum").attr('disabled', false);
                    $("#total_aset").attr('disabled', false);

                    $("#pendapatan_bdn_hukum").val(omset_tahun_terakhir);
                    $("#total_aset").val(tot_aset_tahun_terakhir);

                } else {
                    $("#pendapatan_bdn_hukum").attr('disabled', true);
                    $("#total_aset").attr('disabled', true);

                    $("#pendapatan_bdn_hukum").val('');
                    $("#total_aset").val('');

                }
            });
            let bidang_usaha = "{{ $bidang_usaha }}"
            if (bidang_usaha == '20') {
                $("#pendapatan_bdn_hukum").attr('disabled', true);
                $("#total_aset").attr('disabled', true);
            }
            // END: Bidang Usaha Badan Hukum
            // $('#total_aset').val(total_aset);
            
            $("#notifg").hide();

            $("#form_bdn_hukum").on('submit', function postinput(e){
                e.preventDefault();

                var sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';
                swal.fire({
                    text: "Anda Yakin akan Mengubah data Profil Badan Hukum ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Proses",
                    cancelButtonText: "Batal",
                    reverseButtons: true,
                }).then((result) => {
                    if (result.value) {
                        var brw_id 		= $('#id').val(); 
                        var brw_type 	= $('#brw_type').val();
                
                        /***************** BADAN HUKUM ******************/
                        
                        var nm_bdn_hukum 				    = $("#nm_bdn_hukum").val();
                        var npwp_bdn_hukum 			        = $("#npwp_bdn_hukum").val();
                        var nama_pendaftar 			        = $("#nama_pendaftar").val();
                        var nikPendaftar 			        = $("#nikPendaftar").val();
                        var hpPendaftar 			        = "120".concat($("#hpPendaftar").val());
                        var jabatanPendaftar 				= $("#jabatanPendaftar").val();
                        var nib                             = $("#nib").val();
                        var no_akta_pendirian               = $("#no_akta_pendirian").val();
                        var tgl_berdiri                     = $("#tgl_berdiri").val();

                        // var namapengurus 				    = $("#namapengurus").val();
                        // var nikpengurus 				    = $("#nikpengurus").val();
                        // var teleponpengurus 				= "62".concat($("#teleponpengurus").val());
                        // var jabatanpengurus 				= $("#jabatanpengurus").val();

                        var namapemilikrekening_bdnhkm 		= $("#namapemilikrekening_bdnhkm").val();
                        var norekening_bdnhkm 				= $("#norekening_bdnhkm").val();
                        var bank_bdnhkm 				    = $("#bank_bdnhkm option:selected").val();
                        var bidang_usaha 				    = $("#bidang_pekerjaan_bdn_hukum option:selected").val();

                        // alamat
                        var provinsi_bdn_hukum 				= $("#provinsi_bdn_hukum option:selected").val();
                        var kota_bdn_hukum 				    = $("#kota_bdn_hukum option:selected").val();
                        var kecamatan_bdn_hukum 			= $("#kecamatan_bdn_hukum option:selected").val();
                        var kelurahan_bdn_hukum 			= $("#kelurahan_bdn_hukum option:selected").val();
                        var kode_pos_bdn_hukum 				= $("#kode_pos_bdn_hukum option:selected").val();
                        var alamat_bdn_hukum 				= $("#alamat_bdn_hukum").val();

                        // alamat domisili
                        var domisili_status                 = $(".domisili_status_bdn_hukum").val();
                        var domisili_provinsi 	            = $("#domisili_provinsi_bdn_hukum option:selected").val();
                        var domisili_kota 				    = $("#domisili_kota_bdn_hukum option:selected").val();
                        var domisili_kecamatan              = $("#domisili_kecamatan_bdn_hukum option:selected").val();
                        var domisili_kelurahan              = $("#domisili_kelurahan_bdn_hukum option:selected").val();
                        var domisili_alamat 			    = $("#domisili_alamat_bdn_hukum").val();
                        var domisili_kd_pos 			    = $("#domisili_kode_pos_bdn_hukum option:selected").val();   

                        var bidang_pekerjaan_bdn_hukum 	    = $("#bidang_pekerjaan_bdn_hukum option:selected").val();
                        var bidang_online_bdn_hukum 		= $("#bidang_online_bdn_hukum option:selected").val();
                        var pendapatan_bdn_hukum 			= $("#pendapatan_bdn_hukum option:selected").val();
                        var total_aset 				        = $("#total_aset option:selected").val();

                        // pengurus 1
                        var id_pengurus1                    = $("#id_pengurus1").val();
                        var nm_pengurus1 				    = $("#nama_pengurus1").val();
                        var jns_kelamin_pengurus1			= $("input[name=jns_kelamin_pengurus1]:checked").val();
                        var nik_pengurus1   			    = $("#ktp_pengurus1").val();
                        var tmpt_lahir_pengurus1 	        = $("#tmpt_lahir_pengurus1").val();
                        var tgl_lahir_pengurus1 	        = $("#tgl_lahir_pengurus1").val();
                        var tlp_pengurus1 				    = "62".concat($("#tlp_pengurus1").val());
                        var agama_pengurus1 			    = $("#agama_pengurus1 option:selected").val();
                        var pendidikan_terakhir_pengurus1	= $("#pendidikan_terakhir_pengurus1 option:selected").val();
                        var npwp_pengurus1 				    = $("#npwp_pengurus1").val();
                        var jabatan_pengurus1 				= $("#jabatan_pengurus1").val();
                        var alamat_pengurus1 	            = $("#alamat_pengurus1").val();
                        var provinsi_pengurus1      		= $("#provinsi_pengurus1 option:selected").val();
                        var kota_pengurus1  	            = $("#kota_pengurus1 option:selected").val();
                        var kecamatan_pengurus1             = $("#kecamatan_pengurus1 option:selected").val();
                        var kelurahan_pengurus1             = $("#kelurahan_pengurus1 option:selected").val();
                        var kode_pos_pengurus1              = $("#kode_pos_pengurus1 option:selected").val();

                        // pengurus 2
                        var id_pengurus2                    = $("#id_pengurus2").val();
                        var nm_pengurus2 				    = $("#nama_pengurus2").val();
                        var jns_kelamin_pengurus2			= $("input[name=jns_kelamin_pengurus2]:checked").val();
                        var nik_pengurus2 				    = $("#ktp_pengurus2").val();
                        var tmpt_lahir_pengurus2 	        = $("#tmpt_lahir_pengurus2").val();
                        var tgl_lahir_pengurus2 	        = $("#tgl_lahir_pengurus2").val();
                        var tlp_pengurus2 				    = "62".concat($("#tlp_pengurus2").val());
                        var agama_pengurus2 			    = $("#agama_pengurus2 option:selected").val();
                        var pendidikan_terakhir_pengurus2	= $("#pendidikan_terakhir_pengurus2 option:selected").val();
                        var npwp_pengurus2 				    = $("#npwp_pengurus2").val();
                        var jabatan_pengurus2 				= $("#jabatan_pengurus2").val();
                        var alamat_pengurus2 	            = $("#alamat_pengurus2").val();
                        var provinsi_pengurus2      		= $("#provinsi_pengurus2 option:selected").val();
                        var kota_pengurus2  	            = $("#kota_pengurus2 option:selected").val();
                        var kecamatan_pengurus2             = $("#kecamatan_pengurus2 option:selected").val();
                        var kelurahan_pengurus2             = $("#kelurahan_pengurus2 option:selected").val();
                        var kode_pos_pengurus2              = $("#kode_pos_pengurus2 option:selected").val();

                        // Foto Badan Hukum
                        var exist_url_pic_brw_bdn_hukum     = $("#url_pic_brw_bdn_hukum").val();

                        if (exist_url_pic_brw_bdn_hukum == "" || exist_url_pic_brw_bdn_hukum == null)
                        {
                            var url_pic_brw_bdn_hukum       = $("#brw_pic_bdn_hukum").val();
                        }else{
                            var url_pic_brw_bdn_hukum       = $("#url_pic_brw_bdn_hukum").val();
                        }
                        
                        var exist_url_pic_brw_ktp_bdn_hukum = $("#url_pic_brw_ktp_bdn_hukum").val();
                        if (exist_url_pic_brw_ktp_bdn_hukum == "" || exist_url_pic_brw_ktp_bdn_hukum == null)
                        {
                            var url_pic_brw_ktp_bdn_hukum   = $("#brw_pic_ktp_bdn_hukum").val();
                        }else{
                            var url_pic_brw_ktp_bdn_hukum   = $("#url_pic_brw_ktp_bdn_hukum").val();
                        }
                        
                        var exist_url_pic_brw_dengan_ktp_bdn_hukum = $("#url_pic_brw_dengan_ktp_bdn_hukum").val();
                        if (exist_url_pic_brw_dengan_ktp_bdn_hukum == "" || exist_url_pic_brw_dengan_ktp_bdn_hukum == null)
                        {
                            var url_pic_brw_dengan_ktp_bdn_hukum   = $("#brw_pic_user_ktp_bdn_hukum").val();
                        }else{
                            var url_pic_brw_dengan_ktp_bdn_hukum   = $("#url_pic_brw_dengan_ktp_bdn_hukum").val();
                        }

                        var exist_url_pic_brw_npwp_bdn_hukum = $("#url_pic_brw_npwp_bdn_hukum").val();
                        if (exist_url_pic_brw_npwp_bdn_hukum == "" || exist_url_pic_brw_npwp_bdn_hukum == null)
                        {
                            var url_pic_brw_npwp_bdn_hukum   = $("#brw_pic_npwp_bdn_hukum").val();
                        }else{
                            var url_pic_brw_npwp_bdn_hukum   = $("#url_pic_brw_npwp_bdn_hukum").val();
                        }

                        // START: Foto Pengurus 1
                        var exist_url_pic_brw_pengurus1     = $("#url_pic_brw_pengurus1").val();

                        if (exist_url_pic_brw_pengurus1 == "" || exist_url_pic_brw_pengurus1 == null)
                        {
                            var url_pic_brw_pengurus1       = $("#brw_pic_pengurus1").val();
                        }else{
                            var url_pic_brw_pengurus1       = $("#url_pic_brw_pengurus1").val();
                        }
                        
                        var exist_url_pic_brw_ktp_pengurus1 = $("#url_pic_brw_ktp_pengurus1").val();
                        if (exist_url_pic_brw_ktp_pengurus1 == "" || exist_url_pic_brw_ktp_pengurus1 == null)
                        {
                            var url_pic_brw_ktp_pengurus1   = $("#brw_pic_ktp_pengurus1").val();
                        }else{      
                            var url_pic_brw_ktp_pengurus1   = $("#url_pic_brw_ktp_pengurus1").val();
                        }
                        
                        var exist_url_pic_brw_dengan_ktp_pengurus1 = $("#url_pic_brw_dengan_ktp_pengurus1").val();
                        if (exist_url_pic_brw_dengan_ktp_pengurus1 == "" || exist_url_pic_brw_dengan_ktp_pengurus1 == null)
                        {
                            var url_pic_brw_dengan_ktp_pengurus1   = $("#brw_pic_user_ktp_pengurus1").val();
                        }else{
                            var url_pic_brw_dengan_ktp_pengurus1   = $("#url_pic_brw_dengan_ktp_pengurus1").val();
                        }

                        var exist_url_pic_brw_npwp_pengurus1 = $("#url_pic_brw_npwp_pengurus1").val();
                        if (exist_url_pic_brw_npwp_pengurus1 == "" || exist_url_pic_brw_npwp_pengurus1 == null)
                        {
                            var url_pic_brw_npwp_pengurus1   = $("#brw_pic_npwp_pengurus1").val();
                        }else{
                            var url_pic_brw_npwp_pengurus1   = $("#url_pic_brw_npwp_pengurus1").val();
                        }
                        // END: Foto Pengurus 1

                        // START: Foto Pengurus 2
                        var exist_url_pic_brw_pengurus2     = $("#url_pic_brw_pengurus2").val();

                        if (exist_url_pic_brw_pengurus2 == "" || exist_url_pic_brw_pengurus2 == null)
                        {
                            var url_pic_brw_pengurus2       = $("#brw_pic_pengurus2").val();
                        }else{
                            var url_pic_brw_pengurus2       = $("#url_pic_brw_pengurus2").val();
                        }
                        
                        var exist_url_pic_brw_ktp_pengurus2 = $("#url_pic_brw_ktp_pengurus2").val();
                        if (exist_url_pic_brw_ktp_pengurus2 == "" || exist_url_pic_brw_ktp_pengurus2 == null)
                        {
                            var url_pic_brw_ktp_pengurus2   = $("#brw_pic_ktp_pengurus2").val();
                        }else{
                            var url_pic_brw_ktp_pengurus2   = $("#url_pic_brw_ktp_pengurus2").val();
                        }
                        
                        var exist_url_pic_brw_dengan_ktp_pengurus2 = $("#url_pic_brw_dengan_ktp_pengurus2").val();
                        if (exist_url_pic_brw_dengan_ktp_pengurus2 == "" || exist_url_pic_brw_dengan_ktp_pengurus2 == null)
                        {
                            var url_pic_brw_dengan_ktp_pengurus2  = $("#brw_pic_user_ktp_pengurus2").val();
                        }else{
                            var url_pic_brw_dengan_ktp_pengurus2   = $("#url_pic_brw_dengan_ktp_pengurus2").val();
                        }

                        var exist_url_pic_brw_npwp_pengurus2 = $("#url_pic_brw_npwp_pengurus2").val();
                        if (exist_url_pic_brw_npwp_pengurus2 == "" || exist_url_pic_brw_npwp_pengurus2 == null)
                        {
                            var url_pic_brw_npwp_pengurus2   = $("#brw_pic_npwp_pengurus2").val();
                        }else{
                            var url_pic_brw_npwp_pengurus2   = $("#url_pic_brw_npwp_pengurus2").val();
                        }
                        // END: Foto Pengurus 2
                        $.ajax({
                            url: "/borrower/proses_updateprofile",
                            type: "POST",
                            data:  { "_token": "{{ csrf_token() }}",'type_borrower':brw_type,'brw_id':brw_id,
                                'nm_bdn_hukum':nm_bdn_hukum,
                                'tgl_berdiri': tgl_berdiri,
                                'npwp_bdn_hukum':npwp_bdn_hukum,
                                'nama_pendaftar':nama_pendaftar,
                                'nikPendaftar':nikPendaftar,
                                'hpPendaftar':hpPendaftar,
                                'jabatanPendaftar':jabatanPendaftar,
                                'nib':nib,
                                'no_akta_pendirian':no_akta_pendirian,
                                'bidang_usaha':bidang_usaha,
                                // 'namapengurus':namapengurus,
                                // 'nikpengurus':nikpengurus,
                                // 'teleponpengurus':teleponpengurus,
                                // 'jabatanpengurus':jabatanpengurus,

                                // pengurus
                                'id_pengurus1': id_pengurus1,
                                'nm_pengurus1': nm_pengurus1, 				    
                                'jns_kelamin_pengurus1': jns_kelamin_pengurus1,			
                                'nik_pengurus1': nik_pengurus1,   			    
                                'tmpt_lahir_pengurus1': tmpt_lahir_pengurus1,	        
                                'tgl_lahir_pengurus1': tgl_lahir_pengurus1,	        
                                'tlp_pengurus1': tlp_pengurus1,				    
                                'agama_pengurus1': agama_pengurus1 , 			    
                                'pendidikan_terakhir_pengurus1': pendidikan_terakhir_pengurus1,	
                                'npwp_pengurus1': npwp_pengurus1, 				    
                                'jabatan_pengurus1': jabatan_pengurus1, 				
                                'alamat_pengurus1': alamat_pengurus1, 	            
                                'provinsi_pengurus1': provinsi_pengurus1,      		
                                'kota_pengurus1': kota_pengurus1,  	            
                                'kecamatan_pengurus1': kecamatan_pengurus1,             
                                'kelurahan_pengurus1': kelurahan_pengurus1,             
                                'kode_pos_pengurus1': kode_pos_pengurus1,

                                'url_pic_brw_pengurus1': url_pic_brw_pengurus1,
                                'url_pic_brw_ktp_pengurus1': url_pic_brw_ktp_pengurus1, 
                                'url_pic_brw_dengan_ktp_pengurus1': url_pic_brw_dengan_ktp_pengurus1, 
                                'url_pic_brw_npwp_pengurus1': url_pic_brw_npwp_pengurus1,   

                                // pengurus 2
                                'id_pengurus2': id_pengurus2,                    
                                'nm_pengurus2': nm_pengurus2, 				    
                                'jns_kelamin_pengurus2': jns_kelamin_pengurus2,			
                                'nik_pengurus2': nik_pengurus2, 				    
                                'tmpt_lahir_pengurus2': tmpt_lahir_pengurus2, 	        
                                'tgl_lahir_pengurus2': tgl_lahir_pengurus2, 	        
                                'tlp_pengurus2': tlp_pengurus2, 				    
                                'agama_pengurus2': agama_pengurus2, 			    
                                'pendidikan_terakhir_pengurus2': pendidikan_terakhir_pengurus2,	
                                'npwp_pengurus2': npwp_pengurus2, 				    
                                'jabatan_pengurus2': jabatan_pengurus2, 				
                                'alamat_pengurus2': alamat_pengurus2, 	            
                                'provinsi_pengurus2': provinsi_pengurus2,      		
                                'kota_pengurus2': kota_pengurus2,  	            
                                'kecamatan_pengurus2': kecamatan_pengurus2,             
                                'kelurahan_pengurus2': kelurahan_pengurus2,             
                                'kode_pos_pengurus2': kode_pos_pengurus2,

                                'url_pic_brw_pengurus2': url_pic_brw_pengurus2,        
                                'url_pic_brw_ktp_pengurus2': url_pic_brw_ktp_pengurus2,   
                                'url_pic_brw_dengan_ktp_pengurus2': url_pic_brw_dengan_ktp_pengurus2,  
                                'url_pic_brw_npwp_pengurus2': url_pic_brw_npwp_pengurus2,                                

                                'namapemilikrekening_bdnhkm':namapemilikrekening_bdnhkm,
                                'norekening_bdnhkm':norekening_bdnhkm,
                                'bank_bdnhkm':bank_bdnhkm,

                                'provinsi_bdn_hukum':provinsi_bdn_hukum,
                                'kota_bdn_hukum':kota_bdn_hukum,
                                'kecamatan_bdn_hukum':kecamatan_bdn_hukum,
                                'kelurahan_bdn_hukum':kelurahan_bdn_hukum,
                                'kode_pos_bdn_hukum':kode_pos_bdn_hukum,
                                'alamat_bdn_hukum':alamat_bdn_hukum,
                                // alamat domisili
                                "domisili_alamat" : domisili_alamat, 
                                "domisili_provinsi" : domisili_provinsi,
                                "domisili_kota" : domisili_kota,
                                "domisili_kecamatan" : domisili_kecamatan, 
                                "domisili_kelurahan" : domisili_kelurahan, 
                                "domisili_kd_pos" : domisili_kd_pos, 
                                "domisili_status_rumah" : domisili_status_rumah, 
                                'domisili_status': domisili_status,
                                

                                'bidang_pekerjaan_bdn_hukum':bidang_pekerjaan_bdn_hukum,
                                'bidang_online_bdn_hukum':bidang_online_bdn_hukum,
                                'pendapatan_bdn_hukum':pendapatan_bdn_hukum,
                                'total_aset':total_aset,

                                'url_pic_brw':url_pic_brw_bdn_hukum,
                                'url_pic_brw_ktp' : url_pic_brw_ktp_bdn_hukum,
                                'url_pic_brw_dengan_ktp':url_pic_brw_dengan_ktp_bdn_hukum,
                                'url_pic_brw_npwp':url_pic_brw_npwp_bdn_hukum,
                                
                            },

                            beforeSend: function() {
                                swal.fire({
                                    html: '<h5>Loading...</h5>',
                                    showConfirmButton: false,
                                    allowOutsideClick: () => false,
                                    onRender: function() {
                                        $('.swal2-content').prepend(sweet_loader);
                                    }
                                });
                            },
                                
                            success:function(response){
                                // kulll
                                if(response.error == false){
                                    
                                    swal.fire({
                                        title: "Proses Ubah Berhasil",
                                        //text: "Your will not be able to recover this imaginary file!",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        // closeOnConfirm: false
                                    }).then((result) => {
                                        location.href = "/borrower/beranda";
                                    });
                                    
                                } else if(response.error = true) {
                                    swal.fire({
                                        title: "Gagal",
                                        text: response.msg,
                                        type: "warning",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        onAfterClose: () => {
                                            if(response.type == '1'){ 
                                                $("#npwp_bdn_hukum").focus();
                                                $("#npwp_bdn_hukum").addClass('is-invalid');
                                            } else if(response.type == '2'){ 
                                                $("#npwp_pengurus1").focus();
                                                $("#npwp_pengurus1").addClass('is-invalid');
                                            } else if(response.type == '3'){ 
                                                $("#npwp_pengurus2").focus();
                                                $("#npwp_pengurus2").addClass('is-invalid');
                                            } else if(response.type == '4'){ 
                                                $("#tlp_pengurus1").focus();
                                                $("#tlp_pengurus1").addClass('is-invalid');
                                            } else if(response.type == '5'){ 
                                                $("#tlp_pengurus2").focus();
                                                $("#tlp_pengurus2").addClass('is-invalid');
                                            }
                                        }
                                    });
                                }
                                
                            },
                            error:function(data) {
                                swal.fire({
                                    title: "Error",
                                    text: data,
                                    type: "error",
                                })
                            }
                        });

                    }
                });
            });

            $("#form_profile").on('submit', function postinput(e){
                e.preventDefault();

                // foto pribadi
                var exist_url_pic_brw               = $("#url_pic_brw").val();
                if (exist_url_pic_brw == "" || exist_url_pic_brw == null)
                {
                    var url_pic_brw                 = $("#brw_pic").val();
                    if (!url_pic_brw) {
                        swal.fire({
                            title: "Gagal",
                            text: "Foto Diri tidak boleh kosong",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        });
                        return false
                    }
                }else{
                    var url_pic_brw                 = $("#url_pic_brw").val();
                }
                
                var exist_url_pic_brw_ktp           = $("#url_pic_brw_ktp").val();
                if (exist_url_pic_brw_ktp == "" || exist_url_pic_brw_ktp == null)
                {
                    var url_pic_brw_ktp             = $("#brw_pic_ktp").val();
                    if (!url_pic_brw_ktp) {
                        swal.fire({
                            title: "Gagal",
                            text: "Foto KTP tidak boleh kosong",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        });
                        return false   
                    }
                }else{
                    var url_pic_brw_ktp             = $("#url_pic_brw_ktp").val();
                }
                
                var exist_url_pic_brw_dengan_ktp    = $("#url_pic_brw_dengan_ktp").val();
                if (exist_url_pic_brw_dengan_ktp == "" || exist_url_pic_brw_dengan_ktp == null)
                {
                    var url_pic_brw_dengan_ktp      = $("#brw_pic_user_ktp").val();
                    if (!url_pic_brw_dengan_ktp) {
                        swal.fire({
                            title: "Gagal",
                            text: "Foto Diri & KTP tidak boleh kosong",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        });
                        return false
                    }
                }else{
                    var url_pic_brw_dengan_ktp      = $("#url_pic_brw_dengan_ktp").val();
                }

                var exist_url_pic_brw_npwp          = $("#url_pic_brw_npwp").val();
                if (exist_url_pic_brw_npwp == "" || exist_url_pic_brw_npwp == null)
                {
                    var url_pic_brw_npwp            = $("#brw_pic_npwp").val();
                    if (!url_pic_brw_npwp) {
                        swal.fire({
                            title: "Gagal",
                            text: "Foto NPWP tidak boleh kosong",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        });
                        return false
                    }
                }else{
                    var url_pic_brw_npwp            = $("#url_pic_brw_npwp").val();
                }

                var sweet_loader = '<div class="sweet_loader"><svg viewBox="0 0 140 140" width="140" height="140"><g class="outline"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="rgba(0,0,0,0.1)" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></g><g class="circle"><path d="m 70 28 a 1 1 0 0 0 0 84 a 1 1 0 0 0 0 -84" stroke="#71BBFF" stroke-width="4" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-dashoffset="200" stroke-dasharray="300"></path></g></svg></div>';
                swal.fire({
                    text: "Anda Yakin akan Mengubah Data Profil ?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Proses",
                    cancelButtonText: "Batal", 
                    reverseButtons: true,
                }).then((result) => {
                    if (result.value) {
                        var brw_id 		= $('#id').val(); 
                        var brw_type 	= $('#brw_type').val();
                
                        /***************** PRIBADI ******************/
                        //killl
                        var nama 				            = $("#nama").val();
                        var ibukandung                      = $("#ibukandung").val();
                        var pendidikan_terakhir 			= $("#pendidikan_terakhir option:selected").val();
                        var ktp 			                = $("#ktp").val();
                        var npwp 			                = $("#npwp").val();
                        var no_tlp                          = "62".concat($("#telepon").val());
                        var tempat_lahir 			        = $("#tempat_lahir").val();
                        var tgl_lahir                       = $("#tanggal_lahir").val();
                        var tgl_lahir_hari 					= $("#tgl_lahir_hari option:selected").val();
                        var tgl_lahir_bulan 				= $("#tgl_lahir_bulan option:selected").val();
                        var tgl_lahir_tahun 				= $("#tgl_lahir_tahun option:selected").val();
                        var jns_kelamin 			    	= $("input[name=jns_kelamin]:checked").val();
                        var agama 						    = $("#agama option:selected").val();
                        var status_kawin 					= $("input[name=status_kawin]:checked").val();
                        // alamat tinggal
                        var provinsi 	            		= $("#provinsi option:selected").val();
                        var kota 				            = $("#kota option:selected").val();
                        var kecamatan                       = $("#kecamatan option:selected").val();
                        var kelurahan                       = $("#kelurahan option:selected").val();
                        var alamat 			                = $("#alamat").val();
                        var kode_pos 			            = $("#kode_pos option:selected").val();   
                        var status_rumah 				    = $("input[name=status_rumah]:checked").val();
                        
                        // alamat domisili
                        var domisili_status                 = $(".domisili_status").val();
                        var domisili_provinsi 	            = $("#domisili_provinsi option:selected").val();
                        var domisili_kota 				    = $("#domisili_kota option:selected").val();
                        var domisili_kecamatan              = $("#domisili_kecamatan option:selected").val();
                        var domisili_kelurahan              = $("#domisili_kelurahan option:selected").val();
                        var domisili_alamat 			    = $("#domisili_alamat").val();
                        var domisili_kd_pos 			    = $("#domisili_kode_pos option:selected").val();   
                        var domisili_status_rumah 			= $("input[name=domisili_status_rumah]:checked").val();

                        // pasangan
                        var nama_pasangan                   = $("#nama_pasangan").val();
                        var jns_kelamin_pasangan            =  $("input[name=jns_kelamin_pasangan]:checked").val();
                        var ktp_pasangan                    =  $("#ktp_pasangan").val();
                        var tmpt_lahir_pasangan             =  $("#tmpt_lahir_pasangan").val();
                        var tgl_lahir_pasangan              =  $("#tgl_lahir_pasangan").val();
                        var no_tlp_pasangan                 =  "62".concat($("#telepon_pasangan").val());
                        var agama_pasangan                  =  $("#agama_pasangan option:selected").val();
                        var pendidikan_terakhir_pasangan    =  $("#pendidikan_terakhir_pasangan option:selected").val();
                        var npwp_pasangan                   =  $("#npwp_pasangan").val();
                        var alamat_pasangan                 =  $("#alamat_pasangan").val();
                        var provinsi_pasangan               =  $("#provinsi_pasangan option:selected").val();
                        var kota_pasangan                   =  $("#kota_pasangan option:selected").val();
                        var kecamatan_pasangan              =  $("#kecamatan_pasangan option:selected").val();
                        var kelurahan_pasangan              =  $("#kelurahan_pasangan option:selected").val();
                        var kode_pos_pasangan               =  $("#kode_pos_pasangan option:selected").val();

                        // rekening
                        var namapemilikrekening 		    = $("#namapemilikrekening").val(); 
                        var norekening 			            = $("#norekening").val(); 
                        var bank 				            = $("#bank option:selected").val();
                        
                        // informasi lain lain
                        var pekerjaan 			            = $("#pekerjaan option:selected").val();
                        var bidang_pekerjaan 	            = $("#bidang_pekerjaan option:selected").val();
                        var bidang_online 		            = $("#bidang_online option:selected").val();
                        var pengalaman_kerja 		        = $("#pengalaman_kerja option:selected").val();
                        var pendapatan_bulanan 	            = $("#pendapatan_bulanan option:selected").val();

                        $.ajax({
                            
                            url: "/borrower/proses_updateprofile",
                            type: "POST",
                            data:  { "_token": "{{ csrf_token() }}",'type_borrower':brw_type,'brw_id':brw_id,'nama':nama, 'ibukandung':ibukandung,'pendidikan_terakhir':pendidikan_terakhir,'ktp':ktp, 'npwp':npwp, 'no_tlp':no_tlp,
                        
                                'tempat_lahir':tempat_lahir, 'tgl_lahir':tgl_lahir, 'jns_kelamin':jns_kelamin, 'agama':agama,'status_kawin':status_kawin,
                                // alamat ktp
                                'provinsi':provinsi,'kota':kota,'alamat':alamat, 'kode_pos':kode_pos, 'status_rumah':status_rumah, 'kecamatan':kecamatan,'kelurahan': kelurahan,
                                // pekerjaan
                                'pekerjaan':pekerjaan, 'bidang_pekerjaan':bidang_pekerjaan, 'bidang_online':bidang_online, 'pengalaman_kerja':pengalaman_kerja, 'pendapatan_bulanan':pendapatan_bulanan,
                                // rekening
                                "namapemilikrekening":namapemilikrekening,"norekening":norekening,"bank":bank,
                                // foto
                                "url_pic_brw":url_pic_brw, "url_pic_brw_ktp" : url_pic_brw_ktp, "url_pic_brw_dengan_ktp":url_pic_brw_dengan_ktp,"url_pic_brw_npwp":url_pic_brw_npwp,
                                // bank
                                // "txt_nm_pemilik_individu":txt_nm_pemilik_individu, "txt_no_rek_individu" : txt_no_rek_individu, "txt_bank_individu":txt_bank_individu,
                                // alamat domisili
                                "domisili_alamat" : domisili_alamat, "domisili_provinsi" : domisili_provinsi, "domisili_kota" : domisili_kota, "domisili_kecamatan" : domisili_kecamatan, "domisili_kelurahan" : domisili_kelurahan, "domisili_kd_pos" : domisili_kd_pos, "domisili_status_rumah" : domisili_status_rumah, 'domisili_status': domisili_status,
                                // bank
                                "namapemilikrekening" : namapemilikrekening, "norekening" : norekening, "bank" : bank,
                                // pasangan
                                "nama_pasangan": nama_pasangan, "jns_kelamin_pasangan": jns_kelamin_pasangan, "ktp_pasangan": ktp_pasangan, "tmpt_lahir_pasangan": tmpt_lahir_pasangan, "tgl_lahir_pasangan": tgl_lahir_pasangan,
                                "no_tlp_pasangan": no_tlp_pasangan, "agama_pasangan": agama_pasangan, "pendidikan_terakhir_pasangan": pendidikan_terakhir_pasangan, "npwp_pasangan": npwp_pasangan,
                                "alamat_pasangan": alamat_pasangan, "provinsi_pasangan": provinsi_pasangan, "kota_pasangan": kota_pasangan, "kecamatan_pasangan": kecamatan_pasangan, "kelurahan_pasangan": kelurahan_pasangan,
                                "kode_pos_pasangan": kode_pos_pasangan,
                            },

                            // kill2
                            beforeSend: function() {
                                swal.fire({
                                    html: '<h5>Loading...</h5>',
                                    showConfirmButton: false,
                                    allowOutsideClick: () => false,
                                    onRender: function() {
                                        $('.swal2-content').prepend(sweet_loader);
                                    }
                                });
                            },

                            success:function(data){
                                if (data.error == false) {
                                    swal.fire({
                                        title: "Data Berhasil diubah",
                                        text: "Berhasil",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                    }).then((result) => {
                                        location.href = "/borrower/beranda";
                                    });
                                    $("#telepon").removeClass('is-invalid');
                                } else if(data.error == true) {
                                    swal.fire({
                                        title: "Gagal",
                                        text: data.msg,
                                        type: "warning",
                                        showCancelButton: false,
                                        confirmButtonClass: "btn-success",
                                        onAfterClose: () => {
                                            if(data.type == '1'){ 
                                                $("#telepon_pasangan").focus();
                                                $("#telepon_pasangan").addClass('is-invalid');
                                            }
                                            else if(data.type == '2'){ 
                                                $("#ktp_pasangan").focus();
                                                $("#ktp_pasangan").addClass('is-invalid');
                                            }
                                            else if(data.type == '3'){ 
                                                $("#npwp_pasangan").focus();
                                                $("#npwp_pasangan").addClass('is-invalid');
                                            }
                                            else if(data.type == '4'){ 
                                                $("#ktp").focus();
                                                $("#ktp").addClass('is-invalid');
                                            }
                                            else if(data.type == '5'){ 
                                                $("#npwp").focus();
                                                $("#npwp").addClass('is-invalid');
                                            }
                                        }
                                    });
                                }
                                
                            },
                            error:function(data) {
                                swal.fire({
                                    title: "Error",
                                    text: data,
                                    type: "error",
                                })
                            }
                        });
                    }
                });
            });
    
        }); 

        // end
        $('.notallowCharacter').on('input', function (event) { 
            this.value = this.value.replace(/ /g, '');
        });

        // var uhuy = 0;
        $('#side-overlay-profile-password, #side-overlay-profile-password-confirm').on('keyup', function () {

        //if ($('#side-overlay-profile-password').val() == $('#side-overlay-profile-password-confirm').val()) {
            // if(($('#side-overlay-profile-password').val() != "" || $('#side-overlay-profile-password-confirm').val() != "")){
        if (true) {

            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");
            var spchar = new RegExp("[!@#$%^&*]");

            
            if($("#side-overlay-profile-password").val().length >= 8){
                $("#8char").removeClass("fa fa-times");
                $("#8char").addClass("fa fa-check");
                $("#8char").css("color","#00A41E");
                $("#char").val(1);
            }else{
                $("#8char").removeClass("fa fa-check");
                $("#8char").addClass("fa fa-times");
                $("#8char").css("color","#FF0004");
                $("#char").val(0);
            }

            if(ucase.test($("#side-overlay-profile-password").val())){
                $("#ucase").removeClass("fa fa-times");
                $("#ucase").addClass("fa fa-check");
                $("#ucase").css("color","#00A41E");
                $("#upper").val(1);
            }else{
                $("#ucase").removeClass("fa fa-check");
                $("#ucase").addClass("fa fa-times");
                $("#ucase").css("color","#FF0004");
                $("#upper").val(0);
            }
            if(lcase.test($("#side-overlay-profile-password").val())){
                $("#lcase").removeClass("fa fa-times");
                $("#lcase").addClass("fa fa-check");
                $("#lcase").css("color","#00A41E");
                $("#lower").val(1);
            }else{
                $("#lcase").removeClass("fa fa-check");
                $("#lcase").addClass("fa fa-times");
                $("#lcase").css("color","#FF0004");
                $("#lower").val(0);
            }
            if(num.test($("#side-overlay-profile-password").val())){
                $("#num").removeClass("fa fa-times");
                $("#num").addClass("fa fa-check");
                $("#num").css("color","#00A41E");
                $("#int").val(1);
            }else{
                $("#num").removeClass("fa fa-check");
                $("#num").addClass("fa fa-times");
                $("#num").css("color","#FF0004");
                $("#int").val(0);
            }
            if(spchar.test($("#side-overlay-profile-password").val())){
                $("#special").removeClass("fa fa-times");
                $("#special").addClass("fa fa-check");
                $("#special").css("color","#00A41E");
                $("#spc").val(1);
            }else{
                $("#special").removeClass("fa fa-check");
                $("#special").addClass("fa fa-times");
                $("#special").css("color","#FF0004");
                $("#spc").val(0);
            }

            if($("#side-overlay-profile-password").val() == $("#side-overlay-profile-password-confirm").val() && $("#int").val()== 1 && $("#lower").val()== 1 && $("#upper").val()== 1 && $("#char").val()== 1 && $("#spc").val()==1 )
                {
                    
                    
                    $('#message').html(' - Sesuai standard minimum password').css('color', 'green');
                    $('#btnsubmitpwd').attr('disabled', false);

                }
                else{
                    $('#message').html(' - Tidak sama / tidak sesuai standard minimum password').css('color', 'red');
                    $('#btnsubmitpwd').attr('disabled', true);
                }

		
        } else 
            $('#message').html(' - Tidak Sesuai').css('color', 'red');
        });

</script>
@endsection