<div class="tab-pane pb-30 " id="informasi-pendanaan" role="tabpanel">

    <div class="layout">
        <h3 class="block-title text-black mb-10 font-w600">Informasi Objek
            Pendanaan</h3>
        <div class="row ml-30 mt-30">
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Tipe Pendanaan</h6>
                <p class="font-w600 text-dark">{{ $pendanaanTipe }}</p>
            </div>


            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Tujuan Pendanaan</h6>
                <p class="font-w600 text-dark">{{ $tujuanPendanaan }}</p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kebutuhan Dana</h6>
                <p class="font-w600 text-dark">Rp.
                    {{ number_format($pengajuan->pendanaan_dana_dibutuhkan, 2, ',', '.') }}
                </p>
            </div>

        </div>
     
      
        <div class="row ml-30 mt-30">
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Perkiraan estimasi proyek akan mulai dikerjakan</h6>
                <p class="font-w600 text-dark">
                    {{ \Carbon\Carbon::parse($pengajuan->estimasi_mulai)->format('d/m/Y') }}</p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Jangka Waktu (Bulan)</h6>
                <p class="font-w600 text-dark">
                    {{ $pengajuan->durasi_proyek }}</p>
            </div>
            
        </div>
        
    </div>
   
    <div class="layout mt-30">
        <h3 class="block-title text-black mb-10 font-w600">Status Pengajuan
            Pendanaan </h3>
        <div class="row ml-30 mt-20">
            <div class="col-12 col-md-12 pl-10">
                <p class="font-w600 text-dark">{{ $status_pengajuan }}</p>
            </div>

        </div>
    </div>

    @if (empty($status_tolak) &&  ($pengajuan->status >= 1 && $pengajuan->status != '2') )
        <div class="row ml-10 mt-20">
            <div class="col-12 text-right">
                <button type="button" class="btn btn-success text-left" id="btnNext"> Selanjutnya >> </button>
            </div>

        </div>
    @endif

</div>
@push('info-scripts')
    <script>
        $(function() {
            $('#btnNext').click(function(e) {
                e.preventDefault();
                $('.nav-tabs a[href="#pengajuan-pendanaan"]').tab('show');
            })

        });
    </script>
@endpush
