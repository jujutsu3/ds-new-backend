@extends('layouts.borrower.master')

@section('title', 'Selamat Datang Penerima Dana')

@section('style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
        type="text/css" rel="stylesheet" />
@endsection

@section('content')
    <!-- select2 -->
    <!-- Main Container -->
    <style>
        .line {
            display: flex;
            flex-direction: row;
        }

        .line:after {
            content: "";
            flex: 1 1;
            border-bottom: 1px solid #000;
            margin: auto;
        }

        #overlay {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 900;
            width: 100%;
            height: 100%;
            display: none;
            background: rgba(0, 0, 0, 0.6);
        }

        .green-dsi {
            background-color: #057758;
        }

        .error {
            color: red;
        }

        #type_tujuan_pendanaan-error {
            position: absolute;
            top: 60%;
            left: 10px;
        }

        #txt_hp_pemilik-error {
            position: absolute;
            top: 100%;
        }

        #txt_provinsi-error,
        #txt_provinsi_pemilik-error,
        #txt_kota-error,
        #txt_kota_pemilik-error,
        #txt_kecamatan-error,
        #txt_kecamatan_pemilik-error,
        #txt_kelurahan-error,
        #txt_kelurahan_pemilik-error {
            position: absolute;
            top: 50%;
            left: 10px;
        }

        .cv-spinner {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .spinner {
            width: 40px;
            height: 40px;
            border: 4px #ddd solid;
            border-top: 4px #2e93e6 solid;
            border-radius: 50%;
            animation: sp-anime 0.8s infinite linear;
        }

        @keyframes sp-anime {
            100% {
                transform: rotate(360deg);
            }
        }

        .mandatory_label {
            color: red;
        }

    </style>
    <div id="overlay">
        <div class="cv-spinner">
            <span class="spinner"></span>
        </div>
    </div>

    <main id="main-container">
        <!-- Page Content -->
        <div id="detect-screen" class="content-full-right">
            <div class="container">
                <div class="row mb-4">
                    <div id="col" class="col-12 col-md-12 mt-30">
                        <span class="mb-10 pb-10 ">
                            <h1 class="font-size-h3 pull-left">Ajukan Pendanaan</h1>
                        </span>
                    </div>
                </div>
                <div class="row mt-5 pt-5">
                    <div class="js-wizard-simple block border pt-0 layout" id="layout-pengajuan">
                        <div class="block-content block-content-full tab-content pt-0">
                            <!-- START: Form -->
                            <form id="form_add_pengajuan" action="{{ route('borrower.proses_pendanaan_kpr') }}"
                                method="POST" class="js-wizard-validation-classic-form" enctype="multipart/form-data">

                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <h5 class="pt-0" style="margin-top: -0.7rem"> <span class="bg-white">Form Informasi
                                                Objek Pendanaan</span></h5>
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapengguna">Tipe
                                                Pendanaan</label><label id="txt_tipe_borrower"></label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="type_pendanaan_select"
                                                name="type_pendanaan_select" required></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapengguna">Tujuan
                                                Pendanaan</label><label id="txt_tipe_borrower"></label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="type_tujuan_pendanaan"
                                                name="type_tujuan_pendanaan" required></select>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-tempatlahir">Alamat
                                                Objek Pendanaan</label> <label class="mandatory_label">*</label>
                                            <input class="form-control" rows="4" cols="80" id="txt_alamat" name="txt_alamat"
                                                placeholder="Masukkan Alamat Pendanaan" required>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapengguna">Provinsi</label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="txt_provinsi" name="txt_provinsi"
                                                required></select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-kota">Kota /
                                                Kabupaten</label> <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="txt_kota" name="txt_kota"
                                                required>
                                                <option value="">Pilih kota</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="txt_kecamatan"
                                                name="txt_kecamatan" required>
                                                <option value="">Pilih Kecamatan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                            <label class="mandatory_label">*</label>
                                            <select class="form-control custom-select" id="txt_kelurahan"
                                                name="txt_kelurahan" required>
                                                <option value="">Pilih Kelurahan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="txt_kd_pos_">Kode Pos <i class="text-danger">*</i></label>
                                            <input type="hidden" id="txt_kd_pos" name="txt_kd_pos">
                                            <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_"
                                                name="txt_kd_pos_" placeholder="--" value="" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="wizard-progress2-namapendanaan">Harga Rumah </label> <label class="mandatory_label">*</label>
                                            <input type="text" class="form-control numberOnly no-zero"
                                                id="txt_harga_objek_pendanaan" name="txt_harga_objek_pendanaan" required
                                                onkeyup="this.value = formatRupiah(this.value);"
                                                placeholder="Masukkan Harga Rumah">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="txt_uang_muka">Uang
                                                Muka </label> <label class="mandatory_label">*</label>
                                            <input type="text" class="form-control numberOnly no-zero" id="txt_uang_muka"
                                                name="txt_uang_muka" onkeyup="this.value = formatRupiah(this.value);"
                                                value="0" disabled required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="txt_nilai_pengajuan">Nilai
                                                Pengajuan Pendanaan </label> <label class="mandatory_label">*</label>
                                            <input type="text" class="form-control numberOnly no-zero"
                                                id="txt_nilai_pengajuan" name="txt_nilai_pengajuan" required
                                                onkeyup="this.value = formatRupiah(this.value);"
                                                placeholder="Nilai Pengajuan Pendanaan">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label id="label_txt_jangka_waktu" for="txt_jangka_waktu">Jangka
                                                Waktu (Bulan) </label> <label class="mandatory_label">*</label>
                                                <select name="txt_jangka_waktu" id="txt_jangka_waktu" class="form-control custom-select" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="txt_detail_pendanaan">Detail
                                                Informasi Rumah</label>
                                            <textarea class="form-control" rows="6" cols="80" id="txt_detail_pendanaan"
                                                name="txt_detail_pendanaan"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 mb-4">
                                        <div class="form-check form-check-inline line">
                                            <label class="form-check-label text-black h5 ml-0">Informasi
                                                Pemilik Objek Pendanaan &nbsp</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-tempatlahir">Nama</label>
                                                    <label class="mandatory_label">*</label>
                                                    <input class="form-control" rows="4" cols="80" id="txt_nm_pemilik"
                                                        name="txt_nm_pemilik" placeholder="Masukkan Nama Pemilik" required>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-tempatlahir">No.
                                                        Telepon/HP</label> <label class="mandatory_label">*</label>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text input-group-text-dsi">
                                                                +62
                                                            </span>
                                                        </div>
                                                        <input class="form-control" id="txt_hp_pemilik"
                                                            name="txt_hp_pemilik"
                                                            onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,'')"
                                                            pattern=".{9, 13}" minlength="9" maxlength="13"
                                                            placeholder="Masukkan No. Telepon/Hp Pemilik" required>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-tempatlahir">Alamat</label>
                                                    <label class="mandatory_label">*</label>
                                                    <input class="form-control" rows="4" cols="80" id="txt_alamat_pemilik"
                                                        name="txt_alamat_pemilik" placeholder="Masukkan Alamat Pemilik"
                                                        required>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-namapengguna">Provinsi</label>
                                                    <label class="mandatory_label">*</label>
                                                    <select class="form-control custom-select" id="txt_provinsi_pemilik"
                                                        name="txt_provinsi_pemilik" required>
                                                        <option value="">Pilih Provinsi</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-kota">Kota /
                                                        Kabupaten</label> <label class="mandatory_label">*</label>
                                                    <select class="form-control custom-select" id="txt_kota_pemilik"
                                                        name="txt_kota_pemilik" required>
                                                        <option value="">Pilih kota</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                                    <label class="mandatory_label">*</label>
                                                    <select class="form-control custom-select" id="txt_kecamatan_pemilik"
                                                        name="txt_kecamatan_pemilik" required>
                                                        <option value="">Pilih Kecamatan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                                    <label class="mandatory_label">*</label>
                                                    <select class="form-control custom-select" id="txt_kelurahan_pemilik"
                                                        name="txt_kelurahan_pemilik" required>
                                                        <option value="">Pilih Kelurahan</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="txt_kd_pos_pemilik_">Kode Pos <i
                                                            class="text-danger">*</i></label>
                                                    <input type="hidden" id="txt_kd_pos_pemilik" name="txt_kd_pos_pemilik">
                                                    <input class="form-control" type="text" maxlength="30"
                                                        id="txt_kd_pos_pemilik_" name="txt_kd_pos_pemilik_" placeholder="--"
                                                        value="" readonly>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 text-right float-right">
                                        <button type="button" class="btn green-dsi text-white" id="btn_simpan_kpr">
                                            Simpan <i class="fa fa-angle-right ml-5"></i>
                                        </button>

                                    </div>
                                </div>
                            </form>
                            <!-- END: Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- START: Modal Confirmation -->
    <div class="modal fade" id="modal_konfirmasi" tabindex="-1" role="dialog" aria-labelledby="modal-popin"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0">
                    <div class="block-header bg-primary-dark">
                        <h3 class="block-title">Simpan Data</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <p>Anda yakin ingin menyimpan data ini ?</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cek Lagi</button>
                    <button type="button" id="btn_simpan_konfirmasi" class="btn btn-alt-success"
                        data-dismiss="modal">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Modal Confirmation -->
@endsection

@section('script')
    <script src="{{ asset('/tinymce/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('/tinymce/js/tinymce/jquery.tinymce.min.js') }}"></script>
    <script language="javascript" src="{{ asset('vendor/credoApp/credoappsdk.js') }}"></script>
    <!-- SweetAlert JS -->
    <script src="{{ url('assetsBorrower/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    {{-- START: Credo --}}
    <script>
        // CredoApp authentication key provided by CredoLab team
        var authKey = "{{ config('app.credo_auth_key') }}";
        // value from CredoLab URLs table
        var url = "{{ config('app.credo_url') }}";
        // record number intended to associate collected dataset with record on lender side
        var recordNumber = "";
        var credoAppService = new credoappsdk.CredoAppService(url, authKey);
        credoAppService.startEventTracking();

        function collectData() {
            credoAppService.stopEventTracking();
            credoAppService.collectAsync(recordNumber).then((recordNumber) => {});
        }
    </script>
    {{-- END: Credo --}}

    {{-- START: DOM Manipulation --}}
    <script>
        // Set Validation
        $("#form_add_pengajuan").validate({
            onfocusout: false,
            invalidHandler: function(e, validator) {
                if (validator.errorList.length) {
                    validator.errorList[0].element.focus()
                }
            }
        });

        // Validasi No HP
        $(".no-zero").on("input", function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });

        // Rupiah Currency Format
        const formatRupiah = (angka, prefix) => {
            let number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }

        // tipe borrower
        var type_borrower = "{{ Session::get('brw_type') }}";

        if (type_borrower == 1) {

            $('#txt_tipe_borrower').text('Individu');
            $("#divIndividu").show();

        } else if (type_borrower == 2) {

            $('#txt_tipe_borrower').text('Wirausaha');

        } else {

            $('#txt_tipe_borrower').text('Badan Hukum');
            $("#divBadanHukum").show();
        }



        $(".stringOnly").on("input", function() {
            this.value = this.value.replace(/[^a-z A-Z]/g, "");
        });

        $(".numberOnly").on("input", function() {
            this.value = this.value.replace(/[^0-9]/g, "");
        });

        $(".StringNumber").on("input", function() {
            this.value = this.value.replace(/[^a-z A-Z 0-9]/g, "");
        });

        $(".StringNumChar").on("input", function() {
            this.value = this.value.replace(/[^a-z A-Z 0-9._-/]/g, "");
        });

        $('#txt_hp_pemilik').keyup(function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        })

        $.getJSON("/borrower/tipe_pendanaan/{{ Session::get('brw_type') }}", function(data_tipe_pendanaan) {
            $('#type_pendanaan_select').prepend("<option value='2' selected>Dana Rumah</option>").select2({
                theme: "bootstrap",
                placeholder: "Pilih Tipe Pendanaan",
                allowClear: true,
                data: data_tipe_pendanaan,
                width: '100%'
            });

        });

        $.getJSON("/borrower/tujuan_pendanaan_kpr/", function(data_tujuan_pendanaan) {

            $('#type_tujuan_pendanaan').prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Tujuan Pembiayaan",
                allowClear: true,
                data: data_tujuan_pendanaan,
                width: '100%'
            });

        });

        $('#txt_provinsi, #txt_provinsi_pemilik, #txt_kota, #txt_kota_pemilik, #txt_kecamatan, #txt_kecamatan_pemilik, #txt_kelurahan, #txt_kelurahan_pemilik')
            .prepend('<option selected></option>').select2({
                theme: "bootstrap",
                placeholder: "Pilih Satu",
                allowClear: true,
                width: '100%'
            });

        let dataProvinsiBandungJabodetabek = ['Banten', 'DKI Jakarta', 'Jawa Barat'];

        $.getJSON("/borrower/data_provinsi", function(data_provinsi) {
            for (let i = 0; i < data_provinsi.length; i++) {

                if (type_borrower == 1 && $('#type_pendanaan_select option:selected').text() == 'Dana Rumah') {
                    if (dataProvinsiBandungJabodetabek.includes(data_provinsi[i].text)) {

                        $('#txt_provinsi').append($('<option>', {
                            value: data_provinsi[i].text,
                            text: data_provinsi[i].text
                        }));

                    }

                } else {
                    $('#txt_provinsi').append($('<option>', {
                        value: data_provinsi[i].text,
                        text: data_provinsi[i].text
                    }));
                }


                $('#txt_provinsi_pemilik').append($('<option>', {
                    value: data_provinsi[i].text,
                    text: data_provinsi[i].text
                }));
            }
        });

        // data provinsi individu
        $(function() {


            $('#txt_provinsi').change(function() {
                $(this).valid()
                var provinsi = $('#txt_provinsi option:selected').val();
                var provinsi_text = $('#txt_provinsi option:selected').text();

                $("#txt_kota").empty(); // set null

                $.getJSON("/borrower/data_kota/" + provinsi_text, function(data_kota) {

                    $('#txt_kota').append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    for (let i = 0; i < data_kota.length; i++) {

                        $('#txt_kota').append($('<option>', {
                            value: data_kota[i].text,
                            text: data_kota[i].text
                        }));

                    }
                });


            });
        });


        $(function() {
            $('#txt_kota').change(function() {
                $(this).valid()
                var kota = $('#txt_kota option:selected').val();
                var kota_text = $('#txt_kota option:selected').text();

                $("#txt_kecamatan").empty(); // set null

                $.getJSON("/borrower/data_kecamatan/" + kota_text, function(data_kecamatan) {
                    $('#txt_kecamatan').append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    for (let i = 0; i < data_kecamatan.length; i++) {
                        $('#txt_kecamatan').append($('<option>', {
                            value: data_kecamatan[i].text,
                            text: data_kecamatan[i].text
                        }));
                    }
                });


            });
        });


        $(function() {
            $('#txt_kecamatan').change(function() {
                $(this).valid()
                var kecamatan = $('#txt_kecamatan option:selected').val();
                var kecamatan_text = $('#txt_kecamatan option:selected').text();

                $("#txt_kelurahan").empty(); // set null

                $.getJSON("/borrower/data_kelurahan/" + kecamatan_text, function(data_kelurahan) {
                    $('#txt_kelurahan').append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    for (let i = 0; i < data_kelurahan.length; i++) {
                        $('#txt_kelurahan').append($('<option>', {
                            value: data_kelurahan[i].text,
                            text: data_kelurahan[i].text
                        }));
                    }

                });

            });
        });

        // change kelurahan individu
        $(function() {
            $('#txt_kelurahan').change(function() {
                $(this).valid()
                var kelurahan = $('#txt_kelurahan option:selected').val();
                var kelurahan_text = $('#txt_kelurahan option:selected').text();

                $.getJSON("/borrower/data_kode_pos/" + kelurahan_text, function(data_kode_pos) {
                    $('#txt_kd_pos').val(data_kode_pos[0].text)
                    $('#txt_kd_pos_').val(data_kode_pos[0].text)
                    $("#txt_kd_pos").valid()
                });
            });
        });

        /**************************************/
        // event provinsi pemilik pendanaan
        // data provinsi individu
        $(function() {
            $('#txt_provinsi_pemilik').change(function() {
                $(this).valid()
                var provinsi = $('#txt_provinsi_pemilik option:selected').val();
                var provinsi_text = $('#txt_provinsi_pemilik option:selected').text();

                $("#txt_kota_pemilik").empty(); // set null

                $.getJSON("/borrower/data_kota/" + provinsi_text, function(data_kota) {
                    $('#txt_kota_pemilik').append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    for (let i = 0; i < data_kota.length; i++) {
                        $('#txt_kota_pemilik').append($('<option>', {
                            value: data_kota[i].text,
                            text: data_kota[i].text
                        }));
                    }

                });


            });
        });

        $(function() {
            $('#txt_kota_pemilik').change(function() {
                $(this).valid()
                var kota = $('#txt_kota_pemilik option:selected').val();
                var kota_text = $('#txt_kota_pemilik option:selected').text();

                $("#txt_kecamatan_pemilik").empty(); // set null

                $.getJSON("/borrower/data_kecamatan/" + kota_text, function(data_kecamatan) {
                    $('#txt_kecamatan_pemilik').append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    for (let i = 0; i < data_kecamatan.length; i++) {
                        $('#txt_kecamatan_pemilik').append($('<option>', {
                            value: data_kecamatan[i].text,
                            text: data_kecamatan[i].text
                        }));
                    }

                });


            });
        });


        $(function() {
            $('#txt_kecamatan_pemilik').change(function() {
                $(this).valid()
                var kecamatan = $('#txt_kecamatan_pemilik option:selected').val();
                var kecamatan_text = $('#txt_kecamatan_pemilik option:selected').text();

                $("#txt_kelurahan_pemilik").empty(); // set null

                $.getJSON("/borrower/data_kelurahan/" + kecamatan_text, function(data_kelurahan) {
                    $('#txt_kelurahan_pemilik').append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));
                    for (let i = 0; i < data_kelurahan.length; i++) {
                        $('#txt_kelurahan_pemilik').append($('<option>', {
                            value: data_kelurahan[i].text,
                            text: data_kelurahan[i].text
                        }));
                    }

                });

            });
        });

        // change kelurahan individu
        $(function() {
            $('#txt_kelurahan_pemilik').change(function() {
                $(this).valid()
                var kelurahan = $('#txt_kelurahan_pemilik option:selected').val();
                var kelurahan_text = $('#txt_kelurahan_pemilik option:selected').text();

                $.getJSON("/borrower/data_kode_pos/" + kelurahan_text, function(data_kode_pos) {
                    $('#txt_kd_pos_pemilik').val(data_kode_pos[0].text)
                    $('#txt_kd_pos_pemilik_').val(data_kode_pos[0].text)
                    $("#txt_kd_pos_pemilik").valid()
                });
            });
        });


        // data persyaratan pendanaan pribadi
        $(function() {
            $('#type_pendanaan_select').change(function() {

                var tipe_borrower_val = $("#type_borrower option:selected").val();
                var tipe_borrower_text = $("#type_borrower option:selected").text();
                var tipe_pendanaan = $('#type_pendanaan_select option:selected').val();
                var tipe_pendanaan_text = $('#type_pendanaan_select option:selected').text();
                var tipe_borrower = "{{ Session::get('brw_type') }}";
                var brw_id = "{{ Session::get('brw_id') }}";

                if (tipe_pendanaan == 2 && tipe_pendanaan_text == "Dana Rumah") {
                    // 
                } else {
                    location.href = "/borrower/ajukanPendanaan";
                }

            });
        });

        // on change tujuan pendanaan
        $(function() {
            $('#type_tujuan_pendanaan').change(function() {
                var id = this.options[this.selectedIndex].value;
                var val = this.options[this.selectedIndex].text;
                $(this).valid()

                if (id == 1 && val == "Pra Kepemilikan Rumah") {
                    $("#txt_uang_muka").attr("disabled", true).val(0);

                    $("#txt_nilai_pengajuan").val("").attr({
                        "readonly": false,
                        required: true
                    });

                    $('#txt_jangka_waktu').empty().append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));

                    for (let i = 1; i <= 12; i++) {
                        $('#txt_jangka_waktu').append($('<option>', {
                            value: i,
                            text: i
                        }));
                    }
                    $('#label_txt_jangka_waktu').text('Jangka Waktu (Maksimal 12 Bulan)')
                } else {
                    $("#txt_uang_muka").attr("disabled", false);

                    $("#txt_harga_objek_pendanaan").val("");
                    $("#txt_nilai_pengajuan").val("").attr({
                        "readonly": true,
                        required: false
                    });

                    $('#txt_jangka_waktu').empty().append($('<option>', {
                        value: "",
                        text: "-- Pilih Satu --"
                    }));

                    for (let i = 5; i <= 15; i += 5) {
                        $('#txt_jangka_waktu').append($('<option>', {
                            value: i * 12,
                            text: i
                        }));
                    
                    }
                    $('#label_txt_jangka_waktu').text('Jangka Waktu (Maksimal 15 Tahun)')
                }
            });
        });

        // on change value uang muka

        $('#txt_uang_muka').keyup(function() {
            var tujuan = $("#type_tujuan_pendanaan option:selected").val();
            if (tujuan != 1) {

                var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
                var uang_muka = $("#txt_uang_muka").val();

                let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
                let int_uang_muka = parseInt(uang_muka.replaceAll('.', ''))

                let total_pendanaan = int_harga_pendanaan - int_uang_muka
                $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
            }

        });

        $("#txt_nilai_pengajuan").keyup(function() {

            var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
            let nilai_pengajuan = $(this).val()

            let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
            let int_nilai_pengajuan = parseInt(nilai_pengajuan.replaceAll('.', ''))

        })


        $("#txt_harga_objek_pendanaan").blur(function() {

            if (type_borrower == 1 && $('#type_pendanaan_select option:selected').text() == 'Dana Rumah') {
                var harga_pendanaan_rumah = $(this).val().replaceAll('.', '');
            }

        })

        $("#txt_harga_objek_pendanaan").keyup(function (){
            var harga_pendanaan = $("#txt_harga_objek_pendanaan").val();
            var uang_muka = $("#txt_uang_muka").val();

            let int_harga_pendanaan = parseInt(harga_pendanaan.replaceAll('.', ''))
            let int_uang_muka = parseInt(uang_muka.replaceAll('.', ''))

            if (int_uang_muka > 0 && int_harga_pendanaan > int_uang_muka) {
                let total_pendanaan = int_harga_pendanaan - int_uang_muka
                $("#txt_nilai_pengajuan").val(formatRupiah(total_pendanaan.toString())).valid();
            } else {
                $("#txt_nilai_pengajuan").val('0');
            }
        })


        $('#btn_simpan_konfirmasi').click(function() {
            $("#form_add_pengajuan").trigger("submit");
        })

        $("form#form_add_pengajuan").submit(function(e) {
            e.preventDefault();
            
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: () => {
                    $('#otp').modal('hide');;
                    swal.fire({
                        html: '<h5>Menyimpan Data...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(data) {
                    console.log(data)
                    if (data.status == 'sukses') {
                        recordNumber = data.pengajuan_id
                        collectData();
                        swal.fire({
                            title: "Proses Berhasil",
                            text: "Pengajuan Pendanaan Anda Berhasil",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        }).then((result) => {
                            location.href = "/borrower/beranda";
                        });
                    } else {
                        let error_msg = data.message

                        swal.fire({
                            title: "Proses Gagal",
                            text: "Pengajuan Pendanaan Anda Gagal",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        }).then((result) => {
                            validationKPR(error_msg)
                        })
                    }
                },
                error: function(data) {
                    console.log(data)
                    swal.fire({
                        title: "Pendaftaran Gagal",
                        type: "error",
                        text: data,
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                    })
                },
            })
        })

        $("#btn_simpan_kpr").click(function() {
            if ($("#form_add_pengajuan").valid()) {
                $('#modal_konfirmasi').modal('show');
            }
        });

        // START: Validation from server
        const validationKPR = (error_msg) => {
            let input_id = {"1": "#txt_harga_objek_pendanaan", "2": "#txt_uang_muka", "3": "#txt_nilai_pengajuan" };

            let arr_msg = error_msg.split("#")
            
            $(input_id['1']).removeClass('is-invalid')
            $(input_id['2']).removeClass('is-invalid')
            $(input_id['3']).removeClass('is-invalid')

            arr_msg.forEach((element, index) => {
                let id = element.split(';')[0]
                let msg = element.split(';')[1]

                if (msg) {
                    $(input_id[id]).after(`
                    <label id="${input_id[id]}-error" class="error" for="${input_id[id]}">${msg}</label>
                    `).addClass('is-invalid')

                    if (index == 0) {
                        $(input_id[id]).focus()
                    }
                }
            });
        }
        // END: Validation from server
    </script>
    {{-- END: DOM Manipulation --}}
@endsection
