<div class="tab-pane pb-30 " id="informasi-pendanaan" role="tabpanel">

    <div class="layout">
        <h3 class="block-title text-black mb-10 font-w600">Informasi Objek
            Pendanaan</h3>
        <div class="row ml-30 mt-30">
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Tipe Pendanaan</h6>
                <p class="font-w600 text-dark">{{ $pendanaanTipe }}</p>
            </div>


            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Tujuan Pendanaan</h6>
                <p class="font-w600 text-dark">{{ $tujuanPendanaan }}</p>
            </div>

        </div>
        <div class="row ml-30 mt-30">

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Alamat Objek Pendanaan
                </h6>
                <p class="font-w600 text-dark" id="p_in_alamat_obj_pendanaan">
                    {{ !empty($pengajuan->lokasi_proyek) ? $pengajuan->lokasi_proyek : '-'}}
                </p>
            </div>
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Provinsi</h6>
                <p class="font-w600 text-dark" id="p_in_provinsi_obj_pendanaan">{{ !empty($pengajuan->provinsi) ? $pengajuan->provinsi : '-'}}</p>
            </div>
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kota/Kabupaten</h6>
                <p class="font-w600 text-dark" id="p_in_kota_obj_pendanaan">{{ !empty($pengajuan->kota) ? $pengajuan->kota : '-' }}</p>
            </div>
        </div>
        <div class="row ml-30 mt-30">

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kecamatan</h6>
                <p class="font-w600 text-dark" id="p_in_kecamatan_obj_pendanaan">{{ !empty($pengajuan->kecamatan) ? $pengajuan->kecamatan : '-' }}
                </p>
            </div>
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kelurahan</h6>
                <p class="font-w600 text-dark"  id="p_in_kelurahan_obj_pendanaan">{{ !empty($pengajuan->kelurahan) ? $pengajuan->kelurahan : '-' }}
                </p>
            </div>
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kode Pos</h6>
                <p class="font-w600 text-dark" id="p_in_kode_pos_obj_pendanaan">{{ !empty($pengajuan->kode_pos) ? $pengajuan->kode_pos : '-' }}</p>
            </div>
        </div>
        <div class="row ml-30 mt-30">

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Harga Objek Pendanaan</h6>
                <p class="font-w600 text-dark">Rp.
                    {{ number_format($pengajuan->harga_objek_pendanaan, 2, ',', '.') }}
                </p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Uang Muka</h6>
                <p class="font-w600 text-dark">Rp.
                    {{ number_format($pengajuan->uang_muka, 2, ',', '.') }}
                </p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Nilai Pengajuan Pendanaan
                </h6>
                <p class="font-w600 text-dark">Rp.
                    {{ number_format($pengajuan->pendanaan_dana_dibutuhkan, 2, ',', '.') }}
                </p>
            </div>

        </div>
        <div class="row ml-30 mt-30">

            <div class="col-12 col-md-4 pl-10">
                {{-- TODO : Jangka waktu dana rumah dan konstruksi (dana rumah : maks 15 tahun, dana konstruksi: maks 12 Bulan) --}}
                <h6 class="mb-0 text-muted font-w300">Jangka Waktu (Bulan)</h6>
                <p class="font-w600 text-dark">
                    {{ $pengajuan->durasi_proyek }}</p>
            </div>
        </div>

        <div class="row ml-30 mt-30">


            <div class="col-12 col-md-12 pl-10">
                <h6 class="mb-0 text-muted font-w300">Detail Informasi Objek
                    Pendanaan</h6>
                <p class="font-w600 text-dark" id="p_in_detail_obj_pendanaan">
                    {{ $pengajuan->detail_pendanaan }}</p>
            </div>

        </div>
    </div>
    <div class="layout mt-30">
        <h3 class="block-title text-black mb-10 font-w600">Informasi Pemilik
            Objek Pendanaan</h3>
        <div class="row ml-30 mt-30">
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Nama</h6>
                <p class="font-w600 text-dark" id="p_in_nama_pemilik_obj">{{ !empty($pengajuan->nm_pemilik) ? $pengajuan->nm_pemilik : '-' }}
                </p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">No Telp/HP</h6>
                <p class="font-w600 text-dark" id="p_in_no_hp_pemilik_obj">
                    {{ !empty($pengajuan->no_tlp_pemilik) ? $pengajuan->no_tlp_pemilik : '-'}}</p>
            </div>

        </div>
        <div class="row ml-30 mt-30">

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Alamat Objek Pendanaan
                </h6>
                <p class="font-w600 text-dark" id="p_in_alamat_pemilik_obj">
                    {{ !empty($pengajuan->alamat_pemilik) ? $pengajuan->alamat_pemilik : '-' }}</p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Provinsi</h6>
                <p class="font-w600 text-dark" id="p_in_provinsi_pemilik_obj">
                    {{ !empty($pengajuan->provinsi_pemilik) ? $pengajuan->provinsi_pemilik : '-' }}</p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kota/Kabupaten</h6>
                <p class="font-w600 text-dark" id="p_in_kota_pemilik_obj">{{ !empty($pengajuan->kota_pemilik) ? $pengajuan->kota_pemilik : '-' }}
                </p>
            </div>

        </div>
        <div class="row ml-30 mt-30">
            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kecamatan</h6>
                <p class="font-w600 text-dark" id="p_in_kecamatan_pemilik_obj">
                    {{ !empty($pengajuan->kecamatan_pemilik) ? $pengajuan->kecamatan_pemilik : '-' }}</p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kelurahan</h6>
                <p class="font-w600 text-dark" id="p_in_kelurahan_pemilik_obj">
                    {{ !empty($pengajuan->kelurahan_pemilik) ? $pengajuan->kelurahan_pemilik : '-' }}</p>
            </div>

            <div class="col-12 col-md-4 pl-10">
                <h6 class="mb-0 text-muted font-w300">Kode Pos</h6>
                <p class="font-w600 text-dark" id="p_in_kode_pos_pemilik_obj">
                    {{ !empty($pengajuan->kd_pos_pemilik) ? $pengajuan->kd_pos_pemilik : '-' }}</p>
            </div>

        </div>


    </div>
    <div class="layout mt-30">
        <h3 class="block-title text-black mb-10 font-w600">Status Pengajuan
            Pendanaan </h3>
        <div class="row ml-30 mt-20">
            <div class="col-12 col-md-12 pl-10">
                <p class="font-w600 text-dark">{{ $status_pengajuan }}</p>
            </div>

        </div>
    </div>

    <div class="layout mt-30">
        <h3 class="block-title text-black mb-10 font-w600">Catatan </h3>
        <div class="row ml-30 mt-20">
            <div class="col-12 col-md-12 pl-10">
                <span class="badge badge-warning text-black">{{ $pengajuan->catatan }}</span>
            </div>
        </div>
    </div>

    @if (empty($status_tolak) &&  ($pengajuan->status >= 1 && $pengajuan->status != '2') && Auth::user()->status == 'active' )
        <div class="row ml-10 mt-20">
            <div class="col-12 text-right">
                <button type="button" class="btn btn-success text-left" id="btnNext"> Selanjutnya >> </button>
            </div>

        </div>
    @endif

</div>
@push('info-scripts')
    <script>
        $(function() {
            $('#btnNext').click(function(e) {
                e.preventDefault();
                $('.nav-tabs a[href="#pengajuan-pendanaan"]').tab('show');
            })

        });
    </script>
@endpush
