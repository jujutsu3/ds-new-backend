<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Surat Perintah Kerja - Verifikasi {{ $data['pendanaan_nama'] }}</title>
    <style type="text/css">
        body {
            /* background-image: url("{{asset('img/body_spk_template.png')}}");*/
         
            width: 100% !important;
            height: 100%;
            margin: 0;
            line-height: 1.4;
            -webkit-text-size-adjust: none;
        }
    </style>
</head>
<body>
    <img src="{{asset('img/body_spk_template.png')}}" alt="background" style="width: 100%" />
    <div style="text-align: left; position:absolute; top:120px; width:100%;left: 30px;font-size:
    14px;">
         <table  width="100%" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
           
             <tr>
              <td style="padding: 20px 30px 40px 30px;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tr>
                   <td>
                    Jakarta, {{ $data['tanggal_surat'] }}
                   </td>
                  </tr>
                  <tr>
                   <td>
                    Nomor {{ $data['nomor_surat'] }}
                   </td>
                  </tr>
                  <tr>
                   <td>
                   &nbsp;
                   </td>
                  </tr>
                  <tr>
                    <td>
                      Kepada Yth.
                    </td>
                   </tr>
        
                   <tr>
                    <td>
                      Tim Verifikator (PT. OCCASIO MITRA INDONESIA)
                    </td>
                   </tr>
                   <tr>
                    <td>
                      Gedung The CEO, Lantai 12, Jalan TB. Simatupang Nomor 18C
                    </td>
                   </tr>
                   <tr>
                    <td>
                      Kelurahan Cilandak Barat, Kecamatan Cilandak, Kota Jakarta Selatan, 
                    </td>
                   </tr>
        
                   <tr>
                    <td>
                      Provinsi DKI Jakarta
                    </td>
                   </tr>
                   <tr>
                    <td>
                      (Selaku Rekanan Credit Analyst)
                    </td>
                   </tr>
                   <tr>
                    <td>
                    &nbsp;
                    </td>
                   </tr>
                   <tr>
                    <td>
                      Perihal : Perintah Kerja Untuk Melakukan Verifikasi dan Analisa Pendanaan {{ $data['pendanaan_nama'] }}
                    </td>
                   </tr>
                   <tr>
                    <td>
                    &nbsp;
                    </td>
                   </tr>
                   <tr>
                    <td>
                      <b>Assalamu’alaikum Warahmatullahi Wabarakatuh</b> <br>
                      Semoga Allah SWT senantiasa melimpahkan kesehatan, keselamatan dan rizky Nya. Kami mohon bantuannya untuk dapat dilakukan Verifikasi dan Analisa Pendanaan {{ $data['pendanaan_nama'] }}, dengan data-data sebagai berikut :
                    </td>
                   </tr>
                   <tr>
                    <td>
                    &nbsp;
                    </td>
                   </tr>
                   <tr>
                     <td align="left">
                        <table cellpadding="1" cellspacing="0" width="95%" style="font-size: 11px;">
                          <tr>
                            <th bgcolor="grey" style="border: 1px solid black;">ID Penerima Dana </th>
                            <th bgcolor="grey" style="border: 1px solid black;">ID Pengajuan Pendanaan </th>
                            <th bgcolor="grey" style="border: 1px solid black;">Nama Penerima Dana </th>
                            <th bgcolor="grey" style="border: 1px solid black;">Alamat </th>
                            <th bgcolor="grey" style="border: 1px solid black;">Nomor Telepon </th>
                          </tr>
        
                          <tr>
                            <td align="center" valign="top" style="border: 1px solid black;">{{ $data['borrower']->brw_id }}</td>
                            <td align="center" valign="top" style="border: 1px solid black;">{{ $data['pengajuan_id'] }} </td>
                            <td align="center" valign="top" style="border: 1px solid black;">{{ ($data['borrower']->brw_type === 1) ? $data['borrower']->nama : $data['borrower']->nm_bdn_hukum  }} </td>
                            <td valign="top" style="border: 1px solid black;">{{ $data['domisili_alamat'] }}  </td>
                            <td valign="top" style="border: 1px solid black;">{{ $data['borrower']->no_tlp }} </td>
                          </tr>
        
                        </table>
        
                     </td>
        
                   </tr>
                   <tr>
                    <td>
                    &nbsp;
                    </td>
                   </tr>
                   <tr>
                    <td>
                    &nbsp;
                    </td>
                   </tr>
                   <tr>
                    <td>
                      Demikian Surat Perintah Kerja (SPK) ini berlaku sejak diterbitkan. <br/>
                      <b>Wassalamu’alaikum warahmatullahi wabarakatuh</b>
                    </td>
                   </tr>
        
                   <tr>
                    <td>
                    &nbsp;
                    </td>
                   </tr>
        
                   <tr>
                    <td>
                      <img src="{{ asset('img/ttd_spk.png') }}" alt="ttd_spk" width="150" height="150" style="display: block;" />
                    </td>
                   </tr>
                   <tr>
                    <td>
                     <b><u>Adithya Wira Negara</u></b><br/><u>Credit Analyst Officer</u></b>
                    </td>
                   </tr>
        
                 </table>
              </td>
             </tr>
             <tr>
              <td>
              &nbsp;
              </td>
             </tr>
             <tr>
              <td>
              &nbsp;
              </td>
             </tr>
             
       
           </table> 

    </div>
</body>
</html>