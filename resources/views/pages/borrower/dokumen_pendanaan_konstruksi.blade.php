<div class="tab-pane" id="dokumen-pendanaan" role="tabpanel">
    <div class="block-title text-black mb-20">
        <p class="text-justify text-muted" style="font-size: 12px !important;">Silahkan unggah dokumen pendukung untuk
            validasi data pada tahap selanjutnya.
            Dokumen yang diunggah adalah hasil scan dengan format PDF atau gambar (pdf/.jpeg/.jpg/.png/.bmp).
            <br /><br />
            <span style="color: red;">*Maksimum 1MB per file re-size dari sistem dan resoulsi masih jelas</span>
            </label>
        </p>
        <form id="form_dokumen_pendanaan">
            <div class="layout">
                <h1 class="block-title text-black mb-10 font-w600 mb-20" style="font-size: 1em !important;">Daftar
                    Dokumen
                    Pendukung Penerima Pendanaan</h1>
                <h3 class="block-title text-black mb-10 font-w500">A. Dokumen Terkait Proyek</h3>
                
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Surat Permohonan Pengajuan Pembiayaan</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small>
                    </div>
                    <div class="col-4 text-right">

                        @if (isset($pengajuan->surat_permohonan) && !empty($pengajuan->surat_permohonan))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="surat_permohonan"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->surat_permohonan)]) }}"
                                id="btn_dokumen_daftar_harga_perumahan" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>

                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="surat_permohonan"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                id="btn_dokumen_surat_permohonan" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif

                    </div>
                </div>
                
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Daftar Surat Pemesanan Rumah (SPR) </label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small>
                    </div>
                    <div class="col-4 text-right">

                        @if (isset($pengajuan->surat_pemesanan_rumah) && !empty($pengajuan->surat_pemesanan_rumah))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="surat_pemesanan_rumah"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->surat_pemesanan_rumah)]) }}"
                                id="btn_dokumen_surat_pemesanan_rumah" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="surat_pemesanan_rumah"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                id="btn_dokumen_surat_pemesanan_rumah" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif

                    </div>
                </div>

                


                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Gambar Objek Pendanaan </label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small>
                    </div>
                    <div class="col-4 text-right">

                        @if (isset($pengajuan->gambar_objek_pendanaan) && !empty($pengajuan->gambar_objek_pendanaan))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="gambar_objek_pendanaan"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->gambar_objek_pendanaan)]) }}"
                                id="btn_dokumen_gambar_objek_pendanaan" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="gambar_objek_pendanaan"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                id="btn_dokumen_gambar_objek_pendanaan" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Sertifikat Objek Pendanaan (SHM atau SHGB) </label>
                        <small>(pdf)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->sertifikat) && !empty($pengajuan->sertifikat))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="sertifikat"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->sertifikat)]) }}"
                                id="btn_dokumen_sertifikat" data-filetype="pdf"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="sertifikat"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_sertifikat"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20" id="div_dokumen_imb">
                    <div class="col-8">
                        <label>IMB (Izin Mendirikan Bangunan) </label>
                        <small>(pdf)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->imb) && !empty($pengajuan->imb))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="imb"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->imb)]) }}"
                                id="btn_dokumen_imb" data-filetype="pdf"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="imb"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_imb"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>


                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Pajak Bumi dan Bangunan (PBB) Tahun Terakhir</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->pbb) && !empty($pengajuan->pbb))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="pbb"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->pbb)]) }}"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_pbb"
                                data-filetype="pdf|jpeg|jpg|png|bmp">
                                <i class="fa fa-eye" aria-hidden="true"></i> Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="pbb"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_pbb"
                                data-filetype="pdf|jpeg|jpg|png|bmp">
                                <i class="fa fa-upload" aria-hidden="true"></i> Unggah File</button>
                        @endif
                    </div>
                </div>
                
                
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Rencana Jadwal Pembangunan Proyek / Timeline</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small>
                    </div>
                    <div class="col-4 text-right">

                        @if (isset($pengajuan->planned_timeline) && !empty($pengajuan->planned_timeline))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="planned_timeline"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->planned_timeline)]) }}"
                                id="btn_dokumen_planned_timeline" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>

                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="planned_timeline"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}"
                                id="btn_dokumen_planned_timeline" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif

                    </div>
                </div>
            </div>

            <div class="layout mt-30">
                <h3 class="block-title text-black mb-10 font-w500">B. Dokumen Legalitas Pribadi Penerima Pendanaan</h3>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>KTP</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->ktp) && !empty($pengajuan->ktp))

                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="ktp"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->ktp)]) }}"
                                id="btn_dokumen_ktp" data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="ktp"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_ktp"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>NPWP</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->npwp) && !empty($pengajuan->npwp))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="npwp"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->npwp)]) }}"><i
                                    class="fa fa-eye" aria-hidden="true" id="btn_dokumen_npwp"
                                    data-filetype="pdf|jpeg|jpg|png|bmp"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="npwp"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_npwp"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Kartu Keluarga</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->kartu_keluarga) && !empty($pengajuan->kartu_keluarga))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="kartu_keluarga"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->kartu_keluarga)]) }}"
                                id="btn_dokumen_kartu_keluarga" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="kartu_keluarga"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_kartu_keluarga"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20" id="div_buku_nikah">
                    <div class="col-8">
                        <label>Buku Nikah/Akta Nikah (jika menikah)</label>
                        <small>(pdf)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->buku_nikah) && !empty($pengajuan->buku_nikah))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="buku_nikah"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->buku_nikah)]) }}"
                                id="btn_dokumen_buku_nikah" data-filetype="pdf"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="buku_nikah"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_buku_nikah"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <!-- TODO : akan muncul jika borrower nya status nya belum menikah -->
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Surat Keterangan Belum Menikah dari Kelurahan (jika belum menikah)</label>
                        <small>(pdf)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->surat_keterangan_belum_menikah) && !empty($pengajuan->surat_keterangan_belum_menikah))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung"
                                data-fieldname="surat_keterangan_belum_menikah"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->surat_keterangan_belum_menikah)]) }}"
                                id="btn_dokumen_surat_keterangan_belum_menikah" data-filetype="pdf"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung"
                                data-fieldname="surat_keterangan_belum_menikah"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                id="btn_dokumen_surat_keterangan_belum_menikah" data-filetype="pdf"><i
                                    class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20 d-none" id="div_akta_cerai">
                    <div class="col-8">
                        <label>Akta Cerai (Untuk Cerai Hidup) / Surat Kematian dari Rumah <br /> Sakit /
                            Pusksesmas/Desa/Kelurahan (Untuk Cerai Mati)</label><br />
                        <small>(pdf)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->akta_cerai) && !empty($pengajuan->akta_cerai))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="akta_cerai"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->akta_cerai)]) }}"
                                id="btn_dokumen_akta_cerai" data-filetype="pdf"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="akta_cerai"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_akta_cerai"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Surat Keterangan Domisili (Jika KTP tidak sesuai dengan tempat tinggal)</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->surat_domisili) && !empty($pengajuan->surat_domisili))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="surat_domisili"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->surat_domisili)]) }}"
                                id="btn_dokumen_surat_domisili" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="surat_domisili"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_surat_domisili"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Surat Keterangan Beda Nama atau tanggal lahir antara KTP/KK/Akta Nikah (Jika berbeda
                            salah
                            satunya)</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->surat_beda_nama) && !empty($pengajuan->surat_beda_nama))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="surat_beda_nama"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->surat_beda_nama)]) }}"
                                id="btn_dokumen_surat_beda_nama" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="surat_beda_nama"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_surat_beda_nama"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Perjanjian Pra-Nikah Penerima Pendanaan dari notaris yang di daftarkan ke KUA/catatan
                            sipil
                            (jika menikah dengan WNA atau membuat perjanjian pra nikah atas permintaan sendiri sebelum
                            menikah)
                        </label>
                        <small>(.pdf)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->perjanjian_pra_nikah) && !empty($pengajuan->perjanjian_pra_nikah))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="perjanjian_pra_nikah"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->perjanjian_pra_nikah)]) }}"
                                id="btn_dokumen_perjanjian_pra_nikah" data-filetype="pdf"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="perjanjian_pra_nikah"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_perjanjian_pra_nikah"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>
                
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>KTP Penjamin / Pendamping Proyek (Asosiasi) </label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->ktp_pendamping) && !empty($pengajuan->ktp_pendamping))

                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="ktp"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->ktp_pendamping)]) }}"
                                id="btn_dokumen_ktp_pendamping" data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="ktp_pendamping"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_ktp_pendamping"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>NPWP Penjamin / Pendamping Proyek (Asosiasi) </label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->npwp) && !empty($pengajuan->npwp))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="npwp_pendamping"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->npwp_pendamping)]) }}"><i
                                    class="fa fa-eye" aria-hidden="true" id="btn_dokumen_npwp_pendamping"
                                    data-filetype="pdf|jpeg|jpg|png|bmp"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="npwp"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_npwp"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Kartu Keluarga Penjamin / Pendamping Proyek (Asosiasi) </label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->kartu_keluarga) && !empty($pengajuan->kartu_keluarga))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="kartu_keluarga_pendamping"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->kartu_keluarga_pendamping)]) }}"
                                id="btn_dokumen_kartu_keluarga" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="kartu_keluarga_pendamping"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_kartu_keluarga_pendamping"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>
                
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Struktur Organisasi: KPI dan SOP Developer
                        </label>
                        <small>(.pdf)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->struktur_organisasi) && !empty($pengajuan->struktur_organisasi))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="struktur_organisasi"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->struktur_organisasi)]) }}"
                                id="btn_dokumen_perjanjian_pra_nikah" data-filetype="pdf"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="struktur_organisasi"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_struktur_organisasi"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>
                
                 <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Sertifikat Keanggotaan Developer (terdaftar di APERSI / REI / DPS)
                        </label>
                        <small>(.pdf)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->sertifikasi_developer) && !empty($pengajuan->sertifikasi_developer))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="sertifikasi_developer"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->sertifikasi_developer)]) }}"
                                id="btn_dokumen_perjanjian_pra_nikah" data-filetype="pdf"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="sertifikasi_developer"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_sertifikasi_developer"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>
                
                <div class="row ml-10 mt-20">
                        <div class="col-8">
                            <label>Surat Ijin Usaha Perdagangan (SIUP) / Nomor Induk Berusaha (NIB)
                            </label>
                            <small>(pdf)</small>
                        </div>
                        <div class="col-4 text-right">
                            @if (isset($pengajuan->siup) && !empty($pengajuan->siup))
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#div_view_dokumen_pendukung" data-fieldname="siup"
                                    data-tablename="brw_dokumen_legalitas_pribadi"
                                    data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->siup)]) }}"
                                    id="btn_dokumen_siup" data-filetype="pdf"><i class="fa fa-eye"
                                        aria-hidden="true"></i>
                                    Lihat File</button>
                            @else

                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                    data-target="#div_upload_dokumen_pendukung" data-fieldname="siup"
                                    data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_siup"
                                    data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                    Unggah File</button>
                            @endif
                        </div>
                    </div>
                    
                    
                    <div class="row ml-10 mt-20">
                        <div class="col-8">
                            <label>Surat Keterangan Domisili Usaha (SKDU) bila ada </label>
                            <small>(pdf, jpeg/jpg, png, bmp)</small>
                        </div>
                        <div class="col-4 text-right">
                            @if (isset($pengajuan->skdu) && !empty($pengajuan->skdu))
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#div_view_dokumen_pendukung" data-fieldname="skdu"
                                    data-tablename="brw_dokumen_legalitas_pribadi"
                                    data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->skdu)]) }}"
                                    id="btn_dokumen_skdu" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                        class="fa fa-eye" aria-hidden="true"></i>
                                    Lihat File</button>
                            @else
                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                    data-target="#div_upload_dokumen_pendukung" data-fieldname="skdu"
                                    data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_skdu"
                                    data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload"
                                        aria-hidden="true"></i>
                                    Unggah File</button>
                            @endif
                        </div>
                    </div>


            </div>
            <div class="layout mt-30">
                <h3 class="block-title text-black mb-10 font-w500">C. Dokumen Penghasilan</h3>

               

                @if (true)
                    

                    <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Rincian Anggaran Bangunan (RAB) proyek</label>
                        <small>(pdf)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->rab) && !empty($pengajuan->rab))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="rab"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->rab)]) }}"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_rab"
                                data-filetype="pdf">
                                <i class="fa fa-eye" aria-hidden="true"></i> Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="rab"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_rab"
                                data-filetype="pdf">
                                <i class="fa fa-upload" aria-hidden="true"></i> Unggah File</button>
                        @endif
                    </div>
                </div>
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Rencana Cash Flow/Arus Kas proyek</label>
                        <small>(pdf)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->ktp_pemilik) && !empty($pengajuan->ktp_pemilik))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="ktp_pemilik"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->ktp_pemilik)]) }}"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_ktp_pemilik"
                                data-filetype="pdf"><i class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="ktp_pemilik"
                                data-tablename="brw_dokumen_objek_pendanaan"
                                data-pengajuanid="{{ $pengajuan->pengajuan_id }}" id="btn_dokumen_ktp_pemilik"
                                data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>
                     

                    <div class="row ml-10 mt-20">
                        <div class="col-8">
                            <label>Laporan Keuangan (R/L dan Neraca) 2 tahun terakhir dan bulan berjalan
                            </label>
                            <small>(pdf)</small>
                        </div>
                        <div class="col-4 text-right">
                            @if (isset($pengajuan->laporan_keuangan) && !empty($pengajuan->laporan_keuangan))
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#div_view_dokumen_pendukung" data-fieldname="laporan_keuangan"
                                    data-tablename="brw_dokumen_legalitas_pribadi"
                                    data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->laporan_keuangan)]) }}"
                                    id="btn_dokumen_laporan_keuangan" data-filetype="pdf"><i class="fa fa-eye"
                                        aria-hidden="true"></i>
                                    Lihat File</button>
                            @else
                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                    data-target="#div_upload_dokumen_pendukung" data-fieldname="laporan_keuangan"
                                    data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_laporan_keuangan"
                                    data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                    Unggah File</button>
                            @endif
                        </div>
                    </div>

                    <div class="row ml-10 mt-20">
                        <div class="col-8">
                            <label>Mutasi Rekening Koran Yang Mencerminkan Pendapatan Usaha 6 Bulan Terakhir
                            </label>
                            <small>(pdf)</small>
                        </div>
                        <div class="col-4 text-right">
                            @if (isset($pengajuan->mutasi_rekening) && !empty($pengajuan->mutasi_rekening))
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#div_view_dokumen_pendukung" data-fieldname="mutasi_rekening"
                                    data-tablename="brw_dokumen_legalitas_pribadi"
                                    data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->mutasi_rekening)]) }}"
                                    id="btn_dokumen_mutasi_rekening" data-filetype="pdf"><i class="fa fa-eye"
                                        aria-hidden="true"></i>
                                    Lihat File</button>
                            @else
                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                    data-target="#div_upload_dokumen_pendukung" data-fieldname="mutasi_rekening"
                                    data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_mutasi_rekening"
                                    data-filetype="pdf"><i class="fa fa-upload" aria-hidden="true"></i>
                                    Unggah File</button>
                            @endif
                        </div>
                    </div>


                @endif


                <div class="row ml-10 mt-20 mb-20">
                    <div class="col-8">
                        <label>SPT Tahun Terakhir</label>
                        <small>(pdf, jpeg/jpg, png, bmp )</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->spt) && !empty($pengajuan->spt))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="spt"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->spt)]) }}"
                                id="btn_dokumen_spt" data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-eye"
                                    aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="spt"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_spt"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>


                <div class="row ml-10 mt-20 mb-20">
                    <div class="col-8">
                        <label>Dokumen Pendukung Pendapatan Lainnya (Jika Ada) </label>
                        <small>(pdf, jpeg/jpg, png, bmp )</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->dokumen_pendukung) && !empty($pengajuan->dokumen_pendukung))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="spt"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->dokumen_pendukung)]) }}"
                                id="btn_dokumen_dokumen_pendukung" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="dokumen_pendukung"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_dokumen_pendukung"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>

                        @endif
                    </div>
                </div>

            </div>


            <div class="layout mt-30 div_pasangan">
                <h1 class="block-title text-black mb-10 font-w600 mb-20" style="font-size: 20px !important;">Daftar
                    Dokumen
                    Pendukung Pasangan Penerima
                    Pendanaan</h1>
                <h3 class="block-title text-black mb-10 font-w500">A. Data Legalitas Pribadi Pasangan (Suami/Istri)
                    Penerima
                    Pendanaan </h3>
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>KTP Pasangan (Suami/Istri) Penerima Pendanaan (jika menikah)</label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->ktp_pasangan) && !empty($pengajuan->ktp_pasangan))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="ktp_pasangan"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->ktp_pasangan)]) }}"
                                id="btn_dokumen_ktp_pasangan" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="ktp_pasangan"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_ktp_pasangan"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>
                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>NPWP Pasangan (Suami/Istri) Penerima Pendanaan </label>
                        <small>(pdf, jpeg/jpg, png, bmp)</small><span style="color: red;">* </span>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->npwp_pasangan) && !empty($pengajuan->npwp_pasangan))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="npwp_pasangan"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->npwp_pasangan)]) }}"
                                id="btn_dokumen_npwp_pasangan" data-filetype="pdf|jpeg|jpg|png|bmp"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="npwp_pasangan"
                                data-tablename="brw_dokumen_legalitas_pribadi" id="btn_dokumen_npwp_pasangan"
                                data-filetype="pdf|jpeg|jpg|png|bmp"><i class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>

                <div class="row ml-10 mt-20">
                    <div class="col-8">
                        <label>Surat Persetujuan Pasangan (Suami/Istri) Penerima Pendanaan Jika tidak sama Domisili
                            dibawah tangan atau Notariil
                        </label>
                        <small>(pdf)</small>
                    </div>
                    <div class="col-4 text-right">
                        @if (isset($pengajuan->surat_persetujuan_pasangan) && !empty($pengajuan->surat_persetujuan_pasangan))
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#div_view_dokumen_pendukung" data-fieldname="surat_persetujuan_pasangan"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->surat_persetujuan_pasangan)]) }}"
                                id="btn_dokumen_surat_persetujuan_pasangan" data-filetype="pdf"><i
                                    class="fa fa-eye" aria-hidden="true"></i>
                                Lihat File</button>
                        @else
                            <button type="button" class="btn btn-secondary" data-toggle="modal"
                                data-target="#div_upload_dokumen_pendukung" data-fieldname="surat_persetujuan_pasangan"
                                data-tablename="brw_dokumen_legalitas_pribadi"
                                id="btn_dokumen_surat_persetujuan_pasangan" data-filetype="pdf"><i
                                    class="fa fa-upload" aria-hidden="true"></i>
                                Unggah File</button>
                        @endif
                    </div>
                </div>
            </div>

             

    </div>

    <!-- modal view file -->
    <div id="div_view_dokumen_pendukung" tabindex="-1" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered"
            style="max-height: calc(100vh - 200px);max-width: 50%;overflow-y: initial !important;">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Lihat File</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="preview"></div>
                </div>

                <div class="modal-footer">
                    {{-- <button type="button" data-dismiss="modal" class="btn btn-outline-secondary ml-1">Batal</button> --}}
                    <button type="button" id="btnUploadUlang" class="btn btn-secondary text-left" data-toggle="modal"
                        data-target="#div_upload_dokumen_pendukung" data-dismiss="modal"><i class="fa fa-upload"
                            aria-hidden="true"></i> Unggah Ulang File </button>
                </div>

            </div>

        </div>
        </form>
    </div>

    <!-- modal upload form -->
    <!-- Modal -->
    <div id="div_upload_dokumen_pendukung" tabindex="-1" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered"
            style="max-height: calc(100vh - 200px);overflow-y: auto;max-width: 30%;">

            <!-- Modal content-->
            <form id="form_upload_dokumen_pendukung" method='post' enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Unggah File</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- Form -->

                        {{-- <input type='file' name='file' id='file' class='form-control' required> --}}
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file" id="file"
                                oninput="input_filename();">
                            <label id="file_input_label" class="custom-file-label" for="image"></label>
                        </div>
                        <br>
                        <input type="hidden" name="fieldname" id="fieldname" value="" />
                        <input type="hidden" name="tablename" id="tablename" value="" />
                        <input type="hidden" name="pengajuanid" id="pengajuanId" value="" />
                        <input type="hidden" name="filetype" id="filetype" value="" />

                        <div class="mb-3 ml-10">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="setujuUpload" id="setujuUpload">
                                <label class="custom-control-label" for="setujuUpload">Dengan mengklik
                                tombol
                                unggah
                                file saya setuju dokumen ini akan tersimpan pada sistem
                                dan menyetujui proses selanjutnya oleh PT Dana Syariah Indonesia</label>
                            </div>
                            {{-- <input type="checkbox" name="setujuUpload" id="setujuUpload" /> <label class="text-dark font-weight-normal" for="setujuUpload"> Dengan mengklik
                                tombol
                                unggah
                                file saya setuju dokumen ini akan tersimpan pada sistem
                                dan menyetujui proses selanjutnya oleh PT Dana Syariah Indonesia
                            </label> --}}

                        </div>
                    </div>
                    <div class="modal-footer">
                        {{-- <button type="button" data-dismiss="modal" class="btn btn-outline-secondary ml-1">Batal</button> --}}
                        <button type="button" class="btn btn-success text-left" id="uploadDokumen" disabled><i
                                class="fa fa-upload" aria-hidden="true"></i> Unggah File </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

@push('upload-scripts')
    <script>
        var file_input_label = document.getElementById("file_input_label");
        const inputFile = document.getElementById('file');

        function input_filename() {
            file_input_label.innerText = inputFile.files[0].name;
        }

        $(function() {

            inputFile.addEventListener('change', (event) => {
                const target = event.target
                if (target.files && target.files[0]) {

                    const maxAllowedSize = 1 * 1024 * 1024;
                    if (target.files[0].size > maxAllowedSize) {

                        swal.fire({
                            title: "Proses Gagal",
                            text: "Maksimum Unggah File Size 1 MB ",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        }).then((result) => {
                            target.value = '';
                            file_input_label.innerText = '';
                        })
                    }
                }
            })

            $('#form_dokumen_pendanaan button').each(function() {
                var button_id = $(this).attr("id");
                $('#' + button_id).click(function() {

                    var field_name = $(this).attr('data-fieldname');
                    var table_name = $(this).attr('data-tablename');
                    var pengajuan_id = $(this).attr('data-pengajuanid');
                    var file_data = $(this).attr('data-filedata');
                    var file_type = $(this).attr('data-filetype');


                    // console.log(file_data);
                    $('#div_upload_dokumen_pendukung').on('show.bs.modal', function(e) {
                        $(e.currentTarget).find('#fieldname').val(field_name);
                        $(e.currentTarget).find('#tablename').val(table_name);
                        $(e.currentTarget).find('#pengajuanId').val(pengajuan_id);
                        $(e.currentTarget).find('#filetype').val(file_type);
                    });


                    $('#div_view_dokumen_pendukung').on('show.bs.modal', function(e) {
                        var pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");
                        view_dokumen_pendukung(file_data, field_name, table_name,
                            pengajuan_id, pdfSearch);
                    });

                });
            });

            $('#div_upload_dokumen_pendukung').on('hidden.bs.modal', function() {
                $(this).find('form')[0].reset();
                $("#uploadDokumen").attr("disabled", "disabled");
            });


            $("#setujuUpload").click(function() {
                var checked_status = this.checked;
                if (checked_status == true) {
                    $("#uploadDokumen").removeAttr("disabled");
                } else {
                    $("#uploadDokumen").attr("disabled", "disabled");
                }
            });


            $('#btnUploadUlang').bind('click', function() {

                var field_name_upload_ulang = $(this).attr('data-fieldname');
                var table_name_upload_ulang = $(this).attr('data-tablename');
                var pengajuan_id_upload_ulang = $(this).attr('data-pengajuanid');

                $('#div_upload_dokumen_pendukung').on('show.bs.modal', function(e) {
                    $(e.currentTarget).find('#fieldname').val(field_name_upload_ulang);
                    $(e.currentTarget).find('#tablename').val(table_name_upload_ulang);
                    $(e.currentTarget).find('#pengajuanId').val(pengajuan_id_upload_ulang);
                });

            });

            $('#div_upload_dokumen_pendukung').on('show.bs.modal', function(event) {
                var buttonUpload = $(event.relatedTarget);

                var fieldName = buttonUpload.data('fieldname');
                var tableName = buttonUpload.data('tablename');
                var pengajuanId = buttonUpload.data('pengajuanid');

                $(event.currentTarget).find('#fieldname').val(fieldName);
                $(event.currentTarget).find('#tablename').val(tableName);
                $(event.currentTarget).find('#pengajuanId').val(pengajuanId);

            });


            $("#uploadDokumen").click(function(e) {

                e.preventDefault();

                var formData = new FormData();
                var files = $('#file')[0].files;

                formData.append('_token', "{{ csrf_token() }}");
                formData.append('file', files[0]);
                formData.append('fieldName', $("#fieldname").val());
                formData.append('tableName', $("#tablename").val());
                formData.append('pengajuanId', $("#pengajuanId").val());
                formData.append('filetype', $("#filetype").val());
  
                $.ajax({
                    type: "post", 
                    url: "{{ route('borrower.danakonstruksi.upload_file') }}",
                    data: formData,
                    contentType: false,
                    processData: false,
                    beforeSend: () => {
                        swal.fire({
                            html: '<h5>Unggah File ...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                                $('#div_upload_dokumen_pendukung').modal('hide');
                            }
                        });
                    },
                    success: function(data) {

                        if (data.status == 'success') {
                            swal.fire({
                                title: "Proses Berhasil",
                                text: "Unggah File  Berhasil",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                            }).then((result) => {

                                $("#btn_dokumen_" + data.field).html(
                                    " <i class='fa fa-eye' aria-hidden='true'></i> Lihat File"
                                );
                                $("#btn_dokumen_" + data.field).attr("data-target",
                                    "#div_view_dokumen_pendukung");
                                $("#btn_dokumen_" + data.field).attr("data-filedata",
                                    data.file_link);
                                $("#btn_dokumen_" + data.field).removeClass(
                                    "btn btn-secondary");
                                $("#btn_dokumen_" + data.field).addClass(
                                    "btn btn-primary");

                                file_input_label.innerText = '';
                            });
                        } else {
                            let error_msg = data.message

                            swal.fire({
                                title: "Proses Gagal",
                                text: error_msg,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            }).then((result) => {
                                file_input_label.innerText = '';
                            })
                        }
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;

                        if (xhr.status === 419) {
                            swal.fire({
                                title: "Upload  Gagal",
                                type: "error",
                                text: "Page expired. please re-login again",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            })

                        }

 
                    },
                })
 
            });


            $('#div_view_dokumen_pendukung').on('show.bs.modal', function(event) {

                var button = $(event.relatedTarget)

                var fieldName = button.data('fieldname');
                var tableName = button.data('tablename');
                var pengajuanId = button.data('pengajuanid');
                var fileData = button.data('filedata');
                var pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");

                view_dokumen_pendukung(fileData, fieldName, tableName, pengajuanId, pdfSearch);


            });



            function view_dokumen_pendukung(fileData, fieldName, tableName, pengajuanId, searchFilter) {
                $('#preview').empty();
                if (searchFilter.test(fileData)) {
                    $('#preview').append($('<embed>', {
                        class: 'pdf_data',
                        src: fileData,
                        frameborder: '0',
                        width: '100%',
                        height: '500px'
                    }))
 
                } else {
                    $('#preview').append($('<img>', {
                        class: 'img_data',
                        src: fileData,
                        width: '100%',
                        height: '500px'
                    }))
                }

                $('#btnUploadUlang').attr('data-fieldname', fieldName);
                $('#btnUploadUlang').attr('data-tablename', tableName);
                $('#btnUploadUlang').attr('data-pengajuanid', pengajuanId);

            }


 
        });
    </script>
@endpush
