<div class="col-12">
    <!-- START: Form Badan Hukum -->
    <form id="form_lengkapi_profile" class="js-wizard-validation-classic-form" method="POST" enctype="multipart/form-data">
        @csrf
        <div id="layout-perusahaan" class="layout">
            <div class="row">
                <div class="col-md-12">
                    <!-- START: Informasi Perusahaan -->
                    <div class="js-wizard-simple block border">
                        <div class="block-content p-0 block-content-full tab-content">
                            <div class="block">
                                <div class="block-header block-header-default">
                                    <a class="input-hidden" id="label_tipe_borrower"></a>
                                    <h3 class="block-title">Informasi Perusahaan</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Nama
                                                    Badan Hukum</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" type="text" id="txt_nm_badan_hukum" maxlength="50" name="txt_nm_badan_hukum" placeholder="Masukkan Nama Badan Hukum..." value="{{ $data_individu ? $data_individu->nm_bdn_hukum : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_nib_perusahaan">Nomor Induk Berusaha (NIB)
                                                    </label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkNIB" type="text" maxlength="40" id="txt_nib_badan_hukum" name="txt_nib_badan_hukum" value="{{ $data_individu ? $data_individu->nib : ''}}" placeholder="Nomor Induk Berusaha (NIB)" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_npwp_perusahaan">NPWP</label>
                                                <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" minlength="15" maxlength="15" id="txt_npwp_badan_hukum" name="txt_npwp_badan_hukum" placeholder="Masukkan Nomor NPWP" value="{{ $data_individu ? $data_individu->npwp_perusahaan : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-ktp">Nomor Akta
                                                    Pendirian</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" maxlength="5" id="txt_akta_badan_hukum" name="txt_akta_badan_hukum" value="{{ $data_individu ? $data_individu->no_akta_pendirian : ''}}" placeholder="Masukkan nomor Akta Pendirian" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="wizard-progress2-namapengguna">Tanggal
                                                        Berdiri</label> <label class="mandatory_label">*</label>
                                                    <input class="form-control" type="date" id="txt_tgl_berdiri_badan_hukum" name="txt_tgl_berdiri_badan_hukum" value="{{ $data_individu ? $data_individu->tgl_berdiri : ''}}" max="<?= date("Y-m-d"); ?>" placeholder="Masukkan Tanggal Berdiri Perusahaan..." required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-ktp">Akta
                                                    Perubahan (Terakhir)</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" maxlength="5" id="no_akta_perubahan" name="no_akta_perubahan" value="{{ $data_individu ? $data_individu->no_akta_perubahan : ''}}" placeholder="Masukkan nomor Akta Perubahan (Terakhir)" required>
                                            </div>
                                        </div>

                                     

                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label class="wizard-progress2-namapengguna">Tanggal
                                                        Akta Perubahan (Terakhir)</label> <label class="mandatory_label">*</label>
                                                    <input class="form-control" type="date" id="tgl_akta_perubahan" name="tgl_akta_perubahan" value="{{ $data_individu ? $data_individu->tgl_akta_perubahan : ''}}" max="<?= date("Y-m-d"); ?>" placeholder="Masukkan Tanggal Akta Perubahan " required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_is_tbk">Status Perusahaan /
                                                    Badan Hukum  <i class="text-danger">*</i></label>
                                                <select class="form-control custom-select" id="is_tbk" name="is_tbk"  required>
                                                    <option value="">-- Pilih Satu --</option>
                                                    <option value="Y" {{ ($data_individu && $data_individu->is_tbk == 'Y') ?  ' selected' : '' }}>Tbk (Terbuka)</option>
                                                    <option value="N" {{ ($data_individu && $data_individu->is_tbk == 'N') ?  ' selected' : '' }}>Non Tbk</option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_no_tlp_perusahaan">Nomor
                                                    Telepon <i class="text-danger">*</i></label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                    <select class="form-control input-group-addon" id="kode_telepon"><option value="">Pilih</option></select>
                                                    </div>
                                                    <input class="form-control no-zero validasiString" type="text" minlength="6" maxlength="13" id="txt_tlp_badan_hukum" name="txt_tlp_badan_hukum" value="{{ $data_individu ? $data_individu->telpon_perusahaan : ''}}" placeholder="Masukkan Nomor Telepon" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label id="lbl_foto_npwp_perusahaan">Foto
                                                    NPWP Perusahaan <label class="mandatory_label">*</label>
                                                </label>
                                                    <div id="preview_npwp_perusahan" name="preview_npwp_perusahan">
                                                        <img class="imagePreview mb-3" id="preview-1" src="{{ $data_individu ? route('getUserFile', ['filename' => str_replace('/',':', $data_individu->foto_npwp_perusahaan)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_individu ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                    </div>
                                                    <div id="take_camera_npwp_badan_hukum">
                                                        <button class="btn btn-primary green-dsi col-md-9" id="btn_npwp_perusahan" name="btn_npwp_perusahan" type="button" onclick="btnCameraClick('camera_npwp_badan_hukum', 'preview_npwp_perusahan', 'take_camera_npwp_badan_hukum')">
                                                            <i class="fa fa-camera"> Kamera</i>
                                                        </button>
                                                    </div>
                                                    <div id="webcam_npwp_badan_hukum">
                                                        <div id="camera_npwp_badan_hukum" style="z-index:-1 !important;"></div>
                                                        <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 62px;">
                                                        <button class="btn btn-success col-md-9" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_npwp_badan_hukum(); $('#url_npwp_badan_hukum_val').val('1').valid()">Ambil Foto</button>
                                                        <input type="hidden" name="image_npwp_badan_hukum" id="image_npwp_badan_hukum" class="image-tag">
                                                        <input type="hidden" name="url_npwp_badan_hukum" id="url_npwp_badan_hukum" value="{{$data_individu ? $data_individu->foto_npwp_perusahaan : ''}}">
                                                    </div>
                                                    <div id="results_npwp_badan_hukum"></div>
                                                    <input type="text" class="input-hidden2" id="url_npwp_badan_hukum_val" name="url_npwp_badan_hukum_val" value="{{$data_individu ? $data_individu->foto_npwp_perusahaan : ''}}" required>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat Sesuai AKTA
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <!-- satuBaris -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Alamat</label>
                                                        <label class="mandatory_label">*</label>
                                                        <textarea class="form-control form-control-lg" maxlength="90" id="txt_alamat_badan_hukum" name="txt_alamat_badan_hukum" rows="2" placeholder="Masukkan alamat lengkap sesuai Akta Anda.." onchange="$('#domisili_badan_hukum').trigger('change')" required>{{ $data_individu ? $data_individu->alamat : ''}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Provinsi</label>
                                                        <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_provinsi_badan_hukum" name="txt_provinsi_badan_hukum" onchange="provinsiChange(this.value, this.id, 'txt_kota_badan_hukum'); $(this).valid(); $('#domisili_badan_hukum').trigger('change')" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-kota">Kota /
                                                            Kabupaten</label> <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_kota_badan_hukum" name="txt_kota_badan_hukum" onChange="kotaChange(this.value, this.id, 'txt_kecamatan_badan_hukum'); $(this).valid(); $('#domisili_badan_hukum').trigger('change')" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                                        <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_kecamatan_badan_hukum" name="txt_kecamatan_badan_hukum" onChange="kecamatanChange(this.value, this.id, 'txt_kelurahan_badan_hukum'); $(this).valid(); $('#domisili_badan_hukum').trigger('change')" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                                        <label class="mandatory_label">*</label>
                                                        <select class="form-control custom-select" id="txt_kelurahan_badan_hukum" name="txt_kelurahan_badan_hukum" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_badan_hukum',$('#txt_kecamatan_badan_hukum').val(),$('#txt_kota_badan_hukum').val(),$('#txt_provinsi_badan_hukum').val()); $(this).valid(); $('#domisili_badan_hukum').trigger('change');" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Kode
                                                            Pos</label> <label class="mandatory_label">*</label>
                                                        <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_badan_hukum" name="txt_kd_pos_badan_hukum" placeholder="--" value="{{$data_individu ? $data_individu->kode_pos : ''}}" readonly>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat
                                                    Domisili Sama Dengan AKTA <input type="checkbox" id="domisili_badan_hukum">
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="div_domisili_badan_hukum">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <!-- satuBaris -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-namapengguna">Alamat <i class="text-danger">*</i> </label>
                                                        <textarea class="form-control form-control-lg" maxlength="90" id="txt_alamat_domisili_badan_hukum" name="txt_alamat_domisili_badan_hukum" rows="2" placeholder="Masukkan alamat domisili sesuai dengan akta.." required>{{$data_individu ? $data_individu->domisili_alamat : ''}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_provinsi_domisili_badan_hukum">Provinsi <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_provinsi_domisili_badan_hukum" name="txt_provinsi_domisili_badan_hukum" onchange="provinsiChange(this.value, this.id, 'txt_kota_domisili_badan_hukum'); $(this).valid();" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kota_domisili_badan_hukum">Kota /
                                                            Kabupaten <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_kota_domisili_badan_hukum" name="txt_kota_domisili_badan_hukum" onchange="kotaChange(this.value, this.id, 'txt_kecamatan_domisili_badan_hukum'); $(this).valid();" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kecamatan_domisili_badan_hukum">Kecamatan <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_kecamatan_domisili_badan_hukum" name="txt_kecamatan_domisili_badan_hukum" onchange="kecamatanChange(this.value, this.id, 'txt_kelurahan_domisili_badan_hukum'); $(this).valid();" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kelurahan_domisili_badan_hukum">Kelurahan <i class="text-danger">*</i></label>
                                                        <select class="form-control custom-select" id="txt_kelurahan_domisili_badan_hukum" name="txt_kelurahan_domisili_badan_hukum" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_domisili_badan_hukum',$('#txt_kecamatan_domisili_badan_hukum').val(),$('#txt_kota_domisili_badan_hukum').val(),$('#txt_provinsi_domisili_badan_hukum').val()); $(this).valid();" required>
                                                            <option value="">-- Pilih Satu --</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_kd_pos_domisili_badan_hukum">Kode
                                                            Pos <i class="text-danger">*</i></label>
                                                        <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_domisili_badan_hukum" name="txt_kd_pos_domisili_badan_hukum" placeholder="--" value="{{$data_individu ? $data_individu->domisili_kd_pos : ''}}" readonly>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- START: Rekening -->
                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Informasi
                                                    Rekening
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END: Rekening -->

                                    <div class="row" id="div_hide_rek_bank_badan_hukum">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">No
                                                    Rekening </label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" minlength="10" maxlength="15" id="txt_no_rekening_badan_hukum" name="txt_no_rekening_badan_hukum" placeholder="Masukkan No Rekening..." value="{{$data_rekening ? $data_rekening->brw_norek : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Nama Pemilik
                                                    Rekening </label> <label class="mandatory_label">*</label>
                                                <input class="form-control hanyaAlfabet" type="text" maxlength="35" id="txt_nm_pemilik_badan_hukum" name="txt_nm_pemilik_badan_hukum" placeholder="Masukkan Nama Pemilik Rekening..." value="{{$data_rekening ? $data_rekening->brw_nm_pemilik : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Bank </label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_bank_pribadi" name="txt_bank_pribadi" onchange="$(this).valid()" required>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_kantor_cabang_pembuka">Kantor Cabang
                                                    Pembuka <i class="text-danger">*</i></label>
                                                <input class="form-control" type="text" maxlength="35" id="txt_kantor_cabang_pembuka" name="txt_kantor_cabang_pembuka" placeholder="Masukkan Kantor Cabang Pembuka" value="{{$data_rekening ? $data_rekening->kantor_cabang_pembuka : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Rekening -->

                                    <!-- START: Informasi Lain Lain -->
                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Informasi
                                                    Lain Lain
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <!-- satuBaris -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-kota">Bidang Usaha
                                                        </label> <label class="mandatory_label">*</label>
                                                        <select class="form-control" id="txt_bd_pekerjaan_badan_hukum" name="txt_bd_pekerjaan_badan_hukum" onchange="$(this).valid()" required></select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="wizard-progress2-kota">Omset Tahun
                                                            Terakhir </label> <label class="mandatory_label">*</label>
                                                        <input class="form-control no-zero" type="text" maxlength="30" id="txt_omset_thn_akhir" name="txt_omset_thn_akhir" placeholder="Masukkan Omset Tahun Terakhir" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))"  value="{{$data_individu ? $data_individu->omset_tahun_terakhir : ''}}" required>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="txt_aset_thn_akhir">Total Aset Tahun
                                                            Terakhir </label> <label class="mandatory_label">*</label>
                                                        <input class="form-control no-zero" type="text" maxlength="30" id="txt_aset_thn_akhir" name="txt_aset_thn_akhir" placeholder="Masukkan Omset Tahun Terakhir" onkeyup="if (/[^\d/]/g.test(this.value)) this.value = this.value.replace(/[^\d/]/g,''); $(this).val(formatRupiah($(this).val()))" value="{{$data_individu ? $data_individu->tot_aset_tahun_terakhr : ''}}" required>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                    <!-- END: Informasi Lain Lain -->
                                    <!-- END Informasi Perusahaan -->
                                </div>
                                {{-- END: Content --}}
                            </div>
                        </div>
                    </div>
                    <!-- END: Informasi Perusahaan -->
                    <!-- START: Informasi Pengurus 1-->
                    <div class="js-wizard-simple block border">
                        <div class="block-content p-0 block-content-full tab-content">
                            <div class="block ClassPengurus" id="divCountPengurus_1">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Informasi Pengurus</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Nama Pengurus
                                                </label> <label class="mandatory_label">*</label>
                                                <input class="form-control hanyaAlfabet" maxlength="35" type="text" id="txt_nm_pengurus" name="txt_nm_pengurus[]" placeholder="Masukkan Nama Pengurus ..." value="{{$data_pengurus_1 ? $data_pengurus_1->nm_pengurus : ''}}" required>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_jns_kelamin_pengurus_1">Jenis Kelamin <i class="text-danger">*</i></label>
                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus" id="txt_jns_kelamin_pengurus_l" value="1" required>
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_1">Laki -
                                                                Laki</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus" id="txt_jns_kelamin_pengurus_p" value="2">
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_p">Perempuan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group" id="divNoKTPPengurus">
                                                <label id="lbl_no_ktp_pengurus">Nomor Kartu Tanda Penduduk
                                                    (KTP)</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString checkNIKPengurus" type="text" minlength="16" maxlength="16" id="txt_no_ktp_pengurus" name="txt_no_ktp_pengurus[]" placeholder="Masukkan nomor KTP" onkeyup="CheckNIKPengurus()" value="{{$data_pengurus_1 ? $data_pengurus_1->nik_pengurus : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tempat
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="txt_tmpt_lahir_pengurus" name="txt_tmpt_lahir_pengurus[]" placeholder="Masukkan Tempat Lahir..." value="{{$data_pengurus_1 ? $data_pengurus_1->tempat_lahir : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tanggal
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control" type="date" id="txt_tgl_lahir_pengurus" name="txt_tgl_lahir_pengurus[]" max="{{date('Y-m-d')}}" placeholder="Masukkan Tanggal Lahir..." value="{{$data_pengurus_1 ? $data_pengurus_1->tgl_lahir : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group" id="div_hp_pengurus">
                                                <label id="lbl_no_hp_pengurus">Nomor HP</label> <label class="mandatory_label">*</label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi">
                                                            +62 </span>
                                                    </div>
                                                    <input class="form-control no-zero checkNOHPPengurus validasiString" type="text" size="13" minlength="9" maxlength="13" id="txt_noHP_pengurus" name="txt_noHP_pengurus[]" onkeyup="if($(this).val().length > 8){check_hp_pengurus()}" placeholder="Masukkan Nomor HP Pengurus" value="{{$data_pengurus_1 ? (substr($data_pengurus_1->no_tlp, 0,2) == '62' ? substr($data_pengurus_1->no_tlp,2) : $data_pengurus_1->no_tlp) : ''}}" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Agama</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_agama_pengurus" id="txt_agama_pengurus_1" name="txt_agama_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_pendidikanT_pengurus">Pendidikan
                                                    Terakhir</label> <label class="mandatory_label">*</label>
                                                <select class="form-control txt_pendidikanT_pengurus" id="txt_pendidikanT_pengurus" name="txt_pendidikanT_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_npwp_pengurus">Nomor NPWP</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString txt_npwp_pengurus" type="text" minlength="15" maxlength="15" id="txt_npwp_pengurus" name="txt_npwp_pengurus[]" placeholder="Masukkan nomor NPWP" value="{{$data_pengurus_1 ? $data_pengurus_1->npwp : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Jabatan</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_jabatan_pengurus" id="txt_jabatan_pengurus" name="txt_jabatan_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat Sesuai KTP
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Alamat Sesuai
                                                    KTP</label> <label class="mandatory_label">*</label>
                                                <textarea class="form-control form-control-lg txt_alamat_pengurus" maxlength="90" id="txt_alamat_pengurus" name="txt_alamat_pengurus[]" rows="4" placeholder="Masukkan alamat lengkap sesuai KTP Anda.." required>{{$data_pengurus_1 ? $data_pengurus_1->alamat : ''}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Provinsi</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_provinsi_pengurus" name="txt_provinsi_pengurus[]" onchange="provinsiChange(this.value, this.id, 'txt_kota_pengurus'); $(this).valid()" required>
                                                    {{-- <option value="">-- Pilih Satu --</option> --}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Kota / Kabupaten</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kota_pengurus" name="txt_kota_pengurus[]" onchange="kotaChange(this.value, this.id, 'txt_kecamatan_pengurus'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kecamatan_pengurus" name="txt_kecamatan_pengurus[]" onchange="kecamatanChange(this.value, this.id, 'txt_kelurahan_pengurus'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kelurahan_pengurus" name="txt_kelurahan_pengurus[]" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_pengurus',$('#txt_kecamatan_pengurus').val(),$('#txt_kota_pengurus').val(),$('#txt_provinsi_pengurus').val()); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kode Pos</label>
                                                <label class="mandatory_label">*</label>
                                                <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_pengurus" name="txt_kd_pos_pengurus[]" placeholder="--"  value="{{$data_pengurus_1 ? $data_pengurus_1->kode_pos : ''}}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Foto Diri Individu -->
                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Foto Pengurus
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <!-- <div class="col-md-12" id="div_foto">
                                            <div class="row"> -->
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_pengurus">Foto Pengurus <label class="mandatory_label">*</label></label>
                                                        <div class="col-12">
                                                            <div id="preview_camera_pengurus" name="preview_camera_pengurus">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_1 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_1->foto_diri)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_1 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_pengurus">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_diri" name="btn_camera_diri" type="button" onclick="btnCameraClick('camera_pengurus', 'preview_camera_pengurus', 'take_camera_pengurus')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_pengurus">
                                                                <img id="user-guide" src="{{URL::to('assets/img/user-guide.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_pengurus" style="z-index:-1 !important;"></div>
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_pengurus(); $('#url_pic_pengurus_val').val('1').valid()">Ambil Foto</button>
                                                                <input type="hidden" name="image_foto_pengurus[]" id="image_foto_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_pengurus[]" id="url_pic_pengurus" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_diri : '' }}">
                                                            </div>
                                                            <div id="results_foto_pengurus"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_pengurus_val" name="url_pic_pengurus_val" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_diri : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_ktp_pengurus">Foto KTP Pengurus <label class="mandatory_label">*</label></label>
                                                        <div class="col-12">
                                                            <div id="foto_ktp_pengurus" name="foto_ktp_pengurus">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_1 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_1->foto_ktp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_1 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_foto_ktp_pengurus">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_tkp" name="btn_camera_ktp" type="button" onclick="btnCameraClick('camera_ktp_pengurus', 'foto_ktp_pengurus', 'take_camera_foto_ktp_pengurus')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_foto_ktp_pengurus">
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_ktp_pengurus" style="z-index:-1 !important;"></div>
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktp_pengurus(); $('#url_pic_ktp_pengurus_val').val('1').valid()">Ambil Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_pengurus[]" id="image_foto_ktp_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_ktp_pengurus[]" id="url_pic_ktp_pengurus" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_ktp : '' }}">
                                                            </div>
                                                            <div id="results_foto_ktp_pengurus"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_ktp_pengurus_val" name="url_pic_ktp_pengurus_val" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_ktp : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_ktp_pengurus">Foto Pengurus Dengan KTP <label class="mandatory_label">*</label></label>
                                                        <div class="col-12">
                                                            <div id="foto_ktpdiri_pengurus" name="foto_ktpdiri_pengurus">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_1 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_1->foto_diri_ktp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_1 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_foto_ktpdiri_pengurus">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_ktp_diri" name="btn_camera_ktp_diri" type="button" onclick="btnCameraClick('camera_ktpdiri_pengurus', 'foto_ktpdiri_pengurus', 'take_camera_foto_ktpdiri_pengurus')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_foto_ktpdiri_pengurus">
                                                                <div id="camera_ktpdiri_pengurus" style="z-index:-1 !important;"></div>
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-diridanktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktpdiri_pengurus(); $('#url_pic_brw_dengan_ktp_pengurus_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_diri_pengurus[]" id="image_foto_ktp_diri_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_brw_dengan_ktp_pengurus[]" id="url_pic_brw_dengan_ktp_pengurus" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_diri_ktp : '' }}">
                                                            </div>
                                                            <div id="results_foto_ktp_diri_pengurus"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_brw_dengan_ktp_pengurus_val" name="url_pic_brw_dengan_ktp_pengurus_val" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_diri_ktp : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_npwp_pengurus">Foto NPWP Pengurus<label class="mandatory_label">*</label></label>
                                                        <div class="col-12">
                                                            <div id="foto_npwp_pengurus" name="foto_npwp_pengurus">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_1 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_1->foto_npwp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_1 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_foto_npwp_pengurus">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_npwp_pengurus" name="btn_camera_npwp_pengurus" type="button" onclick="btnCameraClick('camera_npwp_pengurus', 'foto_npwp_pengurus', 'take_camera_foto_npwp_pengurus')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_foto_npwp_pengurus">
                                                                <div id="camera_npwp_pengurus" style="z-index:-1 !important;"></div>
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_npwp_pengurus(); $('#url_pic_npwp_pengurus_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="image_foto_npwp_pengurus[]" id="image_foto_npwp_pengurus" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_npwp_pengurus[]" id="url_pic_npwp_pengurus" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_npwp : '' }}">
                                                            </div>
                                                            <div id="results_foto_npwp_pengurus"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_npwp_pengurus_val" name="url_pic_npwp_pengurus_val" value="{{ $data_pengurus_1 ? $data_pengurus_1->foto_npwp : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END Foto Diri Pengurus -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Informasi Pengurus 1-->
                    <!-- START: Informasi Pengurus 2-->
                    <div class="js-wizard-simple block border">
                        <div class="block-content p-0 block-content-full tab-content">
                            <div class="block ClassPengurus" id="divCountPengurus_2">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Informasi Pengurus 2</h3>
                                    <div class="block-options">
                                        <button type="button" class="btn-block-option" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Nama
                                                    Pengurus</label> <label class="mandatory_label">*</label>
                                                <input class="form-control hanyaAlfabet" maxlength="35" type="text" id="txt_nm_pengurus_2" name="txt_nm_pengurus[]" placeholder="Masukkan Nama Pengurus ..." value="{{$data_pengurus_2 ? $data_pengurus_2->nm_pengurus : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="txt_jns_kelamin_pengurus_2">Jenis Kelamin <i class="text-danger">*</i></label>
                                                <div class="col-12">
                                                    <div class="input-group">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus2" id="txt_jns_kelamin_pengurus_2l" value="1" required>
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_2l">Laki -
                                                                Laki</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="txt_jns_kelamin_pengurus2" id="txt_jns_kelamin_pengurus_2p" value="2">
                                                            <label class="form-check-label text-muted" for="txt_jns_kelamin_pengurus_2p">Perempuan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_no_ktp_pengurus_2">Nomor Kartu Tanda Penduduk
                                                    (KTP)</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString" type="text" minlength="16" maxlength="16" id="txt_no_ktp_pengurus_2" name="txt_no_ktp_pengurus[]" placeholder="Masukkan nomor KTP" value="{{$data_pengurus_2 ? $data_pengurus_2->nik_pengurus : ''}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tempat
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control checkKarakterAneh" maxlength="35" type="text" id="txt_tmpt_lahir_pengurus_2" name="txt_tmpt_lahir_pengurus[]" placeholder="Masukkan Tempat Lahir..." value="{{$data_pengurus_2 ? $data_pengurus_2->tempat_lahir : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Tanggal
                                                    Lahir</label> <label class="mandatory_label">*</label>
                                                <input class="form-control" type="date" id="txt_tgl_lahir_pengurus_2" name="txt_tgl_lahir_pengurus[]" max="{{date("Y-m-d")}}" placeholder="Masukkan Tanggal Lahir..." value="{{$data_pengurus_2 ? $data_pengurus_2->tgl_lahir : ''}}" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_no_hp_pengurus_2">Nomor HP</label> <label class="mandatory_label">*</label>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text input-group-text-dsi">
                                                            +62 </span>
                                                    </div>
                                                    <input class="form-control no-zero eight checkNOHPPengurus validasiString" type="text" minlength="9" maxlength="13" id="txt_noHP_pengurus_2" name="txt_noHP_pengurus[]" placeholder="Masukkan Nomor HP Pengurus" value="{{$data_pengurus_2 ? (substr($data_pengurus_2->no_tlp, 0,2) == '62' ? substr($data_pengurus_2->no_tlp,2) : $data_pengurus_2->no_tlp) : ''}}" required>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Agama</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_agama_pengurus_2" id="txt_agama_pengurus_2" name="txt_agama_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-npwp">Pendidikan
                                                    Terakhir</label> <label class="mandatory_label">*</label>
                                                <select class="form-control txt_pendidikanT_pengurus_2" id="txt_pendidikanT_pengurus_2" name="txt_pendidikanT_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label id="lbl_npwp_pengurus_2">Nomor NPWP</label> <label class="mandatory_label">*</label>
                                                <input class="form-control validasiString txt_npwp_pengurus" type="text" minlength="15" maxlength="15" id="txt_npwp_pengurus_2" name="txt_npwp_pengurus[]" placeholder="Masukkan nomor NPWP" value="{{$data_pengurus_2 ? $data_pengurus_2->npwp : ''}}" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Jabatan</label> <label class="mandatory_label">*</label>
                                                <select class="form-control select2 txt_jabatan_pengurus" id="txt_jabatan_pengurus_2" name="txt_jabatan_pengurus[]" onchange="$(this).valid()" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Alamat Sesuai KTP
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Alamat Sesuai
                                                    KTP</label> <label class="mandatory_label">*</label>
                                                <textarea class="form-control form-control-lg txt_alamat_pengurus" maxlength="90" id="txt_alamat_pengurus_2" name="txt_alamat_pengurus[]" rows="4" placeholder="Masukkan alamat lengkap sesuai KTP Anda.." required>{{$data_pengurus_2 ? $data_pengurus_2->alamat : ''}}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Provinsi</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_provinsi_pengurus_2" name="txt_provinsi_pengurus[]" onchange="provinsiChange(this.value, this.id, 'txt_kota_pengurus_2'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-kota">Kota / Kabupaten</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kota_pengurus_2" name="txt_kota_pengurus[]" onchange="kotaChange(this.value, this.id, 'txt_kecamatan_pengurus_2'); $(this).valid()" required>
                                                    <option>-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kecamatan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kecamatan_pengurus_2" name="txt_kecamatan_pengurus[]" onchange="kecamatanChange(this.value, this.id, 'txt_kelurahan_pengurus_2'); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kelurahan</label>
                                                <label class="mandatory_label">*</label>
                                                <select class="form-control custom-select" id="txt_kelurahan_pengurus_2" name="txt_kelurahan_pengurus[]" onchange="kelurahanChange(this.value, this.id, 'txt_kd_pos_pengurus_2',$('#txt_kecamatan_pengurus_2').val(),$('#txt_kota_pengurus_2').val(),$('#txt_provinsi_pengurus_2').val()); $(this).valid()" required>
                                                    <option value="">-- Pilih Satu --</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="wizard-progress2-namapengguna">Kode Pos</label>
                                                <label class="mandatory_label">*</label>
                                                <input class="form-control" type="text" maxlength="30" id="txt_kd_pos_pengurus_2" name="txt_kd_pos_pengurus[]" placeholder="--" value="{{$data_pengurus_2 ? $data_pengurus_2->kode_pos : ''}}" readonly>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 mb-4 pb-4">
                                            <div class="form-check form-check-inline line">
                                                <label class="form-check-label text-black h6 ml-0">Foto Pengurus
                                                    &nbsp</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" id="div_foto">
                                            <div class="row">
                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_pengurus_2">Foto Pengurus <i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="preview_camera_pengurus_2" name="preview_camera_pengurus_2">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_2 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_2->foto_diri)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_2 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_pengurus_2">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_diri" name="btn_camera_diri" type="button" onclick="btnCameraClick('camera_pengurus_2', 'preview_camera_pengurus_2', 'take_camera_pengurus_2')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_pengurus_2">
                                                                <img id="user-guide" src="{{URL::to('assets/img/user-guide.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_pengurus_2" style="z-index:-1 !important;"></div>
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_pengurus_2(); $('#url_pic_pengurus_2_val').val('1').valid()">Ambil
                                                                    Foto</button>
                                                                <input type="hidden" name="results_foto_pengurus[]" id="image_foto_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_pengurus[]" id="url_pic_pengurus_2" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_diri : '' }}" required>
                                                            </div>
                                                            <div id="results_foto_pengurus_2"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_pengurus_2_val" name="url_pic_pengurus_2_val" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_diri : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_ktp_pengurus_2">Foto KTP Pengurus <i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="foto_ktp_pengurus_2" name="foto_ktp_pengurus_2">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_2 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_2->foto_ktp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_2 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_foto_ktp_pengurus_2">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_ktp_2" name="btn_camera_ktp_2" type="button" onclick="btnCameraClick('camera_ktp_pengurus_2', 'foto_ktp_pengurus_2', 'take_camera_foto_ktp_pengurus_2')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_foto_ktp_pengurus_2">
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <div id="camera_ktp_pengurus_2" style="z-index:-1 !important;"></div>
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktp_pengurus_2(); $('#url_pic_ktp_pengurus_2_val').val('1').valid()">Ambil Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_pengurus[]" id="image_foto_ktp_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_ktp_pengurus[]" id="url_pic_ktp_pengurus_2" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_ktp : '' }}" required>
                                                            </div>
                                                            <div id="results_foto_ktp_pengurus_2"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_ktp_pengurus_2_val" name="url_pic_ktp_pengurus_2_val" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_ktp : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_ktp_pengurus_2">Foto Pengurus Dengan KTP <i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="foto_ktpdiri_pengurus_2" name="foto_ktpdiri_pengurus_2">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_2 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_2->foto_diri_ktp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_2 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_foto_ktpdiri_pengurus_2">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_ktp_diri_2" name="btn_camera_ktp_diri_2" type="button" onclick="btnCameraClick('camera_ktpdiri_pengurus_2', 'foto_ktpdiri_pengurus_2', 'take_camera_foto_ktpdiri_pengurus_2')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_foto_ktpdiri_pengurus_2">
                                                                <div id="camera_ktpdiri_pengurus_2" style="z-index:-1 !important;"></div>
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-diridanktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_ktpdiri_pengurus_2(); $('#url_pic_brw_dengan_ktp_pengurus_2_val').val('1').valid()">Ambil Foto</button>
                                                                <input type="hidden" name="image_foto_ktp_diri_pengurus[]" id="image_foto_ktp_diri_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_brw_dengan_ktp_pengurus[]" id="url_pic_brw_dengan_ktp_pengurus_2" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_diri_ktp : '' }}" required>
                                                            </div>
                                                            <div id="results_foto_ktp_diri_pengurus_2"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_brw_dengan_ktp_pengurus_2_val" name="url_pic_brw_dengan_ktp_pengurus_2_val" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_diri_ktp : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12 col-xl-3 col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-12" id="lbl_foto_npwp_pengurus_2">Foto NPWP Pengurus<i class="text-danger">*</i></label>
                                                        <div class="col-12">
                                                            <div id="foto_npwp_pengurus_2" name="foto_npwp_pengurus_2">
                                                                <img class="imagePreview mb-3" id="preview-1" src="{{ $data_pengurus_2 ? route('getUserFile', ['filename' => str_replace('/',':', $data_pengurus_2->foto_npwp)]) .'?t='.date('Y-m-d h:i:sa') : '' }}" style="{{ $data_pengurus_2 ? 'width: 180px; height: 138px; top: 32px;' : ''}}">
                                                            </div>
                                                            <div id="take_camera_foto_npwp_pengurus_2">
                                                                <button class="btn btn-primary green-dsi col-md-10" id="btn_camera_ktp_diri_2" name="btn_camera_ktp_diri_2" type="button" onclick="btnCameraClick('camera_npwp_pengurus_2', 'foto_npwp_pengurus_2', 'take_camera_foto_npwp_pengurus_2')">
                                                                    <i class="fa fa-camera"> Kamera</i>
                                                                </button>
                                                            </div>
                                                            <div id="webcam_foto_npwp_pengurus_2">
                                                                <div id="camera_npwp_pengurus_2" style="z-index:-1 !important;"></div>
                                                                <img id="user-guide" src="{{URL::to('assets/img/guide-ktp.png')}}" alt="guide" style="position: absolute; z-index: 1;  width: 180px; height: 138px; top: 32px;">
                                                                <button class="btn btn-success col-md-10" style="margin-bottom: 20px; z-index:2 !important; position:relative;" type="button" onClick="take_snapshot_foto_npwp_pengurus_2(); $('#url_pic_npwp_pengurus_2_val').val('1').valid()">Ambil Foto</button>
                                                                <input type="hidden" name="image_foto_npwp_pengurus[]" id="image_foto_npwp_pengurus_2" class="image-tag"><br />
                                                                <input type="hidden" name="url_pic_npwp_pengurus[]" id="url_pic_npwp_pengurus_2" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_npwp : '' }}" required>
                                                            </div>
                                                            <div id="results_foto_npwp_pengurus_2"></div>
                                                            <input type="text" class="input-hidden2" id="url_pic_npwp_pengurus_2_val" name="url_pic_npwp_pengurus_2_val" value="{{ $data_pengurus_2 ? $data_pengurus_2->foto_npwp : '' }}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END: Informasi Pengurus 2-->
                    <!-- START: Steps Navigation -->
                    <div class="block-content block-content-sm block-content-full bg-body-light">
                        <div class="row">
                            <!-- <div class="col-6 text-left d-none">
                                <button type="reset" class="btn btn-alt-info" >
                                    <i class="fa fa-refresh mr-5"></i> Atur Ulang
                                </button>

                            </div> -->
                            <div class="col-12 text-right">

                                <button type="submit" id="btn_lengkapi_profile" class="btn btn-alt-primary">
                                    <i class="fa fa-check mr-5"></i> Kirim
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- END: Steps Navigation -->
                </div>
            </div>
        </div>
    </form>
    <!-- END: Form Badan Hukum -->
</div>