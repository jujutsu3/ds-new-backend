<div class="tab-pane" id="dokumen-pendanaan" role="tabpanel">
    <div class="block-title text-black mb-20">
        <p class="text-justify text-muted" style="font-size: 12px !important;">Silahkan unggah dokumen pendukung untuk
            validasi data pada tahap selanjutnya.
            Dokumen yang diunggah adalah hasil scan dengan format PDF atau gambar (pdf/.jpeg/.jpg/.png/.bmp).
            <br /><br />
            <span style="color: red;">*Maksimum 1MB per file re-size dari sistem dan resolusi masih jelas</span>
            </label>
        </p>



        <form id="form_dokumen_pendanaan">

            <div class="layout" id="layout-setuju-dokumen-verifikator">
                <h1 class="block-title text-black mb-10 font-w600 mb-20">Informasi Persetujuan</h1>
                <div class="row ml-10 mt-20">
                    <div class="col-12">
                        <div class="form-check">
                            <input type="checkbox" name="setujuVerifikatorDokumen" id="setujuVerifikatorDokumen"
                                value="1" class="form-check-input" checked disabled>
                            <label class="form-label" for="setujuVerifikatorDokumen"> Bersama ini saya menunjuk dan
                                memberi kuasa penuh kepada PT. Dana Syariah Indonesia, Bertindak untuk dan atas nama
                                saya dalam mengunggah data di bawah ini</label>
                        </div>
                    </div>
                </div>
                <br />

                @if (isset($data_dokumen_persyaratan) && count($data_dokumen_persyaratan) > 0)

                    @foreach ($data_dokumen_persyaratan as $keyPageTitle => $valCategoryTitle)
                        <h2>{{ $keyPageTitle }}</h2>


                        @foreach ($valCategoryTitle as $keyCategoryTitle => $valFieldName)
                            <h3 class="block-title text-black mb-10 font-w500">{{ $keyCategoryTitle }}</h3>

                            @foreach ($valFieldName as $keyFieldName => $value_field)
                                @php $field_name = $value_field->field_name @endphp

                                @php
                                    if (isset($value_field->dokumen_log)) {
                                        $class_button = 'btn btn-danger';
                                        $class_fa = 'fa fa-warning';
                                        $catatan_dokumen = $value_field->dokumen_log->alasan_tolak;
                                    } else {
                                        $class_button = 'btn btn-primary';
                                        $class_fa = 'fa fa-eye';
                                        $catatan_dokumen = '';
                                    }
                                    
                                @endphp

                                @if ($field_name == 'imb')
                                    <div class="row ml-10 mt-10 {{ $field_name }}" id="div_dokumen_imb">
                                        @if (isset($pengajuan->no_imb) && !empty($pengajuan->no_imb))
                                            <div class="col-8">
                                                <label>{{ $value_field->persyaratan_nama }} </label>
                                                <small> ({{ $value_field->file_type }})</small>
                                                @if ($value_field->persyaratan_mandatory == '1')
                                                    <span style="color: red;">* </span>
                                                @endif
                                            </div>
                                            <div class="col-4 text-right">
                                                @if (isset($pengajuan->$field_name) && !empty($pengajuan->$field_name))
                                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                                        data-target="#div_view_dokumen_pendukung"
                                                        data-fieldname="{{ $value_field->field_name }}"
                                                        data-tablename="{{ $value_field->table_name }}"
                                                        data-pengajuanid="{{ $pengajuan_id }}"
                                                        data-brwid="{{ $brw_id }}"
                                                        data-catatan="{{ $catatan_dokumen }}"
                                                        data-namadokumen="{{ $value_field->persyaratan_nama }}"
                                                        data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->$field_name)]) }}"
                                                        id="btn_dokumen_{{ $value_field->field_name }}"
                                                        data-filetype="{{ $value_field->file_type }}"><i
                                                            class="fa fa-eye" aria-hidden="true"></i>
                                                        Lihat File</button>
                                                @else
                                                    <button type="button" class="btn btn-secondary" data-toggle="modal"
                                                        data-target="#div_upload_dokumen_pendukung"
                                                        data-fieldname="{{ $value_field->field_name }}"
                                                        data-tablename="{{ $value_field->table_name }}"
                                                        data-pengajuanid="{{ $pengajuan_id }}"
                                                        data-brwid="{{ $brw_id }}"
                                                        id="btn_dokumen_{{ $value_field->field_name }}"
                                                        data-filetype="{{ $value_field->file_type }}"><i
                                                            class="fa fa-upload" aria-hidden="true"></i>
                                                        Unggah File</button>
                                                @endif
                                            </div>
                                        @endif
                                    </div>
                                @else
                                    <div class="row ml-10 mt-10">

                                        <div class="col-8">
                                            <label>{{ $value_field->persyaratan_nama }} </label>
                                            <small> ({{ $value_field->file_type }})</small>
                                            @if ($value_field->persyaratan_mandatory == '1')
                                                <span style="color: red;">* </span>
                                            @endif
                                        </div>
                                        <div class="col-4 text-right">
                                            @if (isset($pengajuan->$field_name) && !empty($pengajuan->$field_name))
                                                <button type="button" class="{{ $class_button }}"
                                                    data-toggle="modal" data-target="#div_view_dokumen_pendukung"
                                                    data-fieldname="{{ $value_field->field_name }}"
                                                    data-tablename="{{ $value_field->table_name }}"
                                                    data-pengajuanid="{{ $pengajuan_id }}"
                                                    data-brwid="{{ $brw_id }}"
                                                    data-catatan="{{ $catatan_dokumen }}"
                                                    data-namadokumen="{{ $value_field->persyaratan_nama }}"
                                                    data-filedata="{{ route('getUserFile', ['filename' => str_replace('/', ':', $pengajuan->$field_name)]) }}"
                                                    id="btn_dokumen_{{ $value_field->field_name }}"
                                                    data-filetype="{{ $value_field->file_type }}"><i
                                                        class="{{ $class_fa }}" aria-hidden="true"></i>
                                                    Lihat File</button>
                                            @else
                                                <button type="button" class="btn btn-secondary" data-toggle="modal"
                                                    data-target="#div_upload_dokumen_pendukung"
                                                    data-fieldname="{{ $value_field->field_name }}"
                                                    data-tablename="{{ $value_field->table_name }}"
                                                    data-pengajuanid="{{ $pengajuan_id }}"
                                                    data-brwid="{{ $brw_id }}"
                                                    id="btn_dokumen_{{ $value_field->field_name }}"
                                                    data-filetype="{{ $value_field->file_type }}"><i
                                                        class="fa fa-upload" aria-hidden="true"></i>
                                                    Unggah File</button>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endforeach
                    @endforeach


                @endif
            </div>
        </form>



    </div>


    <!-- modal view file -->
    <div id="div_view_dokumen_pendukung" tabindex="-1" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered"
            style="max-height: calc(100vh - 200px);max-width: 50%;overflow-y: initial !important;">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title lihat-file" id="lihatFile">Lihat File</h5>
                    <span class="badge badge-danger div_catatan_dokumen d-none"
                        style="font-size: 14px; !important;"></span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="preview"></div>
                </div>

                {{-- @if (in_array($pengajuan->status, [1, 2, 11])) --}}
                <div class="modal-footer">
                    {{-- <button type="button" data-dismiss="modal" class="btn btn-outline-secondary ml-1">Batal</button> --}}
                    <button type="button" id="btnUploadUlang" class="btn btn-secondary text-left"
                        data-toggle="modal" data-target="#div_upload_dokumen_pendukung" data-dismiss="modal"><i
                            class="fa fa-upload" aria-hidden="true"></i> Unggah Ulang File </button>
                </div>
                {{-- @endif --}}

            </div>

        </div>
    </div>

    <!-- modal upload form -->
    <!-- Modal -->
    <div id="div_upload_dokumen_pendukung" tabindex="-1" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-sm modal-dialog-centered"
            style="max-height: calc(100vh - 200px);overflow-y: auto;max-width: 30%;">

            <!-- Modal content-->
            <form id="form_upload_dokumen_pendukung" method='post' enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="scrollmodalLabel">Unggah File</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- Form -->

                        {{-- <input type='file' name='file' id='file' class='form-control' required> --}}
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file" id="file"
                                oninput="input_filename();">
                            <label id="file_input_label" class="custom-file-label text-truncate"
                                for="image"></label>
                        </div>
                        <br>
                        <input type="hidden" name="fieldname" id="fieldname" value="" />
                        <input type="hidden" name="tablename" id="tablename" value="" />
                        <input type="hidden" name="pengajuanid" id="pengajuanId" value="" />
                        <input type="hidden" name="filetype" id="filetype" value="" />

                        <div class="mb-3 ml-10">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="setujuUpload"
                                    id="setujuUpload">
                                <label class="custom-control-label" for="setujuUpload">Dengan mengklik
                                    tombol
                                    unggah
                                    file saya setuju dokumen ini akan tersimpan pada sistem
                                    dan menyetujui proses selanjutnya oleh PT Dana Syariah Indonesia</label>
                            </div>
                            {{-- <input type="checkbox" name="setujuUpload" id="setujuUpload" /> <label class="text-dark font-weight-normal" for="setujuUpload"> Dengan mengklik
                                tombol
                                unggah
                                file saya setuju dokumen ini akan tersimpan pada sistem
                                dan menyetujui proses selanjutnya oleh PT Dana Syariah Indonesia
                            </label> --}}

                        </div>
                    </div>
                    <div class="modal-footer">
                        {{-- <button type="button" data-dismiss="modal" class="btn btn-outline-secondary ml-1">Batal</button> --}}
                        <button type="button" class="btn btn-success text-left" id="uploadDokumen" disabled><i
                                class="fa fa-upload" aria-hidden="true"></i> Unggah File </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

@push('upload-scripts')
    <script>
        var file_input_label = document.getElementById("file_input_label");
        const inputFile = document.getElementById('file');

        function input_filename() {
            file_input_label.innerText = inputFile.files[0].name;
        }

        $(function() {

            var status_pengajuan = "{{ $pengajuan->status }}";
            var status_kawin = "{{ $status_kawin }}";
            var skema_pembiayaan = "{{ $skema_pembiayaan }}";
            var status_verifikator = "{{ $status_verifikator }}";

            var enable_edit = "{{ $enable_edit }}";
            if (!enable_edit) $(".btn-secondary").addClass("d-none");

            switch (status_kawin) {
                case '1':
                    $(".div_pasangan").removeClass("d-none");
                    $(".div_sudah_kawin").removeClass("d-none");
                    $(".div_belum_kawin").addClass("d-none");


                    if (skema_pembiayaan == '1') {
                        $(".div_skema_pembiayaan_pasangan").addClass("d-none");
                    } else if (skema_pembiayaan == '2') {
                        $(".div_skema_pembiayaan_pasangan").removeClass("d-none");
                    }

                    break;
                case '2':
                    $(".div_pasangan").addClass("d-none");
                    $(".div_sudah_kawin").addClass("d-none");
                    $(".div_skema_pembiayaan_pasangan").addClass("d-none");
                    break;
                case '3':
                    $(".div_pasangan").addClass("d-none");
                    $(".div_sudah_kawin").addClass("d-none");
                    $(".div_belum_kawin").addClass("d-none");
                    $(".div_akta_cerai").removeClass("d-none");
                    $(".div_skema_pembiayaan_pasangan").addClass("d-none");
                    break;
            }

            inputFile.addEventListener('change', (event) => {
                const target = event.target
                if (target.files && target.files[0]) {

                    const maxAllowedSize = 1 * 1024 * 1024;
                    if (target.files[0].size > maxAllowedSize) {

                        swal.fire({
                            title: "Proses Gagal",
                            text: "Maksimum Unggah File Size 1 MB ",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        }).then((result) => {
                            target.value = '';
                            file_input_label.innerText = '';
                        })
                    }
                }
            })

            $('#form_dokumen_pendanaan button').each(function() {
                var button_id = $(this).attr("id");
                $('#' + button_id).click(function() {

                    var field_name = $(this).attr('data-fieldname');
                    var table_name = $(this).attr('data-tablename');
                    var pengajuan_id = $(this).attr('data-pengajuanid');
                    var file_data = $(this).attr('data-filedata');
                    var file_type = $(this).attr('data-filetype');
                    var catatan = $(this).attr('data-catatan');


                    // console.log(file_data);
                    $('#div_upload_dokumen_pendukung').on('show.bs.modal', function(e) {
                        $(e.currentTarget).find('#fieldname').val(field_name);
                        $(e.currentTarget).find('#tablename').val(table_name);
                        $(e.currentTarget).find('#pengajuanId').val(pengajuan_id);
                        $(e.currentTarget).find('#filetype').val(file_type);
                    });


                    $('#div_view_dokumen_pendukung').on('show.bs.modal', function(e) {
                        var pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");
                        view_dokumen_pendukung(file_data, field_name, table_name,
                            pengajuan_id, pdfSearch, catatan);
                    });

                });
            });

            $('#div_upload_dokumen_pendukung').on('hidden.bs.modal', function() {
                $('#form_upload_dokumen_pendukung')[0].reset();
                $("#uploadDokumen").attr("disabled", "disabled");
            });


            $("#setujuUpload").click(function() {
                var checked_status = this.checked;
                if (checked_status == true) {
                    $("#uploadDokumen").removeAttr("disabled");
                } else {
                    $("#uploadDokumen").attr("disabled", "disabled");
                }
            });


            $('#btnUploadUlang').bind('click', function() {

                var field_name_upload_ulang = $(this).attr('data-fieldname');
                var table_name_upload_ulang = $(this).attr('data-tablename');
                var pengajuan_id_upload_ulang = $(this).attr('data-pengajuanid');

                $('#div_upload_dokumen_pendukung').on('show.bs.modal', function(e) {
                    $(e.currentTarget).find('#fieldname').val(field_name_upload_ulang);
                    $(e.currentTarget).find('#tablename').val(table_name_upload_ulang);
                    $(e.currentTarget).find('#pengajuanId').val(pengajuan_id_upload_ulang);
                });

            });

            $('#div_upload_dokumen_pendukung').on('show.bs.modal', function(event) {
                var buttonUpload = $(event.relatedTarget);

                var fieldName = buttonUpload.data('fieldname');
                var tableName = buttonUpload.data('tablename');
                var pengajuanId = buttonUpload.data('pengajuanid');

                $(event.currentTarget).find('#fieldname').val(fieldName);
                $(event.currentTarget).find('#tablename').val(tableName);
                $(event.currentTarget).find('#pengajuanId').val(pengajuanId);

            });


            $("#uploadDokumen").click(function(e) {

                e.preventDefault();

                var formData = new FormData();
                var files = $('#file')[0].files;

                formData.append('_token', "{{ csrf_token() }}");
                formData.append('file', files[0]);
                formData.append('fieldName', $("#fieldname").val());
                formData.append('tableName', $("#tablename").val());
                formData.append('pengajuanId', $("#pengajuanId").val());
                formData.append('filetype', $("#filetype").val());

                $.ajax({
                    type: "post",
                    url: "{{ route('borrower.danarumah.upload_file') }}",
                    data: formData,
                    contentType: false,
                    processData: false,
                    beforeSend: () => {
                        swal.fire({
                            html: '<h5>Unggah File ...</h5>',
                            showConfirmButton: false,
                            allowOutsideClick: () => false,
                            onBeforeOpen: () => {
                                swal.showLoading();
                                $('#div_upload_dokumen_pendukung').modal('hide');
                            }
                        });
                    },
                    success: function(data) {

                        if (data.status == 'success') {
                            swal.fire({
                                title: "Proses Berhasil",
                                text: "Unggah File  Berhasil",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonClass: "btn-success",
                            }).then((result) => {

                                $("#btn_dokumen_" + data.field).html(
                                    " <i class='fa fa-eye' aria-hidden='true'></i> Lihat File"
                                );
                                $("#btn_dokumen_" + data.field).attr("data-target",
                                    "#div_view_dokumen_pendukung");
                                $("#btn_dokumen_" + data.field).attr("data-filedata",
                                    data.file_link);
                                $("#btn_dokumen_" + data.field).removeClass(
                                    "btn btn-secondary");
                                $("#btn_dokumen_" + data.field).addClass(
                                    "btn btn-primary");

                                file_input_label.innerText = '';
                            });
                        } else {
                            let error_msg = data.message

                            swal.fire({
                                title: "Proses Gagal",
                                text: error_msg,
                                type: "error",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            }).then((result) => {
                                file_input_label.innerText = '';
                            })
                        }
                    },
                    error: function(xhr, status, error) {
                        var errorMessage = xhr.status + ': ' + xhr.statusText;

                        if (xhr.status === 419) {
                            swal.fire({
                                title: "Upload  Gagal",
                                type: "error",
                                text: "Page expired. please re-login again",
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                            })

                        }


                    },
                })

            });


            $('#div_view_dokumen_pendukung').on('show.bs.modal', function(event) {

                var button = $(event.relatedTarget)

                var fieldName = button.data('fieldname');
                var tableName = button.data('tablename');
                var pengajuanId = button.data('pengajuanid');
                var fileData = button.data('filedata');
                var pdfSearch = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.pdf)$");

                view_dokumen_pendukung(fileData, fieldName, tableName, pengajuanId, pdfSearch);


            });

            function view_dokumen_pendukung(fileData, fieldName, tableName, pengajuanId, searchFilter, catatan) {
                $('#preview').empty();
                $('.div_catatan_dokumen').empty();
                if (searchFilter.test(fileData)) {
                    $('#preview').append($('<embed>', {
                        class: 'pdf_data',
                        src: fileData,
                        frameborder: '0',
                        width: '100%',
                        height: '500px'
                    }))

                } else {
                    $('#preview').append($('<img>', {
                        class: 'img_data',
                        src: fileData,
                        width: '100%',
                        height: '500px'
                    }))
                }

                $('#btnUploadUlang').attr('data-fieldname', fieldName);
                $('#btnUploadUlang').attr('data-tablename', tableName);
                $('#btnUploadUlang').attr('data-pengajuanid', pengajuanId);

                if (catatan) {
                    $('.div_catatan_dokumen').removeClass("d-none");
                    $('.lihat-file').addClass("d-none");
                    $('.div_catatan_dokumen').html("Catatan : " + catatan + "");
                } else {
                    $('.div_catatan_dokumen').addClass("d-none");
                    $('.lihat-file').removeClass("d-none");
                }

            }



        });
    </script>
@endpush
