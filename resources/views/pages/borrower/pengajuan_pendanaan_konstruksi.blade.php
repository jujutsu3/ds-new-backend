@push('more-style')
<style>
    .line {
        display: flex;
        flex-direction: row;
    }

    .line:after {
        content: "";
        flex: 1 1;
        border-bottom: 1px solid #000;
        margin: auto;
    }

    #in_no_hp_pemilik_obj-error {
        position: absolute;
        top: 100%;
    }
</style>
@endpush

<div class="tab-pane" id="pengajuan-pendanaan" role="tabpanel">
    <form id="form_pengajuan_pendanaan_konstruksi" method='post'
        action="{{ route('borrower.danarumah.pengajuan_pendanaan') }}">
        @csrf
        <input type="hidden" name="id_pengajuan" value="{{ $pengajuan->pengajuan_id }}" />
        <div id="layout-informasi-objek-pendanaan" class="layout mt-4">
            <h3 class="block-title text-black mb-10 font-w600 line">Informasi Objek Pendanaan &nbsp;</h3>

            <div class="row">
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_alamat_obj_pendanaan" class="form-label">Alamat Objek Pendanaan</label>
                    <input type="text" name="in_alamat_obj_pendanaan" id="in_alamat_obj_pendanaan"
                        value="{{ $pengajuan ? $pengajuan->lokasi_proyek : ''}}" placeholder="ketik disini"
                        class="form-control" />
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_provinsi_obj_pendanaan" class="form-label">Provinsi</label>
                    <select name="in_provinsi_obj_pendanaan" class="form-control custom-select"
                        id="in_provinsi_obj_pendanaan"
                        onChange="provinsiChange(this.value, this.id, 'in_kota_obj_pendanaan')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kota_obj_pendanaan" class="form-label">Kabupaten/Kota</label>
                    <select name="in_kota_obj_pendanaan" class="form-control custom-select" id="in_kota_obj_pendanaan"
                        onChange="kotaChange(this.value, this.id, 'in_kecamatan_obj_pendanaan')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kecamatan_obj_pendanaan" class="form-label">Kecamatan</label>
                    <select name="in_kecamatan_obj_pendanaan" class="form-control custom-select"
                        id="in_kecamatan_obj_pendanaan"
                        onChange="kecamatanChange(this.value, this.id, 'in_kelurahan_obj_pendanaan')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kelurahan_obj_pendanaan" class="form-label">Kelurahan</label>
                    <select name="in_kelurahan_obj_pendanaan" class="form-control custom-select"
                        id="in_kelurahan_obj_pendanaan"
                        onChange="kelurahanChange(this.value, this.id, 'in_kode_pos_obj_pendanaan',$('#in_kecamatan_obj_pendanaan').val(),$('#in_kota_obj_pendanaan').val(),$('#in_provinsi_obj_pendanaan').val())">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kode_pos_obj_pendanaan" class="form-label">Kode Pos</label>
                    <input type="text" name="in_kode_pos_obj_pendanaan" id="in_kode_pos_obj_pendanaan"
                        placeholder="--" class="form-control" value="{{ $pengajuan->kode_pos }}" readonly />
                </div>
                <div class="col-12 pl-10 mb-20">
                    <label for="in_dtl_obj_pendanaan" class="form-label">Detil Informasi Proyek Konstruksi</label>
                    <textarea class="form-control" rows="6" cols="80" id="in_dtl_obj_pendanaan"
                        name="in_dtl_obj_pendanaan">{{ $pengajuan ? $pengajuan->detail_pendanaan : ''}}</textarea>
                </div>
            </div>

        </div>

        <div id="layout-informasi-pemilik-objek" class="layout mt-4">
            <h3 class="block-title text-black mb-10 font-w600 line">Informasi Pemilik Objek Pendanaan &nbsp;</h3>
            <div class="row">
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_nama_pemilik_obj" class="form-label">Nama</label>
                    <input type="text" name="in_nama_pemilik_obj" id="in_nama_pemilik_obj"
                        value="{{ $pengajuan ? $pengajuan->nm_pemilik : ''}}" placeholder="ketik disini"
                        class="form-control" />
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_no_hp_pemilik_obj">No.
                        Telepon/HP</label> <label class="mandatory_label">*</label>
                    <div class="input-group">
                        <div class="input-group-append">
                            <span class="input-group-text input-group-text-dsi">
                                +62
                            </span>
                        </div>
                        <input class="form-control no-zero" id="in_no_hp_pemilik_obj" name="in_no_hp_pemilik_obj"
                            onkeyup="this.value = this.value.replace(/[^\d/]/g,'')" pattern=".{9, 13}" minlength="9"
                            maxlength="13" placeholder="ketik disini"
                            value="{{ $pengajuan ? substr($pengajuan->no_tlp_pemilik, 2) : '' }}">
                    </div>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_alamat_pemilik_obj" class="form-label">Alamat Pemilik Objek Pendanaan</label>
                    <input type="text" name="in_alamat_pemilik_obj" id="in_alamat_pemilik_obj"
                        value="{{ $pengajuan ? $pengajuan->alamat_pemilik : '' }}" placeholder="ketik disini"
                        class="form-control" />
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_provinsi_pemilik_obj" class="form-label">Provinsi</label>
                    <select name="in_provinsi_pemilik_obj" class="form-control custom-select"
                        id="in_provinsi_pemilik_obj"
                        onChange="provinsiChange(this.value, this.id, 'in_kota_pemilik_obj')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kota_pemilik_obj" class="form-label">Kabupaten/Kota</label>
                    <select name="in_kota_pemilik_obj" class="form-control custom-select" id="in_kota_pemilik_obj"
                        onChange="kotaChange(this.value, this.id, 'in_kecamatan_pemilik_obj')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kecamatan_pemilik_obj" class="form-label">Kecamatan</label>
                    <select name="in_kecamatan_pemilik_obj" class="form-control custom-select"
                        id="in_kecamatan_pemilik_obj"
                        onChange="kecamatanChange(this.value, this.id, 'in_kelurahan_pemilik_obj')">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kelurahan_pemilik_obj" class="form-label">Kelurahan</label>
                    <select name="in_kelurahan_pemilik_obj" class="form-control custom-select"
                        id="in_kelurahan_pemilik_obj"
                        onChange="kelurahanChange(this.value, this.id, 'in_kode_pos_pemilik_obj',$('#in_kecamatan_pemilik_obj').val(),$('#in_kota_pemilik_obj').val(),$('#in_provinsi_pemilik_obj').val())">
                        <option value="">-- Pilih Satu --</option>
                    </select>
                </div>
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="in_kode_pos_pemilik_obj" class="form-label">Kode Pos</label>
                    <input type="text" name="in_kode_pos_pemilik_obj" id="in_kode_pos_pemilik_obj"
                        placeholder="--" class="form-control" value="{{ $pengajuan->kd_pos_pemilik }}" readonly />
                </div>
            </div>
        </div>

        <div id="layout-informasi-pendanaan-berjalan" class="layout mt-4">
            <h3 class="block-title text-black mb-10 font-w600 line">Informasi Pendanaan
                Berjalan &nbsp;</h3>


            <div class="row ml-10 mt-20">
                <div class="col-8 my-auto">
                    <label class="form-label">Apakah anda memiliki fasilitas
                        pembiayaan konstruksi lainnya ?
                    </label>
                </div>
                <div class="col-4 text-right my-auto">
                    <select name="status_bank" id="status_bank" class="form-control custom-select">
                        <option value="1" {{ $status_bank_non_rumah_lain == '1' ? 'selected' : '' }}>Ya</option>
                        <option value="2" {{ $status_bank_non_rumah_lain == '2' ? 'selected' : '' }}>Tidak</option>
                    </select>
                </div>

            </div>

            <div class="row ml-10 mt-20">
                <div class="table-responsive">
                    <table class="table table-sm d-none" id="table_dana_non_rumah_lain">
                        <thead class="thead-dark" style="font-size: 12px;">
                            <tr>
                                <th>Bank</th>
                                <th>Plafond (Rp)</th>
                                <th>Jangka Waktu</th>
                                <th>Outstanding (Rp)</th>
                                <th>Angsuran (Rp)</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>

        <div id="layout-informasi-agunan" class="layout mt-4">
            <h3 class="block-title text-black mb-10 font-w600 line">Informasi Agunan &nbsp;</h3>

            <div class="row ml-10 mt-20">
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="jenis_agunan" class="form-label">Jenis
                        Agunan</label>
                    {!! Form::select('jenis_agunan', ["1"=>"SHM","2"=>"HGB"], $pengajuan->jenis_agunan, ['id' =>
                    'jenis_agunan', 'class' => 'form-control custom-select']) !!}
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="nomor_agunan" class="form-label">Nomor
                        Agunan</label>
                    <input type="text" name="nomor_agunan" value="{{ $pengajuan->nomor_agunan }}" id="nomor_agunan"
                        placeholder="ketik disini" class="form-control" />
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="nomor_agunan" class="form-label">Atas Nama
                        Agunan</label>
                    <input type="text" name="atas_nama_agunan" id="atas_nama_agunan"
                        value="{{ $pengajuan->atas_nama_agunan }}" placeholder="ketik disini" class="form-control" />
                </div>

            </div>

            <div class="row ml-10 mt-20">

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="luas_bangunan" class="form-label">Luas Bangunan
                        (m2)</label>
                    <input type="text" name="luas_bangunan" id="luas_bangunan" value="{{ $pengajuan->luas_bangunan }}"
                        placeholder="ketik disini" class="form-control numberOnly no-zero" />
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="luas_tanah" class="form-label">Luas Tanah
                        (m2)</label>
                    <input type="text" name="luas_tanah" id="luas_tanah" value="{{ $pengajuan->luas_tanah }}"
                        placeholder="ketik disini" class="form-control numberOnly no-zero" />
                </div>


                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="nomor_surat_ukur" class="form-label">Nomor Surat
                        Ukur </label>
                    <input type="text" name="nomor_surat_ukur" id="nomor_surat_ukur"
                        value="{{ $pengajuan->nomor_surat_ukur }}" placeholder="ketik disini" class="form-control" />
                </div>

            </div>
            <div class="row ml-10 mt-20">

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="nomor_surat_ukur" class="form-label">Tanggal Surat
                        Ukur </label>
                    <input type="date" class="form-control bg-white" id="tanggal_surat_ukur" data-min-date="today"
                        data-date-format="d-m-Y" name="tanggal_surat_ukur" value="{{ $pengajuan->tanggal_surat_ukur }}"
                        id="tanggal_surat_ukur" placeholder="dd-mm-YYYY" data-allow-input="false">
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="nomor_surat_ukur" class="form-label">Tanggal Terbit
                    </label>
                    <input type="date" class="form-control bg-white" id="tanggal_terbit" data-min-date="today"
                        data-date-format="d-m-Y" name="tanggal_terbit" placeholder="dd-mm-YYYY"
                        data-allow-input="false">
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="kantor_penerbit" class="form-label">Kantor
                        Penerbit</label>
                    {!! Form::select('kantor_penerbit', $kantor_bpn, $pengajuan->kantor_penerbit, ['id' =>
                    'kantor_penerbit', 'class' => 'form-control custom-select', 'placeholder' => 'Pilih Kantor']) !!}
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20 d-none" id="div_tanggal_jatuh_tempo">
                    <label for="nomor_surat_ukur" class="form-label">Tanggal Jatuh Tempo
                    </label>
                    <input type="date" class="form-control bg-white" id="tanggal_jatuh_tempo" data-min-date="today"
                        data-date-format="d-m-Y" name="tanggal_jatuh_tempo"
                        value="{{ $pengajuan->tanggal_jatuh_tempo }}" placeholder="dd-mm-YYYY" data-allow-input="false"
                        required>
                </div>
            </div>

            <div class="row ml-10 mt-20">
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="alamat_agunan" class="form-label">Alamat
                        Agunan</label>
                    <input type="text" name="alamat_agunan" id="alamat_agunan" value="{{ $pengajuan->lokasi_proyek }}"
                        class="form-control" />
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="blok_nomor" class="form-label">Blok/Nomor
                    </label>
                    <input type="text" id="blok_nomor" name="blok_nomor" value="{{ $pengajuan->blok_nomor }}"
                        placeholder="ketik disini" class="form-control" />
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="RT" class="form-label">RT
                    </label>
                    <input type="text" name="RT" value="{{ $pengajuan->RT }}" id="RT" placeholder="ketik disini"
                        class="form-control" />
                </div>
            </div>

            <div class="row ml-10 mt-20">

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="RW" class="form-label">RW
                    </label>
                    <input type="text" name="RW" value="{{ $pengajuan->RW }}" id="RW" placeholder="ketik disini"
                        class="form-control" />
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="provinsi" class="form-label">Provinsi *
                    </label>
                    <select name="provinsi" class="form-control custom-select" id="provinsi" disabled>
                    </select>
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="kota" class="form-label">Kabupaten/Kota *
                    </label>
                    <select name="kota" class="form-control custom-select" id="kota" disabled>
                    </select>
                </div>

            </div>
            <div class="row ml-10 mt-20">


                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="kecamatan" class="form-label">Kecamatan *
                    </label>
                    <select name="kecamatan" class="form-control custom-select" id="kecamatan" disabled>
                    </select>
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="kelurahan" class="form-label">Kelurahan *
                    </label>
                    <select name="kelurahan" class="form-control custom-select" id="kelurahan" disabled>
                    </select>
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="kode_pos" class="form-label">Kode Pos
                    </label>
                    <input type="text" name="kode_pos" id="kode_pos" value="{{ $pengajuan->kode_pos }}" readonly
                        class="form-control" />
                </div>
            </div>

            <div class="row ml-10 mt-20">
                <div class="col-12 col-md-4 pl-10 mb-20">
                    <label for="cek_imb" class="form-label">IMB
                    </label>
                    <select name="cek_imb" class="form-control custom-select" id="cek_imb">
                        <option value="1" {{ !empty($pengajuan->no_imb) ? 'selected' : '' }}>Ada</option>
                        <option value="2" {{ empty($pengajuan->no_imb) ? 'selected' : '' }}>Tidak Ada</option>
                    </select>
                </div>

                <div class="col-12 col-md-4 pl-10 mb-20" id="div_no_imb">
                    <label for="no_imb" class="form-label">Nomor IMB
                    </label>
                    <input type="text" name="no_imb" id="no_imb" value="{{ $pengajuan->no_imb }}"
                        placeholder="ketik disini" class="form-control" />
                </div>


                <div class="col-12 col-md-4 pl-10 mb-20" id="div_tanggal_imb">
                    <label for="tanggal_terbit_imb" class="form-label">Tanggal
                        Terbit IMB
                    </label>
                    <input type="date" class="form-control bg-white" id="tanggal_terbit_imb" data-min-date="today"
                        data-date-format="d-m-Y" name="tanggal_terbit_imb" value="{{ $pengajuan->tanggal_terbit_imb }}"
                        placeholder="dd-mm-YYYY" data-allow-input="false">
                </div>
            </div>

            @if ($pengajuan->status < 3) <div class="row ml-10 mt-20">
                <div class="col-12 text-right">
                    <button type="button" class="btn btn-success text-left" href="#" id="updatePengajuanKonstruksi"><i
                            class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                </div>
        </div>
        @endif
        </div>
    </form>
</div>

<!-- START: Modal Confirmation -->
<div class="modal fade" id="modal_konfirmasi" tabindex="-1" role="dialog" aria-labelledby="modal-popin"
    aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-popin" role="document" style="max-width: 50%;">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Simpan Data</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <p>Anda yakin ingin menyimpan data ini ?</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cek Lagi</button>
                <button type="button" id="btn_simpan_konfirmasi" class="btn btn-alt-success"
                    data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- END: Modal Confirmation -->
@push('pengajuan-scripts')
<script>
    const formatRupiah = (angka, prefix) => {
        let number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

    $(function() {

        var count_dana_non_rumah_lain = "{{ $count_dana_non_rumah_lain }}";
        var x = (count_dana_non_rumah_lain > 0) ? count_dana_non_rumah_lain : 1;

        load_data_non_rumah_lain();

        var status_bank_non_rumah_lain = "{{ $status_bank_non_rumah_lain }}";
        var pengajuan_id = "{{ $pengajuan->pengajuan_id }}";
        var pengajuan_provinsi = "{{ $pengajuan->provinsi }}";
        var pengajuan_kota = "{{ $pengajuan->kota }}";
        var pengajuan_kecamatan = "{{ $pengajuan->kecamatan }}";
        var pengajuan_kelurahan = "{{ $pengajuan->kelurahan }}";
        var status_kawin = "{{ $status_kawin }}";
        switch (status_kawin) {
            case '1':
                $(".div_pasangan").removeClass("d-none");
                $("#div_buku_nikah").removeClass("d-none");
                break;
            case '2':
                $(".div_pasangan").addClass("d-none");
                $("#div_buku_nikah").addClass("d-none");
                break;
            case '3':
                $(".div_pasangan").addClass("d-none");
                $("#div_buku_nikah").addClass("d-none");
                $("#div_akta_cerai").removeClass("d-none");
                break;
        }

        $("#kantor_penerbit").select2({ theme:"bootstrap"});
        
        $(".numberOnly").on("input", function() {
            this.value = this.value.replace(/[^0-9]/g, "");
        });

        $(".no-zero").on("input", function() {
            let thisValue = $(this).val()
            if (thisValue.charAt(0) == '0') {
                $(this).val(thisValue.substring(1))
            }
        });

        $("#form_pengajuan_pendanaan_konstruksi").validate({
            onfocusout: false,
            invalidHandler: function(e, validator) {
                if (validator.errorList.length) {
                    validator.errorList[0].element.focus()
                }
            }
        });


        if ($("#jenis_agunan option:selected").val() == '1') {
            $("#div_tanggal_jatuh_tempo").addClass('d-none');
        } else {
            $("#div_tanggal_jatuh_tempo").removeClass('d-none');
        }


        if ($("#cek_imb option:selected").val() == '1') {
            $("#div_no_imb").removeClass('d-none');
            $("#div_tanggal_imb").removeClass('d-none');
            $("#div_dokumen_imb").removeClass('d-none');

            $("#no_imb").prop('required', 'true');
            $("#tanggal_terbit_imb").prop('required', 'true');
        } else {
            $("#div_no_imb").addClass('d-none');
            $("#div_tanggal_imb").addClass('d-none');
            $("#div_dokumen_imb").addClass('d-none');

            $("#no_imb").prop('required', 'false');
            $("#tanggal_terbit_imb").prop('required', 'false');

            $("#no_imb").val('');
            $("#tanggal_terbit_imb").val('');
        }

        $("#jenis_agunan").change(function(e) {
            if ($(this).val() == '2') {
                $("#div_tanggal_jatuh_tempo").removeClass('d-none');
                $("#tanggal_jatuh_tempo").prop('required', 'true');
            } else {
                $("#div_tanggal_jatuh_tempo").addClass('d-none');
                $("#tanggal_jatuh_tempo").prop('required', 'false');
            }

        });



        $("#status_bank").change(function(e) {
            if ($(this).val() == "1") {
                $('#table_dana_non_rumah_lain').removeClass('d-none');
                (status_bank_non_rumah_lain == "2") ? $('#table_dana_non_rumah_lain > tbody').append(
                    buildRowFirstNonRumahLain()): '';
            } else {
                $('#table_dana_non_rumah_lain').addClass('d-none');

                (status_bank_non_rumah_lain == "2") ? $('#table_dana_non_rumah_lain > tbody').html(''):
                    '';
            }

        });


        $(document).on('click', '#add_dana_non_rumahlain', function(e) {
            e.preventDefault();
            var buildRowNonRumah = "";
            x++;
            buildRowNonRumah += '<tr id="row' + x + '">';
            buildRowNonRumah +=
                '<td><input type="text" name="bank_non_rumah_lain[]" id="bank_non_rumah_lain_' + x +
                '"  placeholder="ketik disini" class="form-control" maxlength="30" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="plafond_non_rumah_lain[]" id="plafond_non_rumah_lain_' +
                x +
                '"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value, ' + "Rp" +');" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="jangkawaktu_non_rumah_lain[]" id="jangkawaktu_non_rumah_lain_' +
                x + '"  placeholder="ketik disini" class="form-control" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="outstanding_non_rumah_lain[]" id="outstanding_non_rumah_lain_' +
                x +
                '"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="angsuran_non_rumah_lain[]" id="angsuran_non_rumah_lain_' +
                x +
                '"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah += '<td> <button type="button" name="remove" id="' + x +
                '" class="btn btn-danger btn_remove_non_rumah_lain"><i class="fa fa-trash"></i></button></td></tr>';
            $('#table_dana_non_rumah_lain > tbody').append(buildRowNonRumah);
        });

        $(document).on('click', '.btn_remove_non_rumah_lain', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
            x--;
        });



        $("#cek_imb").change(function(e) {
            if ($(this).val() == '2') {
                $("#div_no_imb").addClass('d-none');
                $("#div_tanggal_imb").addClass('d-none');
                $("#div_dokumen_imb").addClass('d-none');

                $("#no_imb").val('');
                $("#tanggal_terbit_imb").val('');
            } else {
                $("#div_no_imb").removeClass('d-none');
                $("#div_tanggal_imb").removeClass('d-none');
                $("#div_dokumen_imb").removeClass('d-none');
            }

        });
        
        $("#updatePengajuanKonstruksi").click(function() {
            if ($("#form_pengajuan_pendanaan_konstruksi").valid()) {
                $('#modal_konfirmasi').modal('show');
            }
        });

        $('#btn_simpan_konfirmasi').click(function() {
            $("#form_pengajuan_pendanaan_konstruksi").trigger("submit");
        });

        function buildRowFirstNonRumahLain() {
            var buildRowNonRumah = '';
            buildRowNonRumah += '<tr id="row1">';
            buildRowNonRumah +=
                '<td><input type="text" name="bank_non_rumah_lain[]" id="bank_non_rumah_lain_1" placeholder="ketik disini" class="form-control" maxlength="30" required/></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="plafond_non_rumah_lain[]" id="plafond_non_rumah_lain_1"  placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="jangkawaktu_non_rumah_lain[]" id="jangkawaktu_non_rumah_lain_1" placeholder="ketik disini" class="form-control" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="outstanding_non_rumah_lain[]" id="outstanding_non_rumah_lain_1" placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                '<td><input type="text" name="angsuran_non_rumah_lain[]" id="angsuran_non_rumah_lain_1" placeholder="ketik disini" class="form-control" onkeyup="this.value = formatRupiah(this.value);" /></td>';
            buildRowNonRumah +=
                ' <td><button type="button" class="btn btn-primary" id="add_dana_non_rumahlain"><i class="fa fa-plus"></i></button></td>';
            buildRowNonRumah += '</tr>';
            return buildRowNonRumah;
        }

        function load_data_non_rumah_lain() {
            $.ajax({
                url: "{{ route('borrower.danarumah.nonrumahlain') }}",
                type: "post",
                dataType: "html",
                data: {
                    _token: "{{ csrf_token() }}",
                    pengajuan_id: "{{ $pengajuan->pengajuan_id }}"
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText

                },
                success: function(result) {
                    if (result) {
                        $('#table_dana_non_rumah_lain').removeClass('d-none');
                        $('#table_dana_non_rumah_lain > tbody').html(result);
                    }

                }
            });

        }


        $("form#form_pengajuan_pendanaan_konstruksi").submit(function(e) {
            e.preventDefault();

            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                beforeSend: () => {
                    swal.fire({
                        html: '<h5>Menyimpan Data ...</h5>',
                        showConfirmButton: false,
                        allowOutsideClick: () => false,
                        onBeforeOpen: () => {
                            swal.showLoading();
                        }
                    });
                },
                success: function(data) {

                    if (data.status == 'success') {

                        swal.fire({
                            title: "Proses Berhasil",
                            text: "Simpan Data Berhasil",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonClass: "btn-success",
                        }).then((result) => {
                            $('.nav-tabs a[href="#dokumen-pendanaan"]').tab('show');
                        });
                    } else {
                        let error_msg = data.message
                        console.log(error_msg)
                        swal.fire({
                            title: "Proses Gagal",
                            text: "Simpan Data Gagal",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonClass: "btn-danger",
                        }).then((result) => {

                        })
                    }
                },
                error: function(data) {
                    // console.log(data)
                    swal.fire({
                        title: "Proses Gagal",
                        type: "error",
                        text: data,
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                    })
                },
            })
        });

    });
</script>

<script>
    $(document).ready(function() {
        $(`#in_provinsi_obj_pendanaan, #in_provinsi_pemilik_obj, 
        #in_kota_obj_pendanaan, #in_kota_pemilik_obj, 
        #in_kecamatan_obj_pendanaan, #in_kecamatan_pemilik_obj,
        #in_kelurahan_obj_pendanaan, #in_kelurahan_pemilik_obj
        `).prepend('<option selected></option>').select2({
            theme: "bootstrap",
            placeholder: "-- Pilih Satu --",
            allowClear: true,
            width: '100%'
        });

        $.getJSON("/borrower/data_provinsi/", function(data){
            for(let i = 0; i<data.length; i++){
                $('#in_provinsi_obj_pendanaan, #in_provinsi_pemilik_obj').append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        })

  
    })

    const provinsiChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kota/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kotaChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kecamatan/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kecamatanChange = (thisValue, thisId, nextId) => {
        $('#'+nextId).empty(); // set null
        $('#'+nextId).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kelurahan/"+thisValue+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                $('#'+nextId).append($('<option>', {
                    value: data[i].text,
                    text : data[i].text
                }));
            }
        });
    }

    const kelurahanChange = (thisValue, thisId, nextId,vkec,vkota,vprov) => {
        $.getJSON("/borrower/data_kode_pos_v2/"+thisValue+"/"+vkec+"/"+vkota+"/"+vprov, function(data){
            $('#'+nextId).val(data[0].text)
            $('#kode_pos').val(data[0].text)
        });
    }

    // Load data from db
    const getProvinsiData = (data_provinsi, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_provinsi/", function(data){
            for(let i = 0; i<data.length; i++){
                let option = $(`<option ${data[i].text == data_provinsi ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        })
    }

    const getKotaData = (data_provinsi, this_data_kota, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kota/"+data_provinsi+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                let option = $(`<option ${data[i].text == this_data_kota ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        });
    }

    const getKecamatan = (data_kota, this_data_kecamatan, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kecamatan/"+data_kota+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
                let option = $(`<option ${data[i].text == this_data_kecamatan ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        });
    }

    const getKelurahanData = (data_kecamatan, this_data_kelurahan, element_id) => {
        $('#'+element_id).empty(); // set null
        $('#'+element_id).append($('<option>', { value: '', text : '-- Pilih Satu --'}));
        $.getJSON("/borrower/data_kelurahan/"+data_kecamatan+"/", function(data){
            for(let i = 0; i<data.length; i++){ 
               let option = $(`<option ${data[i].text == this_data_kelurahan ? selected='selected' : ''}></option>`).val(data[i].text).text(data[i].text)
                $("#"+element_id).append(option);
            }
        });
    }

    let provinsi_obj_pendanaan = "{{ $pengajuan->provinsi }}";
    let kota_obj_pendanaan = "{{ $pengajuan->kota }}";
    let kecamatan_obj_pendanaan = "{{ $pengajuan->kecamatan }}";
    let kelurahan_obj_pendanaan = "{{ $pengajuan->kelurahan }}";
    
    let provinsi_pemilik_obj_pendanaan = "{{ $pengajuan->provinsi_pemilik }}";
    let kota_pemilik_obj_pendanaan = "{{ $pengajuan->kota_pemilik }}";
    let kecamatan_pemilik_obj_pendanaan = "{{ $pengajuan->kecamatan_pemilik }}";
    let kelurahan_pemilik_obj_pendanaan = "{{ $pengajuan->kelurahan_pemilik }}";
    
    getProvinsiData(provinsi_obj_pendanaan, 'in_provinsi_obj_pendanaan')
    getKotaData(provinsi_obj_pendanaan, kota_obj_pendanaan, 'in_kota_obj_pendanaan')
    getKecamatan(kota_obj_pendanaan, kecamatan_obj_pendanaan, 'in_kecamatan_obj_pendanaan')
    getKelurahanData(kecamatan_obj_pendanaan, kelurahan_obj_pendanaan, 'in_kelurahan_obj_pendanaan')
    
    getProvinsiData(provinsi_pemilik_obj_pendanaan, 'in_provinsi_pemilik_obj')
    getKotaData(provinsi_pemilik_obj_pendanaan, kota_pemilik_obj_pendanaan, 'in_kota_pemilik_obj')
    getKecamatan(kota_pemilik_obj_pendanaan, kecamatan_pemilik_obj_pendanaan, 'in_kecamatan_pemilik_obj')
    getKelurahanData(kecamatan_pemilik_obj_pendanaan, kelurahan_pemilik_obj_pendanaan, 'in_kelurahan_pemilik_obj')
    
    getProvinsiData(provinsi_obj_pendanaan, 'provinsi')
    getKotaData(provinsi_obj_pendanaan, kota_obj_pendanaan, 'kota')
    getKecamatan(kota_obj_pendanaan, kecamatan_obj_pendanaan, 'kecamatan')
    getKelurahanData(kecamatan_obj_pendanaan, kelurahan_obj_pendanaan, 'kelurahan')
    function populate_data_kota(pengajuan_provinsi) {

        $.getJSON("/borrower/data_kota/" + pengajuan_provinsi + "/", function(data_kota) {
            for (var i = 0; i < data_kota.length; i++) {
                $('#kota').append($('<option>', {
                    value: data_kota[i].text,
                    text: data_kota[i].text
                }));
            }

            $("#kota option[value='" + pengajuan_kota + "']").attr('selected', 'selected');
        });

    }

    function populate_data_kecamatan(pengajuan_kota) {

        $.getJSON("/borrower/data_kecamatan/" + pengajuan_kota + "/", function(data_kecamatan) {
            for (var i = 0; i < data_kecamatan.length; i++) {
                $('#kecamatan').append($('<option>', {
                    value: data_kecamatan[i].text,
                    text: data_kecamatan[i].text
                }));
            }

            $("#kecamatan option[value='" + pengajuan_kecamatan + "']").attr('selected',
                'selected');
        });

    }

    function populate_data_kelurahan(pengajuan_kecamatan) {

        $.getJSON("/borrower/data_kelurahan/" + pengajuan_kecamatan + "/", function(data_kelurahan) {
            for (var i = 0; i < data_kelurahan.length; i++) {
                $('#kelurahan').append($('<option>', {
                    value: data_kelurahan[i].text,
                    text: data_kelurahan[i].text
                }));
            }

            $("#kelurahan option[value='" + pengajuan_kelurahan + "']").attr('selected',
                'selected');
        });

    }
</script>
@endpush
