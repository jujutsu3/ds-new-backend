<style>
    thead {font-weight: bold;}
    h1   {color: blue;}
    .warna    {background-color: #98FB98;}
</style>
<table>
    <thead>
        <tr>
            <th rowspan='3' style="background-color:#98FB98;text-align:center;border:1;"><b>External ID</b></th>
            <th rowspan='3' style="background-color:#98FB98;text-align:center;"><b>Amount</b></th>
            <th rowspan='3' style="background-color:#98FB98;text-align:center;"><b>Purpose Code</b></th>
            <th rowspan='3' style="background-color:#98FB98;text-align:center;"><b>Description</b></th>
            <th colspan="4" style="background-color:#AFEEEE;text-align:center;"><b>Sender</b></th>
            <th rowspan='3' style="background-color:#AFEEEE;text-align:center;"><b>DOB</b></th>
            <th colspan="11" style="background-color:#FFEFD5;text-align:center;"><b>Recipient</b></th>
        </tr>
        <tr>
            <th rowspan='2' style="background-color:#AFEEEE;text-align:center;"><b>Business / Individual</b></th>
            <th rowspan='2' style="background-color:#AFEEEE;text-align:center;"><b>Name</b></th>
            <th colspan='2' style="background-color:#AFEEEE;text-align:center;"><b>Address</b></th>
            <th rowspan='2' style="background-color:#FFEFD5;text-align:center;"><b>Business / Individual</b></th>
            <th rowspan='2' style="background-color:#FFEFD5;text-align:center;"><b>Name</b></th>
            <th colspan='2' style="background-color:#FFEFD5;text-align:center;"><b>Address</b></th>
            <th colspan='3' style="background-color:#FFEFD5;text-align:center;"><b>Account Details</b></th>
            <th rowspan='2' style="background-color:#FFEFD5;text-align:center;"><b>Email</b></th>
            <th rowspan='2' style="background-color:#FFEFD5;text-align:center;"><b>CC</b></th>
            <th rowspan='2' style="background-color:#FFEFD5;text-align:center;"><b>Mobile Number</b></th>
            <th rowspan='2' style="background-color:#FFEFD5;text-align:center;"><b>Phone Number</b></th>
        </tr>
        <tr>
            <th style="background-color:#AFEEEE;text-align:center;"><b>Country Code</b></th>
            <th style="background-color:#AFEEEE;text-align:center;"><b>City</b></th>
            <th style="background-color:#FFEFD5;text-align:center;"><b>Country Code</b></th>
            <th style="background-color:#FFEFD5;text-align:center;"><b>City</b></th>
            <th style="background-color:#FFEFD5;text-align:center;"><b>Account Code</b></th>
            <th style="background-color:#FFEFD5;text-align:center;"><b>Account Number</b></th>
            <th style="background-color:#FFEFD5;text-align:center;"><b>Account Holder Name</b></th>

        </tr>
    </thead>

    <tbody>
        @foreach($dataExport as $data)
        <tr>
            <td>{{$data['external_id']}}</td>
            <td>{{$data['amount']}}</td>
            <td>{{$data['purpose_code']}}</td>
            <td>{{$data['description']}}</td>
            <td>{{$data['bussiness_individual_sender']}}</td>
            <td>{{$data['name_sender']}}</td>
            <td>{{$data['country_code_sender']}}</td>
            <td>{{$data['city_sender']}}</td>
            <td>{{$data['dob_sender']}}</td>
            <td>{{$data['bussiness_individual_recipient']}}</td>
            <td>{{$data['name_recipient']}}</td>
            <td>{{$data['country_code_recipient']}}</td>
            <td>{{$data['city_recipient']}}</td>
            <td>{{$data['account_code_recipient']}}</td>
            <td>{{$data['account_number_recipient']}}</td>
            <td>{{$data['account_holder_name_recipient']}}</td>
            <td>{{$data['email_recipient']}}</td>
            <td>{{$data['cc_recipient']}}</td>
            <td>{{$data['mobile_number_recipient']}}</td>
            <td>{{$data['phone_number_recipient']}}</td>
        </tr>
        @endforeach
    </tbody>
</table>