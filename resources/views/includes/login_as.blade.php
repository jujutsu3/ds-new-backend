@include('component.modal_login_as')
@component('component.modal_login')
@slot('modal_id') loginModalAs @endslot

@slot('modal_content')

<div class="panel-heading">
    <div class="row">
        <div class="col-12">
            <ul class=" nav nav-pills nav-fill">
                <li class="nav-item">
                    <a href="#" data-toggle="modal" data-target="#modal_login_investor" data-dismiss="modal"
                        aria-label="Close" class="nav-link active mx-1 my-1">PENDANA</a>
                </li>

                <li class="nav-item">
                    <a href="#" data-toggle="modal" data-target="#modal_login_borrower" data-dismiss="modal"
                        aria-label="Close" class="nav-link active mx-1 my-1">PENERIMA PENDANAAN</a>
                </li>
            </ul>
        </div>
    </div>
    <hr>
</div>
@endslot
@endcomponent

@component('component.modal_login')
@slot('modal_id') registerModalAs @endslot

@slot('modal_content')

<div class="panel-heading">
    <div class="row">
        <div class="col-12">
            <ul class=" nav nav-pills nav-fill">
                <li class="nav-item">
                    <a href="#" data-toggle="modal" data-target="#modal_register_investor" data-dismiss="modal"
                        aria-label="Close" class="nav-link active mx-1 my-1">PENDANA</a>
                </li>

                <li class="nav-item">
                    <a href="#" data-toggle="modal" data-target="#modal_register_borrower" class="nav-link active mx-1 my-1"
                        id="register-form-link" onclick="$('#registerModalAs').modal('hide')">PENERIMA PENDANAAN</a>
                </li>
            </ul>
        </div>
    </div>
    <hr>
</div>
@endslot
@endcomponent