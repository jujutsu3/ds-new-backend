<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

{{-- <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" > --}}
<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" >

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="/css/style.css">
<link rel="stylesheet" href="/admin/assets/css/lib/datatable/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="{{url('assetsBorrower/js/plugins/sweetalert2/sweetalert2.min.css')}}">


<!-- <link rel="stylesheet" href="/css/open-iconic-bootstrap.css"> -->

<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->

<!-- External CSS libraries -->
<!-- <link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="/css/magnific-popup.css">
    <link type="text/css" rel="stylesheet" href="/css/jquery.selectBox.css">
    <link type="text/css" rel="stylesheet" href="/css/dropzone.css">
    <link type="text/css" rel="stylesheet" href="/css/rangeslider.css">
    <link type="text/css" rel="stylesheet" href="/css/animate.min.css">
    <link type="text/css" rel="stylesheet" href="/css/leaflet.css">
    <link type="text/css" rel="stylesheet" href="/css/map.css">
    <link type="text/css" rel="stylesheet" href="/css/jquery.mCustomScrollbar.css">-->
<!-- <link type="text/css" rel="stylesheet" href="/fonts/font-awesome/css/font-awesome.min.css"> -->
<!-- <link type="text/css" rel="stylesheet" href="/fonts/flaticon/font/flaticon.css"> -->

<!-- Custom Stylesheet -->
<link type="text/css" rel="stylesheet" href="/css/style2.css">
<link rel="stylesheet" type="text/css" href="/css/skins/white.css">

<link type="text/css" rel="stylesheet" href="/css/jquery.selectBox.css">
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

<script src="/js/jquery-3.3.1.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> -->
<!--script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="/js/jquery.selectBox.js"></script>
<script src="{{url('assetsBorrower/js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
{{-- <script src="/js/dropzone.js"></script>
 --}}
<!-- Global site tag (gtag.js) - AdWords: 1008615673 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1008615673"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'AW-1008615673');
</script>

<!-- end of global site tag -->

<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '444018075722130');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=444018075722130&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->

<!-- Matomo 
<script type="text/javascript">
    var _paq = window._paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u = "//analytics.danasyariah.id/";
        _paq.push(['setTrackerUrl', u + 'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d = document,
            g = d.createElement('script'),
            s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.async = true;
        g.src = u + 'matomo.js';
        s.parentNode.insertBefore(g, s);
    })();
</script>
<!-- End Matomo Code -->

<!-- add aut logout after 2 minutes ARL -->
<script type="text/javascript" language="javascript">
    var idleMax = 2; // Logout after 2 minutes of IDLE
    var idleTime = 0;

    var idleInterval = setInterval("timerIncrement()", 120000); // 2 minute interval
    $("body").mousemove(function(event) {
        idleTime = 0; // reset to zero
    });

    // count minutes
    function timerIncrement() {
        idleTime = idleTime + 1;
        if (idleTime > idleMax) {
            window.location = "/logout";
        }
    }
</script>

<!-- Google Tag Manager -->
<script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-N6RKMM6');
</script>
<!-- End Google Tag Manager -->