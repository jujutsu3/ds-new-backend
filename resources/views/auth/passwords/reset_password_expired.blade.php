@extends('layouts.guest.master')

@section('navbar')
{{-- @include('includes.navbar2') --}}
@endsection

@section('body')
<br><br>

<div class="container">
    <div class="row justify-content-center">
        <div class="alert alert-warning alert-dismissable fade show" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            Kata Sandi anda harus diubah karena sudah digunakan selama 180 hari.
        </div>
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header">{{ __('Reset Kata Sandi') }}</div>

                <div class="card-body">
                    @if($user_from == 'investor')
                        <form method="POST" action="{{ route('updatePasswordExpiredInvestor') }}" aria-label="{{ __('Reset Password') }}">
                    @else
                        <form method="POST" action="{{ route('updatePasswordExpiredBorrower') }}" aria-label="{{ __('Reset Password') }}">
                    @endif
                        @csrf

                        <input type="hidden" name="user_from" value="{{ $user_from }}">

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Kata Sandi') }}</label>

                            <div class="col-md-6">
                                <input type="password" id="NEW_PASSWORD" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  placeholder="Kata Sandi Baru" required>

                                <!-- <input id="NEW_PASSWORD" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required> -->

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Konfirmasi Kata Sandi') }}</label>

                            <div class="col-md-6">
                                <input id="NEW_PASSWORD_CONFIRMATION" type="password" class="form-control" name="password_confirmation" onkeyup="cek_confirm_reset_all()" required>
                                <span id="error_confirm_password_reset" style="color:red;font-size:11px;margin-left:15px"></span>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success" id="submit">
                                    {{ __('Reset Kata Sandi') }}
                                </button>
                            </div>
                        </div>
                        <br><br>
                        <span id="8char_reset" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Minimal 8 Karakter
                        <input type="hidden" id = "char_reset">
                        <span id="ucase_reset" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Huruf Besar
                        <input type="hidden" id = "upper_reset">
                        <span id="lcase_reset" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Huruf Kecil
                        <input type="hidden" id = "lower_reset">
                        <span id="num_reset" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Karakter Angka
                        <input type="hidden" id = "int_reset">
                        <span id="special_character_reset" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Karakter Spesial
                        <input type="hidden" id = "special_char_reset">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br><br>

<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/jquery_step/jquery.steps.js"></script>
<script src="/js/jquery_step/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.1.2/bootstrap-show-password.js"></script>

<script type="text/javascript">

    $(document).on("keyup", "input[type=password]", function(){
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");
            var special_character = new RegExp("[#?!@$%^&*-]+");


            var thru = false;
            // console.log($("#NEW_PASSWORD").val());
            $('.allowCharacter').on('input', function (event) { 
                this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
            });

            if($("#NEW_PASSWORD").val().length >= 8){
                $("#8char_reset").removeClass("fa fa-times");
                $("#8char_reset").addClass("fa fa-check");
                $("#8char_reset").css("color","#00A41E");
                $("#char_reset").val(1);
            }else{
                $("#8char_reset").removeClass("fa fa-check");
                $("#8char_reset").addClass("fa fa-times");
                $("#8char_reset").css("color","#FF0004");
                $("#char_reset").val(0);
            }
            
            if(ucase.test($("#NEW_PASSWORD").val())){
                $("#ucase_reset").removeClass("fa fa-times");                
                $("#ucase_reset").addClass("fa fa-check");
                $("#ucase_reset").css("color","#00A41E");
                $("#upper_reset").val(1);
            }else{
                $("#ucase_reset").removeClass("fa fa-check");
                $("#ucase_reset").addClass("fa fa-times");
                $("#ucase_reset").css("color","#FF0004");
                $("#upper_reset").val(0);
            }
            
            if(lcase.test($("#NEW_PASSWORD").val())){
                $("#lcase_reset").removeClass("fa fa-times");
                $("#lcase_reset").addClass("fa fa-check");
                $("#lcase_reset").css("color","#00A41E");
                $("#lower_reset").val(1);
            }else{
                $("#lcase_reset").removeClass("fa fa-check");
                $("#lcase_reset").addClass("fa fa-times");
                $("#lcase_reset").css("color","#FF0004");
                $("#lower_reset").val(0);
            }
             
            if(num.test($("#NEW_PASSWORD").val())){
                $("#num_reset").removeClass("fa fa-times");
                $("#num_reset").addClass("fa fa-check");
                $("#num_reset").css("color","#00A41E");
                $("#int_reset").val(1);
            }else{
                $("#num_reset").removeClass("fa fa-check");
                $("#num_reset").addClass("fa fa-times");
                $("#num_reset").css("color","#FF0004");
                $("#int_reset").val(0);
            }

            if(special_character.test($("#NEW_PASSWORD").val())){
                $("#special_character_reset").removeClass("fa fa-times");
                $("#special_character_reset").addClass("fa fa-check");
                $("#special_character_reset").css("color","#00A41E");
                $("#special_char_reset").val(1);
            }else{
                $("#special_character_reset").removeClass("fa fa-check");
                $("#special_character_reset").addClass("fa fa-times");
                $("#special_character_reset").css("color","#FF0004");
                $("#special_char_reset").val(0);
            }

            if (thru = true)
            {
                if($("#NEW_PASSWORD").val() == $("#NEW_PASSWORD_CONFIRMATION").val() && $("#int_reset").val()== 1 && $("#lower_reset").val()== 1 && $("#upper_reset").val()== 1 && $("#char_reset").val()== 1 && $("#special_char_reset").val()== 1)
                {    
                    document.getElementById("submit").disabled = false; 
                }
                else{
                    document.getElementById("submit").disabled = true; 
                }    
            }
            else
            {
                document.getElementById("submit").disabled = true;      
            }
            
    })

    function cek_confirm_reset_all()
    {
        if($("#NEW_PASSWORD_CONFIRMATION").val() != $("#NEW_PASSWORD").val())
        {
            $('#error_confirm_password_reset').html('<b id="confirm_password_error_reset">Konfirmasi kata sandi tidak sesuai dengan kata sandi baru.</b>');
        }
        else{
            $('#confirm_password_error_reset').hide();
        }

    }

</script>
@endsection
