@extends('layouts.guest.master')

@section('navbar')
@include('includes.navbar2')
@endsection

@section('body')
<style>
    .img-forgot {
        width: 60%;
    }

    @media only screen and (max-width: 600px) {
        .img-forgot {
            width: 100%;
        }
    }

    @media only screen and (max-width: 768px) {
        .img-forgot {
            width: 80%;
        }
    }
</style>
<div class="container pt-5">
    <div class="row justify-content-center pt-4">
        <div class="col-8 pb-4">
            <h2 class="text-center pt-5">Lupa Kata Sandi</h2>
        </div>
        <div class="col-md-8">
            <div class="team-wrapper ">
                <img class="pb-3 img-responsive img-forgot" src="/img/forgotpassword.png" alt="Pendanaan Halal">

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{-- session('status') --}}
                        <div class="alert alert-success" role="alert">
                            Link atur ulang kata sandi sudah dikirim ke email <b>{{ session('email') }}</b>, silakan cek email Anda untuk mengatur ulang kata sandi. <b>Belum menerima email? <a href="#" id="btngayakin" class="text-danger">kirim ulang</a></b>
                            <input type="hidden" id="response" value="2">
                        </div>
                    </div>
                    @endif

                    @if ($errors->has('email'))
                    {{-- email benar tetapi tidak ditemukan --}}
                    @if ($errors->first('email') == 'passwords.user')
                    <div class="alert alert-success" role="alert">
                        Email tidak terdaftar, silahkan masukan email yang valid
                    </div>
                    @elseif ($errors->first('email') == 'validation.email')
                    <div class="alert alert-success" role="alert">
                        Email tidak sesuai, masukan alamat email dengan format yang benar misal: <i>example@domainemail.com</i>
                    </div>
                    @endif
                    @endif

                    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}" id="form-send-reset-link">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12 ">
                                <!-- <input id="txt_email" type="email" class="form-control " name="email" placeholder="Masukkan Alamat Email Anda..." value="" required> -->
                                <input type="hidden" name="user_type" value="{{ $user_type }}" required>
                                <input id="email" type="email" class="form-control frmEmail" name="email"
                                    placeholder="Masukkan Alamat Email Anda..." value="{{ session('email') }}" required
                                    autofocus>

                                {{-- @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                         <strong>{{ $errors->first('email') }}</strong>
                                <!-- <strong>Email Tidak Terdaftar</strong>  -->
                                </span>
                                @endif --}}
                            </div>
                            <div class="col-md-12 pt-4 ">
                                <button type="submit" id="btnsbt" class="btn btn-success btn-block btn-danaSyariah">
                                    {{ __('Lanjut') }}
                                </button>
                                <button type="button" hidden="hidden" onclick="toLandingPage('/')" id="btnToLandingPage"
                                    class="btn btn-success btn-block btn-danaSyariah">{{ __('OK') }}</button>
                                <span id="gayakin" role="alert">
                                    {{-- <a href='#' id='btngayakin'></a> --}}
                                </span>
                            </div>
                            <div class="col-12 pt-4">
                                <small id="passwordHelpBlock" class="form-text text-muted">
                                    *Link atur ulang kata sandi akan dikirimkan ke email Anda.
                                </small>
                                <small id="passwordHelpBlock" class="form-text text-muted pt-4">
                                    <a href="/"> <i class="fas fa-arrow-left pr-2"></i> Kembali ke halaman Beranda</a>
                                </small>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- modal notifikasi -->
    <div id="myModal" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Verifikasi Email</h5>
                </div>
                <form id="agree">
                    <div class="modal-body" id="modalBodyNotif">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success text-center" id="btnModal">Saya Setuju</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->
</div>
<br><br><br><br><br>
<script type="text/javascript">
    $(document).ready(function() {
        if ($('#response').val() == 2) {
            $('.frmEmail').attr('type', 'hidden');
            $('#btnToLandingPage').attr('hidden', false);
            $('#btnsbt').attr('hidden', true);
        }
    })
    $('#modalBodyNotif').append('');
    $('#btngayakin').on('click', function() {

        var email = "{{ session('email') }}";
        Swal.fire({
            title: '',
            text: `Apakah email Anda ${email} sudah benar dan terdaftar di Dana Syariah?`,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'Ya, sudah benar',
            confirmButtonText: 'Salah',
            showCloseButton: true,
            allowOutsideClick: false
        }).then((result) => {
            console.log(result)
            if (result.value) {
                location.reload();
            } else if(result.dismiss == 'cancel') {
                Swal.fire({
                    title: 'Notifikasi!',
                    text: 'Link Atur Ulang sudah terkirim ke Email Anda !',
                    type: 'success',
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        $('#btnsbt').click();
                    }
                })
            }
        })
        // $('#myModal').modal('show');
        // $('#modalBodyNotif').append('<p class="text-center" style="font-size:15px;">'+email+'</p>');
        // $('#modalBodyNotif').append('<p class="text-center" style="font-size:20px;">Apakah Email di Atas Benar ?</p>');
    })

    function toLandingPage(url) {
        document.location.href = url;
    }

    $('#form-send-reset-link').submit(function(e) {
        $('#btnsbt').text('').prepend('<i class="fa fa-spin fa-spinner"></i>').attr('disabled', true);
    })
</script>
@endsection
