@extends('layouts.guest.master')

@section('navbar')
@include('includes.navbar2')
@endsection

@section('body')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin-top: 150px">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if ($status == '01')
                        <div class="alert alert-success" role="alert">
                            Gagal Memperbaharui Kata sandi, Silahkan Coba Lagi !
                        </div>
                    @elseif($status == '02')
                        <div class="alert alert-danger" role="alert">
                            Kata sandi Baru Tidak Sama Dengan Konfirmasi kata sandi !
                        </div>
                    @elseif($status == '03')
                        <div class="alert alert-danger" role="alert">
                            Password Harus Lebih dari 8 Karakter
                        </div>
                    @endif

                    <form method="POST" action="{{ route('kirim.data') }}" aria-label="{{ __('Reset Password') }}">
                        @csrf
                        <input type="hidden" name="aidi" value="{{ $id }}">
                        <input type="hidden" name="email" value="{{ $email }}">


                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Alamat Email</label>

                            <div class="col-md-6">
                                <input name="email" id="email" type="email" class="form-control"  value="{{ $email }}" disabled>
                                <!-- @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Kata sandi Baru</label>

                            <div class="col-md-6">
                                <input id="password-baru" type="password" class="form-control" name="password_baru" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Konfirmasi Kata sandi</label>

                            <div class="col-md-6">
                                <input id="password-confirm_brw" type="password" class="form-control" name="password_confirmation" required onkeyup="cek_confirm_reset_brw()">
                                <span id="error_confirm_password_reset_brw" style="color:red;font-size:11px;margin-left:15px"></span>
                            </div>
                        </div>
                        <br>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button id="reset_button" type="submit" class="btn btn-success">
                                    {{ __('Atur Ulang Kata Sandi') }}
                                </button>
                            </div>
                        </div>
                        <br>
                        <span id="8char_reset_brw" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Minimal 8 Karakter
                        <input type="hidden" id = "char_reset_brw">
                        <span id="ucase_reset_brw" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Huruf Besar
                        <input type="hidden" id = "upper_reset_brw">
                        <span id="lcase_reset_brw" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Huruf Kecil
                        <input type="hidden" id = "lower_reset_brw">
                        <span id="num_reset_brw" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Karakter Angka
                        <input type="hidden" id = "int_reset_brw">
                        <span id="special_character_reset_brw" class="fa fa-times" style="color:#FF0004;"></span>&nbsp; Karakter Spesial
                        <input type="hidden" id = "special_char_reset_brw">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br><br>
<script type="text/javascript">

    $(document).on("keyup", "input[type=password]", function(){
            var ucase = new RegExp("[A-Z]+");
            var lcase = new RegExp("[a-z]+");
            var num = new RegExp("[0-9]+");
            var special_character = new RegExp("[#?!@$%^&*-]+");


            var thru = false;
            // console.log($("#NEW_PASSWORD").val());
            $('.allowCharacter').on('input', function (event) {
                this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
            });

            if($("#password-baru").val().length >= 8){
                $("#8char_reset_brw").removeClass("fa fa-times");
                $("#8char_reset_brw").addClass("fa fa-check");
                $("#8char_reset_brw").css("color","#00A41E");
                $("#char_reset_brw").val(1);
            }else{
                $("#8char_reset_brw").removeClass("fa fa-check");
                $("#8char_reset_brw").addClass("fa fa-times");
                $("#8char_reset_brw").css("color","#FF0004");
                $("#char_reset_brw").val(0);
            }

            if(ucase.test($("#password-baru").val())){
                $("#ucase_reset_brw").removeClass("fa fa-times");
                $("#ucase_reset_brw").addClass("fa fa-check");
                $("#ucase_reset_brw").css("color","#00A41E");
                $("#upper_reset_brw").val(1);
            }else{
                $("#ucase_reset_brw").removeClass("fa fa-check");
                $("#ucase_reset_brw").addClass("fa fa-times");
                $("#ucase_reset_brw").css("color","#FF0004");
                $("#upper_reset_brw").val(0);
            }

            if(lcase.test($("#password-baru").val())){
                $("#lcase_reset_brw").removeClass("fa fa-times");
                $("#lcase_reset_brw").addClass("fa fa-check");
                $("#lcase_reset_brw").css("color","#00A41E");
                $("#lower_reset_brw").val(1);
            }else{
                $("#lcase_reset_brw").removeClass("fa fa-check");
                $("#lcase_reset_brw").addClass("fa fa-times");
                $("#lcase_reset_brw").css("color","#FF0004");
                $("#lower_reset_brw").val(0);
            }

            if(num.test($("#password-baru").val())){
                $("#num_reset_brw").removeClass("fa fa-times");
                $("#num_reset_brw").addClass("fa fa-check");
                $("#num_reset_brw").css("color","#00A41E");
                $("#int_reset_brw").val(1);
            }else{
                $("#num_reset_brw").removeClass("fa fa-check");
                $("#num_reset_brw").addClass("fa fa-times");
                $("#num_reset_brw").css("color","#FF0004");
                $("#int_reset_brw").val(0);
            }

            if(special_character.test($("#password-baru").val())){
                $("#special_character_reset_brw").removeClass("fa fa-times");
                $("#special_character_reset_brw").addClass("fa fa-check");
                $("#special_character_reset_brw").css("color","#00A41E");
                $("#special_char_reset_brw").val(1);
            }else{
                $("#special_character_reset_brw").removeClass("fa fa-check");
                $("#special_character_reset_brw").addClass("fa fa-times");
                $("#special_character_reset_brw").css("color","#FF0004");
                $("#special_char_reset_brw").val(0);
            }

            if (thru = true)
            {
                if($("#password-baru").val() == $("#password-confirm_brw").val() && $("#int_reset_brw").val()== 1 && $("#lower_reset_brw").val()== 1 && $("#upper_reset_brw").val()== 1 && $("#char_reset_brw").val()== 1 && $("#special_char_reset_brw").val()== 1)
                {
                    document.getElementById("reset_button").disabled = false;
                }
                else{
                    document.getElementById("reset_button").disabled = true;
                }
            }
            else
            {
                document.getElementById("reset_button").disabled = true;
            }

    })

    function cek_confirm_reset_brw()
    {
        if($("#password-baru").val() !== $("#password-confirm_brw").val())
        {
            $('#error_confirm_password_reset_brw').html('<b id="confirm_password_error_reset_brw">Konfirmasi kata sandi tidak sesuai dengan kata sandi baru.</b>');
        }
        else{
            $('#confirm_password_error_reset_brw').hide();
        }

    }

</script>
@endsection
