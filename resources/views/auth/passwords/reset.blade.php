@extends('layouts.guest.master')

@section('navbar')
@include('includes.navbar2')
@endsection

@section('body')
<br><br>
<div class="container-fluid mt-5 mb-5">
    <div class="row justify-content-center">
        <div class="col-12">
            <p class="text-center h5 mb-4 font-weight-bold">Atur Ulang Kata Sandi</p>
        </div>
        <div class="col-md-8">
            <div class="card rounded pt-2">
                {{-- <div class="card-header">{{ __('Atur Ulang Kata Sandi') }}</div> --}}

                <div class="card-body">
                    <form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}" id="form-password">
                        @csrf

                        <input type="hidden" name="user_type" value="{{ $user_type }}" required>
                        <input type="hidden" id='token' name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email" class="my-auto h6">{{ __('Alamat Email') }}</label>

                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email }}" readonly quired autofocus>

                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    @if($errors->first('email') == 'passwords.token')
                                    {{-- <strong>link reset password telah kadaluarsa</strong> --}}
                                    <input type="hidden" id="response" value="3">
                                    @elseif($errors->first('email') == 'passwords.user')
                                    <strong>Email yang dimasukan Salah</strong>
                                    <input type="hidden" id="response" value="2">
                                    @elseif($errors->first('email') == 'validation.email')
                                    <strong>Pastikan Penulisan Email Sudah Benar</strong>
                                    <input type="hidden" id="response" value="2">
                                    @else
                                    <input type="hidden" id="response" value="1">
                                    @endif
                                </span>
                                @endif
                        </div>

                        <div class="form-group">
                            <label for="password" class="my-auto h6">{{ __('Kata Sandi') }}</label>

                            <input type="password" id="NEW_PASSWORD" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Kata Sandi Baru" required>

                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                            <label class="my-auto h6" for="password-confirm">{{ __('Konfirmasi Kata Sandi') }}</label>
                                <div class="input-group-append">
                                    <span id="error_confirm_password_reset" class="text-danger ml-2 my-auto" style="font-size:11px;"></span>
                                </div>
                            </div>
                            <input id="NEW_PASSWORD_CONFIRMATION" type="password" class="form-control mt-1" placeholder="konfirmasi kata sandi"
                                    name="password_confirmation" onkeyup="cek_confirm_reset()" required>
                        </div>

                        <div class="input-group mb-3">
                            <span class="h6 text-muted mr-2" style="font-size:13px"><i id="8char_reset" class="fa fa-times mr-1" style="color:#FF0004;"></i> 8 Karakter</span>
                            <input type="hidden" id="char_reset">

                            <span class="h6 text-muted mr-2" style="font-size:13px"><i id="ucase_reset" class="fa fa-times mr-1" style="color:#FF0004;"></i> Huruf Besar</span>
                            <input type="hidden" id="upper_reset">

                            <span class="h6 text-muted mr-2" style="font-size:13px"><i id="lcase_reset" class="fa fa-times mr-1" style="color:#FF0004;"></i> Huruf Kecil</span>
                            <input type="hidden" id="lower_reset">

                            <span class="h6 text-muted mr-2" style="font-size:13px"><i id="num_reset" class="fa fa-times mr-1" style="color:#FF0004;"></i> Karakter Angka</span>
                            <input type="hidden" id="int_reset">

                            <span class="h6 text-muted mr-2" style="font-size:13px"><i id="special_character_reset" class="fa fa-times mr-1" style="color:#FF0004;"></i> Karakter Spesial ( contoh :!@#$% ) </span>
                            <input type="hidden" id="special_char_reset">
                        </div>

                        {{-- <div class="d-flex justify-content-center"> --}}
                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn text-white btn-block btn-danaSyariah" id="submit">
                                {{ __('Simpan') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- modal notifikasi -->
    <div id="myModal" class="modal fade in" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="scrollmodalLabel">Notifikasi</h5>
                </div>
                <form id="agree">
                    <div class="modal-body pr-3 pl-3" id="modalBodyNotif">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success text-center" id="btnModal">Saya Setuju</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end modal -->
</div>

<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/jquery_step/jquery.steps.js"></script>
<script src="/js/jquery_step/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.1.2/bootstrap-show-password.js"></script>

@if (session('status') == 'passwords.reset')
    <script>
        $(document).ready(function() {
            $('#modalBodyNotif').append('<p class="text-center">Kata Sandi Berhasil Dirubah, Silahkan Login Kembali..</p>');
            $('#btnModal').html('OK');
            $('#myModal').modal({backdrop: 'static', keyboard: true, show: true});
        })
    </script>
@endif

<script type="text/javascript">
    $(document).ready(function() {
        $('#submit').attr('disabled', true);
        
        if ($('#response').val() == null) {
            console.log('teu aya uy');
        } else if ($('#response').val() == 3) {
            console.log($('#response').val());
            $('#myModal').modal({backdrop: 'static', keyboard: true, show: true});

            $('#modalBodyNotif').append('<p class="text-center">Link Atur Ulang Kata Sandi telah kadaluarsa, Silahkan klik Tombol dibawah untuk Kirim ulang Link Atur Ulang Kata Sandi Ke Email Anda..</p>');
            $('#btnModal').html('Lanjutkan');
            $('#btnModal').attr('value', '3');

        } else if ($('#response').val() == 1) {
            console.log($('#response').val());
            $('#myModal').modal({backdrop: 'static', keyboard: true, show: true});

            $('#modalBodyNotif').append('<p class="text-center">Kata Sandi Berhasil Dirubah, Silahkan Login Kembali..</p>');
            $('#btnModal').html('OK');
            $('#btnModal').attr('value', '1');
        }
    });

    $('#btnModal').on('click', function() {
        let user_type = "{{ $user_type }}"
        let modal_login = user_type == "users" ? 'modal_login_investor' : 'modal_login_borrower'
        if ($(this).val() == 3) {
            window.location.href = "{{route('index')}}/password/reset?user_type="+user_type;
        } else {
            window.location.href = "{{route('index')}}/#"+modal_login;
        }
    });

    $(document).on("keyup", "input[type=password]", function() {
        var ucase = new RegExp("[A-Z]+");
        var lcase = new RegExp("[a-z]+");
        var num = new RegExp("[0-9]+");
        var special_character = new RegExp("[#?!@$%^&*-]+");


        var thru = false;
        // console.log($("#NEW_PASSWORD").val());
        $('.allowCharacter').on('input', function(event) {
            this.value = this.value.replace(/[^a-zA-Z0-9 ]/g, '');
        });

        if ($("#NEW_PASSWORD").val().length >= 8) {
            $("#8char_reset").removeClass("fa fa-times");
            $("#8char_reset").addClass("fa fa-check");
            $("#8char_reset").css("color", "#00A41E");
            $("#char_reset").val(1);
        } else {
            $("#8char_reset").removeClass("fa fa-check");
            $("#8char_reset").addClass("fa fa-times");
            $("#8char_reset").css("color", "#FF0004");
            $("#char_reset").val(0);
        }

        if (ucase.test($("#NEW_PASSWORD").val())) {
            $("#ucase_reset").removeClass("fa fa-times");
            $("#ucase_reset").addClass("fa fa-check");
            $("#ucase_reset").css("color", "#00A41E");
            $("#upper_reset").val(1);
        } else {
            $("#ucase_reset").removeClass("fa fa-check");
            $("#ucase_reset").addClass("fa fa-times");
            $("#ucase_reset").css("color", "#FF0004");
            $("#upper_reset").val(0);
        }

        if (lcase.test($("#NEW_PASSWORD").val())) {
            $("#lcase_reset").removeClass("fa fa-times");
            $("#lcase_reset").addClass("fa fa-check");
            $("#lcase_reset").css("color", "#00A41E");
            $("#lower_reset").val(1);
        } else {
            $("#lcase_reset").removeClass("fa fa-check");
            $("#lcase_reset").addClass("fa fa-times");
            $("#lcase_reset").css("color", "#FF0004");
            $("#lower_reset").val(0);
        }

        if (num.test($("#NEW_PASSWORD").val())) {
            $("#num_reset").removeClass("fa fa-times");
            $("#num_reset").addClass("fa fa-check");
            $("#num_reset").css("color", "#00A41E");
            $("#int_reset").val(1);
        } else {
            $("#num_reset").removeClass("fa fa-check");
            $("#num_reset").addClass("fa fa-times");
            $("#num_reset").css("color", "#FF0004");
            $("#int_reset").val(0);
        }

        if (special_character.test($("#NEW_PASSWORD").val())) {
            $("#special_character_reset").removeClass("fa fa-times");
            $("#special_character_reset").addClass("fa fa-check");
            $("#special_character_reset").css("color", "#00A41E");
            $("#special_char_reset").val(1);
        } else {
            $("#special_character_reset").removeClass("fa fa-check");
            $("#special_character_reset").addClass("fa fa-times");
            $("#special_character_reset").css("color", "#FF0004");
            $("#special_char_reset").val(0);
        }

        if (thru = true) {
            if ($("#NEW_PASSWORD").val() == $("#NEW_PASSWORD_CONFIRMATION").val() && $("#int_reset").val() == 1 && $("#lower_reset").val() == 1 && $("#upper_reset").val() == 1 && $("#char_reset").val() == 1 && $("#special_char_reset").val() == 1) {
                document.getElementById("submit").disabled = false;
            } else {
                document.getElementById("submit").disabled = true;
            }
        } else {
            document.getElementById("submit").disabled = true;
        }

    })

    function cek_confirm_reset() {
        if ($("#NEW_PASSWORD_CONFIRMATION").val() != $("#NEW_PASSWORD").val()) {
            $('#error_confirm_password_reset').html('<b id="confirm_password_error_reset">- Konfirmasi kata sandi tidak sesuai dengan kata sandi baru.</b>');
        } else {
            $('#confirm_password_error_reset').hide();
        }

    }

    $('#form-password').submit(function(e) {
        $('#submit').text('').prepend('<i class="fa fa-spin fa-spinner"></i>').attr('disabled', true);
    })
</script>
@endsection