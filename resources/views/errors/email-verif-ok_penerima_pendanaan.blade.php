@extends('layouts.guest.master')

@section('navbar')
@include('includes.navbar2')
@endsection
@section('style')
<style>
    /* .golden_text h1, .golden_text p, .golden_text h3, .golden_text h5, .golden_text i, .golden_text a{
        color:#F1C411 !important;
    } */

    .white_text h1, .white_text i, .white_text h5 {
        color: #FFF !important;
    }

    .img-forgot{
        width: 60%;
    }
    @media only screen and (max-width: 600px) {
        .img-forgot{
            width: 100%;
        }
    }
    @media only screen and (max-width: 768px) {
        .img-forgot{
            width: 80%;
        }
    }
</style>
@endsection


@section('body')
<div class="sub-banner-2 pt-5">
    <div class="container pt-5">
        <div class="team-wrapper pt-5">
        <img class="pb-3 img-responsive img-forgot" src="/img/forgotpassword.png" alt="Pendanaan Halal">

        <h5><b><i>Assalamu'alaikum Warahmatullahi Wabarakatuh</i></b></h5>
        <br>
        <h3>Alhamdulillah, Anda telah melakukan <b><i>Verifikasi</i></b> akun.</h3>
        <br>

        @if (Helper::isMobile())  
            <h3>Silahkan pindah ke aplikasi Danasyariah untuk masuk/login</h3>
         @else 
            <h3>Silahkan pindah ke halaman utama untuk <a href="/#modal_login_borrower"> masuk/login </a></h3>
         @endif   

        <!--
            <h5><b><i>Assalamu'alaikum Warahmatullahi Wabarakatuh</i></b></h5>
            <br>
            <h3>Alhamdulillah, Anda telah melakukan <b><i>Verifikasi</i></b> akun, Terimakasih.</h3>
            <small id="passwordHelpBlock" class="form-text text-muted pt-4">
                @if (Helper::isMobile())  
                   <a href="danasyariah://login/borrower"> <i class="fas fa-arrow-left pr-2"></i> Kembali ke halaman Utama </a>
                @else 
                   <a href="/#modal_login_borrower"> <i class="fas fa-arrow-left pr-2"></i> Kembali ke halaman Utama </a>
                @endif 
            </small>
        -->
        </div>
    </div>
    <br><br><br><br>
</div>
@endsection