@component('component.mail_analyst')
    
    @slot('perihal')
        Surat Pemberitahuan Pembayaran Cicilan {{$data['no_invoice']}}
    @endslot
    @slot('content')
        <div>
            Hai : {{$data['nama_analis']}},
        </div>
        <div>
            Berikut informasi transaksi pembayaran cicilan :
        </div>
        <div>
            <b>Nomor Invoice</b> : {{$data['no_invoice']}} <br>
            <b>Pendanaan</b> : {{$data['pendanaan']}} <br>
            <b>Penerima Pendanaan</b>: {{$data['nama_pengguna']}} <br>
            <b>Tanggal Pembayaran</b> : {{$data['tgl_bayar']}} <br>
        </div>
        
        <p>
            Segerea lakukan konfirmasi pembayaran cicilan tersebut dalam waktu 1 x 24 jam (hari kerja).
        </p>

    @endslot
@endcomponent