@component('component.mail_analyst')
    
    @slot('perihal')
        Penggalangan Dana Selesai Pendanaan {{$data['nama_pendanaan']}}
    @endslot
    @slot('content')
        <div>
            Hai : {{$data['nama_pengguna']}},
        </div>
        <div>
            Penggalangan dana untuk {{$data['nama_pendanaan']}} telah selesai dilakukan dengan detail sebagai berikut :
        </div>
        <div>
            <b>Nama Pendanaan</b> : {{$data['nama_pendanaan']}} <br>
            <b>Dana Dibutuhkan</b>: {{$data['dana_dibutuhkan']}} <br>
            <b>Dana Terkumpul</b> : {{$data['dana_terkumpul']}} <br>
            <b>Data Pendana</b>   : <br>
            <table border='1'>
                <tr>
                    <td><b>No</b></td>
                    <td><b>Nama Pendana</b></td>
                    <td><b>Nominal Pendanaan</b></td>
                </tr>
                <?php $i=1;foreach($data['data_pendana'] as $dataPendana){?>
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$dataPendana->nama_investor}}</td>
                        <td>{{$dataPendana->total_dana}}</td>
                    </tr>
                <?php }?>
            </table>
            <br>
        </div>
        
        <p>
            Klik <a href="{{config('app.url')}}/borrower/beranda/"> disini </a> untuk melakukan pencairan dana. Demikian informasi iini disampaikan semoga bermanfaat.<br>

           <!--  Jika ada informasi yang kurang jelas dan perlu ditanyakan silahkan hubungi kami melalui email admin@danasyariah.id atau melalui telepon di +62 (21) 508-58821.<br> -->
        </p>

        <p>
            <!-- Hormat Kami,<br><br>
            PT. Dana Syariah Indonesia -->
        </p>
    @endslot
@endcomponent