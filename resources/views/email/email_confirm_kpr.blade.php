@component('component.mail')
    
    @slot('perihal')
        Aktivasi Akun Anda di
    @endslot

    @slot('content')
        <h3>Assalamualaikum Pengguna Dana Syariah</h3>
        <p>
            Anda telah mendaftarkan sebagai pengguna Dana Syariah dengan username : <br/> <b>{{$data->email}}</b>.
        </p>
        <p>
            Silahkan klik link di bawah untuk menyelesaikan pendaftaran.
        </p>

        <p>{{config('app.url')}}/{{('user_kpr/confirm-email')}}/{{$data->email_verif}}</p> 
    {{-- @endif --}}
        
    @endslot
@endcomponent