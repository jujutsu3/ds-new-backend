@component('component.mail_analyst')
    
    @slot('perihal')
        <div>
            Hai {{$data->nama}},
        </div>
        <div>
            Pembayaran Cicilan {{$data->no_invoice}} {{$data->status}}. Berikut Informasi Transaksi Pembayaran Cicilan :
        </div>
    @endslot
    @slot('content')
        <div>
            Tanggal Pembayaran : {{$data->tgl_pembayaran}}
        </div>
        <div>
            Jenis Transaksi : Pembayaran Cicilan
        </div>
        <div>
            Jumlah : Rp. {{number_format($data->nominal_tagihan_perbulan)}}
        </div>
        <div>
            Tanggal Konfirmasi : {{$data->tgl_konfirmasi}}
        </div>
        <div>
            Status Transaksi: {{$data->status_pembayaran}}
        </div>
        <div>
            Keterangan: {{$data->keterangan ? $data->keterangan : '-'}}
        </div>

        <p>
            Silahkan menyimpan Email ini sebagai referensi dari transaksi Anda. Demikian informasi ini disampaikan semoga bermanfaat.
        </p>
    @endslot
@endcomponent