@component('component.mail_analyst')
    
    @slot('perihal')
        <div>
            Hai : {{$data2->firstname}} {{$data2->lastname}},
        </div>
        <div>
            Pengajuan Pendanaan {{$data->pendanaan_nama}} telah diverifikasi oleh Tim Analis PT. Dana Syariah Indonesia dengan hasil sebagai berikut :
        </div>
    @endslot
    @slot('content')
        <div>
            Nama Pendanaan : {{$data->pendanaan_nama}}
        </div>
        <div>
            Penerima Pendanaan : {{$data->nama}}
        </div>
        <div>
            Dana Dibutuhkan : Rp. {{number_format($data->pendanaan_dana_dibutuhkan)}}
        </div>
        <div>
            Tanggal Pengajuan : {{$data->created_at}}
        </div>
        <div>
            Skor Pefindo : {{$data->nilai}}
        </div>
        <div>
            Skor DSI : {{$data->scorring_nilai}}
        </div>
        <div>
            Grade : {{$data->scorring_judul}}
        </div>
        <div>
            Keterangan : {{$data->keterangan ? $data->keterangan : '-'}}
        </div>

        <p>
            Segera berikan persetujuan Anda dengan klik <a href="{{config('app.url')}}/admin/borrower/view_persetujuan_pendanaan">disini</a>. Demikian informasi ini disampaikan semoga bermanfaat.
        </p>
    @endslot
@endcomponent