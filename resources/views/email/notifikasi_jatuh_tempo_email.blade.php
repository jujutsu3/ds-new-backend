@component('component.mail_analyst')
    
    @slot('perihal')
        Tagihan Jatuh Tempo (Subjek)
    @endslot
    @slot('content')
        <div>
            Hai {{$data['nama_pengguna']}},
        </div>
        <div>
            Anda memiliki tagihan jatuh tempo hari ini {{$data['tanggal']}} dengan detail sebagai berikut : <br>
        </div>
        <div>
            <b>No. Invoice</b> : {{$data['no_invoice']}} <br>
            <b>Nama Pendanaan</b>: {{$data['nama_pendanaan']}} <br>
            <b>Tanggal Jatuh Tempo</b> : {{$data['tgl_jatuh_tempo']}}<br>
            <b>Total Tagihan</b> : {{$data['total_tagihan']}} <br>
        </div>
        
        <p>
            Silahkan melakukan pembayaran melalui aplikasi penerima pendanaan Dana Syariah atau klik <a href='{{config('app.url')}}/borrower/beranda/'>disini</a> .<br>
        </p>
            <!-- Demikian inomrasi ini dilakukan semoga bermanfaat.
            
            Jika ada informasi yang kurang jelas dan perlu ditanyakan silahkan hubungi kami melalui email admin@danasyariah.id atau melalui telepon di +62 (21) 508-58821.<br>
        </p>

        <p>
            Hormat Kami,<br><br>
            PT. Dana Syariah Indonesia
        </p> -->
    @endslot
@endcomponent