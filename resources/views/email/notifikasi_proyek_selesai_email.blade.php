@component('component.mail_analyst')
    
    @slot('perihal')
        Proyek Pendanaan {{$data['nama_pendanaan']}} Selesai
    @endslot
    @slot('content')
        <div>
            Hai : {{$data['nama_penerima_pendanaan']}},
        </div>
        <div>
            Proyek {{$data['nama_pendanaan']}} telah selesai dilakukan dengan detail sebagai berikut :<br>
        </div>
        <div>
            <b>Nama Pendanaan</b> : {{$data['nama_pendanaan']}} <br>
            <b>Nama Penerima Pendanaan</b>: {{$data['nama_penerima_pendanaan']}} <br>
            <b>Total Pendanaan</b> : {{$data['total_pendanaan']}} <br>
            <b>Tanggal Mulai</b> : {{$data['tanggal_mulai']}} <br>
            <b>Tanggal Selesai</b> : {{$data['tanggal_selesai']}} <br>
        </div>
        
        <p>
            Demikian informasi ini dilakukan semoga bermanfaat.<br>

            <!-- Jika ada informasi yang kurang jelas dan perlu ditanyakan silahkan hubungi kami melalui email admin@danasyariah.id atau melalui telepon di +62 (21) 508-58821.<br> -->
        </p>

        <p>
            <!-- Hormat Kami,<br><br>
            PT. Dana Syariah Indonesia -->
        </p>
    @endslot
@endcomponent