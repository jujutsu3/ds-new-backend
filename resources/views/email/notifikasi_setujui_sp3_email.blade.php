@component('component.mail_analyst')
    
    @slot('perihal')
        Surat Persetujuan dan Penawaran Pendanaan (SP3) Disetujui 
    @endslot
    @slot('content')
        <div>
            Hai : {{$data['nama_analis']}},
        </div>
        <div>
            Surat Persetujuan dan Penawaran Pendanaan (SP3) telah disetujui oleh <b>{{$data['nama_pengguna']}}</b>. Penggalangan dana telah dimulai dengan detail sebagai berikut :<br>
        </div>
        <div>
            <b>Pendanaan</b> : {{$data['pendanaan']}} <br>
            <b>Dana Dibutuhkan</b>: {{$data['dana_dibutuhkan']}} <br>
            <b>Imbal Hasil</b> : {{$data['imbal_hasil']}} % <br>
            <b>Harga Paket</b> : {{$data['harga_paket']}} <br>
            <b>Tanggal Mulai Penggalangan</b> : {{$data['tgl_mulai_penggalangan']}} <br>
            <b>Tanggal Selesai Penggalangan</b> : {{$data['tgl_selesai_penggalangan']}} <br>
        </div>
        
        <p>
            Lihat penggalangan dana <a href='{{config('app.url')}}/admin/dashboard/'>disini</a>. Demikian informasi ini disampaikan semoga bermanfaat.<br>

            <!-- Jika ada informasi yang kurang jelas dan perlu ditanyakan silahkan hubungi kami melalui email admin@danasyariah.id atau melalui telepon di +62 (21) 508-58821.<br> -->
        </p>

        <!-- <p>
            Hormat Kami,<br><br>
            PT. Dana Syariah Indonesia
        </p> -->
    @endslot
@endcomponent