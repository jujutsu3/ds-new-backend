@component('component.mail_analyst')
    
    @slot('perihal')
        <div>
            Hai : {{$data_analis->firstname}} {{$data_analis->lastname}}
        </div>
        <div>
            Pengajuan Pendanaan {{$data->pendanaan_nama}} telah dikirim, segera hubungi calon penerima pendanaan untuk melakukan verifikasi dan penilaian pendanaan.
        </div>
    @endslot
    @slot('content')
		<div>
			<b> Detail Pendanaan </b>
		</div>
        <div>
			<img src="{{url("storage/$data->gambar_utama")}}">
		</div>
	   <div>
            Tipe Pendanaan : {{$data->tipe_pendanaan_nama}}
        </div>
        <div>
            Nama Pendanaan : {{$data->pendanaan_nama}}
        </div>
        <div>
            Jumlah Dana : Rp.{{number_format($data->pendanaan_dana_dibutuhkan)}}
        </div>
        <div>
            Imbal Hasil yang ditawarkan : {{$data->estimasi_imbal_hasil}}%
        </div>
        <div>
            Estimasi Mulai Proyek : {{$data->estimasi_mulai}}
        </div>
         <div>
            Akad : {{$data->pendanaan_akad}}
        </div>
        <div>
            Lama Proyek (temor) : {{$data->durasi_proyek}} Bulan
        </div>
        <div>
            Lokasi Proyek : {{$data->lokasi_proyek ? $data->lokasi_proyek : '-'}}
        </div>
		<div>
            Deskripsi Proyek : {{$data->keterangan ? $data->keterangan : '-'}}
        </div>
        <p>
            Demikian informasi ini disampaikan semoga bermanfaat.
        </p>
		<p>
            Jika ada informasi yang kurang jelas dan perlu ditanyakan silahkan hubungi kami melalui email admin@danasyariah.id atau telpon di 021 xxx xxxxx. Terima Kasih
        </p>
		<p style="float:left">
            Hormat Kami,
        </p>
		<p style="float:left">
            PT. Dana Syariah Indonesia,
        </p>
    @endslot
@endcomponent