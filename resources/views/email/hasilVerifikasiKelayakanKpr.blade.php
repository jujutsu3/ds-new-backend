@component('component.mailVerif')

@slot('perihal')
STATUS PENGAJUAN DANA RUMAH - DANA SYARIAH
@endslot
@slot('content')
<h3>Assalamualaikum warahmatullahi wabarakatuh.</h3>
<p>
    Hai <b> {{$penerimaPendanaan->nama_pengguna}}</b>
</p>
<br>
<p>
    Data Pengajuan tahap I Dana Rumah sudah selesai diverifikasi, dengan detail sebagai berikut :
</p>
<br>
<p>
    <b>Nama Pendanaan</b> : {{$penerimaPendanaan->nama_pendanaan}}<br>
    <b>Penerima Pendanaan</b> : {{$penerimaPendanaan->nama_pengguna}}<br>
    <b>Dana Dibutuhkan</b> : </b>Rp.{{number_format($penerimaPendanaan->dana_dibutuhkan, 2, ",", ".")}}<br>
    <b>Tanggal Pengajuan</b> : {{$penerimaPendanaan->tanggal_pengajuan}}<br>
    <b>Keterangan</b> :
    <?php
    if ($penerimaPendanaan->status == 1) {
        echo "[VERIFIKASI TAHAP I  - Pengajuan Pendanaan Anda sudah LOLOS VERIFIKASI TAHAP I, silakan melanjutkan proses ke tahap selanjutnya]";
    } else {
        echo "[VERIFIKASI TAHAP I  - Mohon maaf pengajuan pendanaan Anda tidak dapat dipenuhi]";
    }
    ?>
    <br>
    <?php

    if ($penerimaPendanaan->status == 1) {
        echo "<br>"; 
        echo "<br>";
        echo "Untuk proses selanjutnya mohon lengkapi data dan dokumen pendukung pengajuan melalui link berikut ini : ";
        echo "<a href='https://www.danasyariah.id'>di sini</a>";
        echo "<br>";
        echo "<br>"; 
        echo "Jika ada pertanyaan lebih lanjut, silakan hubungi contact center resmi kami melalui nomor telepon 1500-091";

    } 

    echo "<br>";
    echo "<br>";
    echo "Demikian informasi ini disampaikan.";
    ?>
    <br>
    <br>
    Terima Kasih
</p>
<p>
    <br>
    Wassalamualaikum warahmatullah wabarakatuh
    <br>
    Hormat Kami<br>
    PT Dana Syariah Indonesia
</p>
@endslot
@endcomponent