@component('component.mail_analyst')
    
    @slot('perihal')
        <div>
            Hai : {{ $email_ke == 'borrower' ? $data->nama : $data2->firstname.' '.$data2->lastname}},
        </div>
        <div>
            Pengajuan Pendanaan {{$data->pendanaan_nama}} dengan detil sebagai berikut :
        </div>
    @endslot
    @slot('content')
        <div>
            Nama Pendanaan : {{$data->pendanaan_nama}}
        </div>
        <div>
            Tanggal Pengajuan : {{$data->created_at}}
        </div>
        <div>
            Dana Dibutuhkan : Rp. {{number_format($data->pendanaan_dana_dibutuhkan)}}
        </div>
        <div>
            Status : {{$data->status}}
        </div>
        <div>
            Keterangan : {{$data->keterangan ? $data->keterangan : '-'}}
        </div>

        <p>
            Demikian informasi ini disampaikan semoga bermanfaat.
        </p>
    @endslot
@endcomponent