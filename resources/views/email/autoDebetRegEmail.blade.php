@component('component.mailVerif')

@slot('perihal')
<?php
if ($dataReg['flag'] == 0) {
    $judul = "AUTO DEBET REGISTRASI PRIVY - DANA SYARIAH";
    $textPengantar = "Proses Auto Debet Registrasi Privy Anda Berhasil. dengan rincian sebagai berikut :";
    $labelTgl = "Tanggal Registrasi";
} else {
    $judul = "AUTO DEBET TandaTangan PRIVY - DANA SYARIAH";
    $textPengantar = "Proses Auto Debet TandaTangan Privy Anda Berhasil. dengan rincian sebagai berikut :";
    $labelTgl = "Tanggal TandaTangan Digital";
} ?>

{{$judul}}
@endslot
@slot('content')
<h3>Assalamualaikum warahmatullahi wabarakatuh.</h3>
<p>
    Hai <b> {{$dataReg['nama_investor']}}</b>
</p>
<br>
<p>
    {{$textPengantar}}
</p>
<br>
<p>
    <b>Nama Pendana</b> : {{$dataReg['nama_investor']}}<br>
    <b>{{$labelTgl}}</b> : {{$dataReg['tglReg']}}<br>
    <b>Nominal</b> : {{$dataReg['nominal']}}<br>
    <b>Keterangan</b> : Auto Debet Dari Imbal Hasil Proyek Tanggal {{$dataReg['tgl_autoDebet']}}
    <br>
    <br>
    Terima Kasih
</p>
<p>
    <br>
    Wassalamualaikum warahmatullah wabarakatuh
    <br>
    Hormat Kami<br>
    Tim Dana Syariah
</p>
@endslot
@endcomponent