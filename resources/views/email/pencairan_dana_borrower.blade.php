@component('component.mail_analyst')
    
    @slot('perihal')
        <div>
            Hai {{ $email_ke == 'borrower' ? $data->nama : $data2->firstname.' '.$data2->lastname}},
        </div>
        <div>
            Permintaan Pencairan dana dengan detil sebagai berikut :
        </div>
    @endslot
    @slot('content')
        <div>
            Nama Penerima Pendanaan : {{$data->nama}}
        </div>
        <div>
            Nomor Akad : {{$data->no_akad_bor.'/AMRB/DSI/'.$data->bln_akad_bor.'/'.$data->thn_akad_bor}}
        </div>
        <div>
            Tanggal Akad : {{$data->created_at}}
        </div>
        <div>
            Jumlah Dana : Rp.{{number_format($data->nominal_pencairan)}}
        </div>
        <div>
            No.Rekening : {{$data->brw_norek}}
        </div>
        <div>
            Nama Pemilik Rekening : {{$data->brw_nm_pemilik}}
        </div>
        <div>
            Nama Bank : {{$data->nama_bank}}
        </div>
        <div>
            Status : {{$data->status}}
        </div>
        <div>
            Keterangan : {{$data->status == 'Disetujui' ? '-' : $data->keterangan}}
        </div>

        <p>
            *waktu pencairan dana harus sama dengan tanggal akad.
        </p>
        <p>
            Demikian informasi ini disampaikan semoga bermanfaat.
        </p>
    @endslot
@endcomponent