@component('component.mail_analyst')
    
    @slot('perihal')
        <div>
            Hai : {{$data->nama}},
        </div>
        <div>
            Pengajuan Pendanaan {{$data->pendanaan_nama}} dengan detil sebagai berikut :
        </div>    @endslot
    @slot('content')
        <div>
            Nama Pendanaan : {{$data->pendanaan_nama}}
        </div>
        <div>
            Tanggal Pengajuan : {{$data->created_at}}
        </div>
        <div>
            Dana Dibutuhkan : Rp. {{number_format($data->pendanaan_dana_dibutuhkan)}}
        </div>
        <div>
            Status : {{$data->status}}
        </div>
        <div>
            Keterangan : {{$data->keterangan ? $data->keterangan : '-'}}
        </div>

        <p>
            Penggalangan dana hanya akan dilakukan setelah anda menyetujui Surat Persetujuan dan Penawaran Pendanaan(SP3). Untuk menyetujui SP3 silahkan klik <a href="{{config('app.url')}}/borrower/sppp/{{$data->brw_id}}">disini</a>.
        </p>

        <p>
            Demikian informasi ini disampaikan semoga bermanfaat.
        </p>
    @endslot
@endcomponent