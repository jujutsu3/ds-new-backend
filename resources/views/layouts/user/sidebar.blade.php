<!DOCTYPE html>
<html lang="en">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Google Tag Manager -->
<script>
  (function(w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
      'gtm.start': new Date().getTime(),
      event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0],
      j = d.createElement(s),
      dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src =
      'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  })(window, document, 'script', 'dataLayer', 'GTM-N6RKMM6');
</script>
<!-- End Google Tag Manager -->

<head>
  @include('includes.user.head')
  <title>@yield('title')</title>
  @yield('style')
</head>

<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N6RKMM6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="wrapper">
    @include('includes.user.sidebar_nav')
    <div id="content">
      <div class="container-fluid" id="body_container">
        @include('includes.user.navbar')
        <div class="py-5 px-3" id="content_body">
          @yield('content')
        </div>
      </div>
      {{-- <div id="footer_container">
        @include('includes.footer')
      </div> --}}
    </div>

  </div>

  <script type="text/javascript" src="{{ asset('/js/user/controller.js') }}"></script>
  <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
  @yield('script')
  <script>
    function CopyText(btnid) {
      var copyText = $(btnid).val();

      var textarea = document.createElement('textarea');
      textarea.id = 'temp_element';
      textarea.style.height = 0;
      document.body.appendChild(textarea);
      textarea.value = copyText;
      var selector = document.querySelector('#temp_element')
      selector.select();
      document.execCommand('copy');
      document.body.removeChild(textarea);
      swal.fire('No Virtual Account Tersalin');
    }
  </script>
</body>

</html>