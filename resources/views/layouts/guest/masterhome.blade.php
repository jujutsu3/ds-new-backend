<!DOCTYPE html>
<html lang="id">

<head>
    <title>Dana Syariah</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id" content="437102301747-mc6atn6ljrtv6fanh7st1dcqmlp8gcc9.apps.googleusercontent.com">
    <meta name="description" content="pendanaan syariah, hijrah finansial">
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" >
    <link rel="apple-touch-icon" href="img/danasyariah-apple-icon.png">

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://cdn.jsdelivr.net" crossorigin>
    <link rel="preconnect" href="https://www.facebook.com">
    <link rel="dns-prefetch" href="https://connect.facebook.net">
    <link rel="preconnect" href="https://www.google.com">
    <link rel="dns-prefetch" href="https://www.googletagmanager.com">
    <link rel="dns-prefetch" href="https://www.googleadservices.com">
    <link rel="dns-prefetch" href="https://www.google-analytics.com">

    <link rel="preload" href="/img/logo4.png" as="image">
    <link rel="preload" href="/img/16092021---Hero-Header-DSI-min.png" as="image">
    <link rel="preload" href="/css/fonts/slick.woff" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/webfonts/fa-solid-900.woff2" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/webfonts/fa-brands-400.woff2" as="font" type="font/woff" crossorigin>
    <link rel="preload" href="/css/all.css" as="style">
    <link rel="preload" href="/js/allNew.js" as="script">
    <link rel="preload" href="/js/jquery.mCustomScrollbar.concat.min.js" as="script">
    <link rel="preload" href="/js/app.js" as="script">
    <link rel="preload" href="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js" as="script">
    <link rel="preload" href="https://unpkg.com/aos@2.3.1/dist/aos.js" as="script">
    <link rel="preload" href="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js" as="script">

    <script src="/js/slick.min.js" type="text/javascript" defer></script>
    <script src="/js/jquery-3.3.1.min.js" type="text/javascript" async></script>

    <!-- chromeOpera -->
    <meta name="theme-color" content="#0F7851">
    <!-- Windows Phone browser -->
    <meta name="msapplication-navbutton-color" content="#0F7851">
    <!-- safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#0F7851">
    <!-- External CSS libraries -->
    <!-- chace control -->
    <meta http-equiv="Cache-control" content="public">
    <!--script src="https://apis.google.com/js/platform.js" async></script-->
    <link type="text/css" rel="stylesheet" href="/css/all.css">
    @yield('style')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142214442-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-142214442-1');
    </script>

    <!-- end of global site tag -->

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '444018075722130');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=444018075722130&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-N6RKMM6');
    </script>
    <!-- End Google Tag Manager -->
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N6RKMM6" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    @yield('navbar')

    @yield('body')

    <div class="modal fade" id="modalMd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalMdTitle"></h4>
                </div>
                <div class="modal-body">
                    <div class="modalError"></div>
                    <div id="modalMdContent"></div>
                </div>
            </div>
        </div>
    </div>
    @include('includes.footerhome')

    <script async src="/js/allNew.js"></script>
    <script async src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- canvas three js -->
    <!--script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script-->
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.0/TweenMax.min.js"></script>
    <!-- Custom JS Script -->
    <script async src="/js/app.js"></script>
    <script async src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <script>
        // lazy load
        const observer = lozad(); // lazy loads elements with default selector as '.lozad'
        observer.observe();
        // lazy load end
    </script>

    {{-- ====================================== 23 Juni 2020 ( untuk Pengumuman Nasabah) ========================================= --}}

    <script>
        $(function() {
            $('.modal-notification').modal('show');
        });
    </script>


    @if ($errors->get('username') || $errors->get('email') || $errors->get('password') )
    <script defer>
        $(function() {
            $('#registerModal').modal('show')
        });
    </script>
    @endif

    {{-- <script defer>
        $(document).ready(function() {
            $('#newversionModal').modal('show')
        });
    </script> --}}
    @yield('script')

    <!--script type="text/javascript">
        $(document).ready(function() {
            //   slider v2
            var swiper = new Swiper('.blog-slider', {
                spaceBetween: 30,
                effect: 'fade',
                loop: true,
                mousewheel: {
                    invert: false,
                },

                // autoHeight: true,
                pagination: {
                    el: '.blog-slider__pagination',
                    clickable: true,
                }
            });
        });
        // SLideHome
        $next = 1; // fixed, please do not modfy;
        $current = 0; // fixed, please do not modfy;
        $interval = 4000; // You can set single picture show time;
        $fadeTime = 800; // You can set fadeing-transition time;
        $imgNum = 3; // How many pictures do you have

        $(document).ready(function() {
            //NOTE : Div Wrapper should with css: relative;
            //NOTE : img should with css: absolute;
            //NOTE : img Width & Height can change by you;
            $('.fadeImg').css('position', 'relative');
            $('.fadeImg img').css({
                'position': 'absolute'
            });

            nextFadeIn();
        });
        $('#overlay').modal('show');

        setTimeout(function() {
            $('#overlay').modal('hide');
        }, 10000);

        function nextFadeIn() {
            //make image fade in and fade out at one time, without splash vsual;
            $('.fadeImg img').eq($current).delay($interval).fadeOut($fadeTime)
                .end().eq($next).delay($interval).hide().fadeIn($fadeTime, nextFadeIn);

            // if You have 5 images, then (eq) range is 0~4 
            // so we should reset to 0 when value > 4; 
            if ($next < $imgNum - 1) {
                $next++;
            } else {
                $next = 0;
            }
            if ($current < $imgNum - 1) {
                $current++;
            } else {
                $current = 0;
            }
        };
        // End SLide Home
        // Video Popup
        $(document).ready(function() {

            // Gets the video src from the data-src on each button

            var $videoSrc;
            $('.video-btn').click(function() {
                $videoSrc = $(this).data("src");
            });
            // console.log($videoSrc);



            // when the modal is opened autoplay it  
            $('#myModal').on('shown.bs.modal', function(e) {

                // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
                $("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
            })



            // stop playing the youtube video when I close the modal
            $('#myModal').on('hide.bs.modal', function(e) {
                // a poor man's stop video
                $("#video").attr('src', $videoSrc);
            })

            // document ready  
        });
        // end video popup
        // $('#newversionModal').delay(10000).hide();
        // setTimeout(function() {
        //   $('#newversionModal').hide();
        // }, 5000);
    </script-->
<!--
    <script type="text/javascript">
        var PrimeTelcoIFrame = document.createElement("iframe");
        PrimeTelcoIFrame.setAttribute("src", "https://cc.ptdigital.co.id/cc_plugin/index.php?token=95e58e8df4ec3ae1c36d78e2fea55953"), PrimeTelcoIFrame.setAttribute("allow", "geolocation; microphone; camera"), PrimeTelcoIFrame.setAttribute("id", "PrimeTelcoPluginIFrame"), PrimeTelcoIFrame.setAttribute("allowfullscreen", "true"), PrimeTelcoIFrame.setAttribute("webkitallowfullscreen", "true"), PrimeTelcoIFrame.setAttribute("mozallowfullscreen", "true"), PrimeTelcoIFrame.setAttribute("scrolling", "no"), PrimeTelcoIFrame.setAttribute("frameborder", "0"), document.body.appendChild(PrimeTelcoIFrame);
        var PrimeTelcoPluginIFrame = document.getElementById("PrimeTelcoPluginIFrame");
        PrimeTelcoPluginIFrame.setAttribute('style', 'bottom:0;right:10px;position:fixed;z-index:99999;');
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent",
            eventer = window[eventMethod],
            messageEvent = "attachEvent" === eventMethod ? "onmessage" : "message";
        eventer(messageEvent, function(e) {
            var t = JSON.parse(e.data);
            var r = t["type"];
            for (key in t) {
                PrimeTelcoPluginIFrame && ((r == "css") ? PrimeTelcoPluginIFrame.style[key] = t[key] : PrimeTelcoPluginIFrame.setAttribute(key, t[key]))
            }
        });
    </script>
    -->

    <!-- Start of Qontak Webchat Script -->
    <script>
        const qontakId  = "{{ config('app.qontak_id') }}";
        const qontakCode  = "{{ config('app.qontak_code') }}";

        const qchatInit = document.createElement('script');
        qchatInit.src = "https://webchat.qontak.com/qchatInitialize.js";
        const qchatWidget = document.createElement('script');
        qchatWidget.src = "https://webchat.qontak.com/js/app.js";
        document.head.prepend(qchatInit);
        document.head.prepend(qchatWidget);
        qchatInit.onload = function () {qchatInitialize({ id: qontakId, code: qontakCode })};
    </script>
    <!-- End of Qontak Webchat Script -->
</body>

</html>