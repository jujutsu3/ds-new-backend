<div class="modal fade" id="loading_modal" tabindex="-1" role="dialog" aria-labelledby="loadingLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document" style="overflow-y: initial !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
              </div>
            <div class="modal-body p-0" style="max-height: calc(100vh - 200px);overflow-y: auto;">
                <div class="p-2 text-center">
                    <i data-feather="refresh-ccw" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                    <div class="text-3xl mt-2" id="loading_title"> @lang('verify.loading_title')</div>
                    <div class="text-gray-600 mt-2" id="loading_content">
                        @lang('verify.loading_content')
                    </div>
                </div>
            </div>
            <div id="loading_footer" class="modal-footer px-5 pb-8 text-center d-none">
                <button type="button" id="btnCloseLoading" data-dismiss="modal"
                    onclick="javascript:$(this).parent().addClass('hidden');$('#loading_title').html('{{ trans('verify.loading_title') }}');$('#loading_content').html('{{ trans('verify.loading_content') }}');"
                    class="btn btn-primary">
                    @lang('verify.btn_close')
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>