<div class="modal fade" id="verijelas_modal" tabindex="-1" role="dialog" aria-labelledby="verijelasLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document" style="max-width: 80%;overflow-y: initial !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
              </div>
              <div class="modal-body p-0" style="max-height: calc(100vh - 200px);overflow-y: auto;">
                <div class="p-2 text-center">
                    <i data-feather="refresh-ccw" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                    <div class="text-gray-600 mt-2" id="loading_content_data">
                    </div>
                </div>
            </div>
            <div id="loading_footer_data" class="modal-footer px-5 pb-8 text-center d-none">
                <button type="button" data-toggle="modal" data-target="#loading_modal"
                class="btn btn-success btnCheckNext d-none"  data-backdrop="static" data-keyboard="false"><i class="fa fa-check"
                aria-hidden="true"></i> Lanjut Cek</button>

                <button type="button" data-dismiss="modal"
                    onclick="javascript:$(this).parent().addClass('hidden');"
                    class="btn btn-primary" id="btnCloseHasil">
                    @lang('verify.btn_close')
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>