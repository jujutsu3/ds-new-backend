<div class="modal fade" id="form_kirim_pengajuan" tabindex="-1" role="dialog" aria-labelledby="editLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <form method="post" id="form_kirim_pengajuan" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="pengajuan_id" id="pengajuan_id" />
                <input type="hidden" name="brw_id" id="brw_id" />
                <input type="hidden" name="mode" id="mode" />
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title bg-primary text-white" id="editLabel">
                         Pengajuan Id : <span id="jsPengajuanId"></span>
                        </h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group col-sm-12">
                            {!! Form::label('Nama Penerima Pendanaan', 'Nama Penerima Pendanaan') !!}
                            {!! Form::text('pendanaan_nama', null, ['id'=>'pendanaan_nama', 'class'=>'form-control','readonly'=>true]) !!}

                        </div>
                            <div class="form-group col-sm-6">
                                {!! Form::label('Pendanaan', 'Pendanaan') !!}
                                {!! Form::text('pendanaan_info', null, ['id'=>'pendanaan_info', 'class'=>'form-control', 'readonly'=>true]) !!}
                            </div>

                            <div class="form-group col-sm-6">
                                {!! Form::label('Nilai Pengajuan', 'Nilai Pengajuan') !!}
                                {!! Form::text('nilai_pengajuan', null, ['id'=>'nilai_pengajuan', 'class'=>'form-control', 'readonly'=>true]) !!}

                            </div>

                        <div class="form-group col-sm-12">
                            {!! Form::label('Catatan', 'Catatan Rekomendasi:') !!}
                            {!! Form::textarea('catatan_rekomendasi', null, ['id' => 'catatan_rekomendasi', 'class' => 'form-control', 'rows' => 8, 'cols' => 40, 'required' => 'required']) !!}
                        </div>
            
                        <div class="form-group col-sm-12">
                            <label class="control-label" for="upload">Upload
                               File Rekomendasi *</label>
                              <input type="file" id="file_rekomendasi" name="file_rekomendasi" class="form-control jsFileRekomendasi" accept=".pdf" />
                              <span id="view_file_rekomendasi" class="form-control"></span>
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary" id="btnKirimPengajuan"> <i class="fa fa-paper-plane icon"
                                    style="font-size:18px"></i> Kirim</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
            </form>
        </div>
    </div>