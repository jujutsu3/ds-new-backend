{{-- START: Modal Informasi Brw --}}
<div class="modal fade" id="{{ $modal_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Detail Informasi Penerima Pendanaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col py-1">
                        <img class="img-fluid rounded img-thumbnail"
                            src="{{ !empty($brw_pic) ? route('getUserFileForAdmin', ['filename' => str_replace('/',':', $brw_pic)]) .'?t='.date("Y-m-d h:i:sa") : '' }}" height="160" width="160" alt="">
                    </div>
                    <div class="col py-1">
                        <p class="font-weight-normal text-dark mb-0">Nama : </p>
                        <p class="">{{ $brw_nama }}</p>
                        <p class="font-weight-normal text-dark mb-0">NIK : </p>
                        <p class="">{{ $brw_nik }}</p>
                        <p class="font-weight-normal text-dark mb-0">Jenis Kelamin : </p>
                        <p class="">{{ $brw_jns_kelamin == 1 ? 'Laki - Laki' : 'Perempuan' }}</p>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
{{-- END: Modal Informasi Brw --}}