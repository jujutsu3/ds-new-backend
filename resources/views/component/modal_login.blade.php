<div id="{{$modal_id}}" class="modal fade in modal_scroll" role="dialog" >
  <div class="modal-dialog modal-lg modal-dialog-centered ">
    <!-- Modal content-->
    <div class="modal-content modal-body">
      <!--div class="modal-body"-->
        <div class="row no-gutters">   
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="login100-more" style="background-image: url('img/bg-login.jpg');">
                <div class="d-flex ">
                  <div class="p-2 mx-auto text-white p-5 pt-2">
                    <img class="logo-render lozad" data-src="/img/logo_only_white.png" alt="">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="content-form-box forgot-box ">
                    <div class=" container-fluid ">
                        <div>
                            <!--<h5>Hi, Kawan..</h5>-->
                            <label>Silahkan pilih untuk melanjutkan sebagai: </label>
                            <hr>
                          <button type="button" class="close ml-5 mr-5 pr-2 mt-3 text-dark" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true"><i class="lni-close size-sm"></i> </span>
                          </button>
                        </div>
                    </div>
                    <div class="container-fluid">                    
                    {{$modal_content}}
                    </div>
                </div>
            </div>
        </div>
      <!--/div-->
    </div>
  </div>
</div>




