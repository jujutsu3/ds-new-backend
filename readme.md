# Danasyariah Backend
 Dana Syariah Backend menggunakan Laravel framework
 - PHP 8.1.12
 - Laravel 9.19
 - Database MariaDb/Mysql (versi 10.4.*)
 - Server OS Ubuntu 22.04 (paling kompatibel)
 - Local Dev Environment tested (MacOS BigSur, Ubuntu..)
 
## Standar Modul OS: 
- openssl 
- php-bcmath 
- php-curl 
- php-json 
- php-mbstring 
- php-mysql 
- php-tokenizer 
- php-xml 
- php-zip 
- php-gd

## Langkah Instalasi
Disesuaikan dengan environment OS, bisa menggunakan Ubuntu atau CentOS
- Install modul standar OS
- Install PHP 8.1.12
- Install MariaDB; lalu konfigurasi config database dengan user dan password yang sudah dibuat. Dump database di ```db/*.sql``` ke database
- Install Composer; lalu masuk ke direktori root project dan jalankan ```composer install```
- Jalankan aplikasi ```php artisan serve``` lalu panggil dari [localhost:3000](http://localhost:3000)

Untuk beberapa kasus diperlukan clear pada temporary data:
```
php artisan route:clear
php artisan cache:clear
php artisan config:clear
php artisan view:clear
php artisan key:generate
```
